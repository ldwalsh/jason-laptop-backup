﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="ProviderAnalysisAdministration.aspx.cs" Inherits="CW.Website.ProviderAnalysisAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/Analyses/nonkgs/ViewAnalyses.ascx" tagname="ViewAnalyses" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Analyses/nonkgs/BuildingVariables.ascx" tagname="BuildingVariables" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Analyses/nonkgs/EquipmentVariables.ascx" tagname="EquipmentVariables" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Analyses/nonkgs/InputPointTypes.ascx" tagname="InputPointTypes" tagprefix="CW" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
               	
  <h1>Provider Analysis Administration</h1>

  <div class="richText">The provider administration area is used to view analyses, analysis equipment variables, and analysis input and output types.</div>
                                           
  <div class="updateProgressDiv">
    <asp:UpdateProgress ID="updateProgressTop" runat="server">
      <ProgressTemplate>
        <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
      </ProgressTemplate>
    </asp:UpdateProgress>  
  </div>

  <div class="administrationControls">                                
    <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage">
      <Tabs>
        <telerik:RadTab Text="View Analyses" />
        <telerik:RadTab Text="View Building Vars." />
        <telerik:RadTab Text="View Equipment Vars." />
        <telerik:RadTab Text="View Input Point Types" />
      </Tabs>
    </telerik:RadTabStrip>

    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

      <telerik:RadPageView ID="RadPageView1" runat="server">                        
        <CW:ViewAnalyses ID="ViewAnalyses" runat="server" />
      </telerik:RadPageView>
      
      <telerik:RadPageView ID="RadPageView2" runat="server">                        
        <CW:BuildingVariables ID="BuildingVariables" runat="server" />
      </telerik:RadPageView>
      
      <telerik:RadPageView ID="RadPageView3" runat="server">                        
        <CW:EquipmentVariables ID="EquipmentVariables" runat="server" />
      </telerik:RadPageView>
       
      <telerik:RadPageView ID="RadPageView4" runat="server">                        
        <CW:InputPointTypes ID="InputPointTypes" runat="server" />
      </telerik:RadPageView>
            
    </telerik:RadMultiPage>
  </div>

</asp:Content>