﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Helpers;
using CW.Data.Models.DataTransferService;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class SystemServiceAdministration : SitePage
    {
        #region Properties

            private DataTransferService mService;
            const string addServiceSuccess = " service addition was successful.";
            const string addServiceFailed = "Adding service failed. Please contact an administrator.";
            const string addServiceExists = "Cannot add service because the name already exists.";
            const string serviceExists = "Service with that name or id already exists.";
            const string updateSuccessful = "Service update was successful.";
            const string updateFailed = "Service update failed. Please contact an administrator.";
            const string updateServiceExists = "Cannot update service name because the name already exists.";
            const string deleteSuccessful = "Service deletion was successful.";
            const string deleteFailed = "Service deletion failed. Please contact an administrator.";
            const string associatedToDataSource = "Cannot delete retrieval method becuase a data source requires it.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "DataTransferServiceName";
            const string serviceRetrievalMethodExists = "Service with provided RetrievalMethodID already exists.";
            const string configRequired = "Config content is required.";
            protected string selectedValue;

            static CharHelper.CharSets StripCharSets = CharHelper.CharSets.Tab | CharHelper.CharSets.NewLine;

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion
        
        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs system admin.
                if (!siteUser.IsKGSSystemAdmin)
                    Response.Redirect("/Home.aspx");        

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindServices();
                    BindRetrievalMethods(ddlEditRetrievalMethod, ddlAddRetrievalMethod);
                    BindBMSInfo(new[] { ddlAddBMSInfo, ddlEditBMSInfo }, siteUser.CID);
                    BindServiceKeys(new[] {ddlAddServiceKey, ddlEditServiceKey}, siteUser.CID);
                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Load and bind fields

            /// <summary>
            /// Binds a dropdown list of retrieval methods
            /// </summary>
            /// <param name="ddl"></param>
            private void BindRetrievalMethods(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<DataSourceRetrievalMethod> methods = DataMgr.DataSourceRetrievalMethodDataMapper.GetAllRetrievalMethods();

                ddl.DataTextField = "RetrievalMethod";
                ddl.DataValueField = "RetrievalMethodID";
                ddl.DataSource = methods;
                ddl.DataBind();

                ddl2.DataTextField = "RetrievalMethod";
                ddl2.DataValueField = "RetrievalMethodID";
                ddl2.DataSource = methods;
                ddl2.DataBind();
            }

        
            /// <summary>
            /// Binds a dropdown list of service keys by cid
            /// </summary>
            /// <param name="ddl"></param>
            private void BindServiceKeys(IEnumerable<DropDownList> ddlList, int cid)
            {
                foreach (var ddl in ddlList)
                {
                    ListItem lItem = new ListItem();
                    lItem.Text = "Select one...";
                    lItem.Value = "-1";
                    lItem.Selected = true;

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);
                    ddl.DataTextField = "ServiceKeyName";
                    ddl.DataValueField = "SKID";
                    ddl.DataSource = DataMgr.ServiceKeyDataMapper.GetAllServiceKeysByCID(cid);
                    ddl.DataBind();
                }
            }

            private void BindBMSInfo(IEnumerable<DropDownList> ddlList, int cid)
            {
                IEnumerable<BMSInfo> methods = DataMgr.BMSInfoDataMapper.GetBMSInfoByClientID(null, siteUser.CID);

                foreach (var ddl in ddlList)
                {
                    ListItem lItem = new ListItem();
                    lItem.Text = "Select one...";
                    lItem.Value = "-1";
                    lItem.Selected = true;

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);
                    ddl.DataTextField = "BMSInfoName";
                    ddl.DataValueField = "BMSInfoID";
                    ddl.DataSource = methods;
                    ddl.DataBind();
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetServiceIntoEditForm(DataTransferService service)
            {
                //ID
                hdnEditID.Value = Convert.ToString(service.DTSID);
                //Service Name
                txtEditDataTransferServiceName.Text = service.DataTransferServiceName;
                //Service GUID
                txtEditDataTransferServiceGUID.Text = service.DataTransferServiceGuid;
                //Service Key
                ddlEditServiceKey.SelectedValue = Convert.ToString(service.SKID);
                //Retrieval Method
                ddlEditRetrievalMethod.SelectedValue = Convert.ToString(service.RetrievalMethodID);
                //BMS Info
                ddlEditBMSInfo.SelectedValue = service.BMSInfoID == null ? "-1" : Convert.ToString(service.BMSInfoID);
                //Config content
                txtEditConfigContent.Text = Server.HtmlDecode(service.ConfigContent);
                //IP list
                txtEditIPList.Text = service.IPList;
                //Is Active
                chkEditActive.Checked = service.IsActive;
            }

        #endregion

        #region Load Service

            protected void LoadAddFormIntoService(DataTransferService service)
            {
                //Service Name
                service.DataTransferServiceName = txtAddDataTransferServiceName.Text;       
                //SKID
                service.SKID = Convert.ToInt32(ddlAddServiceKey.SelectedValue);
                //Retrieval method
                service.RetrievalMethodID = Convert.ToInt32(ddlAddRetrievalMethod.SelectedValue);
                //Config content
                service.ConfigContent = Server.HtmlEncode(CharHelper.StripCharSets(txtAddConfigContent.Text.Trim(), StripCharSets));
                //IP list
                service.IPList = txtAddIPList.Text;
                //Is Active
                service.IsActive = true;
                //BMS Info
                service.BMSInfoID = ddlAddBMSInfo.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddBMSInfo.SelectedValue) : null;
                service.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoService(DataTransferService service)
            {
                //ID
                service.DTSID = Convert.ToInt32(hdnEditID.Value);
                //Vendor Product Name
                service.DataTransferServiceName = txtEditDataTransferServiceName.Text;
                //Service GUID
                service.DataTransferServiceGuid = txtEditDataTransferServiceGUID.Text;
                //BMS Info
                service.BMSInfoID = ddlEditBMSInfo.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditBMSInfo.SelectedValue) : null;
                //SKID
                service.SKID = Convert.ToInt32(ddlEditServiceKey.SelectedValue);
                //Retrieval method
                service.RetrievalMethodID = Convert.ToInt32(ddlEditRetrievalMethod.SelectedValue);
                //Config Content
                service.ConfigContent = Server.HtmlEncode(CharHelper.StripCharSets(txtEditConfigContent.Text.Trim(), StripCharSets));
                //IP List
                service.IPList = txtEditIPList.Text;
                //Is Active
                service.IsActive = chkEditActive.Checked;
            }

        #endregion

        #region Dropdown Events



            #endregion

        #region Button Events

            /// <summary>
            /// Add service Button on click.
            /// </summary>
            protected void addServiceButton_Click(object sender, EventArgs e)
            {
                mService = new DataTransferService();

                //load the form into the service
                LoadAddFormIntoService(mService);

                // assign Data Transfer Service ID, guid without the formatting, just the number.
                mService.DataTransferServiceGuid = Guid.NewGuid().ToString("N");

                if (DataMgr.DataTransferServiceDataMapper.DoesServiceNameOrGUIDExist(mService.DataTransferServiceName, mService.DataTransferServiceGuid))
                {
                    LabelHelper.SetLabelMessage(lblAddError, serviceExists, lnkSetFocusAdd);
                    return;
                }

                if (String.IsNullOrEmpty(mService.ConfigContent))
                {
                    LabelHelper.SetLabelMessage(lblAddError, configRequired, lnkSetFocusAdd);
                    return;
                }

                try
                {
                    string xmlValidationError = XmlHelper.ValidateXml(Server.HtmlDecode(mService.ConfigContent));
                    if (!String.IsNullOrWhiteSpace(xmlValidationError))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, xmlValidationError, lnkSetFocusAdd);
                        return;
                    }

                    //insert new service
                    DataMgr.DataTransferServiceDataMapper.InsertService(mService);

                    LabelHelper.SetLabelMessage(lblAddError, mService.DataTransferServiceName + addServiceSuccess, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding service config.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addServiceFailed, lnkSetFocusAdd);
                }
            }


            /// <summary>
            /// Update service Button on click. Updates retrieval method data.
            /// </summary>
            protected void updateServiceButton_Click(object sender, EventArgs e)
            {
                var mService = new DataTransferService();

                //load the form into the service
                LoadEditFormIntoService(mService);

                if (String.IsNullOrEmpty(mService.ConfigContent))
                {
                    LabelHelper.SetLabelMessage(lblEditError, configRequired, lnkSetFocusView);
                    return;
                }

                if (DataMgr.DataTransferServiceDataMapper.DoesServiceNameOrDTSGUIDExist(mService.DTSID, mService.DataTransferServiceName, mService.DataTransferServiceGuid))
                {
                    LabelHelper.SetLabelMessage(lblEditError, serviceExists, lnkSetFocusView);
                    return;
                }

                //try to update the service    
                try
                {
                    string xmlValidationError = XmlHelper.ValidateXml(Server.HtmlDecode(mService.ConfigContent));
                    if (!String.IsNullOrWhiteSpace(xmlValidationError))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, xmlValidationError, lnkSetFocusView);
                        return;
                    }

                    //Update
                    DataMgr.DataTransferServiceDataMapper.UpdateService(mService);

                    LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                    //Bind services again
                    BindServices();
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating service.", ex);
                }

            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindServices();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindServices();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind equipment variable grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind equipment variables to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindServices();
                }
            }

        #endregion

        #region Grid Events

            protected void gridServices_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridServices_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditService.Visible = false;
                dtvService.Visible = true;

                int dtsID = Convert.ToInt32(gridServices.DataKeys[gridServices.SelectedIndex].Values["DTSID"]);

                //set data source
                dtvService.DataSource = DataMgr.DataTransferServiceDataMapper.GetFullServiceByID(dtsID);
                //bind services to details view
                dtvService.DataBind();
            }

            protected void gridServices_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridServices_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvService.Visible = false;
                pnlEditService.Visible = true;

                int dtsID = Convert.ToInt32(gridServices.DataKeys[e.NewEditIndex].Values["DTSID"]);

                DataTransferService mService = DataMgr.DataTransferServiceDataMapper.GetService(dtsID, true);

                //Set service data
                SetServiceIntoEditForm(mService);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridServices_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows equipmentVariableid
                int dtsID = Convert.ToInt32(gridServices.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a service if it as not been assocaited with
                    //a data source

                    //TODO
                    
                    //check if any vendor product is associated with this retrieval method
                    /*if (mDataManager.DataSourceVendorProductDataMapper.IsVendorProductRetrievalMethodAssociatedWithAnyVendorProducts(retrievalMethodID))
                    {
                        lblErrors.Text = associatedToDataSource;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }

                    else
                    {*/
                        //delete service
                         DataMgr.DataTransferServiceDataMapper.DeleteService(dtsID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    //}  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting service.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = ConvertIEnumerableToDataTable(QueryServices());
                gridServices.PageIndex = gridServices.PageIndex;
                gridServices.DataSource = SortDataTable(dataTable as DataTable, true);
                gridServices.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditService.Visible = false;
            }

            protected void gridServices_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = ConvertIEnumerableToDataTable(QuerySearchedServices(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridServices.DataSource = SortDataTable(dataTable, true);
                gridServices.PageIndex = e.NewPageIndex;
                gridServices.DataBind();
            }

            protected void gridServices_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridServices.EditIndex = -1;

                DataTable dataTable = ConvertIEnumerableToDataTable(QuerySearchedServices(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridServices.DataSource = SortDataTable(dataTable, false);
                gridServices.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetServiceData> QueryServices()
            {
                try
                {
                    //get all services
                    return DataMgr.DataTransferServiceDataMapper.GetAllServicesWithPartialDataByCID(siteUser.CID);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving services.", ex);
                    return null;
                }
            }

            private IEnumerable<GetServiceData> QuerySearchedServices(string searchText)
            {
                try
                {
                    //get all services 
                    return DataMgr.DataTransferServiceDataMapper.GetAllSearchedServicesWithPartialDataByCID(searchText, siteUser.CID);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving services.", ex);
                    return null;
                }
            }

            private void BindServices()
            {
                //query services
                IEnumerable<GetServiceData> services = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryServices() : QuerySearchedServices(txtSearch.Text);

                int count = services.Count();

                gridServices.DataSource = services;

                // bind grid
                gridServices.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedRetrievalMethods(string[] searchText)
            //{
            //    //query equipment variables
            //    IEnumerable<GetRetrievalMethodData> variables = QuerySearchedRetrievalMethods(searchText);

            //    int count = variables.Count();

            //    gridServiceConfigs.DataSource = variables;

            //    // bind grid
            //    gridServiceConfigs.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} service found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} service found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No services found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            //***Linq to sql IEnumerable to DataTable conversion. Needed for sorting.****
            private DataTable ConvertIEnumerableToDataTable(IEnumerable dataSource)
            {
                System.Reflection.PropertyInfo[] propInfo = null;
                DataTable dt = new DataTable();

                foreach (object o in dataSource)
                {
                    propInfo = o.GetType().GetProperties();

                    for (int i = 0; i < propInfo.Length; i++)
                    {
                        dt.Columns.Add(propInfo[i].Name);
                    }
                    break;
                }

                foreach (object tempObject in dataSource)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < propInfo.Length; i++)
                    {
                        object t = tempObject.GetType().InvokeMember(propInfo[i].Name, BindingFlags.GetProperty, null, tempObject, new object[] { });
                        if (t != null)
                            dr[i] = t.ToString();
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }

        #endregion
    }
}

