﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

using CW.Data;
using CW.Data.AzureStorage;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using CW.Website._framework;
using CW.Website._utilitybilling;
using CW.Website.entity;
using CW.Data.Models.EquipmentVariable;
using CW.Data.Models.UtilityBilling;
using CW.Data.AzureStorage.Models.Queue;
using System.Linq.Expressions;
using CW.Common.Constants;
using CW.Business.Blob.Images;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using System.Web;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class UtilityBilling : SiteModulePage
    {
        #region Properties

        private LinkHelper.ViewByMode currentMode;
        private ClientEntity clientEntity;

        private DropDownSequencer dropDownSequence;

        const string variablesUpdateFailed = "Allocation variables update failed. Please contact an administrator.";
        const string editableEquipmentVarsEmpty = "No editable allocation variables have been assigned.";
        const string equipmentVarsEmpty = "No allocation variables have been assigned.";
        const string generateFailed = "Utility billing allocation generation failed. Please contact an administrator.";
        const string generateSuccesful = "Utility billing allocation generation was successful. Your report should be available shortly.";
        const string deleteSuccessful = "Report deletion was successful.";
        const string deleteFailed = "Report deletion failed. Please contact an administrator.";
        private const string pdfFailed = "Failed to generate pdf report. Please try again later.";

        [
        ViewStateProperty
        ]
        private SortDirection GridViewSortDirection
        {
            get;
            set;
        }

        [
        ViewStateProperty
        ]
        private String gridViewSortExpression
        {
            get;
            set;
        }

        private UtilityBillingBindingParameters bindingParameters
        {
            get { return viewState.Get<UtilityBillingBindingParameters>("BindingParameters"); }
            set {viewState.Set("BindingParameters", value);}
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //datamanger in sitepage

            //logged in security check in master page

            dropDownSequence = new DropDownSequencer(new[]{ddlBuildings, ddlEquipment, ddlAnalysis});

            clientEntity = new ClientEntity(siteUser.CID, String.Empty);
        }

        protected void Page_FirstLoad(Object sender, EventArgs e)
        {           
            //no need to set initial gridviewsortexpression as its by the combination of comfort,energy,maintenance,and cost savings. 
            
            BindBuildingDropdownList();
            BindBuildings(ddlVariablesBuildings);
            dropDownSequence.ResetSubDropDownsOf(ddlBuildings);

            if (Request.QueryString.HasKeys())
            {
                ProcessRequestWithQueryString();
            }
            else
            {
                ProcessRequest();
            }

            rblViewBy.SelectedValue = currentMode.ToString();

            VisibilityHelper(currentMode);

            //set admin global settings
            //Make sure to decode from ascii to html 
            GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
            litUtilityBillingBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.UtilityBillingBody);
        }

        protected void Page_AsyncPostbackLoad(Object sender, EventArgs e)
        {
            currentMode = EnumerableHelper.ParseEnum<LinkHelper.ViewByMode>(rblViewBy.SelectedValue);
        }

        #endregion

        #region Load and Bind Fields

        /// <summary>
        /// Binds a dropdown list with all buildings
        /// </summary>
        /// <param name="ddl"></param>
        private void BindBuildings(DropDownList ddl)
        {
            ddl.DataTextField = "BuildingName";
            ddl.DataValueField = "BID";

            //get all buildings by client id
            ddl.DataSource = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID);

            ddl.DataBind();
        }

        /// <summary>
        /// Binds equipment dropdown list by building id
        /// </summary>
        /// <param name="bid"></param>
        private void BindUtilityEquipmentDropdownList(int bid, DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataTextField = "EquipmentName";
            ddl.DataValueField = "EID";
            ddl.DataSource = DataMgr.EquipmentDataMapper.GetAllActiveAndVisibleUtilityBillingWholeBuildingEquipmentByBID(bid);
            ddl.DataBind();
        }

        private void BindBuildingDropdownList()
        {
            //TEMP: remove view all on buildings because we need a buidlings timezone.
            //we would need to pass a list of building instead into the queries and then do a foreach to get each timezone.
            DropDownSequencer.ClearAndCreateDefaultItems(ddlBuildings, true, false, false);

            ddlBuildings.DataTextField = "BuildingName";
            ddlBuildings.DataValueField = "BID";

            //get all buildings by client id. kgs and provider will have selected client.  get all regardless of visible or active, as they once may have been visible or active.                   
            //if session clientid empty then admin
            var buildings = siteUser.VisibleBuildings;


            ddlBuildings.DataSource = buildings;
            ddlBuildings.DataBind();
            
            //populate address in dropdown items by added a title attribute
            int counter = 1;
            foreach (var b in buildings)
            {
                ddlBuildings.Items[counter].Attributes.Add("title", String.Format("{0}, {1}, {2} {3}", b.Address, b.City, b.State.StateName, b.Zip));
                counter++;
            }
        }

        private void BindUtilityEquipmentDropdownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipment, true, true, false);

            ddlEquipment.DataTextField = "EquipmentName";
            ddlEquipment.DataValueField = "EID";

            var equipment = DataMgr.EquipmentDataMapper.GetAllVisibleUtilityBillingTenantEquipmentByBID(Int32.Parse(ddlBuildings.SelectedValue));

            ddlEquipment.DataSource = equipment;

            ddlEquipment.DataBind();
        }

        /// <summary>
        /// Binds equipment dropdown list by eid
        /// </summary>
        /// <param name="eid"></param>
        private void BindAnalysisDropdownList(int eid, DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataTextField = "AnalysisName";
            ddl.DataValueField = "AID";
            ddl.DataSource = DataMgr.AnalysisDataMapper.GetAllActiveAndVisibleAnalysesByEID(eid, true);
            ddl.DataBind();
        }

        private void BindAnalysisDropdownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlAnalysis, true, true, false);

            ddlAnalysis.DataTextField = "AnalysisName";
            ddlAnalysis.DataValueField = "AID";

            //
            //get all visible analyses, regardless of its active state because we stll need to plot if it was once active.

            IEnumerable<Analyse> analyses = Enumerable.Empty<Analyse>();

            if(ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByEquipmentTypeID(Int32.Parse(ddlBuildings.SelectedValue), BusinessConstants.EquipmentType.TenantUtilitiesTypeID, true);
            }
            else
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByEID(Int32.Parse(ddlEquipment.SelectedValue), true);
            }

            ddlAnalysis.DataSource = analyses;
            
            ddlAnalysis.DataBind();

            //populate analysis teaser in dropdown items by added a title attribute
            int counter = 2;
            foreach (Analyse a in analyses)
            {
                ddlAnalysis.Items[counter].Attributes.Add("title", a.AnalysisTeaser);
                counter++;
            }
        }

        /// <summary>
        /// Binds equipment variables repeater list by eid.
        /// NOT DISPLAYED BECAUSE VALUES GET UPDATED AND IS CONFUSING FOR OLD RESULTS
        /// </summary>
        /// <param name="eid"></param>
        private void BindEquipmentVariables(int eid)
        {
            //get equipment variables associated to equipment 
            IEnumerable<GetEquipmentEquipmentVariableData> mEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesWithDataByEID(eid);

            if (mEquipmentVariables.Any())
            {
                //bind equipment variabels to repeater
                rptEquipmentVars.DataSource = mEquipmentVariables;
                rptEquipmentVars.DataBind();
                rptEquipmentVars.Visible = true;

                lblEquipmentVarsEmpty.Visible = false;
            }
            else
            {
                rptEquipmentVars.Visible = false;
                lblEquipmentVarsEmpty.Text = equipmentVarsEmpty;
                lblEquipmentVarsEmpty.Visible = true;
            }
        }

        /// <summary>
        /// Binds repeater list of equipment variables associated with the equipment
        /// </summary>
        /// <param name="eid"></param>
        private void BindEditableEquipmentVariablesWithData(int eid)
        {
            //get editable equipment variables associated to equipment 
            IEnumerable<GetEquipmentEquipmentVariableData> mEquipmentEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesWithDataByEID(eid, true);

            if (mEquipmentEquipmentVariables.Any())
            {               
                rptEditableEquipmentVars.DataSource = mEquipmentEquipmentVariables;
                rptEditableEquipmentVars.DataBind();
                rptEditableEquipmentVars.Visible = true;
                lblEditableEquipmentVarsEmpty.Visible = false;
            }
            else
            {
                rptEditableEquipmentVars.Visible = false;
                lblEditableEquipmentVarsEmpty.Text = editableEquipmentVarsEmpty;
                lblEditableEquipmentVarsEmpty.Visible = true;
            }

            //show info
            editableEquipmentVariables.Visible = true;
        }

        #endregion

        #region Button Click Events

        protected void generateButton_Click(Object sender, EventArgs e)
        {
            //clear existing gridviewsortexpression
            gridViewSortExpression = String.Empty;

            //just one type for now
            var utilityAnalysisType = DataConstants.UtiliityAnalysisTypes.Utility; //EnumHelper.Parse<DataConstants.UtiliityAnalysisTypes>(rblType.SelectedValue);

            bindingParameters =
            new UtilityBillingBindingParameters
            {
                    clientID            = clientEntity.ID,
                    buildingID          = UtilityBillingBindingParameters.ToInt(ddlBuildings.SelectedValue).Value,
                    equipmentID         = UtilityBillingBindingParameters.ToInt(ddlEquipment.SelectedValue),
                    analysisID          = UtilityBillingBindingParameters.ToInt(ddlAnalysis.SelectedValue),

                    startingEndDate = (DateTime)txtStartingEndDate.SelectedDate,
                    endingEndDate =(DateTime)txtEndingEndDate.SelectedDate,
                    type = utilityAnalysisType,
            };

            Generate();
        }

        protected void emailButton_Click(object sender, EventArgs e)
        {
            ClearMessaging();

            var analysisPDFReport = CreateAnalysisPDFReportDTO();

            var reportService = ReportFileGenerator.Create(siteUser.IsSchneiderTheme, siteUser.FullName, LogMgr, analysisPDFReport, null, null, null, null, null, null);

            if (reportService == null)
            {
                DisplayErrorOutput(pdfFailed);

                return;
            }

            Boolean result;

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    reportService,
                    LogMgr
                ).SendInEmail(Email.GenerateReportSubject(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product Utility Billing Report for "), analysisPDFReport, null, null, null), Email.GenerateReportBody(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product Utility Billing Report for "), analysisPDFReport, null, null, null));
            }
            catch(FilePublishService.MonthlyReportEmailAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot email report, max monthly email limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                return;
            }

            DisplayErrorOutput
            (
                result ?
                    "Utility Billing report was emailed successfully." :
                    "Utility Billing report email failed. Please try again later."
            );
        }

        protected void downloadButton_Click(Object sender, EventArgs e)
        {
            ClearMessaging();

            var reportService = ReportFileGenerator.Create(siteUser.IsSchneiderTheme, siteUser.FullName, LogMgr, CreateAnalysisPDFReportDTO(), null, null, null, null, null, null);

            if (reportService == null)
            {
                DisplayErrorOutput(pdfFailed);

                return;
            }

            Boolean result;

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    reportService,
                    LogMgr
                ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productUtilityReport.pdf", true));
            }
            catch(FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                return;
            }

            if (result) return;

            DisplayErrorOutput("An error occured attempting to download. Please try again later.");
        }

        //protected void downloadExcelFullButton_Click(Object sender, EventArgs e)
        //{
        //    ClearMessaging();

        //        Boolean result;

        //        int originalPageIndex = gridUtility.PageIndex;
        //        List<UtilityBillingResult> list = BuildUtility().ToList();
        //        int downloadRowQuota = Convert.ToInt32(CloudConfiguration.GetConfigurationSetting("DownloadRowQuote", "500"));
        //        bool isDownloadRowQuoteExceeded = list.Count > downloadRowQuota;
        //        if (isDownloadRowQuoteExceeded)
        //        {
        //            list = list.Take(downloadRowQuota).ToList();
        //            DisplayErrorOutput(String.Format("Maximum download exceeded, only first {0} rows have been included.", downloadRowQuota), lblErrorTop);
        //        }
    

        //        gridUtility.AllowPaging = false;

        //        gridUtility.DataSource = String.IsNullOrWhiteSpace(gridViewSortExpression) ? list : EnumerableHelper.CreateSortedEnumerable(list, gridViewSortExpression, GridViewSortDirection);
        //        gridUtility.DataBind();

        //        try
        //        {
        //            result = new FilePublishService(siteUser, new GridViewXLSFileGenerator(gridUtility, false, null)).AttachAsDownload(Page, TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productUtility.xls", true));
        //        }
        //        catch(FilePublishService.MonthlyDownloadAllowanceReachedException)
        //        {
        //            DisplayErrorOutput("Cannot download grid, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.", lblErrorTop);

        //            return;
        //        }

        //        if (isDownloadRowQuoteExceeded)
        //        {
        //            gridUtility.DataSource = String.IsNullOrWhiteSpace(gridViewSortExpression) ? BuildUtility() : EnumerableHelper.CreateSortedEnumerable(BuildUtility(), gridViewSortExpression, GridViewSortDirection);
        //        }

        //        gridUtility.AllowPaging = true;
        //        gridUtility.DataBind();

        //        if (result) return;

        //        DisplayErrorOutput("An error occured attempting to download. Please try again later.", lblErrorTop);
        //}

        //protected void downloadExcelCurrentPageButton_Click(Object sender, EventArgs e)
        //{
        //    ClearMessaging();

        //    Boolean result;

        //    try
        //    {
        //        result = new FilePublishService(siteUser, new GridViewXLSFileGenerator(gridUtility, true, null)).AttachAsDownload(Page, TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productUtility.xls", true));
        //    }
        //    catch(FilePublishService.MonthlyDownloadAllowanceReachedException)
        //    {
        //        DisplayErrorOutput("Cannot download grid, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.", lblErrorTop);

        //        return;
        //    }

        //    BindGrid(BuildUtility());

        //    if (result) return;

        //    DisplayErrorOutput("An error occured attempting to download. Please try again later.", lblErrorTop);

        //}

        /// <summary>
        /// Updates values for associated equipment variables.
        /// 
        /// Generates allocation report by putting message in matlab utility queue.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void generateAllocationButton_Click(object sender, EventArgs e)
        {
            //TRY TO UPDATE EQUIP VARS
            try
            {
                //declare new temp controls
                TextBox tempTextBox = new TextBox();
                HiddenField tempHiddenField = new HiddenField();

                //for each equipment variable in repeater                           
                foreach (RepeaterItem rptItem in rptEditableEquipmentVars.Items)
                {
                    if (rptItem.ItemType == ListItemType.Item || rptItem.ItemType == ListItemType.AlternatingItem)
                    {
                        tempTextBox = (TextBox)rptItem.FindControl("txtEditValue");
                        tempHiddenField = (HiddenField)rptItem.FindControl("hdnID");

                        //gets values and updates Equipment variable
                        DataMgr.EquipmentVariableDataMapper.UpdatePartialEquipmentEquipmentVariable(Convert.ToInt32(tempHiddenField.Value), tempTextBox.Text);
                    }
                }

                //TRY TO GENERATE MESSAGE
                try
                {
                    //get analysis by aid
                    Analyse a = DataMgr.AnalysisDataMapper.GetAnalysis(Convert.ToInt32(ddlVariablesAnalysis.SelectedValue), 
                        new Expression<Func<Analyse, Object>>[] { (_=>_.MatlabAssembly) });

                    //create queue context
                    AzureQueueDataContext context = new AzureQueueDataContext(DataMgr.OrgBasedQueueStorageProvider, AzureConstants.UtilityBillingQueue);

                    //build queue message 
                    UtilityBillingQueueMessage queueMessage = new UtilityBillingQueueMessage
                    {
                        AID = a.AID,
                        BID = Convert.ToInt32(ddlVariablesBuildings.SelectedValue),
                        EID = Convert.ToInt32(ddlVariablesEquipment.SelectedValue),
                        StartDate = (DateTime)txtVariablesStartDate.SelectedDate,

                        //so enddate data is inclusive
                        EndDate = ((DateTime)txtVariablesEndDate.SelectedDate).AddDays(1),

                        AnalysisType = DataConstants.UtiliityAnalysisTypes.Utility.ToString(),
                        BuildingName = ddlVariablesBuildings.SelectedItem.Text,
                        EquipmentName = ddlVariablesEquipment.SelectedItem.Text,
                        AnalysisName = a.AnalysisName,
                        MAID = a.MatlabAssembly.MAID,
                        AnalysisBaseFileName = a.AnalysisBaseFileName,
                        AnalysisFunctionName = a.AnalysisFunctionName,
                        AnalysisTeaser = a.AnalysisTeaser,
                        AnalysisDescription = a.AnalysisDescription
                    };

                    //add to queue
                    context.AddMessage<UtilityBillingQueueMessage>(queueMessage);

                    lblVariablesError.Text = generateSuccesful;
                    lblVariablesError.Visible = true;
                    lnkSetFocusVariables.Focus();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error generating utility billing allocation analysis message.", sqlEx);
                    lblVariablesError.Text = generateFailed;
                    lblVariablesError.Visible = true;
                    lnkSetFocusVariables.Focus();
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error generating utility billing allocation analysis message.", ex);
                    lblVariablesError.Text = generateFailed;
                    lblVariablesError.Visible = true;
                    lnkSetFocusVariables.Focus();
                }
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipments equipment variables.", sqlEx);
                lblVariablesError.Text = variablesUpdateFailed;
                lblVariablesError.Visible = true;
                lnkSetFocusVariables.Focus();
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipments equipment variables", ex);
                lblVariablesError.Text = variablesUpdateFailed;
                lblVariablesError.Visible = true;
                lnkSetFocusVariables.Focus();
            }        
        }

        #endregion

        #region Dropdown Events

        protected void ddlBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequence.ResetSubDropDownsOf(ddlBuildings);

            if (ddlBuildings.SelectedIndex == -1) return;

            BindUtilityEquipmentDropdownList();
        }

        protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequence.ResetSubDropDownsOf(ddlEquipment);

            if (ddlEquipment.SelectedIndex == -1) return;

            BindAnalysisDropdownList();
        }

        /// <summary>
        /// ddlVariablesBuildings on selected index changed, binds equipment dropdow by selected building id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlVariablesBuildings_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BindUtilityEquipmentDropdownList(Convert.ToInt32(ddlVariablesBuildings.SelectedValue), ddlVariablesEquipment);

            //hide equipment variables
            editableEquipmentVariables.Visible = false;
        }

        /// <summary>
        /// ddlVariablesEquipment on selected index changed, binds equipment variables list boxes
        /// and equipment variables data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlVariablesEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int eid = Convert.ToInt32(ddlVariablesEquipment.SelectedValue);

            if (eid != -1)
            {
                BindEditableEquipmentVariablesWithData(eid);

                BindAnalysisDropdownList(Convert.ToInt32(ddlVariablesEquipment.SelectedValue), ddlVariablesAnalysis);
            }
            else
            {
                //hide equipment variables
                editableEquipmentVariables.Visible = false;
            }

            //hide error message
            lblVariablesError.Visible = false;
        }

        protected void rblViewBy_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            VisibilityHelper(EnumerableHelper.ParseEnum<LinkHelper.ViewByMode>(rblViewBy.SelectedValue));
        }

        #endregion

        #region Tab Events

        /// <summary>
        /// Tab changed event, bind points grid after tab changed back from add point tab
        /// </summary>
        protected void onTabClick(object sender, EventArgs e)
        {
        }

        #endregion

        #region Grid Events

        protected void gridUtility_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ClearMessaging();

            IOrderedDictionary dka = gridUtility.DataKeys[gridUtility.SelectedIndex].Values;
            
            //get row
            GridViewRow row = gridUtility.Rows[gridUtility.SelectedIndex];

            PopulateAnalysisInfo(Convert.ToInt32(dka["CID"]), siteUser.ClientName, Convert.ToInt32(dka["BID"]), ((HtmlAnchor)row.Cells[0].Controls[1]).Title, Convert.ToInt32(dka["EID"]), ((HtmlAnchor)row.Cells[1].Controls[1]).Title, Convert.ToInt32(dka["AID"]), ((HtmlAnchor)row.Cells[2].Controls[1]).Title, DateTime.Parse(dka["EndDate"].ToString()));
        }

        protected void gridUtility_OnDataBound(object sender, EventArgs e)
        {
            GridViewRowCollection rows = gridUtility.Rows;

            foreach (GridViewRow row in rows)
            {
                if ((row.RowState & DataControlRowState.Edit) == 0)
                {
                    try
                    {
                        //hide delete linkbuttons if not fulladmin or higher
                        if (!siteUser.IsFullAdminOrHigher)
                        {
                            ((LinkButton)row.Cells[11].Controls[1]).Visible = false;
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        protected void gridUtility_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                {
                    var columnIndex = ControlHelper.GridViewControl.GetSortColumnIndex(gridUtility, gridViewSortExpression);

                    if (columnIndex != -1)
                    {
                        e.Row.Cells[columnIndex].Attributes.Add("class", String.Concat("gridSort", GridViewSortDirection.ToString()));
                    }

                    break;
                }
                case DataControlRowType.Pager:
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);

                    break;
                }
            }
        }

        protected void gridUtility_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            gridUtility.PageIndex = e.NewPageIndex;
            BindGrid(BuildUtility());
        }

        protected void gridUtility_Sorting(Object sender, GridViewSortEventArgs e)
        {
            if (gridViewSortExpression == e.SortExpression)
            {
                GridViewSortDirection = ((GridViewSortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending);
            }
            else
            {
                gridViewSortExpression = e.SortExpression;
                GridViewSortDirection = SortDirection.Descending;

                if (typeof(UtilityBillingResult).GetProperty(gridViewSortExpression).PropertyType == typeof(String))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
            }

            gridUtility.DataSource = EnumerableHelper.CreateSortedEnumerable(BuildUtility(), e.SortExpression, GridViewSortDirection);

            gridUtility.DataBind();
        }

        protected void gridUtility_Deleting(object sender, GridViewDeleteEventArgs e)
        {
            //get deleting rows eid
            int eid = Convert.ToInt32(gridUtility.DataKeys[e.RowIndex].Values["EID"]);
            int aid = Convert.ToInt32(gridUtility.DataKeys[e.RowIndex].Values["AID"]);
            int cid = Convert.ToInt32(gridUtility.DataKeys[e.RowIndex].Values["CID"]);
            int bid = Convert.ToInt32(gridUtility.DataKeys[e.RowIndex].Values["BID"]);
            DateTime endDate = DateTime.Parse(gridUtility.DataKeys[e.RowIndex].Values["EndDate"].ToString());
            DataConstants.UtiliityAnalysisTypes utilityAnalysisType = DataConstants.UtiliityAnalysisTypes.Utility;

            try
            {
                var bp = new UtilityBillingBindingParameters()
                   {
                       analysisID = aid,
                       buildingID = bid,
                       clientID = cid,
                       endingEndDate = endDate,
                       equipmentID = eid,
                       type = utilityAnalysisType
                   };

                //get main priority utility
                PriorityUtilityBilling p = DataMgr.UtilityBillingDataMapper.GetUtilityBill(bp);
             
                if (p != null)
                {
                    //clean up newly inserted figure in blob storage and azure tables
                    if (!String.IsNullOrEmpty(p.FigureBlobExt))                         
					    new UtilityFigureImage(p, DataMgr.OrgBasedBlobStorageProvider).Delete();
                }

                //clean up newly inserted main priority
                DataMgr.UtilityBillingDataMapper.DeleteUtilityBill(bp);

                //clean up all newly inserted main vdata 
                var vDataDeleteCount = DataMgr.VAndVPreDataMapper.DeleteAllSpecifiedVDataUtilityBillingAsync(bp).Result;

                //also clean up any main vpredata that did get inserted in for loop
                var vPreDataDeleteCount = DataMgr.VAndVPreDataMapper.DeleteAllSpecifiedVPreDataUtilityBillingAsync(bp).Result;

                ////get all by cid and enddate, then filter by building and aid, delete tenant prioirities. no figures or vdata or vpredata will exist.
                ////for removing all tenant priorities
                //IEnumerable<PriorityUtilityBilling> tenantPriorities = mDataManager.UtilityBillingDataMapper.GetUtilityBills(utilityAnalysisType, cid, bid, null, null, endDate, endDate);

                //foreach (PriorityUtilityBilling tp in tenantPriorities)
                //{
                //    //clean up newly inserted tenant priority. the only thing that should vary is the eid.
                //    mDataManager.UtilityBillingDataMapper.DeleteUtilityBill(utilityAnalysisType, cid, bid, tp.EID, aid, endDate);
                //}

                lblErrorTop.Text = deleteSuccessful;
            }
            catch (Exception ex)
            {
                lblErrorTop.Text = deleteFailed;

                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting utility report.", ex);
            }

            Generate();

            lblErrorTop.Visible = true;
        }

        #endregion

        #region Grid Helper Methods

        private void Generate()
        {
            List<UtilityBillingResult> results = new List<UtilityBillingResult>();

            //bind grid---
            gridUtility.PageIndex = 0;

            ////TEMP: 
            //results.Add(new UtilityBillingResult() { CID= 1, EquipmentClassID=1, AID = 75, NotesSummary="", AnalysisTeaser="", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("1/3/2012"), EndDate = DateTime.Parse("2/4/2012"), AllocatedCost = 5000, AllocatedFraction = 30, ElectricAllocatedUse = 37037, ElectricRate = .135, ElectricPeakUse = 100, PeakUseTime = DateTime.Parse("1/22/2012")});
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("2/5/2012"), EndDate = DateTime.Parse("3/2/2012"), AllocatedCost = 6000, AllocatedFraction = 10, ElectricAllocatedUse = 42857, ElectricRate = .140, ElectricPeakUse = 110, PeakUseTime = DateTime.Parse("2/12/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("3/3/2012"), EndDate = DateTime.Parse("4/1/2012"), AllocatedCost = 5500, AllocatedFraction = 20, ElectricAllocatedUse = 35484, ElectricRate = .155, ElectricPeakUse = 120, PeakUseTime = DateTime.Parse("3/15/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("4/2/2012"), EndDate = DateTime.Parse("5/3/2012"), AllocatedCost = 5750, AllocatedFraction = 10, ElectricAllocatedUse = 44231, ElectricRate = .130, ElectricPeakUse = 115, PeakUseTime = DateTime.Parse("4/20/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("5/4/2012"), EndDate = DateTime.Parse("6/2/2012"), AllocatedCost = 6100, AllocatedFraction = 20, ElectricAllocatedUse = 40667, ElectricRate = .150, ElectricPeakUse = 130, PeakUseTime = DateTime.Parse("5/25/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("6/3/2012"), EndDate = DateTime.Parse("7/5/2012"), AllocatedCost = 6200, AllocatedFraction = 10, ElectricAllocatedUse = 42759, ElectricRate = .145, ElectricPeakUse = 125, PeakUseTime = DateTime.Parse("6/30/2012") });

            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("1/3/2012"), EndDate = DateTime.Parse("2/4/2012"), AllocatedCost = 5400, AllocatedFraction = 10, ElectricAllocatedUse = 32037, ElectricRate = .135, ElectricPeakUse = 100, PeakUseTime = DateTime.Parse("1/22/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("2/5/2012"), EndDate = DateTime.Parse("3/2/2012"), AllocatedCost = 6900, AllocatedFraction = 30, ElectricAllocatedUse = 41857, ElectricRate = .140, ElectricPeakUse = 110, PeakUseTime = DateTime.Parse("2/12/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("3/3/2012"), EndDate = DateTime.Parse("4/6/2012"), AllocatedCost = 5000, AllocatedFraction = 10, ElectricAllocatedUse = 39484, ElectricRate = .155, ElectricPeakUse = 120, PeakUseTime = DateTime.Parse("3/15/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("4/2/2012"), EndDate = DateTime.Parse("5/6/2012"), AllocatedCost = 5950, AllocatedFraction = 20, ElectricAllocatedUse = 41231, ElectricRate = .130, ElectricPeakUse = 115, PeakUseTime = DateTime.Parse("4/20/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("5/4/2012"), EndDate = DateTime.Parse("6/2/2012"), AllocatedCost = 6000, AllocatedFraction = 10, ElectricAllocatedUse = 48667, ElectricRate = .150, ElectricPeakUse = 130, PeakUseTime = DateTime.Parse("5/25/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 1, AID = 75, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkElectricAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("6/3/2012"), EndDate = DateTime.Parse("7/5/2012"), AllocatedCost = 6800, AllocatedFraction = 20, ElectricAllocatedUse = 40759, ElectricRate = .145, ElectricPeakUse = 125, PeakUseTime = DateTime.Parse("6/30/2012") });

            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("1/3/2012"), EndDate = DateTime.Parse("2/4/2012"), AllocatedCost = 5000, AllocatedFraction = 10, GasAllocatedUse = 37037, GasRate = .135, GasPeakUse = 100, PeakUseTime = DateTime.Parse("1/22/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("2/5/2012"), EndDate = DateTime.Parse("3/2/2012"), AllocatedCost = 6000, AllocatedFraction = 20, GasAllocatedUse = 42857, GasRate = .140, GasPeakUse = 110, PeakUseTime = DateTime.Parse("2/12/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("3/3/2012"), EndDate = DateTime.Parse("4/1/2012"), AllocatedCost = 5500, AllocatedFraction = 10, GasAllocatedUse = 35484, GasRate = .155, GasPeakUse = 120, PeakUseTime = DateTime.Parse("3/15/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("4/2/2012"), EndDate = DateTime.Parse("5/3/2012"), AllocatedCost = 5750, AllocatedFraction = 20, GasAllocatedUse = 44231, GasRate = .130, GasPeakUse = 115, PeakUseTime = DateTime.Parse("4/20/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("5/4/2012"), EndDate = DateTime.Parse("6/2/2012"), AllocatedCost = 6100, AllocatedFraction = 10, GasAllocatedUse = 40667, GasRate = .150, GasPeakUse = 130, PeakUseTime = DateTime.Parse("5/25/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 1, EquipmentName = "TenantGroupA", StartDate = DateTime.Parse("6/3/2012"), EndDate = DateTime.Parse("7/5/2012"), AllocatedCost = 6200, AllocatedFraction = 30, GasAllocatedUse = 42759, GasRate = .145, GasPeakUse = 125, PeakUseTime = DateTime.Parse("6/30/2012") });

            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("1/3/2012"), EndDate = DateTime.Parse("2/4/2012"), AllocatedCost = 5400, AllocatedFraction = 10, GasAllocatedUse = 32037, GasRate = .135, GasPeakUse = 100, PeakUseTime = DateTime.Parse("1/22/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("2/5/2012"), EndDate = DateTime.Parse("3/2/2012"), AllocatedCost = 6900, AllocatedFraction = 20, GasAllocatedUse = 41857, GasRate = .140, GasPeakUse = 110, PeakUseTime = DateTime.Parse("2/12/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("3/3/2012"), EndDate = DateTime.Parse("4/6/2012"), AllocatedCost = 5000, AllocatedFraction = 20, GasAllocatedUse = 39484, GasRate = .155, GasPeakUse = 120, PeakUseTime = DateTime.Parse("3/15/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("4/2/2012"), EndDate = DateTime.Parse("5/6/2012"), AllocatedCost = 5950, AllocatedFraction = 10, GasAllocatedUse = 41231, GasRate = .130, GasPeakUse = 115, PeakUseTime = DateTime.Parse("4/20/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("5/4/2012"), EndDate = DateTime.Parse("6/2/2012"), AllocatedCost = 6000, AllocatedFraction = 30, GasAllocatedUse = 48667, GasRate = .150, GasPeakUse = 130, PeakUseTime = DateTime.Parse("5/25/2012") });
            //results.Add(new UtilityBillingResult() { CID = 1, EquipmentClassID = 2, AID = 5, NotesSummary = "", AnalysisTeaser = "", AnalysisName = "GatewayParkGasAllocations", BuildingName = "Gateway Park", BID = 1, EID = 2, EquipmentName = "TenantGroupB", StartDate = DateTime.Parse("6/3/2012"), EndDate = DateTime.Parse("7/5/2012"), AllocatedCost = 6800, AllocatedFraction = 10, GasAllocatedUse = 40759, GasRate = .145, GasPeakUse = 125, PeakUseTime = DateTime.Parse("6/30/2012") });

            gridUtility.DataSource = results = BuildUtility().ToList();

            gridUtility.DataBind();

            ResetAllChartVisibility();

            //plot charts-----
            if (results != null && results.Any())
            {
                ClearAllChartSeries();

                //get distinct eids
                List<int> eids = results.Select(e => e.EID).Distinct().ToList();

                foreach (int eid in eids)
                {
                    //get distinct aids from eid
                    List<int> aids = results.Where(e => e.EID == eid).Select(a => a.AID).Distinct().ToList();

                        foreach (int aid in aids)
                        {
                            //get eid aid results
                            List<UtilityBillingResult> eaResults = results.Where(e => e.EID == eid && e.AID == aid).ToList();

                            UtilityBillingResult eaFirstResults = eaResults.First();

                            string shortName = eaFirstResults.EquipmentName;
                            string longName = shortName + " (" + eaFirstResults.AnalysisName + ")";
                            //string shortName = ubr.EquipmentName;

                            //Allocated Use---
                            //create new series, set chart type and name
                            var seriesAllocatedUse =
                            new Series
                            {
                                ChartType = SeriesChartType.Column,
                                //BorderWidth = 2,
                                Name = longName,
                                ToolTip = "X: #VALX{d}, Y: #VALY",
                                LegendToolTip = longName,
                                LegendText = longName,
                            };


                            //plot appropriate chart
                            if (eaFirstResults.CHWAllocatedUse != null)
                            {
                                //set axis intervaltype
                                chartCHWAllocatedUse.ChartAreas["chwAllocatedUseChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                //set axis format:  g = short date and time, d = short date
                                chartCHWAllocatedUse.ChartAreas["chwAllocatedUseChartArea"].AxisX.LabelStyle.Format = "d";

                                //add new data to series
                                //seriesAllocatedUse.Points.AddXY(ubr.EndDate, ((ubr.ElectricAllocatedUse != null) ? ubr.ElectricAllocatedUse :
                                //                                    ((ubr.GasAllocatedUse != null) ? ubr.GasAllocatedUse :
                                //                                        ((ubr.OilAllocatedUse != null) ? ubr.OilAllocatedUse :
                                //                                            ((ubr.PropaneAllocatedUse != null) ? ubr.PropaneAllocatedUse :
                                //                                                ((ubr.SteamAllocatedUse != null) ? ubr.SteamAllocatedUse :
                                //                                                    ((ubr.CHWAllocatedUse != null) ? ubr.CHWAllocatedUse :
                                //                                                        ((ubr.WaterAllocatedUse != null) ? ubr.WaterAllocatedUse :
                                //                                                            ((ubr.EnergyAllocatedUse != null) ? ubr.EnergyAllocatedUse : 0
                                //                                        )))))))));

                                seriesAllocatedUse.Points.DataBindXY(eaResults, "EndDate", eaResults, "CHWAllocatedUse");

                                //TODO: on front end after microsoft bug fix.
                                //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                chartCHWAllocatedUse.ChartAreas["chwAllocatedUseChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                chartCHWAllocatedUse.ChartAreas["chwAllocatedUseChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                //add series to chart
                                chartCHWAllocatedUse.Series.Add(seriesAllocatedUse);

                                chartCHWAllocatedUse.Visible = true;
                            }
                            else if(eaFirstResults.ElectricAllocatedUse != null)
                            {
                                //set axis intervaltype
                                chartElectricAllocatedUse.ChartAreas["electricAllocatedUseChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                //set axis format:  g = short date and time, d = short date
                                chartElectricAllocatedUse.ChartAreas["electricAllocatedUseChartArea"].AxisX.LabelStyle.Format = "d";

                                //add new data to series
                                //seriesAllocatedUse.Points.AddXY(ubr.EndDate, ((ubr.ElectricAllocatedUse != null) ? ubr.ElectricAllocatedUse :
                                //                                    ((ubr.GasAllocatedUse != null) ? ubr.GasAllocatedUse :
                                //                                        ((ubr.OilAllocatedUse != null) ? ubr.OilAllocatedUse :
                                //                                            ((ubr.PropaneAllocatedUse != null) ? ubr.PropaneAllocatedUse :
                                //                                                ((ubr.SteamAllocatedUse != null) ? ubr.SteamAllocatedUse :
                                //                                                    ((ubr.CHWAllocatedUse != null) ? ubr.CHWAllocatedUse :
                                //                                                        ((ubr.WaterAllocatedUse != null) ? ubr.WaterAllocatedUse :
                                //                                                            ((ubr.EnergyAllocatedUse != null) ? ubr.EnergyAllocatedUse : 0
                                //                                        )))))))));

                                seriesAllocatedUse.Points.DataBindXY(eaResults, "EndDate", eaResults, "ElectricAllocatedUse");

                                //TODO: on front end after microsoft bug fix.
                                //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                chartElectricAllocatedUse.ChartAreas["electricAllocatedUseChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                chartElectricAllocatedUse.ChartAreas["electricAllocatedUseChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                //add series to chart
                                chartElectricAllocatedUse.Series.Add(seriesAllocatedUse);

                                chartElectricAllocatedUse.Visible = true;
                            }
                            else if (eaFirstResults.EnergyAllocatedUse != null)
                            {
                                //set axis intervaltype
                                chartEnergyAllocatedUse.ChartAreas["energyAllocatedUseChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                //set axis format:  g = short date and time, d = short date
                                chartEnergyAllocatedUse.ChartAreas["energyAllocatedUseChartArea"].AxisX.LabelStyle.Format = "d";

                                //add new data to series
                                //seriesAllocatedUse.Points.AddXY(ubr.EndDate, ((ubr.ElectricAllocatedUse != null) ? ubr.ElectricAllocatedUse :
                                //                                    ((ubr.GasAllocatedUse != null) ? ubr.GasAllocatedUse :
                                //                                        ((ubr.OilAllocatedUse != null) ? ubr.OilAllocatedUse :
                                //                                            ((ubr.PropaneAllocatedUse != null) ? ubr.PropaneAllocatedUse :
                                //                                                ((ubr.SteamAllocatedUse != null) ? ubr.SteamAllocatedUse :
                                //                                                    ((ubr.CHWAllocatedUse != null) ? ubr.CHWAllocatedUse :
                                //                                                        ((ubr.WaterAllocatedUse != null) ? ubr.WaterAllocatedUse :
                                //                                                            ((ubr.EnergyAllocatedUse != null) ? ubr.EnergyAllocatedUse : 0
                                //                                        )))))))));

                                seriesAllocatedUse.Points.DataBindXY(eaResults, "EndDate", eaResults, "EnergyAllocatedUse");

                                //TODO: on front end after microsoft bug fix.
                                //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                chartEnergyAllocatedUse.ChartAreas["energyAllocatedUseChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                chartEnergyAllocatedUse.ChartAreas["energyAllocatedUseChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                //add series to chart
                                chartEnergyAllocatedUse.Series.Add(seriesAllocatedUse);

                                chartEnergyAllocatedUse.Visible = true;
                            }
                            else if(eaFirstResults.GasAllocatedUse != null) 
                            {
                                //set axis intervaltype
                                chartGasAllocatedUse.ChartAreas["gasAllocatedUseChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                //set axis format:  g = short date and time, d = short date
                                chartGasAllocatedUse.ChartAreas["gasAllocatedUseChartArea"].AxisX.LabelStyle.Format = "d";

                                //add new data to series
                                //seriesAllocatedUse.Points.AddXY(ubr.EndDate, ((ubr.ElectricAllocatedUse != null) ? ubr.ElectricAllocatedUse :
                                //                                    ((ubr.GasAllocatedUse != null) ? ubr.GasAllocatedUse :
                                //                                        ((ubr.OilAllocatedUse != null) ? ubr.OilAllocatedUse :
                                //                                            ((ubr.PropaneAllocatedUse != null) ? ubr.PropaneAllocatedUse :
                                //                                                ((ubr.SteamAllocatedUse != null) ? ubr.SteamAllocatedUse :
                                //                                                    ((ubr.CHWAllocatedUse != null) ? ubr.CHWAllocatedUse :
                                //                                                        ((ubr.WaterAllocatedUse != null) ? ubr.WaterAllocatedUse :
                                //                                                            ((ubr.EnergyAllocatedUse != null) ? ubr.EnergyAllocatedUse : 0
                                //                                        )))))))));

                                seriesAllocatedUse.Points.DataBindXY(eaResults, "EndDate", eaResults, "GasAllocatedUse");

                                //TODO: on front end after microsoft bug fix.
                                //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                chartGasAllocatedUse.ChartAreas["gasAllocatedUseChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                chartGasAllocatedUse.ChartAreas["gasAllocatedUseChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                //add series to chart
                                chartGasAllocatedUse.Series.Add(seriesAllocatedUse);

                                chartGasAllocatedUse.Visible = true;
                            }
                            else if(eaFirstResults.OilAllocatedUse != null)
                            {
                                //set axis intervaltype
                                chartOilAllocatedUse.ChartAreas["oilAllocatedUseChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                //set axis format:  g = short date and time, d = short date
                                chartOilAllocatedUse.ChartAreas["oilAllocatedUseChartArea"].AxisX.LabelStyle.Format = "d";

                                //add new data to series
                                //seriesAllocatedUse.Points.AddXY(ubr.EndDate, ((ubr.ElectricAllocatedUse != null) ? ubr.ElectricAllocatedUse :
                                //                                    ((ubr.GasAllocatedUse != null) ? ubr.GasAllocatedUse :
                                //                                        ((ubr.OilAllocatedUse != null) ? ubr.OilAllocatedUse :
                                //                                            ((ubr.PropaneAllocatedUse != null) ? ubr.PropaneAllocatedUse :
                                //                                                ((ubr.SteamAllocatedUse != null) ? ubr.SteamAllocatedUse :
                                //                                                    ((ubr.CHWAllocatedUse != null) ? ubr.CHWAllocatedUse :
                                //                                                        ((ubr.WaterAllocatedUse != null) ? ubr.WaterAllocatedUse :
                                //                                                            ((ubr.EnergyAllocatedUse != null) ? ubr.EnergyAllocatedUse : 0
                                //                                        )))))))));

                                seriesAllocatedUse.Points.DataBindXY(eaResults, "EndDate", eaResults, "OilAllocatedUse");

                                //TODO: on front end after microsoft bug fix.
                                //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                chartOilAllocatedUse.ChartAreas["oilAllocatedUseChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                chartOilAllocatedUse.ChartAreas["oilAllocatedUseChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                //add series to chart
                                chartOilAllocatedUse.Series.Add(seriesAllocatedUse);

                                chartOilAllocatedUse.Visible = true;
                            }
                            else if(eaFirstResults.PropaneAllocatedUse != null)
                            {
                                //set axis intervaltype
                                chartPropaneAllocatedUse.ChartAreas["propaneAllocatedUseChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                //set axis format:  g = short date and time, d = short date
                                chartPropaneAllocatedUse.ChartAreas["propaneAllocatedUseChartArea"].AxisX.LabelStyle.Format = "d";

                                //add new data to series
                                //seriesAllocatedUse.Points.AddXY(ubr.EndDate, ((ubr.ElectricAllocatedUse != null) ? ubr.ElectricAllocatedUse :
                                //                                    ((ubr.GasAllocatedUse != null) ? ubr.GasAllocatedUse :
                                //                                        ((ubr.OilAllocatedUse != null) ? ubr.OilAllocatedUse :
                                //                                            ((ubr.PropaneAllocatedUse != null) ? ubr.PropaneAllocatedUse :
                                //                                                ((ubr.SteamAllocatedUse != null) ? ubr.SteamAllocatedUse :
                                //                                                    ((ubr.CHWAllocatedUse != null) ? ubr.CHWAllocatedUse :
                                //                                                        ((ubr.WaterAllocatedUse != null) ? ubr.WaterAllocatedUse :
                                //                                                            ((ubr.EnergyAllocatedUse != null) ? ubr.EnergyAllocatedUse : 0
                                //                                        )))))))));

                                seriesAllocatedUse.Points.DataBindXY(eaResults, "EndDate", eaResults, "PropaneAllocatedUse");

                                //TODO: on front end after microsoft bug fix.
                                //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                chartPropaneAllocatedUse.ChartAreas["propaneAllocatedUseChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                chartPropaneAllocatedUse.ChartAreas["propaneAllocatedUseChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                //add series to chart
                                chartPropaneAllocatedUse.Series.Add(seriesAllocatedUse);

                                chartPropaneAllocatedUse.Visible = true;
                            }
                            else if(eaFirstResults.SteamAllocatedUse != null)
                            {
                                //set axis intervaltype
                                chartSteamAllocatedUse.ChartAreas["steamAllocatedUseChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                //set axis format:  g = short date and time, d = short date
                                chartSteamAllocatedUse.ChartAreas["steamAllocatedUseChartArea"].AxisX.LabelStyle.Format = "d";

                                //add new data to series
                                //seriesAllocatedUse.Points.AddXY(ubr.EndDate, ((ubr.ElectricAllocatedUse != null) ? ubr.ElectricAllocatedUse :
                                //                                    ((ubr.GasAllocatedUse != null) ? ubr.GasAllocatedUse :
                                //                                        ((ubr.OilAllocatedUse != null) ? ubr.OilAllocatedUse :
                                //                                            ((ubr.PropaneAllocatedUse != null) ? ubr.PropaneAllocatedUse :
                                //                                                ((ubr.SteamAllocatedUse != null) ? ubr.SteamAllocatedUse :
                                //                                                    ((ubr.CHWAllocatedUse != null) ? ubr.CHWAllocatedUse :
                                //                                                        ((ubr.WaterAllocatedUse != null) ? ubr.WaterAllocatedUse :
                                //                                                            ((ubr.EnergyAllocatedUse != null) ? ubr.EnergyAllocatedUse : 0
                                //                                        )))))))));

                                seriesAllocatedUse.Points.DataBindXY(eaResults, "EndDate", eaResults, "SteamAllocatedUse");

                                //TODO: on front end after microsoft bug fix.
                                //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                chartSteamAllocatedUse.ChartAreas["steamAllocatedUseChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                chartSteamAllocatedUse.ChartAreas["steamAllocatedUseChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                //add series to chart
                                chartSteamAllocatedUse.Series.Add(seriesAllocatedUse);

                                chartSteamAllocatedUse.Visible = true;
                            }                            
                            else if(eaFirstResults.WaterAllocatedUse != null) 
                            {
                                //set axis intervaltype
                                chartWaterAllocatedUse.ChartAreas["waterAllocatedUseChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                //set axis format:  g = short date and time, d = short date
                                chartWaterAllocatedUse.ChartAreas["waterAllocatedUseChartArea"].AxisX.LabelStyle.Format = "d";

                                //add new data to series
                                //seriesAllocatedUse.Points.AddXY(ubr.EndDate, ((ubr.ElectricAllocatedUse != null) ? ubr.ElectricAllocatedUse :
                                //                                    ((ubr.GasAllocatedUse != null) ? ubr.GasAllocatedUse :
                                //                                        ((ubr.OilAllocatedUse != null) ? ubr.OilAllocatedUse :
                                //                                            ((ubr.PropaneAllocatedUse != null) ? ubr.PropaneAllocatedUse :
                                //                                                ((ubr.SteamAllocatedUse != null) ? ubr.SteamAllocatedUse :
                                //                                                    ((ubr.CHWAllocatedUse != null) ? ubr.CHWAllocatedUse :
                                //                                                        ((ubr.WaterAllocatedUse != null) ? ubr.WaterAllocatedUse :
                                //                                                            ((ubr.EnergyAllocatedUse != null) ? ubr.EnergyAllocatedUse : 0
                                //                                        )))))))));

                                seriesAllocatedUse.Points.DataBindXY(eaResults, "EndDate", eaResults, "WaterAllocatedUse");

                                //TODO: on front end after microsoft bug fix.
                                //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                chartWaterAllocatedUse.ChartAreas["waterAllocatedUseChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                chartWaterAllocatedUse.ChartAreas["waterAllocatedUseChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                //add series to chart
                                chartWaterAllocatedUse.Series.Add(seriesAllocatedUse);

                                chartWaterAllocatedUse.Visible = true;
                            }
                            

                            //Allocated Cost---
                            //create new series, set chart type and name
                            var seriesAllocatedCost =
                            new Series
                            {
                                ChartType = SeriesChartType.Column,
                                //BorderWidth = 2,
                                Name = longName,
                                ToolTip = "X: #VALX{d}, Y: #VALY",
                                LegendToolTip = longName,
                                LegendText = longName,
                            };

                            //set series name as point name

                            //set axis intervaltype
                            chartAllocatedCost.ChartAreas["allocatedCostChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                            //set axis format:  g = short date and time, d = short date
                            chartAllocatedCost.ChartAreas["allocatedCostChartArea"].AxisX.LabelStyle.Format = "d";

                            //add new data to series
                            seriesAllocatedCost.Points.DataBindXY(eaResults, "EndDate", eaResults, "AllocatedCost");

                            //TODO: on front end after microsoft bug fix.
                            //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                            chartAllocatedCost.ChartAreas["allocatedCostChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                            chartAllocatedCost.ChartAreas["allocatedCostChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                            //add series to chart
                            chartAllocatedCost.Series.Add(seriesAllocatedCost);
                        }                        
                    }

                divAllocatedUse.Visible = divAllocatedCost.Visible = true;
            }           

            //-----
            ControlHelper.HideControls(new Control[] { analysisDetailsTop, analysisDetailsBottom, divDownloadBoxInTabControl, lblErrorTop });
        }

        private void BindGrid(IEnumerable<UtilityBillingResult> list)
        {
            gridUtility.DataSource = String.IsNullOrWhiteSpace(gridViewSortExpression) ? list : EnumerableHelper.CreateSortedEnumerable(list, gridViewSortExpression, GridViewSortDirection);
            gridUtility.DataBind();
        }

        private IEnumerable<UtilityBillingResult> BuildUtility()
        {
            switch (currentMode)
            {
                case LinkHelper.ViewByMode.Building:
                {
                    bindingParameters.equipmentID = null;

                    goto case LinkHelper.ViewByMode.Equipment;
                }
                case LinkHelper.ViewByMode.Equipment:
                {
                    bindingParameters.analysisID = null;

                    break;
                }
            }

            var pb = new UtilityBillingBinder(siteUser, DataMgr);

            var results = pb.BuildUtilityBills(bindingParameters).ToList();

            var resultCount = results.Count;
            var rangeText = String.Format("{0} to {1}", bindingParameters.startingEndDate.ToShortDateString(), bindingParameters.endingEndDate.ToShortDateString());

            lblResults.Text =
                    String.Format
                            (
                             String.Concat
                                     (
                                      "{0} data record",
                                      ((resultCount == 1) ? String.Empty : "s"),
                                      " found for {1}. "
                                     ),
                             new[]
                             {
                                     resultCount.ToString(),
                                     rangeText,
                             }
                            );

            hdnResultCount.Value = resultCount.ToString();

            return results;
        }

        protected void PopulateAnalysisInfo(int cid, string clientName, int bid, string buildingName, int eid, string equipmentName, int aid, string analysisName, DateTime endDate)
        {
            try
            {
                //get based on submitted, not by selected at the moment

                //get utilty bill
                CW.Data.Models.UtilityBilling.UtilityBillingResult utilityResult = DataMgr.UtilityBillingDataMapper.GetUtilityBillAsResult(DataConstants.UtiliityAnalysisTypes.Utility, cid, bid, eid, aid, endDate);

                //test notes format
                //utilityResult.Notes = "<table style='boarder: 1px solid #666;'><thead style='background-color: #EFEFEF;'><tr style='background-color: #DFDFDF;'><th colspan='4' style='padding: 2px 6px; font-weight: bolder; text-align: left;'>Total Tenant Electric Allocation</th></tr><tr align='left'><th style='padding: 2px 6px; font-weight: bolder; text-align: center;'>Tenant</th><th style='padding: 2px 6px; font-weight: bolder; text-align: center;'>Use (kWh)</th><th style='padding: 2px 6px; font-weight: bolder; text-align: center;'>Cost</th><th style='padding: 2px 6px; font-weight: bolder; text-align: center;'>Percent</th></tr></thead><tbody><tr><td style='padding: 2px; text-align: left;'>WPI-FPE</td><td style='padding: 2px; text-align: left;'>16042</td><td style='padding: 2px; text-align: left;'>$1604</td><td>16%</td></tr><tr><td>WPI-SOB</td><td style='padding: 2px; text-align: left;'>14093</td><td style='padding: 2px; text-align: left;'>$1409</td><td style='padding: 2px; text-align: left;'>14%</td></tr><tr><td style='padding: 2px; text-align: left;'>BSB/MBI</td><td style='padding: 2px; text-align: left;'>1727</td><td style='padding: 2px; text-align: left;'>$173</td><td style='padding: 2px; text-align: left;'>2%</td></tr><tr><td style='padding: 2px; text-align: left;'>WPI/BETC</td><td style='padding: 2px; text-align: left;'>4637</td><td style='padding: 2px; text-align: left;'>$464</td><td style='padding: 2px; text-align: left;'>5%</td></tr><tr><td style='padding: 2px; text-align: left;'>Siemens</td><td style='padding: 2px; text-align: left;'>60537</td><td style='padding: 2px; text-align: left;'>$6054</td><td style='padding: 2px; text-align: left;'>61%</td></tr></tbody></table>";

                if (utilityResult != null)
                {
                    //Query the url of the graphic
                    //Figure df = mDataManager.FigureAndNoteDataMapper.GetFigure(analysisRange, eid, aid, startDate);

                    if (!String.IsNullOrEmpty(utilityResult.FigureBlobExt))
                    {
                        //FigureBlobDataContext blobContext = new FigureBlobDataContext(mAccount, df.EID, df.AID);
                        //BlobProperties properties = new BlobProperties();
                        //var byteArray = blobContext.GetBlob(df.StartDate, analysisRange, Path.GetExtension(df.BlobName)).Figure;
                        var byteArray = new byte[0];  //TODO: mDataManager.FigureAndNoteDataMapper.GetFigureBlob(analysisRange, utilityResult);

                        //MemoryStream imageStream = 

                        //GetBlobContent(df.BlobName, out properties);
                        if (byteArray == null)
                        {
                            // clear image graph and hide div
                            binImageGraph.Visible = false;
                            divGraph.Visible = false;
                        }
                        else
                        {
                            binImageGraph.DataValue = byteArray;
                            binImageGraph.Visible = true;
                            divGraph.Visible = true;
                        }
                        
                        //binImageGraph.ImageUrl = BlobHelper.FigureBlobUrl(df.EID, df.AID, df.BlobName);
                        DataMgr.DefaultCacheRepository.Upsert(GetImageKey(), binImageGraph.DataValue);
                        //sessionState.Set("imgStream", binImageGraph.DataValue);
                    }
                    else
                    {
                        // clear image graph and add hide div
                        binImageGraph.Visible = false;
                        divGraph.Visible = false;

                        //clear image by setting empty byte array, and set no graph alt text.
                        //DataValue = new byte[0];
                        //binImageGraph.AlternateText = "Graph not available.";
                    }

                    if (siteUser.IsKGSFullAdminOrHigher)
                    {
                        lblClientName.Text = clientName; // utilityResult.ClientName;
                        hdnCID.Value = cid.ToString();

                        divAnalysisClient.Visible = true;
                    }
                    lblBuildingName.Text = buildingName; //utilityResult.BuildingName;
                    lblEquipmentName.Text = equipmentName; //utilityResult.EquipmentName;

                    //used to get extra equipment info for the report on downloadclick
                    hdnEID.Value = eid.ToString();

                    lblAnalysisName.Text = analysisName; // utilityResult.AnalysisName;
                    lblStartDate.Text = utilityResult.StartDate.ToShortDateString();
                    lblEndDate.Text = utilityResult.EndDate.AddDays(-1).ToShortDateString();

                    litNotes.Text = utilityResult.Notes;
                    divNotes.Visible = String.IsNullOrEmpty(utilityResult.Notes) ? false : true;

                    lblPoints.Text = StringHelper.ParseStringArrayToCSVString(DataMgr.PointDataMapper.GetAllVisiblePointsByCommonPointTypesByAIDAndEID(utilityResult.AID, utilityResult.EID).Select(p => p.PointName).ToArray(), true);

                    //show top and bottom details
                    analysisDetailsTop.Visible = true;
                    analysisDetailsBottom.Visible = true;

                    //details label
                    lblDetails.Text = String.Format("Utility bill {0} allocation data for {1} performed on {2}.", utilityResult.AnalysisName, utilityResult.EquipmentName, utilityResult.EndDate.AddDays(-1).ToShortDateString());

                    //bind equipment variables
                    //BindEquipmentVariables(eid);
                    divVariables.Visible = false;

                    lnkSetFocusView.Focus();
                }
                else
                {
                    //hide bottom details
                    analysisDetailsBottom.Visible = false;

                    //show top
                    lblDetails.Text = "No allocations data exists for your selection.";
                    lnkSetFocusMessage.Focus();
                    analysisDetailsTop.Visible = true;
                }

                divDownloadBoxInTabControl.Visible = analysisDetailsBottom.Visible;
                //divEmail.Visible = analysisInfo.Visible;
                //divPdfDownload.Visible = analysisInfo.Visible;                       
            }
            catch(SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving and populating utility allocation.", sqlEx);
            }
            catch(Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving and populating utility allocation.", ex);
            }
        }

        #endregion

        #region Helper Methods

        private void ClearMessaging()
        {
            DisplayErrorOutput("", lblError);
            DisplayErrorOutput("", lblErrorTop);
        }

        private void ProcessRequestWithQueryString()
        {
            var qs = queryString.ToObject<UtilityBillingParamsObject>();

            if (qs == null) return;

            if (qs.cid != siteUser.CID) return;

            var modes =
                new Dictionary<String, LinkHelper.ViewByMode>
                {
                    {
                        PropHelper.G<UtilityBillingParamsObject>(x => x.aid),
                        LinkHelper.ViewByMode.Analysis
                    },
                    {
                        PropHelper.G<UtilityBillingParamsObject>(x => x.eid),
                        LinkHelper.ViewByMode.Equipment
                    },
                    {
                        PropHelper.G<UtilityBillingParamsObject>(x => x.bid),
                        LinkHelper.ViewByMode.Building
                    },
                };

            LinkHelper.ViewByMode? mode = null;

            foreach (var m in modes)
            {
                if (queryString.Get<Int32>(m.Key) == null)
                {
                    if (mode == null) continue;

                    return;
                }

                if (mode == null)
                {
                    mode = m.Value;
                }
            }

            if (mode == null) return;

            currentMode = mode.Value;

            bindingParameters =
            new UtilityBillingBindingParameters
            {
                startingEndDate = qs.sed,
                endingEndDate = qs.eed,

                clientID = qs.cid,
                buildingID = qs.bid,               
                equipmentID = qs.eid,
                analysisID = qs.aid,
            };

            Generate();

            ApplyBindingParameters();
        }

        private void ProcessRequest()
        {
            currentMode = LinkHelper.ViewByMode.Building;

            bindingParameters =
            new UtilityBillingBindingParameters
            {
                clientID = siteUser.CID,
                buildingID = siteUser.VisibleBuildings.First().BID,
                equipmentID = -1,
                analysisID = -1,
            };

            DateTimeHelper.GenerateDefaultDatesPastXMonthsEndingToday(out bindingParameters.startingEndDate, out bindingParameters.endingEndDate, siteUser.UserTimeZoneOffset);

            Generate();

            ApplyBindingParameters(true);
        }

        private void ApplyBindingParameters(Boolean initialLoad = false)
        {
            txtStartingEndDate.SelectedDate = bindingParameters.startingEndDate;
            txtEndingEndDate.SelectedDate = bindingParameters.endingEndDate;

            ControlHelper.DropDownListControl.SelectItem(ddlBuildings, bindingParameters.buildingID ?? 0);

            if (initialLoad) return;

            ControlHelper.DropDownListControl.SelectItem(ddlEquipment, bindingParameters.equipmentID ?? 0);
            ddlAnalysis.SelectedValue = bindingParameters.analysisID.ToString();
        }

        private void VisibilityHelper(LinkHelper.ViewByMode mode)
        {
            switch (mode)
            {
                case LinkHelper.ViewByMode.Building:
                {
                    pnlEquipment.Visible = false;
                    equipmentRequiredValidator.Enabled = false;

                    pnlAnalysis.Visible = false;
                    analysisRequiredValidator.Enabled = false;

                    dropDownSequence.ResetSubDropDownsOf(ddlBuildings, true);

                    break;
                }
                case LinkHelper.ViewByMode.Equipment:
                {
                    pnlEquipment.Visible = true;
                    equipmentRequiredValidator.Enabled = true;

                    pnlAnalysis.Visible = false;
                    analysisRequiredValidator.Enabled = false;

                    dropDownSequence.ResetSubDropDownsOf(ddlEquipment, true);

                    break;
                }
                case LinkHelper.ViewByMode.Analysis:
                {
                    pnlEquipment.Visible = true;
                    equipmentRequiredValidator.Enabled = true;

                    pnlAnalysis.Visible = true;
                    analysisRequiredValidator.Enabled = true;

                    break;
                }
            }
        }

        protected ITextSharpHelper.AnalysisPDFReport CreateAnalysisPDFReportDTO()
        {
            var dto = new ITextSharpHelper.AnalysisPDFReport();

            Equipment equipment = DataMgr.EquipmentDataMapper.GetEquipmentByID(Convert.ToInt32(hdnEID.Value));

            if (divAnalysisClient.Visible)
            {
                dto.ClientName = lblClientName.Text;
            }
            dto.BuildingName = lblBuildingName.Text;
            dto.EquipmentName = lblEquipmentName.Text;
            dto.EquipmentLocation = equipment.EquipmentLocation;
            dto.AnalysisName = lblAnalysisName.Text;
            dto.Points = Convert.ToBoolean(pointsCollapsiblePanelExtender.ClientState) ? "" : lblPoints.Text;
            dto.StartDate = lblStartDate.Text; //BindingParameters.start.Date.ToString();
            dto.EndDate = lblEndDate.Text;
            //dto.CostSavings = lblCost.Text;
            //dto.ComfortPriority = lblComfort.Text;
            //dto.EnergyPriority = lblEnergy.Text;
            //dto.MaintenancePriority = lblMaintenance.Text;
            dto.Notes = litNotes.Text;
            dto.ImageByteArray = DataMgr.DefaultCacheRepository.Get<Byte[]>(GetImageKey());
                //sessionState.Get<Byte[]>("imgStream");
            //analysisPDFReport.ImageFilePath       = ConfigurationManager.AppSettings["ChartAssetPath"] + imgGraph.ImageUrl.Remove(0, imgGraph.ImageUrl.LastIndexOf("/") + 1);

            return dto;
        }

        private string GetImageKey()
        {
            return String.Format("{0}-{1}-{2}", HttpContext.Current.Session.SessionID, ID, "imgStream");
        }

        //protected String BuildQuickLink(Object dataItem, ViewByMode mode)
        //{
        //    var result = (UtilityDataMapper.UtilityResult)dataItem;

        //    var link = "Utility.aspx?";

        //    QueryString.AppendToQueryString(ref link, "cid", result.CID);

        //    switch (mode)
        //    {
        //        case ViewByMode.Analysis:
        //        {
        //            QueryString.AppendToQueryString(ref link, "aid", result.AID);

        //            goto case ViewByMode.Equipment;
        //        }
        //        case ViewByMode.Equipment:
        //        {
        //            QueryString.AppendToQueryString(ref link, "eid", result.EID);

        //            goto case ViewByMode.EquipmentClass;
        //        }
        //        case ViewByMode.EquipmentClass:
        //        {
        //            QueryString.AppendToQueryString(ref link, "ecid", result.EquipmentClassID);

        //            goto case ViewByMode.Building;
        //        }
        //        case ViewByMode.Building:
        //        {
        //            QueryString.AppendToQueryString(ref link, "bid", result.BID);

        //            break;
        //        }
        //    }

        //    QueryString.AppendToQueryString(ref link, "rng", rblRange.SelectedValue);
        //    QueryString.AppendToQueryString(ref link, "sd", result.StartDate.ToShortDateString());

        //    return link;
        //}

        private void ClearAllChartSeries()
        {
            chartCHWAllocatedUse.Series.Clear();
            chartElectricAllocatedUse.Series.Clear();
            chartEnergyAllocatedUse.Series.Clear();
            chartGasAllocatedUse.Series.Clear();
            chartOilAllocatedUse.Series.Clear();
            chartPropaneAllocatedUse.Series.Clear();
            chartSteamAllocatedUse.Series.Clear();
            chartWaterAllocatedUse.Series.Clear();

            chartAllocatedCost.Series.Clear();
        }

        private void ResetAllChartVisibility()
        {
            chartCHWAllocatedUse.Visible = chartElectricAllocatedUse.Visible = chartEnergyAllocatedUse.Visible = chartGasAllocatedUse.Visible = chartOilAllocatedUse.Visible = chartPropaneAllocatedUse.Visible = chartSteamAllocatedUse.Visible = chartWaterAllocatedUse.Visible =
            divAllocatedUse.Visible = divAllocatedCost.Visible = false;
        }

        #endregion
    }
}