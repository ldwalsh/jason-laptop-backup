﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Security.Claims;
using System.Web.Security;
using System.Threading.Tasks;
using System.Configuration;
using Newtonsoft.Json.Linq;
using System.Collections;
using CW.RESTAdapter.Configuration;
using CW.Common.Constants;

namespace CW.Website
{
    static public class AuthnHelper
    {
        /// <summary>
        /// Both oid and alias cannot be null; at least one MUST have a value
        /// </summary>
        /// <param name="oid"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        static public async Task<AuthNSSOInfo> GetSSOInfoAsync(int? oid = null, Guid? alias = null)
        {
            if (null == oid && null == alias)
            {
                throw new Exception("Both identifiers cannot be null");
            }
            AuthNSSOInfo result = null;
            RESTSSO restSSO = new RESTSSO(ConfigurationManager.AppSettings["DeploymentID"]);
            // Could be a string (DA version 2.0.0.3 or lower) or json (DA version 2.0.0.4 or higher)
            try
            {
                JObject json = null;
                if (null != oid)
                {
                    json = await restSSO.IsOrganizationEnabledForSSO(Convert.ToString(oid.Value)).ConfigureAwait(false);
                }
                else
                {
                    json = await restSSO.IsOrganizationEnabledForSSO(alias.Value).ConfigureAwait(false);
                    if (null != json)
                    {
                        json["aliasoid"] = alias.Value.ToString("D");
                    }
                }
                IEnumerable tokenTypes = null;
                JArray coll = json["tokentypes"] as JArray;
                if (null != coll)
                {
                    tokenTypes = coll.Select(_ => new { Name = _["name"].Value<string>() });
                }
                string aliasoid = null == json["aliasoid"] ? null : json["aliasoid"].Value<string>();
                bool hasMetadata = null == json["hasMetadata"] ? false : json["hasMetadata"].Value<bool>();
                bool allowMetadata = null == json["allowMetadata"] ? false : json["allowMetadata"].Value<bool>();
                result = new AuthNSSOInfo(json["ssoenabled"].Value<bool>(), json["hascert"].Value<bool>(), json["etag"].Value<string>(), json["tokentype"].Value<string>(), tokenTypes, aliasoid, hasMetadata, allowMetadata);
                if (result.SSOEnabled)
                {
                    if( null == oid)
                    {
                        var orgId = json["oid"];
                        if (null != orgId)
                        {
                            oid = Convert.ToInt32(orgId.Value<string>());
                        }
                    }
                    result.OID = oid.Value;
                }
            }
            catch (FormatException)
            {
                //Invalid json format
                bool isSSO = false;
                if (bool.TryParse(restSSO.Response, out isSSO))
                {
                    result = new AuthNSSOInfo(isSSO);
                }
            }
            catch (Exception ex)
            {
                bool isSSO = false;
                if (bool.TryParse(restSSO.Response, out isSSO))
                {
                    result = new AuthNSSOInfo(isSSO);
                }
            }
            return (result?? new AuthNSSOInfo(false));
        }

        private const string LoginStateKey = "ls";
        static private string CreateLoginStateUrl(HttpRequest req, string referrer, string emailAddress = null)
        {
            string partialUrl = "";
            if (!string.IsNullOrWhiteSpace(referrer) && !referrer.ToUpper().Contains("HOME.ASPX"))
            {
                partialUrl = referrer;
            }
            else
            {
                partialUrl = "Home.aspx";
            }

            Uri baseUri = new Uri(req.Url.GetLeftPart(UriPartial.Authority));
            Uri fullUrl = new Uri(baseUri, partialUrl);
            string url = fullUrl.PathAndQuery;
            
            if (!string.IsNullOrEmpty(emailAddress))
            {
                string pairToAdd = LoginStateKey + "=" + CreateMD5(emailAddress);
                if (!url.Contains(pairToAdd))
                {
                    url += (url.Contains('?') ? "&" : "?") + pairToAdd;
                }
            }
            
            // url will always start with a forward slash
            return ("~" + url);
        }

        static public bool CompareLoginStateInfo(HttpRequest req, string userEmailAddress, ref bool authnFromIdP)
        {
            authnFromIdP = false;
            bool matches = false;
            string loginstate = req[LoginStateKey];
            if (string.IsNullOrEmpty(loginstate))
            {
                // Let's check referrer 
                string referrer = req.QueryString[CW.Common.Constants.DataConstants.ReferrerQSKey];
                if (!string.IsNullOrEmpty(referrer))
                {
                    // Remove everything from index = 0 up to and including the leading "?"
                    int index = referrer.IndexOf('?');
                    if (index > 0)
                    {
                        referrer = referrer.Remove(0, index + 1);
                    }
                    System.Collections.Specialized.NameValueCollection collection = HttpUtility.ParseQueryString(referrer);
                    if (collection.AllKeys.Contains(LoginStateKey))
                    {
                        loginstate = collection[LoginStateKey];
                    }
                }
            }
            if (!string.IsNullOrEmpty(loginstate))
            {
                authnFromIdP = true;
                try
                {
                    string s = AuthnHelper.CreateMD5(userEmailAddress);
                    if (loginstate == s)
                    {
                        matches = true;
                    }
                }
                catch (Exception)
                {
                }
            }
            return (matches);
        }

        static private string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input.ToLower());
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            // Convert the byte array to hexadecimal string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
            {
                sb.Append(hashBytes[i].ToString("X2"));
            }
            return (sb.ToString());
        }

        static public string Signin(string data)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(data);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                sb.Append(bytes[i].ToString("X2"));
            }
            return (sb.ToString());
        }

        static public void OnSignin(HttpRequest req, HttpResponse res)
        {
            CW.Logging.ISectionLogManager logger = CW.Website.DependencyResolution.IoC.Resolve<CW.Logging.ISectionLogManager>();
            logger.Log(Common.Constants.DataConstants.LogLevel.INFO, "In AuthnHelper.OnSignin()");
            string urlToGo = "";
            var principal = (ClaimsPrincipal)Thread.CurrentPrincipal;
            Claim emailClaim = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
            logger.Log(Common.Constants.DataConstants.LogLevel.INFO, "Claim exists = " + (null == emailClaim ? "No" : "Yes"));
            Claim conflictClaim = principal.Claims.FirstOrDefault(c => c.Type.EndsWith("cwconflict"));
            if (null != emailClaim || null != conflictClaim)
            {
                string emailAddress = null;
                if (null != emailClaim)
                {
                    bool rememberMe = false;
                    //var token = new System.IdentityModel.Tokens.SessionSecurityToken(principal);
                    //var sam = FederatedAuthentication.SessionAuthenticationModule;
                    //sam.WriteSessionTokenToCookie(token);
                    //set rememberme email cookie if exists
                    HttpCookie cookie = HttpContext.Current.Request.Cookies[CW.Website._controls.Login.cookieName];
                    if (null != cookie)
                    {
                        FormsAuthenticationTicket ticket = null;
                        try
                        {
                            ticket = FormsAuthentication.Decrypt(cookie.Value);

                            if (!ticket.Expired)
                            {
                                rememberMe = true;
                            }
                        }
                        catch (HttpUnhandledException huex)
                        {
                            logger.Log(DataConstants.LogLevel.ERROR, $"Error decrypting remember me cookie value for email: {emailClaim?.Value}", huex);
                        }
                    }

                    FormsAuthentication.SetAuthCookie(emailClaim.Value, rememberMe); // Email address
                    Claim orgClaim = principal.Claims.FirstOrDefault(c => c.Type.EndsWith("organizationid"));
                    if (null != orgClaim)
                    {
                        emailAddress = emailClaim.Value;
                    }
                }
                else if (null != conflictClaim)
                {
                    emailAddress = conflictClaim.Value;
                }

                // If orgClaim is present, implies SSO authenticated; otherwise non SSO (hybrid) and seamless.
                string referrer = "";
                Claim referrerClaim = principal.Claims.FirstOrDefault(c => c.Type.EndsWith("cwreferrer"));
                if (null != referrerClaim)
                {
                    referrer = referrerClaim.Value;
                }

                urlToGo = CreateLoginStateUrl(req, referrer, emailAddress);
            }
            else
            {
                urlToGo = "~/Home.aspx";
            }

            logger.Log(Common.Constants.DataConstants.LogLevel.INFO, "urlToGo = " + urlToGo);
            res.Redirect(urlToGo);
        }

        static public void OnSignout(HttpContext context)
        {
            bool isHttpsEnabled;
            bool.TryParse(ConfigurationManager.AppSettings["IsHttpsEnabled"], out isHttpsEnabled);

            // We do not signout of IdP
            //FederatedAuthentication.WSFederationAuthenticationModule.SignOut();
            try
            {
                DateTime UtcNow = DateTime.UtcNow;
                FormsAuthentication.SignOut();
                string cookieName = _framework.SiteUserControl.KeyForTimeoutCookie();
                HttpCookie ck = context.Request.Cookies[cookieName];
                if (null != ck)
                {
                    context.Response.Cookies.Remove(cookieName);
                    ck.HttpOnly = false;
                    ck.Secure = isHttpsEnabled;
                    ck.Expires = UtcNow.AddDays(-10);
                    ck.Value = null;
                    context.Response.SetCookie(ck);
                }

                var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, string.Empty) { Expires = UtcNow.AddYears(-1), HttpOnly = true };
                context.Response.Cookies.Add(authCookie);
                var sessionCookie = new HttpCookie("ASP.NET_SessionId", string.Empty) { Expires = UtcNow.AddYears(-1), HttpOnly = true };
                context.Response.Cookies.Add(sessionCookie);
                CW.Business.DataManager.Delete(context.Session.SessionID);

            }
            finally
            {
                //clear user session
                //session.Clear();
                if (null != context.Session)
                {
                    context.Session.Abandon();
                }
                // We need the following to ensure that IsAuthenticated is reset to false
                context.User = new System.Security.Principal.GenericPrincipal(new System.Security.Principal.GenericIdentity(string.Empty), null);
            }
        }

        static private System.Text.RegularExpressions.Regex mLSRegex = new System.Text.RegularExpressions.Regex(LoginStateKey + "=([^&]*)", System.Text.RegularExpressions.RegexOptions.Compiled);
        /// <summary>
        /// This will remove the LoginState info from the url
        /// </summary>
        /// <param name="partialUrl"></param>
        /// <returns></returns>
        static public string Cleanse(string partialUrl)
        {
            string cleansedUrl = ExecuteCleanse(partialUrl.Trim());
            return (!cleansedUrl.StartsWith("/") ? "/" + cleansedUrl : cleansedUrl);
        }
        static private string ExecuteCleanse(string url)
        {
            string cleansedUrl = url;
            System.Text.RegularExpressions.MatchCollection matches = null;
            do
            {
                matches = mLSRegex.Matches(cleansedUrl);
                if (null != matches && matches.Count > 0)
                {
                    System.Text.RegularExpressions.Match match = matches[0];
                    int index = match.Index;
                    int len = match.Length;
                    // if cleansedUrl does not end with this match, it implies that there are more query string values. Therefore remove
                    // next char also after this match. This will take care of leading ? and &
                    int pos = index + len; // This is the index of the char after this match (may not be valid if cleansedUrl ends with this match)
                    if (pos < cleansedUrl.Length)
                    {
                        ++len;
                    }
                    cleansedUrl = cleansedUrl.Remove(index, len);
                }
            } while (null != matches && matches.Count > 0);
            cleansedUrl = cleansedUrl.Trim();
            int length = cleansedUrl.Length;
            char lastChar = cleansedUrl[length - 1];
            if ('?' == lastChar || '&' == lastChar)
            {
                cleansedUrl = cleansedUrl.Remove(length - 1);
            }
            return (cleansedUrl);
        }

        static public void OnRedirectingToIdentityProvider(HttpResponse response, IDictionary<string, string> signInRequestMessageParameters)
        {
            if (null != signInRequestMessageParameters)
            {
                if (signInRequestMessageParameters.ContainsKey("wctx"))
                {
                    string parameters = signInRequestMessageParameters["wctx"];
                    string[] p = parameters.Split('&');
                    var pair = p.FirstOrDefault(nvp => nvp.StartsWith("id"));
                    if (null != pair)
                    {
                        string[] pp = pair.Split('=');
                        if (null != pp && pp.Length > 1)
                        {
                            string payload = pp[1];
                            if (!string.IsNullOrEmpty(payload))
                            {
                                if ("passive" == payload)
                                {
                                    string reply = signInRequestMessageParameters["wreply"];
                                    // Possibly a bookmarked resource?
                                    // Let's get the requested url (ru)
                                    pair = p.FirstOrDefault(nvp => nvp.StartsWith("ru"));
                                    if (null != pair)
                                    {
                                        p = pair.Split('=');
                                        if (null != p && p.Length > 1)
                                        {
                                            string ru = p[1]; // Requested Url
                                            string urlPrevDecoded = "";
                                            do
                                            {
                                                urlPrevDecoded = ru;
                                                ru = HttpUtility.UrlDecode(ru);
                                            } while (urlPrevDecoded != ru);

                                            Uri baseUri = new Uri(reply);
                                            string qs = ("/" == urlPrevDecoded) ? "" : "?" + CW.Common.Constants.DataConstants.ReferrerQSKey + "=" + urlPrevDecoded;
                                            string url = "~/Home.aspx" + qs;
                                            response.Redirect(url);
                                        }
                                    }

                                    response.Redirect("~/Home.aspx");
                                }
                            }
                            else
                            {
                                response.Redirect("~/Home.aspx");
                            }
                        }
                    }
                }
            }
        }

        static public bool GetEmailAddressFromClaims( ref string emailAddress, ref int orgId)
        {
            emailAddress = null;
            orgId = 0;
            ClaimsPrincipal principal = Thread.CurrentPrincipal as ClaimsPrincipal;
            if (null != principal)
            {
                Claim emailClaim = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email);
                if (null != emailClaim)
                {
                    emailAddress = emailClaim.Value;
                }
                Claim orgClaim = principal.Claims.FirstOrDefault(c => c.Type.EndsWith("organizationid"));
                if (null != orgClaim)
                {
                    if (!int.TryParse(orgClaim.Value, out orgId) || 0 >= orgId)
                    {
                        orgId = 0;
                    }
                }
            }
            return (!string.IsNullOrEmpty(emailAddress) && orgId > 0);
        }
    }

    public class AuthNSSOInfo
    {
        public AuthNSSOInfo(bool ssoEnabled, bool certExists = false, string etag = null, string tokenType = null, IEnumerable tokenTypes = null, string aliasOID = null, bool hasMetadata = false, bool allowMetadata = false)
        {
            SSOEnabled = ssoEnabled;
            CertExists = certExists;
            ETag = etag;
            TokenType = tokenType;
            TokenTypes = tokenTypes;
            AliasOID = string.IsNullOrEmpty(aliasOID) ? null : aliasOID;
            HasMetadata = hasMetadata;
            AllowMetadata = allowMetadata;
        }

        public bool AllowMetadata { get; private set; }
        public bool HasMetadata { get; private set; }
        public int OID { get; set; }
        /// <summary>
        /// whether sso is enabled (true) or not (false) for organization
        /// </summary>
        public bool SSOEnabled { get; private set; }
        /// <summary>
        /// whether cert exists (true) or not (false) for organization
        /// </summary>
        public bool CertExists { get; private set; }
        /// <summary>
        /// TokenType
        /// </summary>
        public string TokenType { get; private set; }
        /// <summary>
        /// Collection of Token Types
        /// </summary>
        public IEnumerable TokenTypes { get; private set; }
        public string ETag { get; private set; }
        public string AliasOID { get; private set; }
    }
}