﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.WindowsAzure.ServiceRuntime;
using Telerik.Web.UI;

namespace CW.Website._customreports
{
    public class CustomReportsFileHelper
    {
        public static string ReplacePath(string path, string html, HtmlGenericControl div, Chart chart = null)
        {
            string handlerChartImg = "src=\"/ChartImg.axd?";

            if (chart != null)
            {
                if (div.Visible && chart.Visible)
                {
                    return Replace(path, html, handlerChartImg);
                }
            }
            else if (div.Visible)
            {
                return Replace(path, html, handlerChartImg);
            }

            return html;
        }
        //public static string ReplacePath(string path, string html, HiddenField hdn, Image img)
        //{
        //    int start = 0, end = 0, length = 0;
        //    string handlerChartImg = "src=\"BinaryImageHandler.axd?";

        //    if (Convert.ToBoolean(hdn.Value) && !String.IsNullOrEmpty(img.ImageUrl))
        //    {
        //        start = html.IndexOf(handlerChartImg);
        //        end = html.IndexOf('"', start + handlerChartImg.Length);
        //        length = (end - start) + 1;
        //        return html.Replace(html.Substring(start, length), path);
        //    }

        //    return html;
        //}
        public static string ReplacePath(string path, string html, HiddenField hdn, RadBinaryImage radImg)
        {
            string handlerChartImg = "src=\"BinaryImageHandler.axd?";

            if (Convert.ToBoolean(hdn.Value) && !String.IsNullOrEmpty(radImg.ImageUrl))
            {
                return Replace(path, html, handlerChartImg);
            }

            return html;
        }

        private static string Replace(string path, string html, string handlerChartImg)
        {
            int start = 0, end = 0, length = 0;

            start = html.IndexOf(handlerChartImg);

            if (start >= 0)
            {
                end = html.IndexOf('"', start + handlerChartImg.Length);
                length = (end - start) + 1;
                return html.Replace(html.Substring(start, length), path);
            }

            return html;
        }


        public static void SaveFile(string filepath, HtmlGenericControl div, Chart chart)
        {
            if (div.Visible && chart.Visible)
                chart.SaveImage(filepath);
        }
        //public static void SaveFile(string filepath, HiddenField hdn, RadBinaryImage img)
        //{
        //    if (Convert.ToBoolean(hdn.Value) && !String.IsNullOrEmpty(img.ImageUrl))
        //    {
        //        File.WriteAllBytes(filepath, img.DataValue);
                 
        //        //using (WebClient client = new WebClient ())
        //        //{
        //            //client.DownloadFile(img.ImageUrl, filepath);
        //        //}
        //    }         
        //}
        public static void SaveFile(string filepath, HiddenField hdn, HiddenField imgByteArray)
        {
            if (Convert.ToBoolean(hdn.Value) && !String.IsNullOrEmpty(imgByteArray.Value))
            {
                File.WriteAllBytes(filepath, Convert.FromBase64String((imgByteArray.Value)));
            }
        }

        public static void DeleteFile(string filepath, HtmlGenericControl div, Chart chart = null)
        {
            if (chart != null)
            {
                if (div.Visible && chart.Visible)
                    File.Delete(filepath);
            }
            else if (div.Visible)
            {
                File.Delete(filepath);
            }
        }
        public static void DeleteFile(string filepath, HiddenField hdn)
        {
            if (Convert.ToBoolean(hdn.Value))
                File.Delete(filepath);            
        }

    }
}