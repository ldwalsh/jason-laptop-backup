﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CW.Data.Models.Diagnostics;
using CW.Utility;

namespace CW.Website._customreports
{
    public class CustomReportsGridResultsHelper
    {
        public static IEnumerable<DiagnosticsBureauReportResult> CalculateAndMergeBuildingGridResults(IEnumerable<DiagnosticsResult> diagnosticsResults, double days, IEnumerable<DiagnosticsResult> previousDiagnosticsResults, double previousDays)
        {
            IEnumerable<DiagnosticsBureauReportResult> tempResults = CalculateBuildingGridResults(diagnosticsResults, days);
            IEnumerable<DiagnosticsBureauReportResult> tempPreviousResults = CalculateBuildingGridResults(previousDiagnosticsResults, previousDays);
            List<DiagnosticsBureauReportResult> newResults = new List<DiagnosticsBureauReportResult>();
            
            //merge. 
            //must do a foreach rather then a join, because the results sets might not always match if anayses werent ran.
            foreach (DiagnosticsBureauReportResult r in tempResults)
            {
                if (tempPreviousResults.Any())
                {
                    DiagnosticsBureauReportResult pr = tempPreviousResults.Where(tpr => tpr.BID == r.BID).FirstOrDefault();

                    if (pr != null)
                    {
                        r.PreviousTotalCostSavingsPerDay = pr.TotalCostSavings;
                        r.PreviousPercentEquipmentWithMaintenanceFaults = pr.PercentEquipmentWithMaintenanceFaults;
                        r.PreviousPercentEquipmentWithEnergyFaults = pr.PercentEquipmentWithEnergyFaults;
                        r.PreviousPercentEquipmentWithComfortFaults = pr.PercentEquipmentWithComfortFaults;
                    }
                }

                newResults.Add(r);
            }

            return newResults.OrderBy(b => b.BuildingName);


            //return (from tr in tempResults
            //        join tpr in tempPreviousResults on tr.BID equals tpr.BID
            //        select new DiagnosticsBureauReportResult
            //        {
            //            BID = tr.BID,
            //            BuildingName = tr.BuildingName,
            //            EquipmentCount = tr.EquipmentCount,
            //            EquipmentWithFaultsCount = tr.EquipmentWithFaultsCount,

            //            TotalCostSavings = tr.TotalCostSavings,
            //            TotalCostSavingsCultureFormated = tr.TotalCostSavingsCultureFormated,
            //            TotalCostSavingsPerDay = tr.TotalCostSavingsPerDay,
            //            PercentEquipmentWithMaintenanceFaults = tr.PercentEquipmentWithMaintenanceFaults,
            //            PercentEquipmentWithEnergyFaults = tr.PercentEquipmentWithEnergyFaults,
            //            PercentEquipmentWithComfortFaults = tr.PercentEquipmentWithComfortFaults,
            //            PercentEquipmentWithFaults = tr.PercentEquipmentWithFaults,
            //            PercentEquipmentWithoutFaults = tr.PercentEquipmentWithoutFaults,

            //            PreviousTotalCostSavingsPerDay = tpr.TotalCostSavings,
            //            PreviousPercentEquipmentWithMaintenanceFaults = tpr.PercentEquipmentWithMaintenanceFaults,
            //            PreviousPercentEquipmentWithEnergyFaults = tpr.PercentEquipmentWithEnergyFaults,
            //            PreviousPercentEquipmentWithComfortFaults = tpr.PercentEquipmentWithComfortFaults,

            //        }).OrderBy(b => b.BuildingName);
        }
        
        protected static IEnumerable<DiagnosticsBureauReportResult> CalculateBuildingGridResults(IEnumerable<DiagnosticsResult> results, double days)
        {
            return results
                    .GroupBy(g => g.BID)
                    .Select(s => new DiagnosticsBureauReportResult
                    {
                        BID = s.Key,
                        BuildingName = s.First().BuildingName,
                        EquipmentCount = s.Select(e => e.EID).Distinct().Count(),
                        EquipmentWithFaultsCount = s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count(),
                        TotalCostSavings = s.Sum(c => c.CostSavings),                       
                        TotalCostSavingsCultureFormated = CultureHelper.FormatCurrencyAsString(s.First().LCID, s.Sum(c => c.CostSavings)),                        
                        TotalCostSavingsPerDay = s.Sum(c => c.CostSavings) / days,
                        PercentEquipmentWithMaintenanceFaults = Math.Round((Convert.ToDouble(s.Where(d => d.MaintenancePriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                        PercentEquipmentWithEnergyFaults = Math.Round((Convert.ToDouble(s.Where(d => d.EnergyPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                        PercentEquipmentWithComfortFaults = Math.Round((Convert.ToDouble(s.Where(d => d.ComfortPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                        PercentEquipmentWithFaults = Math.Round((Convert.ToDouble(s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                        
                        //equipment that have diagnostics with no priority can still have diagnostics with a priority. must go minus off equipment with faults.
                        PercentEquipmentWithoutFaults = Math.Round(100 - ((Convert.ToDouble(s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100), 1, MidpointRounding.AwayFromZero),

                        //filter out 0's before averaging. cannot be empty.
                        //AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                        //AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                        //AverageEnergyPriority = s.Where(e => e.EnergyPriority != 0).Any() ? s.Where(e => e.EnergyPriority != 0).Average(e => e.EnergyPriority) : 0,
                    });
        }

        public static IEnumerable<DiagnosticsBureauReportResult> CalculateAndMergeEquipmentClassGridResults(IEnumerable<DiagnosticsResult> diagnosticsResults, double days, IEnumerable<DiagnosticsResult> previousDiagnosticsResults, double previousDays)
        {
            IEnumerable<DiagnosticsBureauReportResult> tempResults = CalculateEquipmentClassGridResults(diagnosticsResults, days);
            IEnumerable<DiagnosticsBureauReportResult> tempPreviousResults = CalculateEquipmentClassGridResults(previousDiagnosticsResults, previousDays);
            List<DiagnosticsBureauReportResult> newResults = new List<DiagnosticsBureauReportResult>();

            //merge. 
            //must do a foreach rather then a join, because the results sets might not always match if anayses werent ran.
            foreach (DiagnosticsBureauReportResult r in tempResults)
            {
                if (tempPreviousResults.Any())
                {
                    DiagnosticsBureauReportResult pr = tempPreviousResults.Where(tpr => tpr.EquipmentClassID == r.EquipmentClassID).FirstOrDefault();

                    if (pr != null)
                    {
                        r.PreviousTotalCostSavingsPerDay = pr.TotalCostSavings;
                        r.PreviousPercentEquipmentWithMaintenanceFaults = pr.PercentEquipmentWithMaintenanceFaults;
                        r.PreviousPercentEquipmentWithEnergyFaults = pr.PercentEquipmentWithEnergyFaults;
                        r.PreviousPercentEquipmentWithComfortFaults = pr.PercentEquipmentWithComfortFaults;
                    }
                }
                newResults.Add(r);
            }            

            return newResults.OrderBy(e => e.EquipmentClassName);

            //return (from tr in tempResults
            //        join tpr in tempPreviousResults on tr.EquipmentClassID equals tpr.EquipmentClassID
            //        select new DiagnosticsBureauReportResult
            //        {
            //            EquipmentClassID = tr.EquipmentClassID,
            //            EquipmentClassName = tr.EquipmentClassName,
            //            EquipmentCount = tr.EquipmentCount,
            //            EquipmentWithFaultsCount = tr.EquipmentWithFaultsCount,

            //            TotalCostSavings = tr.TotalCostSavings,
            //            TotalCostSavingsCultureFormated = tr.TotalCostSavingsCultureFormated,
            //            TotalCostSavingsPerDay = tr.TotalCostSavingsPerDay,
            //            PercentEquipmentWithMaintenanceFaults = tr.PercentEquipmentWithMaintenanceFaults,
            //            PercentEquipmentWithEnergyFaults = tr.PercentEquipmentWithEnergyFaults,
            //            PercentEquipmentWithComfortFaults = tr.PercentEquipmentWithComfortFaults,
            //            PercentEquipmentWithFaults = tr.PercentEquipmentWithFaults,
            //            PercentEquipmentWithoutFaults = tr.PercentEquipmentWithoutFaults,

            //            PreviousTotalCostSavingsPerDay = tpr.TotalCostSavings,
            //            PreviousPercentEquipmentWithMaintenanceFaults = tpr.PercentEquipmentWithMaintenanceFaults,
            //            PreviousPercentEquipmentWithEnergyFaults = tpr.PercentEquipmentWithEnergyFaults,
            //            PreviousPercentEquipmentWithComfortFaults = tpr.PercentEquipmentWithComfortFaults,

            //        }).OrderBy(e => e.EquipmentClassName);

        }

        protected static IEnumerable<DiagnosticsBureauReportResult> CalculateEquipmentClassGridResults(IEnumerable<DiagnosticsResult> results, double days)
        {
            return results
                    .GroupBy(g => g.EquipmentClassID)
                    .Select(s => new DiagnosticsBureauReportResult
                    {
                        EquipmentClassID = s.Key,
                        EquipmentClassName = s.First().EquipmentClassName,
                        EquipmentCount = s.Select(e => e.EID).Distinct().Count(),
                        EquipmentWithFaultsCount = s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count(),
                        TotalCostSavings = s.Sum(c => c.CostSavings),
                        TotalCostSavingsCultureFormated = CultureHelper.FormatCurrencyAsString(s.First().LCID, s.Sum(c => c.CostSavings)),
                        TotalCostSavingsPerDay = s.Sum(c => c.CostSavings) / days,
                        PercentEquipmentWithMaintenanceFaults = Math.Round((Convert.ToDouble(s.Where(d => d.MaintenancePriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                        PercentEquipmentWithEnergyFaults = Math.Round((Convert.ToDouble(s.Where(d => d.EnergyPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                        PercentEquipmentWithComfortFaults = Math.Round((Convert.ToDouble(s.Where(d => d.ComfortPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                        PercentEquipmentWithFaults = Math.Round((Convert.ToDouble(s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),

                        //equipment that have diagnostics with no priority can still have diagnostics with a priority. must go minus off equipment with faults.
                        PercentEquipmentWithoutFaults = Math.Round(100 - ((Convert.ToDouble(s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100), 1, MidpointRounding.AwayFromZero),

                        //filter out 0's before averaging. cannot be empty.
                        //AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                        //AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                        //AverageEnergyPriority = s.Where(e => e.EnergyPriority != 0).Any() ? s.Where(e => e.EnergyPriority != 0).Average(e => e.EnergyPriority) : 0,
                    });
        }

        public static IEnumerable<DiagnosticsBureauReportResult> CalculateAndMergeEquipmentGridResults(IEnumerable<DiagnosticsResult> diagnosticsResults, double days, IEnumerable<DiagnosticsResult> previousDiagnosticsResults, double previousDays)
        {
            IEnumerable<DiagnosticsBureauReportResult> tempResults = CalculateEquipmentGridResults(diagnosticsResults, days);
            IEnumerable<DiagnosticsBureauReportResult> tempPreviousResults = CalculateEquipmentGridResults(previousDiagnosticsResults, previousDays);
            List<DiagnosticsBureauReportResult> newResults = new List<DiagnosticsBureauReportResult>();

            //merge. 
            //must do a foreach rather then a join, because the results sets might not always match if anayses werent ran.
            foreach (DiagnosticsBureauReportResult r in tempResults) 
            {
                if (tempPreviousResults.Any())
                {
                    DiagnosticsBureauReportResult pr = tempPreviousResults.Where(tpr => tpr.AID == r.AID && tpr.EID == r.EID).FirstOrDefault();

                    if (pr != null)
                    {
                        r.PreviousTotalCostSavingsPerDay = pr.TotalCostSavings;
                        r.PreviousAverageMaintenancePriority = pr.AverageMaintenancePriority;
                        r.PreviousAverageComfortPriority = pr.AverageComfortPriority;
                        r.PreviousAverageEnergyPriority = pr.AverageEnergyPriority;
                    }
                }

                newResults.Add(r);
            }

            return newResults.OrderByDescending(t => t.TotalCostSavings).ThenBy(t => t.TotalPriority);

            //return (from tr in tempResults
            //        join tpr in tempPreviousResults on new { tr.EID, tr.AID } equals new { tpr.EID, tpr.AID }
            //        select new DiagnosticsBureauReportResult
            //        {
            //            AID = tr.AID,
            //            EID = tr.EID,
            //            AnalysisName = tr.AnalysisName,
            //            EquipmentName = tr.EquipmentName,
            //            BuildingName = tr.BuildingName,

            //            TotalCostSavings = tr.TotalCostSavings,
            //            TotalCostSavingsCultureFormated = tr.TotalCostSavingsCultureFormated,
            //            TotalCostSavingsPerDay = tr.TotalCostSavingsPerDay,
            //            Occurrences = tr.Occurrences,
            //            AverageMaintenancePriority = tr.AverageMaintenancePriority,
            //            AverageComfortPriority = tr.AverageComfortPriority,
            //            AverageEnergyPriority = tr.AverageEnergyPriority,
                         
            //            PreviousTotalCostSavingsPerDay = tpr.TotalCostSavings,
            //            PreviousAverageMaintenancePriority = tpr.AverageMaintenancePriority,
            //            PreviousAverageComfortPriority = tpr.AverageComfortPriority,
            //            PreviousAverageEnergyPriority = tpr.AverageEnergyPriority,

            //        }).OrderByDescending(t => t.TotalCostSavings).ThenBy(t => t.TotalPriority);
        }

        protected static IEnumerable<DiagnosticsBureauReportResult> CalculateEquipmentGridResults(IEnumerable<DiagnosticsResult> results, double days)
        {
            return results
                    .Where(p => p.TotalPriority != 0)
                    .GroupBy(g => new { g.AID, g.EID })
                    .Select(s => new DiagnosticsBureauReportResult
                    {
                        AID = s.Key.AID,
                        EID = s.Key.EID,
                        AnalysisName = s.First().AnalysisName,
                        EquipmentName = s.First().EquipmentName,
                        BuildingName = s.First().BuildingName,
                        TotalCostSavings = s.Sum(c => c.CostSavings),
                        TotalCostSavingsCultureFormated = CultureHelper.FormatCurrencyAsString(s.First().LCID, s.Sum(c => c.CostSavings)),
                        TotalCostSavingsPerDay = s.Sum(c => c.CostSavings) / days,
                        Occurrences = s.Where(t => t.TotalPriority != 0).Count(),
                        //filter out 0's before averaging. cannot be empty.
                        AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? Math.Round(s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority), 1, MidpointRounding.AwayFromZero) : 0,
                        AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? Math.Round(s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority), 1, MidpointRounding.AwayFromZero) : 0,
                        AverageEnergyPriority = s.Where(e => e.EnergyPriority != 0).Any() ? Math.Round(s.Where(e => e.EnergyPriority != 0).Average(e => e.EnergyPriority), 1, MidpointRounding.AwayFromZero) : 0,
                    });
        }    

    }
}