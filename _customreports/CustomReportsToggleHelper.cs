﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._customreports
{
    public class CustomReportsToggleHelper
    {
        public static void ToggleGridEdit(GridView gv)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                //if (row.RowState == DataControlRowState.Edit)
                //{
                //    ((Label)gridEnergy.Rows[row.RowIndex].Cells[3].Controls[2]).Text = ((TextBox)gridEnergy.Rows[row.RowIndex].Cells[3].Controls[4]).Text;
                //}

                //row.RowState = (row.RowState == DataControlRowState.Edit) ? DataControlRowState.Normal : DataControlRowState.Edit;

                if ((gv.Rows[row.RowIndex].Cells[0].Controls[3]).Visible)
                {
                    for (int c = 0; c < gv.Rows[row.RowIndex].Cells.Count; c++)
                    {
                        ((Label)gv.Rows[row.RowIndex].Cells[c].Controls[1]).Text = ((TextBox)gv.Rows[row.RowIndex].Cells[c].Controls[3]).Text;

                        (gv.Rows[row.RowIndex].Cells[c].Controls[3]).Visible = false;
                        (gv.Rows[row.RowIndex].Cells[c].Controls[1]).Visible = true;
                    }
                }
                else
                {
                    for (int c = 0; c < gv.Rows[row.RowIndex].Cells.Count; c++)
                    {
                        (gv.Rows[row.RowIndex].Cells[c].Controls[3]).Visible = true;
                        (gv.Rows[row.RowIndex].Cells[c].Controls[1]).Visible = false;
                    }
                }
            }
        }

        public static void ToggleDoulbeRowGridEdit(GridView gv)
        {
            foreach (GridViewRow row in gv.Rows)
            {
                if ((gv.Rows[row.RowIndex].Cells[0].Controls[3]).Visible)
                {
                    for (int c = 1; c < gv.Rows[row.RowIndex].Cells[0].Controls.Count; c = c + 4)
                    {
                        ((Label)gv.Rows[row.RowIndex].Cells[0].Controls[c]).Text = ((TextBox)gv.Rows[row.RowIndex].Cells[0].Controls[c+2]).Text;

                        (gv.Rows[row.RowIndex].Cells[0].Controls[c+2]).Visible = false;
                        (gv.Rows[row.RowIndex].Cells[0].Controls[c]).Visible = true;
                    }
                }
                else
                {
                    for (int c = 1; c < gv.Rows[row.RowIndex].Cells[0].Controls.Count; c = c + 4)
                    {
                        (gv.Rows[row.RowIndex].Cells[0].Controls[c + 2]).Visible = true;
                        (gv.Rows[row.RowIndex].Cells[0].Controls[c]).Visible = false;
                    }
                }
            }
        }

        public static void ToggleListViewEdit(ListView lv)           
        {
            foreach (ListViewDataItem item in lv.Items)
            {  
                int counter = 1;

                while(counter < item.Controls.Count)
                {
                    if ((item.Controls[counter + 4]).Visible)
                    {
                        ((Label)item.Controls[counter + 2]).Text = ((TextBox)item.Controls[counter + 4]).Text;

                        (item.Controls[counter + 4]).Visible = false;
                        (item.Controls[counter + 2]).Visible = true;
                    }
                    else
                    {
                        (item.Controls[counter + 4]).Visible = true;
                        (item.Controls[counter + 2]).Visible = false;
                    }

                    counter = counter + 6;
                }
            }
        }

        public static void ToggleRadEdit(RadEditor re, Literal lit)
        {
            if (re.Visible)
            {
                lit.Text = re.Content;
            }
            else
            {
                re.Content = lit.Text;
            }

            lit.Visible = re.Visible;
            re.Visible = !re.Visible;
        }
    }
}