﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using CW.Utility;

namespace CW.Website._customreports
{
    public class CustomReportsChartHelper
    {
        public static void PrePaintLegends(Chart c)
        {
            c.Legends[0].Position.Width = 85;
            c.Legends[0].Position.Height = 7;
            c.Legends[0].Position.X = 7;
            c.Legends[0].Position.Y = 84;
        }

        public static void AddCostAnnotation(Chart chart, string costText, string cost)
        {
            RectangleAnnotation ann = new RectangleAnnotation();
            ann.Text = costText + ": " + cost;
            ann.LineColor = ann.ForeColor = Color.DimGray;
            ann.Font = new Font(new FontFamily("Arial"), (float)7.5, FontStyle.Bold);
            ann.AnchorAlignment = ContentAlignment.TopLeft;
            ann.AnchorY = 14;
            ann.AnchorX = 5;
            chart.Annotations.Add(ann);
        }

        public static void SwitchChartImageStorage(Chart chart, string name)
        {
            chart.RenderType = RenderType.ImageTag;
            chart.ImageStorageMode = ImageStorageMode.UseImageLocation;
            chart.ImageLocation = "~/tempCustomReports/" + name + "_#SEQ(300,3)";
        }

        public static SmartLabelStyle CreateSmartLabel()
        {
            SmartLabelStyle sl = new SmartLabelStyle();

            sl.AllowOutsidePlotArea = LabelOutsidePlotAreaStyle.Yes;
            sl.IsMarkerOverlappingAllowed = false;
            sl.MovingDirection = LabelAlignmentStyles.Top;
            sl.CalloutLineColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed);

            return sl;
        }
    }
}