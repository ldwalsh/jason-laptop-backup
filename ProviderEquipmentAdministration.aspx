﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="ProviderEquipmentAdministration.aspx.cs" Inherits="CW.Website.ProviderEquipmentAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/Equipment/ViewEquipment.ascx" tagname="ViewEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/AddEquipment.ascx" tagname="AddEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentVariables.ascx" tagname="EquipmentVariables" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentVariablesToMultipleEquipment.ascx" tagname="EquipmentVariablesToMultipleEquipment" tagprefix="CW" %>
<%@ Register src="~/_administration/equipment/nonkgs/EquipmentAnalyses.ascx" tagPrefix="CW" tagName="EquipmentAnalyses" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentToEquipment.ascx" tagname="EquipmentToEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentStats.ascx" tagname="EquipmentStats" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <h1>Provider Equipment Administration</h1>

      <div class="richText">The provider equipment administration area is used to view, add, and edit equipment.</div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>
      </div>

      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" CausesValidation="false" runat="server" SelectedIndex="0" MultiPageID="radMultiPage">
          <Tabs>
            <telerik:RadTab Text="View Equip." />
            <telerik:RadTab Text="Add Equip." />
            <telerik:RadTab Text="Equip. Vars To An Equip." />
            <telerik:RadTab Text="Equip. Var To Mult. Equip." />
            <telerik:RadTab Text="Equipment Analyses" />
            <telerik:RadTab Text="Equip. To Equip." />
            <telerik:RadTab Text="Stats" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:ViewEquipment ID="ViewEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:AddEquipment ID="AddEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView3" runat="server">
            <CW:EquipmentVariables ID="EquipmentVariables" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView4" runat="server">   
            <CW:EquipmentVariablesToMultipleEquipment ID="EquipmentVariablesToMultipleEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView5" runat="server">
            <CW:EquipmentAnalyses ID="equipmentAnalyses" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView7" runat="server">
            <CW:EquipmentToEquipment ID="EquipmentToEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView8" runat="server">
            <CW:EquipmentStats ID="EquipmentStats" runat="server" />
          </telerik:RadPageView>

        </telerik:RadMultiPage>
      </div>

</asp:Content>