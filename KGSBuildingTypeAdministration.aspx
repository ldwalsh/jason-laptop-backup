﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSBuildingTypeAdministration.aspx.cs" Inherits="CW.Website.KGSBuildingTypeAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                  	                  	
            	<h1>KGS Building Type Administration</h1>                        
                <div class="richText">The kgs building type administration area is used to view, add, and edit building types.</div>                                 
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Building Types"></telerik:RadTab>
                            <telerik:RadTab Text="Add Building Type"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server"> 
                                    <h2>View Building Types</h2> 
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                                            <asp:ValidationSummary ID="valSummary" ShowSummary="true" ValidationGroup="EditBuildingTypes" runat="server" />                                            
                                    </p>  
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>      
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridBuildingTypes"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="BuildingTypeID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridBuildingTypes_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridBuildingTypes_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridBuildingTypes_Sorting"   
                                             OnSelectedIndexChanged="gridBuildingTypes_OnSelectedIndexChanged"                                                                                                            
                                             OnRowEditing="gridBuildingTypes_Editing"  
                                             OnRowDeleting="gridBuildingTypes_Deleting"
                                             OnDataBound="gridBuildingTypes_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingTypeName" HeaderText="Building Type">  
                                                    <ItemTemplate>
                                                        <%# StringHelper.TrimText(Eval("BuildingTypeName"),50) %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Building Class" SortExpression="BuildingClassName">  
                                                    <ItemTemplate>                                                        
                                                        <%# StringHelper.TrimText(Eval("BuildingClassName"),50) %>
                                                        <asp:HiddenField ID="hdnBuildingClassID" runat="server" Visible="true" Value='<%# Eval("BuildingClassID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Building Type Description">                                                      
                                                        <ItemTemplate><%# Eval("BuildingTypeDescription") %></ItemTemplate>                                                    
                                                </asp:TemplateField>                                                  
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this building type permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                                  
                                    <!--SELECT BUILDING TYPE DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvBuildingType" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Building Type Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Building Type Name: </strong>" + Eval("BuildingTypeName") + "</li>"%> 
                                                            <%# "<li><strong>Building Class Name: </strong>" + Eval("BuildingClassName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("BuildingTypeDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("BuildingTypeDescription") + "</li>"%>                                                                                                                                                                                                             	                                                                	                                                                                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT BUILDING TYPE PANEL -->                              
                                    <asp:Panel ID="pnlEditBuildingType" runat="server" Visible="false" DefaultButton="btnUpdateBuildingType">                                                                                             
                                        <div>
                                            <h2>Edit Building Type</h2>
                                        </div>  
                                        <div>
                                            <a id="lnkSetFocusEdit" runat="server"></a>                                                       
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Building Type Name:</label>
                                                <asp:TextBox ID="txtEditBuildingTypeName" MaxLength="50" runat="server"></asp:TextBox>                                  
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">*Building Class:</label>    
                                                <asp:DropDownList ID="ddlEditBuildingClass" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                     <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBuildingType" runat="server" Text="Edit Type"  OnClick="updateBuildingTypeButton_Click" ValidationGroup="EditBuildingType"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editBuildingTypeNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Type Name is a required field."
                                        ControlToValidate="txtEditBuildingTypeName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditBuildingType">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingTypeNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editBuildingTypeNameRequiredValidatorExtender"
                                        TargetControlID="editBuildingTypeNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editBuildingClassRequiredValidator" runat="server"
                                        ErrorMessage="Building Class is a required field." 
                                        ControlToValidate="ddlEditBuildingClass"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditBuildingType">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingClassRequiredValidatorExtender" runat="server"
                                        BehaviorID="editBuildingClassRequiredValidatorExtender" 
                                        TargetControlID="editBuildingClassRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                    </asp:Panel>
                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">    
                                <asp:Panel ID="pnlAddBuildingType" runat="server" DefaultButton="btnAddBuildingType">
                                    <h2>Add Building Type</h2> 
                                    <div>              
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                      
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Building Type Name:</label>
                                            <asp:TextBox ID="txtAddBuildingTypeName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                                        </div>      
                                        <div class="divForm">   
                                            <label class="label">*Building Class:</label>    
                                            <asp:DropDownList ID="ddlAddBuildingClass" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">Description:</label>
                                            <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                            <div id="divAddDescriptionCharInfo"></div>
                                        </div>                                                                                        
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddBuildingType" runat="server" Text="Add Type"  OnClick="addBuildingTypeButton_Click" ValidationGroup="AddBuildingType"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="buildingTypeNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Type Name is a required field."
                                        ControlToValidate="txtAddBuildingTypeName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddBuildingType">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="buildingTypeNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="buildingTypeNameRequiredValidatorExtender"
                                        TargetControlID="buildingTypeNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addBuildingClassRequiredValidator" runat="server"
                                        ErrorMessage="Building Class is a required field." 
                                        ControlToValidate="ddlAddBuildingClass"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddBuildingType">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addBuildingClassRequiredValidatorExtender" runat="server"
                                        BehaviorID="addBuildingClassRequiredValidatorExtender" 
                                        TargetControlID="addBuildingClassRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>                                                                                                                                                                  
                           </telerik:RadPageView>                          
                        </telerik:RadMultiPage>
                   </div>                                                                                
</asp:Content>


                    
                  
