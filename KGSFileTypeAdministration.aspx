﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSFileTypeAdministration.aspx.cs" Inherits="CW.Website.KGSFileTypeAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                      	                  	
            	<h1>KGS File Type Administration</h1>                        
                <div class="richText">The kgs file type administration area is used to view, add, and edit file types.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View File Types"></telerik:RadTab>
                            <telerik:RadTab Text="Add File Type"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View File Types</h2>
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                                            <asp:ValidationSummary ID="valSummary" ShowSummary="true" ValidationGroup="EditFileTypes" runat="server" />                                            
                                    </p>    
                                    
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>        
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridFileTypes"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="FileTypeID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridFileTypes_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridFileTypes_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridFileTypes_Sorting"   
                                             OnSelectedIndexChanged="gridFileTypes_OnSelectedIndexChanged"                                                                                                            
                                             OnRowEditing="gridFileTypes_Editing"       
                                             OnRowDeleting="gridFileTypes_Deleting"
                                             OnDataBound="gridFileTypes_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FileTypeName" HeaderText="File Type">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("FileTypeName"),50) %></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FileClassName" HeaderText="File Class">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("FileClassName"),50) %>
                                                        <asp:HiddenField ID="hdnFileClassID" runat="server" Visible="true" Value='<%# Eval("FileClassID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                   
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this file type permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                                  
                                    <!--SELECT FILE TYPE DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvFileType" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>File Type Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>File Type Name: </strong>" + Eval("FileTypeName") + "</li>"%> 
                                                            <%# "<li><strong>File Class Name: </strong>" + Eval("FileClassName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("FileTypeDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("FileTypeDescription") + "</li>"%>                                                                                                                                                                                                             	                                                                	                                                                                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT FILE PANEL -->                              
                                    <asp:Panel ID="pnlEditFileType" runat="server" Visible="false" DefaultButton="btnUpdateFileType">                                                                                             
                                        <div>
                                            <h2>Edit File Type</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*File Type Name:</label>
                                                <asp:TextBox ID="txtEditFileTypeName" MaxLength="50" runat="server"></asp:TextBox>                                  
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">*File Class:</label>    
                                                <asp:DropDownList ID="ddlEditFileClass" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                     <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateFileType" runat="server" Text="Edit Type"  OnClick="updateFileTypeButton_Click" ValidationGroup="EditFileType"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editFileTypeNameRequiredValidator" runat="server"
                                        ErrorMessage="File Type Name is a required field."
                                        ControlToValidate="txtEditFileTypeName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditFileType">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editFileTypeNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editFileTypeNameRequiredValidatorExtender"
                                        TargetControlID="editFileTypeNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editFileClassRequiredValidator" runat="server"
                                        ErrorMessage="File Class is a required field." 
                                        ControlToValidate="ddlEditFileClass"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditFileType">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editFileClassRequiredValidatorExtender" runat="server"
                                        BehaviorID="editFileClassRequiredValidatorExtender" 
                                        TargetControlID="editFileClassRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                    </asp:Panel>
                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddFileType" runat="server" DefaultButton="btnAddFileType">  
                                    <h2>Add File Type</h2>
                                    <div>           
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                         
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*File Type Name:</label>
                                            <asp:TextBox ID="txtAddFileTypeName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                                        </div>      
                                        <div class="divForm">   
                                            <label class="label">*File Class:</label>    
                                            <asp:DropDownList ID="ddlAddFileClass" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">Description:</label>
                                            <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                            <div id="divAddDescriptionCharInfo"></div>
                                        </div>                                                                                      
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddFileType" runat="server" Text="Add Type"  OnClick="addFileTypeButton_Click" ValidationGroup="AddFileType"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="fileTypeNameRequiredValidator" runat="server"
                                        ErrorMessage="File Type Name is a required field."
                                        ControlToValidate="txtAddFileTypeName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddFileType">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="fileTypeNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="fileTypeNameRequiredValidatorExtender"
                                        TargetControlID="fileTypeNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addFileClassRequiredValidator" runat="server"
                                        ErrorMessage="File Class is a required field." 
                                        ControlToValidate="ddlAddFileClass"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddFileType">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addFileClassRequiredValidatorExtender" runat="server"
                                        BehaviorID="addFileClassRequiredValidatorExtender" 
                                        TargetControlID="addFileClassRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>      
                                </asp:Panel>                                                                                                                                                            
                             </telerik:RadPageView>                         
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>


                    
                  
