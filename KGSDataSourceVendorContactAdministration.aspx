﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSDataSourceVendorContactAdministration.aspx.cs" Inherits="CW.Website.KGSDataSourceVendorContactAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                      	                  	
            	<h1>KGS Data Source Vendor Contact Administration</h1>                        
                <div class="richText">The kgs data source vendor contact administration area is used to view, add, and edit data source vendor contacts.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
                </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0"  OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Vendor Contacts"></telerik:RadTab>
                            <telerik:RadTab Text="Add Vendor Contact"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                   <h2>View Data Source Vendor Contacts</h2> 
                                    <p>
                                        <a id="lnkSetFocusView" href="#" runat="server"></a>
                                        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>         
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>  
                                                                                                                
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridDataSourceVendorContacts"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="ContactID"  
                                             GridLines="None"                                      
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridDataSourceVendorContacts_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridDataSourceVendorContacts_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridDataSourceVendorContacts_Sorting"   
                                             OnSelectedIndexChanged="gridDataSourceVendorContacts_OnSelectedIndexChanged"                                                                                                             
                                             OnRowDeleting="gridDataSourceVendorContacts_Deleting"
                                             OnRowEditing="gridDataSourceVendorContacts_Editing"
                                             OnDataBound="gridDataSourceVendorContacts_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Name" HeaderText="Name">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("Name"),30) %></ItemTemplate>       
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Phone">  
                                                    <ItemTemplate><%# String.IsNullOrEmpty(Convert.ToString(Eval("Phone"))) ? "" : Eval("Phone")%></ItemTemplate>
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Email">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("Email"),30) %></ItemTemplate>       
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="VendorName" HeaderText="Vendor">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("VendorName"),30) %></ItemTemplate>       
                                                </asp:TemplateField> 
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this vendor contact permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                                                          
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT DATA SOURCE VENDOR CONTACT DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvDataSourceVendorContact" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>                                                    
                                                    <div>
                                                        <h2>Data Source Vendor Contact Details</h2>                                                                                                                       
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Name: </strong>" + Eval("Name") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Phone"))) ? "" : "<li><strong>Phone: </strong>" + Eval("Phone") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Email"))) ? "" : "<li><strong>Email: </strong>" + Eval("Email") + "</li>"%>                                                             
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Address"))) ? "" : "<li><strong>Address: </strong>" + Eval("Address") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("City"))) ? "" : "<li><strong>City: </strong>" + Eval("City") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("StateName"))) ? "" : "<li><strong>State: </strong>" + Eval("StateName") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Zip"))) ? "" : "<li><strong>Zip: </strong>" + Eval("Zip") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("CountryName"))) ? "" : "<li><strong>Country: </strong>" + Eval("CountryName") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Notes"))) ? "" : "<li><strong>Notes: </strong>" + Eval("Notes") + "</li>"%>
                                                            <%# "<li><strong>Vendor: </strong>" + Eval("VendorName") + "</li>"%> 
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>                                     
                                                                     
                                    <!--EDIT DATASOURCE VENDOR CONTACT PANEL -->                              
                                    <asp:Panel ID="pnlEditDataSourceVendorContact" runat="server" Visible="false" DefaultButton="btnUpdateDataSourceVendorContact">                                                                                             
                                              <div>
                                                    <h2>Edit Vendor Contact</h2>
                                              </div>  
                                              <div>                                                    
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*Name:</label>
                                                        <asp:TextBox ID="txtEditName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Phone:</label>
                                                        <telerik:RadMaskedTextBox ID="txtEditPhone" CssClass="textbox" runat="server"
                                                            Mask="(###)###-####" ValidationGroup="EditDataSourceVendorContact"></telerik:RadMaskedTextBox>      
                                                    </div>                                                                                                                                                                                           
                                                    <div class="divForm">
                                                         <label class="label">Email:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditEmail" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>                                                                                                                                                                                               
                                                    <div class="divForm">
                                                        <label class="label">Address:</label>
                                                        <asp:TextBox ID="txtEditAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                                                                              
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">City:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditCity" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">Country:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlEditCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                         <asp:ListItem Value="-1">Select one...</asp:ListItem>
                                                         </asp:DropDownList>
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">State:</label>
                                                        <asp:DropDownList ID="ddlEditState" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                        <asp:ListItem Value="-1">Select country first...</asp:ListItem>
                                                        </asp:DropDownList>                                                                                                                                                              
                                                    </div>     
                                                    <div class="divForm">
                                                        <label class="label">Zip:</label>
                                                        <telerik:RadMaskedTextBox ID="txtEditZip" CssClass="textbox" runat="server" 
                                                                Mask="aaaaaaaaaa" PromptChar="" ValidationGroup="EditDataSourceVendor"></telerik:RadMaskedTextBox>    
                                                    </div> 
                                                    <div class="divForm">
                                                         <label class="label">Notes:</label>
                                                         <textarea name="txtEditNotes" id="txtEditNotes" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditNotesCharInfo')" runat="server"></textarea>
                                                         <div id="divEditNotesCharInfo"></div>                                                           
                                                    </div>  
                                                    <div class="divForm">   
                                                        <label class="label">*Vendor:</label>    
                                                        <asp:DropDownList ID="ddlEditVendor" CssClass="dropdown" runat="server">
                                                        </asp:DropDownList> 
                                                    </div> 
                                                                                                                                                                                                                                                                                        
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateDataSourceVendorContact" runat="server" Text="Update Contact"  OnClick="updateDataSourceVendorContactButton_Click" ValidationGroup="EditDataSourceVendorContact"></asp:LinkButton>    
                                                </div>
                                                
                                                <!--Ajax Validators-->      
                                                <asp:RequiredFieldValidator ID="editDataSourceVendorContactNameRequiredValidator" runat="server"
                                                    ErrorMessage="Contact name is a required field."
                                                    ControlToValidate="txtEditName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditDataSourceVendorContact">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editDataSourceVendorContactNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editDataSourceVendorContactNameRequiredValidatorExtender"
                                                    TargetControlID="editDataSourceVendorContactNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                                <asp:RegularExpressionValidator ID="editDataSourceVendorContactPhoneRegExValidator" runat="server"
                                                        ErrorMessage="Invalid Phone Format."
                                                        ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                                        ControlToValidate="txtEditPhone"
                                                        SetFocusOnError="true" 
                                                        Display="None" 
                                                        ValidationGroup="EditDataSourceVendorContact">
                                                        </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editDataSourceVendorContactPhoneRegExValidatorExtender" runat="server"
                                                    BehaviorID="editDataSourceVendorContactPhoneRegExValidatorExtender" 
                                                    TargetControlID="editDataSourceVendorContactPhoneRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RegularExpressionValidator ID="editDataSourceVendorContactEmailRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Email Format."
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ControlToValidate="txtEditEmail"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="EditDataSourceVendorContact">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editDataSourceVendorContactEmailRegExValidatorExtender" runat="server"
                                                    BehaviorID="editDataSourceVendorContactEmailRegExValidatorExtender" 
                                                    TargetControlID="editDataSourceVendorContactEmailRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RegularExpressionValidator ID="editDataSourceVendorContactZipRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Zip Format."
                                                    ValidationExpression="\d{5}"
                                                    ControlToValidate="txtEditZip"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="EditDataSourceVendorContact"
                                                    Enabled="false">
                                                    </asp:RegularExpressionValidator>  
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editDataSourceVendorContactZipRegExValidatorExtender" runat="server"
                                                    BehaviorID="editDataSourceVendorContactZipRegExValidatorExtender"
                                                    TargetControlID="editDataSourceVendorContactZipRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                    </asp:Panel>                                                                                                         
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server"> 
                                    <asp:Panel ID="pnlAddDataSourceVendorContact" runat="server" DefaultButton="btnAddDataSourceVendorContact"> 
                                              <h2>Add Data Source Vendor Contact</h2> 
                                              <div>     
                                                    <a id="lnkSetFocusAdd" runat="server"></a>                                               
                                                    <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <div class="divForm">
                                                        <label class="label">*Name:</label>
                                                        <asp:TextBox ID="txtAddName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Phone:</label>
                                                        <telerik:RadMaskedTextBox ID="txtAddPhone" CssClass="textbox" runat="server"
                                                            Mask="(###)###-####" ValidationGroup="AddDataSourceVendorContact"></telerik:RadMaskedTextBox>    
                                                    </div>                                                                                                                                                                                           
                                                    <div class="divForm">
                                                         <label class="label">Email:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtAddEmail" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>                                                    
                                                    <div class="divForm">
                                                        <label class="label">Address:</label>
                                                        <asp:TextBox ID="txtAddAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">City:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtAddCity" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>                                                                                                                                                                                           
                                                    <div class="divForm">
                                                         <label class="label">Country:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlAddCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                         <asp:ListItem Value="-1">Select one...</asp:ListItem>
                                                         </asp:DropDownList>
                                                    </div>                                                                                                                                                                                         
                                                    <div class="divForm">
                                                         <label class="label">State:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlAddState" AppendDataBoundItems="true" runat="server">
                                                         <asp:ListItem Value="-1">Select country first...</asp:ListItem>
                                                         </asp:DropDownList>
                                                    </div>       
                                                    <div class="divForm">
                                                        <label class="label">Zip:</label>
                                                        <telerik:RadMaskedTextBox ID="txtAddZip" CssClass="textbox" runat="server" 
                                                            Mask="aaaaaaaaaa" PromptChar="" ValidationGroup="AddDataSourceVendorContact"></telerik:RadMaskedTextBox>                                                                                                                                                           
                                                    </div>                                                            
                                                    <div class="divForm">
                                                         <label class="label">Notes:</label>
                                                         <textarea name="txtAddNotes" id="txtAddNotes" cols="40" onkeyup="limitChars(this, 1000, 'divAddNotesCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                         <div id="divAddNotesCharInfo"></div>
                                                    </div>   
                                                    <div class="divForm">   
                                                        <label class="label">*Vendor:</label>    
                                                        <asp:DropDownList ID="ddlAddVendor" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                             <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                                        </asp:DropDownList> 
                                                    </div>                                                                                                                                                                                                                                                            
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnAddDataSourceVendorContact" runat="server" Text="Add Contact"  OnClick="addDataSourceVendorContactButton_Click" ValidationGroup="AddDataSourceVendorContact"></asp:LinkButton>    
                                                </div>
                                                
                                                <!--Ajax Validators-->   
                                                <asp:RequiredFieldValidator ID="addDataSourceVendorContactNameRequiredValidator" runat="server"
                                                    ErrorMessage="Contact Name is a required field."
                                                    ControlToValidate="txtAddName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddDataSourceVendorContact">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addDataSourceVendorContactNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addDataSourceVendorContactNameRequiredValidatorExtender"
                                                    TargetControlID="addDataSourceVendorContactNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                                <asp:RegularExpressionValidator ID="addDataSourceVendorContactPhoneRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Phone Format."
                                                    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                                    ControlToValidate="txtAddPhone"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="AddDataSourceVendorContact">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="AddDataSourceVendorContactPhoneRegExValidatorExtender" runat="server"
                                                    BehaviorID="addDataSourceVendorContactPhoneRegExValidatorExtender" 
                                                    TargetControlID="addDataSourceVendorContactPhoneRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RegularExpressionValidator ID="addDataSourceVendorContactEmailRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Email Format."
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ControlToValidate="txtAddEmail"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="AddDataSourceVendorContact">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addDataSourceVendorContactEmailRegExValidatorExtender" runat="server"
                                                    BehaviorID="addDataSourceVendorContactEmailRegExValidatorExtender" 
                                                    TargetControlID="addDataSourceVendorContactEmailRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RegularExpressionValidator ID="addDataSourceVendorContactZipRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Zip Format."
                                                    ValidationExpression="\d{5}"
                                                    ControlToValidate="txtAddZip"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="AddDataSourceVendorContact"
                                                    Enabled="false">
                                                    </asp:RegularExpressionValidator> 
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addDataSourceVendorContactZipRegExValidatorExtender" runat="server"
                                                    BehaviorID="addDataSourceVendorContactZipRegExValidatorExtender"
                                                    TargetControlID="addDataSourceVendorContactZipRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                                <asp:RequiredFieldValidator ID="addDataSourceVendorContactVendorRequiredValidator" runat="server"
                                                    ErrorMessage="Vendor is a required field."
                                                    ControlToValidate="ddlAddVendor"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    InitialValue="-1"
                                                    ValidationGroup="AddDataSourceVendorContact">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addDataSourceVendorContactVendorRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addDataSourceVendorContactVendorRequiredValidatorExtender"
                                                    TargetControlID="addDataSourceVendorContactVendorRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                    </asp:Panel>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                            </telerik:RadPageView>                      
                        </telerik:RadMultiPage>
                   </div>                                                                 
               
</asp:Content>


                    
                  
