﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.Analyses.nonkgs;
using System;

namespace CW.Website
{
    public partial class ProviderAnalysisAdministration : AdminSitePageTemp
    {
        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewAnalyses).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.PROVIDER; } }

            #endregion

        #endregion
    }
}
