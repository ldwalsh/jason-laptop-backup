﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Linq;

using CW.Website._framework;
using CW.Business;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using CW.Data.Models.Client;
using CW.Common.Helpers;
using System.Configuration;
using CW.Common.Constants;
using CW.Logging;
using System.Threading.Tasks;
using System.Collections;

namespace CW.Website
{
    /// <summary>
    /// Public access uses the referring domain to auto login a user with a public role. 
    /// Multiple referring domains can be allowed for the public user.
    /// Optional referrer redirect can be part of the link, to redirect the public user to a specific page first.
    /// Will work for both cw and ba.
    /// 
    /// Ex: https://clockworks.kgsbuildings.com/PublicAccess.aspx
    ///     optional  -  ?referrer=xxxxx.aspx
    /// 
    /// NOTE: Public Access does not, and should not, use an IdP.
    ///       Public Access does not, and should not, use the Organizations session timeout override (it will use our default session timeout) because the data exposed is public and does not require this measure of addtional security.
    /// </summary>    
    public partial class PublicAccess : SitePage
    {
        #region Properties

        protected User mUser;
        protected IEnumerable<GetClientsWithRoleFormat> mClients;

        #endregion

        protected void Page_Init(object sender, EventArgs e)
        {
            // if has not posted the localTime
            if ((Request.UrlReferrer != null || !String.IsNullOrEmpty(Request.ServerVariables["HTTP_REFERER"])) && String.IsNullOrEmpty(Request.Params["ReferringDomain"]))
            {
                //LogMgr.Log(DataConstants.LogLevel.INFO, "Public Access. TestingA: " + Request.ServerVariables["HTTP_REFERER"] + ", " + Request.UserHostName + ", " + Request.UrlReferrer.Host);
                
                // then clear the content and write some html, a javascript code which submits the users timezoneoffset
                Response.ClearContent();
                Response.Write(@"<form id='localform' method='post' name='localform'>" +
                                    "<input type='hidden' id='TimeZoneOffset' name='TimeZoneOffset' />" +
                                    "<input type='hidden' id='ReferringDomain' name='ReferringDomain' />" +
                                    "<script type='text/javascript'>" +
                                        "document.getElementById('ReferringDomain').value = '" + UrlHelper.TrimWWW(Request.UrlReferrer.Host) + "';" +
                                        "document.getElementById('TimeZoneOffset').value = (new Date().getTimezoneOffset() / -60);" +
                                        "document.getElementById('localform').submit();" +
                                    "</script>" +
                                "</form>");

                Response.Flush();

                // end the response so PageLoad won't be executed
                Response.End();
            }
            else if (String.IsNullOrEmpty(Request.Params["ReferringDomain"]))
            {
                //LogMgr.Log(DataConstants.LogLevel.INFO, "Public Access. TestingB: " + Request.ServerVariables["HTTP_REFERER"] + ", " + Request.UserHostName + ", " + Request.UrlReferrer);
                
                //log no url referrer. input ip from potential hacker in log.
                LogMgr.Log(DataConstants.LogLevel.INFO, "Public Access. No url referrer. Possible http and https mis-match. Invalid attempt from: " + ServerVariableHelper.DetermineHostIP(Request.ServerVariables["HTTP_X_FORWARDED_FOR"], Request.ServerVariables["HTTP_HOST"]));
                
                Redirect();
            }            
        }

        private bool mIsSSO = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            //if postback and not logged in
            if (!Page.IsPostBack && siteUser.IsAnonymous)
            {
                //get referring domain
                ClientSetting cs = DataMgr.ClientDataMapper.GetClientSettingsByReferringDomain(Request.Form["ReferringDomain"]);

                if (cs != null)
                {
                    if (cs.ReferringDomainAutoLoginUserID != null)
                    {
                        //get user
                        mUser = DataMgr.UserDataMapper.GetUser((int)cs.ReferringDomainAutoLoginUserID);


                        // try to clear failed login attempts after success, and increment login success count
                        try
                        {
                            UsersAudit mUserAudit = DataMgr.UserAuditDataMapper.GetUserAuditByUID(mUser.UID);
                            mUserAudit.LoginFailedAttemptCount = 0;
                            mUserAudit.LoginSuccessTotalCount = mUserAudit.LoginSuccessTotalCount + 1;
                            DataMgr.UserAuditDataMapper.UpdateUserAudit(mUserAudit);
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("User audit update failed for user: UserEmail={0}.", mUser.Email), ex);
                        }

                        Session.Clear();


                        //Get Clients------

                        //get distinct active client associated to the public restricted user. should just be one. Distinct because restricted by building will present more than one of the same client.                       
                        Client mClient = DataMgr.ClientDataMapper.GetAllActiveClientsAssociatedToUID(mUser.UID, true).FirstOrDefault();

                        //if one client and primary roleid is not restricted then get all clients , else populate with restricted clients
                        //active clients only.
                        //always public role and not kgs.
                        //mClients = mDataManager.ClientDataMapper.GetAllActiveClientsAssociatedToUIDWithRole(mUser.UID, Convert.ToInt32(BusinessConstants.UserRole.UserRoleEnum.PublicUser), false, true, usersClients);
                    

                        if (mClient != null)
                        {
                            AuthnHelper.GetSSOInfoAsync(mClient.CID).Wait();
                            List<GetClientsWithRoleFormat> clientsWithRoleFormat = new List<GetClientsWithRoleFormat>{ new GetClientsWithRoleFormat
                                {
                                    CID = mClient.CID,
                                    ClientName = mClient.ClientName,
                                    IsProxy = mClient.IsProxy,
                                    OOTID = mClient.OOTID,
                                    OID = DataMgr.OrganizationDataMapper.GetOIDByCID(mClient.CID),
                                    RoleID = Convert.ToInt32(BusinessConstants.UserRole.UserRoleEnum.PublicUser),
                                    RoleName = BusinessConstants.UserRole.UserRoleEnum.PublicUser.ToString(),
                                }
                            };

                            //role id is set to primary or secondary accordingly already
                            SiteUser.LogOn(mUser, mIsSSO, Convert.ToInt32(BusinessConstants.UserRole.UserRoleEnum.PublicUser), mClient.CID, null, mClient.ClientName, clientsWithRoleFormat, null, Convert.ToSingle(Request.Form["TimeZoneOffset"]), DataMgr); //the SiteUser object will handle all Session variables related to user account... above lines will be obsolete                       
                            string referrer = null;
                            if (!String.IsNullOrWhiteSpace(Request.QueryString[DataConstants.ReferrerQSKey]))
                            {
                                referrer = Request.QueryString[DataConstants.ReferrerQSKey];
                            }
							
							// Note [02/05/2016]: IdP enaabled orgs are not supported for PublicAccess flow (4th parameter is null).
							// If an IdP enabled is chosen for PublicAccess, subsequent authentication flow will NOT work.
							// We (John, Nate & Sanjeev) have agreed on this behavior.
                            CW.Website._controls.Login.CompleteLogin(mUser, siteUser, DataMgr, null, Context, referrer, false);
                            return; // Do not redirect
                        }
                    }
                    else
                    {
                        //log no client setting user assigned.
                        LogMgr.Log(DataConstants.LogLevel.INFO, "Public Access. Referring domain client setting no user is assigned for CID: " + cs.CID);                
                    }
                }
                else
                {
                    //log referring domain client setting not found.
                    LogMgr.Log(DataConstants.LogLevel.INFO, "Public Access. Referring domain client setting not found for: " + Request.Form["ReferringDomain"]);                
                }
            }

            Redirect();
        }

        private async Task GetSSOInfoAsync(int organizationid)
        {
            AuthNSSOInfo results = await AuthnHelper.GetSSOInfoAsync(organizationid).ConfigureAwait(false);
            mIsSSO = results.SSOEnabled;
        }
        private void Redirect()
        {
            //check if referrer querystring exists and redirect
            if (!String.IsNullOrWhiteSpace(Request.QueryString[DataConstants.ReferrerQSKey]))
                Response.Redirect("~/" + Request.QueryString[DataConstants.ReferrerQSKey]);

            Response.Redirect("/Home.aspx");
        }
    }
}