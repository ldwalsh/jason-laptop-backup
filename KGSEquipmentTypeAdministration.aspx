﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSEquipmentTypeAdministration.aspx.cs" Inherits="CW.Website.KGSEquipmentTypeAdministration" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="~/_controls/admin/EquipmentTypes/ViewEquipmentTypes.ascx" TagName="ViewEquipmentTypes" TagPrefix="CW" %>
<%@ Register Src="~/_controls/admin/EquipmentTypes/AddEquipmentType.ascx" TagName="AddEquipmentType" TagPrefix="CW" %>
<%@ Register Src="~/_controls/admin/EquipmentTypes/EquipmentTypesToEquipmentType.ascx" TagName="EquipmentTypesToEquipmentType" TagPrefix="CW" %>
<%@ Register Src="~/_controls/admin/EquipmentTypes/AnalysesToAnEquipmentType.ascx" TagName="AnalysesToAnEquipmentType" TagPrefix="CW" %>
<%@ Register Src="~/_controls/admin/EquipmentTypes/PointTypesToEquipmentType.ascx" TagName="PointTypesToEquipmentType" TagPrefix="CW" %>
<%@ Register Src="~/_controls/admin/EquipmentTypes/EquipmentTypesToPointType.ascx" TagName="EquipmentTypesToPointType" TagPrefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <h1>KGS Equipment Type Administration</h1>

    <div class="richText">The kgs equipment type administration area is used to view, add, and edit equipment types.</div>

    <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
            <ProgressTemplate>
                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>

    <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage">
            <Tabs>
                <telerik:RadTab Text="View Equipment Types" />
                <telerik:RadTab Text="Add Equipment Type" />
                <telerik:RadTab Text="Equipment Types To An Equipment Type" />
                <telerik:RadTab Text="Analyses To An Equipment Type" />
                <telerik:RadTab Text="Point Types To An Equipment Type" />
                <telerik:RadTab Text="Equipment Types To A Point Type " />
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

            <telerik:RadPageView ID="RadPageView1" runat="server">
                <CW:ViewEquipmentTypes ID="ViewEquipmentTypes" runat="server" />
            </telerik:RadPageView>

            <telerik:RadPageView ID="RadPageView2" runat="server">
                <CW:AddEquipmentType ID="AddEquipmentType" runat="server" />
            </telerik:RadPageView>

            <telerik:RadPageView ID="RadPageView3" runat="server">
                <CW:EquipmentTypesToEquipmentType ID="EquipmentTypesToEquipmentType" runat="server" />
            </telerik:RadPageView>

            <telerik:RadPageView ID="RadPageView4" runat="server">
                <CW:AnalysesToAnEquipmentType ID="AnalysesToAnEquipmentType" runat="server" />
            </telerik:RadPageView>

            <telerik:RadPageView ID="RadPageView5" runat="server">
                <CW:PointTypesToEquipmentType ID="PointTypesToEquipmentType" runat="server" />
            </telerik:RadPageView>

            <telerik:RadPageView ID="RadPageView6" runat="server">
                <CW:EquipmentTypesToPointType ID="EquipmentTypesToPointType" runat="server" />
            </telerik:RadPageView>

        </telerik:RadMultiPage>
    </div>

</asp:Content>
