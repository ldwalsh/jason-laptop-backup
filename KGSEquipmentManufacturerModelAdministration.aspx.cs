﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.EquipmentManufacturerModel;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSEquipmentManufacturerModelAdministration : SitePage
    {
        #region Properties

            private EquipmentManufacturerModel mManufacturerModel;

            const string addManufacturerModelSuccess = " manufacturer model addition was successful.";
            const string addManufacturerModelFailed = "Adding manufacturer model failed. Please contact an administrator.";
            const string addManufacturerModelNameExists = "Cannot add manufacturer model because the name already exists."; 
            const string updateSuccessful = "Manufacturer model update was successful.";
            const string updateFailed = "Manufacturer model update failed. Please contact an administrator.";
            const string updateManufacturerModelNameExists = "Cannot update manufacturer model name because the name already exists.";
            const string deleteSuccessful = "Manufacturer model deletion was successful.";
            const string deleteFailed = "Manufacturer model deletion failed. Please contact an administrator.";
            const string associatedToDataSource = "Cannot delete manufacturer model becuase an equipment requires it.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "ManufacturerModelName";
            const string manufacturerModelExists = "Manufacturer model with provided ManufacturerModelID already exists.";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindManufacturerModels();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindManufacturers(ddlAddManufacturer, ddlEditManufacturer, ddlManufacturerList);

                    sessionState["Search"] = String.Empty;

                    if (!queryString.IsEmpty)
                        ProcessRequestWithQueryString();
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetManufacturerModelIntoEditForm(CW.Data.EquipmentManufacturerModel manufacturerModel)
            {
                //ID
                hdnEditID.Value = Convert.ToString(manufacturerModel.ManufacturerModelID);

                //Equipment Manufacturer Model Name (set both the editable field and the hidden field in case the user changes this value)
                txtEditManufacturerModelName.Text = manufacturerModel.ManufacturerModelName;
                hdnEditManufacturerModelName.Value = manufacturerModel.ManufacturerModelName;

                //ManufacturerModel Description
                txtEditDescription.Value = String.IsNullOrEmpty(manufacturerModel.ManufacturerModelDescription) ? null : manufacturerModel.ManufacturerModelDescription;

                //Manufacturer (set both the editable field and the hidden field in case the user changes this value)
                ddlEditManufacturer.SelectedValue = Convert.ToString(manufacturerModel.ManufacturerID);
                hdnEditManufacturer.Value = Convert.ToString(manufacturerModel.ManufacturerID);
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Bind all manufacturers including unknown with id as 1
            /// </summary>
            /// <param name="ddl"></param>
            private void BindManufacturers(DropDownList ddl, DropDownList ddl2, DropDownList ddl3)
            {
                IEnumerable<EquipmentManufacturer> manufacturers = DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturers(true);

                ddl.DataTextField = "ManufacturerName";
                ddl.DataValueField = "ManufacturerID";
                ddl.DataSource = manufacturers;
                ddl.DataBind();

                ddl2.DataTextField = "ManufacturerName";
                ddl2.DataValueField = "ManufacturerID";
                ddl2.DataSource = manufacturers;
                ddl2.DataBind();

                ddl3.DataTextField = "ManufacturerName";
                ddl3.DataValueField = "ManufacturerID";
                ddl3.DataSource = manufacturers;
                ddl3.DataBind();
            }

            //method for intial bind/rebind of grid
            private void BindManufacturerModels()
            {
                IEnumerable<GetEquipmentManufacturerModelData> variables = null;

                if (ddlManufacturerList.SelectedValue == "-1")
                    variables = QueryManufacturerModels();
                else
                    variables = QuerySearchedManufacturerData(string.Empty, ddlManufacturerList.SelectedValue);
                
                BindGridEquipmentManufacturerModels(variables);
            }

            //method specifically for binding grid via search results
            private void BindManufacturerDataForSearch()
            {
                HideSelectEditPanels();

                IEnumerable<GetEquipmentManufacturerModelData> variables = QuerySearchedManufacturerData(txtSearch.Text, ddlManufacturerList.SelectedValue);

                BindGridEquipmentManufacturerModels(variables);
            }

        #endregion

        #region Load Manufacturer Model

            protected void LoadAddFormIntoManufacturerModel(EquipmentManufacturerModel manufacturerModel)
            {
                //Manufacturer Model Name
                manufacturerModel.ManufacturerModelName = txtAddManufacturerModelName.Text;
                //Manufacturer Model Description
                manufacturerModel.ManufacturerModelDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                //Manufacturer
                manufacturerModel.ManufacturerID = Convert.ToInt32(ddlAddManufacturer.SelectedValue);

                manufacturerModel.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoManufacturerModel(EquipmentManufacturerModel manufacturerModel)
            {
                //ID
                manufacturerModel.ManufacturerModelID = Convert.ToInt32(hdnEditID.Value);
                //Manufacturer Model Name
                manufacturerModel.ManufacturerModelName = txtEditManufacturerModelName.Text;
                //Manufacturer Model Description
                manufacturerModel.ManufacturerModelDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //Manufacturer
                manufacturerModel.ManufacturerID = Convert.ToInt32(ddlEditManufacturer.SelectedValue);                
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add Manufacturer model Button on click.
            /// </summary>
            protected void addManufacturerModelButton_Click(object sender, EventArgs e)
            {
                mManufacturerModel = new EquipmentManufacturerModel();

                //load the form into the Manufacturer model
                LoadAddFormIntoManufacturerModel(mManufacturerModel);

                try
                {
                    //insert new equipment manufacturer model, check if the manufacturer model has been entered already. If it has display the error and disallow insertion of the record
                    if (DataMgr.EquipmentManufacturerModelDataMapper.DoesEquipmentManufacturerModelExist(mManufacturerModel.ManufacturerID, mManufacturerModel.ManufacturerModelName))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, addManufacturerModelNameExists, lnkSetFocusAdd);
                    }
                    else
                    {
                        DataMgr.EquipmentManufacturerModelDataMapper.InsertManufacturerModel(mManufacturerModel);

                        LabelHelper.SetLabelMessage(lblAddError, mManufacturerModel.ManufacturerModelName + addManufacturerModelSuccess, lnkSetFocusAdd);
                    }
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment manufacturer model.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addManufacturerModelFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment manufacturer model.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addManufacturerModelFailed, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update manufacturer model Button on click. Updates manufacturer model data.
            /// </summary>
            protected void updateManufacturerModelButton_Click(object sender, EventArgs e)
            {
                EquipmentManufacturerModel mManufacturerModel = new EquipmentManufacturerModel();

                //load the form into the manufacturer model
                LoadEditFormIntoManufacturerModel(mManufacturerModel);

                //try to update the manufacturer model           
                try
                {
                    //Only call the method check for existing manufacturer model if the model or manufacturer value has changed (via the hidden values)
                    if (hdnEditManufacturerModelName.Value != mManufacturerModel.ManufacturerModelName || hdnEditManufacturer.Value != Convert.ToString(mManufacturerModel.ManufacturerID))
                    {
                        //update existing equipment manufacturer model, check if the model already exists. If it has display the error and disallow update of the record
                        if (DataMgr.EquipmentManufacturerModelDataMapper.DoesEquipmentManufacturerModelExist(mManufacturerModel.ManufacturerID, mManufacturerModel.ManufacturerModelName))
                        {
                            LabelHelper.SetLabelMessage(lblEditError, updateManufacturerModelNameExists, lnkSetFocusView);
                        }
                        else
                            UpdateEquipmentManufacturerModel(mManufacturerModel);
                    }
                    else
                        UpdateEquipmentManufacturerModel(mManufacturerModel);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment manufacturer model.", ex);
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                if (!String.IsNullOrWhiteSpace(txtSearch.Text))
                {
                    sessionState["Search"] = txtSearch.Text;
                    BindManufacturerDataForSearch();
                }
                else
                    BindManufacturerModels();
            }

            ///// <summary>
            ///// Rebinds grid to view all.
            ///// </summary>
            ///// <param name="sender"></param>
            ///// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                ResetSearch();
                BindManufacturerModels();
            }

        #endregion

        #region Dropdown events

            //method specifically for binding grid via selecting ddl for specific manufacturer
            protected void BindManufacturerDataForDDL_SelectedIndexChanged(object sender, EventArgs e)
            {
                HideSelectEditPanels();
                ResetSearch();

                IEnumerable<GetEquipmentManufacturerModelData> variables = (ddlManufacturerList.SelectedValue != "-1") ? QuerySearchedManufacturerData(txtSearch.Text, ddlManufacturerList.SelectedValue) : QueryManufacturerModels();

                BindGridEquipmentManufacturerModels(variables);
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind manufacturer model grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind manufacturer models to grid and hide an edit/selection panels and reset all searches
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    ResetSearch();
                    HideSelectEditPanels();
                    BindManufacturerModels();
                }
            }

        #endregion

        #region Grid Events

            protected void gridEquipmentManufacturerModels_DataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridEquipmentManufacturerModels_SelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditManufacturerModel.Visible = false;
                dtvManufacturerModel.Visible = true;
                
                int manufacturerModelID = Convert.ToInt32(gridEquipmentManufacturerModels.DataKeys[gridEquipmentManufacturerModels.SelectedIndex].Values["ManufacturerModelID"]);

                //set data source and bind manufacturer model to details view
                dtvManufacturerModel.DataSource = DataMgr.EquipmentManufacturerModelDataMapper.GetFullEquipmentManufacturerModelByID(manufacturerModelID);
                dtvManufacturerModel.DataBind();
            }

            protected void gridEquipmentManufacturerModels_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.
                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);

                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";

                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridEquipmentManufacturerModels_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvManufacturerModel.Visible = false;
                pnlEditManufacturerModel.Visible = true;

                int manufacturerModelID = Convert.ToInt32(gridEquipmentManufacturerModels.DataKeys[e.NewEditIndex].Values["ManufacturerModelID"]);

                EquipmentManufacturerModel mManufacturerModel = DataMgr.EquipmentManufacturerModelDataMapper.GetManufacturerModel(manufacturerModelID);

                //Set manufacturer model data
                SetManufacturerModelIntoEditForm(mManufacturerModel);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridEquipmentManufacturerModels_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows manufacturermodelid
                int manufacturerModelID = Convert.ToInt32(gridEquipmentManufacturerModels.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a manufacturer model if it as not been assocaited with
                    //an equipment

                    //check if manufacturer models associated to any equipment
                    if (DataMgr.EquipmentManufacturerModelDataMapper.AreEquipmentAssignedToEquipmentManufacturerModel(manufacturerModelID))
                    {
                        lblErrors.Text = associatedToDataSource;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //delete manufacturer model
                        DataMgr.EquipmentManufacturerModelDataMapper.DeleteEquipmentManufacturerModel(manufacturerModelID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting equipment manufacturer model.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryManufacturerModels());
                gridEquipmentManufacturerModels.PageIndex = gridEquipmentManufacturerModels.PageIndex;
                gridEquipmentManufacturerModels.DataSource = SortDataTable(dataTable as DataTable, true);
                gridEquipmentManufacturerModels.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditManufacturerModel.Visible = false;     
            }

            protected void gridEquipmentManufacturerModels_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedManufacturerData(sessionState["Search"].ToString(), ddlManufacturerList.SelectedValue));

                //maintain current sort direction and expresstion on paging
                gridEquipmentManufacturerModels.DataSource = SortDataTable(dataTable, true);
                gridEquipmentManufacturerModels.PageIndex = e.NewPageIndex;
                gridEquipmentManufacturerModels.DataBind();
            }

            protected void gridEquipmentManufacturerModels_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridEquipmentManufacturerModels.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedManufacturerData(sessionState["Search"].ToString(), ddlManufacturerList.SelectedValue));

                GridViewSortExpression = e.SortExpression;

                gridEquipmentManufacturerModels.DataSource = SortDataTable(dataTable, false);
                gridEquipmentManufacturerModels.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetEquipmentManufacturerModelData> QueryManufacturerModels()
            {
                try
                {
                    return DataMgr.EquipmentManufacturerModelDataMapper.GetAllEquipmentManufacturerModelsWithPartialData(); //get all manufacturer models 
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturer models.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturer models.", ex);
                    return null;
                }
            }

            private IEnumerable<GetEquipmentManufacturerModelData> QuerySearchedManufacturerData(string searchText, string manufacturerName)
            {
                try
                {
                    return DataMgr.EquipmentManufacturerModelDataMapper.GetAllSearchedEquipmentManufacturerData(searchText, Convert.ToInt32(manufacturerName)); //get searched manufacturer data
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturer models or manufacturers.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturer models or manufacturers.", ex);
                    return null;
                }
            }

            //helper method that binds the grid for method results above (BindManufacturerModels, BindManufacturerDataForSearch and BindManufacturerDataForDDL_SelectedIndexChanged)
            private void BindGridEquipmentManufacturerModels(IEnumerable<GetEquipmentManufacturerModelData> variables)
            {
                if (variables != null)
                {
                    int count = variables.Count();

                    // bind grid
                    gridEquipmentManufacturerModels.DataSource = variables;
                    gridEquipmentManufacturerModels.DataBind();

                    SetGridCountLabel(count);

                    //set search visibility. dont remove when last result from searched results in the grid is deleted.
                    pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && count == 0) ? false : true;
                }
            }

            //helper method to hide panels of selected items or items to be edited.  For UI.
            private void HideSelectEditPanels()
            {
                //Reset the edit index, in case a record was edited
                gridEquipmentManufacturerModels.EditIndex = -1;
                
                //rebind the selected item to null, in case a record was selected
                dtvManufacturerModel.DataSource = null;
                dtvManufacturerModel.DataBind();

                dtvManufacturerModel.Visible = false;
                pnlEditManufacturerModel.Visible = false;
            }

            //helper method to reset search
            private void ResetSearch()
            {
                txtSearch.Text = String.Empty;
                sessionState["Search"] = String.Empty;
            }

            //private void BindSearchedManufacturerModels(string[] searchText)
            //{
            //    //query equipment manufacturer model
            //    IEnumerable<GetEquipmentManufacturerModelData> variables = QuerySearchedManufacturerModels(searchText);

            //    int count = variables.Count();

            //    gridEquipmentManufacturerModels.DataSource = variables;

            //    // bind grid
            //    gridEquipmentManufacturerModels.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                String itemOrItems = (count > 1) ? "items" : "item";

                if (count != 0)
                    lblResults.Text = String.Format("{0} equipment manufacturer " + itemOrItems + " found.", count);
                else
                    lblResults.Text = "No equipment manufacturer items found.";
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }

                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection); //maintains the currect viewstate sorting for paging
                        else
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection()); //reverses the sort direction on sort
                    }

                    return dataView;
                }
                else
                    return new DataView();
            }      

            public void ProcessRequestWithQueryString()
            {
                if (!String.IsNullOrEmpty(Request.QueryString["mid"]))
                {
                    int manufacturerModelID;

                    if (!Int32.TryParse(Request.QueryString["mid"].ToString(), out manufacturerModelID)) return;

                    IEnumerable<GetEquipmentManufacturerModelData> manufacturerModel = DataMgr.EquipmentManufacturerModelDataMapper.GetFullEquipmentManufacturerModelByID(manufacturerModelID);

                    if (manufacturerModel.Any())
                    {
                        dtvManufacturerModel.DataSource = manufacturerModel; //set data source
                        dtvManufacturerModel.DataBind(); //bind manufacturer model to details view
                    }
                }
            }

            private void UpdateEquipmentManufacturerModel(EquipmentManufacturerModel equipmentManufacturerModel)
            {
                DataMgr.EquipmentManufacturerModelDataMapper.UpdateManufacturerModel(equipmentManufacturerModel);

                //re-update the hidden values, other if there is another edit, the subsequent changed value checks will be incorrect.
                hdnEditManufacturerModelName.Value = equipmentManufacturerModel.ManufacturerModelName;
                hdnEditManufacturer.Value = Convert.ToString(equipmentManufacturerModel.ManufacturerID);

                lblEditError.Text = updateSuccessful;
                lblEditError.Visible = true;
                lnkSetFocusView.Focus();

                //Bind manufacturer models again
                BindManufacturerModels();
            }

        #endregion
    }
}