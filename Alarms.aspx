﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Alarms.Master" AutoEventWireup="false" CodeBehind="Alarms.aspx.cs" Inherits="CW.Website.Alarms" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <script type="text/javascript" src="_assets/scripts/alarmWidgets.js"></script>

  <telerik:RadAjaxManager ID="radAjaxManager" runat="server">  
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="imgPdfDownload">
        <UpdatedControls>                    
          <telerik:AjaxUpdatedControl ControlID="pnlFilters" LoadingPanelID="loadingPanel" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>

  <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Skin="Default" />  
  
  <div id="iframesContainer" class="iframesContainer">

      <iframe id="buildingMapFrame" class="iframeInitialDisplay" src="_controls/alarm/LoadUserControl.aspx" control-to-load="/_controls/alarm/BuildingMap.ascx" scrolling="no" frameborder="0" onload="ShowIframe('buildingMap'); AutoResize('buildingMap')" data-skip-reload="true"></iframe>

      <iframe id="topTenMostFrequentAlarmsFrame" class="iframeInitialDisplay" src="_controls/alarm/LoadUserControl.aspx" control-to-load="/_controls/alarm/TopTenMostFrequentAlarms.ascx" scrolling="no" frameborder="0" onload="ShowIframe('topTenMostFrequentAlarms'); AutoResize('topTenMostFrequentAlarms');" data-buildingMap="attached"></iframe>
      
      <iframe id="todaysTopTenLongestStandingAlarmsFrame" class="iframeInitialDisplay" src="_controls/alarm/LoadUserControl.aspx" control-to-load="/_controls/alarm/TodaysTopTenLongestStandingAlarms.ascx" scrolling="no" frameborder="0" onload="ShowIframe('todaysTopTenLongestStandingAlarms'); AutoResize('todaysTopTenLongestStandingAlarms');" data-buildingMap="attached"></iframe>
        
      <iframe id="equipmentClassAlarmSummaryFrame" class="iframeInitialDisplay" src="_controls/alarm/LoadUserControl.aspx" control-to-load="/_controls/alarm/EquipmentClassAlarmSummary.ascx" scrolling="no" frameborder="0" onload="ShowIframe('equipmentClassAlarmSummary'); AutoResize('equipmentClassAlarmSummary');" data-buildingMap="attached"></iframe>
      
      <iframe id="alarmIncidenceGraphFrame" class="iframeInitialDisplay" src="_controls/alarm/LoadUserControl.aspx" control-to-load="/_controls/alarm/AlarmIncidenceGraph.ascx" scrolling="no" frameborder="0" onload="ShowIframe('alarmIncidenceGraph'); AutoResize('alarmIncidenceGraph');" data-buildingMap="attached"></iframe>

      <iframe id="topTenTotalTimeInAlarmFrame" class="iframeInitialDisplay" src="_controls/alarm/LoadUserControl.aspx" control-to-load="/_controls/alarm/TopTenTotalTimeInAlarm.ascx" scrolling="no" frameborder="0" onload="ShowIframe('topTenTotalTimeInAlarm'); AutoResize('topTenTotalTimeInAlarm');" data-buildingMap="attached"></iframe>

      <iframe id="buildingAlarmSummaryFrame" class="iframeInitialDisplay" src="_controls/alarm/LoadUserControl.aspx" control-to-load="/_controls/alarm/BuildingAlarmSummary.ascx" scrolling="no" frameborder="0" onload="ShowIframe('buildingAlarmSummary'); AutoResize('buildingAlarmSummary');"></iframe>

      <iframe id="searchAlarmsFrame" class="iframeInitialDisplayFull" src="_controls/alarm/LoadUserControl.aspx" control-to-load="/_controls/alarm/SearchAlarms.ascx" scrolling="no" frameborder="0" onload="ShowIframe('searchAlarms'); AutoResize('searchAlarms');"></iframe>

  </div>

  <div id="alarmsDashboard" class="alarmsDashboard">
      				   
    <div id="dashboardHeader" class="dashboardHeader">  
      
      <div class="alarmsModuleIcon"></div>

      <div class="headerTitle">
	    Alarms
	  </div>

      <div class="headerSpacer"></div>

	  <div class="headerClientInfo">

        <div class="headerClientImage">     
          <asp:Image ID="imgHeaderClientImage" CssClass="imgHeader" runat="server" />
        </div>

        <div class="headerClientName">
		  <label id="lblHeaderClientName" runat="server" />
	    </div>

	  </div>

      <div class="headerSpacer"></div>

      <div class="headerContent">
        <asp:Literal ID="litAlarmsBody" runat="server" />
      </div>     

    </div> <!--end of dashboardHeader div-->

    <hr />

    <div id="dashboardBody">

      <div class="dashboardBody dockWrapper">

        <asp:Panel ID="pnlFilters" ValidateRequestMode="Disabled" runat="server">

          <div id="formWrapper" class="divDockFormWrapperShort">
            
            <div class="divDockForm"> 
              <label class="labelBold">Start Date:</label>
              <telerik:RadDatePicker ID="txtStartDate" runat="server" CssClass="data-text-box" />
            </div>

            <div class="divDockForm">
              <label class="labelBold">End Date:</label>
              <telerik:RadDatePicker ID="txtEndDate" runat="server" CssClass="data-text-box" />
            </div>

            <div class="divDockForm">
              <span id="refreshData" class="labelBold">Refresh Data:</span>
              <div id="refreshBtn" class="imgBtnRefreshDiv">
                <img src="_assets/styles/images/refresh.jpg" onclick="CheckDateFieldsAndProcess();" class="imgBtn" alt="Refresh" />
              </div>
            </div>
            
            <input type="hidden" runat="server" id="hdn_container" clientidmode="Static" />

            <asp:ImageButton ID="imgPdfDownload" CssClass="imgDashboardDownload" ImageUrl="../../_assets/images/pdf-icon.jpg" OnClientClick="GetHtml('hdn_container', 'dashboardBody');" OnClick="btnDownloadButton_Click" AlternateText="download" runat="server" CausesValidation="false" />
            
          </div>

        </asp:Panel>

        <div class="dashboardLeftColumn">
        
          <telerik:RadDockLayout ID="radDockLayout1" runat="server">
            <telerik:RadDockZone ID="radDockZone1" runat="server">

              <telerik:RadDock ID="radDockBuildingMap" Title="Building Map" runat="server">
                <ContentTemplate>

                  <div id="updateProgressBuildingMap" class="updateProgressDivForWidgets" data-skip-reload="true"></div>
                  <div id="buildingMap" class="divForWidgets"></div>

			    </ContentTemplate>
              </telerik:RadDock>

              <telerik:RadDock ID="radDockTopTenMostFrequentAlarms" Title="Top Ten Most Frequent Alarms" runat="server">
                <ContentTemplate>

                  <div id="updateProgressTopTenMostFrequentAlarms" class="updateProgressDivForWidgets"></div>
                  <div id="topTenMostFrequentAlarms" class="divForWidgets"></div>
                
			    </ContentTemplate>
              </telerik:RadDock>

              <telerik:RadDock ID="radDockTodaysTopTenLongestStandingAlarms" Title="Today's Top Ten Longest Standing Alarms" runat="server">
                <ContentTemplate>

                  <div id="updateProgressTodaysTopTenLongestStandingAlarms" class="updateProgressDivForWidgets"></div>
                  <div id="todaysTopTenLongestStandingAlarms" class="divForWidgets"></div>
                
			    </ContentTemplate>
              </telerik:RadDock>

              <telerik:RadDock ID="radDockEquipmentClassAlarmSummary" Title="Equipment Alarm Summary" runat="server">
                <ContentTemplate>

                  <div id="updateProgressEquipmentClassAlarmSummary" class="updateProgressDivForWidgets"></div>
                  <div id="equipmentClassAlarmSummary" class="divForWidgets"></div>

			    </ContentTemplate>
              </telerik:RadDock>

            </telerik:RadDockZone>
          </telerik:RadDockLayout>

        </div> <!--end of dashboardLeftColumn div-->

        <div class="dashboardRightColumn">

          <telerik:RadDockLayout ID="radDockLayout2" runat="server">
            <telerik:RadDockZone ID="radDockZone2" runat="server">

               <telerik:RadDock ID="radDockAlarmIncidenceGraph" Title="Alarm Incidence Graph" runat="server">
                <ContentTemplate>
 
                  <div id="updateProgressAlarmIncidenceGraph" class="updateProgressDivForWidgets"></div>
                  <div id="alarmIncidenceGraph" class="divForWidgets"></div>

			    </ContentTemplate>
              </telerik:RadDock>

              <telerik:RadDock ID="radDockTopTenTotalTimeInAlarm" Title="Top Ten Total Time in Alarm" runat="server">
                <ContentTemplate>

                  <div id="updateProgressTopTenTotalTimeInAlarm" class="updateProgressDivForWidgets"></div>
                  <div id="topTenTotalTimeInAlarm" class="divForWidgets"></div>

			    </ContentTemplate>
              </telerik:RadDock>

              <telerik:RadDock ID="radDockBuildingAlarmSummary" Title="Building Alarm Summary" runat="server">
                <ContentTemplate>
                  
                  <div id="updateProgressBuildingAlarmSummary" class="updateProgressDivForWidgets"></div>
                  <div id="buildingAlarmSummary" class="divForWidgets"></div>

			    </ContentTemplate>
              </telerik:RadDock>

            </telerik:RadDockZone>
          </telerik:RadDockLayout>

        </div> <!--end of dashboardRightColumn div-->

        <telerik:RadDockLayout ID="radDockLayout3" runat="server">
          <telerik:RadDockZone ID="radDockZone3" runat="server">

            <telerik:RadDock ID="radDockSearchAlarms" Title="Search Alarms" runat="server">
              <ContentTemplate>

                <div id="updateProgressSearchAlarms" class="updateProgressDivForWidgets"></div>
                <div id="searchAlarms" class="divForWidgets"></div>
  
              </ContentTemplate>
            </telerik:RadDock>

          </telerik:RadDockZone>
        </telerik:RadDockLayout>
      
      </div> <!--end of dashboardBody dockWrapper class div for EO-->

    </div> <!--end of dashboardBody div-->

    <div id="dashboardFooter" class="dashboardFooter"></div>

  </div> <!--end of alarmsDashboard div-->

</asp:Content>