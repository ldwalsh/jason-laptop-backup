﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemQuickLinkAdministration.aspx.cs" Inherits="CW.Website.SystemQuickLinkAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>System Quick Links Administration</h1>                        
                <div class="richText">The system quick links administration area is used to view, add, and edit quick links.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        
                    <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Quick Links"></telerik:RadTab>
                            <telerik:RadTab Text="Add Quick Link"></telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                        <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Quick Links</h2> 
                                    <p>
                                        <a id="lnkSetFocusView" href="#" runat="server"></a>
                                        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                                    </p> 
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>          
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridQuickLinks"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="QuickLinkID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridQuickLinks_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridQuickLinks_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridQuickLinks_Sorting"  
                                             OnSelectedIndexChanged="gridQuickLinks_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridQuickLinks_Editing"                                                                                                                                                                                   
                                             OnRowDeleting="gridQuickLinks_Deleting"
                                             OnDataBound="gridQuickLinks_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="QuickLinkName" HeaderText="Quick Link Name">  
                                                    <ItemTemplate><label title="<%# Eval("QuickLinkName") %>"><%# StringHelper.TrimText(Eval("QuickLinkName"),30) %></label></ItemTemplate>                      
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Link">  
                                                    <ItemTemplate><label title="<%# Eval("QuickLink") %>"><%# StringHelper.TrimText(Eval("QuickLink"),40) %></label></ItemTemplate>                                              
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Role">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("RoleName"),20) %></ItemTemplate>                                                           
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Module">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("ModuleName"),30) %></ItemTemplate>                      
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>                                                                                               
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this quick link permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT QUICK LINK DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvQuickLink" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Quick Link Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Quick Link Name: </strong>" + Eval("QuickLinkName") + "</li>"%> 
                                                            <%# "<li><strong>Quick Link: </strong>" + Eval("QuickLink") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("QuickLinkDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("QuickLinkDescription") + "</li>"%>                                                                                                                     
                                                            <%# "<li><strong>Role: </strong>" + Eval("RoleName") + "</li>"%>    
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ModuleName"))) ? "" : "<li><strong>ModuleName: </strong>" + Eval("ModuleName") + "</li>"%>                                                                                                                                                                                        	                                                                	                                                                                                                            
                                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%> 
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT QUICK LINK PANEL -->                              
                                    <asp:Panel ID="pnlEditQuickLink" runat="server" Visible="false" DefaultButton="btnUpdateQuickLink">                                                                                             
                                        <div>
                                            <h2>Edit Quick Link</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <asp:HiddenField ID="hdnEditRoleID" runat="server" Visible="false" />
                                            <asp:HiddenField ID="hdnEditModuleID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Quick Link Name:</label>
                                                <asp:TextBox ID="txtEditQuickLinkName" MaxLength="50" runat="server"></asp:TextBox>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Quick Link:</label>
                                                <asp:TextBox ID="txtEditQuickLink" MaxLength="50" runat="server"></asp:TextBox>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 250, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>  
                                            <div class="divForm">   
                                                <label class="label">*Role:</label>    
                                                <asp:DropDownList ID="ddlEditRole" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                </asp:DropDownList> 
                                            </div>   
                                            <div class="divForm">   
                                                <label class="label">Module:</label>    
                                                <asp:DropDownList ID="ddlEditModule" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                    <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>   
                                                </asp:DropDownList> 
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Active:</label>
                                                <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                        
                                            </div>                                                                                                                                           
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateQuickLink" runat="server" Text="Update Link"  OnClick="updateQuickLinkButton_Click" ValidationGroup="EditQuickLink"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editQuickLinkNameRequiredValidator" runat="server"
                                        ErrorMessage="Quick Link Name is a required field."
                                        ControlToValidate="txtEditQuickLinkName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditQuickLink">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editQuickLinkNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editQuickLinkNameRequiredValidatorExtender"
                                        TargetControlID="editQuickLinkNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editQuickLinkRequiredValidator" runat="server"
                                        ErrorMessage="Quick Link is a required field."
                                        ControlToValidate="txtEditQuickLink"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditQuickLink">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editQuickLinkRequiredValidatorExtender" runat="server"
                                        BehaviorID="editQuickLinkRequiredValidatorExtender"
                                        TargetControlID="editQuickLinkRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editRoleRequiredValidator" runat="server"
                                        ErrorMessage="Role is a required field." 
                                        ControlToValidate="ddlEditRole"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditQuickLink">
                                        </asp:RequiredFieldValidator>
                                      <ajaxToolkit:ValidatorCalloutExtender ID="editRoleRequiredValidatorExtender" runat="server"
                                        BehaviorID="editRoleRequiredValidatorExtender" 
                                        TargetControlID="editRoleRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                                    </asp:Panel>
                                                                                     
                            </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddQuickLink" runat="server" DefaultButton="btnAddQuickLink"> 
                                    <h2>Add Quick Link</h2>
                                    <div>    
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                                
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Quick Link Name:</label>
                                            <asp:TextBox ID="txtAddQuickLinkName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">*Quick Link:</label>
                                            <asp:TextBox ID="txtAddQuickLink" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div>
                                        <div class="divForm">
                                             <label class="label">Description:</label>
                                             <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 250, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                             <div id="divAddDescriptionCharInfo"></div>
                                        </div>  
                                        <div class="divForm">   
                                            <label class="label">*Role:</label>    
                                            <asp:DropDownList ID="ddlAddRole" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div> 
                                        <div class="divForm">   
                                            <label class="label">Module:</label>    
                                            <asp:DropDownList ID="ddlAddModule" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                 <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                        
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddQuickLink" runat="server" Text="Add Link"  OnClick="addQuickLinkButton_Click" ValidationGroup="AddQuickLink"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addQuickLinkNameRequiredValidator" runat="server"
                                        ErrorMessage="Quick Link Name is a required field."
                                        ControlToValidate="txtAddQuickLinkName"          
                                        SetFocusOnError="true"
                                        Display="None"
                               
                                        ValidationGroup="AddQuickLink">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addQuickLinkNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addQuickLinkNameRequiredValidatorExtender"
                                        TargetControlID="addQuickLinkNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addQuickLinkRequiredValidator" runat="server"
                                        ErrorMessage="Quick Link is a required field."
                                        ControlToValidate="txtAddQuickLink"          
                                        SetFocusOnError="true"
                                        Display="None"
                                     
                                        ValidationGroup="AddQuickLink">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addQuickLinkRequiredValidatorExtender" runat="server"
                                        BehaviorID="addQuickLinkRequiredValidatorExtender"
                                        TargetControlID="addQuickLinkRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addRoleRequiredValidator" runat="server"
                                        ErrorMessage="Role is a required field." 
                                        ControlToValidate="ddlAddRole"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                 
                                        ValidationGroup="AddQuickLink">
                                        </asp:RequiredFieldValidator>
                                      <ajaxToolkit:ValidatorCalloutExtender ID="addRoleRequiredValidatorExtender" runat="server"
                                        BehaviorID="addRoleRequiredValidatorExtender" 
                                        TargetControlID="addRoleRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                </asp:Panel>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>

                   </div>                                                                 
                
</asp:Content>


                    
                  
