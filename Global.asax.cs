﻿using System;
using System.Web;
using System.Web.Management;
using CW.Logging;
using CW.Utility;
using CW.Utility.Extension;
using CW.Website.DependencyResolution;
using System.Configuration;
using System.Linq;
using CW.SecureConfig;
using CW.ClientDataAccess;
using System.IdentityModel.Services;
using System.IdentityModel.Services.Configuration;
using System.Xml;
using System.Reflection;
using CW.Data.Instrumentation;
using CW.Data.AzureStorage;
using CW.Common.Constants;
using CW.RESTAdapter;
using System.Collections.Generic;
using CW.RESTAdapter.Configuration;
using Newtonsoft.Json.Linq;

namespace CW.Website
{
    public class Global: HttpApplication
    {
        static private string Thumbprint { get; set; }
        static private string EncryptionKey { get; set; }
        static private string DataAccessEndPoint { get; set; }
        static private string DeploymentID { get; set; }

        protected void Application_Start(Object sender, EventArgs e)
        {
            //10/5/2015, 15.2.78.2
            //// Code that runs on application startup  
            string error = null;
            DataAccessEndPoint = ConfigurationManager.AppSettings["endPoint"];
            if (string.IsNullOrEmpty(DataAccessEndPoint))
            {
                error = "EndPoint not defined";
            }
            else
            {
                DataFromRepoFactory.DataAccessEndPoint = DataAccessEndPoint;
                DataFromRepoFactory.DlgGetAuthenticationHeaderValue = GetAuthenticationHeaderValue;
                bool replaceSettings = "1" == ConfigurationManager.AppSettings["replaceSettings"];
                DeploymentID = ConfigurationManager.AppSettings["DeploymentID"];
                error = CW.ConfigProxy.Proxy.Setup(DataAccessEndPoint + "Config/Settings", System.Reflection.Assembly.GetExecutingAssembly(), DeploymentID, ProcessAppSettings, replaceSettings);
                if (string.IsNullOrEmpty(error))
                {
                    mIdAndKey = new IdKey(System.Reflection.Assembly.GetExecutingAssembly());
                    Thumbprint = ConfigurationManager.AppSettings["stsProxyThumbprint"].ToUpper();
                    EncryptionKey = CW.SecureConfig.EncryptKey.GetEncryptionKey(Thumbprint, ref error);

                    IDictionary<string, string> props = ConfigurationManager.AppSettings.AllKeys.ToDictionary(t => t, t => ConfigurationManager.AppSettings[t]);
                    EO.Pdf.Runtime.AddLicense(ConfigurationManager.AppSettings["EOLicense"]);

                    FederatedAuthentication.FederationConfigurationCreated += FederatedAuthentication_FederationConfigurationCreated;

                    // The following of exclusively for extending non idp fed authn token to 8 hours (only once - session based)
                    FederatedAuthentication.SessionAuthenticationModule.SessionSecurityTokenReceived += SessionAuthenticationModule_SessionSecurityTokenReceived;
                }
            }

            if (!string.IsNullOrEmpty(error))
            {
                throw new ApplicationException(error);
            }

            IoC.Init(new ScanningRegistry());
        }

        #region Extend non idp fed authn token to 8 hours (only once - session based)
        private const int FED_AUTHN_TOKEN_LIFETIME = 8; // Hours
        void SessionAuthenticationModule_SessionSecurityTokenReceived(object sender, SessionSecurityTokenReceivedEventArgs e)
        {
            try
            {
                if (null == e.SessionToken.ClaimsPrincipal.Claims.FirstOrDefault(c => c.Type.EndsWith("organizationid"))) // non idp
                {
                    DateTime now = DateTime.UtcNow;
                    DateTime validTo = e.SessionToken.ValidTo;

                    if (now < validTo)
                    {
                        DateTime validFrom = e.SessionToken.ValidFrom;
                        TimeSpan ts = validTo - validFrom;
                        if (ts.Hours < FED_AUTHN_TOKEN_LIFETIME)
                        {
                            SessionAuthenticationModule sam = sender as SessionAuthenticationModule;
                            e.SessionToken = sam.CreateSessionSecurityToken(e.SessionToken.ClaimsPrincipal, e.SessionToken.Context, validFrom, validFrom.AddHours(FED_AUTHN_TOKEN_LIFETIME), e.SessionToken.IsPersistent);
                            e.ReissueCookie = true;
                        }
                    }
                }
            }
            catch 
            {
            }
        }
        #endregion Extend non idp fed authn token to 8 hours (only once)
        private bool HttpsEnabled
        {
            get
            {
                bool isHttpsEnabled = false;
                bool.TryParse(ConfigurationManager.AppSettings["IsHttpsEnabled"], out isHttpsEnabled);
                return (isHttpsEnabled);
            }
        }
        private void ProcessAppSettings(System.Collections.Specialized.NameValueCollection settings)
        {
            // Is there a versionNumber key in the app settings? If so and the value does not start and end with a '#'
            // we need to replace the value for all keys that have a value that contains (yes, greedy) _assets with _assets + buildNumber .
            // This is being done so that browser caching of assets is broken.
            const string versionNumber = "versionNumber";
            const string assets = "_assets";
            if (settings.AllKeys.Contains(versionNumber))
            {
                string val = settings[versionNumber].Trim();
                if (!string.IsNullOrEmpty(val) && '#' != val[0] && '#' != val[val.Length - 1])
                {
                    string[] segments = val.Split('.');
                    if (null != segments && segments.Length > 2) // Build number is 3rd segment
                    {
                        string buildNumber = segments[2].Trim();
                        if( ! string.IsNullOrEmpty(buildNumber))
                        {
                            for (int i = 0, num = settings.AllKeys.Count(); i < num; i++)
                            {
                                string key = settings.AllKeys[i];
                                switch (key)
                                {
                                    case versionNumber:
                                        {
                                            break;
                                        }
                                    default:
                                        {
                                            val = settings[key];
                                            if (val.Contains(assets))
                                            {
                                                settings[key] = val.Replace(assets, assets + buildNumber);
                                            }
                                            break;
                                        }
                                }
                            }
                        }
                    }
                }
            }

            const string siteAddress = "SiteAddress";
            if (settings.AllKeys.Contains(siteAddress))
            {
                bool httpsEnabled = HttpsEnabled;
                string sSiteAddress = settings[siteAddress].Trim();
                System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex((httpsEnabled ? "^https://" : "^http://"), System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                if (!regex.IsMatch(sSiteAddress))
                {
                    settings[siteAddress] = sSiteAddress.Replace((httpsEnabled ? "http://" : "https://"), (httpsEnabled ? "https://" : "http://"));
                }
            }
        }

#region WIF + FAM
        private void FederatedAuthentication_FederationConfigurationCreated(object sender, FederationConfigurationCreatedEventArgs e)
        {
            //Uri uri = this.Context.Request.Url;
            string allowedAudience = ConfigurationManager.AppSettings["SiteAddress"];
            Uri uri = new Uri(allowedAudience);
            string rpRealm = uri.AbsoluteUri; //identifies the application that is requesting the token
            string reply = uri.AbsoluteUri; //where the STS should be posting the token to. In most cases, this will be the current application’s address
            string issuer = ConfigurationManager.AppSettings["stsProxy"]; // the address of the STS that we rely on to get tokens
            try
            {
                string msg = "allowedAudience = " + allowedAudience + ", issuer = " + issuer;
                IoC.Resolve<ISectionLogManager>().Log(Common.Constants.DataConstants.LogLevel.INFO, msg);
            }
            catch { };

            var federationConfiguration = new FederationConfiguration();
            federationConfiguration.IdentityConfiguration.AudienceRestriction.AllowedAudienceUris.Add(uri);

            // Due to white labeling (branding), site address will vary. To account for that, if AdditionalAudienceUrls is configured, add to AllowedAudienceUris collection
            // of the FederationConfiguration so that WIF is happy.
            string additionalAudienceUrls = ConfigurationManager.AppSettings["AdditionalAudienceUrls"];
            if( ! string.IsNullOrEmpty(additionalAudienceUrls))
            { 
                string [] urls = additionalAudienceUrls.Split('|');
                if (null != urls && urls.Length > 0)
                {
                    urls.Distinct().Where(u => u.Length > 0 ).ForEach(url =>
                        {
                            uri = new Uri(url);
                            if (null == federationConfiguration.IdentityConfiguration.AudienceRestriction.AllowedAudienceUris.FirstOrDefault( uriExists => uriExists == uri))
                            {
                                federationConfiguration.IdentityConfiguration.AudienceRestriction.AllowedAudienceUris.Add(uri);
                            }
                        });
                }
            }
        
            var chunkedCookieHandler = new ChunkedCookieHandler() { RequireSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["requireSSL"]) };
            federationConfiguration.CookieHandler = chunkedCookieHandler;
            federationConfiguration.WsFederationConfiguration.PassiveRedirectEnabled = true; // Whether or not users should be automatically redirected to the STS when considered unauthorized.
            // Turning it on makes no difference what so ever unless module WSFederationAuthenticationModule is added.
            federationConfiguration.WsFederationConfiguration.Issuer = issuer;
            federationConfiguration.WsFederationConfiguration.Realm = rpRealm;
            federationConfiguration.WsFederationConfiguration.RequireHttps = chunkedCookieHandler.RequireSsl;
            federationConfiguration.WsFederationConfiguration.Reply = reply;

            XmlDocument x = new XmlDocument();
            XmlNode n = x.AppendChild(x.CreateElement("trustedIssuers"));
            n = n.AppendChild(x.CreateElement("add"));
            ((XmlElement)n).SetAttribute("thumbprint", Thumbprint);
            ((XmlElement)n).SetAttribute("name", issuer);
            federationConfiguration.IdentityConfiguration.IssuerNameRegistry.LoadCustomConfiguration(x.SelectNodes("/trustedIssuers"));
            federationConfiguration.IdentityConfiguration.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.None;
            e.FederationConfiguration = federationConfiguration;
            FederatedAuthentication.WSFederationAuthenticationModule.RedirectingToIdentityProvider += new EventHandler<RedirectingToIdentityProviderEventArgs>(WSFederationAuthenticationModule_RedirectingToIdentityProvider);
        }

        void WSFederationAuthenticationModule_RedirectingToIdentityProvider(object sender, RedirectingToIdentityProviderEventArgs e)
        {
            if (null != e.SignInRequestMessage)
            {
                // Preform the reply url switching logic ONLY if scheme is HTTPS
                Uri uriActual = this.Context.Request.Url;
                if ("https" == uriActual.Scheme.ToLower())
                {
                    string replyActual = (new Uri(uriActual.GetLeftPart(UriPartial.Authority))).AbsoluteUri;
                    string replyConfigured = e.SignInRequestMessage.Reply;
                    if (replyActual.ToLower() != replyConfigured.ToLower()) // Different, set reply url to that of the actual
                    {
                        e.SignInRequestMessage.Reply = replyActual;
                    }
                }
                AuthnHelper.OnRedirectingToIdentityProvider(Response, e.SignInRequestMessage.Parameters);
            }
        }
        void WSFederationAuthenticationModule_SignedOut(object sender, EventArgs e)
        {
        }
        void WSFederationAuthenticationModule_SignedIn(object sender, EventArgs e)
        {
            IoC.Resolve<ISectionLogManager>().Log(Common.Constants.DataConstants.LogLevel.INFO, "IsAuthenticated = " + Request.IsAuthenticated);
            AuthnHelper.OnSignin(Request, Response);
        }
        static public void Signin(string data)
        {
            FederatedAuthentication.WSFederationAuthenticationModule.SignIn(AuthnHelper.Signin(data));
        }

        static public void Signout(HttpContext context)
        {
            AuthnHelper.OnSignout(context);
        }
#endregion WIF + FAM

        private static IdKey mIdAndKey = null;
        static private System.Net.Http.HttpRequestMessage GetAuthenticationHeaderValue(System.Net.Http.HttpRequestMessage req, byte[] content)
        {
            HMAC hmac = new HMAC(HMAC.eType.client, mIdAndKey.ID, mIdAndKey.Key, DeploymentID);
            req = hmac.PrepareForSendingToServer(req, content).Result;
            return (req);

        }

        private void Application_AcquireRequestState(Object source, EventArgs e)
        {
            if (null != HttpContext.Current.Session)
            {
                DateTime dtUtcNow = DateTime.UtcNow;
                bool isHttpsEnabled;
                bool.TryParse(ConfigurationManager.AppSettings["IsHttpsEnabled"], out isHttpsEnabled);
                string sessionId = HttpContext.Current.Session.SessionID;
                string cookieName = CW.Website._framework.SiteUserControl.KeyForTimeoutCookie();
                HttpCookie ck = HttpContext.Current.Request.Cookies[cookieName];

                if (null != ck) // Gets created on login
                {
                    ck.HttpOnly = false;
                    ck.Secure = isHttpsEnabled;

                    if (HttpContext.Current.Session.IsNewSession)
                    {
                        HttpContext.Current.Response.Cookies.Remove(cookieName);
                        ck.Expires = dtUtcNow.AddDays(-10);
                        ck.Value = null;
                        HttpContext.Current.Response.SetCookie(ck);
                    }
                    else
                    {
                        DateTime dtFuture = dtUtcNow.AddMinutes(HttpContext.Current.Session.Timeout);
                        ck.Value = dtFuture.ToString();
                        ck.Expires = dtFuture;
                        HttpContext.Current.Response.SetCookie(ck);
                    }
                }
            }
        }

        protected void Session_Start(Object sender, EventArgs e)
        {
        }
        protected void Application_Error(Object sender, EventArgs e)
        {
            var ex = Server.GetLastError();

            // The following has been added to likely handle "Key not valid for use in specified-state" exception. 
            // If true, simply signout (to delete FedAuth cookie)
            if (ex is System.Security.Cryptography.CryptographicException || ex.InnerException is System.Security.Cryptography.CryptographicException)
            {
                var message = "Uncaught error (Resetting): " + ExceptionHelper.WriteExceptionMessage(ex);
                IoC.Resolve<ISectionLogManager>().Log(Common.Constants.DataConstants.LogLevel.INFO, message, ex);
                Server.ClearError();
                HttpContext context = HttpContext.Current;
                if (null != context)
                {
                    try
                    {
                        Signout(context);
                    }
                    catch { }
                    finally
                    {
                        try
                        {
                            FederatedAuthentication.WSFederationAuthenticationModule.SignOut();
                        }
                        catch { }
                        finally
                        {
                            if (null != context.Session)
                            {
                                try
                                {
                                    context.Session.Abandon();
                                }
                                catch { }
                            }
                        }
                    }
                }
                Response.Redirect("/Home.aspx");
            }
            else
            {
                var message = "Uncaught error: " + ExceptionHelper.WriteExceptionMessage(ex);
                IoC.Resolve<ISectionLogManager>().Log(Common.Constants.DataConstants.LogLevel.ERROR, message, ex);

                //
                // This error is generated by the AsyncFileUpload postback when the file is larger than alloted request size.
                //
                if ((ex.InnerException is HttpException) && (((HttpException)ex.InnerException).WebEventCode == WebEventCodes.RuntimeErrorPostTooLarge) && (Request.Url.Query.Contains("AsyncFileUploadID=")))
                {
                    Response.Close();
                }
            }
         }
    }  
}