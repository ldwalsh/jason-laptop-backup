﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemFAQAdministration.aspx.cs" Inherits="CW.Website.SystemFAQAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                  	                  	
            	<h1>System FAQ Administration</h1>                        
                <div class="richText">The system faq administration area is used to view, add, and edit faq's and faq sections.</div>                                 
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>                                       
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View FAQ Sections"></telerik:RadTab>
                            <telerik:RadTab Text="Add FAQ Section"></telerik:RadTab>
                            <telerik:RadTab Text="View FAQs"></telerik:RadTab>
                            <telerik:RadTab Text="Add FAQ"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View FAQ Sections</h2>
                                    <p>
                                            <a id="lnkFAQSectionSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblFAQSectionResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblFAQSectionErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>        
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridFAQSections"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="SectionID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridFAQSections_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridFAQSections_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridFAQSections_Sorting"                                                                                                               
                                             OnSelectedIndexChanged="gridFAQSections_OnSelectedIndexChanged"
                                             OnRowEditing="gridFAQSections_Editing"     
                                             OnRowDeleting="gridFAQSections_Deleting"
                                             OnDataBound="gridFAQSections_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Title" HeaderText="Title">  
                                                    <ItemTemplate><%# Eval("Title")%></ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="SortOrder" HeaderText="Sort Order">  
                                                    <ItemTemplate><%# Eval("SortOrder")%></ItemTemplate>
                                                </asp:TemplateField>                                                 
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this faq section permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br />
                                    <div>                                                  
                                    <!--SELECT FAQ SECTION DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvFAQSection" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>FAQ Section Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Title: </strong>" + Eval("Title") + "</li>"%> 
                                                            <%# "<li><strong>Sort Order: </strong>" + Eval("SortOrder") + "</li>"%>                                                                                                                                                                                                             	                                                                	                                                                                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT FAQ SECTION PANEL -->                              
                                    <asp:Panel ID="pnlEditFAQSection" runat="server" Visible="false" DefaultButton="btnUpdateFAQSection">                                                                                             
                                        <div>
                                            <h2>Edit FAQ Section</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditFAQSectionError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditFAQSectionSectionID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Title:</label>
                                                <asp:TextBox ID="txtEditFAQSectionTitle" MaxLength="50" runat="server"></asp:TextBox>                                  
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Sort Order:</label>
                                                <asp:TextBox ID="txtEditFAQSectionSortOrder" MaxLength="2" runat="server"></asp:TextBox>                                  
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateFAQSection" runat="server" Text="Edit Section"  OnClick="updateFAQSectionButton_Click" ValidationGroup="EditFAQSection"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editFAQSectionTitleRequiredValidator" runat="server"
                                        ErrorMessage="Title is a required field."
                                        ControlToValidate="txtEditFAQSectionTitle"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditFAQSection">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editFAQSectionTitleRequiredValidatorExtender" runat="server"
                                        BehaviorID="editFAQSectionTitleRequiredValidatorExtender"
                                        TargetControlID="editFAQSectionTitleRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                            
                                    <ajaxToolkit:FilteredTextBoxExtender ID="editFAQSectionSortOrderFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtEditFAQSectionSortOrder"         
                                            FilterType="Numbers"
                                            />   
                                    <asp:RequiredFieldValidator ID="editFAQSectionSortOrderRequiredValidator" runat="server"
                                        ErrorMessage="Sort Order is a required field."
                                        ControlToValidate="txtEditFAQSectionSortOrder"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditFAQSection">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editFAQSectionSortOrderRequiredValidatorExtender" runat="server"
                                        BehaviorID="editFAQSectionSortOrderRequiredValidatorExtender"
                                        TargetControlID="editFAQSectionSortOrderRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                    </asp:Panel>                                                                                               
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server"> 
                                <asp:Panel ID="pnlAddFAQSection" runat="server" DefaultButton="btnAddFAQSection">
                                    <h2>Add FAQ Section</h2> 
                                    <div>        
                                        <a id="lnkAddFAQSectionSetFocus" runat="server"></a>                                            
                                        <asp:Label ID="lblAddFAQSectionError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Title:</label>
                                            <asp:TextBox ID="txtAddFAQSectionTitle" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                                        </div>  
                                        <div class="divForm">
                                            <label class="label">*Sort Order:</label>
                                            <asp:TextBox ID="txtAddFAQSectionSortOrder" MaxLength="2" runat="server"></asp:TextBox>                                  
                                        </div>                                                   
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddFAQSection" runat="server" Text="Add Section"  OnClick="addFAQSectionButton_Click" ValidationGroup="AddFAQSection"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addFAQSectionTitleRequiredValidator" runat="server"
                                        ErrorMessage="Title is a required field."
                                        ControlToValidate="txtAddFAQSectionTitle"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddFAQSection">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addFAQSectionTitleRequiredValidatorExtender" runat="server"
                                        BehaviorID="addFAQSectionTitleRequiredValidatorExtender"
                                        TargetControlID="addFAQSectionTitleRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <ajaxToolkit:FilteredTextBoxExtender ID="addFAQSectionSortOrderFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtAddFAQSectionSortOrder"         
                                            FilterType="Numbers"
                                            />   
                                    <asp:RequiredFieldValidator ID="addFAQSectionSortOrderRequiredValidator" runat="server"
                                        ErrorMessage="Sort Order is a required field."
                                        ControlToValidate="txtAddFAQSectionSortOrder"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddFAQSection">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addFAQSectionSortOrderRequiredValidatorExtender" runat="server"
                                        BehaviorID="addFAQSectionSortOrderRequiredValidatorExtender"
                                        TargetControlID="addFAQSectionSortOrderRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                </asp:Panel>                                                                                                                                                                  
                            </telerik:RadPageView> 
                            <telerik:RadPageView ID="RadPageView3" runat="server">
                                    <h2>View FAQs</h2>
                                    <p>
                                            <a id="lnkFAQSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblFAQResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblFAQErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>          
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridFAQs"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="FAQID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridFAQs_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridFAQs_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridFAQs_Sorting"                                                                                                               
                                             OnSelectedIndexChanged="gridFAQs_OnSelectedIndexChanged"
                                             OnRowEditing="gridFAQs_Editing"     
                                             OnRowDeleting="gridFAQs_Deleting"
                                             OnDataBound="gridFAQs_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Question" HeaderText="Question">  
                                                    <ItemTemplate><%# Eval("Question")%></ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="SortOrder" HeaderText="Sort Order">  
                                                    <ItemTemplate><%# Eval("SortOrder")%></ItemTemplate>
                                                </asp:TemplateField>  
					                            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="SectionTitle" HeaderText="Section Title">  
                                                    <ItemTemplate>
                                                        <%# Eval("SectionTitle")%>
                                                        <asp:HiddenField ID="hdnFAQSectionID" runat="server" Visible="true" Value='<%# Eval("SectionID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                 
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton2" Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this faq permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br />      
                                    <div>                                                  
                                    <!--SELECT FAQ DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvFAQ" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>FAQ Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Question: </strong>" + Eval("Question") + "</li>"%> 
                                                            <%# "<li><strong>Answer: </strong>" + Eval("Answer") + "</li>"%> 
                                                            <%# "<li><strong>Sort Order: </strong>" + Eval("SortOrder") + "</li>"%>                                                                          
                                                            <%# "<li><strong>Section Title: </strong>" + Eval("SectionTitle") + "</li>"%>                                                                                                                                                                                                	                                                                	                                                                                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT FAQ PANEL -->                              
                                    <asp:Panel ID="pnlEditFAQ" runat="server" Visible="false" DefaultButton="btnUpdateFAQ">                                                                                             
                                        <div>
                                            <h2>Edit FAQ</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditFAQError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditFAQFAQID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Question:</label>
                                                <textarea name="txtEditFAQQuestion" id="txtEditFAQQuestion" cols="40" onkeyup="limitChars(this, 1000, 'divEditFAQQuestionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditFAQQuestionCharInfo"></div>
                                            </div> 
					                        <div class="divForm">
                                                <label class="label">*Answer:</label>
                                                <textarea name="txtEditFAQAnswer" id="txtEditFAQAnswer" cols="40" onkeyup="limitChars(this, 5000, 'divEditFAQAnswerCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditFAQAnswerCharInfo"></div>
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">*Section:</label>    
                                                <asp:DropDownList ID="ddlEditFAQSection" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                     <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Sort Order:</label>
                                                <asp:TextBox ID="txtEditFAQSortOrder" MaxLength="2" runat="server"></asp:TextBox>                                  
                                            </div>                                                                                        
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateFAQ" runat="server" Text="Edit FAQ"  OnClick="updateFAQButton_Click" ValidationGroup="EditFAQSection"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->                                                                           
                                    <asp:RequiredFieldValidator ID="editFAQSectionRequiredValidator" runat="server"
                                        ErrorMessage="Section is a required field." 
                                        ControlToValidate="ddlEditFAQSection"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditFAQ">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editFAQSectionRequiredValidatorExtender" runat="server"
                                        BehaviorID="editFAQSectionRequiredValidatorExtender" 
                                        TargetControlID="editFAQSectionRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="editFAQSortOrderFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtEditFAQSortOrder"         
                                            FilterType="Numbers"
                                            />
                                    <asp:RequiredFieldValidator ID="editFAQSortOrderRequiredValidator" runat="server"
                                        ErrorMessage="Sort Order is a required field."
                                        ControlToValidate="txtEditFAQSortOrder"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditFAQ">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editFAQSortOrderRequiredValidatorExtender" runat="server"
                                        BehaviorID="editFAQSortOrderRequiredValidatorExtender"
                                        TargetControlID="editFAQSortOrderRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    </asp:Panel>                                                                                              
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView4" runat="server">  
                                <asp:Panel ID="pnlAddFAQ" runat="server" DefaultButton="btnAddFAQ">
                                    <h2>Add FAQ</h2> 
                                    <div>        
                                        <a id="lnkAddFAQSetFocus" runat="server"></a>                                            
                                        <asp:Label ID="lblAddFAQError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Question:</label>
                                            <textarea name="txtAddFAQQuestion" id="txtAddFAQQuestion" cols="40" onkeyup="limitChars(this, 1000, 'divAddFAQQuestionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                            <div id="divAddFAQQuestionCharInfo"></div>
                                        </div> 
					                    <div class="divForm">
                                            <label class="label">*Answer:</label>
                                            <textarea name="txtAddFAQAnswer" id="txtAddFAQAnswer" cols="40" onkeyup="limitChars(this, 5000, 'divAddFAQAnswerCharInfo')" rows="5" runat="server"></textarea>                                                         
                                            <div id="divAddFAQAnswerCharInfo"></div>
                                        </div>                                          
                                        <div class="divForm">   
                                            <label class="label">*Section:</label>    
                                            <asp:DropDownList ID="ddlAddFAQSection" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                    <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Sort Order:</label>
                                            <asp:TextBox ID="txtAddFAQSortOrder" MaxLength="2" runat="server"></asp:TextBox>                                  
                                        </div>                                                   
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddFAQ" runat="server" Text="Add FAQ"  OnClick="addFAQButton_Click" ValidationGroup="AddFAQ"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->                                                                              
                                    <asp:RequiredFieldValidator ID="addFAQSectionRequiredValidator" runat="server"
                                        ErrorMessage="Section is a required field." 
                                        ControlToValidate="ddlAddFAQSection"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddFAQ">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addFAQSectionRequiredValidatorExtender" runat="server"
                                        BehaviorID="addFAQSectionRequiredValidatorExtender" 
                                        TargetControlID="addFAQSectionRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="addFAQSortOrderFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtAddFAQSortOrder"         
                                            FilterType="Numbers"
                                            />   
                                    <asp:RequiredFieldValidator ID="addFAQSortOrderRequiredValidator" runat="server"
                                        ErrorMessage="Sort Order is a required field."
                                        ControlToValidate="txtAddFAQSortOrder"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddFAQ">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addFAQSortOrderRequiredValidatorExtender" runat="server"
                                        BehaviorID="addFAQSortOrderRequiredValidatorExtender"
                                        TargetControlID="addFAQSortOrderRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                </asp:Panel>
                            </telerik:RadPageView>                         
                        </telerik:RadMultiPage>
                   </div>                                                                 
             
</asp:Content>