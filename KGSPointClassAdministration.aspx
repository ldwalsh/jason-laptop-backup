﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSPointClassAdministration.aspx.cs" Inherits="CW.Website.KGSPointClassAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                     	                  	
            	<h1>KGS Point Class Administration</h1>                        
                <div class="richText">The kgs point class administration area is used to view, add, and edit point classes.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Point Classes"></telerik:RadTab>
                            <telerik:RadTab Text="Add Point Class"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Point Classes</h2> 
                                    <p>
                                        <a id="lnkSetFocusView" href="#" runat="server"></a>
                                        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                                        <asp:ValidationSummary ID="valSummary" ShowSummary="true" ValidationGroup="EditPointClasses" runat="server" />
                                    </p>     
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>     
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridPointClasses"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="PointClassID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridPointClasses_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridPointClasses_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridPointClasses_Sorting"                                                                                                               
                                             OnSelectedIndexChanged="gridPointClasses_OnSelectedIndexChanged"
                                             OnRowEditing="gridPointClasses_Editing"    
                                             OnRowDeleting="gridPointClasses_Deleting"
                                             OnDataBound="gridPointClasses_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointClassName" HeaderText="Point Class">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("PointClassName"),50) %></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="IP Engineering Units"> 
                                                    <ItemTemplate><%# Eval("IPEngUnits") %>
                                                        <asp:HiddenField ID="hdnIPEngUnitID" runat="server" Visible="true" Value='<%# Eval("IPEngUnitID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField HeaderText="SI Engineering Units"> 
                                                    <ItemTemplate><%# Eval("SIEngUnits") %>
                                                        <asp:HiddenField ID="hdnSIEngUnitID" runat="server" Visible="true" Value='<%# Eval("SIEngUnitID")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Point Class Description">  
                                                    <ItemTemplate><%# Eval("PointClassDescription") %></ItemTemplate>                                    
                                                </asp:TemplateField>                                                 
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this point class permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br />         
                                    <div>                                                  
                                     <!--SELECT POINT CLASS DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvPointClass" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Point Class Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Point Class Name: </strong>" + Eval("PointClassName") + "</li>"%> 
                                                            <%# "<li><strong>IP Eng. Units: </strong>" + Eval("IPEngUnits") + "</li>"%> 
                                                            <%# "<li><strong>SI Eng. Units: </strong>" + Eval("SIEngUnits") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PointClassDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("PointClassDescription") + "</li>"%>                                                                                                                                                                                                             	                                                                	                                                                                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT POINT CLASS PANEL -->                              
                                    <asp:Panel ID="pnlEditPointClass" runat="server" Visible="false" DefaultButton="btnUpdatePointClass">                                                                                             
                                        <div>
                                            <h2>Edit Point Class</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Point Class Name:</label>
                                                <asp:TextBox ID="txtEditPointClassName" MaxLength="50" runat="server"></asp:TextBox>                                  
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">*IP Eng Units:</label>   
                                                <asp:DropDownList CssClass="dropdown" ID="ddlEditIPEngUnits" runat="server" >  
                                                     <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">*SI Eng Units:</label>   
                                                <asp:DropDownList CssClass="dropdown" ID="ddlEditSIEngUnits" runat="server" >  
                                                     <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdatePointClass" runat="server" Text="Update Class"  OnClick="updatePointClassButton_Click" ValidationGroup="EditPointClass"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editPointClassNameRequiredValidator" runat="server"
                                        ErrorMessage="Point Class Name is a required field."
                                        ControlToValidate="txtEditPointClassName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditPointClass">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editPointClassNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editPointClassNameRequiredValidatorExtender"
                                        TargetControlID="editPointClassNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editIPEngUnitRequiredValidator" runat="server"
                                        ErrorMessage="IP Engineering Units is a required field." 
                                        ControlToValidate="ddlEditIPEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditPointClass">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editIPEngUnitRequiredValidatorExtender" runat="server"
                                        BehaviorID="editIPEngUnitRequiredValidatorExtender" 
                                        TargetControlID="editIPEngUnitRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                    <asp:RequiredFieldValidator ID="editSIEngUnitRequiredValidator" runat="server"
                                        ErrorMessage="SI Engineering Units is a required field." 
                                        ControlToValidate="ddlEditSIEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditPointClass">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editSIEngUnitRequiredValidatorExtender" runat="server"
                                        BehaviorID="editSIEngUnitRequiredValidatorExtender" 
                                        TargetControlID="editSIEngUnitRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                    </asp:Panel>
                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddPointClass" runat="server" DefaultButton="btnAddPointClass">  
                                    <h2>Add Point Class</h2>
                                    <div>           
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                         
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Point Class Name:</label>
                                            <asp:TextBox ID="txtAddPointClassName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="divForm">   
                                            <label class="label">*IP Engineering Units:</label>    
                                            <asp:DropDownList ID="ddlAddIPEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div>  
                                        <div class="divForm">   
                                            <label class="label">*SI Engineering Units:</label>    
                                            <asp:DropDownList ID="ddlAddSIEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div>  
                                        <div class="divForm">
                                            <label class="label">Description:</label>
                                            <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                            <div id="divAddDescriptionCharInfo"></div>
                                        </div>                                                                                              
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddPointClass" runat="server" Text="Add Class"  OnClick="addPointClassButton_Click" ValidationGroup="AddPointClass"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="pointClassNameRequiredValidator" runat="server"
                                        ErrorMessage="Point Class Name is a required field."
                                        ControlToValidate="txtAddPointClassName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddPointClass">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="pointClassNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="pointClassNameRequiredValidatorExtender"
                                        TargetControlID="pointClassNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                     <asp:RequiredFieldValidator ID="addIPEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="IP Engineering Units is a required field." 
                                        ControlToValidate="ddlAddIPEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddPointClass">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addIPEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="addIPEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="addIPEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="addSIEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="SI Engineering Units is a required field." 
                                        ControlToValidate="ddlAddSIEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddPointClass">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addSIEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="addSIEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="addSIEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                </asp:Panel>                                                                                                                                                       
                            </telerik:RadPageView>                         
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>


                    
                  
