﻿<%@ Page Language="C#" EnableEventValidation="false" EnableSessionState="true" MasterPageFile="~/_masters/LiveDataDashboard.master" AutoEventWireup="false" CodeBehind="LiveDataDashboard.aspx.cs" Inherits="CW.Website.LiveDataDashboard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
		<div id="liveDashboard" class="liveDashboard">	
                 		   
			<div id="dashboardHeader" class="dashboardHeader"> 
                <div class="liveDataModuleIcon">
                </div>
				<!--cw dashboard logo-->
				<div class="dashboardHeader">
					Live Data<br />Dashboard
				</div>
				<div class="headerSpacer"></div>
				<div class="headerClientInfo"> 
					<div class="headerClientImage">     
					<asp:Image ID="imgHeaderClientImage" CssClass="imgHeader" runat="server" />
					</div>          
					<div class="headerClientName">
						<label id="lblHeaderClientName" runat="server"></label>
					</div>
				</div>
				<div class="headerSpacer"></div>
				<div class="headerBuildingInfo">         
					<div class="headerBuildingImage">          
					<asp:Image ID="imgHeaderBuildingImage" CssClass="imgHeader" runat="server" Visible="false" />     
					</div>
					<div class="headerBuildingName">
						<!--<label id="lblHeaderBuildingTypeName" runat="server"></label>-->
						<label id="lblHeaderBuildingName" runat="server"></label>
					</div>
				</div>
				<div class="headerBuildingSelect">
					<label class="labelLargeBold">Select Building:</label>
					<asp:DropDownList CssClass="dropdown" ID="ddlBuilding"  AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
						<asp:ListItem Value="-1">Select one...</asp:ListItem>     
					</asp:DropDownList>
				</div>
			</div>
			<div id="dashboardBody" class="dashboardBody">

					<div class="dashboardBodyLeftColumn">

						<ul class="dashboardNav">
							<li><asp:LinkButton CssClass="lnkBtnDashboardActive" ID="btnHome" runat="server" Text="Home" OnClientClick="" OnClick="homeButton_Click"></asp:LinkButton></li>
							<li><asp:LinkButton CssClass="lnkBtnDashboard" ID="btnLive" runat="server" Text="Live Data" OnClick="liveButton_Click"></asp:LinkButton></li>                                                
						</ul>
						<hr />
						<div class="dashboardSeondarySelections">
							<asp:MultiView ID="mvSecondary" ActiveViewIndex="0" runat="server">
								<asp:View ID="vSecondaryHome" runat="server">
								</asp:View> 
								<asp:View ID="vSecondaryLive" runat="server">
									<div class="divSecondaryForm">
										<label class="labelBold">*Equipment Class:</label>  
										<asp:DropDownList ID="ddlLiveEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlLiveEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
											<asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
										</asp:DropDownList> 
									</div>
                                    <div class="divSecondaryForm">
										<label class="labelBold">*Equipment:</label>  
										<asp:DropDownList ID="ddlLiveEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlLiveEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
											<asp:ListItem Value="-1">Select equipment class first...</asp:ListItem>                                 
										</asp:DropDownList> 
									</div>
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Points:</label>
                                        <extensions:ListBoxExtension ID="lbLivePoints" OnSelectedIndexChanged="lbLivePoints_OnSelectedIndexChanged" runat="server" SelectionMode="Multiple" CssClass="listbox" AutoPostBack="true" />
                                        <asp:HiddenField ID="hdnLivePointsList" runat="server" />
                                    </div>
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Chart Type:</label>
                                        <asp:DropDownList ID="ddlLiveChartType" CssClass="dropdown" runat="server">
                                            <asp:ListItem Value="Line">Line</asp:ListItem>   
                                            <asp:ListItem Value="Point">Point</asp:ListItem>  
                                        </asp:DropDownList>
                                    </div>                                                                        
								</asp:View>                       
							</asp:MultiView>
						</div>
					</div>

				<div class="dashboardBodyRightColumnWithTabs"> 
					<asp:MultiView ID="mvMain" ActiveViewIndex="0" runat="server">
						<asp:View ID="vMainHome" runat="server">
                            							
							<div id="divHomeInner" class="dashboardHomeBodyInner" runat="server">
                            <h1>Live Data Dashboard</h1>                                       
							<div class="richText">
								<asp:Literal ID="litDashboardBody" runat="server"></asp:Literal>                                
							</div>  
							<div>
                                <div id="divSelectedBuilding" class="divSelectedBuilding" runat="server">                                  
                                </div>                            
                                <telerik:RadRotator Enabled="false" Visible="false" ID="rrBuildingScroller" runat="server" 
                                    CssClass="radRotaterBuildingScroller"
                                    Width="736px" Height="380px"
                                    RotatorType="CoverFlowButtons"
                                    ItemHeight="204" ItemWidth="272"
                                    OnItemClick="radRotaterBuildingScroller_OnItemClick"
                                    >                                
                                    <ItemTemplate>
                                        <asp:HiddenField ID="HiddenField1" Value='<%# Eval("bid") %>' runat="server" />
                                        <asp:Image ID="imgBuildingImage" ImageUrl='<%# Eval("src") %>' Visible='<%# Eval("visible") %>' AlternateText='<%# Eval("alt") %>' runat="server" />
                                        <div class="radRotaterTeaser"><%# Eval("alt") %></div>
                                    </ItemTemplate>
                                </telerik:RadRotator>
							</div> 
                            </div>                            
					   </asp:View>
					    <asp:View ID="vMainLive" runat="server">

							<div id="divLiveInner" class="dashboardBodyInner" runat="server">
							
							<!--live chart iframe new-->
							<!--<div id="divLiveChartIFrames" ></div>-->
							
							<div id="divResults" class="dashboardMessage" runat="server">
							    <asp:Label ID="lblResults"  runat="server" Text=""></asp:Label>
							</div>
							

                            <asp:Chart ID="rawChart" runat="server" ImageType="Png" EnableViewState="true" Height="438px" Width="780px" Visible="false">
                                <Titles>
                                    <asp:Title Name="rawTitle" Font="Trebuchet MS, 12pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>
                                </Titles>
                                <Legends>                                    
                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                </Legends>
                                <Series></Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="rawChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                        <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>                                        
                                        <AxisY Title="Engineering Units" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false">
                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                            <MajorGrid LineColor="#CCCCCC" />
                                            <MajorTickMark LineColor="#CCCCCC" />
                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                        </AxisY>
                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                            <MajorGrid LineColor="#CCCCCC" />
                                            <MajorTickMark LineColor="#CCCCCC" />
                                        </AxisX>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>


                           <!--<telerik:RadChart ID="radChart" runat="server" EnableViewState="true" Height="412px" Width="750px" AutoLayout="true" AutoTextWrap="true" Skin="Office2007" Visible="false">
                                <ClientSettings ScrollMode="Both" EnableZoom="true" />
                                <ChartTitle TextBlock-Text="Test" TextBlock-Appearance-TextProperties-Color="Black"></ChartTitle>
                                <Legend Appearance-Position-AlignedPosition="Center" Appearance-Overflow="Row" Visible="true">
                                    <Appearance Position-Auto="true" ItemAppearance-Visible="true"></Appearance>
                                </Legend>
                                <PlotArea>
                                    <XAxis AxisLabel-TextBlock-Text="Date and Time" Visible="true" AxisLabel-Visible="true">
                                    <Appearance MajorGridLines-Visible="true" MajorGridLines-Color="LightGray"></Appearance>
                                        <AxisLabel Visible="true" TextBlock-Visible="true"></AxisLabel>
                                    </XAxis>
                                    <YAxis AxisLabel-TextBlock-Text="Engineering Units" Visible="true" AxisLabel-Visible="true">
                                    <Appearance MajorGridLines-Visible="true" MajorGridLines-Color="LightGray"></Appearance>
                                        <AxisLabel Visible="true" TextBlock-Visible="true"></AxisLabel>
                                    </YAxis>
                                    <Appearance>
                                        <FillStyle MainColor="White" SecondColor="White"></FillStyle>
                                    </Appearance>
                                </PlotArea>
                                <Series></Series>
                                <Appearance ImageQuality="AntiAlias"></Appearance>
                            </telerik:RadChart>-->

							</div>

                            <!--Chart is required in comments, microsoft charting bug.-->
                            <!--<asp:chart id="Chart2" runat="server" Height="1px" Width="1px"></asp:chart>-->

					   </asp:View>                                                      
				   </asp:MultiView>
			   </div> 
			</div>

            <!--TODO: RadDock for each widget-->
            <!--TODO: RadScheduler for the calendar based chart widget-->
			<!--dashboard tabs-->
            <div id="dashboardBottomTabs" class="dashboardBottomTabs">

                <telerik:RadTabStrip ID="tabStrip" runat="server" MultiPageID="radMultiPage" Visible="false" CssClass="tabStrip" >
                    <Tabs>
                        <telerik:RadTab Text="Air Handlers" Visible="false">
                        </telerik:RadTab>
                        <telerik:RadTab Text="Boilers" Visible="false">
                        </telerik:RadTab>
                        <telerik:RadTab Text="Chillers" Visible="false">
                        </telerik:RadTab>
                        <telerik:RadTab Text="Fans" Visible="false">
                        </telerik:RadTab>
                        <telerik:RadTab Text="Heat Rejections" Visible="false">
                        </telerik:RadTab>
                        <telerik:RadTab Text="Pumps" Visible="false">
                        </telerik:RadTab>
                    </Tabs>                
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" Width="100%">
                <telerik:RadPageView ID="RadPageView1" runat="server">
                        <div class="dashboardBottomBodyLeftColumn">

									<div id="widgetAirHandlerTemperatures" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Air Handler Outdoor and Supply Air Temperatures</div>
										<div class="widgetBodyNoInnerPadding">
												
                                            <%--GOOGLE CHARTS ATTEMPT
                                            <script type="text/javascript">
                                                google.load("visualization", "1", { packages: ["corechart"] });
                                                google.setOnLoadCallback(drawChart);
                                                function drawChart() {
                                                    var data = <%= airHandlerTempChartString %>;

                                                    var options = {
                                                        title: 'Air Handler Temperatures',
                                                    };

                                                    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                                                    chart.draw(data, options);
                                                }
                                            </script>                                                                                                										

                                            <div id="chart_div" style="width: 100%; height: 100%;"></div>--%>


                                            <asp:Chart ID="chartAirHandlerTemperatures" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>            
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends> 
                                                <Series></Series>
                                                <ChartAreas >
                                                    <asp:ChartArea Name="airHandlerTemperaturesChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                   
                                                        <AxisY Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                            
										</div>
									</div>

                                    <div id="widgetAirHandlerSupplyFanStatus"  class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Air Handler Supply Fan Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartAirHandlerSupplyFanStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="airHandlerSupplyFanStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                            
                                        </div>
									</div>

								</div>
								<div class="dashboardBottomBodyRightColumn"> 
										
								</div> 
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageView2" runat="server">
                                <div class="dashboardBottomBodyLeftColumn">
										
                                    <div id="widgetBoilerHWStatus"  class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Boiler Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartBoilerHWStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="boilerHWStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
									</div>

								</div>
                                
								<div class="dashboardBottomBodyRightColumn"> 

								</div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageView3" runat="server">
                                <div class="dashboardBottomBodyLeftColumn">

                                    <div id="widgetChillerStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Chiller Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartChillerStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="chillerStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

                                    <div id="widgetChillerPower"  class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Chiller Power</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartChillerPower" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="chillerPowerChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                                                                                
                                                        <AxisY Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
									</div>										

								</div>
								<div class="dashboardBottomBodyRightColumn"> 

								</div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageView4" runat="server">

                                <div class="dashboardBottomBodyLeftColumn">

                                    <div id="widgetFanSupplyStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Fan Supply Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartFanSupplyStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="fanSupplyStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

                                    <div id="widgetFanReturnStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Fan Return Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartFanReturnStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="fanReturnStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                      
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

                                    <div id="widgetFanExhaustStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Fan Exhaust Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartFanExhaustStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="fanExhaustStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

								</div>
								
                                <div class="dashboardBottomBodyRightColumn"> 

								</div>

                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageView5" runat="server">

                                <div class="dashboardBottomBodyLeftColumn">

                                    <div id="widgetHeatRejectionCoolingTowerEnable" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Cooling Tower Enable</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartHeatRejectionCoolingTowerEnable" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="heatRejectionCoolingTowerEnableChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Enable" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

                                    <div id="widgetHeatRejectionCoolingTowerFanStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Cooling Tower Fan Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartHeatRejectionCoolingTowerFanStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="heatRejectionCoolingTowerFanStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

                                    <div id="widgetHeatRejectionCoolingTowerSprayStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Cooling Tower Spray Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartHeatRejectionCoolingTowerSprayStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="heatRejectionCoolingTowerSprayStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

				                    <div id="widgetHeatRejectionCoolingTowerSumpTemp" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Cooling Tower Sump Temperatures</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartHeatRejectionCoolingTowerSumpTemp" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="heatRejectionCoolingTowerSumpTempChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

								</div>

								<div class="dashboardBottomBodyRightColumn"> 

								</div>

                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageView6" runat="server">

                                <div class="dashboardBottomBodyLeftColumn">

                                    <div id="widgetPumpCHWLoopStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Chilled Water Pump Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartPumpCHWLoopStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="pumpCHWLoopStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                       
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

                                    <div id="widgetPumpHWLoopStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Hot Water Pump Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartPumpHWLoopStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="pumpHWLoopStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

                                    <div id="widgetPumpCWLoopStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Condenser Water Pump Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartPumpCWLoopStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="pumpCWLoopStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                     
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

				                    <div id="widgetPumpDHWLoopStatus" class="widgetBoxWidest" runat="server" visible="false">
										<div class="widgetTopWidest">Domestic Hot Water Pump Statuses</div>
										<div class="widgetBodyNoInnerPadding">
    
                                                <asp:Chart ID="chartPumpDHWLoopStatus" runat="server" ImageType="Png" EnableViewState="true" Height="400px" Width="926px">
                                                <Legends>
                                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                                </Legends>
                                                <Series></Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="pumpDHWLoopStatusChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                        
                                                        <AxisY Title="Status" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false" Maximum="1.05" Minimum="0" Interval="1">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                                        </AxisY>
                                                        <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                            <MajorGrid LineColor="#CCCCCC" />
                                                            <MajorTickMark LineColor="#CCCCCC" />
                                                        </AxisX>
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>

                                        </div>
                                    </div>

								</div>
								
                                <div class="dashboardBottomBodyRightColumn"> 

								</div>

                 </telerik:RadPageView>
                 </telerik:RadMultiPage>

            </div>
            
			<div id="dashboardFooter" class="dashboardFooter">             
			</div>

        </div>					
</asp:Content>
