﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="True" CodeBehind="KGSClientAdministration.aspx.cs" Inherits="CW.Website.KGSClientAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
    
        <div id="intro" class="pod">
        	<div class="top"> </div>
            <div id="middle" class="middle">                    	                  	
            	<h1>KGS Client Administration</h1>                        
                <div class="richText">The kgs client administration area is used to view, add, and edit clients.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                Loading...
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
               <div class="administrationControls">
                        <ajaxToolkit:TabContainer ID="TabContainer1"                  
                            runat="server" 
                            OnLoad="loadTabs" 
                            OnActiveTabChanged="onActiveTabChanged"
                            AutoPostBack="true"                            
                            Width="100%">
                            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" 
                                HeaderText="View Clients">
                                <ContentTemplate>
                                   
                                    <p>
                                        <a id="lnkSetFocusView" runat="server"></a>
                                        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>   
                                    <div class="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </div>         
                                                                                                                
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridClients"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="CID"  
                                             GridLines="None"                                      
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridClients_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridClients_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridClients_Sorting"   
                                             OnSelectedIndexChanged="gridClients_OnSelectedIndexChanged"                                                                                                             
                                             OnRowDeleting="gridClients_Deleting"
                                             OnRowEditing="gridClients_Editing"
                                             OnDataBound="gridClients_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="False" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client Name">  
                                                    <ItemTemplate><%# Eval("ClientName").ToString().Length <= 25 ? Eval("ClientName") : Eval("ClientName").ToString().Remove(25, (Eval("ClientName").ToString().Length - 25)) + "..."%></ItemTemplate>                                                                                                                                       
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientAddress" HeaderText="Address">  
                                                    <ItemTemplate><%# Eval("ClientAddress").ToString().Length <= 25 ? Eval("ClientAddress") : Eval("ClientAddress").ToString().Remove(25, (Eval("ClientAddress").ToString().Length - 25)) + "..."%></ItemTemplate>                                    
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientCity" HeaderText="City">  
                                                    <ItemTemplate><%# Eval("ClientCity").ToString().Length <= 25 ? Eval("ClientCity") : Eval("ClientCity").ToString().Remove(25, (Eval("ClientCity").ToString().Length - 25)) + "..."%></ItemTemplate>                           
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientStateID" HeaderText="State">  
                                                    <ItemTemplate><%# Eval("StateName")%></ItemTemplate>                           
                                                </asp:TemplateField> 
                                                <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="False"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this client permanently?\n\nAlternatively you can deactivate a client temporarily instead.');"
                                                               CommandName="Delete">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                                                          
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT CLIENT DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvClient" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Client Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Name: </strong>" + Eval("ClientName") + "</li>"%>
                                                            <%# "<li><strong>Address: </strong>" + Eval("ClientAddress") + "</li>"%> 
                                                            <%# "<li><strong>City: </strong>" + Eval("ClientCity") + "</li>"%> 
                                                            <%# "<li><strong>State: </strong>" + Eval("StateName") + "</li>"%> 
                                                            <%# "<li><strong>Zip: </strong>" + Eval("ClientZip") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ClientPhone"))) ? "" : "<li><strong>Phone: </strong>" + Eval("ClientPhone") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ClientFax"))) ? "" : "<li><strong>Fax: </strong>" + Eval("ClientFax") + "</li>"%>            
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PrimaryContact"))) ? "" : "<li><strong>Primary Contact: </strong>" + Eval("PrimaryContact") + "</li>"%>   
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PrimaryContactEmail"))) ? "" : "<li><strong>Primary Contact Email: </strong><a href=\"mailto:" + Eval("PrimaryContactEmail") + "\">" + Eval("PrimaryContactEmail") + "</a></li>"%>  
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PrimaryContactPhone"))) ? "" : "<li><strong>Primary Contact Phone: </strong>" + Eval("PrimaryContactPhone") + "</li>"%>   
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("SecondaryContact"))) ? "" : "<li><strong>Secondary Contact: </strong>" + Eval("SecondaryContact") + "</li>"%>   
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("SecondaryContactEmail"))) ? "" : "<li><strong>Secondary Contact Email: </strong><a href=\"mailto:" + Eval("SecondaryContactEmail") + "\">" + Eval("SecondaryContactEmail") + "</a></li>"%>  
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("SecondaryContactPhone"))) ? "" : "<li><strong>Secondary Contact Phone: </strong>" + Eval("SecondaryContactPhone") + "</li>"%>   
                                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>                                             	                                                                	                                                                
                                                         </ul>
                                                         <h2>Client Content Details</h2>                                                                                                                                            
                                                         <ul class="detailsList">
                                                            <%# "<li><strong>Home Page Header: </strong>" + Eval("HomePageHeader") + "</li>"%> 
                                                            <%# "<li><strong>Home Page Body: </strong>" + Eval("HomePageBody") + "</li>"%> 
                                                            <%# "<li><strong>News Body: </strong>" + Eval("NewsBody") + "</li>"%> 
                                                         </ul>
                                                         <h2>Client Account Details</h2>                                                                                                                                            
                                                         <ul class="detailsListWide">
                                                            <%# "<li><strong>Max Monthly Repository Downloads: </strong>" + Eval("MaxMonthlyRepositoryDownloads") + "</li>"%> 
                                                            <%# "<li><strong>Current Monthly Repository Downloads: </strong>" + Eval("CurrentMonthlyRepositoryDownloads") + "</li>"%> 
                                                            <%# "<li><strong>Max Monthly Repository Uploads: </strong>" + Eval("MaxMonthlyRepositoryUploads") + "</li>"%> 
                                                            <%# "<li><strong>Current Monthly Repository Uploads: </strong>" + Eval("CurrentMonthlyRepositoryUploads") + "</li>"%> 
                                                            <%# "<li><strong>Max Total Repository Storage: </strong>" + Eval("MaxTotalRepositoryStorage") + "</li>"%> 
                                                            <%# "<li><strong>Current Total Repository Storage: </strong>" + Eval("CurrentTotalRepositoryStorage") + "</li>"%> 
                                                            <%# "<li><strong>Max Monthly Report Downloads: </strong>" + Eval("MaxMonthlyReportDownloads") + "</li>"%>
                                                            <%# "<li><strong>Current Monthly Report Downloads: </strong>" + Eval("CurrentMonthlyReportDownloads") + "</li>"%>
                                                            <%# "<li><strong>Max Monthly Report Emails: </strong>" + Eval("MaxMonthlyReportEmails") + "</li>"%>
                                                            <%# "<li><strong>Current Monthly Report Emails: </strong>" + Eval("CurrentMonthlyReportEmails") + "</li>"%>
                                                            <%# "<li><strong>Last Modified Date: </strong>" + Eval("SettingsDateModified") + "</li>"%>
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>                                     
                                                                     
                                    <!--EDIT CLIENT PANEL -->                              
                                    <asp:Panel ID="pnlEditClients" runat="server" Visible="false">                                                                                             
                                              <div>
                                                    <h2>Edit Client</h2>
                                              </div>  
                                              <div>                                                    
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*Name:</label>
                                                        <asp:TextBox ID="txtEditClientName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Address:</label>
                                                        <asp:TextBox ID="txtEditAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                                    </div>                                                                                                                                                                                           
                                                    <div class="divForm">
                                                         <label class="label">*City:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditCity" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">*State:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlEditState" AppendDataBoundItems="true" runat="server" >                                                                                          
                                                         </asp:DropDownList>
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">*Zip Code:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditZip" runat="server" MaxLength="5" ></asp:TextBox>   
                                                    </div>                                                    
                                                    <div class="divForm">
                                                         <label class="label">Phone:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditPhone" runat="server" MaxLength="15" ></asp:TextBox>   
                                                    </div>  
                                                    <div class="divForm">
                                                         <label class="label">Fax:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditFax" runat="server" MaxLength="15" ></asp:TextBox>   
                                                    </div>  
                                                    <div class="divForm">
                                                        <label class="label">Primary Contact:</label>
                                                        <asp:TextBox ID="txtEditPrimaryContact" CssClass="textbox" MaxLength="15" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Primary Contact Email:</label>
                                                        <asp:TextBox ID="txtEditPrimaryEmail" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Primary Contact Phone:</label>
                                                        <asp:TextBox ID="txtEditPrimaryPhone" CssClass="textbox" MaxLength="15" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Secondary Contact:</label>
                                                        <asp:TextBox ID="txtEditSecondaryContact" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Secondary Contact Email:</label>
                                                        <asp:TextBox ID="txtEditSecondaryEmail" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Secondary Contact Phone:</label>
                                                        <asp:TextBox ID="txtEditSecondaryPhone" CssClass="textbox" MaxLength="15" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Active:</label>
                                                        <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />   
                                                        <p>(Warning: Setting a client as inactive will not allow scheduled analyses to be put in the queue, and no incoming data will be recorded.)</p>                                                        
                                                    </div>                                             
                                                    <asp:LinkButton CssClass="lnk-button" ID="btnUpdateClient" runat="server" Text="Update Client"  OnClick="updateClientButton_Click" ValidationGroup="EditClient"></asp:LinkButton>                                                                                   
                                                </div>
                                                
                                                <!--Ajax Validators-->      
                                                <asp:RequiredFieldValidator ID="editClientNameRequiredValidator" runat="server"
                                                    ErrorMessage="Name is a required field."
                                                    ControlToValidate="txtEditClientName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditClient">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editClientNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editClientNameRequiredValidatorExtender"
                                                    TargetControlID="editClientNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>   
                                                <asp:RequiredFieldValidator ID="editAddressRequiredValidator" runat="server"
                                                    ErrorMessage="Address is a required field."
                                                    ControlToValidate="txtEditAddress"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditClient">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editAddressRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editAddressRequiredValidatorExtender"
                                                    TargetControlID="editAddressRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>      
                                                <asp:RequiredFieldValidator ID="editCityRequiredValidator" runat="server"
                                                    ErrorMessage="City is a required field."
                                                    ControlToValidate="txtEditCity"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditClient">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editCityRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editCityRequiredValidatorExtender"
                                                    TargetControlID="editCityRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>      
                                                <asp:RequiredFieldValidator ID="editStateRequiredValidator" runat="server"
                                                    ErrorMessage="State is a required field." 
                                                    ControlToValidate="ddlEditState"  
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    InitialValue="-1"
                                                    ValidationGroup="EditClient">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editStateRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editStateRequiredValidatorExtender" 
                                                    TargetControlID="editStateRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                          
                                                <ajaxToolkit:MaskedEditExtender ID="editZipMaskedEditExtender" runat="server"
                                                        TargetControlID="txtEditZip" 
                                                        Mask="99999"
                                                        AutoComplete="true"                                
                                                        MaskType="Number" 
                                                        InputDirection="LeftToRight"                                
                                                        />     
                                                <asp:RequiredFieldValidator ID="editZipRequiredValidator" runat="server"
                                                    ErrorMessage="Zip Code is a required field."
                                                    ControlToValidate="txtEditZip"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditClient">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editZipRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editZipRequiredValidatorExtender"
                                                    TargetControlID="editZipRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                   
                                                <ajaxToolkit:MaskedEditExtender ID="editPhoneMaskedEditExtender" runat="server"
                                                        TargetControlID="txtEditPhone" 
                                                        Mask="(999)999-9999"
                                                        AutoComplete="true"                                
                                                        MaskType="Number" 
                                                        InputDirection="LeftToRight"                                 
                                                        />  
                                                <ajaxToolkit:MaskedEditExtender ID="editFaxMaskedEditExtender" runat="server"
                                                        TargetControlID="txtEditFax" 
                                                        Mask="(999)999-9999"
                                                        AutoComplete="true"                                
                                                        MaskType="Number" 
                                                        InputDirection="LeftToRight"                                 
                                                        />     
                                                <asp:RegularExpressionValidator ID="editPrimaryEmailRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Email Format."
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ControlToValidate="txtEditPrimaryEmail"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="EditClient">
                                                    </asp:RegularExpressionValidator>   
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editPrimaryEmailRegExValidatorExtender" runat="server"
                                                    BehaviorID="editPrimaryEmailRegExValidatorExtender" 
                                                    TargetControlID="editPrimaryEmailRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>   
                                                <ajaxToolkit:MaskedEditExtender ID="editPrimaryPhoneMaskedEditExtender" runat="server"
                                                        TargetControlID="txtEditPrimaryPhone" 
                                                        Mask="(999)999-9999"
                                                        AutoComplete="true"                                
                                                        MaskType="Number" 
                                                        InputDirection="LeftToRight"                                 
                                                        />                                                                                                         
                                                <asp:RegularExpressionValidator ID="editSecondaryEmailRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Email Format."
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ControlToValidate="txtEditSecondaryEmail"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="EditClient">
                                                    </asp:RegularExpressionValidator>  
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editSecondaryEmailRegExValidatorExtender" runat="server"
                                                    BehaviorID="editSecondaryEmailRegExValidatorExtender" 
                                                    TargetControlID="editSecondaryEmailRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">   
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                 
                                                <ajaxToolkit:MaskedEditExtender ID="editSecondaryPhoneMaskedEditExtender" runat="server"
                                                        TargetControlID="txtEditSecondaryPhone" 
                                                        Mask="(999)999-9999"
                                                        AutoComplete="true"                                
                                                        MaskType="Number" 
                                                        InputDirection="LeftToRight"                                 
                                                        />                                                                                                                                           
                                    </asp:Panel>
                                   
                                                                      
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" 
                                HeaderText="Add Client">
                                <ContentTemplate>  
                                    <h2>Add Client</h2> 
                                    <div>   
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                                 
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        <div class="divForm">
                                            <label class="label">*Name:</label>
                                            <asp:TextBox ID="txtAddClientName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Address:</label>
                                            <asp:TextBox ID="txtAddAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                        </div>                                                                                                                                                                                           
                                        <div class="divForm">
                                             <label class="label">*City:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddCity" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>
                                        <div class="divForm">
                                             <label class="label">*State:</label>
                                             <asp:DropDownList CssClass="dropdown" ID="ddlAddState" AppendDataBoundItems="true" runat="server" >                                                                                          
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>   
                                             </asp:DropDownList>
                                        </div>
                                        <div class="divForm">
                                             <label class="label">*Zip Code:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddZip" runat="server" MaxLength="5" ></asp:TextBox>   
                                        </div>                                                    
                                        <div class="divForm">
                                             <label class="label">Phone:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddPhone" runat="server" MaxLength="15" ></asp:TextBox>   
                                        </div>  
                                        <div class="divForm">
                                             <label class="label">Fax:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddFax" runat="server" MaxLength="15" ></asp:TextBox>   
                                        </div>  
                                        <div class="divForm">
                                            <label class="label">Primary Contact:</label>
                                            <asp:TextBox ID="txtAddPrimaryContact" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Primary Contact Email:</label>
                                            <asp:TextBox ID="txtAddPrimaryEmail" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Primary Contact Phone:</label>
                                            <asp:TextBox ID="txtAddPrimaryPhone" CssClass="textbox" MaxLength="15" runat="server"></asp:TextBox>                                                    
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Secondary Contact:</label>
                                            <asp:TextBox ID="txtAddSecondaryContact" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Secondary Contact Email:</label>
                                            <asp:TextBox ID="txtAddSecondaryEmail" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Secondary Contact Phone:</label>
                                            <asp:TextBox ID="txtAddSecondaryPhone" CssClass="textbox" MaxLength="15" runat="server"></asp:TextBox>                                                    
                                        </div>
                                                                                              
                                        <asp:LinkButton CssClass="lnk-button" ID="btnAddClient" runat="server" Text="Add Client"  OnClick="addClientButton_Click" ValidationGroup="AddClient"></asp:LinkButton>                                                                                   
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addClientNameRequiredValidator" runat="server"
                                        ErrorMessage="Name is a required field."
                                        ControlToValidate="txtAddClientName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddClient">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addClientNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addClientNameRequiredValidatorExtender"
                                        TargetControlID="addClientNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                    <asp:RequiredFieldValidator ID="addAddressRequiredValidator" runat="server"
                                        ErrorMessage="Address is a required field."
                                        ControlToValidate="txtAddAddress"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddClient">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addAddressRequiredValidatorExtender" runat="server"
                                        BehaviorID="addAddressRequiredValidatorExtender"
                                        TargetControlID="addAddressRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>      
                                    <asp:RequiredFieldValidator ID="addCityRequiredValidator" runat="server"
                                        ErrorMessage="City is a required field."
                                        ControlToValidate="txtAddCity"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddClient">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addCityRequiredValidatorExtender" runat="server"
                                        BehaviorID="addCityRequiredValidatorExtender"
                                        TargetControlID="addCityRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>      
                                    <asp:RequiredFieldValidator ID="addStateRequiredValidator" runat="server"
                                        ErrorMessage="State is a required field." 
                                        ControlToValidate="ddlAddState"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddClient">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addStateRequiredValidatorExtender" runat="server"
                                        BehaviorID="addStateRequiredValidatorExtender" 
                                        TargetControlID="addStateRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                          
                                    <ajaxToolkit:MaskedEditExtender ID="addZipMaskedEditExtender" runat="server"
                                            TargetControlID="txtAddZip" 
                                            Mask="99999"
                                            AutoComplete="true"                                
                                            MaskType="Number" 
                                            InputDirection="LeftToRight"                                
                                            />     
                                    <asp:RequiredFieldValidator ID="addZipRequiredValidator" runat="server"
                                        ErrorMessage="Zip Code is a required field."
                                        ControlToValidate="txtAddZip"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddClient">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addZipRequiredValidatorExtender" runat="server"
                                        BehaviorID="addZipRequiredValidatorExtender"
                                        TargetControlID="addZipRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                   
                                    <ajaxToolkit:MaskedEditExtender ID="addPhoneMaskedEditExtender" runat="server"
                                            TargetControlID="txtAddPhone" 
                                            Mask="(999)999-9999"
                                            AutoComplete="true"                                
                                            MaskType="Number" 
                                            InputDirection="LeftToRight"                                 
                                            />  
                                    <ajaxToolkit:MaskedEditExtender ID="addFaxMaskedEditExtender" runat="server"
                                            TargetControlID="txtAddFax" 
                                            Mask="(999)999-9999"
                                            AutoComplete="true"                                
                                            MaskType="Number" 
                                            InputDirection="LeftToRight"                                 
                                            />     
                                    <asp:RegularExpressionValidator ID="addPrimaryEmailRegExValidator" runat="server"
                                        ErrorMessage="Invalid Email Format."
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="txtAddPrimaryEmail"
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        ValidationGroup="AddClient">
                                        </asp:RegularExpressionValidator> 
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addPrimaryEmailRegExValidatorExtender" runat="server"
                                            BehaviorID="addPrimaryEmailRegExValidatorExtender" 
                                            TargetControlID="addPrimaryEmailRegExValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                         
                                    <ajaxToolkit:MaskedEditExtender ID="addPrimaryPhoneMaskedEditExtender" runat="server"
                                            TargetControlID="txtAddPrimaryPhone" 
                                            Mask="(999)999-9999"
                                            AutoComplete="true"                                
                                            MaskType="Number" 
                                            InputDirection="LeftToRight"                                 
                                            />                                                                                    
                                    <asp:RegularExpressionValidator ID="addSecondaryEmailRegExValidator" runat="server"
                                        ErrorMessage="Invalid Email Format."
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="txtAddSecondaryEmail"
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        ValidationGroup="AddClient">
                                        </asp:RegularExpressionValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addSecondaryEmailRegExValidatorExtender" runat="server"
                                            BehaviorID="addSecondaryEmailRegExValidatorExtender" 
                                            TargetControlID="addSecondaryEmailRegExValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                  
                                    <ajaxToolkit:MaskedEditExtender ID="addSecondaryPhoneMaskedEditExtender" runat="server"
                                            TargetControlID="txtAddSecondaryPhone" 
                                            Mask="(999)999-9999"
                                            AutoComplete="true"                                
                                            MaskType="Number" 
                                            InputDirection="LeftToRight"                                 
                                            />                                                                                                                                                                                                        
                                </ContentTemplate>                  
                            </ajaxToolkit:TabPanel>                                                                                       
                            <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" 
                                    HeaderText="Client Modules">
                                    <ContentTemplate>                                      
                                        <h2>Modules</h2>
                                        <p>
                                            Please select a client in order to select and assign modules.
                                        </p>
                                        <div>
                                            <a id="lnkSetFocusModule" runat="server"></a>
                                            <asp:Label ID="lblModulesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Client:</label>    
                                            <asp:DropDownList ID="ddlModulesClients" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlModulesClients_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="modules" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                All modules are avaialable to an analysis. Please select only which apply.
                                            </p>
                                            <p>
                                                Note: Most likely the Administration module should always be added at the minimum.
                                            </p>                                            
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbModulesTop" AppendDataBoundItems="false" Height="100" Width="246" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnModulesUp"  OnClick="btnModulesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnModulesDown"  OnClick="btnModulesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbModulesBottom" AppendDataBoundItems="false" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnk-button" ID="btnUpdateModules" runat="server" Text="Reassign" OnClick="updateModulesButton_Click" ValidationGroup="UpdateModules"></asp:LinkButton>    
                                        </div>                                                  
                                    
                                    </ContentTemplate>                  
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanel4" runat="server" 
                                    HeaderText="Client Content Settings">
                                    <ContentTemplate>
                                                                             
                                        <h2>Content Settings</h2>
                                        <p>
                                            Please select a client in order to configure settings.
                                        </p>
                                        <p>
                                            Custom token variables are available for the rich text editors as well. Including the following: 
                                            <ul>
                                                <li>Client = $client</li>
                                                <li>ClientID = $id</li>
                                            </ul>                                                
                                        </p> 
                                        <div>
                                            <a id="lnkSetFocusContentSettings" runat="server"></a>
                                            <asp:Label ID="lblContentSettingsError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Client:</label>    
                                            <asp:DropDownList ID="ddlContentSettingsClients" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlContentSettingsClients_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="contentSettings" visible="false" runat="server">                                            
                                            <hr />                                                
                                            <div class="divFormWidest">
                                                <label class="textareaWideLabel">Home Page Header:</label>
                                                <textarea class="textareaWide" name="txtHomePageHeader" id="txtHomePageHeader" cols="80" onkeyup="limitChars(this, 100, 'divHomePageHeaderCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divHomePageHeaderCharInfo"></div>
                                            </div>
                                            <div class="divFormWidest">
                                                <label>Home Page Body (Max HTML Characters = 5000):</label>                                                                                               
                                                <telerik:RadEditor ID="editorHomePageBody" 
                                                                runat="server" 
                                                                CssClass="editor"                                                  
                                                                Height="475px" 
                                                                Width="662px"
                                                                MaxHtmlLength="5000"
                                                                NewLineMode="Div"
                                                                ToolsWidth="664px"                                                                                                                                       
                                                                ToolbarMode="ShowOnFocus"        
                                                                ToolsFile="~/_assets/xml/CustomRadEditorTools.xml"                                                 
                                                                > 
                                                                <CssFiles>
                                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                                </CssFiles>                                                                                  
                                                 </telerik:RadEditor>
                                                <div class="textareaWideCharacterDiv" id="divEditorHomePageBody"></div>
                                            </div>
                                            <div class="divFormWidest">
                                                <label>News Body (Max HTML Characters = 5000):</label>                                                     
                                                <telerik:RadEditor ID="editorNewsBody" 
                                                                runat="server" 
                                                                CssClass="editor"                                                  
                                                                Height="475px" 
                                                                Width="662px"
                                                                MaxHtmlLength="5000"
                                                                NewLineMode="Div"
                                                                ToolsWidth="664px"                                                                                                                                       
                                                                ToolbarMode="ShowOnFocus"        
                                                                ToolsFile="~/_assets/xml/CustomRadEditorTools.xml"                                                 
                                                                >  
                                                                <CssFiles>
                                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                                </CssFiles>                                                                                 
                                                 </telerik:RadEditor>
                                                <div class="textareaWideCharacterDiv" id="divEditorNewsBody"></div>
                                            </div>
                                            <asp:LinkButton CssClass="lnk-button" ID="btnUpdateContentSettings" runat="server" Text="Update" OnClick="updateContentSettingsButton_Click" ValidationGroup="UpdateContentSettings"></asp:LinkButton>
                                        </div>                                          
                                    </ContentTemplate>                  
                            </ajaxToolkit:TabPanel>     
                            <ajaxToolkit:TabPanel ID="TabPanel5" runat="server" 
                                    HeaderText="Client Account Settings">
                                    <ContentTemplate>

                                        <h2>Account Settings</h2>
                                        <p>
                                            Please select a client in order to configure settings.
                                        </p>
                                        <div>
                                            <a id="lnkSetFocusAccountSettings" runat="server"></a>
                                            <asp:Label ID="lblAccountSettingsError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Client:</label>    
                                            <asp:DropDownList ID="ddlAccountSettingsClients" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAccountSettingsClients_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="accountSettings" visible="false" runat="server">                                            
                                            <hr /> 
                                            <h2>Reports</h2>                                          
                                            <div class="divForm">   
                                                <label class="label">Max Monthly Report Downloads (Standard = 50 Count):</label>                                                                                                                   
                                                <ajaxToolkit:SliderExtender
                                                    ID="settingsMaxMonthlyReportDownloadsSliderExtender"
                                                    runat="server"
                                                    BoundControlID="sliderSettingsMaxMonthlyReportDownloads_display"                                                       
                                                    Maximum="500"
                                                    Minimum="0"   
                                                    Length="292" 
                                                    Steps="500"                       
                                                    TargetControlID="sliderSettingsMaxMonthlyReportDownloads"
                                                    EnableHandleAnimation="true"
                                                    TooltipText="{0}">
                                                </ajaxToolkit:SliderExtender>                                                       
                                                <asp:TextBox ID="sliderSettingsMaxMonthlyReportDownloads" runat="server"></asp:TextBox>
                                                <asp:TextBox CssClass="sliderDisplayWide" ID="sliderSettingsMaxMonthlyReportDownloads_display" runat="server"></asp:TextBox>
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">Current Monthly Report Downloads (Count):</label>    
                                                <asp:Label ID="lblSettingsCurrentMonthlyReportDownloads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Current Total Report Downloads (Count):</label>    
                                                <asp:Label ID="lblSettingsCurrentTotalReportDownloads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Max Monthly Report Emails (Standard = 50 Count):</label>                                                                                                                   
                                                <ajaxToolkit:SliderExtender
                                                    ID="settingsMaxMonthlyReportEmailsSliderExtender"
                                                    runat="server"
                                                    BoundControlID="sliderSettingsMaxMonthlyReportEmails_display"                                                       
                                                    Maximum="500"
                                                    Minimum="0"   
                                                    Length="292" 
                                                    Steps="500"                       
                                                    TargetControlID="sliderSettingsMaxMonthlyReportEmails"
                                                    EnableHandleAnimation="true"
                                                    TooltipText="{0}">
                                                </ajaxToolkit:SliderExtender>                                                       
                                                <asp:TextBox ID="sliderSettingsMaxMonthlyReportEmails" runat="server"></asp:TextBox>
                                                <asp:TextBox CssClass="sliderDisplayWide" ID="sliderSettingsMaxMonthlyReportEmails_display" runat="server"></asp:TextBox>
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">Current Monthly Report Emails (Count):</label>    
                                                <asp:Label ID="lblSettingsCurrentMonthlyReportEmails" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Current Total Report Emails (Count):</label>    
                                                <asp:Label ID="lblSettingsCurrentTotalReportEmails" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <hr />
                                            <h2>Repository</h2>
                                            <div class="divForm">   
                                                <label class="label">Max Monthly Repository Downloads (Standard = 100 MB):</label>                                                                                                                   
                                                <ajaxToolkit:SliderExtender
                                                    ID="settingsMaxMonthlyRepositoryDownloadsSliderExtender"
                                                    runat="server"
                                                    BoundControlID="sliderSettingsMaxMonthlyRepositoryDownloads_display"                                                       
                                                    Maximum="500"
                                                    Minimum="0"   
                                                    Length="292" 
                                                    Steps="500"                       
                                                    TargetControlID="sliderSettingsMaxMonthlyRepositoryDownloads"
                                                    EnableHandleAnimation="true"
                                                    TooltipText="{0}">
                                                </ajaxToolkit:SliderExtender>                                                       
                                                <asp:TextBox ID="sliderSettingsMaxMonthlyRepositoryDownloads" runat="server"></asp:TextBox>
                                                <asp:TextBox CssClass="sliderDisplayWide" ID="sliderSettingsMaxMonthlyRepositoryDownloads_display" runat="server"></asp:TextBox>
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">Current Monthly Repository Downloads (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentMonthlyRepositoryDownloads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Current Total Repository Downloads (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentTotalRepositoryDownloads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Max Monthly Repository Uploads (Standard = 100 MB):</label>                                                                                                                   
                                                <ajaxToolkit:SliderExtender
                                                    ID="settingsMaxMonthlyRepositoryUploadsSliderExtender"
                                                    runat="server"
                                                    BoundControlID="sliderSettingsMaxMonthlyRepositoryUploads_display"                                                       
                                                    Maximum="500"
                                                    Minimum="0"   
                                                    Length="292" 
                                                    Steps="500"                       
                                                    TargetControlID="sliderSettingsMaxMonthlyRepositoryUploads"
                                                    EnableHandleAnimation="true"
                                                    TooltipText="{0}">
                                                </ajaxToolkit:SliderExtender>                                                       
                                                <asp:TextBox ID="sliderSettingsMaxMonthlyRepositoryUploads" runat="server"></asp:TextBox>
                                                <asp:TextBox CssClass="sliderDisplayWide" ID="sliderSettingsMaxMonthlyRepositoryUploads_display" runat="server"></asp:TextBox>
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">Current Monthly Repository Uploads (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentMonthlyRepositoryUploads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Current Total Repository Uploads (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentTotalRepositoryUploads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Max Total Repository Storage (Standard = 1000 MB):</label>                                                                                                                   
                                                <ajaxToolkit:SliderExtender
                                                    ID="settingsMaxTotalRepositoryStorageSliderExtender"
                                                    runat="server"
                                                    BoundControlID="sliderSettingsMaxTotalRepositoryStorage_display"                                                       
                                                    Maximum="5000"
                                                    Minimum="0"   
                                                    Length="292" 
                                                    Steps="5000"                       
                                                    TargetControlID="sliderSettingsMaxTotalRepositoryStorage"
                                                    EnableHandleAnimation="true"
                                                    TooltipText="{0}">
                                                </ajaxToolkit:SliderExtender>                                                       
                                                <asp:TextBox ID="sliderSettingsMaxTotalRepositoryStorage" runat="server"></asp:TextBox>
                                                <asp:TextBox CssClass="sliderDisplayWide" ID="sliderSettingsMaxTotalRepositoryStorage_display" runat="server"></asp:TextBox>
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">Current Total Repository Storage (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentTotalRepositoryStorage" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <hr />
                                            <h2>Raw Data</h2>
                                            <div class="divForm">   
                                                <label class="label">Max Monthly Raw Data Downloads (Standard = 100 MB):</label>                                                                                                                   
                                                <ajaxToolkit:SliderExtender
                                                    ID="settingsMaxMonthlyRawDataDownloadsSliderExtender"
                                                    runat="server"
                                                    BoundControlID="sliderSettingsMaxMonthlyRawDataDownloads_display"                                                       
                                                    Maximum="500"
                                                    Minimum="0"   
                                                    Length="292" 
                                                    Steps="500"                       
                                                    TargetControlID="sliderSettingsMaxMonthlyRawDataDownloads"
                                                    EnableHandleAnimation="true"
                                                    TooltipText="{0}">
                                                </ajaxToolkit:SliderExtender>                                                       
                                                <asp:TextBox ID="sliderSettingsMaxMonthlyRawDataDownloads" runat="server"></asp:TextBox>
                                                <asp:TextBox CssClass="sliderDisplayWide" ID="sliderSettingsMaxMonthlyRawDataDownloads_display" runat="server"></asp:TextBox>
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">Current Monthly Raw Data Downloads (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentMonthlyRawDataDownloads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Current Total Raw Data Downloads (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentTotalRawDataDownloads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <hr />
                                            <h2>Portfolio</h2>
                                            <div class="divForm">   
                                                <label class="label">Max Monthly Portfolio Downloads (Standard = 100 MB):</label>                                                                                                                   
                                                <ajaxToolkit:SliderExtender
                                                    ID="settingsMaxMonthlyPortfolioDownloadsSliderExtender"
                                                    runat="server"
                                                    BoundControlID="sliderSettingsMaxMonthlyPortfolioDownloads_display"                                                       
                                                    Maximum="500"
                                                    Minimum="0"   
                                                    Length="292" 
                                                    Steps="500"                       
                                                    TargetControlID="sliderSettingsMaxMonthlyPortfolioDownloads"
                                                    EnableHandleAnimation="true"
                                                    TooltipText="{0}">
                                                </ajaxToolkit:SliderExtender>                                                       
                                                <asp:TextBox ID="sliderSettingsMaxMonthlyPortfolioDownloads" runat="server"></asp:TextBox>
                                                <asp:TextBox CssClass="sliderDisplayWide" ID="sliderSettingsMaxMonthlyPortfolioDownloads_display" runat="server"></asp:TextBox>
                                            </div> 
                                            <div class="divForm">   
                                                <label class="label">Current Monthly Portfolio Downloads (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentMonthlyPortfolioDownloads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">Current Total Portfolio Downloads (MB):</label>    
                                                <asp:Label ID="lblSettingsCurrentTotalPortfolioDownloads" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                
                                            </div>

                                            <asp:LinkButton CssClass="lnk-button" ID="btnUpdateAccountSettings" runat="server" Text="Update" OnClick="updateAccountSettingsButton_Click" ValidationGroup="UpdateAccountSettings"></asp:LinkButton>
                                        </div>                                        
                                    </ContentTemplate>
                            </ajaxToolkit:TabPanel>                                                                                      
                        </ajaxToolkit:TabContainer>                        
                   </div>                                                                 
             </div>
        </div>                
</asp:Content>


                    
                  
