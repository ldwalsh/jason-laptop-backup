﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.master" EnableViewState="true" AutoEventWireup="true" CodeBehind="ProviderAdministration.aspx.cs" Inherits="CW.Website.ProviderAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                              	                  	        	
    <h1>Provider Administration</h1>                          
    <div class="richText">
        The above provider administration pages are used to view, and administer users, buildings, equipment, points, data sources, analyses, and automated scheduled analyses. 
    </div>                             
    
</asp:Content>                