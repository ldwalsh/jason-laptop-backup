﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StorageAccountSelector.ascx.cs" Inherits="CW.Website._administration.backport.StorageAccountSelector" %>
<div id="Div1" class="divForm" runat="server">   
    <label class="label"><%=LabelPrefix%>Select From Account:</label>    
    <asp:DropDownList ID="ddlFromEnvironment" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />
    <asp:DropDownList ID="ddlFromLevel" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />
</div>
<div id="Div2" class="divForm" runat="server">   
    <label class="label"><%=LabelPrefix%>Select To Account:</label>    
    <asp:DropDownList ID="ddlToEnvironment" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlToEnvironment_SelectedIndexChanged" AutoPostBack="true" />
    <asp:DropDownList ID="ddlToLevel" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />
</div>

<% if(IsRequired) { %>
<asp:RequiredFieldValidator ID="FromAccountLevelValidator" runat="server" ErrorMessage="From Account Level is a required field." ControlToValidate="ddlFromLevel" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="FromAccountLevelValidatorExtender" runat="server" TargetControlID="FromAccountLevelValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="ToAccountLevelValidator" runat="server" ErrorMessage="To Account Level is a required field." ControlToValidate="ddlToLevel" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="ToAccountLevelValidatorExtender" runat="server" TargetControlID="ToAccountLevelValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="FromEnviromentValidator" runat="server" ErrorMessage="To Account is a required field." ControlToValidate="ddlFromEnvironment" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="FromEnviromentValidatorExtender" runat="server" TargetControlID="FromEnviromentValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="ToEnviromentValidator" runat="server" ErrorMessage="From Account is a required field." ControlToValidate="ddlToEnvironment" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="ToEnviromentValidatorExtender" runat="server" TargetControlID="ToEnviromentValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<% } %>


