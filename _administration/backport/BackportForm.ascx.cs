﻿using CW.Common.Constants;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using System;
using System.Linq;
using System.Web.UI.WebControls;
using CW.Data.Helpers;
using CW.Logging;
using System.Reflection;
using CW.Data.Collections;
using System.Collections.Generic;

namespace CW.Website._administration.backport
{
    public partial class BackportForm: FormGenericBase<BackportForm, Backport>
    {
        #region field

        public const string HEADER_ALL_CLIENTS = "All Clients";

        #endregion

        #region method

        #region override

        public override void CreateEntity(Backport obj)
        {
            SaveHandlers.Add(
                PropHelper.G<BackportForm>(f => f.ddlCID),
                delegate(Backport backport)
                {
                    backport.StartDate = txtStartDate.SelectedDate.Value;
                    backport.EndDate = txtEndDate.SelectedDate.Value;
                    backport.CID = Convert.ToInt32(ddlCID.SelectedValue);
                    backport.ClientName = ddlCID.SelectedItem.Text;
                    backport.ToTableName = ddlTableName.SelectedItem.Text;
                    backport.FromTableName = ddlTableName.SelectedItem.Text;
                }
            );

            base.CreateEntity(obj);
        }

        #endregion

        private void Page_FirstInit()
        {
            ListItemCollection viewAll = new ListItemCollection() { };
            viewAll.Add(new ListItem(HEADER_ALL_CLIENTS, "-2"));

            tab.BindClients(ddlCID, viewAll);
            BindBackportTable(ddlTableName);
            dateStartEarlyCompareValidator.ValueToCompare = dateEndEarlyCompareValidator.ValueToCompare = new DateTime(2005, 1, 1).ToShortDateString();
        }

        public Boolean CanBackportBePersisted(Backport backport, out String message)
        {
            if (backport.StartDate > DateTime.UtcNow.AddDays(1) || backport.EndDate > DateTime.UtcNow.AddDays(1))
            {
                message = "Start Date or End Date can not be in the future.";
                return false;
            }

            if (chbClearDestinationTable.Checked && AzureConstants.DeletableTables.Any(b => b == ddlTableName.SelectedValue))
            {
                message = "This table is not deletable, if you intended to delete this table in the destination account, please add to the Deletable Tables in constants file";
                return false;
            }

            message = null;

            return true;
        }

        public void PersistBackport(Backport backport)
        {
            switch (FormMode)
            {
                case PersistModeEnum.Insert:
                    {
                        backport.ToAccountLevel = acctSelector.ToLevel;
                        backport.FromAccountLevel = acctSelector.FromLevel;
                        backport.ToEnvironment = acctSelector.ToEnvironment;
                        backport.FromEnvironment = acctSelector.FromEnvironment;
                        
                        if (backport.CID != 0 && backport.ClientName == HEADER_ALL_CLIENTS)
                        {
                            foreach(var client in DataMgr.ClientDataMapper.GetAllActiveClients())
                            {
                                backport.OID = DataMgr.OrganizationDataMapper.GetOIDByCID(client.CID);
                                backport.CID = client.CID;
                                backport.ClientName = client.ClientName;

                                ScheduleBackportJobs(backport, chbReplace.Checked);
                            }
                        }
                        else
                        {
                            if (backport.CID != 0)
                                backport.OID = DataMgr.OrganizationDataMapper.GetOIDByCID(backport.CID);
    
                            ScheduleBackportJobs(backport, chbReplace.Checked);
                        }

                        break;
                    }
                case PersistModeEnum.Update:
                    {
                        break;
                    }
            }
        }

        private IEnumerable<Backport> ExpandBackports(Backport backport)
        { 
            DateTime currentDate = backport.StartDate;
            DateTime endDate = backport.EndDate;

            var backportList = new List<Backport>();

            while (currentDate < endDate)
            {
                // Copy backport
                Backport expandedRecord = new Backport(backport);
                
                // reset new backport fields
                expandedRecord.StartDate = currentDate;
                expandedRecord.EndDate = currentDate.AddDays(1);
                expandedRecord.Status = "Scheduled";
                expandedRecord.ErrorRecords = 0;
                expandedRecord.ExistingRecords = 0;
                expandedRecord.InsertedRecords = 0;
                expandedRecord.ProcessingTimeInMilliseconds = 0;

                expandedRecord.HasClearToAccountTable = chbClearDestinationTable.Checked;
                expandedRecord.HasTimestampTableName = chbAppendDateRangeToDestinationTable.Checked;

                backportList.Add(expandedRecord);

                currentDate = currentDate.AddDays(1);
            }

            return backportList;
        }

        private void ScheduleBackportJobs(Backport backport, bool replaceOldTasks)
        {
            foreach (var expandedBackport in ExpandBackports(backport))
            {
                if (!chbReplace.Checked && DataMgr.BackportDataMapper.DoesBackportEntityExists(backport))
                    continue;

                // Insert Table Entity
                DataMgr.BackportDataMapper.InsertBackportJobEntity(expandedBackport);

                // Insert Queue Message
                DataMgr.BackportDataMapper.InsertBackportJobQueueMessage(expandedBackport);

                // Remove exisiting backports with this criteria
                RemoveExisting(expandedBackport, replaceOldTasks);
            }
        }

        private void RemoveExisting(Backport backport, bool replaceOldTasks)
        {
            if (replaceOldTasks)
            {
                // exclude current backport schedule but include any backport that as the same StartDate 
                foreach (Backport b in DataMgr.BackportDataMapper.GetBackportJobEntities(backport.StartDate, backport.EndDate, backport.CID)
                    .Where( b=>b.ToTableName == backport.ToTableName && b.FromTableName == backport.FromTableName && b.ToAccountLevel == backport.ToAccountLevel && b.FromAccountLevel == backport.FromAccountLevel && 
                        b.BPID != backport.BPID)) 
                {
                    try
                    {
                        DataMgr.BackportDataMapper.DeleteBackportJobBlob(b.BPID, b.CID, b.JobBegin);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Deletion of Backport Blob failed for backport: " + ObjectHelper.PropsToString(backport), ex);
                    }

                    try
                    {
                        DataMgr.BackportDataMapper.DeleteBackportJobEntity(b.BPID, b.CID, b.StartDate, b.DateCreatedUTC);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Deletion of Backport Entity failed for backport: " + ObjectHelper.PropsToString(backport), ex);
                    }
                }
            }
        }

        #endregion

        #region methods

        public void BindBackportTable(DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataSource = new string[] { "RawData" }; //AzureConstants.Azure_Table_Storage_Tables;
            ddl.DataBind();
        }

        #endregion

        #region property

        public override Delegates<Backport>.CanEntityBePersistedDelegate CanEntityBePersisted
        {
            get { return CanBackportBePersisted; }
        }

        public override Action<Backport> PersistEntityAction
        {
            get { return PersistBackport; }
        }

        #endregion
    }
}
