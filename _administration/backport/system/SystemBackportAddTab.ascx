﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemBackportAddTab.ascx.cs" Inherits="CW.Website._administration.backport.system.SystemBackportAddTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="~/_administration/backport/BackportForm.ascx" tagPrefix="CW" tagName="BackportForm" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Schedule Backport Job" />
<CW:BackportForm runat="server" ID="backportForm" FormMode="Insert" UpdateButtonCaption="Schedule" />