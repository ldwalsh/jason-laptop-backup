<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemBackportViewTab.ascx.cs" Inherits="CW.Website._administration.backport.system.SystemBackportViewTab" %>

<%@ Import Namespace="CW.Data.AzureStorage.Models" %>
<%@ Import Namespace="CW.Utility" %>

<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Backport Jobs" />
<div class="divForm" runat="server">   
    <label class="label">*Select Client:</label>  <span style="float:right"><asp:LinkButton runat="server" ID="lnkBtnRefreshCache" Text="Refresh Cache" AutoPostBack="true" OnClick="lnkBtnRefreshCache_Click"></asp:LinkButton></span>  
    <asp:DropDownList ID="ddlClient" runat="server" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlClient_OnSelectedIndexChanged">                           
    </asp:DropDownList>
</div>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>   

<div id="gridTbl">
    <asp:GridView runat="server" ID="grid" EnableViewState="true" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
        <Columns>
            <asp:CommandField ShowSelectButton="true" />
             <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FromTableName" HeaderText="From Table"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.FromTableName)) %>
                </ItemTemplate> 
            </asp:TemplateField>  
            
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ToTableName" HeaderText="To Table"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.ToTableName)) %>
                </ItemTemplate> 
            </asp:TemplateField>                                                                                                   

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.ClientName)) %>
                </ItemTemplate> 
            </asp:TemplateField>
            
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DateCreatedUTC" HeaderText="Date Created"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.DateCreatedUTC)) %>
                </ItemTemplate> 
            </asp:TemplateField>                                                                                                   

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="StartDate" HeaderText="Start Date"> 
                <ItemTemplate>
                    <%# GetCellContentAsDateTime(Container, PropHelper.G<Backport>(_=>_.StartDate)).ToShortDateString() %>
                </ItemTemplate> 
            </asp:TemplateField>                                                                                                   

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EndDate" HeaderText="End Date"> 
                <ItemTemplate>
                    <%# GetCellContentAsDateTime(Container, PropHelper.G<Backport>(_=>_.EndDate)).ToShortDateString() %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FromEnvironment" HeaderText="From Env"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.FromEnvironment)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FromAccountLevel" HeaderText="From Level"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.FromAccountLevel)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ToEnvironment" HeaderText="To Env"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.ToEnvironment)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ToAccountLevel" HeaderText="To Level"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.ToAccountLevel)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Status" HeaderText="Status"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Backport>(_=>_.Status)) %>
                </ItemTemplate> 
            </asp:TemplateField>



            <asp:TemplateField Visible="true">
                <ItemTemplate>                                                
                    <asp:LinkButton ID="lbnDeleteLinkButton" Runat="server" CausesValidation="false"
						OnClientClick="return confirm('Are you sure you wish to delete this backport job permanently?');"
                        CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="true">
                <ItemTemplate>                                                
                    <asp:LinkButton ID="lbnDownloadLinkButton" Runat="server" CausesValidation="false"
                        CommandName="Download">Download</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="true">
                <ItemTemplate>                                                
                    <asp:LinkButton ID="lbnRescheduleLinkButton" Runat="server" CausesValidation="false"
                        CommandName="Reschedule">Reschedule</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                   
        </Columns>

    </asp:GridView>
</div>

<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
    <Fields>
        <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
            <ItemTemplate>
                <div>
                    <h2>Backport Job Details</h2>                                                                                                                                            
                    <ul class="detailsList">
                        <li>
                            <strong>BPID: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.BPID)) %>
                        </li>
                        <li>
                            <strong>CID: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.CID)) %>
                        </li>
                        <li>
                            <strong>Job Begin: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.JobBegin)) %>
                        </li>
                        <li>
                            <strong>Job Finish: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.JobFinish)) %>
                        </li>
                        <li>
                            <strong>Date Created (UTC): </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.DateCreatedUTC)) %>
                        </li>
                        <li>
                            <strong>Start Date: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.StartDate)) %>
                        </li>
                        <li>
                            <strong>End Date: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.EndDate)) %>
                        </li>
                        <li>
                            <strong>Error Records: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.ErrorRecords)) %>
                        </li>
                        <li>
                            <strong>Existing Records: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.ExistingRecords)) %>
                        </li>
                        <li>
                            <strong>Inserted Records: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.InsertedRecords)) %>
                        </li>
                        <li>
                            <strong>From Account Environment: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.FromEnvironment)) %>
                        </li>
                        <li>
                            <strong>From Account Level: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.FromAccountLevel)) %>
                        </li>
                        <li>
                            <strong>To Account Environment: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.ToEnvironment)) %>
                        </li>
                        <li>
                            <strong>To Account Level: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.ToAccountLevel)) %>
                        </li>
                        <li>
                            <strong>Processing Time (Milliseconds): </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.ProcessingTimeInMilliseconds)) %>
                        </li>
                        <li>
                            <strong>Status: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.Status)) %>
                        </li>
                        <li>
                            <strong>From Table Name:</strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.FromTableName)) %>
                        </li>
                        <li>
                            <strong>To Table Name: </strong>
                            <%# Eval(PropHelper.G<Backport>(b=>b.ToTableName)) %>
                        </li>
                    </ul>
                </div>
            </ItemTemplate>                                             
        </asp:TemplateField>
    </Fields>
</asp:DetailsView>

<!--EDIT PANEL -->                              
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">                                                                                             
    <div>
        <h2>Edit Backport</h2>
    </div>  
    <div>
    </div>
</asp:Panel>
