﻿using CW.Common.Constants;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Utility.Web;

namespace CW.Website._administration.backport.system
{
    public partial class SystemBackportViewTab : ViewTabWithFormBase<Backport, SystemBackportAdministration>, IPostBackEventHandler
    {
        #region method

            #region override

            protected override void PopulateEditForm(Backport obj)
            {
                throw new NotImplementedException();
            }

            protected override void GridRowCreated(Object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ControlHelper.GridViewControl.SetLinkButtonRowIndex("lbnDownloadLinkButton", e);
                    ControlHelper.GridViewControl.SetLinkButtonRowIndex("lbnRescheduleLinkButton", e);
                }

                base.GridRowCreated(sender, e);
            }

            protected override void GridRowCommand(Object sender, GridViewCommandEventArgs e)
            {
                if (e.CommandName == "Download")
                {
                    DownloadObject(grid.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
                    return;
                }

                if (e.CommandName == "Reschedule")
                {
                    RescheduleObject(grid.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
                    return;
                }

                base.GridRowCommand(sender, e);
            }

			#endregion

            private void Page_Configure()
			{
                //CanObjectBeUpdated = clientForm.CanObjectBePersisted;
                
                CanEntityBeDeleted = CanBackportBeDeleted;

                //UpdateObjectMethod = clientForm.PersistObjectAction;
                //CanObjectBeDownload = CanBackportBeDownloaded; 

                DeleteEntityMethod = DeleteBackport;
                //DownloadObjectMethod = DownloadBackport;
                //RescheduleObjectMethod = RescheduleBackport;
			}

            private void Page_FirstInit()
            {
                pnlSearch.Visible = false;

                base.BindClients(ddlClient);
            }

            private void Page_Load()
            {
            }

            protected void DownloadObject(String id)
            {
                string message;

                FilePublishService file = CanBackportBeDownloaded(id, out message);
                if( null != file)
                {
                    file.AttachAsDownloadWithNoDataCheck(new DownloadRequestHandler.HandledFileAttacher());
                }

                DisplayMessage(message);
            }

            protected void RescheduleObject(String id)
            {
                string message = RescheduleBackport(id);

                DisplayMessage(message);
            }

            private FilePublishService CanBackportBeDownloaded(string backportID, out String message)
            {
                message = null;

                FilePublishService file = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    new BackportFileGenerator(DataMgr.BackportDataMapper.GetBackportJobBlob, DataMgr.BackportDataMapper.GetBackportJobEntity, Convert.ToInt32(ddlClient.SelectedValue), backportID),
                    LogMgr
                );
                if (!file.HasDataCheck(new DownloadRequestHandler.HandledFileAttacher()))
                {
                    message = "No data to download.";
                }
                return( string.IsNullOrEmpty(message) ? file : null);
            }

            private Boolean CanBackportBeDeleted(String backportID, out String message)
            {
                message = null;

                return true;
            }

            private string RescheduleBackport(String backportID)
            {
                var backport = DataMgr.BackportDataMapper.GetBackportJobEntity(Convert.ToInt32(ddlClient.SelectedValue), backportID);

                // Delete old table and blob records, let queue expire or get removed when entity is not found
                DataMgr.BackportDataMapper.DeleteBackportJobEntity(backport.BPID, backport.CID, backport.StartDate, backport.DateCreatedUTC);
                DataMgr.BackportDataMapper.DeleteBackportJobBlob(backport.BPID, backport.CID, backport.JobBegin);

                // Copy backport
                Backport insertRecord = new Backport(backport);

                // reset new backport fields
                insertRecord.Status = "Scheduled";
                insertRecord.ErrorRecords = 0;
                insertRecord.ExistingRecords = 0;
                insertRecord.InsertedRecords = 0;
                insertRecord.ProcessingTimeInMilliseconds = 0;

                // Insert Table Entity
                DataMgr.BackportDataMapper.InsertBackportJobEntity(insertRecord);

                // Insert Queue Message
                DataMgr.BackportDataMapper.InsertBackportJobQueueMessage(insertRecord);

                return "Backport Rescheduled";
            }

            private void DeleteBackport(String backportID)
            {
                try
                {
                    var backport = DataMgr.BackportDataMapper.GetBackportJobEntity(Convert.ToInt32(ddlClient.SelectedValue), backportID);

                    // Delete Blob File
                    DataMgr.BackportDataMapper.DeleteBackportJobBlob(backport.BPID, backport.CID, backport.DateCreatedUTC);

                    // Delete Table Entity
                    DataMgr.BackportDataMapper.DeleteBackportJobEntity(backport.BPID, backport.CID, backport.StartDate, backport.DateCreatedUTC);
                }
                catch(Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Failed to delete backport: " + backportID, ex);
                }

                page.TabStateMonitor.ChangeState();
            }

		#endregion

        #region Dropdown Events

            protected void ddlClient_OnSelectedIndexChanged(Object sender, EventArgs e)
            {
                var visible = (ddlClient.SelectedValue != "-1");

                ControlHelper.ToggleVisibility
                (
                    visible,
                    new Control[]
                    {
                        pnlSearch,
                        grid,
                    }
                );

                if (!visible) return;

                BindGrid();
            }

        #endregion

		#region property

			protected override IEnumerable<String> GridColumnNames
			{
				get
				{
					return
                    PropHelper.F<Backport>(_ => _.FromTableName, _ => _.ToTableName, _ => _.ClientName, _ => _.DateCreatedUTC, _ => _.StartDate, _ => _.EndDate, _ => _.ToAccountLevel, _ => _.FromAccountLevel, _ => _.ToEnvironment, _ => _.FromEnvironment, _ => _.Status);
				}
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
                get
                {
                    return (searchText => DataMgr.BackportDataMapper.GetAllSearchedBackportJobs(Convert.ToInt32(ddlClient.SelectedValue), searchText).ToList());
                }
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
                get
                {
                    return (id => DataMgr.BackportDataMapper.GetBackportJobEntity(Convert.ToInt32(ddlClient.SelectedValue), id));                     
                }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
                get
                {
                    return (id => DataMgr.BackportDataMapper.GetBackportJobEntity(Convert.ToInt32(ddlClient.SelectedValue), id));  
                }
			}

            private GetEntitiesForGridDelegate GetBackportJobs()
            {
                return (searchText => DataMgr.BackportDataMapper.GetAllSearchedBackportJobs(Convert.ToInt32(ddlClient.SelectedValue), searchText).ToList());
            }

            protected override String CacheSubKey
            {
                get { return ddlClient.SelectedValue; }
            }

		#endregion

        #region
            //private void BindClients(DropDownList ddl)
            //{
            //    ddl.DataTextField = "ClientName";
            //    ddl.DataValueField = "CID";
            //    ddl.DataSource = mDataManager.ClientDataMapper.GetAllClients();
            //    ddl.DataBind();
            //}
        #endregion

        #region IPostBackEventHandler

            void IPostBackEventHandler.RaisePostBackEvent(String eventArgument) //revise this approach and access the TabStateMonitor instance directly client-side
            {
                switch (eventArgument)
                {
                    case "TabStateMonitor.ChangeState":
                        {
                            this.page.TabStateMonitor.ChangeState();

                            BindGrid(true);

                            break;
                        }
                }
            }

		#endregion

        #region Page Events
            protected void lnkBtnRefreshCache_Click(object sender, EventArgs e)
            {
                page.TabStateMonitor.ChangeState();
            }
        #endregion

    }
}