﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" AutoEventWireup="false" CodeBehind="SystemBackportAdministration.aspx.cs" Inherits="CW.Website._administration.backport.system.SystemBackportAdministration" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="SystemBackportViewTab.ascx" tagPrefix="CW" tagName="SystemBackportViewTab" %>
<%@ Register src="SystemBackportAddTab.ascx" tagPrefix="CW" tagName="SystemBackportAddTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   

            <CW:PageHeader runat="server" Title="Backport Administration"  Description="The backport area is used to schedule backport jobs." />
            <div class="administrationControls">
                <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage"> 
                    <Tabs>
                        <telerik:RadTab Text="View Backport Jobs"></telerik:RadTab>
                        <telerik:RadTab Text="Schedule Backport Job"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <CW:SystemBackportViewTab runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView2" runat="server">
                        <CW:SystemBackportAddTab runat="server" />
                    </telerik:RadPageView>                
                </telerik:RadMultiPage>
            </div>

</asp:Content>