﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Data.AzureStorage.Models;
using CW.Data.AzureStorage;

namespace CW.Website._administration.backport.system
{
    public partial class SystemBackportAdministration : AdministrationPageBase
	{
        private void Page_Init()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check kgs system admin.
            if (!siteUser.IsKGSSystemAdmin)
                Response.Redirect("/Home.aspx");
        }

        public static char IdentifierDelimeter { get { return '|'; } }

		public override String IdentifierField
		{
            get
            {
                return PropHelper.G<Backport>(_ =>_.BPID);
            }
		}

		public override String NameField
		{
            get
            {
                return PropHelper.G<Backport>(_ => _.ClientName);
            }
		}

		public override String EntityTypeName
		{
            get { return "Backport"; }
		}
	}
}
