﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CW.Data.AzureStorage.Models;

namespace CW.Website._administration.backport.system
{
    public partial class SystemBackportAddTab : AddTabBase<Backport, SystemBackportAdministration>
    {
        private void Page_Init()
        {
            CanEntityBeAdded = backportForm.CanEntityBePersisted;
            AddEntityMethod = backportForm.PersistEntityAction;
        }
    }
}