﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BackportForm.ascx.cs" Inherits="CW.Website._administration.backport.BackportForm" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="CW" TagName="AccountSelector" src="~/_administration/backport/StorageAccountSelector.ascx" %>
<asp:HiddenField ID="hdnClientName" runat="server" Visible="false" />

<div class="divForm" runat="server">   
    <label class="label">*Select Client:</label>    
    <asp:DropDownList ID="ddlCID" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />
</div>
<CW:AccountSelector id="acctSelector" IsRequired="true" runat="server" />
<div class="divForm" runat="server">   
    <label class="label">*Select Backport Table:</label>    
    <asp:DropDownList ID="ddlTableName" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />
</div>
<div class="divForm" runat="server">   
    <label class="label">Replace Existing Backports:</label>
    <asp:CheckBox ID="chbReplace" runat="server" CssClass="checkbox"/>
</div>
<div class="divForm" runat="server">   
    <label class="label">Clear Destination Table:</label>
    <asp:CheckBox ID="chbClearDestinationTable" runat="server" CssClass="checkbox"/>
</div>
<div class="divForm" runat="server">   
    <label class="label">Append Date Range To Destination Table:</label>
    <asp:CheckBox ID="chbAppendDateRangeToDestinationTable" runat="server" CssClass="checkbox"/>
</div>
<div class="divForm" runat="server"> 
    <label class="label">Start Date:</label>
    <telerik:RadDatePicker ID="txtStartDate" runat="server"></telerik:RadDatePicker>      
    <br />    
    <asp:CompareValidator ID="dateStartEarlyCompareValidator"  runat="server"
        CssClass="errorMessage" 
        ErrorMessage="Invalid date."
        ControlToValidate="txtStartDate"
        ValueToCompare=""
        Type="Date"
        Operator="GreaterThanEqual"
        ValidationGroup="Backport"
        Display="Dynamic"
        SetFocusOnError="true"
        >
        </asp:CompareValidator>   
</div>
<div class="divForm" runat="server"> 
    <label class="label">End Date:</label>
    <telerik:RadDatePicker ID="txtEndDate" runat="server"></telerik:RadDatePicker>    
    <br />   
    <asp:CompareValidator ID="dateEndEarlyCompareValidator" CssClass="errorMessage"  Display="Dynamic" runat="server" ErrorMessage="Invalid date." ControlToValidate="txtEndDate" ValueToCompare="" Type="Date" Operator="GreaterThanEqual" ValidationGroup="Backport" SetFocusOnError="true" />    
    <asp:CompareValidator ID="dateCompareValidator" CssClass="errorMessage"  Display="Dynamic" runat="server" ErrorMessage="Invalid range." ControlToValidate="txtEndDate" ControlToCompare="txtStartDate" Type="Date" Operator="GreaterThanEqual" ValidationGroup="Backport" SetFocusOnError="true" />       
    <asp:CompareValidator ID="dateSameCompareValidator" CssClass="errorMessage"  Display="Dynamic" runat="server" ErrorMessage="Dates need to be at least on day apart" ControlToValidate="txtEndDate" ControlToCompare="txtStartDate" Type="Date" Operator="NotEqual" ValidationGroup="Backport" SetFocusOnError="true" />
</div>

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<asp:RequiredFieldValidator ID="ClientValidator" runat="server" ErrorMessage="Client is a required field." ControlToValidate="ddlCID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="ClientValidatorExtension" runat="server" TargetControlID="ClientValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="BackportTableValidator" runat="server" ErrorMessage="Backport Table is a required field." ControlToValidate="ddlTableName" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="BackportTableValidatorExtender" runat="server" TargetControlID="BackportTableValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<div style="min-height:120px;"></div>