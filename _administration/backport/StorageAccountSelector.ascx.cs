﻿using CW.Data.Collections;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._administration.backport
{
    public partial class StorageAccountSelector : SiteUserControl
    {
        #region properties

            public String ToLevel           { get { return ddlToLevel.SelectedValue;  } }
            public String FromLevel         { get { return ddlFromLevel.SelectedValue; } }
            public String ToEnvironment     { get { return ddlToEnvironment.SelectedValue; } }
            public String FromEnvironment   { get { return ddlFromEnvironment.SelectedValue; } }

            public String LabelPrefix       { get { return IsRequired ? "*" : ""; } }
            public Boolean IsRequired       { get; set; }

        #endregion

        #region page events
            private void Page_FirstInit()
            {
                BindLevel(ddlToLevel, l => l.Equals(StorageAccountLevel.Primary));
                BindLevel(ddlFromLevel);
                BindEnvironment(ddlFromEnvironment);
                BindEnvironment(ddlToEnvironment);
            }

            protected void ddlToEnvironment_SelectedIndexChanged(object sender, EventArgs e)
            {
                if(ddlToEnvironment.SelectedValue == EnvironmentType.Production.Value)
                    BindLevel(ddlToLevel, l => !l.Equals(StorageAccountLevel.Primary));
            }


        #endregion

        #region private methods

            #region bind 

                private void BindLevel(DropDownList ddl, Func<StorageAccountLevel, bool> predicate = null)
                {
                    var levels = StorageAccountLevel.GetAll<StorageAccountLevel>();

                    if (predicate != null)
                        levels = levels.Where(predicate);

                    BindDDL<StorageAccountLevel>(ddl, levels);
                }

                private void BindEnvironment(DropDownList ddl, Func<EnvironmentType, bool> predicate = null)
                {
                    var envs = EnvironmentType.GetAll<EnvironmentType>();

                    if (predicate != null)
                        envs = envs.Where(predicate);

                    BindDDL<EnvironmentType>(ddl, envs);
                }

                private void BindDDL<T>(DropDownList ddl, IEnumerable<T> items)
                {
                    ListItem lItem = new ListItem();
                    lItem.Text = "Select one...";
                    lItem.Value = "-1";
                    lItem.Selected = true;

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    // Exclude Production from to account
                    ddl.DataSource = items;
                    ddl.DataTextField = "DisplayName";
                    ddl.DataValueField = "Value";
                    ddl.DataBind();
                }
            #endregion


        #endregion

        #region public

        #endregion
    }
}