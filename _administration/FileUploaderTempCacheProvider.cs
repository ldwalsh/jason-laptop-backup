﻿using CW.Business;
using CW.Caching;
using CW.Common.Config;
using CW.Common.Constants;
using CW.Data.Collections;
using CW.Data.Interfaces.State;
using CW.Website.DependencyResolution;
using FileUploaderControl;
using System;

namespace CW.Website._administration
{
	public class FileUploaderTempCacheProvider: FileUploaderControl.ITempFileCache
	{
		public static ICacheStore _cacheStore;

		public void Add(String key, CachedFileViewerHttpHandler.CachedFile value)
		{
			CacheStore.Add<CachedFileViewerHttpHandler.CachedFile>(key, value);
		}

		public CachedFileViewerHttpHandler.CachedFile Get(String key)
		{
			return CacheStore.Get<CachedFileViewerHttpHandler.CachedFile>(key);
		}

		public void Remove(String key)
		{
			CacheStore.Remove<CachedFileViewerHttpHandler.CachedFile>(key);
		}

		private ICacheStore CacheStore
		{
			get
			{
				_cacheStore = (_cacheStore ?? IoC.Resolve<DataManagerCommonStorage>().DefaultCacheRepository);

				return _cacheStore; 
			}
		}
	}
}
