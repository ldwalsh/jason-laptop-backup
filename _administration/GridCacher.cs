﻿using CW.Caching;
using CW.Data;
using CW.Utility.Search.Factories;
using CW.Utility.Search.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace CW.Website._administration
{
    public class GridCacher
	{
		#region CLASS

			[DataContractAttribute()]
			public class GridSet //un-nest from GridCacher class?
			{
				#region STRUCT

					[DataContractAttribute()]
					public struct Row //un-nest from GridSet class?
					{
						[DataMemberAttribute()]
						public readonly IDictionary<string, string> Cells;

						public Row(string id, IDictionary<string, string> cells): this()
						{
							ID	  = id;

							Cells = cells;
						}

						[DataMemberAttribute()]
						public string ID { get; private set; }
					}

				#endregion

				#region STATIC

					static string GetFieldValue(object entity, string propName)
					{
						var current = entity;

						var sa = propName.Split(new[]{'.'}, StringSplitOptions.RemoveEmptyEntries);

						for (var i=0; i<sa.Length; i++)
						{
							current = ((dynamic)GetMember(current.GetType(), sa[i])).GetValue(current);

                            if (current == null) return string.Empty;

                            if (i < sa.Length-1) continue;

                            return ((current is DateTime) ? ((DateTime)current).ToString(CultureInfo.InvariantCulture) : current.ToString());
						}

						throw new Exception();
					}

					static Type GetNestedMemberType(Type type, string fullName)
					{
						var sa = fullName.Split(new[]{'.'}, StringSplitOptions.RemoveEmptyEntries);

						foreach (var n in sa)
						{
                            var t = GetMemberType(GetMember(type, n));
                            
                            if (sa.Last() == n) return (Nullable.GetUnderlyingType(t) ?? t);

							type = t;
						}

						throw new InvalidOperationException($"The member path '{fullName}' could not be accessed on type: {type.Name}");
					}

                    static MemberInfo GetMember(Type type, string memberName)
                    {
                        var f = type.GetField(memberName);

                        if (f != null) return f;

                        var p = type.GetProperty(memberName);

                        if (p == null) throw new InvalidOperationException($"The object type does not have a member named: {memberName}");

                        return p;
                    }

                    static Type GetMemberType(MemberInfo member)
                    {
                        return (member is FieldInfo ? ((FieldInfo)member).FieldType : ((PropertyInfo)member).PropertyType);
                    }

					public static GridSet CreateFromEnumerable(IEnumerable<string> columnNames, string idColumnName, Type kgsEntityType, IEnumerable entities)
					{
						return new GridSet
						(
							columnNames.ToDictionary
                            (
                                _=>_.Split('.').Last(),
								_=>GetNestedMemberType(kgsEntityType, _).Name),

								((IEnumerable<object>)entities).Select(entity=>
                                new Row(GetFieldValue(entity, idColumnName), 
                                columnNames.ToDictionary(field=>field.Split('.').Last(), field=>GetFieldValue(entity, field))))
						);
					}

				#endregion

				[DataMemberAttribute()]
				public readonly IDictionary<string, string> ColumnTypes;

				[DataMemberAttribute()]
				public IEnumerable<Row> Rows;
				
				public GridSet(IDictionary<string, string> columnTypes, IEnumerable<Row> rows)
				{
					ColumnTypes = columnTypes;

					Rows = rows;
				}
			}

			public class CacheKeyInfo
			{
				public readonly string KeyMain;

				public readonly Func<string> GetSubKey;

				public readonly Func<IEnumerable<string>> GetSubKeyList;

				public CacheKeyInfo(string keyMain, string subKey): this(keyMain, ()=>{return subKey;}, ()=>{return null;})
				{
				}

				public CacheKeyInfo(string keyMain, Func<string> getSubKey, Func<IEnumerable<string>> getSubKeyList)
				{
					KeyMain = keyMain;

					GetSubKey     = getSubKey;

					GetSubKeyList = getSubKeyList;
				}

				public string CreateKey() => CreateKey(GetSubKey());
				
				public string CreateKey(string subKey) => string.Concat("ADMIN.", KeyMain, '[', subKey, ']');
			}

		#endregion

		#region STATIC

			public static void Clear(ICacheStore cacheStore, CacheKeyInfo keyInfo) => new GridCacher(cacheStore, keyInfo).Clear();

			static bool SearchRow(ISearchFilterService filterSvc, GridSet.Row row, IEnumerable<string> searchColumnNames, IEnumerable<string> searchTerms)
			{
				foreach (var c in searchColumnNames)
				{
					var val = row.Cells[c].ToLower();

					if (searchTerms.Any(term => filterSvc.Compare(val, term))) return true;
				}

				return false;
			}

		#endregion

		#region field

			readonly Type kgsEntityType;

			readonly CacheKeyInfo keyInfo;

			readonly CacheManager<GridSet> cacheManager;

			readonly Func<IEnumerable<String>,IEnumerable> getEntitiesForGrid;	
        		
			readonly String idColumnName;

			readonly IEnumerable<String> columnNames;

			readonly IEnumerable<String> searchColumnNames;			

			public Boolean AlwaysForceRefresh;

		#endregion

		public sealed class ColumnInfo
		{
			public String IdColumnName;

			public IEnumerable<String> ColumnNames;

			public IEnumerable<String> SearchColumnNames;

			public ColumnInfo(String idColumnName, IEnumerable<String> columnNames, IEnumerable<String> searchColumnNames)
			{
				IdColumnName      = idColumnName;

				ColumnNames       = columnNames;

				SearchColumnNames = searchColumnNames;
			}

			ColumnInfo() { }
		}

		#region constructor
			
			private GridCacher(ICacheStore cacheStore, CacheKeyInfo keyInfo)
			{
				cacheManager = new CacheManager<GridSet>(cacheStore, keyInfo.CreateKey()); //should be created on first use not by default in constructor (here)
			}

			public GridCacher(Type kgsEntityType, ICacheStore cacheStore, CacheKeyInfo keyInfo, Func<IEnumerable<String>,IEnumerable> getEntitiesForGrid, ColumnInfo options):this(cacheStore, keyInfo)
			{
				this.kgsEntityType      = kgsEntityType;

				this.keyInfo            = keyInfo;

				this.getEntitiesForGrid = getEntitiesForGrid;
				
				idColumnName      = options.IdColumnName;

				columnNames       = options.ColumnNames;

				searchColumnNames = (options.SearchColumnNames ?? options.ColumnNames).Select(_=>_.Split('.').Last()); //revise?
			}

		#endregion

		#region method

			public GridSet Get(string searchText)
			{
				return Get(searchText, false);
			}

			public GridSet Get(Boolean forceRefresh=false)
			{
				return Get(null, forceRefresh);
			}

			public GridSet Get(string searchText, Boolean forceRefresh=false)
			{
				if (forceRefresh || AlwaysForceRefresh)
				{
					var caches = keyInfo.GetSubKeyList();

					if (caches == null)
						cacheManager.Clear();
					else
						cacheManager.Clear(caches.Select(_=>keyInfo.CreateKey(_))); //should be refined to only delete the specific cache that has been updated not all
				}

				var gs = cacheManager.Retrieve(() => CreateEnumerable());

				if (string.IsNullOrWhiteSpace(searchText)) return gs;

                //the search results are filtered via a service, which requires the input text (search text here)
                //since the GridCacher is entirely coupled to itself, it was necessary to process the input, then
                //expose the search terms here, in order to loop through the rows and search for the text value(s)
                //via the SearchRow method below
                var filterSvc = new SearchFilterServiceFactory().GetSearchFilterService();

                filterSvc.ProcessSearchInput(searchText);

                var searchTerms = filterSvc.SearchTerms;
   				
				return new GridSet(gs.ColumnTypes, gs.Rows.Where(row => SearchRow(filterSvc, row, searchColumnNames, searchTerms)));
			}

			GridSet CreateEnumerable()
			{
				return GridSet.CreateFromEnumerable(columnNames, idColumnName, kgsEntityType, getEntitiesForGrid(Enumerable.Empty<string>()));
			}

			public void Clear() => cacheManager.Clear();
			
		#endregion
	}
}