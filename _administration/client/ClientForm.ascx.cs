﻿using CW.Business.Blob.Images;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._administration.client
{
	public partial class ClientForm: FormGenericBase<ClientForm,Client>
	{
		#region field

			public Boolean OrganizationEnabled;
			public Boolean ClientNameEnabled;
			public Boolean IsPrimaryEnabled;			
			public Boolean IsProxyEnabled;
			public Boolean IsActiveVisible;

		#endregion

		#region method

			#region override

				public override void PopulateForm(Client client)
				{
					tab.BindCountries(ddlCountryAlpha2Code);
					tab.BindStates(ddlStateID, client.ClientCountryAlpha2Code);

					LoadHandlers.Add(PropHelper.G<ClientForm>(_=>_.ddlOrganization), OrganizationLoadHandler);
					LoadHandlers.Add(PropHelper.G<ClientForm>(_=>_.ddlCountryAlpha2Code), _=>_.ClientCountryAlpha2Code);

					base.PopulateForm(client);

					if (!String.IsNullOrEmpty(client.ImageExtension))
					{
						var image = new ClientProfileImage(client, DataMgr.OrgBasedBlobStorageProvider);

						if (image.Exists())
						{
							imageFileUploader.SetFile(image.Retrieve(), image.FileName);
						}
					}

                    if (FormMode == PersistModeEnum.Update)
                    {
						//set zip validators accordingly
						zipRequiredValidator.Enabled = zipRegExValidator.Enabled = ((client.ClientCountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code) ? true : false);
						txtZip.Mask = ((client.ClientCountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code) ? "#####" : "aaaaaaaaaa");

						//set state validator accordingly
						stateRequiredValidator.Enabled = (ddlStateID.Items.Count > 1);
                    }

                    chkIsProxy.Enabled = chkIsActive.Enabled = pnlInternalInformation.Visible = (page.AdminMode == AdminModeEnum.KGS);

                    kioskInformationHeaderText.InnerText = (((FormMode == PersistModeEnum.Insert) ? "Add" : "Edit") + " Kiosk Information");

                    internalInformationHeaderText.InnerText = (((FormMode == PersistModeEnum.Insert) ? "Add" : "Edit") + " Internal Client Information");
				}

				public override void CreateEntity(Client obj)
				{
					base.CreateEntity(obj);
				}

				public override void ClearForm()
				{
					base.ClearForm();

					lblPrimaryNote.InnerHtml = String.Empty;

					imageFileUploader.Clear();
				} 

			#endregion

			private void Page_FirstInit()
			{
				if (FormMode == PersistModeEnum.Insert)
				{
					chkIsActive.Checked = true;

					ddlOrganization.Items.Add(new ListItem("Select one...", "-1"));					
				}

				ddlCountryAlpha2Code.Items.Add(new ListItem("Select one...", "-1"));
				ddlStateID.Items.Add(new ListItem("Select country first...", "-1"));

				tab.BindOrganizations(ddlOrganization);
				tab.BindCountries(ddlCountryAlpha2Code);
			}

			public Boolean CanClientBePersisted(Client client, out String message)
			{
				if (DataMgr.ClientDataMapper.DoesClientExist(client.CID, client.ClientName))
				{
					message = "{EntityTypeName} already exists.";

					return false;
				}

				message = null;

				return true;
			}

			public void PersistClient(Client client)
			{
                ApplyOrgToOrgTypeID(client);

				switch (FormMode)
				{
					case PersistModeEnum.Insert:
					{
						client.ImageExtension = ((imageFileUploader.File == null) ? null : imageFileUploader.File.Extension);

						DataMgr.ClientDataMapper.InsertClient(client);

						if (client.ImageExtension != null)
						{
							new ClientProfileImage(client, DataMgr.OrgBasedBlobStorageProvider, imageFileUploader.File.Extension).Insert(imageFileUploader.File.Content);
						}

                        //insert default modules for the client after client insert
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.Alarms });
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.AnalysisBuilder });
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.CommissioningDashboard });
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.Diagnostics }); 
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.Documents });                        
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.BuildingProfiles });
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.EquipmentProfiles });
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.Projects });
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.Reporting });
                        DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = client.CID, ModuleID = (int)BusinessConstants.Module.Modules.Tasks });

						break;
					}
					case PersistModeEnum.Update:
					{
						if (imageFileUploader.HasChanged)
						{
							var image = new ClientProfileImage(client, DataMgr.OrgBasedBlobStorageProvider);

							if (!String.IsNullOrEmpty(hdnImageExtension.Value))
							{
								image.SetExtension(hdnImageExtension.Value).Delete();
                                
								client.ImageExtension = String.Empty;
							}

							if (imageFileUploader.File != null)
							{
								image.SetExtension(imageFileUploader.File.Extension).Insert(imageFileUploader.File.Content);

								client.ImageExtension = imageFileUploader.File.Extension;
							}
						}

						DataMgr.ClientDataMapper.UpdateClient(client);
                        
						break;
					}
				}



				hdnImageExtension.Value = client.ImageExtension;
			}

			private Object OrganizationLoadHandler(Client client)
			{
				return client.Organizations_OrganizationTypeName.OID;
			}

			private void ApplyOrgToOrgTypeID(Client client)
			{
				var oid = Int32.Parse(ddlOrganization.SelectedValue);

				Organizations_OrganizationTypeName ootn;

				switch (FormMode)
				{
					case PersistModeEnum.Insert:
					{
						ootn = new Organizations_OrganizationTypeName{OID=oid, OTNID=(Int32)BusinessConstants.OrgTypeName.OrgTypeNameEnum.Client};

						DataMgr.OrganizationTypeDataMapper.InsertOrganizationTypeName(ootn);

						break;
					}
					case PersistModeEnum.Update:
					{
						ootn = DataMgr.OrganizationTypeDataMapper.GetOOTByOID(BusinessConstants.OrgTypeName.OrgTypeNameEnum.Client, oid);

						break;
					}
					default: throw new InvalidOperationException();
				}

				client.OOTID = ootn.OOTID;
			}

			protected void ddlOrganization_OnSelectedIndexChanged(Object sender, EventArgs e)
			{
				if (ddlOrganization.SelectedValue == "-1") return;

				var hasPrimary = DataMgr.ClientDataMapper.GetAllClientsByOrg(DataMgr.OrganizationDataMapper.GetOrganizationByID(Convert.ToInt32(ddlOrganization.SelectedValue)).OID).Any(c => c.IsPrimary);

				if (IsUpdate) return;

				chkIsPrimary.Checked = !hasPrimary;

				//if (chkIsPrimary.Checked)
				//{
				//	chkIsProxy.Enabled = false;
				//}

				lblPrimaryNote.InnerText = (chkIsPrimary.Checked ? "The client will be created as primary for the selected organization." : "The selected organization already has a primary client.");
			}

			//protected void chkIsProxy_OnCheckedChanged(Object sender, EventArgs e)
			//{
				//if (IsInsert) return;

				//chkIsPrimary.Enabled = !chkIsProxy.Checked;

				//lblPrimaryNote.InnerText = (chkIsProxy.Checked ? "The client can not be primary because it is a proxy." : String.Empty);
			//}

		#endregion

		#region property

			public override Delegates<Client>.CanEntityBePersistedDelegate CanEntityBePersisted
			{
				get {return CanClientBePersisted;}
			}

			public override Action<Client> PersistEntityAction
			{
				get {return PersistClient;}
			}

		#endregion

		#region Dropdown Events

            protected void ddlCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                tab.BindStates(ddlStateID, ddlCountryAlpha2Code.SelectedValue);

                zipRequiredValidator.Enabled = zipRegExValidator.Enabled = ddlCountryAlpha2Code.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtZip.Mask = ddlCountryAlpha2Code.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
                stateRequiredValidator.Enabled = ddlStateID.Items.Count > 1;
            }

        #endregion
	}
}