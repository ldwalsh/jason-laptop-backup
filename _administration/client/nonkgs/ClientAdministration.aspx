﻿<%@ Page Language="C#" AutoEventWireup="false" MasterPageFile="~/_masters/Admin.Master" CodeBehind="ClientAdministration.aspx.cs" Inherits="CW.Website._administration.client.nonkgs.ClientAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="~/_administration/client/nonkgs/ClientEditTab.ascx" tagPrefix="CW" tagName="ClientEditTab" %>
<%@ Register src="~/_administration/client/nonkgs/ClientContentTab.ascx" tagPrefix="CW" tagName="ClientContentTab" %>
<%@ Register src="~/_administration/client/ClientCMMSSettingsTab.ascx" tagPrefix="CW" tagName="ClientCMMSSettingsTab" %>
<%@ Register src="~/_administration/client/ClientGoalsSettingsTab.ascx" tagPrefix="CW" tagName="ClientGoalsSettingsTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   

    <script type="text/javascript" src="/_administration/_script/TabStateMonitor.js?v=_assets/"></script>

            <CW:PageHeader runat="server" Title="Client Administration" GlobalSettingProperty="AdminClientBody" />
            <div class="administrationControls">
                <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
                    <Tabs>
                        <telerik:RadTab Text="Edit Client"></telerik:RadTab>
                        <telerik:RadTab Text="Client Content Settings"></telerik:RadTab>
                        <telerik:RadTab Text="Client WO Settings" />
                        <telerik:RadTab Text="Client Goal Settings" />
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <CW:ClientEditTab ID="ClientEditTab1" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView2" runat="server">
                        <CW:ClientContentTab ID="ClientContentTab" runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView3" runat="server">
                        <CW:ClientCMMSSettingsTab ID="ClientCMMSSettingsTab" runat="server" IsReadOnly="True" />
                    </telerik:RadPageView>   
                    <telerik:RadPageView runat="server">
                        <CW:ClientGoalsSettingsTab ID="ClientGoalsSettingsTab" runat="server" />
                    </telerik:RadPageView>           
                </telerik:RadMultiPage>
            </div>

</asp:Content>