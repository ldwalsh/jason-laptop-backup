﻿using System;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;

namespace CW.Website._administration.client.nonkgs
{
    public partial class ClientContentTab: EditTabBase<ClientSetting, ClientAdministration>
    {
        #region method

            private void Page_FirstInit()
            {
                PopulateForm(GetEntityForEditing);
            }

            private void Page_Init()
            {
                CanEntityBeUpdated = (ClientSetting setting, out String message) => {message = null; return true;};
                UpdateEntityAction = DataMgr.ClientDataMapper.UpdateClientContentSettings;

				OnTabStateChanged += delegate{Page_FirstInit();};
            }

        #endregion

        #region property

            protected override ClientSetting GetEntityForEditing
            {
                get {return DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID);}
            }

        #endregion
    }
}