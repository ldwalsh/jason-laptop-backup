﻿using CW.Data;
using System;
using System.Web.UI.WebControls;

namespace CW.Website._administration.client.nonkgs
{
    public partial class ClientContentForm: FormGenericBase<ClientContentForm, ClientSetting>
    {
        #region method

            public Boolean CanClientContentBePersisted(ClientSetting setting, out String message)
            {
                message = null;

                return true;
            }

        #endregion
        
        #region property

            #region override
            
                public override Action<ClientSetting> PersistEntityAction
                {
                    get { return DataMgr.ClientDataMapper.UpdateClientContentSettings; }
                }

                public override Delegates<ClientSetting>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get {return CanClientContentBePersisted;}
                }

            #endregion

        #endregion
    }
}