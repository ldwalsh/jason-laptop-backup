﻿using System;

namespace CW.Website._administration.client.nonkgs
{
    public partial class ClientAdministration: ClientAdministrationPageBase
    {
        private void Page_Init()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check kgs super admin or higher, or super admin non provider, or super admin provider proxy.
            if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && !siteUser.IsLoggedInUnderProviderClient) || (siteUser.IsSuperAdmin && siteUser.IsProxyClient && siteUser.IsLoggedInUnderProviderClient)))
                Response.Redirect("/Home.aspx");
        }
    }
}