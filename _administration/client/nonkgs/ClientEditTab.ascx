﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ClientEditTab.ascx.cs" Inherits="CW.Website._administration.client.nonkgs.ClientEditTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" TagName="ClientForm" Src="~/_administration/client/ClientForm.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Edit Client" /> 
<CW:ClientForm ID="clientForm" runat="server" FormMode="Update" UpdateButtonCaption="Update Client" IsActiveVisible="false" />
