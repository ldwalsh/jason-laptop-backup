﻿using System;

namespace CW.Website._administration.client.kgs
{
    public partial class KGSClientAdministration: ClientAdministrationPageBase
    {
        private void Page_Init()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check kgs super admin or higher.
            if (!siteUser.IsKGSSuperAdminOrHigher)
                Response.Redirect("/Home.aspx");
        }
    }
}