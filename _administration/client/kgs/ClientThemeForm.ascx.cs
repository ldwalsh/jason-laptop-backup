﻿using CW.Data;
using System;
using System.Web.UI.WebControls;

namespace CW.Website._administration.client.kgs
{
	public partial class ClientThemeForm: FormGenericBase<ClientThemeForm,ClientSetting>
	{
		public DropDownList ddlThemedProvider;

		#region method

			private Boolean CanClientSettingsBePersisted(ClientSetting setting, out String message)
			{
				message = null;

				return true;
			}

		#endregion

		#region property

			public override Delegates<ClientSetting>.CanEntityBePersistedDelegate CanEntityBePersisted
			{
				get {return CanClientSettingsBePersisted;}
			}

			public override Action<ClientSetting> PersistEntityAction
			{
				get {return DataMgr.ClientDataMapper.UpdateClientThemeSettings;}
			}

		#endregion
	}
}