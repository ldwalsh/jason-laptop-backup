﻿using CW.Data;
using System;

namespace CW.Website._administration.client.kgs
{
    public partial class KGSClientAccountTab: EditTabBase<ClientSetting,KGSClientAdministration>
    {
		#region method

			private void Page_FirstInit()
			{
				BindClients(ddlCurrentClient);

				ddlCurrentClient.SelectedIndex = 0;

				ddlClient_OnSelectedIndexChanged(null, null);
			}

			private void Page_Init()
			{
				CanEntityBeUpdated = clientAccountForm.CanEntityBePersisted;
				UpdateEntityAction = clientAccountForm.PersistEntityAction;

				OnTabStateChanged += delegate{Page_FirstInit();};
			}

			protected void ddlClient_OnSelectedIndexChanged(Object sender, EventArgs e)
			{
				clientAccountForm.Visible = (ddlCurrentClient.SelectedIndex != 0);

				if (ddlCurrentClient.SelectedIndex == 0) return;

                PopulateForm(GetEntityForEditing);
            }

		#endregion

		#region property

			protected override ClientSetting GetEntityForEditing
			{
				get {return DataMgr.ClientDataMapper.GetClientSettingsByClientID(Convert.ToInt32(ddlCurrentClient.SelectedValue));}
			}

		#endregion
    }
}