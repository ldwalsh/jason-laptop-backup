﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSClientViewTab.ascx.cs" Inherits="CW.Website._administration.client.kgs.KGSClientViewTab" %>
<%@ Register TagPrefix="CW" TagName="ClientForm" Src="~/_administration/client/ClientForm.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Clients" />  
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>                                                                                                                                                     
<div id="gridTbl">
    <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
        <Columns>
            <asp:CommandField ShowSelectButton="true" />
            <asp:TemplateField>
                <ItemTemplate>                                                
                    <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>														                                               
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client Name">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Client>(_ => _.ClientName)) %>"><%# GetCellContent(Container, PropHelper.G<Client>(_=>_.ClientName), 30) %></label>
                </ItemTemplate>
            </asp:TemplateField>   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientAddress" HeaderText="Address">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Client>(_ => _.ClientAddress)) %>"><%# GetCellContent(Container, PropHelper.G<Client>(_=>_.ClientAddress), 25) %></label>
                </ItemTemplate>
            </asp:TemplateField>   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientCity" HeaderText="City">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Client>(_ => _.ClientCity)) %>"><%# GetCellContent(Container, PropHelper.G<Client>(_=>_.ClientCity), 25) %></label>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="CountryName" HeaderText="Country">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Country>(_ => _.CountryName)) %>"><%# GetCellContent(Container, PropHelper.G<Country>(_=>_.CountryName), 20)%></label>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
                <ItemTemplate>
                 <%# GetCellContent(Container, PropHelper.G<Client>(_=>_.IsActive)) %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>                                                
                    <asp:LinkButton Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this client permanently?\n\nAlternatively you can deactivate a client temporarily instead.');" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                                                                                          
        </Columns>
    </asp:GridView>                                                                                                             
</div>
<br />
<br /> 
<!--SELECT CLIENT DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
    <Fields>
        <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
            <ItemTemplate>
                <div>
                    <h2>Client Details</h2>
                    <ul class="detailsListNarrow">
                        <CW:UListItem runat="server" Label="Name" Value="<%#Eval(PropHelper.G<Client>(c=>c.ClientName)) %>" />
                        <CW:UListItem runat="server" Label="Address" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientAddress)) %>" />
                        <CW:UListItem runat="server" Label="City" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientCity)) %>" />
                        <CW:UListItem runat="server" Label="Country" Value="<%# Eval(PropHelper.G<Client, Country>(c=>c.Country, c=>c.CountryName)) %>" />
                        <CW:UListItem runat="server" Label="State" Value="<%# Eval(PropHelper.G<Client, State>(c=>c.State, s=>s.StateName)) %>" />
                        <CW:UListItem runat="server" Label="Zip" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientZip)) %>" />
                        <CW:UListItem runat="server" Label="Phone" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientPhone)) %>" />
                        <CW:UListItem runat="server" Label="Fax" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientFax)) %>" />
                        <CW:UListItem runat="server" Label="Primary Contact" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientPrimaryContactName)) %>" />
                        <CW:UListItem runat="server" Label="Primary Contact Email" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientPrimaryContactEmail))%>" Format="Email" />
                        <CW:UListItem runat="server" Label="Primary Contact Phone" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientPrimaryContactPhone)) %>" />
                        <CW:UListItem runat="server" Label="Secondary Contact" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientSecondaryContactName)) %>" />
                        <CW:UListItem runat="server" Label="Secondary Contact Email" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientSecondaryContactName)) %>" Format="Email" />
                        <CW:UListItem runat="server" Label="Secondary Contact Phone" Value="<%# Eval(PropHelper.G<Client>(c=>c.ClientSecondaryContactPhone)) %>" />
                        <CW:UListImagePreview runat="server" id="ipc" ClientId="<%# Convert.ToInt32(Eval(PropHelper.G<Client>(c=>c.CID))) %>" Extension="<%#Eval(PropHelper.G<Client>(c=>c.ImageExtension)) %>" />                        
                        <CW:UListItem runat="server" Label="Active" Value="<%# Eval(PropHelper.G<Client>(c=>c.IsActive)) %>" />
                    </ul>                    
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.KioskContent)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.KioskQRContent)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.KioskQRLink)))) ? "<div style='display:none'>" : ""%>
                    <br />
                    <h2>Client Kiosk Details</h2>                                                                                                                                            
                    <ul class="detailsListNarrow">
                        <CW:UListItem runat="server" Label="Kiosk Content" Value="<%# Eval(PropHelper.G<Client>(c=>c.KioskContent)) %>" />
                        <CW:UListItem runat="server" Label="Kiosk QR Content" Value="<%# Eval(PropHelper.G<Client>(c=>c.KioskQRContent)) %>" />
                        <%# (String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.KioskQRLink)))) ? "" : "<li><strong>Kiosk QR Link: </strong><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval(PropHelper.G<Client>(c=>c.KioskQRLink)).ToString()) + "'>" + Eval(PropHelper.G<Client>(c=>c.KioskQRLink)) + "</a></li>") %>
                    </ul>
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.KioskContent)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.KioskQRContent)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.KioskQRLink)))) ? "</div>" : ""%>
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.SalesContactName)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.InternalNotes)))) ? "<div style='display:none'>" : ""%>
                    <br />
                    <h2>Client Internal Details</h2>                                                                                                                                            
                    <ul class="detailsListNarrow"> 
                        <CW:UListItem runat="server" Label="Sales Contact Name" Value="<%# Eval(PropHelper.G<Client>(c=>c.SalesContactName)) %>" />
                        <CW:UListItem runat="server" Label="Internal Notes" Value="<%# Eval(PropHelper.G<Client>(c=>c.InternalNotes)) %>" />
                    </ul> 
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.SalesContactName)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client>(c=>c.InternalNotes)))) ? "</div>" : ""%>
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.HomePageHeader, 0)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.HomePageBody, 0)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.NewsBody, 0)))) ? "<div style='display:none'>" : ""%>
                    <br />
                    <h2>Client Content Details</h2>                                                                                                                                            
                    <ul class="detailsList">
                        <CW:UListItem runat="server" Label="Home Page Header" Value="<%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.HomePageHeader, 0)) %>" />
                        <CW:UListItem runat="server" Label="Home Page Body" Value="<%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.HomePageBody, 0)) %>" />
                        <CW:UListItem runat="server" Label="News Body" Value="<%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.NewsBody, 0)) %>" />
                    </ul>
                    <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.HomePageHeader, 0)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.HomePageBody, 0)))) && String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.NewsBody, 0)))) ? "</div>" : ""%>
                    <br />
                    <h2>Client Account Details - Admin</h2>  
                    <ul class="detailsListWide">  
                        <li>
                            <strong>Max Monthly Admin Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c => c.ClientSettings, s => s.MaxMonthlyAdminDownloads, 0)), 0)%>
                        </li>
                        <li>
                            <strong>Current Monthly Admin Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c => c.ClientSettings, s => s.CurrentMonthlyAdminDownloads, 0)), 0)%>
                        </li>
                        <li>
                            <strong>Current Total Admin Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c => c.ClientSettings, s => s.CurrentTotalAdminDownloads, 0)), 0)%>
                        </li>
                    </ul>
                    <br />
                    <h2>Client Account Details - Scheduled Reports</h2>   
                    <ul class="detailsListWide">
                        <li>
                            <strong>Priority Daily Emails: </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.AutomatedPriorityDailyEmails, 0)) %>
                        </li>
                        <li>
                            <strong>Priority Weekly Emails: </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.AutomatedPriorityWeeklyEmails, 0)) %>
                        </li>
                        <li>
                            <strong>Priority Monthly Emails: </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.AutomatedPriorityMonthlyEmails, 0)) %>
                        </li>
                        <li>
                            <strong>Data Source Health Emails Half Day: </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.HasAutomatedDataSourceHealthEmailsHalfDay, 0)) %>
                        </li>
                        <li>
                            <strong>Data Source Health Emails End of Day: </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.HasAutomatedDataSourceHealthEmailsEndOfDay, 0)) %>
                        </li>
                    </ul>
                    <br />
                    <div runat="server" visible='<%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.SupportEmailOverride, 0)) != null ? (String.IsNullOrWhiteSpace(Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.SupportEmailOverride, 0)).ToString()) ? false : true) : false %>'>                    
                        <h2>Client Account Details - Support</h2>   
                        <ul class="detailsListWide">
                            <li>
                                <strong>Support Email Override: </strong>
                                <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.SupportEmailOverride, 0)) %>
                            </li>                        
                        </ul>
                        <br />
                    </div>
                    <h2>Client Account Details - Referring Domain Auto Login</h2>   
                    <ul class="detailsListWide">
                        <li>
                            <strong>Referring Domain Auto Login: </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.EnableReferringDomainAutoLogin, 0)) %>
                        </li>                        
                    </ul>
                    <br />
                    <h2>Client Account Details - Reports</h2>                                                                                                                                       
                    <ul class="detailsListWide">
                        <li>
                            <strong>Max Monthly Report Downloads (Count): </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.MaxMonthlyReportDownloads, 0)) %>
                        </li>
                        <li>
                            <strong>Current Monthly Report Downloads (Count): </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.CurrentMonthlyReportDownloads, 0)) %>
                        </li>
                        <li>
                            <strong>Max Monthly Report Emails (Count): </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.MaxMonthlyReportEmails, 0)) %>
                        </li>
                        <li>
                            <strong>Current Monthly Report Emails (Count): </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.CurrentMonthlyReportEmails, 0)) %>
                        </li>
                    </ul>
                    <br />
                    <h2>Client Account Details - Documents</h2>                                                                                                                                       
                    <ul class="detailsListWide">                                                
                        <li>
                            <strong>Max Monthly Documents Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.MaxMonthlyDocumentDownloads, 0)), 0) %>
                        </li>
                        <li>
                            <strong>Current Monthly Documents Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.CurrentMonthlyDocumentDownloads, 0)), 0) %>
                        </li>
                        <li>
                            <strong>Max Monthly Documents Uploads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.MaxMonthlyDocumentUploads, 0)), 0) %>
                        </li>
                        <li>
                            <strong>Current Monthly Documents Uploads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.CurrentMonthlyDocumentUploads, 0)), 0) %>
                        </li>
                        <li>
                            <strong>Max Total Documents Storage (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.MaxTotalDocumentStorage, 0)), 0) %>
                        </li>
                        <li>
                            <strong>Current Total Documents Storage (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.CurrentTotalDocumentStorage, 0)), 0) %>
                        </li>                   
                    </ul>   
                    <br />
                    <h2>Client Account Details - Raw</h2>  
                    <ul class="detailsListWide">  
                        <li>
                            <strong>Max Monthly Raw Data Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.MaxMonthlyRawDataDownloads, 0)), 0) %>
                        </li>
                        <li>
                            <strong>Current Monthly Raw Data Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.CurrentMonthlyRawDataDownloads, 0)), 0) %>
                        </li>
                        <li>
                            <strong>Current Total Raw Data Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.CurrentTotalRawDataDownloads, 0)), 0) %>
                        </li>
                    </ul>
                    <br />
                    <h2>Client Account Details - Modules</h2>  
                    <ul class="detailsListWide">  
                        <li>
                            <strong>Max Monthly Module Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c => c.ClientSettings, s => s.MaxMonthlyModuleDownloads, 0)), 0)%>
                        </li>
                        <li>
                            <strong>Current Monthly Module Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c => c.ClientSettings, s => s.CurrentMonthlyModuleDownloads, 0)), 0)%>
                        </li>
                        <li>
                            <strong>Current Total Module Downloads (MB): </strong>
                            <%# MathHelper.ConvertBytesToMB((long)Eval(PropHelper.G<Client, ClientSetting>(c => c.ClientSettings, s => s.CurrentTotalModuleDownloads, 0)), 0)%>
                        </li>
                    </ul>
                    <br />
                    <h2>Client Theme Details</h2>  
                    <ul class="detailsList"> 
                        <li>
                            <strong>Themed Provider: </strong>
                            <%# GetThemedProviderName(Convert.ToInt32(Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.ThemedProvider, 0)))) %>
                        </li>                        
                    </ul>
                    <br />
                    <h2>Client Settings Details - Other</h2>  
                    <ul class="detailsList"> 
                        <li>
                            <strong>Last Modified Date: </strong>
                            <%# Eval(PropHelper.G<Client, ClientSetting>(c=>c.ClientSettings, s=>s.DateModified, 0)) %>
                        </li>  
                    </ul>
                </div>
            </ItemTemplate>                                             
        </asp:TemplateField>
    </Fields>
</asp:DetailsView>
                                                                     
<!--EDIT Client PANEL -->                              
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">                                                                                             
    <div>
        <h2>Edit Client</h2>
    </div>  
    <div>
        <CW:ClientForm ID="clientForm" runat="server" FormMode="Update" UpdateButtonCaption="Edit Client" ClientNameEnabled="true" IsProxyEnabled="true" IsActiveVisible="true" />
    </div>
</asp:Panel>
