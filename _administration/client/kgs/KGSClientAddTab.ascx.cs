﻿using CW.Data;

namespace CW.Website._administration.client.kgs
{
    public partial class KGSClientAddTab: AddTabBase<Client,KGSClientAdministration>
    {
		private void Page_Init()
        {
            CanEntityBeAdded = clientForm.CanEntityBePersisted;
            AddEntityMethod  = clientForm.PersistEntityAction;
        }
    }
}