﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSClientContentTab.ascx.cs" Inherits="CW.Website._administration.client.kgs.KGSClientContentTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="ClientContentForm.ascx" tagPrefix="CW" tagName="ClientContentForm" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Content Settings" />
<div class="richText">
Custom token variables are available as well. Including the following:
<ul>
    <li>Address = $address</li>
    <li>City = $city</li>
    <li>Client Name = $client</li>
    <li>Company Name (KGS Buildings or Schneider Electric) = $company</li>
    <li>Company Abbreviation (KGS or SE) = $companyShort</li>
    <li>Product Name (Clockworks or Building Analytics) = $product</li>
    <li>Product Abbreviation (CW or BA) = $productShort</li>
</ul>
</div>
<div runat="server" class="divForm">
    <label class="label">*Select Client:</label>    
    <asp:DropDownList ID="ddlCurrentClient" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCurrentClient_OnSelectedIndexChanged" AutoPostBack="true">
        <asp:ListItem>Select one...</asp:ListItem>                           
    </asp:DropDownList> 
</div>
<CW:ClientContentForm runat="server" ID="clientContentForm" Visible="false" UpdateButtonCaption="Update" FormMode="Update" />
