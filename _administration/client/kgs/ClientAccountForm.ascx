﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ClientAccountForm.ascx.cs" Inherits="CW.Website._administration.client.kgs.ClientAccountForm" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="CW" TagName="ReportCriteria" Src="~/_administration/ReportAssignmentCriteria.ascx" %>
<%@ Import Namespace="CW.Website._framework.httpmodules" %>
    
<hr />
<asp:HiddenField ID="hdnClientSettingsID" runat="server" Visible="false" />

<h2>Scheduled Reports</h2>
<CW:ReportCriteria runat="server" ID="reportCriteriaDailyPriority" Title="Priority Daily Emails:" />
<CW:ReportCriteria runat="server" ID="reportCriteriaWeeklyPriority" Title="Priority Weekly Emails:" />
<CW:ReportCriteria runat="server" ID="reportCriteriaMonthlyPriority" Title="Priority Monthly Emails:" />
<div class="divForm">
    <label class="label">Automated Data Source Health Emails Half Day:</label>
    <asp:CheckBox ID="chkHasAutomatedDataSourceHealthEmailsHalfDay" CssClass="checkbox" OnCheckedChanged="chkHasAutomatedDataSourceHealthEmails_OnCheckedChanged" AutoPostBack="true" runat="server" />                                                        
</div> 
<div class="divForm">
    <label class="label">Automated Data Source Health Emails End of Day:</label>
    <asp:CheckBox ID="chkHasAutomatedDataSourceHealthEmailsEndOfDay" CssClass="checkbox" OnCheckedChanged="chkHasAutomatedDataSourceHealthEmails_OnCheckedChanged" AutoPostBack="true" runat="server" />                                                        
</div> 
<div id="divAutoDataSourceHealthEmails" class="divForm" visible="false" runat="server">                                                                                                                               
    <div>                                       
        <label class="label">Available User Groups:</label> 
        <asp:ListBox ID="lbDataSourceUserGroupsTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
        </asp:ListBox>
        <div class="divArrows">
        <asp:ImageButton CssClass="up-arrow"  ID="btnUp"  OnClick="btnDataSourceUserGroupsUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
        <asp:ImageButton CssClass="down-arrow" ID="btnDown"  OnClick="btnDataSourceUserGroupsDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
        </div>
        <label class="label">Assigned User Groups:</label>
        <asp:ListBox ID="lbDataSourceUserGroupsBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
        </asp:ListBox> 
    </div>   
    <a target="_blank" href="/<%=PathRewriteModule.KGSUserGroupAdministration%>">Create User Groups</a>
    <asp:HiddenField ID="hdnAutomatedDataSourceHealthEmailsGuid" runat="server" Visible="false" />
</div>
<hr />
<h2>Support</h2>
    <div class="divForm">
        <label class="label">Support Email Override:</label>
        <asp:TextBox ID="txtSupportEmailOverride" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>  
        <p>(Warning: Inputting an email here will override where system level support emails for this client will be delivered.)</p>                                          
        <asp:RegularExpressionValidator ID="supportEmailOverrideRegExValidator" runat="server"
            ErrorMessage="Invalid Email Format."
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            ControlToValidate="txtSupportEmailOverride"
            SetFocusOnError="true" 
            Display="None" 
            ValidationGroup="UpdateAccountSettings">
            </asp:RegularExpressionValidator>                         
        <ajaxToolkit:ValidatorCalloutExtender ID="supportEmailOverrideRegExValidatorExtender" runat="server"
            BehaviorID="supportEmailOverrideRegExValidatorExtender" 
            TargetControlID="supportEmailOverrideRegExValidator"
            HighlightCssClass="validatorCalloutHighlight"
            Width="175">
            </ajaxToolkit:ValidatorCalloutExtender>
    </div>
<hr />
<h2>Referring Domain Auto Login</h2>
<div class="divForm">
    <label class="label">Enable Referring Domain Auto Login:</label>
    <asp:CheckBox ID="chkEnableReferringDomainAutoLogin" CssClass="checkbox" OnCheckedChanged="chkEnableReferringDomainAutoLogin_OnCheckedChanged" AutoPostBack="true" runat="server" />                                                        
</div> 
<div id="divReferringDomainAutoLogin" visible="false" runat="server">   
    <div class="divForm">
        <label class="label">Referring Domains (Can be comma separated.):</label>               
        <asp:TextBox ID="txtReferringDomainAutoLoginDomains" CssClass="textbox" MaxLength="500" runat="server" />
        <p>(Note: Public access uses the referring domain to auto login a user with a public role.  Multiple referring domains can be allowed for the public user, comma separated, no www or http. Optional referrer redirect can be part of the link, to redirect the public user to a specific page first. Will work with cw url and branded urls. Public Access does not use an IdP. Public Access does not use the Organizations session timeout override.)
        <p>(Ex: https://clockworks.kgsbuildings.com/PublicAccess.aspx )(Optional: ?referrer=xxxxx.aspx)</p>    
        <asp:RequiredFieldValidator ID="settingsReferringDomainRequiredValidator" runat="server" 
            ErrorMessage="Referring Domains is a required field." 
            ControlToValidate="txtReferringDomainAutoLoginDomains" 
            SetFocusOnError="true" 
            Display="None" 
            ValidationGroup="UpdateAccountSettings" />
        <ajaxToolkit:ValidatorCalloutExtender ID="settingsReferringDomainRequiredValidatorExtender" runat="server" 
            BehaviorID="settingsReferringDomainRequiredValidatorExtender" 
            TargetControlID="settingsReferringDomainRequiredValidator" 
            HighlightCssClass="validatorCalloutHighlight" 
            Width="175" />
    </div>
    <div class="divForm">
        <label class="label">Auto Login User:</label>   
        <asp:DropDownList ID="ddlReferringDomainAutoLoginUserID" 
            CssClass="dropdown" AppendDataBoundItems="true" 
            runat="server">                                                                
        </asp:DropDownList> 
        <p>(Note: Make sure to set restrictions on this user.)</p>
        <asp:RequiredFieldValidator ID="settingsAutoLoginUserRequiredValidator" runat="server"
            ErrorMessage="User is a required field." 
            ControlToValidate="ddlReferringDomainAutoLoginUserID"
            SetFocusOnError="true" 
            Display="None" 
            InitialValue="-1"
            ValidationGroup="UpdateAccountSettings" />
        <ajaxToolkit:ValidatorCalloutExtender ID="settingsAutoLoginUserRequiredValidatorExtender" runat="server"
            BehaviorID="settingAutoLoginUserRequiredValidatorExtender" 
            TargetControlID="settingsAutoLoginUserRequiredValidator"
            HighlightCssClass="validatorCalloutHighlight"
            Width="175px" />
    </div>
</div>

<hr />
<h2>Admin</h2>
<p>Pertains to all administrative area downloads.</p>
<p>*Anything over standard allocations, requires contractual approval.</p>
<div class="divFormNarrow">
    <label class="labelWide">Max Monthly Admin Downloads (Standard = 1000 MB):</label>
    <input id="rngMaxMonthlyAdminDownloads" runat="server" type="range" style="width:300px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyAdminDownloads')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyAdminDownloads')" min="0" max="10000" step="50" />
    <asp:TextBox ID="txtMaxMonthlyAdminDownloads" MaxLength="5" CssClass="sliderDisplayWide" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_ctl10_clientAccountForm_rngMaxMonthlyAdminDownloads")'></asp:TextBox>
</div> 
<div class="divFormNarrow">
    <label class="labelWide">Current Monthly Admin Downloads (MB):</label>
    <asp:Label ID="lblCurrentMonthlyAdminDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentMonthlyAdminDownloads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Total Admin Downloads (MB):</label>
    <asp:Label ID="lblCurrentTotalAdminDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentTotalAdminDownloads" runat="server" Visible="false" />
</div>
<hr />
<h2>Reports</h2>
<p>Pertains to all pdf reports downloads and emails found on the reporting, diagnostics, and utility billing modules.</p>
<p>*Anything over standard allocations, requires contractual approval.</p>
<div class="divFormNarrow">   
    <label class="labelWide">Max Monthly Report Downloads (Standard = 500 Count):</label>
    <input id="rngMaxMonthlyReportDownloads" runat="server" type="range" style="width:300px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyReportDownloads')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyReportDownloads')" min="0" max="5000" step="25" />
    <asp:TextBox ID="txtMaxMonthlyReportDownloads" MaxLength="5" CssClass="sliderDisplayWide" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_ctl10_clientAccountForm_rngMaxMonthlyReportDownloads")'></asp:TextBox>
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Monthly Report Downloads (Count):</label>
    <asp:Label ID="lblCurrentMonthlyReportDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentMonthlyReportDownloads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Total Report Downloads (Count):</label>
    <asp:Label ID="lblCurrentTotalReportDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentTotalReportDownloads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Max Monthly Report Emails (Standard = 500 Count):</label>
    <input id="rngMaxMonthlyReportEmails" runat="server" type="range" style="width:300px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyReportEmails')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyReportEmails')" min="0" max="5000" step="25" />
    <asp:TextBox ID="txtMaxMonthlyReportEmails" MaxLength="5" CssClass="sliderDisplayWide" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_ctl10_clientAccountForm_rngMaxMonthlyReportEmails")'></asp:TextBox>
</div> 
<div class="divFormNarrow">
    <label class="labelWide">Current Monthly Report Emails (Count):</label>
    <asp:Label ID="lblCurrentMonthlyReportEmails" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentMonthlyReportEmails" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Total Report Emails (Count):</label>
    <asp:Label ID="lblCurrentTotalReportEmails" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentTotalReportEmails" runat="server" Visible="false" />
</div>
<hr />
<h2>Documents</h2>
<p>Pertains to all document downloads, uploads, and alloted storage in the documents module.</p>
<p>*Anything over standard allocations, requires contractual approval.</p>
<div class="divFormNarrow">
    <label class="labelWide">Max Monthly Document Downloads (Standard = 1000 MB):</label>
    <input id="rngMaxMonthlyDocumentDownloads" runat="server" type="range" style="width:300px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyDocumentDownloads')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyDocumentDownloads')" min="0" max="10000" step="50" />
    <asp:TextBox ID="txtMaxMonthlyDocumentDownloads" MaxLength="5" CssClass="sliderDisplayWide" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_ctl10_clientAccountForm_rngMaxMonthlyDocumentDownloads")'></asp:TextBox>
</div> 
<div class="divFormNarrow">
    <label class="labelWide">Current Monthly Document Downloads (MB):</label>
    <asp:Label ID="lblCurrentMonthlyDocumentDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentMonthlyDocumentDownloads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Total Document Downloads (MB):</label>
    <asp:Label ID="lblCurrentTotalDocumentDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentTotalDocumentDownloads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Max Monthly Document Uploads (Standard = 1000 MB):</label>
    <input id="rngMaxMonthlyDocumentUploads" runat="server" type="range" style="width:300px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyDocumentUploads')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyDocumentUploads')" min="0" max="10000" step="50" />
    <asp:TextBox ID="txtMaxMonthlyDocumentUploads" MaxLength="5" CssClass="sliderDisplayWide" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_ctl10_clientAccountForm_rngMaxMonthlyDocumentUploads")'></asp:TextBox>
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Monthly Document Uploads (MB):</label>
    <asp:Label ID="lblCurrentMonthlyDocumentUploads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentMonthlyDocumentUploads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Total Document Uploads (MB):</label>
    <asp:Label ID="lblCurrentTotalDocumentUploads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentTotalDocumentUploads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">   
    <label class="labelWide">Max Total Document Storage (Standard = 5000 MB):</label>
    <input id="rngMaxTotalDocumentStorage" runat="server" type="range" style="width:300px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxTotalDocumentStorage')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxTotalDocumentStorage')" min="0" max="50000" step="100" />
    <asp:TextBox ID="txtMaxTotalDocumentStorage" MaxLength="5" CssClass="sliderDisplayWide" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_ctl10_clientAccountForm_rngMaxTotalDocumentStorage")'></asp:TextBox>
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Total Document Storage (MB):</label>
    <asp:Label ID="lblCurrentTotalDocumentStorage" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentTotalDocumentStorage" runat="server" Visible="false" />
</div>
<hr />
<h2>Raw Data</h2>
<p>Pertains to all raw data downloads in the reporting and analysis builder modules.</p>
<p>*Anything over standard allocations, requires contractual approval.</p>
<div class="divFormNarrow">
    <label class="labelWide">Max Monthly Raw Data Downloads (Standard = 1000 MB):</label>
    <input id="rngMaxMonthlyRawDataDownloads" runat="server" type="range" style="width:300px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyRawDataDownloads')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyRawDataDownloads')" min="0" max="10000" step="50" />
    <asp:TextBox ID="txtMaxMonthlyRawDataDownloads" MaxLength="5" CssClass="sliderDisplayWide" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_ctl10_clientAccountForm_rngMaxMonthlyRawDataDownloads")'></asp:TextBox>
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Monthly Raw Data Downloads (MB):</label>
    <asp:Label ID="lblCurrentMonthlyRawDataDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentMonthlyRawDataDownloads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Total Raw Data Downloads (MB):</label>
    <asp:Label ID="lblCurrentTotalRawDataDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentTotalRawDataDownloads" runat="server" Visible="false" />
</div>
<hr />
<h2>Modules</h2>
<p>Pertains to all general exports and downloads in all modules.</p>
<p>*Anything over standard allocations, requires contractual approval.</p>
<div class="divFormNarrow">
    <label class="labelWide">Max Monthly Module Downloads (Standard = 1000 MB):</label>
    <input id="rngMaxMonthlyModuleDownloads" runat="server" type="range" style="width:300px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyModuleDownloads')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_ctl10_clientAccountForm_txtMaxMonthlyModuleDownloads')" min="0" max="10000" step="50" />
    <asp:TextBox ID="txtMaxMonthlyModuleDownloads" MaxLength="5" CssClass="sliderDisplayWide" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_ctl10_clientAccountForm_rngMaxMonthlyModuleDownloads")'></asp:TextBox>
</div> 
<div class="divFormNarrow">
    <label class="labelWide">Current Monthly Module Downloads (MB):</label>
    <asp:Label ID="lblCurrentMonthlyModuleDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentMonthlyModuleDownloads" runat="server" Visible="false" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">Current Total Module Downloads (MB):</label>
    <asp:Label ID="lblCurrentTotalModuleDownloads" runat="server" CssClass="labelContent" />
	<asp:HiddenField ID="hdnCurrentTotalModuleDownloads" runat="server" Visible="false" />
</div>
<hr />
<h2>Theme</h2>
<div class="divForm">
    <label class="label">Report Theme:</label>
    <asp:DropDownList ID="ddlReportTheme" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    <p>(Note: Setting applies to notification email branding.)</p>
</div> 

<asp:LinkButton CssClass="lnkButton" ID="btnUpdate" runat="server" ValidationGroup="UpdateAccountSettings" />
