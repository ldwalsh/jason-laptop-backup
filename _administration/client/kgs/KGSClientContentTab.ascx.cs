﻿using System;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;

namespace CW.Website._administration.client.kgs
{
    public partial class KGSClientContentTab: EditTabBase<ClientSetting, KGSClientAdministration>
    {
        #region method

            private void Page_FirstInit()
            {
                BindClients(ddlCurrentClient);

                ddlCurrentClient.SelectedIndex = 0;

                ddlCurrentClient_OnSelectedIndexChanged(null, null);
            }

            private void Page_Init()
            {
                CanEntityBeUpdated = (ClientSetting setting, out String message) => {message = null; return true;};
                UpdateEntityAction = DataMgr.ClientDataMapper.UpdateClientContentSettings;

				OnTabStateChanged += delegate{Page_FirstInit();};
            }

            protected void ddlCurrentClient_OnSelectedIndexChanged(Object sender, EventArgs e)
            {
                clientContentForm.Visible = (ddlCurrentClient.SelectedIndex != 0);

                if (ddlCurrentClient.SelectedIndex == 0) return;

                PopulateForm(GetEntityForEditing);
            }

        #endregion

        #region property

            protected override ClientSetting GetEntityForEditing
            {
                get {return DataMgr.ClientDataMapper.GetClientSettingsByClientID(Convert.ToInt32(ddlCurrentClient.SelectedValue));}
            }

        #endregion
    }
}