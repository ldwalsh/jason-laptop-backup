﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ClientThemeForm.ascx.cs" Inherits="CW.Website._administration.client.kgs.ClientThemeForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
                                          
<hr />
<h2>Theming</h2>
<asp:HiddenField ID="hdnClientSettingsID" runat="server" Visible="false" />
<div class="divForm">
    <label class="label">Themed Provider:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlThemedProvider" AppendDataBoundItems="true" runat="server">                
    </asp:DropDownList>
</div>
<asp:LinkButton CssClass="lnkButton" ID="btnUpdate" runat="server" ValidationGroup="UpdateThemeSettings" />
