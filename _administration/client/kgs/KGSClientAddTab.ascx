﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSClientAddTab.ascx.cs" Inherits="CW.Website._administration.client.kgs.KGSClientAddTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="../ClientForm.ascx" tagPrefix="CW" tagName="ClientForm" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Client" />
<CW:ClientForm runat="server" ID="clientForm" FormMode="Insert" UpdateButtonCaption="Add Client" OOTIDEnabled="true" IsProxyEnabled="true" ClientNameEnabled="true" OrganizationEnabled="true"/>

