﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSClientAccountTab.ascx.cs" Inherits="CW.Website._administration.client.kgs.KGSClientAccountTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="ClientAccountForm.ascx" tagPrefix="CW" tagName="ClientAccountForm" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Account Settings" />
<div class="divForm" runat="server">   
    <label class="label">*Select Client:</label>    
    <asp:DropDownList ID="ddlCurrentClient" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlClient_OnSelectedIndexChanged" AutoPostBack="true">
        <asp:ListItem>Select one...</asp:ListItem>                           
    </asp:DropDownList> 
</div>
<CW:ClientAccountForm runat="server" ID="clientAccountForm" Visible="false" UpdateButtonCaption="Update" FormMode="Update" />
