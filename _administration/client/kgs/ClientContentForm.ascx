﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ClientContentForm.ascx.cs" Inherits="CW.Website._administration.client.kgs.ClientContentForm" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:HiddenField ID="hdnClientSettingsID" runat="server" Visible="false" />
<hr />
<div class="divFormLeftmost">
    <label class="textareaWideLabel">Home Page Header:</label>
    <textarea id="areaHomePageHeader" runat="server" class="textareaWide" name="txtHomePageHeader" cols="80" onkeyup="limitChars(this, 100, 'divHomePageHeaderCharInfo')" rows="5"></textarea>
    <div id="divHomePageHeaderCharInfo"></div>
</div>
<div class="divFormLeftmost">
    <label>Home Page Body (Max HTML Characters = 5000):</label>                                                                                               
    <telerik:RadEditor ID="editorHomePageBody" runat="server" CssClass="editorNewLine" Height="475px" Width="674px" MaxHtmlLength="5000" NewLineMode="Div" ToolsWidth="676px" ToolbarMode="ShowOnFocus" ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"> 
        <CssFiles>
            <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
        </CssFiles>
    </telerik:RadEditor>
    <div id="divEditorHomePageBody" class="textareaWideCharacterDiv"></div>
</div>
<div class="divFormLeftmost">
    <label>News Body (Max HTML Characters = 5000):</label>
    <telerik:RadEditor ID="editorNewsBody" runat="server" CssClass="editorNewLine" Height="475px" Width="674px" MaxHtmlLength="5000" NewLineMode="Div" ToolsWidth="676px" ToolbarMode="ShowOnFocus" ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags">  
        <CssFiles>
            <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
        </CssFiles>                                                                                 
    </telerik:RadEditor>
    <div id="divEditorNewsBody" class="textareaWideCharacterDiv"></div>
</div>
<asp:LinkButton ID="btnUpdate" runat="server" CssClass="lnkButton" />
