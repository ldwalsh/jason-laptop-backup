﻿using CW.Business;
using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._administration.client.kgs
{
    public partial class ClientAccountForm: FormGenericBase<ClientAccountForm,ClientSetting>
    {
		#region method

			#region override

				public override void PopulateForm(ClientSetting setting)
				{
                    //tab.BindAllNonRestrictedAdminRoles(ddlAutomatedDataSourceHealthEmails);

                    GenericListControlBind(ddlReportTheme, BusinessConstants.Theme.ReportThemes, "Value", "Key");
                    
                    BindPublicUsersByClient(setting.CID, ddlReferringDomainAutoLoginUserID);

                    base.PopulateForm(setting);
                   
                    reportCriteriaMonthlyPriority.ClearForm();
                    reportCriteriaMonthlyPriority.UserGroupAccessLevel = ReportAssignmentCriteria.UserGroupAccessLevels.KGS;
                    reportCriteriaMonthlyPriority.ReportType = ReportAssignmentCriteria.ReportTypes.Client; 
                    reportCriteriaMonthlyPriority.IsActive = setting.AutomatedPriorityMonthlyEmails;
                    reportCriteriaMonthlyPriority.UserGroups = DataMgr.UserGroupDataMapper.GetUserGroupIDsByGUID(setting.AutomatedPriorityMonthlyEmailsUserGroupsGUID);
                    reportCriteriaMonthlyPriority.GUID = setting.AutomatedPriorityMonthlyEmailsUserGroupsGUID;
                    reportCriteriaMonthlyPriority.CID = setting.CID;
                    reportCriteriaMonthlyPriority.CurrentUID = siteUser.UID;
                    reportCriteriaMonthlyPriority.AnalysisRange = DataConstants.AnalysisRange.Monthly;
                    reportCriteriaMonthlyPriority.BindForm();
                    
                    reportCriteriaWeeklyPriority.ClearForm();
                    reportCriteriaWeeklyPriority.UserGroupAccessLevel = ReportAssignmentCriteria.UserGroupAccessLevels.KGS;
                    reportCriteriaWeeklyPriority.ReportType = ReportAssignmentCriteria.ReportTypes.Client; 
                    reportCriteriaWeeklyPriority.IsActive = setting.AutomatedPriorityWeeklyEmails;
                    reportCriteriaWeeklyPriority.UserGroups = DataMgr.UserGroupDataMapper.GetUserGroupIDsByGUID(setting.AutomatedPriorityWeeklyEmailsUserGroupsGUID);
                    reportCriteriaWeeklyPriority.GUID = setting.AutomatedPriorityWeeklyEmailsUserGroupsGUID;
                    reportCriteriaWeeklyPriority.CID = setting.CID;
                    reportCriteriaWeeklyPriority.CurrentUID = siteUser.UID;
                    reportCriteriaWeeklyPriority.AnalysisRange = DataConstants.AnalysisRange.Weekly;
                    reportCriteriaWeeklyPriority.BindForm();

                    reportCriteriaDailyPriority.ClearForm();
                    reportCriteriaDailyPriority.UserGroupAccessLevel = ReportAssignmentCriteria.UserGroupAccessLevels.KGS;
                    reportCriteriaDailyPriority.ReportType = ReportAssignmentCriteria.ReportTypes.Client; 
                    reportCriteriaDailyPriority.IsActive = setting.AutomatedPriorityDailyEmails;
                    reportCriteriaDailyPriority.UserGroups = DataMgr.UserGroupDataMapper.GetUserGroupIDsByGUID(setting.AutomatedPriorityDailyEmailsUserGroupsGUID);
                    reportCriteriaDailyPriority.GUID = setting.AutomatedPriorityDailyEmailsUserGroupsGUID;
                    reportCriteriaDailyPriority.CID = setting.CID;
                    reportCriteriaDailyPriority.CurrentUID = siteUser.UID;
                    reportCriteriaDailyPriority.AnalysisRange = DataConstants.AnalysisRange.Daily;
                    reportCriteriaDailyPriority.BindForm();

                    divAutoDataSourceHealthEmails.Visible = chkHasAutomatedDataSourceHealthEmailsHalfDay.Checked || chkHasAutomatedDataSourceHealthEmailsEndOfDay.Checked;
                    hdnAutomatedDataSourceHealthEmailsGuid.Value = setting.AutomatedDataSourceHealthEmailsUserGroupsGUID.ToString();
                    divReferringDomainAutoLogin.Visible = chkEnableReferringDomainAutoLogin.Checked;

                    //datasource health emails
                        int clientOID = DataMgr.OrganizationDataMapper.GetOIDByCID(setting.CID);
                        IEnumerable<ProvidersClients> providerClients = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByCID(setting.CID);
                        IEnumerable<UserGroup> fullUserGroupSet = Enumerable.Empty<UserGroup>();
                        List<int> assignedUserGroupIDs = new List<int>();
                        assignedUserGroupIDs = DataMgr.UserGroupDataMapper.GetUserGroupIDsByGUID(setting.AutomatedDataSourceHealthEmailsUserGroupsGUID); ;

                        //get all user groups for that client, all providers for that client, and for kgs.
                        fullUserGroupSet = DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(clientOID, null)
                            .Union(DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(BusinessConstants.Organization.KGSBuildingsOID, null));

                        foreach (ProvidersClients pc in providerClients)
                        {
                            fullUserGroupSet = fullUserGroupSet.Union(DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(DataMgr.OrganizationDataMapper.GetOrganizationByProviderID(pc.ProviderID).OID, null));
                        }

                        //set as distinct
                        fullUserGroupSet = fullUserGroupSet.Distinct(new UserGroupDataMapper.UserGroupComparer()).ToList();

                        BindUserGroups(lbDataSourceUserGroupsTop, fullUserGroupSet.Where(ug => !assignedUserGroupIDs.Contains(ug.UserGroupID)));
                        BindUserGroups(lbDataSourceUserGroupsBottom, fullUserGroupSet.Where(ug => assignedUserGroupIDs.Contains(ug.UserGroupID)));                  
                    }

                public override void CreateEntity(ClientSetting entity)
                {
                    base.CreateEntity(entity);

                    entity.AutomatedPriorityMonthlyEmails = reportCriteriaMonthlyPriority.IsActive;
                    entity.AutomatedPriorityWeeklyEmails = reportCriteriaWeeklyPriority.IsActive;
                    entity.AutomatedPriorityDailyEmails = reportCriteriaDailyPriority.IsActive;

                    entity.HasAutomatedDataSourceHealthEmailsEndOfDay = chkHasAutomatedDataSourceHealthEmailsEndOfDay.Checked;
                    entity.HasAutomatedDataSourceHealthEmailsHalfDay = chkHasAutomatedDataSourceHealthEmailsHalfDay.Checked;                    
                }
 
			#endregion

			private void Page_Load()
			{
				OnFieldBind = FieldBind;
				OnFieldSave = FieldSave;
			}

			private Boolean CanClientSettingsBePersisted(ClientSetting setting, out String message)
			{
				message = null;

				return true;
			}

			private void FieldBind(Control control, ref Object value)
			{
                var isMax = control.ID.StartsWith("txtMax") || control.ID.StartsWith("rngMax");

				if ((isMax || control.ID.StartsWith("lblCurrent")) && !control.ID.Contains("Report"))
				{
					value = MathHelper.ConvertBytesToMB(Convert.ToInt64(value), (isMax ? 0 : 1));
				}
			}

            private void FieldSave(Control control, ref Object value)
            {
                var isMax = control.ID.StartsWith("txtMax") || control.ID.StartsWith("rngMax");

                if (isMax && !control.ID.Contains("Report"))
                {
                    value = MathHelper.ConvertMBToBytes(Convert.ToInt32(value));
                }                
                else if (control.ID == "txtReferringDomainAutoLoginDomains")
                {
                    //remove csv whitespace
                    value = value.ToString().Replace(" ", string.Empty);
                }
            }
            
            private void BindUserGroups(ListBox lb, IEnumerable<UserGroup> dataSource)
            {
                GenericListControlBind(lb, dataSource, "UserGroupName", "UserGroupID");
            }

            private void BindPublicUsersByClient(int cid, DropDownList ddl)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                GenericListControlBind(ddl, DataMgr.UserDataMapper.GetUsersClientsUsersByRole(cid, Convert.ToInt32(BusinessConstants.UserRole.UserRoleEnum.PublicUser)), "Email", "UID", new[]{lItem});
            }

            protected void chkHasAutomatedDataSourceHealthEmails_OnCheckedChanged(object sender, EventArgs e)
            {
                divAutoDataSourceHealthEmails.Visible = chkHasAutomatedDataSourceHealthEmailsHalfDay.Checked || chkHasAutomatedDataSourceHealthEmailsEndOfDay.Checked;
            }

            protected void chkEnableReferringDomainAutoLogin_OnCheckedChanged(object sender, EventArgs e)
            {
                divReferringDomainAutoLogin.Visible = chkEnableReferringDomainAutoLogin.Checked;
            }

            /// <summary>
            /// on button up click, remove user groups from bottom listbox
            /// </summary>
            protected void btnDataSourceUserGroupsUpButton_Click(object sender, EventArgs e)
            {
                while (lbDataSourceUserGroupsBottom.SelectedIndex != -1)
                {
                    lbDataSourceUserGroupsTop.Items.Add(lbDataSourceUserGroupsBottom.SelectedItem);
                    lbDataSourceUserGroupsBottom.Items.Remove(lbDataSourceUserGroupsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add user groups to bottom listbox
            /// </summary>
            protected void btnDataSourceUserGroupsDownButton_Click(object sender, EventArgs e)
            {
                while (lbDataSourceUserGroupsTop.SelectedIndex != -1)
                {
                    lbDataSourceUserGroupsBottom.Items.Add(lbDataSourceUserGroupsTop.SelectedItem);
                    lbDataSourceUserGroupsTop.Items.Remove(lbDataSourceUserGroupsTop.SelectedItem);
                }
            }

            protected void UpdateClientAccountSettings(ClientSetting cs)
            {
                try
                {
                    Guid dsGuid = new Guid(hdnAutomatedDataSourceHealthEmailsGuid.Value);

                    //delete
                    DataMgr.UserGroupDataMapper.DeleteUserGroupCollectionAssociationsByGuids(new Guid[] { reportCriteriaDailyPriority.GUID, reportCriteriaWeeklyPriority.GUID, reportCriteriaMonthlyPriority.GUID, dsGuid });

                    //re insert all
                    DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups((from g in reportCriteriaDailyPriority.UserGroups select new UserGroupCollectionLookup_UserGroup { GUID = reportCriteriaDailyPriority.GUID, UserGroupID = g }).ToList());
                    DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups((from g in reportCriteriaWeeklyPriority.UserGroups select new UserGroupCollectionLookup_UserGroup { GUID = reportCriteriaWeeklyPriority.GUID, UserGroupID = g }).ToList());
                    DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups((from g in reportCriteriaMonthlyPriority.UserGroups select new UserGroupCollectionLookup_UserGroup { GUID = reportCriteriaMonthlyPriority.GUID, UserGroupID = g }).ToList());

                    //datasources
                    List<UserGroupCollectionLookup_UserGroup> ugs = new List<UserGroupCollectionLookup_UserGroup>();

                    foreach (ListItem item in lbDataSourceUserGroupsBottom.Items)
                    {
                        UserGroupCollectionLookup_UserGroup ug = new UserGroupCollectionLookup_UserGroup { UserGroupID = Convert.ToInt32(item.Value), GUID = dsGuid };
                        ugs.Add(ug);
                    }

                    DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups(ugs);

                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Unable to update client account usergroup related settings.", ex);
                }

                DataMgr.ClientDataMapper.UpdateClientAccountSettings(cs);
            }

        #endregion

		#region property

			#region override

				public override Delegates<ClientSetting>.CanEntityBePersistedDelegate CanEntityBePersisted
				{
					get {return CanClientSettingsBePersisted;}
				}

                public override Action<ClientSetting> PersistEntityAction
                {
                    get {return UpdateClientAccountSettings;}
                }

			#endregion

		#endregion
    }
}