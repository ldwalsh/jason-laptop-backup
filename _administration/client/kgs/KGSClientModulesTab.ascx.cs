﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._administration.client.kgs
{
    public partial class KGSClientModulesTab: EditTabBase<Client,KGSClientAdministration>
    {
		#region method

			#region override

				protected override void PopulateForm(Client client)
				{
					var CID = Convert.ToInt32(ddlCurrentClient.SelectedValue);

                    lbAvailable.DataSource      = DataMgr.ModuleDataMapper.GetAllActiveModulesNotAssociatedToClientID(CID);
                    lbAvailable.DataTextField   = PropHelper.G<Module>(m => m.ModuleName);
                    lbAvailable.DataValueField  = PropHelper.G<Module>(m => m.ModuleID);
                
					lbAvailable.DataBind();

                    lbClients_Modules.DataSource      = DataMgr.ModuleDataMapper.GetAllActiveModulesAssociatedToClientID(CID);
                    lbClients_Modules.DataTextField   = PropHelper.G<Module>(m => m.ModuleName);
                    lbClients_Modules.DataValueField  = PropHelper.G<Module>(m => m.ModuleID);

					lbClients_Modules.DataBind();
				}

                protected override void PersistEntity(Client client)
                {
                    var CID = Convert.ToInt32(ddlCurrentClient.SelectedValue);

					String message;

					try
					{
						//add modules that are new
						foreach (ListItem item in lbClients_Modules.Items)
						{
							var moduleID = Convert.ToInt32(item.Value);


                            if (!DataMgr.ModuleDataMapper.DoesModuleExistForClient(CID, moduleID))
                            {
                                DataMgr.ModuleDataMapper.InsertClientModule(new Clients_Module { CID = CID, ModuleID = moduleID });
                            }
                        }


						foreach (ListItem item in lbAvailable.Items)
						{
							var moduleID = Convert.ToInt32(item.Value);

                            if (DataMgr.ModuleDataMapper.DoesModuleExistForClient(CID, moduleID))
                            {
                                //delete quick links for users with client and module ids
                                DataMgr.QuickLinksDataMapper.DeleteAllUserClientQuickLinksByClientIDAndModuleID(CID, moduleID);


                                //then delete the module for the client
                                DataMgr.ModuleDataMapper.DeleteModuleByClientIDAndModuleID(CID, moduleID);


                                //delete module for users if restricted---
                                var usersClientsList = DataMgr.UserClientDataMapper.GetUsersClientsByCID(CID).ToList();
                                var usersClientsListUniqueUsers = usersClientsList.GroupBy(g => new { g.UID })
                                                                        .Select(s => new UsersClients
                                                                        {
                                                                            UID = s.First().UID,
                                                                            PrimaryRoleID = s.First().PrimaryRoleID,
                                                                            SecondaryRoleID = s.First().SecondaryRoleID
                                                                        });
                                foreach (UsersClients uc in usersClientsListUniqueUsers)
                                {
                                    var userClientsModuleRestrictedList = usersClientsList.Where(u => u.ModuleID == moduleID && u.UID == uc.UID).ToList();
                                    var userClientsBuildingRestrictedList = usersClientsList.Where(u => (u.BID != 0 || u.BuildingGroupID != 0) && u.UID == uc.UID).ToList();

                                    //delete all userclients for cid, uid, and moduleid
                                    DataMgr.UserClientDataMapper.DeleteUsersClients(userClientsModuleRestrictedList);
                                   
                                    //if the user was in the restricted list, all module restrictions that existed and are now removed, set the user back to full mode if buidings and building groups are not restricted.
                                    if (userClientsBuildingRestrictedList.Where(u => u.UID == uc.UID).Any() && !userClientsModuleRestrictedList.Where(u => u.UID == uc.UID && uc.ModuleID != moduleID).ToList().Any() && !userClientsBuildingRestrictedList.Any())
                                    {
                                        DataMgr.UserClientDataMapper.AddUsersClients(new UsersClients() { CID = CID, UID = uc.UID, Guid = new Guid(), BID = 0, BuildingGroupID = 0, ModuleID = 0, PrimaryRoleID = uc.PrimaryRoleID, SecondaryRoleID = (int)uc.SecondaryRoleID, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow });
                                    }
                                }
                            }
                        }

                        message = "Client modules update successful.";
                    }
                    catch(Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, InsertEntityTypeName("Error updating {EntityTypeName}."), ex);
                        
						message = "Client modules update failed. Please contact an administrator.";
					}

					DisplayMessage(message);
				}

			#endregion

			private void Page_FirstInit()
			{
				BindClients(ddlCurrentClient);

				ddlCurrentClient.SelectedIndex = 0;

				ddlCurrentClient_OnSelectedIndexChanged(null, null);
			}

			private void Page_Init()
			{
				OnTabStateChanged += delegate{Page_FirstInit();};
			}
            
			protected void ddlCurrentClient_OnSelectedIndexChanged(Object sender, EventArgs e)
			{
				divModules.Visible = (ddlCurrentClient.SelectedIndex != 0);

				if (ddlCurrentClient.SelectedIndex == 0) return;

                PopulateForm(GetEntityForEditing);
            }

			protected void btnUpButton_Click(Object sender, EventArgs e)
			{
				while (lbClients_Modules.SelectedIndex != -1)
				{
					lbAvailable.Items.Add(lbClients_Modules.SelectedItem);

					lbClients_Modules.Items.Remove(lbClients_Modules.SelectedItem);
				}
			}

			protected void btnDownButton_Click(Object sender, EventArgs e)
			{
				while (lbAvailable.SelectedIndex != -1)
				{
					lbClients_Modules.Items.Add(lbAvailable.SelectedItem);

					lbAvailable.Items.Remove(lbAvailable.SelectedItem);
				}
			}

		#endregion
                    
		#region property

            protected override Client GetEntityForEditing
            {
                get {return null;}
            }

		#endregion
    }
}