﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" AutoEventWireup="false" CodeBehind="KGSClientAdministration.aspx.cs" Inherits="CW.Website._administration.client.kgs.KGSClientAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="KGSClientViewTab.ascx" tagPrefix="CW" tagName="ClientViewTab" %>
<%@ Register src="KGSClientAddTab.ascx" tagPrefix="CW" tagName="ClientAddTab" %>
<%@ Register src="KGSClientModulesTab.ascx" tagPrefix="CW" tagName="ClientModulesTab" %>
<%@ Register src="KGSClientContentTab.ascx" tagPrefix="CW" tagName="ClientContentTab" %>
<%@ Register src="KGSClientAccountTab.ascx" tagPrefix="CW" tagName="ClientAccountTab" %>
<%@ Register src="KGSClientThemeTab.ascx" tagPrefix="CW" tagName="ClientThemeTab" %>
<%@ Register src="~/_administration/client/ClientCMMSSettingsTab.ascx" tagPrefix="CW" tagName="ClientCMMSSettingsTab" %>
<%@ Register src="~/_administration/client/ClientGoalsSettingsTab.ascx" tagPrefix="CW" tagName="ClientGoalsSettingsTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   

    <script type="text/javascript" src="/_administration/_script/TabStateMonitor.js?v=_assets/"></script>

    <CW:PageHeader runat="server" Title="KGS Client Administration" Description="The kgs client administration area is used to view, add, and edit clients. Additionally you can assign modules, alter client content, set account settings, and associate a provider theme." />
    <div class="administrationControls">
        <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage"> 
            <Tabs>
                <telerik:RadTab Text="View Clients" />
                <telerik:RadTab Text="Add Clients" />
                <telerik:RadTab Text="Client Modules" />
                <telerik:RadTab Text="Client Content Settings" />
                <telerik:RadTab Text="Client Account Settings" />
                <telerik:RadTab Text="Client Theme Settings" />
                <telerik:RadTab Text="Client WO Settings" />
                <telerik:RadTab Text="Client Goal Settings" />
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
            <telerik:RadPageView runat="server">
                <CW:ClientViewTab runat="server"/>
            </telerik:RadPageView>
            <telerik:RadPageView runat="server">
                <CW:ClientAddTab runat="server" />
            </telerik:RadPageView>                
            <telerik:RadPageView runat="server">
                <CW:ClientModulesTab runat="server" ManualBind="true" />
            </telerik:RadPageView>                
            <telerik:RadPageView runat="server">
                <CW:ClientContentTab runat="server" ManualBind="true" />
            </telerik:RadPageView>                
            <telerik:RadPageView runat="server">
				<CW:ClientAccountTab runat="server" ManualBind="true" />
            </telerik:RadPageView>                
            <telerik:RadPageView runat="server">
                <CW:ClientThemeTab runat="server" ManualBind="true" />
            </telerik:RadPageView>  
            <telerik:RadPageView runat="server">
                <CW:ClientCMMSSettingsTab ID="ClientCMMSSettingsTab" runat="server" />
            </telerik:RadPageView>         
            <telerik:RadPageView runat="server">
                <CW:ClientGoalsSettingsTab ID="ClientGoalsSettingsTab" runat="server" />
            </telerik:RadPageView>           
        </telerik:RadMultiPage>
    </div>

</asp:Content>
