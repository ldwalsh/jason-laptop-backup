﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSClientModulesTab.ascx.cs" Inherits="CW.Website._administration.client.kgs.KGSClientModulesTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Client Modules">
    <SubTitleTemplate>Please select a client in order to select and assign modules.</SubTitleTemplate>
</CW:TabHeader>
<div class="divForm">
    <label class="label">*Select Client:</label>
    <asp:DropDownList ID="ddlCurrentClient" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCurrentClient_OnSelectedIndexChanged" AutoPostBack="true">
        <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
    </asp:DropDownList> 
</div>
<div id="divModules" runat="server">
    <hr /> 
    <p>All modules are avaialable to a client. Please select only which apply.</p>
    <div class="divForm">
        <label class="label">Available:</label>
        <asp:ListBox ID="lbAvailable" runat="server" CssClass="listbox" SelectionMode="Multiple" />
        <div class="divArrows">
            <asp:ImageButton ID="btnUp" runat="server" CssClass="up-arrow" OnClick="btnUpButton_Click" ImageUrl="/_assets/images/up-arrow.png" />
            <asp:ImageButton ID="btnDown" runat="server" CssClass="down-arrow" OnClick="btnDownButton_Click" ImageUrl="/_assets/images/down-arrow.png" />
        </div>
        <label class="label">Assigned:</label>
        <asp:ListBox ID="lbClients_Modules" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>
    <asp:LinkButton ID="btnUpdate" runat="server" CssClass="lnkButton">Reassign</asp:LinkButton>
</div>