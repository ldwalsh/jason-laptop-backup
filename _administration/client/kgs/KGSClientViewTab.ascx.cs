﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._administration.client.kgs
{
	public partial class KGSClientViewTab: ViewTabWithFormBase<Client,KGSClientAdministration>
	{
		#region method

			private void Page_Init()
			{
				CanEntityBeUpdated = clientForm.CanEntityBePersisted;
				CanEntityBeDeleted = CanClientBeDeleted;

				UpdateEntityMethod = clientForm.PersistEntityAction;
				DeleteEntityMethod = DeleteClient;
			}

			private Boolean CanClientBeDeleted(String cid, out String message)
			{
				var id = Convert.ToInt32(cid);

				if (DataMgr.BuildingDataMapper.AreBuildingsAssociatedToClient(id))
				{
					message = "Cannot delete client because it has one or more buildings associated to it.";

					return false;
				}

				if (DataMgr.ProviderClientDataMapper.GetProvidersClientsList().Any(p => p.CID == id))
				{
					message = "Cannot delete client because it has one or more providers associated to it.";

					return false;
				}

                if (DataMgr.ClientDataMapper.GetClient(id).IsPrimary && DataMgr.UserDataMapper.GetAllUsersByCID(id).Any())
                {
                    message = "Cannot delete a primary client whose organization has users.";

                    return false;
                }
					
				message = null;

				return true;
			}

			protected void DeleteClient(String cid)
			{
                DataMgr.GoalDataMapper.RemoveClientGoalsByCID(Int32.Parse(cid));

				DataMgr.ClientDataMapper.DeleteClient(Int32.Parse(cid));

				page.TabStateMonitor.ChangeState(DeleteChangeStateEnums); //move to base eventually? does not belong here
			}

			private Client GetClientForDetailView(String cid)
			{
                return DataMgr.ClientDataMapper.GetClient(Convert.ToInt32(cid), (_=>_.State), (_=>_.Country), (_=>_.ClientSettings));
			}

            protected String GetThemedProviderName(Int32 themedProvider)
            {
                return ((themedProvider == 0) ? "None" : DataMgr.ProviderDataMapper.GetProviderByID(themedProvider).ProviderName);
            }

	    #endregion

		#region property

			protected override IEnumerable<String> GridColumnNames
			{
				get {return PropHelper.F<Client>(_=>_.ClientName, _=>_.ClientAddress, _=>_.ClientCity, _=>_.Country.CountryName, _=>_.IsActive);}
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get {return (searchText => DataMgr.ClientDataMapper.GetAllSearchedClients(searchText.FirstOrDefault(), (_=>_.State), (_=>_.Country)));}
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get {return (cid=>DataMgr.ClientDataMapper.GetClient(Convert.ToInt32(cid), (_=>_.State), (_=>_.Organizations_OrganizationTypeName)));}
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get {return GetClientForDetailView;}
			}

			protected override String CacheSubKey
			{
				get {return null;}
			}

		#endregion
	}
}