﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSClientThemeTab.ascx.cs" Inherits="CW.Website._administration.client.kgs.KGSClientThemeTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="ClientThemeForm.ascx" tagPrefix="CW" tagName="ClientThemeForm" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Theme Settings">
    <SubTitleTemplate>Please select a client in order to select and assign a themed provider. In the event of a change or new association you must log out and login again to view the theme.</SubTitleTemplate>
</CW:TabHeader>
<div class="divForm" runat="server">   
    <label class="label">*Select Client:</label>    
    <asp:DropDownList ID="ddlCurrentClient" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlClient_OnSelectedIndexChanged" AutoPostBack="true">
        <asp:ListItem>Select one...</asp:ListItem>                           
    </asp:DropDownList> 
</div>
<CW:ClientThemeForm runat="server" ID="clientThemeForm" Visible="false" UpdateButtonCaption="Update" FormMode="Update" />
