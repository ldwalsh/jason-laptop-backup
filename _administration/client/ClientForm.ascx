﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ClientForm.ascx.cs" Inherits="CW.Website._administration.client.ClientForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="FileUploaderControl" Assembly="FileUploaderControl" %>

<asp:HiddenField ID="hdnCID" runat="server" Visible="false" />
<div class="divForm">
    <label class="label">*Organization:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlOrganization" AppendDataBoundItems="true" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOrganization_OnSelectedIndexChanged" />
</div>
<div class="divForm">
    <label class="label">*Client Name:</label>
    <asp:TextBox ID="txtClientName" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divForm">
    <label class="label">*Primary:</label>
    <asp:CheckBox ID="chkIsPrimary" CssClass="checkbox" runat="server" AutoPostBack="true" />
    <label runat="server" ID="lblPrimaryNote" />
</div>
<div class="divForm">
    <label class="label">*Proxy:</label>
    <asp:CheckBox ID="chkIsProxy" CssClass="checkbox" runat="server" />
    <p>(Note: Providers have administrative access to assigned proxy clients user administration, client administration, and settings pages.)</p>
</div>
<div class="divForm">
    <label class="label">Address:</label>
    <asp:TextBox ID="txtAddress" CssClass="textbox" MaxLength="200" runat="server" />
</div>
<div class="divForm">
    <label class="label">City:</label>
    <asp:TextBox CssClass="textbox" ID="txtCity" runat="server" MaxLength="100" />
</div>
<div class="divForm">
    <label class="label">*Country:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlCountryAlpha2Code" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                
    </asp:DropDownList>
</div>
<div class="divForm">
    <label class="label">State (Required based on Country):</label>
    <asp:DropDownList ID="ddlStateID" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
    </asp:DropDownList>                                                                                                                                                              
</div>
<div class="divForm">
    <label class="label">Zip Code (Required based on Country):</label>
    <telerik:RadMaskedTextBox ID="txtZip" CssClass="textbox" runat="server" 
        PromptChar="" Mask="aaaaaaaaaa" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>   
<div class="divForm">
    <label class="label">Phone:</label>
    <telerik:RadMaskedTextBox ID="txtPhone" CssClass="textbox" runat="server"
        Mask="(###)###-####" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>  
<div class="divForm">
    <label class="label">Fax:</label>
    <telerik:RadMaskedTextBox ID="txtFax" CssClass="textbox" runat="server"
        Mask="(###)###-####" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>  
<div class="divForm">
    <label class="label">Primary Contact:</label>
    <asp:TextBox ID="txtPrimaryContactName" CssClass="textbox" MaxLength="15" runat="server" />
</div>
<div class="divForm">
    <label class="label">Primary Contact Email:</label>
    <asp:TextBox ID="txtPrimaryContactEmail" CssClass="textbox" MaxLength="100" runat="server" />
</div>
<div class="divForm">
    <label class="label">Primary Contact Phone:</label>
    <telerik:RadMaskedTextBox ID="txtPrimaryContactPhone" CssClass="textbox" runat="server"
        Mask="(###)###-####" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>
<div class="divForm">
    <label class="label">Secondary Contact:</label>
    <asp:TextBox ID="txtSecondaryContactName" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divForm">
    <label class="label">Secondary Contact Email:</label>
    <asp:TextBox ID="txtSecondaryContactEmail" CssClass="textbox" MaxLength="100" runat="server" />
</div>
<div class="divForm">
    <label class="label">Secondary Contact Phone:</label>
    <telerik:RadMaskedTextBox ID="txtSecondaryContactPhone" CssClass="textbox" runat="server"
        Mask="(###)###-####" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>
<asp:HiddenField runat="server" ID="hdnImageExtension" Visible="false" />
<div class="divForm">
    <label class="label">Image (800x600):</label>
    <CW:FileUploader runat="server" ID="imageFileUploader" />
</div>
<div class="divForm" runat="server">
    <label class="label">*Active:</label>
    <asp:CheckBox ID="chkIsActive" CssClass="checkbox" runat="server" />
    <p>(Note: Setting a client as inactive will hide the client from its organization.)</p>
    <p>(Warning: Setting a client as inactive will not allow scheduled analyses to be put in the queue, and no incoming data will be recorded.)</p>
</div>

<asp:Panel ID="pnlKioskInformation" runat="server">
    <hr />
    <h2 id="kioskInformationHeaderText" runat="server"></h2>
    <div class="divForm">
            <label class="label">Kiosk Content:</label>
            <textarea name="txtKioskContent" id="txtKioskContent" cols="40" rows="5" onkeyup="limitChars(this, 500, 'divKioskContentCharInfo')" runat="server"></textarea>
            <div id="divKioskContentCharInfo"></div>                                                         
    </div>
    <div class="divForm">
            <label class="label">Kiosk QR Content:</label>
            <textarea name="txtKioskQRContent" id="txtKioskQRContent" cols="40" rows="5" onkeyup="limitChars(this, 250, 'divKioskQRContentCharInfo')" runat="server"></textarea>
            <div id="divKioskQRContentCharInfo"></div>                                                         
    </div>
    <div class="divForm">
            <label class="label">Kiosk QR Link:</label>
            <asp:TextBox CssClass="textbox" ID="txtKioskQRLink" runat="server" MaxLength="100"></asp:TextBox>
    </div>
</asp:Panel>

<asp:Panel ID="pnlInternalInformation" runat="server">
    <hr />
    <h2 id="internalInformationHeaderText" runat="server"></h2>
                  
    <div class="divForm">
        <label class="label">Internal Notes:</label>
        <textarea name="txtInternalNotes" id="txtInternalNotes" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divInternalNotesCharInfo')" runat="server"></textarea>
        <div id="divInternalNotesCharInfo"></div>                                                         
    </div>
    <div class="divForm">
        <label class="label">Sales Contact Name:</label>
        <asp:TextBox CssClass="textbox" ID="txtSalesContactName" runat="server" MaxLength="100"></asp:TextBox>
    </div>   
</asp:Panel>

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />
                                                
<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="organizationValidator" runat="server" ErrorMessage="Parent organization is a required field." ControlToValidate="ddlOrganization" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="organizationValidatorExtension" runat="server" TargetControlID="OrganizationValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="clientNameRequiredValidator" runat="server" ErrorMessage="Client Name is a required field." ControlToValidate="txtClientName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="clientNameRequiredValidatorExtender" runat="server" TargetControlID="ClientNameRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="countryRequiredValidator" runat="server" ErrorMessage="Country is a required field." ControlToValidate="ddlCountryAlpha2Code" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="countryRequiredValidatorExtender" runat="server" TargetControlID="CountryRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="stateRequiredValidator" runat="server" ErrorMessage="State is a required field." ControlToValidate="ddlStateID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="stateRequiredValidatorExtender" runat="server" TargetControlID="StateRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="zipRequiredValidator" runat="server"
    ErrorMessage="Zip Code is a required field."
    ControlToValidate="txtZip"          
    SetFocusOnError="true"
    Display="None"
    ValidationGroup=""
    Enabled="false">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="zipRequiredValidatorExtender" runat="server"
    TargetControlID="zipRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="zipRegExValidator" runat="server"
    ErrorMessage="Invalid Zip Format."
    ValidationExpression="\d{5}"
    ControlToValidate="txtZip"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup=""
    Enabled="false">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="zipRegExValidatorExtender" runat="server"
    TargetControlID="zipRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender> 
<asp:RegularExpressionValidator ID="phoneRegExValidator" runat="server"
    ErrorMessage="Invalid Phone Format."
    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
    ControlToValidate="txtPhone"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup="">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="phoneRegExValidatorExtender" runat="server"
    TargetControlID="phoneRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="faxRegExValidator" runat="server"
    ErrorMessage="Invalid Fax Format."
    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
    ControlToValidate="txtFax"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup="">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="faxRegExValidatorExtender" runat="server"
    TargetControlID="faxRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="primaryEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtPrimaryContactEmail" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="primaryEmailRegExValidatorExtender" runat="server" TargetControlID="PrimaryEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RegularExpressionValidator ID="primaryPhoneRegExValidator" runat="server"
    ErrorMessage="Invalid Phone Format."
    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
    ControlToValidate="txtPrimaryContactPhone"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup="">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="primaryPhoneRegExValidatorExtender" runat="server"
    TargetControlID="primaryPhoneRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="secondaryEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtSecondaryContactEmail" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="secondaryEmailRegExValidatorExtender" runat="server" TargetControlID="SecondaryEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RegularExpressionValidator ID="secondaryPhoneRegExValidator" runat="server"
    ErrorMessage="Invalid Phone Format."
    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
    ControlToValidate="txtSecondaryContactPhone"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup="">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="secondaryPhoneRegExValidatorExtender" runat="server" 
    TargetControlID="secondaryPhoneRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="addKioskQRLinkRegExValidator" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtKioskQRLink" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="addKioskQRLinkRegExValidatorExtender" runat="server" TargetControlID="addKioskQRLinkRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />