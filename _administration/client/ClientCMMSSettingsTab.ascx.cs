﻿using CW.CMMSIntegration;
using CW.Website._administration._shared.CMMS;
using System;

namespace CW.Website._administration.client
{
    public partial class ClientCMMSSettingsTab : TabBase
    {
        #region fields

            SettingsViewManager _viewManager;

        #endregion

        #region properties

            CMMSSettingsView View => CMMSSettingsView;
            SettingsViewManager ViewManager => _viewManager = _viewManager ?? new SettingsViewManager(DataMgr.ConnectionString, LogMgr, View);
            int ClientId => siteUser.CID;
            public bool IsReadOnly { private get; set;}
            bool HasClientSettings => (ViewManager.SettingsType == ConfigTypeEnum.Client);

        #endregion

        #region events

            void Page_FirstLoad()
            {
                ViewManager.ConfigureView();

                ViewManager.SetCMMSConfiguration(ConfigTypeEnum.Client, ClientId);

                chkEnable.Checked = ViewManager.IsEnabled;
 
                if (DisableIfReadOnly()) return; //the form is read only, no need to enable submittal or deletion below

                //only display the update button and the delete button if settings already exists for client, 
                //this is to ensure the user does not submit an empty form or delete a nonexistent config
                btnUpdate.Visible = btnDelete.Visible = HasClientSettings;
            }

            //need to reset message on async post backs, due to view state saving value
            void Page_AsyncPostbackLoad() { lblMessage.Text = string.Empty; }

            #region button events

                protected override void UpdateButton_Click(object sender, EventArgs e)
                {
                    ViewManager.SaveConfig(ClientId, chkEnable.Checked);

                    lblMessage.Text = (ViewManager.SettingsType == ConfigTypeEnum.None) ? ViewManager.Message.Replace("updated", "created") : ViewManager.Message;

                    lnkSetFocus.Focus();

                    ViewManager.SettingsType = ConfigTypeEnum.Client; //explicitly set settings type viewState prop to client, as user created or updated a client config
            
                    btnUpdate.Visible = btnDelete.Visible = true; //explicitly enable submittal and deletion of config settings, now that they exist
                }

                protected void DeleteButton_Click(object sender, EventArgs e) //TODO: Determine a way to centralize this later in a clean way
                {
                    ViewManager.DeleteClientConfig(siteUser.CID);
            
                    lblMessage.Text = ViewManager.Message;

                    ViewManager.SettingsType = ConfigTypeEnum.None; //explicitly set settings type viewState prop to None, as user deleted a client config

                    //explicitly disable submittal and deletion of config settings, now that they were deleted, and uncheck the enabled checkbox
                    chkEnable.Checked = btnUpdate.Visible = btnDelete.Visible = false; 

                    ViewManager.ClearCMMSViewFields();
                }

            #endregion

            #region checkbox events

                protected void chkEnable_CheckedChanged(object sender, EventArgs e)
                {
                    var chkEnableChecked = chkEnable.Checked;

                    ViewManager.SetEditability(chkEnableChecked);

                    //we only want to enable form submittal if the client either has setting, or it's the initial record (new client settings)
                    //where the form has been enabled via the check box being checked
                    btnUpdate.Visible = (HasClientSettings || chkEnableChecked);

                    //for deletion, it only matters that client settings exist to be deleted, check box value is irrelevant
                    btnDelete.Visible = HasClientSettings;
                }

            #endregion

        #endregion

        #region methods

            bool DisableIfReadOnly()
            {
                if (IsReadOnly) chkEnable.Enabled = View.IsEditable = btnUpdate.Visible = btnDelete.Visible = false;

                return IsReadOnly;
            }

        #endregion
    }
}