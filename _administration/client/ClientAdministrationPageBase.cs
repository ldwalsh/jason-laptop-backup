﻿using System;
using CW.Data;
using CW.Utility;

namespace CW.Website._administration.client
{
    public abstract class ClientAdministrationPageBase: AdministrationPageBase
    {
        public override String IdentifierField
        {
            get {return PropHelper.G<Client>(_=>_.CID);}
        }

        public override String NameField
        {
            get {return PropHelper.G<Client>(_=>_.ClientName);}
        }

        public override String EntityTypeName
        {
            get {return typeof(Client).Name;}
        }
    }
}