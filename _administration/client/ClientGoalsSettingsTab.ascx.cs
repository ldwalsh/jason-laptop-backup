﻿using CW.Data;
using CW.Utility.Web;
using CW.Website._administration._shared.Goals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static CW.Website._administration.client.GoalService;

namespace CW.Website._administration.client
{
    public partial class ClientGoalsSettingsTab : TabBase
    {
        #region fields
        GoalsViewManager _viewManager;

        GoalsViewManager ViewManager => _viewManager = _viewManager ?? 
            new GoalsViewManager(this.GoalsSettingsView, this.DataMgr, this.siteUser.CID, ServiceGoalType.Client);
        #endregion

        protected void Page_FirstLoad(object sender, EventArgs e)
        {            
            ViewManager.ConfigureGoalViewState();
            setChkEnabled();            
        }

        #region override

        protected override void UpdateButton_Click(Object sender, EventArgs e) { }

        #endregion

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string result = ViewManager.SaveGoals();

            if (result.ToLower().Contains("success"))
            {
                lblAddError.Visible = false;
                LabelHelper.SetLabelMessage(lblResults, result, lnkSetFocus);
            }
            else
            {
                lblResults.Visible = false;
                LabelHelper.SetLabelMessage(lblAddError, result, lnkSetFocus);
            }
        }

        #region methods
        protected void chkEnable_CheckChanged(object sender, EventArgs e)
        {
            bool isEnabled = true;
            bool success = false;

            if (bool.TryParse(Request["__EventArgument"], out isEnabled))
            {
                GoalService goalService = new GoalService(ServiceGoalType.Client);                

                if(isEnabled)
                {
                    success = goalService.CreateClientGoalsEnabledIfNotExists(siteUser.CID);
                                        
                    if(success)
                    {
                        chkEnable.Checked = true;
                        LabelHelper.SetLabelMessage(lblResults, "Goals enabled successfully.", lnkSetFocus);
                        lblAddError.Visible = false;
                    }
                    else
                    {
                        chkEnable.Checked = false;
                        LabelHelper.SetLabelMessage(lblAddError, "Error enabling goals.", lnkSetFocus);
                        lblResults.Visible = false;
                    }
                }
                else
                {
                    success = goalService.RemoveClientGoalsEnabledIfExists(siteUser.CID);

                    if(success)
                    {
                        chkEnable.Checked = false;
                        LabelHelper.SetLabelMessage(lblResults, "Goals disabled successfully.", lnkSetFocus);
                        lblAddError.Visible = false;
                    }
                    else
                    {
                        chkEnable.Checked = true;
                        LabelHelper.SetLabelMessage(lblAddError, "Error disabling goals.", lnkSetFocus);
                        lblResults.Visible = false;
                    }
                }                    
            }
                        
            ViewManager.UpdateGoalView();
        }

        private void setChkEnabled()
        {
            GoalService goalService = new GoalService(ServiceGoalType.Client);

            chkEnable.Checked = goalService.GetClientGoalsEnabledByCID(siteUser.CID) != null;
        }

        #endregion
    }
}