﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderClientContentTab.ascx.cs" Inherits="CW.Website._administration.client.provider.ProviderClientContentTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="~/_administration/client/nonkgs/ClientContentForm.ascx" tagPrefix="CW" tagName="ClientContentForm" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Content Settings" />
<CW:ClientContentForm runat="server" ID="clientContentForm" UpdateButtonCaption="Update" FormMode="Update" />
