﻿using CW.Data;
using System;
using System.Linq.Expressions;
using System.Web.UI.WebControls;

namespace CW.Website._administration.client.provider
{
    public partial class ProviderClientEditTab : EditTabBase<Client, ProviderClientAdministration>
    {
        #region method

            private void Page_Init()
            {
                CanEntityBeUpdated = clientForm.CanEntityBePersisted;
                UpdateEntityAction = clientForm.PersistEntityAction;
            }

        #endregion

        #region property

            protected override Client GetEntityForEditing
            {
                get { return DataMgr.ClientDataMapper.GetClient(siteUser.CID, new Expression<Func<Client, Object>>[] { _ => _.State, _ => _.Country, _ => _.Organizations_OrganizationTypeName }); }
            }

        #endregion
    }
}