﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ClientGoalsSettingsTab.ascx.cs" Inherits="CW.Website._administration.client.ClientGoalsSettingsTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="~/_administration/_shared/Goals/GoalsSettingsView.ascx" tagPrefix="CW" tagName="GoalsSettingsView" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Goals Settings" />

<script type="text/javascript" src="/_assets/scripts/goalViewLogic.js"></script>
<script type="text/javascript" src="/_administration/_script/TabStateMonitor.js?v=_assets/"></script>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblMessage" CssClass="successMessage" runat="server" />
</p> 

<div class="divForm">
  <label class="label">Enable:</label>
  <asp:CheckBox ID="chkEnable" runat="server" CssClass="checkbox chkEnableGoals" onchange="KGS.ChkEnableClicked()" OnCheckedChanged="chkEnable_CheckChanged" AutoPostBack="false"/>
</div>

<div class="goalSettings" style="display: none;">
    <CW:GoalsSettingsView ID="GoalsSettingsView" runat="server"/>
</div>

<div class="divForm saveButton">       
    <asp:LinkButton ID="btnSave" runat="server" Text="Save" CssClass="lnkButton" OnClick="btnSave_Click"/>    
</div>

<asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
<asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />