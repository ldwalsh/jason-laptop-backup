﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ClientCMMSSettingsTab.ascx.cs" Inherits="CW.Website._administration.client.ClientCMMSSettingsTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="~/_administration/_shared/CMMS/CMMSSettingsView.ascx" tagPrefix="CW" tagName="CMMSSettingsView" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Work Order Settings" />

<script type="text/javascript" src="/_assets/scripts/testConnection.js"></script>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblMessage" CssClass="successMessage" runat="server" />
</p> 

<div class="divForm">
  <label class="label">Enable:</label>
  <asp:CheckBox ID="chkEnable" runat="server" CssClass="checkbox" AutoPostBack="true" OnCheckedChanged="chkEnable_CheckedChanged" />
</div>

<CW:CMMSSettingsView ID="CMMSSettingsView" runat="server" />

<asp:LinkButton ID="btnUpdate" runat="server" Text="Update" CssClass="lnkButton" ValidationGroup="WorkOrder" />
<asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CssClass="lnkButton" OnClick="DeleteButton_Click" OnClientClick="return confirm('Are you sure you wish to delete this CMMS Configuration?')" CausesValidation="false" />