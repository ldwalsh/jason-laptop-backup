﻿using CW.Business;
using CW.Data;
using CW.Website._framework;
using CW.Website.DependencyResolution;
using CW.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CW.Data.Models;

namespace CW.Website._administration.client
{
    public class GoalService
    {        
        protected DataManager dataManager;
        private readonly string _GOAL_SERVICE_KEY = "GOAL_SERVICE_KEY";
        private enum GoalMetric { Cost, Energy, Other };
        private IDictionary<GoalMetric, int[]> _goalMetricIDLookup;
        private ServiceGoalType _goalType;
        public enum ServiceGoalType { Client, Building }
        public GoalService(ServiceGoalType goalType)
        {
            _goalType = goalType;
            setDataManager();
            setGoalMetricIDLookup();
        }

        private void setDataManager()
        {            
            dataManager = DataManager.Get(_GOAL_SERVICE_KEY);
            if (dataManager == null)
            {
                dataManager = IoC.Resolve<DataManager>();
                DataManager.Set(_GOAL_SERVICE_KEY, dataManager);
            }
        }

        private void setGoalMetricIDLookup()
        {
            _goalMetricIDLookup = new Dictionary<GoalMetric, int[]>();            
            _goalMetricIDLookup.Add(GoalMetric.Other, new int[] {
                BusinessConstants.GoalMetric.CarbonMetricID,
                BusinessConstants.GoalMetric.CarMetricID,
                BusinessConstants.GoalMetric.TreeMetricID });
            _goalMetricIDLookup.Add(GoalMetric.Cost, new int[] { BusinessConstants.GoalMetric.CostMetricID });
            _goalMetricIDLookup.Add(GoalMetric.Energy, new int[] { BusinessConstants.GoalMetric.EnergyMetricID });
        }

        public string ParseAndSaveGoalsFromRequestFields(GoalsViewUpdateModel goalsUpdateData, int id)
        {
            if (goalsUpdateData == null) return "Goals were not saved: error parsing goals";

            bool hasGoalsEnabled = checkIfGoalsEnabled(id);

            if (!hasGoalsEnabled) return "Goals were not saved: client does not have goals enabled";

            Tuple<object, GoalMetric>[] goals = getGoals(goalsUpdateData, id);            

            try
            {
                foreach (Tuple<object, GoalMetric> goalTuple in goals)
                {
                    if (goalTuple.Item2 == GoalMetric.Other)
                    {
                        removeOtherGoals(id, _goalMetricIDLookup[goalTuple.Item2]);                        
                    }

                    if (goalTuple.Item1 != null)
                    {
                        updateOrInsertGoal(goalTuple);                        
                    }
                    else
                    {
                        removeOtherGoals(id, _goalMetricIDLookup[goalTuple.Item2]);                        
                    }
                }
                return "Goals were successfully saved.";
            }
            catch (Exception)
            {
                return "Error saving goals.";
            }
        }

        private bool checkIfGoalsEnabled(int id)
        {
            bool hasGoalsEnabled = false;

            if (_goalType == ServiceGoalType.Client)
            {
                hasGoalsEnabled = dataManager.GoalDataMapper.GetClientGoalsEnabledByCID(id) != null;
            }
            else if (_goalType == ServiceGoalType.Building)
            {
                hasGoalsEnabled = dataManager.GoalDataMapper.GetBuildingGoalsEnabledByBID(id) != null;
            }

            return hasGoalsEnabled;
        }

        private Tuple<object, GoalMetric>[] getGoals(GoalsViewUpdateModel goalsUpdateData, int id)
        {
            //parse cost goal
            object costGoal = parseGoal(goalsUpdateData, GoalMetric.Cost, id);
            //parse energy goal
            object energyGoal = parseGoal(goalsUpdateData, GoalMetric.Energy, id);
            //parse carbon goal
            object otherGoal = parseGoal(goalsUpdateData, GoalMetric.Other, id);

            return new Tuple<object, GoalMetric>[]
            {
                new Tuple<object, GoalMetric>(costGoal, GoalMetric.Cost),
                new Tuple<object, GoalMetric>(energyGoal, GoalMetric.Energy),
                new Tuple<object, GoalMetric>(otherGoal, GoalMetric.Other),
            };
        }              

        private void removeOtherGoals(int id, int[] metricTypeIDs)
        {
            if (_goalType == ServiceGoalType.Client)
            {
                dataManager.GoalDataMapper.RemoveClientGoalsByCIDAndMetricIDs(id, metricTypeIDs);                
            }
            else if (_goalType == ServiceGoalType.Building)
            {
                dataManager.GoalDataMapper.RemoveBuildingGoalsByBIDAndMetricIDs(id, metricTypeIDs);
            }        
        }

        private void updateOrInsertGoal(Tuple<object, GoalMetric> goalTuple)
        {
            if (_goalType == ServiceGoalType.Client)
            {
                dataManager.GoalDataMapper.UpdateExistingOrInsertNewClientGoal((ClientGoal)goalTuple.Item1);
            }
            else if (_goalType == ServiceGoalType.Building)
            {
                dataManager.GoalDataMapper.UpdateExistingOrInsertNewBuildingGoal((BuildingGoal)goalTuple.Item1);
            }
        }

        private object parseGoal(GoalsViewUpdateModel goalsUpdateData, GoalMetric goalMetric, int id)
        {
            bool isGoalTypeID;
            bool isGoalMetricID;
            bool isGoalValue;

            short goalTypeID;
            short goalMetricID;
            short goalValue;

            switch (goalMetric)
            {
                case GoalMetric.Cost:                    
                    isGoalTypeID = short.TryParse(goalsUpdateData.DDLCostAbsPctVal, out goalTypeID) && 
                        goalTypeID != BusinessConstants.GoalType.NoGoalTypeID &&
                        !(goalsUpdateData.AreMultipleCurrencies && goalTypeID == BusinessConstants.GoalType.AbsoluteGoalTypeID);
                    string costVal = goalTypeID == BusinessConstants.GoalType.AbsoluteGoalTypeID ? goalsUpdateData.RadCostValueAbsVal : goalsUpdateData.RadCostValuePctVal;
                    isGoalValue = short.TryParse(costVal, out goalValue);
                    goalMetricID = BusinessConstants.GoalMetric.CostMetricID;
                    isGoalMetricID = true;
                    break;
                case GoalMetric.Energy:
                    isGoalTypeID = short.TryParse(goalsUpdateData.DDLEnergyAbsPctVal, out goalTypeID) && 
                        goalTypeID != BusinessConstants.GoalType.NoGoalTypeID &&
                        !(goalsUpdateData.AreMultipleUnits && goalTypeID == BusinessConstants.GoalType.AbsoluteGoalTypeID);
                    string energyVal = goalTypeID == BusinessConstants.GoalType.AbsoluteGoalTypeID ? goalsUpdateData.RadEnergyValueAbsVal : goalsUpdateData.RadEnergyValuePctVal;
                    isGoalValue = short.TryParse(energyVal, out goalValue);
                    goalMetricID = BusinessConstants.GoalMetric.EnergyMetricID;
                    isGoalMetricID = true;
                    break;
                case GoalMetric.Other:
                    isGoalTypeID = short.TryParse(goalsUpdateData.DDLCarbonAbsPctVal, out goalTypeID) && 
                        goalTypeID != BusinessConstants.GoalType.NoGoalTypeID &&
                        !(goalsUpdateData.AreMultipleUnits && goalTypeID == BusinessConstants.GoalType.AbsoluteGoalTypeID);
                    string otherVal = goalTypeID == BusinessConstants.GoalType.AbsoluteGoalTypeID ? goalsUpdateData.RadCarbonValueAbsVal : goalsUpdateData.RadCarbonValuePctVal;
                    isGoalValue = short.TryParse(otherVal, out goalValue);
                    isGoalMetricID = short.TryParse(goalsUpdateData.DDLOtherTypeVal, out goalMetricID);
                    break;
                default:
                    return null;
            }

            if (!(isGoalMetricID && isGoalTypeID && isGoalValue))
            {
                return null;
            }
            else
            {
                if (_goalType == ServiceGoalType.Client)
                {
                    ClientGoal clientGoal = new ClientGoal();
                    clientGoal.GoalMetricID = goalMetricID;
                    clientGoal.GoalTypeID = goalTypeID;
                    clientGoal.GoalValue = goalValue;
                    clientGoal.CID = id;
                    return clientGoal;
                }
                else if (_goalType == ServiceGoalType.Building)
                {
                    BuildingGoal buildingGoal = new BuildingGoal();
                    buildingGoal.GoalMetricID = goalMetricID;
                    buildingGoal.GoalTypeID = goalTypeID;
                    buildingGoal.GoalValue = goalValue;
                    buildingGoal.BID = id;
                    return buildingGoal;
                }
                else
                {
                    return null;
                }
            }
        }

        public List<ClientGoal> GetClientGoalsByCID(int cid)
        {
            return dataManager.GoalDataMapper.GetClientGoalsByCID(cid).ToList();
        }

        public ClientGoalsEnabled GetClientGoalsEnabledByCID(int cid)
        {
            return dataManager.GoalDataMapper.GetClientGoalsEnabledByCID(cid);
        }

        public bool CreateClientGoalsEnabledIfNotExists(int cid)
        {
            ClientGoalsEnabled existingClientGoalsEnabled = dataManager.GoalDataMapper.GetClientGoalsEnabledByCID(cid);

            if(existingClientGoalsEnabled == null)
            {
                ClientGoalsEnabled newClientGoalsEnabled = new ClientGoalsEnabled();
                newClientGoalsEnabled.CID = cid;                

                dataManager.GoalDataMapper.InsertClientGoalsEnabled(newClientGoalsEnabled);
            }

            setBuildingGoalsEnabledForCIDIfNotOverridden(cid);            

            return true;
        }

        public bool RemoveClientGoalsEnabledIfExists(int cid)
        {
            //remove all the building goals that inherit the client goals
            removeBuildingGoalsEnabledIfExistsAndInheritedByCID(cid);

            dataManager.GoalDataMapper.RemoveClientGoalsEnabledIfExists(cid);
            
            return true;
        }

        public List<BuildingGoal> GetBuildingGoalsByBID(int bid)
        {
            return dataManager.GoalDataMapper.GetBuildingGoalsByBID(bid).ToList();
        }

        public BuildingGoalsEnabled GetBuildingGoalsEnabledByBID(int bid)
        {
            return dataManager.GoalDataMapper.GetBuildingGoalsEnabledByBID(bid).SingleOrDefault();
        }

        public bool CreateBuildingGoalsEnabledIfNotExists(int bid)
        {
            BuildingGoalsEnabled existingBuildingGoalsEnabled = GetBuildingGoalsEnabledByBID(bid);

            if (existingBuildingGoalsEnabled == null)
            {
                BuildingGoalsEnabled newBuildingGoalsEnabled = new BuildingGoalsEnabled();
                newBuildingGoalsEnabled.BID = bid;
                newBuildingGoalsEnabled.DateModified = DateTime.UtcNow;
                newBuildingGoalsEnabled.InheritClientGoals = true;

                dataManager.GoalDataMapper.InsertBuildingGoalsEnabled(newBuildingGoalsEnabled);
            }

            return true;
        }

        /// <summary>
        /// Creates buildinggoalsenabled for each building for the given client id
        /// if it doesn't already exist.
        /// </summary>
        /// <param name="cid">Client ID</param>
        private void setBuildingGoalsEnabledForCIDIfNotOverridden(int cid)
        {
            List<int> bids = dataManager.BuildingDataMapper.GetAllBuildingIDsByCID(cid).ToList();

            foreach(int bid in bids)
            {
                CreateBuildingGoalsEnabledIfNotExists(bid);
            }
        }

        /// <summary>
        /// Deletes all buildinggoalsenabled for give client id that are inherited
        /// from the client level.
        /// </summary>
        /// <param name="cid">Client ID</param>
        private void removeBuildingGoalsEnabledIfExistsAndInheritedByCID(int cid)
        {
            List<int> bids = dataManager.BuildingDataMapper.GetAllBuildingIDsByCID(cid).ToList();

            foreach(int bid in bids)
            {
                BuildingGoalsEnabled buildingGoalsEnabled = GetBuildingGoalsEnabledByBID(bid);

                if (buildingGoalsEnabled != null && buildingGoalsEnabled.InheritClientGoals)
                {
                    RemoveBuildingGoalsEnabledIfExists(bid);
                }
            }
        }

        public bool RemoveBuildingGoalsEnabledIfExists(int bid)
        {
            dataManager.GoalDataMapper.RemoveBuildingGoalsEnabledIfExists(bid);

            return true;
        }

        public bool SetBuildingGoalsEnabledInherited(int bid, bool inherits)
        {
            dataManager.GoalDataMapper.SetBuildingGoalsEnabledInherited(bid, inherits);

            return true;
        }

        public bool CheckIfClientHasBuildingsMulitpleCurrencies(int cid)
        {
            List<int> bids = dataManager.BuildingDataMapper.GetAllBuildingIDsByCID(cid).ToList();

            return !dataManager.BuildingDataMapper.AreBuildingSettingsCulturesTheSame(bids);
        }

        public bool CheckIfClientHasBuildingsMultipleUnits(int cid)
        {
            int[] bids = dataManager.BuildingDataMapper.GetAllBuildingIDsByCID(cid).ToArray();

            return !dataManager.BuildingDataMapper.AreUnitsTheSameForBuildingsByBID(bids);
        }
    }
}