﻿using CW.Business;
using CW.Caching;
using CW.Common.Constants;
using CW.Data.Interfaces.State;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._administration
{
    public abstract class ViewTabBase<TKGSEntity,TAdministrationPage>: TabGenericBase<TKGSEntity> where TKGSEntity: new() where TAdministrationPage: AdministrationPageBaseBase
    {
		#region STATIC

			private static TableCell CreateCellWithCommand(String argument, String text) //move out?
			{
				var cell = new TableCell();

				cell.Controls.Add(new LinkButton{CommandArgument=argument, CommandName="Page", Text=text, CssClass="pageLink"});

				return cell;
			}

			private static String TryGetCellContent(IDataItemContainer container, String propName)
			{
				String content;
				
				if (((GridCacher.GridSet.Row)container.DataItem).Cells.TryGetValue(propName, out content)) return content;

				throw new InvalidOperationException(String.Format("The field '{0}' is not included in the .GridColumnNames enumerable property.", propName));
			}

			//

			public static ICacheStore Repository
			{
                get
                {
                    DataManager dm = DataManager.Get(System.Web.HttpContext.Current.Session.SessionID);
                    return (dm.DefaultCacheRepository);
                }
			}

			public static GridCacher.CacheKeyInfo CreateCacheKeyInfoStatic(String subKey=null)
			{
				return CreateCacheKeyInfoStatic(()=>{return subKey;});
			}
			//
			public static GridCacher.CacheKeyInfo CreateCacheKeyInfoStatic(Func<String> getSubKey)
			{
				return new GridCacher.CacheKeyInfo(String.Concat(typeof(TAdministrationPage).Name, '.', typeof(TKGSEntity).Name), getSubKey, ()=>{return null;});
			}

			public static void ClearGridCache(GridCacher.CacheKeyInfo keyInfo)
			{
				GridCacher.Clear(Repository, keyInfo);
			}

		#endregion

		#region delegate

			protected delegate IEnumerable<TKGSEntity> GetEntitiesForGridDelegate(IEnumerable<String> criteria);

			protected delegate TKGSEntity GetEntityDelegate(String id);
            
			protected delegate Boolean CanEntityBeDeletedDelegate(String id, out String message);

		#endregion

		#region field

			#region readonly

				protected readonly AdministrationControl<TAdministrationPage> administrationControl;

			#endregion

			private GridCacher _gridCacher;

			protected Delegates<TKGSEntity>.CanEntityBePersistedDelegate CanEntityBeUpdated;
			protected CanEntityBeDeletedDelegate CanEntityBeDeleted;

			protected Action<TKGSEntity> UpdateEntityMethod;
			protected Action<String>     DeleteEntityMethod; 
			
			protected TextBox     txtSearch;
			protected LinkButton  searchBtn;
			protected GridView    grid;
			protected Panel       pnlSearch;

			protected DetailsView EntityDetailView;
			protected Panel       PanelEditEntity;

            public bool KeepEditPanelOpenOnError;

        #endregion

        #region constructor

        protected ViewTabBase()
			{
				administrationControl = new AdministrationControl<TAdministrationPage>(this);
			}

		#endregion

		#region method

			#region virtual

				protected virtual void CreateEntity(TKGSEntity obj)
				{
					CallFormMethod("CreateEntity", obj);
				}

				protected virtual void UpdateEntity(TKGSEntity obj)
				{
					ValidateDelegate(PropHelper.G<ViewTabBase<TKGSEntity,TAdministrationPage>>(_=>_.CanEntityBeUpdated));
					ValidateDelegate(PropHelper.G<ViewTabBase<TKGSEntity,TAdministrationPage>>(_=>_.UpdateEntityMethod));

					//

					String message;

					if (!CanEntityBeUpdated(obj, out message))
					{
						if (String.IsNullOrWhiteSpace(message)) {message = "{EntityTypeName} could not be updated.";}

						DisplayMessage(message);

						return;
					}

                    try
					{
						UpdateEntityMethod(obj);

						message = "{EntityTypeName} update was successful.";
					}
					catch (Exception ex)
					{
						LogMgr.Log(DataConstants.LogLevel.ERROR, InsertEntityTypeName("Error updating {EntityTypeName}."), ex);
                        if (!KeepEditPanelOpenOnError)
                        {
                            message = "{EntityTypeName} update failed. Please contact an administrator.";
                        }
					}

                    if (!KeepEditPanelOpenOnError)
                    {
                        DisplayMessage(message);
                    }
					BindGrid();

                    PanelEditEntity.Visible = KeepEditPanelOpenOnError;
				}

				protected virtual void DeleteEntity(String id)
				{
					ValidateDelegate(PropHelper.G<ViewTabBase<TKGSEntity,TAdministrationPage>>(_=>_.CanEntityBeDeleted));
					ValidateDelegate(PropHelper.G<ViewTabBase<TKGSEntity,TAdministrationPage>>(_=>_.DeleteEntityMethod));

					//

					String message;

					if (!CanEntityBeDeleted(id, out message))
					{
						if (String.IsNullOrWhiteSpace(message)) {message = "Delete '{EntityTypeName}' failed.";}

						DisplayMessage(message);

						return;
					}

					try
					{
						DeleteEntityMethod(id);

						message = "{EntityTypeName} deletion was successful.";
					}
					catch(Exception ex)
					{
						LogMgr.Log(DataConstants.LogLevel.ERROR, InsertEntityTypeName("Error deleting {EntityTypeName}."), ex);

						message = "{EntityTypeName} deletion failed. Please contact an administrator.";
					}

					DisplayMessage(message);

					PanelEditEntity .Visible = false;
					EntityDetailView.Visible = false;

					//page.TabStateMonitor.ChangeState(DeleteChangeStateEnums); //should be default behavior?

					BindGrid(true);
				}

				protected virtual GridCacher.CacheKeyInfo CreateCacheKeyInfo()
				{
					return CreateCacheKeyInfoStatic(()=>{return CacheSubKey;});
				}

                protected virtual IEnumerable<GridCacher.GridSet.Row> FilterRows(GridCacher.GridSet gridSet) //rename
                {
                    return gridSet.Rows;
                }

			#endregion

			#region override

				protected override void UpdateButton_Click(Object sender, EventArgs e)
				{
					var obj = new TKGSEntity();
					
					CreateEntity(obj);					
					UpdateEntity(obj);
				}

			#endregion

			#region method: page

				private void Page_Init()
				{
					grid.RowCreated           += GridRowCreated;
					grid.RowDataBound         += GridRowDataBound;
					grid.PageIndexChanging    += GridPageIndexChanging;
					grid.Sorting              += GridSorting;
					grid.SelectedIndexChanged += GridSelectedIndexChanged;
					grid.RowDeleting          += GridRowDeleting;
					grid.RowCommand           += GridRowCommand;
                
					OnTabStateChanged += ViewTabBase_OnTabRefresh;
				}

				private void Page_FirstInit()
				{
					if (EntityDetailView == null) throw new Exception("A DetailsView object (EntityDetailView) could not be found.");

					if (!GridColumnNames.Contains(DefaultGridViewSortExpression)) throw new Exception("The 'DefaultGridViewSortExpression' field must exist in the list of 'GridColumnNames'");

					GridViewSortExpression = DefaultGridViewSortExpression;
					GridViewSortDirection  = SortDirection.Ascending;
				}

				private void Page_Load()
				{
					page.TabStateMonitor.OnChangeState += TabStateMonitor_OnChangeState;

					//

                    var formBase = ControlHelper.GetControlByType<FormBase>(this, true);

					if (formBase == null) return;
					
					formBase.OnUpdate += formBase_OnUpdate;
				}

				private void TabStateMonitor_OnChangeState(TabBase tab, IEnumerable<Enum> messages)
				{
					//revise to only ,Clear when state change requires it (by comparing passed in enum value)
					
					gridCacher.Clear();
				}

				private void formBase_OnUpdate()
				{
					page.TabStateMonitor.ChangeState(UpdateChangeStateEnums);

					BindGrid(true);
				}

				private void Page_FirstLoad()
				{
					//OBSOLETE due to panel default button attribute
					//set enter key to submit search
					//txtSearch.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + searchBtn.UniqueID + "','')");

					grid.GridLines           = GridLines.None;
					grid.AllowPaging         = true;
					grid.PageSize            = 20;
					grid.AllowSorting        = true;
					grid.AutoGenerateColumns = false;
					grid.DataKeyNames        = new[]{PropHelper.G<GridCacher.GridSet.Row>(_=>_.ID)};

					grid.PagerSettings.PageButtonCount = 20;

					if (ManualBindGrid) return;

					BindGrid();
				}

			#endregion

			#region method: grid

				protected virtual void GridRowCommand(Object sender, GridViewCommandEventArgs e)
				{
				}

				protected virtual void GridRowCreated(Object sender, GridViewRowEventArgs e)
				{
					/*
						* Future project to generalize Delete confirmation using JS
						* 
					if (e.Row.RowType == DataControlRowType.DataRow)
					{
						var row = e.Row;

						if (row.Cells[row.Cells.Count-1].Controls.Count != 3) return;

						var deleteLink = row.Cells[row.Cells.Count - 1].Controls[1] as LinkButton;

						if (deleteLink == null) return;

						if (deleteLink.CommandName != "Delete") return;

						if (deleteLink.OnClientClick != String.Empty) return;

						deleteLink.OnClientClick = "";
					}
					*/

					if (e.Row.RowType == DataControlRowType.Pager)
					{
						//Note: The autogenerated pager table is not shown with just one page.

						var tblPager = (Table)e.Row.Cells[0].Controls[0];

						tblPager.CssClass = "pageTable";

						var row = tblPager.Rows[0];

						row.Cells.AddAt(0, CreateCellWithCommand("Prev", "« Previous Page"));
						
						row.Cells.Add(CreateCellWithCommand("Next", "Next Page »"));
					}
				}

				protected virtual void GridRowDataBound(Object sender, EventArgs e)
				{
				}

				protected void GridPageIndexChanging(Object sender, GridViewPageEventArgs e)
				{
					if (e.NewPageIndex < 0) return;

					grid.PageIndex = e.NewPageIndex;

					BindGrid();
				}

				protected void GridSorting(Object sender, GridViewSortEventArgs e)
				{
					grid.EditIndex = -1;

					if (GridViewSortExpression == e.SortExpression)
					{
						GridViewSortDirection = ((GridViewSortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending);
					}
					else
					{
						GridViewSortExpression = e.SortExpression;
					}

					BindGrid();
				}

				//protected void GridEditing(Object sender, GridViewEditEventArgs e)
				//{
				//	EntityDetailView.Visible = false;
				//	PanelEditEntity.Visible  = true;

				//	EditEntity(GetEntityForEditing(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_=>_.ID)].ToString()));

				//	//Cancels the edit auto grid selected.
				//	e.Cancel = true;
				//}

				protected virtual void GridSelectedIndexChanged(Object sender, EventArgs e)
				{
					PanelEditEntity.Visible = false;

					EntityDetailView.Visible    = true;
					EntityDetailView.DataSource = new[]{GetEntityForDetailView(grid.DataKeys[grid.SelectedIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_=>_.ID)].ToString())};

					EntityDetailView.DataBind();
				}

				protected void GridRowDeleting(Object sender, GridViewDeleteEventArgs e)
				{
					DeleteEntity(grid.DataKeys[e.RowIndex].Value.ToString());
				}

			#endregion

			private void ViewTabBase_OnTabRefresh(IEnumerable<Enum> messages)
			{
				BindGrid(true);
			}

			protected String DefaultIfNullOrWhiteSpace(Object value, String customEmtpyValue = null)
			{
				if ((value == null) || String.IsNullOrWhiteSpace(value.ToString())) return (customEmtpyValue ?? "(empty)");

				return value.ToString();
			}

			protected String GetCellContent(IDataItemContainer container, String propName, Int32 maxLength=0)
			{
				var v = TryGetCellContent(container, propName);

				return ((maxLength > 0) ? StringHelper.TrimText(v, maxLength) : v);
			}

			protected DateTime GetCellContentAsDateTime(IDataItemContainer container, String propName)
			{
                return DateTime.Parse(TryGetCellContent(container, propName), CultureInfo.InvariantCulture);
			}

			protected String GetCellContentAsEnum<T>(IDataItemContainer container, String propName, Int32 maxLength=0)
			{
				var v = Enum.GetName(typeof(T), Convert.ToInt32(TryGetCellContent(container, propName)));

				return ((maxLength > 0) ? StringHelper.TrimText(v, maxLength) : v);
			}

			protected void BindGrid(Boolean forceRefresh=false)
			{				
				GridCacher.GridSet gs;

				if (SearchCriteria == null) //todo: clean-up the .Get
				{
					gs = gridCacher.Get(forceRefresh);

					pnlSearch.Visible = gs.Rows.Any();
				}
				else
				{
					gs = gridCacher.Get(SearchCriteria);
				}

				if (gs.Rows != null)
				{
					var sort = GridViewSortExpression;

                gs.Rows = (gs.ColumnTypes[sort] == "DateTime") ? gs.Rows.OrderBy(_ => DateTime.Parse(_.Cells[sort], CultureInfo.InvariantCulture))
                                 : (gs.ColumnTypes[sort] == "Int32") ? gs.Rows.OrderBy(_ => Int32.Parse(_.Cells[sort])) : gs.Rows.OrderBy(_ => _.Cells[sort]);
					
					if (GridViewSortDirection == SortDirection.Descending)
					{
						gs.Rows = gs.Rows.Reverse();
					}
				}

				var rows = FilterRows(gs).ToList();
				
				grid.DataSource = rows;
                
				grid.DataBind();

				SetGridCountLabel(rows.Count());
			}

			protected void SetGridCountLabel(Int32 count) //combine with above
			{
				String message;
                
				switch (count)
				{
					case 0:  message = "No {EntityTypeName} found."; break;
					case 1:  message = "1 {EntityTypeName} found."; break;
					default: message = String.Format("{0} {{EntityTypeName}} found.", count); break;
				}

				tabHeader.SubTitle = InsertEntityTypeName(message, (count == 1));
			}

			protected void viewAll_Click(Object sender, EventArgs e)
			{
				txtSearch.Text = String.Empty;

				rebindGridForSearch();
			}

			protected void SearchButtonClick(Object sender, EventArgs e)
			{
				rebindGridForSearch(txtSearch.Text);
			}

			private void rebindGridForSearch(string criteria = null)
			{
				SearchCriteria = criteria;

				BindGrid();

				if (PanelEditEntity == null) return;

				EntityDetailView.Visible = PanelEditEntity.Visible = false;
			}

		#endregion
        
		#region property

			#region abstract

				protected abstract IEnumerable<String> GridColumnNames {get;}

                protected abstract GetEntitiesForGridDelegate GetEntitiesForGrid {get;}

                protected abstract GetEntityDelegate GetEntityForDetailView {get;}

				protected abstract String CacheSubKey {get;}

			#endregion

			#region virtual

				protected virtual String DefaultGridViewSortExpression
				{
					get {return NameField;}
				}

				protected virtual IEnumerable<String> CacheHashKeySubList //convert to method? probably.
				{
					get {return null;}
				}

				protected virtual IEnumerable<Enum> UpdateChangeStateEnums
				{
					get {return new Enum[]{StateChangeEnum.EditEntity,};}
				}

				protected virtual IEnumerable<Enum> DeleteChangeStateEnums
				{
					get {return new Enum[]{StateChangeEnum.DeleteEntity,};}
				}

				public virtual IEnumerable<Enum> ReactToChangeStateList
				{
					get {return new Enum[]{StateChangeEnum.AddEntity,};}
				}

				public virtual Boolean AlwaysForceGridCacherRefresh
				{
					get {return false;}
				}

				public virtual IEnumerable<String> SearchGridColumnNames
				{
					get {return new[]{NameField};}
				}

			#endregion
			
			protected new TAdministrationPage page
			{
				[
				DebuggerStepThrough()
				]							
				get {return administrationControl.page;}
			}

			private GridCacher gridCacher
			{
				get
				{
					_gridCacher =
					(
						_gridCacher ??
						new GridCacher
							(
								typeof(TKGSEntity),
								Repository,
								CreateCacheKeyInfo(),
								new Func<IEnumerable<String>,IEnumerable>(GetEntitiesForGrid),
								new GridCacher.ColumnInfo(IdColumnName, GridColumnNames, SearchGridColumnNames)
							)
							{AlwaysForceRefresh=AlwaysForceGridCacherRefresh}
					);
		
					return _gridCacher;
				}
			}

			public Boolean ManualBindGrid {get;set;}
			
			[
			ViewStateProperty
			]
			protected String GridViewSortExpression {set;get;}

			[
			ViewStateProperty
			]
			protected SortDirection GridViewSortDirection {set;get;}

			[
			ViewStateProperty
			]
			protected string SearchCriteria {set;get;} //should be made private when PropertyPersistanceManager can support it


		#endregion
    }
}