﻿using System;

namespace CW.Website._administration
{
    public static class Delegates<TKGSEntity>
    {
        public delegate Boolean CanEntityBePersistedDelegate(TKGSEntity obj, out String message);
    }
}