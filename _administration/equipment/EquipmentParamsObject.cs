﻿using System;
using CW.Utility.Web;

namespace CW.Website._administration.equipment
{
    public sealed class EquipmentParamsObject: QueryString.IParamsObject
    {
        #region QueryString.IParamsObject

            Boolean QueryString.IParamsObject.RequirePropertyAttribute
            {
                get {return false;}
            }

        #endregion

        public  Int32
                tab;

        [
        QueryString.Property(Requires = "tab")
        ]
        public Int32
                bid;

        [
        QueryString.Property(Requires = "bid")
        ]
        public  Int32
                eid;
    }
}