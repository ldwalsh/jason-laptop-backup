﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentForm.ascx.cs" Inherits="CW.Website._administration.equipment.EquipmentForm" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register tagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="CW" Namespace="FileUploaderControl" Assembly="FileUploaderControl" %>

<asp:HiddenField ID="hdnEID" runat="server" Visible="false" />
<div class="divForm">
    <label class="label">*Equipment Name:</label>
    <asp:TextBox ID="txtEquipmentName" CssClass="textbox" MaxLength="50" runat="server" />
</div> 
<div class="divForm">
    <label class="label">*Building:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlBID" AppendDataBoundItems="true" runat="server">
        <asp:ListItem Value="-1">Select one...</asp:ListItem>
    </asp:DropDownList>
</div>
<div class="divForm">   
    <label class="label">*Equipment Class:</label>    
    <asp:DropDownList ID="ddlEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
        <asp:ListItem Value="-1">Select building first...</asp:ListItem>
    </asp:DropDownList>    
</div>
<div class="divForm">   
    <label class="label">*Equipment Type:</label>    
    <asp:DropDownList ID="ddlEquipmentTypeID" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                                                                
        <asp:ListItem Value="-1">Select class first...</asp:ListItem>                             
    </asp:DropDownList> 
</div>
<div class="divForm">   
    <label class="label">*Manufacturer:</label>    
    <asp:DropDownList ID="ddlManufacturer" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlManufacturer_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
        <asp:ListItem Value="-1">Select class first...</asp:ListItem>                                 
    </asp:DropDownList> 
</div>  
<div class="divForm">
    <label class="label">*Model:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlManufacturerModelID" AppendDataBoundItems="true" runat="server" >  
        <asp:ListItem Value="-1">Select manufacturer first...</asp:ListItem>                             
    </asp:DropDownList>
</div>                                                    
<div class="divForm">
    <label class="label">Serial Number:</label>
    <asp:TextBox ID="txtSerialNumber" CssClass="textbox" MaxLength="50" runat="server" />
</div>                                                           
<div class="divForm">
    <label class="label">Location:</label>                                                         
    <textarea name="txtEquipmentLocation" id="txtEquipmentLocation" cols="40" rows="5" onkeyup="limitChars(this, 250, 'divEquipmentLocationCharInfo')" runat="server" />
    <div id="divEquipmentLocationCharInfo"></div>
</div> 
<div class="divForm">
    <label class="label">Description:</label>                                                         
    <textarea name="txtDescription" id="txtDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divDescriptionCharInfo')" runat="server" />
    <div id="divDescriptionCharInfo"></div>
</div>
<asp:HiddenField runat="server" ID="hdnImageExtension" Visible="false" />
<div class="divForm">
    <label class="label">Image (800x600):</label>
    <CW:FileUploader runat="server" ID="equipmentImageUploader" ErrorMessageClass="errorMessage" />
</div>
<div class="divForm">   
    <label class="label">Primary Manufacturer Contact:</label>    
    <asp:DropDownList ID="ddlPrimaryContactID" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                                                                
        <asp:ListItem Value="-1">Select manufacturer first...</asp:ListItem>                              
    </asp:DropDownList> 
</div>    
<div class="divForm">   
    <label class="label">Secondary Manufacturer Contact:</label>    
    <asp:DropDownList ID="ddlSecondaryContactID" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                                                                
        <asp:ListItem Value="-1">Select manufacturer first...</asp:ListItem>                              
    </asp:DropDownList> 
</div>
<div class="divForm">
    <label class="label">Equipment Notes (Max HTML Characters = 5000):</label>
    <telerik:RadEditor ID="editorEquipmentNotes" 
        runat="server"                                                                                                                                                
        CssClass="editorNarrow"                                                        
        Height="275px" 
        Width="462px"
        MaxHtmlLength="5000"
        NewLineMode="Div"
        ToolsWidth="464px"                                                                                                                                       
        ToolbarMode="ShowOnFocus"         
        ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"  
        EditModes="Design,Preview"                                                                      
    > 
    <CssFiles>
        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
    </CssFiles>                                                                                                                             
    </telerik:RadEditor>                                                         
</div>
<div class="divForm" runat="server">
    <label class="label">*Active:</label>
    <asp:CheckBox ID="chkIsActive" CssClass="checkbox" Checked="true" runat="server" />                                                        
    <p>(Warning: Setting an equipment as inactive will not allow scheduled analyses to be put in the queue, and no incoming data will be recorded.)</p>
</div>
<div class="divForm" runat="server">
    <label class="label">*Visible:</label>
    <asp:CheckBox ID="chkIsVisible" CssClass="checkbox" Checked="true" runat="server" />                                                        
    <p>(Warning: Setting an equipment as not visible will hide the equipment on non administrative pages.)</p>
</div>

<asp:Panel ID="pnlEquipmentServiced" runat="server">
    <hr />
    <h2 id="servicedInfoHeaderText" runat="server"></h2>
                                                                                     
    <div class="divFormNarrow">
        <label class="labelWide">Last Serviced Date:</label>
        <telerik:RadDatePicker ID="txtLastServicedDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>        
    </div>
    <div class="divFormNarrow">
        <label class="labelWide">Last Serviced By:</label>
        <asp:TextBox ID="txtLastServicedBy" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
    </div>
    <div class="divFormNarrow">
        <label class="labelWide">Last Serviced Company:</label>
        <asp:TextBox ID="txtLastServicedCompany" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
    </div>
</asp:Panel>

<hr />
<h2 id="additionalInfoHeaderText" runat="server"></h2>
                                 
<div class="divFormNarrow">
    <label class="labelWide">External Equipment Type Name:</label>
    <asp:TextBox ID="txtExternalEquipmentTypeName" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">CMMS (Computerized Maintenance Mgmt System) Reference/Asset ID:</label>
    <asp:TextBox ID="txtCMMSReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">CMMS (Computerized Maintenance Management System) Location ID:</label>
    <asp:TextBox ID="txtCMMSLocationID" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">CMMS (Computerized Maintenance Management System) Site ID:</label>
    <asp:TextBox ID="txtCMMSSiteID" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">CMMS (Computerized Maintenance Management System) Link:</label>
    <asp:TextBox ID="txtCMMSLink" CssClass="textbox" MaxLength="100" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">BIM (Building Information Management) Reference ID:</label>
    <asp:TextBox ID="txtBIMReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">BIM (Building Information Management) Link:</label>
    <asp:TextBox ID="txtBIMLink" CssClass="textbox" MaxLength="100" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">DMS (Document Management Software) Reference ID:</label>
    <asp:TextBox ID="txtDMSReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">DMS (Document Management Software) Link:</label>
    <asp:TextBox ID="txtDMSLink" CssClass="textbox" MaxLength="100" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">GIS (Geographical Information System) Reference ID:</label>
    <asp:TextBox ID="txtGISReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divFormNarrow">
    <label class="labelWide">GIS (Geographical Information System) Link:</label>
    <asp:TextBox ID="txtGISLink" CssClass="textbox" MaxLength="100" runat="server" />
</div>

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" /> 

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="equipmentNameRequiredValidator" runat="server"
    ErrorMessage="Equipment Name is a required field."
    ControlToValidate="txtEquipmentName"
    SetFocusOnError="true"
    Display="None">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="equipmentNameRequiredValidatorExtender" runat="server"
    TargetControlID="equipmentNameRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>  
<asp:RequiredFieldValidator ID="buildingRequiredValidator" runat="server"
    ErrorMessage="Building is a required field." 
    ControlToValidate="ddlBID"  
    SetFocusOnError="true" 
    Display="None" 
    InitialValue="-1">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="buildingRequiredValidatorExtender" runat="server"
    TargetControlID="buildingRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>     
<asp:RequiredFieldValidator ID="equipmentClassRequiredValidator" runat="server"
    ErrorMessage="Equipment Class is a required field." 
    ControlToValidate="ddlEquipmentClass"  
    SetFocusOnError="true" 
    Display="None" 
    InitialValue="-1">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassRequiredValidatorExtender" runat="server"
    TargetControlID="equipmentClassRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>     
<asp:RequiredFieldValidator ID="equipmentTypeRequiredValidator" runat="server"
    ErrorMessage="Equipment Type is a required field." 
    ControlToValidate="ddlEquipmentTypeID"  
    SetFocusOnError="true" 
    Display="None" 
    InitialValue="-1">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="equipmentTypeRequiredValidatorExtender" runat="server"
    TargetControlID="equipmentTypeRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>  
<asp:RequiredFieldValidator ID="manufacturerRequiredValidator" runat="server"
    ErrorMessage="Manufacturer is a required field." 
    ControlToValidate="ddlManufacturer"  
    SetFocusOnError="true" 
    Display="None" 
    InitialValue="-1">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="manufacturerRequiredValidatorExtender" runat="server"
    TargetControlID="manufacturerRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>    
<asp:RequiredFieldValidator ID="manufacturerModelRequiredValidator" runat="server"
    ErrorMessage="Model is a required field." 
    ControlToValidate="ddlManufacturerModelID"  
    SetFocusOnError="true" 
    Display="None" 
    InitialValue="-1">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="manufacturerModelRequiredValidatorExtender" runat="server"
    TargetControlID="manufacturerModelRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                  
<asp:RegularExpressionValidator ID="cmmsLinkRegExValidator" runat="server"
    ErrorMessage="Invalid URL Format."
    ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)"
    ControlToValidate="txtCMMSLink"
    SetFocusOnError="true" 
    Display="None" >
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="cmmsLinkRegExValidatorExtender" runat="server"
    TargetControlID="cmmsLinkRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>        
<asp:RegularExpressionValidator ID="bimLinkRegExValidator" runat="server"
    ErrorMessage="Invalid URL Format."
    ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)"
    ControlToValidate="txtBIMLink"
    SetFocusOnError="true" 
    Display="None" >
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="bimLinkRegExValidatorExtender" runat="server"
    TargetControlID="bimLinkRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender> 
<asp:RegularExpressionValidator ID="dmsLinkRegExValidator" runat="server"
    ErrorMessage="Invalid URL Format."
    ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)"
    ControlToValidate="txtDMSLink"
    SetFocusOnError="true" 
    Display="None" >
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="dmsLinkRegExValidatorExtender" runat="server"
    TargetControlID="dmsLinkRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>  
<asp:RegularExpressionValidator ID="gisLinkRegExValidator" runat="server"
    ErrorMessage="Invalid URL Format."
    ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)"
    ControlToValidate="txtGISLink"
    SetFocusOnError="true" 
    Display="None" >
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="gisLinkRegExValidatorExtender" runat="server"
    TargetControlID="gisLinkRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>  