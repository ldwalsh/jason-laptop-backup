﻿using System;
using CW.Utility;
using CW.Data;

namespace CW.Website._administration.equipment
{
    public class EquipmentAdministrationPageBase: AdministrationPageBase
    {
        public override String IdentifierField
        {
            get {return PropHelper.G<Equipment>(e => e.EID);}
        }

        public override String NameField
        {
            get {return PropHelper.G<Equipment>(e => e.EquipmentName);}
        }

        public override String EntityTypeName
        {
            get {return typeof(Equipment).Name;}
        }
    }
}