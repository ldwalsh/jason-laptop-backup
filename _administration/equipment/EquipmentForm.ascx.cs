﻿using CW.Business;
using CW.Business.Blob;
using CW.Business.Blob.Images;
using CW.Data;
using CW.Utility;
using CW.Website._framework;
using System;
using System.IO;
using System.Web.UI.WebControls;

namespace CW.Website._administration.equipment
{
    public static class ListItemCollectionExtensions //move to CW.Utility?
    {
        public static void Clear(this ListItemCollection value, Boolean preserveDefaultItem)
        {
            var defaultItem = value[0];

            if (defaultItem.Value != "-1") throw new InvalidOperationException("The default item must have a value of '-1'.");

            value.Clear();

            value.Add(defaultItem);
        }
    }

    public partial class EquipmentForm: FormGenericBase<EquipmentForm,Equipment>, SiteUserControl.IClientControl2
    {
		#region IClientControl2

			String IClientControl2.ScriptSource
			{
				get
				{
					using (var sr = new FileInfo(Server.MapPath(@"~\_administration\_script\EquipmentForm.js")).OpenText()) return sr.ReadToEnd();
					//return new ResourceHelper(Assembly.GetExecutingAssembly()).GetEmbedded("_administration._script.EquipmentForm.js").ToString();
				}
			}

		#endregion

		#region field

			public Boolean BIDEnabled;
			public Boolean EquipmentClassEnabled;
            public Boolean EquipmentTypeIDEnabled;
			public Boolean IsActiveVisible;
			public Boolean IsVisibleVisible;

			public Boolean NameEnabled;

		#endregion

		#region method

			#region override

				protected override void ConfigureForm()
				{
					BindBuildings(ddlBID);
					BindEquipmentClasses(ddlEquipmentClass);

					base.ConfigureForm();
				}

				public override void ClearForm()
				{
					base.ClearForm();

					equipmentImageUploader.Clear();
				}

				public override void PopulateForm(Equipment obj)
				{
					BindFormControls(obj);

					LoadHandlers.Add(PropHelper.G<EquipmentForm>(_=>_.ddlEquipmentClass), _=>(_.EquipmentType.EquipmentClassID));
					LoadHandlers.Add(PropHelper.G<EquipmentForm>(_=>_.ddlManufacturer),   _=>(_.EquipmentManufacturerModel.ManufacturerID));

					base.PopulateForm(obj);

					//
					//load equipment preview image if extension field is populated
					
					if (!String.IsNullOrEmpty(obj.ImageExtension))
					{
						var image = new EquipmentProfileImage(obj.Building.Client.CID, DataMgr.OrgBasedBlobStorageProvider, obj.ImageExtension, new EquipmentProfileImage.Args(obj.EID));
                            
						if (image.Exists())
						{
							equipmentImageUploader.SetFile(image.Retrieve(), image.FileName);
						}
					}
				}

			#endregion

            public Boolean CanEquipmentBePersisted(Equipment equipment, out String message)
            {
                if (DataMgr.EquipmentDataMapper.DoesEquipmentNameExistForBuilding(equipment.EID, equipment.BID, equipment.EquipmentName))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

				if (FormMode == PersistModeEnum.Update)
				{
					//TODO: this could probably be a hidden variable set on the front end earlier rather then quering here
					int classID = DataMgr.EquipmentClassDataMapper.GetEquipmentClassIDByEID(equipment.EID);

                    //TEMP: this should be unreachable for now, but we may allow it later on.
                    //if the equipment class changed
                    //if (classID != Convert.ToInt32(ddlEquipmentClass.SelectedValue))
                    //{
                    //    if (mDataManager.AnalysisEquipmentDataMapper.IsAnyAnalysisAssignedToEquipmentWhereAnEquipmentVariableIsRequired(equipment.EID))
                    //    {
                    //        message = "Cannot change class because equipment variables assigned from the existing class to the equipment are required for analyses already assigned to the equipment. Please unassign the analyses from the equipment first.";

                    //        return false;
                    //    }

                    //    //clear manufacturer model to unknown
                    //    equipment.ManufacturerModelID = BusinessConstants.EquipmentManufacturerModelConstants.Unknown;
                    //}
				}

                message = null;

                return true;
            }

			public void PersistEquipment(Equipment equipment)
			{
				switch (FormMode)
				{
					case PersistModeEnum.Insert:
					{
						equipment.ImageExtension = ((equipmentImageUploader.File == null) ? null : equipmentImageUploader.File.Extension);

						DataMgr.EquipmentDataMapper.InsertEquipment(equipment);

						if (equipment.ImageExtension != null)
						{
							new EquipmentProfileImage(siteUser.CID, DataMgr.OrgBasedBlobStorageProvider, equipmentImageUploader.File.Extension, new EquipmentProfileImage.Args(equipment.EID)).Insert(equipmentImageUploader.File.Content);
						}

						break;
					}
					case PersistModeEnum.Update:
					{
						if (equipmentImageUploader.HasChanged)
						{
							var image = new EquipmentProfileImage(siteUser.CID, DataMgr.OrgBasedBlobStorageProvider, new EquipmentProfileImage.Args(equipment.EID));

							if (!String.IsNullOrEmpty(hdnImageExtension.Value))
							{
								image.SetExtension(hdnImageExtension.Value).Delete();
								
								equipment.ImageExtension = String.Empty;
							}

							if (equipmentImageUploader.File != null)
							{
								image.SetExtension(equipmentImageUploader.File.Extension).Insert(equipmentImageUploader.File.Content);

								equipment.ImageExtension = equipmentImageUploader.File.Extension;
							}
						}

						//
						//TODO: this could probably be a hidden variable set on the front end earlier rather then quering here
						var classID = DataMgr.EquipmentClassDataMapper.GetEquipmentClassIDByEID(equipment.EID);
                        
						if (classID != Convert.ToInt32(ddlEquipmentClass.SelectedValue))
						{
							//delete all existing equipment variables associated to the equipment not associated to new equipmentclass
							DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariablesNotFoundInChangingEquipmentClass(equipment.EID, Convert.ToInt32(ddlEquipmentClass.SelectedValue));
						}

						DataMgr.EquipmentDataMapper.UpdateEquipment(equipment);
                        
						break;
					}
				}

				hdnImageExtension.Value = equipment.ImageExtension;
			}

			#region Page Events

				private void Page_FirstInit()
				{
					//datamanager in siteusercontrol

					if (FormMode == PersistModeEnum.Update)
					{
						servicedInfoHeaderText.InnerText = "Edit Equipment Serviced Information";
						pnlEquipmentServiced.Visible = true;

						//non kgs cannot change equipment classes because they may not know how to set up equipvariables again if done accidentally.
						//ddlEquipmentClass.Enabled = false;

						//non kgs cannot change buildings. Only allow for kgs if no analyses are assigned. This way either all data has been purged so it can be moved or no analytics have yet been ran. Partition and row keys use bid and eid together.
						//ddlBID.Enabled = false;
					}
					else
					{
						pnlEquipmentServiced.Visible = false;
                        
						ddlEquipmentClass.Enabled = true;
					}

					additionalInfoHeaderText.InnerText += (((FormMode == PersistModeEnum.Insert) ? "Add" : "Edit") + " Additional Equipment Information");
				}

			#endregion

			#region Load and Bind Fields

				private void BindFormControls(Equipment equipment)
				{
					BindBuildings(ddlBID);
					BindEquipmentClasses(ddlEquipmentClass);
					BindEquipmentTypes(ddlEquipmentTypeID, equipment.EquipmentType.EquipmentClassID);
					BindManufacturers(ddlManufacturer, ddlManufacturerModelID, equipment.EquipmentType.EquipmentClassID);
					BindManufacturerModels(equipment.EquipmentManufacturerModel.ManufacturerID, ddlManufacturerModelID);
					BindManufacturerContacts(ddlPrimaryContactID, equipment.EquipmentManufacturerModel.ManufacturerID);
					BindManufacturerContacts(ddlSecondaryContactID, equipment.EquipmentManufacturerModel.ManufacturerID);
				}

				//private void BindFormControls()
				//{
				//    BindBuildings(siteUser.CID, ddlBID);
				//    BindEquipmentClasses(ddlEquipmentClass);
				//}

				/// <summary>
				/// Binds a dropdown list by client id
				/// </summary>
				/// <param name="ddl"></param>
				public void BindBuildings(DropDownList ddl)
				{
					ddl.Items.Clear(true);
					ddl.DataTextField = "BuildingName";
					ddl.DataValueField = "BID";
                    ddl.DataSource = siteUser.VisibleBuildings;
					ddl.DataBind();
				}

				/// <summary>
				/// Binds a dropdown list with all equipment manufacturers
				/// </summary>
				/// <param name="ddl"></param>
				private void BindManufacturers(DropDownList ddl, DropDownList ddlModel, int equipmentClassID)
				{
					ddl.Items.Clear(true);
					ddl.DataTextField = "ManufacturerName";
					ddl.DataValueField = "ManufacturerID";
					ddl.DataSource = DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturersAssociatedToEquipmentClassID(equipmentClassID);
					ddl.DataBind();

					ddlModel.Items.Clear(true);
					ddlModel.DataTextField = "ManufacturerModelName";
					ddlModel.DataValueField = "ManufacturerModelID";
					ddlModel.DataBind();
				}

				/// <summary>
				/// Binds a dropdownlist with all manufacturer models by equipment maufacturer id
				/// </summary>
				/// <param name="ddl"></param>
				/// <param name="manufacturerID"></param>
				private void BindManufacturerModels(int manufacturerID, DropDownList ddl)
				{
					ddl.Items.Clear(true);
					ddl.DataTextField = "ManufacturerModelName";
					ddl.DataValueField = "ManufacturerModelID";
					ddl.DataSource = DataMgr.EquipmentManufacturerModelDataMapper.GetAllManufacturerModelsByManufacturerID(manufacturerID);
					ddl.DataBind();
				}

				/// <summary>
				/// Binds a dropdownlist with all equipment classes
				/// </summary>
				/// <param name="ddl"></param>
				private void BindEquipmentClasses(DropDownList ddl)
				{
					//ListItem lItem = new ListItem();
					//lItem.Text = "Select one...";
					//lItem.Value = "-1";
					//lItem.Selected = true;

					ddl.Items.Clear(true);
					ddl.DataTextField = "EquipmentClassName";
					ddl.DataValueField = "EquipmentClassID";
					ddl.DataSource = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();
					ddl.DataBind();
				}

				/// <summary>
				/// Binds a dropdownlist with all equipment types by equipment class id
				/// </summary>
				/// <param name="ddl"></param>
				/// <param name="equipmentClassID"></param>
				private void BindEquipmentTypes(DropDownList ddl, int equipmentClassID)
				{
					ddl.Items.Clear(true);
					ddl.DataTextField = "EquipmentTypeName";
					ddl.DataValueField = "EquipmentTypeID";
					ddl.DataSource = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByEquipmentClassID(equipmentClassID);
					ddl.DataBind();
				}

				/// <summary>
				/// Binds a dropdownlist with all contact by manufacturer id
				/// </summary>
				/// <param name="ddl"></param>
				/// <param name="manufacturerID"></param>
				private void BindManufacturerContacts(DropDownList ddl, int manufacturerID)
				{
					ddl.Items.Clear(true);
					ddl.DataTextField = "Name";
					ddl.DataValueField = "ContactID";
					ddl.DataSource = DataMgr.EquipmentManufacturerContactDataMapper.GetAllManufacturerContactsByManufacturerID(manufacturerID);
					ddl.DataBind();
				}

			#endregion

			#region Dropdown Events

			/// <summary>
			/// ddlEditManufacturer_OnSelectedIndexChanged, binds manufacturer model dropdown by selected manufacturerID
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			protected void ddlManufacturer_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				BindManufacturerModels(Convert.ToInt32(ddlManufacturer.SelectedValue), ddlManufacturerModelID);

				BindManufacturerContacts(ddlPrimaryContactID, Convert.ToInt32(ddlManufacturer.SelectedValue));
				BindManufacturerContacts(ddlSecondaryContactID, Convert.ToInt32(ddlManufacturer.SelectedValue));
			}

			/// <summary>
			/// ddlAddEquipmentClass_OnSelectedIndexChanged, binds equipment types dropdown by selected classid
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			protected void ddlEquipmentClass_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				BindEquipmentTypes(ddlEquipmentTypeID, Convert.ToInt32(ddlEquipmentClass.SelectedValue));

				BindManufacturers(ddlManufacturer, ddlManufacturerModelID, Convert.ToInt32(ddlEquipmentClass.SelectedValue));               
			}

			#endregion

		#endregion

		#region property

			public override Delegates<Equipment>.CanEntityBePersistedDelegate CanEntityBePersisted
			{
				get {return CanEquipmentBePersisted;}
			}

			public override Action<Equipment> PersistEntityAction
			{
				get {return PersistEquipment;}
			}

		#endregion
    }
}