﻿<%@ Page Language="C#" AutoEventWireup="false" MasterPageFile="~/_masters/Admin.Master" CodeBehind="EquipmentAdministration.aspx.cs" Inherits="CW.Website._administration.equipment.nonkgs.EquipmentAdministrationPage" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="~/_administration/equipment/nonkgs/EquipmentViewTab.ascx" tagPrefix="CW" tagName="EquipmentViewTab" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentVariables.ascx" tagPrefix="CW" tagName="EquipmentVariables" %>
<%@ Register src="~/_administration/equipment/nonkgs/EquipmentAnalyses.ascx" tagPrefix="CW" tagName="EquipmentAnalyses" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <CW:PageHeader ID="PageHeader" runat="server" Title="Equipment Administration"  GlobalSettingProperty="AdminEquipmentBody" />

  <div class="administrationControls">
    <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
      <Tabs>
        <telerik:RadTab Text="View Equipment" />
        <telerik:RadTab Text="Equipment Variables" />
        <telerik:RadTab Text="Equipment Analyses" />
      </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

      <telerik:RadPageView ID="RadPageView1" runat="server">
        <CW:EquipmentViewTab runat="server" ID="equipmentViewTab" ManualBindGrid="true"/>
      </telerik:RadPageView>
    
      <telerik:RadPageView ID="RadPageView3" runat="server">
        <CW:EquipmentVariables ID="equipmentVariables" runat="server" />
      </telerik:RadPageView>

      <telerik:RadPageView ID="RadPageView4" runat="server">
        <CW:EquipmentAnalyses ID="equipmentAnalyses" runat="server" />
      </telerik:RadPageView>
    </telerik:RadMultiPage>
  </div>

</asp:Content>