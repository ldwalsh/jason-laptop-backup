﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentAddTab.ascx.cs" Inherits="CW.Website._administration.equipment.nonkgs.EquipmentAddTab" %>
<%@ Register src="../EquipmentForm.ascx" tagPrefix="CW" tagName="EquipmentForm" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Equipment" />
<CW:EquipmentForm runat="server" ID="equipmentForm" FormMode="Insert" UpdateButtonCaption="Add Equipment" ClientVisible="false"  BIDEnabled="true" EquipmentClassEnabled="true" EquipmentTypeEnabled="true" />
