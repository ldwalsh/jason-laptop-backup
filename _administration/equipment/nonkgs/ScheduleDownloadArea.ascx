﻿<%@ Control CodeBehind="ScheduleDownloadArea.ascx.cs" Inherits="CW.Website._administration.equipment.nonkgs.ScheduleDownloadArea" %>

<div class="divDownloadBox2">        
    <asp:Label ID="lblError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
    <div class="divDownload">            
    <asp:ImageButton runat="server" CausesValidation="false" ID="imgDownload" CssClass="imgDownload" ImageUrl="/_assets/images/excel-icon.jpg" AlternateText="download" />
    <asp:LinkButton runat="server" CausesValidation="false" ID="lnkDownload" CssClass="lnkDownload" Text="Download Equipment Schedule" />    
    </div> 
</div>