﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;
using CW.Website._framework;

namespace CW.Website._administration.equipment.nonkgs
{
    public partial class EquipmentAnalyses: SiteUserControl
    {
        #region Properties

        const string equipmentAnalysesEmpty = "No analyses have been assigned.";

        #endregion

        #region Page Events

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindBuildings(ddlAnalysesBuildings);
            }
        }

        #endregion

        #region Load and Bind Fields

        /// <summary>
        /// Binds a dropdown list with all buildings
        /// </summary>
        /// <param name="ddl"></param>
        private void BindBuildings(DropDownList ddl)
        {
            ddl.DataTextField = "BuildingName";
            ddl.DataValueField = "BID";

            //get all buildings by client id
            ddl.DataSource = siteUser.VisibleBuildings;

            ddl.DataBind();
        }

        /// <summary>
        /// Binds repeater with analyses associated to the equipemnts.     
        /// </summary>
        /// <param name="eid"></param>
        private void BindEquipmentAnalyses(int eid)
        {    
            //get all analyses in system
            IEnumerable<Analyse> mAnalyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByEID(eid);

            if (mAnalyses.Any())
            {
                //bind available assigned analyses to repeater
                rptEquipmentAnalyses.DataSource = mAnalyses;
                rptEquipmentAnalyses.DataBind();
                rptEquipmentAnalyses.Visible = true;
                lblEquipmentAnalysesEmpty.Visible = false;
            }
            else
            {
                rptEquipmentAnalyses.Visible = false;
                lblEquipmentAnalysesEmpty.Text = equipmentAnalysesEmpty;
                lblEquipmentAnalysesEmpty.Visible = true;
            }

            equipmentAnalyses.Visible = true;
        }

        #endregion

        #region Dropdown Events

        /// <summary>
        /// Binds equipment dropdown list by building id
        /// </summary>
        /// <param name="bid"></param>
        private void BindEquipmentDropdownList(int bid, DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataTextField = "EquipmentName";
            ddl.DataValueField = "EID";
            ddl.DataSource = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid);
            ddl.DataBind();
        }

        /// <summary>
        /// ddlAnalysesBuildings on selected index changed, binds equipment dropdow by selected building id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlAnalysesBuildings_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BindEquipmentDropdownList(Convert.ToInt32(ddlAnalysesBuildings.SelectedValue), ddlAnalysesEquipment);

            //hide equipment analyses
            equipmentAnalyses.Visible = false;

            //hide error message
            lblAnalysesError.Visible = false;
        }

        /// <summary>
        /// ddlAnalysesEquipment on selected index changed, binds equipment analyses list boxes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlAnalysesEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int eid = Convert.ToInt32(ddlAnalysesEquipment.SelectedValue);

            if (eid != -1)
            {
                BindEquipmentAnalyses(eid);
            }
            else
            {
                //hide equipment analyses
                equipmentAnalyses.Visible = false;
            }

            //hide error message
            lblAnalysesError.Visible = false;
        }

        #endregion
    }
}