﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EquipmentAnalyses.ascx.cs" Inherits="CW.Website._administration.equipment.nonkgs.EquipmentAnalyses" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Equipment Analyses">
    <SubTitleTemplate>Please select building and then a piece of equipment to view associated analyses.</SubTitleTemplate>
</CW:TabHeader>
<div>
    <a id="lnkSetFocusAnalyses" runat="server"></a>
    <asp:Label ID="lblAnalysesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
</div>
<div id="Div1" class="divForm" runat="server">   
    <label class="label">*Select Building:</label>    
    <asp:DropDownList ID="ddlAnalysesBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalysesBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
        <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
    </asp:DropDownList> 
</div>                                                                                                                                                       
<div class="divForm">   
    <label class="label">*Select Equipment:</label>    
    <asp:DropDownList ID="ddlAnalysesEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalysesEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
        <asp:ListItem Value="-1" >Select building first...</asp:ListItem>                           
    </asp:DropDownList>
</div>                                                                                       
<div id="equipmentAnalyses" visible="false" runat="server">                                            
    <hr />   
    <h2>Analyses List</h2>
    <div>
        <asp:Label ID="lblEquipmentAnalysesEmpty" Visible="false" runat="server"></asp:Label>                  
    </div>                                                                                                                                                                                                                                                                                                                   
    <asp:Repeater ID="rptEquipmentAnalyses" Visible="false" runat="server">
        <HeaderTemplate>                                                   
            <ul class="detailsListDefinitions">
        </HeaderTemplate>
        <ItemTemplate>                                                                                                                                                                                                                                              
                <%# "<li><strong>" + Eval("AnalysisName") + (String.IsNullOrEmpty(Convert.ToString(Eval("AnalysisTeaser"))) ? "</strong>" : ":</strong><span>" + Eval("AnalysisTeaser")) + "</span></li>"%>                                                   
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>   
</div>
