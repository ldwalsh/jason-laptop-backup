﻿using CW.Website._controls.admin.Equipment;

namespace CW.Website._administration.equipment.nonkgs
{
    public partial class EquipmentAdministrationPage: EquipmentAdministrationPageBase
    {
        #region events

            private void Page_Init()
            {
                //secondary security check specific to this page.
                //Check kgs super admin or higher, or super admin non provider, or super admin provider proxy.
                //equipmentAddTabPanel.Visible = (siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && !siteUser.IsLoggedInUnderProviderClient) || (siteUser.IsSuperAdmin && siteUser.IsProxyClient && siteUser.IsLoggedInUnderProviderClient));  
                EquipmentVariables.AdminMode = EquipmentVariables.AdminModeEnum.NONKGS;
            }

        #endregion
    }
}