﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentViewTab.ascx.cs" Inherits="CW.Website._administration.equipment.nonkgs.EquipmentViewTab" %>
<%@ Register TagPrefix="CW" Tagname="DropDowns" Src="~/_controls/DropDowns.ascx" %>
<%@ Register tagPrefix="CW" tagName="EquipmentForm" src="../EquipmentForm.ascx"  %>
<%@ Register tagPrefix="CW" tagName="ScheduleDownloadArea" src="ScheduleDownloadArea.ascx" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Data.Models.Equipment" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Equipment">
    <RightAreaTemplate>
        <CW:ScheduleDownloadArea runat="server" ID="scheduleDownloadArea" Visible="false" />
    </RightAreaTemplate>
</CW:TabHeader>

<CW:DropDowns runat="server" ID="dropDowns" validationEnabled="true" ValidationGroup="Search" StartViewMode="Building" ViewMode="EquipmentClass" OnBuildingChanged="dropDowns_BuildingChanged" OnEquipmentClassChanged="dropDowns_EquipmentClassChanged" />
<br/>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>                                                                                                                                                     
<div id="gridTbl">
    <asp:GridView runat="server" ID="grid" OnDataBound="gridEquipment_OnDataBound" EnableViewState="true" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true" >
        <Columns>
            <asp:CommandField ShowSelectButton="true" />
            <asp:TemplateField>
                <ItemTemplate>                                                
                        <asp:LinkButton ID="lbnEditLinkButton" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentName" HeaderText="Equipment Name"> 
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Equipment>(_ => _.EquipmentName)) %>"><%# GetCellContent(Container, PropHelper.G<Equipment>(_=>_.EquipmentName), 25) %></label>
                </ItemTemplate> 
            </asp:TemplateField>                                                                                                   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building Name">
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Equipment>(_ => _.Building.BuildingName)) %>"><%# GetCellContent(Container, PropHelper.G<Equipment>(_=>_.Building.BuildingName), 25) %></label>
                </ItemTemplate>   
            </asp:TemplateField>   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentTypeName" HeaderText="Equipment Type Name">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Equipment>(_ => _.EquipmentType.EquipmentTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<Equipment>(_=>_.EquipmentType.EquipmentTypeName), 25) %></label>
                </ItemTemplate>   
            </asp:TemplateField>   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ManufacturerModelName" HeaderText="Manufacturer Name">
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Equipment>(_ => _.EquipmentManufacturerModel.ManufacturerModelName)) %>"><%# GetCellContent(Container, PropHelper.G<Equipment>(_=>_.EquipmentManufacturerModel.ManufacturerModelName), 25) %></label>
                </ItemTemplate>
            </asp:TemplateField>  
            <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Equipment>(_ => _.IsActive)) %>"><%# GetCellContent(Container, PropHelper.G<Equipment>(_=>_.IsActive), 5) %></label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField SortExpression="IsVisible" HeaderText="Visible">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Equipment>(_ => _.IsVisible)) %>"><%# GetCellContent(Container, PropHelper.G<Equipment>(_=>_.IsVisible), 5) %></label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" NavigateUrl='<%# "/Connect.aspx?eid=" + GetCellContent(Container, PropHelper.G<Equipment>(_=>_.EID))%>' runat="server" Target="_blank">Connect</asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>                                                
                        <asp:LinkButton ID="lbnDeleteLinkButton" Runat="server" CausesValidation="false"
                            OnClientClick="return confirm('Are you sure you wish to delete this equipment permanently?\n\nAlternatively you can deactivate equipment temporarily instead.');"
                            CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</div>
<br />
<br /> 
<!--SELECT EQUIPMENT DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
    <Fields>
        <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
            <ItemTemplate>
                <div>
                    <h2>Equipment Details</h2>                                                                                                                                            
                    <ul class="detailsList">
                        <li>
                            <strong>Building Name: </strong>
                            <%# Eval(PropHelper.G<Equipment, Building>(eq=>eq.Building, b=>b.BuildingName)) %>
                        </li>
                        <li>
                            <strong>Equipment Name: </strong>
                            <%# Eval(PropHelper.G<Equipment>(eq=>eq.EquipmentName)) %>
                        </li>
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval("Description"))) ? "" : "<li><strong>Equipment Description: </strong><div class='divContentPreWrap'>" + Eval("Description") + "</div></li>"%>
                        <li>
                            <strong>Equipment Class Name: </strong>
                            <%# Eval(PropHelper.G<Equipment, EquipmentType, EquipmentClass>(eq => eq.EquipmentType, et => et.EquipmentClass, ec=>ec.EquipmentClassName))%>
                        </li>                        
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Equipment, EquipmentType, EquipmentClass>(eq => eq.EquipmentType, et => et.EquipmentClass, ec => ec.EquipmentClassDescription)))) ? "" : "<li><strong>Equipment Class Description: </strong>" + Eval(PropHelper.G<Equipment, EquipmentType, EquipmentClass>(eq => eq.EquipmentType, et => et.EquipmentClass, ec => ec.EquipmentClassDescription)) + "</li>"%>
                        <li>
                            <strong>Equipment Type Name: </strong>
                            <%# Eval(PropHelper.G<Equipment, EquipmentType>(eq=>eq.EquipmentType, et=>et.EquipmentTypeName)) %>
                        </li>
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Equipment, EquipmentType>(eq => eq.EquipmentType, et => et.EquipmentTypeDescription)))) ? "" : "<li><strong>Equipment Type Description: </strong>" + Eval(PropHelper.G<Equipment, EquipmentType>(eq => eq.EquipmentType, et => et.EquipmentTypeDescription)) + "</li>"%>
                        <li>
                            <strong>Manufacturer Name: </strong>
                            <%# Eval(PropHelper.G<Equipment, EquipmentManufacturerModel, EquipmentManufacturer>(eq=>eq.EquipmentManufacturerModel, emm=>emm.EquipmentManufacturer, em=>em.ManufacturerName))%>
                        </li>
                        <li>
                            <strong>Manufacturer Model Name: </strong>
                            <%# Eval(PropHelper.G<Equipment, EquipmentManufacturerModel>(eq=>eq.EquipmentManufacturerModel, emm=>emm.ManufacturerModelName))%>
                        </li>
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval("SerialNumber"))) ? "" : "<li><strong>Serial Number: </strong>" + Eval("SerialNumber") + "</li>"%>
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentLocation"))) ? "" : "<li><strong>Equipment Location: </strong><div class='divContentPreWrap'>" + Eval("EquipmentLocation") + "</div></li>"%>
                        
                        <li>
                            <strong>Equipment Image: </strong>
                            <%# "<img id='imgPreview' src='" + equipmentImageUrl + "' alt='" + Eval("EquipmentName") + "' class='imgDetailsListPreview' />"%>
                        </li>
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Equipment, EquipmentManufacturerContact>(eq => eq.EquipmentManufacturerContact, emc => emc.Name)))) ? "" : "<li><strong>Primary Manufacturer Contact: </strong><a href=\"/KGSEquipmentManufacturerContactAdministration.aspx?cid=" + Eval("PrimaryContactID") + "\">" + Eval(PropHelper.G<Equipment, EquipmentManufacturerContact>(eq => eq.EquipmentManufacturerContact, emc => emc.Name)) + "</a></li>"%> 
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<Equipment, EquipmentManufacturerContact>(eq => eq.EquipmentManufacturerContact1, emc => emc.Name)))) ? "" : "<li><strong>Secondary Manufacturer Contact: </strong><a href=\"/KGSEquipmentManufacturerContactAdministration.aspx?cid=" + Eval("SecondaryContactID") + "\">" + Eval(PropHelper.G<Equipment, EquipmentManufacturerContact>(eq => eq.EquipmentManufacturerContact1, emc => emc.Name)) + "</a></li>"%>                                                                                          
                        <asp:Literal Visible='<%# !(String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentNotes")))) %>' ID="litAdminBuildingsBody" Text='<%# "<li><strong>Equipment Notes: </strong><div class=richtext>" + Convert.ToString(Eval("EquipmentNotes")) + "</div></li>" %>' runat="server"></asp:Literal>                                                                                                                                                                                        
                        <li>
                            <strong>Active: </strong>
                            <%# Eval(PropHelper.G<Equipment>(eq=>eq.IsActive)) %>
                        </li>    
                        <li>
                            <strong>Visible: </strong>
                            <%# Eval(PropHelper.G<Equipment>(eq=>eq.IsVisible)) %>
                        </li>  
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedDate"))) && String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedBy"))) && String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedCompany"))) ? "" : "<br /><h2>Equipment Service Details</h2><ul class='detailsList'>" + (String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedDate"))) ? "" : "<li><strong>Last Serviced Date: </strong>" + Convert.ToDateTime(Eval("LastServicedDate")).ToShortDateString() + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedBy"))) ? "" : "<li><strong>Last Serviced By: </strong>" + Eval("LastServicedBy") + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedCompany"))) ? "" : "<li><strong>Last Serviced Company: </strong>" + Eval("LastServicedCompany") + "</li>") + "</ul>"%>                                                                                                                                      
                        <%# String.IsNullOrEmpty(Convert.ToString(Eval("ExternalEquipmentTypeName"))) && String.IsNullOrEmpty(Convert.ToString(Eval("CMMSReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("CMMSLocationID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("CMMSSiteID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("CMMSLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("BIMReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("BIMLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("DMSReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("DMSLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("GISReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("GISLink"))) ? "" : "<br /><h2>Additional Equipment Details</h2><ul class='detailsListWidest'>" + (String.IsNullOrEmpty(Convert.ToString(Eval("ExternalEquipmentTypeName"))) ? "" : "<li><strong>External Equipment Type Name: </strong>" + Eval("ExternalEquipmentTypeName") + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("CMMSReferenceID"))) ? "" : "<li><strong>CMMS (Computerized Maintenance Mgmt System) Reference/Asset ID: </strong>" + Eval("CMMSReferenceID") + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("CMMSLocationID"))) ? "" : "<li><strong>CMMS (Computerized Maintenance Management System) Location ID: </strong>" + Eval("CMMSLocationID") + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("CMMSSiteID"))) ? "" : "<li><strong>CMMS (Computerized Maintenance Management System) Site ID: </strong>" + Eval("CMMSSiteID") + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("CMMSLink"))) ? "" : "<li><strong>CMMS (Computerized Maintenance Management System) Link: </strong><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("CMMSLink").ToString()) + "'>" + Eval("CMMSLink") + "</a></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("BIMReferenceID"))) ? "" : "<li><strong>BIM (Building Information Management) Reference ID: </strong>" + Eval("BIMReferenceID") + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("BIMLink"))) ? "" : "<li><strong>BIM (Building Information Management) Link: </strong><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("BIMLink").ToString()) + "'>" + Eval("BIMLink") + "</a></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("DMSReferenceID"))) ? "" : "<li><strong>DMS (Document Management Software) Reference ID: </strong>" + Eval("DMSReferenceID") + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("DMSLink"))) ? "" : "<li><strong>DMS (Document Management Software) Link: </strong><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("DMSLink").ToString()) + "'>" + Eval("DMSLink") + "</a></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("GISReferenceID"))) ? "" : "<li><strong>GIS (Geographic Information System) Reference ID: </strong>" + Eval("GISReferenceID") + "</li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("GISLink"))) ? "" : "<li><strong>GIS (Geographic Information System) Link: </strong><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("GISLink").ToString()) + "'>" + Eval("GISLink") + "</a></li>") + "</ul>"%>
                    </ul>
                </div>
            </ItemTemplate>                                             
        </asp:TemplateField>
    </Fields>
</asp:DetailsView>
                                                                     
<!--EDIT EQUIPMENT PANEL -->                              
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">                                                                                             
    <h2>Edit Equipment</h2>
    <div>
        <CW:EquipmentForm runat="server" ID="equipmentForm" FormMode="Update" UpdateButtonCaption="Update Equipment" IsActiveVisible="true" IsVisibleVisible="true" />
    </div>
</asp:Panel>