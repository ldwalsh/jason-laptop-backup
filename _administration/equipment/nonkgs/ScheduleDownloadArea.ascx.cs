﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Website._framework;
using CW.Website.DependencyResolution;

using CW.Logging;
using CW.Reporting._fileservices;
using CW.Reporting._administration.equipment;
using CW.Website._framework.httphandlers;
using CW.Business.Query;
using CW.Website.AnalysisBuilder.Service;
using CW.Data;

namespace CW.Website._administration.equipment.nonkgs
{
    // TODO: NP - check if we can change to SiteUserControl?
    public partial class ScheduleDownloadArea: UserControl
    {
        #region properties
        protected CW.Business.DataManager DataMgr { get { return CW.Business.DataManager.Get(System.Web.HttpContext.Current.Session.SessionID); } }

        #endregion properties
        #region method

        #region override

        protected override void OnInit(EventArgs e)
                {
                    base.OnInit(e);

                    lnkDownload.Click += Download_Click;
                    imgDownload.Click += Download_Click;

                    LogMgr = IoC.Resolve<ISectionLogManager>();
                    mSiteUser = SiteUser.Current;
                }

            #endregion

            private void Download_Click(Object sender, EventArgs ea)
            {

                if (BuildingDropDown == null) throw new InvalidOperationException("The 'BuildingDropDown' property must be set on the ScheduleDownloadArea instance.");

                try
                {
                    FilePublishService file = new FilePublishService
                    (
                        DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                        DataMgr.ClientDataMapper.GetClientSettingsByClientID(mSiteUser.CID),
                        mSiteUser.IsKGSFullAdminOrHigher,
                        mSiteUser.IsSchneiderTheme,
                        mSiteUser.Email,
                        new EquipmentScheduleGenerator(new EquipmentScheduleGenerator.BuildingInfo(Int32.Parse(BuildingDropDown.SelectedValue), BuildingDropDown.SelectedItem.Text), GetAllEquipVarVals),
                        LogMgr
                    );
                    if (file.AttachAsDownloadWithNoDataCheck(new DownloadRequestHandler.HandledFileAttacher()))
                    {
                        lblError.Visible = false;
                    }
                    else
                    {
                        lblError.Text = "No data to download.";
                        lblError.Visible = true;
                    }
                }
                catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
                {
                    lblError.Text = "Max monthly download limit reached.";
                    lblError.Visible = true;

                    return;
                }            
            }
            static public System.Collections.Generic.IList<Equipment_EquipmentVariable> GetAllEquipVarVals(int iBID)
            {
                return(new Equipment_EquipmentVariableQuery(System.Configuration.ConfigurationManager.ConnectionStrings["CWConnectionString"].ConnectionString,null)
                            .LoadWith<CW.Data.Equipment, EquipmentType>(_ => _.Equipment, _ => _.EquipmentType, _ => _.EquipmentClass)
                            .LoadWith(_ => _.EquipmentVariable)
                            .FinalizeLoadWith
                            .All
                            .ByBuilding(iBID)
                            .Sort(_ => _.Equipment.EquipmentType.EquipmentClass.EquipmentClassName)
                            .ToList()
                );
            }

        #endregion

        #region field

            private ISiteUser mSiteUser; 

        #endregion

        #region property

            private ISectionLogManager LogMgr { get; set; }

            public DropDownList BuildingDropDown
            {
                set;
                private get;
            }

        #endregion
    }
}