﻿using CW.Data;
using System;

namespace CW.Website._administration.equipment.nonkgs
{
    public partial class EquipmentAddTab: AddTabBase<Equipment,EquipmentAdministrationPage>
    {
        #region method

            protected void Page_Init(Object sender, EventArgs e)
            {
                CanEntityBeAdded = equipmentForm.CanEntityBePersisted;
                AddEntityMethod  = equipmentForm.PersistEntityAction;
            }

        #endregion
    }
}