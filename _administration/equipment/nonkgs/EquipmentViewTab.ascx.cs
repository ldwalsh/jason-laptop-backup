﻿using CW.Business.Query;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;

namespace CW.Website._administration.equipment.nonkgs
{
    public partial class EquipmentViewTab: ViewTabWithFormBase<Equipment,EquipmentAdministrationPage>
    {
		#region const

			public const String noEquipmentImage = "no-equipment-image.png";
			public const String pointsExist = "Cannot delete equipment because points are associated. Please unassociate or delete points first.";

		#endregion
		
		#region fields

			public Byte[] equipmentImage;
			public String equipmentImageUrl;

		#endregion

		#region method

            #region overrides

                protected override IEnumerable<GridCacher.GridSet.Row> FilterRows(GridCacher.GridSet gridSet) //rename
                {
                    if (dropDowns.EquipmentClassID != null) gridSet.Rows = gridSet.Rows.Where(r => Convert.ToInt32(r.Cells[PropHelper.G<EquipmentClass>(_ => _.EquipmentClassID)]) == dropDowns.EquipmentClassID);

                    return gridSet.Rows;
                }

            #endregion

            private void Page_Configure()
			{
				CanEntityBeUpdated = equipmentForm.CanEquipmentBePersisted;
				CanEntityBeDeleted = CanEquipmentBeDeleted;

				UpdateEntityMethod = equipmentForm.PersistEntityAction;
				DeleteEntityMethod = ((id) => DataMgr.EquipmentDataMapper.DeleteEquipment(siteUser.CID, Int32.Parse(id)));
			}

			private void Page_FirstInit()
			{
				pnlSearch.Visible = false;

                var buildings = siteUser.VisibleBuildings;

                dropDowns.BindBuildingList(buildings);
			}

			private void Page_Load()
			{
                scheduleDownloadArea.BuildingDropDown = dropDowns.GetDDL(DropDowns.ViewModeEnum.Building);
			}

            protected void dropDowns_BuildingChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
            {
                Int32 bid;

                scheduleDownloadArea.Visible = (Int32.TryParse(ea.SelectedValue, out bid) && (ea.SelectedValue != "-1"));
                scheduleDownloadArea.FindControl("lblError").Visible = false;

                if (ea.SelectedValue == "-1") return;

                sender.BindEquipmentClassList((ea.SelectedValue == "0") ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses() : DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(Int32.Parse(ea.SelectedValue)), true, true);
            }

            protected void dropDowns_EquipmentClassChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
            {
                var visible = ea.SelectedValue != "-1";

                ControlHelper.ToggleVisibility(visible, new Control[] { equipmentForm, pnlSearch, scheduleDownloadArea, grid, });

                scheduleDownloadArea.FindControl("lblError").Visible = false;

                if (!visible) return;

                BindGrid();
            }

			protected void gridEquipment_OnDataBound(object sender, EventArgs e)
			{
				if (RoleHelper.IsSuperAdminOrHigher(siteUser.RoleID.ToString())) return;

				var columns = grid.Columns;
                
				columns[1].Visible = false;
				columns[9].Visible = false;
			}

			private Boolean CanEquipmentBeDeleted(String eid, out String message)
			{
				var id = Convert.ToInt32(eid);

				if (DataMgr.EquipmentPointsDataMapper.ArePointsAssignedToEquipment(id))
				{
					message = pointsExist;

					return false;
				}

				message = null;

				return true;
			}

		#endregion

		#region property

			protected override IEnumerable<String> GridColumnNames
			{
				get
				{
					return
					PropHelper.F<Equipment>
					(
						_=>_.EquipmentName, _=>_.Building.BuildingName, _=>_.EquipmentType.EquipmentTypeName, _=>_.EquipmentManufacturerModel.ManufacturerModelName, _=>_.IsActive, _=>_.IsVisible, _=>_.EID, _=>_.EquipmentType.EquipmentClassID
					);
				}
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
                get { return (searchText => DataMgr.EquipmentDataMapper.SearchEquipmentByBuildingID(searchText.FirstOrDefault(), Convert.ToInt32(dropDowns.BID))); }
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get
				{
					return
					(
						eid => DataMgr.EquipmentDataMapper.GetEquipment
						(
							Convert.ToInt32(eid),
							new Expression<Func<Equipment, Object>>[] { (eq => eq.Building), (eq => eq.EquipmentManufacturerModel), (eq => eq.EquipmentType), (eq => eq.EquipmentManufacturerContact), (eq => eq.EquipmentManufacturerContact1) },
							new Expression<Func<Building, Object>>[] { (b => b.Client) },
							new Expression<Func<EquipmentManufacturerModel, Object>>[] { (emm => emm.EquipmentManufacturer) },
							new Expression<Func<EquipmentType, Object>>[] { (eqt => eqt.EquipmentClass) }
						)
					); 
				}
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get
				{
					return
					(
						eid => DataMgr.EquipmentDataMapper.GetEquipment
						(
							Convert.ToInt32(eid),
							new Expression<Func<Equipment,Object>>[]{(eq=> eq.Building), (eq=> eq.EquipmentManufacturerModel), (eq=> eq.EquipmentType), (eq=> eq.EquipmentManufacturerContact), (eq=> eq.EquipmentManufacturerContact1)},
							new Expression<Func<Building,Object>>[]{(b=> b.Client) },
							new Expression<Func<EquipmentManufacturerModel, Object>>[] { (emm => emm.EquipmentManufacturer) },
							new Expression<Func<EquipmentType, Object>>[] { (eqt => eqt.EquipmentClass) }
						)
					);
				}
			}

			protected override String CacheSubKey
			{
                get { return (dropDowns.BID != null) ? dropDowns.BID.ToString() : String.Empty; }
			}

		#endregion
    }
}