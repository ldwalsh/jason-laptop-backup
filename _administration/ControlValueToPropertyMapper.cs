﻿using CW.Utility;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._administration
{
	public static class ControlValueToPropertyMapper
	{
		#region CLASS

			public class PropertyAccessor
			{
				#region STATIC

					private static PropertyAccessor CreateTextD<TControl>(String propName) where TControl: Control
					{
						var prop = GetProperty(typeof(TControl), propName);

						return new TypedPropertyAccessor<TControl>
						(
							delegate(TControl control, Object value)
							{							
								var val = value;

								if (prop.PropertyType == typeof(Boolean)) //revise
								{
									if (val is String)
									{
										val = Boolean.Parse(val.ToString());
									}
								}

								if (prop.PropertyType == typeof(String)) //revise
								{
									val = val.ToString();
								}

								prop.SetValue(control, val, null);
							},
							_=> prop.GetValue(_, null)
						);
					}

					public static KeyValuePair<Type,PropertyAccessor> CreateDirect<TControl>(String propName) where TControl: Control
					{
						return new KeyValuePair<Type,PropertyAccessor>(typeof(TControl), CreateTextD<TControl>(propName));
					}

				#endregion

				public Action<Control,Object> Set;
				public Func  <Control,Object> Get;
			
				protected PropertyAccessor()
				{}

				public PropertyAccessor(Action<Control,Object> setter, Func<Control,Object> getter)
				{
					Set = setter;
					Get = getter;
				}
			}

			public class TypedPropertyAccessor<TControl>: PropertyAccessor where TControl: Control
			{
				private TypedPropertyAccessor()
				{}

				public TypedPropertyAccessor(Action<TControl,Object> setter, Func<TControl,Object> getter)
				{
					Set = new Action<Control,Object>((control,value)=> setter((TControl)control, value));
					Get = new Func  <Control,Object>((control      )=> getter((TControl)control       ));
				}
			}

		#endregion

		#region constructor

			static ControlValueToPropertyMapper()
			{
				Maps = new Dictionary<Type,PropertyAccessor>();

				Maps.Add(PropertyAccessor.CreateDirect<HiddenField>(nameof(HiddenField.Value)));
				Maps.Add(PropertyAccessor.CreateDirect<TextBox>(nameof(TextBox.Text)));
				Maps.Add(PropertyAccessor.CreateDirect<CheckBox>(nameof(CheckBox.Checked)));
				Maps.Add(PropertyAccessor.CreateDirect<DropDownList>(nameof(DropDownList.SelectedValue)));
				Maps.Add(PropertyAccessor.CreateDirect<Label>(nameof(Label.Text)));
				Maps.Add(PropertyAccessor.CreateDirect<ListBox>(nameof(ListBox.SelectedValue)));
				Maps.Add(PropertyAccessor.CreateDirect<HtmlTextArea>(nameof(HtmlTextArea.InnerHtml)));
				Maps.Add(PropertyAccessor.CreateDirect<RadEditor>(nameof(RadEditor.Content)));
				Maps.Add(PropertyAccessor.CreateDirect<RadColorPicker>(nameof(RadColorPicker.SelectedColor)));
				Maps.Add(PropertyAccessor.CreateDirect<RadMaskedTextBox>(nameof(RadMaskedTextBox.Text)));
				Maps.Add(PropertyAccessor.CreateDirect<Literal>(nameof(Literal.Text)));
                Maps.Add(PropertyAccessor.CreateDirect<HtmlInputGenericControl>(nameof(HtmlInputGenericControl.Value)));
			}

			public static PropertyInfo GetProperty(Type type, String name)=> type.GetProperty(name, (BindingFlags.Instance|BindingFlags.Public));

		#endregion

		#region property

			public static IDictionary<Type,PropertyAccessor> Maps
			{
				private set;
				get;
			}

		#endregion
	}
}