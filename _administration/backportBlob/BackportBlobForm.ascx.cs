﻿using CW.Common.Constants;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using System;
using System.Linq;
using System.Web.UI.WebControls;
using CW.Data.Helpers;
using System.Collections.Generic;
using CW.Data;
using CW.Data.Collections;

namespace CW.Website._administration.backportBlob
{
    public partial class BackportBlobForm : FormGenericBase<BackportBlobForm, BlobBackport>
    {
        #region method

            #region override

                public override void CreateEntity(BlobBackport obj)
                {
                    base.CreateEntity(obj);
                }

            #endregion

            private void Page_FirstInit()
            {
                BindOrganizations(ddlOrganizations);
                BindClients(ddlClients);
            }

            public Boolean CanBackportBePersisted(BlobBackport backport, out String message)
            {
                bool canBePersisted = true;
                message = null;

                if(String.IsNullOrWhiteSpace(backport.ContainerNamePrefixes))
                {
                    message += "Container Name Prefixes missing; ";
                    canBePersisted = false;
                }

                StorageAccountLevel tmpLevel = null;
                if (!(StorageAccountLevel.TryParseFromValue<StorageAccountLevel>(backport.ToAccountLevel, out tmpLevel)))
                {
                    message += String.Format("To Account Level {0} not found; ", backport.ToAccountLevel);
                    canBePersisted = false;
                }

                if (!(StorageAccountLevel.TryParseFromValue<StorageAccountLevel>(backport.FromAccountLevel, out tmpLevel)))
                {
                    message += String.Format("From Account Level {0} not found; ", backport.FromAccountLevel);
                    canBePersisted = false;
                }

                EnvironmentType tmpEnv = null;

                if (!(EnvironmentType.TryParseFromValue<EnvironmentType>(backport.FromEnvironment, out tmpEnv)))
                {
                    message += String.Format("From Environment {0} not found; ", backport.FromEnvironment);
                    canBePersisted = false;
                }

                if (!(EnvironmentType.TryParseFromValue<EnvironmentType>(backport.ToEnvironment, out tmpEnv)))
                {
                    message += String.Format("To Environment {0} not found; ", backport.ToEnvironment);
                    canBePersisted = false;
                }

                return canBePersisted;
            }

            public void PersistBackport(BlobBackport backport)
            {
                switch (FormMode)
                {
                    case PersistModeEnum.Insert:
                        {
                            ScheduleBackportJobs(backport);

                            break;
                        }
                    case PersistModeEnum.Update:
                        {
                            break;
                        }
                }
            }

            private void ScheduleBackportJobs(BlobBackport backport)
            {
                // Copy backport
                var insertRecord = new BlobBackport(backport);

                // reset new backport fields
                insertRecord.Status = "Scheduled";

                int orgValue = Convert.ToInt32(ddlOrganizations.SelectedValue);
                int clientValue = Convert.ToInt32(ddlClients.SelectedValue);

                if (orgValue > 0)
                {
                    insertRecord.OrganizationName = ddlOrganizations.SelectedItem.Text;
                    insertRecord.OID = orgValue;
                }

                if (clientValue > 0)
                {
                    insertRecord.ClientName = ddlClients.SelectedItem.Text;
                    insertRecord.CID = clientValue;
                }

                insertRecord.HasSameContainer = rblTransferType.Text == "Backport";

                insertRecord.FromAccountLevel = acctSelector.FromLevel;
                insertRecord.ToAccountLevel = acctSelector.ToLevel;
                insertRecord.FromEnvironment = acctSelector.FromEnvironment;
                insertRecord.ToEnvironment = acctSelector.ToEnvironment;
                // set automatically by naming convention
                //insertRecord.FromAccountType = ddlFromAccountType.SelectedValue;
                //insertRecord.ToAccountType = ddlToAccountType.SelectedValue;
                //insertRecord.ContainerNamePrefixes = txtContainerNamePrefixes.Text;

                // Insert Table Entity
                DataMgr.BlobBackportDataMapper.InsertBackportJobEntity(insertRecord);

                // Insert Queue Message
                DataMgr.BlobBackportDataMapper.InsertBackportJobQueueMessage(insertRecord);
            }

        #endregion

        #region Dropdown Events

            protected void ddlOrganizations_OnSelectedIndexChanged(Object sender, EventArgs e)
            {
                if (ddlOrganizations.SelectedValue != "-1")
                    BindClients(ddlClients, null , DataMgr.ClientDataMapper.GetAllActiveClientsByOrg(Convert.ToInt32(ddlOrganizations.SelectedValue)));
            }

        #endregion

        #region bind methods

            private void BindOrganizations(DropDownList ddl, ListItemCollection customItems = null)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);

                if (customItems != null)
                    foreach (ListItem li in customItems)
                        ddl.Items.Add(li);
                
                ddl.DataTextField = "OrganizationName";
                ddl.DataValueField = "OID";
                ddl.DataSource = DataMgr.OrganizationDataMapper.GetAllOrganizations();
                ddl.DataBind();
            }

            private void BindClients(DropDownList ddl, ListItemCollection customItems = null, IEnumerable<Client> clients = null)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select organization...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);

                if (customItems != null)
                    foreach (ListItem li in customItems)
                        ddl.Items.Add(li);

                ddl.DataTextField = "ClientName";
                ddl.DataValueField = "CID";
                ddl.DataSource = clients == null ? DataMgr.ClientDataMapper.GetAllActiveClients() : clients;    
                ddl.DataBind();
            }

        #endregion

        #region property

            public override Delegates<BlobBackport>.CanEntityBePersistedDelegate CanEntityBePersisted
            {
                get { return CanBackportBePersisted; }
            }

            public override Action<BlobBackport> PersistEntityAction
            {
                get { return PersistBackport; }
            }

        #endregion
    }
}
