﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Utility.Web;

namespace CW.Website._administration.backportBlob.system
{
    public partial class SystemBackportBlobViewTab : ViewTabWithFormBase<BlobBackport, SystemBackportBlobAdministration>, IPostBackEventHandler
    {
        #region method

            #region override

            protected override void PopulateEditForm(BlobBackport obj)
            {
                throw new NotImplementedException();
            }

            protected override void GridRowCreated(Object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ControlHelper.GridViewControl.SetLinkButtonRowIndex("lbnDownloadLinkButton", e);
                    ControlHelper.GridViewControl.SetLinkButtonRowIndex("lbnRescheduleLinkButton", e);
                }

                base.GridRowCreated(sender, e);
            }

            protected override void GridRowCommand(Object sender, GridViewCommandEventArgs e)
            {
                if (e.CommandName == "Download")
                {
                    DownloadObject(grid.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
                    return;
                }

                if (e.CommandName == "Reschedule")
                {
                    RescheduleObject(grid.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
                    return;
                }

                base.GridRowCommand(sender, e);
            }

			#endregion

            private void Page_Configure()
			{
                //CanObjectBeUpdated = clientForm.CanObjectBePersisted;
                
                CanEntityBeDeleted = CanBackportBeDeleted;

                //UpdateObjectMethod = clientForm.PersistObjectAction;
                //CanObjectBeDownload = CanBackportBeDownloaded; 

                DeleteEntityMethod = DeleteBackport;
                //DownloadObjectMethod = DownloadBackport;
                //RescheduleObjectMethod = RescheduleBackport;
			}

            private void Page_FirstInit()
            {
                pnlSearch.Visible = false;

                BindOrganizations(ddlOrganizations);
                BindClients(ddlClients, null);
            }

            private void Page_Load()
            {
            }

            protected void DownloadObject(String id)
            {
                string message;

                if (CanBackportBeDownloaded(id, out message))
                {
                    DownloadBackport(id);
                }

                DisplayMessage(message);
            }

            protected void RescheduleObject(String id)
            {
                string message = RescheduleBackport(id);

                DisplayMessage(message);
            }

            private Boolean CanBackportBeDownloaded(string backportID, out String message)
            {
                message = null;

                var dates = GetSearchDates();

                var backport = DataMgr.BlobBackportDataMapper.GetBlobBackportJobEntities(dates.Item1, dates.Item2).Where(b => b.BPID == backportID).FirstOrDefault();
                FilePublishService file = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    new BlobBackportFileGenerator(DataMgr.BlobBackportDataMapper.GetBlobBackportJobBlob, DataMgr.BlobBackportDataMapper.GetBlobBackportJobEntity, backport.DateCreatedUTC, backportID),
                    LogMgr
                );
                if (!file.HasDataCheck(new DownloadRequestHandler.HandledFileAttacher()))
                {
                    message = "No data to download.";
                    return false;
                }

                return true;
            }

            private Boolean CanBackportBeDeleted(String backportID, out String message)
            {
                message = null;

                return true;
            }

            private string RescheduleBackport(String backportID)
            {
                var dates = GetSearchDates();
                var oldBackport = DataMgr.BlobBackportDataMapper.GetBlobBackportJobEntities(dates.Item1, dates.Item2).Where(b => b.BPID == backportID).FirstOrDefault();

                var newBackport = new BlobBackport(oldBackport);

                // delete blob report
                DataMgr.BlobBackportDataMapper.DeleteBlobBackportJobEntity(oldBackport.BPID, oldBackport.DateCreatedUTC);
                DataMgr.BlobBackportDataMapper.DeleteBlobBackportJobBlob(oldBackport.BPID, oldBackport.DateCreatedUTC);

                // reset new backport fields
                newBackport.Reset();
                newBackport.Status = "Scheduled";
                newBackport.DateCreatedUTC = DateTime.UtcNow;
                newBackport.BPID = backportID;

                // update table entity
                DataMgr.BlobBackportDataMapper.InsertBackportJobEntity(newBackport);

                // insert queue message
                DataMgr.BlobBackportDataMapper.InsertBackportJobQueueMessage(newBackport);

                return "Backport Rescheduled";
            }

            private void DownloadBackport(String backportID)
            {
                var dates = GetSearchDates();
                var backport = DataMgr.BlobBackportDataMapper.GetBlobBackportJobEntities(dates.Item1, dates.Item2).Where(b => b.BPID == backportID).FirstOrDefault();

                new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    new BlobBackportFileGenerator(DataMgr.BlobBackportDataMapper.GetBlobBackportJobBlob, DataMgr.BlobBackportDataMapper.GetBlobBackportJobEntity, backport.DateCreatedUTC, backportID),
                    LogMgr
                ).AttachAsDownloadWithNoDataCheck(new DownloadRequestHandler.HandledFileAttacher());
            }

            private void DeleteBackport(String backportID)
            {
                try
                {
                    var dates = GetSearchDates();
                    var backport = DataMgr.BlobBackportDataMapper.GetBlobBackportJobEntities(dates.Item1, dates.Item2).Where(b => b.BPID == backportID).FirstOrDefault();

                    // Delete Blob File
                    DataMgr.BlobBackportDataMapper.DeleteBlobBackportJobBlob(backport.BPID, backport.DateCreatedUTC);

                    // Delete Table Entity
                    DataMgr.BlobBackportDataMapper.DeleteBlobBackportJobEntity(backport.BPID, backport.DateCreatedUTC);
                }
                catch(Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Failed to delete backport: " + backportID, ex);
                }

                page.TabStateMonitor.ChangeState();
            }

            private Tuple<DateTime,DateTime> GetSearchDates()
            {
                DateTime startDate;
                DateTime endDate;

                if(!DateTime.TryParse(txtStartDate.Text, out startDate))
                    return null;
                
                if(!DateTime.TryParse(txtEndDate.Text, out endDate))
                    return null;



                return Tuple.Create<DateTime, DateTime>(startDate, endDate);
            }

            private IEnumerable<Expression<Func<BlobBackport, bool>>> GetQueryExpressions()
            {
                var expressions = new List<Expression<Func<BlobBackport, bool>>>();

                if (ddlOrganizations.SelectedValue != "-1")
                    expressions.Add(b => b.OID == Convert.ToInt32(ddlOrganizations.SelectedValue));
                else if (chbNonOrgClient.Checked)
                    expressions.Add(b => b.OID == 0);

                if (ddlClients.SelectedValue != "-1")
                    expressions.Add(b => b.CID == Convert.ToInt32(ddlClients.SelectedValue));
                else if (chbNonOrgClient.Checked)
                    expressions.Add(b => b.CID == 0);

                if (acctSelector.FromLevel != "-1")
                    expressions.Add(b => b.FromAccountLevel == acctSelector.FromLevel);

                if (acctSelector.ToLevel != "-1")
                    expressions.Add(b => b.ToAccountLevel == acctSelector.ToLevel);

                if (acctSelector.FromLevel != "-1")
                    expressions.Add(b => b.FromEnvironment == acctSelector.FromEnvironment);

                if (acctSelector.ToLevel != "-1")
                    expressions.Add(b => b.ToEnvironment == acctSelector.ToEnvironment);

                return expressions.ToArray();
            }

		#endregion

        #region Button Events

                protected void btnSelect_Click(object sender, EventArgs e)
                {
                    BindGrid();
                }

        #endregion

        #region Dropdown Events

                protected void ddlOrganizations_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindClients(ddlClients, DataMgr.ClientDataMapper.GetAllActiveClientsByOrg(Convert.ToInt32(ddlOrganizations.SelectedValue)));
                }

        #endregion

		#region property

			protected override IEnumerable<String> GridColumnNames
			{
				get
				{
					return
					PropHelper.F<BlobBackport>(_=>_.BPID, _=>_.ClientName, _=>_.OrganizationName, _=>_.ContainerNamePrefixes, _=>_.DateCreatedUTC, _=>_.ToAccountLevel, _=>_.FromAccountLevel, _=>_.ToEnvironment, _=>_.FromEnvironment, _=>_.Status);
				}
			}

            protected override String EntityTypeName
            {
                get { return "blob backport"; } //typeof(BlobBackport).Name
            }

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
                get
                {
                    var dates = GetSearchDates();
                    var expressions = GetQueryExpressions();

                    if (dates == null)
                        return (searchText => Enumerable.Empty<BlobBackport>());

                    return (searchText => DataMgr.BlobBackportDataMapper.SearchBlobBackport(expressions, dates.Item1, dates.Item2, searchText).ToList());
                }
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
                get
                {
                    var dates = GetSearchDates();
                    var expressions = GetQueryExpressions();

                    if (dates == null)
                        return (searchText => null);

                    return (id => DataMgr.BlobBackportDataMapper.SearchBlobBackport(expressions, dates.Item1, dates.Item2, null).Where(b => b.BPID == id).FirstOrDefault());
                }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
                get
                {
                    var dates = GetSearchDates();

                    if (dates == null)
                        return (searchText => null);

                    return (id => DataMgr.BlobBackportDataMapper.SearchBlobBackport(null, dates.Item1, dates.Item2, null).Where(b => b.BPID == id).FirstOrDefault());
                }
			}

            private GetEntitiesForGridDelegate GetBackportJobs()
            {
                var dates = GetSearchDates();
                var expressions = GetQueryExpressions();

                if (dates == null)
                    return (searchText => Enumerable.Empty<BlobBackport>());

                return (searchText => DataMgr.BlobBackportDataMapper.SearchBlobBackport(expressions, dates.Item1, dates.Item2, searchText).ToList());
            }

            protected override String CacheSubKey
            {
                get { return String.Join("-", txtStartDate.Text, txtEndDate.Text, ddlOrganizations.SelectedValue, ddlClients.SelectedValue, acctSelector.FromLevel, acctSelector.ToLevel, acctSelector.FromEnvironment, acctSelector.ToEnvironment, chbNonOrgClient.Checked); }
            }

			public override IEnumerable<String> SearchGridColumnNames
			{
				get {return new[]{PropHelper.G<BlobBackport>(_=>_.ContainerNamePrefixes)};}
			}

		#endregion

        #region Bind methods

            private void BindClients(DropDownList ddl, IEnumerable<Client> clients)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);

                if (clients != null)
                {
                    ddl.DataTextField = "ClientName";
                    ddl.DataValueField = "CID";
                    ddl.DataSource = clients;
                    ddl.DataBind();
                }
            }

        #endregion

        #region IPostBackEventHandler

            void IPostBackEventHandler.RaisePostBackEvent(String eventArgument) //revise this approach and access the TabStateMonitor instance directly client-side
            {
                switch (eventArgument)
                {
                    case "TabStateMonitor.ChangeState":
                        {
                            this.page.TabStateMonitor.ChangeState();

                            BindGrid(true);

                            break;
                        }
                }
            }

		#endregion

        #region Page Events
            protected void lnkBtnRefreshCache_Click(object sender, EventArgs e)
            {
                page.TabStateMonitor.ChangeState();
            }
        #endregion
    }
}