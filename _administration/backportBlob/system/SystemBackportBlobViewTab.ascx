<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemBackportBlobViewTab.ascx.cs" Inherits="CW.Website._administration.backportBlob.system.SystemBackportBlobViewTab" %>

<%@ Import Namespace="CW.Data.AzureStorage.Models" %>
<%@ Import Namespace="CW.Utility" %>

<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register TagPrefix="CW" TagName="AccountSelector" src="~/_administration/backport/StorageAccountSelector.ascx" %>

<script type="text/javascript">
    var $this = new function () {
        this.PopulateDates = function (interval) {
            var d = new Date(new Date().toUTCString().substr(0, 25));
            var dateFormat = "MM/dd/yyyy hh:mm:ss tt";

            document.getElementById('<%=txtEndDate.ClientID %>').value = d.format(dateFormat);
            document.getElementById('<%=txtStartDate.ClientID %>').value = new Date(d.setHours(d.getHours() - parseInt(interval))).format(dateFormat);
        };
    };

    var DatetimeComparer = function (sender, args) {
        var start = new Date(document.getElementById(sender.controltovalidate.replace("End", "Start")).value);
        var end = new Date(document.getElementById(sender.controltovalidate).value);

        args.IsValid = (end >= start);
    };
</script>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Blob Backport Jobs" />
<div>
    <div class="divForm" runat="server">  
        <div class="quickDates">Prior:
            <a href="javascript:" onclick="$this.PopulateDates(2016);">3 months</a>
            <a href="javascript:" onclick="$this.PopulateDates(672);">1 month</a>
            <a href="javascript:" onclick="$this.PopulateDates(168);">1 week</a>
            <a href="javascript:" onclick="$this.PopulateDates(24);">1 day</a>
        </div>
    </div>
    <div class="divForm">
        <label class="label">Start Date (UTC):</label>  <span style="float:right"><asp:LinkButton runat="server" ID="lnkBtnRefreshCache" Text="Refresh Cache" AutoPostBack="true" OnClick="lnkBtnRefreshCache_Click"></asp:LinkButton></span>  
        <asp:TextBox ID="txtStartDate" CssClass="textbox" runat="server" />
        <ajaxToolkit:MaskedEditExtender ID="startDateMaskedEditExtender" runat="server" TargetControlID="txtStartDate" Mask="99/99/9999 99:99:99" AcceptAMPM="true" MaskType="DateTime" ClipboardEnabled="true" InputDirection="LeftToRight" ClearMaskOnLostFocus="true" />
        <asp:RequiredFieldValidator ID="dateStartRequiredValidator" runat="server"
                            ErrorMessage="Date is a required field."
                            ControlToValidate="txtStartDate"          
                            SetFocusOnError="true"
                            Display="None"
                            ValidationGroup="SystemBackportBlob">
                            </asp:RequiredFieldValidator>
        <ajaxToolkit:ValidatorCalloutExtender ID="dateStartRequiredValidatorExtender" runat="server"
                            BehaviorID="dateStartRequiredValidatorExtender"
                            TargetControlID="dateStartRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>                                          
    </div>
    <div class="divForm">
        <label class="label">End Date (UTC):</label>
        <asp:TextBox ID="txtEndDate" CssClass="textbox" runat="server" />
        <ajaxToolkit:MaskedEditExtender ID="endDateMaskedEditExtender" runat="server" TargetControlID="txtEndDate" Mask="99/99/9999 99:99:99" AcceptAMPM="true" MaskType="DateTime" ClipboardEnabled="true" InputDirection="LeftToRight" ClearMaskOnLostFocus="true" />                    
        <asp:RequiredFieldValidator ID="dateEndRequiredValidator" runat="server"
                            ErrorMessage="End date is a required field."
                            ControlToValidate="txtEndDate"          
                            SetFocusOnError="true"
                            Display="None"
                            ValidationGroup="SystemBackportBlob">
                            </asp:RequiredFieldValidator>
        <ajaxToolkit:ValidatorCalloutExtender ID="dateEndRequiredValidatorExtender" runat="server"
                            BehaviorID="dateEndRequiredValidatorExtender"
                            TargetControlID="dateEndRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>                       
        <asp:CustomValidator ID="dateCustomValidator" runat="server" ClientValidationFunction="DatetimeComparer" ControlToValidate="txtEndDate" EnableClientScript="true" ErrorMessage="End Date cannot be before Start Date" Display="None" ValidationGroup="SystemBackportBlob" />
        <ajaxToolkit:ValidatorCalloutExtender ID="dateValidatorCalloutExtender" runat="server" BehaviorID="dateValidatorCalloutExtender" TargetControlID="dateCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
    </div>
<!--    <div class="divForm">
        <label class="label">*Select Year:</label>
        <asp:DropDownList ID="ddlYears" runat="server" CssClass="dropdown" AppendDataBoundItems="true">                           
        </asp:DropDownList>
    </div>-->
    <div class="divForm">
        <label class="label">Select Organzation:</label>
        <asp:DropDownList ID="ddlOrganizations" runat="server" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlOrganizations_OnSelectedIndexChanged">                            
        </asp:DropDownList>
    </div>
    <div class="divForm">
        <label class="label">Select Client:</label>  
        <asp:DropDownList ID="ddlClients" runat="server" CssClass="dropdown" AppendDataBoundItems="true">                   
        </asp:DropDownList>   
    </div>
    <CW:AccountSelector id="acctSelector" runat="server" />
    <div class="divForm">
        <label class="label">Non Org/Client Query</label>  
        <asp:Checkbox ID="chbNonOrgClient" runat="server" />
    </div>
    <asp:LinkButton ID="btnSelect" ValidationGroup="SystemBackportBlob" Text="Select" runat="server" CssClass="lnkButton" OnClick="btnSelect_Click" />
</div>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>   

<div id="gridTbl">
    <asp:GridView runat="server" ID="grid" EnableViewState="true" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
        <Columns>
            <asp:CommandField ShowSelectButton="true" />
                                 
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BPID" HeaderText="BPID"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.BPID)) %>
                </ItemTemplate> 
            </asp:TemplateField> 
                                                                           
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DateCreatedUTC" HeaderText="Date Created"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.DateCreatedUTC)) %>
                </ItemTemplate> 
            </asp:TemplateField>
                                   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FromEnvironment" HeaderText="To Env"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.FromEnvironment)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FromAccountLevel" HeaderText="From Level"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.FromAccountLevel)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ToEnvironment" HeaderText="From Env"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.ToEnvironment)) %>
                </ItemTemplate> 
            </asp:TemplateField>                                                                            

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ToAccountLevel" HeaderText="To Level"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.ToAccountLevel)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Status" HeaderText="Status"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.Status)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContainerNamePrefixes" HeaderText="Container Prefixes"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.ContainerNamePrefixes)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="OrganizationName" HeaderText="Organization Name"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.OrganizationName)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="ClientName"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<BlobBackport>(_=>_.ClientName)) %>
                </ItemTemplate> 
            </asp:TemplateField>

            <asp:TemplateField Visible="true">
                <ItemTemplate>                                                
                    <asp:LinkButton ID="lbnDeleteLinkButton" Runat="server" CausesValidation="false"
						OnClientClick="return confirm('Are you sure you wish to delete this backport job permanently?');"
                        CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="true">
                <ItemTemplate>                                                
                    <asp:LinkButton ID="lbnDownloadLinkButton" Runat="server" CausesValidation="false"
                        CommandName="Download">Download</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="true">
                <ItemTemplate>                                                
                    <asp:LinkButton ID="lbnRescheduleLinkButton" Runat="server" CausesValidation="false"
                        CommandName="Reschedule">Reschedule</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                   
        </Columns>

    </asp:GridView>
</div>

<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
    <Fields>
        <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
            <ItemTemplate>
                <div>
                    <h2>Blob Backport Job Details</h2>                                                                                                                                            
                    <ul class="detailsList">
                        <li>
                            <strong>BPID: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.BPID))) %>
                        </li>
                        <li>
                            <strong>Organization Name: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.OrganizationName))) %>
                        </li>
                        <li>
                            <strong>Client Name: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.ClientName))) %>
                        </li>
                        <li>
                            <strong>Container Prefixes: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.ContainerNamePrefixes))) %>
                        </li>
                        <li>
                            <strong>Job Begin (UTC): </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.JobBeginUTC))) %>
                        </li>
                        <li>
                            <strong>Job Finish (UTC): </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.JobFinishUTC))) %>
                        </li>
                        <li>
                            <strong>Date Created (UTC): </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.DateCreatedUTC))) %>
                        </li>
                        <li>
                            <strong>Bytes Copied: </strong>
                            <%# Eval(PropHelper.G<BlobBackport>(b=>b.BytesCopied)) %>
                        </li>
                        <li>
                            <strong>Existing Records: </strong>
                            <%# Eval(PropHelper.G<BlobBackport>(b=>b.TotalBytes)) %>
                        </li>
                        <li>
                            <strong>Retention Amount: </strong>
                            <%# Eval(PropHelper.G<BlobBackport>(b=>b.RetentionAmount)) %>
                        </li>
                        <li>
                            <strong>Items Copied: </strong>
                            <%# Eval(PropHelper.G<BlobBackport>(b=>b.ItemsCopied)) %>
                        </li>
                        <li>
                            <strong>From Account Environment: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.FromEnvironment))) %>
                        </li>
                        <li>
                            <strong>From Account Level: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.FromAccountLevel))) %>
                        </li>
                        <li>
                            <strong>To Account Environment: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.ToEnvironment))) %>
                        </li>
                        <li>
                            <strong>To Account Level: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.ToAccountLevel))) %>
                        </li>
                        <li>
                            <strong>Status: </strong>
                            <%# DefaultIfNullOrWhiteSpace(Eval(PropHelper.G<BlobBackport>(b=>b.Status))) %>
                        </li>
                    </ul>
                </div>
            </ItemTemplate>                                             
        </asp:TemplateField>
    </Fields>
</asp:DetailsView>

<!--EDIT PANEL -->                              
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">                                                                                             
    <div>
        <h2>Edit Backport</h2>
    </div>  
    <div>
    </div>
</asp:Panel>
