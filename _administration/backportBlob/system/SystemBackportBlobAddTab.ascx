﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemBackportBlobAddTab.ascx.cs" Inherits="CW.Website._administration.backportBlob.system.SystemBackportBlobAddTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="~/_administration/backportBlob/BackportBlobForm.ascx" tagPrefix="CW" tagName="BackportBlobForm" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Schedule Blob Backport Job" />
<CW:BackportBlobForm runat="server" ID="backportBlobForm" FormMode="Insert" UpdateButtonCaption="Schedule" />