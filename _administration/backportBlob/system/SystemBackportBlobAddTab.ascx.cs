﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using CW.Data.AzureStorage.Models;

namespace CW.Website._administration.backportBlob.system
{
    public partial class SystemBackportBlobAddTab : AddTabBase<BlobBackport, SystemBackportBlobAdministration>
    {
        private void Page_Init()
        {
            CanEntityBeAdded = backportBlobForm.CanEntityBePersisted;
            AddEntityMethod = backportBlobForm.PersistEntityAction;
        }
    }
}