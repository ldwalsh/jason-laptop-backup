﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" AutoEventWireup="false" CodeBehind="SystemBackportBlobAdministration.aspx.cs" Inherits="CW.Website._administration.backportBlob.system.SystemBackportBlobAdministration" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="SystemBackportBlobViewTab.ascx" tagPrefix="CW" tagName="SystemBackportBlobViewTab" %>
<%@ Register src="SystemBackportBlobAddTab.ascx" tagPrefix="CW" tagName="SystemBackportBlobAddTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   

            <CW:PageHeader runat="server" Title="Blob Backport Administration"  Description="The blob backport area is used to schedule blob backport jobs." />
            <div class="administrationControls">
                <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage"> 
                    <Tabs>
                        <telerik:RadTab Text="View Blob Backport Jobs"></telerik:RadTab>
                        <telerik:RadTab Text="Schedule Blob Backport Job"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                        <CW:SystemBackportBlobViewTab runat="server" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView2" runat="server">
                        <CW:SystemBackportBlobAddTab runat="server" />
                    </telerik:RadPageView>                
                </telerik:RadMultiPage>
            </div>

</asp:Content>