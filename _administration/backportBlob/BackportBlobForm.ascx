﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BackportBlobForm.ascx.cs" Inherits="CW.Website._administration.backportBlob.BackportBlobForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="CW" TagName="AccountSelector" src="~/_administration/backport/StorageAccountSelector.ascx" %>

<div class="divForm">  
    <label class="label">Select Organzation:</label>
    <asp:DropDownList ID="ddlOrganizations" runat="server" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlOrganizations_OnSelectedIndexChanged" />                            
</div>
<div class="divForm">  
    <label class="label">Select Client:</label>  
    <asp:DropDownList ID="ddlClients" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />                   
</div>
<CW:AccountSelector id="acctSelector" IsRequired="true" runat="server" />
<div class="divForm">   
    <label class="label">*Container Name Prefixes:</label>
    <asp:TextBox ID="txtContainerNamePrefixes" runat="server" />    
</div>
<div class="divForm">   
    <asp:RadioButtonList ID="rblTransferType" runat="server" CssClass="radio">
        <asp:ListItem Text="Backup" Value="Backup" Selected="True" />
        <asp:ListItem Text="Backport" Value="Backport" />
    </asp:RadioButtonList>
</div>
<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<div style="min-height:120px;"></div>