﻿KGS_.Namespace("_administration.equipment");

_administration.equipment.EquipmentForm = function()
{
    //var t = new Date();
    //setInterval(function(){alert(t + "\n" + window.$this);}, 5000);
    
    this.findControl = function(id) //move to base class
    {
        ///<returns type="Object" />

        id = ("_" + id);
        
        var controls = jQuery("[id^='" + this.ClientID + "_']").get();
        
        for (var i=0; i<controls.length; i++)
        {
            var c = controls[i];
                
            if (String$endsWith(c.id, id)) return c;
        }

        return null;
    };

    var String$endsWith = function(string, endsWith)
    {
        ///<param name="string" type="String" />
        ///<param name="endsWith" type="String" />
        ///<returns type="Boolean" />
        
        return (string.slice(-endsWith.length) == endsWith);

    };
};