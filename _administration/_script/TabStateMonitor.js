﻿
window.OnClientActiveTabChanged = function(sender, args)
{
    var activeTab = args.get_tab()

    //
    //clears user message when selecting tab  -> this does not belong here will move out in future
    
    var lblMessage = jQuery(activeTab.get_element()).find("span[id$='lblMessage']")[0];
    
    if (lblMessage != null)
    {
        lblMessage.innerHTML = "";
    }
    
    //
    //creates postback on tab selection if a refresh is required
    
    var tabsArray = OnClientActiveTabChanged.refreshTabsArray;
	var index	  = activeTab.get_index()

    if (!tabsArray || !tabsArray[index]) return;

    tabsArray[index] = false;

	if (OnClientActiveTabChanged.stateChangeMessages)
	{
		var name = "__EVENTDATA_STATECHANGEMESSAGES";

		var hidden =  document.getElementsByName(name)[0]

		if (!hidden)
		{
			hidden = document.createElement("input")

			hidden.type  = "hidden";
			hidden.name	 = name;
			
			document.getElementsByTagName("form")[0].appendChild(hidden)
		}

		hidden.value = JSON.stringify(OnClientActiveTabChanged.stateChangeMessages)
	}

	__doPostBack(args._tab._parent._element.id.replace(/_/g, "$"), JSON.stringify({type:0,index:index}))
};
