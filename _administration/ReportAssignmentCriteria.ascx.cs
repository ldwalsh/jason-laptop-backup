﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Website._framework;
using CW.Data;
using System.Text;
using System.Collections.Specialized;
using CW.Business;
using CW.Utility;
using CW.Data.AzureStorage.Models.Queue;
using CW.Common.Constants;
using CW.Data.AzureStorage;
using CW.Data.Helpers;
using CW.Data.AzureStorage.Models;
using CW.Business.Messaging.Schedule;
using CW.Business.Query;
using System.Linq.Expressions;
using CW.Data.Models.Building;

namespace CW.Website._administration
{
    public partial class ReportAssignmentCriteria : SiteUserControl
    {
        public enum ReportTypes
        {
            Building = 0, 
            Client = 1, 
        }

        public enum UserGroupAccessLevels
        {
            Client = 0,  //client org usergroups only
            Provider = 1, //provider org usergroups only
            ProviderClient = 2,  //provider org and client org usergroups
            KGS = 3 //kgs, all providers orgs for that client, and client org usergroups
        }

        #region fields

            private List<UserGroup> mUserGroups;
        
        #endregion

        #region Properties

            public int CurrentUID
            {
                get
                {
                    int tempInt = 0;
                    Int32.TryParse(hdnCurrentUID.Value, out tempInt);
                    return tempInt;
                }
                set
                {
                    hdnCurrentUID.Value = value.ToString();
                }
            }

            public DataConstants.AnalysisRange AnalysisRange
            {
                get
                {
                    DataConstants.AnalysisRange tempRange = DataConstants.AnalysisRange.Daily;
                    Enum.TryParse<DataConstants.AnalysisRange>(hdnAnalysisRange.Value, out tempRange);
                    return tempRange;
                }
                set
                {
                    hdnAnalysisRange.Value = Enum.GetName(typeof(DataConstants.AnalysisRange), value);
                }
            }

            private int ReportTypeID
            {
                get
                {
                    int tempInt = 0;
                    Int32.TryParse(hdnReportTypeID.Value, out tempInt);
                    return tempInt;
                }
                set
                {
                    hdnReportTypeID.Value = value.ToString();
                }
            }
            public ReportTypes ReportType
            {
                get
                {
                    return EnumHelper.Parse<ReportTypes>(ReportTypeID.ToString());
                }
                set
                {
                    ReportTypeID = (int)value;
                }
            }

            private int UserGroupAccessLevelID
            {
                get
                {
                    int tempInt = 0;
                    Int32.TryParse(hdnUserGroupAccessLevelID.Value, out tempInt);
                    return tempInt;
                }
                set
                {
                    hdnUserGroupAccessLevelID.Value = value.ToString();
                }
            }
            public UserGroupAccessLevels UserGroupAccessLevel
            {
                get
                {
                    return EnumHelper.Parse<UserGroupAccessLevels>(UserGroupAccessLevelID.ToString());
                }
                set
                {
                    UserGroupAccessLevelID = (int)value;
                }
            }

            public String Title
            {
                set { htmlblReport.InnerHtml = value; }
            }

            public Int32 ProviderID
            {
                set
                {
                    hdnProviderID.Value = value.ToString();
                }
                get
                {
                    int tempInt = 0;
                    Int32.TryParse(hdnProviderID.Value, out tempInt);
                    return tempInt;
                }
            }
            public Int32 CID
            {
                set 
                {
                    hdnCID.Value = value.ToString();
                }
                get
                {
                    int tempInt = 0;
                    Int32.TryParse(hdnCID.Value, out tempInt);
                    return tempInt;
                }
            }
            public Int32 BID
            {
                set
                {
                    hdnBID.Value = value.ToString();
                }
                get
                {
                    int tempInt = 0;
                    Int32.TryParse(hdnBID.Value, out tempInt);
                    return tempInt;
                }
            }

            public Guid GUID
            {
                set
                {                   
                    hdnGUID.Value = value.ToString();
                }
                get
                {                    
                    Guid tempGuid = new Guid();
                    Guid.TryParse(hdnGUID.Value, out tempGuid);
                    return tempGuid;
                }
            }

            public List<int> UserGroups 
            {
                set
                {
                    hdnUserGroupIDs.Value = string.Join(",", value);
                }
                get
                {                       
                    return !String.IsNullOrEmpty(hdnUserGroupIDs.Value) ? hdnUserGroupIDs.Value.Split(',').Select(n => Convert.ToInt32(n)).ToList() : new List<int>();
                }
            }

            public bool IsActive
            {
                set
                {
                    chkIsActive.Checked = value;
                }
                get
                {
                    return chkIsActive.Checked;
                }
            }

        #endregion

        #region Method

            public void ClearForm()
            {
                divReportAssignment.Visible = false;
                divSendTestReport.Visible = false;
                divSendUserGroupReport.Visible = false;
                lbUserGroupsTop.Items.Clear();
                lbUserGroupsBottom.Items.Clear();
                chkIsActive.Checked = false;
            }

            public void BindForm()
            {
                IEnumerable<UserGroup> fullUserGroupSet = Enumerable.Empty<UserGroup>();

                divReportAssignment.Visible = chkIsActive.Checked;
                divSendTestReport.Visible = chkIsActive.Checked;
                divSendUserGroupReport.Visible = chkIsActive.Checked;

                int clientOID;
                int providerOID;

                if (UserGroupAccessLevel == UserGroupAccessLevels.KGS)
                {
                    if (CID == 0) throw new ArgumentException("CID property to be set on: " + this.GetType().Name);

                    //TODO get for all provider clients

                    clientOID = DataMgr.OrganizationDataMapper.GetOIDByCID(CID);

                    IEnumerable<ProvidersClients> providerClients = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByCID(CID);

                    //get all user groups for that client, all providers for that client, and for kgs.
                    fullUserGroupSet = DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(clientOID, null)
                        .Union(DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(BusinessConstants.Organization.KGSBuildingsOID, null));

                    foreach (ProvidersClients pc in providerClients)
                    {
                        fullUserGroupSet = fullUserGroupSet.Union(DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(DataMgr.OrganizationDataMapper.GetOrganizationByProviderID(pc.ProviderID).OID, null));
                    }
                }
                else if (UserGroupAccessLevel == UserGroupAccessLevels.ProviderClient)
                {
                    if (ProviderID == 0) throw new ArgumentException("ProviderID property to be set on: " + this.GetType().Name); 
                    if (CID == 0) throw new ArgumentException("CID property to be set on: " + this.GetType().Name);

                    clientOID = DataMgr.OrganizationDataMapper.GetOIDByCID(CID);
                    providerOID = DataMgr.OrganizationDataMapper.GetOrganizationByProviderID(ProviderID).OID;

                    //get all user groups for that client, all providers for that client, and for kgs.
                    fullUserGroupSet = DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(clientOID, null)
                        .Union(DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(providerOID, null));                               
                }
                else if (UserGroupAccessLevel == UserGroupAccessLevels.Provider)
                {
                    if (ProviderID == 0) throw new ArgumentException("ProviderID property to be set on: " + this.GetType().Name);

                    providerOID = DataMgr.OrganizationDataMapper.GetOrganizationByProviderID(ProviderID).OID;

                    fullUserGroupSet = DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(providerOID, null);
                }
                else if (UserGroupAccessLevel == UserGroupAccessLevels.Client)
                {
                    if (CID == 0) throw new ArgumentException("CID property to be set on: " + this.GetType().Name);

                    clientOID = DataMgr.OrganizationDataMapper.GetOIDByCID(CID);

                    fullUserGroupSet = DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(clientOID, null);
                }
                else
                {
                    throw new ArgumentException("Level property needs to be set on: " + this.GetType().Name);
                }

                //set as distinct
                fullUserGroupSet = fullUserGroupSet.Distinct(new UserGroupDataMapper.UserGroupComparer()).ToList();

                if (chkIsActive.Checked)
                {
                    BindUserGroups(lbUserGroupsTop, fullUserGroupSet.Where(ug => !UserGroups.Contains(ug.UserGroupID)));
                    BindUserGroups(lbUserGroupsBottom, fullUserGroupSet.Where(ug => UserGroups.Contains(ug.UserGroupID)));
                }
                else
                {
                    //resets if unchecked
                    BindUserGroups(lbUserGroupsTop, fullUserGroupSet.Where(ug => UserGroups.Contains(ug.UserGroupID)));
                    BindUserGroups(lbUserGroupsBottom, null);
                    hdnUserGroupIDs.Value = String.Empty;
                }
            }

            private void BindUserGroups(ListBox lb, IEnumerable<UserGroup> dataSource)
            {
                lb.Items.Clear();
                lb.DataTextField = "UserGroupName";
                lb.DataValueField = "UserGroupID";
                lb.DataSource = dataSource;
                lb.DataBind();
            }

        #endregion

        #region Button Events

            private int ScheduleClientReport(IEnumerable<int> uids)
            {
                var bsq = new BuildingSettingQuery(DataMgr.ConnectionString, siteUser);

                if (CID == 0)
                    throw new ArgumentException("CID property to be set on: " + this.GetType().Name);

                ClientSetting cs = DataMgr.ClientDataMapper.GetClientSettingsByClientID(CID, _ => _.Client);
                var buildingSettings = bsq.LoadWith(bs=>bs.Building).FinalizeLoadWith.ByClient(cs.CID).ToList();
				var maxOffset = buildingSettings.Select(bs => new BuildingOffset(bs, DateTime.UtcNow)).OrderBy(bo => bo.TotalOffset.TotalHours).Take(1).ToList();
                
				var sdnt = new ScheduleTaskDiagNotification(DataMgr);
                var inputs = new ScheduleInputsDiagNotification(new BuildingOffsetCollection(maxOffset, true), ScheduleInputs.ScheduleType.Client, new[]{cs}, null, true, new[] { AnalysisRange }, uids);
                var auditExecutor = new AuditExecutorFunc<ScheduleInputsDiagNotification, IEnumerable<PriorityNotificationMessage>>(new ScheduleExecutorDiagNotification(DataMgr), LogMgr);
                return auditExecutor.Execute(inputs).Count();
            }

            private int ScheduleBuildingReport(IEnumerable<int> uids)
            {
                var bsq = new BuildingSettingQuery(DataMgr.ConnectionString, siteUser);

                if (BID == 0)
                    throw new ArgumentException("BID property to be set on: " + this.GetType().Name);

                var buildingSetting = bsq.LoadWith<Building, Client>(_=>_.Building, _ => _.Client, _=>_.ClientSettings).FinalizeLoadWith.GetById(BID);
                //var rddl = new RawDataDelayLookup(buildingSetting.Building.CID, buildingSetting.BID, buildingSetting.RawDataDelay);
                var sdnt = new ScheduleTaskDiagNotification(DataMgr);
                var lookup = new BuildingOffset(buildingSetting, DateTime.UtcNow);

                var inputs = new ScheduleInputsDiagNotification(new BuildingOffsetCollection(lookup, true), ScheduleInputs.ScheduleType.Building, null, new[]{buildingSetting}, true, new[] { AnalysisRange }, uids);
                var auditExecutor = new AuditExecutorFunc<ScheduleInputsDiagNotification, IEnumerable<PriorityNotificationMessage>>(new ScheduleExecutorDiagNotification(DataMgr), LogMgr);
                return auditExecutor.Execute(inputs).Count();
            }

            private string StringJoinExt(IEnumerable<string> source, string delimiter, string endDelimiter)
            {
                if (!source.Any())
                    return "";

                if (source.Count() == 1)
                    return source.First();

                return String.Join(delimiter, source.ToArray(), 0, source.Count() - 1) + endDelimiter + source.Last();
            }


            protected void btnSendLastReportToUserGroups_Click(object sender, EventArgs e)
            {
                if (lbUserGroupsBottom.Items.Count == 0)
                {
                    lblSendUserGroupMessage.Text = "Please make a User Group selection";
                    return;
                }

                int reportsScheduled;
                var userGroupItems = lbUserGroupsBottom.Items.Cast<ListItem>().Select(li => li.Text);
                string userGroupsString = StringJoinExt(userGroupItems, ", ", "and ");

                IEnumerable<int> uids = DataMgr.UserGroupDataMapper.GetAllUsersInUserGroupsByGUID(new Guid(hdnGUID.Value)).Select(u => u.UID);

                var bsq = new BuildingSettingQuery(DataMgr.ConnectionString, siteUser);

                if (ReportType == ReportTypes.Client)
                    reportsScheduled = ScheduleClientReport(uids);
                else if (ReportType == ReportTypes.Building)
                    reportsScheduled = ScheduleBuildingReport(uids);
                else
                    throw new ArgumentException("Level property needs to be set on: " + this.GetType().Name);

                lblSendUserGroupMessage.Text = String.Format("Emailed {0} report(s) to {1}", reportsScheduled, userGroupsString);
            }

            protected void btnSendTestReport_Click(object sender, EventArgs e)
            {
                int reportsScheduled;
                IEnumerable<int> uids = new int[] { CurrentUID };

                var bsq = new BuildingSettingQuery(DataMgr.ConnectionString, siteUser);

                if (ReportType == ReportTypes.Client)
                    reportsScheduled = ScheduleClientReport(uids);
                else if (ReportType == ReportTypes.Building)
                    reportsScheduled = ScheduleBuildingReport(uids);
                else
                    throw new ArgumentException("Level property needs to be set on: " + this.GetType().Name);

                lblSendTestMessage.Text = String.Format("Emailed {0} report(s) to you", reportsScheduled);
            }


            /// <summary>
            /// on button up click, remove user groups from bottom listbox
            /// </summary>
            protected void btnUserGroupsUpButton_Click(object sender, EventArgs e)
            {
                while (lbUserGroupsBottom.SelectedIndex != -1)
                {
                    if (!String.IsNullOrEmpty(hdnUserGroupIDs.Value))
                    {
                        List<int> tempList = hdnUserGroupIDs.Value.Split(',').Select(n => Convert.ToInt32(n)).ToList();
                        tempList.Remove(Convert.ToInt32(lbUserGroupsBottom.SelectedItem.Value));
                        hdnUserGroupIDs.Value = string.Join(",", tempList);                        
                    }

                    lbUserGroupsTop.Items.Add(lbUserGroupsBottom.SelectedItem);                    
                    lbUserGroupsBottom.Items.Remove(lbUserGroupsBottom.SelectedItem);
                }                
            }

            /// <summary>
            /// on button down click, add user groups to bottom listbox
            /// </summary>
            protected void btnUserGroupsDownButton_Click(object sender, EventArgs e)
            {
                while (lbUserGroupsTop.SelectedIndex != -1)
                {
                    if (!String.IsNullOrEmpty(hdnUserGroupIDs.Value))
                    {
                        List<int> tempList = hdnUserGroupIDs.Value.Split(',').Select(n => Convert.ToInt32(n)).ToList();
                        tempList.Add(Convert.ToInt32(lbUserGroupsTop.SelectedItem.Value));
                        hdnUserGroupIDs.Value = string.Join(",", tempList);
                    }
                    else
                    {
                        hdnUserGroupIDs.Value = lbUserGroupsTop.SelectedItem.Value;
                    }

                    lbUserGroupsBottom.Items.Add(lbUserGroupsTop.SelectedItem);
                    lbUserGroupsTop.Items.Remove(lbUserGroupsTop.SelectedItem);
                }
            }

        #endregion

        #region Checkbox Events

            protected void chkIsActive_OnCheckedChanged(object sender, EventArgs e)
            {
                BindForm();
            }

        #endregion

    }
}