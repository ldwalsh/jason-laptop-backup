﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CW.Website._framework;

namespace CW.Website._administration
{
    [
    ParseChildren(true)
    ]
    public partial class TabHeader: SiteUserControl
    {
        #region STATIC

            #region method

                public static void InitTemplate(ITemplate template, Control target)
                {
                    if (template == null) return;

                    var container = new Control();

                    template.InstantiateIn(container);
                
                    target.Controls.Add(container);
                }

            #endregion
 
        #endregion
        
        #region property

            public String Title
            {
                set {headerTitle.InnerText = value;}
            }

            public String SubTitle
            {
                set
                {
                    SubTitlePlaceHolder.Controls.Clear();

                    SubTitlePlaceHolder.Controls.Add(new HtmlGenericControl("span"){InnerHtml = value});
                }
            }

            public String Message
            {
                set {SetMessage(value);}
            }

            [
            TemplateContainer(typeof(TemplateControl)),
            PersistenceMode(PersistenceMode.InnerProperty),
            TemplateInstance(TemplateInstance.Single)
            ]
            public ITemplate SubTitleTemplate
            {
                get;
                set;
            }

            [
            TemplateContainer(typeof(TemplateControl)),
            PersistenceMode(PersistenceMode.InnerProperty),
            TemplateInstance(TemplateInstance.Single)
            ]
            public ITemplate RightAreaTemplate
            {
                get;
                set;
            }

        #endregion

        #region method
            
            private void Page_Init()
            {
                InitTemplate(SubTitleTemplate, SubTitlePlaceHolder);
                InitTemplate(RightAreaTemplate, RightAreaPlaceHolder);
            }

            public void SetMessage(String text, Boolean isRed=true)
            {
                lblMessage.Text     = text;
                lblMessage.Visible  = !String.IsNullOrEmpty(text);
                lblMessage.CssClass = isRed ? "errorMessage" : "successMessage";

                lnkSetFocusMessage.Focus();
            }

        #endregion
    }
}