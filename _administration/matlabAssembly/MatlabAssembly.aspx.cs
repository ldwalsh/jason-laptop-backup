﻿using CW.Data;
using CW.Utility;
using System;

namespace CW.Website._administration.matlabAssembly
{
	public partial class MatlabAssemblyPage: AdministrationPageBaseBase
	{
		public override String IdentifierField
		{
			get {return PropHelper.G<MatlabAssembly>(_=>_.MAID);}
		}

		public override String NameField
		{
			get {return PropHelper.G<MatlabAssembly>(_=>_.FileName);}
		}

		public override String EntityTypeName
		{
			get {return "MatlabAssembly";}
		}
	}
}
