﻿using CW.Data;
using CW.Utility;
using iCL;
using iCLib;
using JSAsset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._administration.matlabAssembly
{
	public partial class MatlabViewTab: ViewTabWithFormBase<MatlabAssembly,MatlabAssemblyPage>, IPostBackEventHandler
	{
		#region method

			#region override

				protected override void PopulateEditForm(MatlabAssembly obj)
				{
					throw new NotImplementedException();
				}

			#endregion

			private void Page_Init()
			{
				((UpdatePanel)Page.Master.FindControl("updatePanelMain")).Triggers.Add(new AsyncPostBackTrigger{ControlID= this.UniqueID,});

				ScriptIncluder.GetInstance(Page) //will not be necessary in future... required now as a manual means to resolve dependencies
					.Add<core.DELEGATE>()
					.Add<core.FunctionPrototype>()
					.Add<core.HTMLLinkElement>()
                    .Add<core.DateRange>()
					.Add<core.ui.windows.MessageBox>()
					.Add<core.asp.webForm.AsyncPostbackManager>()
					.Add<core.ui.Globalizer>()
					.Add<core.ui.windows.DialogWindow>()
					.Add<core.ui.windows.WaitBox>()
					.Add<core.ui.controls.FileUploader>()
					.Add<core.asp.webForm>()
					.Add<core.HTMLInputElement>()
					.Add<core.CSSStyleDeclaration>();

				JSAssetManager.GetInstance(page).Add<MatlabAssemblyViewTabAsset>();
				
				PanelEditEntity = new Panel();

				//

				//CanEntityBeDeleted = CanAssemblyBeDeleted;
                //DeleteEntityMethod = (id) => {DataMgr.MatlabAssemblyDataMapper.DeleteMatlabAssembly(Int32.Parse(id)); };
        }

   //     private Boolean CanAssemblyBeDeleted(String maid, out String message)
			//{
			//	if (DataMgr.AnalysisDataMapper.GetAllAnalysisByMAID(Convert.ToInt32(maid)).Any())
			//	{
			//		message = "Cannot delete assembly because it has one or more analysis associated to it.";

			//		return false;
			//	}
					
			//	message = null;

			//	return true;
			//}

		#endregion

		#region property

			protected override IEnumerable<String> GridColumnNames
			{
				get {return PropHelper.F<MatlabAssembly>(_=>_.MAID, _=>_.FileName, _=>_.FileSize, _=>_.DateCreated, _=>_.DateModified);}
			}

            protected override String EntityTypeName
            {
                get { return "matlab assembly"; }
            }
			
			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get {return (searchText => DataMgr.MatlabAssemblyDataMapper.GetAllSearchedAssemblies(searchText.FirstOrDefault()));}
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get {return (id => DataMgr.MatlabAssemblyDataMapper.GetAssemblyById(Int32.Parse(id)));}
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get {return (id => DataMgr.MatlabAssemblyDataMapper.GetAssemblyById(Int32.Parse(id)));}
			}

			protected override String CacheSubKey
			{
				get {return null;}
			}

		#endregion

		#region IPostBackEventHandler

			void IPostBackEventHandler.RaisePostBackEvent(String eventArgument) //revise this approach and access the TabStateMonitor instance directly client-side
			{
				switch (eventArgument)
				{
					case "TabStateMonitor.ChangeState":
					{
						this.page.TabStateMonitor.ChangeState();

						BindGrid(true);

						break;
					}
				}
			}

		#endregion
	}
}