﻿using JSAsset;
using System;

namespace CW.Website._administration.matlabAssembly
{
	public class MatlabAssemblyViewTabAsset: EmbeddedAssetBase
	{
		public override String RelativePath
		{
			get {return "_administration/matlabAssembly/script/";}
		}

		public override Boolean ProbeFolder
		{
			get {return false;}
		}

		public override String[] Dependencies
		{
			get {return new[]{"iCL"};}
		}

		public override String InitFunction
		{
			get {return "initMatlabAssemblyViewTab";} //remove when loadScript can handle code versus just calling callback function
		}
	}
}