﻿using CW.Business;
using CW.Website.DependencyResolution;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Services;

namespace CW.Website._administration.matlabAssembly
{
	public class MatlabAssemblyUploadService: WebService
	{
        private readonly DataManagerCommonStorage mDataManagerCommon;

        public MatlabAssemblyUploadService()
        {
            mDataManagerCommon = IoC.Resolve<DataManagerCommonStorage>();
        }

		[
		WebMethod
		]
		public void Save()
		{
			var req = HttpContext.Current.Request;
			var res = HttpContext.Current.Response;

			if (req.Files.Count != 1) throw new Exception();

			Byte mode;

			if (!Byte.TryParse(req.QueryString["m"], out mode)) throw new Exception();

			var maid		= Int32.Parse(req.QueryString["maid"] ?? "0");
			var postedFile	= req.Files[0];
            var name        = new FileInfo(postedFile.FileName).Name; //IE passes in the full file path (depends on user setting)
            var matlabDM    = mDataManagerCommon.MatlabAssemblyDataMapper;

			if (matlabDM.DoesAssemblyNameExist(name, ((mode == 1) ? new Nullable<Int32>(maid) : null)))
			{
				res.Write("{\"error\":\"FileExistsError\"}"); //revise

				return;
			}
			
			//

			Byte[] file;

			using (var reader = new BinaryReader(postedFile.InputStream))
			{
				file = reader.ReadBytes(postedFile.ContentLength);
			}

			//

			var currentDomain = AppDomain.CurrentDomain;

			var appDomain = AppDomain.CreateDomain(("TempAppDomainForMatlabAssemblyInspection" + new DateTime().Ticks), null, currentDomain.BaseDirectory, currentDomain.RelativeSearchPath, false);

			var inspector =
				appDomain.CreateInstanceFromAndUnwrap
                (
                    Assembly.GetExecutingAssembly().Location, typeof(MatlabAssemblyInspector).FullName, false, BindingFlags.CreateInstance, null, new Object[]{file}, null, null
                
                ) as MatlabAssemblyInspector;

            var analysis = mDataManagerCommon.AnalysisDataMapper.GetAllActiveAnalysisByMAID(maid);
			
			var missingBaseFile = inspector.HasBaseFiles(analysis.Select(_=>_.AnalysisBaseFileName).Distinct().ToList());
			var missingFunction = inspector.HasRequiredFunctions(analysis.Select(_=>_.AnalysisFunctionName).ToList());

			AppDomain.Unload(appDomain);

			if (missingBaseFile != null)
			{
				res.Write("{\"error\":\"MissingBaseFile\", \"missingBaseFile\":\"" + missingBaseFile + "\"}"); //revise

				return;
			}

			if (missingFunction != null)
			{
				res.Write("{\"error\":\"MissingFunction\", \"missingFunction\":\"" + missingFunction + "\"}"); //revise

				return;
			}

			//

			if (mode == 0)
			{
				matlabDM.InsertMatlabAssembly(name, file);
			}
			else
			{
				matlabDM.UpdateMatlabAssembly(maid, name, file);
			}

			res.Write((true).ToString());
		}
	}
}
