﻿///
///<reference path="~/AnalysisBuilder/script/$iCL.js" />

window.initMatlabAssemblyViewTab = function(){MatlabAssemblyViewTab.NEW()} //revise

$.class
(
	{
		namespace: window,
	},

	function MatlabAssemblyViewTab()
	{
		this._window = MatlabAssemblyViewTab.EditAssemblyWindow.VAR;
                
		this.Cstr =
			function()
			{
				this.init()
			}

		this.init =
			function()
			{
                if (!document.getElementById("lnkAddAssembly")) return rU(this.init.invokeAsync()) //revise!

				this._async_pageLoaded()
                
                iCL.asp.webForm.AsyncPostbackManager.NEW().pageLoaded.attach(this._async_pageLoaded)
				
				this._window = MatlabAssemblyViewTab.EditAssemblyWindow.NEW()
			}

		this._async_pageLoaded =
			function()
			{
                var lnkAdd = document.getElementById("lnkAddAssembly")

				if (lnkAdd.ready) return;

                lnkAdd.ready = true;
                
                lnkAdd.addEventListener("click", this._lnkAddAssembly_click)
				
				document.body.getElements(HTMLAnchorElement).search({"data-command":"edit"}).forEach
				(
					function(anchor) //replace with function caller versus explicit function declaration
					{
						anchor.event.attach.click(this._edit_click)
					
					}.bind(this)
				)
			}

		this._lnkAddAssembly_click =
			function()
			{
				if (!this._window.isOpened) this._window.open(true)

				this._window.show(MatlabAssemblyViewTab.EditAssemblyWindow.mode.add)
			}

		this._edit_click =
			function(dE)
			{
				///<param name="dE" type="$.DomEvent" />

				if (!this._window.isOpened) this._window.open(true)

				var anchor = dE.targetAsElmnt;

				this._window.show(MatlabAssemblyViewTab.EditAssemblyWindow.mode.edit, anchor.getData("maid"), anchor.parentNode.nextElementSibling.textContent/**/)
			}
	}
)





$.class
(
	{
		namespace:	MatlabAssemblyViewTab,
		prototype:	iCL.ui.windows.DialogWindow,
		statics:
		{
			mode:{add:0,edit:1,},
		}, 
	},
	
	function EditAssemblyWindow()
	{
		var $base = EditAssemblyWindow.prototype;

		var $MessageBox	  = iCL.ui.windows.MessageBox;
		var $FileUploader = iCL.ui.controls.FileUploader;

		this._windowMode	= Number.VAR;
		this._fileUploader	= $FileUploader.VAR;
		this._maid			= Number.VAR;
				
		this.Cstr =
			function()
			{
				this._hasTitlebar = false;

				//

				var div = document.getElementById("divEditMatlabAssembly")  //get from resource
				document.body.appendChild(div)

				//

				iCL.ui.Globalizer.NEW(div).apply({title:"Matlab Assembly", select:"Please select a Matlab Assembly file (.dll)", current:"Current Assembly:", save:"Save",})

				div.getElmntById("lnkSave").event.attach.click(this._lnkSave_click)

				//

				this._fileUploader = $FileUploader.NEW(div.getElmntById("divFileUploader")).SET({extension:"dll",})

				this._fileUploader.fileSelected = this._fileUploader_fileSelected;				
				this._fileUploader.posted		= this._fileUploader_posted;
								
				this._fileUploader.render()

				//

				//$base.Cstr.apply(this, [div, "Edit Assembly"])
				this._contentElement = div;
				this._title			 = "Edit Assembly";
			}

		this.show =
			function(windowMode, maid, name)
			{
				///<signature>
				/// <param name="windowMode" type="Number" />
				///</signature>
				///<signature>
				/// <param name="windowMode" type="Number" />
				/// <param name="maid" type="Number" />
				/// <param name="name" type="String" />
				///</signature>

				this._windowMode = windowMode;
				this._maid		 = maid;

				if (windowMode)
				{
					this._contentElement.getElmntById("spnCurrentName").textContent = name;
				}

				this._contentElement.getElmntById("divError").innerHTML = "&nbsp;";

				$base.show.apply(this)
			}

		this._show =
			function()
			{
				this.title_set((this._windowMode ? "Edit" : "Add") + " Assembly")

				$base._show.apply(this)

				this._contentElement.getElmntById("divCurrentAssembly").toggleVisibility(this._windowMode)
			}

		this._hide =
			function()
			{
				$base._hide.apply(this)

				this._contentElement.getElmntById("divCurrentAssembly").toggleVisibility(false)
			}

		this._showErrorMessage =
			function(message)
			{
				message = ifFalsy(message, "&nbsp;")

				this._windowDiv.getElmntById("divError").innerHTML = (isDll ? "" : "Please select a matlab assembly file.")
			}

		this._fileUploader_fileSelected =
			function(ea)
			{
				///<param name="ea" type="$.ui.controls.FileUploader.FileSelectedEventArgs" />

				var isDll = (ea.extension == "dll")

				if (!isDll) this._fileUploader.clear()

				this._windowDiv.getElmntById("divError").innerHTML = (isDll ? "" : "Please select a matlab assembly file.")
			}

		this._fileUploader_posted =
			function(ea)
			{
				///<param name="ea" type="$.ui.controls.FileUploader.PostedEventArgs" />

				//var tabs = $find("ctl00_plcCopy_tabContainer")
				//OnClientActiveTabChanged.refreshTabsArray = [true]
				//OnClientActiveTabChanged(tabs)
				//tabs.set_activeTab(tabs.get_tabs()[0])

				document.enable()

				if (ea.returned && ea.returned.error)
				{
					this._windowDiv.getElmntById("divError").innerHTML =
					{
						"FileExistsError": "The selected Matlab file must be unique.",
						"MissingBaseFile": ("The selected Matlab assembly does not define the base file: " + ea.returned.missingBaseFile),
						"MissingFunction": ("The selected Matlab assembly does not define the function: " + ea.returned.missingFunction),
						
					}[ea.returned.error];

					return;
				}

				iCL.asp.webForm.doPostback("ctl00_plcCopy_ViewTab", "TabStateMonitor.ChangeState") //revise?

				this.hide()
			}

		this._lnkSave_click =
			function()
			{
				if (!this._fileUploader.selectedFilePath) return rU(this._windowDiv.getElmntById("divError").innerHTML = "Please select a matlab assembly file.")

				var target = ("/_administration/matlabAssembly/MatlabAssemblyUploadService.asmx/Save?m=" + this._windowMode + (this._windowMode ? ("&maid=" + this._maid) : ""))

				this._fileUploader.postFile(target)

				//iCL.ui.windows.WaitBox.open("Please wait while the assembly uploads.")
				document.disable()
			}
	}
)


Object.define
(
	{
		namespace:	iCL.ui,
		statics:
		{
			defaultInterpreter:
			function(elmnt)
			{
				///<param name="elmnt" type="HTMLElement" />

				var textId = elmnt.getData("text")

				if (!textId)
				{
					textId = elmnt.getData("id")
				}

				if (!textId) return;

				return textId;
			},
		},
	},

	function Globalizer(div)
	{
		this._div = HTMLDivElement.VAR;

		this.apply =
			function(lookup, interpreter)
			{
				///<param name="lookup" type="Object" />

				interpreter = (interpreter || Globalizer.defaultInterpreter)

				this._div.getElements().FOR
				(
					function(elmnt)
					{
						var textId = interpreter(elmnt)

						if (!textId) return;

						var text = lookup[textId]

						if (!text) return;

						switch (elmnt.constructor)
						{
							case HTMLDivElement:
							case HTMLSpanElement:
							{
								elmnt.innerHTML = text;

								break;
							}
							case HTMLInputElement:
							{
								HTMLInputElement.CAST(elmnt).value = text;

								break;
							}
							default:
							{
								throw new Error(9876546513351160)
							}
						}

					}.bind(this)
				)
			}
	}
)
