﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CW.Website._administration.matlabAssembly
{
	public class MatlabAssemblyInspector: MarshalByRefObject
	{
		#region CONST

			public const String ANALYSIS_FUNCTION_NAME_SUFFIX = "_analysis";

		#endregion

		#region field

			private readonly IEnumerable<String> methods;

		#endregion

		#region constructor

			public MatlabAssemblyInspector(Byte[] assembly)
			{
				methods = Assembly.Load(assembly).GetTypes()[0].GetMethods().Select(_=>_.Name)
                    .Distinct().OrderBy(_=>_)
                    .ToArray(); //could use type name instead
			}

		#endregion

		#region method

			public String HasBaseFiles(IEnumerable<String> baseFileNames)
			{
				foreach (var fn in baseFileNames)
				{
					if (!this.methods.Contains(fn)) return fn;
				}

				return null;
			}

			public String HasRequiredFunctions(IEnumerable<String> functionNames)
			{
				foreach (var fn in functionNames)
				{
					if (!this.methods.Contains(fn + ANALYSIS_FUNCTION_NAME_SUFFIX)) return fn;
				}

				return null;
			}

		#endregion
	}
}