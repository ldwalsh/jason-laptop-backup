﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" AutoEventWireup="false" CodeBehind="MatlabAssembly.aspx.cs" Inherits="CW.Website._administration.matlabAssembly.MatlabAssemblyPage" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="MatlabAssemblyViewTab.ascx" tagPrefix="CW" tagName="MatlabAssemblyViewTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   

            <CW:PageHeader runat="server" Title="Matlab Assembly Administration" Description="The matlab assembly area is used to upload mfiles." />
            <div class="administrationControls">
                <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
                    <Tabs>
                        <telerik:RadTab Text="Matlab Assemblies"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                    <telerik:RadPageView runat="server">
                        <CW:MatlabAssemblyViewTab ID="ViewTab" runat="server" />
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </div>

</asp:Content>