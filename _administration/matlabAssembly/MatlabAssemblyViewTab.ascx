﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MatlabAssemblyViewTab.ascx.cs" Inherits="CW.Website._administration.matlabAssembly.MatlabViewTab" %>

<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Matlab Assemblies">
    <RightAreaTemplate>
		<div class="divDownloadBox2">
            <div class="divUpload">   
			<img src="/_assets/images/upload-icon.png" class="imgUpload" />
			<a id="lnkAddAssembly" href="javascript:" class="lnkUpload">Add Assembly</a>
            </div>
		</div>
    </RightAreaTemplate>
</CW:TabHeader>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>                                                                                                                                                     

<div id="gridTbl">
    <asp:GridView runat="server" ID="grid" EnableViewState="true" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true" >
        <Columns>
            <asp:CommandField ShowSelectButton="false" />
            <asp:TemplateField>
                <ItemTemplate>                                                
                    <a href="javascript:" data-command="edit" data-maid="<%# GetCellContent(Container, PropHelper.G<MatlabAssembly>(_=>_.MAID)) %>">Edit</a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FileName" HeaderText="File Name"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<MatlabAssembly>(_=>_.FileName)) %>
                </ItemTemplate> 
            </asp:TemplateField>                                                                                                   

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FileSize" HeaderText="File Size"> 
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<MatlabAssembly>(_=>_.FileSize)) %>
                </ItemTemplate> 
            </asp:TemplateField>                                                                                                   

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DateCreated" HeaderText="Date Created"> 
                <ItemTemplate>
                    <%# GetCellContentAsDateTime(Container, PropHelper.G<MatlabAssembly>(_=>_.DateCreated)) %>
                </ItemTemplate> 
            </asp:TemplateField>                                                                                                   

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DateModified" HeaderText="Date Modified"> 
                <ItemTemplate>
                    <%# GetCellContentAsDateTime(Container, PropHelper.G<MatlabAssembly>(_=>_.DateModified)) %>
                </ItemTemplate> 
            </asp:TemplateField>                                                                                                   

<%--            <asp:TemplateField>
                <ItemTemplate>                                                
                    <asp:LinkButton ID="lbnDeleteLinkButton" Runat="server" CommandName="Delete" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this assembly permanently?');">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>--%>
        </Columns>
    </asp:GridView>
</div>

<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" />

<div id="divEditMatlabAssembly" class="DialogWindowContent modalPanelLight">
	<a href="javascript:" class="modalLinkLight modalLinkLightCancel" data-window-hide="true">Cancel</a>
	<input type="button" class="modalButtonSmall" title="X" data-window-hide="true" />
	<div class="modalTitleLight" data-text="title"></div>
	<div class="modalContentLight" data-text="select"></div>
    <br />
	<div class="modalContentLight" data-id="divCurrentAssembly">
		<span data-text="current"></span>
		<span data-id="spnCurrentName"></span>
	</div>	
	<div data-id="divFileUploader" data-controlType="FileUploader"></div>
	<div data-id="divError" class="errorMessage">&nbsp;</div>

	<a href="javascript:" data-id="lnkSave" class="modalButton">Save</a>
</div>