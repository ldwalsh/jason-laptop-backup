﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GlobalFileForm.ascx.cs" Inherits="CW.Website._administration.globalFiles.GlobalFileForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="CW" Namespace="FileUploaderControl" Assembly="FileUploaderControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<script type="text/javascript">
     function ValidateUpload(sender, args) {
         var formMode = document.getElementById(sender.controltovalidate).parentNode.getAttribute('formMode');
         var input = document.querySelector('input[type="file"][id*="' + ((formMode == "Insert") ? 'Add' : 'View') + '"]');
         var fileUploaderId = sender.id;

         if (input != null) args.IsValid = (input.id == null);

         //if (!args.IsValid) ToggleValidatorToolTip(fileUploaderId, "Global File is a required field.", 'visible');
         if (!args.IsValid) ToggleValidatorToolTip(fileUploaderId, "Global File is a required field.", 'block');
     }

     function CheckExtensionAndNameLength(sender, args) {
         var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
         var fileName = args.get_fileName().substring(0, args.get_fileName().lastIndexOf('.'));
         var fileUploaderId = sender.get_element().id;
  
         if (args.get_fileName().lastIndexOf('.') != -1) {
             if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {

                 //ToggleValidatorToolTip(fileUploaderId, "File Extension must be .epw, you chose File Extension ." + fileExtention, 'visible');
                 ToggleValidatorToolTip(fileUploaderId, "File Extension must be .epw, you chose File Extension ." + fileExtention, 'block');

                 sender._uploadedFiles.shift();
                 sender.deleteFileInputAt(0);
                 sender.updateClientState();

                 args.get_row().setAttribute("stop", "stop")
             }
             else { //else, check max length value
                 var lblMaxLengthId = (fileUploaderId.indexOf("Add") > -1) ? 'SystemGlobalFilesAdd_form_lblMaxLength' : 'SystemGlobalFilesView_form_lblMaxLength';
                 var maxLength = (lblMaxLengthId != null) ? document.querySelector('label[id*="' + lblMaxLengthId + '"]') : null;
                 
                 if (maxLength != null) { 
                     var maxLengthText = maxLength.getAttribute('value');

                     if (fileName.length > maxLengthText) {
                         ToggleValidatorToolTip(fileUploaderId, "File Name length must be less than " + maxLengthText + " characters.", 'block');

                         sender._uploadedFiles.shift();
                         sender.deleteFileInputAt(0);
                         sender.updateClientState();

                         args.get_row().setAttribute("stop", "stop")
                     }
                 }
             }
         }
     }

     function FileUploading(sender, args) {
         if (args.get_row().getAttribute("stop") == "stop") args.set_cancel(true);
     }

     function FileUploaded(sender, args) {
         var fileUploaderId = sender.get_element().id;

         //ToggleValidatorToolTip(fileUploaderId, "Global File is a required field.", 'hidden');
         ToggleValidatorToolTip(fileUploaderId, "Global File is a required field.", 'none');
     }

     function ToggleValidatorToolTip(fileUploaderId, toolTipText, visibility) {
         if (fileUploaderId != null) {
             //var customValidatorId = (fileUploaderId.indexOf("Add") > -1) ? 'SystemGlobalFilesAdd_form_fileUploaderVCE_popupTable' : 'SystemGlobalFilesView_form_fileUploaderVCE_popupTable';
             //var validatorToolTip = (customValidatorId != null) ? document.querySelector('table[id*="' + customValidatorId + '"]') : null;
             //var validatorToolTipText = (validatorToolTip != null) ? validatorToolTip.querySelector("td.ajax__validatorcallout_error_message_cell") : null;
             
             //if (validatorToolTip != null && validatorToolTipText != null) {
             //    validatorToolTip.style.visibility = visibility;
             //    validatorToolTipText.textContent = toolTipText;
             //}
             var errorMsgLabelId = (fileUploaderId.indexOf("Add") > -1) ? 'SystemGlobalFilesAdd_form_errorMsg' : 'SystemGlobalFilesView_form_errorMsg';
             var errorMsgLabel = (errorMsgLabelId != null) ? document.querySelector('label[id*="' + errorMsgLabelId + '"]') : null;

             if (errorMsgLabel != null) {
                 errorMsgLabel.style.display = visibility;
                 errorMsgLabel.textContent = toolTipText;
             }
         }
     }
</script>

<asp:HiddenField ID="hdnGFID" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">*Global File Type:</label>
  <asp:DropDownList ID="ddlGlobalFileTypeID" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlGlobalFileTypeID_SelectedIndexChanged" runat="server" />
</div>

<div id="divConfluence" class="divForm" runat="server" visible="false">
  <label class="label"><asp:Literal ID="litAsterisk1" runat="server" Text="*" />Confluence Pages:</label>
  <asp:DropDownList ID="ddlConfluencePages" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
  <label id="lblConfluenceError" runat="server" class="errorMessage"></label>
</div>

<div id="divUploader" runat="server" class="divForm">
  <label class="label"><asp:Literal ID="litAsterisk2" runat="server" Text="*" />Global File:</label>

  <asp:TextBox ID="placeholder" runat="server" Width="230" Height="0" BackColor="White" BorderStyle="None" /> <!-- for validating against an empty upload field -->

  <div style="height: 0px"><label id="lblMaxLength" runat="server"></label></div> <!-- label to hold the max length val pulled from the dbml file -->

  <telerik:RadAsyncUpload ID="fileUploader" runat="server" AllowedFileExtensions="epw" MultipleFileSelection="Disabled" MaxFileInputsCount="1" OnClientFileSelected="CheckExtensionAndNameLength" OnClientFileUploading="FileUploading" OnClientFileUploaded="FileUploaded" />

  <label id="errorMsg" runat="server" class="errorMessage"></label> <!-- have to use the red label errors for now, until I can get the ValidatorCalloutExtender to work -->

  <p>(Note: Go to the <a href="http://apps1.eere.energy.gov/buildings/energyplus/weatherdata_about.cfm" target="_blank" >Energy Plus</a> website to get weather files.)</p>
</div>

<div class="divForm">
  <label class="label">Notes:</label>
  <textarea id="areaNotes" runat="server" name="txtNotes" cols="40" onkeyup="limitChars(this, 500, 'divNotesCharInfo')" rows="5"></textarea>
  <div id="divNotesCharInfo"></div>
</div>
<br />

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="globalFileTypeRFV" runat="server" ErrorMessage="Global File Type is a required field." ControlToValidate="ddlGlobalFileTypeID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="globalFileTypeVCE" runat="server" TargetControlID="globalFileTypeRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="confluencePagesRFV" runat="server" ErrorMessage="Confluence Page is a required field." ControlToValidate="ddlConfluencePages" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="confluencePagesVCE" runat="server" TargetControlID="confluencePagesRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:CustomValidator ID="fileUploaderCV" runat="server" ClientValidationFunction="ValidateUpload" ValidateEmptyText="true" ErrorMessage="Global File is a required field." ControlToValidate="placeholder" Display="None" ValidationGroup="" />
<%--<ajaxToolkit:ValidatorCalloutExtender ID="fileUploaderVCE" runat="server" TargetControlID="fileUploaderCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />--%>