﻿using CW.Business;
using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Data.Linq.Mapping;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CW.Website._confluence;
using System.Linq;
using CW.Common.Constants;

namespace CW.Website._administration.globalFiles
{
    public partial class GlobalFileForm : FormGenericBase<GlobalFileForm, GlobalFilesLookup>
    {
        #region fields

            private UploadedFile file;  
            private const string confluenceError = "Error with confluence api. Please contact administrator.";

        #endregion

        #region methods

        #region overrides

                public override void PopulateForm(GlobalFilesLookup obj)
                {
                    base.PopulateForm(obj);

                    ddlGlobalFileTypeID_SelectedIndexChanged(null, null);
                }

                public Boolean CanGlobalFileBePersisted(GlobalFilesLookup globalFile, out String message)
                {
                    if (IsFileSet(globalFile))
                    {
                        if (DataMgr.GlobalFileDataMapper.DoesGlobalFileItemExist<GlobalFilesLookup>((gfl => gfl.GlobalFileTypeID == globalFile.GlobalFileTypeID &&
                                                                                                                 gfl.DisplayName == globalFile.DisplayName && 
                                                                                                                 gfl.GFID != globalFile.GFID), (gfl => gfl)))
                        {
                            message = "{EntityTypeName} already exists.";

                            return false;
                        }
                    }

                    message = null;

                    return true;
                }

            #endregion

            #region Page Events

                private void Page_FirstInit()
                {
                    tab.BindGlobalFileTypes(ddlGlobalFileTypeID);
                }

                private void Page_Load()
                {
                    this.divUploader.Attributes.Add("formMode", Enum.GetName(typeof(PersistModeEnum), FormMode));

                    var fieldProp = typeof(GlobalFilesLookup).GetProperty("DisplayName");
                    this.lblMaxLength.Attributes.Add("value", Regex.Match(((ColumnAttribute)(fieldProp.GetCustomAttributes(typeof(ColumnAttribute), true)[0])).DbType, @"\d+").Value);
                
                    page.TabStateMonitor.OnTabStateChanged += TabStateMonitor_OnTabStateChanged;

                    litAsterisk1.Visible = litAsterisk2.Visible = (FormMode == PersistModeEnum.Insert) ? true : false;
                    fileUploaderCV.Enabled = (FormMode == PersistModeEnum.Insert) ? true : false;
                    confluencePagesRFV.Enabled = (FormMode == PersistModeEnum.Insert) ? true : false;
                }

            #endregion

            #region Tab and Dropdown Events

                void TabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages)
                {
                    tab.BindGlobalFileTypes(ddlGlobalFileTypeID);
                }

                protected void ddlGlobalFileTypeID_SelectedIndexChanged(object sender, EventArgs e)
                {
                    ResetFormVisibility();

                    switch(Convert.ToInt32(ddlGlobalFileTypeID.SelectedValue))
                    {
                        case BusinessConstants.GlobalFileType.ConfluenceFileTypeID:
                            try
                            {                                
                                var apiService = new ApiDocumentationService();

                                var lItem = new ListItem();

                                lItem.Text = (FormMode == PersistModeEnum.Insert) ? "Select one..." : "Optionally select one to update...";
                                lItem.Value = "-1";
                                lItem.Selected = true;

                                ddlConfluencePages.Items.Clear();
                                ddlConfluencePages.Items.Add(lItem);
                    
                                ddlConfluencePages.DataTextField = "label";
                                ddlConfluencePages.DataValueField = "self";
                                ddlConfluencePages.DataSource = apiService.GetConfluencePages();
                                ddlConfluencePages.DataBind();                                                               
                            }
                            catch(Exception ex)
                            {
                                lblConfluenceError.InnerText = confluenceError;
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving confluence pages." + ex);
                            }

                            divConfluence.Visible = true;
                            divUploader.Visible = false;

                            break;
                        default:
                            break;
                    }                                
                }

            #endregion

            public void PersistGlobalFile(GlobalFilesLookup globalFile)
            {
                byte[] bytes = null;
                bool isFileSet = IsFileSet(globalFile);
                string contentType = "";

                if (isFileSet)
                {
                    switch (globalFile.GlobalFileTypeID)
                    {
                        case BusinessConstants.GlobalFileType.ConfluenceFileTypeID:
                            try
                            {
                                var apiService = new ApiDocumentationService();
                                var html = apiService.GetDocumentation(ddlConfluencePages.SelectedValue);
                                bytes = StringHelper.ConvertToByteArray(html);

                                globalFile.Extension = ".html";
                                globalFile.FileSize = bytes.LongLength;

                                contentType = "text/html";
                            }
                            catch (Exception ex)
                            {
                                
                                lblConfluenceError.InnerText = confluenceError;

                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving confluence documentation." + ex);
                            }

                            break;
                        default:
                            globalFile.Extension = file.GetExtension().ToUpper();
                            globalFile.FileSize = file.ContentLength;

                            bytes = StreamHelper.ReadAll(file.InputStream);
                            contentType = file.ContentType;

                            break;
                    }
                }

                //Insert requires the file so no check necessary there for sql/blob, but updates don't require another file, so file null check is made there before updating blob.
                switch (FormMode)
                {
                    case PersistModeEnum.Insert:
                        {
                            var blobGuid = DataMgr.GlobalFileDataMapper.InsertGlobalFile(globalFile);

                            DataMgr.GlobalFileDataMapper.InsertGlobalFileBlob(ddlGlobalFileTypeID.SelectedValue, blobGuid.ToString(), bytes, contentType);

                            break;
                        }
                    case PersistModeEnum.Update:
                        {
                            var globalFileItem = DataMgr.GlobalFileDataMapper.GetGlobalFileItem<GlobalFilesLookup>((gfl => gfl.GFID == globalFile.GFID), (gfl => gfl));

                            DataMgr.GlobalFileDataMapper.UpdateGlobalFile(globalFile);

                            if (isFileSet) DataMgr.GlobalFileDataMapper.UpdateGlobalFileBlob(ddlGlobalFileTypeID.SelectedValue, globalFileItem.BlobGUID.ToString(), bytes, contentType);

                            break;
                        }
                }
            }

            private Boolean IsFileSet(GlobalFilesLookup globalFile)
            {
                switch (globalFile.GlobalFileTypeID)
                {
                    case BusinessConstants.GlobalFileType.ConfluenceFileTypeID:

                        if (ddlConfluencePages.SelectedValue != "-1")
                        {
                            globalFile.DisplayName = ddlConfluencePages.SelectedItem.Text;
                            return true;
                        } 
                        break;               
                    default:
                        file = (fileUploader.UploadedFiles.Count > 0) ? fileUploader.UploadedFiles[0] : null;

                        if (file != null)
                        {
                            globalFile.DisplayName = file.GetNameWithoutExtension();
                            return true;
                        }
                        break;                                                
                }

                return false;
            }

            private void ResetFormVisibility()
            {
                divConfluence.Visible = false;
                divUploader.Visible = true;                
            }

        #endregion

        #region properties

            #region overrides

                public override Delegates<GlobalFilesLookup>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get { return CanGlobalFileBePersisted; }
                }

                public override Action<GlobalFilesLookup> PersistEntityAction
                {
                    get { return PersistGlobalFile; }
                }

        #endregion

        #endregion

    }
}