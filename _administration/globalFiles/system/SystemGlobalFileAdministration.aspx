﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_masters/SystemAdmin.Master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemGlobalFileAdministration.aspx.cs" Inherits="CW.Website._administration.globalFiles.system.SystemGlobalFileAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="SystemGlobalFileTypesViewTab.ascx" tagPrefix="CW" tagName="SystemGlobalFileTypesViewTab" %>
<%@ Register src="SystemGlobalFileTypesAddTab.ascx" tagPrefix="CW" tagName="SystemGlobalFileTypesAddTab" %>
<%@ Register src="SystemGlobalFilesViewTab.ascx" tagPrefix="CW" tagName="SystemGlobalFilesViewTab" %>
<%@ Register src="SystemGlobalFilesAddTab.ascx" tagPrefix="CW" tagName="SystemGlobalFilesAddTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <CW:PageHeader runat="server" Title="Global File Administration" Description="The global file administration area is used to view, add, and edit global files." />

      <div class="administrationControls">
        <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
          <Tabs>
            <telerik:RadTab Text="View Global File Types" />
            <telerik:RadTab Text="Add Global File Type" />
            <telerik:RadTab Text="View Global Files" />
            <telerik:RadTab Text="Add Global File" />
          </Tabs>
        </telerik:RadTabStrip>

        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:SystemGlobalFileTypesViewTab runat="server" ID="SystemGlobalFileTypesView" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:SystemGlobalFileTypesAddTab runat="server" ID="SystemGlobalFileTypesAdd" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView3" runat="server">
            <CW:SystemGlobalFilesViewTab runat="server" ID="SystemGlobalFilesView" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView4" runat="server">
            <CW:SystemGlobalFilesAddTab runat="server" ID="SystemGlobalFilesAdd" />
          </telerik:RadPageView>
        </telerik:RadMultiPage>
      </div>

</asp:Content>