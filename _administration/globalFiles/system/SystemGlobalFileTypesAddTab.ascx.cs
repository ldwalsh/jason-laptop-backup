﻿using CW.Data;
using CW.Utility;
using System;

namespace CW.Website._administration.globalFiles.system
{
    public partial class SystemGlobalFileTypesAddTab : AddTabBase<GlobalFileType, SystemGlobalFileAdministration>
    {
        #region methods

            private void Page_Init()
            {
                CanEntityBeAdded = form.CanGlobalFileTypeBePersisted;
                AddEntityMethod = form.PersistEntityAction;
            }

        #endregion

        #region properties

            protected override String IdColumnName
            {
                get { return PropHelper.G<GlobalFileType>(gft => gft.GlobalFileTypeID); }
            }

            protected override String NameField
            {
                get { return PropHelper.G<GlobalFileType>(gft => gft.GlobalFileTypeName); }
            }

            protected override String EntityTypeName
            {
                get { return typeof(GlobalFileType).Name; }
            }

        #endregion
    }
}