﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace CW.Website._administration.globalFiles.system
{
    public partial class SystemGlobalFileTypesViewTab : ViewTabWithFormBase<GlobalFileType, AdministrationPageBase>
    {
		#region methods

			#region override

				protected override void GridRowDataBound(Object sender, EventArgs e)
				{
					var rows = grid.Rows;

					foreach (GridViewRow row in rows)
					{
						if ((row.RowState & DataControlRowState.Edit) == 0 && row.RowType != DataControlRowType.Header)
						{
							//hide delete link for undeletable global file types, which cannot be deleted
							try
							{
								if (BusinessConstants.GlobalFileType.UnDeletableWeatherFileTypesDictionary.ContainsValue(Convert.ToInt32(((GridView)sender).DataKeys[row.RowIndex].Value)))
								{
									var lnkButton = row.FindControl("lnkDelete");
									if (lnkButton != null) ((LinkButton)lnkButton).Visible = false;
								}
							}
							catch { }
						}
					}
				}

			#endregion

			protected void Page_Init(Object sender, EventArgs e)
			{
				CanEntityBeUpdated = form.CanEntityBePersisted;
				CanEntityBeDeleted = CanGlobalFileTypeBeDeleted;

				UpdateEntityMethod = form.PersistEntityAction;

				DeleteEntityMethod = DeleteGlobalFileType;
			}

			private Boolean CanGlobalFileTypeBeDeleted(String gftid, out String message)
			{
				var id = Convert.ToInt32(gftid);

				if (DataMgr.GlobalFileDataMapper.DoesGlobalFileItemExist<GlobalFilesLookup>((gfl => gfl.GlobalFileTypeID == id), (c => c)))
				{
					message = "Cannot delete a global file type which is associated with global files.";

					return false;
				}

				message = null;

				return true;
			}

			private void DeleteGlobalFileType(String gftid)
			{
				DataMgr.GlobalFileDataMapper.DeleteGlobalFileItem<GlobalFileType>((gft => gft), (gft => gft.GlobalFileTypeID == Convert.ToInt32(gftid)));

				page.TabStateMonitor.ChangeState();
			}

		#endregion

		#region properties

			protected override IEnumerable<String> GridColumnNames
			{
				get { return PropHelper.F<GlobalFileType>(_ => _.GlobalFileTypeID, _ => _.GlobalFileTypeName, _ => _.GlobalFileTypeDescription); }
			}

			protected override String IdColumnName
			{
				get { return PropHelper.G<GlobalFileType>(f => f.GlobalFileTypeID); }
			}

			protected override String NameField
			{
				get { return PropHelper.G<GlobalFileType>(f => f.GlobalFileTypeName); }
			}

			protected override String EntityTypeName
			{
				get { return "global file type"; } //typeof(GlobalFileType).Name;
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get
				{
					return (searchText => DataMgr.GlobalFileDataMapper.GetAllGlobalFileSearchedItems<GlobalFileType>(searchText, (gft => gft), null, (gft => gft.GlobalFileTypeName), "GlobalFileTypeName"));
				}
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get { return (gftid => DataMgr.GlobalFileDataMapper.GetGlobalFileItem<GlobalFileType>((gft => gft.GlobalFileTypeID == Convert.ToInt32(gftid)), (gft => gft))); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get { return (gftid => DataMgr.GlobalFileDataMapper.GetGlobalFileItem<GlobalFileType>((gft => gft.GlobalFileTypeID == Convert.ToInt32(gftid)), (gft => gft))); }
			}

			protected override String CacheSubKey
			{
				get {return null;}
			}

		#endregion
    }
}