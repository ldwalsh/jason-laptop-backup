﻿using System;
using System.Web.UI.WebControls;
using CW.Data;
using CW.Utility;

namespace CW.Website._administration.globalFiles.system
{
    public partial class SystemGlobalFilesAddTab : AddTabBase<GlobalFilesLookup, SystemGlobalFileAdministration>
    {
        #region methods

            private void Page_Init()
            {
                CanEntityBeAdded = form.CanGlobalFileBePersisted;
                AddEntityMethod = form.PersistEntityAction;
            }

        #endregion

        #region properties

            protected override String IdColumnName
            {
                get { return PropHelper.G<GlobalFilesLookup>(gfl => gfl.GFID); }
            }

            protected override String NameField
            {
                get { return PropHelper.G<GlobalFilesLookup>(gfl => gfl.DisplayName); }
            }

            protected override String EntityTypeName
            {
                get { return typeof(GlobalFilesLookup).Name; }
            }

        #endregion
    }
}