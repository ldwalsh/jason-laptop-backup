﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemGlobalFilesViewTab.ascx.cs" Inherits="CW.Website._administration.globalFiles.system.SystemGlobalFilesViewTab" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="GlobalFileForm" src="../GlobalFileForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Global Files" />

<asp:Label ID="lblResults" runat="server" Text="" />

<div class="divForm">
  <label class="label">Global File Type:</label>
  <asp:DropDownList ID="ddlGlobalFileType" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlGlobalFileType_OnSelectedIndexChanged" />
</div>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>                                                                                                    

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DisplayName" HeaderText="Display Name">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<GlobalFilesLookup>(gfl=>gfl.DisplayName))%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Extension" HeaderText="Extension">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<GlobalFilesLookup>(gfl=>gfl.Extension))%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FileSize" HeaderText="File Size (MB)">
        <ItemTemplate>
          <%# MathHelper.ConvertBytesToMB(Convert.ToInt64(GetCellContent(Container, PropHelper.G<GlobalFilesLookup>(gfl=> gfl.FileSize))), 1) %>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="GlobalFileTypeName" HeaderText="Global File Type Name">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<GlobalFileType>(gft=>gft.GlobalFileTypeName)) %>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" CommandName="Download" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" Text="Download" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this Global File permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<!--SELECT GLOBAL FILE DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
  <Fields>
    <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
      <ItemTemplate>
        <div>
          <h2>Global File Details</h2>

          <ul class="detailsList">
            <CW:UListItem runat="server" Label="Global File ID" Value="<%# Eval(PropHelper.G<GlobalFilesLookup>(gfl=>gfl.GFID)) %>" />
            <CW:UListItem runat="server" Label="Global File Display Name" Value="<%# Eval(PropHelper.G<GlobalFilesLookup>(gfl=>gfl.DisplayName)) %>" />
            <CW:UListItem runat="server" Label="Global File Extension" Value="<%# Eval(PropHelper.G<GlobalFilesLookup>(gfl=>gfl.Extension)) %>" />
            <CW:UListItem runat="server" Label="Global File Size (MB)" Value="<%# MathHelper.ConvertBytesToMB(Convert.ToInt64(Eval(PropHelper.G<GlobalFilesLookup>(gfl=>gfl.FileSize))), 1) %>" />
            <CW:UListItem runat="server" Label="Notes" Value='<%# !String.IsNullOrEmpty(Eval(PropHelper.G<GlobalFilesLookup>(gfl => gfl.Notes)).ToString()) ? Eval(PropHelper.G<GlobalFilesLookup>(gfl=>gfl.Notes)) : String.Empty %>' />
            <CW:UListItem runat="server" Label="Global File Type Name" Value="<%# Eval(PropHelper.G<GlobalFilesLookup, GlobalFileType>(gfl => gfl.GlobalFileType, gft => gft.GlobalFileTypeName))  %>" />
          </ul>
        </div>
      </ItemTemplate> 
    </asp:TemplateField>
  </Fields>
</asp:DetailsView>
                                                                     
<!--EDIT GLOBAL FILE PANEL -->
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">
  <div>
    <h2>Edit Global File</h2>
  </div>
  <div>
    <CW:GlobalFileForm ID="form" FormMode="Update" UpdateButtonCaption="Update Global File" runat="server" />
  </div>
</asp:Panel>