﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemGlobalFilesAddTab.ascx.cs" Inherits="CW.Website._administration.globalFiles.system.SystemGlobalFilesAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx"  %>
<%@ Register TagPrefix="CW" TagName="GlobalFileForm" Src="~/_administration/globalFiles/GlobalFileForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Global File" />
<CW:GlobalFileForm ID="form" runat="server" FormMode="Insert" UpdateButtonCaption="Add Global File" />