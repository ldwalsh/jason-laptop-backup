﻿using CW.Data;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Linq.Expressions;

namespace CW.Website._administration.globalFiles.system
{
    public partial class SystemGlobalFilesViewTab : ViewTabWithFormBase<GlobalFilesLookup, AdministrationPageBase>
    {
		#region methods

			#region override

				protected override void GridRowCommand(Object sender, GridViewCommandEventArgs e)
				{
					if (e.CommandName == "Download")
					{
						var index = Convert.ToInt32(e.CommandArgument);
						var gfid = grid.DataKeys[index].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)];
						var globalFileItem = DataMgr.GlobalFileDataMapper.GetGlobalFileItem<GlobalFilesLookup>((gfl => gfl.GFID == Convert.ToInt32(gfid)), (gfl => gfl));
						var fileToAttach = DataMgr.GlobalFileDataMapper.GetGlobalFileBlob(globalFileItem.GlobalFileTypeID.ToString(), globalFileItem.BlobGUID.ToString());
						var fileHandler = new DownloadRequestHandler.HandledFileAttacher();
						var fileName = globalFileItem.DisplayName + globalFileItem.Extension.ToLower();

						((IFileAttacher)fileHandler).AttachForDownload(fileName, fileToAttach);
					}
				}

			#endregion

			protected void Page_Init(Object sender, EventArgs e)
			{
				BindGlobalFileTypes(ddlGlobalFileType);

				CanEntityBeUpdated = form.CanEntityBePersisted;
				CanEntityBeDeleted = CanGlobalFileBeDeleted;

				UpdateEntityMethod = form.PersistEntityAction;

				DeleteEntityMethod = DeleteGlobalFile;
			}

			private void Page_Load()
			{
				page.TabStateMonitor.OnTabStateChanged += TabStateMonitor_OnTabStateChanged;
			}

			void TabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages)
			{
				BindGlobalFileTypes(ddlGlobalFileType);
			}

			private Boolean CanGlobalFileBeDeleted(String gfid, out String message)
			{
				var id = Convert.ToInt32(gfid);

				if (DataMgr.GlobalFileDataMapper.DoesGlobalFileItemExist<Building>((b => b.WeatherGFID == id), (b => b)))
				{
					message = "Cannot delete a global file which is associated with a building.";

					return false;
				}

				message = null;

				return true;
			}

			private void DeleteGlobalFile(String gfid)
			{
				var globalFilesLookupItem = DataMgr.GlobalFileDataMapper.GetGlobalFileItem<GlobalFilesLookup>((gfl => gfl.GFID == Convert.ToInt32(gfid)), (gfl => gfl));

				DataMgr.GlobalFileDataMapper.DeleteGlobalFileItem<GlobalFilesLookup>((gfl => gfl), (gfl => gfl.GFID == Convert.ToInt32(gfid)));
				DataMgr.GlobalFileDataMapper.DeleteGlobalFileBlob(globalFilesLookupItem.GlobalFileTypeID.ToString(), globalFilesLookupItem.BlobGUID.ToString());

				page.TabStateMonitor.ChangeState();
			}

		#endregion

		#region properties

			protected override IEnumerable<String> CacheHashKeySubList
			{
				get {return ddlGlobalFileType.Items.Cast<ListItem>().Select(_ => _.Value);}
			}

			protected override IEnumerable<String> GridColumnNames
			{
				get { return PropHelper.F<GlobalFilesLookup>(_ => _.DisplayName, _ => _.Extension, _ => _.FileSize, _ => _.GlobalFileType.GlobalFileTypeName); }
			}

			protected override String IdColumnName
			{
				get { return PropHelper.G<GlobalFilesLookup>(f => f.GFID); }
			}

			protected override String NameField
			{
				get { return PropHelper.G<GlobalFilesLookup>(f => f.DisplayName); }
			}

			protected override String EntityTypeName
			{
				get { return "global file"; } //typeof(GlobalFilesLookup).Name;
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get
				{
					return (searchText => DataMgr.GlobalFileDataMapper.GetAllGlobalFileSearchedItems<GlobalFilesLookup>(searchText,
																																(gfl => gfl),
																																(gfl => gfl.GlobalFileTypeID == Convert.ToInt32(ddlGlobalFileType.SelectedValue)),
																																(gfl => gfl.DisplayName),
																																"DisplayName",
																																(_ => _.GlobalFileType)));
				}
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get { return (gfid => DataMgr.GlobalFileDataMapper.GetGlobalFileItem<GlobalFilesLookup>((gfl => gfl.GFID == Convert.ToInt32(gfid)), (gfl => gfl))); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get { return (gfid => DataMgr.GlobalFileDataMapper.GetGlobalFileItem<GlobalFilesLookup>((gfl => gfl.GFID == Convert.ToInt32(gfid)), (gfl => gfl), (gf => gf.GlobalFileType))); }
			}

			protected override String CacheSubKey
			{
				get { return ddlGlobalFileType.SelectedValue; }
			}

		#endregion

		#region Dropdown Events

			protected void ddlGlobalFileType_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				BindGrid();

				EntityDetailView.Visible = false;
				PanelEditEntity.Visible = false;
			}

		#endregion
    }
}