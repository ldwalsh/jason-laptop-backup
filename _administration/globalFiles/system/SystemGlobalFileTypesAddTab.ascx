﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemGlobalFileTypesAddTab.ascx.cs" Inherits="CW.Website._administration.globalFiles.system.SystemGlobalFileTypesAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx"  %>
<%@ Register TagPrefix="CW" TagName="GlobalFileTypeForm" Src="../GlobalFileTypeForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Global File Type" />
<CW:GlobalFileTypeForm ID="form" runat="server" FormMode="Insert" UpdateButtonCaption="Add Global File Type" />