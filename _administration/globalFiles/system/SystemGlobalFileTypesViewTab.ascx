﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemGlobalFileTypesViewTab.ascx.cs" Inherits="CW.Website._administration.globalFiles.system.SystemGlobalFileTypesViewTab" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="GlobalFileTypeForm" src="../GlobalFileTypeForm.ascx"  %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Global File Types" />

<asp:Label ID="lblResults" runat="server" Text="" />

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>                                                                                                    

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="GlobalFileTypeName" HeaderText="Global File Type Name">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<GlobalFileType>(gft=>gft.GlobalFileTypeName)) %>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="GlobalFileTypeDescription" HeaderText="Global File Type Description">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<GlobalFileType>(_ => _.GlobalFileTypeDescription)) %>"><%# GetCellContent(Container, PropHelper.G<GlobalFileType>(gft=>gft.GlobalFileTypeDescription), 30) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkDelete" runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this Global File Type permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<!--SELECT GLOBAL FILE TYPE DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
  <Fields>
    <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
      <ItemTemplate>
        <div>
          <h2>Global File Type Details</h2>

          <ul class="detailsList">
            <CW:UListItem runat="server" Label="Global File Type ID" Value="<%# Eval(PropHelper.G<GlobalFileType>(gft=>gft.GlobalFileTypeID)) %>" />
            <CW:UListItem runat="server" Label="Global File Type Name" Value="<%# Eval(PropHelper.G<GlobalFileType>(gft=>gft.GlobalFileTypeName)) %>" />
            <CW:UListItem runat="server" Label="Global File Type Description" Value="<%# Eval(PropHelper.G<GlobalFileType>(gft=>gft.GlobalFileTypeDescription)) %>" />
          </ul>
        </div>
      </ItemTemplate> 
    </asp:TemplateField>
  </Fields>
</asp:DetailsView>
                                                                     
<!--EDIT GLOBAL FILE TYPE PANEL -->
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">
  <div>
    <h2>Edit Global File Type</h2>
  </div>
  <div>
    <CW:GlobalFileTypeForm ID="form" FormMode="Update" UpdateButtonCaption="Update Global File Type" runat="server" />
  </div>
</asp:Panel>