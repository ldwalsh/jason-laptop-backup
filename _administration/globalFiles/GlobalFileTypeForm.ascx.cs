﻿using CW.Data;
using System;
using System.Web.UI.WebControls;

namespace CW.Website._administration.globalFiles
{
    public partial class GlobalFileTypeForm : FormGenericBase<GlobalFileTypeForm, GlobalFileType>
    {
        #region methods

            public Boolean CanGlobalFileTypeBePersisted(GlobalFileType globalFileType, out String message)
            {
                if (DataMgr.GlobalFileDataMapper.DoesGlobalFileItemExist<GlobalFileType>((gft => gft.GlobalFileTypeID != globalFileType.GlobalFileTypeID &&
                                                                                                      gft.GlobalFileTypeName == globalFileType.GlobalFileTypeName), (gft => gft)))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

        #endregion

        #region properties

            #region overrides

                public override Delegates<GlobalFileType>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get { return CanGlobalFileTypeBePersisted; }
                }

                public override Action<GlobalFileType> PersistEntityAction
                {
                    get
                    {
                        switch (FormMode)
                        {
                            case PersistModeEnum.Insert:
                                {
                                    return DataMgr.GlobalFileDataMapper.InsertGlobalFileType;
                                }
                            case PersistModeEnum.Update:
                                {
                                    return DataMgr.GlobalFileDataMapper.UpdateGlobalFileType;
                                }
                        }

                        return null;
                    }
                }

            #endregion

        #endregion
    }
}