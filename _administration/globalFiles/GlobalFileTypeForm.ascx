﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GlobalFileTypeForm.ascx.cs" Inherits="CW.Website._administration.globalFiles.GlobalFileTypeForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:HiddenField ID="hdnGlobalFileTypeID" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">*Global File Type Name:</label>
  <asp:TextBox ID="txtGlobalFileTypeName" CssClass="textbox" MaxLength="50" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Global File Type Description:</label>
  <textarea name="txtGlobalFileTypeDescription" id="txtGlobalFileTypeDescription" cols="40" rows="5" onkeyup="limitChars(this, 500, 'divGlobalFileTypeDescription')" runat="server"></textarea>
  <div id="divGlobalFileTypeDescription"></div>
</div>

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="globalFileTypeNameRFV" runat="server" ErrorMessage="Global File Type Name is a required field." ControlToValidate="txtGlobalFileTypeName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="globalFileTypeName" runat="server" TargetControlID="globalFileTypeNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="globalFileTypeDescriptionRFV" runat="server" ErrorMessage="Global File Type Description is a required field." ControlToValidate="txtGlobalFileTypeDescription" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="globalFileTypeDescriptionVCE" runat="server" TargetControlID="globalFileTypeDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />