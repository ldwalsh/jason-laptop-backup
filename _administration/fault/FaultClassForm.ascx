﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="FaultClassForm.ascx.cs" Inherits="CW.Website._administration.fault.FaultClassForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:HiddenField ID="hdnFaultClassID" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">*Fault Class Name:</label>
  <asp:TextBox ID="txtFaultClassName" CssClass="textbox" MaxLength="50" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Fault Class Description:</label>
  <asp:TextBox ID="txtFaultClassDescription" CssClass="textbox" MaxLength="200" runat="server" />
</div>

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="faultClassNameRFV" runat="server" ErrorMessage="Fault Class Name is a required field." ControlToValidate="txtFaultClassName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultClassNameVCE" runat="server" TargetControlID="faultClassNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="faultClassDescriptionRFV" runat="server" ErrorMessage="Fault Class Description is a required field." ControlToValidate="txtFaultClassDescription" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultClassDescriptionVCE" runat="server" TargetControlID="faultClassDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />