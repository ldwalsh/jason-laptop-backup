﻿using System;
using System.Web.UI.WebControls;
using CW.Data;
using CW.Utility;

namespace CW.Website._administration.fault.system
{
    public partial class SystemFaultTypesAddTab : AddTabBase<FaultType, SystemFaultAdministration>
    {
        #region methods

            private void Page_Init()
            {
                CanEntityBeAdded = form.CanFaultClassBePersisted;
                AddEntityMethod = form.PersistEntityAction;
            }

        #endregion

        #region properties

            protected override String IdColumnName
            {
                get { return PropHelper.G<FaultType>(f => f.FaultTypeID); }
            }

            protected override String NameField
            {
                get { return PropHelper.G<FaultType>(f => f.FaultTypeName); }
            }

            protected override String EntityTypeName
            {
                get { return typeof(FaultType).Name; }
            }

        #endregion
    }
}