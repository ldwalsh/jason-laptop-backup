﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_masters/SystemAdmin.Master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemFaultAdministration.aspx.cs" Inherits="CW.Website._administration.fault.system.SystemFaultAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="SystemFaultClassesViewTab.ascx" tagPrefix="CW" tagName="FaultClassesViewTab" %>
<%@ Register src="SystemFaultClassesAddTab.ascx" tagPrefix="CW" tagName="FaultClassesAddTab" %>
<%@ Register src="SystemFaultTypesViewTab.ascx" tagPrefix="CW" tagName="FaultTypesViewTab" %>
<%@ Register src="SystemFaultTypesAddTab.ascx" tagPrefix="CW" tagName="FaultTypesAddTab" %>
<%@ Register src="SystemFaultContentViewTab.ascx" tagPrefix="CW" tagName="FaultContentViewTab" %>
<%@ Register src="SystemFaultContentAddTab.ascx" tagPrefix="CW" tagName="FaultContentAddTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <CW:PageHeader runat="server" Title="Fault Administration" Description="The fault administration area is used to view, add, and edit faults." />

      <div class="administrationControls">
        <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
          <Tabs>
            <telerik:RadTab Text="View Fault Classes" />
            <telerik:RadTab Text="Add Fault Class" />
            <telerik:RadTab Text="View Fault Types" />
            <telerik:RadTab Text="Add Fault Type" />
            <telerik:RadTab Text="View Fault Content" />
            <telerik:RadTab Text="Add Fault Content" />
          </Tabs>
        </telerik:RadTabStrip>

        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:FaultClassesViewTab runat="server" ID="FaultClassesView" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:FaultClassesAddTab runat="server" ID="FaultClassesAdd" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView3" runat="server">
            <CW:FaultTypesViewTab runat="server" ID="FaultTypesView" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView4" runat="server">
            <CW:FaultTypesAddTab runat="server" ID="FaultTypesAdd" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView5" runat="server">
            <CW:FaultContentViewTab runat="server" ID="FaultContentView" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView6" runat="server">
            <CW:FaultContentAddTab runat="server" ID="FaultContentAdd" />
          </telerik:RadPageView>
        </telerik:RadMultiPage>
      </div>

</asp:Content>