﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;

namespace CW.Website._administration.fault.system
{
    public partial class SystemFaultTypesViewTab : ViewTabWithFormBase<FaultType, AdministrationPageBase>
    {
		#region methods

			protected void Page_Init(Object sender, EventArgs e)
			{
				CanEntityBeUpdated = form.CanEntityBePersisted;
				CanEntityBeDeleted = CanFaultTypeBeDeleted;

				UpdateEntityMethod = form.PersistEntityAction;

				DeleteEntityMethod = DeleteFaultType;
			}

			private Boolean CanFaultTypeBeDeleted(String ftid, out String message)
			{
				var id = Convert.ToInt32(ftid);

				if (DataMgr.FaultDataMapper.DoesFaultItemExist<FaultContent>((fc => fc.FaultTypeID == id), (fc => fc)))
				{
					message = "Cannot delete a fault type which is associated with a fault content.";

					return false;
				}

				message = null;

				return true;
			}

			private void DeleteFaultType(String ftid)
			{
				DataMgr.FaultDataMapper.DeleteFaultItem<FaultType>((ft => ft), (ft => ft.FaultTypeID == Convert.ToInt32(ftid)));

				page.TabStateMonitor.ChangeState();
			}

		#endregion

		#region properties

			protected override IEnumerable<String> GridColumnNames
			{
				get { return PropHelper.F<FaultType>(_ => _.FaultTypeName, _ => _.FaultTypeDescription, _ => _.FaultClass.FaultClassName); }
			}
			
			protected override String IdColumnName
			{
				get { return PropHelper.G<FaultType>(f => f.FaultTypeID); }
			}

			protected override String NameField
			{
				get { return PropHelper.G<FaultType>(f => f.FaultTypeName); }
			}

			protected override String EntityTypeName
			{
				get { return "fault type"; } //typeof(FaultType).Name;
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get {return (searchText => DataMgr.FaultDataMapper.GetAllFaultSearchedItems<FaultType>(searchText, (ft => ft), null, (ft => ft.FaultTypeName), "FaultTypeName",  (_ => _.FaultClass)));}
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get { return (ftid => DataMgr.FaultDataMapper.GetFaultItem<FaultType>((ft => ft.FaultTypeID == Convert.ToInt32(ftid)), (ft => ft), (_ => _.FaultClass))); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get { return (ftid => DataMgr.FaultDataMapper.GetFaultItem<FaultType>((ft => ft.FaultTypeID == Convert.ToInt32(ftid)), (ft => ft), (_ => _.FaultClass))); }
			}

			protected override String CacheSubKey
			{
				get {return null;}
			}

		#endregion
    }
}