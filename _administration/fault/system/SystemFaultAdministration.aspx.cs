﻿using System;

namespace CW.Website._administration.fault.system
{
    public partial class SystemFaultAdministration : AdministrationPageBase
    {
        private void Page_Init()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
        }

		public override String IdentifierField
		{
			get {return null;}
		}

		public override String NameField
		{
			get {return null;}
		}

		public override String EntityTypeName
		{
			get {return null;}
		}
    }
}