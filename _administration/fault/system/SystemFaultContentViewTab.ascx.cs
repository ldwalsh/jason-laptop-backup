﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._administration.fault.system
{
    public partial class SystemFaultContentViewTab : ViewTabWithFormBase<FaultContent, AdministrationPageBase>
    {
		#region methods

			protected void Page_Init(Object sender, EventArgs e)
			{
				BindCultures(ddlCulture);
				BindCultures(ddlCultureComparison);

				CanEntityBeUpdated = form.CanEntityBePersisted;
				CanEntityBeDeleted = CanFaultContentBeDeleted;

				UpdateEntityMethod = form.PersistEntityAction;

				DeleteEntityMethod = DeleteFaultContent;
			}

			protected void Page_PreRender(Object sender, EventArgs e)
			{
				ddlCultureComparison_OnSelectedIndexChanged(null, null);
			}

			private Boolean CanFaultContentBeDeleted(String fcid, out String message)
			{
				//No contraints for deletes as of yet.

				message = null;

				return true;
			}

			private void DeleteFaultContent(String fcid)
			{
				DataMgr.FaultDataMapper.DeleteFaultItem<FaultContent>((fc => fc), (fc => fc.ID == Convert.ToInt32(fcid)));

				page.TabStateMonitor.ChangeState();
			}

		#endregion

		#region properties

			protected override IEnumerable<String> CacheHashKeySubList
			{
				get {return ddlCulture.Items.Cast<ListItem>().Select(_=>_.Value);}
			}

			protected override IEnumerable<String> GridColumnNames
			{
				get { return PropHelper.F<FaultContent>(_ => _.ContentName, _ => _.FaultID, _ => _.FaultType.FaultTypeName, _ => _.TwoLetterISOLanguageName); }
			}

			protected override String IdColumnName
			{
				get { return PropHelper.G<FaultContent>(f => f.ID); }
			}

			protected override String NameField
			{
				get { return PropHelper.G<FaultContent>(f => f.ContentName); }
			}

			protected override String EntityTypeName
			{
				get { return "fault content"; } //typeof(FaultContent).Name;
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get
				{
					return (searchText => DataMgr.FaultDataMapper.GetAllFaultSearchedItems<FaultContent>(searchText,
																												(fc => fc),
																												(fc => fc.TwoLetterISOLanguageName == ddlCulture.SelectedValue.Split('-')[0]),
																												(fc => fc.ContentName),
																												"ContentName",
																												(_ => _.FaultType)));
				}
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get { return (fcid => DataMgr.FaultDataMapper.GetFaultItem<FaultContent>((fc => fc.ID == Convert.ToInt32(fcid)), (fc => fc), (_ => _.FaultType))); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get { return (fcid => DataMgr.FaultDataMapper.GetFaultItem<FaultContent>((fc => fc.ID == Convert.ToInt32(fcid)), (fc => fc), (_ => _.FaultType))); }
			}

			protected override String CacheSubKey
			{
				get { return ddlCulture.SelectedValue; }
			}

		#endregion

		#region Dropdown Events

			protected void ddlCulture_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				BindGrid();
				EntityDetailView.Visible = false;
				PanelEditEntity.Visible = false;
			}

			protected void ddlCultureComparison_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				var faultItem = DataMgr.FaultDataMapper.GetFaultItem<FaultContent>((fc => fc.TwoLetterISOLanguageName == ddlCultureComparison.SelectedValue.Split('-')[0]), (fc => fc));
				form.ComparisonContent = (faultItem != null) ? faultItem.Content : "No Content";
			}

		#endregion
    }
}