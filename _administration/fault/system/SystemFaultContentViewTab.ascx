﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemFaultContentViewTab.ascx.cs" Inherits="CW.Website._administration.fault.system.SystemFaultContentViewTab" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="FaultContentEditForm" src="../FaultContentEditForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Fault Content" />

<asp:Label ID="lblResults" runat="server" Text="" />

<div class="divForm">
  <label class="label">Culture:</label>
  <asp:DropDownList ID="ddlCulture" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlCulture_OnSelectedIndexChanged" />
</div>

<div class="divForm">
  <label class="label">Culture Comparison:</label>
  <asp:DropDownList ID="ddlCultureComparison" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlCultureComparison_OnSelectedIndexChanged" />
</div>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentName" HeaderText="Fault Content Name">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<FaultContent>(fc=>fc.ContentName), 30)%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FaultID" HeaderText="Fault ID">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<FaultContent>(fc=>fc.FaultID))%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FaultTypeName" HeaderText="Fault Type">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<FaultType>(ft=>ft.FaultTypeName), 30)%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="TwoLetterISOLanguageName" HeaderText="Culture">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<FaultContent>(fc=>fc.TwoLetterISOLanguageName))%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this Fault Content permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<!--SELECT FAULT CONTENT DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
  <Fields>
    <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
      <ItemTemplate>
        <div>
          <h2>Fault Content Details</h2>

          <ul class="detailsList">
            <CW:UListItem runat="server" Label="Fault ID" Value="<%# Eval(PropHelper.G<FaultContent>(fc=>fc.FaultID)) %>" />
            <CW:UListItem runat="server" Label="Fault Type" Value="<%# Eval(PropHelper.G<FaultContent, FaultType>(fc => fc.FaultType, ft => ft.FaultTypeName)) %>" />
            <CW:UListItem runat="server" Label="Fault Content Name" Value="<%# Eval(PropHelper.G<FaultContent>(fc=>fc.ContentName)) %>" />
            <CW:UListItem runat="server" Label="Fault Content" Value='<%# "<div>" + Eval(PropHelper.G<FaultContent>(fc=>fc.Content)) + "</div>" %>' />
            <CW:UListItem runat="server" Label="Culture" Value="<%# Eval(PropHelper.G<FaultContent>(fc=>fc.CultureName)) %>" />
          </ul>
        </div>
      </ItemTemplate> 
    </asp:TemplateField>
  </Fields>
</asp:DetailsView>

<!--EDIT FAULT CONTENT PANEL -->
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">
  <div>
    <h2>Edit Fault Content</h2>
  </div>
  <div>
    <CW:FaultContentEditForm ID="form" FormMode="Update" UpdateButtonCaption="Update Fault Content" runat="server" />
  </div>
</asp:Panel>