﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemFaultClassesViewTab.ascx.cs" Inherits="CW.Website._administration.fault.system.SystemFaultClassesViewTab" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="FaultClassForm" src="../FaultClassForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Fault Classes" />

<asp:Label ID="lblResults" runat="server" Text="" />

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>                                                                                                    

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FaultClassName" HeaderText="Fault Class Name">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<FaultClass>(fc=>fc.FaultClassName), 30)%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FaultClassDescription" HeaderText="Fault Class Description">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<FaultClass>(fc=>fc.FaultClassDescription), 30)%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this Fault Class permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<!--SELECT FAULT CLASS DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
  <Fields>
    <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
      <ItemTemplate>
        <div>
          <h2>Fault Class Details</h2>

          <ul class="detailsList">
            <CW:UListItem runat="server" Label="Fault Class Name" Value="<%# Eval(PropHelper.G<FaultClass>(fc=>fc.FaultClassName)) %>" />
            <CW:UListItem runat="server" Label="Fault Class Description" Value="<%# Eval(PropHelper.G<FaultClass>(fc=>fc.FaultClassDescription)) %>" />
          </ul>
        </div>
      </ItemTemplate> 
    </asp:TemplateField>
  </Fields>
</asp:DetailsView>
                                                                     
<!--EDIT FAULT CLASS PANEL -->
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">
  <div>
    <h2>Edit Fault Class</h2>
  </div>
  <div>
    <CW:FaultClassForm ID="form" FormMode="Update" UpdateButtonCaption="Update Fault Class" runat="server" />
  </div>
</asp:Panel>