﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemFaultClassesAddTab.ascx.cs" Inherits="CW.Website._administration.fault.system.SystemFaultClassesAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="../../TabHeader.ascx"  %>
<%@ Register TagPrefix="CW" TagName="FaultClassForm" Src="../FaultClassForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Fault Class" />
<CW:FaultClassForm ID="form" runat="server" FormMode="Insert" UpdateButtonCaption="Add Fault Class" />