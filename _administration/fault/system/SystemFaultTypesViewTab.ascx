﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemFaultTypesViewTab.ascx.cs" Inherits="CW.Website._administration.fault.system.SystemFaultTypesViewTab" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="FaultTypeForm" src="../FaultTypeForm.ascx"  %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Fault Types" />

<asp:Label ID="lblResults" runat="server" Text="" />

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>                                                                                                    

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FaultTypeName" HeaderText="Fault Type Name">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<FaultType>(_ => _.FaultTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<FaultType>(ft=>ft.FaultTypeName), 30) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FaultTypeDescription" HeaderText="Fault Type Description">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<FaultType>(_ => _.FaultTypeDescription)) %>"><%# GetCellContent(Container, PropHelper.G<FaultType>(ft=>ft.FaultTypeDescription), 30) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FaultClassName" HeaderText="Fault Class Name">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<FaultClass>(_ => _.FaultClassName)) %>"><%# GetCellContent(Container, PropHelper.G<FaultClass>(fc=>fc.FaultClassName), 30) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this Fault Type permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>

<!--SELECT FAULT TYPE DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
  <Fields>
    <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
      <ItemTemplate>
        <div>
          <h2>Fault Type Details</h2>

          <ul class="detailsList">
            <CW:UListItem runat="server" Label="Fault Type Name" Value="<%# Eval(PropHelper.G<FaultType>(ft=>ft.FaultTypeName)) %>" />
            <CW:UListItem runat="server" Label="Fault Class Name" Value="<%# Eval(PropHelper.G<FaultType, FaultClass>(ft => ft.FaultClass, fc => fc.FaultClassName)) %>" />
          </ul>
        </div>
      </ItemTemplate> 
    </asp:TemplateField>
  </Fields>
</asp:DetailsView>

<!--EDIT FAULT TYPE PANEL -->
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">
  <div>
    <h2>Edit Fault Type</h2>
  </div>
  <div>
    <CW:FaultTypeForm ID="form" FormMode="Update" UpdateButtonCaption="Update Fault Type" runat="server" />
  </div>
</asp:Panel>