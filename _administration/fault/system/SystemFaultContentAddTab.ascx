﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemFaultContentAddTab.ascx.cs" Inherits="CW.Website._administration.fault.system.SystemFaultContentAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="../../TabHeader.ascx"  %>
<%@ Register TagPrefix="CW" TagName="FaultContentAddForm" Src="../FaultContentAddForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Fault Content" />
<CW:FaultContentAddForm ID="form" runat="server" FormMode="Insert" UpdateButtonCaption="Add Fault Content" />