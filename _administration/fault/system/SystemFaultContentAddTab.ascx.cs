﻿using System;
using System.Web.UI.WebControls;
using CW.Data;
using CW.Utility;

namespace CW.Website._administration.fault.system
{
    public partial class SystemFaultContentAddTab : AddTabBase<FaultContent, SystemFaultAdministration>
    {
        #region methods

            private void Page_Init()
            {
                CanEntityBeAdded = form.CanFaultContentBePersisted;
                AddEntityMethod = form.PersistEntityAction;
            }

        #endregion

        #region properties

            protected override String IdColumnName
            {
                get { return PropHelper.G<FaultContent>(f => f.ID); }
            }

            protected override String NameField
            {
                get { return PropHelper.G<FaultContent>(f => f.ContentName); }
            }

            protected override String EntityTypeName
            {
                get { return typeof(FaultContent).Name; }
            }

        #endregion
    }
}