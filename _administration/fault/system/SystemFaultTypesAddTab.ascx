﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemFaultTypesAddTab.ascx.cs" Inherits="CW.Website._administration.fault.system.SystemFaultTypesAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="../../TabHeader.ascx"  %>
<%@ Register TagPrefix="CW" TagName="FaultTypeForm" Src="../FaultTypeForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Fault Type" />
<CW:FaultTypeForm ID="form" runat="server" FormMode="Insert" UpdateButtonCaption="Add Fault Type" />