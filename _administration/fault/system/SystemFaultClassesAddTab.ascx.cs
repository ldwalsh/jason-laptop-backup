﻿using CW.Data;
using CW.Utility;
using System;

namespace CW.Website._administration.fault.system
{
    public partial class SystemFaultClassesAddTab : AddTabBase<FaultClass, SystemFaultAdministration>
    {
        #region methods

            private void Page_Init()
            {
                CanEntityBeAdded = form.CanFaultClassBePersisted;
                AddEntityMethod = form.PersistEntityAction;
            }

        #endregion

        #region properties

            protected override String IdColumnName
            {
                get { return PropHelper.G<FaultClass>(f => f.FaultClassID); }
            }

            protected override String NameField
            {
                get { return PropHelper.G<FaultClass>(f => f.FaultClassName); }
            }

            protected override String EntityTypeName
            {
                get { return typeof(FaultClass).Name; }
            }

        #endregion
    }
}