﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;

namespace CW.Website._administration.fault.system
{
    public partial class SystemFaultClassesViewTab : ViewTabWithFormBase<FaultClass, AdministrationPageBase>
    {
		#region methods

			protected void Page_Init(Object sender, EventArgs e)
			{
				CanEntityBeUpdated = form.CanEntityBePersisted;
				CanEntityBeDeleted = CanFaultClassBeDeleted;

				UpdateEntityMethod = form.PersistEntityAction;

				DeleteEntityMethod = DeleteFaultClass;
			}

			private Boolean CanFaultClassBeDeleted(String fcid, out String message)
			{
				var id = Convert.ToInt32(fcid);

				if (DataMgr.FaultDataMapper.DoesFaultItemExist<FaultType>((ft => ft.FaultClassID == id), (fc => fc)))
				{
					message = "Cannot delete a fault class which is associated with a fault type.";

					return false;
				}

				message = null;

				return true;
			}

			private void DeleteFaultClass(String fcid)
			{
				DataMgr.FaultDataMapper.DeleteFaultItem<FaultClass>((fc => fc), (fc => fc.FaultClassID == Convert.ToInt32(fcid)));

				page.TabStateMonitor.ChangeState();
			}

		#endregion

		#region properties

			protected override IEnumerable<String> GridColumnNames
			{
				get { return PropHelper.F<FaultClass>(_ => _.FaultClassName, _ => _.FaultClassDescription); }
			}
			
			protected override String IdColumnName
			{
				get { return PropHelper.G<FaultClass>(f => f.FaultClassID); }
			}

			protected override String NameField
			{
				get { return PropHelper.G<FaultClass>(f => f.FaultClassName); }
			}

			protected override String EntityTypeName
			{
				get { return "fault class"; } //typeof(FaultClass).Name;
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get { 
					return (searchText => DataMgr.FaultDataMapper.GetAllFaultSearchedItems<FaultClass>(searchText, (fc => fc), null, (fc => fc.FaultClassName), "FaultClassName"));
				}
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get { return (fcid => DataMgr.FaultDataMapper.GetFaultItem<FaultClass>((fc => fc.FaultClassID == Convert.ToInt32(fcid)), (fc => fc))); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get { return (fcid => DataMgr.FaultDataMapper.GetFaultItem<FaultClass>((fc => fc.FaultClassID == Convert.ToInt32(fcid)), (fc => fc))); }
			}

			protected override String CacheSubKey
			{
				get {return null;}
			}

		#endregion
    }
}