﻿using CW.Data;
using System;
using System.Web.UI.WebControls;

namespace CW.Website._administration.fault
{
    public partial class FaultClassForm : FormGenericBase<FaultClassForm, FaultClass>
    {
        #region methods

            public Boolean CanFaultClassBePersisted(FaultClass faultClass, out String message)
            {
                if (DataMgr.FaultDataMapper.DoesFaultItemExist<FaultClass>((fc => fc.FaultClassID != faultClass.FaultClassID && fc.FaultClassName == faultClass.FaultClassName), (fc => fc)))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

        #endregion

        #region properties

            #region overrides

                public override Delegates<FaultClass>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get { return CanFaultClassBePersisted; }
                }

                public override Action<FaultClass> PersistEntityAction
                {
                    get
                    {
                        switch (FormMode)
                        {
                            case PersistModeEnum.Insert:
                                {
                                    return DataMgr.FaultDataMapper.InsertFaultClass;
                                }
                            case PersistModeEnum.Update:
                                {
                                    return DataMgr.FaultDataMapper.UpdateFaultClass;
                                }
                        }

                        return null;
                    }
                }

            #endregion

        #endregion
    }
}