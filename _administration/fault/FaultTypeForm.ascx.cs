﻿using CW.Data;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace CW.Website._administration.fault
{
    public partial class FaultTypeForm : FormGenericBase<FaultTypeForm, FaultType>
    {
        #region methods

            #region overrides

                public override void PopulateForm(FaultType obj)
                {
                    base.PopulateForm(obj);
                }

            #endregion

            public Boolean CanFaultClassBePersisted(FaultType faultType, out String message)
            {
                if (DataMgr.FaultDataMapper.DoesFaultItemExist<FaultType>((ft => ft.FaultTypeID != faultType.FaultTypeID && ft.FaultTypeName == faultType.FaultTypeName), (ft => ft)))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

            private void Page_FirstInit()
            {
                tab.BindFaultClasses(ddlFaultClassID);
            }

            private void Page_Load()
            {
                page.TabStateMonitor.OnTabStateChanged += TabStateMonitor_OnTabStateChanged;
            }

            void TabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages)
            {
                tab.BindFaultClasses(ddlFaultClassID);
            }

        #endregion

        #region properties

            #region overrides

                public override Delegates<FaultType>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get { return CanFaultClassBePersisted; }
                }

                public override Action<FaultType> PersistEntityAction
                {
                    get
                    {
                        switch (FormMode)
                        {
                            case PersistModeEnum.Insert:
                                {
                                    return DataMgr.FaultDataMapper.InsertFaultType;
                                }
                            case PersistModeEnum.Update:
                                {
                                    return DataMgr.FaultDataMapper.UpdateFaultType;
                                }
                        }

                        return null;
                    }
                }

            #endregion

        #endregion
    }
}