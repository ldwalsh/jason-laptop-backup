﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="FaultContentAddForm.ascx.cs" Inherits="CW.Website._administration.fault.FaultContentAddForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:HiddenField ID="hdnID" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">*Fault Content Name:</label>
  <asp:TextBox ID="txtContentName" CssClass="textbox" MaxLength="50" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Fault Type:</label>
  <asp:DropDownList ID="ddlFaultTypeID" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Fault ID:</label>
  <asp:TextBox ID="txtFaultID" CssClass="textbox" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Content:</label>
  <telerik:RadEditor ID="radContent" runat="server" Height="300px" Width="450px" NewLineMode="Div" ToolsWidth="450px" ToolbarMode="ShowOnFocus" ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" EnableResize="false">
    <CssFiles>
      <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
    </CssFiles>
  </telerik:RadEditor> 
</div>

<div class="divForm">
<label class="label">*Culture:</label>
<asp:DropDownList ID="ddlCultureName" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<br />

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="faultContentNameRFV" runat="server" ErrorMessage="Fault Content Name is a required field." ControlToValidate="txtContentName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultContentNameVCE" runat="server" TargetControlID="faultContentNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="faultTypeIDRFV" runat="server" ErrorMessage="Fault Type is a required field." ControlToValidate="ddlFaultTypeID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultTypeIDVCE" runat="server" TargetControlID="faultTypeIDRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="faultIDRFV" runat="server" ErrorMessage="Fault ID is a required field." ControlToValidate="txtFaultID" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultIDVCE" runat="server" TargetControlID="faultIDRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RegularExpressionValidator ID="faultIDREV" runat="server" ErrorMessage="Invalid Fault ID Format." ValidationExpression="^\d+$" ControlToValidate="txtFaultID" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultIDREVVCE" runat="server" TargetControlID="faultIDREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="faultContentRFV" runat="server" ErrorMessage="Fault Content is a required field." ControlToValidate="radContent" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultContentVCE" runat="server" TargetControlID="faultContentRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="cultureNameRFV" runat="server" ErrorMessage="Culture is a required field." ControlToValidate="ddlCultureName" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="cultureNameVCE" runat="server" TargetControlID="cultureNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />