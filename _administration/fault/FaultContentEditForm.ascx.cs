﻿using System;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using System.Collections.Generic;

namespace CW.Website._administration.fault
{
    public partial class FaultContentEditForm : FormGenericBase<FaultContentEditForm, FaultContent>
    {
        #region methods

            public Boolean CanFaultContentBePersisted(FaultContent faultContent, out String message)
            {
                if (DataMgr.FaultDataMapper.DoesFaultItemExist<FaultContent>((fc => fc.ID != faultContent.ID && fc.FaultID == faultContent.FaultID && fc.CultureName == faultContent.CultureName), (fc => fc)))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

            private void Page_FirstInit()
            {
                tab.BindFaultTypes(ddlFaultTypeID);
            }

            private void Page_Load()
            {
                page.TabStateMonitor.OnTabStateChanged += TabStateMonitor_OnTabStateChanged;
            }

            void TabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages)
            {
                tab.BindFaultTypes(ddlFaultTypeID);
            }

        #endregion

        #region properties

            public String ComparisonContent { set { litComparisonContent.Text = value; } }

            #region overrides

                public override Delegates<FaultContent>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get { return CanFaultContentBePersisted; }
                }

                public override Action<FaultContent> PersistEntityAction
                {
                    get { return DataMgr.FaultDataMapper.UpdateFaultContent; }
                }

            #endregion

        #endregion
    }
}