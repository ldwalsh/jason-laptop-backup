﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="FaultTypeForm.ascx.cs" Inherits="CW.Website._administration.fault.FaultTypeForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:HiddenField ID="hdnFaultTypeID" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">*Fault Type Name:</label>
  <asp:TextBox ID="txtFaultTypeName" CssClass="textbox" MaxLength="50" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Fault Type Description:</label>
  <asp:TextBox ID="txtFaultTypeDescription" CssClass="textbox" MaxLength="50" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Fault Class:</label>
  <asp:DropDownList ID="ddlFaultClassID" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="faultTypeNameRFV" runat="server" ErrorMessage="Fault Type Name is a required field." ControlToValidate="txtFaultTypeName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultTypeNameVCE" runat="server" TargetControlID="faultTypeNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="faultTypeDescriptionRFV" runat="server" ErrorMessage="Fault Type Description is a required field." ControlToValidate="txtFaultTypeDescription" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultTypeDescriptionVCE" runat="server" TargetControlID="faultTypeDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="faultClassIDRFV" runat="server" ErrorMessage="Fault Class is a required field." ControlToValidate="ddlFaultClassID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="faultClassIDVCE" runat="server" TargetControlID="faultClassIDRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />