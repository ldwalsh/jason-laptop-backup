﻿using CW.Utility;
using System;
using System.Web.UI.WebControls;
namespace CW.Website._administration
{
	public abstract class ViewTabWithFormBase<TKGSEntity,TAdministrationPage>: ViewTabBase<TKGSEntity,TAdministrationPage> where TKGSEntity: new() where TAdministrationPage: AdministrationPageBaseBase
	{
		#region method
			
			#region virtual

				protected virtual void ClearEditForm()
				{
					CallFormMethod("ClearForm");
				}

				protected virtual void PopulateEditForm(TKGSEntity obj)
				{
					ClearEditForm();

					CallFormMethod("PopulateForm", obj);
				}

				protected virtual void EditEntity(TKGSEntity obj)
				{
 					PopulateEditForm(obj);
				}

			#endregion

			private void Page_Init()
			{
				grid.RowEditing += GridEditing;
			}

			private void Page_FirstInit()
			{
				if (PanelEditEntity  == null) throw new InvalidOperationException("A Panel object (PanelEditEntity) could not be found.");
			}

			protected void GridEditing(Object sender, GridViewEditEventArgs e)
			{
				EntityDetailView.Visible = false;
				PanelEditEntity.Visible  = true;

				EditEntity(GetEntityForEditing(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_=>_.ID)].ToString()));

				//Cancels the edit auto grid selected.
				e.Cancel = true;
			}

		#endregion

		#region property

			protected abstract GetEntityDelegate GetEntityForEditing    {get;}

		#endregion
	}
}