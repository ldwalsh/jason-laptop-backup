﻿using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._administration
{
	public class FormBase: SiteUserControl
    {
        #region STATIC

            protected void GenericListControlBind(ListControl control, Object items, String textFieldName, String valueFieldName, IEnumerable<ListItem> defaultItems=null)
            {
                control.Items.Clear();

                if (defaultItems != null)
                {
                    Array.ForEach(defaultItems.ToArray(), (_) => { control.Items.Add(_); });
                }

                control.DataSource = items;
                control.DataTextField = textFieldName;
                control.DataValueField = valueFieldName;

                control.DataBind();
            }

        #endregion

        #region event

            public event Action OnUpdate;

		#endregion

		#region field

			private IButtonControl updateButton;

		#endregion

		#region method

			protected void FireOnUpdate()
			{
                if (OnUpdate == null) return;

				OnUpdate();
			}

		#endregion

		#region property

			public virtual IButtonControl UpdateButton
			{
				get
				{
					if (updateButton != null) return updateButton; //cause extrenous execution when updateButton not found

					foreach (var n in new[]{"updateButton", "btnUpdate",})
					{
						var b = FindControl(n) as IButtonControl;

						if (b != null) return updateButton = b;
					}

					return updateButton = null;
				}
			}

		#endregion
	}
}