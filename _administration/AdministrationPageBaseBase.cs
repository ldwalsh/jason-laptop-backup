﻿using CW.Website._framework;
using System;
using Telerik.Web.UI;

namespace CW.Website._administration
{
    public abstract class AdministrationPageBaseBase: SitePage, IAdminPage
    {
		#region CONST

			private const String OVERRIDE_MESSAGE = "The {0} property must be overridden or the ViewBase.{0} must be overridden";

		#endregion

		#region field

			protected RadTabStrip tabContainer;

		#endregion

		#region method

			private void Page_Init()
			{
				TabStateMonitor = new TabStateMonitor(this, tabContainer);
			}

            public TabStateMonitor GetTabStateMonitor()
            {
                return TabStateMonitor;
            }

		#endregion

		#region property

			#region abstract

				public abstract String IdentifierField
				{
					get; // {throw new Exception(String.Format(OVERRIDE_MESSAGE, "IdentifierField"));}
				}

				public abstract String NameField
				{
					get; // {throw new Exception(String.Format(OVERRIDE_MESSAGE, "NameField"));}
				}

			#endregion

			#region virtual

				public virtual String EntityTypeName
				{
					get {return null;}
				}

			#endregion

			public TabStateMonitor TabStateMonitor
			{
				private set;
				get;
			}

            public string GetIdentifierField()
            {
                return IdentifierField;
            }

            public string GetNameField()
            {
                return NameField;
            }

        #endregion
    }
}