﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ReportAssignmentCriteria.ascx.cs" Inherits="CW.Website._administration.ReportAssignmentCriteria" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Import Namespace="CW.Website._framework.httpmodules" %>

<asp:HiddenField ID="hdnCID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnBID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnProviderID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnReportTypeID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnUserGroupAccessLevelID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnCurrentUID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnAnalysisRange" runat="server" Visible="false" />
<asp:HiddenField ID="hdnGUID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnUserGroupIDs" runat="server" Visible="false" />
<div class="divForm">
    <label runat="server" id="htmlblReport" class="label"></label>
    <asp:CheckBox ID="chkIsActive" CssClass="checkbox" OnCheckedChanged="chkIsActive_OnCheckedChanged" AutoPostBack="true" runat="server" />                                                        
</div> 
<div id="divReportAssignment" class="divForm" visible="false" runat="server">     
    <div>                                                                                                                                                              
        <label class="label">Available User Groups:</label> 
        <asp:ListBox ID="lbUserGroupsTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
        </asp:ListBox>
        <div class="divArrows">
        <asp:ImageButton CssClass="up-arrow"  ID="btnUp"  OnClick="btnUserGroupsUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
        <asp:ImageButton CssClass="down-arrow" ID="btnDown"  OnClick="btnUserGroupsDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
        </div>
        <label class="label">Assigned User Groups:</label>
        <asp:ListBox ID="lbUserGroupsBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
        </asp:ListBox> 
    </div>   
    <a target="_blank" href="/<%=PathRewriteModule.KGSUserGroupAdministration%>">Create User Groups</a>
</div>
<div id="divSendTestReport" class="divForm" visible="false" runat="server">
    <asp:Button ID="btnSendTestReport" Text="Send Test Report To Me" runat="server" OnClick="btnSendTestReport_Click" style="height: 26px; margin-right: 10px;" /><asp:Label ID="lblSendTestMessage" CssClass="errorMessage" runat="server" />
</div>
<div id="divSendUserGroupReport" class="divForm" visible="false" runat="server">
    <asp:Button ID="btnSendLastReportToUserGroups" Text="Send Last Report To Assigned User Groups" runat="server" OnClick="btnSendLastReportToUserGroups_Click" style="height: 26px; margin-right: 10px;" /><asp:Label ID="lblSendUserGroupMessage" CssClass="errorMessage" runat="server" />
</div>