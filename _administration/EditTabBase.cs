﻿using CW.Common.Constants;
using CW.Utility;
using System;
using System.Collections.Generic;

namespace CW.Website._administration
{
    public abstract class EditTabBase<TKGSEntity,TAdministrationPage>: TabGenericBase<TKGSEntity>, TabStateMonitor.IReactive where TKGSEntity: new() where TAdministrationPage: AdministrationPageBase
    {
		#region field

			#region readonly

				protected readonly AdministrationControl<TAdministrationPage> administrationControl;

			#endregion

			protected Delegates<TKGSEntity>.CanEntityBePersistedDelegate CanEntityBeUpdated;

			protected Action<TKGSEntity> UpdateEntityAction;

		#endregion

		#region constructor

			protected EditTabBase()
			{
				administrationControl = new AdministrationControl<TAdministrationPage>(this);
			}

		#endregion

		#region method

			#region virtual

				protected virtual void ClearForm()
				{
					CallFormMethod("ClearForm");
				}

				protected virtual void PopulateForm(TKGSEntity obj)
				{
					//CallFormMethod("ClearForm"); //need to get this working?

					CallFormMethod("PopulateForm", obj);
				}

				protected virtual void CreateEntity(TKGSEntity obj)
				{
					CallFormMethod("CreateEntity", obj);
				}

				protected virtual void PersistEntity(TKGSEntity obj)
				{
					ValidateDelegate(PropHelper.G<EditTabBase<TKGSEntity,TAdministrationPage>>(_=>_.CanEntityBeUpdated));
					ValidateDelegate(PropHelper.G<EditTabBase<TKGSEntity,TAdministrationPage>>(_=>_.UpdateEntityAction));

					//

					String message;

					if (!CanEntityBeUpdated(obj, out message))
					{
						if (String.IsNullOrWhiteSpace(message))
						{
							message = "{EntityTypeName} could not be updated.";
						}

						DisplayMessage(message);

						return;
					}

					try
					{
						UpdateEntityAction(obj);

						message = "{EntityTypeName} update was successful.";
					}
					catch (Exception ex)
					{
						LogMgr.Log(DataConstants.LogLevel.ERROR, InsertEntityTypeName("Error updating {EntityTypeName}."), ex);
                        
						message = "{EntityTypeName} update failed. Please contact an administrator.";
					}

					DisplayMessage(message);

					page.TabStateMonitor.ChangeState(EditChangeStateEnums);
				}

			#endregion

			#region override

				protected override void UpdateButton_Click(Object sender, EventArgs e)
				{
					var obj = new TKGSEntity();

					CreateEntity (obj);				
					PersistEntity(obj);
				}

			#endregion

			private void Page_FirstLoad()
			{
				if (ManualBind) return;

				PopulateForm(GetEntityForEditing);
			}

		#endregion

		#region property

			#region abstract

				protected abstract TKGSEntity GetEntityForEditing {get;}

			#endregion

			public TAdministrationPage page //should be in TabBase already
			{
				get {return administrationControl.page;}
			}

			public Boolean ManualBind
			{
				get;
				set;
			}

			protected virtual IEnumerable<Enum> EditChangeStateEnums
			{
				get {return new Enum[]{StateChangeEnum.EditEntity,};}
			}

		#endregion
    }
}