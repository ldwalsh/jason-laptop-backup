﻿using CW.Utility;
using System;

namespace CW.Website._administration
{
	public abstract class TabGenericBase<TKGSEntity>: TabBase where TKGSEntity: new()
	{
		#region method

			private void Page_Load()
			{
				//if (UpdateButton != null) {UpdateButton.Click += UpdateButton_Click;} //should be moved to init but needs to run before FormBase click handler

				DisplayMessage(null); //TODO: remove... to be handled by JS //removes message when navigating tabs
			}

            protected String InsertEntityTypeName(String message)
            {
                return message.Replace("{EntityTypeName}", EntityTypeName);
            }

			protected String InsertEntityTypeName(String message, Boolean isSingular)
			{
                var formattedEntityTypeName = LanguageHelper.GetFormattedName(EntityTypeName, isSingular);

                return message.Replace("{EntityTypeName}", formattedEntityTypeName);
			}

			protected void DisplayMessage(String message)
			{
				if (tabHeader == null) throw new Exception(String.Format("A TabHeader control with id=\"tabHeader\" must exist on the tab {0}.", GetType().BaseType.Name));

				tabHeader.Message = String.IsNullOrWhiteSpace(message) ? String.Empty : InsertEntityTypeName(message);
			}
					
		#endregion

		#region property

			protected virtual String EntityTypeName
			{
				get {return (((AdministrationPageBaseBase)page).EntityTypeName ?? typeof(TKGSEntity).Name);}
			}

		#endregion
	}
}