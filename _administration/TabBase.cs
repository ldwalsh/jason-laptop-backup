﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._controls.admin;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._administration
{
    public abstract class TabBase: SiteUserControl, TabStateMonitor.IReactive
    {
		#region event

			public event Action<IEnumerable<Enum>> OnTabStateChanged;

		#endregion

		#region field

			protected TabHeader tabHeader;

			private FormBase form;

		#endregion

		#region method

			#region abstract

				protected abstract void UpdateButton_Click(Object sender, EventArgs e);

			#endregion

			#region virtual

				public virtual String GetEntityName(Object KGSEntity)
				{
					return (String)KGSEntity.GetType().GetProperty(NameField).GetValue(KGSEntity, null);
				}

			#endregion

			private void Page_Init()
			{
				page.Init += ((Object sender, EventArgs e) => {((IAdminPage)page).GetTabStateMonitor().OnTabStateChanged += tabStateMonitor_OnTabStateChanged;});
			}

            private void Page_Load() { if (UpdateButton != null) { UpdateButton.Click += UpdateButton_Click; } }
        
            private FormBase GetForm()
			{
				if (form != null) return form;

                return form = ControlHelper.GetControlByType<FormBase>(this, true, null);
			}

			protected void CallFormMethod(String methodName, Object arg) //revise this approach use delegates instead
			{
				CallFormMethod(methodName, new Object[]{arg});
			}

			protected void CallFormMethod(String methodName, Object[] args = null) //revise this approach use delegates instead
			{
				var f = GetForm();
	
				if (f == null) return;

				var mi = f.GetType().GetMethod(methodName);

				if (mi == null) throw new Exception(String.Format("The virtual ViewTabBase.{0} method could not find a Form.{0} method on the form.", new[]{methodName}));

				try
				{
					mi.Invoke(f, args);
				}
				catch (Exception e)
				{
					throw;
				}
			}

			private void tabStateMonitor_OnTabStateChanged(TabBase tabBase, IEnumerable<Enum> messages)
			{
				if ((OnTabStateChanged == null) || (tabBase != this)) return;

				OnTabStateChanged(messages);
			}

			[
			DebuggerStepThrough
			]
			protected void ValidateDelegate(String name)
			{
				if (ReflectionHelper.ValidateMember(this, name)) return;

				throw new
				InvalidOperationException
				(
					String.Format
					(
						"The delete field '{0}' was not set by your sub-class. " +
						"It is required by the {1} method. You may choose to avoid setting it by overriding the {1} method in your sub-class.",
						name,
						new StackFrame(1).GetMethod().Name
					)
				);
			}

			#region method: binders

            internal void BindBuildings(DropDownList ddl)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "BuildingName";
                ddl.DataValueField = "BID";
                ddl.DataSource = siteUser.VisibleBuildings;
                ddl.DataBind();
            }

			internal void BindCountries(DropDownList ddl)
			{
				ddl.DataTextField = "CountryName";
				ddl.DataValueField = "Alpha2Code";
				ddl.DataSource = DataMgr.CountryDataMapper.GetAllCountries();
				ddl.DataBind();
			}

			internal void BindStates(DropDownList ddl, string alpha2Code)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "StateName";
				ddl.DataValueField = "StateID";
				ddl.DataSource = DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code);
				ddl.DataBind();
			}

			internal void BindOrganizations(DropDownList ddl)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "OrganizationName";
				ddl.DataValueField = "OID";
				ddl.DataSource = DataMgr.OrganizationDataMapper.GetAllOrganizations();
				ddl.DataBind();
			}

			internal void BindRoles(DropDownList ddl, IEnumerable<Data.UserRole> roles)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "RoleName";
				ddl.DataValueField = "RoleID";
				ddl.DataSource = roles;
				ddl.DataBind();
			}

			internal void BindOrganizationsWithProviders(DropDownList ddl)
			{
				ddl.DataTextField = "OrganizationName";
				ddl.DataValueField = "OID";
				ddl.DataSource = DataMgr.OrganizationDataMapper.GetOrganizationsWithProviders();
				ddl.DataBind();
			}

			internal void BindProvidersByOID(int oid, DropDownList ddl)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "ProviderName";
				ddl.DataValueField = "ProviderID";
				ddl.DataSource = DataMgr.ProviderDataMapper.GetAllProvidersByOrg(oid);
				ddl.DataBind();
			}

			internal void BindProvidersByClient(int cid, DropDownList ddl, string textOveride = null)
			{
				ListItem lItem = new ListItem();
				lItem.Text = String.IsNullOrEmpty(textOveride) ? "Select one..." : textOveride;
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "ProviderName";
				ddl.DataValueField = "ProviderID";
				ddl.DataSource = DataMgr.ProviderClientDataMapper.GetFullProvidersClientsListFlattenedByCID(cid);
				ddl.DataBind();
			}

			internal void BindYears(DropDownList ddl, int yearCount = 4, ListItemCollection customItems = null)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
                
				if(customItems != null)
				{
					foreach(ListItem li in customItems)
					{
						ddl.Items.Add(li);
					}
				}

				var years = new List<int>(); 
				int counter = 0;
				var year = DateTime.UtcNow.Year;
				while(counter < yearCount)
				{
					years.Add(year-counter);
					counter++;
				}

				ddl.DataSource = years;
				ddl.DataBind();
			}


			internal void BindClients(DropDownList ddl, ListItemCollection customItems = null)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
                
				if(customItems != null)
				{
					foreach(ListItem li in customItems)
					{
						ddl.Items.Add(li);
					}
				}

				ddl.DataTextField = "ClientName";
				ddl.DataValueField = "CID";
				ddl.DataSource = DataMgr.ClientDataMapper.GetAllActiveClients();
				ddl.DataBind();
			}

			internal void BindClientsExcludingOrgClientsAndExistingProviderClientAssocations(int oid, int providerID, DropDownList ddl)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "ClientName";
				ddl.DataValueField = "CID";
				ddl.DataSource = DataMgr.ClientDataMapper.GetAllActiveClientsExcludingOrgClientsAndExistingProviderClientAssocations(oid, providerID);
				ddl.DataBind();
			}

			internal void BindNonKGSRoles(DropDownList ddl)
			{
				var greaterThanRoleID = Convert.ToInt32(BusinessConstants.UserRole.UserRoleEnum.KGSRestrictedAdmin);
				var dataSource = RoleHelper.FilterRestrictedRoles(DataMgr.UserRoleDataMapper.GetAllRolesGreaterThan(greaterThanRoleID));

				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "RoleName";
				ddl.DataValueField = "RoleID";
				ddl.DataSource = dataSource;
				ddl.DataBind();
			}

			internal void BindAllNonRestrictedAdminRoles(DropDownList ddl)
			{
				var dataSource = RoleHelper
					.FilterRestrictedRoles(
					DataMgr.UserRoleDataMapper.GetAllRoles());

				dataSource = RoleHelper.FilterUserRoles(dataSource);

				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "RoleName";
				ddl.DataValueField = "RoleID";
				ddl.DataSource = dataSource;
				ddl.DataBind();
			}

			internal void BindUserGroupsByOrganization(int oid, DropDownList ddl)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "UserGroupName";
				ddl.DataValueField = "UserGroupID";

				ddl.DataSource = DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(oid, null);
				ddl.DataBind();
			}

			internal void BindFaultClasses(DropDownList ddl)
			{
				ClearItemsAndAddInitialItem(ddl);

				ddl.DataTextField = "FaultClassName";
				ddl.DataValueField = "FaultClassID";
				ddl.DataSource = DataMgr.FaultDataMapper.GetAllFaultItems<FaultClass>((fc => fc), null, (fc => fc.FaultClassName));
				ddl.DataBind();
			}

			internal void BindFaultTypes(DropDownList ddl)
			{
				ClearItemsAndAddInitialItem(ddl);

				ddl.DataTextField = "FaultTypeName";
				ddl.DataValueField = "FaultTypeID";
				ddl.DataSource = DataMgr.FaultDataMapper.GetAllFaultItems<FaultType>((fc => fc), null, (fc => fc.FaultTypeName));
				ddl.DataBind();
			}

			internal void BindContentSections(DropDownList ddl, Boolean isAddOrEditForm = false)
			{
				if (isAddOrEditForm) ClearItemsAndAddInitialItem(ddl);

				ddl.DataTextField = "ContentSectionName";
				ddl.DataValueField = "ContentSectionID";
				ddl.DataSource = DataMgr.ContentDataMapper.GetAllContentItems<ContentSection>((cs => cs), null, (cs => cs.ContentSectionName));
				ddl.DataBind();
			}

			internal void BindCultures(DropDownList ddl, Boolean isAddForm = false)
			{
				var cultureItems = CultureHelper.AllowedContentCultures(CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.DisplayName).ToArray());

				if (isAddForm) ClearItemsAndAddInitialItem(ddl);

				ddl.DataTextField = "DisplayName";
				ddl.DataValueField = "Name";
				ddl.DataSource = cultureItems.Select(c => new { DisplayName = c.DisplayName, Name = c.Name });
				ddl.SelectedValue = (isAddForm) ? "-1" : BusinessConstants.Culture.DefaultCultureName;
				ddl.DataBind();
			}

			internal void BindQuickLinks(DropDownList ddl)
			{
				ClearItemsAndAddInitialItem(ddl);

				ddl.DataTextField = "QuickLinkName";
				ddl.DataValueField = "QuickLinkID";
				ddl.DataSource = DataMgr.QuickLinksDataMapper.GetAllQuickLinks();
				ddl.DataBind();
			}

			internal void BindContentFormats(DropDownList ddl, Boolean useViewAllText = false)
			{
				ClearItemsAndAddInitialItem(ddl, useViewAllText);

				ddl.DataSource = BusinessConstants.Content.ContentFormats;
				ddl.DataBind();

			}
			internal void BindContentTypes(DropDownList ddl, Boolean useViewAllText = false)
			{
				ClearItemsAndAddInitialItem(ddl, useViewAllText);

				ddl.DataSource = BusinessConstants.Content.ContentTypes;
				ddl.DataBind();
			}

			internal void BindGlobalFileTypes(DropDownList ddl)
			{
				ClearItemsAndAddInitialItem(ddl);

				ddl.DataTextField = "GlobalFileTypeName";
				ddl.DataValueField = "GlobalFileTypeID";
				ddl.DataSource = DataMgr.GlobalFileDataMapper.GetAllGlobalFileItems<GlobalFileType>((gft => gft), null, (gft => gft.GlobalFileTypeName));
				ddl.DataBind();
			}

			internal void BindProviders(DropDownList ddl)
			{
				ClearItemsAndAddInitialItem(ddl, true);

				ddl.DataTextField = "ProviderName";
				ddl.DataValueField = "ProviderID";
				ddl.DataSource = DataMgr.ProviderDataMapper.GetAllProviders();
				ddl.DataBind();
			}

            internal void BindStorageAccounts(DropDownList ddl, StorageAccountCriteria criteria, Boolean includeSelectOneItem = false)
            {
                if (includeSelectOneItem)
                {
                    ListItem lItem = new ListItem();
                    lItem.Text = "Select one...";
                    lItem.Value = "-1";
                    lItem.Selected = true;

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);
                }
                ddl.DataTextField = "DisplayName";
                ddl.DataValueField = "StorageAccountID";
                ddl.DataSource = DataMgr.StorageAccountDataMapper.GetStorageAccounts(criteria.CWEnvironment, criteria.IsCommon, criteria.StorageAcctLevel, true, true);
                ddl.DataBind();
            }

			private void ClearItemsAndAddInitialItem(DropDownList ddl, Boolean useViewAllText = false)
			{
				var lItem = new ListItem();

				lItem.Text = (useViewAllText) ? "View All..." : "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
			}

			#endregion

		#endregion

		#region property

			#region virtual

				protected virtual String IdColumnName
				{
					get { return ((IAdminPage)page).GetIdentifierField();}
				}

				protected virtual String NameField
				{
					get { return ((IAdminPage)page).GetNameField(); }
				}

				protected virtual IButtonControl UpdateButton //this method needs optimization
				{
					get
					{
						var f = GetForm();

						IButtonControl b = null;
						
						if (f != null) {b = f.UpdateButton;};

						if (b != null) return b;

						foreach (var n in new[]{"updateButton", "btnUpdate",})
						{
							b = FindControl(n) as IButtonControl;

							if (b != null) return b;
						}

						return null;
					}
				}

			#endregion

			public Page page
			{
				[
				DebuggerStepThrough()
				]							
				get
                {
                    return Page;
                    //if (typeof() == typeof(AdministrationPageBaseBase))
                    //{
                    //    return (AdministrationPageBaseBase)Page;
                    //}
                    //else
                    //{
                    //    return (AdminSitePageTemp)Page;
                    //}
                }
			}

			public virtual IEnumerable<Enum> ReactToChangeStateList
			{
				get {return null;}
			}

		#endregion
    }
}