﻿using CW.Utility;
using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._administration
{
	public abstract class FormGenericBase<TForm,TKGSEntity>: FormBase where TKGSEntity: new()
	{
		#region STATIC

			#region field

				protected static readonly IDictionary<String,MemberInfo> ControlStateModifiers; //move to a non-generic base

				private static readonly Random random;
				private static readonly Type NullableDateTimeType;
                private static readonly Type NullableIntType;

			#endregion
			
			#region constructor

				static FormGenericBase()
				{
					NullableDateTimeType = typeof(DateTime?);
                    NullableIntType = typeof(Int32?);

					//
					//searches for fields/properties that are "modifiers" for states of visible and enable

					ControlStateModifiers = new Dictionary<String,MemberInfo>();

					const BindingFlags bindingFlags = (BindingFlags.Instance|BindingFlags.Public);

					var set = new HashSet<MemberTypes>{MemberTypes.Field, MemberTypes.Property};

					var type = typeof(TForm);

					var typeBoolean = typeof(Boolean);

					foreach (var mi in type.GetMembers(bindingFlags))
					{
						if (!set.Contains(mi.MemberType)) continue;

						if (!mi.Name.EndsWith(ControlModifierEnum.Enabled.ToString()) && !mi.Name.EndsWith(ControlModifierEnum.Visible.ToString())) continue;

						if (mi.Name == "Visible") continue;

						MemberInfo memberInfo = null;                        

						switch (mi.MemberType)
						{
							case MemberTypes.Field:
							{
								var fi = type.GetField(mi.Name, bindingFlags);

								if (fi.FieldType != typeBoolean) continue;

								memberInfo = fi;
								
								break;
							}
							case MemberTypes.Property:
							{
								var pi = type.GetProperty(mi.Name, bindingFlags);

								memberInfo = pi;

								if (pi.PropertyType != typeBoolean) continue;

								break;
							}
						}

						ControlStateModifiers.Add(mi.Name, memberInfo);
					}

					//
					//random seed created for creating unique validation groups for multiple instances for the form

					random = new Random();
				}

			#endregion

			#region method

				private static void DisableControl(Control control)
				{
					if (control is HtmlControl)
					{
						((HtmlControl)control).Disabled = true;
					}
                    else if (control is WebControl)
					{
						((WebControl)control).Enabled = false;
					}
				}

			#endregion

		#endregion

		#region DELEGATE

			protected delegate void OnFieldBindDelegate(Control control, ref Object value);

		#endregion

		#region field

			#region readonly

				private readonly Type KGSEntityType;

				protected readonly IDictionary<String,Func<TKGSEntity,Object>> LoadHandlers;
				protected readonly IDictionary<String,Action<TKGSEntity>>      SaveHandlers;

			#endregion

			private TabBase _tab;

			private DynamicControlTracker dct;

			protected IList<Control> FormControls;

			protected OnFieldBindDelegate OnFieldBind;
			protected OnFieldBindDelegate OnFieldSave;

		#endregion

		#region constructor

			protected FormGenericBase()
			{
				KGSEntityType = typeof(TKGSEntity);

				ControlValueToPropertyMap = new Dictionary<Type,ControlValueToPropertyMapper.PropertyAccessor>(ControlValueToPropertyMapper.Maps);

				LoadHandlers = new Dictionary<String,Func<TKGSEntity,Object>>();
				SaveHandlers = new Dictionary<String,Action<TKGSEntity>>();
			}

		#endregion

		#region method

			#region virtual

				protected virtual void ConfigureForm()
				{
					if (FormMode != PersistModeEnum.Insert) return;

					FormControls.ToList().ForEach(_=> SetControlState(_));
				}

				protected virtual void CaptureDefaultValues()
				{
					var defaultControlValues = new Dictionary<String,String>();

					FormControls.ToList().ForEach(_=> defaultControlValues.Add(_.UniqueID, ControlValueToPropertyMap[_.GetType()].Get(_).ToString()));

					DefaultControlValues = new Dictionary<String,String>(defaultControlValues);
				}

				public virtual void PopulateForm(TKGSEntity obj)
				{
					if (FormMode == PersistModeEnum.Insert) throw new InvalidOperationException("PopulateForm should only be invoked when the FormMode is 'Update'.");

					if (obj == null) throw new InvalidOperationException(String.Format("The provided '{0}' cannot be null.", KGSEntityType.Name));

					//

					dct.Clear();

					//

					foreach (var control in FormControls)
					{
						var memberName = control.ID.TrimControlPrefix();

						//

						Func<TKGSEntity,Object> loadHandler;

						LoadHandlers.TryGetValue(control.ID, out loadHandler);
						
						Object value = null;

						if (loadHandler == null)
						{
							//
							//find a property of the object that corresponds to the control's ID

							var pi = KGSEntityType.GetProperty(memberName) ?? KGSEntityType.GetProperty(KGSEntityType.Name + memberName);

							if (pi != null)
							{
								value = pi.GetValue(obj, null);
							}
						}
						else
						{
							value = loadHandler(obj);
						}

						if (OnFieldBind != null)
						{
							OnFieldBind(control, ref value);
						}

                        var controlType = control.GetType();

						//try to set even if null, as some values are stored as and allow nulls //this needs to be revised! no need for try/catch here at all
						try{
							ControlValueToPropertyMap[controlType].Set(control, value);
						}
						catch{
						}

						//

						if (control is HiddenField) continue;

						if (!GetModifierValue(memberName, ControlModifierEnum.Enabled))
						{
							viewState.Set(control.ID, ControlValueToPropertyMap[controlType].Get(control));

							DisableControl(control);

							//
							//create label placeholder for control that was disabled

                            if (ControlHelper.Is<DropDownList,TextBox,RadMaskedTextBox>(control))
							{
								control.Visible = false;

								var labelText =
                                    ((control is TextBox) ? ((TextBox)control).Text : ((control is DropDownList) ? ((DropDownList)control).SelectedItem.Text : ((RadMaskedTextBox)control).Text));

								dct.AddControl(new Label{Text=labelText, CssClass="labelContent"}, control);
							}
						}

						if (!GetModifierValue(memberName, ControlModifierEnum.Visible))
						{
							//this functionality will be replaced with JS
								
							if (control.Parent == this)
							{
								throw new InvalidOperationException
								(
									String.Format("The field '{0}' must have an enclosing parent div with runat='server' containing the label. The div is hidden for the purpose of hiding the label.", control.ID)
								);
							}

							control.Parent.Visible = false;
						};
					}
				}

				public virtual void CreateEntity(TKGSEntity obj)
				{
					foreach (var control in FormControls)
					{
						if (control is Label) continue; //Labels are skipped as they are not used for data-entry

						//
						
						Action<TKGSEntity> saveHandler;

						SaveHandlers.TryGetValue(control.ID, out saveHandler);

						if (saveHandler != null)
						{
							saveHandler(obj);

							continue;
						}

						//

						var memberName = control.ID.TrimControlPrefix();

						//
						//find a property of the object that corresponds to the control's ID
						
						var pi = KGSEntityType.GetProperty(memberName);
				
						if (pi == null)
						{
							//preprend the KGSEntity name to field and try to GetProperty

							memberName = (KGSEntityType.Name + memberName);

							pi = KGSEntityType.GetProperty(memberName);
						}

						if (pi == null) continue;

						//

						Object value;

						//
						//if the control is disabled than the value used for persistance is pulled from ViewState
						//ignore check if it is a Hidden Field that has Visible property set to false
						if (!GetModifierValue(control.ID.TrimControlPrefix(), ControlModifierEnum.Enabled) && (!(control is HiddenField) && !control.Visible))
						{
							value = viewState.Get<Object>(control.ID);
						}
						else
						{
							if (control is HiddenField)
							{
								var hf = (HiddenField)control;

								value = ((control.ID.ToLower().EndsWith("id") && (hf.Value == String.Empty)) ? '0'.ToString() : hf.Value); //populate id field with default 0 since object value was empty
							}
							else
							{
								value = ControlValueToPropertyMap[control.GetType()].Get(control);

								if (control is DropDownList && (value.ToString() == "-1"))
								{
									value = null;
								}
                                if (control is RadEditor) value = StringHelper.ClearRadEditorFirefoxBR(value.ToString());
							}
						}
							
						//                        
						
						if (OnFieldSave != null)
						{
							OnFieldSave(control, ref value);
						}

						if (value == null) continue;

						SetEntityPropertyValue(obj, pi, value);
					}
				}

				public virtual void ClearForm()
				{
					foreach (var kvp in DefaultControlValues)
					{
                        var control = ControlHelper.FindControlByUniqueID(this, kvp.Key);

                        var dropDown = control as DropDownList;
                        if (dropDown != null)
                        {
                            var def = dropDown.Items.FindByValue("-1");

                            if (def == null) continue;

                            dropDown.SelectedIndex = dropDown.Items.IndexOf(def);

                            continue;
                        }

                        var listBox = control as ListBox;
                        if (listBox != null) //revise? use extension method to clear?
                        {
                            listBox.Items.Clear();

                            continue;
                        }

                        var colorPicker = control as RadColorPicker; //revise!!! use extension method to clear?
                        if (colorPicker != null)
                        {
                            colorPicker.SelectedColor = Color.Empty;

                            continue;
                        }

                		ControlValueToPropertyMap[control.GetType()].Set(control, kvp.Value);
					}
				}

			#endregion

			#region method: Page

			private void Page_Init()
			{
				if (FormMode == PersistModeEnum.NotSet) throw new InvalidOperationException(String.Format("The FormMode property must be set on your EditForm control ({0}).", ID));

				//               
                
                FormControls = ControlHelper.GetControlsByType<Control>(this, false, null, ControlValueToPropertyMapper.Maps.Keys);

                foreach (var c in ControlHelper.GetControlsByType<Control>(this, false, null, new[]{typeof(HtmlGenericControl), typeof(Panel)}))
                {
                    ControlHelper.GetControlsByType<Control>(c, false, null, ControlValueToPropertyMapper.Maps.Keys).ForEach(_=> FormControls.Add(_));
                }

				dct = new DynamicControlTracker(this, ViewState);
			}

			protected virtual void Page_Load()
			{
                UpdateButton.Click += delegate{FireOnUpdate();}; //should move to init but conflicts with TabBase handler???
			}

            private void Page_FirstLoad()
			{
				if (String.IsNullOrWhiteSpace(ValidationGroup))
				{
					ValidationGroup = ("FVG" + random.Next(1000, 9999));
				}

                ControlHelper.GetControlsByType<BaseValidator>(this, true).ForEach(_=> _.ValidationGroup=ValidationGroup);

				if (UpdateButton != null)
				{
					UpdateButton.ValidationGroup = ValidationGroup;

					if (!String.IsNullOrWhiteSpace(UpdateButtonCaption))
					{
						UpdateButton.Text = UpdateButtonCaption;
					}
				}

				//

				CaptureDefaultValues();

				if (FormMode == PersistModeEnum.Insert)
				{
					ConfigureForm();
				}
			}

			#endregion

			/// <summary>
			/// A modifier is a property of the form that determines a particular data-entry control's 'Visibile' or 'Enable' state.
			/// The value this method returns determines that state.
			/// </summary>
			/// <param name="name">Name of the field used to find the modifier value.</param>
			/// <param name="state">The modifier for which the value is desired.</param>
			private Boolean GetModifierValue(String name, ControlModifierEnum state)
			{
				name = (name + state);

				MemberInfo mi;

				ControlStateModifiers.TryGetValue(name, out mi);

				if (mi == null)
				{
					name = (KGSEntityType + name);

					ControlStateModifiers.TryGetValue(name, out mi);
				}

				if (mi == null) return true;

				Boolean value;

				switch (mi.MemberType)
				{
					case MemberTypes.Field:
					{
						value = (Boolean)((FieldInfo)mi).GetValue(this);

						break;
					}
					case MemberTypes.Property:
					{
						value = (Boolean)((PropertyInfo)mi).GetValue(this, null);

						break;
					}
					default: return true;
				}

				return value;
			}

			private void SetControlState(Control control)
			{
				if (control is HiddenField) return;

				var propName = control.ID.TrimControlPrefix();

				if (!GetModifierValue(propName, ControlModifierEnum.Enabled)){DisableControl(control);}

				if (!GetModifierValue(propName, ControlModifierEnum.Visible))
				{
					var parent = control.Parent as HtmlGenericControl;

					if ((parent == null) || (parent.TagName.ToLower() != "div"))
					{
						control.Visible = false;
					}
					else
					{
						parent.Visible = false;
					}
				}
			}

			private void SetEntityPropertyValue(Object obj, PropertyInfo pi, Object value)
			{
                if ((pi.PropertyType == NullableDateTimeType || pi.PropertyType == NullableIntType) && String.IsNullOrWhiteSpace(value.ToString())) //may be similar scenarios yet to be discovered
				{
					pi.SetValue(obj, null, null);

					return;
				}
					
				pi.SetValue(obj, Convert.ChangeType(value, (Nullable.GetUnderlyingType(pi.PropertyType) ?? pi.PropertyType)), null);
			}

		#endregion
		
		#region property

			#region abstract

				public abstract Delegates<TKGSEntity>.CanEntityBePersistedDelegate CanEntityBePersisted{get;}

				public abstract Action<TKGSEntity> PersistEntityAction{get;}

			#endregion

			protected AdministrationPageBase page
			{
				get {return Page as AdministrationPageBase;}
			}

			protected TabBase tab
			{
				get
				{
					if (_tab == null)
					{
						var c = Parent;

						while (!(c is TabBase))
						{
							c = Parent.Parent;
						}

						_tab = c as TabBase;
					}

					return _tab;
				}
			}

			//

			public PersistModeEnum FormMode
			{
				set;
				get;
			}

			public Boolean IsInsert=> (FormMode == PersistModeEnum.Insert);

			public Boolean IsUpdate=> (FormMode == PersistModeEnum.Update);

			public String ValidationGroup
			{
				set;
				get;
			}

			public String UpdateButtonCaption
			{
				set;
				get;
			}

			//

			protected IDictionary<Type,ControlValueToPropertyMapper.PropertyAccessor> ControlValueToPropertyMap
			{
				private set;
				get;
			}

			//

			[
			ViewStateProperty
			]
			protected IDictionary<String,String> DefaultControlValues
			{
				set;
				get;
			}

		#endregion
	}
}