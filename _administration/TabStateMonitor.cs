﻿using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using Telerik.Web.UI;

namespace CW.Website._administration
{
	public sealed class TabStateMonitor
	{
		#region INTERFACE

			public interface IReactive //rename
			{
				IEnumerable<Enum> ReactToChangeStateList {get;} //rename
			}
	
		#endregion

		#region ENUM

			private enum PlaceholderEnum
			{
				Default,
			}

		#endregion

		#region CONST

			private const String SCRIPT_TabChangeFunctionName = "OnClientActiveTabChanged";
			private const String SCRIPT_TabsArrayVariableName = "refreshTabsArray";
			private const String SCRIPT_TabChangeMessagesName = "stateChangeMessages";

		#endregion

		#region delegate

			public delegate void StateChanged(TabBase tab, IEnumerable<Enum> messages);

		#endregion

		#region event

			public event StateChanged OnChangeState;
			public event StateChanged OnTabStateChanged;

		#endregion

		#region field

			#region readonly

				private readonly SitePage page;
				private readonly RadTabStrip tabContainer;

			#endregion

			private Boolean InRefreshMode;
			private List<Enum> changeStateMessages;

		#endregion

		#region constructor

			public TabStateMonitor(SitePage page, RadTabStrip tabContainer)
			{
				changeStateMessages = new List<Enum>();

				if (tabContainer.AutoPostBack || (tabContainer.Tabs.Count == 1)) return; //if AutoPostBack is enabled than there is no point to TabStateMonitor
				
				this.page         = page;
				this.tabContainer = tabContainer;
				
				tabContainer.OnClientTabSelected = SCRIPT_TabChangeFunctionName; //move to PreInit? or Render?

				page.FirstLoad += delegate{page.RegisterFileAsScriptInclude("/_administration/_script/TabStateMonitor.js?v=_assets/".TrimEnd('/'));};
				page.Load      += OnPageLoad;				
				page.PreRender += OnPagePreRender;
			}

		#endregion

		#region method
            
			#region page:events

			private void OnPageLoad(Object sender, EventArgs e)
			{
				if ((page.EventTarget != tabContainer.UniqueID) || (OnTabStateChanged == null)) return;

				OnTabStateChanged
				(
					ControlHelper.FindControl<TabBase>(((RadMultiPage)tabContainer.Parent.FindControl(tabContainer.MultiPageID)).PageViews[tabContainer.SelectedIndex], (c=>(c is TabBase))),
					new JavaScriptSerializer().Deserialize<String[]>(page.Request.Form["__EVENTDATA_STATECHANGEMESSAGES"]).Select(_=>_.Split('|')).Select(_=>(Enum)Enum.Parse(Type.GetType(_[0]), _[1]))
				);
			}
            
			private void OnPagePreRender(Object sender, EventArgs e)
			{
				if (!InRefreshMode) return;

				ScriptInterop.SetProperty(page, String.Concat("window.", SCRIPT_TabChangeFunctionName), SCRIPT_TabChangeMessagesName, changeStateMessages.Select(_=>_.GetType().FullName + '|' + Convert.ToInt32(_)));

				ScriptInterop.SetProperty
				(
					page,
					String.Concat("window.", SCRIPT_TabChangeFunctionName),
					SCRIPT_TabsArrayVariableName,
					tabContainer.Tabs
						.Select(_=>new{tab=_, enums=ControlHelper.FindControlsByInterface<IReactive>(_.PageView, true).SelectMany(__=>(__.ReactToChangeStateList ?? new Enum[]{PlaceholderEnum.Default})).Distinct()})
                        .Select(_=>(_.tab != tabContainer.SelectedTab) && (_.enums.Contains(PlaceholderEnum.Default) || _.enums.Intersect(changeStateMessages).Any()))
				);
			}

			#endregion

			public void ChangeState(Enum message)
			{
				ChangeState((message == null) ? null : new[]{message});
			}

			public void ChangeState(IEnumerable<Enum> messages=null)
			{
                if (tabContainer == null) return;

				InRefreshMode = true;

                changeStateMessages.AddRange(messages ?? Enumerable.Empty<Enum>());

                changeStateMessages = changeStateMessages.Distinct().ToList();

                if (OnChangeState != null)
				{
                    OnChangeState(ControlHelper.FindControl<TabBase>(tabContainer.Tabs[tabContainer.SelectedIndex], (_=>(_ is TabBase))), changeStateMessages);
				}
			}

		#endregion
	}
}