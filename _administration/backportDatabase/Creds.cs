﻿using CW.Common.Config;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Website.DependencyResolution;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._administration.backportDatabase
{
	public struct Creds
	{
		#region STATIC

			private static String[] ExtractFromRecord(Database db, String field="BackportCreds")
			{
				return RijndaelEncryptor.Decrypt
				(
					((field == "Creds") ? db.Creds : db.BackportCreds), IoC.Resolve<IConfigManager>().GetConfigurationSetting(DataConstants.EncryptionParaphrase, String.Empty, true)
				
				).Split('¥');
			}

		#endregion

		public String User;
		public String Pass;

		private Creds(String user, String pass)
		{
			User = user;
			Pass = pass;
		}

		private Creds(IEnumerable<String> enumerable)
		{
			User = enumerable.First();
			Pass = enumerable.Last();
		}

		public Creds(Database db, String field="BackportCreds"): this(ExtractFromRecord(db, field)
)
		{
		}
	}
}