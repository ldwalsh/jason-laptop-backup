﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BackportDatabaseViewTab.ascx.cs" Inherits="CW.Website._administration.backportDatabase.system.BackportDatabaseViewTab" %>

<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Website._administration.backportDatabase" %>
<%@ Import Namespace="CW.Data.AzureStorage.Mgmt" %>

<%@ Register TagPrefix="CW" Namespace="CW.Website._administration.backportDatabase.system" Assembly="CW.Website" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Database Backport Jobs" />  

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>                                                                                                                                                     

<div id="gridTbl">
    <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
        <Columns>

			<asp:TemplateField ItemStyle-Wrap="true" SortExpression="RequestId" HeaderText="Request">
                <ItemTemplate>
					<label title="<%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.RequestId)) %>"><%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.RequestId), 30) %></label>
                </ItemTemplate>
            </asp:TemplateField>   

			<asp:TemplateField ItemStyle-Wrap="true" SortExpression="RequestType" HeaderText="Type">
                <ItemTemplate>
					<label title="<%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.RequestType)) %>"><%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.RequestType), 30) %></label>
                </ItemTemplate>
            </asp:TemplateField>   
			
			<asp:TemplateField ItemStyle-Wrap="true" SortExpression="DatabaseName" HeaderText="DatabaseName">
                <ItemTemplate>
					<label title="<%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.ServerName)) %>"><%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.ServerName), 30) %></label>
                </ItemTemplate>
            </asp:TemplateField>   


            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="QueuedTime" HeaderText="Queued">  
                <ItemTemplate>
					<label title="<%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.QueuedTime)) %>"><%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.QueuedTime), 30) %></label>
                </ItemTemplate>
            </asp:TemplateField>   


            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Status" HeaderText="Status">
                <ItemTemplate>
					<label title="<%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.Status)) %>"><%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.Status), 30) %></label>
                </ItemTemplate>
            </asp:TemplateField>   

            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ErrorMessage" HeaderText="Message">
                <ItemTemplate>
					<label title="<%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.ErrorMessage)) %>"><%# GetCellContent(Container, PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.ErrorMessage), 30) %></label>
                </ItemTemplate>
            </asp:TemplateField>   

        </Columns>
    </asp:GridView>                                                                                                             
</div>


<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" ></asp:DetailsView>