﻿using AjaxControlToolkit;
using CW.Data;
using CW.Data.AzureStorage.Mgmt;
using CW.Utility.Extension;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace CW.Website._administration.backportDatabase.system
{
    public partial class BackportDatabaseAddTab: AddTabBase<MgmtSqlController.BacpacStatus,SystemBackportDatabaseAdministration> 
	{
		#region CONST

			private const String EmptyItemValue = "-1";

		#endregion

		#region STATIC

			#region field
		
				private static ListItem defaultEnvironmentListItem;

			#endregion

			#region constructor

				static BackportDatabaseAddTab()
				{
					defaultEnvironmentListItem = new ListItem("Select one...", EmptyItemValue);
				}

			#endregion

		#endregion

		#region field

			private IDictionary<DropDownList,DropDownList> DropDownsMapping;

			private IDictionary<String,String> CustomErrorMessages;

		#endregion

		#region constructor

			public BackportDatabaseAddTab()
			{
				CustomErrorMessages = new Dictionary<String,String>	{};
			}

		#endregion

		#region method

			#region page_events

				private void Page_Init()
				{
					DropDownsMapping = new Dictionary<DropDownList,DropDownList>{{ddlEnvironmentFrom, ddlDatabaseFrom}, {ddlEnvironmentTo, ddlDatabaseTo},};
				
					new[]{ddlEnvironmentFrom, ddlEnvironmentTo}.ForEach(_=>_.SelectedIndexChanged += (Object sender, EventArgs e)=>{EnvironmentDropDownChanged((DropDownList)sender);});

					ddlEnvironmentFrom.SelectedIndexChanged += (sender, args)=>{RestoreDatabaseToItem();};
					ddlEnvironmentTo  .SelectedIndexChanged += (sender, args)=>{ValidateDatabaseTo();};

					ddlDatabaseFrom   .SelectedIndexChanged += (sender, args)=>{ValidateDatabaseTo();};

					btnRun.Click += btnRun_Click;
				}

				private void Page_FirstInit()
				{
                    using (SqlQueryWrapper sqlWrapper = new SqlQueryWrapper(DataMgr.ConnectionString))
                    {
                        sqlWrapper.Run
                        (
                            (db) => { PopulateDropDowns(new[] { ddlEnvironmentFrom, ddlEnvironmentTo }, db.Databases.Select(_ => _.Environment).Distinct().Select(_ => new ListItem(_, _)), defaultEnvironmentListItem); }
                        );
                    }
				}

				private void Page_Load()
				{
					foreach (var v in FindControls<RequiredFieldValidator>())
					{
						var controlToValidate = FindControl(v.ID.Split('_').First());

						v.Display           = ValidatorDisplay.None;
						v.SetFocusOnError   = true;
						v.InitialValue      = EmptyItemValue;
						v.ValidationGroup   = "Main";
						v.ControlToValidate = controlToValidate.ID;

						((ListControl)controlToValidate).ValidationGroup = v.ValidationGroup;
					
						String val;

						if (!CustomErrorMessages.TryGetValue(v.ControlToValidate, out val)) continue;
					
						v.ErrorMessage = val;
					}

					foreach (var ve in FindControls<ValidatorCalloutExtender>())
					{
						ve.TargetControlID = ve.ID.Split(new[]{"Extender",}, StringSplitOptions.None)[0];
					}
				}

			#endregion

			private void PopulateDropDowns(DropDownList ddl, IEnumerable<ListItem> items, ListItem defaultItem=null)
			{
				PopulateDropDowns(new[]{ddl}, items, defaultItem);
			}

			private void PopulateDropDowns(IEnumerable<DropDownList> ddls, IEnumerable<ListItem> items, ListItem defaultItem=null)
			{
				foreach (var ddl in ddls)
				{
					ddl.Items.Clear();

					ddl.Items.Add(defaultItem ?? new ListItem(String.Empty, EmptyItemValue));

					Array.ForEach(((ddl == ddlEnvironmentTo) ? items.Where(_=>_.Value != BackportScheduler.PROD_SYMBOL) : items).ToArray(), _=>ddl.Items.Add(_));
				}
			}
			
			private void RestoreDatabaseToItem()
			{
				if (selectedDatabaseFromItem == null) return;

				var itm = selectedDatabaseFromItem.Value;

				ddlDatabaseTo.Items.Add(new ListItem(itm.Key, itm.Value));
	
				selectedDatabaseFromItem = null;
			}

			private void ValidateDatabaseTo()
			{
				if (ddlEnvironmentFrom.SelectedValue != ddlEnvironmentTo.SelectedValue) return;

				RestoreDatabaseToItem();

				if (ddlDatabaseFrom.SelectedValue == EmptyItemValue) return;
	
				var item = ddlDatabaseFrom.SelectedItem;	

				ddlDatabaseTo.Items.Remove(ddlDatabaseTo.Items.FindByValue(item.Value));

				selectedDatabaseFromItem = new KeyValuePair<String,String>(item.Text, item.Value);
			}

			private void btnRun_Click(Object sender, EventArgs e)
			{
				if (ddlDatabaseFrom.SelectedValue == ddlDatabaseTo.SelectedValue) throw new InvalidOperationException("The from and to databases cannot be equal.");

                var dm = DataMgr; //required because DataMgr getter uses the SessionID to get the DataManager instance which does not exist in the thread created by Task.Run

				Task.Run(async () => {await new BackportScheduler(dm, LogMgr).ScheduleAsync(Int32.Parse(ddlDatabaseFrom.SelectedValue), Int32.Parse(ddlDatabaseTo.SelectedValue));});

				DisplayMessage("{EntityTypeName} has been initiated.");

				Page_FirstInit();

				ddlDatabaseFrom.Items.Clear();
				ddlDatabaseTo  .Items.Clear();

				page.TabStateMonitor.ChangeState();
			}

			private void EnvironmentDropDownChanged(DropDownList ddl)
			{
				var items = new List<ListItem>();

				if (ddl.SelectedValue != EmptyItemValue)
				{
                    using (SqlQueryWrapper sqlWrapper = new SqlQueryWrapper(DataMgr.ConnectionString))
                    {
                        items = sqlWrapper.Run<List<ListItem>>((db) => { return db.Databases.Where(_ => _.Environment == ddl.SelectedValue).Select(_ => new ListItem(_.DisplayName, _.DatabaseID.ToString())).ToList(); });
                    }
                    
				}

				PopulateDropDowns(DropDownsMapping[ddl], items);
			}

		#endregion

		#region property

			[
			ViewStateProperty
			]
			private KeyValuePair<String,String>? selectedDatabaseFromItem {set;get;}

		#endregion
	}
}