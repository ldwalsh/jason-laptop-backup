﻿using CW.Business;
using CW.Common.Config;
using CW.Common.Constants;
using CW.Logging;
using CW.Data;
using CW.Data.AzureStorage.Mgmt;
using CW.Utility;
using CW.Utility.Extension;
using CW.Website.DependencyResolution;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CW.Website._administration.backportDatabase.system
{
	public class BackportScheduler
	{
		#region CONST

			public  const String PROD_SYMBOL = "P"; //this const is likely duplicate and should use one that's already in CW.Data?

		#endregion

		#region STATIC

			public static IEnumerable<MgmtSqlController.BacpacStatus> GetStatuses(string connectionString, Int32 daysBack=1)
			{
				var passphrase = IoC.Resolve<IConfigManager>().GetConfigurationSetting(DataConstants.EncryptionParaphrase, String.Empty, true);

                using (SqlQueryWrapper sqlWrapper = new SqlQueryWrapper(connectionString))
                {
                    var dbs = sqlWrapper.Run<IEnumerable<Database>>((db) => { return db.Databases.ToList(); });

                    var statuses = new List<MgmtSqlController.BacpacStatus>();

                    if (!dbs.Any()) return statuses;

                    using (var con = new MgmtSqlController(new MgmtControllerInputs(RijndaelEncryptor.Decrypt(dbs.First().SubscriptionID, passphrase), null, SystemBackportDatabaseAdministration.GetMgmtCertString(connectionString), null)))
                    {
                        foreach (var db in dbs.GroupBy(_ => _.Environment, (key, _) => _.First()))
                        {
                            var creds = new Creds(db);

                            con.GetDacStatus(RijndaelEncryptor.Decrypt(db.ServerName, passphrase), creds.User, creds.Pass).ForEach(_ => statuses.Add(_));
                        }
                    }

                    return statuses.Where(_ => _.QueuedTime.Date > DateTime.Today.AddDays(-1 * daysBack));
                }
			}

		#endregion

		#region DELEGATE

			private delegate String DacFunc(String server, String database, String user, String pass, String storageAccessKey, Uri blobUri);

		#endregion

		#region field

			private readonly DataManager dataManager;
			private readonly ISectionLogManager logger;
			
		#endregion

		#region constructor

			public BackportScheduler(DataManager dataManager, ISectionLogManager logger)
			{
				this.dataManager = dataManager;
				this.logger      = logger;
			}

		#endregion

		#region method

			public async Task ScheduleAsync(Int32 fromDatabaseId, Int32 toDatabaseId)
			{
                await Task.Run(() => { Schedule(fromDatabaseId, toDatabaseId); });
			}
			
			public void Schedule(Int32 fromDatabaseId, Int32 toDatabaseId)
			{
				var blobUri = new Uri(String.Format(@"{0}{1}/{2}.bacpac", dataManager.AccountCommon.BlobEndpoint, "sql-exports", DateTime.UtcNow.Ticks.ToString()));

				var accountAccessKey = IoC.Resolve<DataManager>().AccountCommon.Credentials.ExportBase64EncodedKey(); //revise and pass this into method

                using (SqlQueryWrapper sqlWrapper = new SqlQueryWrapper(dataManager.ConnectionString))
                {
                    var sourceDB = sqlWrapper.Run<Database>((_) => { return _.Databases.Single(__ => __.DatabaseID == fromDatabaseId); });
                    var targetDB = sqlWrapper.Run<Database>((_) => { return _.Databases.Single(__ => __.DatabaseID == toDatabaseId); });

                    if ((targetDB.Environment == PROD_SYMBOL) && (targetDB.DatabaseName == DataConstants.DB_PRODUCTION_NAME)) throw new InvalidOperationException("Can not Schedule import against Production CWCurrent.");

                    var passphrase = IoC.Resolve<IConfigManager>().GetConfigurationSetting(DataConstants.EncryptionParaphrase, String.Empty, true);

                    using (var con = new MgmtSqlController(new MgmtControllerInputs(RijndaelEncryptor.Decrypt(sourceDB.SubscriptionID, passphrase), null, SystemBackportDatabaseAdministration.GetMgmtCertString(dataManager.ConnectionString), null)))
                    {
                        Creds creds;
                        String requestRef;

                        creds = new Creds(sourceDB);

                        var sourceServerName = RijndaelEncryptor.Decrypt(sourceDB.ServerName, passphrase);

                        try
                        {
                            requestRef = con.ExportToBACPAC(sourceServerName, sourceDB.DatabaseName, creds.User, creds.Pass, accountAccessKey, blobUri);
                        }
                        catch (Exception ex)
                        {
                            LogException(ex);

                            return;
                        }

                        if (requestRef == null) return;

                        if (!con.WaitForComplete(requestRef.ToString(), sourceServerName, creds.User, creds.Pass))
                        {
                            logger.Log(DataConstants.LogLevel.ERROR, String.Format("The Bacpac ({0}) export service returned with a failed message.", requestRef));

                            return;
                        }
                    }

                    using (var con = new MgmtSqlController(new MgmtControllerInputs(RijndaelEncryptor.Decrypt(targetDB.SubscriptionID, passphrase), null, SystemBackportDatabaseAdministration.GetMgmtCertString(dataManager.ConnectionString), null)))
                    {
                        Creds creds;
                        String requestRef;

                        creds = new Creds(targetDB);

                        var targetServerName = RijndaelEncryptor.Decrypt(targetDB.ServerName, passphrase);

                        try
                        {
                            requestRef =
                            con.ImportFromBACPAC
                            (
                                targetServerName, targetDB.DatabaseName, creds.User, creds.Pass, accountAccessKey, blobUri, new MgmtSqlController.DatabaseOptions(targetDB.Edition, targetDB.Size)
                            );
                        }
                        catch (Exception ex)
                        {
                            LogException(ex);

                            return;
                        }

                        if (!con.WaitForComplete(requestRef.ToString(), targetServerName, creds.User, creds.Pass))
                        {
                            logger.Log(DataConstants.LogLevel.ERROR, String.Format("The Bacpac ({0}) export service returned with a failed message.", requestRef));

                            return;
                        }

                        //
                        //create default user

                        var defaultUsername = ("CW_User_" + targetDB.DatabaseName.Substring("CWCurrent".Length));

                        creds = new Creds(targetDB, "Creds");

                        if (!con.CreateUser(targetServerName, targetDB.DatabaseName, creds.User, creds.Pass, defaultUsername))
                        {
                            logger.Log(DataConstants.LogLevel.ERROR, ("The user '" + defaultUsername + "' could not be created for Backport: " + targetServerName + "." + targetDB.DatabaseName));
                        }

                        //Anonymize the user table data
                        var anonymizeUserData = new AnonymizeUserData(targetServerName, targetDB.DatabaseName, creds, logger);
                        var userAnonymizedData = anonymizeUserData.Anonymize();

                        anonymizeUserData.UpdateTableWithAnonymizedData(userAnonymizedData);
                    }
                }
			}

			private void LogException(Exception ex)
			{
				logger.Log(DataConstants.LogLevel.ERROR, ex.Message, ex);
			}

		#endregion
	}
}