﻿using CW.Business.Bootstrapping;
using CW.Business.Config;
using CW.Common.Constants;
using CW.Utility;
using System;
using System.Security.Cryptography.X509Certificates;

namespace CW.Website._administration.backportDatabase.system
{
	public partial class SystemBackportDatabaseAdministration: AdministrationPageBase
	{
		#region STATIC

			public static String GetMgmtCertString(string connectionString)
			{
				return
				Convert.ToBase64String
				(
					CertificateHelper.LoadCertificate
					(
						StoreName.My, StoreLocation.LocalMachine, new ConfigManagerWrapper().CM.GetConfigurationSetting(DataConstants.CertMgmtFootprint, String.Empty)
			
					).RawData
				);
			}

		#endregion

		public override String NameField
		{
			get {return "RequestId";}
		}

		public override String IdentifierField
		{
			get {return "RequestId";}
		}
	}
}