﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BackportDatabaseAddTab.ascx.cs" Inherits="CW.Website._administration.backportDatabase.system.BackportDatabaseAddTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Schedule Database Backport Job" />

<div class="divForm">
	<label for="ddlEnvironmentFrom" class="label">Environment From:</label>
	<asp:DropDownList ID="ddlEnvironmentFrom" CssClass="dropdown" ClientIDMode="Predictable" runat="server" AutoPostBack="true" />
	<asp:RequiredFieldValidator ID="ddlEnvironmentFrom_Validator" runat="server" />
	<ajaxToolkit:ValidatorCalloutExtender ID="ddlEnvironmentFrom_ValidatorExtender" runat="server" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</div>

<div class="divForm">
	<label for="ddlDatabaseFrom" class="label">Database From:</label>
	<asp:DropDownList ID="ddlDatabaseFrom" CssClass="dropdown" ClientIDMode="Predictable" runat="server" AutoPostBack="true" />
	<asp:RequiredFieldValidator ID="ddlDatabaseFrom_Validator" runat="server" />
	<ajaxToolkit:ValidatorCalloutExtender ID="ddlDatabaseFrom_ValidatorExtender" runat="server" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</div>

<hr />

<div class="divForm">
	<label for="ddlEnvironmentTo" class="label">Environment To:</label>
	<asp:DropDownList ID="ddlEnvironmentTo" CssClass="dropdown" ClientIDMode="Predictable" runat="server" AutoPostBack="true" />
	<asp:RequiredFieldValidator ID="ddlEnvironmentTo_Validator" runat="server" />
	<ajaxToolkit:ValidatorCalloutExtender ID="ddlEnvironmentTo_ValidatorExtender" runat="server" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</div>

<div class="divForm">
	<label for="ddlDatabaseTo" class="label">Database To:</label>
	<asp:DropDownList ID="ddlDatabaseTo" CssClass="dropdown" ClientIDMode="Predictable" runat="server" />
	<asp:RequiredFieldValidator ID="ddlDatabaseTo_Validator" runat="server" />
	<ajaxToolkit:ValidatorCalloutExtender ID="ddlDatabaseTo_ValidatorExtender" runat="server" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</div>

<asp:LinkButton ID="btnRun" runat="server" CssClass="lnkButton" Text="Schedule" ValidationGroup="Main" />
