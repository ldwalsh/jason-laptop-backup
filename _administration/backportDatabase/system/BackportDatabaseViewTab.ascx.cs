﻿using CW.Data.AzureStorage.Mgmt;
using CW.Utility;
using System;
using System.Collections.Generic;

namespace CW.Website._administration.backportDatabase.system
{
    public partial class BackportDatabaseViewTab: ViewTabBase<MgmtSqlController.BacpacStatus,SystemBackportDatabaseAdministration>
	{
		protected override IEnumerable<String> GridColumnNames
		{
			get {return PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.RequestId, _=>_.DatabaseName, _=>_.QueuedTime, _=>_.RequestType, _=>_.ServerName, _=>_.Status, _=>_.ErrorMessage);}
		}

		protected override GetEntityDelegate GetEntityForDetailView
		{
			get {return null;}
		}

		protected override GetEntitiesForGridDelegate GetEntitiesForGrid
		{
			get
            {
                return (search)=>{return BackportScheduler.GetStatuses(DataMgr.ConnectionString, 1);};}
		}

		public override Boolean AlwaysForceGridCacherRefresh
		{
			get {return true;}
		}

		protected override String DefaultGridViewSortExpression
		{
			get {return PropHelper.G<MgmtSqlController.BacpacStatus>(_=>_.QueuedTime);}
		}

		protected override String CacheSubKey
		{
			get {return null;}
		}

		protected override String EntityTypeName
		{
			get {return "Backport Job";}
		}
	}
}