﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" AutoEventWireup="false" CodeBehind="SystemBackportDatabaseAdministration.aspx.cs" Inherits="CW.Website._administration.backportDatabase.system.SystemBackportDatabaseAdministration" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="BackportDatabaseViewTab.ascx" tagPrefix="CW" tagName="BackportDatabaseViewTab" %>
<%@ Register src="BackportDatabaseAddTab.ascx" tagPrefix="CW" tagName="BackportDatabaseAddTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   
    
    <CW:PageHeader runat="server" Title="Database Backport Administration" Description="The database backport administration area is used to scheduled database backport jobs." />
    <div class="administrationControls">
        <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
            <Tabs>
                <telerik:RadTab Text="View Database Backport Jobs"></telerik:RadTab>
            </Tabs>
            <Tabs>
                <telerik:RadTab Text="Schedule Database Backport Job"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
            <telerik:RadPageView ID="RadPageView1" runat="server">
				<CW:BackportDatabaseViewTab runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView2" runat="server">
				<CW:BackportDatabaseAddTab runat="server" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    
</asp:Content>
