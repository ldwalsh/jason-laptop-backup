﻿using CW.Logging;
using CW.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._administration.backportDatabase.system
{
    public sealed class AnonymizeUserData
    {
        #region const

            const string possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        
        #endregion

        #region fields

            readonly char[] _possibleCharsArray = possibleChars.ToCharArray();
            readonly string _connectionString;
            readonly ISectionLogManager _logger;
            readonly Random _random = new Random();

        #endregion

        #region constructor

            public AnonymizeUserData(String serverName, String databaseName, Creds creds, ISectionLogManager logger)
            {
                _connectionString = ("server=tcp:" + serverName + ".database.windows.net,1433;Initial Catalog=" + databaseName + ";UID=" + creds.User + ";PWD=" + creds.Pass);	
                _logger = logger;
            }

        #endregion

        #region methods

            public List<User> Anonymize()
            {
                using (SqlQueryWrapper sqlWrapper = new SqlQueryWrapper(_connectionString))
                {
                    return sqlWrapper.Run<List<User>>((db) =>
                        {
                            var users = db.Users.Where(u => !u.IsTestUser && u.OID != 1).ToList();
                            var usersAnonymized = new List<User>();
                            var anonymizedEmails = new List<string>();

                            try
                            {
                                foreach (var user in users)
                                {
                                    var userAnonymized = user;

                                    userAnonymized.FirstName = GenerateRandomChars();
                                    userAnonymized.LastName = GenerateRandomChars();
                                    userAnonymized.Email = SetUniqueAnonymizedEmail(anonymizedEmails); //ensure anonymized email is unique
                                    userAnonymized.Address = GenerateRandomChars();

                                    usersAnonymized.Add(userAnonymized);
                                }

                                return usersAnonymized;
                            }
                            catch (Exception ex) { _logger.Log(Common.Constants.DataConstants.LogLevel.ERROR, "Error anonymizing data", ex); }

                            return null;
                        });
                }
            }

            public void UpdateTableWithAnonymizedData(List<User> usersAnonymized)
            {
                if (usersAnonymized != null)
                {
                    using (SqlQueryWrapper sqlWrapper = new SqlQueryWrapper(_connectionString))
                    {
                        sqlWrapper.Run((db) =>
                            {
                                try
                                {
                                    foreach (var userAnonymized in usersAnonymized)
                                    {
                                        var originalUser = db.Users.Single(u => u.UID == userAnonymized.UID);

                                        originalUser.FirstName = userAnonymized.FirstName;
                                        originalUser.LastName = userAnonymized.LastName;
                                        originalUser.Email = userAnonymized.Email;
                                        originalUser.Address = userAnonymized.Address;
                                        originalUser.Phone = originalUser.MobilePhone = null;
                                        originalUser.DateModified = DateTime.UtcNow;

                                        db.SubmitChanges();
                                    }
                                }
                                catch (Exception ex) { _logger.Log(Common.Constants.DataConstants.LogLevel.ERROR, "Error updating anonymized data", ex); }
                            });
                    }
                }
            }

            private string GenerateRandomChars() { return new String(Enumerable.Repeat(_possibleCharsArray, 10).Select(s => s[_random.Next(s.Length)]).ToArray()); }

            private string SetUniqueAnonymizedEmail(List<string> anonymizedEmails)
            {
                var formattedText = "{0}@{1}.com";
                var anonymizedEmail = String.Format(formattedText, GenerateRandomChars(), GenerateRandomChars());

                if (anonymizedEmails.Contains(anonymizedEmail))
                {
                    var newAnonymizedEmail = String.Format(formattedText, GenerateRandomChars(), GenerateRandomChars());

                    anonymizedEmails.Add(newAnonymizedEmail);

                    return newAnonymizedEmail;
                }

                anonymizedEmails.Add(anonymizedEmail);

                return anonymizedEmail;
            }

        #endregion
    }
}