﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW.Website._administration
{
    /// <summary>
    /// Interface ensuring that implementing members should return fields 
    /// common to AdminSitePageTemp and AdministrationPageBaseBase
    /// </summary>
    interface IAdminPage
    {
        TabStateMonitor GetTabStateMonitor();
        string GetIdentifierField();
        string GetNameField();
    }
}
