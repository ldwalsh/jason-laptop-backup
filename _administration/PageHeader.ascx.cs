﻿using System;
using System.Reflection;

using CW.Data;
using CW.Website._framework;

namespace CW.Website._administration
{
    public partial class PageHeader: SiteUserControl
    {
        private String mDescription;
        private String mGlobalSettingProperty;

        public String Title
        {
            set
            {
                hdrTitle.InnerText = value;
            }
        }

        public String Description
        {
            get
            {
                return mDescription;
            }
            set
            {
                mDescription = value;
            }
        }

        public String GlobalSettingProperty
        {
            get
            {
                return mGlobalSettingProperty;
            }
            set
            {
                mGlobalSettingProperty = value;
            }
        }

        protected void Page_Load(Object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(Description))
            {
                if (String.IsNullOrWhiteSpace(GlobalSettingProperty))
                    throw new Exception("GlobalSettingProperty is not set.  If Description is not hard-coded, make sure to specify the property on the GlobalSetting object");

                GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                const BindingFlags bindingFlags = (BindingFlags.Instance | BindingFlags.Public);
                var pi = globalSetting.GetType().GetProperty(GlobalSettingProperty, bindingFlags);
                var propertyValue = pi.GetValue(globalSetting, null);

                if (propertyValue != null) divDesc.InnerHtml = propertyValue.ToString();
            }
            else
            {
                divDesc.InnerHtml = Description;
            }
        }
    }
}