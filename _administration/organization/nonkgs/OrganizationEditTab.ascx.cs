﻿using CW.Data;

namespace CW.Website._administration.organization.nonkgs
{
    public partial class OrganizationEditTab: EditTabBase<Organization,OrganizationAdministrationPageBase>
    {
        #region method

            private void Page_Init()
            {
                CanEntityBeUpdated = organizationForm.CanEntityBePersisted;
                UpdateEntityAction = organizationForm.PersistEntityAction;
            }

        #endregion

        #region property

            protected override Organization GetEntityForEditing
            {
                get { return DataMgr.OrganizationDataMapper.GetOrganizationByCID(siteUser.CID, (_ => _.State), (_ => _.Organizations_StorageAccounts)); }
            }

        #endregion
    }
}