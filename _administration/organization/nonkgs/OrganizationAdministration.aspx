﻿<%@ Page Language="C#" AutoEventWireup="false" MasterPageFile="~/_masters/Admin.Master" CodeBehind="OrganizationAdministration.aspx.cs" Inherits="CW.Website._administration.organization.nonkgs.OrganizationAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="OrganizationEditTab.ascx" TagPrefix="CW" TagName="OrganizationEditTab"  %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   

            <CW:PageHeader ID="PageHeader1" runat="server" Title="Organization Administration" GlobalSettingProperty="AdminOrganizationBody" />
            <div class="administrationControls">
                <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
                    <Tabs>
                        <telerik:RadTab Text="Edit Organization"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                            <CW:OrganizationEditTab ID="organizationEdit" runat="server"/>
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </div>

</asp:Content>