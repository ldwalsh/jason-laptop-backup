﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="OrganizationEditTab.ascx.cs" Inherits="CW.Website._administration.organization.nonkgs.OrganizationEditTab" %>
<%@ Register TagPrefix="CW" TagName="OrganizationForm" Src="../OrganizationForm.ascx"  %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Edit Organization" />
<CW:OrganizationForm ID="organizationForm" runat="server" FormMode="Update" OrganizationNameEnabled="false" ProdSAIDEnabled="true" ProdBackupSAIDEnabled="true" QASAIDEnabled="true" QABackupSAIDEnabled="true" DevSAIDEnabled="true" DevBackupSAIDEnabled="true" SessionTimeoutOverrideVisible="false" SessionTimeoutOverrideEnabled="true" UpdateButtonCaption="Update Organization" />
