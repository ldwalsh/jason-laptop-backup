﻿using System;

namespace CW.Website._administration.organization.kgs
{
    public partial class KGSOrganizationAdministration: OrganizationAdministrationPageBase
    {
        private void Page_Init()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check if user is orgadmin.
            //is org admin can only be set on superadmins or kgssuperadminsandrhigher, and master page dissallows superadmin access here
            if (!siteUser.IsOrgAdmin)
            {
                Response.Redirect("/Home.aspx");
            }
        }
    }
}

