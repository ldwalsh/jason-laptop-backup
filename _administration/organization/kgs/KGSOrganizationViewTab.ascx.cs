﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CW.Website._administration.organization.kgs
{
    public partial class KGSOrganizationViewTab: ViewTabWithFormBase<Organization,OrganizationAdministrationPageBase>
    {
		#region method

			protected void Page_Init(Object sender, EventArgs e)
			{
				CanEntityBeUpdated = organizationForm.CanEntityBePersisted;
				CanEntityBeDeleted = CanOrganizationBeDeleted;

				UpdateEntityMethod = organizationForm.PersistEntityAction;

				DeleteEntityMethod = DeleteOrganization;
			}

			private Boolean CanOrganizationBeDeleted(String oid, out String message)
			{
				var id = Convert.ToInt32(oid);

				if (DataMgr.ClientDataMapper.GetAllClientsByOrg(id).Count > 0)
				{
					message = "Cannot delete an organization which is associated with a client.";

					return false;
				}

				if (DataMgr.ProviderDataMapper.GetAllProvidersByOrg(id).Count > 0)
				{
					message = "Cannot delete an organization which is associated with a provider.";

					return false;
				}

				if (DataMgr.UserDataMapper.AreUsersAssociatedToOrganization(id))
				{
					message = "Cannot delete an organization which has existing users.";

					return false;
				}

				message = null;

				return true;
			}

			private void DeleteOrganization(String oid)
			{
				DataMgr.StorageAccountDataMapper.DeleteOrgToStorageAccountsMappings(Convert.ToInt32(oid));
 
				DataMgr.OrganizationDataMapper.DeleteOrganization(Convert.ToInt32(oid));

				page.TabStateMonitor.ChangeState();
			}

		#endregion

		#region property

			protected override IEnumerable<String> GridColumnNames
			{
				get	{return PropHelper.F<Organization>(_=>_.OrganizationName, _=>_.OrganizationAddress, _=>_.OrganizationCity, _=>_.Country.CountryName, _=>_.IsActive);} 
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get {return (searchText => DataMgr.OrganizationDataMapper.GetAllSearchedOrganizations(searchText, (_=>_.State), (_=>_.Country)));}
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
                get { return (oid => DataMgr.OrganizationDataMapper.GetOrganizationByID(Convert.ToInt32(oid), (_ => _.State), (_ => _.Organizations_StorageAccounts))); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
                get { return (oid => DataMgr.OrganizationDataMapper.GetOrganizationByID(Convert.ToInt32(oid), (_ => _.State), (_ => _.Country), (_ => _.Organizations_StorageAccounts))); }
			}

			protected override String CacheSubKey
			{
				get {return null;}
			}

		#endregion
    }
}