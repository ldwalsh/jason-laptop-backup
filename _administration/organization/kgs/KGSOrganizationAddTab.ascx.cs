﻿using System;
using System.Web.UI.WebControls;
using CW.Data;

namespace CW.Website._administration.organization.kgs
{
    public partial class KGSOrganizationAddTab: AddTabBase<Organization, KGSOrganizationAdministration>
    {
        #region method

            private void Page_Init()
            {
                CanEntityBeAdded = organizationForm.CanOrganizationBePersisted;
                AddEntityMethod  = organizationForm.PersistEntityAction;
            }

        #endregion
    }
}