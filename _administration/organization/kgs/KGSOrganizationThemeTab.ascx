﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSOrganizationThemeTab.ascx.cs" Inherits="CW.Website._administration.organization.kgs.KGSOrganizationThemeTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="../OrganizationThemeForm.ascx" tagPrefix="CW" tagName="OrganizationThemeForm" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Organziation Theme" />
<div id="Div1" class="divForm" runat="server">   
    <label class="label">*Select Organization:</label>    
    <asp:DropDownList ID="ddlOrganization" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlOrganization_OnSelectedIndexChanged" AutoPostBack="true">
        <asp:ListItem>Select one...</asp:ListItem>                           
    </asp:DropDownList> 
</div>
<CW:OrganizationThemeForm runat="server" ID="organizationThemeForm" Visible="false" UpdateButtonCaption="Update" FormMode="Update" />

