﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSOrganizationAdministration.aspx.cs" Inherits="CW.Website._administration.organization.kgs.KGSOrganizationAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="KGSOrganizationViewTab.ascx" tagPrefix="CW" tagName="OrganizationViewTab" %>
<%@ Register src="KGSOrganizationAddTab.ascx" tagPrefix="CW" tagName="OrganizationAddTab" %>
<%@ Register src="KGSOrganizationThemeTab.ascx" tagPrefix="CW" tagName="OrganizationThemeTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   

            <CW:PageHeader runat="server" Title="KGS Organization Administration" Description="The kgs organization administration area is used to view, add, and edit organizations." />
            <div class="administrationControls">
                <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
                    <Tabs>
                        <telerik:RadTab Text="View Organizations"></telerik:RadTab>
                        <telerik:RadTab Text="Add Organization"></telerik:RadTab>
                        <telerik:RadTab Text="Organization Theme"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                            <CW:OrganizationViewTab runat="server" ID="organizationView" />
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView2" runat="server">
                            <CW:OrganizationAddTab runat="server" ID="organizationAdd"/>
                    </telerik:RadPageView>
                    <telerik:RadPageView ID="RadPageView3" runat="server">
                            <CW:OrganizationThemeTab runat="server" ID="organizationTheme" ManualBind="true" />
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </div>

</asp:Content>


                    
                  
