<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSOrganizationAddTab.ascx.cs" Inherits="CW.Website._administration.organization.kgs.KGSOrganizationAddTab" %>
<%@ Register TagPrefix="CW" TagName="OrganizationForm" Src="../OrganizationForm.ascx"  %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="../../TabHeader.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Organization" />
<CW:OrganizationForm ID="organizationForm" runat="server" FormMode="Insert" UpdateButtonCaption="Add Organization" OrganizationNameEnabled="true" ProdSAIDVisible="true" ProdSAIDEnabled="true" ProdVerificationVisible="true" ProdVerificationEnabled="true" ProdBackupSAIDVisible="true" ProdBackupSAIDEnabled="true" QASAIDVisible="true" QASAIDEnabled="true" QABackupSAIDVisible="true" QABackupSAIDEnabled="true" DevSAIDVisible="true" DevSAIDEnabled="true" DevBackupSAIDVisible="true" DevBackupSAIDEnabled="true" SessionTimeoutOverrideVisible="true" SessionTimeoutOverrideEnabled="true" />
