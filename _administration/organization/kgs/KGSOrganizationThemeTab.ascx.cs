﻿using CW.Data;
using System;

namespace CW.Website._administration.organization.kgs
{
	public partial class KGSOrganizationThemeTab: EditTabBase<OrganizationTheme,KGSOrganizationAdministration>
	{
		#region method

		private void Page_Init()
		{
			CanEntityBeUpdated = organizationThemeForm.CanEntityBePersisted;
			UpdateEntityAction = organizationThemeForm.PersistEntityAction;
		}

		private void Page_FirstInit()
		{
			BindOrganizations(ddlOrganization);

			ddlOrganization.SelectedIndex = 0;

			ddlOrganization_OnSelectedIndexChanged(null, null);
		}

        private void Page_Load()
        {
            OnTabStateChanged += delegate{Page_FirstInit();};
        }

		protected void ddlOrganization_OnSelectedIndexChanged(Object sender, EventArgs e)
		{
			organizationThemeForm.Visible = (ddlOrganization.SelectedIndex != 0);

			if (ddlOrganization.SelectedIndex == 0) return;

			PopulateForm(GetEntityForEditing);
		}

		#endregion

		#region property

		protected override OrganizationTheme GetEntityForEditing
		{
			get
			{
				var oid = Convert.ToInt32(ddlOrganization.SelectedValue);

				return (oid == -1 ? null : DataMgr.OrganizationDataMapper.GetOrganizationThemeByOID(oid));
			}
		}

		#endregion
	}
}