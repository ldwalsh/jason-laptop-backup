<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSOrganizationViewTab.ascx.cs" Inherits="CW.Website._administration.organization.kgs.KGSOrganizationViewTab" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="OrganizationForm" src="../OrganizationForm.ascx"  %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Organizations" />
<asp:Label ID="lblResults" runat="server" Text="" />
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>                                                                                                                                                      
<div id="gridTbl">
    <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
        <Columns>
            <asp:CommandField ShowSelectButton="true" />
            <asp:TemplateField>
                <ItemTemplate>                                                
                    <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                    
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="OrganizationName" HeaderText="Name">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Organization>(_ => _.OrganizationName)) %>"><%# GetCellContent(Container, PropHelper.G<Organization>(_=>_.OrganizationName), 30) %></label>
                </ItemTemplate>
            </asp:TemplateField>   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="OrganizationAddress" HeaderText="Address">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Organization>(_ => _.OrganizationAddress)) %>"><%# GetCellContent(Container, PropHelper.G<Organization>(_=>_.OrganizationAddress), 25) %></label>
                </ItemTemplate>
            </asp:TemplateField>   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="OrganizationCity" HeaderText="City">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Organization>(_ => _.OrganizationCity)) %>"><%# GetCellContent(Container, PropHelper.G<Organization>(_=>_.OrganizationCity), 25) %></label>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="CountryName" HeaderText="Country">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Country>(_ => _.CountryName)) %>"><%# GetCellContent(Container, PropHelper.G<Country>(_=>_.CountryName), 20) %></label>
                </ItemTemplate>
            </asp:TemplateField> 
            <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<Organization>(_=>_.IsActive))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>                                                
                    <asp:LinkButton runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this organization permanently?\n\nAlternatively you can deactivate an organization temporarily instead.');" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                                                                                          
        </Columns>
    </asp:GridView>
</div>                                                                                                             
<br />
<br /> 
<!--SELECT ORGANIZATION DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
    <Fields>
        <asp:TemplateField  ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
            <ItemTemplate>
                <div>
                    <h2>Organization Details</h2>                                                                                                                                            
                    <ul class="detailsList">
                        <CW:UListItem runat="server" Label="Name" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationName)) %>" />
                        <CW:UListItem runat="server" Label="Address" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationAddress)) %>" />
                        <CW:UListItem runat="server" Label="City" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationCity)) %>" />
                        <CW:UListItem runat="server" Label="Country" Value="<%# Eval(PropHelper.G<Organization, Country>(o=>o.Country, c=>c.CountryName)) %>" />
                        <CW:UListItem runat="server" Label="State" Value="<%# Eval(PropHelper.G<Organization, State>(o=>o.State, s=>s.StateName)) %>" />
                        <CW:UListItem runat="server" Label="Zip" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationZip)) %>" />
                        <CW:UListItem runat="server" Label="Phone" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationPhone)) %>" />
                        <CW:UListItem runat="server" Label="Fax" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationFax)) %>" />
                        <CW:UListItem runat="server" Label="Primary Contact" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationPrimaryContactName)) %>" />
                        <CW:UListItem runat="server" Label="Primary Contact Email" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationPrimaryContactEmail)) %>" />
                        <CW:UListItem runat="server" Label="Primary Contact Phone" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationPrimaryContactPhone)) %>" />
                        <CW:UListItem runat="server" Label="Secondary Contact" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationSecondaryContactName)) %>" />
                        <CW:UListItem runat="server" Label="Secondary Contact Email" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationSecondaryContactEmail)) %>" />
                        <CW:UListItem runat="server" Label="Secondary Contact Phone" Value="<%# Eval(PropHelper.G<Organization>(o=>o.OrganizationSecondaryContactPhone)) %>" />
                        <CW:UListItem runat="server" Label="Session Timeout Override" Value="<%# Eval(PropHelper.G<Organization>(o=>o.SessionTimeoutOverride)) %>" />
                        <CW:UListItem runat="server" Label="Active" Value="<%# Eval(PropHelper.G<Organization>(o=>o.IsActive)) %>" />
                    </ul>
                </div>
            </ItemTemplate>                                             
        </asp:TemplateField>
    </Fields>
</asp:DetailsView>
                                                                     
<!--EDIT ORGANIZATION PANEL -->                              
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">                                                                                             
    <div>
        <h2>Edit Organization</h2>
    </div>
    <div>
        <CW:OrganizationForm ID="organizationForm" FormMode="Update" UpdateButtonCaption="Update Organization" OrganizationNameEnabled="true" ProdSAIDVisible="true" ProdSAIDEnabled="false" ProdBackupSAIDVisible="true" ProdBackupSAIDEnabled="false" ProdVerificationVisible="false" ProdVerificationEnabled="false" QASAIDVisible="true" QASAIDEnabled="false" QABackupSAIDVisible="true" QABackupSAIDEnabled="false" DevSAIDVisible="true" DevSAIDEnabled="false" DevBackupSAIDVisible="true" DevBackupSAIDEnabled="false" SessionTimeoutOverrideVisible="true" SessionTimeoutOverrideEnabled="true" runat="server" />
    </div>
</asp:Panel>
