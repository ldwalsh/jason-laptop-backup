﻿using System;
using CW.Data;
using CW.Utility;

namespace CW.Website._administration.organization
{
    public abstract class OrganizationAdministrationPageBase: AdministrationPageBase
    {
        public override String IdentifierField
        {
            get {return PropHelper.G<Organization>(o => o.OID);}
        }

        public override String NameField
        {
            get {return PropHelper.G<Organization>(o => o.OrganizationName);}
        }

        public override String EntityTypeName
        {
            get {return typeof(Organization).Name;}
        }
    }
}