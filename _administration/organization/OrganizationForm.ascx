<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="OrganizationForm.ascx.cs" Inherits="CW.Website._administration.organization.OrganizationForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<style type="text/css">
    .hideMe {visibility: hidden!important;}
    .showMe {visibility: visible!important;}
</style>

<script type="text/javascript">
    function verifyText(src, args){

        var prod = document.getElementById("ctl00_plcCopy_organizationAdd_organizationForm_ddlProdSAID");
        var prodBackup = document.getElementById("ctl00_plcCopy_organizationAdd_organizationForm_ddlProdBackupSAID");

        var prodText = prod.options[prod.selectedIndex].text;
        var prodBackupText = prodBackup.options[prodBackup.selectedIndex].text.replace(" Backup", "");

        args.IsValid = (prodText == args.Value && prodBackupText == args.Value);
    }
    function uploadFailed(sender, eventArgs) {
        var container = getErrorContainer();
        if (null != container) {
            container.innerText = "Validation failed for '" + eventArgs.get_fileName();
        }
    }
    function uploadSuccessful(sender, eventArgs) {
        clearError();
    }
    function clearError() {
        var container = getErrorContainer();
        if (null != container) {
            container.innerText = "";
            show(container, true);
        }
    }
    function uploadRemoved(sender, eventArgs) {
        clearError();
    }
    function anyError() {
        var err = '';
        var container = getErrorContainer();
        if (null != container) {
            err = container.innerText;
            if (null == err) {
                err = '';
            }
        }
        return (err);
    }

    function show(element, hide) {
        if (null != element) {
            if (null != hide && hide) {
                removeClass(element, "showMe");
                removeClass(element, "hideMe");
                addClass(element, "hideMe");
            }
            else {
                removeClass(element, "hideMe");
                removeClass(element, "showMe");
                addClass(element, "showMe");
            }
        }
    }
    function removeClass(element, className) {
        if (null != element) {
            element.className = element.className.replace(className, '').trim();
        }
    }

    function addClass(element, className) {
        if (null != element) {
            var c = element.className.trim();
            if (c.length > 0) {
                c = c + ' ';
            }
            element.className = c + className;
        }
    }

    function allowUpdate(buttonContext) {
        var allow = true;
        if (-1 != buttonContext.indexOf("Update")) {// When adding, no upload errors will exist
            var err = anyError();
            if (err.length > 0) {
                alert('Please correct previous error:\n' + err);
                allow = false;
            }
            else if('undefined' != typeof (changeChecked) && changeChecked()) {
                var idpHdr = document.getElementById('idpHdr');
                if (idpHdr) {
                    if (!confirm('You may have made changes to the way users will be authenticated for this Organization! Are you sure?')) {
                        allow = false;
                    }
                }
            }
        }
        return (allow);
    }
</script>

<asp:HiddenField ID="hdnOID" runat="server" />
<div class="divForm">
    <label class="label">*Organization Name:</label>
    <asp:TextBox ID="txtOrganizationName" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divForm">
    <label class="label">Address:</label>
    <asp:TextBox ID="txtAddress" CssClass="textbox" MaxLength="200" runat="server" />
</div>                                                                                                                                                                                           
<div class="divForm">
    <label class="label">City:</label>
    <asp:TextBox CssClass="textbox" ID="txtCity" runat="server" MaxLength="100" />
</div>
<div class="divForm">
    <label class="label">*Country:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlCountryAlpha2Code" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                
    </asp:DropDownList>
</div>
<div class="divForm">
    <label class="label">State (Required based on Country):</label>
    <asp:DropDownList ID="ddlStateID" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
    </asp:DropDownList>                                                                                                                                                              
</div>
<div class="divForm">
    <label class="label">Zip Code (Required based on Country):</label>
    <telerik:RadMaskedTextBox ID="txtZip" CssClass="textbox" runat="server" 
        PromptChar="" Mask="aaaaaaaaaa" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>                                                    
<div class="divForm">
    <label class="label">Phone:</label>
    <telerik:RadMaskedTextBox ID="txtPhone" CssClass="textbox" runat="server"
        Mask="(###)###-####" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>  
<div class="divForm">
    <label class="label">Fax:</label>
    <telerik:RadMaskedTextBox ID="txtFax" CssClass="textbox" runat="server"
        Mask="(###)###-####" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>  
<div class="divForm">
    <label class="label">Primary Contact:</label>
    <asp:TextBox ID="txtPrimaryContactName" CssClass="textbox" MaxLength="15" runat="server" />
</div>
<div class="divForm">
    <label class="label">Primary Contact Email:</label>
    <asp:TextBox ID="txtPrimaryContactEmail" CssClass="textbox" MaxLength="100" runat="server" />
</div>
<div class="divForm">
    <label class="label">Primary Contact Phone:</label>
    <telerik:RadMaskedTextBox ID="txtPrimaryContactPhone" CssClass="textbox" runat="server"
        Mask="(###)###-####" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>
<div class="divForm">
    <label class="label">Secondary Contact:</label>
    <asp:TextBox ID="txtSecondaryContactName" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divForm">
    <label class="label">Secondary Contact Email:</label>
    <asp:TextBox ID="txtSecondaryContactEmail" CssClass="textbox" MaxLength="100" runat="server" />
</div>
<div class="divForm">
    <label class="label">Secondary Contact Phone:</label>
    <telerik:RadMaskedTextBox ID="txtSecondaryContactPhone" CssClass="textbox" runat="server"
        Mask="(###)###-####" ValidationGroup=""></telerik:RadMaskedTextBox>
</div>
<div class="divForm" runat="server"><%-- Requires runat on div for radmaskedtextbox to recognize as parent --%>
      <label class="label">Session Timeout Override (Minutes):</label>
      <telerik:RadMaskedTextBox ID="txtSessionTimeoutOverride" CssClass="textbox"  runat="server" Mask="#####" PromptChar="" />
</div>
<div class="divForm" runat="server">
    <label class="label">*Production Storage Account:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlProdSAID" AppendDataBoundItems="true" runat="server">                
    </asp:DropDownList>
</div>
<div class="divForm" runat="server">
    <label class="label">*Production Backup Storage Account:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlProdBackupSAID" AppendDataBoundItems="true" runat="server">                
    </asp:DropDownList>
</div>
<div class="divForm" runat="server">
    <label class="label">*Production Verification:</label>
    <asp:TextBox ID="txtProdVerification" CssClass="textbox" MaxLength="50" runat="server" />
    <p>(Note: Type the common distiguishing name of the production and production backup storage accounts. i.e. "North America Prod")</p>
</div>
<div class="divForm" runat="server">
    <label class="label">*QA Storage Account:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlQASAID" AppendDataBoundItems="true" runat="server">                
    </asp:DropDownList>
</div>
<div class="divForm" runat="server">
    <label class="label">*QA Backup Storage Account:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlQABackupSAID" AppendDataBoundItems="true" runat="server">                
    </asp:DropDownList>
</div>
<div class="divForm" runat="server">
    <label class="label">*Development Storage Account:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlDevSAID" AppendDataBoundItems="true" runat="server">                
    </asp:DropDownList>
</div>
<div class="divForm" runat="server">
    <label class="label">*Development Backup Storage Account:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlDevBackupSAID" AppendDataBoundItems="true" runat="server">                
    </asp:DropDownList>
</div>
<div class="divForm" runat="server">
    <label class="label">*Active:</label>
    <asp:CheckBox ID="chkIsActive" CssClass="checkbox" runat="server" />
</div>
<asp:PlaceHolder ID="idpConfigArea" runat="server" Visible="false">
    <hr />
    <div id="idpHdr">
        <h2>Identity Provider Authentication Configuration</h2>
    </div>
    <div class="labelContent" runat="server" style="display:inline-table;">
        Changing any of the following has immediate impact on user authentication. Please updated with caution. <b>Would you like to make any changes?</b>&nbsp;<asp:CheckBox ID="changeConfiguration" CssClass="checkbox" runat="server" Checked="false" />
    </div>
    <asp:HiddenField ID="deleteExistingIdPConfig" runat="server" Value="" />
    <div id="changeArea" class="hideMe" runat="server">
        <div class="divForm hideMe" style="padding-bottom:10px;" id="cfgTypeArea" runat="server">
            <label class="label">Select Configuration Type:</label>
            <asp:RadioButtonList ID="rblConfigurationType" CssClass="radio" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Text="Certificate" Value="0"/>
                <asp:ListItem Text="Metadata" Value="1"/>
                <asp:ListItem Text="OnlyCertificate" Value="2" Enabled="false"/>
            </asp:RadioButtonList>
        </div>
        <div id="certFileArea" class="hideMe" runat="server">
            <asp:HiddenField ID="etag" runat="server" Value="" />
            <div class="divForm hideMe" id="divTokenTypes" runat="server">
                <label class="label">*Token Type:</label>
                <asp:DropDownList CssClass="dropdown" ID="ddlTokenTypes" AppendDataBoundItems="true" runat="server">                
                </asp:DropDownList>
            </div>
            <div class="divForm hideMe" runat="server" id="divDeleteCertificate">
                <label class="label">Delete associated certificate:</label>
                <asp:CheckBox ID="deleteCertificate" CssClass="checkbox" runat="server" />
            </div>
            <div class="divForm hideMe" runat="server" id="divUploadCertificate">
                <label class="label">*Public Key File (.cer):</label>
                <telerik:RadAsyncUpload Enabled="true" ID="rauUploadCert" runat="server"
                    AllowedFileExtensions="cer" 
                    OnClientFileUploadRemoved="uploadRemoved"
                    MaxFileInputsCount="1" 
                    OnClientFileUploaded="uploadSuccessful"
                    OnClientValidationFailed="uploadFailed">
                </telerik:RadAsyncUpload>
            </div>
        </div>
        <div id="metadataArea" class="hideMe" runat="server">
            <div class="divForm hideMe" runat="server" id="divDeleteMetadata">
                <label class="label">Delete associated metadata:</label>
                <asp:CheckBox ID="deleteMetadata" CssClass="checkbox" runat="server" />
            </div>
            <div class="divForm hideMe" runat="server" id="divUploadMetadata">
                <label class="label">*Metadata File (.xml):</label>
                <telerik:RadAsyncUpload Enabled="true" ID="rauUploadMetadata" runat="server"
                    AllowedFileExtensions="xml" 
                    OnClientFileUploadRemoved="uploadRemoved"
                    MaxFileInputsCount="1" 
                    OnClientFileUploaded="uploadSuccessful"
                    OnClientValidationFailed="uploadFailed">
                </telerik:RadAsyncUpload>
            </div>
        </div>

        <div class="divForm hideMe" id="aliasArea" runat="server">
            <label class="label">Alias for Organization ID:</label>
            <label class="labelContent" runat="server" id="aliasOID"></label>
        </div>
        <div class="errorMessage hideMe" runat="server" id="authnCfgFailure"></div>
    </div>
</asp:PlaceHolder>

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" OnClientClick="return allowUpdate(this.innerHTML);" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="organizationNameRequiredValidator" runat="server" ErrorMessage="Organization Name is a required field." ControlToValidate="txtOrganizationName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="organizationNameRequiredValidatorExtender" runat="server" TargetControlID="organizationNameRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="countryRequiredValidator" runat="server" ErrorMessage="Country is a required field." ControlToValidate="ddlCountryAlpha2Code" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="countryRequiredValidatorExtender" runat="server" TargetControlID="countryRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="stateRequiredValidator" runat="server" ErrorMessage="State is a required field." ControlToValidate="ddlStateID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="stateRequiredValidatorExtender" runat="server" TargetControlID="stateRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="zipRequiredValidator" runat="server"
    ErrorMessage="Zip Code is a required field."
    ControlToValidate="txtZip"          
    SetFocusOnError="true"
    Display="None"
    ValidationGroup=""
    Enabled="false">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="zipRequiredValidatorExtender" runat="server"
    TargetControlID="zipRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="zipRegExValidator" runat="server"
    ErrorMessage="Invalid Zip Format."
    ValidationExpression="\d{5}"
    ControlToValidate="txtZip"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup=""
    Enabled="false">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="zipRegExValidatorExtender" runat="server"
    TargetControlID="zipRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender> 
<asp:RegularExpressionValidator ID="phoneRegExValidator" runat="server"
    ErrorMessage="Invalid Phone Format."
    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
    ControlToValidate="txtPhone"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup="">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="phoneRegExValidatorExtender" runat="server"
    TargetControlID="phoneRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="faxRegExValidator" runat="server"
    ErrorMessage="Invalid Fax Format."
    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
    ControlToValidate="txtFax"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup="">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="faxRegExValidatorExtender" runat="server"
    TargetControlID="faxRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="primaryEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtPrimaryContactEmail" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="primaryEmailRegExValidatorExtender" runat="server" TargetControlID="primaryEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RegularExpressionValidator ID="primaryPhoneRegExValidator" runat="server"
    ErrorMessage="Invalid Phone Format."
    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
    ControlToValidate="txtPrimaryContactPhone"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup="">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="primaryPhoneRegExValidatorExtender" runat="server"
    TargetControlID="primaryPhoneRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>
<asp:RegularExpressionValidator ID="secondaryEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtSecondaryContactEmail" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="secondaryEmailRegExValidatorExtender" runat="server" TargetControlID="secondaryEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RegularExpressionValidator ID="secondaryPhoneRegExValidator" runat="server"
    ErrorMessage="Invalid Phone Format."
    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
    ControlToValidate="txtSecondaryContactPhone"
    SetFocusOnError="true" 
    Display="None" 
    ValidationGroup="">
    </asp:RegularExpressionValidator>                         
<ajaxToolkit:ValidatorCalloutExtender ID="secondaryPhoneRegExValidatorExtender" runat="server" 
    TargetControlID="secondaryPhoneRegExValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
    </ajaxToolkit:ValidatorCalloutExtender>


<asp:CompareValidator ID="sessionTimeoutOverrideCompareValidator" runat="server" ErrorMessage="Must be at least 5 minutes" ControlToValidate="txtSessionTimeoutOverride" ValueToCompare="5" Type="Integer" Operator="GreaterThanEqual" Display="None" SetFocusOnError="true" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="sessionTimeoutOverrideCompareValidatorExtender" runat="server" TargetControlID="sessionTimeoutOverrideCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="StorageAccountProdRequiredValidator" runat="server" ErrorMessage="Production Storage Account is a required field." ControlToValidate="ddlProdSAID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="StorageAccountProdRequiredValidatorExtender" runat="server" TargetControlID="StorageAccountProdRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="StorageAccountProdBackupRequiredValidator" runat="server" ErrorMessage="Production Backup Storage Account is a required field." ControlToValidate="ddlProdBackupSAID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="StorageAccountProdBackupRequiredValidatorExtender" runat="server" TargetControlID="StorageAccountProdBackupRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="StorageAccountProdVerificationRequiredValidator" runat="server" ErrorMessage="Production Verification is a required field." ControlToValidate="txtProdVerification" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="StorageAccountProdVerificationRequiredValidatorExtender" runat="server" TargetControlID="StorageAccountProdVerificationRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:CustomValidator ID="StorageAccountProdVerificationCustomValidator" runat="server" ClientValidationFunction="verifyText" ErrorMessage="Production Verification text does not match selections." ControlToValidate="txtProdVerification" SetFocusOnError="true" Display="None" ValidationGroup="" /> 
<ajaxToolkit:ValidatorCalloutExtender ID="StorageAccountProdVerificationCustomValidatorExtender" runat="server" TargetControlID="StorageAccountProdVerificationCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="StorageAccountQARequiredValidator" runat="server" ErrorMessage="QA Storage Account is a required field." ControlToValidate="ddlQASAID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="StorageAccountQARequiredValidatorExtender" runat="server" TargetControlID="StorageAccountQARequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="StorageAccountQABackupRequiredValidator" runat="server" ErrorMessage="QA Backup Storage Account is a required field." ControlToValidate="ddlQABackupSAID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="StorageAccountQABackupRequiredValidatorExtender" runat="server" TargetControlID="StorageAccountQABackupRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="StorageAccountDevRequiredValidator" runat="server" ErrorMessage="Development Storage Account is a required field." ControlToValidate="ddlDevSAID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="StorageAccountDevRequiredValidatorExtender" runat="server" TargetControlID="StorageAccountDevRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="StorageAccountDevBackupRequiredValidator" runat="server" ErrorMessage="Development Backup Storage Account is a required field." ControlToValidate="ddlDevBackupSAID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="StorageAccountDevBackupRequiredValidatorExtender" runat="server" TargetControlID="StorageAccountDevBackupRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
