﻿using CW.Business.Blob;
using CW.Business.Blob.Images;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models;
using FileUploaderControl;
using System;
using System.Web.UI;
using Telerik.Web.UI;

namespace CW.Website._administration.organization
{
	public partial class OrganizationThemeForm: FormGenericBase<OrganizationThemeForm,OrganizationTheme>
	{
		#region method

			#region override

				public override void PopulateForm(OrganizationTheme theme)
				{
					base.PopulateForm(theme);

					if (!String.IsNullOrEmpty(theme.LogoExtension))
					{
						populateFileUploader(new OrganizationLogoImage(theme, DataMgr.OrgBasedBlobStorageProvider), logoFileUploader);
					}

					if (!String.IsNullOrEmpty(theme.IconExtension))
					{
						populateFileUploader(new OrganizationIconImage(theme, DataMgr.OrgBasedBlobStorageProvider), iconFileUploader);
					}
				}

				private void populateFileUploader(OrganizationBlob image, FileUploader fileUploader)
				{
					if (image.Exists())
					{
						fileUploader.SetFile(image.Retrieve(), image.FileName);
					}
				}

				public override void ClearForm()
				{
					base.ClearForm();

					logoFileUploader.Clear();
					iconFileUploader.Clear();
				} 

			#endregion

			private void Page_Load()
			{
				OnFieldBind = FieldBind;
				OnFieldSave = FieldSave;
			}

			private Boolean CanOrganizationThemeBePersisted(OrganizationTheme theme, out String message)
			{
				message = null;

				return true;
			}

			public void PersistOrganization(OrganizationTheme theme)
			{
				if (logoFileUploader.HasChanged)
				{
					var image = new OrganizationLogoImage(theme, DataMgr.OrgBasedBlobStorageProvider);

					if (!String.IsNullOrEmpty(hdnLogoExtension.Value))
					{
						image.SetExtension(hdnLogoExtension.Value).Delete();

						theme.LogoExtension = String.Empty;
					}
                    
					if (logoFileUploader.File != null)
					{
						image.SetExtension(logoFileUploader.File.Extension).Insert(logoFileUploader.File.Content);

						theme.LogoExtension = logoFileUploader.File.Extension;
					}
				}

				if (iconFileUploader.HasChanged)
				{
					var image = new OrganizationIconImage(theme, DataMgr.OrgBasedBlobStorageProvider);
					
					if (!String.IsNullOrEmpty(hdnIconExtension.Value))
					{
						image.SetExtension(hdnIconExtension.Value).Delete();

						theme.IconExtension = String.Empty;
					}

					if (iconFileUploader.File != null)
					{
						image.SetExtension(iconFileUploader.File.Extension).Insert(iconFileUploader.File.Content);

						theme.IconExtension = iconFileUploader.File.Extension;
					}
				}

				DataMgr.OrganizationDataMapper.UpdateOrganizationTheme(theme);

				hdnLogoExtension.Value = theme.LogoExtension;
				hdnIconExtension.Value = theme.IconExtension;
			}

			private void FieldBind(Control control, ref Object value)
			{
				if (control.GetType() == typeof(RadColorPicker))
				{ 
					value = System.Drawing.ColorTranslator.FromHtml((string)value);
				}
			}

			private void FieldSave(Control control, ref Object value)
			{
				if (control.GetType() ==  typeof(RadColorPicker))
				{
					value = System.Drawing.ColorTranslator.ToHtml((System.Drawing.Color)value);
				}
			}

		#endregion

		#region property

			#region override

				public override Delegates<OrganizationTheme>.CanEntityBePersistedDelegate CanEntityBePersisted
				{
					get { return CanOrganizationThemeBePersisted; }
				}

				public override Action<OrganizationTheme> PersistEntityAction
				{
					get { return PersistOrganization; }
				}

			#endregion

		#endregion
	}
}