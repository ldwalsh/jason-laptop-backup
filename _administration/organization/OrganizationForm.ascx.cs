using CW.Common.Constants;
using CW.Data;
using CW.Data.Collections;
using CW.Data.Models;
using CW.Utility;
using System;
using System.Web.UI.WebControls;
using System.Linq;
using System.IO;
using System.Configuration;
using CW.RESTAdapter.Configuration;
using System.Web.UI;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;

namespace CW.Website._administration.organization
{
    public partial class OrganizationForm : FormGenericBase<OrganizationForm, Organization>
    {
        #region field

        public Boolean IsActiveVisible;

        public Boolean OrganizationNameEnabled;

        public Boolean SessionTimeoutOverrideVisible;
        public Boolean SessionTimeoutOverrideEnabled;

        public Boolean ProdSAIDVisible;
        public Boolean ProdSAIDEnabled;
        public Boolean ProdBackupSAIDVisible;
        public Boolean ProdBackupSAIDEnabled;
        public Boolean ProdVerificationVisible;
        public Boolean ProdVerificationEnabled;
        public Boolean QASAIDVisible;
        public Boolean QASAIDEnabled;
        public Boolean QABackupSAIDVisible;
        public Boolean QABackupSAIDEnabled;
        public Boolean DevSAIDVisible;
        public Boolean DevSAIDEnabled;
        public Boolean DevBackupSAIDVisible;
        public Boolean DevBackupSAIDEnabled;
        #endregion

        #region method
        #region constructor

        #endregion
        #region override

        protected override void Page_Load()
        {
            base.Page_Load();
            if (FormMode == PersistModeEnum.Update)
            {
                if (page.AdminMode == AdminModeEnum.KGS && idpConfigArea.Visible && changeConfiguration.Checked)
                {
                    AuthnSave = new Dictionary<string, Func<int, string>>();
                    AuthnSave.Add(rblConfigurationType.Items[0].Value, ProcessOrganizationCertificate);
                    AuthnSave.Add(rblConfigurationType.Items[1].Value, ProcessOrganizationMetadata);
                    AuthnSave.Add(rblConfigurationType.Items[2].Value, ProcessOrganizationCertificate);
                }
            }
        }

        public override void PopulateForm(Organization obj)
        {
            tab.BindStates(ddlStateID, obj.OrganizationCountryAlpha2Code);

            LoadHandlers.Add(PropHelper.G<OrganizationForm>(_ => _.ddlProdSAID), _ => (_.Organizations_StorageAccounts.First().ProdSAID));
            LoadHandlers.Add(PropHelper.G<OrganizationForm>(_ => _.ddlProdBackupSAID), _ => (_.Organizations_StorageAccounts.First().ProdBackupSAID));
            LoadHandlers.Add(PropHelper.G<OrganizationForm>(_ => _.ddlQASAID), _ => (_.Organizations_StorageAccounts.First().QASAID));
            LoadHandlers.Add(PropHelper.G<OrganizationForm>(_ => _.ddlQABackupSAID), _ => (_.Organizations_StorageAccounts.First().QABackupSAID));
            LoadHandlers.Add(PropHelper.G<OrganizationForm>(_ => _.ddlDevSAID), _ => (_.Organizations_StorageAccounts.First().DevSAID));
            LoadHandlers.Add(PropHelper.G<OrganizationForm>(_ => _.ddlDevBackupSAID), _ => (_.Organizations_StorageAccounts.First().DevBackupSAID));

            base.PopulateForm(obj);
            if (FormMode == PersistModeEnum.Update)
            {
                //set zip validators accordingly
                zipRequiredValidator.Enabled = zipRegExValidator.Enabled = obj.OrganizationCountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtZip.Mask = obj.OrganizationCountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";

                //set state validator accordingly
                stateRequiredValidator.Enabled = ddlStateID.Items.Count > 1;

                #region IdP enabled organization certificate 
                authnCfgFailure.InnerText = "";
                authnCfgFailure.Hide();
                idpConfigArea.Visible = false;
                changeConfiguration.Checked = false;
                if (page.AdminMode == AdminModeEnum.KGS)
                {
                    AuthNSSOInfo results = AuthnHelper.GetSSOInfoAsync(obj.OID).Result; // Blocking call!
                    idpConfigArea.Visible = results.SSOEnabled;
                    if (idpConfigArea.Visible)
                    {
                        SetupIdPConfiguration(results);
                    }
                }
                #endregion IdP enabled organization certificate 
            }
        }
        #endregion

        private void Page_FirstInit()
        {
            if (FormMode == PersistModeEnum.Insert)
            {
                chkIsActive.Checked = true;
            }

            ddlCountryAlpha2Code.Items.Add(new ListItem("Select one...", "-1"));
            ddlStateID.Items.Add(new ListItem("Select country first...", "-1"));

            tab.BindCountries(ddlCountryAlpha2Code);

            if (page.AdminMode == AdminModeEnum.KGS)
            {
                tab.BindStorageAccounts(ddlProdSAID, new StorageAccountCriteria() { StorageAcctLevel = StorageAccountLevel.Primary, IsCommon = false, CWEnvironment = EnvironmentType.Production }, true);
                tab.BindStorageAccounts(ddlProdBackupSAID, new StorageAccountCriteria() { StorageAcctLevel = StorageAccountLevel.Backup, IsCommon = false, CWEnvironment = EnvironmentType.Production }, true);
                tab.BindStorageAccounts(ddlQASAID, new StorageAccountCriteria() { StorageAcctLevel = StorageAccountLevel.Primary, IsCommon = false, CWEnvironment = EnvironmentType.QA });
                tab.BindStorageAccounts(ddlQABackupSAID, new StorageAccountCriteria() { StorageAcctLevel = StorageAccountLevel.Backup, IsCommon = false, CWEnvironment = EnvironmentType.QA });
                tab.BindStorageAccounts(ddlDevSAID, new StorageAccountCriteria() { StorageAcctLevel = StorageAccountLevel.Primary, IsCommon = false, CWEnvironment = EnvironmentType.Development });
                tab.BindStorageAccounts(ddlDevBackupSAID, new StorageAccountCriteria() { StorageAcctLevel = StorageAccountLevel.Backup, IsCommon = false, CWEnvironment = EnvironmentType.Development });
            }
        }

        public Boolean CanOrganizationBePersisted(Organization org, out String message)
        {
            if (DataMgr.OrganizationDataMapper.DoesOrganizationExist(org.OID, org.OrganizationName))
            {
                message = "{EntityTypeName} already exists.";

                return false;
            }

            message = null;

            return true;
        }

        #endregion

        #region property
        private Dictionary<string, Func<int, string>> AuthnSave { get; set; }

        #region override

        public override Delegates<Organization>.CanEntityBePersistedDelegate CanEntityBePersisted
        {
            get { return CanOrganizationBePersisted; }
        }

        public override Action<Organization> PersistEntityAction
        {
            get { return PersistOrganization; }
        }

        #endregion

        #endregion

        public void PersistOrganization(Organization org)
        {
            switch (FormMode)
            {
                case PersistModeEnum.Insert:
                    {
                        DataMgr.OrganizationDataMapper.InsertOrganization(org);
                        DataMgr.OrganizationStorageAccountDataMapper.InsertOrgStorageAccount(org.OID,
                            Convert.ToInt32(ddlProdSAID.SelectedValue), Convert.ToInt32(ddlProdBackupSAID.SelectedValue), Convert.ToInt32(ddlQASAID.SelectedValue), Convert.ToInt32(ddlQABackupSAID.SelectedValue), Convert.ToInt32(ddlDevSAID.SelectedValue), Convert.ToInt32(ddlDevBackupSAID.SelectedValue));
                        break;
                    }
                case PersistModeEnum.Update:
                    {
                        //TODO: deactivate Organization must disable user accounts, cascade deactivate clients, and more
                        if (page.AdminMode == AdminModeEnum.KGS && idpConfigArea.Visible && changeConfiguration.Checked)
                        {
                            string area = rblConfigurationType.SelectedValue;
                            string error = null;
                            if (AuthnSave.ContainsKey(area))
                            {
                                error = AuthnSave[area](org.OID);
                            }
                            else
                            {
                                error = "Did not find save function";
                                ViewTabBase<Organization, OrganizationAdministrationPageBase> t = this.tab as ViewTabBase<Organization, OrganizationAdministrationPageBase>;
                                if (null != t)
                                {
                                    t.KeepEditPanelOpenOnError = true;
                                }
                            }

                            if (!string.IsNullOrEmpty(error))
                            {
                                SetupConfigurationTypeOptionsBehavior();
                                authnCfgFailure.InnerText = error;
                                authnCfgFailure.Show();
                                throw new Exception(error);
                            }
                        }
                        DataMgr.OrganizationDataMapper.UpdateOrganization(org);
                        break;
                    }
            }
        }

        private string ProcessOrganizationCertificate(int orgId)
        {
            string error = null;
            if (!deleteCertificate.Checked && 1 != rauUploadCert.UploadedFiles.Count)
            {
                error = "Public Key File is required";
            }
            else
            {
                byte[] certData = null;
                if (!deleteCertificate.Checked)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        rauUploadCert.UploadedFiles[0].InputStream.CopyTo(ms);
                        certData = ms.ToArray();
                    }
                }
                RESTOrganizationCertificate restOrgCertificate = new RESTOrganizationCertificate(ConfigurationManager.AppSettings["DeploymentID"]);
                bool ok = restOrgCertificate.UpdateOrganizationCertificateAsync(orgId,
                                                                                ddlTokenTypes.SelectedValue,
                                                                                etag.Value,
                                                                                deleteCertificate.Checked,
                                                                                certData
                                                                                ).Result; // Blocking call!;
                if (!ok)
                {
                    error = restOrgCertificate.Response;
                }
            }

            if (!string.IsNullOrEmpty(error))
            {
                ViewTabBase<Organization, OrganizationAdministrationPageBase> t = this.tab as ViewTabBase<Organization, OrganizationAdministrationPageBase>;
                if (null != t)
                {
                    t.KeepEditPanelOpenOnError = true;
                }
            }
            return (error);
        }

        private string ProcessOrganizationMetadata(int orgId)
        {
            string error = null;
            if (!deleteMetadata.Checked && 1 != rauUploadMetadata.UploadedFiles.Count)
            {
                error = "A Metadata File is required";
            }
            else
            {
                byte[] metadata = null;
                if (!deleteMetadata.Checked)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        rauUploadMetadata.UploadedFiles[0].InputStream.CopyTo(ms);
                        metadata = ms.ToArray();
                    }
                }
                RESTOrganizationMetadata restOrgMetadata = new RESTOrganizationMetadata(ConfigurationManager.AppSettings["DeploymentID"]);
                bool ok = restOrgMetadata.UpdateOrganizationMetadataAsync(orgId,
                                                                          etag.Value,
                                                                          deleteMetadata.Checked,
                                                                          metadata
                                                                         ).Result; // Blocking call!;
                if (!ok)
                {
                    error = restOrgMetadata.Response;
                }
            }

            if (!string.IsNullOrEmpty(error))
            {
                ViewTabBase<Organization, OrganizationAdministrationPageBase> t = this.tab as ViewTabBase<Organization, OrganizationAdministrationPageBase>;
                if (null != t)
                {
                    t.KeepEditPanelOpenOnError = true;
                }
            }
            return (error);
        }
        #region Dropdown Events

        protected void ddlCountry_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            tab.BindStates(ddlStateID, ddlCountryAlpha2Code.SelectedValue);

            zipRequiredValidator.Enabled = zipRegExValidator.Enabled = ddlCountryAlpha2Code.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
            txtZip.Mask = ddlCountryAlpha2Code.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            stateRequiredValidator.Enabled = ddlStateID.Items.Count > 1;
        }

        #endregion

        private void SetupConfigurationTypeOptionsBehavior()
        {
            rblConfigurationType.Items[2].Attributes.Add("hidden", "hidden");
            rblConfigurationType.Items.Cast<ListItem>().Where(item => item.Value != "2").ToList().ForEach(item =>
            {
                item.Attributes["onclick"] = "selectUploadType(); clearError();";
            });
        }
        private void SetupIdPConfiguration(AuthNSSOInfo results)
        {
            SetupConfigurationTypeOptionsBehavior();
            changeConfiguration.Attributes["onclick"] = "OnCheckChange(this.checked);clearError();";

            changeConfiguration.Checked = false;
            deleteCertificate.Checked = false;
            deleteCertificate.Attributes["onclick"] = "";
            deleteCertificate.Enabled = false;
            ddlTokenTypes.Items.Clear();

            deleteMetadata.Checked = false;
            deleteMetadata.Attributes["onclick"] = "";
            deleteMetadata.Enabled = false;

            deleteExistingIdPConfig.Value = "";
            aliasOID.InnerText = (results.AliasOID ?? "Not defined");
            etag.Value = results.ETag;
            if (results.TokenTypes.Cast<object>().Any()) // TokenType is populated (never null but can be empty) by DA v2.0.0.4 or higher
            {
                ddlTokenTypes.Attributes["onchange"] = "clearError();";
                ddlTokenTypes.DataTextField = "Name";
                ddlTokenTypes.DataValueField = "Name";
                ddlTokenTypes.Items.Add(new ListItem("", ""));
                ddlTokenTypes.DataSource = results.TokenTypes;
                ddlTokenTypes.DataBind();
                ddlTokenTypes.SelectedValue = results.TokenType ?? "";
            }

            if (results.CertExists)
            {
                deleteExistingIdPConfig.Value = "0";
                deleteCertificate.Attributes["onclick"] = "removeUploadedData('" + rauUploadCert.ClientID + "'); selectUploadType();  clearError();";
                deleteCertificate.Enabled = true;
                rblConfigurationType.SelectedIndex = 0;
            }
            else if (results.AllowMetadata && results.HasMetadata)
            {
                deleteExistingIdPConfig.Value = "1";
                deleteMetadata.Attributes["onclick"] = "removeUploadedData('" + rauUploadMetadata.ClientID + "'); ; selectUploadType();  clearError();";
                deleteMetadata.Enabled = true;
                rblConfigurationType.SelectedIndex = 1;
            }
            else
            {
                deleteExistingIdPConfig.Value = "";
            }

            if (!results.AllowMetadata)
            {
                rblConfigurationType.SelectedIndex = 2; // Only allow Cert upload
            }
            else if(-1 == rblConfigurationType.SelectedIndex || 2 == rblConfigurationType.SelectedIndex)
            {
                // If metadata allowed and rblConfigurationType.SelectedIndex is either unset or 2, default to 0 b/c either cert or metadata can be configured
                rblConfigurationType.SelectedIndex = 0;
            }

            AddIdPConfigurationScripts();
        }
        private void AddIdPConfigurationScripts()
        {
            HtmlGenericControl[] areas = new HtmlGenericControl[] { changeArea, cfgTypeArea, certFileArea, divDeleteCertificate, divUploadCertificate, metadataArea, divDeleteMetadata, divUploadMetadata, aliasArea, divTokenTypes };
            string hideAllAreas = "";
            areas.ToList().ForEach(area =>
            {
                hideAllAreas += @"
                                show(document.getElementById('" + area.ClientID + @"'), true);";
            });

            string script = @"
                                function changeChecked() {
                                    var changeConfiguration = document.getElementById('" + changeConfiguration.ClientID + @"');
                                    return(null != changeConfiguration ? changeConfiguration.checked : false);
                                }

                                function selectUploadType() {
                                    OnCheckChange(changeChecked());
                                }

                                function OnCheckChange(isChecked) {
                                    // Hide all areas" + hideAllAreas + @"

                                    if( isChecked ) {
                                        var cfgType = getConfigurationType();
                                        var deleteExistingIdPConfig = document.getElementById('" + deleteExistingIdPConfig.ClientID + @"');
                                        var deleteExistingIdPConfigValue = deleteExistingIdPConfig.value;
                                        if( '0' == cfgType || '1' == cfgType ) {
                                            // Show cfgTypeArea
                                            show(document.getElementById('" + areas[1].ClientID + @"'));

                                            if ('0' == cfgType) // Certificate
                                            {
                                                // Show delete certificate checkbox if cert exists
                                                if( '0' == deleteExistingIdPConfigValue) {
                                                    show( document.getElementById('" + areas[3].ClientID + @"') );
                                                }
                                                // If delete cert is not checked, show cert upload
                                                if(! document.getElementById('" + deleteCertificate.ClientID + @"').checked ) {
                                                    show( document.getElementById('" + areas[4].ClientID + @"') );
                                                }

                                                var numTokenTypes = document.getElementById('" + ddlTokenTypes.ClientID + @"').length;
                                                if( numTokenTypes > 0) {
                                                    show(document.getElementById('" + areas[9].ClientID + @"'));
                                                }
                                            }
                                            else // Metadata
                                            {
                                                // Show delete metadata checkbox if metadata exists
                                                if( '1' == deleteExistingIdPConfigValue) {
                                                    show( document.getElementById('" + areas[6].ClientID + @"') );
                                                }
                                                // If delete metadata is not checked, show metadata upload
                                                if(! document.getElementById('" + deleteMetadata.ClientID + @"').checked ) {
                                                    show( document.getElementById('" + areas[7].ClientID + @"') );
                                                }
                                            }
                                        }
                                        else if( '2' == cfgType ) { // Cert only
                                            // Show delete certificate checkbox if cert exists
                                            if( '0' == deleteExistingIdPConfigValue) {
                                                show( document.getElementById('" + areas[3].ClientID + @"') );
                                            }
                                            // If delete cert is not checked, show cert upload
                                            if(! document.getElementById('" + deleteCertificate.ClientID + @"').checked ) {
                                                show( document.getElementById('" + areas[4].ClientID + @"') );
                                            }
                                        }
                                        show( document.getElementById('" + areas[8].ClientID + @"') );
                                    }
                                }

                                function getConfigurationType() {
                                    var list = document.getElementById('" + rblConfigurationType.ClientID + @"'); // radiolist
                                    var inputs = list.getElementsByTagName('input');
                                    var selected = '';
                                    for (var i = 0; i < inputs.length; i++) {
                                        if (inputs[i].checked) {
                                            selected = inputs[i].value;
                                            break;
                                        }
                                    }
                                    return(selected);
                                }
        
                                function removeUploadedData(uploadCtrlId) {
                                    var cfgType = getConfigurationType();
                                    if (uploadCtrlId.length > 0) {
                                        var rowsToRemoveCollection = document.getElementsByName('RowRemove');
                                        if (null != rowsToRemoveCollection) {
                                            for (i = 0; i < rowsToRemoveCollection.length; i++) {
                                                var parent = rowsToRemoveCollection[i].parentElement;
                                                if (null != parent && null != parent.id && -1 != parent.id.indexOf(uploadCtrlId)) {
                                                    rowsToRemoveCollection[i].click();
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }

                                function getErrorContainer() {
                                    return(document.getElementById('" + authnCfgFailure.ClientID + @"'));
                                }
                                ";
            ScriptManager.RegisterClientScriptBlock(this, typeof(Page), "IdPScripts", script, true);
            ScriptManager.RegisterStartupScript(this, typeof(Page), "runIdPScripts", "Sys.Application.add_load(selectUploadType);", true);
        }
    }

    static public class HtmlGenericControlExtension
    {
        static public void Hide(this HtmlGenericControl control)
        { 
            control.SetControlClass("hideMe", "showMe");
        }
        static public void Show(this HtmlGenericControl control)
        {
            control.SetControlClass("showMe", "hideMe");
        }
        static public void SetControlClass(this HtmlGenericControl control, string classToAdd, string classToRemove)
        {
            //remove a class
            control.Attributes.Add("class", string.Join(" ", control
                      .Attributes["class"]
                      .Split(' ')
                      .Except(new string[] { "", classToRemove })
                      .ToArray()
              ));
            control.Attributes.Add("class", string.Join(" ", control
                           .Attributes["class"]
                           .Split(' ')
                           .Except(new string[] { "", classToAdd })
                           .Concat(new string[] { classToAdd })
                           .ToArray()
                   ));
        }
    }
}