﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="OrganizationThemeForm.ascx.cs" Inherits="CW.Website._administration.organization.OrganizationThemeForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="FileUploaderControl" Assembly="FileUploaderControl" %>

<asp:HiddenField ID="hdnOrganizationThemeID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnOID" runat="server" Visible="false" />
<div class="divForm">   
    <label class="label">Enable Custom Theme:</label>    
    <asp:CheckBox ID="chkEnableCustomTheme" CssClass="checkbox" runat="server" />
</div>
<div class="divForm">   
    <label class="label">Header BG Color:</label>    
    <telerik:RadColorPicker ID="radpickerHeaderBGColor" ShowIcon="true" PaletteModes="All" Columns="30" KeepInScreenBounds="true" CssClass="radColorPicker" runat="server" />
</div>
<div class="divForm">   
    <label class="label">Header Text Color:</label>        
    <telerik:RadColorPicker ID="radpickerHeaderTextColor" ShowIcon="true" PaletteModes="All" Columns="30" KeepInScreenBounds="true" CssClass="radColorPicker" runat="server" />
</div>
<asp:HiddenField runat="server" ID="hdnLogoExtension" Visible="false" />
<div class="divForm">
    <label class="label">Logo Image (800x600):</label>
    <CW:FileUploader runat="server" ID="logoFileUploader" />
</div>
<asp:HiddenField runat="server" ID="hdnIconExtension" Visible="false" />
<div class="divForm">
    <label class="label">Icon Image (32x32):</label>
    <CW:FileUploader runat="server" ID="iconFileUploader" />
</div>
<br /><br /><br /><br /><br /><br /><br /><br />
<asp:LinkButton CssClass="lnkButton" ID="btnUpdate" runat="server" ValidationGroup="UpdateTheme" />
