﻿using System;

namespace CW.Website._administration.organization.provider
{
    public partial class ProviderOrganizationAdministration: OrganizationAdministrationPageBase
    {
        private void Page_Init()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check if user is orgadmin.
            //is org admin can only be set on superadmins or kgssuperadminsandrhigher
            if (!((siteUser.IsKGSSuperAdminOrHigher && siteUser.IsOrgAdmin) || (siteUser.IsOrgAdmin && siteUser.IsProxyClient && siteUser.IsLoggedInUnderProviderClient)))
            {
                Response.Redirect("/Home.aspx");
            }
        }
    }
}