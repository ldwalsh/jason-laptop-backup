﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TabHeader.ascx.cs" Inherits="CW.Website._administration.TabHeader" %>

<div style="width:100%;">
    <div style="float:left">
        <h2 runat="server" id="headerTitle" />
        <asp:PlaceHolder id="SubTitlePlaceHolder" runat="server" />
        <a runat="server" id="lnkSetFocusMessage" />
        <asp:Label runat="server" ID="lblMessage" Visible="false" />
    </div>
    <div style="float:right;">
        <asp:PlaceHolder id="RightAreaPlaceHolder" runat="server" />
    </div>
</div>
<div style="clear:both;"></div>