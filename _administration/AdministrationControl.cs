﻿using System;
using System.Web.UI;

namespace CW.Website._administration
{
    public class AdministrationControl<TAdministrationPage> where TAdministrationPage: AdministrationPageBaseBase
    {
        #region field

            private readonly Control control;

            private TAdministrationPage _parent;

        #endregion

        #region constructor

            public AdministrationControl(Control control)
            {
                this.control = control;
            }

        #endregion

        #region property

            public TAdministrationPage page
            {
                get
                {
                    if (_parent == null)
                    {
                        _parent = (TAdministrationPage)control.Page;
                    }

                    return _parent;
                }
            }

        #endregion
    }
}