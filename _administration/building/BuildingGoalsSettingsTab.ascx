﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingGoalsSettingsTab.ascx.cs" Inherits="CW.Website._administration.building.BuildingGoalsSettingsTab" %>
<%@ Register src="~/_administration/TabHeader.ascx" tagPrefix="CW" tagName="TabHeader" %>
<%@ Register src="~/_administration/_shared/Goals/GoalsSettingsView.ascx" tagPrefix="CW" tagName="GoalsSettingsView" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Goals Settings" />

<script type="text/javascript" src="/_assets/scripts/goalViewLogic.js"></script>
<script type="text/javascript" src="/_administration/_script/TabStateMonitor.js?v=_assets/"></script>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblMessage" CssClass="successMessage" runat="server" />
</p> 

<div class="divForm">
  <label class="label">*Select Building:</label>
  <asp:DropDownList CssClass="dropdown" ID="ddlBuildings" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<div class="goalSettingsSubTab" id="goalsSettingsDiv" runat="server">
    <div class="divForm">
        <label class="label">Enable:</label>
        <asp:CheckBox ID="chkEnable" runat="server" CssClass="checkbox chkEnableGoals" onchange="KGS.ChkEnableClicked()" OnCheckedChanged="chkEnable_CheckChanged" AutoPostBack="false"/>
    </div>

    <div class="divForm inheritDiv"  runat="server">
        <label class="label">Inherit Client Goals:</label>
        <asp:CheckBox ID="chkInherit" runat="server" CssClass="checkbox chkInheritGoals" onchange="KGS.ChkInheritClicked();" OnCheckedChanged="chkInherit_CheckChanged" AutoPostBack="false"/>
    </div>

    <div class="goalSettings" style="display: none;">
        <CW:GoalsSettingsView ID="GoalsSettingsView" runat="server"/>
    </div>

    <div class="divForm saveButton">       
        <asp:LinkButton ID="btnSave" runat="server" Text="Save" CssClass="lnkButton" OnClick="btnSave_Click"/>    
    </div>

    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />
</div>
