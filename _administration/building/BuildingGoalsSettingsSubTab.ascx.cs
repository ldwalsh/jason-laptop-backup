﻿using CW.Data;
using CW.Website._administration._shared.Goals;
using CW.Website._administration.client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._administration.building
{
    public partial class BuildingGoalsSettingsSubTab : TabBase
    {
        GoalsViewManager<BuildingGoal> _viewManager;

        GoalsViewManager<BuildingGoal> ViewManager => _viewManager = _viewManager ??
            new GoalsViewManager<BuildingGoal>(this.GoalsSettingsView, this.DataMgr, 1);//last param bogus
        
        #region override
        protected override void UpdateButton_Click(Object sender, EventArgs e) { }
        #endregion

        protected void Page_FirstLoad(object sender, EventArgs e)
        {
            ViewManager.ConfigureGoalViewState();
            setChkEnabled();
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ViewManager.SaveGoals();
        }

        #region methods
        protected void chkEnable_CheckChanged(object sender, EventArgs e)
        {
            bool isEnabled = true;
            bool success = false;

            if (bool.TryParse(Request["__EventArgument"], out isEnabled))
            {
                GoalService goalService = new GoalService();

                if (isEnabled)
                {
                    success = goalService.CreateClientGoalsEnabledIfNotExists(siteUser.CID);
                    chkEnable.Checked = true;
                }
                else
                {
                    success = goalService.RemoveClientGoalsEnabledIfExists(siteUser.CID);
                    chkEnable.Checked = false;
                }
            }

            ViewManager.UpdateGoalView();
        }

        private void setChkEnabled()
        {
            GoalService goalService = new GoalService();

            chkEnable.Checked = goalService.GetBuildingGoalsEnabledByBID(1) != null;
        }
    }
    #endregion

}