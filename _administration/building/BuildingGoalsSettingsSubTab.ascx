﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingGoalsSettingsSubTab.ascx.cs" Inherits="CW.Website._administration.building.BuildingGoalsSettingsSubTab" %>
<%@ Register src="~/_administration/_shared/Goals/GoalsSettingsView.ascx" tagPrefix="CW" tagName="GoalsSettingsView" %>

<div class="divForm">
  <label class="label">Enable:</label>
  <asp:CheckBox ID="chkEnable" runat="server" CssClass="checkbox chkEnableGoals" onchange="KGS.ChkEnableClicked()" OnCheckedChanged="chkEnable_CheckChanged" AutoPostBack="false"/>
</div>

<div class="goalSettings" style="display: none;">
    <CW:GoalsSettingsView ID="GoalsSettingsView" runat="server"/>
</div>

<div class="divForm goalSettings">       
    <asp:LinkButton ID="btnSave" runat="server" Text="Save" CssClass="lnkButton" OnClick="btnSave_Click"/>    
</div>