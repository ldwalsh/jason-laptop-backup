﻿using CW.Data;
using CW.Utility.Web;
using CW.Website._administration._shared.Goals;
using CW.Website._administration.client;
using CW.Website._controls.admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static CW.Website._administration.client.GoalService;

namespace CW.Website._administration.building
{
    public partial class BuildingGoalsSettingsTab : AdminUserControlBase
    {
        GoalsViewManager _viewManager;
        GoalsViewManager ViewManager => _viewManager = _viewManager ??
            new GoalsViewManager(this.GoalsSettingsView, this.DataMgr, parseBuildingID(ddlBuildings.SelectedValue), ServiceGoalType.Building);

        protected void Page_FirstLoad(object sender, EventArgs e)
        {
            BindBuildings(ddlBuildings);
            ToggleGoalSectionVisibility();
            ViewManager.ConfigureGoalViewState();            
            setChkBoxes();            
        }       

        protected void Page_Load(object sender, EventArgs e)
        {            
            setChkBoxes();            
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string result = ViewManager.SaveGoals();
            
            if(result.ToLower().Contains("success"))
            {
                LabelHelper.SetLabelMessage(lblResults, result, lnkSetFocus);
            }
            else
            {
                LabelHelper.SetLabelMessage(lblAddError, result, lnkSetFocus);
            }            
        }

        #region methods
        protected void chkEnable_CheckChanged(object sender, EventArgs e)
        {
            bool isEnabled = true;
            bool success = false;

            if (bool.TryParse(Request["__EventArgument"], out isEnabled))
            {
                GoalService goalService = new GoalService(ServiceGoalType.Building);
                int bid = getCurrentBID();            
                
                if (bid > 0 && isEnabled)
                {
                    success = goalService.CreateBuildingGoalsEnabledIfNotExists(bid);

                    if (success)
                    {
                        chkEnable.Checked = true;
                        LabelHelper.SetLabelMessage(lblResults, "Goals enabled successfully.", lnkSetFocus);
                        lblAddError.Visible = false;
                    }
                    else
                    {
                        chkEnable.Checked = false;
                        LabelHelper.SetLabelMessage(lblAddError, "Error enabling goals.", lnkSetFocus);
                        lblResults.Visible = false;
                    }                    
                }
                else if (bid > 0 )
                {
                    success = goalService.RemoveBuildingGoalsEnabledIfExists(bid);

                    if (success)
                    {
                        chkEnable.Checked = false;
                        LabelHelper.SetLabelMessage(lblResults, "Goals disabled successfully.", lnkSetFocus);
                        lblAddError.Visible = false;
                    }
                    else
                    {
                        chkEnable.Checked = true;
                        LabelHelper.SetLabelMessage(lblAddError, "Error disabling goals.", lnkSetFocus);
                        lblResults.Visible = false;
                    }                    
                }

                setChkInherit(goalService, bid);
            }          

            ViewManager.UpdateGoalView();
        }
        
        protected void chkInherit_CheckChanged(object sender, EventArgs e)
        {
            bool inherits = false;
            bool success = false;

            if (bool.TryParse(Request["__EventArgument"], out inherits))
            {
                GoalService goalService = new GoalService(ServiceGoalType.Building);

                int bid = getCurrentBID();

                if(bid >0)
                {
                    success = goalService.SetBuildingGoalsEnabledInherited(bid, inherits);
                }                
                
                if(inherits)
                {
                    if(success)
                    {
                        chkInherit.Checked = true;
                        LabelHelper.SetLabelMessage(lblResults, "Goals successfully inherited. Goals for this building now inherit from client goals.", lnkSetFocus);
                        lblAddError.Visible = false;
                    }
                    else
                    {
                        chkInherit.Checked = false;
                        LabelHelper.SetLabelMessage(lblAddError, "Error inheriting goals.", lnkSetFocus);
                        lblResults.Visible = false;
                    }
                }
                else
                {
                    if(success)
                    {
                        chkInherit.Checked = false;
                        LabelHelper.SetLabelMessage(lblResults, "Client goals successfully overridden. Goals for this building are now custom.", lnkSetFocus);
                        lblAddError.Visible = false;
                    }
                    else
                    {
                        chkInherit.Checked = true;
                        LabelHelper.SetLabelMessage(lblAddError, "Error overriding client goals.", lnkSetFocus);
                        lblResults.Visible = false;
                    }
                } 
            }
            ViewManager.UpdateGoalView();
        }

        private void setChkBoxes()
        {
            int bid;
            GoalService goalService = new GoalService(ServiceGoalType.Building);

            if (int.TryParse(ddlBuildings.SelectedValue, out bid))
            {
                setChkEnabled(goalService, bid);
                setChkInherit(goalService, bid);                
            }         
        }

        private void setChkEnabled(GoalService goalService, int bid)
        {
            chkEnable.Checked = goalService.GetBuildingGoalsEnabledByBID(bid) != null;
        }

        private void setChkInherit(GoalService goalService, int bid)
        {
            BuildingGoalsEnabled buildingGoalsEnabled = goalService.GetBuildingGoalsEnabledByBID(bid);
            chkInherit.Checked = buildingGoalsEnabled == null ? false : buildingGoalsEnabled.InheritClientGoals;
        }

        protected void ddlBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
        {            
            ToggleGoalSectionVisibility();
            ViewManager.UpdateGoalView();
            lblAddError.Visible = false;
            lblResults.Visible = false;
        }

        private void BindBuildings(DropDownList ddl, Boolean isDDLRefresh = false)
        {
            BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
        }

        private void ToggleGoalSectionVisibility()
        {
            goalsSettingsDiv.Visible = ddlBuildings.SelectedValue == "-1" ? false : true;
        }

        private int parseBuildingID(string id)
        {
            int result = -1;

            if (int.TryParse(id, out result))
            {
                return result;
            }
            else
            {
                return -1;
            }
        }

        private int getCurrentBID()
        {
            int bid;
            bool isBid = int.TryParse(ddlBuildings.SelectedValue, out bid);

            if(isBid)
            {
                return bid;
            }
            else
            {
                return -1;
            }
        }
        #endregion
    }
}