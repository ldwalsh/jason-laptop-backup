﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UserGroupForm.ascx.cs" Inherits="CW.Website._administration.userGroup.UserGroupForm" %>
<%@ Register tagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:HiddenField ID="hdnUserGroupID" runat="server" Visible="false" />
<div class="divForm">
    <label class="label">*User Group Name:</label>
    <asp:TextBox ID="txtUserGroupName" CssClass="textbox" MaxLength="50" runat="server" />
</div> 
<%--<div class="divForm">
    <label class="label">*Organization:</label>
    <asp:DropDownList 
        CssClass="dropdown" 
        ID="ddlOID" 
        OnSelectedIndexChanged="ddlOID_OnSelectedIndexChanged" 
        AppendDataBoundItems="true" 
        AutoPostBack = "true"
        runat="server">
        <asp:ListItem Value="-1">Select one...</asp:ListItem>
    </asp:DropDownList>
</div>
<div class="divForm">
    <label class="label">*Minimum Role:</label>
    <asp:DropDownList 
        CssClass="dropdown" 
        ID="ddlRoleID" 
        OnSelectedIndexChanged="ddlRoleID_OnSelectedIndexChanged" 
        AppendDataBoundItems="true" 
        AutoPostBack = "true"
        runat="server">
        <asp:ListItem Value="-1">Select one...</asp:ListItem>
    </asp:DropDownList>
</div>-->
<div class="divForm">
    <label runat="server" class="label">Include KGS Users</label>
    <asp:CheckBox ID="chkHasKGSUsers" CssClass="checkbox" OnCheckedChanged="chkHasKGSUsers_OnCheckedChanged" AutoPostBack="true" runat="server" />                                                        
</div>--%> 
<div class="divForm">
    <label class="label">Users Available:</label>
    <asp:ListBox ID="lbUsersAvailable" CssClass="listboxWide" runat="server" SelectionMode="Multiple" />
    <div class="divArrowsWide">
        <asp:ImageButton 
            CssClass="up-arrow"  
            ID="btnUnassignUserButton" 
            OnClick="btnUnassignUserButton_Click" 
            ImageUrl="/_assets/images/up-arrow.png" 
            runat="server" />
        <asp:ImageButton 
            CssClass="down-arrow" 
            ID="btnAssignUserButton" 
            OnClick="btnAssignUserButton_Click" 
            ImageUrl="/_assets/images/down-arrow.png" 
            runat="server" />                                            
    </div>
    <label class="label">Users Assigned:</label>
    <asp:ListBox ID="lbUsersAssigned" CssClass="listboxWide" runat="server" SelectionMode="Multiple" />
</div>  
<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />
<asp:RequiredFieldValidator ID="userGroupNameRequiredValidator" runat="server"
    ErrorMessage="User Group is a required field."
    ControlToValidate="txtUserGroupName"          
    SetFocusOnError="true"
    Display="None"
    ValidationGroup="UserGroupForm">
    </asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="userGroupNameRequiredValidatorExtender" runat="server"
    TargetControlID="userGroupNameRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
</ajaxToolkit:ValidatorCalloutExtender> 
<%--<asp:RequiredFieldValidator ID="organizationRequiredValidator" runat="server"
    ErrorMessage="Organization is a required field." 
    ControlToValidate="ddlOID"  
    SetFocusOnError="true" 
    Display="None" 
    InitialValue="-1"
    ValidationGroup="UserGroupForm">
</asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="organizationRequiredValidatorExtender" runat="server"
    TargetControlID="organizationRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
</ajaxToolkit:ValidatorCalloutExtender> 
<asp:RequiredFieldValidator ID="roleRequiredValidator" runat="server"
    ErrorMessage="Minimum Role is a required field." 
    ControlToValidate="ddlRoleID"  
    SetFocusOnError="true" 
    Display="None" 
    InitialValue="-1"
    ValidationGroup="UserGroupForm">
</asp:RequiredFieldValidator>
<ajaxToolkit:ValidatorCalloutExtender ID="roleRequiredValidatorExtender" runat="server"
    TargetControlID="roleRequiredValidator"
    HighlightCssClass="validatorCalloutHighlight"
    Width="175">
</ajaxToolkit:ValidatorCalloutExtender> --%>

