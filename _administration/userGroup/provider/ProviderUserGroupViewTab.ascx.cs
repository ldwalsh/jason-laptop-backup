﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._administration.userGroup.provider
{
    public partial class ProviderUserGroupViewTab : ViewTabWithFormBase<UserGroup, ProviderUserGroupAdministration>
    {
		#region method

			private void Page_Init()
			{
				CanEntityBeUpdated = userGroupForm.CanEntityBePersisted;
				CanEntityBeDeleted = CanUserGroupBeDeleted;

				UpdateEntityMethod = userGroupForm.PersistEntityAction;
            
				DeleteEntityMethod = DeleteUserGroup;
			}

			private Boolean CanUserGroupBeDeleted(String userGroupID, out String message)
			{
				var id = Convert.ToInt32(userGroupID);
            
				//No Checks
				//We just delete the usergroup from any collections now.
				//We just delete the usergroup users now as well.

				message = null;

				return true;
			}

			private void DeleteUserGroup(String userGroupIDString)
			{
				int userGroupID = 0;
				Int32.TryParse(userGroupIDString, out userGroupID);
            
				DataMgr.UserGroupDataMapper.DeleteAllUserGroupUsersByUserGroupID(userGroupID);
				DataMgr.UserGroupDataMapper.DeleteUserGroup(userGroupID);
				page.TabStateMonitor.ChangeState();
			}

			private UserGroup GetUserGroupForDetailView(String userGroupID)
			{
				//var userGroup = mDataManager.UserGroupDataMapper.GetUserGroup(Convert.ToInt32(userGroupID), (_ => _.Client), (_ => _.UserRole), (_ => _.UserGroups_Users));
				var userGroup = DataMgr.UserGroupDataMapper.GetUserGroup(Convert.ToInt32(userGroupID), (_ => _.Organization), (_ => _.UserGroups_Users));

				return userGroup;
			}

			private void BindUsersRepeater(int userGroupID)
			{
				if (EntityDetailView.CurrentMode == DetailsViewMode.ReadOnly)
				{
					Repeater rptUsers = (Repeater)EntityDetailView.FindControl("rptUsers");

					rptUsers.DataSource = DataMgr.UserGroupDataMapper.GetAllUsersByUserGroupID(userGroupID);
					rptUsers.DataBind();
				}
			}


			protected override void GridSelectedIndexChanged(Object sender, EventArgs e)
			{
				string id = grid.DataKeys[grid.SelectedIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString();

				PanelEditEntity.Visible = false;

				EntityDetailView.Visible = true;
				EntityDetailView.DataSource = new[] { GetEntityForDetailView(id) };

				EntityDetailView.DataBind();

				BindUsersRepeater(Convert.ToInt32(id));
			}

		#endregion

		#region property

			protected override String CacheSubKey
			{
				get { return siteUser.ClientOID.ToString(); }
			}

			protected override IEnumerable<String> GridColumnNames
			{
				get {return PropHelper.F<UserGroup>(_=>_.UserGroupName, _=>_.Organization.OrganizationName);}
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get { return (searchText => DataMgr.UserGroupDataMapper.GetAllSearchedUserGroups(siteUser.ClientOID, searchText.FirstOrDefault(), (_ => _.Organization))); }
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get { return (userGroupID => DataMgr.UserGroupDataMapper.GetUserGroup(Convert.ToInt32(userGroupID), (_ => _.Organization), (_ => _.UserGroups_Users))); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get { return GetUserGroupForDetailView; }
			}

		#endregion
    }
}