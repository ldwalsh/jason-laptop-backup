﻿using CW.Data;

namespace CW.Website._administration.userGroup.provider
{
    public partial class ProviderUserGroupAddTab : AddTabBase<UserGroup, ProviderUserGroupAdministration>
    {
        private void Page_Init()
        {
            CanEntityBeAdded = userGroupForm.CanEntityBePersisted;
            AddEntityMethod = userGroupForm.PersistEntityAction;
        }
    }
}