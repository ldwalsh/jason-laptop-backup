﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CW.Data;
using CW.Utility;

namespace CW.Website._administration.userGroup
{
    public abstract class UserGroupAdministrationPageBase : AdministrationPageBase
    {
        public override String IdentifierField
        {
            get { return PropHelper.G<UserGroup>(ug => ug.UserGroupID); }
        }

        public override String NameField
        {
            get { return PropHelper.G<UserGroup>(c => c.UserGroupName); }
        }

        public override String EntityTypeName
        {
            get { return typeof(UserGroup).Name; }
        }
    }
}