﻿using CW.Data;

namespace CW.Website._administration.userGroup.kgs
{
    public partial class KGSUserGroupAddTab : AddTabBase<UserGroup, KGSUserGroupAdministration>
    {
        private void Page_Init()
        {
            CanEntityBeAdded = userGroupForm.CanEntityBePersisted;
            AddEntityMethod = userGroupForm.PersistEntityAction;
        }
    }
}