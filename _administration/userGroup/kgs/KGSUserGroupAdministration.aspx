﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" AutoEventWireup="false" CodeBehind="KGSUserGroupAdministration.aspx.cs" Inherits="CW.Website._administration.userGroup.kgs.KGSUserGroupAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="KGSUserGroupAddTab.ascx" tagPrefix="CW" tagName="UserGroupAddTab" %>
<%@ Register src="KGSUserGroupViewTab.ascx" tagPrefix="CW" tagName="UserGroupViewTab" %>


<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">   

            <CW:PageHeader ID="PageHeader1" runat="server" Title="KGS User Group Administration" Description="<p>The kgs user group administration area is used to view, add, and edit user groups.</p><p>User groups are used for building and client level settings, as well as bureau reports. Including: data source notifications, priority emails, and scheduled bureau reports.</p><p>User groups do not factor in a users building or building group restrictions.</p><p>User groups do not factor in client restrictions.</p>" />
            <div class="administrationControls">
                <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
                    <Tabs>
                        <telerik:RadTab Text="View User Groups"></telerik:RadTab>
                        <telerik:RadTab Text="Add User Groups"></telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
                <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                    <telerik:RadPageView ID="RadPageView1" runat="server">
                            <CW:UserGroupViewTab ID="UserGroupViewTab1" runat="server"/>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server">
                            <CW:UserGroupAddTab ID="UserGroupAddTab1" runat="server" />
                    </telerik:RadPageView>
                </telerik:RadMultiPage>
            </div>
 
</asp:Content>