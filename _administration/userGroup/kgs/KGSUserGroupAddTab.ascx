﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSUserGroupAddTab.ascx.cs" Inherits="CW.Website._administration.userGroup.kgs.KGSUserGroupAddTab" %>
<%@ Register src="../UserGroupForm.ascx" tagPrefix="CW" tagName="UserGroupForm" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add User Groups" />  
<CW:UserGroupForm runat="server" ID="userGroupForm" FormMode="Insert" UpdateButtonCaption="Add User Group" />
