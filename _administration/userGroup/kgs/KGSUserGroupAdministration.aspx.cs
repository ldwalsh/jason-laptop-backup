﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._administration.userGroup.kgs
{
    public partial class KGSUserGroupAdministration : UserGroupAdministrationPageBase
    {
        private void Page_Init()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check kgs super admin or higher.
            if (!siteUser.IsKGSSuperAdminOrHigher)
                Response.Redirect("/Home.aspx");
        }
    }
}