﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSUserGroupViewTab.ascx.cs" Inherits="CW.Website._administration.userGroup.kgs.KGSUserGroupViewTab" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" src="../UserGroupForm.ascx" tagName="UserGroupForm" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View User Groups" />  
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>                                                                                                                                                     
<div id="gridTbl">
    <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true" >
        <Columns>
            <asp:CommandField ShowSelectButton="true" />
            <asp:TemplateField>
                <ItemTemplate>                                                
                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                    
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="UserGroupName" HeaderText="User Group Name">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<UserGroup>(_ => _.UserGroupName)) %>"><%# GetCellContent(Container, PropHelper.G<UserGroup>(_=>_.UserGroupName), 25) %></label>
                </ItemTemplate>
            </asp:TemplateField>   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="OrganizationName" HeaderText="Organization Name">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Organization>(_ => _.OrganizationName)) %>"><%# GetCellContent(Container, PropHelper.G<Organization>(_=>_.OrganizationName), 25) %></label>
                </ItemTemplate>
            </asp:TemplateField> 
            <%--<asp:TemplateField ItemStyle-Wrap="true" SortExpression="RoleName" HeaderText="Role Name">  
                <ItemTemplate>
                    <%# GetCellContent(Container, PropHelper.G<UserRole>(_=>_.RoleName), 25)%>
                </ItemTemplate>
            </asp:TemplateField> --%>
            <asp:TemplateField>
                <ItemTemplate>                                                
                    <asp:LinkButton ID="LinkButton2" Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this user group permanently?');" CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                                                                                          
        </Columns>
    </asp:GridView>                                                                                                             
</div>
<br />
<br /> 
<!--SELECT USER GROUP DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
    <Fields>
        <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
            <ItemTemplate>
                <div>
                    <h2>User Group Details</h2>                                                                                                                                            
                    <ul class="detailsListNarrow">
                        <CW:UListItem ID="UListItem1" runat="server" Label="User Group Name" Value="<%# Eval(PropHelper.G<UserGroup>(ug=>ug.UserGroupName)) %>" />
                        <CW:UListItem ID="UListItem2" runat="server" Label="Organization Name" Value="<%# Eval(PropHelper.G<UserGroup, Organization>(ug => ug.Organization, c => c.OrganizationName)) %>" />
                        <%--<CW:UListItem ID="UListItem3" runat="server" Label="Role Name" Value="<%# Eval(PropHelper.G<UserGroup, UserRole>(ug => ug.UserRole, ur => ur.RoleName)) %>" /> --%>
                        <asp:Repeater ID="rptUsers" runat="server">
                            <HeaderTemplate><li><strong>Users:</strong></HeaderTemplate>
                            <ItemTemplate><div><%# Eval("LastName") + ", " + Eval("FirstName") + "(" + Eval("Email") + ")" %></div></ItemTemplate>
                            <FooterTemplate></li></FooterTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </ItemTemplate>                                             
        </asp:TemplateField>
    </Fields>
</asp:DetailsView>
                                                                     
<!--EDIT User Group PANEL -->                              
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">                                                                                             
    <div>
        <h2>Edit User Group</h2>
    </div>  
    <div>
        <CW:UserGroupForm ID="userGroupForm" runat="server" FormMode="Update" UpdateButtonCaption="Edit User Group" UserGroupNameEnabled="true" IsActiveVisible="true" />
    </div>
</asp:Panel>
