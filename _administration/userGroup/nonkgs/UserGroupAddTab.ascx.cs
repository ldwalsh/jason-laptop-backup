﻿using CW.Data;

namespace CW.Website._administration.userGroup.nonkgs
{
    public partial class UserGroupAddTab : AddTabBase<UserGroup, UserGroupAdministration>
    {
        private void Page_Init()
        {
            CanEntityBeAdded = userGroupForm.CanEntityBePersisted;
            AddEntityMethod = userGroupForm.PersistEntityAction;
        }
    }
}