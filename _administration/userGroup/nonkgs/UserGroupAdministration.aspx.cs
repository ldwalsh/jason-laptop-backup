﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._administration.userGroup.nonkgs
{
    public partial class UserGroupAdministration : UserGroupAdministrationPageBase
    {
        private void Page_Init()
        {
            //logged in security check in base page.
            //secondary security check specific to this page.
            //Check kgs super admin or higher, or super admin non provider, or super admin provider proxy.
            if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && !siteUser.IsLoggedInUnderProviderClient) || (siteUser.IsSuperAdmin && siteUser.IsProxyClient && siteUser.IsLoggedInUnderProviderClient)))
                Response.Redirect("/Home.aspx");
        }
    }
}