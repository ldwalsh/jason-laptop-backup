﻿using CW.Business;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._administration.userGroup
{
    public partial class UserGroupForm : FormGenericBase<UserGroupForm, UserGroup>
    {
        #region method

            #region override
                public override void PopulateForm(UserGroup userGroup)
                {
                    IEnumerable<User> assignedUsers;
                    IEnumerable<User> availableUsers;
                    SetUsers(userGroup.UserGroupID, userGroup.OID, out assignedUsers, out availableUsers);

                    BindUserListBox(lbUsersAssigned, assignedUsers);
                    BindUserListBox(lbUsersAvailable, availableUsers);

                    //LoadHandlers.Add(PropHelper.G<UserGroupForm>(f => f.ddlClientID), ClientLoadHandler);

                    base.PopulateForm(userGroup);
                }

                private void SetUsers(int? userGroupID, int oid, out IEnumerable<User> assignedUsers, out IEnumerable<User> availableUsers)
                {
                    //IEnumerable<User> kgsUsers = Enumerable.Empty<User>();
                    //kgsUsers = mDataManager.UserDataMapper.GetKGSUsers(roleID);
                    assignedUsers = Enumerable.Empty<User>();

                    //availableUsers = mDataManager.UserDataMapper.GetAllUsersWithMinimumRole(cid, roleID);//.Union(kgsUsers, new UserDataMapper.UserComparer());
                    availableUsers = DataMgr.UserDataMapper.GetAllUsersByOID(oid, page.AdminMode == AdminModeEnum.KGS);
                    
                    if (userGroupID != null)
                    {
                        assignedUsers = DataMgr.UserGroupDataMapper.GetAllUsersByUserGroupID((int)userGroupID, page.AdminMode == AdminModeEnum.KGS).Intersect(availableUsers, new UserDataMapper.UserComparer()).ToList();
                        availableUsers = availableUsers.Except(assignedUsers, new UserDataMapper.UserComparer()).ToList();
                    }
                }

                //public override void CreateObject(UserGroup obj)
                //{
                //    SaveHandlers.Add(PropHelper.G<UserGroupForm>(f => f.ddlOID), OrganizationSaveHandler);

                //    base.CreateObject(obj);
                //}

            #endregion 

            private void Page_FirstInit()
            {
                IEnumerable<User> assignedUsers;
                IEnumerable<User> availableUsers;
                SetUsers(null, siteUser.ClientOID, out assignedUsers, out availableUsers);

                BindUserListBox(lbUsersAssigned, assignedUsers);
                BindUserListBox(lbUsersAvailable, availableUsers);

                //tab.BindOrganizations(ddlOID);

                //if (FormMode == PersistModeEnum.Update)
                    //ddlOID.Enabled = false;

                //IEnumerable<UserRole> roles = RoleHelper.FilterRestrictedRoles(mDataManager.UserRoleDataMapper.GetAllRoles());
                //tab.BindRoles(ddlRoleID, roles);
            }

            public Boolean CanUserGroupBePersisted(UserGroup userGroup, out String message)
            {
                if (DataMgr.UserGroupDataMapper.DoesUserGroupExist(userGroup.UserGroupID, userGroup.UserGroupName, userGroup.OID))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

            public void PersistUserGroup(UserGroup userGroup)
            {
                switch (FormMode)
                {
                    case PersistModeEnum.Insert:
                        {
                            userGroup.OID = siteUser.ClientOID;
                            userGroup.DateModified = DateTime.Now;
                            int userGroupID = DataMgr.UserGroupDataMapper.InsertUserGroup(userGroup);

                            foreach (ListItem item in lbUsersAssigned.Items)
                            {
                                var userGroups_User = new UserGroups_User();

                                //set values
                                userGroups_User.UID = Convert.ToInt32(item.Value);
                                userGroups_User.UserGroupID = userGroupID;

                                //insert
                                DataMgr.UserGroupDataMapper.InsertUserGroupUser(userGroups_User);
                            }

                            break;
                        }
                    case PersistModeEnum.Update:
                        {
                            DataMgr.UserGroupDataMapper.UpdateUserGroup(userGroup);

                            DataMgr.UserGroupDataMapper.DeleteAllUserGroupUsersByUserGroupID(userGroup.UserGroupID);

                            foreach (ListItem item in lbUsersAssigned.Items)
                            {
                                var userGroups_User = new UserGroups_User();

                                //set values
                                userGroups_User.UID = Convert.ToInt32(item.Value);
                                userGroups_User.UserGroupID = userGroup.UserGroupID;
                                
                                //insert
                                DataMgr.UserGroupDataMapper.InsertUserGroupUser(userGroups_User);
                            }

                            break;
                        }
                }
            }

            //private void OrganizationSaveHandler(UserGroup userGroup)
            //{
            //    userGroup.OID = siteUser.ClientOID; // Int32.Parse(ddlOID.SelectedValue);
            //}

            //private object ClientLoadHandler(UserGroup userGroup)
            //{
            //    return userGroup.CID;
            //}

            public void BindUserListBox(ListBox lb, IEnumerable<User> users)
            {
                ControlHelper.BindListBoxWithTuple(lb, from u in users
                                              orderby u.LastName
                                              select Tuple.Create<string, string>(String.Format("{0},{1}({2})", u.LastName, u.FirstName, u.Email), u.UID.ToString())
                                         );
            }

            //protected void chkHasKGSUsers_OnCheckedChanged(Object sender, EventArgs e)
            //{
            //    IEnumerable<User> assignedUsers;
            //    IEnumerable<User> availableUsers;
            //    SetDataSources(Convert.ToInt32(hdnUserGroupID.Value), Convert.ToInt32(ddlCID.SelectedValue), Convert.ToInt32(ddlRoleID.SelectedValue), out assignedUsers, out availableUsers);

            //    BindUserListBox(lbUsersAvailable, availableUsers);
            //    BindUserListBox(lbUsersAssigned, assignedUsers);
            //}

            //protected void ddlCID_OnSelectedIndexChanged(Object sender, EventArgs e)
            //{
            //    if (ddlCID.SelectedValue == "-1") return;

            //    ddlRoleID.SelectedValue = "-1";
            //    lbUsersAvailable.Items.Clear();
            //    lbUsersAssigned.Items.Clear();

            //}

            //protected void ddlRoleID_OnSelectedIndexChanged(Object sender, EventArgs e)
            //{
            //    if (ddlRoleID.SelectedValue == "-1") return;

            //    IEnumerable<User> assignedUsers;
            //    IEnumerable<User> availableUsers;
            //    SetDataSources(Convert.ToInt32(ddlRoleID.SelectedValue), Convert.ToInt32(ddlCID.SelectedValue), Convert.ToInt32(ddlRoleID.SelectedValue), out assignedUsers, out availableUsers);

            //    BindUserListBox(lbUsersAvailable, availableUsers);
            //    BindUserListBox(lbUsersAssigned, assignedUsers);
            //}


            //protected void ddlOID_OnSelectedIndexChanged(Object sender, EventArgs e)
            //{
            //    //if (ddlRoleID.SelectedValue == "-1") return;

            //    IEnumerable<User> assignedUsers;
            //    IEnumerable<User> availableUsers;
            //    SetDataSources(null, Convert.ToInt32(ddlOID.SelectedValue), out assignedUsers, out availableUsers);

            //    BindUserListBox(lbUsersAvailable, availableUsers);
            //    BindUserListBox(lbUsersAssigned, assignedUsers);
            //}

            protected void btnUnassignUserButton_Click(Object sender, EventArgs e)
            {
                while (lbUsersAssigned.SelectedIndex != -1)
                {
                    lbUsersAvailable.Items.Add(lbUsersAssigned.SelectedItem);
                    lbUsersAssigned.Items.Remove(lbUsersAssigned.SelectedItem);
                }
            }

            protected void btnAssignUserButton_Click(Object sender, EventArgs e)
            {
                while (lbUsersAvailable.SelectedIndex != -1)
                {
                    lbUsersAssigned.Items.Add(lbUsersAvailable.SelectedItem);
                    lbUsersAvailable.Items.Remove(lbUsersAvailable.SelectedItem);
                }    
            }
        
        #endregion 

        #region property

        public override Delegates<UserGroup>.CanEntityBePersistedDelegate CanEntityBePersisted
        {
            get { return CanUserGroupBePersisted; }
        }

        public override Action<UserGroup> PersistEntityAction
        {
            get { return PersistUserGroup; }
        }

        #endregion
    }
}