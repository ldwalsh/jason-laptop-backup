﻿using CW.Data.Extensions;
using System;

namespace CW.Website._administration
{
    public abstract class AdministrationPageBase: AdministrationPageBaseBase
    {
        #region constructor

            protected AdministrationPageBase()
            {
                switch (GetType().BaseType.Namespace.Split('.').GetLast().ToLower())
                {
                    case "kgs":
                    {
                        AdminMode = AdminModeEnum.KGS;

                        break;
                    }
                    case "provider":
                    {
                        AdminMode = AdminModeEnum.PROVIDER;

                        break;
                    }
                    case "nonkgs":
                    {
                        AdminMode = AdminModeEnum.NONKGS;

                        break;
                    }
                    case "system":
                    {
                        AdminMode = AdminModeEnum.SYSTEM;

                        break;
                    }
                    default: throw new Exception();
                }
            }

        #endregion

		#region method

            private void Page_PreInit()
            {              
            }

		#endregion

		#region property

            public AdminModeEnum AdminMode
            {
                private set;
                get;
            }

		#endregion
    }
}