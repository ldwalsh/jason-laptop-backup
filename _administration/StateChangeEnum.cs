﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._administration
{
	public enum StateChangeEnum
	{
		AddEntity,
		EditEntity,
		DeleteEntity,
	}
}