﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;

namespace CW.Website._administration
{
    public abstract class AddTabBase<TKGSEntity,TAdministrationPage>: TabGenericBase<TKGSEntity> where TKGSEntity: new() where TAdministrationPage: AdministrationPageBase
    {
		#region field

			#region readonly
            
				protected readonly AdministrationControl<TAdministrationPage> administrationControl;

			#endregion

			protected Delegates<TKGSEntity>.CanEntityBePersistedDelegate CanEntityBeAdded;

			protected Action<TKGSEntity> AddEntityMethod;
            
		#endregion

		#region constructor

			protected AddTabBase()
			{
				administrationControl = new AdministrationControl<TAdministrationPage>(this);
			}

		#endregion

		#region method

			#region virtual
				
				protected virtual void ClearForm()
				{
					CallFormMethod("ClearForm");
				}

				protected virtual void CreateEntity(TKGSEntity obj)
				{
					CallFormMethod("CreateEntity", obj);
				}

				protected virtual void AddEntity(TKGSEntity obj)
				{
					ValidateDelegate(PropHelper.G<AddTabBase<TKGSEntity,TAdministrationPage>>(_=>_.CanEntityBeAdded));
					ValidateDelegate(PropHelper.G<AddTabBase<TKGSEntity,TAdministrationPage>>(_=>_.AddEntityMethod));

					//
            
					String message;
            
					if (!CanEntityBeAdded(obj, out message))
					{
						if (String.IsNullOrWhiteSpace(message))
						{
							message = "{EntityTypeName} could not be added.";
						}
                
						DisplayMessage(message);

						return;
					}

					try
					{
						AddEntityMethod(obj);

						message = String.Concat(GetEntityName(obj), " {EntityTypeName} addition was successful.");
					}
					catch (Exception ex)
					{
						LogMgr.Log(DataConstants.LogLevel.ERROR, InsertEntityTypeName("Error adding {EntityTypeName}."), ex);

						message = "Adding {EntityTypeName} failed. Please contact an administrator.";

						return;
					}
					finally
					{
						DisplayMessage(message);
					}

					if (obj.GetType() != typeof(BuildingGroup)) ClearForm();

					page.TabStateMonitor.ChangeState(AddChangeStateEnums);
				}

			#endregion

			#region override

				protected override void UpdateButton_Click(Object sender, EventArgs e)
				{
					var obj = new TKGSEntity();

                    CreateEntity(obj);

					AddEntity(obj);
				}

			#endregion

		#endregion

		#region property

			protected new TAdministrationPage page
			{
				get {return administrationControl.page;}
			}

			protected virtual IEnumerable<Enum> AddChangeStateEnums
			{
				get {return new Enum[]{StateChangeEnum.AddEntity};}
			}

		#endregion
    }
}