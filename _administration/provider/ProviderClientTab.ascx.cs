﻿using System;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using CW.Diagnostics.Helpers;
using CW.Data.AzureStorage.Models;

namespace CW.Website._administration.provider
{
    public partial class ProviderClientTab : System.Web.UI.UserControl
    {
        #region Properties

        private readonly DataManager mDataManager = DataManager.Instance;
        const string reassignProviderClientUpdateSuccessful = "Provider Client reassignment was successful.";
        const string reassignProviderUpdateFailed = "Provider Client reassignment failed. Please contact an administrator.";

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindOrganizations(ddlOrganizations);
            }
        }

        #endregion

        #region Load and Bind Fields

        private void BindProviders(DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataTextField = "ProviderName";
            ddl.DataValueField = "ProviderID";
            ddl.DataSource = mDataManager.ProviderDataMapper.GetAllProviders();
            ddl.DataBind();
        }

        internal void BindOrganizations(DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataTextField = "OrganizationName";
            ddl.DataValueField = "OID";
            ddl.DataSource = mDataManager.OrganizationDataMapper.GetAllOrganizations();
            ddl.DataBind();
        }

        private void BindProviders(int OID, DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataTextField = "ProviderName";
            ddl.DataValueField = "ProviderID";
            ddl.DataSource = mDataManager.ProviderDataMapper.GetAllProvidersByOrg(OID);
            ddl.DataBind();
        }

        private void BindRoles(DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataTextField = "RoleName";
            ddl.DataValueField = "RoleID";
            ddl.DataSource = mDataManager.UserRoleDataMapper.GetAllRoles();
            ddl.DataBind();
        }

        private void BindProviderClient(int providerID, int OID)
        {
            bool assigned = false;

            var clients = mDataManager.ClientDataMapper.GetAllClientsByOrg(OID);

            //declare new ienumerable for available unassigned clients for provider
            List<Client> availableUnassignedClientsList = new List<Client>();

            //declare new ienumerable for available assigned clients for provider
            List<Client> availableAssignedClientsList = new List<Client>();

            //for each ienumerable providers                           
            using (IEnumerator<Client> list = clients.GetEnumerator())
            {
                int counter = 1;
                while (list.MoveNext())
                {
                    //current equipment
                    Client item = (Client)list.Current;

                    assigned = mDataManager.ProviderClientDataMapper.IsClientAssignedToProvider(providerID, item.CID);

                    if (assigned)
                    {
                        availableAssignedClientsList.Add(item);
                    }
                    else
                    {
                        availableUnassignedClientsList.Add(item);
                    }

                    counter++;
                }
            }

            if (availableUnassignedClientsList.Count > 0)
            {
                //bind unassigned clients to top listbox
                lbProviderClientTop.DataSource = availableUnassignedClientsList;
                lbProviderClientTop.DataTextField = "ClientName";
                lbProviderClientTop.DataValueField = "CID";
                lbProviderClientTop.DataBind();
            }
            else
            {
                //clear current list
                lbProviderClientTop.Items.Clear();
            }

            if (availableAssignedClientsList.Count > 0)
            {
                //bind assigned clients to bottom listbox
                lbProviderClientBottom.DataSource = availableAssignedClientsList;
                lbProviderClientBottom.DataTextField = "ClientName";
                lbProviderClientBottom.DataValueField = "CID";
                lbProviderClientBottom.DataBind();
            }
            else
            {
                //clear current list
                lbProviderClientBottom.Items.Clear();
            }

            clientProvider.Visible = true;
        }

        #endregion

        #region Dropdown Events

        protected void ddlOrganizations_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BindProviders(Convert.ToInt32(ddlOrganizations.SelectedValue), ddlProviders);
        }

        protected void ddlProviders_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BindRoles(ddlRoles); 
        }

        protected void ddlRole_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BindProviderClient(Convert.ToInt32(ddlProviders.SelectedValue), Convert.ToInt32(ddlOrganizations.SelectedValue));
        }

        #endregion

        #region Button Events

        protected void updateProviderClientButton_Click(object sender, EventArgs e)
        {
            int oid = Convert.ToInt32(ddlOrganizations.SelectedValue);
            int providerID = Convert.ToInt32(ddlProviders.SelectedValue);
            int roleID = Convert.ToInt32(ddlRoles.SelectedValue);
            int cid;

            //declare new provider_client
            ProvidersClients mProvidersClient = new ProvidersClients();

            try
            {
                List<ProvidersClients> insertList = new List<ProvidersClients>();
                List<ProvidersClients> deleteList = new List<ProvidersClients>();

                //inserting each client to provider in the bottom listbox if they dont exist
                foreach (ListItem item in lbProviderClientBottom.Items)
                {
                    cid = Convert.ToInt32(item.Value);
                    mProvidersClient.CID = cid;
                    mProvidersClient.ProviderID = providerID;
                    mProvidersClient.MaxRoleID = roleID;

                    //if the client to provider doesnt already exist, insert
                    if (!mDataManager.ProviderClientDataMapper.IsClientAssignedToProvider(cid, providerID))
                    {
                        insertList.Add(mProvidersClient);
                    }
                }

                //deleting each client to provider in the top listbox if they exist
                foreach (ListItem item in lbProviderClientTop.Items)
                {
                    cid = Convert.ToInt32(item.Value);

                    //if the client to provider exists, delete
                    if (mDataManager.ProviderClientDataMapper.IsClientAssignedToProvider(cid, providerID))
                    {
                        deleteList.Add(mProvidersClient);
                    }
                }

                mDataManager.ProviderClientDataMapper.AddProvidersClients(insertList);
                mDataManager.ProviderClientDataMapper.DeleteProvidersClients(deleteList);

                lblProviderClientError.Text = reassignProviderClientUpdateSuccessful;
                lblProviderClientError.Visible = true;
                lnkSetFocusProvider.Focus();
            }
            catch (Exception ex)
            {
                LogHelper.LogCWError("Error reassigning provider to multiple client.", ex);
                lblProviderClientError.Text = reassignProviderUpdateFailed;
                lblProviderClientError.Visible = true;
                lnkSetFocusProvider.Focus();
            }

            //Rebind list
            BindProviderClient(providerID, oid);
        }

        /// <summary>
        /// on button up click, remove client from bottom listbox
        /// </summary>
        protected void btnProviderClientUpButton_Click(object sender, EventArgs e)
        {
            while (lbProviderClientBottom.SelectedIndex != -1)
            {
                lbProviderClientTop.Items.Add(lbProviderClientBottom.SelectedItem);
                lbProviderClientBottom.Items.Remove(lbProviderClientBottom.SelectedItem);
            }
        }

        /// <summary>
        /// on button down click, add client to bottom listbox
        /// </summary>
        protected void btnProviderClientDownButton_Click(object sender, EventArgs e)
        {
            while (lbProviderClientTop.SelectedIndex != -1)
            {
                {
                    lbProviderClientBottom.Items.Add(lbProviderClientTop.SelectedItem);
                    lbProviderClientTop.Items.Remove(lbProviderClientTop.SelectedItem);
                }
            }
        }



        #endregion
    }
}