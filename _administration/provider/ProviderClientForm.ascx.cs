﻿using CW.Data.Models.ProviderClient;
using System;

namespace CW.Website._administration.provider
{
    public partial class ProviderClientForm : FormGenericBase<ProviderClientForm, ProvidersClientView>
    {
        protected void Page_FirstLoad(Object sender, EventArgs e)
        {
            if (FormMode == PersistModeEnum.Insert)
            {
                tab.BindOrganizationsWithProviders(ddlOrganizations);
            }
            else
            {
                tab.BindNonKGSRoles(ddlMaxRoles);
            }
        }

        public Boolean OrganizationEnabled
        {
            set;
            get;
        }

        public Boolean IsActiveEnabled
        {
            set;
            get;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (FormMode == PersistModeEnum.Update)
            {
                plhAdd.Visible = false;
                plhEdit.Visible = true;
                addOrganizationRequiredValidator.Enabled = false;
                addOrganizationRequiredValidatorExtender.Enabled = false;
                addProviderRequiredValidator.Enabled = false;
                addProviderRequiredValidatorExtender.Enabled = false;
                addClientRequiredValidator.Enabled = false;
                addClientRequiredValidatorExtender.Enabled = false;
                addMaxRoleRequiredValidator.Enabled = false;
                addMaxRoleRequiredValidatorExtender.Enabled = false;
                   
            }
            else
            {
                plhAdd.Visible = true;
                plhEdit.Visible = false;
                addOrganizationRequiredValidator.Enabled = true;
                addOrganizationRequiredValidatorExtender.Enabled = true;
                addProviderRequiredValidator.Enabled = true;
                addProviderRequiredValidatorExtender.Enabled = true;
                addClientRequiredValidator.Enabled = true;
                addClientRequiredValidatorExtender.Enabled = true;
                addMaxRoleRequiredValidator.Enabled = true;
                addMaxRoleRequiredValidatorExtender.Enabled = true;
            }

            if (!String.IsNullOrWhiteSpace(UpdateButtonCaption))
            {
                updateButton.Text = UpdateButtonCaption;
            }

            base.OnPreRender(e);
        }

        public void PopulateForm(ProvidersClientView providerClient)
        {
            hdnID.Value                                     = Convert.ToString(providerClient.PCID);
            hdnCID.Value                                    = Convert.ToString(providerClient.CID);
            litClient.Text                                  = providerClient.ClientName;
            hdnProviderID.Value                             = Convert.ToString(providerClient.ProviderID);
            litProvider.Text                                = providerClient.ProviderName;
            ddlMaxRoles.SelectedValue                       = Convert.ToString(providerClient.MaxRoleID);
        }

        public void CreateEntity(ProvidersClientView pcv)
        {
            pcv.PCID		= (String.IsNullOrWhiteSpace(hdnID.Value) ? "0" : hdnID.Value);
			pcv.MaxRoleID	= Convert.ToInt32(ddlMaxRoles.SelectedValue);
			pcv.MaxRoleName	= ddlMaxRoles.Text;

            if (FormMode == PersistModeEnum.Insert)
            {
                pcv.CID              = Convert.ToInt32(ddlClients.SelectedValue);
                pcv.ClientName       = ddlClients.SelectedItem.Text;
                pcv.ProviderID       = Convert.ToInt32(ddlProviders.SelectedValue);
                pcv.ProviderName     = ddlProviders.SelectedItem.Text;
            }
            else
            {
                pcv.CID             = Convert.ToInt32(hdnCID.Value);
                pcv.ClientName      = litClient.Text;
                pcv.ProviderID      = Convert.ToInt32(hdnProviderID.Value);
                pcv.ProviderName    = litProvider.Text;
            }
        }

        public override Delegates<ProvidersClientView>.CanEntityBePersistedDelegate CanEntityBePersisted
        {
            get {return CanProviderBeUpdated;}
        }

        public override Action<ProvidersClientView> PersistEntityAction
        {
            get { return AddOrUpdateProvidersClient; }
        }

        public Boolean CanProviderBeUpdated(ProvidersClientView provider, out String message)
        {
            if (DataMgr.ProviderDataMapper.DoesProviderExist(provider.ProviderID, provider.ProviderName))
            {
                message = "{EntityTypeName} already exists.";

                return false;
            }

            if(provider.CID <= 0 || provider.ProviderID <= 0 || provider.MaxRoleID <= 0)
            {
                message = "{EntityTypeName} needs to have valid assignment of Client, Provider and Max Role.";

                return false;
            }

            message = null;

            return true;
        }

        public void AddOrUpdateProvidersClient(ProvidersClientView providersClient)
        {
            switch (FormMode)
            {
                case PersistModeEnum.Insert:
                    {
                        DataMgr.ProviderClientDataMapper.AddProvidersClients(providersClient);

                        break;
                    }
                case PersistModeEnum.Update:
                    {
                        DataMgr.ProviderClientDataMapper.UpdateProvidersClientsByProviderIDAndCID(providersClient);

                        break;
                    }
            }
        }

        #region Dropdown Events

            protected void ddlOrganizations_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                tab.BindProvidersByOID(Convert.ToInt32(ddlOrganizations.SelectedValue), ddlProviders);
            }

            protected void ddlProviders_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                tab.BindClientsExcludingOrgClientsAndExistingProviderClientAssocations(Convert.ToInt32(ddlOrganizations.SelectedValue), Convert.ToInt32(ddlProviders.SelectedValue), ddlClients);
            }

            protected void ddlClients_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                tab.BindNonKGSRoles(ddlMaxRoles);
            }

        #endregion
    }
}