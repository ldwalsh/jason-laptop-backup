﻿using CW.Data.Models.ProviderClient;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._administration.provider.kgs
{
    public partial class ProviderClientViewTab : ViewTabWithFormBase<ProvidersClientView, KGSProviderClientAdministration>
    {
        #region method

            protected void Page_Init(Object sender, EventArgs e)
            {
                BindProviders(ddlProvider);

                CanEntityBeUpdated = providerClientForm.CanProviderBeUpdated;
                CanEntityBeDeleted = CanProviderClientBeDeleted;

                DeleteEntityMethod = DataMgr.ProviderClientDataMapper.DeleteProviderClientRelationship;
                UpdateEntityMethod = providerClientForm.PersistEntityAction;
            }

            private void Page_Load()
            {
                page.TabStateMonitor.OnTabStateChanged += TabStateMonitor_OnTabStateChanged;
            }

            void TabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages)
            {
                BindProviders(ddlProvider);
            }

            private Boolean CanProviderClientBeDeleted(String pcid, out String message)
            {
                if (DataMgr.ClientDataMapper.DoesClientSettingWithProviderIDExist(pcid))
                {
                    message = InsertEntityTypeName("{EntityTypeName} can not be deleted.  Provider is associated to at least one clients themed setting.");
                    return false;
                }

                message = null;

                return true;
            }

        #endregion

        #region property

			protected override IEnumerable<String> CacheHashKeySubList
            {
				get {return ddlProvider.Items.Cast<ListItem>().Select(_=>_.Value);}
            }

			protected override IEnumerable<String> GridColumnNames
			{
				get {return PropHelper.F<ProvidersClientView>(_=>_.ProviderName, _=>_.ClientName, _=>_.MaxRoleID);}
			}

            protected override GetEntitiesForGridDelegate GetEntitiesForGrid
            {
                get
                {
                    return (searchText => DataMgr.ProviderClientDataMapper.GetAllSearchedProvidersClients(Convert.ToInt32(ddlProvider.SelectedValue), searchText));
                }
            }

            protected override GetEntityDelegate GetEntityForEditing
            {
                get
                {
                    return DataMgr.ProviderClientDataMapper.GetProvidersClientsView;
                }
            }

            protected override GetEntityDelegate GetEntityForDetailView
            {
                get
                {
                    return DataMgr.ProviderClientDataMapper.GetProvidersClientsView;
                }
            }

            protected override String CacheSubKey
            {
                get { return ddlProvider.SelectedValue; }
            }

        #endregion

        #region Dropdown Events

            protected void ddlProvider_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindGrid();

                EntityDetailView.Visible = false;
                PanelEditEntity.Visible = false;
            }

        #endregion
    }
}