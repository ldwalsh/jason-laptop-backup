﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace CW.Website._administration.provider.kgs
{
    public partial class ProviderViewTab : ViewTabWithFormBase<Provider, KGSProviderAdministration>, IPostBackEventHandler
    {
		#region constructor

			public ProviderViewTab()
			{
				CanEntityBeUpdated = (Provider provider, out String message)=> providerForm.CanProviderBeUpdated(provider, out message);
				CanEntityBeDeleted = CanProviderBeDeleted;

				DeleteEntityMethod = (id => DataMgr.ProviderDataMapper.DeleteProvider(Convert.ToInt32(id)));
			}

		#endregion

		#region method

			protected void Page_Init(Object sender, EventArgs e)
			{
				UpdateEntityMethod = providerForm.UpdateProviderMethod;
			}

			private Boolean CanProviderBeDeleted(String providerID, out String message)
			{
				if (DataMgr.ProviderClientDataMapper.IsProviderAssociatedToAClient(Convert.ToInt32(providerID)))
				{
					message = "{EntityTypeName} can not be deleted. Provider is associated to at least one client.";

					return false;
				}

				message = null;

				return true;
			}

		#endregion

		#region property

			protected override IEnumerable<String> GridColumnNames
			{
				get	{return PropHelper.F<Provider>(_=>_.ProviderName, _=>_.Organizations_OrganizationTypeName.Organization.OrganizationName, _=>_.IsActive);}
			}
			
			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get { return DataMgr.ProviderDataMapper.GetAllSearchedProviders; }
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get { return id => DataMgr.ProviderDataMapper.GetProviderByID(Convert.ToInt32(id)); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get { return id => DataMgr.ProviderDataMapper.GetProviderByID(Convert.ToInt32(id)); }
			}

			protected override String CacheSubKey
			{
				get {return null;}
			}

		#endregion

		#region IPostBackEventHandler

			void IPostBackEventHandler.RaisePostBackEvent(String eventArgument) //revise this approach and access the TabStateMonitor instance directly client-side
			{
				switch (eventArgument)
				{
					case "TabStateMonitor.ChangeState":
					{
						this.page.TabStateMonitor.ChangeState();

						BindGrid(true);

						break;
					}
				}
			}

		#endregion
    }
}