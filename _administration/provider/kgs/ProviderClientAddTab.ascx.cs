﻿using CW.Data.Models.ProviderClient;
using System;

namespace CW.Website._administration.provider.kgs
{
    public partial class ProviderClientAddTab: AddTabBase<ProvidersClientView, KGSProviderClientAdministration>
    {
        #region constructor

            public ProviderClientAddTab()
            {
                CanEntityBeAdded = CanProviderBeAdded;
                AddEntityMethod = DataMgr.ProviderClientDataMapper.AddProvidersClients;
            }

        #endregion

        #region method

            private Boolean CanProviderBeAdded(ProvidersClientView providersClient, out String message)
            {
                if (DataMgr.ProviderClientDataMapper.IsClientAssignedToProvider(providersClient.ProviderID, providersClient.CID))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

        #endregion
    }
}