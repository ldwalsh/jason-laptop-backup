﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderClientViewTab.ascx.cs" Inherits="CW.Website._administration.provider.kgs.ProviderClientViewTab" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Common.Constants" %>
<%@ Import Namespace="CW.Data.Models.ProviderClient" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register tagPrefix="CW" tagName="ProviderClientForm" src="../ProviderClientForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Provider's Clients" />  

<div class="divForm">
  <label class="label">Provider:</label>
  <asp:DropDownList ID="ddlProvider" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlProvider_OnSelectedIndexChanged" />
</div>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>        
                                                                                                                
<div id="gridTbl">                                        
  <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true"> 
    <Columns>
      <asp:CommandField ShowSelectButton="true" />
      
      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ProviderName" HeaderText="Provider Name">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<ProvidersClientView>(_=>_.ProviderName), 40) %>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client Name">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<ProvidersClientView>(_=>_.ClientName), 40) %>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="MaxRoleID" HeaderText="Max Role">  
        <ItemTemplate>
          <%# GetCellContentAsEnum<BusinessConstants.UserRole.UserRoleEnum>(Container, PropHelper.G<ProvidersClientView>(_=>_.MaxRoleID), 40) %>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this providers client relationship permanently?');" CommandName="Delete">Delete</asp:LinkButton>
        </ItemTemplate>
      </asp:TemplateField>            
    </Columns>        
  </asp:GridView>                                                                    
</div>                                    
<br />
<br /> 

<div>                                                            
  <!--SELECT PROVIDER DETAILS VIEW -->
  <asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
    <Fields>
      <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none">
        <ItemTemplate>
          <div>
            <h2>Provider's Client Details</h2>
            <ul class="detailsList">
              <li><strong>Provider Name: </strong><%# Eval("ProviderName")%></li>
              <li><strong>Client Name: </strong><%# Eval("ClientName")%></li>
              <li><strong>Role Name: </strong><%# Eval("MaxRoleName")%></li>
            </ul>
          </div>
        </ItemTemplate>
      </asp:TemplateField>
    </Fields>
  </asp:DetailsView>                                  
</div>                                     
                                                                     
<!--EDIT PROVIDER PANEL -->                              
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">                                                                                             
  <div>
    <h2>Edit Providers Client</h2>
  </div>  
  <div>
    <CW:ProviderClientForm ID="providerClientForm" runat="server" FormMode="Update" UpdateButtonCaption="Update Provider" OrganizationEnabled="false" IsActiveEnabled="true" />
  </div>                                                                                                                                                                               
</asp:Panel>