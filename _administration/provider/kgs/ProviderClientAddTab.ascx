﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderClientAddTab.ascx.cs" Inherits="CW.Website._administration.provider.kgs.ProviderClientAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register tagPrefix="CW" tagName="ProvidersClientForm" src="../ProviderClientForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Provider Client" />
<CW:ProvidersClientForm ID="providersClientForm" runat="server" FormMode="Insert" UpdateButtonCaption="Add Provider Client" OrganizationEnabled="true" IsActiveEnabled="true" />
