﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderAddTab.ascx.cs" Inherits="CW.Website._administration.provider.kgs.ProviderAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register tagPrefix="CW" tagName="ProviderForm" src="../ProviderForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Provider" />
<CW:ProviderForm ID="providerForm" runat="server" FormMode="Insert" UpdateButtonCaption="Add Provider" OrganizationEnabled="true" />
