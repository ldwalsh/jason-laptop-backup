﻿<%@ Page Language="C#" AutoEventWireup="false" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" CodeBehind="KGSProviderAdministration.aspx.cs" Inherits="CW.Website._administration.provider.kgs.KGSProviderAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="ProviderViewTab.ascx" tagPrefix="CW" tagName="ProviderViewTab" %>
<%@ Register src="ProviderAddTab.ascx" tagPrefix="CW" tagName="ProviderAddTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
    
                <h1>
                    KGS Provider Administration</h1>
                <div class="richText">
                    The kgs provider administration area is used to view, add, and edit providers.</div>
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>
                            <img src="/_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div class="administrationControls">
                    <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
                        <Tabs>
                            <telerik:RadTab Text="View Providers"></telerik:RadTab>
                            <telerik:RadTab Text="Add Provider"></telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                        <telerik:RadPageView ID="RadPageView1" runat="server">
                                <CW:ProviderViewTab runat="server" ID="providerViewTab"/>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server">
                                <CW:ProviderAddTab runat="server" ID="providerAddTab"/>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </div>

</asp:Content>


                    
                  
