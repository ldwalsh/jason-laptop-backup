﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderViewTab.ascx.cs" Inherits="CW.Website._administration.provider.kgs.ProviderViewTab" %>
<%@ Register tagPrefix="CW" tagName="ProviderForm" src="../ProviderForm.ascx"  %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Providers" />  
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>         
                                                                                                                
<div id="gridTbl">                                        
        <asp:GridView 
            ID="grid"   
            EnableViewState="true"           
            runat="server"  
            HeaderStyle-CssClass="tblTitle" 
            RowStyle-CssClass="tblCol1"
            AlternatingRowStyle-CssClass="tblCol2"   
            RowStyle-Wrap="true"                                                                                   
            > 
            <Columns>
            <asp:CommandField ShowSelectButton="true" />
            <asp:TemplateField>
                <ItemTemplate>                                                
                        <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                    
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ProviderName" HeaderText="Provider Name">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Provider>(_ => _.ProviderName)) %>"><%# GetCellContent(Container, PropHelper.G<Provider>(_=>_.ProviderName), 40) %></label>
                </ItemTemplate>
            </asp:TemplateField>   
            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Organizations_OrganizationTypeName.Organization.OrganizationName" HeaderText="Provider Organization">  
                <ItemTemplate>
                  <label title="<%# GetCellContent(Container, PropHelper.G<Provider>(_ => _.Organizations_OrganizationTypeName.Organization.OrganizationName)) %>"><%# GetCellContent(Container, PropHelper.G<Provider>(_=>_.Organizations_OrganizationTypeName.Organization.OrganizationName), 40) %></label>
                </ItemTemplate>
            </asp:TemplateField>   
            <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
                <ItemTemplate>
                  <%# GetCellContent(Container, PropHelper.G<Provider>(_=>_.IsActive)) %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>                                                
                        <asp:LinkButton runat="server" CausesValidation="false"
                            OnClientClick="return confirm('Are you sure you wish to delete this provider permanently?\n\nAlternatively you can deactivate an provider temporarily instead.');"
                            CommandName="Delete">Delete</asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>                                                                                                                                                                          
            </Columns>        
        </asp:GridView> 
                                                                          
</div>                                    
<br /><br /> 
<div>                                                            
<!--SELECT PROVIDER DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
    <Fields>
        <asp:TemplateField  
        ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
        ItemStyle-BorderWidth="0" 
        ItemStyle-BorderColor="White" 
        ItemStyle-BorderStyle="none"
        ItemStyle-Width="100%"
        HeaderStyle-Width="1" 
        HeaderStyle-BorderWidth="0" 
        HeaderStyle-BorderColor="White" 
        HeaderStyle-BorderStyle="none"
        >
            <ItemTemplate>
                <div>
                    <h2>Provider Details</h2>                                                                                                                                            
                    <ul class="detailsList">
                        <li><strong>Provider Name: </strong><%# Eval("ProviderName")%></li>
                        <li><strong>Organization Name: </strong><%# Eval("Organizations_OrganizationTypeName.Organization.OrganizationName")%></li>
                        <li><strong>Active: </strong><%# Eval("IsActive")%></li>
                    </ul>
                </div>
            </ItemTemplate>                                             
        </asp:TemplateField>
    </Fields>
</asp:DetailsView>   
                                    
</div>                                     
                                                                     
<!--EDIT PROVIDER PANEL -->                              
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">                                                                                             
    <div>
        <h2>Edit Provider</h2>
    </div>  
    <div>
        <CW:ProviderForm ID="providerForm" runat="server" FormMode="Update" UpdateButtonCaption="Update Provider" IsActiveVisible="true" />
    </div>                                                                                                                                                                               
</asp:Panel>
