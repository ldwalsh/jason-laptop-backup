﻿using CW.Data;
using System;

namespace CW.Website._administration.provider.kgs
{
    public partial class ProviderAddTab: AddTabBase<Provider,KGSProviderAdministration>
    {
        #region constructor

            public ProviderAddTab()
            {
                CanEntityBeAdded = CanProviderBeAdded;
                AddEntityMethod = DataMgr.ProviderDataMapper.InsertProvider;
            }

        #endregion

        #region method

            private Boolean CanProviderBeAdded(Provider provider, out String message)
            {
                if (DataMgr.ProviderDataMapper.DoesProviderNameExist(provider.ProviderName, provider.Organizations_OrganizationTypeName.OID))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                //if (OrganizationDataMapper.DoesOrganizationHaveAProvderOrgType(provider.Organizations_OrganizationTypeName.OID))
                //{
                //    message = "Organization already has a provider org type.";
                //
                //    return false;
                //}

                message = null;

                return true;
            }

        #endregion
    }
}