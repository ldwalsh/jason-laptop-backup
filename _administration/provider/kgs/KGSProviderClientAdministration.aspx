﻿<%@ Page Language="C#" AutoEventWireup="false" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" CodeBehind="KGSProviderClientAdministration.aspx.cs" Inherits="CW.Website._administration.provider.kgs.KGSProviderClientAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="ProviderClientViewTab.ascx" tagPrefix="CW" tagName="ProviderClientViewTab" %>
<%@ Register src="ProviderClientAddTab.ascx" tagPrefix="CW" tagName="ProviderClientAddTab" %>
<%@ Register src="../ProviderClientBuildingsTab.ascx" tagPrefix="CW" tagName="ProviderClientBuildingsTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
    
                <h1>
                    KGS Provider Client Administration</h1>
                <div class="richText">
                    The kgs provider client administration area is used to view, add, and edit providers client relationships.</div>
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>
                            <img src="/_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div class="administrationControls">
                    <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
                        <Tabs>
                            <telerik:RadTab Text="View Provider's Clients"></telerik:RadTab>
                            <telerik:RadTab Text="Add Client to a Provider"></telerik:RadTab>
                            <telerik:RadTab Text="Add/Edit Buildings to a Provider Client"></telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                        <telerik:RadPageView ID="RadPageView1" runat="server">
                            <CW:ProviderClientViewTab runat="server" ID="providerClientViewTab"/>                        
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server">
                            <CW:ProviderClientAddTab runat="server" ID="providerClientAddTab"/>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView3" runat="server">
                            <CW:ProviderClientBuildingsTab runat="server" ID="providerClientBuildingsTab"/>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </div>

</asp:Content>


                    
                  
