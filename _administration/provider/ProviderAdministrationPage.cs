﻿using System;
using CW.Data;
using CW.Utility;

namespace CW.Website._administration.provider
{
    public abstract class ProviderAdministrationPage : AdministrationPageBase
    {
        public override String IdentifierField
        {
            get {return PropHelper.G<Provider>(p => p.ProviderID);}
        }

        public override String NameField
        {
            get {return PropHelper.G<Provider>(p => p.ProviderName);}
        }

        public override String EntityTypeName
        {
            get {return typeof(Provider).Name;}
        }
    }
}