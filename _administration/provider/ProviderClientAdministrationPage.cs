﻿using System;
using System.Web.UI.WebControls;

using CW.Business;
using CW.Data;
using CW.Utility;
using CW.Data.Models.ProviderClient;

namespace CW.Website._administration.provider
{
    public class ProviderClientAdministrationPage : AdministrationPageBase
    {
        public override String IdentifierField
        {
            get { return PropHelper.G<ProvidersClientView>(p => p.PCID); }
        }

        public override String NameField
        {
            get { return PropHelper.G<ProvidersClientView>(p => p.ProviderName); }
        }

        public override String EntityTypeName
        {
            get {return "Provider's Clients";}
        }
    }
}