﻿using System;
using System.Web.UI.WebControls;
using System.Linq;
using CW.Business;
using CW.Data;
using System.Collections.Generic;
using System.Data.SqlClient;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website._administration.provider
{
    public partial class ProviderClientBuildingsTab : SiteUserControl
    {
        #region Properties

        const string reassignProviderClientBuildingUpdateFailed = @"Provider Client Building\Building Group reassignment failed. Please contact an administrator.";
        const string reassignProviderClientBuildingUpdateSuccessful = @"Provider Client Building\Building Group reassignment was successful.";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                if (!Page.IsPostBack)
                {
                    BindOrganizations(ddlOrganizations);
                }
            }

        #endregion

        #region Load and Bind Fields
            internal void BindOrganizations(DropDownList ddl)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "OrganizationName";
                ddl.DataValueField = "OID";
                ddl.DataSource = DataMgr.OrganizationDataMapper.GetOrganizationsWithProviders();
                ddl.DataBind();
            }

            private void BindProviders(int OID, DropDownList ddl)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "ProviderName";
                ddl.DataValueField = "ProviderID";
                ddl.DataSource = DataMgr.ProviderDataMapper.GetAllProvidersByOrg(OID);
                ddl.DataBind();
            }

            private void BindClients(int providerID, DropDownList ddl)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "ClientName";
                ddl.DataValueField = "CID";
                ddl.DataSource = DataMgr.ClientDataMapper.GetAllClientsAssociatedToProvider(providerID);
                ddl.DataBind();
            }

            private bool BindBuildingGroups(Int32 cid, Int32 providerID)
            {
                bool hasAssignedItems = false;

                var buildingGroups = DataMgr.BuildingGroupDataMapper.GetAllBuildingGroupsByCID(cid);
                var providersClientsList = DataMgr.ProviderClientDataMapper.GetProvidersClientsList(providerID, cid).Where(pc => pc.BuildingGroupID != 0);

                var availableAssignedBuildingGroupList = from b in buildingGroups
                                                         join pc in providersClientsList on b.BuildingGroupID equals pc.BuildingGroupID
                                                         select Tuple.Create<string, string>(b.BuildingGroupName, b.BuildingGroupID.ToString() + "_" + pc.Guid.ToString());

                var availableUnassignedBuildingGroupList = from b in buildingGroups
                                                           join pc in providersClientsList on b.BuildingGroupID equals pc.BuildingGroupID
                                                           into groups
                                                           where !groups.Any()
                                                           select Tuple.Create<string, string>(b.BuildingGroupName, b.BuildingGroupID.ToString());

                // Bind Top List Box - Available and Unassigned
                ControlHelper.BindListBoxWithTuple(lbEditBuildingGroupsTop, availableUnassignedBuildingGroupList);
                // Bind Bottom List Box - Available and Assigned
                ControlHelper.BindListBoxWithTuple(lbEditBuildingGroupsBottom, availableAssignedBuildingGroupList);

                if (availableAssignedBuildingGroupList.Any()) hasAssignedItems = true;                

                return hasAssignedItems;
            }

            private bool BindBuildings(Int32 cid, Int32 providerID)
            {
                bool hasAssignedItems = false;

                var buildings = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(cid);
                var providersClientsList = DataMgr.ProviderClientDataMapper.GetProvidersClientsList(providerID, cid).Where(pc => pc.BID != 0);

                var availableAssignedBuildingList = from b in buildings
                                                    join pc in providersClientsList on b.BID equals pc.BID
                                                    select Tuple.Create<string, string>(b.BuildingName, b.BID.ToString() + "_" + pc.Guid.ToString());

                var availableUnassignedBuildingList = from b in buildings
                                                      join pc in providersClientsList on b.BID equals pc.BID
                                                      into groups
                                                      where !groups.Any()
                                                      select Tuple.Create<string,string>( b.BuildingName, b.BID.ToString() );

                // Bind Top List Box - Available and Unassigned
                ControlHelper.BindListBoxWithTuple(lbEditBuildingsTop, availableUnassignedBuildingList);
                // Bind Bottom List Box - Available and Assigned
                ControlHelper.BindListBoxWithTuple(lbEditBuildingsBottom, availableAssignedBuildingList);

                if (availableAssignedBuildingList.Any()) hasAssignedItems = true;

                return hasAssignedItems;
            }
       #endregion

        #region Dropdown and Checkbox Events

            protected void ddlOrganizations_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlOrganizations.SelectedIndex == 0) { return; }

                BindProviders(Convert.ToInt32(ddlOrganizations.SelectedValue), ddlProviders);
            }

            protected void ddlProviders_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlProviders.SelectedIndex == 0){ return; }

                BindClients(Convert.ToInt32(ddlProviders.SelectedValue), ddlClients);
            }

            protected void ddlClients_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlProviders.SelectedIndex == 0 || ddlClients.SelectedIndex == 0) { return; }

                int cid = Convert.ToInt32(ddlClients.SelectedValue);
                int providerID = Convert.ToInt32(ddlProviders.SelectedValue);

                // set max role id
                hdnMaxRoleID.Value = DataMgr.ProviderClientDataMapper.GetProvidersClientsList(providerID, cid).First().MaxRoleID.ToString();

                var unrestrictedProvidersClients = DataMgr.ProviderClientDataMapper.GetUnrestrictedProvidersClients(providerID, cid);
                var guid = unrestrictedProvidersClients == null ? "" : unrestrictedProvidersClients.Guid.ToString();
                if (!String.IsNullOrEmpty(guid))
                {
                    hdnUnrestrictedGuid.Value = guid;

                    ToggleRestrictedControls(false);
                }
                else
                {
                    hdnUnrestrictedGuid.Value = "";

                    ToggleRestrictedControls(true); 
                }

                BindBuildingGroups(cid, providerID);
                BindBuildings(cid, providerID);

                pnlIsRestricted.Visible = true;
            }

            protected void chkIsRestricted_OnCheckedChanged(object sender, EventArgs e)
            {
                ToggleRestrictedControls(chkIsRestricted.Checked);
            }

            private void ToggleRestrictedControls(bool isRestricted)
            {
                if (isRestricted)
                {
                    chkIsRestricted.Checked = true;
                    chkIsRestricted.Enabled = false;

                    // show buildings / building groups list boxes
                    divEditBuildingGroupsListBoxs.Visible = true;
                    divEditBuildingsListBoxs.Visible = true;
                }
                else
                {
                    chkIsRestricted.Checked = false;
                    chkIsRestricted.Enabled = true;

                    // hide buildings / building groups list boxes
                    divEditBuildingGroupsListBoxs.Visible = false;
                    divEditBuildingsListBoxs.Visible = false;
                }
            }

        #endregion

        #region Button Events
       
            /// <summary>
            /// on edit BuildingGroups button up click, add building groups
            /// </summary>
            protected void btnEditBuildingGroupsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditBuildingGroupsBottom.SelectedIndex != -1)
                {
                    lbEditBuildingGroupsTop.Items.Add(lbEditBuildingGroupsBottom.SelectedItem);
                    lbEditBuildingGroupsBottom.Items.Remove(lbEditBuildingGroupsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit BuildingGroups button down click, add building groups
            /// </summary>
            protected void btnEditBuildingGroupsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditBuildingGroupsTop.SelectedIndex != -1)
                {
                    lbEditBuildingGroupsBottom.Items.Add(lbEditBuildingGroupsTop.SelectedItem);
                    lbEditBuildingGroupsTop.Items.Remove(lbEditBuildingGroupsTop.SelectedItem);
                }
            }

            /// <summary>
            /// on edit Buildings button up click, add buildings 
            /// </summary>
            protected void btnEditBuildingsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditBuildingsBottom.SelectedIndex != -1)
                {
                    lbEditBuildingsTop.Items.Add(lbEditBuildingsBottom.SelectedItem);
                    lbEditBuildingsBottom.Items.Remove(lbEditBuildingsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit Buildings button down click, add buildings 
            /// </summary>
            protected void btnEditBuildingsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditBuildingsTop.SelectedIndex != -1)
                {
                    lbEditBuildingsBottom.Items.Add(lbEditBuildingsTop.SelectedItem);
                    lbEditBuildingsTop.Items.Remove(lbEditBuildingsTop.SelectedItem);
                }
            }

            protected void updateAssociationsButton_Click(object sender, EventArgs e)
            {
                int providerID = Convert.ToInt32(ddlProviders.SelectedValue);
                int cid = Convert.ToInt32(ddlClients.SelectedValue);
                int maxRoleID = Convert.ToInt32(hdnMaxRoleID.Value);

                try
                {
                    var buildingDeleteList = updateAssociationsBuildings(providerID, cid, maxRoleID);
                    var buildingGroupDeleteList = updateAssociationsBuildingsGroups(providerID, cid, maxRoleID);

                    // Combine ProvidersClients relationship list and delete
                    buildingDeleteList.AddRange(buildingGroupDeleteList);
                    DataMgr.ProviderClientDataMapper.DeleteProvidersClients(buildingDeleteList);

                    // Delete Unrestricted Providers Clients record if any buildings or building groups are assigned
                    if (lbEditBuildingsTop.Items.Count != 0 && lbEditBuildingGroupsTop.Items.Count != 0 && !String.IsNullOrEmpty(hdnUnrestrictedGuid.Value))
                    {
                        var unrestrictedProviderClient = DataMgr.ProviderClientDataMapper.GetUnrestrictedProvidersClients(providerID, cid);
                        DataMgr.ProviderClientDataMapper.DeleteProvidersClientsForChangeToBuildingRestricted(unrestrictedProviderClient);
                        hdnUnrestrictedGuid.Value = "";
                    }

                    // Add Unrestricted Providers Clients record if no buildings or building groups are assigned
                    if (lbEditBuildingsBottom.Items.Count == 0 && lbEditBuildingGroupsBottom.Items.Count == 0 && String.IsNullOrEmpty(hdnUnrestrictedGuid.Value))
                    {
                        DataMgr.ProviderClientDataMapper.AddProvidersClients(new ProvidersClients() { ProviderID = providerID, CID = cid, MaxRoleID = maxRoleID });
                        var unrestrictedProvidersClients = DataMgr.ProviderClientDataMapper.GetUnrestrictedProvidersClients(providerID, cid);
                        var guid = unrestrictedProvidersClients == null ? "" : unrestrictedProvidersClients.Guid.ToString();
                        if (!String.IsNullOrEmpty(guid)){ hdnUnrestrictedGuid.Value = guid; }
                    }

                    lblError.Text = reassignProviderClientBuildingUpdateSuccessful;
                    lblError.Visible = true;
                    lnkSetFocusMessage.Focus();
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning provider client to multiple buildings/building groups.", ex);
                    lblError.Text = reassignProviderClientBuildingUpdateFailed;
                    lblError.Visible = true;
                    lnkSetFocusMessage.Focus();
                }

                //Rebind list
                if (!BindBuildings(Convert.ToInt32(ddlClients.SelectedValue), Convert.ToInt32(ddlProviders.SelectedValue)) &&
                    !BindBuildingGroups(Convert.ToInt32(ddlClients.SelectedValue), Convert.ToInt32(ddlProviders.SelectedValue)))
                {
                    ToggleRestrictedControls(false);
                }
            }

            #region Helper Functions
            private List<ProvidersClients> getBuildingsInsertList(int providerID, int cid, int maxRoleID)
            {
                var insertList = new List<ProvidersClients>();

                // add association if it doesn't already exist
                foreach (ListItem item in lbEditBuildingsBottom.Items)
                {
                    int bid;
                    string[] itemValues = item.Value.Split('_');

                    // has no value or has a Guid in the value, therefore relationship already exists
                    if (itemValues.Count() == 0 || itemValues.Count() > 1) { continue; }

                    // parse bid
                    if (!Int32.TryParse(itemValues[0], out bid)) { continue; }

                    // add to insert list
                    insertList.Add(new ProvidersClients() { CID = cid, ProviderID = providerID, BID = bid, MaxRoleID = maxRoleID });
                }

                return insertList;
            }

            private List<ProvidersClients> getBuildingsDeleteList(int providerID, int cid)
            {
                var deleteList = new List<ProvidersClients>();

                // remove association if it currently exist
                foreach (ListItem item in lbEditBuildingsTop.Items)
                {
                    int bid;
                    Guid guid;
                    string[] itemValues = item.Value.Split('_');

                    // no Guid, relationship does not exist, therefore delete is not neccessry
                    if (itemValues.Count() < 2) { continue; }

                    // parse bid and guid
                    if (!Int32.TryParse(itemValues[0], out bid)) { continue; }
                    if (!Guid.TryParse(itemValues[1], out guid)) { continue; }

                    // add to delete list
                    deleteList.Add(new ProvidersClients() { CID = cid, ProviderID = providerID, BID = bid, Guid = guid });
                }

                return deleteList;
            }

            private List<ProvidersClients> getBuildingGroupsInsertList(int providerID, int cid, int maxRoleID)
            {
                var insertList = new List<ProvidersClients>();

                // add association if it doesn't already exist
                foreach (ListItem item in lbEditBuildingGroupsBottom.Items)
                {
                    int buildingGroupID;
                    string[] itemValues = item.Value.Split('_');

                    // has no value or has a Guid in the value, therefore relationship already exists
                    if (itemValues.Count() == 0 || itemValues.Count() > 1) { continue; }

                    // parse bid
                    if (!Int32.TryParse(itemValues[0], out buildingGroupID)) { continue; }

                    // add to insert list
                    insertList.Add(new ProvidersClients() { CID = cid, ProviderID = providerID, BuildingGroupID = buildingGroupID, MaxRoleID = maxRoleID });
                }

                return insertList;
            }

            private List<ProvidersClients> getBuildingGroupsDeleteList(int providerID, int cid)
            {
                var deleteList = new List<ProvidersClients>();

                // remove association if it currently exist
                foreach (ListItem item in lbEditBuildingGroupsTop.Items)
                {
                    int buildingGroupID;
                    Guid guid;
                    string[] itemValues = item.Value.Split('_');

                    // no Guid, relationship does not exist, therefore delete is not neccessry
                    if (itemValues.Count() < 2) { continue; }

                    // parse bid and guid
                    if (!Int32.TryParse(itemValues[0], out buildingGroupID)) { continue; }
                    if (!Guid.TryParse(itemValues[1], out guid)) { continue; }

                    // delete association
                    deleteList.Add(new ProvidersClients() { CID = cid, ProviderID = providerID, BuildingGroupID = buildingGroupID, Guid = guid });
                }

                return deleteList;
            }

            protected List<ProvidersClients> updateAssociationsBuildings(int providerID, int cid, int maxRoleID)
            {
                var insertList = getBuildingsInsertList(providerID, cid, maxRoleID);

                var deleteList = getBuildingsDeleteList(providerID, cid);

                // insert associations
                DataMgr.ProviderClientDataMapper.AddProvidersClients(insertList);

                // delete associations
                return deleteList;
            }

            protected List<ProvidersClients> updateAssociationsBuildingsGroups(int providerID, int cid, int maxRoleID)
            {
                var insertList = getBuildingGroupsInsertList(providerID, cid, maxRoleID);
                var deleteList = getBuildingGroupsDeleteList(providerID, cid);

                // insert associations
                DataMgr.ProviderClientDataMapper.AddProvidersClients(insertList);

                // delete associations
                return deleteList;
            }
            #endregion
        #endregion
    }
}