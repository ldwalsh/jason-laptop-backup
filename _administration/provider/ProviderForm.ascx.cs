﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using System;

namespace CW.Website._administration.provider
{
	public partial class ProviderForm: FormGenericBase<ProviderForm,Provider>
	{
		#region method

			#region override

				public override void PopulateForm(Provider provider)
				{
					LoadHandlers.Add(PropHelper.G<ProviderForm>(f=> f.ddlOrganization), p=> p.Organizations_OrganizationTypeName.OID);

					base.PopulateForm(provider);
				}

                public override void CreateEntity(Provider obj)
                {
			        SaveHandlers.Add
			        (
				        PropHelper.G<ProviderForm>(f=> f.ddlOrganization),
				        delegate(Provider provider)
				        {
					        provider.Organizations_OrganizationTypeName =
					        new Organizations_OrganizationTypeName
						        {
							        OID   = Int32.Parse(ddlOrganization.SelectedValue),
                                    OTNID = DataMgr.OrganizationTypeDataMapper.GetOrganizationTypeName(BusinessConstants.OrgTypeName.OrgTypeNameEnum.Provider).OTNID
						        };
				        }
			        );

			        chkIsActive.Checked = true;

                    base.CreateEntity(obj);
                }

			#endregion

			protected void Page_FirstLoad(Object sender, EventArgs e)
			{
				tab.BindOrganizations(ddlOrganization);
			}

			public Boolean CanProviderBeUpdated(Provider provider, out String message)
			{
                if (DataMgr.ProviderDataMapper.DoesProviderExist(provider.ProviderID, provider.ProviderName))
				{
					message = "{EntityTypeName} already exists.";
				
					return false;
				}

				message = null;

				return true;
			}

			public void UpdateProviderMethod(Provider provider)
			{
                DataMgr.ProviderDataMapper.UpdateProvider(provider);
			}

		#endregion

		#region property

			#region override

				public override Delegates<Provider>.CanEntityBePersistedDelegate CanEntityBePersisted
				{
					get {return CanProviderBeUpdated;}
				}

				public override Action<Provider> PersistEntityAction
				{
					get {return UpdateProviderMethod;}
				}

			#endregion

			public Boolean OrganizationEnabled
			{
				set;
				get;
			}

			public Boolean IsActiveVisible
			{
				set;
				get;
			}

		#endregion
	}
}