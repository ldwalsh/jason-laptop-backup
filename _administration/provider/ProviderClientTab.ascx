﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderClientTab.ascx.cs" Inherits="CW.Website._administration.provider.ProviderClientTab" %>

<h2>Clients to a Provider</h2>
<p>
    Please select clients to be associate to a provider.    
</p>
    <div>
        <a id="lnkSetFocusProvider" runat="server"></a>
        <asp:Label ID="lblProviderClientError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
    </div>
    <div id="Div1" class="divForm" runat="server">   
        <label class="label">*Select Organization:</label>    
        <asp:DropDownList ID="ddlOrganizations" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlOrganizations_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
            <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
        </asp:DropDownList> 
    </div> 
    <div id="Div2" class="divForm" runat="server">   
        <label class="label">*Select Provider:</label>    
        <asp:DropDownList ID="ddlProviders" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlProviders_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
            <asp:ListItem Value="-1">Select Organization...</asp:ListItem>                           
        </asp:DropDownList> 
    </div> 
    <div id="Div4" class="divForm" runat="server">   
        <label class="label">*Max Role:</label>    
        <asp:DropDownList  ID="ddlRoles" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlRole_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
            <asp:ListItem Value="-1">Select Provider...</asp:ListItem>                        
        </asp:DropDownList> 
    </div> 
    <div id="clientProvider" visible="false" runat="server">                                            
        <hr /> 
        <div class="richText">
            <p>
            (Warning: If you assign a client(s) to the "Available" list box and click the "Reassign" button, you will remove all provider associations for those clients.  This action is irreversable.)
            </p>
        </div>                                            
        <div id="Div3" class="divForm" runat="server">                                       
            <label class="label">Available:</label>    
            <asp:ListBox ID="lbProviderClientTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
            </asp:ListBox>                                                                                    
            <div class="divArrows">
                <asp:ImageButton CssClass="up-arrow"  ID="ImageButton3"  OnClick="btnProviderClientUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                <asp:ImageButton CssClass="down-arrow" ID="ImageButton4"  OnClick="btnProviderClientDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
            </div>                                                                                               
            <label class="label">Assigned:</label>
            <asp:ListBox ID="lbProviderClientBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
            </asp:ListBox> 
        </div>
        <asp:LinkButton CssClass="lnkButton" ID="LinkButton1" runat="server" Text="Reassign" OnClientClick="return confirm('Are you sure you wish to reassign these clients?\n\nThis will remove all provider to client assocations for clients placed in the available list box and that it is irreversable.');"  OnClick="updateProviderClientButton_Click" ValidationGroup="UpdateProviderClient"></asp:LinkButton>                                           
    </div>
