﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderClientForm.ascx.cs" Inherits="CW.Website._administration.provider.ProviderClientForm" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:HiddenField ID="hdnID" runat="server" Visible="false" />
<asp:PlaceHolder ID="plhAdd" runat="server">
<div class="divForm">
    <label class="label">*Organization (Organizations with providers):</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlOrganizations" OnSelectedIndexChanged="ddlOrganizations_OnSelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" runat="server" >
        <asp:ListItem Value="-1">Select one...</asp:ListItem>
    </asp:DropDownList>
</div>
<div class="divForm">
    <label class="label">*Provider:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlProviders" OnSelectedIndexChanged="ddlProviders_OnSelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" runat="server" >
        <asp:ListItem Value="-1">Select organization first...</asp:ListItem>
    </asp:DropDownList>
</div>
<div class="divForm">
    <label class="label">*Client:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlClients" OnSelectedIndexChanged="ddlClients_OnSelectedIndexChanged" AutoPostBack="true" AppendDataBoundItems="true" runat="server" >
        <asp:ListItem Value="-1">Select provider first...</asp:ListItem>
    </asp:DropDownList>
</div>
</asp:PlaceHolder>
<asp:PlaceHolder ID="plhEdit" runat="server">
<asp:HiddenField ID="hdnCID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnProviderID" runat="server" Visible="false" />
<div class="divForm">
    <label class="label">Provider:</label>
    <asp:Literal ID="litProvider" runat="server"></asp:Literal>
</div>
<div class="divForm">
    <label class="label">Client:</label>
    <asp:Literal ID="litClient" runat="server"></asp:Literal>
</div>
</asp:PlaceHolder>
<div class="divForm">
    <label class="label">*Max Role:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlMaxRoles" AppendDataBoundItems="true" runat="server" ><asp:ListItem Value="-1">Select Role...</asp:ListItem></asp:DropDownList>
</div>
<asp:LinkButton CssClass="lnkButton" ID="updateButton" runat="server" Text="Update" ValidationGroup="AddProvidersClient" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="addOrganizationRequiredValidator" runat="server" ErrorMessage="Organization is a required field." ControlToValidate="ddlOrganizations" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddProvidersClient" />
<ajaxToolkit:ValidatorCalloutExtender ID="addOrganizationRequiredValidatorExtender" runat="server" TargetControlID="addOrganizationRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="addProviderRequiredValidator" runat="server" ErrorMessage="Provider is a required field." ControlToValidate="ddlProviders" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddProvidersClient" />
<ajaxToolkit:ValidatorCalloutExtender ID="addProviderRequiredValidatorExtender" runat="server" TargetControlID="addProviderRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="addClientRequiredValidator" runat="server" ErrorMessage="Client is a required field." ControlToValidate="ddlClients" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddProvidersClient" />
<ajaxToolkit:ValidatorCalloutExtender ID="addClientRequiredValidatorExtender" runat="server" TargetControlID="addClientRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="addMaxRoleRequiredValidator" runat="server" ErrorMessage="Max Role is a required field." ControlToValidate="ddlMaxRoles" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddProvidersClient" />
<ajaxToolkit:ValidatorCalloutExtender ID="addMaxRoleRequiredValidatorExtender" runat="server" TargetControlID="addMaxRoleRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
