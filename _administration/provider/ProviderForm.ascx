﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderForm.ascx.cs" Inherits="CW.Website._administration.provider.ProviderForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:HiddenField ID="hdnID" runat="server" Visible="false" />
<div class="divForm">
    <label class="label">*Name:</label>
    <asp:TextBox ID="txtProviderName" CssClass="textbox" MaxLength="50" runat="server" />
</div>
<div class="divForm">
    <label class="label">*Organization:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlOrganization" AppendDataBoundItems="true" runat="server" >
        <asp:ListItem Value="-1">Select one...</asp:ListItem>
    </asp:DropDownList>
</div>
</asp:PlaceHolder>
<div class="divForm" runat="server">
    <label class="label">*Active:</label>
    <asp:CheckBox ID="chkIsActive" CssClass="checkbox" runat="server" />                                                        
</div>
<asp:LinkButton CssClass="lnkButton" ID="updateButton" runat="server" Text="Update" ValidationGroup="" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="addProviderNameRequiredValidator" runat="server" ErrorMessage="Name is a required field." ControlToValidate="txtProviderName" SetFocusOnError="true" Display="None" ValidationGroup="AddProvider" />
<ajaxToolkit:ValidatorCalloutExtender ID="addProviderNameRequiredValidatorExtender" runat="server" BehaviorID="addProviderNameRequiredValidatorExtender" TargetControlID="addProviderNameRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
<asp:RequiredFieldValidator ID="addOrganizationRequiredValidator" runat="server" ErrorMessage="Organization is a required field." ControlToValidate="ddlOrganization" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddProvider" />
<ajaxToolkit:ValidatorCalloutExtender ID="addOrganizationRequiredValidatorExtender" runat="server" BehaviorID="addOrganizationRequiredValidatorExtender" TargetControlID="addOrganizationRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
