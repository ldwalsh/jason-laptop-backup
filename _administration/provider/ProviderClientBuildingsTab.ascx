﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderClientBuildingsTab.ascx.cs" Inherits="CW.Website._administration.provider.ProviderClientBuildingsTab" %>

<a id="lnkSetFocusMessage" runat="server"></a>
<asp:HiddenField ID="hdnMaxRoleID" runat="server" Visible="false" />
<asp:HiddenField ID="hdnUnrestrictedGuid" runat="server" Visible="false" />
<asp:Label ID="lblError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
<h2>Clients Provider to Buildings</h2>
<p>
    Please select building groups and buildings to associated with a client and provider. Building Groups and Buildings are shown/available based on client selected.
</p>
    <div id="Div1" class="divForm" runat="server">   
        <label class="label">*Select Organization:</label>    
        <asp:DropDownList ID="ddlOrganizations" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlOrganizations_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
            <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
        </asp:DropDownList> 
    </div> 
    <div id="Div2" class="divForm" runat="server">   
        <label class="label">*Select Provider:</label>    
        <asp:DropDownList ID="ddlProviders" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlProviders_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
            <asp:ListItem Value="-1">Select organization first...</asp:ListItem>                           
        </asp:DropDownList> 
    </div> 
    <div id="Div3" class="divForm" runat="server">   
        <label class="label">*Select Client:</label>    
        <asp:DropDownList ID="ddlClients" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlClients_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
            <asp:ListItem Value="-1">Select client first...</asp:ListItem>                           
        </asp:DropDownList> 
    </div> 
    <asp:Panel ID="pnlIsRestricted" CssClass="divForm" runat="server" Visible="false">
        <label class="label">Do you want to change to building restricted mode?:</label>    
        <asp:CheckBox ID="chkIsRestricted" CssClass="dropdown" OnCheckedChanged="chkIsRestricted_OnCheckedChanged" AutoPostBack="true" runat="server" />
    </asp:Panel>

<div id="divEditBuildingGroupsListBoxs" runat="server" visible="false">
        <hr />
        <h2>Assign Building Groups</h2>
        <div class="divForm">                                                        
            <label class="label">Available Building Groups:</label>    
            <asp:ListBox ID="lbEditBuildingGroupsTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
            </asp:ListBox>
            <div class="divArrows">
            <asp:ImageButton CssClass="up-arrow"  ID="btnEditBuildingGroupsUp" ImageUrl="/_assets/images/up-arrow.png" runat="server" OnClick="btnEditBuildingGroupsUpButton_Click" CausesValidation="false"></asp:ImageButton>
            <asp:ImageButton CssClass="down-arrow" ID="btnEditBuildingGroupsDown" ImageUrl="/_assets/images/down-arrow.png" runat="server" OnClick="btnEditBuildingGroupsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
            </div>
            <label class="label">Assigned Building Groups:</label>                
            <asp:ListBox ID="lbEditBuildingGroupsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
            </asp:ListBox>                                                    
        </div>
</div>  
                                                                                                                         
<div id="divEditBuildingsListBoxs" runat="server" visible="false">
        <hr />
        <h2>Assign Buildings</h2>
        <div class="divForm">                                                        
            <label class="label">Available Building:</label>             
            <asp:ListBox ID="lbEditBuildingsTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
            </asp:ListBox>
            <div class="divArrows">
            <asp:ImageButton CssClass="up-arrow"  ID="btnEditBuildingsUp" ImageUrl="/_assets/images/up-arrow.png" runat="server" OnClick="btnEditBuildingsUpButton_Click" CausesValidation="false"></asp:ImageButton>
            <asp:ImageButton CssClass="down-arrow" ID="btnEditBuildingsDown" ImageUrl="/_assets/images/down-arrow.png" runat="server" OnClick="btnEditBuildingsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
            </div>
            <label class="label">Assigned Building:</label>                
            <asp:ListBox ID="lbEditBuildingsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
            </asp:ListBox>                                                    
        </div>
    <asp:LinkButton CssClass="lnkButton" ID="btnAddUser" runat="server" Text="Reassign"  OnClick="updateAssociationsButton_Click" ValidationGroup="AddProviderClientBuildingAssociations"></asp:LinkButton>
</div>

