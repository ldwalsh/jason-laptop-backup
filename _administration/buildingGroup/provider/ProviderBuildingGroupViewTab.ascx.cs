﻿using CW.Data;
using CW.Data.Models.BuildingGroup;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace CW.Website._administration.buildingGroup.provider
{
    public partial class ProviderBuildingGroupViewTab : ViewTabWithFormBase<BuildingGroup, ProviderBuildingGroupAdministration>
    {
		#region methods

			private void Page_Init()
			{
				CanEntityBeUpdated = form.CanEntityBePersisted;
				CanEntityBeDeleted = CanBuildingGroupBeDeleted;

				UpdateEntityMethod = form.PersistEntityAction;
				DeleteEntityMethod = DeleteBuildingGroup;
			}

			private Boolean CanBuildingGroupBeDeleted(String buildingGroupID, out String message)
			{
				//no constraints on building group deletions, yet.
				message = null;

				return true;
			}

			protected void DeleteBuildingGroup(String buildingGroupID)
			{
				var id = Convert.ToInt32(buildingGroupID);

				DataMgr.BuildingGroupDataMapper.DeleteAllBuidingGroupBuildingsByBuildingGroupID(id);
				DataMgr.BuildingGroupDataMapper.DeleteBuildingGroup(id);

				page.TabStateMonitor.ChangeState();
			}

			private BuildingGroup GetBuildingGroupForDetailView(String buildingGroupID)
			{
				return DataMgr.BuildingGroupDataMapper.GetBuildingGroupByID(Convert.ToInt32(buildingGroupID), (_ => _.Client));
			}

		#endregion

		#region events

			protected override void GridRowDataBound(Object sender, EventArgs e)
			{
				var rows = grid.Rows;

				foreach (GridViewRow row in rows)
				{
					var buildings = DataMgr.BuildingDataMapper.GetAllBuildingsByBuildingGroupID(Convert.ToInt32(grid.DataKeys[row.RowIndex].Value));

					if (((row.RowState & DataControlRowState.Edit) == 0) && buildings.Any())
					{
						var buildingList = new StringBuilder();

						buildingList.Append("<ul>");

						foreach (var building in buildings) 
							buildingList.Append("<li>" + building.BuildingName + "</li>");

						buildingList.Append("</ul>");

						var litBuildingList = (Literal)row.FindControl("litBuildingList");

						if (litBuildingList != null) litBuildingList.Text = buildingList.ToString();
					}
				}
			}

		#endregion

		#region properties

			protected override String IdColumnName
			{
				get { return PropHelper.G<BuildingGroup>(bg => bg.BuildingGroupID); }
			}

			protected override String NameField
			{
				get { return PropHelper.G<BuildingGroup>(bg => bg.BuildingGroupName); }
			}

			protected override String EntityTypeName
			{
				get { return "building group"; }
			}

			protected override IEnumerable<String> GridColumnNames
			{
				get { return PropHelper.F<GetBuildingGroupsData>(_ => _.BuildingGroupName); }
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get { return (searchText => DataMgr.BuildingGroupDataMapper.GetAllSearchedBuildingsGroupsByCID(searchText.FirstOrDefault(), siteUser.CID)); }
			}

			protected override GetEntityDelegate GetEntityForEditing
			{
				get { return (bgid => DataMgr.BuildingGroupDataMapper.GetBuildingGroupByID(Int32.Parse(bgid), (_ => _.Client))); }
			}

			protected override GetEntityDelegate GetEntityForDetailView
			{
				get { return GetBuildingGroupForDetailView; }
			}

			protected override String CacheSubKey
			{
				get {return null;}
			}

		#endregion
    }
}