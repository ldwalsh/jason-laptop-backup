﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ProviderBuildingGroupAddTab.ascx.cs" Inherits="CW.Website._administration.buildingGroup.provider.ProviderBuildingGroupAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="../../TabHeader.ascx"  %>
<%@ Register TagPrefix="CW" TagName="BuildingGroupForm" Src="../BuildingGroupForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Building Group" />
<CW:BuildingGroupForm ID="form" runat="server" FormMode="Insert" UpdateButtonCaption="Add Building Group" />