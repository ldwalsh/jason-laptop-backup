﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.Master" AutoEventWireup="false" CodeBehind="ProviderBuildingGroupAdministration.aspx.cs" Inherits="CW.Website._administration.buildingGroup.provider.ProviderBuildingGroupAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="~/_administration/buildingGroup/provider/ProviderBuildingGroupViewTab.ascx" tagname="ProviderBuildingGroupViewTab" tagprefix="CW" %>
<%@ Register src="~/_administration/buildingGroup/provider/ProviderBuildingGroupAddTab.ascx" tagname="ProviderBuildingGroupAddTab" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <CW:PageHeader ID="pageHeader" runat="server" Title="Provider Building Group Administration" Description="The provider building group administration area is used to view, add, and edit building groups." />

      <div class="administrationControls">
        <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Building Groups" />
            <telerik:RadTab Text="Add Building Group" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">  
            <CW:ProviderBuildingGroupViewTab runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server"> 
            <CW:ProviderBuildingGroupAddTab runat="server" />
          </telerik:RadPageView>
        </telerik:RadMultiPage>
      </div>

</asp:Content>
