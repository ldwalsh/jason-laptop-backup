﻿using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._administration.buildingGroup
{
    public partial class BuildingGroupForm : FormGenericBase<BuildingGroupForm, BuildingGroup>
    {
        #region fields

            private String dataTextField = "BuildingName";
            private String dataValueField = "BID";

        #endregion

        #region events

            private void Page_FirstInit()
            {
                if (FormMode == PersistModeEnum.Insert) BindBuildings();
            }

            #region button events

                protected void BuildingsUpButtonClick(Object sender, EventArgs e)
                {
                    while (lbBuildingsBottom.SelectedIndex != -1)
                    {
                        lbBuildingsTop.Items.Add(lbBuildingsBottom.SelectedItem);
                        lbBuildingsBottom.Items.Remove(lbBuildingsBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBuildingsTop);
                }

                protected void BuildingsDownButtonClick(Object sender, EventArgs e)
                {
                    while (lbBuildingsTop.SelectedIndex != -1)
                    {
                        lbBuildingsBottom.Items.Add(lbBuildingsTop.SelectedItem);
                        lbBuildingsTop.Items.Remove(lbBuildingsTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBuildingsBottom);
                }

            #endregion

            private void BindBuildings()
            {
                ClearListBoxes();

                var buildings = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID);

                lbBuildingsTop.DataSource = buildings;
                lbBuildingsTop.DataTextField = dataTextField;
                lbBuildingsTop.DataValueField = dataValueField;
                lbBuildingsTop.DataBind();
            }

        #endregion

        #region methods

            #region overrides

                public override void PopulateForm(BuildingGroup buildingGroup)
                {
                    if (FormMode == PersistModeEnum.Update)
                    {
                        var buildingGroupID = buildingGroup.BuildingGroupID;
   
                        hdnBuildingGroupID.Value = buildingGroupID.ToString();
                        txtBuildingGroupName.Text = buildingGroup.BuildingGroupName;

                        ClearListBoxes();

                        var buildingsNotInGroup = DataMgr.BuildingDataMapper.GetAllBuildingsNotInAGroupByClientID(siteUser.CID, buildingGroupID);

                        if (buildingsNotInGroup.Any())
                        {
                            lbBuildingsTop.DataSource = buildingsNotInGroup;
                            lbBuildingsTop.DataTextField = dataTextField;
                            lbBuildingsTop.DataValueField = dataValueField;
                            lbBuildingsTop.DataBind();
                        }

                        var buildingsInGroup = DataMgr.BuildingDataMapper.GetAllBuildingsByBuildingGroupID(buildingGroupID);

                        if (buildingsInGroup.Any())
                        {
                            lbBuildingsBottom.DataSource = buildingsInGroup;
                            lbBuildingsBottom.DataTextField = dataTextField;
                            lbBuildingsBottom.DataValueField = dataValueField;
                            lbBuildingsBottom.DataBind();
                        }   
                    }
                }

            #endregion

            public Boolean CanBuildingGroupBePersisted(BuildingGroup buildingGroup, out String message)
            {
                if (FormMode == PersistModeEnum.Insert)
                {
                    if (DataMgr.BuildingGroupDataMapper.DoesBuildingGroupExist(buildingGroup.BuildingGroupName))
                    {
                        message = "{EntityTypeName} already exists.";

                        return false;
                    }
                }

                if (lbBuildingsBottom.Items.Count < 2)
                {
                    message = "Two or more buildings are required for a building group.";

                    return false;
                }

                message = null;

                return true;
            }

            public void PersistBuildingGroup(BuildingGroup buildingGroup)
            {
                switch (FormMode)
                {
                    case PersistModeEnum.Insert:
                        buildingGroup.CID = siteUser.CID;
                        buildingGroup.DateModified = DateTime.UtcNow;
                        DataMgr.BuildingGroupDataMapper.InsertBuildingGroup(buildingGroup);
                        break;
                    case PersistModeEnum.Update:
                        DataMgr.BuildingGroupDataMapper.UpdatePartialBuildingGroup(buildingGroup);
                        DataMgr.BuildingGroupDataMapper.DeleteAllBuidingGroupBuildingsByBuildingGroupID(buildingGroup.BuildingGroupID);
                        break;
                }

                foreach (ListItem item in lbBuildingsBottom.Items)
                {
                    var mBuildingGroup_Building = new BuildingGroups_Building();
        
                    mBuildingGroup_Building.BID = Convert.ToInt32(item.Value);
                    mBuildingGroup_Building.BuildingGroupID = buildingGroup.BuildingGroupID;

                    DataMgr.BuildingGroupDataMapper.InsertBuildingGroupBuilding(mBuildingGroup_Building);
                }
            }

            private void ClearListBoxes()
            {
                lbBuildingsTop.Items.Clear();
                lbBuildingsBottom.Items.Clear();
            }

        #endregion

        #region properties

            public override Delegates<BuildingGroup>.CanEntityBePersistedDelegate CanEntityBePersisted
            {
                get { return CanBuildingGroupBePersisted; }
            }

            public override Action<BuildingGroup> PersistEntityAction
            {
                get { return PersistBuildingGroup; }
            }

        #endregion
    }
}