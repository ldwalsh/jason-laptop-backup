﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingGroupForm.ascx.cs" Inherits="CW.Website._administration.buildingGroup.BuildingGroupForm" %>

<asp:HiddenField ID="hdnBuildingGroupID" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div>

<div class="divForm">
  <label class="label">*Building Group Name:</label>
  <asp:TextBox ID="txtBuildingGroupName" CssClass="textbox" MaxLength="50" runat="server" />
</div>

<div class="divForm">
  <label class="label">Buildings Available:</label>
  <asp:ListBox ID="lbBuildingsTop" CssClass="listbox" runat="server" SelectionMode="Multiple" />                                                        

  <div class="divArrows">
    <asp:ImageButton CssClass="up-arrow"  ID="btnAddBuildingsUp" OnClick="BuildingsUpButtonClick" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
    <asp:ImageButton CssClass="down-arrow" ID="btnAddBuildingsDown" OnClick="BuildingsDownButtonClick" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
  </div>

  <label class="label">Buildings Assigned:</label>
  <asp:ListBox ID="lbBuildingsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple" />
</div>
                                                                                                
<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="buildingGroupNameRFV" runat="server" ErrorMessage="Building Group is a required field." ControlToValidate="txtBuildingGroupName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="buildingGroupNameVCE" runat="server" TargetControlID="buildingGroupNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />