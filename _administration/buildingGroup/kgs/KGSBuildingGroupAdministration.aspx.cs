﻿using CW.Data;
using CW.Utility;
using System;
namespace CW.Website._administration.buildingGroup.kgs
{
    public partial class KGSBuildingGroupAdministration : AdministrationPageBase 
    {
        private void Page_Init()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check kgs super admin or higher.
            if (!siteUser.IsKGSSuperAdminOrHigher)
                Response.Redirect("/Home.aspx");
        }

        public override String IdentifierField
        {
            get { return PropHelper.G<BuildingGroup>(bg => bg.BuildingGroupID); }
        }

        public override String NameField
        {
            get { return PropHelper.G<BuildingGroup>(bg => bg.BuildingGroupName); }
        }

        public override String EntityTypeName
        {
            get { return "building group"; }
        }

        public enum TabMessages
        {
            AddBuildingGroup
        }
    }
}