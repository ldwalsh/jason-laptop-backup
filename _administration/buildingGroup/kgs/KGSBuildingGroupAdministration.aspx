﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.Master" AutoEventWireup="false" CodeBehind="KGSBuildingGroupAdministration.aspx.cs" Inherits="CW.Website._administration.buildingGroup.kgs.KGSBuildingGroupAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="KGSBuildingGroupViewTab.ascx" tagname="BuildingGroupViewTab" tagprefix="CW" %>
<%@ Register src="KGSBuildingGroupAddTab.ascx" tagname="BuildingGroupAddTab" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <CW:PageHeader ID="pageHeader" runat="server" Title="KGS Building Group Administration" Description="The kgs building group administration area is used to view, add, and edit building groups." />

      <div class="administrationControls">
        <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Building Groups" />
            <telerik:RadTab Text="Add Building Group" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">  
            <CW:BuildingGroupViewTab runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server"> 
            <CW:BuildingGroupAddTab runat="server" />
          </telerik:RadPageView>
        </telerik:RadMultiPage>
      </div>

</asp:Content>
