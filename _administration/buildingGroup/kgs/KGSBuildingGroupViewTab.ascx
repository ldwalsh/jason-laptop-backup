﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="KGSBuildingGroupViewTab.ascx.cs" Inherits="CW.Website._administration.buildingGroup.kgs.KGSBuildingGroupViewTab" %>
<%@ Register TagPrefix="CW" TagName="BuildingGroupForm" Src="../BuildingGroupForm.ascx"  %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Building Groups" />

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick" />
</asp:Panel>

<div id="gridTbl">                                        
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns> 
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>
          <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingGroupName" HeaderText="Building Group Name">                                                      
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<BuildingGroup>(_ => _.BuildingGroupName)) %>"><%# GetCellContent(Container, PropHelper.G<BuildingGroup>(_ => _.BuildingGroupName), 50) %></label></ItemTemplate>
      </asp:TemplateField> 

      <asp:TemplateField HeaderText="Buildings">  
        <ItemTemplate><asp:Literal ID="litBuildingList" runat="server" /></ItemTemplate>                                                    
      </asp:TemplateField>        

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this building group permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>        
  </asp:GridView> 
                                     
</div>                                    
<br />
<br />

<!--SELECT CLIENT DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
  <Fields>
    <asp:TemplateField>
      <ControlStyle BorderWidth="0" BorderColor="White" BorderStyle="None" />
      <ItemStyle BorderWidth="0" BorderColor="White" BorderStyle="None" Width="100%" />
      <HeaderStyle Width="1" BorderWidth="0" BorderColor="White" BorderStyle="None" />
      <ItemTemplate>
        <div>
          <h2>Building Group Details</h2>
          <ul class="detailsList">
            <li><strong>Building Group Name: </strong><%# Eval(PropHelper.G<BuildingGroup>(bg => bg.BuildingGroupName)) %></li>
            <li><strong>Client: </strong><%# Eval(PropHelper.G<BuildingGroup, Client>(bg => bg.Client, c => c.ClientName)) %></li>
          </ul>
        </div>
      </ItemTemplate>                                             
    </asp:TemplateField>
  </Fields>
</asp:DetailsView>

<!--EDIT BUILDING GROUP PANEL -->
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">
  <div>
    <h2>Edit Building Group</h2>
  </div>
  <div>
    <CW:BuildingGroupForm ID="form" runat="server" FormMode="Update" UpdateButtonCaption="Edit Building Group" />
  </div>
</asp:Panel>