﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._administration.buildingGroup.kgs
{
    public partial class KGSBuildingGroupAddTab : AddTabBase<BuildingGroup, KGSBuildingGroupAdministration>
    {
        #region methods

            private void Page_Init()
            {
                CanEntityBeAdded = form.CanBuildingGroupBePersisted;
                AddEntityMethod = form.PersistEntityAction;
            }

        #endregion

        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList { get { return Enumerable.Empty<Enum>(); } }

                protected override String IdColumnName
                {
                    get { return PropHelper.G<BuildingGroup>(bg => bg.BuildingGroupID); }
                }

                protected override String NameField
                {
                    get { return PropHelper.G<BuildingGroup>(bg => bg.BuildingGroupName); }
                }

                protected override String EntityTypeName
                {
                    get { return "building group"; }
                }

            #endregion

        #endregion
    }
}