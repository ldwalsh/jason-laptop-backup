﻿using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;

namespace CW.Website._administration
{
    public class DynamicControlTracker
    {
        #region STRUCT

            [
            Serializable
            ]
            public struct ControlInfo
            {
                public Int32  ControlIx;
                public Int32? ControlNestedIx;
                public String Html;
            }

        #endregion

        #region field

            private readonly SiteUserControl parent;
            private readonly ViewState viewState;
            private readonly String id;
            
            private List<ControlInfo> controlInfos;

        #endregion

        #region constructor

            public DynamicControlTracker(SiteUserControl parent, StateBag stateBag)
            {
                this.parent = parent;
                
                viewState    = new ViewState(stateBag);
                id           = String.Concat(GetType().Name, "_", parent.ClientID);
                controlInfos = new List<ControlInfo>();

                //

                parent.LoadedViewState += LoadedViewStateEvent;
                parent.PreRender       += PreRenderEvent;
            }

        #endregion

        #region method

            public void AddControl(Control control, Control afterControl)
            {
                var html = ControlHelper.RenderControl(control).ToString();

                AddControl(html, afterControl);
            }
            //
            public void AddControl(String html, Control afterControl)
            {
                Int32 index;

                var lc = ControlHelper.NextControl<LiteralControl>(afterControl, out index);

                if (lc == null)
                {
                    lc = new LiteralControl{Text=String.Empty};

                    parent.Controls.AddAt(index, lc);
                }

                lc.Text = lc.Text.Insert(0, html);

                var ix = parent.Controls.IndexOf(lc);

                var ci = new ControlInfo(){Html=html};

                if (ix == -1)
                {
                    ci.ControlIx       = parent.Controls.IndexOf(lc.Parent);
                    ci.ControlNestedIx = lc.Parent.Controls.IndexOf(lc);
                }
                else
                {
                    ci.ControlIx = ix;
                }

                controlInfos.Add(ci);
            }

            public void Clear()
            {
                viewState.Remove(id);

                if (!controlInfos.Any()) return;
                
                foreach (var ci in controlInfos)
                {
                    var lc = ((ci.ControlNestedIx == null) ? parent.Controls[ci.ControlIx] : parent.Controls[ci.ControlIx].Controls[ci.ControlNestedIx.Value]) as LiteralControl;
                
                    lc.Text = lc.Text.Replace(ci.Html, String.Empty);
                }

                controlInfos.Clear();
            }

            private void LoadedViewStateEvent(Object sender, EventArgs e)
            {
                controlInfos = (viewState.Get<List<ControlInfo>>(id) ?? new List<ControlInfo>());

                if (!controlInfos.Any()) return;

                foreach (var ci in controlInfos)
                {
                    var lc = ((ci.ControlNestedIx == null) ? parent.Controls[ci.ControlIx] : parent.Controls[ci.ControlIx].Controls[ci.ControlNestedIx.Value]) as LiteralControl;
                    
                    //if (lc == null) //apparanetly unnecessary... I could not find a code path where lc is a null value
                    //{
                    //    lc = new LiteralControl{Text=String.Empty};

                    //    parent.Controls.AddAt(ci.ControlIx, lc);
                    //}

                    lc.Text = lc.Text.Insert(0, ci.Html);
                }
            }

            private void PreRenderEvent(Object sender, EventArgs e)
            {
                viewState.Set(id, controlInfos);
            }

        #endregion
    }
}