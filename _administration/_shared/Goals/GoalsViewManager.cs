﻿using CW.Business;
using CW.Data;
using CW.Data.Models;
using CW.Utility;
using CW.Website._administration.client;
using CW.Website._framework;
using CW.Common.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using static CW.Website._administration.client.GoalService;

namespace CW.Website._administration._shared.Goals
{
    public sealed class GoalsViewManager
    {
        #region fields
        private GoalsSettingsView _view;
        private DataManager _dataMgr;                                
        private GoalsViewConfigModel _goalsConfigData;
        private int _entityID;
        private ServiceGoalType _goalType;
        #endregion

        #region properties
        private GoalsViewConfigModel GoalsConfigData
        {
            get
            {
                if (_goalsConfigData == null)
                {                    
                    _goalsConfigData = new GoalsViewConfigModel();
                    _goalsConfigData.GoalTypes = _dataMgr.GoalDataMapper.GetAllGoalTypes();
                    _goalsConfigData.GoalMetrics = _dataMgr.GoalDataMapper.GetAllGoalMetrics();
                    _goalsConfigData.inheritCheckVisible = _goalType == ServiceGoalType.Building ? true : false;
                }
                                                
                return _goalsConfigData;
            }
            set
            {
                _goalsConfigData = value;
            }
        }
        #endregion


        public GoalsViewManager(GoalsSettingsView view, DataManager dataMgr, int id, ServiceGoalType goalType)
        {
            if (view == null || dataMgr == null) throw new ArgumentNullException();
                        
            _view = view;
            _dataMgr = dataMgr;
            _entityID = id;
            _goalType = goalType;  
        }

        public void ConfigureGoalViewState()
        {
            _view.WireUpGoalsView(GoalsConfigData);
            UpdateGoalView();
        }

        public void UpdateGoalView()
        {
            GoalsViewUpdateModel goalsUpdateData = getExistingGoals();
            _view.UpdateView(goalsUpdateData);
        }

        private GoalsViewUpdateModel getExistingGoals()
        {
            GoalService goalService = new GoalService(_goalType);
            GoalsViewUpdateModel goalsUpdateData = new GoalsViewUpdateModel();
            bool areMultipleCurrencies = false;
            bool areMultipleUnits = false;

            //goals below can either be Client or Building goals
            dynamic energyGoal = null;
            dynamic costGoal = null;
            dynamic carbonGoal = null;

            //retrieve existing goals: if invalid type throw and exception
            if (_goalType == ServiceGoalType.Client)
            {
                IEnumerable<ClientGoal> clientGoals = goalService.GetClientGoalsByCID(_entityID);
                energyGoal = clientGoals.Where(_ => _.GoalMetricID == BusinessConstants.GoalMetric.EnergyMetricID).FirstOrDefault();
                costGoal = clientGoals.Where(_ => _.GoalMetricID == BusinessConstants.GoalMetric.CostMetricID).FirstOrDefault();
                carbonGoal = clientGoals.Where(_ => GoalsConfigData.OtherGoalIDs.Contains(_.GoalMetricID)).FirstOrDefault();
                areMultipleCurrencies = goalService.CheckIfClientHasBuildingsMulitpleCurrencies(_entityID);
                areMultipleUnits = goalService.CheckIfClientHasBuildingsMultipleUnits(_entityID);
            }
            else if (_goalType == ServiceGoalType.Building)
            {
                IEnumerable<BuildingGoal> buildingGoals = goalService.GetBuildingGoalsByBID(_entityID);
                energyGoal = buildingGoals.Where(_ => _.GoalMetricID == BusinessConstants.GoalMetric.EnergyMetricID).FirstOrDefault();
                costGoal = buildingGoals.Where(_ => _.GoalMetricID == BusinessConstants.GoalMetric.CostMetricID).FirstOrDefault();
                carbonGoal = buildingGoals.Where(_ => GoalsConfigData.OtherGoalIDs.Contains(_.GoalMetricID)).FirstOrDefault();
            }

            //set abs/pct ddls
            goalsUpdateData.DDLCostAbsPctVal = costGoal == null ? BusinessConstants.GoalType.NoGoalTypeID.ToString() : 
                costGoal.GoalTypeID.ToString();
            goalsUpdateData.DDLEnergyAbsPctVal = energyGoal == null ? BusinessConstants.GoalType.NoGoalTypeID.ToString() : 
                energyGoal.GoalTypeID.ToString();
            goalsUpdateData.DDLCarbonAbsPctVal = carbonGoal == null ? BusinessConstants.GoalType.NoGoalTypeID.ToString() : 
                carbonGoal.GoalTypeID.ToString();

            //set goal value abs fields
            goalsUpdateData.RadCostValueAbsVal = costGoal != null && costGoal.GoalTypeID == 
                BusinessConstants.GoalType.AbsoluteGoalTypeID ? costGoal.GoalValue.ToString() : "0";
            goalsUpdateData.RadEnergyValueAbsVal = energyGoal != null && energyGoal.GoalTypeID == 
                BusinessConstants.GoalType.AbsoluteGoalTypeID ? energyGoal.GoalValue.ToString() : "0";
            goalsUpdateData.RadCarbonValueAbsVal = carbonGoal != null && carbonGoal.GoalTypeID == 
                BusinessConstants.GoalType.AbsoluteGoalTypeID ? carbonGoal.GoalValue.ToString() : "0";

            //set goal value pct fields
            goalsUpdateData.RadCostValuePctVal = costGoal != null && costGoal.GoalTypeID == 
                BusinessConstants.GoalType.PercentageGoalTypeID ? costGoal.GoalValue.ToString() : "0";
            goalsUpdateData.RadEnergyValuePctVal = energyGoal != null && energyGoal.GoalTypeID == 
                BusinessConstants.GoalType.PercentageGoalTypeID ? energyGoal.GoalValue.ToString() : "0";
            goalsUpdateData.RadCarbonValuePctVal = carbonGoal != null && carbonGoal.GoalTypeID == 
                BusinessConstants.GoalType.PercentageGoalTypeID ? carbonGoal.GoalValue.ToString() : "0";

            //set carbon goal type dropdown
            goalsUpdateData.DDLOtherTypeVal = carbonGoal != null ? carbonGoal.GoalMetricID.ToString() : 
                BusinessConstants.GoalType.NoGoalTypeID.ToString();

            //set multiple units/currencies bools
            goalsUpdateData.AreMultipleCurrencies = areMultipleCurrencies;
            goalsUpdateData.AreMultipleUnits = areMultipleUnits;

            return goalsUpdateData;
        }

        public string SaveGoals()
        {
            GoalService goalService = new GoalService(_goalType);
            GoalsViewUpdateModel goalsUpdateData = _view.GetGoalFieldsFromView();            
            return goalService.ParseAndSaveGoalsFromRequestFields(goalsUpdateData, _entityID);
        }
    }
}