﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="GoalsSettingsView.ascx.cs" Inherits="CW.Website._administration._shared.Goals.GoalsSettingsView" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<script>
    userCurrency = "<%=userCurrency%>";;
</script>

<div id="goalsSection" runat="server">
    <div class="divForm">
      <label class="label">Cost:</label>
      <asp:DropDownList ID="ddlCostAbsPct" runat="server" CssClass="dropdownNarrow ddlCost" AppendDataBoundItems="true" onchange="KGS.DisplayValueEditFields()"/>  
      <div class="costValPct">
         <telerik:RadNumericTextBox runat="server" ID="radCostValuePct" CssClass="textboxNarrower" Value="0" MinValue="-100" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
      </div>
      <div class="costValAbs">
         <p id="costAbsMsg" runat="server"><%=costAbsMessage%></p>
         <div id="costAbsEdit" runat="server">
             <telerik:RadMaskedTextBox ID="radCostValueAbs" CssClass="textboxNarrower" runat="server" Mask="##########" PromptChar=""/> 
             <label class="lblCost"></label>
         </div>
      </div>               
    </div>

    <div class="divForm">
      <label class="label">Energy:</label>
      <asp:DropDownList ID="ddlEnergyAbsPct" runat="server" CssClass="dropdownNarrow ddlEnergy" AppendDataBoundItems="true" onchange="KGS.DisplayValueEditFields()"/>    
      <div class ="energyValPct">
          <telerik:RadNumericTextBox runat="server" ID="radEnergyValuePct" CssClass="textboxNarrower" Value="0" MinValue="-100" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
      </div>
      <div class="energyValAbs">
          <p id="energyAbsMsg" runat="server"><%=energyAbsMessage%></p>
          <div id="energyAbsEdit" runat="server">
            <telerik:RadMaskedTextBox ID="radEnergyValueAbs" CssClass="textboxNarrower" runat="server" Mask="##########" PromptChar=""/> 
            <label class="lblEnergy"></label>    
          </div>
      </div>      
    </div>

    <div class="divForm">
      <asp:DropDownList ID="ddlOtherType" runat="server" CssClass="dropdownNarrowLeft ddlCarbonMetric" AppendDataBoundItems="true" onchange="KGS.SetUnitLabels()"/>
      <asp:DropDownList ID="ddlCarbonAbsPct" runat="server" CssClass="dropdownNarrow ddlCarbon" AppendDataBoundItems="true" onchange="KGS.DisplayValueEditFields()"/>   
      <div class="carbonValPct">
          <telerik:RadNumericTextBox runat="server" ID="radCarbonValuePct" CssClass="textboxNarrower" Value="0" MinValue="-100" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
      </div>
      <div class="carbonValAbs">
          <p id="otherAbsMsg" runat="server"><%=energyAbsMessage%></p>
          <div id="otherAbsEdit" runat="server">
            <telerik:RadMaskedTextBox ID="radCarbonValueAbs" CssClass="textboxNarrower" runat="server" Mask="##########" PromptChar=""/> 
            <label class="lblOther"></label>    
          </div>
      </div>      
    </div>
</div>
