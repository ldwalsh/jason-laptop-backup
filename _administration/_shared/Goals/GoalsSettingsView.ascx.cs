﻿using CW.Data;
using CW.Data.Models;
using CW.Utility;
using CW.Website._administration.client;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._administration._shared.Goals
{
    public partial class GoalsSettingsView : SiteUserControl
    {
        #region fields            
                
        protected string userCurrency;
        protected readonly string costAbsMessage = "Cannot set absolute value at the client level: Client has buildings with multiple currencies.";
        protected readonly string energyAbsMessage = "Cannot set absolute value at the client level: Client has buildings with mutliple units.";

        #endregion

        protected void Page_Load()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "refreshGoalFields", "KGS.ToggleGoalSettingsView();", true);
            userCurrency = setUserCurrency();            
        }

        #region events

        #endregion

        #region methods       
        public void UpdateView(GoalsViewUpdateModel goalsUpdateData)
        {
            //set abs/pct ddls
            ddlCostAbsPct.SelectedValue = goalsUpdateData.DDLCostAbsPctVal;
            ddlEnergyAbsPct.SelectedValue = goalsUpdateData.DDLEnergyAbsPctVal;
            ddlCarbonAbsPct.SelectedValue = goalsUpdateData.DDLCarbonAbsPctVal;

            //set goal value abs fields
            radCostValueAbs.Text = goalsUpdateData.RadCostValueAbsVal;
            radEnergyValueAbs.Text = goalsUpdateData.RadEnergyValueAbsVal;
            radCarbonValueAbs.Text = goalsUpdateData.RadCarbonValueAbsVal;

            //set goal value pct fields
            radCostValuePct.Text = goalsUpdateData.RadCostValuePctVal;
            radEnergyValuePct.Text = goalsUpdateData.RadEnergyValuePctVal;
            radCarbonValuePct.Text = goalsUpdateData.RadCarbonValuePctVal;

            //set carbon goal type dropdown
            ddlOtherType.SelectedValue = goalsUpdateData.DDLOtherTypeVal;

            //set visibility of absolute controls (i.e. disable editing if client has 
            //buildings with multiple currencies/units
            costAbsMsg.Visible = goalsUpdateData.AreMultipleCurrencies ? true : false;
            costAbsEdit.Visible = goalsUpdateData.AreMultipleCurrencies ? false : true;
            energyAbsMsg.Visible = goalsUpdateData.AreMultipleUnits ? true : false;
            energyAbsEdit.Visible = goalsUpdateData.AreMultipleUnits ? false : true;
            otherAbsMsg.Visible = goalsUpdateData.AreMultipleUnits ? true : false;
            otherAbsEdit.Visible = goalsUpdateData.AreMultipleUnits ? false : true;
        }

        public GoalsViewUpdateModel GetGoalFieldsFromView()
        {
            GoalsViewUpdateModel goalsUpdateData = new GoalsViewUpdateModel();

            //set abs/pct ddls
            goalsUpdateData.DDLCostAbsPctVal = ddlCostAbsPct.SelectedValue;
            goalsUpdateData.DDLEnergyAbsPctVal = ddlEnergyAbsPct.SelectedValue;
            goalsUpdateData.DDLCarbonAbsPctVal = ddlCarbonAbsPct.SelectedValue;

            //set goal value abs fields
            goalsUpdateData.RadCostValueAbsVal = radCostValueAbs.Text;
            goalsUpdateData.RadEnergyValueAbsVal = radEnergyValueAbs.Text;
            goalsUpdateData.RadCarbonValueAbsVal = radCarbonValueAbs.Text;

            //set goal value pct fields
            goalsUpdateData.RadCostValuePctVal = radCostValuePct.Text;
            goalsUpdateData.RadEnergyValuePctVal = radEnergyValuePct.Text;
            goalsUpdateData.RadCarbonValuePctVal = radCarbonValuePct.Text;

            //set carbon goal type dropdown
            goalsUpdateData.DDLOtherTypeVal = ddlOtherType.SelectedValue;

            //set multiple currencies/units
            goalsUpdateData.AreMultipleCurrencies = costAbsMsg.Visible;
            goalsUpdateData.AreMultipleUnits = energyAbsMsg.Visible;

            return goalsUpdateData;
        }

        public void WireUpGoalsView(GoalsViewConfigModel goalsConfigData)
        {            
            setViewDropDowns(goalsConfigData);
            setOtherTypeDDL(goalsConfigData);              
        }

        private void setViewDropDowns(GoalsViewConfigModel goalsConfigData)
        {
            setAbsPctDDL(ddlCarbonAbsPct, goalsConfigData);
            setAbsPctDDL(ddlCostAbsPct, goalsConfigData);
            setAbsPctDDL(ddlEnergyAbsPct, goalsConfigData);       
        }

        private string setUserCurrency()
        {
            CultureHelper cultureHelper = new CultureHelper();

            User currentUser = DataMgr.UserDataMapper.GetUser(siteUser.UID, false);

            if (currentUser == null)
            {
                return "";
            }

            return CultureHelper.GetCurrencySymbol(currentUser.LCID);
        }

        private void setAbsPctDDL(DropDownList absPctDDL, GoalsViewConfigModel goalsConfigData)
        {

            absPctDDL.DataTextField = PropHelper.G<GoalType>(_ => _.GoalTypeName);

            absPctDDL.DataValueField = PropHelper.G<GoalType>(_ => _.GoalTypeID);

            absPctDDL.DataSource = goalsConfigData.GoalTypes;

            absPctDDL.DataBind();
        }

        private void setOtherTypeDDL(GoalsViewConfigModel goalsConfigData)
        {           

            ddlOtherType.DataTextField = PropHelper.G<GoalMetric>(_ => _.GoalMetricName);

            ddlOtherType.DataValueField = PropHelper.G<GoalMetric>(_ => _.GoalMetricID);

            ddlOtherType.DataSource = goalsConfigData.GoalMetrics.Where(_ => goalsConfigData.OtherGoalIDs.Contains(_.GoalMetricID));

            ddlOtherType.DataBind();
        }
        
        #endregion


    }
}