﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CMMSSettingsView.ascx.cs" Inherits="CW.Website._administration._shared.CMMS.CMMSSettingsView" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="System.Collections.Generic" %>

<asp:HiddenField ID="hdnConfigId" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">*Work Order System:</label>
  <asp:DropDownList ID="ddlWorkOrderVendor" runat="server" CssClass="dropdown ddlWorkOrderVendor" AppendDataBoundItems="true" />
  <asp:RequiredFieldValidator ID="workOrderVenderRFV" runat="server" ErrorMessage="Work Order System is a required field." ControlToValidate="ddlWorkOrderVendor" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="WorkOrder" />
  <ajaxToolkit:ValidatorCalloutExtender ID="workOrderVenderVCE" runat="server" BehaviorID="workOrderVenderVCE" TargetControlID="workOrderVenderRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</div>

<div class="divForm">  
  <label class="label">*Base Url:</label>
  <asp:TextBox ID="txtBaseUrl" runat="server" CssClass="textbox txtWorkOrderBaseUrl" MaxLength="250" />
  <asp:RequiredFieldValidator ID="baseUrlRFV" runat="server" ErrorMessage="Base Url is a required field." ControlToValidate="txtBaseUrl" SetFocusOnError="true" Display="None" ValidationGroup="WorkOrder" />
  <asp:RegularExpressionValidator ID="baseUrlREV" runat="server"  ErrorMessage="Base Url is not a valid Url" ControlToValidate="txtBaseUrl" SetFocusOnError="true" Display="None" ValidationGroup="WorkOrder"
                                  ValidationExpression="^((http|https|www):\/\/)?([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]*)(\.)([a-zA-Z0-9\~\!\@\#\$\%\^\&\*\(\)_\-\=\+\\\/\?\.\:\;\'\,]+)" />
  <ajaxToolkit:ValidatorCalloutExtender ID="baseUrlVCE" runat="server" BehaviorID="baseUrlVCE" TargetControlID="baseUrlRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <ajaxToolkit:ValidatorCalloutExtender ID="baseUrlREVVCE" runat="server" BehaviorID="baseUrlREVVCE" TargetControlID="baseUrlREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</div>

<div id="divCMMSCode" class="divForm" runat="server" visible="false">
  <label class="label">CMMS Code:</label>
  <asp:Label ID="lblCMMSCode" runat="server" CssClass="labelContent" />
</div>

<div class="divForm">
  <label class="label">Username:</label>
  <asp:TextBox ID="txtUserName" runat="server" CssClass="textbox txtWorkOrderUserName" MaxLength="100" />
</div>

<div class="divForm">
  <label class="label">Password:</label>
  <asp:TextBox ID="txtPassKey" runat="server" CssClass="textbox txtWorkOrderPassKey" MaxLength="100" />
</div>

<div class="divForm">
  <label class="label"></label>
  <asp:Button ID="btnTestConnection" runat="server" 
      Text="Test Connection" OnClientClick="KGS.TestConnection(); return false;" 
      CssClass="lnkButton" CausesValidation="false" UseSubmitBehavior="false" BorderStyle="None" />
</div>

<div class="divForm">
  <label class="label">Notes:</label>
  <textarea id="txtNotes" onkeyup="limitChars(this, 500, 'divNotesCharInfo')" cols="40" rows="5" runat="server" />
  <div id="divNotesCharInfo"></div>
</div>

<div class="divForm">
  <label class="label">Version:</label>
  <asp:TextBox ID="txtVersion" runat="server" CssClass="textbox" MaxLength="50" />
</div>

<hr />

<h2>Statuses</h2>

<asp:Repeater ID="rptStatuses" runat="server" OnItemDataBound="rptStatuses_ItemDataBound">
  <ItemTemplate>
    <div class="divForm">
      <asp:Label ID="lblStatus" runat="server" Text='<%# "*" + Eval(PropHelper.G<Status>(_ => _.StatusName)) + ":" %>' CssClass="label" />
      <asp:TextBox ID="txtStatus" runat="server" CssClass="textbox" MaxLength="50" />
      <asp:RequiredFieldValidator ID="statusRFV" runat="server" />
      <ajaxToolkit:ValidatorCalloutExtender ID="statusVCE" runat="server" TargetControlID="statusRFV" />
    </div>
  </ItemTemplate>
</asp:Repeater>

<hr />

<h2>Work Types</h2>

<asp:Repeater ID="rptWorkTypes" runat="server" OnItemDataBound="rptWorkTypes_ItemDataBound">
  <ItemTemplate>
    <div class="divForm">
      <asp:Label ID="lblWorkType" runat="server" Text='<%# "*" + Eval(PropHelper.G<WorkType>(_ => _.WorkType1)) + ":" %>' CssClass="label" />
      <asp:TextBox ID="txtWorkType" runat="server" CssClass="textbox" MaxLength="50" />
      <asp:RequiredFieldValidator ID="workTypeRFV" runat="server" />
      <ajaxToolkit:ValidatorCalloutExtender ID="workTypeVCE" runat="server" TargetControlID="workTypeRFV" />
    </div>
  </ItemTemplate>
</asp:Repeater>