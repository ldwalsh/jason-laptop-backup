using CW.CMMSIntegration;
using CW.Data;
using CW.Logging;
using System;
using System.Linq;

namespace CW.Website._administration._shared.CMMS
{
    public sealed class SettingsViewManager
    {
        #region fields

            readonly CMMSSettingsView _view;

            readonly string saveMsgText = "settings have been updated.";

            readonly string deleteMsgText = "settings have been deleted.";

            readonly Func<ConfigManager> _getConfigManager;

            readonly ISectionLogManager _LogManager;

        #endregion

        #region properties
            
            public bool IsEnabled { get; private set; }

            public bool IsInherit { get; private set; }

            public ConfigTypeEnum SettingsType { get { return _view.ConfigType;} set {_view.ConfigType = value; } }

            public string Message { get; private set; }
            
        #endregion

        #region constructor(s)

            public SettingsViewManager(String connectionString, ISectionLogManager sectionLogger, CMMSSettingsView view): this(() => StrategyFactory.GetConfigManager(connectionString, sectionLogger), sectionLogger, view)
            {}

            public SettingsViewManager(ContextWrapper contextWrapper, ISectionLogManager sectionLogger, CMMSSettingsView view): this(() => StrategyFactory.GetConfigManager(contextWrapper), sectionLogger, view)
            {}

            private SettingsViewManager(Func<ConfigManager> getConfigManager, ISectionLogManager sectionLogger, CMMSSettingsView view)
            {
                _getConfigManager = getConfigManager;

                _LogManager = sectionLogger;

                _view = view;
            }

        #endregion

        #region methods

            public void ConfigureView()
            {
                var cm = _getConfigManager();

                _view.SetDDLWorkOrderSystem(cm.GetVendors());
                
                _view.Statuses = cm.GetWoStatuses().Select(_ => new Status {StatusID=_.Id, StatusName=_.DefaultStatus});

                _view.WorkTypes = cm.GetWoTypes().Select(_ => new WorkType { WorkTypeID = _.Id, WorkType1 = _.DefaultType, });
            }

            public void SetCMMSConfiguration(ConfigTypeEnum type, int id)
            {
                var settings = _getConfigManager().GetConfig(type, id);

                if (settings == null)
                {
                    SettingsType = ConfigTypeEnum.None; //to indicate to the building/client there are no settings

                    _view.IsEditable = IsEnabled = IsInherit = false;

                    ClearCMMSViewFields();

                    return;
                }

                //to indicate to the building/client there are settings
                SettingsType = settings.ConfigType;

                if (type == ConfigTypeEnum.Client)
                {
                    _view.IsEditable = IsEnabled = settings.IsEnabled;
                
                    SetCMMSViewFields(settings, type, id);
                
                    return;
                }

                if (type == ConfigTypeEnum.Building)
                {
                    if (settings.ConfigType == ConfigTypeEnum.Client)
                    {
                        IsEnabled = IsInherit = true;

                        SetCMMSViewFields(settings, type, id);

                        return;
                    }

                    _view.IsEditable = IsEnabled = settings.IsEnabled;

                    IsInherit = false;

                    SetCMMSViewFields(settings, type, id);
                }
            }

            public void ClearCMMSViewFields()
            {
                _view.FieldConfigId = 0;

                _view.FieldVendor = -1;

                _view.LabelCMMSCode = _view.FieldIdentifier = _view.FieldEndPoint = _view.FieldPasskey = _view.FieldNotes = _view.FieldVersion = string.Empty;

                _view.ClearLists();
            }

            void SetCMMSViewFields(CMMSConfig settings, ConfigTypeEnum type, int id)
            {
                _view.FieldConfigId = settings.ConfigId;

                _view.FieldVendor = settings.VendorId;

                _view.FieldEndPoint = settings.Endpoint?.ToString();

                _view.FieldIdentifier = settings.Identifier;

                _view.FieldPasskey = settings.Passkey;

                SetCMMSCodeIfExists(settings, type, id);

                _view.FieldNotes = settings.Notes;

                _view.FieldVersion = settings.Version;

                _view.FieldStatuses = settings.WoStatuses;

                _view.FieldWorkTypes = settings.WoTypes;
            }

            public void SetEditability(bool isEditable) { _view.IsEditable = isEditable; }

            public void SaveConfig(int buildingId, bool isEnabled, bool isInherit)
            {
                var settings = GetConfigSettings(isEnabled);
    
                _getConfigManager().SaveConfig(buildingId, (isInherit ? null : settings), isInherit);

                SetCMMSCodeIfExists(settings, ConfigTypeEnum.Building, buildingId);

                Message = $"{nameof(Building)} {saveMsgText}";

                IsEnabled = isEnabled;

                IsInherit = isInherit;

                _view.IsEditable = IsEnabled && !IsInherit;
            }
        
            public void SaveConfig(int clientId, bool isEnabled)
            {
                var settings = GetConfigSettings(isEnabled);
            
                _getConfigManager().SaveConfig(clientId, settings);

                SetCMMSCodeIfExists(settings, ConfigTypeEnum.Client, clientId);

                Message = $"{nameof(Client)} {saveMsgText}";

                IsEnabled = isEnabled;

                _view.IsEditable = IsEnabled;
            }

            public void DeleteBuildingConfig(int buildingId)
            {
                _getConfigManager().SaveConfig(buildingId, null, true);

                Message = $"{nameof(Building)} {deleteMsgText}";

                IsEnabled = IsInherit = _view.IsEditable = false;
            }

            public void DeleteClientConfig(int clientId)
            {
                _getConfigManager().SaveConfig(clientId, null);

                Message = $"{nameof(Client)} {deleteMsgText}";

                IsEnabled = IsInherit = _view.IsEditable = false;
            }

            public CMMSConfig GetConfigSettings(bool isEnabled)
            {
                return new CMMSConfig
                {
                    IsEnabled = isEnabled,
                    ConfigId = _view.FieldConfigId,
                    VendorId = _view.FieldVendor,
                    Endpoint = new Uri(_view.FieldEndPoint),
                    Identifier = _view.FieldIdentifier,
                    Passkey = _view.FieldPasskey,
                    CMMSCode = _view.LabelCMMSCode,
                    Notes = _view.FieldNotes,
                    Version = _view.FieldVersion,
                    WoStatuses = _view.FieldStatuses,
                    WoTypes = _view.FieldWorkTypes,
                };
            }

            void SetCMMSCodeIfExists(CMMSConfig settings, ConfigTypeEnum type, int id)
            {
                if (string.IsNullOrWhiteSpace(settings.CMMSCode))
                    _view.LabelCMMSCode = _getConfigManager().GetCMMSCode(type, id);
            }

        #endregion
    }
}