﻿using AjaxControlToolkit;
using CW.CMMSIntegration;
using CW.Data;
using CW.Utility;
using CW.Utility.Extension;
using CW.Website._framework;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._administration._shared.CMMS
{
    #region extensions

        public static class RepeaterExtenstion
        {
            public static void DataBind(this Repeater _, object data)
            {
                _.DataSource = data;

                _.DataBind();
            }
        }

    #endregion

    #region decorators

        public class WebControlDecorator : WebControl
        {
            readonly HtmlControl _control;

            public WebControlDecorator(HtmlControl control) { _control = control; }

            public override bool Enabled { get { return !_control.Disabled; } set { _control.Disabled = !value; } }
        }

    #endregion

    public partial class CMMSSettingsView : SiteUserControl
    {
        #region fields

            string statusTxtBoxId = $"txt{nameof(Status)}";

            string workTypeTxtBoxId = $"txt{nameof(WorkType)}";

        #endregion

        #region properties

            public int FieldConfigId
            {
                get { return string.IsNullOrWhiteSpace(hdnConfigId.Value) ? 0 : int.Parse(hdnConfigId.Value); }
                set { hdnConfigId.Value = value.ToString(); }
            }

            public int FieldVendor { get { return int.Parse(ddlWorkOrderVendor.SelectedValue); } set { ddlWorkOrderVendor.SelectedValue = value.ToString(); } }

            public string FieldEndPoint { get { return txtBaseUrl.Text.Trim(); } set { txtBaseUrl.Text = value; } }

            public string FieldIdentifier { get { return txtUserName.Text.Trim(); } set { txtUserName.Text = value; } }

            public string FieldPasskey { get { return txtPassKey.Text.Trim(); } set { txtPassKey.Text = value; } }

            public string LabelCMMSCode
            {
                get
                {
                    var cmmsCode = lblCMMSCode.Text;

                    divCMMSCode.Visible = !string.IsNullOrWhiteSpace(cmmsCode);

                    return lblCMMSCode.Text;
                }
                set
                {
                    var cmmsCode = value;

                    divCMMSCode.Visible = !string.IsNullOrWhiteSpace(cmmsCode);

                    lblCMMSCode.Text = cmmsCode;
                }
            }

            public string FieldNotes { get { return txtNotes.InnerText.Trim(); } set { txtNotes.InnerText = value; } }

            public string FieldVersion { get { return txtVersion.Text.Trim(); } set { txtVersion.Text = value; } }

            public string[] FieldStatuses { get { return GetRepeaterValues(rptStatuses, statusTxtBoxId); } set { SetRepeaterValues(rptStatuses, statusTxtBoxId, value); } }

            public string[] FieldWorkTypes { get { return GetRepeaterValues(rptWorkTypes, workTypeTxtBoxId); } set { SetRepeaterValues(rptWorkTypes, workTypeTxtBoxId, value); } }

            public bool IsEditable { get; set; }

            public IEnumerable<Status> Statuses { set { rptStatuses.DataBind(value); } }

            public IEnumerable<WorkType> WorkTypes { set { rptWorkTypes.DataBind(value); } }

            public ConfigTypeEnum ConfigType { get {return viewState.Get<ConfigTypeEnum>("configType");} set {viewState.Set("configType", value);}}

        #endregion

        #region events

            //this will maintain the value for the "password" field, otherwise value is lost on postback
            void Page_FirstLoad() => txtPassKey.Attributes["type"] = "password";
            
            protected override void Render(HtmlTextWriter writer) //capture all UI fields and toggle their enabled state based on IsEditable property
            {
                var fields = new List<WebControl> { ddlWorkOrderVendor, txtBaseUrl, txtUserName, txtPassKey, lblCMMSCode, new WebControlDecorator(txtNotes), txtVersion };

                fields.AddRange(rptStatuses.Items.Cast<RepeaterItem>().Select(_ => (WebControl)_.FindControl(statusTxtBoxId)));

                fields.AddRange(rptWorkTypes.Items.Cast<RepeaterItem>().Select(_ => (WebControl)_.FindControl(workTypeTxtBoxId)));

                fields.ForEach(_ => _.Enabled = IsEditable);

                btnTestConnection.Visible = IsEditable;

                base.Render(writer);
            }

            #region repeater events

                protected void rptStatuses_ItemDataBound(object sender, RepeaterItemEventArgs e) => SetRequiredFields(e, "lblStatus", statusTxtBoxId, "statusRFV", "statusVCE");

                protected void rptWorkTypes_ItemDataBound(object sender, RepeaterItemEventArgs e) => SetRequiredFields(e, "lblWorkType", workTypeTxtBoxId, "workTypeRFV", "workTypeVCE");

            #endregion

        #endregion

        #region methods

            public void SetDDLWorkOrderSystem(IEnumerable<WorkOrderVendor> vendors)
            {
                ddlWorkOrderVendor.Items.Add(new ListItem { Text = "Select One...", Value = "-1" });

                ddlWorkOrderVendor.DataTextField = PropHelper.G<WorkOrderVendor>(_ => _.VendorName);

                ddlWorkOrderVendor.DataValueField = PropHelper.G<WorkOrderVendor>(_ => _.WOVID);

                ddlWorkOrderVendor.DataSource = vendors;

                ddlWorkOrderVendor.DataBind();
            }
        
            string[] GetRepeaterValues(Repeater repeater, string id) => repeater.Items.Cast<RepeaterItem>().Select(_ => ((TextBox)_.FindControl(id)).Text).ToArray();

            void SetRepeaterValues(Repeater repeater, string id, string[] values)
            {
                for (var i = 0; i < values.Length; i++)
                    ((TextBox)repeater.Items[i].FindControl(id)).Text = values[i];
            }

            public void ClearLists()
            {
                rptStatuses.Items.Cast<RepeaterItem>().ForEach(_ => ((TextBox)_.FindControl(statusTxtBoxId)).Text = string.Empty);

                rptWorkTypes.Items.Cast<RepeaterItem>().ForEach(_ => ((TextBox)_.FindControl(workTypeTxtBoxId)).Text = string.Empty);
            }

            void SetRequiredFields(RepeaterItemEventArgs e, string lblId, string tbId, string rfvId, string vceId)
            {
                var lbl = (Label)e.Item.FindControl(lblId);

                var rfv = (RequiredFieldValidator)e.Item.FindControl(rfvId);

                var vce = (ValidatorCalloutExtender)e.Item.FindControl(vceId);

                var tb = (TextBox)e.Item.FindControl(tbId);

                rfv.ControlToValidate = tb.ID;

                rfv.ErrorMessage = $"{lbl.Text.Replace("*" , string.Empty).Replace(":", string.Empty)} is a required field.";

                rfv.ControlToValidate = tbId;

                rfv.SetFocusOnError = true;

                rfv.Display = ValidatorDisplay.None;

                rfv.ValidationGroup = "WorkOrder";

                vce.HighlightCssClass = "validatorCalloutHighlight";

                vce.Width = Unit.Pixel(175);
            }

        #endregion
    }
}