﻿
namespace CW.Website._administration
{
    public enum PersistModeEnum
    {
        NotSet,
        Insert,
        Update
    }

    public enum AdminModeEnum
    {
        KGS,
        PROVIDER,
        NONKGS,
        SYSTEM
    };

    public enum ControlModifierEnum
    {
        Enabled,
        Visible,
    }
}