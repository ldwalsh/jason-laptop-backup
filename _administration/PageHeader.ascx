﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PageHeader.ascx.cs" Inherits="CW.Website._administration.PageHeader" %>

<h1 runat="server" id="hdrTitle"></h1>
<div runat="server" id="divDesc" class="richText"></div>
<div class="updateProgressDiv">
    <asp:UpdateProgress ID="updateProgressTop" runat="server">
        <ProgressTemplate>
            <img src="/_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
        </ProgressTemplate>
    </asp:UpdateProgress>
</div>