﻿using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Content = CW.Data.Content;  //need to declare this here, other "Content" conflicts with System.Web.UI.WebControls.Content

namespace CW.Website._administration.content
{
    public partial class ContentEditForm : FormGenericBase<ContentEditForm, Content>
    {
        #region fields

            [ViewStateProperty]
            public String CultureName { get; private set; }

        #endregion

        #region methods

            #region overrides

                public override void PopulateForm(Content obj)
                {
                    base.PopulateForm(obj);

                    CultureName = obj.CultureName;
                }

            #endregion

            public Boolean CanContentBePersisted(Content content, out String message)
            {
                if (DataMgr.ContentDataMapper.DoesContentItemExist<Content>((c => c.ID != content.ID && c.ContentName == content.ContentName && c.CultureName == CultureName), (c => c)))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

            private void Page_FirstInit()
            {
                tab.BindContentSections(ddlContentSectionID, true);
                tab.BindContentTypes(ddlContentType);
                tab.BindContentFormats(ddlContentFormat);
            }

            private void Page_Load()
            {
                page.TabStateMonitor.OnTabStateChanged += TabStateMonitor_OnTabStateChanged;
            }

            void TabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages)
            {
                tab.BindContentSections(ddlContentSectionID, true);
                tab.BindContentTypes(ddlContentType);
                tab.BindContentFormats(ddlContentFormat);
            }

        #endregion

        #region properties

            public String ComparisonContent { set { litComparisonContent.Text = value; } }
            public String ContentName { get { return txtContentName.Text; } }

            #region overrides

                public override Delegates<Content>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get { return CanContentBePersisted; }
                }

                public override Action<Content> PersistEntityAction
                {
                    get { return DataMgr.ContentDataMapper.UpdateContent; }
                }

            #endregion

        #endregion
    }
}