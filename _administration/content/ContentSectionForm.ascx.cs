﻿using CW.Data;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace CW.Website._administration.content
{
    public partial class ContentSectionForm : FormGenericBase<ContentSectionForm, ContentSection>
    {
        #region methods

            public Boolean CanContentSectionBePersisted(ContentSection contentSection, out String message)
            {
                if (DataMgr.ContentDataMapper.DoesContentItemExist<ContentSection>((cs => cs.ContentSectionID != contentSection.ContentSectionID && 
                                                                                               cs.ContentSectionName == contentSection.ContentSectionName), (cs => cs)))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

            private void Page_FirstInit()
            {
                tab.BindQuickLinks(ddlContentQuickLinkID);
            }

            private void Page_Load()
            {
                page.TabStateMonitor.OnTabStateChanged += TabStateMonitor_OnTabStateChanged;
            }

            void TabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages)
            {
                tab.BindQuickLinks(ddlContentQuickLinkID);
            }

        #endregion

        #region properties

            #region overrides

                public override Delegates<ContentSection>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get { return CanContentSectionBePersisted; }
                }

                public override Action<ContentSection> PersistEntityAction
                {
                    get
                    {
                        switch (FormMode)
                        {
                            case PersistModeEnum.Insert:
                                {
                                    return DataMgr.ContentDataMapper.InsertContentSection;
                                }
                            case PersistModeEnum.Update:
                                {
                                    return DataMgr.ContentDataMapper.UpdateContentSection;
                                }
                        }

                        return null;
                    }
                }

            #endregion

        #endregion
    }
}