﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ContentSectionForm.ascx.cs" Inherits="CW.Website._administration.content.ContentSectionForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>

<asp:HiddenField ID="hdnContentSectionID" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">*Content Section Name:</label>
  <asp:TextBox ID="txtContentSectionName" CssClass="textbox" MaxLength="50" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Content Section Description:</label>
  <asp:TextBox ID="txtContentSectionDescription" CssClass="textbox" MaxLength="200" runat="server" />
</div>

<div class="divForm">
  <label class="label">Content Quick Link:</label>
  <asp:DropDownList ID="ddlContentQuickLinkID" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="contenSectionNameRFV" runat="server" ErrorMessage="Content Section Name is a required field." ControlToValidate="txtContentSectionName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="contenSectionNameVCE" runat="server" TargetControlID="contenSectionNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="contentSectionDescriptionRFV" runat="server" ErrorMessage="Content Section Description is a required field." ControlToValidate="txtContentSectionDescription" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="contentSectionDescriptionVCE" runat="server" TargetControlID="contentSectionDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />