﻿using System;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;

using Content = CW.Data.Content;
using System.Collections.Generic; //need to declare this here, other "Content" conflicts with System.Web.UI.WebControls.Content

namespace CW.Website._administration.content
{
    public partial class ContentAddForm : FormGenericBase<ContentAddForm, Content>
    {
        #region methods

            public override void CreateEntity(Content obj)
            {
                base.CreateEntity(obj);

                obj.TwoLetterISOLanguageName = ddlCultureName.SelectedValue.Split('-')[0];
            }

            public Boolean CanContentBePersisted(Content content, out String message)
            {
                if (DataMgr.ContentDataMapper.DoesContentItemExist<Content>((c => c.ContentName == content.ContentName && c.CultureName == content.CultureName), (c => c)))
                {
                    message = "{EntityTypeName} already exists.";

                    return false;
                }

                message = null;

                return true;
            }

            private void Page_FirstInit()
            {
                tab.BindContentSections(ddlContentSectionID, true);
                tab.BindContentTypes(ddlContentType);
                tab.BindContentFormats(ddlContentFormat);
                tab.BindCultures(ddlCultureName, true);
            }

            private void Page_Load()
            {
                page.TabStateMonitor.OnTabStateChanged += TabStateMonitor_OnTabStateChanged;
            }

            void TabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages)
            {
                tab.BindContentSections(ddlContentSectionID, true);
                tab.BindContentTypes(ddlContentType);
                tab.BindContentFormats(ddlContentFormat);
                tab.BindCultures(ddlCultureName, true);
            }

        #endregion

        #region properties

            #region overrides

                public override Delegates<Content>.CanEntityBePersistedDelegate CanEntityBePersisted
                {
                    get { return CanContentBePersisted; }
                }

                public override Action<Content> PersistEntityAction
                {
                    get { return DataMgr.ContentDataMapper.InsertContent; }
                }

            #endregion

        #endregion
    }
}