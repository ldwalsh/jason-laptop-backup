﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ContentAddForm.ascx.cs" Inherits="CW.Website._administration.content.ContentAddForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:HiddenField ID="hdnID" runat="server" Visible="false" />

<div class="divForm">
  <label class="label">*Content Name:</label>
  <asp:TextBox ID="txtContentName" CssClass="textbox" MaxLength="50" runat="server" />
  <p>(Note: Content name must match other cultures content name to be grouped together for each related item.)</p>
  <p>(Note: The content name is used to connect the content for display purposes.)</p>
</div>

<div class="divForm">
  <label class="label">*Content Section:</label>
  <asp:DropDownList ID="ddlContentSectionID" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Content Format:</label>
  <asp:DropDownList ID="ddlContentFormat" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Content Type:</label>
  <asp:DropDownList ID="ddlContentType" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<div class="divForm">
  <label class="label">Content (HTML) Tags:</label>
  <asp:TextBox ID="txtContentTags" CssClass="textbox" MaxLength="1000" runat="server" />
</div>

<div class="divForm">
  <label class="label">*Content:</label>
  <telerik:RadEditor ID="radContent1" runat="server" Height="300px" Width="450px" NewLineMode="Div" ToolsWidth="450px" ToolbarMode="ShowOnFocus" ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" EnableResize="false">
    <CssFiles>
      <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
    </CssFiles>
  </telerik:RadEditor> 
</div>

<div class="divForm">
  <label class="label">*Culture:</label>
  <asp:DropDownList ID="ddlCultureName" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<br />

<asp:LinkButton ID="updateButton" runat="server" CssClass="lnkButton" />

<!--Ajax Validators-->      
<asp:RequiredFieldValidator ID="contentNameRFV" runat="server" ErrorMessage="Content Name is a required field." ControlToValidate="txtContentName" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="contentNameVCE" runat="server" TargetControlID="contentNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="contentSectionIDRFV" runat="server" ErrorMessage="Content Section is a required field." ControlToValidate="ddlContentSectionID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="contentSectionIDVCE" runat="server" TargetControlID="contentSectionIDRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="contentFormatRFV" runat="server" ErrorMessage="Content Format is a required field." ControlToValidate="ddlContentFormat" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="contentFormatVCE" runat="server" TargetControlID="contentFormatRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="contentTypeRFV" runat="server" ErrorMessage="Content Type is a required field." ControlToValidate="ddlContentType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="contentTypeVCE" runat="server" TargetControlID="contentTypeRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="contentRFV" runat="server" ErrorMessage="Content is a required field." ControlToValidate="radContent1" SetFocusOnError="true" Display="None" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="contentVCE" runat="server" TargetControlID="contentRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="cultureNameRFV" runat="server" ErrorMessage="Culture is a required field." ControlToValidate="ddlCultureName" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="" />
<ajaxToolkit:ValidatorCalloutExtender ID="cultureNameVCE" runat="server" TargetControlID="cultureNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />