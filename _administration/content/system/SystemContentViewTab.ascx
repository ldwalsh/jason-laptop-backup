﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemContentViewTab.ascx.cs" Inherits="CW.Website._administration.content.system.SystemContentViewTab" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="ContentEditForm" src="../ContentEditForm.ascx"  %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Content" />

<asp:Label ID="lblResults" runat="server" Text="" />

<div class="divForm">
  <label class="label">Culture:</label>
  <asp:DropDownList ID="ddlCulture" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlCulture_OnSelectedIndexChanged" />
</div>

<div class="divForm">
  <label class="label">Culture Comparison:</label>
  <asp:DropDownList ID="ddlCultureComparison" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlCultureComparison_OnSelectedIndexChanged" />
</div>

<div class="divForm">
  <label class="label">Content Section:</label>
  <asp:DropDownList ID="ddlContentSection" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlContentSection_OnSelectedIndexChanged" />
</div>

<div class="divForm">
  <label class="label">Content Format:</label>
  <asp:DropDownList ID="ddlContentFormat" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlContentFormat_OnSelectedIndexChanged" />

</div>
<div class="divForm">
  <label class="label">Content Type:</label>
  <asp:DropDownList ID="ddlContentType" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlContentType_OnSelectedIndexChanged" />
</div>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentName" HeaderText="Content Name">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<CW.Data.Content>(_ => _.ContentName)) %>"><%# GetCellContent(Container, PropHelper.G<CW.Data.Content>(c=>c.ContentName), 40) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentSectionName" HeaderText="Content Section Name">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<ContentSection>(_ => _.ContentSectionName)) %>"><%# GetCellContent(Container, PropHelper.G<ContentSection>(cs=>cs.ContentSectionName), 26) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentFormat" HeaderText="Content Format">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<CW.Data.Content>(c=>c.ContentFormat))%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentType" HeaderText="Content Type">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<CW.Data.Content>(c=>c.ContentType))%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentTags" HeaderText="Content Tags">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<CW.Data.Content>(_ => _.ContentTags)) %>"><%# GetCellContent(Container, PropHelper.G<CW.Data.Content>(c=>c.ContentTags), 26) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="TwoLetterISOLanguageName" HeaderText="Culture">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<CW.Data.Content>(c=>c.TwoLetterISOLanguageName))%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this Content permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<!--SELECT CONTENT DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
  <Fields>
    <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
      <ItemTemplate>
        <div>
          <h2>Content Details</h2>

          <ul class="detailsList">
            <CW:UListItem runat="server" Label="Content Name" Value="<%# Eval(PropHelper.G<CW.Data.Content>(c=>c.ContentName)) %>" />
            <CW:UListItem runat="server" Label="Content Section Name" Value="<%# Eval(PropHelper.G<CW.Data.Content, ContentSection>(c => c.ContentSection, cs => cs.ContentSectionName)) %>" />
            <CW:UListItem runat="server" Label="Content Format" Value="<%# Eval(PropHelper.G<CW.Data.Content>(c=>c.ContentFormat)) %>" />
            <CW:UListItem runat="server" Label="Content Type" Value="<%# Eval(PropHelper.G<CW.Data.Content>(c=>c.ContentType)) %>" />
            <CW:UListItem runat="server" Label="Content Tags" Value="<%# Eval(PropHelper.G<CW.Data.Content>(c=>c.ContentTags)) %>" />
            <CW:UListItem runat="server" Label="Content" Value='<%# "<div>" + Eval(PropHelper.G<CW.Data.Content>(c=>c.Content1)) + "</div>" %>' />
            <CW:UListItem runat="server" Label="Culture" Value="<%# Eval(PropHelper.G<CW.Data.Content>(c=>c.CultureName)) %>" />
          </ul>
        </div>
      </ItemTemplate> 
    </asp:TemplateField>
  </Fields>
</asp:DetailsView>

<!--EDIT CONTENT PANEL -->
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">
  <div>
    <h2>Edit Content</h2>
  </div>
  <div>
    <CW:ContentEditForm ID="form" FormMode="Update" UpdateButtonCaption="Update Content" runat="server" />
  </div>
</asp:Panel>