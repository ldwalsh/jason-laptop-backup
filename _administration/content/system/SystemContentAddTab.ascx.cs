﻿using System;
using System.Web.UI.WebControls;
using CW.Data;
using CW.Utility;

using Content = CW.Data.Content; //need to declare this here, "Content" conflicts with System.Web.UI.WebControls.Content

namespace CW.Website._administration.content.system
{
    public partial class SystemContentAddTab : AddTabBase<Content, SystemContentAdministration>
    {
        #region methods

            private void Page_Init()
            {
                CanEntityBeAdded = form.CanContentBePersisted;
                AddEntityMethod = form.PersistEntityAction;
            }

        #endregion

        #region properties

            protected override String IdColumnName
            {
                get { return PropHelper.G<Content>(c => c.ID); }
            }

            protected override String NameField
            {
                get { return PropHelper.G<Content>(c => c.ContentName); }
            }

            protected override String EntityTypeName
            {
                get { return typeof(Content).Name; }
            }

        #endregion
    }
}