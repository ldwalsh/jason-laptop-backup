﻿using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions; 
using System.Web.UI.WebControls;

using Content = CW.Data.Content; //need to declare this here, "Content" conflicts with System.Web.UI.WebControls.Content

namespace CW.Website._administration.content.system
{
    public partial class SystemContentViewTab : ViewTabWithFormBase<Content, AdministrationPageBase>
    {
		#region methods

			protected void Page_Init(Object sender, EventArgs e)
			{
				BindCultures(ddlCulture);
				BindCultures(ddlCultureComparison);
				BindContentSections(ddlContentSection);
				BindContentTypes(ddlContentType, true);
				BindContentFormats(ddlContentFormat, true);

                CanEntityBeUpdated = form.CanEntityBePersisted;
                CanEntityBeDeleted = CanContentBeDeleted;

                UpdateEntityMethod = form.PersistEntityAction;
                DeleteEntityMethod = DeleteContent;
			}

			protected void Page_PreRender(Object sender, EventArgs e)
			{
				ddlCultureComparison_OnSelectedIndexChanged(null, null);
			}

			private Boolean CanContentBeDeleted(String fcid, out String message)
			{
				//No contraints for deletes as of yet.

				message = null;

				return true;
			}

			private void DeleteContent(String cid)
			{
				DataMgr.ContentDataMapper.DeleteContentItem<Content>((c => c), (c => c.ID == Convert.ToInt32(cid)));

				page.TabStateMonitor.ChangeState();
			}

			private void BindContentGrid()
			{
				BindGrid();

                EntityDetailView.Visible = false;
                PanelEditEntity.Visible = false;
            }

		#endregion

		#region properties

			protected override IEnumerable<String> CacheHashKeySubList
			{
				get {return ddlCulture.Items.Cast<ListItem>().Select(_ => _.Value);}
			}

			protected override IEnumerable<String> GridColumnNames
			{
				get { return PropHelper.F<Content>(_ => _.ContentName, _ => _.ContentSection.ContentSectionName, _ => _.ContentType, _ => _.ContentFormat, _ => _.ContentTags, _ => _.TwoLetterISOLanguageName); }
			}

			protected override String IdColumnName
			{
				get { return PropHelper.G<Content>(c => c.ID); }
			}

			protected override String NameField
			{
				get { return PropHelper.G<Content>(c => c.ContentName); }
			}

			protected override String EntityTypeName
			{
				get { return typeof(Content).Name; }
			}

			protected override GetEntitiesForGridDelegate GetEntitiesForGrid
			{
				get
				{
					Expression<Func<Content, Boolean>> where = null;

					//so it doesnt break on first page load without any sections existing yet.
					if(!String.IsNullOrEmpty(ddlContentSection.SelectedValue))  
					{
						//TODO: possibly simplify this code, with expression builder or some other method, maybe helper as well
						where = (c => c.CultureName == ddlCulture.SelectedValue &&
																		c.ContentSectionID == Convert.ToInt32(ddlContentSection.SelectedValue));

						if (ddlContentType.SelectedValue != "-1" && ddlContentFormat.SelectedValue != "-1")
						{
							where = (c => c.CultureName == ddlCulture.SelectedValue &&
											c.ContentSectionID == Convert.ToInt32(ddlContentSection.SelectedValue) &&
											c.ContentType == ddlContentType.SelectedValue &&
											c.ContentFormat == ddlContentFormat.SelectedValue);
						}
						else if(ddlContentType.SelectedValue != "-1"){
							where = (c => c.CultureName == ddlCulture.SelectedValue &&
											c.ContentSectionID == Convert.ToInt32(ddlContentSection.SelectedValue) &&
											c.ContentType == ddlContentType.SelectedValue);
						}
						else if(ddlContentFormat.SelectedValue != "-1"){
							where = (c => c.CultureName == ddlCulture.SelectedValue &&
											c.ContentSectionID == Convert.ToInt32(ddlContentSection.SelectedValue) &&
											c.ContentFormat == ddlContentFormat.SelectedValue);
						}
					}

					return (searchText => DataMgr.ContentDataMapper.GetAllContentSearchedItems<Content>(searchText,
																												(c => c),
																												where,
																												(c => c.ContentName),
                                                                                                                new String[] { "Content1", "ContentName" },
																												(_ => _.ContentSection)));
				}
			}

            protected override GetEntityDelegate GetEntityForEditing
            {
                get { return (cid => DataMgr.ContentDataMapper.GetContentItem<Content>((c => c.ID == Convert.ToInt32(cid)), (c => c), (_ => _.ContentSection))); }
            }

            protected override GetEntityDelegate GetEntityForDetailView
            {
                get { return (cid => DataMgr.ContentDataMapper.GetContentItem<Content>((fc => fc.ID == Convert.ToInt32(cid)), (c => c), (_ => _.ContentSection))); }
            }

			protected override String CacheSubKey
			{
				get { return ddlCulture.SelectedValue + ddlContentSection.SelectedValue + ddlContentFormat.SelectedValue + ddlContentType.SelectedValue; }
			}

		#endregion

		#region Dropdown Events

			protected void ddlCulture_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				BindContentGrid();
			}

			protected void ddlContentSection_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				BindContentGrid();
			}

			protected void ddlContentFormat_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				BindContentGrid();
			}

			protected void ddlContentType_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				BindContentGrid();
			}

			protected void ddlCultureComparison_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				var contentItem = DataMgr.ContentDataMapper.GetContentItem<Content>((c => c.CultureName == ddlCultureComparison.SelectedValue && c.ContentName == form.ContentName), 
																							(c => c));

				form.ComparisonContent = (contentItem != null) ? contentItem.Content1 : "No Content";
			}

		#endregion
    }
}