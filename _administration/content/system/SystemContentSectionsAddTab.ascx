﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemContentSectionsAddTab.ascx.cs" Inherits="CW.Website._administration.content.system.SystemContentSectionsAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="../../TabHeader.ascx"  %>
<%@ Register TagPrefix="CW" TagName="ContentSectionForm" Src="../ContentSectionForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Content Section" />
<CW:ContentSectionForm ID="form" runat="server" FormMode="Insert" UpdateButtonCaption="Add Content Section" />