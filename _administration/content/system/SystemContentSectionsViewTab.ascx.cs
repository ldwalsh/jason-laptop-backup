﻿using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;

namespace CW.Website._administration.content.system
{
    public partial class SystemContentSectionsViewTab : ViewTabWithFormBase<ContentSection,AdministrationPageBase>
    {
        #region methods

            protected void Page_Init(Object sender, EventArgs e)
            {
                CanEntityBeUpdated = form.CanEntityBePersisted;
                CanEntityBeDeleted = CanContentSectionBeDeleted;

                UpdateEntityMethod = form.PersistEntityAction;

                DeleteEntityMethod = DeleteContentSection;
            }

            private Boolean CanContentSectionBeDeleted(String csid, out String message)
            {
                var id = Convert.ToInt32(csid);

                if (DataMgr.ContentDataMapper.DoesContentItemExist<Content>((c => c.ContentSectionID == id), (c => c)))
                {
                    message = "Cannot delete a content section which is associated with content.";

                    return false;
                }

                message = null;

                return true;
            }

            private void DeleteContentSection(String csid)
            {
                DataMgr.ContentDataMapper.DeleteContentItem<ContentSection>((cs => cs), (cs => cs.ContentSectionID == Convert.ToInt32(csid)));

                page.TabStateMonitor.ChangeState();
            }

        #endregion

        #region properties

            protected override IEnumerable<String> GridColumnNames
            {
                get { return PropHelper.F<ContentSection>(_ => _.ContentSectionID, _ => _.ContentSectionName, _ => _.ContentSectionDescription, _ => _.QuickLink.QuickLinkName); }
            }

            protected override String IdColumnName
            {
                get { return PropHelper.G<ContentSection>(f => f.ContentSectionID); }
            }

            protected override String NameField
            {
                get { return PropHelper.G<ContentSection>(f => f.ContentSectionName); }
            }

            protected override String EntityTypeName
            {
                get { return "content section"; } //typeof(ContentSection).Name
            }

            protected override GetEntitiesForGridDelegate GetEntitiesForGrid
            {
                get
                {
                    return (searchText => DataMgr.ContentDataMapper.GetAllContentSearchedItems<ContentSection>(searchText,
                                                                                                                    (cs => cs),
                                                                                                                    null,
                                                                                                                    (cs => cs.ContentSectionName),
                                                                                                                    new String [] { "ContentSectionName" },
                                                                                                                    (_ => _.QuickLink)));
                }
            }

            protected override GetEntityDelegate GetEntityForEditing
            {
                get { return (csid => DataMgr.ContentDataMapper.GetContentItem<ContentSection>((cs => cs.ContentSectionID == Convert.ToInt32(csid)), (cs => cs), (_ => _.QuickLink))); }
            }

            protected override GetEntityDelegate GetEntityForDetailView
            {
                get { return (csid => DataMgr.ContentDataMapper.GetContentItem<ContentSection>((cs => cs.ContentSectionID == Convert.ToInt32(csid)), (cs => cs), (_ => _.QuickLink))); }
            }

			protected override String CacheSubKey
			{
				get {return null;}
			}

        #endregion
    }
}