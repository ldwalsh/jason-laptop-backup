﻿using CW.Data;
using CW.Utility;
using System;

namespace CW.Website._administration.content.system
{
    public partial class SystemContentSectionsAddTab : AddTabBase<ContentSection, SystemContentAdministration>
    {
        #region methods

            private void Page_Init()
            {
                CanEntityBeAdded = form.CanContentSectionBePersisted;
                AddEntityMethod = form.PersistEntityAction;
            }

        #endregion

        #region properties

            protected override String IdColumnName
            {
                get { return PropHelper.G<ContentSection>(cs => cs.ContentSectionID); }
            }

            protected override String NameField
            {
                get { return PropHelper.G<ContentSection>(cs => cs.ContentSectionName); }
            }

            protected override String EntityTypeName
            {
                get { return typeof(ContentSection).Name; }
            }

        #endregion
    }
}