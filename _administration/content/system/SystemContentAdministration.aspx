﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_masters/SystemAdmin.Master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemContentAdministration.aspx.cs" Inherits="CW.Website._administration.content.system.SystemContentAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_administration/PageHeader.ascx" tagPrefix="CW" tagName="PageHeader" %>
<%@ Register src="SystemContentSectionsViewTab.ascx" tagPrefix="CW" tagName="SystemContentSectionsViewTab" %>
<%@ Register src="SystemContentSectionsAddTab.ascx" tagPrefix="CW" tagName="SystemContentSectionsAddTab" %>
<%@ Register src="SystemContentViewTab.ascx" tagPrefix="CW" tagName="SystemContentViewTab" %>
<%@ Register src="SystemContentAddTab.ascx" tagPrefix="CW" tagName="SystemContentAddTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <CW:PageHeader runat="server" Title="Content Administration" Description="The content administration area is used to view, add, and edit content.<p>A new group of content will require initial development to hook it up to a page.</p>" />

      <div class="administrationControls">
        <telerik:RadTabStrip ID="tabContainer" runat="server" SelectedIndex="0" AutoPostBack="false" MultiPageID="radMultiPage" Width="100%"> 
          <Tabs>
            <telerik:RadTab Text="View Content Sections" />
            <telerik:RadTab Text="Add Content Section" />
            <telerik:RadTab Text="View Content" />
            <telerik:RadTab Text="Add Content" />
          </Tabs>
        </telerik:RadTabStrip>

        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:SystemContentSectionsViewTab runat="server" ID="SystemContentSectionsView" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:SystemContentSectionsAddTab runat="server" ID="SystemContentSectionsAdd" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView3" runat="server">
            <CW:SystemContentViewTab runat="server" ID="SystemContentView" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView4" runat="server">
            <CW:SystemContentAddTab runat="server" ID="SystemContentAdd" />
          </telerik:RadPageView>
        </telerik:RadMultiPage>
      </div>

</asp:Content>