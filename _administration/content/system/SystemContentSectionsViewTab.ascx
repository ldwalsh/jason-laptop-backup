﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemContentSectionsViewTab.ascx.cs" Inherits="CW.Website._administration.content.system.SystemContentSectionsViewTab" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="ContentSectionForm" src="../ContentSectionForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Content Section" />

<asp:Label ID="lblResults" runat="server" Text="" />

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="SearchButtonClick"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true">
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>                                                                                                    

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentSectionID" HeaderText="Content Section ID">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<ContentSection>(cs=>cs.ContentSectionID))%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentSectionName" HeaderText="Content Section Name">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<ContentSection>(cs=>cs.ContentSectionName), 30)%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ContentSectionDescription" HeaderText="Content Section Description">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<ContentSection>(cs=>cs.ContentSectionDescription), 30)%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="QuickLinkName" HeaderText="Quick Link">  
        <ItemTemplate>
          <%# GetCellContent(Container, PropHelper.G<QuickLink>(ql=>ql.QuickLinkName), 30)%>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this Content Section permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<!--SELECT CONTENT SECTION DETAILS VIEW -->
<asp:DetailsView ID="EntityDetailView" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
  <Fields>
    <asp:TemplateField ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" ItemStyle-BorderWidth="0" ItemStyle-BorderColor="White" ItemStyle-BorderStyle="none" ItemStyle-Width="100%" HeaderStyle-Width="1" HeaderStyle-BorderWidth="0" HeaderStyle-BorderColor="White" HeaderStyle-BorderStyle="none" >
      <ItemTemplate>
        <div>
          <h2>Content Section Details</h2>

          <ul class="detailsList">
            <CW:UListItem runat="server" Label="Content Section ID" Value="<%# Eval(PropHelper.G<ContentSection>(cs=>cs.ContentSectionID)) %>" />
            <CW:UListItem runat="server" Label="Content Section Name" Value="<%# Eval(PropHelper.G<ContentSection>(cs=>cs.ContentSectionName)) %>" />
            <CW:UListItem runat="server" Label="Content Section Description" Value="<%# Eval(PropHelper.G<ContentSection>(cs=>cs.ContentSectionDescription)) %>" />
            <CW:UListItem runat="server" Label="Content Section Quick Link" Value='<%# "<a target=\"_blank\" href=\"/" + Eval(PropHelper.G<ContentSection, QuickLink>(cs=>cs.QuickLink, ql=>ql.QuickLink1)) + "\">" + Eval(PropHelper.G<ContentSection, QuickLink>(cs=>cs.QuickLink, ql=>ql.QuickLinkName)) + "</a>" %>' />
          </ul>
        </div>
      </ItemTemplate> 
    </asp:TemplateField>
  </Fields>
</asp:DetailsView>
                                                                     
<!--EDIT CONTENT SECTION PANEL -->
<asp:Panel ID="PanelEditEntity" runat="server" Visible="false">
  <div>
    <h2>Edit Content Section</h2>
  </div>
  <div>
    <CW:ContentSectionForm ID="form" FormMode="Update" UpdateButtonCaption="Update Content Section" runat="server" />
  </div>
</asp:Panel>