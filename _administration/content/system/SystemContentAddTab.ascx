﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SystemContentAddTab.ascx.cs" Inherits="CW.Website._administration.content.system.SystemContentAddTab" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="../../TabHeader.ascx"  %>
<%@ Register TagPrefix="CW" TagName="ContentAddForm" Src="../ContentAddForm.ascx"  %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Add Content" />
<CW:ContentAddForm ID="form" runat="server" FormMode="Insert" UpdateButtonCaption="Add Content" />