﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using CW.Business;
using CW.Data;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Data.Models.QuickLinks;
using CW.Utility;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class SystemQuickLinkAdministration : SitePage
    {
        #region Properties

            private QuickLink mQuickLink;
            const string addQuickLinkSuccess = " quick link addition was successful.";
            const string addQuickLinkFailed = "Adding quick link failed. Please contact an administrator.";
            const string addQuickLinkExists = "Cannot add quick link because the quick link already exists.";
            const string updateSuccessful = "Quick link update was successful.";
            const string updateFailed = "Quick link update failed. Please contact an administrator.";
            const string updateQuickLinkExists = "Cannot update quick link because the quick links already exists.";     
            const string deleteSuccessful = "Quick Link deletion was successful.";
            const string deleteFailed = "Quick link deletion failed. Please contact an administrator.";            
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "QuickLinkName";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs system admin.
                if (!siteUser.IsKGSSystemAdmin)
                    Response.Redirect("/Home.aspx");   

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindQuickLinks();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindRoles(ddlAddRole, ddlEditRole);
                    BindModules(ddlAddModule, ddlEditModule);

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetQuickLinkIntoEditForm(CW.Data.QuickLink quickLink)
            {
                //ID
                hdnEditID.Value = Convert.ToString(quickLink.QuickLinkID);
                hdnEditRoleID.Value = Convert.ToString(quickLink.RoleID);
                hdnEditModuleID.Value = Convert.ToString(quickLink.ModuleID);
                //Quick Link Name
                txtEditQuickLinkName.Text = quickLink.QuickLinkName;
                //Quick Link
                txtEditQuickLink.Text = quickLink.QuickLink1;
                //Description
                txtEditDescription.Value = quickLink.QuickLinkDescription;
                
                //Role
                ddlEditRole.SelectedValue = Convert.ToString(quickLink.RoleID);
                //Module
                if (quickLink.ModuleID != null)
                {
                    ddlEditModule.SelectedValue = Convert.ToString(quickLink.ModuleID);
                }
                
                chkEditActive.Checked = quickLink.IsActive;
            }

            #endregion

        #region Load and Bind Fields
            
            private void BindRoles(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<UserRole> roles = DataMgr.UserRoleDataMapper.GetAllRoles();

                ddl.DataTextField = "RoleName";
                ddl.DataValueField = "RoleID";
                ddl.DataSource = roles;
                ddl.DataBind();

                ddl2.DataTextField = "RoleName";
                ddl2.DataValueField = "RoleID";
                ddl2.DataSource = roles;
                ddl2.DataBind();
            }

            private void BindModules(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<Data.Module> modules = DataMgr.ModuleDataMapper.GetAllModules();

                ddl.DataTextField = "ModuleName";
                ddl.DataValueField = "ModuleID";
                ddl.DataSource = modules;
                ddl.DataBind();

                ddl2.DataTextField = "ModuleName";
                ddl2.DataValueField = "ModuleID";
                ddl2.DataSource = modules;
                ddl2.DataBind();
            }

        #endregion

        #region Load Quick Link

            protected void LoadAddFormIntoQuickLink(QuickLink quickLink)
            {
                //Quick Link Name
                quickLink.QuickLinkName = txtAddQuickLinkName.Text;
                //Quick Link
                quickLink.QuickLink1 = txtAddQuickLink.Text;
                //Description
                quickLink.QuickLinkDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                //Role
                quickLink.RoleID = Convert.ToInt32(ddlAddRole.SelectedValue);
                //Module
                quickLink.ModuleID = ddlAddModule.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddModule.SelectedValue) : null;

                quickLink.DateModified = DateTime.UtcNow;
                quickLink.IsActive = true;
            }

            protected void LoadEditFormIntoQuickLink(QuickLink quickLink)
            {
                //ID
                quickLink.QuickLinkID = Convert.ToInt32(hdnEditID.Value);
                //Quick Link Name
                quickLink.QuickLinkName = txtEditQuickLinkName.Text;
                //Quick Link
                quickLink.QuickLink1 = txtEditQuickLink.Text;
                //Description
                quickLink.QuickLinkDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //Role
                quickLink.RoleID = Convert.ToInt32(ddlEditRole.SelectedValue);
                //Module
                quickLink.ModuleID = ddlEditModule.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditModule.SelectedValue) : null;
                
                quickLink.IsActive = chkEditActive.Checked;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add quick link Button on click.
            /// </summary>
            protected void addQuickLinkButton_Click(object sender, EventArgs e)
            {
                mQuickLink = new QuickLink();

                //load the form into the quick link
                LoadAddFormIntoQuickLink(mQuickLink);

                try
                {
                    if (!DataMgr.QuickLinksDataMapper.DoesQuickLinkExist(mQuickLink.QuickLinkName, mQuickLink.QuickLink1))
                    {
                        //insert new quick link
                        DataMgr.QuickLinksDataMapper.InsertQuickLink(mQuickLink);

                        LabelHelper.SetLabelMessage(lblAddError, mQuickLink.QuickLinkName + addQuickLinkSuccess, lnkSetFocusAdd);
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblAddError, addQuickLinkExists, lnkSetFocusAdd);
                    }
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding quick link.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addQuickLinkFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding quick link.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addQuickLinkFailed, lnkSetFocusAdd);
                }
                
            }

            /// <summary>
            /// Update quick link Button on click. Updates quick link data.
            /// </summary>
            protected void updateQuickLinkButton_Click(object sender, EventArgs e)
            {
                QuickLink mQuickLink = new QuickLink();

                //load the form into the quick link
                LoadEditFormIntoQuickLink(mQuickLink);

                //try to update the quick link              
                try
                {
                    if (!DataMgr.QuickLinksDataMapper.DoesQuickLinkExist(mQuickLink.QuickLinkID, mQuickLink.QuickLinkName, mQuickLink.QuickLink1))
                    {
                        //if role change and is a higher role (lower number) then delete associations for users with lower role
                        if (Convert.ToInt32(hdnEditRoleID.Value) != mQuickLink.RoleID && Convert.ToInt32(hdnEditRoleID.Value) > mQuickLink.RoleID)
                        {
                            //TODO: by users with lower role
                            //QuickLinksDataMapper.DeleteAllUserClientQuickLinkByQuickLinkIDAndRoleID(mQuickLink.QuickLinkID, mQuickLink.RoleID);

                            //TODO: instead of this query. to be removed.
                            DataMgr.QuickLinksDataMapper.DeleteAllUserClientQuickLinkByQuickLinkID(mQuickLink.QuickLinkID);
                        }

                        int moduleID;

                        if (Int32.TryParse(hdnEditModuleID.Value, out moduleID) && Convert.ToInt32(hdnEditModuleID.Value) != mQuickLink.ModuleID)
                        {
                            DataMgr.QuickLinksDataMapper.DeleteAllUserClientQuickLinkByQuickLinkIDAndModuleID(mQuickLink.QuickLinkID, moduleID);
                        }

                        DataMgr.QuickLinksDataMapper.UpdateQuickLink(mQuickLink);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                        //Bind quick links again
                        BindQuickLinks();
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateQuickLinkExists, lnkSetFocusView);
                    }
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating quick link.", ex);
                }
             
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindQuickLinks();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindQuickLinks();
            }

        #endregion

        #region Dropdown events

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind quick link grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind quick links to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindQuickLinks();
                }
            }

        #endregion

        #region Grid Events

            protected void gridQuickLinks_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridQuickLinks_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditQuickLink.Visible = false;
                dtvQuickLink.Visible = true;
                
                int quickLinkID = Convert.ToInt32(gridQuickLinks.DataKeys[gridQuickLinks.SelectedIndex].Values["QuickLinkID"]);

                //set data source
                dtvQuickLink.DataSource = DataMgr.QuickLinksDataMapper.GetFullQuickLinkByID(quickLinkID);
                //bind quick link to details view
                dtvQuickLink.DataBind();
            }

            protected void gridQuickLinks_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridQuickLinks_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvQuickLink.Visible = false;
                pnlEditQuickLink.Visible = true;

                int quickLinkID = Convert.ToInt32(gridQuickLinks.DataKeys[e.NewEditIndex].Values["QuickLinkID"]);

                QuickLink mQuickLink = DataMgr.QuickLinksDataMapper.GetQuickLink(quickLinkID);

                //Set quick link data
                SetQuickLinkIntoEditForm(mQuickLink);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridQuickLinks_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows quickLinkID
                int quickLinkID = Convert.ToInt32(gridQuickLinks.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a quick if it as not been assocaited with
                    
                    //delete quick link assosiations
                    DataMgr.QuickLinksDataMapper.DeleteAllUserClientQuickLinkByQuickLinkID(quickLinkID);

                    //delete quick link
                    DataMgr.QuickLinksDataMapper.DeleteQuickLink(quickLinkID);

                    lblErrors.Text = deleteSuccessful;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                    
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting quick link.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryQuickLinks());
                gridQuickLinks.PageIndex = gridQuickLinks.PageIndex;
                gridQuickLinks.DataSource = SortDataTable(dataTable as DataTable, true);
                gridQuickLinks.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditQuickLink.Visible = false;
            }

            protected void gridQuickLinks_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedQuickLinks(sessionState["Search"].Split(' ')));

                //maintain current sort direction and expresstion on paging
                gridQuickLinks.DataSource = SortDataTable(dataTable, true);
                gridQuickLinks.PageIndex = e.NewPageIndex;
                gridQuickLinks.DataBind();
            }

            protected void gridQuickLinks_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridQuickLinks.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedQuickLinks(sessionState["Search"].Split(' ')));

                GridViewSortExpression = e.SortExpression;

                gridQuickLinks.DataSource = SortDataTable(dataTable, false);
                gridQuickLinks.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetQuickLinkData> QueryQuickLinks()
            {
                try
                {
                    //get all quick links 
                    return DataMgr.QuickLinksDataMapper.GetAllQuickLinksWithPartialData();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving quick links.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving quick links.", ex);
                    return null;
                }
            }

            private IEnumerable<GetQuickLinkData> QuerySearchedQuickLinks(string[] searchText)
            {
                try
                {
                    //get all quick links 
                    return DataMgr.QuickLinksDataMapper.GetAllSearchedQuickLinksWithPartialData(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving quick links.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving quick links.", ex);
                    return null;
                }
            }

            private void BindQuickLinks()
            {
                //query quick links
                IEnumerable<GetQuickLinkData> variables = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryQuickLinks() : QuerySearchedQuickLinks(txtSearch.Text.Split(' '));

                int count = variables.Count();

                gridQuickLinks.DataSource = variables;

                // bind grid
                gridQuickLinks.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedQuickLinks(string[] searchText)
            //{
            //    //query quick links
            //    IEnumerable<GetQuickLinkData> quicklinks = QuerySearchedQuickLinks(searchText);

            //    int count = quicklinks.Count();

            //    gridQuickLinks.DataSource = quicklinks;

            //    // bind grid
            //    gridQuickLinks.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} quick link found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} quick links found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No quick links found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
       
        #endregion
    }
}

