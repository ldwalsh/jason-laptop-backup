﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace CW.Website {
    
    
    public partial class ProviderBuildingAdministration {
        
        /// <summary>
        /// updateProgressTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdateProgress updateProgressTop;
        
        /// <summary>
        /// RadPageView1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView1;
        
        /// <summary>
        /// ViewBuildings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Buildings.ViewBuildings ViewBuildings;
        
        /// <summary>
        /// RadPageView2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView2;
        
        /// <summary>
        /// AddBuilding control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Buildings.AddBuilding AddBuilding;
        
        /// <summary>
        /// RadPageView4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView4;
        
        /// <summary>
        /// BuildingVariables control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Buildings.BuildingVariables BuildingVariables;
        
        /// <summary>
        /// RadPageView5 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView5;
        
        /// <summary>
        /// BuildingVariablesToMultipleBuildings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Buildings.BuildingVariablesToMultipleBuildings BuildingVariablesToMultipleBuildings;
        
        /// <summary>
        /// RadPageView6 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView6;
        
        /// <summary>
        /// BuildingCMMSSettings control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Buildings.BuildingCMMSSettings BuildingCMMSSettings;
        
        /// <summary>
        /// RadPageView7 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView7;
        
        /// <summary>
        /// BuildingGoalsSettingsTab control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._administration.building.BuildingGoalsSettingsTab BuildingGoalsSettingsTab;
        
        /// <summary>
        /// RadPageView8 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView8;
        
        /// <summary>
        /// BuildingStats control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Buildings.BuildingStats BuildingStats;
    }
}
