﻿using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO;

using CW.Data;
using CW.Business;
using CW.Utility;
using CW.Website._framework;
using CW.Data.Models.Diagnostics;
using CW.Common.Constants;
using CW.Website._customreports;
using CW.Data.Helpers;
using CW.Website.DependencyResolution;

using EO.Pdf;

using CS = CW.Common.Constants.BusinessConstants.Content.ContentSections;
using Content = CW.Data.Content;
using System.Threading;
using System.Globalization;
//need to declare this here, "Content" conflicts with System.Web.UI.WebControls.Content
using CW.Logging;
using CW.Business.Blob.Images;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class CustomerValueReport : Page
    {
        #region Fields

        const string buildingCultureError = "Selected buildings must be of the same culture.";
        const string noBuildingsError = "No buildings were selected.";

        const string cvrInitializationFailed = "Customer value report initialization failed. Please contact an administrator.";
        const string cvrGenerationFailed = "Customer value report generation failed. Please contact an administrator.";
        const string cvrGenerationFailedFormat = "Customer value report generation failed. Potential culture based formatting issue with manual csv inputs. Please check csv date inputs against selected culture.";
        const string cvrGenerationFailedColumns = "Customer value report generation failed. Potential number of columns formatting issue with manual csv inputs. Please check csv inputs against provided headers and columns.";

        const string downloadFailed = "Customer value report download failed. Please contact an administrator.";
        const string noDiagnosticsAccess = "Customer does not have diagnostics enabled. Please manually set key indicators and performance charts, exclude the pages, or upgrade the customers account.";

        const string invisibleElementIds = "hdn_container;" +
                                            "btnFieldQuotes;btnWorkOrders;btnSurveys;btnFeedback;" +
                                            "editorRemarks;" +
                                            "divDownload;";
        private DataManager mDataManager;
        public SiteUser siteUser;

        //whole form
        private enum ReportTheme { KGS, SE };
        private int cid, uid, oid, buildingLCID;
        private string languageCultureName, userCulture, cultureFormat;
        private bool showClientLogos = false, isSETheme = false;
        private bool includeContentsPage = false, manualCustomerDetails = false, includeKeyIndicators = false, manualKeyIndicators = false, includeItemsRequiringAttention = false, includePerformanceCharts = false, manualPerformanceChartsEntry = false, includeServiceDetails = false, includeMetrics = false, includeSiteVisits = false;
        private bool hasDiagnosticsModule = false;
        private string tempFd, tempTd, tempPfd, tempPtd, contractSd, contractEd;
        private DateTime fd, td, pfd, ptd, csd, ced;
        private string customerAddress, customerCity, customerCountryAlpha2Code, customerZip, customerPhone, contactName, contactEmail, contactPhone, servicePlan, primaryServiceTechName, primaryServiceTechEmail, primaryServiceTechPhone, secondaryServiceTechName, secondaryServiceTechEmail, secondaryServiceTechPhone, salesName, salesEmail, salesPhone, remoteCenterEngineerName, remoteCenterEngineerEmail, remoteCenterEngineerPhone, adminName, adminEmail, adminPhone, companyPhone, ratingSystem, ratingValue, criticalityAlarms, energyAnomaliesPriority, maintenanceAnomaliesPriority, comfortAnomaliesPriority, avoidableCostChange, alarmsNotified, alarmsFixed, alarmsAction, issueFixed, responseTime, timeOnsite, meanTime, quotes, energyChartData, energyWorkOrderData, maintenanceChartData, maintenanceWorkOrderData, comfortChartData, comfortWorkOrderData, serviceDetails, surveys, feedback, breakdown, remarks;
        private string avoidableCost;
        //customerStateName,
        private int customerStateID;
        private List<int> bids;
        private Building mBuilding;
        private Client mClient;
        //private Organization mUserOrg;
        private double reportNumberOfDays;
        private double previousReportNumberOfDays;

        private Guid guid;
        private IEnumerable<Content> mContent;

        //portfolio wide
        private double totalCost, previousTotalCost, intervalCost, intervalPreviousCost;
        private int energyFaultCountAccrossWholePeriod, previousEnergyFaultCountAccrossWholePeriod, maintenanceFaultCountAccrossWholePeriod, previousMaintenanceFaultCountAccrossWholePeriod, comfortFaultCountAccrossWholePeriod, previousComfortFaultCountAccrossWholePeriod;
        private double intervalCostPercent;        
        private double intervalEnergyIncidents, intervalMaintenanceIncidents, intervalComfortIncidents;
        private double intervalPreviousEnergyIncidents, intervalPreviousMaintenanceIncidents, intervalPreviousComfortIncidents;

        #endregion

        #region Properties

            private ISectionLogManager Logger { get; set; }
            private ContentHelper CH { get; set; }

        #endregion

        #region Page Events

        protected void Page_Init(object sender, EventArgs e)
        {
            Logger = IoC.Resolve<ISectionLogManager>();
            siteUser = SiteUser.Current;
            mDataManager = DataManager.Get(System.Web.HttpContext.Current.Session.SessionID); // using data manager from site user so we inherit storage account context.
            CH = new ContentHelper(Logger);

            //if the page is not a postback
            if (!Page.IsPostBack)
            {
                try
                {
                    //*FIRST* GET FORM GET OR POST DATA
                    if (Request.QueryString != null && Request.QueryString.HasKeys())
                    {
                        GetQueryStringData();                                
                    }
                    else
                    {                        
                        GetFormPostData();
                    }

                    //*SECOND* Check non anonymous. kgs super admin or higher or provider with super admin access to the client
                    if (siteUser.IsAnonymous || !(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient)))
                    {
                        Response.Redirect("/Home.aspx?referrer=" + Request.Url.PathAndQuery);
                    }
                    

                    //*THIRD* Initialize Culture
                    DelayedInitializeCulture();                   

                    //*FOURTH* Set content
                    GetAndSetInitialContent();

                    //set initial base tag
                    //this.Header.Controls.AddAt(0, new BaseTag(Request)); 

                    //dynamically add themed css and favicons based on se theme check
                    HtmlHead head = (HtmlHead)Page.Header;
                    HtmlLink link = new HtmlLink();
                    link.Attributes.Add("href", isSETheme ? Page.ResolveClientUrl("/_assets/styles/themes/ba.css") : Page.ResolveClientUrl("/_assets/styles/themes/cw.css"));
                    link.Attributes.Add("type", "text/css");
                    link.Attributes.Add("rel", "stylesheet");
                    head.Controls.Add(link);

                    link = new HtmlLink();
                    link.Attributes.Add("href", isSETheme ? Page.ResolveClientUrl("/_assets/styles/themes/images/se-favicon.ico") : Page.ResolveClientUrl("/_assets/styles/themes/images/cw-favicon.ico"));
                    link.Attributes.Add("type", "image/x-icon");
                    link.Attributes.Add("rel", "shortcut icon");
                    head.Controls.Add(link); 
                }
                catch (Exception ex)
                {
                    lblError.Text = cvrInitializationFailed;
                    pnlError.Visible = true;
                    pnlReport.Visible = false;

                    Logger.Log(DataConstants.LogLevel.ERROR, "Customer value report initialization failed.", ex);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {      
            isSETheme = String.IsNullOrEmpty(hdnIsSETheme.Value) ? isSETheme : Convert.ToBoolean(hdnIsSETheme.Value);
            languageCultureName = String.IsNullOrEmpty(hdnLanguageCultureName.Value) ? languageCultureName : hdnLanguageCultureName.Value;

            GetAndSetPostbackContent();

            //if the page is not a postback
            if (!Page.IsPostBack)
            {
                try
                {
                    if (bids.Any())
                    {
                        //check if buildings are all of same culture
                        if (mDataManager.BuildingDataMapper.AreBuildingSettingsCulturesTheSame(bids))
                        {
                            //SET COMMON REPORT CONTENT AND FORMAT-----
                            SetCommonReportData();

                            //PRE DIAGNOSTICS CHECKS---
                            if (!hasDiagnosticsModule && ((!manualKeyIndicators && includeKeyIndicators) || (!manualPerformanceChartsEntry && includePerformanceCharts)))
                            {
                                lblError.Text = noDiagnosticsAccess;
                                pnlError.Visible = true;
                                pnlReport.Visible = false;

                                return;
                            }

                            //GET DIAGNOSTIC DATA-----
                            IEnumerable<DiagnosticsResult> diagnosticsResults = Enumerable.Empty<DiagnosticsResult>();
                            IEnumerable<DiagnosticsResult> previousDiagnosticsResults = Enumerable.Empty<DiagnosticsResult>();

                            if (hasDiagnosticsModule && (!manualKeyIndicators || !manualPerformanceChartsEntry))
                            {
                                diagnosticsResults = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(
                                            new DiagnosticsResultsInputs()
                                            {
                                                AnalysisRange = DataConstants.AnalysisRange.Daily,
                                                StartDate = fd,
                                                EndDate = td,
                                                CID = cid,
                                            },
                                            bids);

                                previousDiagnosticsResults = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(
                                            new DiagnosticsResultsInputs()
                                            {
                                                AnalysisRange = DataConstants.AnalysisRange.Daily,
                                                StartDate = pfd,
                                                EndDate = ptd,
                                                CID = cid
                                            }, bids);
                            }

                            SetCoverPage();
                            SetContentsPage();
                            SetCustomerDetailsPage();
                            SetKeyIndicatorsPage(diagnosticsResults, previousDiagnosticsResults);
                            SetItemsRequiringAttentionPage();
                            SetPerformancePage(diagnosticsResults);
                            SetServiceDetailsPage();
                            SetCustomerSatisfactionPage();
                            SetSitePage();

                            pnlError.Visible = false;
                            pnlReport.Visible = true;
                        }
                        else
                        {
                            lblError.Text = buildingCultureError;
                            pnlError.Visible = true;
                            pnlReport.Visible = false;
                        }
                    }
                    else
                    {
                        lblError.Text = noBuildingsError;
                        pnlError.Visible = true;
                        pnlReport.Visible = false;
                    }
                }
                catch (ArgumentException aex)
                {
                    lblError.Text = cvrGenerationFailedColumns;
                    pnlError.Visible = true;
                    pnlReport.Visible = false;

                    Logger.Log(DataConstants.LogLevel.ERROR, "Customer value report generation failed. Csv number of columns error.", aex);
                }
                catch (FormatException fex)
                {
                    lblError.Text = cvrGenerationFailedFormat;
                    pnlError.Visible = true;
                    pnlReport.Visible = false;

                    Logger.Log(DataConstants.LogLevel.ERROR, "Customer value report generation failed. Format exception.", fex);
                }
                catch (Exception ex)
                {
                    lblError.Text = cvrGenerationFailed;
                    pnlError.Visible = true;
                    pnlReport.Visible = false;

                    Logger.Log(DataConstants.LogLevel.ERROR, "Customer value report generation failed.", ex);
                }
            }
        }

        #endregion

        #region Set Page Methods

        private void SetCommonReportData()
        {
            //set user org info
            //mUserOrg = mDataManager.OrganizationDataMapper.GetOrganizationByID(oid);

            //set client
            mClient = mDataManager.ClientDataMapper.GetClient(cid, (_ => _.State));
            hdnCID.Value = cid.ToString();

            //set theme
            hdnIsSETheme.Value = isSETheme.ToString();

            //set language name
            hdnLanguageCultureName.Value = languageCultureName;

            //header and new page client
            divCoverCustomerName.InnerText = newPageCustomer1.InnerText = newPageCustomer2.InnerText = newPageCustomer3.InnerText = newPageCustomer4.InnerText = newPageCustomer5.InnerText = newPageCustomer6.InnerText = newPageCustomer7.InnerText = newPageCustomer8.InnerText = mClient.ClientName;

            //set report period days
            reportNumberOfDays = DateTimeHelper.GetDiferenceBetweenDateTimesInDays(fd, td) + 1;
            previousReportNumberOfDays = DateTimeHelper.GetDiferenceBetweenDateTimesInDays(pfd, ptd) + 1;

            //set theme 
            form1.Attributes["class"] = "cvr" + (isSETheme ? ReportTheme.SE : ReportTheme.KGS);

            //logos
            imgBottomPage.Src = isSETheme ? "_assets/styles/themes/images/se-header.png" : "_assets/styles/themes/images/kgs-header-no-shadow.png";
            img1.Src = img2.Src = img3.Src = img4.Src = img5.Src = img6.Src = img7.Src = img8.Src = isSETheme ? "_assets/styles/themes/images/se-header-secondary.png" : "_assets/styles/themes/images/kgs-header-no-shadow.png";
            
            //dates
            divCoverDates.InnerText = newPageDates1.InnerText = newPageDates2.InnerText = newPageDates3.InnerText = newPageDates4.InnerText = newPageDates5.InnerText = newPageDates6.InnerText = newPageDates7.InnerText = newPageDates8.InnerText = CreatePeriodContent(fd, td);
            
            //set module
            hasDiagnosticsModule = mDataManager.ModuleDataMapper.DoesModuleExistForClient(cid, (Int32)BusinessConstants.Module.Modules.Diagnostics);

            //set building lcid
            if(bids.Count() > 1){
                //buildings
                IEnumerable<Building> buildings = mDataManager.BuildingDataMapper.GetBuildingsByBIDList(bids, true);

                buildingLCID = buildings.First().BuildingSettings.First().LCID;
            }
            else{
                //get building
                mBuilding = mDataManager.BuildingDataMapper.GetBuildingByBID(bids.First(), true, true, true, true, true, true);

                buildingLCID = mBuilding.BuildingSettings.First().LCID;
            }
            
        }

        private void SetCoverPage()
        {
            hdnShowClientLogos.Value = showClientLogos.ToString();

            if (showClientLogos && !String.IsNullOrEmpty(mClient.ImageExtension))
            {
                var image = new ClientProfileImage(mClient, mDataManager.OrgBasedBlobStorageProvider);

                if (image.Exists())
                {
                    var byteArray = image.Retrieve();

                    radCoverClient.ImageUrl = HandlerHelper.ClientImageUrl(cid, mClient.ImageExtension);
                    hdnCoverClientByteArray.Value = Convert.ToBase64String(byteArray);
                }
                else
                {
                    radCoverClient.Visible = false;
                }
            }
            else
            {
                radCoverClient.Visible = false;
            }
        }

        private void SetContentsPage()
        {
            divContentsPage.Visible = includeContentsPage;
        }

        private void SetCustomerDetailsPage()
        {
            //Customer details--
            divCustomerName.InnerText = mClient.ClientName;

            if (manualCustomerDetails)
            {
                divCustomerLocation.InnerHtml = String.Format("{0},<br/> {1},<br/> {2}, {3}", customerAddress, customerCity, mDataManager.StateDataMapper.GetStateByID(customerStateID).StateName, customerZip);
                divCustomerPhone.InnerText = mClient.ClientPhone;

                divContactName.InnerText = mClient.ClientPrimaryContactName;
                divContactEmail.InnerText = mClient.ClientPrimaryContactEmail;
                divContactPhone.InnerText = mClient.ClientPrimaryContactPhone;   
            }
            else
            {
                divCustomerLocation.InnerHtml = String.Format("{0},<br/> {1},<br/> {2}, {3}", mClient.ClientAddress, mClient.ClientCity, mClient.State.StateName, mClient.ClientZip);
                divCustomerPhone.InnerText = customerPhone;

                divContactName.InnerText = contactName;                
                divContactEmail.InnerText = contactEmail;       
                divContactPhone.InnerText = contactPhone;
            }


            //Contract details--
            divServicePlan.InnerText = servicePlan;
            divContractStartDate.InnerText = csd.ToString("MMM. d, yyyy");
            divContractEndDate.InnerText = ced.ToString("MMM. d, yyyy");
            
            //buildings
            IEnumerable<Building> buildings = mDataManager.BuildingDataMapper.GetBuildingsByBIDList(bids, true);

            divBuildingCount.InnerText = buildings.Count().ToString();
            
            foreach(Building b in buildings)
            {
                divBuildingList.InnerHtml += b.BuildingName + (String.IsNullOrEmpty(b.Location) ? "" : " - " + b.Location) + "</br>";
            }

            //Suport team details--
            divPrimaryServiceTechnicianName.InnerText = primaryServiceTechName;
            divPrimaryServiceTechnicianEmail.InnerText = primaryServiceTechEmail;
            divPrimaryServiceTechnicianPhone.InnerText = primaryServiceTechPhone;
            
            divSecondaryServiceTechnicianName.InnerText = secondaryServiceTechName;
            divSecondaryServiceTechnicianEmail.InnerText = secondaryServiceTechEmail;
            divSecondaryServiceTechnicianPhone.InnerText = secondaryServiceTechPhone;

            divSalesName.InnerText = salesName;
            divSalesEmail.InnerText = salesEmail;
            divSalesPhone.InnerText = salesPhone;

            divRemoteCenterEngineerName.InnerText = remoteCenterEngineerName;
            divRemoteCenterEngineerEmail.InnerText = remoteCenterEngineerEmail;
            divRemoteCenterEngineerPhone.InnerText = remoteCenterEngineerPhone;

            divAdminName.InnerText = adminName;
            divAdminEmail.InnerText = adminEmail;
            divAdminPhone.InnerText = adminPhone;
        }

        private void SetKeyIndicatorsPage(IEnumerable<DiagnosticsResult> diagnosticsResults, IEnumerable<DiagnosticsResult> previousDiagnosticsResults)
        {
            divContentsKeyIndicatorsPage.Visible = divKeyIndicatorsPage.Visible = includeKeyIndicators;

            if (includeKeyIndicators)
            {
                PerformKeyIndicatorsCalculations(diagnosticsResults, previousDiagnosticsResults);

                divEnergyContainer.Visible = hasDiagnosticsModule;
                keyIndicatorsNote.Visible = !hasDiagnosticsModule;

                if (!String.IsNullOrEmpty(ratingSystem) && !String.IsNullOrEmpty(ratingValue))
                {
                    lblRatingSystem.InnerText = ratingSystem + lblRatingSystem.InnerText;
                    divRatingValue.InnerText = ratingValue;
                }
                else
                {
                    divRating.Visible = false;
                }

                SetContentAndVisibility(liNumAlarmsNotified, spanNumAlarmsNotified, alarmsNotified);
                SetContentAndVisibility(liNumAlarmsFixed, spanNumAlarmsFixed, alarmsFixed);
                SetContentAndVisibility(liNumAlarmsAction, spanNumAlarmsAction, alarmsAction);
                SetContentAndVisibility(liNumIssuesFixed, spanNumIssuesFixed, issueFixed);
                SetContentAndVisibility(liResponseTime, spanResponseTime, responseTime);
                SetContentAndVisibility(liOnsiteRepairTime, spanOnsiteRepairTime, timeOnsite);
                SetContentAndVisibility(liMeanRepairTime, spanMeanRepairTime, meanTime);
            }
        }

        private void SetItemsRequiringAttentionPage()
        {
            divContentsItemsRequiringAttentionPage.Visible = divItemsRequiringAttentionPage.Visible = includeItemsRequiringAttention;

            if (includeItemsRequiringAttention)
            {               
                using(TextReader sr = new StringReader(quotes))
                {
                    gridFieldQuotes.DataSource = Csv.ConvertCSVtoDataTable(new CsvReader(sr));
                    gridFieldQuotes.DataBind();
                }              
            }
        }

        private void SetPerformancePage(IEnumerable<DiagnosticsResult> diagnosticsResults)
        {
            divContentsPerformancePage.Visible = divPerformancePage.Visible = includePerformanceCharts;

            if (includePerformanceCharts)
            {
                SetPerformanceChartsAndWorkOrders(diagnosticsResults);
            }
        }

        private void SetServiceDetailsPage()
        {
            divContentsServiceDetailsPage.Visible = divServiceDetailsPage.Visible = includeServiceDetails;

            if (includeServiceDetails)
            {
                using (TextReader sr = new StringReader(serviceDetails))
                {
                    gridWorkOrders.DataSource = Csv.ConvertCSVtoDataTable(new CsvReader(sr));
                    gridWorkOrders.DataBind();
                }    
            }
        }

        private void SetCustomerSatisfactionPage()
        {
            divContentsCustomerSatisfactionPage.Visible = divCustomerSatisfactionPage.Visible = includeMetrics;

            if (includeMetrics)
            {
                using (TextReader sr = new StringReader(surveys))
                {
                    gridSurveys.DataSource = Csv.ConvertCSVtoDataTable(new CsvReader(sr));
                    gridSurveys.DataBind();
                }

                using (TextReader sr = new StringReader(feedback))
                {
                    gridFeedback.DataSource = Csv.ConvertCSVtoDataTable(new CsvReader(sr));
                    gridFeedback.DataBind();
                } 
            }
        }

        private void SetSitePage()
        {
            divContentsSitePage.Visible = divSitePage.Visible = includeSiteVisits;

            if (includeSiteVisits)
            {
                SetSiteBreakdownPieChart();

                //REMARKS                            
                //use url decode because of form post.
                litRemarks.Text = Server.UrlDecode(remarks);
            }
        }

        #endregion

        #region Key Indicators Methods

        protected void PerformKeyIndicatorsCalculations(IEnumerable<DiagnosticsResult> diagnosticsResults, IEnumerable<DiagnosticsResult> previousDiagnosticsResults)
        {
            if (hasDiagnosticsModule && !manualKeyIndicators)
            {
                totalCost = Math.Ceiling(diagnosticsResults.Sum(d => d.CostSavings));
                previousTotalCost = Math.Ceiling(previousDiagnosticsResults.Sum(d => d.CostSavings));
                energyFaultCountAccrossWholePeriod = diagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                previousEnergyFaultCountAccrossWholePeriod = previousDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                maintenanceFaultCountAccrossWholePeriod = diagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                previousMaintenanceFaultCountAccrossWholePeriod = previousDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                comfortFaultCountAccrossWholePeriod = diagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
                previousComfortFaultCountAccrossWholePeriod = previousDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();

                //calculate percentages, incidents, and trend summaries-----
                intervalEnergyIncidents = Math.Round(energyFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousEnergyIncidents = Math.Round(previousEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                imgEnergyArrow.Src = (intervalPreviousEnergyIncidents > intervalEnergyIncidents ? "_assets/styles/themes/customReportImages/arrow-down" : (intervalPreviousEnergyIncidents == intervalEnergyIncidents ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");

                intervalMaintenanceIncidents = Math.Round(maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousMaintenanceIncidents = Math.Round(previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                imgMaintenanceArrow.Src = (intervalPreviousMaintenanceIncidents > intervalMaintenanceIncidents ? "_assets/styles/themes/customReportImages/arrow-down" : (intervalPreviousMaintenanceIncidents == intervalMaintenanceIncidents ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");

                intervalComfortIncidents = Math.Round(comfortFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousComfortIncidents = Math.Round(previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                imgComfortArrow.Src = (intervalPreviousComfortIncidents > intervalComfortIncidents ? "_assets/styles/themes/customReportImages/arrow-down" : (intervalPreviousComfortIncidents == intervalComfortIncidents ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");


                //round up to nearest cost int.
                intervalCost = Math.Ceiling(totalCost / reportNumberOfDays);
                intervalPreviousCost = Math.Ceiling(previousTotalCost / previousReportNumberOfDays);
                intervalCostPercent = Math.Round((((intervalCost - intervalPreviousCost) / NumericHelper.CleanseDivideByZero(intervalPreviousCost)) * 100), 1, MidpointRounding.AwayFromZero);
                //intervalCostPercentString = Math.Abs(intervalCostPercent).ToString() + "%";

                spanCostTrendSummary.InnerText = CH.GetAndFormatSafeContentItem(mContent, "CostTrendSummaryParagraph", new[] { CultureHelper.FormatCurrencyAsString(buildingLCID, intervalCost) }, this.GetType().Name, isSETheme);
                imgCostArrow.Src = (intervalPreviousCost > intervalCost ? "_assets/styles/themes/customReportImages/arrow-down" : (intervalPreviousCost == intervalCost ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");
            }
            else
            {

                imgEnergyArrow.Src = SetManualArrowSrc(energyAnomaliesPriority);
                imgMaintenanceArrow.Src = SetManualArrowSrc(maintenanceAnomaliesPriority);
                imgComfortArrow.Src = SetManualArrowSrc(comfortAnomaliesPriority);

                if(!String.IsNullOrEmpty(avoidableCost))
                    spanCostTrendSummary.InnerText = CH.GetAndFormatSafeContentItem(mContent, "CostTrendSummaryParagraph", new[] { CultureHelper.FormatCurrencyAsString(buildingLCID, avoidableCost) }, this.GetType().Name, isSETheme);

                imgCostArrow.Src = SetManualArrowSrc(avoidableCostChange);                
            }

            imgAlarmsArrow.Src = SetManualArrowSrc(criticalityAlarms);             
        }

        #endregion

        #region Performance Methods

        /// <summary>
        /// sets the performance charts.
        /// </summary>
        /// <param name="diagnosticsResults"></param>
        private void SetPerformanceChartsAndWorkOrders(IEnumerable<DiagnosticsResult> diagnosticsResults)
        {
            IEnumerable<CVRResult> energyDiagnostics = Enumerable.Empty<CVRResult>();
            IEnumerable<CVRResult> maintenanceDiagnostics = Enumerable.Empty<CVRResult>();
            IEnumerable<CVRResult> comfortDiagnostics = Enumerable.Empty<CVRResult>();
            IEnumerable<WorkOrderPoint> energyWorkOrders = Enumerable.Empty<WorkOrderPoint>();
            IEnumerable<WorkOrderPoint> maintenanceWorkOrders = Enumerable.Empty<WorkOrderPoint>();
            IEnumerable<WorkOrderPoint> comfortWorkOrders = Enumerable.Empty<WorkOrderPoint>();

            using (TextReader sr = new StringReader(energyWorkOrderData))
            {
                energyWorkOrders = ConvertToWorkOrderIEnumerable(Csv.ConvertCSVtoDataTable(new CsvReader(sr))).OrderBy(s => s.Date);
            }
            using (TextReader sr = new StringReader(maintenanceWorkOrderData))
            {
                maintenanceWorkOrders = ConvertToWorkOrderIEnumerable(Csv.ConvertCSVtoDataTable(new CsvReader(sr))).OrderBy(s => s.Date);
            }
            using (TextReader sr = new StringReader(comfortWorkOrderData))
            {
                comfortWorkOrders = ConvertToWorkOrderIEnumerable(Csv.ConvertCSVtoDataTable(new CsvReader(sr))).OrderBy(s => s.Date);
            }

            if (manualPerformanceChartsEntry)
            {
                using (TextReader sr = new StringReader(energyChartData))
                {
                    energyDiagnostics = ConvertToPerformanceChartIEnumerable(Csv.ConvertCSVtoDataTable(new CsvReader(sr))).OrderBy(s => s.Date);
                }
                using (TextReader sr = new StringReader(maintenanceChartData))
                {
                    maintenanceDiagnostics = ConvertToPerformanceChartIEnumerable(Csv.ConvertCSVtoDataTable(new CsvReader(sr))).OrderBy(s => s.Date);
                }
                using (TextReader sr = new StringReader(comfortChartData))
                {
                    comfortDiagnostics = ConvertToPerformanceChartIEnumerable(Csv.ConvertCSVtoDataTable(new CsvReader(sr))).OrderBy(s => s.Date);
                }
            }
            else
            {
                energyDiagnostics = diagnosticsResults
                      .GroupBy(d => d.StartDate)
                      .Select(s => new CVRResult
                      {
                          Date = s.Key,
                          TotalIncidents = s.Where(e => e.EnergyPriority != 0).Count(),
                          AvoidableCost = s.Sum(c => c.CostSavings),
                      }).OrderBy(s => s.Date);

                maintenanceDiagnostics = diagnosticsResults
                        .GroupBy(d => d.StartDate)
                        .Select(s => new CVRResult
                        {
                            Date = s.Key,
                            TotalIncidents = s.Where(m => m.MaintenancePriority != 0).Count(),
                            AveragePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                        }).OrderBy(s => s.Date);


                comfortDiagnostics = diagnosticsResults
                        .GroupBy(d => d.StartDate)
                        .Select(s => new CVRResult
                        {
                            Date = s.Key,
                            TotalIncidents = s.Where(c => c.ComfortPriority != 0).Count(),
                            AveragePriority = s.Where(c => c.ComfortPriority != 0).Any() ? s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                        }).OrderBy(s => s.Date);
            }

            //energy chart
            if (energyDiagnostics.Any())
            {
                energyChart.Series.Clear();

                //set axis intervaltype
                energyChart.ChartAreas["energyChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                //set axis format:  g = short date and time, d = short date
                energyChart.ChartAreas["energyChartArea"].AxisX.LabelStyle.Format = "d";

                //area chart
                var series = new Series
                {
                    ChartType = SeriesChartType.Area,
                    Name = "Area Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    LegendText = CH.GetSafeContentItem(mContent, "TotalEnergyIncidents", this.GetType().Name, isSETheme),
                    Color = Color.FromArgb(120, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.EnergyYellow)),
                };

                series.YAxisType = AxisType.Secondary;
                series.Points.DataBindXY(energyDiagnostics.ToArray(), "Date", energyDiagnostics.ToArray(), "TotalIncidents");

                //interpolate
                energyChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                energyChart.Series.Add(series);


                //line
                series = new Series
                {
                    ChartType = SeriesChartType.FastLine,
                    BorderWidth = 3,
                    Name = "Line Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    LegendText = CH.GetSafeContentItem(mContent, "AvoidableCost", this.GetType().Name, isSETheme),
                    Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange)
                };

                //add data to series
                series.Points.DataBindXY(energyDiagnostics.ToArray(), "Date", energyDiagnostics.ToArray(), "AvoidableCost");

                //interpolate
                energyChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                energyChart.Series.Add(series);


                //point
                series = new Series
                {
                    ChartType = SeriesChartType.Point,
                    MarkerStyle = MarkerStyle.Circle,
                    ShadowColor = Color.DimGray,
                    ShadowOffset = 4,
                    MarkerSize = 6,
                    MarkerBorderWidth = 3,
                    MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange),
                    MarkerColor = Color.White,
                    //BorderWidth = 12,
                    Name = "Point Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    //LegendText = ,
                    Legend = "Hidden",
                    //BorderColor = Color.DimGray,
                    //Color = Color.White
                };

                series.Points.DataBindXY(energyDiagnostics.ToArray(), "Date", energyDiagnostics.ToArray(), "AvoidableCost");

                //interpolate
                energyChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                energyChart.Series.Add(series);

                if (energyWorkOrders.Any())
                {
                    IEnumerable<WorkOrderPoint> groupedWorkOrders = from e in energyDiagnostics
                                                                    join w in energyWorkOrders on e.Date equals w.Date
                                                                    group w by new { w.Date, e.AvoidableCost } into g
                                                                    select new WorkOrderPoint
                                                                    {
                                                                        Date = g.Key.Date,
                                                                        Value = (double)g.Key.AvoidableCost,
                                                                    };

                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        MarkerSize = 16,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed),
                        MarkerColor = Color.Transparent,
                        //BorderWidth = 12,
                        Name = "WO Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = , 
                        Legend = "Hidden",
                        //BorderColor = Color.DimGray,
                        //Color = Color.White
                        Label = "#VALX",
                        SmartLabelStyle = CustomReportsChartHelper.CreateSmartLabel(),
                        LabelForeColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed),
                        Font = new System.Drawing.Font("Arial", 7.25F, System.Drawing.FontStyle.Bold),
                    };
                    series.Points.DataBindXY(groupedWorkOrders.ToArray(), "Date", groupedWorkOrders.ToArray(), "Value");

                    //interpolate
                    energyChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    energyChart.Series.Add(series);
                }
            }

            //maintenance chart
            if (maintenanceDiagnostics.Any())
            {
                maintenanceChart.Series.Clear();

                //set axis intervaltype
                maintenanceChart.ChartAreas["maintenanceChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                //set axis format:  g = short date and time, d = short date
                maintenanceChart.ChartAreas["maintenanceChartArea"].AxisX.LabelStyle.Format = "d";

                //area chart
                var series = new Series
                {
                    ChartType = SeriesChartType.Area,
                    Name = "Area Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    LegendText = CH.GetSafeContentItem(mContent, "AverageMaintenancePriority", this.GetType().Name, isSETheme),
                    Color = Color.FromArgb(120, Color.DarkGray),
                };

                series.YAxisType = AxisType.Secondary;
                series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "Date", maintenanceDiagnostics.ToArray(), "AveragePriority");

                //interpolate
                maintenanceChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                maintenanceChart.Series.Add(series);


                //line
                series = new Series
                {
                    ChartType = SeriesChartType.FastLine,
                    BorderWidth = 3,
                    Name = "Line Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    LegendText = CH.GetSafeContentItem(mContent, "TotalMaintenanceIncidents", this.GetType().Name, isSETheme),
                    Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray)
                };

                //add data to series
                series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "Date", maintenanceDiagnostics.ToArray(), "TotalIncidents");

                //interpolate
                maintenanceChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                maintenanceChart.Series.Add(series);


                //point
                series = new Series
                {
                    ChartType = SeriesChartType.Point,
                    MarkerStyle = MarkerStyle.Circle,
                    ShadowColor = Color.DimGray,
                    ShadowOffset = 4,
                    MarkerSize = 6,
                    MarkerBorderWidth = 3,
                    MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray),
                    MarkerColor = Color.White,
                    //BorderWidth = 12,
                    Name = "Point Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    //LegendText = ,
                    Legend = "Hidden",
                    //BorderColor = Color.DimGray,
                    //Color = Color.White
                };

                series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "Date", maintenanceDiagnostics.ToArray(), "TotalIncidents");

                //interpolate
                maintenanceChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                maintenanceChart.Series.Add(series);

                if (maintenanceWorkOrders.Any())
                {
                    IEnumerable<WorkOrderPoint> groupedWorkOrders = from m in maintenanceDiagnostics
                                                                    join w in maintenanceWorkOrders on m.Date equals w.Date
                                                                    group w by new { w.Date, m.TotalIncidents } into g
                                                                    select new WorkOrderPoint
                                                                    {
                                                                        Date = g.Key.Date,
                                                                        Value = g.Key.TotalIncidents,
                                                                    };

                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        MarkerSize = 16,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed),
                        MarkerColor = Color.Transparent,
                        //BorderWidth = 12,
                        Name = "WO Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = , 
                        Legend = "Hidden",
                        //BorderColor = Color.DimGray,
                        //Color = Color.White
                        Label = "#VALX",
                        SmartLabelStyle = CustomReportsChartHelper.CreateSmartLabel(),
                        LabelForeColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed),
                        Font = new System.Drawing.Font("Arial", 7.25F, System.Drawing.FontStyle.Bold),
                    };
                    series.Points.DataBindXY(groupedWorkOrders.ToArray(), "Date", groupedWorkOrders.ToArray(), "Value");

                    //interpolate
                    maintenanceChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    maintenanceChart.Series.Add(series);
                }
            }

            //comfort chart---
            if (comfortDiagnostics.Any())
            {
                comfortChart.Series.Clear();

                //set axis intervaltype
                comfortChart.ChartAreas["comfortChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                //set axis format:  g = short date and time, d = short date
                comfortChart.ChartAreas["comfortChartArea"].AxisX.LabelStyle.Format = "d";

                //area chart
                var series = new Series
                {
                    ChartType = SeriesChartType.Area,
                    Name = "Area Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    LegendText = CH.GetSafeContentItem(mContent, "AverageComfortPriority", this.GetType().Name, isSETheme),
                    Color = Color.FromArgb(120, Color.LightSteelBlue),
                };

                series.YAxisType = AxisType.Secondary;
                series.Points.DataBindXY(comfortDiagnostics.ToArray(), "Date", comfortDiagnostics.ToArray(), "AveragePriority");

                //interpolate
                comfortChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                comfortChart.Series.Add(series);


                //line
                series = new Series
                {
                    ChartType = SeriesChartType.FastLine,
                    BorderWidth = 3,
                    Name = "Line Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    //LegendText = ,
                    LegendText = CH.GetSafeContentItem(mContent, "TotalComfortIncidents", this.GetType().Name, isSETheme),
                    Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue)
                };

                //add data to series
                series.Points.DataBindXY(comfortDiagnostics.ToArray(), "Date", comfortDiagnostics.ToArray(), "TotalIncidents");

                //interpolate
                comfortChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                comfortChart.Series.Add(series);


                //point
                series = new Series
                {
                    ChartType = SeriesChartType.Point,
                    MarkerStyle = MarkerStyle.Circle,
                    ShadowColor = Color.DimGray,
                    ShadowOffset = 4,
                    MarkerSize = 6,
                    MarkerBorderWidth = 3,
                    MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue),
                    MarkerColor = Color.White,
                    //BorderWidth = 12,
                    Name = "Point Series",
                    //ToolTip = "X: #VALX{d}, Y: #VALY",
                    //LegendToolTip = ,
                    //LegendText = ,
                    Legend = "Hidden"
                    //BorderColor = Color.CornflowerBlue,
                    //Color = Color.White
                };

                series.Points.DataBindXY(comfortDiagnostics.ToArray(), "Date", comfortDiagnostics.ToArray(), "TotalIncidents");

                //interpolate
                comfortChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                comfortChart.Series.Add(series);

                if (comfortWorkOrders.Any())
                {
                    IEnumerable<WorkOrderPoint> groupedWorkOrders = from c in comfortDiagnostics
                                                                    join w in comfortWorkOrders on c.Date equals w.Date
                                                                    group w by new { w.Date, c.TotalIncidents } into g
                                                                    select new WorkOrderPoint
                                                                    {
                                                                        Date = g.Key.Date,
                                                                        Value = g.Key.TotalIncidents,
                                                                    };
                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        MarkerSize = 16,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed),
                        MarkerColor = Color.Transparent,
                        //BorderWidth = 12,
                        Name = "WO Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,                          
                        Legend = "Hidden",
                        //BorderColor = Color.CornflowerBlue,
                        //Color = Color.White,
                        Label = "#VALX",
                        SmartLabelStyle = CustomReportsChartHelper.CreateSmartLabel(),
                        LabelForeColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed),
                        Font = new System.Drawing.Font("Arial", 7.25F, System.Drawing.FontStyle.Bold),
                    };
                    series.Points.DataBindXY(groupedWorkOrders.ToArray(), "Date", groupedWorkOrders.ToArray(), "Value");

                    //interpolate
                    comfortChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    comfortChart.Series.Add(series);
                }
            }

            //Workorders
            lvEnergyWorkOrders.DataSource = energyWorkOrders;
            lvEnergyWorkOrders.DataBind();
            lvMaintenanceWorkOrders.DataSource = maintenanceWorkOrders;
            lvMaintenanceWorkOrders.DataBind();
            lvComfortWorkOrders.DataSource = comfortWorkOrders;
            lvComfortWorkOrders.DataBind();
        }
        
        #endregion

        #region Site Methods

        private void SetSiteBreakdownPieChart()
        {
            int pointCounter = 0;

            IEnumerable<SiteBreakdownItem> siteBreakdowns = Enumerable.Empty<SiteBreakdownItem>();

            using (TextReader sr = new StringReader(breakdown))
            {
                siteBreakdowns = ConvertToSiteBreakdownsIEnumerable(Csv.ConvertCSVtoDataTable(new CsvReader(sr))).OrderByDescending(s => s.Percent);
            }

            siteBreakdownPieChart.Series.Clear();

            var series = new Series
            {
                ChartType = SeriesChartType.Pie,
                Name = "Pie Series",
                XValueMember = "Title",
                YValueMembers = "Percent",
                Label = "#VALX\n#VALY (#PERCENT)",
                SmartLabelStyle = CustomReportsChartHelper.CreateSmartLabel(),
                LabelForeColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.DarkGray),
                Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold),
            };

            series.SetCustomProperty("PieLabelStyle", "outside");

            siteBreakdownPieChart.Series.Add(series);
            siteBreakdownPieChart.DataSource = siteBreakdowns;
            siteBreakdownPieChart.DataBind();

            foreach (DataPoint p in siteBreakdownPieChart.Series.First().Points)
            {
                //p.LegendText = StringHelper.TrimText(p.AxisLabel, 15);
                //p.ToolTip = p.LegendToolTip = building + " = " + p.AxisLabel + ", " + cost + " = " + p.YValues.First().ToString();
                //p.AxisLabel = "";

                if (pointCounter < ColorHelper.SEChartingColorsSystem.Count())
                    p.Color = ColorHelper.SEChartingColorsSystem.ElementAtOrDefault(pointCounter);
                else if (pointCounter - ColorHelper.SEChartingColorsSystem.Count() < ColorHelper.ChartingSystemColors.Count())
                    p.Color = ColorHelper.ChartingSystemColors.ElementAtOrDefault(pointCounter);

                pointCounter++;
            }

            //add title
            //buildingSummaryPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "BuildingSummaryPieChartTitle", this.GetType().Name, isSETheme);               
        }


        #endregion

        #region Chart Methods

        protected void ChartEnergy_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(energyChart);
        }
        protected void ChartMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(maintenanceChart);
        }
        protected void ChartComfort_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(comfortChart);
        }

        #endregion

        #region Button Events

        protected void remarksButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleRadEdit(editorRemarks, litRemarks);
        }

        protected void fieldQuotesGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridFieldQuotes);
        }
        protected void workOrdersGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridWorkOrders);
        }
        protected void surveysGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridSurveys);
        }
        protected void feedbackGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridFeedback);
        }

        protected void btnDownloadButton_Click(object sender, EventArgs e)
        {
            //Check if logged in
            if (SiteUser.Current.IsAnonymous)
            {
                //in order to refer back to module after logging in.
                //also for qr code reader redirect to profile pages after loggging in.
                Response.Redirect("/Home.aspx?referrer=" + Request.Url.PathAndQuery);
            }

            try
            {
                DownloadButtonActions(hdn_container.Value);
            }
            catch (Exception ex)
            {
                lblDownloadError.Text = downloadFailed;
                lblDownloadError.Visible = true;
                Logger.Log(DataConstants.LogLevel.ERROR, "Customer value report download failed.", ex);
            }
        }

        #endregion

        #region Culture and Content Helper Methods

        protected void DelayedInitializeCulture()
        {
            if (cultureFormat != userCulture)
            {
                UICulture = Culture = userCulture;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(userCulture);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userCulture);

                base.InitializeCulture();

                CultureInfo tempCulture = CultureInfo.GetCultureInfo(cultureFormat);
                tempFd = Convert.ToDateTime(tempFd).ToString(tempCulture);
                tempTd = Convert.ToDateTime(tempTd).ToString(tempCulture);
                tempPfd = Convert.ToDateTime(tempPfd).ToString(tempCulture);
                tempPtd = Convert.ToDateTime(tempPtd).ToString(tempCulture);
                contractSd = Convert.ToDateTime(contractSd).ToString(tempCulture);
                contractEd = Convert.ToDateTime(contractEd).ToString(tempCulture);                
            }
                
            UICulture = Culture = cultureFormat;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureFormat);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureFormat);

            base.InitializeCulture();

            //set dates based on culture now
            fd = Convert.ToDateTime(tempFd);
            td = Convert.ToDateTime(tempTd);
            pfd = Convert.ToDateTime(tempPfd);
            ptd = Convert.ToDateTime(tempPtd);
            csd = Convert.ToDateTime(contractSd);
            ced = Convert.ToDateTime(contractEd);   
        }

        private void GetAndSetInitialContent()
        {
            //get content            
            mContent = mDataManager.ContentDataMapper.GetSafeContentSections(new int[] { CS.Global, CS.Administration, CS.CVR, CS.Bureau }, languageCultureName);

            //headers
            spanCoverHeader.InnerText = CH.GetSafeContentItem(mContent, "CVRHeader", this.GetType().Name, isSETheme);
            newPageHeader1.InnerText = newPageHeader2.InnerText = newPageHeader3.InnerText = newPageHeader4.InnerText = newPageHeader5.InnerText = newPageHeader6.InnerText = newPageHeader7.InnerText = newPageHeader8.InnerText = CH.GetSafeContentItem(mContent, "CVRPageHeader", this.GetType().Name, isSETheme);

            //cover
            divCoverPreparedFor.InnerText = CH.GetAndPrependSafeContentItem(mContent, "PreparedFor", divCoverPreparedFor.InnerText, this.GetType().Name, isSETheme);

            //table of contents and titles
            lblCustomerPage.InnerText = customerDetailsHeader.InnerText = CH.GetSafeContentItem(mContent, "CustomerDetailsHeader", this.GetType().Name, isSETheme);
            lblKeyIndicatorsPage.InnerText = keyIndicatorsHeader.InnerText = CH.GetSafeContentItem(mContent, "KeyIndicatorsHeader", this.GetType().Name, isSETheme);
            lblItemsRequiringAttentionPage.InnerText = itemsRequiringAttentionHeader.InnerText = CH.GetSafeContentItem(mContent, "ItemsRequiringAttentionHeader", this.GetType().Name, isSETheme);
            lblPerformancePage.InnerText = performanceHeader.InnerText = CH.GetSafeContentItem(mContent, "PerformanceChartsHeader", this.GetType().Name, isSETheme);
            lblServiceDetailsPage.InnerText = serviceDetailsHeader.InnerText = CH.GetSafeContentItem(mContent, "ServiceDetailsHeader", this.GetType().Name, isSETheme);
            lblCustomerSatisfactionPage.InnerText = customerSatisfactionHeader.InnerText = CH.GetSafeContentItem(mContent, "CSATHeader", this.GetType().Name, isSETheme);
            lblSitePage.InnerText = siteHeader.InnerText = CH.GetSafeContentItem(mContent, "SiteHeader", this.GetType().Name, isSETheme);
            contractDetailsHeader.InnerText = CH.GetSafeContentItem(mContent, "ContractDetailsHeader", this.GetType().Name, isSETheme);
            supportTeamHeader.InnerText = CH.GetSafeContentItem(mContent, "SupportTeamHeader", this.GetType().Name, isSETheme);            

            //customer details
            lblCutomerName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Customer", lblCutomerName.InnerText, this.GetType().Name, isSETheme);
            lblCustomerLocation.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Location", lblCustomerLocation.InnerText, this.GetType().Name, isSETheme);
            lblCustomerPhone.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Phone", lblCustomerPhone.InnerText, this.GetType().Name, isSETheme);
            lblContactName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ContactNameLabel", lblContactName.InnerText, this.GetType().Name, isSETheme);
            lblContactEmail.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Email", lblContactEmail.InnerText, this.GetType().Name, isSETheme);
            lblContactPhone.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Phone", lblContactPhone.InnerText, this.GetType().Name, isSETheme);
            lblServicePlan.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ServicePlanLabel", lblServicePlan.InnerText, this.GetType().Name, isSETheme);
            lblContractStartDate.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ContractStartDateLabel", lblContractStartDate.InnerText, this.GetType().Name, isSETheme);
            lblContractEndDate.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ContractEndDateLabel", lblContractEndDate.InnerText, this.GetType().Name, isSETheme);
            lblAdditionalServices.InnerText = CH.GetAndPrependSafeContentItem(mContent, "AdditionalServicesLabel", lblAdditionalServices.InnerText, this.GetType().Name, isSETheme);
            lblBuildingCount.InnerText = CH.GetAndPrependSafeContentItem(mContent, "NumBuildings", lblBuildingCount.InnerText, this.GetType().Name, isSETheme);
            lblBuildingList.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Buildings", lblBuildingList.InnerText, this.GetType().Name, isSETheme);
            lblPrimaryServiceTechnicianName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "PrimaryTechnicianLabel", lblPrimaryServiceTechnicianName.InnerText, this.GetType().Name, isSETheme);
            lblPrimaryServiceTechnicianEmail.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Email", lblPrimaryServiceTechnicianEmail.InnerText, this.GetType().Name, isSETheme);
            lblPrimaryServiceTechnicianPhone.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Phone", lblPrimaryServiceTechnicianPhone.InnerText, this.GetType().Name, isSETheme);
            lblSecondaryServiceTechnicianName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "SecondaryTechnicianLabel", lblSecondaryServiceTechnicianName.InnerText, this.GetType().Name, isSETheme);
            lblSecondaryServiceTechnicianEmail.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Email", lblSecondaryServiceTechnicianEmail.InnerText, this.GetType().Name, isSETheme);
            lblSecondaryServiceTechnicianPhone.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Phone", lblSecondaryServiceTechnicianPhone.InnerText, this.GetType().Name, isSETheme);
            lblSalesName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "SalesEngineerLabel", lblSalesName.InnerText, this.GetType().Name, isSETheme);
            lblSalesEmail.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Email", lblSalesEmail.InnerText, this.GetType().Name, isSETheme);
            lblSalesPhone.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Phone", lblSalesPhone.InnerText, this.GetType().Name, isSETheme);
            lblRemoteCenterEngineerName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "RemoteEngineerLabel", lblRemoteCenterEngineerName.InnerText, this.GetType().Name, isSETheme);
            lblRemoteCenterEngineerEmail.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Email", lblRemoteCenterEngineerEmail.InnerText, this.GetType().Name, isSETheme);
            lblRemoteCenterEngineerPhone.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Phone", lblRemoteCenterEngineerPhone.InnerText, this.GetType().Name, isSETheme);
            lblAdminName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "AdminContactLabel", lblAdminName.InnerText, this.GetType().Name, isSETheme);
            lblAdminEmail.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Email", lblAdminEmail.InnerText, this.GetType().Name, isSETheme);
            lblAdminPhone.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Phone", lblAdminPhone.InnerText, this.GetType().Name, isSETheme);
            serviceContent.InnerText = CH.GetAndFormatSafeContentItem(mContent, "ServiceContent", new[] { companyPhone }, this.GetType().Name, isSETheme);

            //key indicators
            keyIndicatorsNote.InnerText += CH.GetSafeContentItem(mContent, "KeyIndicatorsNote", this.GetType().Name, isSETheme);
            keyIndicatorsNote2.InnerHtml += CH.GetSafeContentItem(mContent, "KeyIndicatorsNote2", this.GetType().Name, isSETheme);
            divAlarmsLabel.InnerText = CH.GetAndPrependSafeContentItem(mContent, "NumAlarms", divAlarmsLabel.InnerText, this.GetType().Name, isSETheme);
            divEnergyLabel.InnerText = CH.GetAndPrependSafeContentItem(mContent, "EnergyIncidents", divEnergyLabel.InnerText, this.GetType().Name, isSETheme);
            divMaintenanceLabel.InnerText = CH.GetAndPrependSafeContentItem(mContent, "MaintenanceIncidents", divMaintenanceLabel.InnerText, this.GetType().Name, isSETheme);
            divComfortLabel.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ComfortIncidents", divComfortLabel.InnerText, this.GetType().Name, isSETheme);
            divCostLabel.InnerText = CH.GetAndPrependSafeContentItem(mContent, "AvoidableCost", divCostLabel.InnerText, this.GetType().Name, isSETheme);

            lblNumAlarmsNotified.InnerText = CH.GetAndPrependSafeContentItem(mContent, "NumCriticalAlarmsNotifiedLabel", lblNumAlarmsNotified.InnerText, this.GetType().Name, isSETheme);
            lblNumAlarmsFixed.InnerText = CH.GetAndPrependSafeContentItem(mContent, "NumCriticalAlarmsFixedLabel", lblNumAlarmsFixed.InnerText, this.GetType().Name, isSETheme);
            lblNumAlarmsAction.InnerText = CH.GetAndPrependSafeContentItem(mContent, "NumCriticalAlarmsActionLabel", lblNumAlarmsAction.InnerText, this.GetType().Name, isSETheme);
            lblNumIssuesFixed.InnerText = CH.GetAndPrependSafeContentItem(mContent, "NumIssuesFixedLabel", lblNumIssuesFixed.InnerText, this.GetType().Name, isSETheme);
            lblResponseTime.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ResponseTimeLabel", lblResponseTime.InnerText, this.GetType().Name, isSETheme);
            lblOnsiteRepairTime.InnerText = CH.GetAndPrependSafeContentItem(mContent, "TimeOnsiteRepairLabel", lblOnsiteRepairTime.InnerText, this.GetType().Name, isSETheme);
            lblMeanRepairTime.InnerText = CH.GetAndPrependSafeContentItem(mContent, "MeanTimeRepairLabel", lblMeanRepairTime.InnerText, this.GetType().Name, isSETheme);

            //items requiring attention
            spanFieldQuotesTitle.InnerText = CH.GetSafeContentItem(mContent, "FieldQuotesTableHeader", this.GetType().Name, isSETheme);
            gridFieldQuotes.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "FieldQuoteNumberTableHeader", this.GetType().Name, isSETheme);
            gridFieldQuotes.Columns[1].HeaderText = CH.GetSafeContentItem(mContent, "QuoteDateTableHeader", this.GetType().Name, isSETheme);
            gridFieldQuotes.Columns[2].HeaderText = CH.GetSafeContentItem(mContent, "FocusAreasTableHeader", this.GetType().Name, isSETheme);
            gridFieldQuotes.Columns[3].HeaderText = CH.GetSafeContentItem(mContent, "Amount", this.GetType().Name, isSETheme);

            //performance charts
            totalEnergyTrendsHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalEnergyTrendsHeader", this.GetType().Name, isSETheme);
            totalMaintenanceTrendsHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalMaintenanceTrendsHeader", this.GetType().Name, isSETheme);
            totalComfortTrendsHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalComfortTrendsHeader", this.GetType().Name, isSETheme);
            maintenanceChart.ChartAreas[0].AxisY.Title = comfortChart.ChartAreas[0].AxisY.Title = CH.GetSafeContentItem(mContent, "TotalIncidents", this.GetType().Name, isSETheme);
            maintenanceChart.ChartAreas[0].AxisY2.Title = comfortChart.ChartAreas[0].AxisY2.Title = CH.GetSafeContentItem(mContent, "AveragePriority", this.GetType().Name, isSETheme);
            energyChart.ChartAreas[0].AxisY.Title = CH.GetSafeContentItem(mContent, "AvoidableCost", this.GetType().Name, isSETheme);
            energyChart.ChartAreas[0].AxisY2.Title = CH.GetSafeContentItem(mContent, "TotalIncidents", this.GetType().Name, isSETheme);

            //service details
            spanWorkOrdersTitle.InnerText = CH.GetSafeContentItem(mContent, "WorkOrderTableHeader", this.GetType().Name, isSETheme);
            gridWorkOrders.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "Date", this.GetType().Name, isSETheme);
            gridWorkOrders.Columns[1].HeaderText = CH.GetSafeContentItem(mContent, "WorkOrderNumTableHeader", this.GetType().Name, isSETheme);
            gridWorkOrders.Columns[2].HeaderText = CH.GetSafeContentItem(mContent, "EngineerName", this.GetType().Name, isSETheme);
            gridWorkOrders.Columns[3].HeaderText = CH.GetSafeContentItem(mContent, "ProblemDescriptionTableHeader", this.GetType().Name, isSETheme);
            gridWorkOrders.Columns[4].HeaderText = CH.GetSafeContentItem(mContent, "Actions", this.GetType().Name, isSETheme);
            gridWorkOrders.Columns[5].HeaderText = CH.GetSafeContentItem(mContent, "ImpactAchievedTableHeader", this.GetType().Name, isSETheme);
            
            //customer satisfaction 
            spanSurveysTitle.InnerText = CH.GetSafeContentItem(mContent, "TechnicianSurveysTableHeader", this.GetType().Name, isSETheme);
            gridSurveys.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "TechnicianTableHeader", this.GetType().Name, isSETheme);
            
            //month headers based on launguage
            CultureInfo tempCulture = CultureInfo.GetCultureInfo(languageCultureName);
            for (int m = 1; m < 13; m++)
            {
                gridSurveys.Columns[m].HeaderText = tempCulture.DateTimeFormat.GetMonthName(m);
            }

            gridSurveys.Columns[13].HeaderText = CH.GetSafeContentItem(mContent, "Total", this.GetType().Name, isSETheme);

            spanFeedbackTitle.InnerText = CH.GetSafeContentItem(mContent, "FeedbackLoopTableHeader", this.GetType().Name, isSETheme);
            gridFeedback.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "WorkOrderNumTableHeader", this.GetType().Name, isSETheme);
            gridFeedback.Columns[1].HeaderText = CH.GetSafeContentItem(mContent, "Date", this.GetType().Name, isSETheme);
            gridFeedback.Columns[2].HeaderText = CH.GetSafeContentItem(mContent, "TechnicianTableHeader", this.GetType().Name, isSETheme);
            gridFeedback.Columns[3].HeaderText = CH.GetSafeContentItem(mContent, "TechnicianScoreTableHeader", this.GetType().Name, isSETheme);
            gridFeedback.Columns[4].HeaderText = CH.GetSafeContentItem(mContent, "CSATScoreTableHeader", this.GetType().Name, isSETheme);
            gridFeedback.Columns[5].HeaderText = CH.GetSafeContentItem(mContent, "CSATScoreReasonsTableHeader", this.GetType().Name, isSETheme);
            gridFeedback.Columns[6].HeaderText = CH.GetSafeContentItem(mContent, "Actions", this.GetType().Name, isSETheme);


            ////site
            breakdownHeader.InnerText = CH.GetSafeContentItem(mContent, "SiteBreakdownChartTitle", this.GetType().Name, isSETheme);
            //heatingSummaryBoilersPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "Boilers", this.GetType().Name, isSETheme);
            //heatingSummaryHeatingSystemsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "HeatingSystems", this.GetType().Name, isSETheme);                                

            remarksHeader.InnerText = CH.GetSafeContentItem(mContent, "RemarksTitle", this.GetType().Name, isSETheme);

            //common grids
            gridFieldQuotes.EmptyDataText = gridWorkOrders.EmptyDataText = gridSurveys.EmptyDataText = gridFeedback.EmptyDataText =
                CH.GetSafeContentItem(mContent, "NoDataDefaultMessage", this.GetType().Name, isSETheme);
        }

        private void GetAndSetPostbackContent()
        {
            //sets content that gets lost on postback   
         
            IEnumerable<Content> tempContent = mDataManager.ContentDataMapper.GetSafeContentSections(new int[] { CS.Global }, languageCultureName);

            //title
            title.Text = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(isSETheme, title.Text).ToUpper();

            //download
            btnDownload.Text = CH.GetSafeContentItem(tempContent, "Download", this.GetType().Name, isSETheme);
            spanDownload.InnerText = CH.GetSafeContentItem(tempContent, "DownloadParagraph", this.GetType().Name, isSETheme);
        }

        private string CreatePeriodContent(DateTime sd, DateTime ed)
        {
            return String.Format("{0} {1} - {2} {3}",  CH.GetSafeContentItem(mContent, "From", this.GetType().Name, isSETheme), sd.ToString("MMM d, yyyy"), CH.GetSafeContentItem(mContent, "To", this.GetType().Name, isSETheme), ed.ToString("MMM d, yyyy"));
        }

        #endregion

        #region GET and POST Request Data

        private void GetFormPostData()
        {
            foreach (string key in Request.Form.AllKeys)
            {
                if (key == null) continue;

                //ignore cw header bar client ddl and module ddl

                //
                if (key.Contains("$hdnCID"))
                {
                    cid = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$hdnUID"))
                {
                    uid = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$hdnOID"))
                {
                    oid = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$hdnUserCulture"))
                {
                    userCulture = Request.Form[key];
                }

                //
                else if (key.Contains("$txtPreviousFromDate"))
                {
                    tempPfd = Request.Form[key];
                }
                else if (key.Contains("$txtPreviousToDate"))
                {
                    tempPtd = Request.Form[key];
                }
                else if (key.Contains("$txtFromDate"))
                {
                    tempFd = Request.Form[key];
                }
                else if (key.Contains("$txtToDate"))
                {
                    tempTd = Request.Form[key];
                }                
                else if (key.Contains("$lbBuildings"))
                {
                    bids = Request.Form[key].Split(',').Select(x => Int32.Parse(x)).ToList();
                }
                else if (key.Contains("$ddlTheme"))
                {
                    isSETheme = Convert.ToBoolean(Convert.ToInt32(Request.Form[key]));
                }
                else if (key.Contains("$ddlLanguage"))
                {
                    languageCultureName = Request.Form[key];
                }
                else if (key.Contains("$ddlCulture"))
                {
                    cultureFormat = CultureHelper.GetCultureName(Convert.ToInt32(Request.Form[key]));
                }
                else if (key.Contains("$chkShowClientLogos"))
                {
                    //form post will post value as "on" only if it is selected, otherwise it wont be in the request.
                    showClientLogos = true;
                }
                //else if (key.Contains("$chkIncludeCoverPage"))
                //{
                //    includeCoverPage = true;
                //}
                else if (key.Contains("$chkIncludeContentsPage"))
                {
                    includeContentsPage = true;
                }

                //
                else if (key.Contains("$chkManualCustomerDetailsEntry"))
                {
                    manualCustomerDetails = true;
                }
                else if (key.Contains("$txtCustomerAddress"))
                {
                    customerAddress = Request.Form[key];
                }
                else if (key.Contains("$txtCustomerCity"))
                {
                    customerCity = Request.Form[key];
                }
                else if (key.Contains("$ddlCustomerCountryAlpha2Code"))
                {
                    customerCountryAlpha2Code = Request.Form[key];
                }
                else if (key.Contains("$ddlCustomerStateID"))
                {
                    customerStateID = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$txtCustomerZip"))
                {
                    customerZip = Request.Form[key];
                }
                else if (key.Contains("$txtCustomerPhone"))
                {
                    customerPhone = Request.Form[key];
                }
                else if (key.Contains("$txtPrimaryContactName"))
                {
                    contactName = Request.Form[key];
                }
                else if (key.Contains("$txtPrimaryContactEmail"))
                {
                    contactEmail = Request.Form[key];
                }
                else if (key.Contains("$txtPrimaryContactPhone"))
                {
                    contactPhone = Request.Form[key];
                }

                //
                else if (key.Contains("$ddlPlan"))
                {
                    servicePlan = Request.Form[key];
                }
                else if (key.Contains("$txtContractStartDate"))
                {
                    contractSd = Request.Form[key];
                }
                else if (key.Contains("$txtContractEndDate"))
                {
                    contractEd = Request.Form[key];
                }

                //
                else if (key.Contains("$txtPrimaryServiceTechnicianName"))
                {
                    primaryServiceTechName = Request.Form[key];
                }
                else if (key.Contains("$txtPrimaryServiceTechnicianEmail"))
                {
                    primaryServiceTechEmail = Request.Form[key];
                }
                else if (key.Contains("$txtPrimaryServiceTechnicianPhone"))
                {
                    primaryServiceTechPhone = Request.Form[key];
                }
                else if (key.Contains("$txtSecondaryServiceTechnicianName"))
                {
                    secondaryServiceTechName = Request.Form[key];
                }
                else if (key.Contains("$txtSecondaryServiceTechnicianEmail"))
                {
                    secondaryServiceTechEmail = Request.Form[key];
                }
                else if (key.Contains("$txtSecondaryServiceTechnicianPhone"))
                {
                    secondaryServiceTechPhone = Request.Form[key];
                }
                else if (key.Contains("$txtSalesEngineerName"))
                {
                    salesName = Request.Form[key];
                }
                else if (key.Contains("$txtSalesEngineerEmail"))
                {
                    salesEmail = Request.Form[key];
                }
                else if (key.Contains("$txtSalesEngineerPhone"))
                {
                    salesPhone = Request.Form[key];
                }
                else if (key.Contains("$txtRemoteCenterEngineerName"))
                {
                    remoteCenterEngineerName = Request.Form[key];
                }
                else if (key.Contains("$txtRemoteCenterEngineerEmail"))
                {
                    remoteCenterEngineerEmail = Request.Form[key];
                }
                else if (key.Contains("$txtRemoteCenterEngineerPhone"))
                {
                    remoteCenterEngineerPhone = Request.Form[key];
                }
                else if (key.Contains("$txtAdminContactPointName"))
                {
                    adminName = Request.Form[key];
                }
                else if (key.Contains("$txtAdminContactPointEmail"))
                {
                    adminEmail = Request.Form[key];
                }
                else if (key.Contains("$txtAdminContactPointPhone"))
                {
                    adminPhone = Request.Form[key];
                }
                else if (key.Contains("$txtCompanyServicePhone"))
                {
                    companyPhone = Request.Form[key];
                }

                //
                else if (key.Contains("$chkIncludeKeyIndicatorsReport"))
                {
                    includeKeyIndicators = true;
                }
                else if (key.Contains("$chkManualKeyIndicatorsEntry"))
                {
                    manualKeyIndicators = true;
                }                
                else if (key.Contains("$ddlEnergyAnomaliesPriority"))
                {
                    energyAnomaliesPriority = Request.Form[key];
                }
                else if (key.Contains("$ddlMaintenanceAnomaliesPriority"))
                {
                    maintenanceAnomaliesPriority = Request.Form[key];
                }
                else if (key.Contains("$ddlComfortAnomaliesPriority"))
                {
                    comfortAnomaliesPriority = Request.Form[key];
                }
                else if (key.Contains("$ddlComfortAnomaliesPriority"))
                {
                    comfortAnomaliesPriority = Request.Form[key];
                }
                else if (key.Contains("$ddlAvoidableCostChange"))
                {
                    avoidableCostChange = Request.Form[key];
                }
                else if (key.Contains("txtAvoidableCost"))
                {
                    avoidableCost = Request.Form[key];
                }
                else if (key.Contains("txtRatingSystem"))
                {
                    ratingSystem = Request.Form[key];
                }
                else if (key.Contains("txtRatingValue"))
                {
                    ratingValue = Request.Form[key];
                }
                else if (key.Contains("$ddlCriticalityAlarms"))
                {
                    criticalityAlarms = Request.Form[key];
                }
                else if (key.Contains("$txtAlarmsNotified"))
                {
                    alarmsNotified = Request.Form[key];
                }
                else if (key.Contains("$txtAlarmsFixed"))
                {
                    alarmsFixed = Request.Form[key];
                }
                else if (key.Contains("$txtAlarmsAction"))
                {
                    alarmsAction = Request.Form[key];
                }
                else if (key.Contains("$txtIssueFixed"))
                {
                    issueFixed = Request.Form[key];
                }
                else if (key.Contains("$txtResponseTime"))
                {
                    responseTime = Request.Form[key];
                }
                else if (key.Contains("$txtTimeOnsite"))
                {
                    timeOnsite = Request.Form[key];
                }
                else if (key.Contains("$txtMeanTime"))
                {
                    meanTime = Request.Form[key];
                }

                //
                else if (key.Contains("$chkIncludeItemsRequiringAttention"))
                {
                    includeItemsRequiringAttention = true;
                }
                else if (key.Contains("$txtQuotes"))
                {
                    quotes = Request.Form[key];
                }

                //
                else if (key.Contains("$chkIncludePerformanceCharts"))
                {
                    includePerformanceCharts = true;
                }
                else if (key.Contains("$chkManualPerformanceChartsEntry"))
                {
                    manualPerformanceChartsEntry = true;
                }
                else if (key.Contains("$txtEnergyChartData"))
                {
                    energyChartData = Request.Form[key];
                }
                else if (key.Contains("$txtEnergyWorkOrderData"))
                {
                    energyWorkOrderData = Request.Form[key];
                }
                else if (key.Contains("$txtMaintenanceChartData"))
                {
                    maintenanceChartData = Request.Form[key];
                }
                else if (key.Contains("$txtMaintenanceWorkOrderData"))
                {
                    maintenanceWorkOrderData = Request.Form[key];
                }
                else if (key.Contains("$txtComfortChartData"))
                {
                    comfortChartData = Request.Form[key];
                }
                else if (key.Contains("$txtComfortWorkOrderData"))
                {
                    comfortWorkOrderData = Request.Form[key];
                }

                //
                else if (key.Contains("$chkIncludeServiceDetails"))
                {
                    includeServiceDetails = true;
                }
                else if (key.Contains("$txtServiceDetails"))
                {
                    serviceDetails = Request.Form[key];
                }

                //
                else if (key.Contains("$chkIncludeMetrics"))
                {
                    includeMetrics = true;
                }
                else if (key.Contains("$txtSurveys"))
                {
                    surveys = Request.Form[key];
                }
                else if (key.Contains("$txtFeedback"))
                {
                    feedback = Request.Form[key];
                }

                //
                else if (key.Contains("$chkIncludeSiteVisits"))
                {
                    includeSiteVisits = true;
                }
                else if (key.Contains("$txtBreakdown"))
                {
                    breakdown = Request.Form[key];
                }
                else if (key.Contains("$editorRemarks"))
                {
                    remarks = Request.Form[key];
                }
            }
        }

        private void GetQueryStringData()
        {
            foreach (string key in Request.QueryString.AllKeys)
            {
                if (String.IsNullOrEmpty(key)) continue;

                if (key.Contains("CID"))
                {
                    cid = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("UID"))
                {
                    uid = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("OID"))
                {
                    oid = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("UserCulture"))
                {
                    userCulture = Request.QueryString[key];
                }

                //
                if (key.Contains("PreviousFromDate"))
                {
                    tempPfd = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("PreviousToDate"))
                {
                    tempPtd = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                if (key.Contains("FromDate"))
                {
                    tempFd = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("ToDate"))
                {
                    tempTd = HttpUtility.HtmlDecode(Request.QueryString[key]);
                } 
                else if (key.Contains("Buildings"))
                {
                    bids = HttpUtility.HtmlDecode(Request.QueryString[key]).Split(',').Select(x => Int32.Parse(x)).ToList();
                }
                else if (key.Contains("Theme"))
                {
                    isSETheme = Convert.ToBoolean(Convert.ToInt32(Request.QueryString[key]));
                }
                else if (key.Contains("Language"))
                {
                    languageCultureName = Request.QueryString[key];
                }
                else if (key.Contains("Culture"))
                {
                    cultureFormat = CultureHelper.GetCultureName(Convert.ToInt32(Request.QueryString[key]));
                }
                else if (key.Contains("ShowClientLogos"))
                {
                    //form post will post value as "on" only if it is selected, otherwise it wont be in the request.
                    showClientLogos = Convert.ToBoolean(Request.QueryString[key]);
                }
                //else if (key.Contains("IncludeCoverPage"))
                //{
                //    includeCoverPage = Convert.ToBoolean(Request.QueryString[key]);
                //}
                else if (key.Contains("IncludeContentsPage"))
                {
                    includeContentsPage = Convert.ToBoolean(Request.QueryString[key]);
                }

                //
                else if (key.Contains("ManualCustomerDetails"))
                {
                    manualCustomerDetails = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("CustomerAddress"))
                {
                    customerAddress = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("CustomerCity"))
                {
                    customerCity = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("CustomerCountryAlpha2Code"))
                {
                    customerCountryAlpha2Code = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("CustomerStateID"))
                {
                    customerStateID = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("CustomerZip"))
                {
                    customerZip = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("CustomerPhone"))
                {
                    customerPhone = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("PrimaryContactName"))
                {
                    contactName = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("PrimaryContactEmail"))
                {
                    contactEmail = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("PrimaryContactPhone"))
                {
                    contactPhone = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }

                //
                else if (key.Contains("Plan"))
                {
                    servicePlan = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("ContractStartDate"))
                {
                    contractSd = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("ContractEndDate"))
                {
                    contractEd = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }

                //
                else if (key.Contains("PrimaryServiceTechnicianName"))
                {
                    primaryServiceTechName = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("PrimaryServiceTechnicianEmail"))
                {
                    primaryServiceTechEmail = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("PrimaryServiceTechnicianPhone"))
                {
                    primaryServiceTechPhone = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("SecondaryServiceTechnicianName"))
                {
                    secondaryServiceTechName = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("SecondaryServiceTechnicianEmail"))
                {
                    secondaryServiceTechEmail = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("SecondaryServiceTechnicianPhone"))
                {
                    secondaryServiceTechPhone = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("SalesEngineerName"))
                {
                    salesName = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("SalesEngineerEmail"))
                {
                    salesEmail = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("SalesEngineerPhone"))
                {
                    salesPhone = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("RemoteCenterEngineerName"))
                {
                    remoteCenterEngineerName = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("RemoteCenterEngineerEmail"))
                {
                    remoteCenterEngineerEmail = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("RemoteCenterEngineerPhone"))
                {
                    remoteCenterEngineerPhone = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("AdminContactPointName"))
                {
                    adminName = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("AdminContactPointEmail"))
                {
                    adminEmail = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("AdminContactPointPhone"))
                {
                    adminPhone = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("CompanyServicePhone"))
                {
                    companyPhone = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }

                //
                else if (key.Contains("IncludeKeyIndicatorsReport"))
                {
                    includeKeyIndicators = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("ManualKeyIndicatorsEntry"))
                {
                    manualKeyIndicators = Convert.ToBoolean(Request.QueryString[key]);
                }                
                else if (key.Contains("MaintenanceAnomaliesPriority"))
                {
                    maintenanceAnomaliesPriority = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("ComfortAnomaliesPriority"))
                {
                    comfortAnomaliesPriority = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("AvoidableCostChange"))
                {
                    avoidableCostChange = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("AvoidableCost"))
                {
                    avoidableCost = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("RatingSystem"))
                {
                    ratingSystem = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("RatingValue"))
                {
                    ratingValue = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("CriticalityAlarms"))
                {
                    criticalityAlarms = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("AlarmsNotified"))
                {
                    alarmsNotified = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("AlarmsFixed"))
                {
                    alarmsFixed = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("AlarmsAction"))
                {
                    alarmsAction = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("IssueFixed"))
                {
                    issueFixed = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("ResponseTime"))
                {
                    responseTime = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("TimeOnsite"))
                {
                    timeOnsite = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("MeanTime"))
                {
                    meanTime = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }

                //
                else if (key.Contains("IncludeItemsRequiringAttention"))
                {
                    includeItemsRequiringAttention = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("Quotes"))
                {
                    quotes = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }

                //
                else if (key.Contains("IncludePerformanceCharts"))
                {
                    includePerformanceCharts = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("ManualPerformanceChartsEntry"))
                {
                    manualPerformanceChartsEntry = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("EnergyChartData"))
                {
                    energyChartData = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("EnergyWorkOrderData"))
                {
                    energyWorkOrderData = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("MaintenanceChartData"))
                {
                    maintenanceChartData = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("MaintenanceWorkOrderData"))
                {
                    maintenanceWorkOrderData = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("ComfortChartData"))
                {
                    comfortChartData = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }             
                else if (key.Contains("ComfortWorkOrderData"))
                {
                    comfortWorkOrderData = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
      
                //
                else if (key.Contains("IncludeServiceDetails"))
                {
                    includeServiceDetails = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("ServiceDetails"))
                {
                    serviceDetails = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }

                //
                else if (key.Contains("IncludeMetrics"))
                {
                    includeMetrics = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("Surveys"))
                {
                    surveys = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("Feedback"))
                {
                    feedback = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }

                //
                else if (key.Contains("IncludeSiteVisits"))
                {
                    includeSiteVisits = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("Breakdown"))
                {
                    breakdown = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("Remarks"))
                {
                    remarks = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
            }
        }

        #endregion

        #region PDF Helper Methods

        public string ReplacePaths(string html)
        {
            //set base url to current instance for this request.
            //string baseString = "id=\"baseTag\"";
            //html = html.Replace(baseString, baseString + " " + "href=\"https://" + RoleEnvironment.CurrentRoleInstance.InstanceEndpoints.Values.First().IPEndpoint.ToString() + "/\"");

            string tempCoverClientLogoPath = "src=\"/tempCustomReports/cvr_coverclientlogo_" + guid + ".png\"";

            string tempEnergyPath = "src=\"/tempCustomReports/cvr_energy_" + guid + ".png\"";
            string tempMaintenancePath = "src=\"/tempCustomReports/cvr_maintenance_" + guid + ".png\"";
            string tempComfortPath = "src=\"/tempCustomReports/cvr_comfort_" + guid + ".png\"";
            string tempSiteBreakdownPath = "src=\"/tempCustomReports/cvr_sitebreakdown_" + guid + ".png\"";

            html = CustomReportsFileHelper.ReplacePath(tempCoverClientLogoPath, html, hdnShowClientLogos, radCoverClient);

            html = CustomReportsFileHelper.ReplacePath(tempEnergyPath, html, divPerformancePage);
            html = CustomReportsFileHelper.ReplacePath(tempMaintenancePath, html, divPerformancePage);
            html = CustomReportsFileHelper.ReplacePath(tempComfortPath, html, divPerformancePage);
            html = CustomReportsFileHelper.ReplacePath(tempSiteBreakdownPath, html, divSitePage);

            return html;
        }

        #endregion  

        #region Helper Methods

        private IEnumerable<CVRResult> ConvertToPerformanceChartIEnumerable(DataTable dataTable)
        {
            return dataTable.AsEnumerable().Select(row =>
                {
                    return new CVRResult
                    {
                        Date = Convert.ToDateTime(row["Date"]),
                        TotalIncidents = Convert.ToInt32(row["Total Incidents"]),
                        AveragePriority = dataTable.Columns.Contains("Average Priority") ? (double?)Convert.ToDouble(row["Average Priority"]) : null,
                        AvoidableCost = dataTable.Columns.Contains("Avoidable Cost") ? (double?)Convert.ToDouble(row["Avoidable Cost"]) : null,
                    };
                });
        }

        private IEnumerable<WorkOrderPoint> ConvertToWorkOrderIEnumerable(DataTable dataTable)
        {
            return dataTable.AsEnumerable().Select(row =>
            {
                return new WorkOrderPoint
                {
                    Date = Convert.ToDateTime(row["Date"]),
                    WorkOrder = Convert.ToString(row["Work Order #"]),
                    FormatedText = CH.GetAndFormatSafeContentItem(mContent, "WorkOrderContent", new string[] { Convert.ToString(row["Work Order #"]) }, this.GetType().Name, isSETheme),
                };
            });
        }

        private IEnumerable<SiteBreakdownItem> ConvertToSiteBreakdownsIEnumerable(DataTable dataTable)
        {
            return dataTable.AsEnumerable().Select(row =>
            {
                return new SiteBreakdownItem
                {
                    Title = Convert.ToString(row["Title"]),
                    Percent = Convert.ToDouble(row["Percent"]),                    
                };
            });
        }

        private void DownloadButtonActions(string html)
        {
            cid = Convert.ToInt32(hdnCID.Value);
            isSETheme = Convert.ToBoolean(hdnIsSETheme.Value);

            Boolean result = false;
            guid = Guid.NewGuid();

            string tempCoverClientLogo = "", tempEnergyChart = "", tempMaintenanceChart = "", tempComfortChart = "", tempSiteBreakdownChart = "";

            tempCoverClientLogo = Server.MapPath("\\tempCustomReports\\cvr_coverclientlogo_" + guid + ".png");

            tempEnergyChart = Server.MapPath("\\tempCustomReports\\cvr_energy_" + guid + ".png");
            tempMaintenanceChart = Server.MapPath("\\tempCustomReports\\cvr_maintenance_" + guid + ".png");
            tempComfortChart = Server.MapPath("\\tempCustomReports\\cvr_comfort_" + guid + ".png");
            tempSiteBreakdownChart = Server.MapPath("\\tempCustomReports\\cvr_sitebreakdown_" + guid + ".png");

            //Essential Objects-----------
            EssentialObjectsHelper eo = new EssentialObjectsHelper(invisibleElementIds, Logger);

            //Instanciate
            PdfDocument pdf = new PdfDocument();
            MemoryStream ms = new MemoryStream();

            //Attachments
            //PdfAttachment attachment = new PdfAttachment(Server.MapPath("\\_assets\\images\\no-building-image.png"));
            //pdf.Attachments.Add(attachment);
            //pdf.Portfolio.InitialView = PdfPortfolioView.Detail;
            //byte[]  bt = mDataManager.FigureAndNoteDataMapper.GetFigureBlobImage(DataConstants.AnalysisRange.Weekly, 26, 15, DateTimeHelper.GetLastSunday(DateTime.UtcNow.AddDays(-7)), ".png");
            //PdfAttachment attachment = new PdfAttachment("test1", bt, "");
            //pdf.Attachments.Add(attachment);
            //attachment = new PdfAttachment("test2", bt, "image/png");
            //pdf.Attachments.Add(attachment);
            //pdf.Portfolio.InitialView = PdfPortfolioView.Detail;

            
            //client image
            CustomReportsFileHelper.SaveFile(tempCoverClientLogo, hdnShowClientLogos, hdnCoverClientByteArray);

            //Get charts to write out locally
            CustomReportsFileHelper.SaveFile(tempEnergyChart, divPerformancePage, energyChart); 
            CustomReportsFileHelper.SaveFile(tempMaintenanceChart, divPerformancePage, maintenanceChart);
            CustomReportsFileHelper.SaveFile(tempComfortChart, divPerformancePage, comfortChart);
            CustomReportsFileHelper.SaveFile(tempSiteBreakdownChart, divSitePage, siteBreakdownPieChart);

            html = ReplacePaths(html);


            //User Button Convert
            HtmlToPdfResult htmlToPdfResult = HtmlToPdf.ConvertHtml(html, pdf, eo.options);

            //Set Bookmarks and Anchors
            List<Tuple<string, string>> htmlBookmarkTags = new List<Tuple<string, string>>();
            List<Tuple<string, string>> htmlAnchorTags = new List<Tuple<string, string>>();

            //Order matters        
            htmlBookmarkTags.Add(Tuple.Create(divCustomerPage.ID, lblCustomerPage.ID)); //use label for bookmark for portfolio, the title doenst contain all content.
            htmlAnchorTags.Add(Tuple.Create(lblCustomerPage.ID, divCustomerPage.ID));

            if (divKeyIndicatorsPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divKeyIndicatorsPage.ID, lblKeyIndicatorsPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblKeyIndicatorsPage.ID, divKeyIndicatorsPage.ID));
            }
            if (divItemsRequiringAttentionPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divItemsRequiringAttentionPage.ID, lblItemsRequiringAttentionPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblItemsRequiringAttentionPage.ID, divItemsRequiringAttentionPage.ID));
            }
            if (divPerformancePage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divPerformancePage.ID, lblPerformancePage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblPerformancePage.ID, divPerformancePage.ID));
            }
            if (divServiceDetailsPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divServiceDetailsPage.ID, lblServiceDetailsPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblServiceDetailsPage.ID, divServiceDetailsPage.ID));
            }
            if (divCustomerSatisfactionPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divCustomerSatisfactionPage.ID, lblCustomerSatisfactionPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblCustomerSatisfactionPage.ID, divCustomerSatisfactionPage.ID));
            }

            if (divSitePage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divSitePage.ID, lblSitePage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblSitePage.ID, divSitePage.ID));
            }

           
            //Generate bookmarks and anchors from set
            eo.GenerateBookmarks(htmlToPdfResult, pdf, htmlBookmarkTags);

            if (divContentsPage.Visible)
                eo.GenerateAnchors(htmlToPdfResult, htmlAnchorTags);

            //Save 
            pdf.Save(ms);


            //byte[] pdfBytes = ms.GetBuffer();
            byte[] pdfBytes = ms.ToArray();

            try
            {
                result = new FilePublishService
                (
                    mDataManager.ClientDataMapper.UpdateClientAccountSettings,
                    mDataManager.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    new CustomReportFileGenerator(pdfBytes),
                    Logger
                ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(isSETheme, "$productCustomerValueReport.pdf", true));

            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                LabelHelper.SetLabelMessage(lblError, "Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                return;
            }

            //clean up temp files
            CustomReportsFileHelper.DeleteFile(tempCoverClientLogo, hdnShowClientLogos);

            CustomReportsFileHelper.DeleteFile(tempEnergyChart, divPerformancePage);
            CustomReportsFileHelper.DeleteFile(tempMaintenanceChart, divPerformancePage);
            CustomReportsFileHelper.DeleteFile(tempComfortChart, divPerformancePage);
            CustomReportsFileHelper.DeleteFile(tempSiteBreakdownChart, divSitePage);

            if (result) return;
        }

        private void SetContentAndVisibility(HtmlControl wrapperControl, HtmlContainerControl contentControl, string contentString)
        {     
            if (!String.IsNullOrEmpty(contentString))
            {
                contentControl.InnerText = contentString;
            }
            else{
                wrapperControl.Visible = false;
            }
        }

        private string SetManualArrowSrc(string arrowDirection)
        {
            string s = "";

            switch(EnumHelper.Parse<DataConstants.ReportArrowDirections>(arrowDirection))
            {
                case DataConstants.ReportArrowDirections.NA: 
                    s = "_assets/styles/themes/customReportImages/na";
                    break;
                case DataConstants.ReportArrowDirections.Up:
                    s = "_assets/styles/themes/customReportImages/arrow-up";
                    break;
                case DataConstants.ReportArrowDirections.Right:
                    s = "_assets/styles/themes/customReportImages/arrow-right";
                    break;
                case DataConstants.ReportArrowDirections.Down:
                    s ="_assets/styles/themes/customReportImages/arrow-down";
                    break;
            };

            s += (isSETheme ? "-se.png" : ".png");

            return s;
        }

        #endregion

        public class CVRResult
        {
            public DateTime Date { get; set; }
            public Int32 TotalIncidents { get; set; }
            public double? AveragePriority { get; set; }
            public double? AvoidableCost { get; set; }
        }

        public class WorkOrderPoint
        {
            public DateTime Date { get; set; }
            public double Value { get; set; }
            public string WorkOrder { get; set; }
            public string FormatedText { get; set; }
        }

        public class SiteBreakdownItem
        {
            public string Title { get; set; }
            public double Percent { get; set; }
        }
    }
}