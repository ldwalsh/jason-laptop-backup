﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="DataSourceAdministration.aspx.cs" Inherits="CW.Website.DataSourceAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                     	                  	
            	<h1>Data Source Administration</h1>                        
                <div class="richText">
                    <asp:Literal ID="litAdminDataSourceBody" runat="server"></asp:Literal>                                        
                </div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
                </div> 
                <div class="administrationControls">                                                   
                    <h2>View Data Sources</h2> 
                    <p>
                        <a id="lnkSetFocus" runat="server"></a>
                        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblErrors" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>                                            
                    </p>         
                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                    </asp:Panel>                                                                                                                   
                    <div id="gridTbl">                                        
                           <asp:GridView 
                             ID="gridDataSources"   
                             EnableViewState="true"           
                             runat="server"  
                             DataKeyNames="DSID"  
                             GridLines="None"                                      
                             PageSize="20" PagerSettings-PageButtonCount="20"
                             OnRowCreated="gridDataSources_OnRowCreated" 
                             AllowPaging="true"  OnPageIndexChanging="gridDataSources_PageIndexChanging"
                             AllowSorting="true"  OnSorting="gridDataSources_Sorting"   
                             OnSelectedIndexChanged="gridDataSources_OnSelectedIndexChanged"                                                                                                                              
                             OnDataBound="gridDataSources_OnDataBound"
                             AutoGenerateColumns="false"                                              
                             HeaderStyle-CssClass="tblTitle" 
                             RowStyle-CssClass="tblCol1"
                             AlternatingRowStyle-CssClass="tblCol2"   
                             RowStyle-Wrap="true"                                                                                   
                             > 
                             <Columns>
                                <asp:CommandField ShowSelectButton="true" />                                                                                                                     
                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataSourceName" HeaderText="Data Source Name">  
                                    <ItemTemplate><label title="<%# Eval("DataSourceName") %>"><%# StringHelper.TrimText(Eval("DataSourceName"),35) %></label></ItemTemplate> 
                                </asp:TemplateField>   
                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="IP List">  
                                    <ItemTemplate><label title="<%# Eval("IPList") %>"><%# StringHelper.TrimText(Eval("IPList"),30) %></label></ItemTemplate>                                    
                                </asp:TemplateField>   
                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="ReferenceID"> 
                                    <ItemTemplate><%# StringHelper.TrimText(Eval("ReferenceID"),30) %></ItemTemplate>                                                                          
                                </asp:TemplateField>  
                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Integrated">  
                                    <ItemTemplate><%# Eval("IsIntegrated")%></ItemTemplate>                                                  
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Active">  
                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                </asp:TemplateField>                                                                                                                                                                                         
                             </Columns>        
                         </asp:GridView> 
                                                          
                    </div>                                    
                    <br /><br /> 
                    <div>                                                            
                    <!--SELECT DATA SOURCE DETAILS VIEW -->
                    <asp:DetailsView ID="dtvDataSource" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                        <Fields>
                            <asp:TemplateField  
                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                            ItemStyle-BorderWidth="0" 
                            ItemStyle-BorderColor="White" 
                            ItemStyle-BorderStyle="none"
                            ItemStyle-Width="100%"
                            HeaderStyle-Width="1" 
                            HeaderStyle-BorderWidth="0" 
                            HeaderStyle-BorderColor="White" 
                            HeaderStyle-BorderStyle="none"
                            >
                                <ItemTemplate>
                                   <div>
                                        <h2>Data Source Details</h2>                                                                                                      
                                        <ul class="detailsList">
                                            <%# "<li><strong>Data Source Name: </strong>" + Eval("DataSourceName") + "</li>"%> 
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("IPList"))) ? "" : "<li><strong>IP List: </strong>" + Eval("IPList") + "</li>"%> 
                                            <%# "<li><strong>ReferenceID: </strong>" + Eval("ReferenceID") + "</li>"%> 
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Location"))) ? "" : "<li><strong>Location: </strong><div class='divContentPreWrap'>" + Eval("Location") + "</div></li>"%>                                  
                                            <%# "<li><strong>Time Offset (hours): </strong>" + Eval("TimeOffset") + "</li>"%> 
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Installer"))) ? "" : "<li><strong>Installer: </strong>" + Eval("Installer") + "</li>"%> 
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Description"))) ? "" : "<li><strong>Description: </strong><div class='divContentPreWrap'>" + Eval("Description") + "</div></li>"%> 
                                            <%# "<li><strong>Vendor: </strong>" + Eval("VendorName") + "</li>"%> 
                                            <%# "<li><strong>Vendor Product: </strong>" + Eval("VendorProductName") + "</li>"%>                                                                                 
                                            <%# "<li><strong>Retrieval Method: </strong>" + Eval("RetrievalMethod") + "</li>"%>                                                                                 
                                            <%# Convert.ToBoolean(Eval("IsIntegrated")) == true ? "<li><strong>Integrated: </strong>" + Eval("IsIntegrated") + "</li><li><strong>Integrated Url: </strong>" + Eval("IntegratedUrl") + "</li><li><strong>Integrated Username: </strong>" + Eval("IntegratedUsername") + "</li>" : "<li><strong>Integrated: </strong>" + Eval("IsIntegrated") + "</li>"%>
                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>                                                                                                                                                    	                                                                	                                                                
                                         </ul>
                                    </div>
                                </ItemTemplate>                                             
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>                       
                   </div>                                                                                                                                          
                   </div>                                                                 
                
</asp:Content>


                    
                  
