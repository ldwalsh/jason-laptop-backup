﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Windows.Controls;
using System.Configuration;

using Microsoft.WindowsAzure;
using Microsoft.Samples.ServiceHosting.AspProviders;
//using Microsoft.WindowsAzure.StorageClient;

using CW.Data;
using CW.Business;
using CW.Utility;
using CW.Data.AzureStorage.Models;
using CW.Data.AzureStorage;
using CW.Website._framework;
using CW.Data.AzureStorage.Helpers;
using CW.Data.AzureStorage.DataContexts.Blob;
using CW.Data.AzureStorage.Models.Blob;
using CW.Data.Models;
using CW.Data.Models.Diagnostics;
using CW.Data.Models.PointClass;
using CW.Data.Models.Raw;
using CW.Data.Models.Equipment;
using CW.Data.Models.EquipmentVariable;
using CW.Data.Models.Building;


namespace CW.Website
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DashboardService2" in code, svc and config file together.
    [Obsolete]
    public class DashboardService : IDashboardService
    {
        #region Properties
        private DataManager mDataManager = null;
        private CloudStorageAccount mAccount = null;
        const string noBuildingImage = "no-building-image.png";
        #endregion

        public DashboardService()
        {
            mDataManager = DataManager.Instance;
        }

        #region Client Methods

        //DEPRICATED RAR
        //List<List<object>> IDashboardService.GetAllClientData(string userRoleID)
        //{
        //    List<List<object>> result = new List<List<object>>();
        //    IEnumerable<Client> clients = ClientDataMapper.GetAllClients();

        //    foreach (Client c in clients)
        //    {
        //        List<object> clientData = new List<object>();
        //        clientData.Add(c.ClientName);
        //        clientData.Add(c.CID);
        //        result.Add(clientData);
        //    }
        //    return result;
        //}

        //DEPRICATED RAR
        //List<List<object>> IDashboardService.GetAllProviderAdminClientData(int uid)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //RAR commented out for build
        //    //IEnumerable<ClientDataMapper.GetClientsDataViewFormat> clients = ClientDataMapper.GetAllActiveClientsAssociatedToUserID(Convert.ToInt32(uid));

        //    //foreach (ClientDataMapper.GetClientsDataViewFormat c in clients)
        //    //{
        //    //    List<object> clientData = new List<object>();
        //    //    clientData.Add(c.ClientName);
        //    //    clientData.Add(c.CID);
        //    //    result.Add(clientData);
        //    //}
        //    return result;
        //}

        //DEPRICATED RAR
        //Client IDashboardService.GetClientData(string cid)
        //{
        //    return ClientDataMapper.GetClient(Convert.ToInt32(cid));
        //}

        #endregion

        #region Building Methods


        public IEnumerable<Building> GetBuildingData()
        {
            //NO ACCESS TO SESSION FROM HERE
            //return SiteUser.current.VisibleBuildings;

            //DEPRICATED RAR
            //return BuildingDataMapper.GetAllVisibleBuildingsByClientID(Convert.ToInt32(clientID));

            return null;
        }

        public GetBuildingsData GetFullBuildingData(int buildingID)
        {
            return mDataManager.BuildingDataMapper.GetFullBuildingByID(buildingID).First();
        }

        public List<object> GetBuildingImage(int buildingID, string imageExtension, string buildingName)
        {
            List<object> result = new List<object>();
            mAccount = CloudConfiguration.GetStorageAccount(CommonConstants.ATS_STORAGE_KEY);

			// Depreciated by BuildingDataMapper 
            //BuildingBlobDataContext blobContext = new BuildingBlobDataContext(mAccount, buildingID);
            //BuildingBlob buildingBlob = blobContext.GetBlob(BlobHelper.GenerateBuildingProfileImageBlobName(buildingID, imageExtension));

            BuildingBlob buildingBlob = mDataManager.BuildingDataMapper.GetBuildingImage(buildingID, imageExtension);


            if (buildingBlob == null)
            {
                result.Add(ConfigurationManager.AppSettings["SilverlightImageAssetPath"] + noBuildingImage);
            }
            else
            {

                result.Add(buildingBlob.Image);
            }

            //always add the actual buidling name
            result.Add(buildingName);

            return result;
        }

        #endregion

        #region Equipment Methods

        public IEnumerable<GetEquipmentData> GetFullEquipmentData(int equipmentID)
        {
            return mDataManager.EquipmentDataMapper.GetFullEquipmentByEID(equipmentID);
        }

        public IEnumerable<GetEquipmentEquipmentVariableData> GetAllEquipmentVariablesByEID(int equipmentID)
        {
            return mDataManager.EquipmentVariableDataMapper.GetAllEquipmentVariablesWithDataByEID(equipmentID);
        }

        public List<List<object>> GetAllEquipmentClassData()
        {
            List<List<object>> result = new List<List<object>>();
            IEnumerable<EquipmentClass> ec = mDataManager.EquipmentClassDataMapper.GetAllEquipmentClasses();

            foreach (EquipmentClass c in ec)
            {
                List<object> classes = new List<object>();
                classes.Add(c.EquipmentClassName);
                classes.Add(c.EquipmentClassID);
                result.Add(classes);
            }
            return result;
        }

        public List<List<object>> GetEquipmentClassInBuildingData(int buildingID, int equimentClassID)
        {
            List<List<object>> result = new List<List<object>>();
            IEnumerable<Equipment> equiment = mDataManager.EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentClassID(buildingID, equimentClassID);

            foreach (Equipment e in equiment)
            {
                List<object> i = new List<object>();

                i.Add(e.EquipmentName);
                i.Add(e.EID);
                result.Add(i);
            }

            return result;
        }

        #endregion

        #region Point Methods

        public List<EngUnit> GetPointEngUnit(List<int> PIDs)
        {
            List<EngUnit> eus = new List<EngUnit>();

            foreach (int PID in PIDs)
            {
                Point p = mDataManager.PointDataMapper.GetPointByPID(PID);
                PointType pt = mDataManager.PointTypeDataMapper.GetPointType(p.PointTypeID);
                PointClass pc = mDataManager.PointClassDataMapper.GetPointClass(pt.PointClassID);
                EngUnit eu = mDataManager.EngineeringUnitsDataMapper.GetEngineeringUnit(pc.EngUnitID);
                eus.Add(eu);
            }

            return eus;
        }

        public List<string[]> GetPointInfo(List<int> PIDs)
        {
            List<string[]> points = new List<string[]>();

            //foreach (int PID in PIDs)
            //{
            //    string[] point = new string[4];
            //    PointTable p = PointDataMapper.GetPointByPID(PID);
            //    point[0] = p.PointName;
            //    point[1] = EquipmentDataMapper.GetEquipmentNameByEID(EquipmentPointsDataMapper.GetPartialEquipmentPointByPID(p.PID).EID);

            //    PointType pt = PointTypeDataMapper.GetPointType(p.PointTypeID);
            //    PointClass pc = PointClassDataMapper.GetPointClass(pt.PointClassID);
            //    EngUnit eu = EngineeringUnitsDataMapper.GetEngineeringUnit(pc.EngUnitID);
            //    point[2] = eu.EngUnitID.ToString();
            //    point[3] = eu.EngUnits;

            //    points.Add(point);
            //}

            return points;
        }

        public List<string[]> GetPointInfoByEID(int eid)
        {
            IEnumerable<Point> eqPoints = mDataManager.PointDataMapper.GetAllPointsByEID(eid);
            List<string[]> points = new List<string[]>();

            IEnumerable<PointType> pointTypes = mDataManager.PointTypeDataMapper.GetAllPointTypes();
            IEnumerable<GetPointClassData> pointClasses = mDataManager.PointClassDataMapper.GetAllFullPointClasses();

            foreach (Point p in eqPoints)
            {
                string[] point = new string[5];
                point[0] = p.PointName;

                PointType pt = pointTypes.Where(t => t.PointTypeID == p.PointTypeID).First();
                GetPointClassData pc = pointClasses.Where(c => c.PointClassID == pt.PointClassID).First();
                point[1] = pc.PointClassName;
                point[2] = pc.EngUnitID.ToString();
                point[3] = pc.EngUnits;
                point[4] = pt.PointTypeName;

                points.Add(point);
            }

            return points;
        }

        public IEnumerable<Point> GetAllPointsByEID(int eid)
        {
            return mDataManager.PointDataMapper.GetAllVisiblePointsByEID(eid);
        }

        #endregion

        #region Performance Methods

        /// <summary>
        /// GETS DIAGNOSTICS RESULTS. CURRENTLY NOT USED IN DASHBOARD
        /// </summary>
        /// <param name="buildingID"></param>
        /// <param name="uid"></param>
        /// <param name="roleID"></param>
        /// <param name="cid"></param>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        public List<List<double>> GetPerformance(int buildingID, int uid, string roleID, int cid, DateTime currentDate)
        {
            List<double> daily = new List<double>();
            List<double> weekly = new List<double>();
            List<double> monthly = new List<double>();
            //is kgs admin or higher

            //get building data for the last day                                    
            IEnumerable<DiagnosticsResult> d = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(CommonDataConstants.AnalysisRange.Daily, currentDate.Date, currentDate.AddDays(1).Date, uid, cid, buildingID, null, null, null);
            IEnumerable<DiagnosticsResult> w = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(CommonDataConstants.AnalysisRange.Weekly, DateTimeHelper.GetLastSunday(currentDate.Date), currentDate.AddDays(7).Date, uid, cid, buildingID, null, null, null);
            IEnumerable<DiagnosticsResult> m = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(CommonDataConstants.AnalysisRange.Monthly, DateTimeHelper.GetFirstOfMonth(currentDate.Date), currentDate.AddMonths(1).Date, uid, cid, buildingID, null, null, null);
            daily.Add(d.Count());

            double dcost = 0;
            double denergy = 0;
            double dcomfort = 0;
            double dmaintenance = 0;
            foreach (DiagnosticsResult i in d)
            {
                dcost += i.CostSavings;
                denergy += i.EnergyPriority;
                dcomfort += i.ComfortPriority;
                dmaintenance += i.MaintenancePriority;
            }

            daily.Add(dcost);
            daily.Add(denergy);
            daily.Add(dcomfort);
            daily.Add(dmaintenance);

            weekly.Add(w.Count());

            double wcost = 0;
            double wenergy = 0;
            double wcomfort = 0;
            double wmaintenance = 0;
            foreach (DiagnosticsResult i in w)
            {
                wcost += i.CostSavings;
                wenergy += i.EnergyPriority;
                wcomfort += i.ComfortPriority;
                wmaintenance += i.MaintenancePriority;
            }

            weekly.Add(wcost);
            weekly.Add(wenergy);
            weekly.Add(wcomfort);
            weekly.Add(wmaintenance);

            monthly.Add(m.Count());

            double mcost = 0;
            double menergy = 0;
            double mcomfort = 0;
            double mmaintenance = 0;
            foreach (DiagnosticsResult i in m)
            {
                mcost += i.CostSavings;
                menergy += i.EnergyPriority;
                mcomfort += i.ComfortPriority;
                mmaintenance += i.MaintenancePriority;
            }

            monthly.Add(mcost);
            monthly.Add(menergy);
            monthly.Add(mcomfort);
            monthly.Add(mmaintenance);

            List<List<double>> result = new List<List<double>>();
            result.Add(daily);
            result.Add(weekly);
            result.Add(monthly);

            return result;
        }

        public IEnumerable<DiagnosticsResult> GetBuildingPerformance(int bid, int uid, int cid, DateTime startDate, DateTime endDate, CommonDataConstants.AnalysisRange range, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        {
            //List<List<object>> result = new List<List<object>>();

            //get for the last 90 days
            //DateTime startDate = currentDate.AddDays(-90).Date;

            IEnumerable<DiagnosticsResult> diagnosticsResults = mDataManager.DiagnosticsDataMapper.GetDashboardPerformanceByBuilding(range, startDate, endDate, uid, cid, bid, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);

            //while (startDate.CompareTo(currentDate.Date) != 0)
            //{
            //    List<object> daily = new List<object>();
            //    //IEnumerable<sp_AID_EID_DailyResult> d = BuildingDataMapper.GetDaily(buildingID, userID, isKGSAdminOrHigher, RoleHelper.IsClientSuperAdminOrFullAdminOrFullUser(roleID), clientID, start.Date, start.Date.AddDays(1).Date);
            //    IEnumerable<Priority> d = dailys.Where(r => r.StartDate.Equals(startDate));
            //    daily.Add(d.Count());

            //    double cost = 0;
            //    double energy = 0;
            //    double comfort = 0;
            //    double maintenance = 0;
            //    foreach (Priority i in d)
            //    {
            //        cost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
            //        energy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
            //        comfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
            //        maintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;
            //    }

            //    daily.Add(cost);
            //    daily.Add(energy);
            //    daily.Add(comfort);
            //    daily.Add(maintenance);
            //    daily.Add(startDate.Date);

            //    result.Add(daily);

            //    startDate = startDate.AddDays(1);
            //}

            return diagnosticsResults;
        }
        //public IEnumerable<Priority> GetPast52WeeksBuildingPerformance(int bid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //get for the last 52 weeks
        //    DateTime startDate = currentDate.AddDays(52 * 7).Date;

        //    IEnumerable<Priority> weeklies = mDataManager.DiagnosticsDataMapper.GetDashboardPerformanceByBuilding(CommonDataConstants.AnalysisRange.Weekly, startDate, currentDate.Date, uid, cid, bid, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);

        //    //while (startDate.CompareTo(currentDate.Date) != 0)
        //    //{
        //    //    List<object> weekly = new List<object>();
        //    //    //IEnumerable<sp_AID_EID_DailyResult> d = BuildingDataMapper.GetDaily(buildingID, userID, isKGSAdminOrHigher, RoleHelper.IsClientSuperAdminOrFullAdminOrFullUser(roleID), clientID, start.Date, start.Date.AddDays(1).Date);
        //    //    IEnumerable<Priority> w = weeklies.Where(r => r.StartDate.Equals(startDate));
        //    //    weekly.Add(w.Count());

        //    //    double cost = 0;
        //    //    double energy = 0;
        //    //    double comfort = 0;
        //    //    double maintenance = 0;
        //    //    foreach (Priority i in w)
        //    //    {
        //    //        cost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
        //    //        energy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
        //    //        comfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
        //    //        maintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;

        //    //    }

        //    //    weekly.Add(cost);
        //    //    weekly.Add(energy);
        //    //    weekly.Add(comfort);
        //    //    weekly.Add(maintenance);
        //    //    weekly.Add(startDate.Date);

        //    //    result.Add(weekly);

        //    //    startDate = startDate.AddDays(1);
        //    //}

        //    return weeklies;
        //}
        //public IEnumerable<Priority> GetPast36MonthsBuildingPerformance(int bid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //get for the last 36 months
        //    DateTime startDate = Utility.DateTimeHelper.GetFirstOfMonth(currentDate.AddMonths(36).Date);

        //    IEnumerable<Priority> monthlies = mDataManager.DiagnosticsDataMapper.GetDashboardPerformanceByBuilding(CommonDataConstants.AnalysisRange.Monthly, startDate, currentDate.Date, uid, cid, bid, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);

        //    return monthlies;
        //}

        //public List<List<object>> GetPastMonthStatistics(int bid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //get for the last 30 days
        //    DateTime startDate = currentDate.AddDays(-30).Date;

        //    IEnumerable<Priority> dailys = mDataManager.DiagnosticsDataMapper.GetDashboardResultsFiltered(CommonDataConstants.AnalysisRange.Daily, startDate, currentDate.Date, uid, cid, bid, null, null, null, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);

        //    while (startDate.CompareTo(currentDate.Date) != 0)
        //    {
        //        List<object> daily = new List<object>();
        //        //IEnumerable<sp_AID_EID_DailyResult> d = BuildingDataMapper.GetDaily(buildingID, userID, isKGSAdminOrHigher, RoleHelper.IsClientSuperAdminOrFullAdminOrFullUser(roleID), clientID, start.Date, start.Date.AddDays(1).Date);
        //        IEnumerable<Priority> d = dailys.Where(r => r.StartDate.Equals(startDate));
        //        daily.Add(d.Count());

        //        double dcost = 0;
        //        double denergy = 0;
        //        double dcomfort = 0;
        //        double dmaintenance = 0;
        //        foreach (Priority i in d)
        //        {
        //            dcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
        //            denergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
        //            dcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
        //            dmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;

        //        }

        //        daily.Add(dcost);
        //        daily.Add(denergy);
        //        daily.Add(dcomfort);
        //        daily.Add(dmaintenance);
        //        daily.Add(startDate.Date);

        //        result.Add(daily);

        //        startDate = startDate.AddDays(1);
        //    }

        //    return result;
        //}

        //public List<List<object>> GetPastMonthEquipmentClassStatistics(int bid, int uid, string roleID, int cid, DateTime currentDate, int classID, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //get for the last 30 days
        //    DateTime startDate = currentDate.AddDays(-30).Date;

        //    IEnumerable<Priority> dailys = mDataManager.DiagnosticsDataMapper.GetDashboardResultsFiltered(CommonDataConstants.AnalysisRange.Daily, currentDate.AddDays(-30).Date, currentDate.Date, uid, cid, bid, null, null, null, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);
        //    IEnumerable<Equipment> validClass = EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentClassID(bid, classID);

        //    while (startDate.CompareTo(currentDate.Date) != 0)
        //    {
        //        List<object> daily = new List<object>();
        //        //IEnumerable<sp_AID_EID_DailyResult> d = BuildingDataMapper.GetDaily(buildingID, userID, isKGSAdminOrHigher, RoleHelper.IsClientSuperAdminOrFullAdminOrFullUser(roleID), clientID, start.Date, start.Date.AddDays(1).Date);
        //        IEnumerable<Priority> d = dailys.Where(r => r.StartDate.Equals(startDate));

        //        double dcount = 0;
        //        double dcost = 0;
        //        double denergy = 0;
        //        double dcomfort = 0;
        //        double dmaintenance = 0;
        //        foreach (Priority i in d)
        //        {
        //            if (validClass.Where(e => e.EID == i.EID).Any())
        //            {
        //                dcount++;
        //                dcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
        //                denergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
        //                dcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
        //                dmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;
        //            }

        //        }

        //        daily.Add(dcount);
        //        daily.Add(dcost);
        //        daily.Add(denergy);
        //        daily.Add(dcomfort);
        //        daily.Add(dmaintenance);
        //        daily.Add(startDate.Date);

        //        result.Add(daily);

        //        startDate = startDate.AddDays(1);
        //    }

        //    return result;
        //}

        //public List<List<object>> GetPastMonthEquipmentStatistics(int bid, int eid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //get for the last 30 days
        //    DateTime startDate = currentDate.AddDays(-30).Date;

        //    IEnumerable<Priority> dailys = mDataManager.DiagnosticsDataMapper.GetDashboardResultsFiltered(CommonDataConstants.AnalysisRange.Daily, startDate, currentDate.Date, uid, cid, bid, null, eid, null, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);

        //    while (startDate.CompareTo(currentDate.Date) != 0)
        //    {
        //        List<object> daily = new List<object>();
        //        IEnumerable<Priority> d = dailys.Where(r => r.StartDate.Equals(startDate.Date));//EquipmentDataMapper.GetDaily(EID, userID, isKGSAdminOrHigher, RoleHelper.IsClientSuperAdminOrFullAdminOrFullUser(roleID), clientID, start.Date, start.Date.AddDays(1).Date);
        //        daily.Add(d.Count());

        //        double dcost = 0;
        //        double denergy = 0;
        //        double dcomfort = 0;
        //        double dmaintenance = 0;
        //        foreach (Priority i in d)
        //        {
        //            dcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
        //            denergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
        //            dcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
        //            dmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;

        //        }

        //        daily.Add(dcost);
        //        daily.Add(denergy);
        //        daily.Add(dcomfort);
        //        daily.Add(dmaintenance);
        //        daily.Add(startDate.Date);

        //        result.Add(daily);

        //        startDate = startDate.AddDays(1);
        //    }

        //    return result;
        //}

        //public List<List<object>> GetPastYearStatistics(int bid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //get for the past year
        //    DateTime startDate = DateTimeHelper.GetFirstOfMonth(currentDate.AddYears(-1).Date);

        //    while (startDate.CompareTo(currentDate.Date) <= 0)
        //    {
        //        List<object> monthly = new List<object>();

        //        IEnumerable<Priority> m = mDataManager.DiagnosticsDataMapper.GetDashboardResultsFiltered(CommonDataConstants.AnalysisRange.Monthly, startDate.Date, startDate.Date.AddMonths(1).Date, uid, cid, bid, null, null, null, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);
        //        monthly.Add(m.Count());

        //        double mcost = 0;
        //        double menergy = 0;
        //        double mcomfort = 0;
        //        double mmaintenance = 0;

        //        foreach (Priority i in m)
        //        {
        //            mcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
        //            menergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
        //            mcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
        //            mmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;

        //        }

        //        monthly.Add(mcost);
        //        monthly.Add(menergy);
        //        monthly.Add(mcomfort);
        //        monthly.Add(mmaintenance);
        //        monthly.Add(DateTimeHelper.GetFirstOfMonth(startDate.Date));

        //        result.Add(monthly);

        //        startDate = startDate.AddMonths(1);
        //    }

        //    return result;
        //}

        //public List<List<object>> GetPastYearEquipmentClassStatistics(int bid, int uid, string roleID, int cid, DateTime currentDate, int classID, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //get for the past year
        //    DateTime startDate = DateTimeHelper.GetFirstOfMonth(currentDate.AddYears(-1).Date);

        //    IEnumerable<Equipment> validClass = EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentClassID(bid, classID);

        //    while (startDate.CompareTo(startDate.Date) <= 0)
        //    {
        //        List<object> monthly = new List<object>();

        //        IEnumerable<Priority> m = mDataManager.DiagnosticsDataMapper.GetDashboardResultsFiltered(CommonDataConstants.AnalysisRange.Monthly, startDate.Date, startDate.Date.AddMonths(1).Date, uid, cid, bid, null, null, null, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);

        //        double mcount = 0;
        //        double mcost = 0;
        //        double menergy = 0;
        //        double mcomfort = 0;
        //        double mmaintenance = 0;

        //        foreach (Priority i in m)
        //        {
        //            if (validClass.Where(e => e.EID == i.EID).Any())
        //            {
        //                mcount++;
        //                mcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
        //                menergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
        //                mcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
        //                mmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;
        //            }

        //        }

        //        monthly.Add(mcount);
        //        monthly.Add(mcost);
        //        monthly.Add(menergy);
        //        monthly.Add(mcomfort);
        //        monthly.Add(mmaintenance);
        //        monthly.Add(DateTimeHelper.GetFirstOfMonth(startDate.Date));

        //        result.Add(monthly);

        //        startDate = startDate.AddMonths(1);
        //    }

        //    return result;
        //}

        //public List<List<object>> GetPastYearEquipmentStatistics(int bid, int eid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //get for the past year
        //    DateTime startDate = DateTimeHelper.GetFirstOfMonth(currentDate.AddYears(-1).Date);

        //    while (startDate.CompareTo(startDate.Date) <= 0)
        //    {
        //        List<object> monthly = new List<object>();

        //        IEnumerable<Priority> m = mDataManager.DiagnosticsDataMapper.GetDashboardResultsFiltered(CommonDataConstants.AnalysisRange.Monthly, startDate.Date, startDate.Date.AddMonths(1).Date, uid, cid, bid, null, eid, null, isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser);

        //        double mfaults = 0;
        //        double mcost = 0;
        //        double menergy = 0;
        //        double mcomfort = 0;
        //        double mmaintenance = 0;
        //        foreach (Priority i in m)
        //        {
        //            mfaults++;
        //            mcost += i.CostSavings;
        //            menergy += i.EnergyPriority;
        //            mcomfort += i.ComfortPriority;
        //            mmaintenance += i.MaintenancePriority;

        //        }

        //        monthly.Add(mfaults);
        //        monthly.Add(mcost);
        //        monthly.Add(menergy);
        //        monthly.Add(mcomfort);
        //        monthly.Add(mmaintenance);
        //        monthly.Add(DateTimeHelper.GetFirstOfMonth(startDate.Date));

        //        result.Add(monthly);

        //        startDate = startDate.AddMonths(1);
        //    }

        //    return result;
        //}

        #endregion

        #region Statictics Methods

        #endregion

        #region Live Data Methods

        public IEnumerable<GetRawPlotData> GetLiveData(int userID, int buildingID)
        {
            List<GetRawPlotData> datas = new List<GetRawPlotData>();

            int[] gauges;

            //get gauges
            gauges = mDataManager.UserDataMapper.GetAllUserGauges(userID);

            //for each gauge                
            foreach (int pid in gauges)
            {
                //get point data
                //EquipmentPointsDataMapper.GetEquipmentPointsData point = EquipmentPointsDataMapper.GetPartialEquipmentPointByPID(pid);

                if (mDataManager.PointDataMapper.IsPointAssociatedWithBuilding(pid, buildingID))
                {
                    //get last live raw data record for point
                    GetRawPlotData liveDataPointRecord = mDataManager.RawDataMapper.GetConvertedLastRawPlotDataRecordForPID(pid);

                    if (liveDataPointRecord != null)
                    {
                        datas.Add(liveDataPointRecord);
                    }
                    else
                    {
                        GetRawPlotData emptyData = new GetRawPlotData();
                        emptyData.PID = pid;
                        datas.Add(emptyData);
                    }
                }
            }

            return datas;
        }

        public IEnumerable<GetRawPlotData> GetLiveDataForPIDs(List<int> pids)
        {
            IEnumerable<GetRawPlotData> result = mDataManager.RawDataMapper.GetConvertedCurrentDaysRawPlotDataForPIDs(pids);
            return result;
        }

        public List<GetRawPlotData> UpdateLiveDataForPIDs(List<int> pids)
        {
            List<GetRawPlotData> updates = new List<GetRawPlotData>();

            foreach (int pid in pids)
            {
                updates.Add((GetRawPlotData)mDataManager.RawDataMapper.GetConvertedLastRawPlotDataRecordForPID(pid));
            }

            return updates;

        }

        public IEnumerable<GetRawPlotData> GetPast30DataForPIDs(IEnumerable<int> pids, DateTime startDate)
        {
            IEnumerable<GetRawPlotData> data = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids.ToList(), startDate.AddDays(-7).Date, startDate.Date);

            return data;
        }

        #endregion

        #region Global Setting Methods

        //DEPRICATED DASHBOARD OVERHAUL
        //string GetDashboardContent()
        //{
        //    return GlobalDataMapper.GetGlobalSettings().DashboardBody;
        //}

        #endregion

    }
}
