﻿using CW.Data.Models.APISubscriber;
using CW.RESTAdapter.MAPIM;
using CW.Website._controls.admin;
using CW.Website._controls.admin.APISubscribers;
using CW.Website._controls.admin.APISubscribers.Models;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.UI;

namespace CW.Website
{
    public partial class SystemAPISubscriberAdministration : AdminSitePageTemp
    {
        private string DeploymentId { get; set; }
        public SystemAPISubscriberAdministration() : base()
        {
            NotifyPageOnChangeEvent+= new OnNotifyPageOnChange(OnControlChange);
            GetCachedObject += new OnGetCachedObject(OnControlGetCachedObject);
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }

        /// <summary>
        /// This will be called whenever any control deems that the product collection has changed so that it can be refreshed from DA
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnControlChange(Enum message = null)
        {
            if (null != message)
            {
                TabMessages tm = (TabMessages)Enum.Parse(typeof(TabMessages), message.ToString());
                switch (tm)
                {
                    case TabMessages.AddAPI:
                    case TabMessages.AddProduct:
                    case TabMessages.AddAPISubscriber:
                    case TabMessages.AddAPIContainer:
                    case TabMessages.DeleteAPI:
                    case TabMessages.DeleteProduct:
                    case TabMessages.DeleteAPIContainer:
                    case TabMessages.DeleteAPISubscriber:
                    case TabMessages.EditAPI:
                    case TabMessages.EditProduct:
                    case TabMessages.EditAPIContainer:
                    case TabMessages.EditAPISubscriber:
                    {
                        Session.Remove(APIHelper.API_PRODUCTS_ID);
                        Session.Remove(APIHelper.API_CONTAINERS_ID);
                        Session.Remove(APIHelper.API_SUBSCRIBERS);
                        break;
                    }
                }
            }
        }
        private object OnControlGetCachedObject(string objectId)
        {
            if (string.IsNullOrEmpty(APIHelper.ServiceRootEndPoint))
            {
                APIHelper.ServiceRootEndPoint = (new RESTPublicServiceRootEndPoint(DeploymentId, ConfigurationManager.AppSettings["CertMgmtFootprint"])).GetServiceEndPoint();
            }
            object obj = null;
            switch(objectId)
            {
                case APIHelper.API_PRODUCTS_ID:
                {
                    obj = Session[objectId];
                    if (null == obj)
                    {
                        obj = Session[objectId] = APIHelper.GetAllProducts(DeploymentId, OnControlGetCachedObject(APIHelper.API_SUBSCRIBERS) as IEnumerable<GetAPISubscriberData>).ToList();
                    }
                    break;
                }
                case APIHelper.API_CONTAINERS_ID:
                {
                    obj = Session[objectId];
                    if (null == obj)
                    {
                        obj = Session[objectId] = APIHelper.GetAllApiContainers(DeploymentId, true).ToList();
                    }
                    break;
                }
                case APIHelper.API_SUBSCRIBERS:
                    {
                        obj = Session[objectId];
                        if (null == obj)
                        {
                            obj = Session[objectId] = APIHelper.GetAllSubscribers(DeploymentId, DataMgr.APISubscriberDataMapper.GetOrgsForSubscribers).ToList();
                        }
                        break;
                    }
            }
            return (obj);
        }
        private void Page_FirstLoad()
        {
            TabStateMonitor.ChangeState(new Enum[] { SystemAPISubscriberAdministration.TabMessages.Init });
        }

        static public async Task<string> SaveUserAsync(CW.Data.APISubscriber apiSubscriber, string firstName, string lastName, string email, string password, string note, IDictionary<string, string> selectedProducts)
        {
            string errors = "";
            bool isNew = string.IsNullOrEmpty(apiSubscriber.SubscriberID);
            RESTAPISubscriber restSubscriber = new RESTAPISubscriber(ConfigurationManager.AppSettings["DeploymentID"]);
            if (isNew)
            {
                string subscriberId = await restSubscriber.CreateSubscriberAsync(firstName, lastName, email, password, note).ConfigureAwait(false);
                if (string.IsNullOrEmpty(subscriberId))
                {
                    errors = restSubscriber.Error;
                }
                else
                {
                    apiSubscriber.SubscriberID = subscriberId;
                }
            }
            else
            {
                bool ok = await restSubscriber.UpdateSubscriberAsync(apiSubscriber.SubscriberID, firstName, lastName, email, password, note).ConfigureAwait(false);
                if (!ok)
                {
                    errors = restSubscriber.Error;
                }
            }

            if (string.IsNullOrEmpty(errors))
            {
                if (selectedProducts.Count > 0 || !isNew)
                {
                    // Now subscribe to each product
                    restSubscriber.ClearProperties();
                    string[] productIds = selectedProducts.Select(product => product.Key).ToArray();
                    bool ok = restSubscriber.UpdateSubscriptionForSubscriberAsync(apiSubscriber.SubscriberID, productIds).Result;
                    if (!ok)
                    {
                        errors = "User was " + (isNew ? "created" : "updated") + ". However updates to subscriptions failed. Please see errors below<ul>";
                        if (!string.IsNullOrEmpty(restSubscriber.Error))
                        {
                            errors += "<li>" + restSubscriber.Error + "</li>";
                        }
                        else
                        {
                            IDictionary<string, string> failures = restSubscriber.Failures;
                            if (null != failures)
                            {
                                failures.ToList().ForEach(failure =>
                                {
                                    string prodId = failure.Key;
                                    KeyValuePair<string,string> product = selectedProducts.FirstOrDefault(p => p.Key == prodId);
                                    if (!product.Equals(default(KeyValuePair<string, string>)))
                                    {
                                        string productName = product.Value;
                                        errors += "<li>" + productName + ": " + failure.Value + "</li>";
                                    }
                                });
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(errors))
                    {
                        errors += "</ul>";
                    }
                }
            }
            return (errors);
        }
        public enum TabMessages
        {
            Init,
            AddAPISubscriber,
            EditAPISubscriber,
            DeleteAPISubscriber,
            AddProduct,
            EditProduct,
            DeleteProduct,
            AddAPIContainer,
            EditAPIContainer,
            DeleteAPIContainer,
            AddAPI,
            EditAPI,
            DeleteAPI
        }

        #region properties

        #region overrides

        protected override String DefaultControl { get { return typeof(ViewProducts).Name; } }

        public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.KGS; } }

        #endregion

        #endregion
    }
}