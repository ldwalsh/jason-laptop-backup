﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Projects.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="Projects.aspx.cs" Inherits="CW.Website.Projects" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="CW.Website" Namespace="CW.Website._extensions" TagPrefix="extensions" %>
<%@ Import Namespace="CW.Data.Models.Projects" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                     	                  	
            	<div class="projectsModuleIcon">
                    <h1>Projects</h1>     
                </div>                   
                <div class="richText">
                    <asp:Literal ID="litProjectsBody" runat="server"></asp:Literal>
                </div>                               
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
               </div> 
               <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Projects"></telerik:RadTab>
                            <telerik:RadTab Text="Add Project"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">  
                                <h2>View Projects</h2> 

                                <asp:UpdatePanel ID="updatePanelNestedTop" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                <fieldset class="moduleSearch">
                                <legend>Search Criteria <asp:HyperLink ID="lnkSearch" runat="server" CssClass="searchToggle"><asp:Label ID="lblSearch" runat="server"></asp:Label></asp:HyperLink>&nbsp;</legend>                    
                                <ajaxToolkit:CollapsiblePanelExtender ID="detailsCollapsiblePanelExtender" runat="Server"
                                            TargetControlID="pnlCriteria"
                                            CollapsedSize="0"                                
                                            ExpandControlID="lnkSearch"
                                            CollapseControlID="lnkSearch"
                                            AutoCollapse="false"
                                            AutoExpand="false"
                                            ScrollContents="false"
                                            ExpandDirection="Vertical"
                                            TextLabelID="lblSearch"
                                            CollapsedText="+"
                                            ExpandedText="-"                              
                                            />


                                <asp:Panel ID="pnlCriteria" runat="server" DefaultButton="btnGenerateData" CssClass="criteria">
                    
                                    <!--View By-->                        
                                    <div class="divCriteria"> 
                                            <div>
                                                <h3>View By</h3>     
                                            </div> 
                                            <div class="divFormWidest">                     
                                                <asp:RadioButtonList ID="rblViewBy" CssClass="radio" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="rblViewBy_OnSelectedIndexChanged" runat="server">
                                                    <asp:ListItem Value="Building" Text="Building"></asp:ListItem>
                                                    <asp:ListItem Value="EquipmentClass" Text="Equipment Class"></asp:ListItem>
                                                    <asp:ListItem Value="Equipment" Text="Equipment"></asp:ListItem>
                                                    <asp:ListItem Value="Analysis" Text="Analysis"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                     </div> 

                                     <div class="divCriteria divViewByCol2">
                                        <!--Building-->     
                                        <asp:Panel ID="pnlBuildings" runat="server">                               
                                                <div class="dropDownPanel">   
                                                    <label>*Select Building:</label> 
                                                    <br />  
                                                    <extensions:DropDownExtension ID="ddlBuildings" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                        <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                                    </extensions:DropDownExtension> 
                                                </div>                            
                                                <asp:RequiredFieldValidator ID="buildingRequiredValidator" runat="server"
                                                    ErrorMessage="Building is a required field." 
                                                    ControlToValidate="ddlBuildings"  
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    InitialValue="-1"
                                                    ValidationGroup="Projects">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="buildingRequiredValidatorExtender" runat="server"
                                                    BehaviorID="buildingRequiredValidatorExtender" 
                                                    TargetControlID="buildingRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                        </asp:Panel>                                          
               
                                        <!--EquipmentClass-->
                                        <asp:Panel ID="pnlEquipmentClass" runat="server">
                                            <div class="dropDownPanel">
                                                <label>*Select Equipment Class:</label>
                                                <br />
                                                <asp:DropDownList ID="ddlEquipmentClasses" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClasses_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                    <asp:ListItem Value="-1">Select one...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <asp:RequiredFieldValidator ID="equipmentClassRequiredValidator" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlEquipmentClasses" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Projects"></asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassRequiredValidatorExtender" runat="server" BehaviorID="equipmentClassRequiredValidatorExtender" TargetControlID="equipmentClassRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175"></ajaxToolkit:ValidatorCalloutExtender>
                                        </asp:Panel>    

                                        <!--Equipment-->
                                        <asp:Panel ID="pnlEquipment" runat="server">
                                                <div class="dropDownPanel">   
                                                    <label>*Select Equipment:</label>
                                                    <br />  
                                                    <extensions:DropDownExtension ID="ddlEquipment" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                        <asp:ListItem Value="-1" >Select building first...</asp:ListItem>                           
                                                    </extensions:DropDownExtension>
                                                </div>                                                                              
                                                <asp:RequiredFieldValidator ID="equipmentRequiredValidator" runat="server"
                                                    ErrorMessage="Equipment is a required field." 
                                                    ControlToValidate="ddlEquipment"  
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    InitialValue="-1"
                                                    ValidationGroup="Projects">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="equipmentRequiredValidatorExtender" runat="server"
                                                    BehaviorID="equipmentRequiredValidatorExtender" 
                                                    TargetControlID="equipmentRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                          
                                        </asp:Panel>
               
                                        <!--Analysis-->
                                        <asp:Panel ID="pnlAnalysis" runat="server">                                    
                                                <div class="dropDownPanel">   
                                                    <label>*Select Analysis:</label>
                                                    <br />
                                                    <extensions:DropDownExtension ID="ddlAnalysis" CssClass="dropdownNarrow" AppendDataBoundItems="true" AutoPostBack="true"  runat="server">    
                                                        <asp:ListItem Value="-1" >Select equipment first...</asp:ListItem>                           
                                                    </extensions:DropDownExtension>
                                                </div>  
                                                <asp:RequiredFieldValidator ID="analysisRequiredValidator" runat="server"
                                                    ErrorMessage="Analysis is a required field." 
                                                    ControlToValidate="ddlAnalysis"  
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    InitialValue="-1"
                                                    ValidationGroup="Projects">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="analysisRequiredValidatorExtender" runat="server"                                        
                                                    BehaviorID="analysisRequiredValidatorExtender" 
                                                    TargetControlID="analysisRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                          
                                        </asp:Panel>  
                                    </div>

                                     <div class="divCriteria"> 
                                     <!--Use bottomright popup validator positions for this column because the browser window will not always have space for the right most positioning. 
                                        Since the button is below, the browser window will always have room for the botom validator positioning.-->                                                                                                                            
                                         <div>
                                	        <h3>Project Start</h3>     
                                         </div>
                                         <div class="divFormWidest">    
                                                <label>*From:</label>
                                                <br />
                                                <telerik:RadDatePicker ID="txtStartDateFrom" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>
                                                <br />   
                                                <asp:RequiredFieldValidator ID="startFromDateRequiredValidator" runat="server"
                                                    CssClass="errorMessage" 
                                                    ErrorMessage="Date is a required field."
                                                    ControlToValidate="txtStartDateFrom"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="Projects">
                                                    </asp:RequiredFieldValidator>      
                                                <ajaxToolkit:ValidatorCalloutExtender ID="startFromDateRequiredValidatorExtender" runat="server"                                        
                                                    BehaviorID="startFromDateRequiredValidatorExtender" 
                                                    TargetControlID="startFromDateRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight" 
                                                    PopupPosition="Right"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                      
                                                <br />
                                                <label>*To:</label>
                                                <br />
                                                <telerik:RadDatePicker ID="txtStartDateTo" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                    
                                                <br />     
                                                <asp:RequiredFieldValidator ID="startToDateRequiredValidator" runat="server"
                                                    CssClass="errorMessage" 
                                                    ErrorMessage="Date is a required field."
                                                    ControlToValidate="txtStartDateTo"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="Projects">
                                                    </asp:RequiredFieldValidator> 
                                                <ajaxToolkit:ValidatorCalloutExtender ID="startToDateRequiredValidatorExtender" runat="server"                                        
                                                    BehaviorID="startToDateRequiredValidatorExtender" 
                                                    TargetControlID="startToDateRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    PopupPosition="Right"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                       
                                                <asp:CompareValidator ID="startDateCompareValidator" Display="None" runat="server"
                                                    CssClass="errorMessage"
                                                    ErrorMessage="Invalid range."
                                                    ControlToValidate="txtStartDateTo"
                                                    ControlToCompare="txtStartDateFrom"
                                                    Type="Date"
                                                    Operator="GreaterThanEqual"
                                                    ValidationGroup="Projects"
                                                    SetFocusOnError="true"
                                                    >
                                                    </asp:CompareValidator>      
                                                <ajaxToolkit:ValidatorCalloutExtender ID="startDateCompareValidatorExtender" runat="server" 
                                                    BehaviorID="startDateCompareValidatorExtender" 
                                                    TargetControlID="startDateCompareValidator" 
                                                    HighlightCssClass="validatorCalloutHighlight" 
                                                    PopupPosition="Right"
                                                    Width="175" />                                                                                                                                                                                                                
                                        </div>
                                    </div>
                                    <div class="divCriteria">
                                        <div>
                                            <h3>Project Info</h3>     
                                        </div>
                                        <div class="divFormWidest">                                
                                            <div>   
                                                <label id="Label1" runat="server">Status:</label><br />
                                                <asp:DropDownList ID="ddlProjectStatus" CssClass="dropdownNarrower" AppendDataBoundItems="true" runat="server">
                                                    <asp:ListItem Value="ALL" Selected="True">All</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <br />                         
                                            <div>   
                                                <label>Priority:</label><br />
                                                <asp:DropDownList ID="ddlProjectPriority" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="ALL" Selected="True">All</asp:ListItem>
							                        <asp:ListItem Value="Low" Text="Low"></asp:ListItem>  
                                                    <asp:ListItem Value="Medium" Text="Medium"></asp:ListItem>  
                                                    <asp:ListItem Value="High" Text="High"></asp:ListItem>   
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="divCriteria">                                                                                                                         
                                        <div>
                                            <h3>Search Filter</h3>     
                                        </div> 
                                        <div class="divFormWidest">    
                                            <label>Text:</label>
                                            <br /> 
                                            <asp:TextBox ID="txtTextFilter" CssClass="textboxNarrowest" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                </asp:Panel>
                                </fieldset>                                        
                                </ContentTemplate>
                            </asp:UpdatePanel>

                            <asp:UpdatePanel ID="updatePanelNestedBottom" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="divGenerateButton">
                                        <asp:LinkButton ID="btnGenerateData" CssClass="lnkButton" ValidationGroup="Projects" OnClick="generateButton_Click" Text="Generate Data" runat="server" />
                                    </div>
                                    
                                    <div class="clear"></div>                                                                                                                                            
                                   <div class="updateProgressDiv">
                                        <asp:UpdateProgress ID="updateProgressBottom" AssociatedUpdatePanelID="updatePanelNestedBottom" runat="server">
                                            <ProgressTemplate>
                                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Updating...</span>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress>  
                                   </div>
                                    <div>
                                        <p>           
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>                                                                                             
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrorTop" CssClass="errorMessage" runat="server" Text="" Visible="false"></asp:Label>
                                        </p>
                                    </div>     
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridProjects"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="ProjectID,CID" 
                                             GridLines="None"                                           
                                             PageSize="50" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridProjects_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridProjects_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridProjects_Sorting"                                                                                                               
                                             OnSelectedIndexChanged="gridProjects_OnSelectedIndexChanged"   
                                             OnRowEditing="gridProjects_Editing"                                                                                                                                                                                                                                                                            
                                             OnRowDeleting="gridProjects_Deleting"
                                             OnDataBound="gridProjects_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField ItemStyle-Wrap="true" SortExpression="TargetStartDate" HeaderText="Start Date">                                                      
                                                    <ItemTemplate><%# Convert.ToDateTime(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.TargetStartDate))).ToShortDateString() %></ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ProjectName" HeaderText="Project Name">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectName)), 30) %></ItemTemplate>
                                                </asp:TemplateField> 
                                                 <asp:TemplateField HeaderText="Buildings">  
                                                    <ItemTemplate><%# String.Join(", ",(string[])Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.BuildingNameArray))) %></ItemTemplate>                                                    
                                                </asp:TemplateField>  
                                                 <asp:TemplateField HeaderText="Equipment Analyses">  
                                                    <ItemTemplate><%# String.Join(", ",(string[])Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.AnalysisEquipmentNameArray))) %></ItemTemplate>                                                    
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EstimatedProjectCost" HeaderText="Estimated Cost">                                                      
                                                    <ItemTemplate><%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.EstimatedProjectCost)))) ? "" : CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.EstimatedProjectCost))), true, true) %></ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ProjectedSavings" HeaderText="Projected Savings">                                                      
                                                    <ItemTemplate><%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectedSavings)))) ? "" : CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectedSavings))), true, true) %></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Payback" HeaderText="Payback Years">                                                      
                                                    <ItemTemplate><%# CultureHelper.FormatNumberFromInvariant(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.Payback)).ToString()) %></ItemTemplate>
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="StatusName" HeaderText="Status">                                                      
                                                    <ItemTemplate><%# Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.StatusName)) %></ItemTemplate>
                                                </asp:TemplateField> 
                                                 <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ProjectPriority" HeaderText="Priority">                                                      
                                                    <ItemTemplate><%# Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectPriority)) %></ItemTemplate>
                                                </asp:TemplateField>                                                          
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this project permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                     
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT PROJECT DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvProject" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Project Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Project Name: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectName)) + "</li>"%>  
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.Description)))) ? "" : "<li><strong>Description: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.Description)) + "</li>"%>                                                       
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ReferenceInfo)))) ? "" : "<li><strong>Reference Info: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ReferenceInfo)) + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.EstimatedProjectCost)))) ? "" : "<li><strong>Estimated Project Cost: </strong>" + CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.EstimatedProjectCost))), true, true) + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectedSavings)))) ? "" : "<li><strong>Projected Annual Savings: </strong>" + CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectedSavings))), true, true) + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.Payback)))) ? "" : "<li><strong>Projected Payback Years: </strong>" + CultureHelper.FormatNumberFromInvariant(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.Payback)).ToString()) + "</li>"%>                                                             
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectOwnerFullName)))) ? "" : "<li><strong>Project Owner: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectOwnerFullName)) + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.InternalProjectAssignedFullName)))) ? "" : "<li><strong>Internal User Assigned: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.InternalProjectAssignedFullName)) + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ExternalProjectAssigned)))) ? "" : "<li><strong>External User Assigned: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ExternalProjectAssigned)) + "</li>"%> 
                                                            <%# "<li><strong>Target Start Date: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.TargetStartDate)) + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.TargetCompletionDate)))) ? "" : "<li><strong>Target Completion Date: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.TargetCompletionDate)) + "</li>"%> 
                                                            <%# "<li><strong>Project Priority: </strong>" + Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.ProjectPriority)) + "</li>"%>  
                                                            <%# "<li><strong>Buildings Associated: </strong>" + String.Join(", ",((string[])Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.BuildingNameArray)))) + "</li>"%>  
                                                            <%# "<li><strong>Equipment Associated: </strong>" + String.Join(", ",((string[])Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.EquipmentNameArray)))) + "</li>"%>  
                                                            <%# "<li><strong>Analyses Associated: </strong>" + String.Join(", ",((string[])Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.AnalysisNameArray)))) + "</li>"%>  
                                                            <%# "<li><strong>Equipment Analyses Associated: </strong>" + String.Join(", ",((string[])Eval(PropHelper.G<GetGroupedProjectAssociationsData>(p=>p.AnalysisEquipmentNameArray)))) + "</li>"%>  
                                                        </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>
                                   
                                   <!--EDIT PROJECT PANEL -->                              
                                   <asp:Panel ID="pnlEditProject" runat="server" Visible="false" DefaultButton="btnUpdateProject">                                                                                             
                                              <div>
                                                    <h2>Edit Project</h2>
                                              </div>  
                                              <div>    
                                                    <a id="lnkSetFocusEdit" runat="server"></a>                                                     
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <!--<asp:HiddenField ID="hdnEditDateCreated" runat="server" Visible="false" />-->
                                                    <asp:HiddenField ID="hdnEditCurrentAEIDs" runat="server" Visible="false" />
                                                    <asp:HiddenField ID="hdnEditEstimatedProjectCost" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*Project Name:</label>
                                                        <asp:TextBox ID="txtEditProjectName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                    </div>  
                                                    <div class="divForm">
                                                         <label class="label">Description:</label>
                                                         <textarea name="txtEditDescription" id="txtEditDescription" cols="40" rows="5" onkeyup="limitChars(this, 5000, 'divEditDescriptionCharInfo')" runat="server"></textarea>
                                                         <div id="divEditDescriptionCharInfo"></div>                                                         
                                                    </div>     
                                                    <div class="divForm">
                                                         <label class="label">Reference Info:</label>
                                                         <textarea name="txtEditReference Info" id="txtEditReferenceInfo" cols="40" rows="5" onkeyup="limitChars(this, 500, 'divEditReferenceInfoCharInfo')" runat="server"></textarea>
                                                         <div id="divEditReferenceInfoCharInfo"></div>                                                         
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">*Currency Type:</label>                                                         
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlEditCurrency" runat="server" Enabled="true">                                                                                          
                                                         </asp:DropDownList>
                                                    </div>  
                                                    <div class="divForm">
                                                         <label class="label">Estimated Project Cost:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditEstimatedProjectCost" runat="server" MaxLength="15"></asp:TextBox>    
                                                         <ajaxToolkit:FilteredTextBoxExtender ID="editEstimatedProjectCostFilteredTextBoxExtender" runat="server"
                                                            TargetControlID="txtEditEstimatedProjectCost"         
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="-.," />                                                                                                                                        
                                                    </div>                                                                     
                                                    <div class="divForm">
                                                         <label class="label">Projected Annual Savings:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditProjectedSavings" runat="server"></asp:TextBox>
                                                         <asp:CheckBox ID="chkEditProjectedSavings" CssClass="checkbox" runat="server" /><span> - Auto calculate savings on update.</span>
                                                         <ajaxToolkit:FilteredTextBoxExtender ID="editProjectedSavingsFilteredTextBoxExtender" runat="server"
                                                            TargetControlID="txtEditProjectedSavings"         
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="-.," />  
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">Projected Payback Years:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditPayback" runat="server"></asp:TextBox>  
                                                         <asp:CheckBox ID="chkEditPayback" CssClass="checkbox" runat="server" /><span> - Auto calculate payback on update.</span>                                                                                                                
                                                        <ajaxToolkit:FilteredTextBoxExtender ID="editPaybackFilteredTextBoxExtender" runat="server"
                                                            TargetControlID="txtEditPayback"         
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="-.," />  
                                                    </div>           
                                                    <div class="divForm">
                                                         <label class="label">Project Owner:</label>                                                         
                                                        <asp:DropDownList CssClass="dropdown" ID="ddlEditOwner" AppendDataBoundItems="true" runat="server">                                                                                          
                                                             <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>
                                                         </asp:DropDownList> 
                                                        <asp:Label CssClass="labelContent" ID="lblEditOwner" runat="server" Visible="false"></asp:Label>
                                                    </div>                                                 
                                                    <div class="divForm">
                                                         <label class="label">Internal User Assigned:</label>                                                         
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlEditInternalUserAssigned" AppendDataBoundItems="true" runat="server">                                                                                          
                                                             <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>
                                                         </asp:DropDownList>
                                                         <asp:Label CssClass="labelContent" ID="lblEditInternalUserAssigned" runat="server" Visible="false"></asp:Label>
                                                    </div>                                                                                         
                                                    <div class="divForm">
                                                        <label class="label">External User Assigned:</label>
                                                        <asp:TextBox ID="txtEditExternalUserAssigned" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                    </div>     
                                                    <div class="divForm">
                                                        <label class="label">*Target Start Date:</label>
                                                        <telerik:RadDatePicker ID="txtEditTargetStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>  
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="editTargetStartRequiredValidator" runat="server"
                                                            CssClass="errorMessage" 
                                                            ErrorMessage="Date is a required field."
                                                            ControlToValidate="txtEditTargetStartDate"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditProject">
                                                            </asp:RequiredFieldValidator>        
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editTargetStartRequiredValidatorExtender" runat="server" 
                                                            BehaviorID="editTargetStartRequiredValidatorExtender" 
                                                            TargetControlID="editTargetStartRequiredValidator" 
                                                            HighlightCssClass="validatorCalloutHighlight" 
                                                            PopupPosition="Right"
                                                            Width="175" />                                                                                                    
                                                </div>
                                                <div class="divForm">
                                                    <label class="label">*Target Completion Date:</label>
                                                    <telerik:RadDatePicker ID="txtEditTargetCompletionDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                      
                                                    <br />    
                                                    <asp:RequiredFieldValidator ID="editTargetCompletionRequiredValidator" runat="server"
                                                        CssClass="errorMessage" 
                                                        ErrorMessage="Date is a required field."
                                                        ControlToValidate="txtEditTargetCompletionDate"          
                                                        SetFocusOnError="true"
                                                        Display="None"
                                                        ValidationGroup="EditProject">
                                                        </asp:RequiredFieldValidator>   
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="editTargetCompletionRequiredValidatorExtender" runat="server" 
                                                        BehaviorID="editTargetCompletionRequiredValidatorExtender" 
                                                        TargetControlID="editTargetCompletionRequiredValidator" 
                                                        HighlightCssClass="validatorCalloutHighlight" 
                                                        PopupPosition="Right"
                                                        Width="175" />                                                                                                
                                                    <asp:CompareValidator ID="editTargetDateCompareValidator" Display="None" runat="server"
                                                        CssClass="errorMessage"
                                                        ErrorMessage="Invalid range."
                                                        ControlToValidate="txtEditTargetCompletionDate"
                                                        ControlToCompare="txtEditTargetStartDate"
                                                        Type="Date"
                                                        Operator="GreaterThanEqual"
                                                        ValidationGroup="EditProject"
                                                        SetFocusOnError="true"
                                                        >
                                                        </asp:CompareValidator>        
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="editTargetDateCompareValidatorExtender" runat="server" 
                                                        BehaviorID="editTargetDateCompareValidatorExtender" 
                                                        TargetControlID="editTargetDateCompareValidator" 
                                                        HighlightCssClass="validatorCalloutHighlight" 
                                                        PopupPosition="Right"
                                                        Width="175" />                                                                                                       
                                                </div>      
                                                  <div class="divForm">
                                                         <label class="label">*Project Status:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlEditProjectStatus" runat="server">                                                                                          
                                                         </asp:DropDownList>
                                                    </div> 
                                                    <div class="divForm">   
                                                        <label class="label">*Project Priority:</label>    
                                                        <asp:DropDownList ID="ddlEditProjectPriority" CssClass="dropdown" runat="server">                                                             
                                                            <asp:ListItem Value="Low" Text="Low"></asp:ListItem>  
                                                            <asp:ListItem Value="Medium" Text="Medium"></asp:ListItem>  
                                                            <asp:ListItem Value="High" Text="High"></asp:ListItem>    
                                                        </asp:DropDownList>
                                                    </div>                                                                                                   
                                                    <div class="divForm">                                                
                                                            <label class="label">Equipment Analyses Available:</label>
                                                            <asp:ListBox ID="lbEditTop" CssClass="listboxWider" runat="server" SelectionMode="Multiple">
                                                            </asp:ListBox>
                                                            <div class="divArrowsWider">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnEditEquipmentAnalysesUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditEquipmentAnalysesUpButton_Click"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnEditEquipmentAnalysesDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditEquipmentAnalysesDownButton_Click"></asp:ImageButton>                                                    
                                                            </div>
                                                            <label class="label">*Equipment Analyses Assigned:</label>
                                                            <asp:ListBox ID="lbEditBottom" CssClass="listboxWider" runat="server" SelectionMode="Multiple">
                                                            </asp:ListBox>                                                                                                           
                                                    </div>                                    
                                                                                                                                                 
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateProject" runat="server" Text="Update Project"  OnClick="updateProjectButton_Click" ValidationGroup="EditProject"></asp:LinkButton>    
                                                </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     
                                    </asp:Panel>
                                                                                                          
                                </ContentTemplate>
                            </asp:UpdatePanel>                                                  
                        </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server"> 
                                <asp:Panel ID="pnlAddProject" runat="server" DefaultButton="btnAddProject"> 
                                    <h2>Add Project</h2> 
                                    <div>    
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                                
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Project Name:</label>
                                            <asp:TextBox ID="txtAddProjectName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                            <asp:RequiredFieldValidator ID="addProjectNameRequiredValidator" runat="server"
                                                ErrorMessage="Project name is a required field."
                                                ControlToValidate="txtAddProjectName"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddProject">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addProjectNameRequiredValidatorExtender" runat="server"
                                                BehaviorID="addProjectNameRequiredValidatorExtender"
                                                TargetControlID="addProjectNameRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>  
                                        </div>
                                        <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtAddDescription" id="txtAddDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" runat="server"></textarea>
                                                <div id="divAddDescriptionCharInfo"></div>                                                         
                                        </div>
                                        <div class="divForm">
                                                <label class="label">Reference Info:</label>
                                                <textarea name="txtAddReference Info" id="txtAddReferenceInfo" cols="40" rows="5" onkeyup="limitChars(this, 500, 'divAddReferenceInfoCharInfo')" runat="server"></textarea>
                                                <div id="divAddReferenceInfoCharInfo"></div>                                                         
                                        </div>
                                        <div class="divForm">
                                                <label class="label">*Currency Type:</label>                                                         
                                                <asp:DropDownList CssClass="dropdown" ID="ddlAddCurrency" runat="server">                                                                                          
                                                </asp:DropDownList>
                                        </div>  
                                        <div class="divForm">
                                                <label class="label">Estimated Project Cost:</label>
                                                <asp:TextBox CssClass="textbox" ID="txtAddEstimatedProjectCost" runat="server" MaxLength="15"></asp:TextBox>
                                                <ajaxToolkit:FilteredTextBoxExtender ID="addEstimatedProjectCostFilteredTextBoxExtender" runat="server"
                                                    TargetControlID="txtAddEstimatedProjectCost"         
                                                    FilterType="Custom, Numbers"
                                                    ValidChars="-.," />   
                                        </div> 
                                        <div class="divForm">
                                                <label class="label">Projected Annual Savings:</label>
                                                <asp:TextBox CssClass="textbox" ID="txtAddProjectedSavings" runat="server"></asp:TextBox>
                                                <asp:CheckBox ID="chkAddProjectedSavings" CssClass="checkbox" runat="server" /><span> - Auto calculate savings on update.</span>                                                                                             
                                                <ajaxToolkit:FilteredTextBoxExtender ID="addProjectedSavingsFilteredTextBoxExtender" runat="server"
                                                            TargetControlID="txtAddProjectedSavings"         
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="-.," />  
                                        </div>
                                        <div class="divForm">
                                                <label class="label">Projected Payback Years:</label>
                                                <asp:TextBox CssClass="textbox" ID="txtAddPayback" runat="server"></asp:TextBox>  
                                                <asp:CheckBox ID="chkAddPayback" CssClass="checkbox" runat="server" /><span> - Auto calculate payback on update.</span>   
                                                <ajaxToolkit:FilteredTextBoxExtender ID="addPaybackFilteredTextBoxExtender" runat="server"
                                                            TargetControlID="txtAddPayback"         
                                                            FilterType="Custom, Numbers"
                                                            ValidChars="-.," />  
                                        </div>
                                        <div class="divForm">
                                                <label class="label">Project Owner:</label>                                                         
                                                <asp:DropDownList CssClass="dropdown" ID="ddlAddOwner" AppendDataBoundItems="true" runat="server">        
                                                    <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>                                                                                  
                                                </asp:DropDownList>
                                        </div> 
                                        <div class="divForm">
                                                <label class="label">Internal User Assigned:</label>                                                         
                                                <asp:DropDownList CssClass="dropdown" ID="ddlAddInternalUserAssigned" AppendDataBoundItems="true" runat="server">        
                                                    <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>                                                                                  
                                                </asp:DropDownList>
                                        </div>       
                                        <div class="divForm">
                                            <label class="label">External User Assigned:</label>
                                            <asp:TextBox ID="txtAddExternalUserAssigned" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                        </div>     
                                        <div class="divForm">
                                            <label class="label">*Target Start Date:</label>
                                            <telerik:RadDatePicker ID="txtAddTargetStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                             
                                            <br />   
                                            <asp:RequiredFieldValidator ID="addTargetStartRequiredValidator" runat="server"
                                                CssClass="errorMessage" 
                                                ErrorMessage="Date is a required field."
                                                ControlToValidate="txtAddTargetStartDate"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddProject">
                                                </asp:RequiredFieldValidator>     
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addTargetStartRequiredValidatorExtender" runat="server" 
                                                BehaviorID="addTargetStartRequiredValidatorExtender" 
                                                TargetControlID="addTargetStartRequiredValidator" 
                                                HighlightCssClass="validatorCalloutHighlight" 
                                                PopupPosition="Right"
                                                Width="175" />                                                                                                                                              
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Target Completion Date:</label>
                                            <telerik:RadDatePicker ID="txtAddTargetCompletionDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                            
                                            <br />   
                                            <asp:RequiredFieldValidator ID="addTargetCompletionRequiredValidator" runat="server"
                                                CssClass="errorMessage" 
                                                ErrorMessage="Date is a required field."
                                                ControlToValidate="txtAddTargetCompletionDate"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddProject">
                                                </asp:RequiredFieldValidator> 
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addTargetCompletionRequiredValidatorExtender" runat="server" 
                                                BehaviorID="addTargetCompletionRequiredValidatorExtender" 
                                                TargetControlID="addTargetCompletionRequiredValidator" 
                                                HighlightCssClass="validatorCalloutHighlight" 
                                                PopupPosition="Right"
                                                Width="175" />                                                 
                                            <asp:CompareValidator ID="addTargetDateCompareValidator" Display="None" runat="server"
                                                CssClass="errorMessage"
                                                ErrorMessage="Invalid range."
                                                ControlToValidate="txtAddTargetCompletionDate"
                                                ControlToCompare="txtAddTargetStartDate"
                                                Type="Date"
                                                Operator="GreaterThanEqual"
                                                ValidationGroup="AddProject"
                                                SetFocusOnError="true"
                                                >
                                                </asp:CompareValidator>   
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addTargetDateCompareValidatorExtender" runat="server" 
                                                BehaviorID="addTargetDateCompareValidatorExtender" 
                                                TargetControlID="addTargetDateCompareValidator" 
                                                HighlightCssClass="validatorCalloutHighlight" 
                                                PopupPosition="Right"
                                                Width="175" />                                                  
                                        </div>  
                                        <div class="divForm">
                                                <label class="label">*Project Status:</label>
                                                <asp:DropDownList CssClass="dropdown" ID="ddlAddProjectStatus" runat="server">                             
                                                </asp:DropDownList>
                                        </div>  
                                        <div class="divForm">   
                                            <label class="label">*Project Priority:</label>    
                                            <asp:DropDownList ID="ddlAddProjectPriority" CssClass="dropdown" runat="server">                                                             
                                                <asp:ListItem Value="Low" Text="Low"></asp:ListItem>  
                                                <asp:ListItem Value="Medium" Text="Medium"></asp:ListItem>  
                                                <asp:ListItem Value="High" Text="High"></asp:ListItem>    
                                            </asp:DropDownList>
                                        </div>   
                                        <div class="divForm">
                                                <label class="label">Equipment Analyses Available:</label>
                                                <asp:ListBox ID="lbAddTop" CssClass="listboxWider" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrowsWider">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnAddEquipmentAnalysesUp" OnClick="btnAddEquipmentAnalysesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server" ></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnAddEquipmentAnalysesDown" OnClick="btnAddEquipmentAnalysesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server" ></asp:ImageButton>                                            
                                                </div>
                                                <label class="label">*Equipment Analyses Assigned:</label>
                                                <asp:ListBox ID="lbAddBottom" CssClass="listboxWider" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                        </div>                                                                                              
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddProject" runat="server" Text="Add Project"  OnClick="addProjectButton_Click" ValidationGroup="AddProject"></asp:LinkButton>                                                                                   
                                    </div>
                                                                        
                                </asp:Panel>                                                                                                                                                                    
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                   </div>                                                                 
            
</asp:Content>


                    
                  
