﻿using System;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Xml;
using System.Windows.Threading;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Web;

using Microsoft.Samples.ServiceHosting.AspProviders;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.StorageClient;

using CW.Business;
using CW.Website.Dashboard;
using CW.Website._framework;
using CW.Data;
using CW.Data.AzureStorage;
using CW.Data.AzureStorage.DataContexts.Blob;
using CW.Data.AzureStorage.Helpers;
using CW.Data.AzureStorage.Models.Blob;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using CW.Data.Models.Client;
using CW.Data.Models.Point;
using CW.Data.Models.Raw;
using CW.Website._administration;
using CW.Data.Models.Performance;

using Telerik.Web.UI;
using Telerik.Charting;
using System.Data;
using System.Web.UI.HtmlControls;
using CW.Business.Results;

namespace CW.Website
{
    public partial class PerformanceDashboard: SitePage
    {
        #region Properties

            private int uid;
            private int cid;
            const string noClientImage = "no-client-image.png";
            private int roleID;
            private bool isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser;
            //private string siteAddress;

            private CacheManager<IEnumerable<PerformanceResult>> cacheManager;
            private Building currentBuilding;

            const string noPlots = "No data has been plotted.";
            const string failedPlots = "Data failed to plot. Please contact an administrator.";
            const string defaultPortfolioOverviewMessage = "Please select a metric for portfolio overview plots.";
            const string defaultPortfolioDataMessage = "Please select one or more buildings to be plotted.";
            const string defaultBuildingMessage = "Please select a building to be plotted.";

        #endregion

        #region Page Events

            protected override void OnLoad(EventArgs e)
            {
                //datamanger in sitepage

                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = mDataManager.GlobalDataMapper.GetGlobalSettings();
                    litDashboardBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme,Server.HtmlDecode(globalSetting.PerformanceDashboardBody));

                    //get and bind buildings
                    BindBuildings(lbPortfolioDataBuildings);
                    BindBuildings(ddlBuildingDataBuilding);
                    BindBuildings(ddlBuildingOverviewBuilding);

                    SetClientInfoAndImages();

                    lblPortfolioOverviewDefaultMessage.Text = defaultPortfolioOverviewMessage;
                    lblPortfolioDataResults.Text = defaultPortfolioDataMessage;
                    lblBuildingDataResults.Text = defaultBuildingMessage;
                    lblBuildingOverviewDefaultMessage.Text = defaultBuildingMessage;
                }   

                //set global info
                uid = siteUser.UID;
                roleID = siteUser.RoleID;
                cid = siteUser.CID;
                //siteAddress = ConfigurationManager.AppSettings["SiteAddress"];            
                //serializedBuildings = GetParam("Buildings");
                isKGSFullAdminOrHigher = siteUser.IsKGSFullAdminOrHigher;
                isSuperAdminOrFullAdminOrFullUser = siteUser.IsSuperAdminOrFullAdminOrFullUser;

                cacheManager = new CacheManager<IEnumerable<PerformanceResult>>(CW.Business.DataManager.Instance.DefaultCacheRepository, CreateCacheKey);
            }

            protected void Page_Init()
            {
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Bind Buildings
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="buildings"></param>
            private void BindBuildings(ListBox lb)
            {
                lb.DataTextField = "BuildingName";
                lb.DataValueField = "BID";
                lb.DataSource = siteUser.VisibleBuildings;
                lb.DataBind();
            }

            /// <summary>
            /// Binds a dropdown list of users buildings
            /// </summary>
            /// <param name="ddl"></param>
            private void BindBuildings(DropDownList ddl)
            {
                ddl.DataTextField = "BuildingName";
                ddl.DataValueField = "BID";
                ddl.DataSource = siteUser.VisibleBuildings;
                ddl.DataBind();
            }

        #endregion

        #region Set Fields and Data

            private void SetClientInfoAndImages()
            {
                GetClientsData client = mDataManager.ClientDataMapper.GetFullClientByIDAsEnumberable(siteUser.CID).FirstOrDefault();

                if (client == null) { return; }

                imgHeaderClientImage.AlternateText = "";
                imgHeaderClientImage.Visible = true;
                imgHeaderClientImage.ImageUrl = HandlerHelper.ClientImageUrl(client.CID, client.ImageExtension);

                //radHeaderClientImage.AlternateText = siteUser.ClientName;
                //radHeaderClientImage.Visible = true;
                //radHeaderClientImage.DefaultImageVirtualPath = "~/" + ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;

                //// Only assign a blob lookup if image extension exists in relational database
                //// If lookup is not set control will display the default image and save on transactions ($$$)
                //if (!String.IsNullOrEmpty(client.ImageExtension))
                //{
                //    var blob = mDataManager.ClientDataMapper.GetClientImage(client.CID, client.ImageExtension);

                //    radHeaderClientImage.DataValue = blob != null ? blob.Image : null;
                //}

                //// Before doing a blob lookup, check if image extension exists in relational database
                //// Save on transactions ($$$)
                //if (String.IsNullOrEmpty(client.First().ImageExtension))
                //{
                //    imgHeaderClientImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;
                //    imgHeaderClientImage.AlternateText = clientName;

                //    radHeaderClientImage.DataValue = null;
                //    radHeaderClientImage.Visible = false;
                //}
                //else
                //{
                //    ClientBlob clientBlob = mDataManager.ClientDataMapper.GetClientImage(client.First().CID, client.First().ImageExtension);

                //    if (clientBlob == null)
                //    {
                //        imgHeaderClientImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;
                //        imgHeaderClientImage.AlternateText = clientName;

                //        radHeaderClientImage.DataValue = null;
                //        radHeaderClientImage.Visible = false;
                //    }
                //    else
                //    {
                //        radHeaderClientImage.DataValue = clientBlob.Image;

                //        imgHeaderClientImage.ImageUrl = "";
                //        imgHeaderClientImage.Visible = false;
                //    }
                //}

                lblHeaderClientName.InnerText = siteUser.ClientName;
            }

        #endregion

        #region Dropdown Events

        #endregion

        #region ListBox Events

        #endregion

        #region Main Nav Button Click Events

            /// <summary>
            /// Home view button click
            /// </summary>
            protected void homeButton_Click(object sender, EventArgs e)
            {
                mvMain.ActiveViewIndex = 0;
                mvSecondary.ActiveViewIndex = 0;

                SetActiveNavButton(btnHome);
            }

            /// <summary>
            /// Portfolio  Overview view button click
            /// </summary>
            protected void portfolioOverviewButton_Click(object sender, EventArgs e)
            {
                mvMain.ActiveViewIndex = 1;
                mvSecondary.ActiveViewIndex = 1;

                SetActiveNavButton(btnPortfolioOverview);
            }

            /// <summary>
            /// Portfolio  Data view button click
            /// </summary>
            protected void portfolioDataButton_Click(object sender, EventArgs e)
            {
                mvMain.ActiveViewIndex = 2;
                mvSecondary.ActiveViewIndex = 2;

                SetActiveNavButton(btnPortfolioData);
            }

            /// <summary>
            /// Building  Overview view button click
            /// </summary>
            protected void buildingOverviewButton_Click(object sender, EventArgs e)
            {
                mvMain.ActiveViewIndex = 3;
                mvSecondary.ActiveViewIndex = 3;

                SetActiveNavButton(btnBuildingOverview);
            }

            /// <summary>
            /// Building  Data view button click
            /// </summary>
            protected void buildingDataButton_Click(object sender, EventArgs e)
            {
                mvMain.ActiveViewIndex = 4;
                mvSecondary.ActiveViewIndex = 4;

                SetActiveNavButton(btnBuildingData);
            }


        #endregion

        #region Button Click Events

            /// <summary>
            /// Generate Portfolio Overview button click
            /// </summary>
            protected void generatePortfolioOverviewButton_Click(object sender, EventArgs e)
            {
                PlotPortfolioOverview();

                divPortfolioOverviewDefaultMessage.Visible = false;
                divPortfolioOverviewButtons.Visible = true;

                //set initial as yesterday
                portfolioYesterdayButton_Click(null, null);
            }

            /// <summary>
            /// Generate Portfolio  Data button click
            /// </summary>
            protected void generatePortfolioDataButton_Click(object sender, EventArgs e)
            {          
                List<int> bids = new List<int>();

                foreach (ListItem li in lbPortfolioDataBuildings.Items)
                {
                    if (li.Selected)
                    {
                        bids.Add(Convert.ToInt32(li.Value));
                    }
                }

                if (bids.Any())
                {
                    PlotPortfolioDataBuildings(bids);
                }
            }

            /// <summary>
            /// Generate Building Overview button click
            /// </summary>
            protected void generateBuildingOverviewButton_Click(object sender, EventArgs e)
            {
                PlotBuildingOverview(Convert.ToInt32(ddlBuildingOverviewBuilding.SelectedValue));

                divBuildingOverviewDefaultMessage.Visible = false;
                divBuildingOverviewButtons.Visible = true;

                //set initial as yesterday drilldown
                buildingDrillDownButton_Click(null, null);
                buildingYesterdayButton_Click(null, null);
            }

            /// <summary>
            /// Generate Building  Data button click
            /// </summary>
            protected void generateBuildingDataButton_Click(object sender, EventArgs e)
            {
                PlotBuildingDataBuilding(Convert.ToInt32(ddlBuildingDataBuilding.SelectedValue));
            }

            protected void buildingTotalsButton_Click(object sender, EventArgs e)
            {
                divBuildingOverviewTotalYesterdayResults.Visible = divBuildingOverviewTotalYesterdayResults.Visible || divBuildingOverviewDrillDownYesterdayResults.Visible;
                divBuildingOverviewTotalWeekResults.Visible = divBuildingOverviewTotalWeekResults.Visible || divBuildingOverviewDrillDownWeekResults.Visible;
                divBuildingOverviewTotalMonthResults.Visible = divBuildingOverviewTotalMonthResults.Visible || divBuildingOverviewDrillDownMonthResults.Visible;
                divBuildingOverviewTotalYearResults.Visible = divBuildingOverviewTotalYearResults.Visible || divBuildingOverviewDrillDownYearResults.Visible;

                buildingOverviewBlocks.Visible = ((divBuildingOverviewTotalYesterdayResults.Visible && buildingOverviewTotalYesterdayChart.Visible) ||
                                (divBuildingOverviewTotalWeekResults.Visible && buildingOverviewTotalWeekChart.Visible) ||
                                (divBuildingOverviewTotalMonthResults.Visible && buildingOverviewTotalMonthChart.Visible) ||
                                (divBuildingOverviewTotalYearResults.Visible && buildingOverviewTotalYearChart.Visible));

                divBuildingOverviewDrillDownYesterdayResults.Visible = false;
                divBuildingOverviewDrillDownWeekResults.Visible = false;
                divBuildingOverviewDrillDownMonthResults.Visible = false;
                divBuildingOverviewDrillDownYearResults.Visible = false;

                btnBuildingTotal.CssClass = "lnkBtnSmallDashboardActive";
                btnBuildingDrillDown.CssClass = "lnkBtnSmallDashboard";

                SetBuildingOverviewBlockTotalsVisibility();                
            }
            protected void buildingDrillDownButton_Click(object sender, EventArgs e)
            {
                divBuildingOverviewDrillDownYesterdayResults.Visible = divBuildingOverviewTotalYesterdayResults.Visible || divBuildingOverviewDrillDownYesterdayResults.Visible;
                divBuildingOverviewDrillDownWeekResults.Visible = divBuildingOverviewTotalWeekResults.Visible || divBuildingOverviewDrillDownWeekResults.Visible;
                divBuildingOverviewDrillDownMonthResults.Visible = divBuildingOverviewTotalMonthResults.Visible || divBuildingOverviewDrillDownMonthResults.Visible;
                divBuildingOverviewDrillDownYearResults.Visible = divBuildingOverviewTotalYearResults.Visible || divBuildingOverviewDrillDownYearResults.Visible;

                buildingOverviewBlocks.Visible = ((divBuildingOverviewDrillDownYesterdayResults.Visible && buildingOverviewDrillDownYesterdayChart.Visible) ||
                                                (divBuildingOverviewDrillDownWeekResults.Visible && buildingOverviewDrillDownWeekChart.Visible) ||
                                                (divBuildingOverviewDrillDownMonthResults.Visible && buildingOverviewDrillDownMonthChart.Visible) ||
                                                (divBuildingOverviewDrillDownYearResults.Visible && buildingOverviewDrillDownYearChart.Visible));


                divBuildingOverviewTotalYesterdayResults.Visible = false;
                divBuildingOverviewTotalWeekResults.Visible = false;
                divBuildingOverviewTotalMonthResults.Visible = false;
                divBuildingOverviewTotalYearResults.Visible = false;

                btnBuildingTotal.CssClass = "lnkBtnSmallDashboard";
                btnBuildingDrillDown.CssClass = "lnkBtnSmallDashboardActive";

                SetBuildingOverviewBlockTotalsVisibility();   
            }
            protected void buildingYesterdayButton_Click(object sender, EventArgs e)
            {
                if (divBuildingOverviewTotalYesterdayResults.Visible || divBuildingOverviewTotalWeekResults.Visible || divBuildingOverviewTotalMonthResults.Visible || divBuildingOverviewTotalYearResults.Visible)
                {
                    divBuildingOverviewTotalYesterdayResults.Visible = true;
                    divBuildingOverviewDrillDownYesterdayResults.Visible = false;

                    buildingOverviewBlocks.Visible = buildingOverviewTotalYesterdayChart.Visible;
                }
                else
                {
                    divBuildingOverviewDrillDownYesterdayResults.Visible = true;
                    divBuildingOverviewTotalYesterdayResults.Visible = false;

                    buildingOverviewBlocks.Visible = buildingOverviewDrillDownYesterdayChart.Visible;
                }

                divBuildingOverviewTotalWeekResults.Visible = false;
                divBuildingOverviewTotalMonthResults.Visible = false;
                divBuildingOverviewTotalYearResults.Visible = false;
                divBuildingOverviewDrillDownWeekResults.Visible = false;
                divBuildingOverviewDrillDownMonthResults.Visible = false;
                divBuildingOverviewDrillDownYearResults.Visible = false;

                btnBuildingYesterday.CssClass = "lnkBtnSmallDashboardActive";
                btnBuildingWeek.CssClass = "lnkBtnSmallDashboard";
                btnBuildingMonth.CssClass = "lnkBtnSmallDashboard";
                btnBuildingYear.CssClass = "lnkBtnSmallDashboard";

                SetBuildingOverviewBlockTotalsVisibility();   
            }
            protected void buildingWeekButton_Click(object sender, EventArgs e)
            {
                if (divBuildingOverviewTotalYesterdayResults.Visible || divBuildingOverviewTotalWeekResults.Visible || divBuildingOverviewTotalMonthResults.Visible || divBuildingOverviewTotalYearResults.Visible)
                {
                    divBuildingOverviewTotalWeekResults.Visible = true;
                    divBuildingOverviewDrillDownWeekResults.Visible = false;

                    buildingOverviewBlocks.Visible = buildingOverviewTotalWeekChart.Visible;
                }
                else
                {
                    divBuildingOverviewDrillDownWeekResults.Visible = true;
                    divBuildingOverviewTotalWeekResults.Visible = false;

                    buildingOverviewBlocks.Visible = buildingOverviewDrillDownWeekChart.Visible;
                }

                divBuildingOverviewTotalYesterdayResults.Visible = false;
                divBuildingOverviewTotalMonthResults.Visible = false;
                divBuildingOverviewTotalYearResults.Visible = false;
                divBuildingOverviewDrillDownYesterdayResults.Visible = false;
                divBuildingOverviewDrillDownMonthResults.Visible = false;
                divBuildingOverviewDrillDownYearResults.Visible = false;

                btnBuildingYesterday.CssClass = "lnkBtnSmallDashboard";
                btnBuildingWeek.CssClass = "lnkBtnSmallDashboardActive";
                btnBuildingMonth.CssClass = "lnkBtnSmallDashboard";
                btnBuildingYear.CssClass = "lnkBtnSmallDashboard";

                SetBuildingOverviewBlockTotalsVisibility();
            }
            protected void buildingMonthButton_Click(object sender, EventArgs e)
            {
                if (divBuildingOverviewTotalYesterdayResults.Visible || divBuildingOverviewTotalWeekResults.Visible || divBuildingOverviewTotalMonthResults.Visible || divBuildingOverviewTotalYearResults.Visible)
                {
                    divBuildingOverviewTotalMonthResults.Visible = true;
                    divBuildingOverviewDrillDownMonthResults.Visible = false;

                    buildingOverviewBlocks.Visible = buildingOverviewTotalMonthChart.Visible;
                }
                else
                {
                    divBuildingOverviewDrillDownMonthResults.Visible = true;
                    divBuildingOverviewTotalMonthResults.Visible = false;

                    buildingOverviewBlocks.Visible = buildingOverviewDrillDownMonthChart.Visible;
                }

                divBuildingOverviewTotalYesterdayResults.Visible = false;
                divBuildingOverviewTotalWeekResults.Visible = false;
                divBuildingOverviewTotalYearResults.Visible = false;
                divBuildingOverviewDrillDownYesterdayResults.Visible = false;
                divBuildingOverviewDrillDownWeekResults.Visible = false;
                divBuildingOverviewDrillDownYearResults.Visible = false;

                btnBuildingYesterday.CssClass = "lnkBtnSmallDashboard";
                btnBuildingWeek.CssClass = "lnkBtnSmallDashboard";
                btnBuildingMonth.CssClass = "lnkBtnSmallDashboardActive";
                btnBuildingYear.CssClass = "lnkBtnSmallDashboard";

                SetBuildingOverviewBlockTotalsVisibility();
            }
            protected void buildingYearButton_Click(object sender, EventArgs e)
            {
                if (divBuildingOverviewTotalYesterdayResults.Visible || divBuildingOverviewTotalWeekResults.Visible || divBuildingOverviewTotalMonthResults.Visible || divBuildingOverviewTotalYearResults.Visible)
                {
                    divBuildingOverviewTotalYearResults.Visible = true;
                    divBuildingOverviewDrillDownYearResults.Visible = false;

                    buildingOverviewBlocks.Visible = buildingOverviewTotalYearChart.Visible;
                }
                else
                {
                    divBuildingOverviewDrillDownYearResults.Visible = true;
                    divBuildingOverviewTotalYearResults.Visible = false;

                    buildingOverviewBlocks.Visible = buildingOverviewDrillDownYearChart.Visible;
                }

                divBuildingOverviewTotalYesterdayResults.Visible = false;
                divBuildingOverviewTotalWeekResults.Visible = false;
                divBuildingOverviewTotalMonthResults.Visible = false;
                divBuildingOverviewDrillDownYesterdayResults.Visible = false;
                divBuildingOverviewDrillDownWeekResults.Visible = false;
                divBuildingOverviewDrillDownMonthResults.Visible = false;

                btnBuildingYesterday.CssClass = "lnkBtnSmallDashboard";
                btnBuildingWeek.CssClass = "lnkBtnSmallDashboard";
                btnBuildingMonth.CssClass = "lnkBtnSmallDashboard";
                btnBuildingYear.CssClass = "lnkBtnSmallDashboardActive";

                SetBuildingOverviewBlockTotalsVisibility();
            }

            protected void portfolioYesterdayButton_Click(object sender, EventArgs e)
            {                
                divPortfolioOverviewYesterdayResults.Visible = true;
                divPortfolioOverviewWeekResults.Visible = false;
                divPortfolioOverviewMonthResults.Visible = false;
                divPortfolioOverviewYearResults.Visible = false;

                portfolioOverviewBlocks.Visible = divPortfolioOverviewYesterdayResults.Visible && portfolioOverviewYesterdayChart.Visible;

                btnPortfolioYesterday.CssClass = "lnkBtnSmallDashboardActive";
                btnPortfolioWeek.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioMonth.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioYear.CssClass = "lnkBtnSmallDashboard";

                SetPortfolioOverviewBlockTotalsVisibility();
            }
            protected void portfolioWeekButton_Click(object sender, EventArgs e)
            {
                divPortfolioOverviewYesterdayResults.Visible = false;
                divPortfolioOverviewWeekResults.Visible = true;
                divPortfolioOverviewMonthResults.Visible = false;
                divPortfolioOverviewYearResults.Visible = false;

                portfolioOverviewBlocks.Visible = divPortfolioOverviewWeekResults.Visible && portfolioOverviewWeekChart.Visible;

                btnPortfolioYesterday.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioWeek.CssClass = "lnkBtnSmallDashboardActive";
                btnPortfolioMonth.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioYear.CssClass = "lnkBtnSmallDashboard";

                SetPortfolioOverviewBlockTotalsVisibility();
            }
            protected void portfolioMonthButton_Click(object sender, EventArgs e)
            {
                divPortfolioOverviewYesterdayResults.Visible = false;
                divPortfolioOverviewWeekResults.Visible = false;
                divPortfolioOverviewMonthResults.Visible = true;
                divPortfolioOverviewYearResults.Visible = false;

                portfolioOverviewBlocks.Visible = divPortfolioOverviewMonthResults.Visible && portfolioOverviewMonthChart.Visible;

                btnPortfolioYesterday.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioWeek.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioMonth.CssClass = "lnkBtnSmallDashboardActive";
                btnPortfolioYear.CssClass = "lnkBtnSmallDashboard";

                SetPortfolioOverviewBlockTotalsVisibility();
            }
            protected void portfolioYearButton_Click(object sender, EventArgs e)
            {
                divPortfolioOverviewYesterdayResults.Visible = false;
                divPortfolioOverviewWeekResults.Visible = false;
                divPortfolioOverviewMonthResults.Visible = false;
                divPortfolioOverviewYearResults.Visible = true;

                portfolioOverviewBlocks.Visible = divPortfolioOverviewYearResults.Visible && portfolioOverviewYearChart.Visible;

                btnPortfolioYesterday.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioWeek.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioMonth.CssClass = "lnkBtnSmallDashboard";
                btnPortfolioYear.CssClass = "lnkBtnSmallDashboardActive";

                SetPortfolioOverviewBlockTotalsVisibility();
            }

        #endregion

        #region Binding Methods

            /// <summary>
            /// 
            /// </summary>
            private void BindPresetWidgets()
            {
            }


        #endregion

        #region Portfolio Overview Methods

            /// <summary>
            /// Plots the buildings selected.
            /// </summary>
            protected void PlotPortfolioOverview()
            {
                bool hasYearPlotData = false, hasMonthPlotData = false, hasWeekPlotData = false, hasYesterdayPlotData = false;

                //clear chart and plot if data
                portfolioOverviewYearChart.Series.Clear();
                portfolioOverviewMonthChart.Series.Clear();
                portfolioOverviewWeekChart.Series.Clear();
                portfolioOverviewYesterdayChart.Series.Clear();

                //new tables format
                DataTable yearTable = new DataTable();
                yearTable.Columns.Add("Building", typeof(string));
                yearTable.Columns.Add("Actual", typeof(double));
                DataTable monthTable = new DataTable();
                monthTable.Columns.Add("Building", typeof(string));
                monthTable.Columns.Add("Actual", typeof(double));
                DataTable weekTable = new DataTable();
                weekTable.Columns.Add("Building", typeof(string));
                weekTable.Columns.Add("Actual", typeof(double));
                DataTable yesterdayTable = new DataTable();
                yesterdayTable.Columns.Add("Building", typeof(string));
                yesterdayTable.Columns.Add("Actual", typeof(double));

                //TODO: targeted?
                //table.Columns.Add("Targeted", typeof(double));

                List<CW.Data.Point> points = new List<CW.Data.Point>();

                DateTime yesterdayDate = DateTime.UtcNow.Date.AddDays(-1);
                DateTime endDate = DateTime.UtcNow.Date;
                DateTime firstOfYear = DateTimeHelper.GetFirstOfYear(DateTime.UtcNow);
                DateTime firstOfMonth = DateTimeHelper.GetFirstOfMonth(DateTime.UtcNow);
                DateTime firstOfWeek = DateTimeHelper.GetLastSunday(DateTime.UtcNow);

                    //get all users buildings
                    IEnumerable<Building> buildings = siteUser.VisibleBuildings;

                    //foreach building
                    foreach (Building b in buildings)
                    {
                        double yearValue = 0, monthValue = 0, weekValue = 0, yesterdayValue = 0;
                        IEnumerable<PerformanceResult> results = Enumerable.Empty<PerformanceResult>();
                        IEnumerable<PerformanceResult> tempFilteredResults = Enumerable.Empty<PerformanceResult>();

                        //GET FROM CACHE
                        results = mDataManager.PerformanceDataMapper.GetPerformanceResults(b);
                        //results = cacheManager.Retrieve(DummyPerformanceResults);

                        //if points exist here
                        if (results != null && results.Any())
                        {
                            //if metric type is 2 then divide by sqft
                            if (Convert.ToInt32(ddlPortfolioOverviewMetric.SelectedValue) == 2)
                            {
                                foreach (PerformanceResult r in results)
                                {
                                    r.Value = r.Value / Convert.ToDouble(b.Sqft);
                                }
                            }


                            //Current Year (excluding today)----

                            //try to get energy meters first
                            tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                            //try to get energy submeters second
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                            //try to get power meters third
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                            //try to get power submeters fourth
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);

                            foreach (PerformanceResult pr in tempFilteredResults)
                            {
                                yearValue = yearValue + pr.Value;
                            }


                            //Current Month (excluding today)----

                            //clear temp results
                            tempFilteredResults = Enumerable.Empty<PerformanceResult>();

                            //try to get energy meters first
                            tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                            //try to get energy submeters second
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                            //try to get power meters third
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                            //try to get power submeters fourth
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);

                            foreach (PerformanceResult pr in tempFilteredResults)
                            {
                                monthValue = monthValue + pr.Value;
                            }



                            //Current Week (excluding today)----

                            //clear temp results
                            tempFilteredResults = Enumerable.Empty<PerformanceResult>();

                            //try to get energy meters first
                            tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                            //try to get energy submeters second
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                            //try to get power meters third
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                            //try to get power submeters fourth
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);

                            foreach (PerformanceResult pr in tempFilteredResults)
                            {
                                weekValue = weekValue + pr.Value;
                            }



                            //Yesterday----

                            //clear temp results
                            tempFilteredResults = Enumerable.Empty<PerformanceResult>();

                            //try to get energy meters first
                            tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                            //try to get energy submeters second
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                            //try to get power meters third
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                            //try to get power submeters fourth
                            if (!tempFilteredResults.Any())
                                tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);

                            foreach (PerformanceResult pr in tempFilteredResults)
                            {
                                yesterdayValue = yesterdayValue + pr.Value;
                            }



                            //if value not 0
                            if (yearValue != 0)
                            {
                                //add row
                                yearTable.Rows.Add(b.BuildingName, yearValue);
                            }
                            if (monthValue != 0)
                            {
                                //add row
                                monthTable.Rows.Add(b.BuildingName, monthValue);
                            }
                            if (weekValue != 0)
                            {
                                //add row
                                weekTable.Rows.Add(b.BuildingName, weekValue);
                            }
                            if (yesterdayValue != 0)
                            {
                                //add row
                                yesterdayTable.Rows.Add(b.BuildingName, yesterdayValue);
                            }
                        }

                    }
              

                //set has data flags
                hasYearPlotData = yearTable.Rows.Count > 0;
                hasMonthPlotData = monthTable.Rows.Count > 0;
                hasWeekPlotData = weekTable.Rows.Count > 0;
                hasYesterdayPlotData = yesterdayTable.Rows.Count > 0;

                //has year plot data-----
                if (hasYearPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    portfolioOverviewYearChart.ChartAreas["portfolioOverviewYearChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    portfolioOverviewYearChart.ChartAreas["portfolioOverviewYearChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set title based on metric
                    portfolioOverviewYearChart.ChartAreas["portfolioOverviewYearChartArea"].AxisY.Title = ddlPortfolioOverviewMetric.SelectedItem.Text;

                    //convert datatable to a IEnumerable form
                    var IListTable = (yearTable as System.ComponentModel.IListSource).GetList();

                    portfolioOverviewYearChart.DataBindTable(IListTable, "Building");

                    //set chart title
                    portfolioOverviewYearChart.Titles["portfolioOverviewYearTitle"].Text = "Energy Consumption for all Accessible Buildings for the Current Year. (Jan 1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                    portfolioOverviewYearChart.Series[0].Color = Color.ForestGreen;

                    double yearTotal = 0;

                    foreach (DataRow r in yearTable.Rows)
                    {
                        yearTotal = yearTotal + Convert.ToDouble(r[1]);
                    }

                    spanPortfolioOverviewTotalYear.InnerText = Math.Round(yearTotal, 2).ToString();
                }
                else
                {
                    lblPortfolioOverviewYearResults.Text = noPlots;
                }

                portfolioOverviewYearChart.Visible = hasYearPlotData;
                lblPortfolioOverviewYearResults.Visible = !hasYearPlotData;


                //has month plot data-----
                if (hasMonthPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    portfolioOverviewMonthChart.ChartAreas["portfolioOverviewMonthChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    portfolioOverviewMonthChart.ChartAreas["portfolioOverviewMonthChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set title based on metric
                    portfolioOverviewMonthChart.ChartAreas["portfolioOverviewMonthChartArea"].AxisY.Title = ddlPortfolioOverviewMetric.SelectedItem.Text;

                    //convert datatable to a IEnumerable form
                    var IListTable = (monthTable as System.ComponentModel.IListSource).GetList();

                    portfolioOverviewMonthChart.DataBindTable(IListTable, "Building");

                    //set chart title
                    portfolioOverviewMonthChart.Titles["portfolioOverviewMonthTitle"].Text = "Energy Consumption for all Accessible Buildings for the Current Month. (1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";                    

                    portfolioOverviewMonthChart.Series[0].Color = Color.ForestGreen;

                    double monthTotal = 0;

                    foreach (DataRow r in monthTable.Rows)
                    {
                        monthTotal = monthTotal + Convert.ToDouble(r[1]);
                    }

                    spanPortfolioOverviewTotalMonth.InnerText = Math.Round(monthTotal, 2).ToString();
                }
                else
                {
                    lblPortfolioOverviewMonthResults.Text = noPlots;
                }

                portfolioOverviewMonthChart.Visible = hasMonthPlotData;
                lblPortfolioOverviewMonthResults.Visible = !hasMonthPlotData;


                //has week plot data------
                if (hasWeekPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    portfolioOverviewWeekChart.ChartAreas["portfolioOverviewWeekChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    portfolioOverviewWeekChart.ChartAreas["portfolioOverviewWeekChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set title based on metric
                    portfolioOverviewWeekChart.ChartAreas["portfolioOverviewWeekChartArea"].AxisY.Title = ddlPortfolioOverviewMetric.SelectedItem.Text;

                    //convert datatable to a IEnumerable form
                    var IListTable = (weekTable as System.ComponentModel.IListSource).GetList();

                    portfolioOverviewWeekChart.DataBindTable(IListTable, "Building");

                    //set chart title
                    portfolioOverviewWeekChart.Titles["portfolioOverviewWeekTitle"].Text = "Energy Consumption for all Accessible Buildings for the Current Week. (Sunday through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                    portfolioOverviewWeekChart.Series[0].Color = Color.ForestGreen;

                    double weekTotal = 0;

                    foreach (DataRow r in weekTable.Rows)
                    {
                        weekTotal = weekTotal + Convert.ToDouble(r[1]);
                    }

                    spanPortfolioOverviewTotalWeek.InnerText = Math.Round(weekTotal, 2).ToString();
                }
                else
                {
                    lblPortfolioOverviewWeekResults.Text = noPlots;
                }

                portfolioOverviewWeekChart.Visible = hasWeekPlotData;
                lblPortfolioOverviewWeekResults.Visible = !hasWeekPlotData;


                //has yesterday plot data------
                if (hasYesterdayPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    portfolioOverviewYesterdayChart.ChartAreas["portfolioOverviewYesterdayChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    portfolioOverviewYesterdayChart.ChartAreas["portfolioOverviewYesterdayChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set title based on metric
                    portfolioOverviewYesterdayChart.ChartAreas["portfolioOverviewYesterdayChartArea"].AxisY.Title = ddlPortfolioOverviewMetric.SelectedItem.Text;

                    //convert datatable to a IEnumerable form
                    var IListTable = (yesterdayTable as System.ComponentModel.IListSource).GetList();

                    portfolioOverviewYesterdayChart.DataBindTable(IListTable, "Building");

                    //set chart title
                    portfolioOverviewYesterdayChart.Titles["portfolioOverviewYesterdayTitle"].Text = "Energy Consumption for all Accessible Buildings for Yesterday.";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                    portfolioOverviewYesterdayChart.Series[0].Color = Color.ForestGreen;

                    double yesterdayTotal = 0;

                    foreach (DataRow r in yesterdayTable.Rows)
                    {
                        yesterdayTotal = yesterdayTotal + Convert.ToDouble(r[1]);
                    }

                    spanPortfolioOverviewTotalYesterday.InnerText = Math.Round(yesterdayTotal, 2).ToString();
                }
                else
                {
                    lblPortfolioOverviewYesterdayResults.Text = noPlots;
                }

                portfolioOverviewYesterdayChart.Visible = hasYesterdayPlotData;
                lblPortfolioOverviewYesterdayResults.Visible = !hasYesterdayPlotData;
            }

            /// <summary>
            /// PRE CACHING
            /// Plots the buildings selected.
            /// </summary>
            //protected void PlotPortfolioOverview()
            //{
            //    bool hasYearPlotData = false, hasMonthPlotData = false, hasWeekPlotData = false, hasYesterdayPlotData = false;

            //    //clear chart and plot if data
            //    portfolioOverviewYearChart.Series.Clear();
            //    portfolioOverviewMonthChart.Series.Clear();
            //    portfolioOverviewWeekChart.Series.Clear();
            //    portfolioOverviewYesterdayChart.Series.Clear();

            //    //new tables format
            //    DataTable yearTable = new DataTable();
            //    yearTable.Columns.Add("Building", typeof(string));
            //    yearTable.Columns.Add("Actual", typeof(double));
            //    DataTable monthTable = new DataTable();
            //    monthTable.Columns.Add("Building", typeof(string));
            //    monthTable.Columns.Add("Actual", typeof(double));
            //    DataTable weekTable = new DataTable();
            //    weekTable.Columns.Add("Building", typeof(string));
            //    weekTable.Columns.Add("Actual", typeof(double));
            //    DataTable yesterdayTable = new DataTable();
            //    yesterdayTable.Columns.Add("Building", typeof(string));
            //    yesterdayTable.Columns.Add("Actual", typeof(double));

            //    //TODO: targeted??
            //    //table.Columns.Add("Targeted", typeof(double));

            //    List<CW.Data.Point> points = new List<CW.Data.Point>();

            //    DateTime yesterdayDate = DateTime.UtcNow.Date.AddDays(-1);
            //    DateTime endDate = DateTime.UtcNow.Date;
            //    DateTime firstOfYear = DateTimeHelper.GetFirstOfYear(DateTime.UtcNow);
            //    DateTime firstOfMonth = DateTimeHelper.GetFirstOfMonth(DateTime.UtcNow);
            //    DateTime firstOfWeek = DateTimeHelper.GetLastSunday(DateTime.UtcNow);


            //    //TODO: if first cache set for all buildings, else
            //    //cache empty for client
            //    if (true)
            //    {
            //        //get all buildings
            //        IEnumerable<Building> buildings = BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID);

            //        //foreach building
            //        foreach (Building b in buildings)
            //        {
            //            double yearValue = 0, monthValue = 0, weekValue = 0, yesterdayValue = 0;

            //            //get building sqft
            //            int sqft = b.Sqft;

            //            //get point or points where point type is the energy metric
            //            points = mDataManager.PointDataMapper.GetBuildingEnergyConsumptionPoints(b.BID, false);

            //            //if points exist here
            //            if (points.Any())
            //            {
            //                foreach (CW.Data.Point p in points)
            //                {
            //                    List<int> pt = new List<int>();
            //                    pt.Add(p.PID);


            //                    //Current Year (excluding today)   

            //                    ////get start date raw data
            //                    //var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfYear, firstOfYear.AddDays(1), false);

            //                    IEnumerable<GetRawPlotData> resultEnd = Enumerable.Empty<GetRawPlotData>();

            //                    ////get end date raw data if different
            //                    //if (DateTimeHelper.GetDiferenceBetweenDateTimesInDays(yesterdayDate, firstOfYear) > 0)
            //                    //{
            //                    //    resultEnd = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, yesterdayDate, endDate, false);
            //                    //}

            //                    ////if any values continues
            //                    //if (result.Any())
            //                    //{
            //                    //    //subtract (last - first) / sqft
            //                    //    double submeterValue = ((resultEnd.Any() ? resultEnd.Last().ConvertedRawValue : result.Last().ConvertedRawValue) - result.First().ConvertedRawValue) / sqft;

            //                    //    //sum 
            //                    //    yearValue = yearValue + submeterValue;
            //                    //}


            //                    //Current Month (excluding today)

            //                    //get start date raw data
            //                    //result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfMonth, firstOfMonth.AddDays(1), false);

            //                    //resultEnd = Enumerable.Empty<GetRawPlotData>();

            //                    ////get end date raw data if different
            //                    //if (DateTimeHelper.GetDiferenceBetweenDateTimesInDays(yesterdayDate, firstOfMonth) > 0)
            //                    //{
            //                    //    resultEnd = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, yesterdayDate, endDate, false);
            //                    //}

            //                    ////if any values continues
            //                    //if (result.Any())
            //                    //{
            //                    //    //subtract (last - first) / sqft
            //                    //    double submeterValue = ((resultEnd.Any() ? resultEnd.Last().ConvertedRawValue : result.Last().ConvertedRawValue) - result.First().ConvertedRawValue) / sqft;

            //                    //    //sum 
            //                    //    monthValue = monthValue + submeterValue;
            //                    //}

            //                    //Current Week (excluding today)

            //                    //get start date raw data
            //                    var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfWeek, firstOfWeek.AddDays(1), false);

            //                    resultEnd = Enumerable.Empty<GetRawPlotData>();

            //                    //get end date raw data if different
            //                    if (DateTimeHelper.GetDiferenceBetweenDateTimesInDays(yesterdayDate, firstOfWeek) > 0)
            //                    {
            //                        resultEnd = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, yesterdayDate, endDate, false);
            //                    }

            //                    //if any values continues
            //                    if (result.Any())
            //                    {
            //                        //subtract (last - first) / sqft
            //                        double submeterValue = ((resultEnd.Any() ? resultEnd.Last().ConvertedRawValue : result.Last().ConvertedRawValue) - result.First().ConvertedRawValue) / sqft;

            //                        //sum 
            //                        weekValue = weekValue + submeterValue;
            //                    }


            //                    //Yesterday

            //                    //get start date raw data
            //                    result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, yesterdayDate, endDate, false);

            //                    //if any values continues
            //                    if (result.Any())
            //                    {
            //                        //subtract (last - first) / sqft
            //                        double submeterValue = (result.Last().ConvertedRawValue - result.First().ConvertedRawValue) / sqft;

            //                        //sum 
            //                        yesterdayValue = yesterdayValue + submeterValue;
            //                    }
            //                }

            //                //if value not 0
            //                if (yearValue != 0)
            //                {
            //                    //add row
            //                    yearTable.Rows.Add(b.BuildingName, yearValue);
            //                }
            //                if (monthValue != 0)
            //                {
            //                    //add row
            //                    monthTable.Rows.Add(b.BuildingName, monthValue);
            //                }
            //                if (weekValue != 0)
            //                {
            //                    //add row
            //                    weekTable.Rows.Add(b.BuildingName, weekValue);
            //                }
            //            }
            //            else
            //            {
            //                //get point or points where point type is the power metric
            //                points = mDataManager.PointDataMapper.GetBuildingPowerConsumptionPoints(b.BID, false);

            //                //if points exist here
            //                if (points.Any())
            //                {
            //                    foreach (CW.Data.Point p in points)
            //                    {
            //                        List<int> pt = new List<int>();
            //                        pt.Add(p.PID);


            //                        //Current Year (excluding today)   
            //                        //var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfYear, endDate, false);

            //                        ////if any values continues
            //                        //if (result.Any())
            //                        //{
            //                        //    //submeter = (sum * (its samplinginterval / 60)) / sqft                                     
            //                        //    double submeterValue = (result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0)) / sqft;

            //                        //    //sum 
            //                        //    yearValue = yearValue + submeterValue;
            //                        //}

            //                        //Current Month (excluding today)   
            //                        //result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfMonth, endDate, false);

            //                        ////if any values continues
            //                        //if (result.Any())
            //                        //{
            //                        //    //submeter = (sum * (its samplinginterval / 60)) / sqft                                     
            //                        //    double submeterValue = (result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0)) / sqft;

            //                        //    //sum 
            //                        //    monthValue = monthValue + submeterValue;
            //                        //}

            //                        //Current Week (excluding today)   
            //                        var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfWeek, endDate, false);

            //                        //if any values continues
            //                        if (result.Any())
            //                        {
            //                            //submeter = (sum * (its samplinginterval / 60)) / sqft                                     
            //                            double submeterValue = (result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0)) / sqft;

            //                            //sum 
            //                            weekValue = weekValue + submeterValue;
            //                        }

            //                        //Yesterday
            //                        result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, yesterdayDate, endDate, false);

            //                        //if any values continues
            //                        if (result.Any())
            //                        {
            //                            //submeter = (sum * (its samplinginterval / 60)) / sqft                                     
            //                            double submeterValue = (result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0)) / sqft;

            //                            //sum 
            //                            yesterdayValue = yesterdayValue + submeterValue;
            //                        }
            //                    }

            //                    //if value not 0
            //                    if (yearValue != 0)
            //                    {
            //                        //add row
            //                        yearTable.Rows.Add(b.BuildingName, yearValue);
            //                    }
            //                    if (monthValue != 0)
            //                    {
            //                        //add row
            //                        monthTable.Rows.Add(b.BuildingName, monthValue);
            //                    }
            //                    if (weekValue != 0)
            //                    {
            //                        //add row
            //                        weekTable.Rows.Add(b.BuildingName, weekValue);
            //                    }
            //                    if (yesterdayValue != 0)
            //                    {
            //                        //add row
            //                        yesterdayTable.Rows.Add(b.BuildingName, yesterdayValue);
            //                    }
            //                }
            //            }
            //        }

            //        //TODO: add tables to cache
            //    }



            //    //TODO: get from cache only users buildings for plot


            //    //set has data flags
            //    hasYearPlotData = yearTable.Rows.Count > 0;
            //    hasMonthPlotData = monthTable.Rows.Count > 0;
            //    hasWeekPlotData = weekTable.Rows.Count > 0;
            //    hasYesterdayPlotData = yesterdayTable.Rows.Count > 0;

            //    //has year plot data-----
            //    if (hasYearPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        portfolioOverviewYearChart.ChartAreas["portfolioOverviewYearChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        portfolioOverviewYearChart.ChartAreas["portfolioOverviewYearChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (yearTable as System.ComponentModel.IListSource).GetList();

            //        portfolioOverviewYearChart.DataBindTable(IListTable, "Building");

            //        //set chart title
            //        portfolioOverviewYearChart.Titles["portfolioOverviewYearTitle"].Text = "Energy Consumption for all Accessible Buildings for the Current Year. (Jan 1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

            //        portfolioOverviewYearChart.Series[0].Color = Color.ForestGreen;

            //        double yearTotal = 0;

            //        foreach (DataRow r in yearTable.Rows)
            //        {
            //            yearTotal = yearTotal + Convert.ToDouble(r[1]);
            //        }

            //        portfolioYearTotal.InnerText = Math.Round(yearTotal, 2).ToString();
            //    }
            //    else
            //    {
            //        lblPortfolioOverviewYearResults.Text = noPlots;
            //    }

            //    portfolioOverviewYearChart.Visible = hasYearPlotData;


            //    //has month plot data-----
            //    if (hasMonthPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        portfolioOverviewMonthChart.ChartAreas["portfolioOverviewMonthChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        portfolioOverviewMonthChart.ChartAreas["portfolioOverviewMonthChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (monthTable as System.ComponentModel.IListSource).GetList();

            //        portfolioOverviewMonthChart.DataBindTable(IListTable, "Building");

            //        //set chart title
            //        portfolioOverviewMonthChart.Titles["portfolioOverviewMonthTitle"].Text = "Energy Consumption for all Accessible Buildings for the Current Month. (1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";                    

            //        portfolioOverviewMonthChart.Series[0].Color = Color.ForestGreen;

            //        double monthTotal = 0;

            //        foreach (DataRow r in monthTable.Rows)
            //        {
            //            monthTotal = monthTotal + Convert.ToDouble(r[1]);
            //        }

            //        portfolioMonthTotal.InnerText = Math.Round(monthTotal, 2).ToString();
            //    }
            //    else
            //    {
            //        lblPortfolioOverviewMonthResults.Text = noPlots;
            //    }

            //    portfolioOverviewMonthChart.Visible = hasMonthPlotData;


            //    //has week plot data------
            //    if (hasWeekPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        portfolioOverviewWeekChart.ChartAreas["portfolioOverviewWeekChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        portfolioOverviewWeekChart.ChartAreas["portfolioOverviewWeekChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (weekTable as System.ComponentModel.IListSource).GetList();

            //        portfolioOverviewWeekChart.DataBindTable(IListTable, "Building");

            //        //set chart title
            //        portfolioOverviewWeekChart.Titles["portfolioOverviewWeekTitle"].Text = "Energy Consumption for all Accessible Buildings for the Current Week. (Sunday through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

            //        portfolioOverviewWeekChart.Series[0].Color = Color.ForestGreen;

            //        double weekTotal = 0;

            //        foreach (DataRow r in weekTable.Rows)
            //        {
            //            weekTotal = weekTotal + Convert.ToDouble(r[1]);
            //        }

            //        portfolioWeekTotal.InnerText = Math.Round(weekTotal, 2).ToString();
            //    }
            //    else
            //    {
            //        lblPortfolioOverviewWeekResults.Text = noPlots;
            //    }

            //    portfolioOverviewWeekChart.Visible = hasWeekPlotData;


            //    //has yesterday plot data------
            //    if (hasYesterdayPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        portfolioOverviewYesterdayChart.ChartAreas["portfolioOverviewYesterdayChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        portfolioOverviewYesterdayChart.ChartAreas["portfolioOverviewYesterdayChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (yesterdayTable as System.ComponentModel.IListSource).GetList();

            //        portfolioOverviewYesterdayChart.DataBindTable(IListTable, "Building");

            //        //set chart title
            //        portfolioOverviewYesterdayChart.Titles["portfolioOverviewYesterdayTitle"].Text = "Energy Consumption for all Accessible Buildings for Yesterday.";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

            //        portfolioOverviewYesterdayChart.Series[0].Color = Color.ForestGreen;

            //        double yesterdayTotal = 0;

            //        foreach (DataRow r in yesterdayTable.Rows)
            //        {
            //            yesterdayTotal = yesterdayTotal + Convert.ToDouble(r[1]);
            //        }

            //        portfolioYesterdayTotal.InnerText = Math.Round(yesterdayTotal, 2).ToString();
            //    }
            //    else
            //    {
            //        lblPortfolioOverviewYesterdayResults.Text = noPlots;
            //    }

            //    portfolioOverviewYesterdayChart.Visible = hasYesterdayPlotData;
            //}

        #endregion

        #region Portfolio Data Plot Methods

            /// <summary>
            /// Plots the buildings selected.
            /// </summary>
            protected void PlotPortfolioDataBuildings(List<int> bids)
            {
                bool hasPlotData = false;

                //continue if one or more buildings were selected
                if (bids.Any())
                {
                    //clear chart and plot if data                    
                    portfolioOverviewYearChart.Series.Clear();
                    portfolioOverviewMonthChart.Series.Clear();
                    portfolioOverviewWeekChart.Series.Clear();

                    //new table format
                    DataTable table = new DataTable();
                    table.Columns.Add("Building", typeof(string));
                    table.Columns.Add("Actual", typeof(double));

                    //TODO: targeted??
                    //table.Columns.Add("Targeted", typeof(double));

                    //set dates
                    DateTime startDate = Convert.ToDateTime(txtPortfolioDataStartDate.Text);
                    DateTime endDate = Convert.ToDateTime(txtPortfolioDataEndDate.Text).AddDays(1);

                    List<CW.Data.Point> points = new List<CW.Data.Point>();

                    //foreach selected bid
                    foreach (int bid in bids)
                    {
                        double value = 0;

                        //get point or points where point type is the energy metric
                        points = mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(bid, CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID);
                        if (!points.Any())
                        {
                            points = mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(bid, CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID);
                        }

                        //if points exist here
                        if (points.Any())
                        {
                            //if metric type is 2 then divide by sqft
                            //get building sqft
                            int sqft = Convert.ToInt32(ddlPortfolioDataMetric.SelectedValue) == 2 ? mDataManager.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false).Sqft : 1;

                            foreach (CW.Data.Point p in points)
                            {
                                List<int> pt = new List<int>();
                                pt.Add(p.PID);

                                //get start date raw data
                                var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, startDate, startDate.AddDays(1));

                                IEnumerable<GetRawPlotData> resultEnd = Enumerable.Empty<GetRawPlotData>();

                                //get end date raw data if different
                                if (DateTimeHelper.GetDiferenceBetweenDateTimesInDays(endDate.AddDays(-1), startDate) > 0)
                                {
                                    resultEnd = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, endDate.AddDays(-1), endDate);
                                }

                                //if any values continues
                                if (result.Any())
                                {
                                    //subtract (last - first) / sqft
                                    double submeterValue = ((resultEnd.Any() ? resultEnd.Last().ConvertedRawValue : result.Last().ConvertedRawValue) - result.First().ConvertedRawValue) / sqft;

                                    //sum 
                                    value = value + submeterValue;
                                }
                            }

                            if (value != 0)
                            {
                                //get building name
                                var buildingName = lbPortfolioDataBuildings.Items.FindByValue(bid.ToString()).Text;

                                //add row
                                table.Rows.Add(buildingName, value);

                                hasPlotData = true;
                            }
                        }
                        else
                        {
                            //get point or points where point type is the power metric
                            points = mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(bid, CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID);
                            if (!points.Any())
                            {
                                mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(bid, CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID);
                            }

                            //if points exist here
                            if (points.Any())
                            {
                                //if metric type is 2 then divide by sqft
                                //get building sqft
                                int sqft = Convert.ToInt32(ddlPortfolioDataMetric.SelectedValue) == 2 ? mDataManager.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false).Sqft : 1;

                                foreach (CW.Data.Point p in points)
                                {
                                    List<int> pt = new List<int>();
                                    pt.Add(p.PID);

                                    var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, startDate, endDate);

                                    //if any values continues
                                    if (result.Any())
                                    {
                                        //submeter = (sum * (its samplinginterval / 60)) / sqft                                     
                                        double submeterValue = (result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0)) / sqft;

                                        //sum 
                                        value = value + submeterValue;
                                    }
                                }

                                //if value not 0
                                if (value != 0)
                                {
                                    //get building name from first value
                                    var buildingName = lbPortfolioDataBuildings.Items.FindByValue(bid.ToString()).Text;

                                    //add row
                                    table.Rows.Add(buildingName, value);

                                    hasPlotData = true;
                                }
                            }
                        }
                    }

                    if (hasPlotData)
                    {
                        //TODO: on front end after microsoft bug fix.
                        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                        portfolioDataChart.ChartAreas["portfolioDataChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                        portfolioDataChart.ChartAreas["portfolioDataChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                        //set title based on metric
                        portfolioDataChart.ChartAreas["portfolioDataChartArea"].AxisY.Title = ddlPortfolioDataMetric.SelectedItem.Text;

                        //convert datatable to a IEnumerable form
                        var IListTable = (table as System.ComponentModel.IListSource).GetList();

                        portfolioDataChart.DataBindTable(IListTable, "Building");

                        //set chart title
                        portfolioDataChart.Titles["portfolioDataTitle"].Text = "Energy Consumption per Selected Building from " + txtPortfolioDataStartDate.Text + " to " + txtPortfolioDataEndDate.Text + ".";

                        portfolioDataChart.Series[0].Color = Color.ForestGreen;

                        double total = 0;

                        foreach (DataRow r in table.Rows)
                        {
                            total = total + Convert.ToDouble(r[1]);
                        }

                        spanPortfolioDataTotal.InnerText = Math.Round(total, 2).ToString();
                    }
                    else
                    {
                        lblPortfolioDataResults.Text = noPlots;
                    }

                    portfolioDataChart.Visible = hasPlotData;
                    divPortfolioDataResults.Visible = !hasPlotData;
                    portfolioDataBlocks.Visible = hasPlotData;
                }
            }

            /// <summary>
            /// PRE CACHING
            /// Plots the buildings selected.
            /// </summary>
            //protected void PlotPortfolioDataBuildings(List<int> bids)
            //{
            //    bool hasPlotData = false;

            //    //continue if one or more buildings were selected
            //    if (bids.Any())
            //    {
            //        //clear chart and plot if data                    
            //        portfolioOverviewYearChart.Series.Clear();
            //        portfolioOverviewMonthChart.Series.Clear();
            //        portfolioOverviewWeekChart.Series.Clear();

            //        //new table format
            //        DataTable table = new DataTable();
            //        table.Columns.Add("Building", typeof(string));
            //        table.Columns.Add("Actual", typeof(double));

            //        //TODO: targeted??
            //        //table.Columns.Add("Targeted", typeof(double));

            //        List<CW.Data.Point> points = new List<CW.Data.Point>();

            //        //foreach selected bid
            //        foreach (int bid in bids)
            //        {
            //            double value = 0;

            //            //get point or points where point type is the energy metric
            //            points = mDataManager.PointDataMapper.GetBuildingEnergyConsumptionPoints(bid, false);

            //            //if points exist here
            //            if (points.Any())
            //            {
            //                //get building sqft
            //                int sqft = mDataManager.BuildingDataMapper.GetBuildingByID(bid, false, false, false, false).Sqft;

            //                foreach (CW.Data.Point p in points)
            //                {
            //                    List<int> pt = new List<int>();
            //                    pt.Add(p.PID);

            //                    //get start date raw data
            //                    var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, Convert.ToDateTime(txtPortfolioDataStartDate.Text), Convert.ToDateTime(txtPortfolioDataStartDate.Text).AddDays(1), false);

            //                    IEnumerable<GetRawPlotData> resultEnd = Enumerable.Empty<GetRawPlotData>();

            //                    //get end date raw data if different
            //                    if (DateTimeHelper.GetDiferenceBetweenDateTimesInDays(Convert.ToDateTime(txtPortfolioDataEndDate.Text).AddDays(-1), Convert.ToDateTime(txtPortfolioDataStartDate.Text)) > 0)
            //                    {
            //                        resultEnd = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, Convert.ToDateTime(txtPortfolioDataEndDate.Text), Convert.ToDateTime(txtPortfolioDataEndDate.Text).AddDays(1), false);
            //                    }

            //                    //if any values continues
            //                    if (result.Any())
            //                    {
            //                        //subtract (last - first) / sqft
            //                        double submeterValue = ((resultEnd.Any() ? resultEnd.Last().ConvertedRawValue : result.Last().ConvertedRawValue) - result.First().ConvertedRawValue) / sqft;

            //                        //sum 
            //                        value = value + submeterValue;
            //                    }
            //                }

            //                if (value != 0)
            //                {
            //                    //get building name
            //                    var buildingName = lbPortfolioDataBuildings.Items.FindByValue(bid.ToString()).Text;

            //                    //add row
            //                    table.Rows.Add(buildingName, value);

            //                    hasPlotData = true;
            //                }
            //            }
            //            else
            //            {
            //                //get point or points where point type is the power metric
            //                points = mDataManager.PointDataMapper.GetBuildingPowerConsumptionPoints(bid, false);

            //                //if points exist here
            //                if (points.Any())
            //                {
            //                    //get building sqft
            //                    int sqft = mDataManager.BuildingDataMapper.GetBuildingByID(bid, false, false, false, false).Sqft;

            //                    foreach (CW.Data.Point p in points)
            //                    {
            //                        List<int> pt = new List<int>();
            //                        pt.Add(p.PID);

            //                        var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, Convert.ToDateTime(txtPortfolioDataStartDate.Text), Convert.ToDateTime(txtPortfolioDataEndDate.Text), false);

            //                        //if any values continues
            //                        if (result.Any())
            //                        {
            //                            //submeter = (sum * (its samplinginterval / 60)) / sqft                                     
            //                            double submeterValue = (result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0)) / sqft;

            //                            //sum 
            //                            value = value + submeterValue;
            //                        }
            //                    }

            //                    //if value not 0
            //                    if (value != 0)
            //                    {
            //                        //get building name from first value
            //                        var buildingName = lbPortfolioDataBuildings.Items.FindByValue(bid.ToString()).Text;

            //                        //add row
            //                        table.Rows.Add(buildingName, value);

            //                        hasPlotData = true;
            //                    }
            //                }
            //            }
            //        }

            //        if (hasPlotData)
            //        {
            //            //TODO: on front end after microsoft bug fix.
            //            //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //            portfolioDataChart.ChartAreas["portfolioDataChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //            portfolioDataChart.ChartAreas["portfolioDataChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //            //convert datatable to a IEnumerable form
            //            var IListTable = (table as System.ComponentModel.IListSource).GetList();

            //            portfolioDataChart.DataBindTable(IListTable, "Building");

            //            //set chart title
            //            portfolioDataChart.Titles["portfolioDataTitle"].Text = "Energy Consumption per Selected Building from " + txtPortfolioDataStartDate.Text + " to " + txtPortfolioDataEndDate.Text + ".";

            //            portfolioDataChart.Series[0].Color = Color.ForestGreen;
            //        }
            //        else
            //        {
            //            lblPortfolioDataResults.Text = noPlots;
            //        }

            //        portfolioDataChart.Visible = hasPlotData;
            //        divPortfolioDataResults.Visible = !hasPlotData;
            //    }
            //}

            /// <summary>
            /// Removed the unselected building plots.
            /// </summary>
            //protected void RemovePortfolioDataBuildingsPlots(List<int> bids)
            //{
            //    if (bids.Any())
            //    {
            //        //get buildings
            //        List<Building> buildings = BuildingDataMapper.GetBuildingsByBIDList(bids);

            //        //foreach pointName
            //        foreach (Building b in buildings)
            //        {
            //            string name = b.BuildingName; //p.PointName + " (" + p.EngUnits + ")";

            //            //if series exists remove... which might not be the cases if no data was ploted even though the point was selected previously
            //            if (portfolioDataChart.Series.IndexOf(name) != -1)
            //            {
            //                Series s = portfolioDataChart.Series[name];
            //                portfolioDataChart.Series.Remove(s);
            //            }
            //        }
            //    }
            //}

        #endregion

        #region Building Data Plot Methods

            /// <summary>
            /// Plots the bulding selected.
            /// </summary>
            protected void PlotBuildingDataBuilding(int bid)
            {
                bool hasPlotData = false;

                //clear chart and plot if data
                buildingDataChart.Series.Clear();

                //new table format
                DataTable table = new DataTable();
                table.Columns.Add("PointName", typeof(string));
                table.Columns.Add("Actual", typeof(double));

                //TODO: targeted??
                //table.Columns.Add("Targeted", typeof(double));

                //set dates                
                DateTime startDate = Convert.ToDateTime(txtBuildingDataStartDate.Text);
                DateTime endDate = Convert.ToDateTime(txtBuildingDataEndDate.Text).AddDays(1);

                //get building
                Building building = mDataManager.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false);

                //if metric type is 2 then divide by sqft
                int sqft = Convert.ToInt32(ddlBuildingDataMetric.SelectedValue) == 2 ? building.Sqft : 1;

                List<CW.Data.Point> points = new List<CW.Data.Point>();

                //get point or points where point type is the power metric
                points = mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(building.BID, CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID);
                if(!points.Any()){
                    mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(building.BID, CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID);
                }

                //if points exist here
                if (points.Any())
                {
                    foreach (CW.Data.Point p in points)
                    {
                        List<int> pt = new List<int>();
                        pt.Add(p.PID);

                        var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, startDate, endDate);

                        //if any values continues
                        if (result.Any())
                        {
                            //submeter = sum * (its samplinginterval / 60)                                     
                            double submeterValue = result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0) / sqft;

                            if (submeterValue != 0)
                            {
                                //add row
                                table.Rows.Add(p.PointName, submeterValue);

                                hasPlotData = true;
                            }
                        }
                    }
                }
                else
                {
                    //get point or points where point type is the energy  metric
                    points = mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(building.BID, CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID);
                    if(!points.Any()){
                        mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(building.BID, CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID);
                    }

                    if (points.Any())
                    {
                        foreach (CW.Data.Point p in points)
                        {
                            //get first value of first day, get last vaue of last day for all pids

                            List<int> pt = new List<int>();
                            pt.Add(p.PID);

                            //get start date raw data
                            var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, startDate, startDate.AddDays(1));

                            IEnumerable<GetRawPlotData> resultEnd = Enumerable.Empty<GetRawPlotData>();

                            //get end date raw data if different
                            if (DateTimeHelper.GetDiferenceBetweenDateTimesInDays(endDate.AddDays(-1), startDate) > 0)
                            {
                                resultEnd = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, endDate.AddDays(-1), endDate);
                            }

                            //if any values continues
                            if (result.Any())
                            {
                                //subtract last - first
                                double submeterValue = (resultEnd.Any() ? resultEnd.Last().ConvertedRawValue : result.Last().ConvertedRawValue) - result.First().ConvertedRawValue;

                                if (submeterValue != 0)
                                {
                                    //add row
                                    table.Rows.Add(p.PointName, submeterValue);

                                    hasPlotData = true;
                                }
                            }
                        }
                    }
                }

                if (hasPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    buildingDataChart.ChartAreas["buildingDataChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingDataChart.ChartAreas["buildingDataChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set title based on metric
                    buildingDataChart.ChartAreas["buildingDataChartArea"].AxisY.Title = ddlBuildingDataMetric.SelectedItem.Text;


                    //convert datatable to a IEnumerable form
                    var IListTable = (table as System.ComponentModel.IListSource).GetList();

                    buildingDataChart.DataBindTable(IListTable, "PointName");

                    //set chart title
                    buildingDataChart.Titles["buildingDataTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter from " + txtBuildingDataStartDate.Text + " to " + txtBuildingDataEndDate.Text + ".";

                    buildingDataChart.Series[0].Color = Color.ForestGreen;

                    double total = 0;

                    foreach (DataRow r in table.Rows)
                    {
                        total = total + Convert.ToDouble(r[1]);
                    }

                    spanBuildingDataTotal.InnerText = Math.Round(total, 2).ToString();
                }
                else
                {
                    lblBuildingDataResults.Text = noPlots;
                }

                buildingDataChart.Visible = hasPlotData;
                divBuildingDataResults.Visible = !hasPlotData;
                buildingDataBlocks.Visible = hasPlotData;
            }

            /// <summary>
            /// PRE CACHING
            /// Plots the bulding selected.
            /// </summary>
            //protected void PlotBuildingDataBuilding(int bid)
            //{
            //    bool hasPlotData = false;

            //    //clear chart and plot if data
            //    buildingDataChart.Series.Clear();

            //    //new table format
            //    DataTable table = new DataTable();
            //    table.Columns.Add("PointName", typeof(string));
            //    table.Columns.Add("Actual", typeof(double));

            //    //TODO: targeted??
            //    //table.Columns.Add("Targeted", typeof(double));

            //    //get building
            //    Building building = mDataManager.BuildingDataMapper.GetBuildingByID(bid, false, false, false, false);

            //    List<CW.Data.Point> points = new List<CW.Data.Point>();

            //    //get point or points where point type is the power metric
            //    points = mDataManager.PointDataMapper.GetBuildingPowerConsumptionPoints(bid, true);

            //    //if points exist here
            //    if (points.Any())
            //    {
            //        foreach (CW.Data.Point p in points)
            //        {
            //            List<int> pt = new List<int>();
            //            pt.Add(p.PID);

            //            var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, Convert.ToDateTime(txtBuildingDataStartDate.Text), Convert.ToDateTime(txtBuildingDataEndDate.Text), false);

            //            //if any values continues
            //            if (result.Any())
            //            {
            //                //submeter = sum * (its samplinginterval / 60)                                     
            //                double submeterValue = result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0);

            //                if (submeterValue != 0)
            //                {
            //                    //add row
            //                    table.Rows.Add(p.PointName, submeterValue);

            //                    hasPlotData = true;
            //                }
            //            }
            //        }
            //    }
            //    else
            //    {
            //        //get point or points where point type is the energy  metric
            //        points = mDataManager.PointDataMapper.GetBuildingEnergyConsumptionPoints(bid, true);

            //        if (points.Any())
            //        {
            //            foreach (CW.Data.Point p in points)
            //            {
            //                //get first value of first day, get last vaue of last day for all pids

            //                List<int> pt = new List<int>();
            //                pt.Add(p.PID);

            //                //get start date raw data
            //                var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, Convert.ToDateTime(txtBuildingDataStartDate.Text), Convert.ToDateTime(txtBuildingDataStartDate.Text).AddDays(1), false);

            //                IEnumerable<GetRawPlotData> resultEnd = Enumerable.Empty<GetRawPlotData>();

            //                //get end date raw data if different
            //                if (DateTimeHelper.GetDiferenceBetweenDateTimesInDays(Convert.ToDateTime(txtBuildingDataEndDate.Text).AddDays(-1), Convert.ToDateTime(txtBuildingDataStartDate.Text)) > 0)
            //                {
            //                    resultEnd = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, Convert.ToDateTime(txtBuildingDataEndDate.Text), Convert.ToDateTime(txtBuildingDataEndDate.Text).AddDays(1), false);
            //                }

            //                //if any values continues
            //                if (result.Any())
            //                {
            //                    //subtract last - first
            //                    double submeterValue = (resultEnd.Any() ? resultEnd.Last().ConvertedRawValue : result.Last().ConvertedRawValue) - result.First().ConvertedRawValue;

            //                    if (submeterValue != 0)
            //                    {
            //                        //add row
            //                        table.Rows.Add(p.PointName, submeterValue);

            //                        hasPlotData = true;
            //                    }
            //                }
            //            }
            //        }
            //    }

            //    if (hasPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        buildingDataChart.ChartAreas["buildingDataChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingDataChart.ChartAreas["buildingDataChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (table as System.ComponentModel.IListSource).GetList();

            //        buildingDataChart.DataBindTable(IListTable, "PointName");

            //        //set chart title
            //        buildingDataChart.Titles["buildingDataTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter from " + txtBuildingDataStartDate.Text + " to " + txtBuildingDataEndDate.Text + ".";

            //        buildingDataChart.Series[0].Color = Color.ForestGreen;
            //    }
            //    else
            //    {
            //        lblBuildingDataResults.Text = noPlots;
            //    }

            //    buildingDataChart.Visible = hasPlotData;
            //    divBuildingDataResults.Visible = !hasPlotData;
            //    buildingDataBlocks.Visible = hasPlotData;
            //}

        #endregion

        #region Building Overview Methods

            /// <summary>
            /// Plots the buildings selected.
            /// </summary>
            protected void PlotBuildingOverview(int bid)
            {
                bool hasYearPlotData = false, hasMonthPlotData = false, hasWeekPlotData = false, hasYesterdayPlotData = false;

                //clear chart and plot if data
                buildingOverviewTotalYearChart.Series.Clear();
                buildingOverviewTotalMonthChart.Series.Clear();
                buildingOverviewTotalWeekChart.Series.Clear();
                buildingOverviewTotalYesterdayChart.Series.Clear();
                buildingOverviewDrillDownYearChart.Series.Clear();
                buildingOverviewDrillDownMonthChart.Series.Clear();
                buildingOverviewDrillDownWeekChart.Series.Clear();
                buildingOverviewDrillDownYesterdayChart.Series.Clear();           

                //new tables format
                DataTable yearTotalTable = new DataTable();
                yearTotalTable.Columns.Add("PointName", typeof(string));
                yearTotalTable.Columns.Add("Actual", typeof(double));
                DataTable monthTotalTable = new DataTable();
                monthTotalTable.Columns.Add("PointName", typeof(string));
                monthTotalTable.Columns.Add("Actual", typeof(double));
                DataTable weekTotalTable = new DataTable();
                weekTotalTable.Columns.Add("PointName", typeof(string));
                weekTotalTable.Columns.Add("Actual", typeof(double));
                DataTable yesterdayTotalTable = new DataTable();
                yesterdayTotalTable.Columns.Add("PointName", typeof(string));
                yesterdayTotalTable.Columns.Add("Actual", typeof(double));

                //TODO: targeted??
                //table.Columns.Add("Targeted", typeof(double));

                List<CW.Data.Point> points = new List<CW.Data.Point>();

                DateTime yesterdayDate = DateTime.UtcNow.Date.AddDays(-1);
                DateTime endDate = DateTime.UtcNow.Date;
                DateTime firstOfYear = DateTimeHelper.GetFirstOfYear(DateTime.UtcNow);
                DateTime firstOfMonth = DateTimeHelper.GetFirstOfMonth(DateTime.UtcNow);
                DateTime firstOfWeek = DateTimeHelper.GetLastSunday(DateTime.UtcNow);

                //double yearValue = 0, monthValue = 0, weekValue = 0, yesterdayValue = 0;
                IEnumerable<PerformanceResult> results = Enumerable.Empty<PerformanceResult>();
                IEnumerable<PerformanceResult> tempFilteredResults = Enumerable.Empty<PerformanceResult>();
                IEnumerable<PerformanceResult> tempSubFilteredResults = Enumerable.Empty<PerformanceResult>();

                //get building
                Building b = mDataManager.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false);

                //GET FROM CACHE
                results = mDataManager.PerformanceDataMapper.GetPerformanceResults(b);

                //if points exist here
                if (results != null && results.Any())
                {
                    //if metric type is 2 then divide by sqft
                    if (Convert.ToInt32(ddlBuildingOverviewMetric.SelectedValue) == 2)
                    {
                        foreach (PerformanceResult r in results)
                        {
                            r.Value = r.Value / Convert.ToDouble(b.Sqft);
                        }
                    }


                    //Current Year Total (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);

                    if (tempFilteredResults.Any())
                    {
                        foreach (PerformanceResult pr in tempFilteredResults)
                        {
                            //add row
                            yearTotalTable.Rows.Add(pr.PointName, pr.Value);
                        }
                    }


                    //Current Year Drilldown (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Year);

                    if (tempFilteredResults.Any())
                    {
                        List<int> pids = tempFilteredResults.Select(p => p.PID).Distinct().ToList();

                        foreach (int pid in pids)
                        {
                            //sub temp results
                            tempSubFilteredResults = tempFilteredResults.Where(r => r.PID == pid).ToList();

                            string shortName = tempSubFilteredResults.First().PointName;

                            //create drilldown chart series, set chart type and name
                            var series =
                            new Series
                            {
                                ChartType = SeriesChartType.StackedColumn,
                                Name = shortName,
                                ToolTip = "X: #VALX, Y: #VALY",
                                LegendToolTip = shortName,
                                LegendText = shortName,
                            };

                            series.Points.DataBindXY(tempSubFilteredResults, "BreakdownFactorOfTime", tempSubFilteredResults, "Value");

                            //add series to chart
                            buildingOverviewDrillDownYearChart.Series.Add(series);
                        }
                    }



                    //Current Month Total (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);

                    if (tempFilteredResults.Any())
                    {
                        foreach (PerformanceResult pr in tempFilteredResults)
                        {
                            //add row
                            monthTotalTable.Rows.Add(pr.PointName, pr.Value);
                        }
                    }


                    //Current Month Drilldown (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Month);

                    if (tempFilteredResults.Any())
                    {
                        List<int> pids = tempFilteredResults.Select(p => p.PID).Distinct().ToList();

                        foreach (int pid in pids)
                        {
                            //sub temp results
                            tempSubFilteredResults = tempFilteredResults.Where(r => r.PID == pid).ToList();

                            string shortName = tempSubFilteredResults.First().PointName;

                            //create drilldown chart series, set chart type and name
                            var series =
                            new Series
                            {
                                ChartType = SeriesChartType.StackedColumn,
                                Name = shortName,
                                ToolTip = "X: #VALX, Y: #VALY",
                                LegendToolTip = shortName,
                                LegendText = shortName,
                            };

                            series.Points.DataBindXY(tempSubFilteredResults, "BreakdownFactorOfTime", tempSubFilteredResults, "Value");

                            //add series to chart
                            buildingOverviewDrillDownMonthChart.Series.Add(series);
                        }
                    }



                    //Current Week Total (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);

                    if (tempFilteredResults.Any())
                    {
                        foreach (PerformanceResult pr in tempFilteredResults)
                        {
                            //add row
                            weekTotalTable.Rows.Add(pr.PointName, pr.Value);
                        }
                    }


                    //Current Week Drilldown (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Week);

                    if (tempFilteredResults.Any())
                    {
                        List<int> pids = tempFilteredResults.Select(p => p.PID).Distinct().ToList();

                        foreach (int pid in pids)
                        {
                            //sub temp results
                            tempSubFilteredResults = tempFilteredResults.Where(r => r.PID == pid).ToList();

                            string shortName = tempSubFilteredResults.First().PointName;

                            //create drilldown chart series, set chart type and name
                            var series =
                            new Series
                            {
                                ChartType = SeriesChartType.StackedColumn,
                                Name = shortName,
                                ToolTip = "X: #VALX, Y: #VALY",
                                LegendToolTip = shortName,
                                LegendText = shortName,
                            };

                            series.Points.DataBindXY(tempSubFilteredResults, "BreakdownFactorOfTime", tempSubFilteredResults, "Value");

                            //add series to chart
                            buildingOverviewDrillDownWeekChart.Series.Add(series);
                        }
                    }




                    //Current Yesterday Total (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Total && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);

                    if (tempFilteredResults.Any())
                    {
                        foreach (PerformanceResult pr in tempFilteredResults)
                        {
                            //add row
                            yesterdayTotalTable.Rows.Add(pr.PointName, pr.Value);
                        }
                    }


                    //Current Yesterday Drilldown (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricPowerPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == CommonBusinessConstants.PointTypeConstants.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == CW.Data.CommonDataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == CW.Data.CommonDataConstants.PerformanceRange.Yesterday);

                    if (tempFilteredResults.Any())
                    {
                        List<int> pids = tempFilteredResults.Select(p => p.PID).Distinct().ToList();

                        foreach (int pid in pids)
                        {
                            //sub temp results
                            tempSubFilteredResults = tempFilteredResults.Where(r => r.PID == pid).ToList();

                            string shortName = tempSubFilteredResults.First().PointName;

                            //create drilldown chart series, set chart type and name
                            var series =
                            new Series
                            {
                                Name = shortName,
                                ToolTip = "X: #VALX, Y: #VALY{0:00}",
                                LegendToolTip = shortName,
                                LegendText = shortName,
                            };                            

                            //add drilldown series

                            //set axis intervaltype
                            //buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Hours;

                            //set axis format:  g = (short date and time)
                            buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.LabelStyle.Format = "g";

                            //buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].Area3DStyle.Enable3D = true;

                            series.Points.DataBindXY(tempSubFilteredResults, "BreakdownFactorOfTime", tempSubFilteredResults, "Value");

                            //add series to chart
                            buildingOverviewDrillDownYesterdayChart.Series.Add(series);
                        }

                        //This ensures all data series have the same number of data points by filling in any empty spaces with 'fake' points.                       
                        buildingOverviewDrillDownYesterdayChart.AlignDataPointsByAxisLabel();

                        //set chart type after AlignDataPointsByAxisLabel
                        foreach (Series series in buildingOverviewDrillDownYesterdayChart.Series)
                        {
                            series.ChartType = SeriesChartType.StackedArea;
                        }
                    }
                }


                //set has data flags
                hasYearPlotData = yearTotalTable.Rows.Count > 0;
                hasMonthPlotData = monthTotalTable.Rows.Count > 0;
                hasWeekPlotData = weekTotalTable.Rows.Count > 0;
                hasYesterdayPlotData = yesterdayTotalTable.Rows.Count > 0;


                //has year plot data-----
                if (hasYearPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    buildingOverviewTotalYearChart.ChartAreas["buildingOverviewTotalYearChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewTotalYearChart.ChartAreas["buildingOverviewTotalYearChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewDrillDownYearChart.ChartAreas["buildingOverviewDrillDownYearChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewDrillDownYearChart.ChartAreas["buildingOverviewDrillDownYearChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set titles based on metric
                    buildingOverviewTotalYearChart.ChartAreas["buildingOverviewTotalYearChartArea"].AxisY.Title = 
                            buildingOverviewDrillDownYearChart.ChartAreas["buildingOverviewDrillDownYearChartArea"].AxisY.Title = ddlBuildingOverviewMetric.SelectedItem.Text;                    


                    //convert datatable to a IEnumerable form
                    var IListTable = (yearTotalTable as System.ComponentModel.IListSource).GetList();
                    buildingOverviewTotalYearChart.DataBindTable(IListTable, "PointName");
                    //IListTable = (yearDrillDownTable as System.ComponentModel.IListSource).GetList();
                    //buildingOverviewDrillDownYearChart.DataBindTable(IListTable, "Month");

                    //set chart title
                    buildingOverviewTotalYearChart.Titles["buildingOverviewTotalYearTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Year. (Jan 1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
                    buildingOverviewDrillDownYearChart.Titles["buildingOverviewDrillDownYearTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Year Monthly. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                    buildingOverviewTotalYearChart.Series[0].Color = Color.ForestGreen;
                    buildingOverviewDrillDownYearChart.Series[0].Color = Color.ForestGreen;

                    double yearTotal = 0;

                    foreach (DataRow r in yearTotalTable.Rows)
                    {
                        yearTotal = yearTotal + Convert.ToDouble(r[1]);
                    }

                    spanBuildingOverviewTotalYear.InnerText = Math.Round(yearTotal, 2).ToString();
                }
                else
                {
                    lblBuildingOverviewTotalYearResults.Text = noPlots;
                    lblBuildingOverviewDrillDownYearResults.Text = noPlots;
                }

                buildingOverviewTotalYearChart.Visible = hasYearPlotData;
                buildingOverviewDrillDownYearChart.Visible = hasYearPlotData;
                lblBuildingOverviewTotalYearResults.Visible = !hasYearPlotData;
                lblBuildingOverviewDrillDownYearResults.Visible = !hasYearPlotData;




                //has month plot data-----
                if (hasMonthPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    buildingOverviewTotalMonthChart.ChartAreas["buildingOverviewTotalMonthChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewTotalMonthChart.ChartAreas["buildingOverviewTotalMonthChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewDrillDownMonthChart.ChartAreas["buildingOverviewDrillDownMonthChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewDrillDownMonthChart.ChartAreas["buildingOverviewDrillDownMonthChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set titles based on metric
                    buildingOverviewTotalMonthChart.ChartAreas["buildingOverviewTotalMonthChartArea"].AxisY.Title =
                            buildingOverviewDrillDownMonthChart.ChartAreas["buildingOverviewDrillDownMonthChartArea"].AxisY.Title = ddlBuildingOverviewMetric.SelectedItem.Text;       

                    //convert datatable to a IEnumerable form
                    var IListTable = (monthTotalTable as System.ComponentModel.IListSource).GetList();
                    buildingOverviewTotalMonthChart.DataBindTable(IListTable, "PointName");

                    //set chart title
                    buildingOverviewTotalMonthChart.Titles["buildingOverviewTotalMonthTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Month. (The 1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
                    buildingOverviewDrillDownMonthChart.Titles["buildingOverviewDrillDownMonthTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Month Daily. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                    buildingOverviewTotalMonthChart.Series[0].Color = Color.ForestGreen;
                    buildingOverviewDrillDownMonthChart.Series[0].Color = Color.ForestGreen;

                    double monthTotal = 0;

                    foreach (DataRow r in monthTotalTable.Rows)
                    {
                        monthTotal = monthTotal + Convert.ToDouble(r[1]);
                    }

                    spanBuildingOverviewTotalMonth.InnerText = Math.Round(monthTotal, 2).ToString();
                }
                else
                {
                    lblBuildingOverviewTotalMonthResults.Text = noPlots;
                    lblBuildingOverviewDrillDownMonthResults.Text = noPlots;
                }

                buildingOverviewTotalMonthChart.Visible = hasMonthPlotData;
                buildingOverviewDrillDownMonthChart.Visible = hasMonthPlotData;
                lblBuildingOverviewTotalMonthResults.Visible = !hasMonthPlotData;
                lblBuildingOverviewDrillDownMonthResults.Visible = !hasMonthPlotData;



                //has week plot data-----
                if (hasWeekPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    buildingOverviewTotalWeekChart.ChartAreas["buildingOverviewTotalWeekChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewTotalWeekChart.ChartAreas["buildingOverviewTotalWeekChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewDrillDownWeekChart.ChartAreas["buildingOverviewDrillDownWeekChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewDrillDownWeekChart.ChartAreas["buildingOverviewDrillDownWeekChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set titles based on metric
                    buildingOverviewTotalWeekChart.ChartAreas["buildingOverviewTotalWeekChartArea"].AxisY.Title =
                            buildingOverviewDrillDownWeekChart.ChartAreas["buildingOverviewDrillDownWeekChartArea"].AxisY.Title = ddlBuildingOverviewMetric.SelectedItem.Text;       

                    //convert datatable to a IEnumerable form
                    var IListTable = (weekTotalTable as System.ComponentModel.IListSource).GetList();
                    buildingOverviewTotalWeekChart.DataBindTable(IListTable, "PointName");
                    //IListTable = (weekDrillDownTable as System.ComponentModel.IListSource).GetList();
                    //buildingOverviewDrillDownWeekChart.DataBindTable(IListTable, "DayOfWeek");

                    //set chart title
                    buildingOverviewTotalWeekChart.Titles["buildingOverviewTotalWeekTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Week. (Sunday through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
                    buildingOverviewDrillDownWeekChart.Titles["buildingOverviewDrillDownWeekTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Week Daily. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                    buildingOverviewTotalWeekChart.Series[0].Color = Color.ForestGreen;
                    buildingOverviewDrillDownWeekChart.Series[0].Color = Color.ForestGreen;

                    double weekTotal = 0;

                    foreach (DataRow r in weekTotalTable.Rows)
                    {
                        weekTotal = weekTotal + Convert.ToDouble(r[1]);
                    }

                    spanBuildingOverviewTotalWeek.InnerText = Math.Round(weekTotal, 2).ToString();
                }
                else
                {
                    lblBuildingOverviewTotalWeekResults.Text = noPlots;
                    lblBuildingOverviewDrillDownWeekResults.Text = noPlots;
                }

                buildingOverviewTotalWeekChart.Visible = hasWeekPlotData;
                buildingOverviewDrillDownWeekChart.Visible = hasWeekPlotData;
                lblBuildingOverviewTotalWeekResults.Visible = !hasWeekPlotData;
                lblBuildingOverviewDrillDownWeekResults.Visible = !hasWeekPlotData;


                //has yesterday plot data-----
                if (hasYesterdayPlotData)
                {
                    //TODO: on front end after microsoft bug fix.
                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                    buildingOverviewTotalYesterdayChart.ChartAreas["buildingOverviewTotalYesterdayChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewTotalYesterdayChart.ChartAreas["buildingOverviewTotalYesterdayChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                    buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                    //set titles based on metric
                    buildingOverviewTotalYesterdayChart.ChartAreas["buildingOverviewTotalYesterdayChartArea"].AxisY.Title =
                            buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisY.Title = ddlBuildingOverviewMetric.SelectedItem.Text;

                    //convert datatable to a IEnumerable form
                    var IListTable = (yesterdayTotalTable as System.ComponentModel.IListSource).GetList();
                    buildingOverviewTotalYesterdayChart.DataBindTable(IListTable, "PointName");

                    //set chart title
                    buildingOverviewTotalYesterdayChart.Titles["buildingOverviewTotalYesterdayTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for Yesterday.";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
                    buildingOverviewDrillDownYesterdayChart.Titles["buildingOverviewDrillDownYesterdayTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for Yesterday over Time. (Stacked Area)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                    buildingOverviewTotalYesterdayChart.Series[0].Color = Color.ForestGreen;
                    buildingOverviewDrillDownYesterdayChart.Series[0].Color = Color.ForestGreen;

                    double yesterdayTotal = 0;

                    foreach (DataRow r in yesterdayTotalTable.Rows)
                    {
                        yesterdayTotal = yesterdayTotal + Convert.ToDouble(r[1]);
                    }

                    spanBuildingOverviewTotalYesterday.InnerText = Math.Round(yesterdayTotal, 2).ToString();
                }
                else
                {
                    lblBuildingOverviewTotalYesterdayResults.Text = noPlots;
                    lblBuildingOverviewDrillDownYesterdayResults.Text = noPlots;
                }

                buildingOverviewTotalYesterdayChart.Visible = hasYesterdayPlotData;
                buildingOverviewDrillDownYesterdayChart.Visible = hasYesterdayPlotData;
                lblBuildingOverviewTotalYesterdayResults.Visible = !hasYesterdayPlotData;
                lblBuildingOverviewDrillDownYesterdayResults.Visible = !hasYesterdayPlotData;
            }

            /// <summary>
            /// PRE CACHING
            /// Plots the buildings selected.
            /// </summary>
            //protected void PlotBuildingOverview(int bid)
            //{
            //    bool hasYearPlotData = false, hasMonthPlotData = false, hasWeekPlotData = false, hasYesterdayPlotData;

            //    //clear chart and plot if data
            //    buildingOverviewTotalYearChart.Series.Clear();
            //    buildingOverviewTotalMonthChart.Series.Clear();
            //    buildingOverviewTotalWeekChart.Series.Clear();
            //    buildingOverviewTotalYesterdayChart.Series.Clear();
            //    buildingOverviewDrillDownYearChart.Series.Clear();
            //    buildingOverviewDrillDownMonthChart.Series.Clear();
            //    buildingOverviewDrillDownWeekChart.Series.Clear();
            //    buildingOverviewDrillDownYesterdayChart.Series.Clear();

            //    //new tables format
            //    DataTable yearTotalTable = new DataTable();
            //    yearTotalTable.Columns.Add("PointName", typeof(string));
            //    yearTotalTable.Columns.Add("Actual", typeof(double));
            //    DataTable monthTotalTable = new DataTable();
            //    monthTotalTable.Columns.Add("PointName", typeof(string));
            //    monthTotalTable.Columns.Add("Actual", typeof(double));
            //    DataTable weekTotalTable = new DataTable();
            //    weekTotalTable.Columns.Add("PointName", typeof(string));
            //    weekTotalTable.Columns.Add("Actual", typeof(double));
            //    DataTable yesterdayTotalTable = new DataTable();
            //    yesterdayTotalTable.Columns.Add("PointName", typeof(string));
            //    yesterdayTotalTable.Columns.Add("Actual", typeof(double));

            //    //TODO: targeted??
            //    //table.Columns.Add("Targeted", typeof(double));

            //    List<CW.Data.Point> points = new List<CW.Data.Point>();

            //    DateTime yesterdayDate = DateTime.UtcNow.Date.AddDays(-1);
            //    DateTime endDate = DateTime.UtcNow.Date;
            //    DateTime firstOfYear = DateTimeHelper.GetFirstOfYear(DateTime.UtcNow);
            //    DateTime firstOfMonth = DateTimeHelper.GetFirstOfMonth(DateTime.UtcNow);
            //    DateTime firstOfWeek = DateTimeHelper.GetLastSunday(DateTime.UtcNow);

            //    //get building
            //    Building building = mDataManager.BuildingDataMapper.GetBuildingByID(bid, false, false, false, false);

            //    //TODO: if first cache set for all buildings, else
            //    //cache empty for client
            //    if (true)
            //    {
            //        double yearValue = 0, monthValue = 0, weekValue = 0, yesterdayValue = 0;

            //        //get point or points where point type is the power metric
            //        points = mDataManager.PointDataMapper.GetBuildingPowerConsumptionPoints(bid, true);

            //        //if points exist here
            //        if (points.Any())
            //        {
            //            foreach (CW.Data.Point p in points)
            //            {
            //                List<int> pt = new List<int>();
            //                pt.Add(p.PID);


            //                //Current Year Total (excluding today)   

            //                ////get all raw data
            //                //var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfYear, endDate, false);

            //                ////if any values continues
            //                //if (result.Any())
            //                //{
            //                //    //submeter = sum * (its samplinginterval / 60)                                     
            //                //    double submeterValue = result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0);

            //                //    if (submeterValue != 0)
            //                //    {
            //                //        //add row
            //                //        yearTotalTable.Rows.Add(p.PointName, submeterValue);

            //                //        //creat drilldown chart series, set chart type and name
            //                //        var series =
            //                //        new Series
            //                //        {
            //                //            ChartType = SeriesChartType.StackedColumn,
            //                //            Name = p.PointName
            //                //        };
            //                //        List<BarData> barDataList = new List<BarData>();

            //                //        //add drilldown series
            //                //        for (int m = 1; m < 13; m++)
            //                //        {
            //                //            //TODO: factor in timezone

            //                //            var resultMonth = result.Where(r => r.DateLoggedLocal.Month == m);

            //                //            submeterValue = resultMonth.Any() ? resultMonth.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0) : 0;

            //                //            BarData barData = new BarData();
            //                //            barData.Range = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(m);
            //                //            barData.Value = submeterValue;

            //                //            barDataList.Add(barData);

            //                //            //yearDrillDownTable.Rows.Add(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(m), p.PointName, submeterValue);
            //                //        }

            //                //        series.Points.DataBindXY(barDataList, "Range", barDataList, "Value");

            //                //        //add series to chart
            //                //        buildingOverviewDrillDownYearChart.Series.Add(series);
            //                //    }
            //                //}


            //                //Current Month (excluding today)------------ 

            //                ////get all raw data
            //                //result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfMonth, endDate, false);

            //                ////if any values continues
            //                //if (result.Any())
            //                //{
            //                //    //submeter = sum * (its samplinginterval / 60)                                     
            //                //    double submeterValue = result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0);

            //                //    if (submeterValue != 0)
            //                //    {
            //                //        //add row
            //                //        monthTotalTable.Rows.Add(p.PointName, submeterValue);

            //                //        //creat drilldown chart series, set chart type and name
            //                //        var series =
            //                //        new Series
            //                //        {
            //                //            ChartType = SeriesChartType.StackedColumn,
            //                //            Name = p.PointName
            //                //        };
            //                //        List<BarData> barDataList = new List<BarData>();

            //                //        //add drilldown series
            //                //        for(int d = 1; d < 32; d++)
            //                //        {
            //                //            //TODO: factor in timezone
            //                //            //TODO: sqft

            //                //            var resultDay = result.Where(r => r.DateLoggedLocal.Day == d);

            //                //            submeterValue = resultDay.Any() ? resultDay.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0) : 0;

            //                //            BarData barData = new BarData();
            //                //            barData.Range = d.ToString();
            //                //            barData.Value = submeterValue;

            //                //            barDataList.Add(barData);
            //                //        }

            //                //        series.Points.DataBindXY(barDataList, "Range", barDataList, "Value");

            //                //        //add series to chart
            //                //        buildingOverviewDrillDownMonthChart.Series.Add(series);
            //                //    }
            //                //}


            //                //Current Week (excluding today)------------ 

            //                //get all raw data
            //                var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfWeek, endDate, false);

            //                //if any values continues
            //                if (result.Any())
            //                {
            //                    //submeter = sum * (its samplinginterval / 60)                                     
            //                    double submeterValue = result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0);

            //                    if (submeterValue != 0)
            //                    {
            //                        //add row
            //                        weekTotalTable.Rows.Add(p.PointName, submeterValue);

            //                        //creat drilldown chart series, set chart type and name
            //                        var series =
            //                        new Series
            //                        {
            //                            ChartType = SeriesChartType.StackedColumn,
            //                            Name = p.PointName
            //                        };
            //                        List<BarData> barDataList = new List<BarData>();

            //                        //add drilldown series
            //                        foreach (string s in Enum.GetNames(typeof(DayOfWeek)))
            //                        {
            //                            //TODO: factor in timezone
            //                            //TODO: sqft

            //                            var resultDay = result.Where(r => r.DateLoggedLocal.DayOfWeek.ToString() == s);

            //                            submeterValue = resultDay.Any() ? resultDay.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0) : 0;

            //                            BarData barData = new BarData();
            //                            barData.FactorOfTime = s;
            //                            barData.Value = submeterValue;

            //                            //weekDrillDownTable.Rows.Add(s, submeterValue);
            //                            barDataList.Add(barData);
            //                        }

            //                        series.Points.DataBindXY(barDataList, "Range", barDataList, "Value");

            //                        //add series to chart
            //                        buildingOverviewDrillDownWeekChart.Series.Add(series);
            //                    }
            //                }


            //                //Yesterday------------

            //                //get all raw data
            //                result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, yesterdayDate, endDate, false);

            //                //if any values continues
            //                if (result.Any())
            //                {
            //                    //submeter = sum * (its samplinginterval / 60)                                     
            //                    double submeterValue = result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0);

            //                    if (submeterValue != 0)
            //                    {
            //                        //add row
            //                        yesterdayTotalTable.Rows.Add(p.PointName, submeterValue);

            //                        //creat drilldown chart series, set chart type and name
            //                        var series =
            //                        new Series
            //                        {
            //                            ChartType = SeriesChartType.StackedArea,
            //                            Name = p.PointName
            //                        };
            //                        //List<TimeBarData> barDataList = new List<TimeBarData>();

            //                        //add drilldown series

            //                        //TODO: factor in timezone
            //                        //TODO: loop through each point and set timebardata and convert to per hour
            //                        //submeterValue = result.Sum(r => r.ConvertedRawValue) * (Convert.ToDouble(p.SamplingInterval) / 60.0);
            //                        //TODO: sqft

            //                        //set axis intervaltype
            //                        buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Hours;

            //                        //set axis format:  g = short date and time, d = short date
            //                        buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.LabelStyle.Format = "g";

            //                        //buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].Area3DStyle.Enable3D = true;

            //                        series.Points.DataBindXY(result, "DateLoggedLocal", result, "ConvertedRawValue");

            //                        //add series to chart
            //                        buildingOverviewDrillDownYesterdayChart.Series.Add(series);
            //                    }
            //                }
            //            }
            //        }
            //        else
            //        {

            //            //get point or points where point type is the energy metric
            //            points = mDataManager.PointDataMapper.GetBuildingEnergyConsumptionPoints(bid, true);

            //            //if points exist here
            //            if (points.Any())
            //            {
            //                foreach (CW.Data.Point p in points)
            //                {
            //                    //get first value of first day, get last vaue of last day for all pids

            //                    List<int> pt = new List<int>();
            //                    pt.Add(p.PID);


            //                    //Current Year Total (excluding today)   

            //                    ////get all raw data
            //                    //var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfYear, endDate, false);

            //                    ////if any values continues
            //                    //if (result.Any())
            //                    //{
            //                    //    //subtract last - first
            //                    //    double submeterValue = result.Last().ConvertedRawValue - result.First().ConvertedRawValue;

            //                    //    if (submeterValue != 0)
            //                    //    {
            //                    //        //add total row
            //                    //        yearTotalTable.Rows.Add(p.PointName, submeterValue);

            //                    //        //creat drilldown chart series, set chart type and name
            //                    //        var series =
            //                    //        new Series
            //                    //        {
            //                    //            ChartType = SeriesChartType.StackedColumn,
            //                    //            Name = p.PointName
            //                    //        };
            //                    //        List<BarData> barDataList = new List<BarData>();

            //                    //        //add drilldown series
            //                    //        for (int m = 1; m < 13; m++)
            //                    //        {
            //                    //            //TODO: factor in timezone

            //                    //            var resultMonth = result.Where(r => r.DateLoggedLocal.Month == m);

            //                    //            submeterValue = resultMonth.Any() ? resultMonth.Last().ConvertedRawValue - resultMonth.First().ConvertedRawValue : 0;

            //                    //            BarData barData = new BarData();
            //                    //            barData.Range = System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(m);
            //                    //            barData.Value = submeterValue;

            //                    //            barDataList.Add(barData);

            //                    //            //yearDrillDownTable.Rows.Add(System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(m), p.PointName, submeterValue);
            //                    //        }

            //                    //        series.Points.DataBindXY(barDataList, "Range", barDataList, "Value");

            //                    //        //add series to chart
            //                    //        buildingOverviewDrillDownYearChart.Series.Add(series);
            //                    //    }
            //                    //}



            //                    //Current Month (excluding today)--------   

            //                    ////get all raw data
            //                    //result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfWeek, endDate, false);

            //                    ////if any values continues
            //                    //if (result.Any())
            //                    //{
            //                    //    //subtract last - first
            //                    //    double submeterValue = result.Last().ConvertedRawValue - result.First().ConvertedRawValue;

            //                    //    if (submeterValue != 0)
            //                    //    {
            //                    //        //add total row
            //                    //        monthTotalTable.Rows.Add(p.PointName, submeterValue);

            //                    //        //creat drilldown chart series, set chart type and name
            //                    //        var series =
            //                    //        new Series
            //                    //        {
            //                    //            ChartType = SeriesChartType.StackedColumn,
            //                    //            Name = p.PointName
            //                    //        };
            //                    //        List<BarData> barDataList = new List<BarData>();

            //                    //        //add drilldown rows
            //                    //        for (int d = 1; d < 32; d++)
            //                    //        {
            //                    //            //TODO: factor in timezone

            //                    //            var resultDay = result.Where(r => r.DateLoggedLocal.Day == d);

            //                    //            submeterValue = resultDay.Any() ? resultDay.Last().ConvertedRawValue - resultDay.First().ConvertedRawValue : 0;

            //                    //            BarData barData = new BarData();
            //                    //            barData.Range = d.ToString();
            //                    //            barData.Value = submeterValue;

            //                    //            //weekDrillDownTable.Rows.Add(s, p.PointName, submeterValue);

            //                    //            barDataList.Add(barData);
            //                    //        }

            //                    //        series.Points.DataBindXY(barDataList, "Range", barDataList, "Value");

            //                    //        //add series to chart
            //                    //        buildingOverviewDrillDownMonthChart.Series.Add(series);
            //                    //    }
            //                    //}



            //                    //Current Week (excluding today)--------   

            //                    //get all raw data
            //                    var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, firstOfWeek, endDate, false);

            //                    //if any values continues
            //                    if (result.Any())
            //                    {
            //                        //subtract last - first
            //                        double submeterValue = result.Last().ConvertedRawValue - result.First().ConvertedRawValue;

            //                        if (submeterValue != 0)
            //                        {
            //                            //add total row
            //                            weekTotalTable.Rows.Add(p.PointName, submeterValue);

            //                            //creat drilldown chart series, set chart type and name
            //                            var series =
            //                            new Series
            //                            {
            //                                ChartType = SeriesChartType.StackedColumn,
            //                                Name = p.PointName
            //                            };
            //                            List<BarData> barDataList = new List<BarData>();

            //                            //add drilldown rows
            //                            foreach (string s in Enum.GetNames(typeof(DayOfWeek)))
            //                            {
            //                                //TODO: factor in timezone

            //                                var resultDay = result.Where(r => r.DateLoggedLocal.DayOfWeek.ToString() == s);

            //                                submeterValue = resultDay.Any() ? resultDay.Last().ConvertedRawValue - resultDay.First().ConvertedRawValue : 0;

            //                                BarData barData = new BarData();
            //                                barData.FactorOfTime = s;
            //                                barData.Value = submeterValue;

            //                                //weekDrillDownTable.Rows.Add(s, p.PointName, submeterValue);

            //                                barDataList.Add(barData);
            //                            }

            //                            series.Points.DataBindXY(barDataList, "Range", barDataList, "Value");

            //                            //add series to chart
            //                            buildingOverviewDrillDownWeekChart.Series.Add(series);
            //                        }
            //                    }



            //                    //Yesterday-----  

            //                    //get all raw data
            //                    result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pt, endDate, endDate, false);

            //                    //if any values continues
            //                    if (result.Any())
            //                    {
            //                        //subtract last - first
            //                        double submeterValue = result.Last().ConvertedRawValue - result.First().ConvertedRawValue;

            //                        if (submeterValue != 0)
            //                        {
            //                            //add total row
            //                            yesterdayTotalTable.Rows.Add(p.PointName, submeterValue);

            //                            //creat drilldown chart series, set chart type and name
            //                            var series =
            //                            new Series
            //                            {
            //                                ChartType = SeriesChartType.StackedArea,
            //                                Name = p.PointName
            //                            };
            //                            //List<TimeBarData> barDataList = new List<TimeBarData>();

            //                            //add drilldown series

            //                            //TODO: factor in timezone
            //                            //TODO: loop through each point and set timebardata
            //                            //submeterValue = resultDay.Any() ? resultDay.Last().ConvertedRawValue - resultDay.First().ConvertedRawValue : 0;


            //                            //set axis intervaltype
            //                            buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Hours;

            //                            //set axis format:  g = short date and time, d = short date
            //                            buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.LabelStyle.Format = "g";

            //                            //buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].Area3DStyle.Enable3D = true;

            //                            series.Points.DataBindXY(result, "DateLoggedLocal", result, "ConvertedRawValue");

            //                            //add series to chart
            //                            buildingOverviewDrillDownYesterdayChart.Series.Add(series);
            //                        }
            //                    }
            //                }
            //            }
            //        }

            //        //TODO: add tables to cache
            //    }



            //    //TODO: get from cache only users buildings for plot


            //    //set has data flags
            //    hasYearPlotData = yearTotalTable.Rows.Count > 0;
            //    hasMonthPlotData = monthTotalTable.Rows.Count > 0;
            //    hasWeekPlotData = weekTotalTable.Rows.Count > 0;
            //    hasYesterdayPlotData = yesterdayTotalTable.Rows.Count > 0;


            //    //has year plot data-----
            //    if (hasYearPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        buildingOverviewTotalYearChart.ChartAreas["buildingOverviewTotalYearChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewTotalYearChart.ChartAreas["buildingOverviewTotalYearChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewDrillDownYearChart.ChartAreas["buildingOverviewDrillDownYearChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewDrillDownYearChart.ChartAreas["buildingOverviewDrillDownYearChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (yearTotalTable as System.ComponentModel.IListSource).GetList();
            //        buildingOverviewTotalYearChart.DataBindTable(IListTable, "PointName");
            //        //IListTable = (yearDrillDownTable as System.ComponentModel.IListSource).GetList();
            //        //buildingOverviewDrillDownYearChart.DataBindTable(IListTable, "Month");

            //        //set chart title
            //        buildingOverviewTotalYearChart.Titles["buildingOverviewTotalYearTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter for the Current Year. (Jan 1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
            //        buildingOverviewDrillDownYearChart.Titles["buildingOverviewDrillDownYearTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter for the Current Year Monthly. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

            //        buildingOverviewTotalYearChart.Series[0].Color = Color.ForestGreen;
            //        buildingOverviewDrillDownYearChart.Series[0].Color = Color.ForestGreen;
            //    }
            //    else
            //    {
            //        lblBuildingOverviewTotalYearResults.Text = noPlots;
            //        lblBuildingOverviewDrillDownYearResults.Text = noPlots;
            //    }

            //    buildingOverviewTotalYearChart.Visible = hasYearPlotData;
            //    buildingOverviewDrillDownYearChart.Visible = hasYearPlotData;
            //    lblBuildingOverviewTotalYearResults.Visible = !hasYearPlotData;
            //    lblBuildingOverviewDrillDownYearResults.Visible = !hasYearPlotData;




            //    //has month plot data-----
            //    if (hasMonthPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        buildingOverviewTotalMonthChart.ChartAreas["buildingOverviewTotalMonthChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewTotalMonthChart.ChartAreas["buildingOverviewTotalMonthChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewDrillDownMonthChart.ChartAreas["buildingOverviewDrillDownMonthChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewDrillDownMonthChart.ChartAreas["buildingOverviewDrillDownMonthChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (monthTotalTable as System.ComponentModel.IListSource).GetList();
            //        buildingOverviewTotalMonthChart.DataBindTable(IListTable, "PointName");

            //        //set chart title
            //        buildingOverviewTotalMonthChart.Titles["buildingOverviewTotalMonthTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter for the Current Month. (The 1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
            //        buildingOverviewDrillDownMonthChart.Titles["buildingOverviewDrillDownMonthTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter for the Current Month Daily. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

            //        buildingOverviewTotalMonthChart.Series[0].Color = Color.ForestGreen;
            //        buildingOverviewDrillDownMonthChart.Series[0].Color = Color.ForestGreen;
            //    }
            //    else
            //    {
            //        lblBuildingOverviewTotalMonthResults.Text = noPlots;
            //        lblBuildingOverviewDrillDownMonthResults.Text = noPlots;
            //    }

            //    buildingOverviewTotalMonthChart.Visible = hasMonthPlotData;
            //    buildingOverviewDrillDownMonthChart.Visible = hasMonthPlotData;
            //    lblBuildingOverviewTotalMonthResults.Visible = !hasMonthPlotData;
            //    lblBuildingOverviewDrillDownMonthResults.Visible = !hasMonthPlotData;



            //    //has week plot data-----
            //    if (hasWeekPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        buildingOverviewTotalWeekChart.ChartAreas["buildingOverviewTotalWeekChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewTotalWeekChart.ChartAreas["buildingOverviewTotalWeekChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewDrillDownWeekChart.ChartAreas["buildingOverviewDrillDownWeekChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewDrillDownWeekChart.ChartAreas["buildingOverviewDrillDownWeekChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (weekTotalTable as System.ComponentModel.IListSource).GetList();
            //        buildingOverviewTotalWeekChart.DataBindTable(IListTable, "PointName");
            //        //IListTable = (weekDrillDownTable as System.ComponentModel.IListSource).GetList();
            //        //buildingOverviewDrillDownWeekChart.DataBindTable(IListTable, "DayOfWeek");

            //        //set chart title
            //        buildingOverviewTotalWeekChart.Titles["buildingOverviewTotalWeekTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter for the Current Week. (Sunday through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
            //        buildingOverviewDrillDownWeekChart.Titles["buildingOverviewDrillDownWeekTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter for the Current Week Daily. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

            //        buildingOverviewTotalWeekChart.Series[0].Color = Color.ForestGreen;
            //        buildingOverviewDrillDownWeekChart.Series[0].Color = Color.ForestGreen;
            //    }
            //    else
            //    {
            //        lblBuildingOverviewTotalWeekResults.Text = noPlots;
            //        lblBuildingOverviewDrillDownWeekResults.Text = noPlots;
            //    }

            //    buildingOverviewTotalWeekChart.Visible = hasWeekPlotData;
            //    buildingOverviewDrillDownWeekChart.Visible = hasWeekPlotData;
            //    lblBuildingOverviewTotalWeekResults.Visible = !hasWeekPlotData;
            //    lblBuildingOverviewDrillDownWeekResults.Visible = !hasWeekPlotData;


            //    //has yesterday plot data-----
            //    if (hasYesterdayPlotData)
            //    {
            //        //TODO: on front end after microsoft bug fix.
            //        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
            //        buildingOverviewTotalYesterdayChart.ChartAreas["buildingOverviewTotalYesterdayChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewTotalYesterdayChart.ChartAreas["buildingOverviewTotalYesterdayChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
            //        buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

            //        //convert datatable to a IEnumerable form
            //        var IListTable = (yesterdayTotalTable as System.ComponentModel.IListSource).GetList();
            //        buildingOverviewTotalYesterdayChart.DataBindTable(IListTable, "PointName");

            //        //set chart title
            //        buildingOverviewTotalYesterdayChart.Titles["buildingOverviewTotalYesterdayTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter for Yesterday.";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
            //        buildingOverviewDrillDownYesterdayChart.Titles["buildingOverviewDrillDownYesterdayTitle"].Text = "Energy Consumption for " + building.BuildingName + " per Meter for Yesterday over Time. (Stacked Area)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

            //        buildingOverviewTotalYesterdayChart.Series[0].Color = Color.ForestGreen;
            //        buildingOverviewDrillDownYesterdayChart.Series[0].Color = Color.ForestGreen;
            //    }
            //    else
            //    {
            //        lblBuildingOverviewTotalYesterdayResults.Text = noPlots;
            //        lblBuildingOverviewDrillDownYesterdayResults.Text = noPlots;
            //    }

            //    buildingOverviewTotalYesterdayChart.Visible = hasWeekPlotData;
            //    buildingOverviewDrillDownYesterdayChart.Visible = hasWeekPlotData;
            //    lblBuildingOverviewTotalYesterdayResults.Visible = !hasWeekPlotData;
            //    lblBuildingOverviewDrillDownYesterdayResults.Visible = !hasWeekPlotData;
            //}

        #endregion

        #region Cache Helper Methods

            private IEnumerable<PerformanceResult> DummyPerformanceResults()
            {
                return Enumerable.Empty<PerformanceResult>();
            }

            private string CreateCacheKey()
            {
                return CommonDataConstants.CacheKeyPerformanceResults + "|" +currentBuilding.CID + "|" + currentBuilding.BID;
            }

        #endregion

        #region Helper Methods

            private void SetPortfolioOverviewBlockTotalsVisibility()
            {
                spanPortfolioOverviewTotalYesterday.Visible = divPortfolioOverviewYesterdayResults.Visible;
                spanPortfolioOverviewTotalWeek.Visible = divPortfolioOverviewWeekResults.Visible;
                spanPortfolioOverviewTotalMonth.Visible = divPortfolioOverviewMonthResults.Visible;
                spanPortfolioOverviewTotalYear.Visible = divPortfolioOverviewYearResults.Visible;
            }

            private void SetBuildingOverviewBlockTotalsVisibility()
            {
                spanBuildingOverviewTotalYesterday.Visible = divBuildingOverviewTotalYesterdayResults.Visible || divBuildingOverviewDrillDownYesterdayResults.Visible;
                spanBuildingOverviewTotalWeek.Visible = divBuildingOverviewTotalWeekResults.Visible || divBuildingOverviewDrillDownWeekResults.Visible;
                spanBuildingOverviewTotalMonth.Visible = divBuildingOverviewTotalMonthResults.Visible || divBuildingOverviewDrillDownMonthResults.Visible;
                spanBuildingOverviewTotalYear.Visible = divBuildingOverviewTotalYearResults.Visible || divBuildingOverviewDrillDownYearResults.Visible;
            }

            private void SetActiveNavButton(LinkButton lb)
            {
                btnHome.CssClass = "lnkBtnDashboard";
                btnPortfolioOverview.CssClass = "lnkBtnDashboard";
                btnPortfolioData.CssClass = "lnkBtnDashboard";
                btnBuildingOverview.CssClass = "lnkBtnDashboard";
                btnBuildingData.CssClass = "lnkBtnDashboard";

                lb.CssClass = "lnkBtnDashboardActive";
            }

            public class BarData
            {
                public string FactorOfTime
                {
                    get;
                    set;
                }
                public double Value
                {
                    get;
                    set;
                }
            }
            public class TimeBarData
            {
                public DateTime Time
                {
                    get;
                    set;
                }
                public double Value
                {
                    get;
                    set;
                }
            }

        #endregion
    }
}