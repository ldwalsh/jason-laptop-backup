﻿using CW.Utility.Web;
using System;

namespace CW.Website._utilitybilling
{
    public sealed class UtilityBillingParamsObject: QueryString.IParamsObject
    {
        #region QueryString.IParamsObject

            Boolean QueryString.IParamsObject.RequirePropertyAttribute
            {
                get {return false;}
            }

        #endregion

        #pragma warning disable 0649

        //startingEndDate
        public  DateTime
                sed;

        //endingEndDate
        public  DateTime
                eed;
            
        public  Int32
                cid;

        [
        QueryString.Property(Requires="cid")
        ]
        public  Int32
                bid;

        [
        QueryString.Property(Optional=true, Requires="bid")
        ]
        public  Int32?
                eid;

        [
        QueryString.Property(Optional=true, Requires="eid")
        ]
        public  Int32?
                aid;

        #pragma warning restore 0649
    }
}