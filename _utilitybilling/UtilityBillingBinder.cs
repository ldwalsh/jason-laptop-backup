﻿using System;
using System.Collections.Generic;
using CW.Business;
using CW.Data.AzureStorage;
using CW.Utility;
using CW.Website._framework;
using CW.Data;
using CW.Data.Models;
using CW.Data.Models.UtilityBilling;

namespace CW.Website._utilitybilling
{
    public sealed class UtilityBillingBinder
    {
        private readonly
                SiteUser
                siteUser;

        private readonly
                DataManager
                dataManager;

        public UtilityBillingBinder(SiteUser siteUser, DataManager dataManager)
        {
            this.siteUser = siteUser;
            this.dataManager = dataManager;
        }


        public IEnumerable<UtilityBillingResult> BuildUtilityBills(UtilityBillingBindingParameters bp)
        {
            return
            dataManager.UtilityBillingDataMapper.GetUtilityBillingResultsFromStorage(bp);
        }
    }
}