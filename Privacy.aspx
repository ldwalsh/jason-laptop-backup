﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Help.master" AutoEventWireup="false" CodeBehind="Privacy.aspx.cs" Inherits="CW.Website.Privacy" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
   
     <div id="intro" class="pod">
    	<div class="top"> </div>
        <div id="middle" class="middle">                    	                  	        	
        	<h1>Privacy Policy</h1>                        
                <div class="richText">                     
                    <asp:Literal ID="litPrivacyPolicyBody" runat="server"></asp:Literal>                                                             
                </div>                      
         </div>
    </div>   
   
</asp:Content>

