﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;
using System.Text;

using CW.Business;
using CW.Data.AzureStorage.Models;
using CW.Data.AzureStorage;
using CW.Common.Constants;
using CW.Website._framework;
using System.Globalization;
using CW.Data.Helpers;
using CW.Utility;


namespace CW.Website
{
    public partial class SystemLogs : SitePage
    {
        #region Properties
            private const string noMessages = "There are no messages for this selection";
            private const string noSectionSelected = "A log section was not selected.  Please select a log section from the pulldown and click \"Submit\" button.";
            private const string noLevelSelected = "A log level was not selected.  Please select a log level from the pulldown and click \"Submit\" button.";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
				//logged in security check in master page  

                if (!Page.IsPostBack)
                {
                    BindSection(ddlLogSection);
                    BindLevels(ddlLogLevel);
                }
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Binds a dropdown list with all log sections
            /// </summary>
            /// <param name="ddl"></param>
            private void BindSection(DropDownList ddl)
            {                
                ddl.DataSource = GetAllSections();
                ddl.DataBind();
            }

            private void BindLevels(DropDownList ddl)
            {
                ddl.DataSource = GetAllLevels();
                ddl.DataBind();
            }

        #endregion

        #region Button Events

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (ddlLogLevel.SelectedValue == "-1") 
            {
                lblErrors.Visible = true;
                lblErrors.Text = noLevelSelected;
            }
            else if(ddlLogSection.SelectedValue == "-1")
            {
                lblErrors.Visible = true;
                lblErrors.Text = noSectionSelected;
            }
            else
            {
                StringBuilder sb = new StringBuilder();

                IList<Log> logs = GetLogs().ToList();
                IOrderedEnumerable<Log> orderedLogs = logs.OrderBy(l => l.LogTimestamp);

                foreach (Log log in orderedLogs)
                {
                    sb.Append("<div>");
                    //Encode so literal doesnt treat log characters as html tags
                    sb.Append(Server.HtmlEncode(log.Message));
                    sb.Append("</div></br></br>");
                }

                lblCount.Text = logs.Count.ToString();

                //Render in a non form control to avoid the following error on async postback request:
                //Sys.WebForms.PageRequestManagerServerErrorException: An unknown error occurred while processing the request on the server. The status code returned from the server was: 500 
                //Used a literal so we can format. 
                logText.Text = sb.ToString();
            }

        }

        #endregion

        #region Helper functions

        // Get All Sections
        private List<string> GetAllSections()
        {
            List<string> list = new List<string>();

            foreach (string section in Enum.GetNames(typeof(DataConstants.LogSection)).OrderBy(s=>s))
            {
                list.Add(section);
            }

            return list;
            
        }

        // Get All Levels
        private List<string> GetAllLevels()
        {
            List<string> list = new List<string>();

            foreach (string level in Enum.GetNames(typeof(DataConstants.LogLevel)).OrderBy(l => l))
            {
                list.Add(level);
            }

            return list;

        }

        // Get Logs
        private IEnumerable<Log> GetLogs()
        {
            DateTime dtStart = (DateTime)txtStartDate.SelectedDate;
            DateTime dtEnd = (DateTime)txtEndDate.SelectedDate;
            //String cultureName = BusinessConstants.Culture.DefaultCultureName;
            //DateTimeHelper.TryParse(txtStartDate.Text, cultureName, out dtStart);
            //DateTimeHelper.TryParse(txtEndDate.Text, cultureName, out dtEnd);

            IEnumerable<Log> logs = DataMgr.LogDataMapper.GetLogByTimespan(ddlLogSection.SelectedValue, dtStart, dtEnd, ddlLogLevel.SelectedValue); 

            if (logs == null)
                logs = Enumerable.Empty<Log>();

            return logs;
        }

        #endregion
    }
}
