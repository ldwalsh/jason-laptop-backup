﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Diagnostics.master" EnableEventValidation="false" EnableViewState="true" AutoEventWireup="false" CodeBehind="Diagnostics.aspx.cs" Inherits="CW.Website.Diagnostics.DiagnosticsPage" %>

<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="CW.Website" Namespace="CW.Website._extensions" TagPrefix="extensions" %>
<%@ Register TagPrefix="CW" TagName="TaskAddFormModal" Src="~/_controls/tasks/TaskAddFormModal.ascx" %>
<%@ Import Namespace="CW.Data.Models.Diagnostics" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Utility.Web" %>
<%@ Import Namespace="CW.Website.Reports" %>
<%@ Import Namespace="CW.Common.Constants" %>
<%@ Import Namespace="System.Globalization" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <script type="text/javascript">
        function HideDiagnosticsModals() {
            $find('mpePostponeBehavior').hide();
            $find('mpeEquipmentNotesBehavior').hide();
            $find('mpeRequestSupportBehavior').hide();
            $find('createTaskMPEBehavior').hide();
        }
    </script>

    <telerik:RadAjaxLoadingPanel ID="lpNested" runat="server" Skin="Default" />

    <div class="diagnosticsModuleIcon">
        <h1>Diagnostics</h1>
    </div>
    <div class="richText">
        <asp:Literal ID="litDiagnosticsBody" runat="server"></asp:Literal>
    </div>

    <telerik:RadAjaxPanel ID="radAjaxPanelNestedTop" runat="server" LoadingPanelID="lpNested">

        <fieldset class="moduleSearch">
            <legend>Search Criteria
                <asp:HyperLink ID="lnkSearch" runat="server" CssClass="searchToggle">
                    <asp:Label ID="lblSearch" runat="server"></asp:Label>
                </asp:HyperLink>&nbsp;</legend>
            <ajaxToolkit:CollapsiblePanelExtender ID="detailsCollapsiblePanelExtender" runat="Server"
                TargetControlID="pnlCriteria"
                CollapsedSize="0"
                ExpandControlID="lnkSearch"
                CollapseControlID="lnkSearch"
                AutoCollapse="false"
                AutoExpand="false"
                ScrollContents="false"
                ExpandDirection="Vertical"
                TextLabelID="lblSearch"
                CollapsedText="+"
                ExpandedText="-" />

            <asp:Panel ID="pnlCriteria" runat="server" DefaultButton="btnGenerateData" CssClass="criteria">

                <!--View By-->
                <div class="divCriteria">
                    <div>
                        <h3>View By</h3>
                    </div>
                    <div class="divFormWidest">
                        <asp:RadioButtonList ID="rblViewBy" CssClass="radio" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="rblViewBy_OnSelectedIndexChanged" runat="server">
                            <asp:ListItem Value="Building" Text="Building"></asp:ListItem>
                            <asp:ListItem Value="EquipmentClass" Text="Equipment Class"></asp:ListItem>
                            <asp:ListItem Value="Equipment" Text="Equipment"></asp:ListItem>
                            <asp:ListItem Value="Analysis" Text="Analysis"></asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                </div>

                <div class="divCriteria divViewByCol2">

                    <!--Building Group-->
                    <asp:Panel ID="pnlBuildingGroups" runat="server">
                        <div class="dropDownPanel">
                            <label>*Select Building Group:</label>
                            <br />
                            <extensions:DropDownExtension ID="ddlBuildingGroups" CssClass="dropdownNarrow" AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Value="-1">Select one...</asp:ListItem>
                            </extensions:DropDownExtension>
                        </div>

                        <asp:RequiredFieldValidator ID="buildingGroupRFV" runat="server" ErrorMessage="Building Group is a required field." ControlToValidate="ddlBuildingGroups" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Diagnostics" />
                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingGroupVCE" runat="server" BehaviorID="buildingGroupVCE" TargetControlID="buildingGroupRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                    </asp:Panel>

                    <!--Building-->
                    <asp:Panel ID="pnlBuildings" runat="server">
                        <div class="dropDownPanel">
                            <label>*Select Building:</label>
                            <br />
                            <extensions:DropDownExtension ID="ddlBuildings" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                <asp:ListItem Value="-1">Select one...</asp:ListItem>
                            </extensions:DropDownExtension>
                        </div>
                        <asp:RequiredFieldValidator ID="buildingRequiredValidator" runat="server"
                            ErrorMessage="Building is a required field."
                            ControlToValidate="ddlBuildings"
                            SetFocusOnError="true"
                            Display="None"
                            InitialValue="-1"
                            ValidationGroup="Diagnostics">
                        </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingRequiredValidatorExtender" runat="server"
                            BehaviorID="buildingRequiredValidatorExtender"
                            TargetControlID="buildingRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>
                    </asp:Panel>

                    <!--EquipmentClass-->
                    <asp:Panel ID="pnlEquipmentClass" runat="server">
                        <div class="dropDownPanel">
                            <label>*Select Equipment Class:</label>
                            <br />
                            <asp:DropDownList ID="ddlEquipmentClasses" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClasses_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                <asp:ListItem Value="-1">Select one...</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:RequiredFieldValidator ID="equipmentClassRequiredValidator" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlEquipmentClasses" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Diagnostics"></asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassRequiredValidatorExtender" runat="server" BehaviorID="equipmentClassRequiredValidatorExtender" TargetControlID="equipmentClassRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175"></ajaxToolkit:ValidatorCalloutExtender>
                    </asp:Panel>

                    <!--Equipment-->
                    <asp:Panel ID="pnlEquipment" runat="server">
                        <div class="dropDownPanel">
                            <label>*Select Equipment:</label>
                            <br />
                            <extensions:DropDownExtension ID="ddlEquipment" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                <asp:ListItem Value="-1">Select building first...</asp:ListItem>
                            </extensions:DropDownExtension>
                        </div>
                        <asp:RequiredFieldValidator ID="equipmentRequiredValidator" runat="server"
                            ErrorMessage="Equipment is a required field."
                            ControlToValidate="ddlEquipment"
                            SetFocusOnError="true"
                            Display="None"
                            InitialValue="-1"
                            ValidationGroup="Diagnostics">
                        </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="equipmentRequiredValidatorExtender" runat="server"
                            BehaviorID="equipmentRequiredValidatorExtender"
                            TargetControlID="equipmentRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>
                    </asp:Panel>

                    <!--Analysis-->
                    <asp:Panel ID="pnlAnalysis" runat="server">
                        <div class="dropDownPanel">
                            <label>*Select Analysis:</label>
                            <br />
                            <extensions:DropDownExtension ID="ddlAnalysis" CssClass="dropdownNarrow" AppendDataBoundItems="true" AutoPostBack="true" runat="server">
                                <asp:ListItem Value="-1">Select equipment first...</asp:ListItem>
                            </extensions:DropDownExtension>
                        </div>
                        <asp:RequiredFieldValidator ID="analysisRequiredValidator" runat="server"
                            ErrorMessage="Analysis is a required field."
                            ControlToValidate="ddlAnalysis"
                            SetFocusOnError="true"
                            Display="None"
                            InitialValue="-1"
                            ValidationGroup="Diagnostics">
                        </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="analysisRequiredValidatorExtender" runat="server"
                            BehaviorID="analysisRequiredValidatorExtender"
                            TargetControlID="analysisRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>
                    </asp:Panel>
                </div>

                <!--Date Range-->
                <div id="dateRange" class="divCriteria">
                    <div>
                        <h3>Display Interval</h3>
                    </div>
                    <div class="divFormWidest">
                        <asp:RadioButtonList ID="rblRange" CssClass="radio" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="rblRange_OnSelectedIndexChanged" runat="server">
                            <asp:ListItem Text="Half Day" Value="HalfDay"></asp:ListItem>
                            <asp:ListItem Text="Daily" Value="Daily" Selected="true"></asp:ListItem>
                            <asp:ListItem Text="Weekly" Value="Weekly"></asp:ListItem>
                            <asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:HiddenField ID="hdnCurrentGridRange" runat="server" Visible="false" />
                    </div>

                </div>

                <div class="divCriteria">
                    <!--Use bottomright popup validator positions for this column because the browser window will not always have space for the right most positioning. 
                            Since the button is below, the browser window will always have room for the botom validator positioning.-->
                    <div>
                        <h3>Date Range</h3>
                    </div>
                    <div class="divFormWidest">
                        <label>*Start Date:</label>
                        <br />
                        <telerik:RadDatePicker ID="txtStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>
                        <br />
                        <asp:RequiredFieldValidator ID="dateStartRequiredValidator" runat="server"
                            CssClass="errorMessage"
                            ErrorMessage="Date is a required field."
                            ControlToValidate="txtStartDate"
                            SetFocusOnError="true"
                            Display="None"
                            ValidationGroup="Diagnostics">
                        </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="dateStartRequiredValidatorExtender" runat="server"
                            BehaviorID="dateStartRequiredValidatorExtender"
                            TargetControlID="dateStartRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            PopupPosition="Right"
                            Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>
                        <br />
                        <label>*End Date:</label>
                        <br />
                        <telerik:RadDatePicker ID="txtEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>
                        <br />
                        <asp:RequiredFieldValidator ID="dateEndRequiredValidator" runat="server"
                            CssClass="errorMessage"
                            ErrorMessage="Date is a required field."
                            ControlToValidate="txtEndDate"
                            SetFocusOnError="true"
                            Display="None"
                            ValidationGroup="Diagnostics">
                        </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="dateEndRequiredValidatorExtender" runat="server"
                            BehaviorID="dateEndRequiredValidatorExtender"
                            TargetControlID="dateEndRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            PopupPosition="Right"
                            Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>
                        <asp:CompareValidator ID="dateCompareValidator" runat="server"
                            CssClass="errorMessage"
                            ErrorMessage="Invalid range."
                            ControlToValidate="txtEndDate"
                            ControlToCompare="txtStartDate"
                            Type="Date"
                            Display="None"
                            Operator="GreaterThanEqual"
                            ValidationGroup="Diagnostics"
                            SetFocusOnError="true">
                        </asp:CompareValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="dateCompareValidatorExtender" runat="server"
                            BehaviorID="dateCompareValidatorExtender"
                            TargetControlID="dateCompareValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            PopupPosition="Right"
                            Width="175" />
                    </div>
                </div>

                <div class="divCriteria">
                    <div>
                        <h3>Top Priorities</h3>
                    </div>
                    <div class="divFormWidest">
                        <div class="">
                            <label runat="server" id="labelTop">Top:</label><br />
                            <asp:DropDownList ID="ddlPrioritiesCount" CssClass="dropdownNarrower" runat="server">
                                <asp:ListItem Value="ALL" Selected="True">All</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="25">25</asp:ListItem>
                                <asp:ListItem Value="50">50</asp:ListItem>
                                <asp:ListItem Value="100">100</asp:ListItem>
                                <asp:ListItem Value="500">500</asp:ListItem>
                                <asp:ListItem Value="1000">1000</asp:ListItem>
                                <asp:ListItem Value="5000">5000</asp:ListItem>
                                <asp:ListItem Value="10000">10000</asp:ListItem>
                                <asp:ListItem Value="25000">25000</asp:ListItem>
                                <asp:ListItem Value="50000">50000</asp:ListItem>
                                <asp:ListItem Value="100000">100000</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>

                <div class="divCriteria">
                    <div>
                        <h3>Text Filter</h3>
                    </div>
                    <div class="divFormWidest">
                        <label>Notes Summary:</label>
                        <br />
                        <asp:TextBox ID="txtNotesFilter" CssClass="textboxNarrower" runat="server" />
                        <br />
                        <br />

                        <label>Tracking Code:</label>
                        <br />
                        <asp:TextBox ID="txtTrackingCode" CssClass="textboxNarrower" runat="server" />
                        <asp:RegularExpressionValidator ID="trackingCodeREV" runat="server" ErrorMessage="Invalid Format. Format must be EXXX-AXXX-FXXX" ValidationExpression="^E(\d+)-A(\d+)-F(\d+)" ControlToValidate="txtTrackingCode" SetFocusOnError="true" Display="None" ValidationGroup="Diagnostics" />
                        <ajaxToolkit:ValidatorCalloutExtender ID="trackingCodeVCE" runat="server" BehaviorID="trackingCodeVCE" TargetControlID="trackingCodeREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                    </div>
                </div>

            </asp:Panel>
        </fieldset>
    </telerik:RadAjaxPanel>

    <telerik:RadAjaxPanel ID="radAjaxPanelNestedBottom" runat="server" LoadingPanelID="lpNested">
        <div class="divGenerateButton">
            <asp:LinkButton ID="btnGenerateData" CssClass="lnkButton" ValidationGroup="Diagnostics" OnClick="generateButton_Click" Text="Generate Data" runat="server" />
        </div>
        <div id="divDownloadBoxTop" class="divDownloadBox" visible="false" runat="server">
            <div id="divExcelDownload" class="divDownload" runat="server">
                <asp:ImageButton ID="imgExcelCurrentPageDownload" CssClass="imgDownload" ImageUrl="_assets/images/excel-icon.jpg" OnClick="downloadExcelCurrentPageButton_Click" AlternateText="download" runat="server" />
                <asp:LinkButton ID="lnkExcelCurrentPageDownload" CssClass="lnkDownload" OnClick="downloadExcelCurrentPageButton_Click" runat="server" Text="Download Current Diagnostics Page"></asp:LinkButton>
            </div>
            <div id="divExcelDownloadFull" class="divDownload" runat="server">
                <asp:ImageButton ID="imgExcelDownloadFull" CssClass="imgDownload" ImageUrl="_assets/images/excel-icon.jpg" OnClick="downloadExcelFullButton_Click" AlternateText="download" runat="server" />
                <asp:LinkButton ID="lnkExcelDownloadFull" CssClass="lnkDownload" OnClick="downloadExcelFullButton_Click" runat="server" Text="Download Full Diagnostics Results"></asp:LinkButton>
            </div>
        </div>
        <div class="clear"></div>
        <div>
            <p>
                <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                <asp:HiddenField ID="hdnResultCount" runat="server" Value=""></asp:HiddenField>
                <br />
                <asp:Label ID="lblErrorTop" CssClass="errorMessage" runat="server" Text="" Visible="false"></asp:Label>
            </p>
        </div>

        <div id="gridTbl">
            <!-- New Grid View with details view-->
            <!--AutoGenerateSelectButton="true"  OnSelectedIndexChanged="gridDiagnostics_OnSelectedIndexChanged" -->
            <!-- please check gridDiagnostics_OnDataBound, gridDiagnostics_OnRowCommand, and equipmentNotesButton_Click if you add or move any controls below-->
            <asp:GridView ID="gridDiagnostics"
                Width="100%"
                runat="server"
                EnableViewState="true"
                DataKeyNames="AID,EID,BID,CID,StartDate,LCID"
                GridLines="None"
                PageSize="50" PagerSettings-PageButtonCount="20"
                HeaderStyle-CssClass="tblTitle"
                RowStyle-CssClass="tblCol1"
                AlternatingRowStyle-CssClass="tblCol2"
                AutoGenerateColumns="false"
                OnDataBound="gridDiagnostics_OnDataBound"
                OnRowCreated="gridDiagnostics_OnRowCreated"
                OnRowDataBound="gridDiagnostics_OnRowDataBound"
                AllowPaging="true" OnPageIndexChanging="gridDiagnostics_PageIndexChanging"
                AllowSorting="true" OnSorting="gridDiagnostics_Sorting"
                OnSelectedIndexChanged="gridDiagnostics_OnSelectedIndexChanged"
                OnRowCommand="gridDiagnostics_OnRowCommand">
                <Columns>
                    <asp:TemplateField SortExpression="BuildingName" HeaderText="Building">
                        <ItemTemplate>
                            <div id="buildingName" runat="server" class="hideInDownload" title='<%# Eval("BuildingName")%>'><%# StringHelper.TrimText(Eval("BuildingName").ToString(), 30)%></div>
                            <a runat="server" class="showInDownload" visible="false" title='<%# Eval("BuildingName")%>' href='<%# LinkHelper.BuildDiagnosticsQuickLink(Container.DataItem, LinkHelper.ViewByMode.Building)%>'><%# Eval("BuildingName")%></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Equipment Class" Visible="false">
                        <ItemTemplate>
                            <a runat="server" class="showInDownload" visible="false" title='<%# Eval("EquipmentClassDescription")%>' href='<%# LinkHelper.BuildDiagnosticsQuickLink(Container.DataItem, LinkHelper.ViewByMode.EquipmentClass)%>'><%# Eval("EquipmentClassName")%></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="EquipmentName" HeaderText="Equipment">
                        <ItemTemplate>
                            <div id="equipmentName" runat="server" class="hideInDownload" title='<%# Eval("EquipmentName")%>'><%# StringHelper.TrimText(Eval("EquipmentName").ToString(), 22)%></div>
                            <a runat="server" class="showInDownload" visible="false" title='<%# Eval("EquipmentName")%>' href='<%# LinkHelper.BuildDiagnosticsQuickLink(Container.DataItem, LinkHelper.ViewByMode.Equipment)%>'><%# Eval("EquipmentName")%></a>
                            <div runat="server" class="hideInDownload" title='<%# Eval("EquipmentClassDescription")%>'>(<%# StringHelper.TrimText(Eval("EquipmentClassName").ToString(), 22)%>)</div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="AnalysisName" HeaderText="Analysis">
                        <ItemTemplate>
                            <div id="analysisName" runat="server" class="hideInDownload" title='<%# Eval("AnalysisTeaser")%>'><%# StringHelper.TrimText(Eval("AnalysisName").ToString(), 20)%></div>
                            <a runat="server" class="showInDownload" visible="false" title='<%# Eval("AnalysisTeaser")%>' href='<%# LinkHelper.BuildDiagnosticsQuickLink(Container.DataItem, LinkHelper.ViewByMode.Analysis)%>'><%# Eval("AnalysisName")%></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField SortExpression="StartDate" HeaderText="Start Date">
                        <ItemTemplate>
                            <div id="startDate" runat="server"><%# DateTime.Parse(Eval("StartDate").ToString()).ToShortDateString()%></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Notes Summary">
                        <ItemTemplate>
                            <div id="NotesSummary" runat="server" class="divNotesSummary">
                                <%# Eval("NotesSummary")%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="gridAlignCenter" SortExpression="TaskCount" HeaderText="Tasks" Visible="false">
                        <ItemTemplate>                                                        
                            <div id="analysisRange" runat="server" class="hideInDownload" style="visibility: hidden; line-height: 0px;"><%# Eval(PropHelper.G<DiagnosticsResult>(_ => _.AnalysisRange)) %></div>
                            <a runat="server" class="showInDownload" title='<%# CultureHelper.FormatNumber(Convert.ToInt32(Eval(PropHelper.G<DiagnosticsResult>(_ => _.LCID))), Convert.ToInt32(Eval(PropHelper.G<DiagnosticsResult>(_ => _.TaskCount))), false) + " Open Tasks" %>' href='<%# LinkHelper.BuildTaskQuickLink(Container.DataItem) %>' target="_blank">
                                <%# CultureHelper.FormatNumber(Convert.ToInt32(Eval(PropHelper.G<DiagnosticsResult>(_ => _.LCID))), Convert.ToInt32(Eval(PropHelper.G<DiagnosticsResult>(_ => _.TaskCount))), false) %>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="gridAlignRight" SortExpression="CostSavings" HeaderText="Cost">
                        <ItemTemplate>
                            <div id="defaultCostValue" runat="server">
                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("CostSavings"))) ? CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), (decimal)0) :  CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToDecimal(Eval("CostSavings")))%>
                            </div>
                            <div id="costvalueWithISO" runat="server" visible="false">
                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("CostSavings"))) ? null : Eval("CostSavings") + " " + new RegionInfo(int.Parse(Eval("LCID").ToString())).ISOCurrencySymbol %>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="" SortExpression="EnergyPriority" HeaderText="E">
                        <ItemTemplate>
                            <div class='<%# "energy energy" + SetPriorityClass(Convert.ToString(Eval("EnergyPriority")))%>'>
                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("EnergyPriority"))) ? 0 : (Convert.ToDecimal(Eval("EnergyPriority").ToString()) > 10 ? 10 : Math.Ceiling(Convert.ToDecimal(Eval("EnergyPriority").ToString())))%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="" SortExpression="ComfortPriority" HeaderText="C">
                        <ItemTemplate>
                            <div class='<%# "comfort comfort" + SetPriorityClass(Convert.ToString(Eval("ComfortPriority")))%>'>
                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("ComfortPriority"))) ? 0 : (Convert.ToDecimal(Eval("ComfortPriority").ToString()) > 10 ? 10 : Math.Ceiling(Convert.ToDecimal(Eval("ComfortPriority").ToString())))%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-CssClass="" SortExpression="MaintenancePriority" HeaderText="M">
                        <ItemTemplate>
                            <div class='<%# "maintenance maintenance" + SetPriorityClass(Convert.ToString(Eval("MaintenancePriority")))%>'>
                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("MaintenancePriority"))) ? 0 : (Convert.ToDecimal(Eval("MaintenancePriority").ToString()) > 10 ? 10 : Math.Ceiling(Convert.ToDecimal(Eval("MaintenancePriority").ToString())))%>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Equipment Notes" Visible="false">
                        <ItemTemplate>
                            <div runat="server" class="showInDownload" visible="false">
                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentNotes"))) ? "" : Eval("EquipmentNotes")  %>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actions">
                        <ItemTemplate>
                            <asp:DropDownList CssClass="dropdownArrowOnly" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlActions_OnSelectedIndexChanged">
                                <asp:ListItem Text="" Value="-1"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:HiddenField ID="diagnosticsQuickLinks" Value='<%# LinkHelper.BuildDiagnosticsQuickLink(Container.DataItem, LinkHelper.ViewByMode.Analysis, true) + DataConstants.DefaultDelimiter + LinkHelper.BuildDiagnosticsQuickLink(Container.DataItem, LinkHelper.ViewByMode.Analysis) %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <br />
        </div>

        <div id="divDownloadBox" class="divDownloadBox" visible="false" runat="server">
            <div id="divEmail" class="divEmail" runat="server">
                <asp:ImageButton ID="imgEmail" CssClass="imgEmail" ImageUrl="_assets/images/email-icon.jpg" OnClick="emailButton_Click" AlternateText="email" runat="server" />
                <asp:LinkButton ID="lnkEmail" CssClass="lnkEmail" OnClick="emailButton_Click" runat="server" Text="Email Me Report"></asp:LinkButton>
            </div>
            <div id="divPdfDownload" class="divDownload" runat="server">
                <asp:ImageButton ID="imgPdfDownload" CssClass="imgDownload" ImageUrl="_assets/images/pdf-icon.jpg" OnClick="downloadButton_Click" AlternateText="download" runat="server" />
                <asp:LinkButton ID="lnkPdfDownload" CssClass="lnkDownload" OnClick="downloadButton_Click" runat="server" Text="Download Report"></asp:LinkButton>
            </div>
            <div class="divAction">
                <asp:LinkButton ID="lnkOpenEquipmentNotes" CssClass="linkAction" OnClick="openEquipmentNotesButton_Click" runat="server" Text="Equipment Notes"></asp:LinkButton>
            </div>
            <div class="divAction">
                <asp:LinkButton ID="lnkOpenRequestSupport" CssClass="linkAction" OnClick="openRequestSupportButton_Click" runat="server" Text="Request Support"></asp:LinkButton>
            </div>
        </div>
        <div id="analysisDetailsTop" visible="false" runat="server">
            <h2>Details</h2>
            <asp:Label ID="lblError" CssClass="errorMessage" runat="server" Text="" Visible="false"></asp:Label>
            <asp:Label ID="lblDetails" CssClass="narrowMessage" runat="server" Text=""></asp:Label>
            <a id="lnkSetFocusMessage" runat="server"></a>

            <asp:HiddenField ID="hdnAnalysisInfoTempGridRowIndex" runat="server" />
        </div>
        <div id="analysisDetailsBottom" visible="false" runat="server">
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Analysis Name:</label>
                <asp:Label CssClass="labelContent" ID="lblAnalysisName" runat="server" Text=""></asp:Label>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Client Name:</label>
                <asp:Label CssClass="labelContent" ID="lblClientName" runat="server" Text=""></asp:Label>
                <asp:HiddenField ID="hdnCID" runat="server" Visible="false" />
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Building Name:</label>
                <asp:Label CssClass="labelContent" ID="lblBuildingName" runat="server" Text=""></asp:Label>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Equipment Name:</label>
                <asp:Label CssClass="labelContent" ID="lblEquipmentName" runat="server" Text=""></asp:Label>
                <asp:HiddenField ID="hdnEID" runat="server" Visible="false" />
            </div>
            <div id="divAssociatedEquipmentContent" class="divFormWideAndShort" runat="server">
                <label class="labelNarrowBold">Associated Equipment:</label>
                <asp:HyperLink ID="lnkCollapsibleAssociatedEquipment" runat="server" CssClass="togglePadded">
                    <asp:Label ID="lblCollapsibleAssociatedEquipment" runat="server"></asp:Label>
                </asp:HyperLink>
                <ajaxToolkit:CollapsiblePanelExtender ID="associatedEquipmentCollapsiblePanelExtender" runat="Server"
                    TargetControlID="pnlCollapsibleAssociatedEquipment"
                    CollapsedSize="0"
                    Collapsed="true"
                    ExpandControlID="lnkCollapsibleAssociatedEquipment"
                    CollapseControlID="lnkCollapsibleAssociatedEquipment"
                    AutoCollapse="false"
                    AutoExpand="false"
                    ScrollContents="false"
                    ExpandDirection="Vertical"
                    TextLabelID="lblCollapsibleAssociatedEquipment"
                    CollapsedText="show associated equipment"
                    ExpandedText="hide associated equipment" />
                <asp:Panel ID="pnlCollapsibleAssociatedEquipment" runat="server">
                    <asp:Label CssClass="labelContent" ID="lblAssociatedEquipment" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </div>
            <div id="divPointsContent" class="divFormWideAndShort" runat="server">
                <label class="labelNarrowBold">Points:</label>
                <asp:HyperLink ID="lnkCollapsiblePoints" runat="server" CssClass="togglePadded">
                    <asp:Label ID="lblCollapsiblePoints" runat="server"></asp:Label>
                </asp:HyperLink>
                <ajaxToolkit:CollapsiblePanelExtender ID="pointsCollapsiblePanelExtender" runat="Server"
                    TargetControlID="pnlCollapsiblePoints"
                    CollapsedSize="0"
                    Collapsed="true"
                    ExpandControlID="lnkCollapsiblePoints"
                    CollapseControlID="lnkCollapsiblePoints"
                    AutoCollapse="false"
                    AutoExpand="false"
                    ScrollContents="false"
                    ExpandDirection="Vertical"
                    TextLabelID="lblCollapsiblePoints"
                    CollapsedText="show points"
                    ExpandedText="hide points" />
                <asp:Panel ID="pnlCollapsiblePoints" runat="server">
                    <asp:Label CssClass="labelContent" ID="lblPoints" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </div>
            <div id="divAssociatedEquipmentPointsContent" class="divFormWideAndShort" runat="server">
                <label class="labelNarrowBold">Associated Equip. Points:</label>
                <asp:HyperLink ID="lnkCollapsibleAssociatedEquipmentPoints" runat="server" CssClass="togglePadded">
                    <asp:Label ID="lblCollapsibleAssociatedEquipmentPoints" runat="server"></asp:Label>
                </asp:HyperLink>
                <ajaxToolkit:CollapsiblePanelExtender ID="associatedEquipmentPointsCollapsiblePanelExtender" runat="Server"
                    TargetControlID="pnlCollapsibleAssociatedEquipmentPoints"
                    CollapsedSize="0"
                    Collapsed="true"
                    ExpandControlID="lnkCollapsibleAssociatedEquipmentPoints"
                    CollapseControlID="lnkCollapsibleAssociatedEquipmentPoints"
                    AutoCollapse="false"
                    AutoExpand="false"
                    ScrollContents="false"
                    ExpandDirection="Vertical"
                    TextLabelID="lblCollapsibleAssociatedEquipmentPoints"
                    CollapsedText="show associated equipment points"
                    ExpandedText="hide associated equipment points" />
                <asp:Panel ID="pnlCollapsibleAssociatedEquipmentPoints" runat="server">
                    <asp:Label CssClass="labelContent" ID="lblAssociatedEquipmentPoints" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Date:</label>
                <asp:Label CssClass="labelContent" ID="lblStartDate" runat="server" Text=""></asp:Label>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Display Interval:</label>
                <asp:Label CssClass="labelContent" ID="lblDisplayInterval" runat="server" Text=""></asp:Label>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Cost Savings:</label>
                <asp:Label CssClass="labelContent" ID="lblCost" runat="server" Text=""></asp:Label>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Comfort Priority (0-10):</label>
                <asp:Label CssClass="labelContent" ID="lblComfort" runat="server" Text=""></asp:Label>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Energy Priority (0-10):</label>
                <asp:Label CssClass="labelContent" ID="lblEnergy" runat="server" Text=""></asp:Label>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Maintenance Priority (0-10):</label>
                <asp:Label CssClass="labelContent" ID="lblMaintenance" runat="server" Text=""></asp:Label>
            </div>
            <div id="divNotes" class="divFormWideAndShort" runat="server">
                <label class="labelNarrowBold">Notes:</label>
                <span class="richText">
                    <asp:Literal ID="litNotes" runat="server" Text=""></asp:Literal>
                </span>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Equip. Variables:</label>
                <asp:HyperLink ID="lnkVars" runat="server" CssClass="togglePadded">
                    <asp:Label ID="lblVars" runat="server"></asp:Label>
                </asp:HyperLink>
                <ajaxToolkit:CollapsiblePanelExtender ID="varsCollapsiblePanelExtender" runat="Server"
                    TargetControlID="pnlVars"
                    CollapsedSize="0"
                    Collapsed="true"
                    ExpandControlID="lnkVars"
                    CollapseControlID="lnkVars"
                    AutoCollapse="false"
                    AutoExpand="false"
                    ScrollContents="false"
                    ExpandDirection="Vertical"
                    TextLabelID="lblVars"
                    CollapsedText="show equipment variables"
                    ExpandedText="hide equipment variables" />
                <asp:Panel ID="pnlVars" runat="server">
                    <br />
                    <asp:Label CssClass="labelContent" ID="lblEquipmentVarsEmpty" Visible="false" runat="server"></asp:Label>
                    <asp:Repeater ID="rptEquipmentVars" Visible="false" runat="server">
                        <HeaderTemplate>
                            <ul class="detailsListProperties">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# "<li><strong>" + Eval("EquipmentVariableDisplayName") + ":</strong>" + (String.IsNullOrEmpty(Convert.ToString(Eval("Value"))) ? "" : "<dl><dt>Value (" + Eval("BuildingSettingBasedEngUnits") + "): </dt><dd>" + Eval("Value").ToString() + " (Default Value: " + Eval("BuildingSettingBasedDefaultValue").ToString() + ")</dd></dl>") + (String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentVariableDescription"))) ? "" : "<dl><dt>Description: </dt><dd>" + Eval("EquipmentVariableDescription") + "</dd></dl></li>")%>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>

                </asp:Panel>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Associated Equip. Vars:</label>
                <asp:HyperLink ID="lnkAssociatedEquipmentVars" runat="server" CssClass="togglePadded">
                    <asp:Label ID="lblAssociatedEquipmentVars" runat="server"></asp:Label>
                </asp:HyperLink>
                <ajaxToolkit:CollapsiblePanelExtender ID="associatedEquipmentVarsCollapsiblePanelExtender" runat="Server"
                    TargetControlID="pnlAssociatedEquipmentVars"
                    CollapsedSize="0"
                    Collapsed="true"
                    ExpandControlID="lnkAssociatedEquipmentVars"
                    CollapseControlID="lnkAssociatedEquipmentVars"
                    AutoCollapse="false"
                    AutoExpand="false"
                    ScrollContents="false"
                    ExpandDirection="Vertical"
                    TextLabelID="lblAssociatedEquipmentVars"
                    CollapsedText="show associated equipment variables"
                    ExpandedText="hide associated equipment variables" />
                <asp:Panel ID="pnlAssociatedEquipmentVars" runat="server">
                    <br />
                    <asp:Label CssClass="labelContent" ID="lblAssociatedEquipmentVarsEmpty" Visible="false" runat="server"></asp:Label>
                    <asp:Repeater ID="rptAssociatedEquipmentVars" Visible="false" runat="server">
                        <HeaderTemplate>
                            <ul class="detailsListProperties">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# "<li><strong>" + Eval("EquipmentVariableDisplayName") + " - Assigned to " + Eval("EquipmentName") + ":</strong>" + (String.IsNullOrEmpty(Convert.ToString(Eval("Value"))) ? "" : "<dl><dt>Value (" + Eval("BuildingSettingBasedEngUnits") + "): </dt><dd>" + Eval("Value").ToString() + " (Default Value: " + Eval("BuildingSettingBasedDefaultValue").ToString() + ")</dd></dl>") + (String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentVariableDescription"))) ? "" : "<dl><dt>Description: </dt><dd>" + Eval("EquipmentVariableDescription") + "</dd></dl></li>")%>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>

                </asp:Panel>
            </div>
            <div class="divFormWideAndShort">
                <label class="labelNarrowBold">Building Variables:</label>
                <asp:HyperLink ID="lnkBuildingVars" runat="server" CssClass="togglePadded">
                    <asp:Label ID="lblBuildingVars" runat="server"></asp:Label>
                </asp:HyperLink>
                <ajaxToolkit:CollapsiblePanelExtender ID="buildingVarsCollapsiblePanelExtender" runat="Server"
                    TargetControlID="pnlBuildingVars"
                    CollapsedSize="0"
                    Collapsed="true"
                    ExpandControlID="lnkBuildingVars"
                    CollapseControlID="lnkBuildingVars"
                    AutoCollapse="false"
                    AutoExpand="false"
                    ScrollContents="false"
                    ExpandDirection="Vertical"
                    TextLabelID="lblBuildingVars"
                    CollapsedText="show building variables"
                    ExpandedText="hide building variables" />
                <asp:Panel ID="pnlBuildingVars" runat="server">
                    <br />
                    <asp:Label CssClass="labelContent" ID="lblBuildingVarsEmpty" Visible="false" runat="server"></asp:Label>
                    <asp:Repeater ID="rptBuildingVars" Visible="false" runat="server">
                        <HeaderTemplate>
                            <ul class="detailsListProperties">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%# "<li><strong>" + Eval("BuildingVariableDisplayName") + ":</strong>" + (String.IsNullOrEmpty(Convert.ToString(Eval("Value"))) ? "" : "<dl><dt>Value (" + Eval("BuildingSettingBasedEngUnits") + "): </dt><dd>" + Eval("Value").ToString() + " (Default Value: " + Eval("BuildingSettingBasedDefaultValue").ToString() + ")</dd></dl>") + (String.IsNullOrEmpty(Convert.ToString(Eval("BuildingVariableDescription"))) ? "" : "<dl><dt>Description: </dt><dd>" + Eval("BuildingVariableDescription") + "</dd></dl></li>")%>
                        </ItemTemplate>
                        <FooterTemplate>
                            </ul>
                        </FooterTemplate>
                    </asp:Repeater>

                </asp:Panel>
            </div>

            <!--Set focus to body of details-->
            <a name="lnkSetFocusView" href="#" id="lnkSetFocusView" runat="server"></a>

            <div class="divGraph">
                <br />
                <img id="imgGraph" runat="server" />
                <asp:Literal ID="litGraphNotAvailable" runat="server" />
            </div>
        </div>


        <!--POSTPONE MODAL-->
        <ajaxToolkit:ModalPopupExtender ID="mpePostpone" runat="server"
            TargetControlID="lnkPostponeHidden"
            PopupControlID="pnlPostpone"
            BackgroundCssClass="modalBackgroundLight"
            CancelControlID="lnkPostponeCancelHidden"
            Y="42"
            BehaviorID="mpePostponeBehavior" />

        <asp:Panel ID="pnlPostpone" ClientIDMode="Inherit" CssClass="modalPanelLight" runat="server" DefaultButton="btnPostpone">
            <a href="javascript:void(0)" onclick="return HideDiagnosticsModals();" class="modalLinkLight" id="lnkPostponeModalCancel">Cancel</a>
            <input type="button" class="modalButtonSmall" id="btnPostponeModalCancel" onclick="return HideDiagnosticsModals();" title="X" />
            <asp:HiddenField ID="hdnPostponeValues" runat="server" />
            <asp:LinkButton ID="lnkPostponeHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>
            <asp:LinkButton ID="lnkPostponeCancelHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>

            <div id="lblPostponeTitle" class="modalTitleLight">Postpone Analysis</div>
            <div id="lblPostponeIntro" class="modalContentLight">Add a date to postpone running this analysis until. Submitting an empty date will remove the existing postpone date.</div>
            <asp:Label ID="lblPostponeError" CssClass="modalError" runat="server"></asp:Label>

            <div class="divFormModal">
                <label id="lblPostponeDate" class="modalLabelLight">Postpone Date:</label>
                <telerik:RadDatePicker ID="txtPostponeDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>
            </div>
            <div class="divFormModal">
                <asp:Button ID="btnPostpone" CssClass="modalButton" Text="Submit" ValidationGroup="Postpone" runat="server" OnClick="postponeButton_Click" />
            </div>

        </asp:Panel>

        <!--EQUIPMENT NOTES MODAL-->
        <ajaxToolkit:ModalPopupExtender ID="mpeEquipmentNotes" runat="server"
            TargetControlID="lnkEquipmentNotesHidden"
            PopupControlID="pnlEquipmentNotes"
            BackgroundCssClass="modalBackgroundLight"
            CancelControlID="lnkEquipmentNotesCancelHidden"
            Y="42"
            BehaviorID="mpeEquipmentNotesBehavior" />

        <asp:Panel ID="pnlEquipmentNotes" ClientIDMode="Inherit" CssClass="modalPanelLight" runat="server" DefaultButton="btnEquipmentNotes">
            <a href="javascript:void(0)" onclick="return HideDiagnosticsModals(); false;" class="modalLinkLight" id="lnkEquipmentNotesModalCancel">Cancel</a>
            <input type="button" class="modalButtonSmall" id="btnEquipmentNotesModalCancel" onclick="return HideDiagnosticsModals();" title="X" />
            <asp:HiddenField ID="hdnEquipmentNotesValue" runat="server" />
            <asp:LinkButton ID="lnkEquipmentNotesHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>
            <asp:LinkButton ID="lnkEquipmentNotesCancelHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>

            <div id="lblEquipmentNotesTitle" class="modalTitleLight">Equipment Notes</div>
            <div id="lblEquipmentNotesIntro" class="modalContentLight">Equipment notes get added to the notes portion on all analysis results for the given equipment.</div>
            <asp:Label ID="lblEquipmentNotesError" CssClass="modalError" runat="server"></asp:Label>
            <br />
            <br />
            <div>
                <label id="lblEquipmentNotesNotes" class="modalContentLight">Max HTML Characters = 5000:</label>

                <telerik:RadEditor ID="editorEquipmentNotesNotes"
                    runat="server"
                    CssClass="editorNarrow"
                    Height="182px"
                    Width="354px"
                    MaxHtmlLength="5000"
                    NewLineMode="Div"
                    ToolsWidth="356px"
                    ToolbarMode="ShowOnFocus"
                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                    EditModes="Design">
                    <CssFiles>
                        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                    </CssFiles>
                </telerik:RadEditor>
            </div>
            <div class="divFormModal">
                <!--UseSubmitBehavior="false" for ie9 and chrome fix-->
                <asp:Button ID="btnEquipmentNotes" UseSubmitBehavior="false" CssClass="modalButton" Text="Submit" ValidationGroup="EquipmentNotes" runat="server" OnClick="equipmentNotesButton_Click" />
            </div>

        </asp:Panel>

        <!--REQUEST SUPPORT MODAL-->
        <ajaxToolkit:ModalPopupExtender ID="mpeRequestSupport" runat="server"
            TargetControlID="lnkRequestSupportHidden"
            PopupControlID="pnlRequestSupport"
            BackgroundCssClass="modalBackgroundLight"
            CancelControlID="lnkRequestSupportCancelHidden"
            Y="42"
            BehaviorID="mpeRequestSupportBehavior" />

        <asp:Panel ID="pnlRequestSupport" ClientIDMode="Inherit" CssClass="modalPanelLight" runat="server" DefaultButton="btnRequestSupport">
            <a href="javascript:void(0)" onclick="return HideDiagnosticsModals();" class="modalLinkLight" id="lnkRequestSupportModalCancel">Cancel</a>
            <input type="button" class="modalButtonSmall" id="btnRequestSupportModalCancel" onclick="return HideDiagnosticsModals();" title="X" />
            <asp:HiddenField ID="hdnRequestSupportInvariantValues" runat="server" />
            <asp:HiddenField ID="hdnRequestSupportValues" runat="server" />
            <asp:LinkButton ID="lnkRequestSupportHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>
            <asp:LinkButton ID="lnkRequestSupportCancelHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>

            <div id="lblRequestSupportTitle" class="modalTitleLight">Request Support</div>
            <div id="lblRequestSupportIntro" class="modalContentLight">Please request support in the event of a false positive or the equipment is functioning properly but diagnostic results appear invalid.</div>
            <asp:Label ID="lblRequestSupportError" CssClass="modalError" runat="server"></asp:Label>
            <br />
            <br />
            <div>
                <label id="lblRequestSupportMessage" class="modalContentLight">Max HTML Characters = 5000:</label>

                <telerik:RadEditor ID="editorRequestSupportMessage"
                    runat="server"
                    CssClass="editorNarrow"
                    Height="182px"
                    Width="354px"
                    MaxHtmlLength="5000"
                    NewLineMode="Div"
                    ToolsWidth="356px"
                    ToolbarMode="ShowOnFocus"
                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                    EditModes="Design">
                    <CssFiles>
                        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                    </CssFiles>
                </telerik:RadEditor>
            </div>
            <div class="divFormModal">
                <!--UseSubmitBehavior="false" for ie9 and chrome fix-->
                <asp:Button ID="btnRequestSupport" UseSubmitBehavior="false" CssClass="modalButton" Text="Submit" ValidationGroup="RequestSupport" runat="server" OnClick="requestSupportButton_Click" />
            </div>
        </asp:Panel>

        <!--CREATE NEW TASK MODAL-->
        <CW:TaskAddFormModal ID="TaskAddFormModal" runat="server" />
    </telerik:RadAjaxPanel>
    <br />
    <br />

</asp:Content>