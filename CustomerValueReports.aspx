﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="CustomerValueReports.aspx.cs" Inherits="CW.Website.CustomerValueReports" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="CW.Website._framework.httpmodules" %>
<%@ Import Namespace="CW.Common.Constants" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <script type="text/javascript">

            var ListBoxCustomValidator = function (sender, args) {
            var listBox = document.getElementById(sender.controltovalidate);
        
            //.disabled changed in framework 4.5, .classList doesnt work in ie9
            if (!$(listBox).hasClass('aspNetDisabled')) {

                var listBoxCnt = 0;

                for (var x = 0; x < listBox.options.length; x++) {
                    if (listBox.options[x].selected) listBoxCnt++;
                }

                args.IsValid = (listBoxCnt > 0)
            }
            else {
                args.IsValid = true;
            }
            };
            
    </script>

             <h1>Customer Value Reports</h1>
             <div class="richText">
                <p>The customer value reports page is used enter manual and dynamic input to generate a service value report.</p>
                <br />                
                <p>Follow the instructions below to generate and download as a pdf:</p>
                <ol>
                    <li>Make your selections and optionally fill out any content ahead of time.</li>
                    <li>Click generate below and a new page will open up with the report.</li>
                    <li>To edit content, click the dots in the right hand corner of any editable area.</li>
                    <li>Click download at the bottom of the generated report.</li>                   
                </ol>
             </div>
             <div class="updateProgressDiv">
                 <asp:UpdateProgress ID="updateProgressTop" runat="server">
                     <ProgressTemplate>
                        <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                     </ProgressTemplate>
                 </asp:UpdateProgress>
             </div>
             <div class="administrationControls">
                          
                <h2>Report Settings</h2>
                <a id="lnkSetFocusAdd" runat="server"></a>                                                  
                <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>   
                            
                <input type="hidden" id="hdnCID" name="hdnCID" runat="server" />
                <input type="hidden" id="hdnUID" name="hdnUID" runat="server" />
                <input type="hidden" id="hdnOID" name="hdnOID" runat="server" />
                <input type="hidden" id="hdnUserCulture" name="hdnUserCulture" runat="server" />

                <div class="divForm">    
                    <label class="label">*From Date:</label>
                    <telerik:RadDatePicker ID="txtFromDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                    
                    <asp:RequiredFieldValidator ID="dateFromRequiredValidator" runat="server"
                                    CssClass="errorMessage" 
                                    ErrorMessage="Date is a required field."
                                    ControlToValidate="txtFromDate"          
                                    SetFocusOnError="true"
                                    Display="None"
                                    ValidationGroup="Generate">
                                    </asp:RequiredFieldValidator>         
                    <ajaxToolkit:ValidatorCalloutExtender ID="dateFromRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="dateFromRequiredValidatorExtender" 
                                        TargetControlID="dateFromRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                    
                </div>
                <div class="divForm">
                    <label class="label">*To Date:</label>                     
                    <telerik:RadDatePicker ID="txtToDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                            
                    <asp:RequiredFieldValidator ID="dateToRequiredValidator" runat="server"
                                    CssClass="errorMessage" 
                                    ErrorMessage="Date is a required field."
                                    ControlToValidate="txtToDate"          
                                    SetFocusOnError="true"
                                    Display="None"
                                    ValidationGroup="Generate">
                                    </asp:RequiredFieldValidator>   
                    <ajaxToolkit:ValidatorCalloutExtender ID="dateToRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="dateToRequiredValidatorExtender" 
                                        TargetControlID="dateToRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                    
                    <asp:CompareValidator ID="dateCompareValidator" Display="None" runat="server"
                        CssClass="errorMessage" 
                        ErrorMessage="Invalid range."
                        ControlToValidate="txtToDate"
                        ControlToCompare="txtFromDate"
                        Type="Date"
                        Operator="GreaterThanEqual"
                        ValidationGroup="Generate"
                        SetFocusOnError="true"
                        >
                        </asp:CompareValidator>   
                    <ajaxToolkit:ValidatorCalloutExtender ID="dateCompareValidatorExtender" runat="server" 
                                        BehaviorID="dateCompareValidatorExtender" 
                                        TargetControlID="dateCompareValidator" 
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175" />                                                                 
                </div>  
                
                <div class="divForm">    
                    <label class="label">*Previous From Date:</label>
                    <telerik:RadDatePicker ID="txtPreviousFromDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                       
                    <asp:RequiredFieldValidator ID="datePreviousFromRequiredValidator" runat="server"
                                    CssClass="errorMessage" 
                                    ErrorMessage="Date is a required field."
                                    ControlToValidate="txtPreviousFromDate"          
                                    SetFocusOnError="true"
                                    Display="None"
                                    ValidationGroup="Generate">
                                    </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="datePreviousFromRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="datePreviousFromRequiredValidatorExtender" 
                                        TargetControlID="datePreviousFromRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                </div>
                <div class="divForm">
                    <label class="label">*Previous To Date:</label>                     
                    <telerik:RadDatePicker ID="txtPreviousToDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                           
                    <asp:RequiredFieldValidator ID="datePreviousToRequiredValidator" runat="server"
                                    CssClass="errorMessage" 
                                    ErrorMessage="Date is a required field."
                                    ControlToValidate="txtPreviousToDate"          
                                    SetFocusOnError="true"
                                    Display="None"
                                    ValidationGroup="Generate">
                                    </asp:RequiredFieldValidator> 
                    <ajaxToolkit:ValidatorCalloutExtender ID="datePreviousToRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="datePreviousToRequiredValidatorExtender" 
                                        TargetControlID="datePreviousToRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                           
                    <asp:CompareValidator ID="previousDateCompareValidator" Display="None" runat="server"
                        CssClass="errorMessage"
                        ErrorMessage="Invalid range."
                        ControlToValidate="txtPreviousToDate"
                        ControlToCompare="txtPreviousFromDate"
                        Type="Date"
                        Operator="GreaterThanEqual"
                        ValidationGroup="Generate"
                        SetFocusOnError="true"
                        >
                        </asp:CompareValidator> 
                    <ajaxToolkit:ValidatorCalloutExtender ID="previousDateCompareValidatorExtender" runat="server" 
                                        BehaviorID="previousDateCompareValidatorExtender" 
                                        TargetControlID="previousDateCompareValidator" 
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175" />                                                                                                                                                                                         
                </div>

                <div class="divForm">                                                        
                    <label class="label">*Buildings:</label>                                                                                                                                         
                    <asp:ListBox ID="lbBuildings" CssClass="listbox" runat="server" SelectionMode="Multiple"></asp:ListBox>
                    <asp:CustomValidator ID="buildingsCustomValidator" runat="server"  ErrorMessage="Buildings is a required field." ControlToValidate="lbBuildings" SetFocusOnError="true" ValidationGroup="Generate" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None"></asp:CustomValidator> 
                    <ajaxToolkit:ValidatorCalloutExtender ID="buildingsCustomValidatorExtender" runat="server" BehaviorID="buildingsCustomValidatorExtender" TargetControlID="buildingsCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                </div>

                            
                <div class="divForm">
                        <label class="label">*Theme:</label>
                        <asp:DropDownList CssClass="dropdown" ID="ddlTheme" runat="server">  
                        </asp:DropDownList>
                </div>
                <div class="divForm">
                    <label class="label">*Language:</label>
                    <asp:DropDownList ID="ddlLanguage" CssClass="dropdown" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">*Culture Format:</label>
                    <asp:DropDownList ID="ddlCulture" CssClass="dropdown" runat="server" />
                </div>          
                    <div class="divForm">
                    <label class="label">Show Client Logos:</label>
                    <asp:CheckBox ID="chkShowClientLogos" CssClass="checkbox" Checked="true" runat="server" />                                                                                        
                </div> 
                <div class="divForm">
                    <label class="label">Include Table Of Contents Page:</label>
                    <asp:CheckBox ID="chkIncludeContentsPage" CssClass="checkbox" runat="server" />                                                                                      
                </div>  

                <hr />
                <h2>Customer Details</h2>

                <div class="divForm">
                    <label class="label">Manual Customer Details Entry:</label>
                    <asp:CheckBox ID="chkManualCustomerDetailsEntry" CssClass="checkbox" OnCheckedChanged="chkManualCustomerDetailsEntry_OnCheckedChanged" Checked="false" AutoPostBack="true" runat="server" />                                                                                        
                </div> 

                <div id="divCustomerDetails" visible="false" runat="server">
                    <div class="divForm">
                        <label class="label">Address:</label>
                        <asp:TextBox ID="txtCustomerAddress" CssClass="textbox" MaxLength="200" runat="server" />
                    </div>
                    <div class="divForm">
                        <label class="label">City:</label>
                        <asp:TextBox CssClass="textbox" ID="txtCustomerCity" runat="server" MaxLength="100" />
                    </div>
                    <div class="divForm">
                        <label class="label">*Country:</label>
                        <asp:DropDownList CssClass="dropdown" ID="ddlCustomerCountryAlpha2Code" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                
                        </asp:DropDownList>
                    </div>
                    <div class="divForm">
                        <label class="label">State (Required based on Country):</label>
                        <asp:DropDownList ID="ddlCustomerStateID" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                        </asp:DropDownList>                                                                                                                                                              
                    </div>
                    <div class="divForm">
                        <label class="label">Zip Code (Required based on Country):</label>
                        <telerik:RadMaskedTextBox ID="txtCustomerZip" CssClass="textbox" runat="server" 
                            PromptChar="" Mask="aaaaaaaaaa" ValidationGroup="Generate"></telerik:RadMaskedTextBox>
                    </div>   
                    <div class="divForm">
                        <label class="label">Phone:</label>
                        <asp:TextBox ID="txtCustomerPhone" CssClass="textbox" MaxLength="25" runat="server" />
                    </div> 

                    <div class="divForm">
                        <label class="label">Customer Contact:</label>
                        <asp:TextBox ID="txtPrimaryContactName" CssClass="textbox" MaxLength="15" runat="server" />
                    </div>
                    <div class="divForm">
                        <label class="label">Customer Contact Email:</label>
                        <asp:TextBox ID="txtPrimaryContactEmail" CssClass="textbox" MaxLength="100" runat="server" />
                    </div>
                    <div class="divForm">
                        <label class="label">Customer Contact Phone:</label>
                        <asp:TextBox ID="txtPrimaryContactPhone" CssClass="textbox" MaxLength="25" runat="server" />
                    </div>
                                    
                    <asp:RequiredFieldValidator ID="customerCountryRequiredValidator" runat="server" ErrorMessage="Country is a required field." ControlToValidate="ddlCustomerCountryAlpha2Code" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Generate" />
                    <ajaxToolkit:ValidatorCalloutExtender ID="customerCountryRequiredValidatorExtender" runat="server" TargetControlID="customerCountryRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                    <asp:RequiredFieldValidator ID="customerStateRequiredValidator" runat="server" ErrorMessage="State is a required field." ControlToValidate="ddlCustomerStateID" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Generate" />
                    <ajaxToolkit:ValidatorCalloutExtender ID="customerStateRequiredValidatorExtender" runat="server" TargetControlID="customerStateRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                    <asp:RequiredFieldValidator ID="customerZipRequiredValidator" runat="server"
                        ErrorMessage="Zip Code is a required field."
                        ControlToValidate="txtCustomerZip"          
                        SetFocusOnError="true"
                        Display="None"
                        ValidationGroup="Generate"
                        Enabled="false">
                        </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="customerZipRequiredValidatorExtender" runat="server"
                        TargetControlID="customerZipRequiredValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>
                    <asp:RegularExpressionValidator ID="customerZipRegExValidator" runat="server"
                        ErrorMessage="Invalid Zip Format."
                        ValidationExpression="\d{5}"
                        ControlToValidate="txtCustomerZip"
                        SetFocusOnError="true" 
                        Display="None" 
                        ValidationGroup="Generate"
                        Enabled="false">
                        </asp:RegularExpressionValidator>                         
                    <ajaxToolkit:ValidatorCalloutExtender ID="customerZipRegExValidatorExtender" runat="server"
                        TargetControlID="customerZipRegExValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender> 
                    <%--<asp:RegularExpressionValidator ID="customerPhoneRegExValidator" runat="server"
                        ErrorMessage="Invalid Phone Format."
                        ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                        ControlToValidate="txtCustomerPhone"
                        SetFocusOnError="true" 
                        Display="None" 
                        ValidationGroup="Generate">
                        </asp:RegularExpressionValidator>                         
                    <ajaxToolkit:ValidatorCalloutExtender ID="customerPhoneRegExValidatorExtender" runat="server"
                        TargetControlID="customerPhoneRegExValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>--%>
                    <asp:RegularExpressionValidator ID="primaryEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtPrimaryContactEmail" SetFocusOnError="true" Display="None" ValidationGroup="Generate" />
                    <ajaxToolkit:ValidatorCalloutExtender ID="primaryEmailRegExValidatorExtender" runat="server" TargetControlID="PrimaryEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                    <%--<asp:RegularExpressionValidator ID="primaryPhoneRegExValidator" runat="server"
                        ErrorMessage="Invalid Phone Format."
                        ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                        ControlToValidate="txtPrimaryContactPhone"
                        SetFocusOnError="true" 
                        Display="None" 
                        ValidationGroup="Generate">
                        </asp:RegularExpressionValidator>                         
                    <ajaxToolkit:ValidatorCalloutExtender ID="primaryPhoneRegExValidatorExtender" runat="server"
                        TargetControlID="primaryPhoneRegExValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>--%>
                </div>

                <hr />
                <h2>Contract Details</h2>

                <div class="divForm">
                    <label class="label">*Service Plan Type:</label>
                    <asp:DropDownList CssClass="dropdown" ID="ddlPlan" runat="server" >  
                        <asp:ListItem Value="Traditional" Selected="True">Traditional</asp:ListItem>
                        <asp:ListItem Value="Plus">Plus</asp:ListItem>
                        <asp:ListItem Value="Prime">Prime</asp:ListItem>
                        <asp:ListItem Value="Ultra">Ultra</asp:ListItem>
                        <asp:ListItem Value="Max">Max</asp:ListItem>                                            
                    </asp:DropDownList>
                </div>
                                    
                <div class="divForm">    
                    <label class="label">*Contract Start Date:</label>
                    <telerik:RadDatePicker ID="txtContractStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                    
                    <asp:RequiredFieldValidator ID="dateContractStartRequiredValidator" runat="server"
                                    CssClass="errorMessage" 
                                    ErrorMessage="Date is a required field."
                                    ControlToValidate="txtContractStartDate"          
                                    SetFocusOnError="true"
                                    Display="None"
                                    ValidationGroup="Generate">
                                    </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="dateContractStartRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="dateContractStartRequiredValidatorExtender" 
                                        TargetControlID="dateContractStartRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                </div>
                <div class="divForm">
                    <label class="label">*Contract End Date:</label>                     
                    <telerik:RadDatePicker ID="txtContractEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                             
                    <asp:RequiredFieldValidator ID="dateContractEndRequiredValidator" runat="server"
                                    CssClass="errorMessage" 
                                    ErrorMessage="Date is a required field."
                                    ControlToValidate="txtContractEndDate"          
                                    SetFocusOnError="true"
                                    Display="None"
                                    ValidationGroup="Generate">
                                    </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="dateContractEndRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="dateContractEndRequiredValidatorExtender" 
                                        TargetControlID="dateContractEndRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                    <asp:CompareValidator ID="contractDateCompareValidator" Display="None" runat="server"
                        CssClass="errorMessage"
                        ErrorMessage="Invalid range."
                        ControlToValidate="txtContractEndDate"
                        ControlToCompare="txtContractStartDate"
                        Type="Date"
                        Operator="GreaterThanEqual"
                        ValidationGroup="Generate"
                        SetFocusOnError="true"
                        >
                        </asp:CompareValidator>  
                    <ajaxToolkit:ValidatorCalloutExtender ID="contractDateCompareValidatorExtender" runat="server" 
                                        BehaviorID="contractDateCompareValidatorExtender" 
                                        TargetControlID="contractDateCompareValidator" 
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175" />                                       
                </div>


                <hr />
                <h2>Support Team</h2>

                <div class="divForm">
                    <label class="label">Primary Service Technician:</label>
                    <asp:TextBox ID="txtPrimaryServiceTechnicianName" CssClass="textbox" MaxLength="15" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Primary Service Technician Email:</label>
                    <asp:TextBox ID="txtPrimaryServiceTechnicianEmail" CssClass="textbox" MaxLength="100" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Primary Service Technician Phone:</label>
                    <asp:TextBox ID="txtPrimaryServiceTechnicianPhone" CssClass="textbox" MaxLength="25" runat="server" />
                </div>

                <div class="divForm">
                    <label class="label">Secondary Service Technician:</label>
                    <asp:TextBox ID="txtSecondaryServiceTechnicianName" CssClass="textbox" MaxLength="15" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Secondary Service Technician Email:</label>
                    <asp:TextBox ID="txtSecondaryServiceTechnicianEmail" CssClass="textbox" MaxLength="100" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Secondary Service Technician Phone:</label>
                    <asp:TextBox ID="txtSecondaryServiceTechnicianPhone" CssClass="textbox" MaxLength="25" runat="server" />
                </div>

                <div class="divForm">
                    <label class="label">Sales Engineer:</label>
                    <asp:TextBox ID="txtSalesEngineerName" CssClass="textbox" MaxLength="15" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Sales Engineer Email:</label>
                    <asp:TextBox ID="txtSalesEngineerEmail" CssClass="textbox" MaxLength="100" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Sales Engineer Phone:</label>
                    <asp:TextBox ID="txtSalesEngineerPhone" CssClass="textbox" MaxLength="25" runat="server" />
                </div>

                <div class="divForm">
                    <label class="label">Remote Center Engineer:</label>
                    <asp:TextBox ID="txtRemoteCenterEngineerName" CssClass="textbox" MaxLength="15" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Remote Center Engineer Email:</label>
                    <asp:TextBox ID="txtRemoteCenterEngineerEmail" CssClass="textbox" MaxLength="100" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Remote Center Engineer Phone:</label>
                    <asp:TextBox ID="txtRemoteCenterEngineerPhone" CssClass="textbox" MaxLength="25" runat="server" />
                </div>

                <div class="divForm">
                    <label class="label">Admin Contact Point:</label>
                    <asp:TextBox ID="txtAdminContactPointName" CssClass="textbox" MaxLength="15" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Admin Contact Point Email:</label>
                    <asp:TextBox ID="txtAdminContactPointEmail" CssClass="textbox" MaxLength="100" runat="server" />
                </div>
                <div class="divForm">
                    <label class="label">Admin Contact Point Phone:</label>
                    <asp:TextBox ID="txtAdminContactPointPhone" CssClass="textbox" MaxLength="25" runat="server" />
                </div>

                <div class="divForm">
                    <label class="label">Company Service Phone:</label>
                    <asp:TextBox ID="txtCompanyServicePhone" CssClass="textbox" MaxLength="25" runat="server" />
                </div>

                <asp:RegularExpressionValidator ID="primaryServiceTechnicianEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtPrimaryServiceTechnicianEmail" SetFocusOnError="true" Display="None" ValidationGroup="Generate" />
                <ajaxToolkit:ValidatorCalloutExtender ID="primaryServiceTechnicianEmailRegExValidatorExtender" runat="server" TargetControlID="PrimaryServiceTechnicianEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                <%--<asp:RegularExpressionValidator ID="primaryServiceTechnicianPhoneRegExValidator" runat="server"
                    ErrorMessage="Invalid Phone Format."
                    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                    ControlToValidate="txtPrimaryServiceTechnicianPhone"
                    SetFocusOnError="true" 
                    Display="None" 
                    ValidationGroup="Generate">
                    </asp:RegularExpressionValidator>                         
                <ajaxToolkit:ValidatorCalloutExtender ID="primaryServiceTechnicianPhoneRegExValidatorExtender" runat="server"
                    TargetControlID="primaryServiceTechnicianPhoneRegExValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>--%>

                <asp:RegularExpressionValidator ID="secondaryServiceTechnicianEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtSecondaryServiceTechnicianEmail" SetFocusOnError="true" Display="None" ValidationGroup="Generate" />
                <ajaxToolkit:ValidatorCalloutExtender ID="secondaryServiceTechnicianEmailRegExValidatorExtender" runat="server" TargetControlID="secondaryServiceTechnicianEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                <%--<asp:RegularExpressionValidator ID="secondaryServiceTechnicianPhoneRegExValidator" runat="server"
                    ErrorMessage="Invalid Phone Format."
                    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                    ControlToValidate="txtSecondaryServiceTechnicianPhone"
                    SetFocusOnError="true" 
                    Display="None" 
                    ValidationGroup="Generate">
                    </asp:RegularExpressionValidator>                         
                <ajaxToolkit:ValidatorCalloutExtender ID="secondaryServiceTechnicianPhoneRegExValidatorExtender" runat="server"
                    TargetControlID="secondaryServiceTechnicianPhoneRegExValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>--%>

                <asp:RegularExpressionValidator ID="salesEngineerEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtSalesEngineerEmail" SetFocusOnError="true" Display="None" ValidationGroup="Generate" />
                <ajaxToolkit:ValidatorCalloutExtender ID="salesEngineerEmailRegExValidatorExtender" runat="server" TargetControlID="salesEngineerEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                <%--<asp:RegularExpressionValidator ID="salesEngineerPhoneRegExValidator" runat="server"
                    ErrorMessage="Invalid Phone Format."
                    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                    ControlToValidate="txtSalesEngineerPhone"
                    SetFocusOnError="true" 
                    Display="None" 
                    ValidationGroup="Generate">
                    </asp:RegularExpressionValidator>                         
                <ajaxToolkit:ValidatorCalloutExtender ID="salesEngineerPhoneRegExValidatorExtender" runat="server"
                    TargetControlID="salesEngineerPhoneRegExValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>--%>

                <asp:RegularExpressionValidator ID="remoteCenterEngineerEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtRemoteCenterEngineerEmail" SetFocusOnError="true" Display="None" ValidationGroup="Generate" />
                <ajaxToolkit:ValidatorCalloutExtender ID="remoteCenterEngineerEmailRegExValidatorExtender" runat="server" TargetControlID="remoteCenterEngineerEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                <%--<asp:RegularExpressionValidator ID="remoteCenterEngineerPhoneRegExValidator" runat="server"
                    ErrorMessage="Invalid Phone Format."
                    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                    ControlToValidate="txtRemoteCenterEngineerPhone"
                    SetFocusOnError="true" 
                    Display="None" 
                    ValidationGroup="Generate">
                    </asp:RegularExpressionValidator>                         
                <ajaxToolkit:ValidatorCalloutExtender ID="remoteCenterEngineerPhoneRegExValidatorExtender" runat="server"
                    TargetControlID="remoteCenterEngineerPhoneRegExValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>--%>

                <asp:RegularExpressionValidator ID="adminContactPointEmailRegExValidator" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtAdminContactPointEmail" SetFocusOnError="true" Display="None" ValidationGroup="Generate" />
                <ajaxToolkit:ValidatorCalloutExtender ID="adminContactPointEmailRegExValidatorExtender" runat="server" TargetControlID="adminContactPointEmailRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                <%--<asp:RegularExpressionValidator ID="adminContactPointPhoneRegExValidator" runat="server"
                    ErrorMessage="Invalid Phone Format."
                    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                    ControlToValidate="txtAdminContactPointPhone"
                    SetFocusOnError="true" 
                    Display="None" 
                    ValidationGroup="Generate">
                    </asp:RegularExpressionValidator>                         
                <ajaxToolkit:ValidatorCalloutExtender ID="adminContactPointPhoneRegExValidatorExtender" runat="server"
                    TargetControlID="adminContactPointPhoneRegExValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>
                <asp:RegularExpressionValidator ID="companyServicePhoneRegExValidator" runat="server"
                    ErrorMessage="Invalid Phone Format."
                    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                    ControlToValidate="txtCompanyServicePhone"
                    SetFocusOnError="true" 
                    Display="None" 
                    ValidationGroup="Generate">
                    </asp:RegularExpressionValidator>                         
                <ajaxToolkit:ValidatorCalloutExtender ID="companyServicePhoneRegExValidatorExtender" runat="server"
                    TargetControlID="companyServicePhoneRegExValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>--%>

                <hr />
                <h2>Key Indicators Report</h2>
                                                                   
                <div class="divForm">
                    <label class="label">Include Key Indicators:</label>
                    <asp:CheckBox ID="chkIncludeKeyIndicatorsReport" CssClass="checkbox" OnCheckedChanged="chkIncludeKeyIndicatorsReport_OnCheckedChanged" Checked="true" AutoPostBack="true" runat="server" />                                                                                      
                </div>
                <div class="divForm">
                    <label class="label">Manual Key Indicators Entry:</label>
                    <asp:CheckBox ID="chkManualKeyIndicatorsEntry" CssClass="checkbox" OnCheckedChanged="chkManualKeyIndicatorsEntry_OnCheckedChanged" Checked="false" AutoPostBack="true" runat="server" />                                                                                      
                </div>
                <div id="divKeyIndicators" visible="true" runat="server">
                    <div id="divManualKeyIndicators" visible="false" runat="server">

                    <div class="divForm">
                        <label class="label">Energy Anomalies Priority:</label>
                        <asp:DropDownList CssClass="dropdown" ID="ddlEnergyAnomaliesPriority" runat="server" >  
                            <asp:ListItem Value="Up">Up</asp:ListItem>
                            <asp:ListItem Value="Right" Selected="true">Right</asp:ListItem>
                            <asp:ListItem Value="Down">Down</asp:ListItem>         
                            <asp:ListItem Value="NA">N/A</asp:ListItem>                        
                        </asp:DropDownList>
                    </div>
                    <div class="divForm">
                        <label class="label">Maintenance Anomalies Priority:</label>
                        <asp:DropDownList CssClass="dropdown" ID="ddlMaintenanceAnomaliesPriority" runat="server" >  
                            <asp:ListItem Value="Up">Up</asp:ListItem>
                            <asp:ListItem Value="Right" Selected="true">Right</asp:ListItem>
                            <asp:ListItem Value="Down">Down</asp:ListItem>     
                            <asp:ListItem Value="NA">N/A</asp:ListItem>                       
                        </asp:DropDownList>
                    </div>
                    <div class="divForm">
                        <label class="label">Comfort Anomalies Priority:</label>
                        <asp:DropDownList CssClass="dropdown" ID="ddlComfortAnomaliesPriority" runat="server" >  
                            <asp:ListItem Value="Up">Up</asp:ListItem>
                            <asp:ListItem Value="Right" Selected="true">Right</asp:ListItem>
                            <asp:ListItem Value="Down">Down</asp:ListItem>   
                            <asp:ListItem Value="NA">N/A</asp:ListItem>                         
                        </asp:DropDownList>
                    </div>
                    <div class="divForm">
                        <label class="label">Avoidable Cost Change:</label>
                        <asp:DropDownList CssClass="dropdown" ID="ddlAvoidableCostChange" runat="server" >  
                            <asp:ListItem Value="Up">Up</asp:ListItem>
                            <asp:ListItem Value="Right" Selected="true">Right</asp:ListItem>
                            <asp:ListItem Value="Down">Down</asp:ListItem>    
                            <asp:ListItem Value="NA">N/A</asp:ListItem>                        
                        </asp:DropDownList>
                    </div>
                    <div class="divForm">
                        <label class="label">Avoidable Cost:</label>
                        <asp:TextBox  ID="txtAvoidableCost" CssClass="textbox" MaxLength="10" runat="server"></asp:TextBox>
                        <p>(Note: Monetary symbol will be generated based on building(s) culture setting.)</p>
                     </div>
                    
                    </div> 

                    <div class="divForm">
                        <label class="label">Facility Rating System:</label>
                        <asp:TextBox  ID="txtRatingSystem" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                    </div>
                    <div class="divForm">
                        <label class="label">Facility Rating/Score Value:</label>
                        <asp:TextBox  ID="txtRatingValue" CssClass="textbox" MaxLength="10" runat="server"></asp:TextBox>
                    </div>

                    <div class="divForm">
                        <label class="label">Criticality of Alarms:</label>
                        <asp:DropDownList CssClass="dropdown" ID="ddlCriticalityAlarms" runat="server" >  
                            <asp:ListItem Value="Up">Up</asp:ListItem>
                            <asp:ListItem Value="Right" Selected="true">Right</asp:ListItem>
                            <asp:ListItem Value="Down">Down</asp:ListItem>       
                            <asp:ListItem Value="NA">N/A</asp:ListItem>                         
                        </asp:DropDownList>
                    </div>
                    <div class="divForm">
                        <label class="label"># of Critical Alarms Notified to Customer:</label>
                        <asp:TextBox  ID="txtAlarmsNotified" CssClass="textbox" MaxLength="10" runat="server"></asp:TextBox>
                    </div>
                    <div class="divForm">
                        <label class="label"># of Critical Alarms Fixed (First time Fix Rate):</label>
                        <asp:TextBox  ID="txtAlarmsFixed" CssClass="textbox" MaxLength="10" runat="server"></asp:TextBox>
                    </div>
                    <div class="divForm">
                        <label class="label"># of Critical Alarms Requiring On-Site Action:</label>
                        <asp:TextBox  ID="txtAlarmsAction" CssClass="textbox" MaxLength="10" runat="server"></asp:TextBox>
                    </div>
                    <div class="divForm">
                        <label class="label"># of Critical Issues fixed On-Site:</label>
                        <asp:TextBox  ID="txtIssueFixed" CssClass="textbox" MaxLength="10" runat="server"></asp:TextBox>
                    </div>
                    <div class="divForm">
                        <label class="label">Response time (From Call Logged):</label>
                        <asp:TextBox  ID="txtResponseTime" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                    </div>
                    <div class="divForm">
                        <label class="label">Time Onsite for the Repair:</label>
                        <asp:TextBox  ID="txtTimeOnsite" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                    </div>
                    <div class="divForm">
                        <label class="label">Mean Time to Repair:</label>
                        <asp:TextBox  ID="txtMeanTime" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                    </div>

                </div>

                <hr />
                <h2>Items Requiring Attention Report</h2>
                                                                   
                <div class="divForm">
                    <label class="label">Include Items Requiring Attention:</label>
                    <asp:CheckBox ID="chkIncludeItemsRequiringAttention" CssClass="checkbox" OnCheckedChanged="chkIncludeItemsRequiringAttention_OnCheckedChanged" Checked="true" AutoPostBack="true" runat="server" />                                                                                      
                </div>

                <div id="divItemsRequiringAttention" visible="true" runat="server">
                    <div class="divForm">
                        <label class="label">List of Field Quotes:</label>
                        <textarea name="txtQuotes" id="txtQuotes" cols="60" rows="10" runat="server"></textarea>   
                        <p>(Columns: Field Quote Number, Quote Date, Focus Areas, Amount)</p>           
                        <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                        <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                    </div>
                </div>            

                <hr />
                <h2>Performance Charts Report</h2>
                                                                   
                <div class="divForm">
                    <label class="label">Include Performance Charts:</label>
                    <asp:CheckBox ID="chkIncludePerformanceCharts" CssClass="checkbox" OnCheckedChanged="chkIncludePerformanceCharts_OnCheckedChanged" Checked="true" AutoPostBack="true" runat="server" />                                                                                      
                </div>
                <div class="divForm">
                    <label class="label">Manual Performance Charts Entry:</label>
                    <asp:CheckBox ID="chkManualPerformanceChartsEntry" CssClass="checkbox" OnCheckedChanged="chkManualPerformanceChartsEntry_OnCheckedChanged" Checked="false" AutoPostBack="true" runat="server" />                                                                                      
                </div>
                <div id="divPerformanceCharts" visible="true" runat="server">
                    <div id="divManualPerformanceCharts" visible="false" runat="server">
                        <div class="divForm">
                            <label class="label">Energy Chart Data Values:</label>
                            <textarea name="txtEnergyChartData" id="txtEnergyChartData" cols="60" rows="10" runat="server"></textarea>           
                            <p>(Columns: Date, Total Incidents, Avoidable Cost)</p>  
                            <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                            <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                        </div> 
                        <div class="divForm">
                            <label class="label">Maintenance Chart Data Values:</label>
                            <textarea name="txtMaintenanceChartData" id="txtMaintenanceChartData" cols="60" rows="10" runat="server"></textarea>           
                            <p>(Columns: Date, Total Incidents, Average Priority)</p>  
                            <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                            <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                        </div>                        
                        <div class="divForm">
                            <label class="label">Comfort Chart Data Values:</label>
                            <textarea name="txtComfortChartData" id="txtComfortChartData" cols="60" rows="10" runat="server"></textarea>           
                            <p>(Columns: Date, Value)</p>  
                            <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                            <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                        </div>                        
                    </div>
                    <div class="divForm">
                        <label class="label">Energy Work Order Data Values:</label>
                        <textarea name="txtEnergyWorkOrderData" id="txtEnergyWorkOrderData" cols="60" rows="10" runat="server"></textarea>           
                        <p>(Columns: Date, Work Order #)</p>  
                        <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                        <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                    </div>
                    <div class="divForm">
                        <label class="label">Maintenance Work Order Data Values:</label>
                        <textarea name="txtMaintenanceWorkOrderData" id="txtMaintenanceWorkOrderData" cols="60" rows="10" runat="server"></textarea>           
                        <p>(Columns: Date, Work Order #)</p>  
                        <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                        <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                    </div>
                    <div class="divForm">
                        <label class="label">Comfort Work Order Data Values:</label>
                        <textarea name="txtComfortWorkOrderData" id="txtComfortWorkOrderData" cols="60" rows="10" runat="server"></textarea>           
                        <p>(Columns: Date, Work Order #)</p>  
                        <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                        <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                    </div>
                </div> 

                <hr />
                <h2>Service Details</h2>
                                                                   
                <div class="divForm">
                    <label class="label">Include Service Details:</label>
                    <asp:CheckBox ID="chkIncludeServiceDetails" CssClass="checkbox" OnCheckedChanged="chkIncludeServiceDetails_OnCheckedChanged" Checked="true" AutoPostBack="true" runat="server" />                                                                                      
                </div>

                <div id="divServiceDetails" visible="true" runat="server">
                    <div class="divForm">
                        <label class="label">Service Details:</label>
                        <textarea name="txtServiceDetails" id="txtServiceDetails" cols="60" rows="10" runat="server"></textarea>   
                        <p>(Columns: Date, Work Order #, Engineer Name, Problem Description, Action Taken, Impact Achieved)</p>           
                        <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                        <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                    </div>
                </div>      

                <hr />
                <h2>Custom Satisfaction (CSAT) Metrics</h2>
                                                                   
                <div class="divForm">
                    <label class="label">Include CSAT Metrics:</label>
                    <asp:CheckBox ID="chkIncludeMetrics" CssClass="checkbox" OnCheckedChanged="chkIncludeMetrics_OnCheckedChanged" Checked="true" AutoPostBack="true" runat="server" />                                                                                      
                </div>

                <div id="divMetrics" visible="true" runat="server">
                    <div class="divForm">
                        <label class="label">Technician (Work Order) Surveys:</label>
                        <textarea name="txtSurveys" id="txtSurveys" cols="60" rows="10" runat="server"></textarea>   
                        <p>(Columns: Technician, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec, Total)</p>           
                        <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                        <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                    </div>
                    <div class="divForm">
                        <label class="label">CSAT Feedback Loop:</label>
                        <textarea name="txtFeedback" id="txtFeedback" cols="60" rows="10" runat="server"></textarea>   
                        <p>(Columns: Work Order #, Date, Tech, Tech Score, Overall Customer Satisfaction Score, Reasons for Customer Satisfaction Score, Action Taken)</p>           
                        <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                        <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                    </div>
                </div>      

                <hr />
                <h2>Site Visits</h2>
                                                                   
                <div class="divForm">
                    <label class="label">Include Site Visits:</label>
                    <asp:CheckBox ID="chkIncludeSiteVisits" CssClass="checkbox" OnCheckedChanged="chkIncludeSiteVisits_OnCheckedChanged" Checked="true" AutoPostBack="true" runat="server" />                                                                                      
                </div>

                <div id="divSiteVisits" visible="true" runat="server">
                    <div class="divForm">
                        <label class="label">Site Visits Breakdown:</label>
                        <textarea name="txtBreakdown" id="txtBreakdown" cols="60" rows="10" runat="server"></textarea>   
                        <p>(Columns: Title, Percent)</p>   
                        <p>(Note: Make sure your percents add up to 100, otherwise it will not be an accurate representation. Alternatively you can input the number of site visits instead of the actual percents, and it will graph appropriately.)</p>        
                        <p>(Note: Save as csv from excel. Open in notepad and copy and paste. Include headers. Remove any carraige returns at end of pasted content.)</p>                                                                     
                        <p>(Warning: Manual csv creation or generation other than from excel will fail to parse due to commas in content and unescapiong of quote's.)</p>
                    </div>
                    
                    <div class="divFormLeftmost">
                    <label class="labelLeft">Remarks (Max HTML Characters = 10000):</label>
                    <telerik:RadEditor ID="editorRemarks"
                                runat="server"                                                                                                                                                
                                CssClass="editorNewLine"                                                        
                                Height="475px" 
                                Width="674px"
                                MaxHtmlLength="10000"
                                NewLineMode="Div"
                                ToolsWidth="676px"                                                                                                                                       
                                ToolbarMode="ShowOnFocus"        
                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                                > 
                                <CssFiles>
                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                </CssFiles>                                                                                                                             
                    </telerik:RadEditor>                                 
                    </div>

                </div>      
          
                <hr />
                <h2>Generate Report</h2>
                <div class="richText">
                    You may generate this report now to provide expert input, alter content, and download as a pdf.
                </div>

                <div class="divForm">
                    <asp:LinkButton CssClass="lnkButton" ID="btnGenerate" runat="server" Text="Generate Now" PostBackUrl="~/CustomerValueReport.aspx" OnClientClick="window.document.forms[0].target='_blank';"  ValidationGroup="Generate"></asp:LinkButton>
                </div>

             </div>

</asp:Content>                