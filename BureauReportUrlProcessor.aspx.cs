﻿using System;
using System.Web.UI;
using System.Configuration;
using System.Net;
using System.IO;
using CW.Utility;

using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web.UI.DataVisualization.Charting;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Linq;
using EO.Pdf;
using System.Collections.Generic;
using CW.Common.Constants;
using CW.Logging;
using CW.Website.DependencyResolution;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class BureauReportUrlProcessor : Page
    {
        #region Fields

        const string invisibleElementIds = "hdn_container;" +
                                            "btnAutomatedSummary;editorAutomatedSummary;btnExpertSummary;editorExpertSummary;" +
                                            "btnBuildingSummary;btnEquipmentClassSummary;" +
                                            "btnEnergyTrend;editorEnergyTrend;btnMaintenanceTrend;editorMaintenanceTrend;btnComfortTrend;editorComfortTrend;" +
                                            "btnTopEnergy;btnTopMaintenance;btnTopComfort;" +
                                            "btnVentilationSummary;btnHeatingSummary;btnCoolingSummary;btnPlantSummary;btnZoneSummary;btnSpecialtySummary;" +
                                            "btnFrequentAlarms;btnLongestAlarms;btnTotalAlarms;" +
                                            "btnProjects;btnOpenTasks;btnCompletedTasks;" +
                                            "divDownload;";
        #endregion

        #region Properties

        private ISectionLogManager Logger { get; set; }
        private EssentialObjectsHelper EOHelper { get; set; }

        #endregion 

        protected void Page_Init(object sender, EventArgs e)
        {
            Logger = IoC.Resolve<ISectionLogManager>();
            EOHelper = new EssentialObjectsHelper(invisibleElementIds, Logger);

            if (Request.QueryString != null && Request.QueryString.HasKeys())
            {
                try
                {

                    //Instanciate
                    PdfDocument pdf = new PdfDocument();
                    MemoryStream ms = new MemoryStream();

                    //Building url
                    string url = "http://" + RoleEnvironment.CurrentRoleInstance.InstanceEndpoints.Values.First().IPEndpoint.ToString() + "/BureauReportv2.aspx" + Request.Url.Query;

                    //Convert Url
                    HtmlToPdfResult htmlToPdfResult = HtmlToPdf.ConvertUrl(url, pdf, EOHelper.options);

                    //Post pdf 
                    pdf = BuildPostReportBookmarksAndAnchors(htmlToPdfResult, pdf);

                    //Save to memory stream
                    pdf.Save(ms);

                    //Write to bytes and close
                    byte[] byteArray = ms.ToArray();
                    ms.Flush();
                    ms.Close();

                    //Write response
                    Response.Clear();
                    Response.AddHeader("Content-Length", byteArray.Length.ToString());
                    Response.ContentType = "application/octet-stream";
                    //ms.WriteTo(Response.OutputStream);
                    Response.BinaryWrite(byteArray);
                }
                catch (Exception ex)
                {
                    Logger.Log(DataConstants.LogLevel.ERROR, "Bureau report url processor failed to set pdf options, convert url, or return the response.", ex);
                }
            }

            //Flush and close response
            Response.Flush();
            Response.Close();
            Response.End();
        }

        private PdfDocument BuildPostReportBookmarksAndAnchors(HtmlToPdfResult htmlToPdfResult, PdfDocument pdf)
        {
            //Set Bookmarks and Anchors
            List<Tuple<string, string>> htmlBookmarkTags = new List<Tuple<string, string>>();
            List<Tuple<string, string>> htmlAnchorTags = new List<Tuple<string, string>>();

            //Order matters        
            htmlBookmarkTags.Add(Tuple.Create("divPortfolioSummaryPage", "lblPortfolioSummaryPage")); //use label for bookmark for portfolio, the title doenst contain all content.
            htmlAnchorTags.Add(Tuple.Create("lblPortfolioSummaryPage", "divPortfolioSummaryPage"));

            //no expert summary page


            if (!String.IsNullOrEmpty(Request.QueryString["IncludeBuildingSummaryReport"]) && Convert.ToBoolean(Request.QueryString["IncludeBuildingSummaryReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divBuildingSummaryPage", "lblBuildingSummaryPage"));
                htmlAnchorTags.Add(Tuple.Create("lblBuildingSummaryPage", "divBuildingSummaryPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeEquipmentClassSummaryReport"]) && Convert.ToBoolean(Request.QueryString["IncludeEquipmentClassSummaryReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divEquipmentClassSummaryPage", "lblEquipmentClassSummaryPage"));
                htmlAnchorTags.Add(Tuple.Create("lblEquipmentClassSummaryPage", "divEquipmentClassSummaryPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeBuildingTrendsSummaryReport"]) && Convert.ToBoolean(Request.QueryString["IncludeBuildingTrendsSummaryReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divBuildingTrendsPage", "lblBuildingTrendsPage"));
                htmlAnchorTags.Add(Tuple.Create("lblBuildingTrendsPage", "divBuildingTrendsPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeBuildingTopIssuesSummaryReport"]) && Convert.ToBoolean(Request.QueryString["IncludeBuildingTopIssuesSummaryReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divBuildingTopIssuesPage", "lblBuildingTopIssuesPage"));
                htmlAnchorTags.Add(Tuple.Create("lblBuildingTopIssuesPage", "divBuildingTopIssuesPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeVentilationEquipmentReport"]) && Convert.ToBoolean(Request.QueryString["IncludeVentilationEquipmentReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divVentilationSummaryPage", "lblVentilationSummaryPage"));
                htmlAnchorTags.Add(Tuple.Create("lblVentilationSummaryPage", "divVentilationSummaryPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeHeatingEquipmentReport"]) && Convert.ToBoolean(Request.QueryString["IncludeHeatingEquipmentReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divHeatingSummaryPage", ""));
                htmlAnchorTags.Add(Tuple.Create("lblHeatingSummaryPage", "divHeatingSummaryPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeCoolingEquipmentReport"]) && Convert.ToBoolean(Request.QueryString["IncludeCoolingEquipmentReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divCoolingSummaryPage", "lblCoolingSummaryPage"));
                htmlAnchorTags.Add(Tuple.Create("lblCoolingSummaryPage", "divCoolingSummaryPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludePlantEquipmentReport"]) && Convert.ToBoolean(Request.QueryString["IncludePlantEquipmentReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divPlantSummaryPage", "lblPlantSummaryPage"));
                htmlAnchorTags.Add(Tuple.Create("lblPlantSummaryPage", "divPlantSummaryPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeZoneEquipmentReport"]) && Convert.ToBoolean(Request.QueryString["IncludeZoneEquipmentReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divZoneSummaryPage", "lblZoneSummaryPage"));
                htmlAnchorTags.Add(Tuple.Create("lblZoneSummaryPage", "divZoneSummaryPage"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeSpecialtyEquipmentReport"]) && Convert.ToBoolean(Request.QueryString["IncludeSpecialtyEquipmentReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divSpecialtySummaryPage", "lblSpecialtySummaryPage"));
                htmlAnchorTags.Add(Tuple.Create("lblSpecialtySummaryPage", "divSpecialtySummaryPage"));
            }
            //if Performance soon
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeAlarmReport"]) && Convert.ToBoolean(Request.QueryString["IncludeAlarmReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divAlarms", "lblAlarms"));
                htmlAnchorTags.Add(Tuple.Create("lblAlarms", "divAlarms"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeProjectReport"]) && Convert.ToBoolean(Request.QueryString["IncludeProjectReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divProjects", "lblProjects"));
                htmlAnchorTags.Add(Tuple.Create("lblProjects", "divProjects"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeTaskReport"]) && Convert.ToBoolean(Request.QueryString["IncludeTaskReport"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divTasks", "lblTasks"));
                htmlAnchorTags.Add(Tuple.Create("lblTasks", "divTasks"));
            }
            if (!String.IsNullOrEmpty(Request.QueryString["IncludeTopDiagnosticsDetails"]) && Convert.ToBoolean(Request.QueryString["IncludeTopDiagnosticsDetails"]))
            {
                htmlBookmarkTags.Add(Tuple.Create("divTopDiagnostics", "lblTopDiagnostics"));
                htmlAnchorTags.Add(Tuple.Create("lblTopDiagnostics", "divTopDiagnostics"));
            }

            //Generate bookmarks and anchors from set
            EOHelper.GenerateBookmarks(htmlToPdfResult, pdf, htmlBookmarkTags);

            if (!String.IsNullOrEmpty(Request.QueryString["IncludeContentsPage"]) && Convert.ToBoolean(Request.QueryString["IncludeContentsPage"]))
                EOHelper.GenerateAnchors(htmlToPdfResult, htmlAnchorTags);

            return pdf;
        }
    }
}