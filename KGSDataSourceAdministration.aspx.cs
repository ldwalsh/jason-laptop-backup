﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.DataSources;
using System;

namespace CW.Website
{
    public partial class KGSDataSourceAdministration : AdminSitePageTemp
    {
        #region events
        
            private void Page_FirstLoad()
            {
                TabStateMonitor.ChangeState(new Enum[] { KGSDataSourceAdministration.TabMessages.Init });
            }

        #endregion

        public enum TabMessages
        {
            Init,
            AddDataSource,
            EditDataSource,
            DeleteDataSource
        }

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewDataSources).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.KGS; } }

            #endregion

        #endregion
    }
}