﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.System;
using System;

namespace CW.Website
{
    public partial class SystemTest : AdminSitePageTemp
    {
        #region properties
        
            protected override String DefaultControl { get { return typeof(IndividualDataSourceCheck).Name; } }

        #endregion

        #region events

            void Page_Load()
            {
                //logged in security check in master page.            
                //radTabStrip.Tabs.FindTabByText("Analyses Check").Visible = (siteUser.IsKGSSuperAdmin);
            }

        #endregion
    }
}