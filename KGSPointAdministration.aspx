﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSPointAdministration.aspx.cs" Inherits="CW.Website.KGSPointAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/Points/ViewPoints.ascx" tagname="ViewPoints" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Points/AddPoint.ascx" tagname="AddPoint" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Points/AddExistingPoints.ascx" tagname="AddExistingPoints" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Points/UnassociateExistingPoints.ascx" tagname="UnassociateExistingPoints" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Points/BulkEditPoints.ascx" tagname="BulkEditPoints" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">                	                  	
    
      <h1>KGS Point Administration</h1>

      <div class="richText">The kgs point administration area is used to view, add, and edit points.</div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>                                       
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div> 
       
      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" CausesValidation="false" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Points" />
            <telerik:RadTab Text="Add New Point" />
            <telerik:RadTab Text="Add Existing Points" />
            <telerik:RadTab Text="Bulk Unassociate Points" />
            <telerik:RadTab Text="Bulk Edit Points" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:ViewPoints ID="ViewPoints" runat="server" />                                                                        
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server"> 
            <CW:AddPoint ID="AddPoint" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView3" runat="server">  
            <CW:AddExistingPoints ID="AddExistingPoints" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView4" runat="server">  
            <CW:UnassociateExistingPoints ID="UnassociateExistingPoints" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView5" runat="server"> 
            <CW:BulkEditPoints ID="BulkEditPoints" runat="server" />
          </telerik:RadPageView>                                                                
        </telerik:RadMultiPage>                     
      </div>
       
</asp:Content>