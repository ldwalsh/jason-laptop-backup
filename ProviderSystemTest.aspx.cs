﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.System;
using System;

namespace CW.Website
{
    public partial class ProviderSystemTest : AdminSitePageTemp
    {
        #region properties

            protected override String DefaultControl { get { return typeof(IndividualDataSourceCheck).Name; } }

            public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.PROVIDER; } }

        #endregion

        #region events
                                                        
        private void Page_Load()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");            
        }

        #endregion
    }
}