﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;
using CW.Diagnostics.Helpers;
using CW.Utility;
using CW.Website._extensions;
using CW.Website._framework;

namespace CW.Website
{
    public partial class UserAdministration : ModulePage
    {
        #region Properties

            private User mUser;           
            const string userExists = "User already exists.";
            const string addUserSuccess = " user addition was successful.";
            const string faliedTransaction = "This transaction has failed. Please contact your system admin for more help.";
            const string assigningBuildingsFailed = " Error assigning buildings to the user.";
            const string reassigningBuildingsFailed = " Error reassigning buildings to the user.";
            const string assigningBuildingGroupsFailed = " Error assigning building groups to the user.";
            const string reassigningBuildingGroupsFailed = " Error reassigning buildings to the user.";
            //const string assigningGaugesFailed = " Error assigning gauges to the user.";
            //const string reassigningGaugesFailed = " Error reassigning gauges to the user.";
            const string assigningQuickLinksFailed = "Error assigning quicklinks to the user.";
            const string reassigningQuickLinksFailed = "Error reassigning quicklinks to the user.";
            const string addingAuditForUserFailed = " Error adding audit for user.";             
            const string updateSuccessful = " user update was successful.";
            const string updateFailed = "User update failed. Please contact an administrator.";
            const string deleteSuccessful = "User deletion was successful.";
            const string deleteFailed = "User deletion failed. Please contact an administrator.";
            const string buildingsExist = "Cannot delete user because user is associated with one or more buildings. Please remove building association first.";
            const string buildingGroupsExist = "Cannot delete user because user is associated with one or more building groups. Please remove building group associations first."; 
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "Email";

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page
                
                //secondary security check 
                if (siteUser.IsSuperAdmin)
                {
                    //hide labels
                    lblErrors.Visible = false;
                    lblAddError.Visible = false;
                    lblEditError.Visible = false;

                    //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                    if (!Page.IsPostBack)
                    {
                        BindUsers();

                        ViewState.Clear();
                        GridViewSortDirection = initialSortDirection;
                        GridViewSortExpression = initialSortExpression;

                        BindRoles(ddlEditRole);
                        BindRoles(ddlAddRole);

                        BindStates(ddlEditState);
                        BindStates(ddlAddState);

                        //set admin global settings
                        //Make sure to decode from ascii to html 
                        GlobalSetting globalSetting = GlobalDataMapper.GetGlobalSettings();
                        litAdminUsersBody.Text = Server.HtmlDecode(globalSetting.AdminUsersBody);

                        Session["Search"] = "";
                    }
                }
                else
                {
                    Response.Redirect("Administration.aspx");
                }

                //set enter key to submit search
                txtSearch.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + searchBtn.UniqueID + "','')");
            }

        #endregion

        #region Set Fields and Data

            protected void SetUserIntoEditForm(User user)
            {
                //ID
                hdnEditID.Value = Convert.ToString(user.UID);
                //First Name
                txtEditFirstName.Text = user.FirstName;
                //Last Name
                txtEditLastName.Text = user.LastName;
                //Email
                txtEditEmail.Text = user.Email;
                //Confirm Email
                txtEditConfirmEmail.Text = user.Email;
                //Address
                txtEditAddress.Text = user.Address;
                //City
                txtEditCity.Text = user.City;
                //State
                ddlEditState.SelectedValue = Convert.ToString(user.StateID);
                //Zip
                txtEditZip.Text = user.Zip;
                //Phone
                txtEditPhone.Text = user.Phone;
                //MobilePhone
                txtEditMobilePhone.Text = user.MobilePhone;
                 
                //Role
                ddlEditRole.SelectedValue = Convert.ToString(siteUser.RoleID);

                //Client
                hdnCID.Value = Convert.ToString(siteUser.CID);

                //Active
                chkEditActive.Checked = user.IsActive;

                //ResetPassword
                txtEditNewPassword.Text = "";

                //clear building group listboxes, just in case
                lbEditBuildingGroupsTop.Items.Clear();
                lbEditBuildingGroupsBottom.Items.Clear();

                //clear building listboxes, just in case
                lbEditBuildingsTop.Items.Clear();
                lbEditBuildingsBottom.Items.Clear();

                //Building Group Access depending on role
                if (!siteUser.IsSuperAdminOrFullAdminOrFullUser)
                {
                    //Bind building groups for user
                    IEnumerable<BuildingGroup> bldgGrps = BuildingGroupDataMapper.GetAllBuildingGroupsAssociatedToUID(user.UID);

                    if (bldgGrps.Count() > 0)
                    {
                        //bind buildings associated to user to bottom listbox
                        lbEditBuildingGroupsBottom.DataSource = bldgGrps;
                        lbEditBuildingGroupsBottom.DataTextField = "BuildingGroupName";
                        lbEditBuildingGroupsBottom.DataValueField = "BuildingGroupID";
                        lbEditBuildingGroupsBottom.DataBind();
                    }

                    //get building groups not for user, and are for client
                    IEnumerable<CW.Data.BuildingGroup> bldgGrps2 = BuildingGroupDataMapper.GetAllBuildingGroupsNotAssociatedToUserID(user.UID, siteUser.CID);

                    if (bldgGrps2.Count() > 0)
                    {
                        //bind building groups not associated to user to top listbox                        
                        lbEditBuildingGroupsTop.DataSource = bldgGrps2;
                        lbEditBuildingGroupsTop.DataTextField = "BuildingGroupName";
                        lbEditBuildingGroupsTop.DataValueField = "BuildingGroupID";
                        lbEditBuildingGroupsTop.DataBind();
                    }

                    divEditBuildingGroupsListBoxs.Visible = true;
                }
                else
                {
                    divEditBuildingGroupsListBoxs.Visible = false;
                }

                //Building Access depending on role
                if (!siteUser.IsSuperAdminOrFullAdminOrFullUser)
                {
                    //Bind buildings for user
                    IEnumerable<Building> bldgs = Enumerable.Empty<Building>(); // BuildingDataMapper.GetAllBuildingsAssociatedToUserID(user.UID);

                    if (bldgs.Count() > 0)
                    {
                        //bind buildings associated to user to bottom listbox
                        lbEditBuildingsBottom.DataSource = bldgs;
                        lbEditBuildingsBottom.DataTextField = "BuildingName";
                        lbEditBuildingsBottom.DataValueField = "BID";
                        lbEditBuildingsBottom.DataBind();
                    }

                    //get buildings not for user, and are for client
                    IEnumerable<CW.Data.Building> bldgs2 = null; // BuildingDataMapper.GetAllVisibleBuildingsNotAssociatedToUserID(user.UID, siteUser.CID);

                    if (bldgs2.Count() > 0)
                    {
                        //bind building not associated to user to top listbox                        
                        lbEditBuildingsTop.DataSource = bldgs2;
                        lbEditBuildingsTop.DataTextField = "BuildingName";
                        lbEditBuildingsTop.DataValueField = "BID";
                        lbEditBuildingsTop.DataBind();
                    }

                    divEditBuildingsListBoxs.Visible = true;
                }
                else
                {
                    divEditBuildingsListBoxs.Visible = false;
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindInitialSetEditUserGauges(user.UserID, user.RoleID, user.ClientID, lbEditBuildingsBottom.Items);

                BindInitialSetEditUserQuickLinks(user.UID, siteUser.RoleID, siteUser.CID);                
            }

        #endregion

        #region Load and Bind Fields

            private void BindRoles(DropDownExtension ddl)
            {                          
                ddl.DataTextField = "Role";
                ddl.DataValueField = "RoleID";

                IEnumerable<UserRole> userRoles = Enumerable.Empty<UserRole>();

                //get all role greater than (which means lower roles)
                userRoles = UserRoleDataMapper.GetAllRolesGreaterThan(Convert.ToInt32(Session["UserRoleID"]));

                ddl.DataSource = userRoles;
                ddl.DataBind();

                //populate decription in dropdown items by added a title attribute
                int counter = ddl.Items.Count > userRoles.Count() ? 1 : 0;
                foreach (UserRole ur in userRoles)
                {
                    ddl.Items[counter].Attributes.Add("title", ur.RoleDescription);
                    counter++;
                }
            }

            private void BindStates(DropDownList ddl)
            {
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = StateDataMapper.GetAllStates();
                ddl.DataBind();
            }

            private void BindClients(DropDownList ddl)
            {
                ddl.DataTextField = "ClientName";
                ddl.DataValueField = "CID";
                ddl.DataSource = ClientDataMapper.GetAllClients();
                ddl.DataBind();
            }

        #endregion

        #region Load User

            protected void LoadAddFormIntoUser(User user)
            {
                //TextBox tempTextBox = new TextBox();
                //DropDownList tempDropDownList = new DropDownList();

                //First Name
                //tempTextBox = (TextBox)this.FindControl("txtFirstName");
                user.FirstName = txtAddFirstName.Text;

                //Last Name
                //tempTextBox = (TextBox)this.FindControl("txtLastName");
                user.LastName = txtAddLastName.Text;

                //Email
                //tempTextBox = (TextBox)this.FindControl("txtEmail");
                user.Email = txtAddEmail.Text;

                //Address
                user.Address = txtAddAddress.Text;

                //City
                user.City = txtAddCity.Text;

                //State
                user.StateID = Convert.ToInt32(ddlAddState.SelectedValue);

                //Zip
                user.Zip = txtAddZip.Text;

                //Phone
                user.Phone = !String.IsNullOrEmpty(txtAddPhone.Text) ? txtAddPhone.Text : null;
                          
                //MobilePhone
                user.MobilePhone = !String.IsNullOrEmpty(txtAddMobilePhone.Text) ? txtAddMobilePhone.Text : null;

                //Role
                //tempDropDownList = (DropDownList)this.FindControl("ddlRole");
                //user.RoleID = Convert.ToInt32(ddlAddRole.SelectedValue);

                //Client
                //user.CID = UserDataMapper.GetUserClientID(Convert.ToInt32(Session["UID"].ToString()));               

                //if inserting new user, insert ip address
                //IP Adresss
                //check is user is behind a proxy server. if the user is behind more than one, then select the first, as each server will be appended after. format is csv                
                //string ip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                //if (String.IsNullOrEmpty(ip) || ip.ToLower() == "unknown")
                //{
                //    // no proxy, get the standard remote address
                //    user.IPAddress = Request.UserHostAddress;
                //}
                //else
                //{
                //    user.IPAddress = ip.Split(',').First();
                //} 
                
                //insterting new user password encription
                //tempTextBox = (TextBox)this.FindControl("txtInitialPassword");
                user.Password = Encryptor.getMd5Hash(txtAddInitialPassword.Text);

                user.IsActive = true;
                user.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoUser(User user)
            {
                //ID
                user.UID = Convert.ToInt32(hdnEditID.Value);

                //First Name
                user.FirstName = txtEditFirstName.Text;

                //Last Name
                user.LastName = txtEditLastName.Text;

                //Email
                user.Email = txtEditEmail.Text;
                
                //Address
                user.Address = txtEditAddress.Text;

                //City
                user.City = txtEditCity.Text;

                //State
                user.StateID = Convert.ToInt32(ddlEditState.SelectedValue);

                //Zip
                user.Zip = txtEditZip.Text;

                //Phone
                user.Phone = String.IsNullOrEmpty(txtEditPhone.Text) ? null : txtEditPhone.Text;

                //MobilePhone
                user.MobilePhone = String.IsNullOrEmpty(txtEditMobilePhone.Text) ? null : txtEditMobilePhone.Text;

                //Client
                //not editable

                //Role
                //try
                //{
                //    user.RoleID = Convert.ToInt32(ddlEditRole.SelectedValue);
                //}
                //catch
                //{
                //}

                //New Password
                user.Password = String.IsNullOrEmpty(txtEditNewPassword.Text) ? "" : Encryptor.getMd5Hash(txtEditNewPassword.Text);              

                //Active
                user.IsActive = chkEditActive.Checked;
            }

        #endregion

        #region Button Events
       
            /// <summary>
            /// Add user Button on click.
            /// </summary>
            protected void addUserButton_Click(object sender, EventArgs e)
            {
                //check that user does not already exist
                if (!UserDataMapper.DoesUserExist(txtAddEmail.Text))
                {
                    int newUID = 0;
                    bool userInsertSuccess = true, buildingInsertSuccess = true, buildingGroupInsertSuccess = true, quickLinkInsertSuccess = true, auditInsertSuccess = true;
                    //gaugeInsertSuccess = true, 

                    mUser = new User();

                    //load the form into the user
                    LoadAddFormIntoUser(mUser);

                    //try to insert the new user
                    try
                    {
                        //insert new user
                        newUID = UserDataMapper.InsertAndReturnUID(mUser);   
                    }
                    catch (SqlException sqlEx)
                    {
                        LogHelper.LogCWError("Error adding user in user administration.", sqlEx);
                        userInsertSuccess = false;
                    }
                    catch (Exception ex)
                    {
                        LogHelper.LogCWError("Error adding user in user administration.", ex);
                        userInsertSuccess = false;                         
                    }

                    //assign user building groups if appropriate role and/or buildings exist in listbox---
                    if (userInsertSuccess && lbAddBuildingGroupsBottom.Items.Count > 0)
                    {
                        //declare new users_clients_building group
                        Users_Clients_BuildingGroup mUsersClientsBuildingGroup = new Users_Clients_BuildingGroup();

                        //try to insert the building groups to user
                        try
                        {
                            //inserting each building group in the bottom listbox.
                            //no need to check if they exist already since its the first addition                                
                            foreach (ListItem item in lbAddBuildingGroupsBottom.Items)
                            {
                                mUsersClientsBuildingGroup.BuildingGroupID = Convert.ToInt32(item.Value);
                                //mUsersClientsBuildingGroup.UID = newUID; //RAR: commented

                                //UsersClientsBuildingGroupsDataMapper.InsertUserBuildingGroup(mUsersClientsBuildingGroup);
                            }
                        }
                        catch (SqlException sqlEx)
                        {
                            LogHelper.LogCWError("Error assigning building groups to user in user administration.", sqlEx);
                            buildingGroupInsertSuccess = false;
                        }
                        catch (Exception ex)
                        {
                            LogHelper.LogCWError("Error assigning building groups to user in user administration.", ex);
                            buildingGroupInsertSuccess = false;
                        }
                    }

                    //assign user buildings if appropriate role and/or buildings exist in listbox---
                    if (userInsertSuccess && lbAddBuildingsBottom.Items.Count > 0)
                    {
                        //declare new Users_Clients_Building
                        Users_Clients_Building mUsersClientsBuilding = new Users_Clients_Building();

                        //try to insert the buildings to user
                        try
                        {
                            //inserting each building in the bottom listbox.
                            //no need to check if they exist already since its the first addition                                
                            foreach (ListItem item in lbAddBuildingsBottom.Items)
                            {
                                mUsersClientsBuilding.BID = Convert.ToInt32(item.Value);
                                //mUsersClientsBuilding.UID = newUID; //RAR:commented

                                //RAR Commented out for build
                                //UserDataMapper.InsertUserClientBuilding(mUsersClientsBuilding);
                            }
                        }
                        catch (SqlException sqlEx)
                        {
                            LogHelper.LogCWError("Error assigning buildings to user in user administration.", sqlEx);  
                            buildingInsertSuccess = false;
                        }
                        catch (Exception ex)
                        {
                            LogHelper.LogCWError("Error assigning buildings to user in user administration.", ex);  
                            buildingInsertSuccess = false;
                        }
                    }

                    //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                    ////assign user gauges if appropriate role, client and/or gauges exist in listbox---
                    //if (userInsertSuccess && lbAddGaugesBottom.Items.Count > 0)
                    //{
                    //    //declare new usergauge
                    //    UserGauge mUserGauge = new UserGauge();

                    //    //try to insert the gauges to user
                    //    try
                    //    {
                    //        //inserting each gauge in the bottom listbox.
                    //        //no need to check if they exist already since its the first addition                                
                    //        foreach (ListItem item in lbAddGaugesBottom.Items)
                    //        {
                    //            mUserGauge.PID = Convert.ToInt32(item.Value);
                    //            mUserGauge.UserID = newUserID;

                    //            UserDataMapper.InsertUserGauge(mUserGauge);
                    //        }
                    //    }
                    //    catch (SqlException sqlEx)
                    //    {
                    //        LogHelper.LogCWError("Error assigning gauges to user in user administration.", sqlEx);  
                    //        gaugeInsertSuccess = false;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        LogHelper.LogCWError("Error assigning gauges to user in user administration.", ex);  
                    //        gaugeInsertSuccess = false;
                    //    }
                    //}

                    //assign user quicklinks if appropriate role, client and/or gauges exist in listbox---
                    if (userInsertSuccess && lbAddQuickLinksBottom.Items.Count > 0)
                    {
                        //declare new Users_Clients_QuickLink
                        Users_Clients_QuickLink mUsersClientsQuickLink = new Users_Clients_QuickLink();

                        //try to insert the quicklink to user
                        try
                        {
                            //inserting each quicklink in the bottom listbox.
                            //no need to check if they exist already since its the first addition                                
                            foreach (ListItem item in lbAddQuickLinksBottom.Items)
                            {
                                mUsersClientsQuickLink.QuickLinkID = Convert.ToInt32(item.Value);
                                mUsersClientsQuickLink.UID = newUID;

                                QuickLinksDataMapper.InsertUserClientQuickLink(mUsersClientsQuickLink);
                            }
                        }
                        catch (SqlException sqlEx)
                        {
                            LogHelper.LogCWError("Error assigning quicklinks to user in user administration.", sqlEx);  
                            quickLinkInsertSuccess = false;
                        }
                        catch (Exception ex)
                        {
                            LogHelper.LogCWError("Error assigning quicklins to user in user administration.", ex);  
                            quickLinkInsertSuccess = false;
                        }
                    }

                    //add userAudit table entry
                    if (userInsertSuccess)
                    {
                        //declare new userAudit
                        UsersAudit mUserAudit = new UsersAudit();
                        mUserAudit.UID = newUID;
                        mUserAudit.DateModified = DateTime.UtcNow;

                        //try to insert the audit for user
                        try
                        {
                            UserDataMapper.InsertUserAudit(mUserAudit);
                        }
                        catch (SqlException sqlEx)
                        {
                            LogHelper.LogCWError("Error adding userAudit for user in user administration.", sqlEx);
                            auditInsertSuccess = false;
                        }
                        catch (Exception ex)
                        {
                            LogHelper.LogCWError("Error adding userAudit for user in user administration.", ex);
                            auditInsertSuccess = false;
                        }
                    }

                    //set appropriate message
                    if (userInsertSuccess)
                    {
                        lblAddError.Text = mUser.Email + addUserSuccess;

                        if (!buildingGroupInsertSuccess)
                        {
                            lblAddError.Text += assigningBuildingGroupsFailed;
                        }
                        if (!buildingInsertSuccess)
                        {
                            lblAddError.Text += assigningBuildingsFailed;
                        }
                        //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                        //if (!gaugeInsertSuccess)
                        //{
                        //    lblAddError.Text += assigningGaugesFailed;
                        //}
                        if (!quickLinkInsertSuccess)
                        {
                            lblAddError.Text += assigningQuickLinksFailed;
                        }
                        if (!auditInsertSuccess)
                        {
                            lblAddError.Text += addingAuditForUserFailed;
                        }
                    }
                    else
                    {
                        lblAddError.Text = faliedTransaction;
                    }                    
                    lblAddError.Visible = true;
                    lnkSetFocusAdd.Focus();            
                }
                else
                {
                    //user exists
                    lblAddError.Text = userExists;
                    lblAddError.Visible = true;
                    lnkSetFocusAdd.Focus();     
                }
            }

            /// <summary>
            /// Update user Button on click.
            /// </summary>
            protected void updateUserButton_Click(object sender, EventArgs e)
            {
                bool userEditSuccess = true, buildingGroupEditSuccess = true, buildingEditSuccess = true, quickLinkEditSuccess = true;
                //gaugeEditSuccess = true, 

                //create and load the form into the user
                User mUser = new User();
                LoadEditFormIntoUser(mUser);

                //check if the user email exists for another user if trying to change
                if (UserDataMapper.DoesUserExist(mUser.UID, mUser.Email))
                {
                    lblEditError.Text = userExists;
                }
                //update the user
                else
                {
                    try
                    {
                        //if new password was entered
                        if (!String.IsNullOrEmpty(txtEditNewPassword.Text))
                        {
                            UserDataMapper.UpdateUser(mUser, true, false, true);
                        }
                        else
                        {
                            UserDataMapper.UpdateUser(mUser, false, false, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHelper.LogCWError(String.Format("Error updating user in user administration: UID={0}, UserEmail={1}, UserFirstName={2} UserLastName={3}.", mUser.UID, mUser.Email, mUser.FirstName, mUser.LastName), ex);
                        userEditSuccess = false;
                    }

                    //RAR commented out for build
                    //deletes all user building groups
                    //UserDataMapper.DeleteUsersBuildingGroupsByUID(Convert.ToInt32(mUser.UID));

                    //assign user building groups if appropriate role and/or building groups exist in listbox---
                    if (lbEditBuildingGroupsBottom.Items.Count > 0)
                    {
                        //declare new Users_Clients_BuildingGroup. 
                        //quicker to delete all, and then insert all...
                        //rather then checkif if each exists then insert, and if exist delete
                        Users_Clients_BuildingGroup mUsersClientsBuildingGroup = new Users_Clients_BuildingGroup();

                        try
                        {
                            //inserting each building group in the bottom listbox if they dont exist
                            foreach (ListItem item in lbEditBuildingGroupsBottom.Items)
                            {
                                int buildingGroupID = Convert.ToInt32(item.Value);

                                //if the user building doesnt already exist, insert
                                //if (!UserDataMapper.DoesBuildingExistForUser(bid, mUser.UserID))
                                //{
                                mUsersClientsBuildingGroup.BuildingGroupID = buildingGroupID;
                                //mUsersClientsBuildingGroup.UID = mUser.UID; //RAR: commented
                                //UsersClientsBuildingGroupsDataMapper.InsertUserBuildingGroup(mUsersClientsBuildingGroup);
                                //}
                            }

                            ////deleting each building in the top listbox if they exist
                            //foreach (ListItem item in lbEditBuildingsTop.Items)
                            //{
                            //    //if the building exists, delete
                            //    if (UserDataMapper.DoesBuildingExistForUser(Convert.ToInt32(item.Value), mUser.UserID))
                            //    {
                            //        UserDataMapper.DeleteUserBuildingByBIDAndUserID(Convert.ToInt32(item.Value), mUser.UserID);
                            //    }
                            //}
                        }
                        catch (SqlException sqlEx)
                        {
                            LogHelper.LogCWError("Error reassigning building groups to user in user administration.", sqlEx);
                            buildingGroupEditSuccess = false;
                        }
                        catch (Exception ex)
                        {
                            LogHelper.LogCWError("Error reassigning building groups to user in user administration.", ex);
                            buildingGroupEditSuccess = false;
                        }
                    }

                    //deletes all user buildings
                    //UsersClientsBuildingsDataMapper.DeleteUsersClientsBuildingsByUID(Convert.ToInt32(mUser.UID));

                    //assign user buildings if appropriate role and/or buildings exist in listbox---
                    if (lbEditBuildingsBottom.Items.Count > 0)
                    {
                        //declare new Users_Clients_Building. 
                        //quicker to delete all, and then insert all...
                        //rather then checkif if each exists then insert, and if exist delete
                        Users_Clients_Building mUsersClientsBuilding = new Users_Clients_Building();     

                        try
                        {
                            //inserting each building in the bottom listbox if they dont exist
                            foreach (ListItem item in lbEditBuildingsBottom.Items)
                            {
                                int bid = Convert.ToInt32(item.Value);

                                //if the user building doesnt already exist, insert
                                //if (!UserDataMapper.DoesBuildingExistForUser(bid, mUser.UserID))
                                //{
                                    mUsersClientsBuilding.BID = bid;
                                    //mUsersClientsBuilding.UID = mUser.UID; //RAR: commented

                                //RAR Commented out for build
                                //UserDataMapper.InsertUserClientBuilding(mUsersClientsBuilding);
                                //}
                            }

                            ////deleting each building in the top listbox if they exist
                            //foreach (ListItem item in lbEditBuildingsTop.Items)
                            //{
                            //    //if the building exists, delete
                            //    if (UserDataMapper.DoesBuildingExistForUser(Convert.ToInt32(item.Value), mUser.UserID))
                            //    {
                            //        UserDataMapper.DeleteUserBuildingByBIDAndUserID(Convert.ToInt32(item.Value), mUser.UserID);
                            //    }
                            //}
                        }
                        catch (SqlException sqlEx)
                        {
                            LogHelper.LogCWError("Error reassigning buildings to user in user administration.", sqlEx);
                            buildingEditSuccess = false;
                        }
                        catch (Exception ex)
                        {
                            LogHelper.LogCWError("Error reassigning buildings to user in user administration.", ex);
                            buildingEditSuccess = false;
                        }
                    }

                    //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                    ////deletes all user gauges
                    //UserDataMapper.DeleteAllUserGaugesByUserID(Convert.ToInt32(mUser.UserID));

                    ////assign user gagues if appropriate role and/or buildings exist in listbox---
                    //if (lbEditGaugesBottom.Items.Count > 0)
                    //{
                    //    //declare new userGauge
                    //    UserGauge mUserGauge = new UserGauge();

                    //    try
                    //    {
                    //        //inserting each usergauge in the bottom listbox if they dont exist
                    //        foreach (ListItem item in lbEditGaugesBottom.Items)
                    //        {
                    //            int pid = Convert.ToInt32(item.Value);

                    //            //if the user gauge doesnt already exist, insert
                    //            //if (!UserDataMapper.DoesGaugeExistForUser(pid, mUser.UserID))
                    //            //{
                    //            mUserGauge.PID = pid;
                    //            mUserGauge.UserID = mUser.UserID;
                    //            UserDataMapper.InsertUserGauge(mUserGauge);
                    //            //}
                    //        }

                    //        ////deleting each gauge in the top listbox if they exist
                    //        //foreach (ListItem item in lbEditGaugesTop.Items)
                    //        //{
                    //        //    //if the gauge exists, delete
                    //        //    if (UserDataMapper.DoesGaugeExistForUser(Convert.ToInt32(item.Value), mUser.UserID))
                    //        //    {
                    //        //        UserDataMapper.DeleteUserGaugeByPIDAndUserID(Convert.ToInt32(item.Value), mUser.UserID);
                    //        //    }
                    //        //}
                    //    }
                    //    catch (SqlException sqlEx)
                    //    {
                    //        LogHelper.LogCWError("Error reassigning gauges to user in kgs user administration.", sqlEx);
                    //        gaugeEditSuccess = false;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        LogHelper.LogCWError("Error reassigning gauges to user in kgs user administration.", ex);
                    //        gaugeEditSuccess = false;
                    //    }
                    //}


                    //RAR: Commmented out for build
                    //TODO: quicklinks are client and user based, so we need a client dropdown before we can edit quicklinks for a user
                    ////deletes all user quicklinks
                    //QuickLinksDataMapper.DeleteAllUserQuickLinksByCIDAndUID(Convert.ToInt32(mUser.UID));

                    ////assign user quicklinks if appropriate role and/or client exists in listbox---
                    //if (lbEditQuickLinksBottom.Items.Count > 0)
                    //{
                    //    //declare new Users_Clients_QuickLink
                    //    Users_Clients_QuickLink mUserClientQuickLink = new Users_Clients_QuickLink();

                    //    try
                    //    {
                    //        //inserting each UserQuickLink in the bottom listbox if they dont exist
                    //        foreach (ListItem item in lbEditQuickLinksBottom.Items)
                    //        {
                    //            int quickLinkID = Convert.ToInt32(item.Value);

                    //            //if the user gauge doesnt already exist, insert
                    //            //if (!UserDataMapper.DoesGaugeExistForUser(pid, mUser.UserID))
                    //            //{
                    //            mUserClientQuickLink.QuickLinkID = quickLinkID;
                    //            mUserClientQuickLink.UID = mUser.UID;
                    //            QuickLinksDataMapper.InsertUserQuickLink(mUserClientQuickLink);
                    //            //}
                    //        }

                    //        ////deleting each quicklink in the top listbox if they exist
                    //        //foreach (ListItem item in lbEditQuickLinksTop.Items)
                    //        //{
                    //        //    //if the quicklink exists, delete
                    //        //    if (QuickLinksDataMapper.DoesQuickLinkExistForUser(Convert.ToInt32(item.Value), mUser.UserID))
                    //        //    {
                    //        //        QuickLinksDataMapper.DeleteUserQuickLinkByQuickLinkIDAndUserID(Convert.ToInt32(item.Value), mUser.UserID);
                    //        //    }
                    //        //}
                    //    }
                    //    catch (SqlException sqlEx)
                    //    {
                    //        LogHelper.LogCWError("Error reassigning quicklinks to user in user administration.", sqlEx);
                    //        quickLinkEditSuccess = false;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        LogHelper.LogCWError("Error reassigning quicklinks to user in user administration.", ex);
                    //        quickLinkEditSuccess = false;
                    //    }
                    //}


                        //set appropriate message
                        if (userEditSuccess)
                        {
                            lblEditError.Text = mUser.Email + updateSuccessful;

                            if (!buildingGroupEditSuccess)
                            {
                                lblEditError.Text += reassigningBuildingGroupsFailed;
                            }
                            if (!buildingEditSuccess)
                            {
                                lblEditError.Text += reassigningBuildingsFailed;
                            }
                            //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                            //if (!gaugeEditSuccess)
                            //{
                            //    lblEditError.Text += reassigningGaugesFailed;
                            //}
                            if (!quickLinkEditSuccess)
                            {
                                lblEditError.Text += reassigningQuickLinksFailed;
                            }
                        }
                        else
                        {
                            lblEditError.Text = updateFailed;
                        }
                        lblEditError.Visible = true;
                        lnkSetFocusView.Focus();     

                        
                        //Bind users again
                        BindUsers();
                }
            }

            /// <summary>
            /// on add BuildingGroups button up click, add buildings 
            /// </summary>
            protected void btnAddBuildingGroupsUpButton_Click(object sender, EventArgs e)
            {
                while (lbAddBuildingGroupsBottom.SelectedIndex != -1)
                {
                    lbAddBuildingGroupsTop.Items.Add(lbAddBuildingGroupsBottom.SelectedItem);
                    lbAddBuildingGroupsBottom.Items.Remove(lbAddBuildingGroupsBottom.SelectedItem);
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(true, false, false, false, true);
            }

            /// <summary>
            /// on add BuildingGroups button down click, add building groups 
            /// </summary>
            protected void btnAddBuildingGroupsDownButton_Click(object sender, EventArgs e)
            {
                while (lbAddBuildingGroupsTop.SelectedIndex != -1)
                {
                    {
                        lbAddBuildingGroupsBottom.Items.Add(lbAddBuildingGroupsTop.SelectedItem);
                        lbAddBuildingGroupsTop.Items.Remove(lbAddBuildingGroupsTop.SelectedItem);
                    }
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(true, false, false, false, true);
            }

            /// <summary>
            /// on edit BuildingGroups button up click, add building groups
            /// </summary>
            protected void btnEditBuildingGroupsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditBuildingGroupsBottom.SelectedIndex != -1)
                {
                    lbEditBuildingGroupsTop.Items.Add(lbEditBuildingGroupsBottom.SelectedItem);
                    lbEditBuildingGroupsBottom.Items.Remove(lbEditBuildingGroupsBottom.SelectedItem);
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(false, true, false, false, true);
            }

            /// <summary>
            /// on edit BuildingGroups button down click, add building groups
            /// </summary>
            protected void btnEditBuildingGroupsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditBuildingGroupsTop.SelectedIndex != -1)
                {
                    lbEditBuildingGroupsBottom.Items.Add(lbEditBuildingGroupsTop.SelectedItem);
                    lbEditBuildingGroupsTop.Items.Remove(lbEditBuildingGroupsTop.SelectedItem);
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(false, true, false, false, true);
            }

            /// <summary>
            /// on add Buildings button up click, add buildings 
            /// </summary>
            protected void btnAddBuildingsUpButton_Click(object sender, EventArgs e)
            {
                while (lbAddBuildingsBottom.SelectedIndex != -1)
                {
                    lbAddBuildingsTop.Items.Add(lbAddBuildingsBottom.SelectedItem);
                    lbAddBuildingsBottom.Items.Remove(lbAddBuildingsBottom.SelectedItem);
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(true, false, false, true);
            }

            /// <summary>
            /// on add Buildings button down click, add buildings 
            /// </summary>
            protected void btnAddBuildingsDownButton_Click(object sender, EventArgs e)
            {
                while (lbAddBuildingsTop.SelectedIndex != -1)
                {
                    {
                        lbAddBuildingsBottom.Items.Add(lbAddBuildingsTop.SelectedItem);
                        lbAddBuildingsTop.Items.Remove(lbAddBuildingsTop.SelectedItem);
                    }
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(true, false, false, true);
            }

            /// <summary>
            /// on edit Buildings button up click, add buildings 
            /// </summary>
            protected void btnEditBuildingsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditBuildingsBottom.SelectedIndex != -1)
                {
                    lbEditBuildingsTop.Items.Add(lbEditBuildingsBottom.SelectedItem);
                    lbEditBuildingsBottom.Items.Remove(lbEditBuildingsBottom.SelectedItem);
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(false, true, false, true);
            }

            /// <summary>
            /// on edit Buildings button down click, add buildings 
            /// </summary>
            protected void btnEditBuildingsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditBuildingsTop.SelectedIndex != -1)
                {
                    lbEditBuildingsBottom.Items.Add(lbEditBuildingsTop.SelectedItem);
                    lbEditBuildingsTop.Items.Remove(lbEditBuildingsTop.SelectedItem);
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(false, true, false, true);
            }

            /// <summary>
            /// on add Gauges button up click, add gauges
            /// </summary>
            protected void btnAddGaugesUpButton_Click(object sender, EventArgs e)
            {
                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //while (lbAddGaugesBottom.SelectedIndex != -1)
                //{
                //    lbAddGaugesTop.Items.Add(lbAddGaugesBottom.SelectedItem);
                //    lbAddGaugesBottom.Items.Remove(lbAddGaugesBottom.SelectedItem);
                //}                
            }

            /// <summary>
            /// on add Gauges button down click, add gauges 
            /// </summary>
            protected void btnAddGaugesDownButton_Click(object sender, EventArgs e)
            {
                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //while (lbAddGaugesTop.SelectedIndex != -1)
                //{
                //    {
                //        lbAddGaugesBottom.Items.Add(lbAddGaugesTop.SelectedItem);
                //        lbAddGaugesTop.Items.Remove(lbAddGaugesTop.SelectedItem);
                //    }
                //}
            }

            /// <summary>
            /// on edit Gauges button up click, add gauges
            /// </summary>
            protected void btnEditGaugesUpButton_Click(object sender, EventArgs e)
            {
                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //while (lbEditGaugesBottom.SelectedIndex != -1)
                //{
                //    lbEditGaugesTop.Items.Add(lbEditGaugesBottom.SelectedItem);
                //    lbEditGaugesBottom.Items.Remove(lbEditGaugesBottom.SelectedItem);
                //}
            }

            /// <summary>
            /// on edit Gauges button down click, add gauges
            /// </summary>
            protected void btnEditGaugesDownButton_Click(object sender, EventArgs e)
            {
                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //while (lbEditGaugesTop.SelectedIndex != -1)
                //{
                //    lbEditGaugesBottom.Items.Add(lbEditGaugesTop.SelectedItem);
                //    lbEditGaugesTop.Items.Remove(lbEditGaugesTop.SelectedItem);
                //}
            }

            /// <summary>
            /// on add QuickLinks button up click, add QuickLinks
            /// </summary>
            protected void btnAddQuickLinksUpButton_Click(object sender, EventArgs e)
            {
                while (lbAddQuickLinksBottom.SelectedIndex != -1)
                {
                    lbAddQuickLinksTop.Items.Add(lbAddQuickLinksBottom.SelectedItem);
                    lbAddQuickLinksBottom.Items.Remove(lbAddQuickLinksBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on add QuickLinks button down click, add QuickLinks
            /// </summary>
            protected void btnAddQuickLinksDownButton_Click(object sender, EventArgs e)
            {
                while (lbAddQuickLinksTop.SelectedIndex != -1)
                {
                    {
                        lbAddQuickLinksBottom.Items.Add(lbAddQuickLinksTop.SelectedItem);
                        lbAddQuickLinksTop.Items.Remove(lbAddQuickLinksTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on edit QuickLinks button up click, add QuickLinks
            /// </summary>
            protected void btnEditQuickLinksUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditQuickLinksBottom.SelectedIndex != -1)
                {
                    lbEditQuickLinksTop.Items.Add(lbEditQuickLinksBottom.SelectedItem);
                    lbEditQuickLinksBottom.Items.Remove(lbEditQuickLinksBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit QuickLinks button down click, add QuickLinks
            /// </summary>
            protected void btnEditQuickLinksDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditQuickLinksTop.SelectedIndex != -1)
                {
                    lbEditQuickLinksBottom.Items.Add(lbEditQuickLinksTop.SelectedItem);
                    lbEditQuickLinksTop.Items.Remove(lbEditQuickLinksTop.SelectedItem);
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                Session["Search"] = txtSearch.Text;
                string[] searchText = txtSearch.Text.Split(' ');
                BindSearchedUsers(searchText);
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                Session["Search"] = "";
                BindUsers();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// On Init for Tabs
            /// </summary>
            protected void loadTabs(object sender, EventArgs e)
            {
                //BindUsers();

                ////disable add buttons only, for enter key search submit action. edit buttons already disable within view tabs panel
                //btnAddBuildingsUp.Enabled = false;
                //btnAddBuildingsDown.Enabled = false;
                ////TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                ////btnAddGaugesUp.Enabled = false;
                ////btnAddGaugesDown.Enabled = false;
                //btnAddQuickLinksUp.Enabled = false;
                //btnAddQuickLinksDown.Enabled = false;         
            }

            /// <summary>
            /// Tab changed event, bind users grid after tab changed back from add user tab
            /// </summary>
            protected void onActiveTabChanged(object sender, EventArgs e)
            {
                if (TabContainer1.ActiveTabIndex == 0)
                {
                    BindUsers();

                    ////disable add buttons only, for enter key search submit action. edit buttons already disable within view tabs panel
                    //btnAddBuildingsUp.Enabled = false;
                    //btnAddBuildingsDown.Enabled = false;
                    ////TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                    ////btnAddGaugesUp.Enabled = false;
                    ////btnAddGaugesDown.Enabled = false;
                    //btnAddQuickLinksUp.Enabled = false;
                    //btnAddQuickLinksDown.Enabled = false;
                }
                //else
                //{
                //    btnAddBuildingsUp.Enabled = true;
                //    btnAddBuildingsDown.Enabled = true;
                //    //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //    //btnAddGaugesUp.Enabled = true;
                //    //btnAddGaugesDown.Enabled = true;
                //    btnAddQuickLinksUp.Enabled = true;
                //    btnAddQuickLinksDown.Enabled = true;
                //}
                //else if (TabContainer1.ActiveTabIndex == 1)
                //{
                //    //hide client add if not kgsadmin or higher, and reset selected value
                //    if (isKGSAdminOrHigher)
                //    {
                //        divAddClient.Visible = true;
                //        ddlAddClient.SelectedValue = "-1";
                //    }
                //    else
                //    {
                //        ddlAddClient.SelectedValue = Session["CID"].ToString();
                //    }
                //}
            }

        #endregion

        #region Grid Events

            protected void gridUsers_OnDataBound(object sender, EventArgs e)
            {
                GridViewRowCollection rows = gridUsers.Rows;

                //check if hidden role id is equal to current users role id. 
                //if so then disable the edit button. 
                //users with same role id can only view others, not edit.
                //you cannot edit youself because you cant change your role, or deactivate yourself.
                //goto myaccount for self edit.
                foreach (GridViewRow row in rows)
                {
                    if ((row.RowState & DataControlRowState.Edit) == 0)
                    {
                        try
                        {
                            if (((HiddenField)row.Cells[5].Controls[1]).Value == Session["UserRoleID"].ToString())
                            {
                                //edit link
                                ((LinkButton)row.Cells[1].Controls[1]).Visible = false;
                                //delete link
                                ((LinkButton)row.Cells[7].Controls[1]).Visible = false;
                            }
                        }
                        catch
                        {
                        }
                    }
                }

            }

            protected void gridUsers_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditUsers.Visible = false;
                dtvUser.Visible = true;
                IOrderedDictionary dka = gridUsers.DataKeys[gridUsers.SelectedIndex].Values;

                int userID = Convert.ToInt32(dka["UID"]);

                //set data source
                dtvUser.DataSource = null; // UserDataMapper.GetFullUser(userID, 1); //RAR: 1 hardcoded
                //bind user to details view
                dtvUser.DataBind();
            }

            protected void gridUsers_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            //protected void gridUsers_Canceling(object sender, GridViewCancelEditEventArgs e)
            //{
            //    //Reset the edit index.
            //    gridUsers.EditIndex = -1;

            //    //Bind data again keeping current page and sort mode
            //    DataTable dataTable = ConvertIEnumerableToDataTable(QueryUsers());
            //    gridUsers.PageIndex = gridUsers.PageIndex;
            //    gridUsers.DataSource = SortDataTable(dataTable as DataTable, true);
            //    gridUsers.DataBind();   
            //}

            protected void gridUsers_Editing(object sender, GridViewEditEventArgs e)
            {
                //gridUsers.EditIndex = e.NewEditIndex;

                ////get selected dropdown value to set selected value after bound again
                //try
                //{
                //    //selectedValue = gridUsers.Rows[e.NewEditIndex].Cells[5].Text;
                //    //selectedValue = ((DataBoundLiteralControl)gridUsers.Rows[e.NewEditIndex].Cells[4].Controls[0]).Text;
                //    selectedValue = ((HiddenField)gridUsers.Rows[e.NewEditIndex].Cells[5].Controls[1]).Value;
                //}
                //catch
                //{
                //}

                ////Bind data again keeping current page and sort mode
                //DataTable dataTable = ConvertIEnumerableToDataTable(QueryUsers());
                //gridUsers.PageIndex = gridUsers.PageIndex;
                //gridUsers.DataSource = SortDataTable(dataTable as DataTable, true);
                //gridUsers.DataBind();     

                dtvUser.Visible = false;
                pnlEditUsers.Visible = true;

                IOrderedDictionary dka = gridUsers.DataKeys[e.NewEditIndex].Values;

                int uid = Convert.ToInt32(dka["UID"]);

                User mUser = UserDataMapper.GetUser(uid);

                SetUserIntoEditForm(mUser);

                //Cancels the edit auto grid selected. Fixes the select link diasspearing in selected mode, and the edit link performing the delete method.
                e.Cancel = true;
            }

            //protected void gridUsers_Updating(object sender, GridViewUpdateEventArgs e)
            //{
            //    bool update = false;

            //    //Retrieve the table from the session object.
            //    //DataTable dt = (DataTable)Session["SomeTable"];
            //    //dt.Rows[row.DataItemIndex]["Id"] = ((TextBox)(row.Cells[1].Controls[0])).Text;
            //    //dt.Rows[row.DataItemIndex]["Description"] = ((TextBox)(row.Cells[2].Controls[0])).Text;
            //    //dt.Rows[row.DataItemIndex]["IsComplete"] = ((CheckBox)(row.Cells[3].Controls[0])).Checked;               
                
            //    //get updating row
            //    GridViewRow row = gridUsers.Rows[gridUsers.EditIndex];
                
            //    //get data
            //    int userID = Convert.ToInt32(gridUsers.DataKeys[e.RowIndex].Value);
            //    string email = ((TextBox)row.Cells[2].Controls[1]).Text; 
            //    string firstName = ((TextBox)row.Cells[3].Controls[1]).Text;
            //    string lastName = ((TextBox)row.Cells[4].Controls[1]).Text;
            //    int roleID = Convert.ToInt32(((DropDownList)row.Cells[5].Controls[1]).SelectedValue);
            //    roleID = Convert.ToInt32(((DropDownList)row.FindControl("ddlEditRole")).SelectedValue);                
            //    bool active = ((CheckBox)row.Cells[6].Controls[1]).Checked;

            //    //get user by id
            //    User mUser = UserDataMapper.GetUserByID(userID, false);

            //    //if the users email is the same then update
            //    if (mUser.Email == email)
            //    {
            //        update = true;
            //    }
            //    //else if the user is trying to change an email check if it already exists
            //    else
            //    {
            //        if (UserDataMapper.DoesUserExist(email))
            //        {
            //            lblErrors.Text = userExists;
            //        }
            //        else
            //        {
            //            update = true;
            //        }
                    
            //    }


            //    //load the form into the user
            //    //LoadFormIntoUser(mUser, true);

            //    //try to update the user
            //    if (update)
            //    {
            //        try
            //        {
            //            UserDataMapper.UpdatePartialUser(userID, email, firstName, lastName, roleID, active);

            //            lblErrors.Text = updateSuccessful;
            //            lblErrors.Visible = true;
            //        }
            //        catch (Exception ex)
            //        {
            //            lblErrors.Text = updateFailed;
            //            lblErrors.Visible = true;

            //            //LogHelper.LogCWError("Unable to update user: UserId=$id UserEmail=$email UserFirstName=$firstname UserLastName=$lastname", mUser, ex);
            //        }
            //    }

            //    //Reset the edit index.
            //    gridUsers.EditIndex = -1;

            //    //Bind data again keeping current page and sort mode
            //    DataTable dataTable = ConvertIEnumerableToDataTable(QueryUsers());
            //    gridUsers.PageIndex = gridUsers.PageIndex;
            //    gridUsers.DataSource = SortDataTable(dataTable as DataTable, true);
            //    gridUsers.DataBind();   
            //}

            protected void gridUsers_Deleting(object sender, GridViewDeleteEventArgs e)
            {                
                //get deleting rows userid
                int userID = Convert.ToInt32(gridUsers.DataKeys[e.RowIndex].Value);

                try
                {
                    //TODO: Maybe... in future just delete building groups and clients assigned to the user with out any checks?

                    //RAR commented out for build
                    //check if users are assigned to a building group regardless of building groups isActive state, and users role
                    bool hasBuildingGroups = false; // UserDataMapper.IsUserAssignedToBuildingGroups(userID);

                    if (hasBuildingGroups)
                    {
                        lblErrors.Text = buildingGroupsExist;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //TODO: Maybe... in future just delete buildings and clients assigned to the user with out any checks?
                        //check if users are assigned to a building regardless of buildings isActive state, and users role
                        //RAR Commented out for build
                        bool hasBuildings = false;//UserDataMapper.IsUserAssignedToBuildings(userID);

                        if (hasBuildings)
                        {
                            lblErrors.Text = buildingsExist;
                            lblErrors.Visible = true;
                            lnkSetFocusView.Focus();
                        }
                        else
                        {
                            //deletes all user quicklinks regardless of client
                            QuickLinksDataMapper.DeleteAllUserClientQuickLinksByUID(userID);

                            //deletes user audit
                            UserDataMapper.DeleteUserAudit(userID);

                            //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                            ////deletes all user gauges
                            //UserDataMapper.DeleteAllUserGaugesByUserID(userID);

                            //delete user
                            UserDataMapper.DeleteUser(userID);

                            lblErrors.Text = deleteSuccessful;
                            lblErrors.Visible = true;
                            lnkSetFocusView.Focus();

                            pnlEditUsers.Visible = false;
                            dtvUser.Visible = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();     

                    LogHelper.LogCWError(String.Format("Error deleting user in user administration: UserId={0}, UserEmail={1}, UserFirstName={2} UserLastName={3}.", mUser.UID, mUser.Email, mUser.FirstName, mUser.LastName), ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = ConvertIEnumerableToDataTable(QueryUsers());
                gridUsers.PageIndex = gridUsers.PageIndex;
                gridUsers.DataSource = SortDataTable(dataTable as DataTable, true);
                gridUsers.DataBind();

                SetGridCountLabel(dataTable.ExtendedProperties.Count);

                //hide edit form if shown
                pnlEditUsers.Visible = false;
            }

            protected void gridUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = ConvertIEnumerableToDataTable(QuerySearchedUsers(Session["Search"].ToString().Split(' ')));

                //maintain current sort direction and expresstion on paging
                gridUsers.DataSource = SortDataTable(dataTable as DataTable, true);
                gridUsers.PageIndex = e.NewPageIndex;
                gridUsers.DataBind();
            }

            protected void gridUsers_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridUsers.EditIndex = -1;

                DataTable dataTable = ConvertIEnumerableToDataTable(QuerySearchedUsers(Session["Search"].ToString().Split(' ')));

                GridViewSortExpression = e.SortExpression;

                gridUsers.DataSource = SortDataTable(dataTable as DataTable, false);
                gridUsers.DataBind();
            }
        
        #endregion

        #region Dropdown and Checkbox events

            /// <summary>
            /// On initialize of the edit role dropdown, bind all roles greater then current users.
            /// Do not reload on autopostback.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            //protected void ddlEdit_OnInit(object sender, EventArgs e)
            //{
            //    DropDownList ddl = (DropDownList)sender;              

            //    ddl.DataTextField = "Role";
            //    ddl.DataValueField = "RoleID";                
            //    ddl.DataSource = UserRoleDataMapper.GetAllRolesGreaterThan(Convert.ToInt32(Session["UserRoleID"]));
            //    ddl.DataBind();
                
            //    ddl.SelectedValue = selectedValue;
            //}

            protected void ddlEditRole_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                //clear listboxes
                lbEditBuildingGroupsTop.Items.Clear();
                lbEditBuildingGroupsBottom.Items.Clear();

                //show building groups if not a full user, or full admin or higher 
                //and clear all building groups from user otherwise
                if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(ddlEditRole.SelectedValue))
                {
                    divEditBuildingGroupsListBoxs.Visible = false;
                }
                else
                {
                    //rebind listboxes again to listboxs on edit user page
                    //get building groups not associated to userid but available
                    IEnumerable<CW.Data.BuildingGroup> bldgGrps = BuildingGroupDataMapper.GetAllBuildingGroupsNotAssociatedToUserID(Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value));

                    //bind building group to top listbox
                    lbEditBuildingGroupsTop.DataSource = bldgGrps;
                    lbEditBuildingGroupsTop.DataTextField = "BuildingGroupName";
                    lbEditBuildingGroupsTop.DataValueField = "BuildingGroupID";
                    lbEditBuildingGroupsTop.DataBind();

                    //get all building groups by client id
                    IEnumerable<CW.Data.BuildingGroup> bldgGrps2 = BuildingGroupDataMapper.GetAllBuildingGroupsByCID(Convert.ToInt32(hdnCID.Value));

                    //get all building groups already assigned and avaialable by client id
                    IEnumerable<CW.Data.BuildingGroup> bldgGrps3 = BuildingGroupDataMapper.GetAllUserBuildingGroupsAssignedToUserAndAvailable(bldgGrps2, (Convert.ToInt32(hdnEditID.Value)));

                    //bind building group to bottom listbox
                    lbEditBuildingGroupsBottom.DataSource = bldgGrps3;
                    lbEditBuildingGroupsBottom.DataTextField = "BuildingGroupName";
                    lbEditBuildingGroupsBottom.DataValueField = "BuildingGroupID";
                    lbEditBuildingGroupsBottom.DataBind();

                    divEditBuildingGroupsListBoxs.Visible = true;
                }

                //clear listboxes
                lbEditBuildingsTop.Items.Clear();
                lbEditBuildingsBottom.Items.Clear();

                //show buildings if not a full user, or full admin or higher 
                //and clear all buildings from user otherwise
                if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(ddlEditRole.SelectedValue))
                {
                    divEditBuildingsListBoxs.Visible = false;
                }
                else
                {
                    //rebind listboxes again to listboxs on edit user page
                    //get buildings not associated to userid but available
                    //RAR commented for build
                    IEnumerable<CW.Data.Building> bldgs = null; //BuildingDataMapper.GetAllBuildingsNotAssociatedToUserID(Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value));

                    //bind building to top listbox
                    lbEditBuildingsTop.DataSource = bldgs;
                    lbEditBuildingsTop.DataTextField = "BuildingName";
                    lbEditBuildingsTop.DataValueField = "BID";
                    lbEditBuildingsTop.DataBind();

                    //get all buildings by client id
                    IEnumerable<CW.Data.Building> bldgs2 = BuildingDataMapper.GetAllBuildingsByClientID(Convert.ToInt32(hdnCID.Value));

                    //get all buildings already assigned and avaialable by client id
                    //RAR commented for build
                    IEnumerable<CW.Data.Building> bldgs3 = null; //BuildingDataMapper.GetAllUserBuildingsAssignedToUserAndAvailable(bldgs2, (Convert.ToInt32(hdnEditID.Value)));

                    //bind building to bottom listbox
                    lbEditBuildingsBottom.DataSource = bldgs3;
                    lbEditBuildingsBottom.DataTextField = "BuildingName";
                    lbEditBuildingsBottom.DataValueField = "BID";
                    lbEditBuildingsBottom.DataBind();

                    divEditBuildingsListBoxs.Visible = true;
                }


                //if the edit client dropdown was already selected run as if selected index changed                
                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(false, true, true, false);
                BindQuickLinks(false, true, true, false);                
            }

            protected void ddlAddRole_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                //clear listboxes
                lbAddBuildingGroupsTop.Items.Clear();
                lbAddBuildingGroupsBottom.Items.Clear();

                //show building groups if not a full user, or full admin or higher 
                if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(ddlAddRole.SelectedValue))
                {
                    divAddBuildingGroupsListBoxs.Visible = false;
                }
                else
                {
                    divAddBuildingGroupsListBoxs.Visible = true;

                    //bind building groups to listbox on add user page
                    //get building groups by client id
                    IEnumerable<CW.Data.BuildingGroup> bldgGrps = BuildingGroupDataMapper.GetAllBuildingGroupsByCID(Convert.ToInt32(Session["CID"].ToString()));

                    //bind building groups to top listbox
                    lbAddBuildingGroupsTop.DataSource = bldgGrps;
                    lbAddBuildingGroupsTop.DataTextField = "BuildingGroupName";
                    lbAddBuildingGroupsTop.DataValueField = "BuildingGroupID";
                    lbAddBuildingGroupsTop.DataBind();
                }

                //clear listboxes
                lbAddBuildingsTop.Items.Clear();
                lbAddBuildingsBottom.Items.Clear();

                //show buildings if not a full user, or full admin or higher 
                if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(ddlAddRole.SelectedValue))
                {
                    divAddBuildingsListBoxs.Visible = false;
                }
                else
                {
                    divAddBuildingsListBoxs.Visible = true;

                    //bind buildings to listbox on add user page
                    //get buildings by client id
                    IEnumerable<CW.Data.Building> bldgs = BuildingDataMapper.GetAllBuildingsByClientID(Convert.ToInt32(Session["CID"].ToString()));

                    //bind buildings to top listbox
                    lbAddBuildingsTop.DataSource = bldgs;
                    lbAddBuildingsTop.DataTextField = "BuildingName";
                    lbAddBuildingsTop.DataValueField = "BID";
                    lbAddBuildingsTop.DataBind();
                }

                //TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard
                //BindGauges(true, false, false, false);
                BindQuickLinks(true, false, true, false);
            }

        #endregion

        #region Bind Gauges

            /// <summary>
            /// Binds the initial set of user ganges when loaded for the first time into the form in edit mode.
            /// </summary>
            /// <param name="initialSetEditUserID"></param>
            /// <param name="initialSetEditRoleID"></param>
            /// <param name="initialSetEditClientID"></param>
            /// <param name="initialSetEditBuilldings"></param>
            private void BindInitialSetEditUserGauges(int initialSetEditUserID, int initialSetEditRoleID, int? initialSetEditClientID, ListItemCollection initialSetEditBuilldings)
            {
                //clear gauge listboxes, just in case
                lbEditGaugesTop.Items.Clear();
                lbEditGaugesBottom.Items.Clear();

                IEnumerable<UserDataMapper.GetUserGaugeData> availableUserGauges = Enumerable.Empty<UserDataMapper.GetUserGaugeData>();

                //bind the gauges based on current role
                if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(initialSetEditRoleID.ToString()))
                {
                    availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(true, null, initialSetEditUserID, initialSetEditClientID, false);
                }
                else
                {
                    availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(false, initialSetEditBuilldings, initialSetEditUserID, initialSetEditClientID, false);
                }

                //bind available user guages
                BindAvailableUserGaugeLoop(availableUserGauges, false, true);

                //get and bind assigned user gauges.
                //this is only if in user edit mode.
                BindAssignedUserGaugeLoop(UserDataMapper.GetAllUserGaugesAssignedToUser(initialSetEditUserID));
            }

            /// <summary>
            /// Binds user guages based on add/edit mode, role changed, buildings chaned
            /// </summary>
            /// <param name="isAddUser"></param>
            /// <param name="isEditUser"></param>
            /// <param name="isRoleChanged"></param>
            /// <param name="isClientChanged"></param>
            /// <param name="isBuildingChanged"></param>
            private void BindGauges(bool isAddUser, bool isEditUser, bool isRoleChanged, bool isBuildingChanged)
            {
                IEnumerable<UserDataMapper.GetUserGaugeData> availableUserGauges = Enumerable.Empty<UserDataMapper.GetUserGaugeData>();
                IEnumerable<UserDataMapper.GetUserGaugeData> availableUserGaugesInlcudingAssociated = Enumerable.Empty<UserDataMapper.GetUserGaugeData>();
                
                if (isAddUser)
                {
                    //clear listboxes
                    lbAddGaugesTop.Items.Clear();
                    lbAddGaugesBottom.Items.Clear();

                    if (isRoleChanged)
                    {
                        //rebind the gauges based on role selection                                                
                        if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(ddlAddRole.SelectedValue))
                        {
                            availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(true, null, null, Convert.ToInt32(Session["CID"].ToString()), false);
                        }
                        else
                        {
                            availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(false, lbAddBuildingsBottom.Items, null, Convert.ToInt32(Session["CID"].ToString()), false);
                        }
                    }
                    else if (isBuildingChanged)
                    {
                        //rebind the gauges based on role selection                        
                        if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(ddlAddRole.SelectedValue))
                        {
                            availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(true, null, null, Convert.ToInt32(Session["CID"].ToString()), false);
                        }
                        else
                        {
                            availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(false, lbAddBuildingsBottom.Items, null, Convert.ToInt32(Session["CID"].ToString()), false);
                        }
                    }                   
                }
                else if (isEditUser)
                {
                    //clear listboxes
                    lbEditGaugesTop.Items.Clear();
                    lbEditGaugesBottom.Items.Clear();

                    if (isRoleChanged)
                    {
                        //rebind the gauges based on role selection
                        if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(ddlEditRole.SelectedValue))
                        {
                            if (!String.IsNullOrEmpty(hdnCID.Value))
                            {
                                availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(true, null, Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), false);
                                availableUserGaugesInlcudingAssociated = UserDataMapper.GetAllUserGaugesAvailableForUser(true, null, Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), true);
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(hdnCID.Value))
                            {
                                availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(false, lbEditBuildingsBottom.Items, Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), false);
                                availableUserGaugesInlcudingAssociated = UserDataMapper.GetAllUserGaugesAvailableForUser(false, lbEditBuildingsBottom.Items, Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), true);
                            }
                        }
                    }
                    else if (isBuildingChanged)
                    {
                        //rebind the gauges based on role selection
                        if (RoleHelper.IsSuperAdminOrFullAdminOrFullUser(ddlEditRole.SelectedValue))
                        {
                            availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(true, null, Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), false);
                            availableUserGaugesInlcudingAssociated = UserDataMapper.GetAllUserGaugesAvailableForUser(true, null, Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), true);                            
                        }
                        else
                        {
                            availableUserGauges = UserDataMapper.GetAllUserGaugesAvailableForUser(false, lbEditBuildingsBottom.Items, Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), false);
                            availableUserGaugesInlcudingAssociated = UserDataMapper.GetAllUserGaugesAvailableForUser(false, lbEditBuildingsBottom.Items, Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), true);                            
                        }
                    }                    
                    //get and bind assigned user gauges.
                    //this is only if in user edit mode.
                    //for each assigned user gauge, format and bind                    
                    BindAssignedUserGaugeLoop(UserDataMapper.GetAllUserGaugesAssignedToUserAndAvailable(availableUserGaugesInlcudingAssociated, Convert.ToInt32(hdnEditID.Value)));
                }

                //for each available user gauge, format and bind
                BindAvailableUserGaugeLoop(availableUserGauges, isAddUser, isEditUser);
            }

            /// <summary>
            /// for each available user gauge, format and bind
            /// </summary>
            /// <param name="availableUserGauges"></param>
            /// <param name="isAddUser"></param>
            /// <param name="isEditUser"></param>
            private void BindAvailableUserGaugeLoop(IEnumerable<UserDataMapper.GetUserGaugeData> availableUserGauges, bool isAddUser, bool isEditUser)
            {
                //for each available user gauge              
                using (IEnumerator<UserDataMapper.GetUserGaugeData> list = availableUserGauges.GetEnumerator())
                {
                    int counter = 1;
                    while (list.MoveNext())
                    {
                        //current user gauge
                        UserDataMapper.GetUserGaugeData item = (UserDataMapper.GetUserGaugeData)list.Current;

                        ListItem listItem = new ListItem(String.Format("{0} - {1} - {2}", item.BuildingName, item.EquipmentName, item.PointName), item.PID.ToString());

                        if (isAddUser)
                        {
                            lbAddGaugesTop.Items.Add(listItem);
                        }
                        else if (isEditUser)
                        {
                            lbEditGaugesTop.Items.Add(listItem);
                        }

                        counter++;
                    }
                }
            }

            /// <summary>
            /// this is only if in user edit mode.
            /// for each assigned user gauge, format and bind
            /// </summary>
            /// <param name="assignedUserGauges"></param>
            private void BindAssignedUserGaugeLoop(IEnumerable<UserDataMapper.GetUserGaugeData> assignedUserGauges)
            {
                //for each available user gauge              
                using (IEnumerator<UserDataMapper.GetUserGaugeData> list = assignedUserGauges.GetEnumerator())
                {
                    int counter = 1;
                    while (list.MoveNext())
                    {
                        //current user gauge
                        UserDataMapper.GetUserGaugeData item = (UserDataMapper.GetUserGaugeData)list.Current;

                        ListItem listItem = new ListItem(String.Format("{0} - {1} - {2}", item.BuildingName, item.EquipmentName, item.PointName), item.PID.ToString());

                        lbEditGaugesBottom.Items.Add(listItem);

                        counter++;
                    }
                }
            }
        
        #endregion

        #region Bind QuickLinks

            /// <summary>
            /// Binds the initial set of user ganges when loaded for the first time into the form in edit mode.
            /// </summary>
            /// <param name="initialSetEditUserID"></param>
            /// <param name="initialSetEditRoleID"></param>
            /// <param name="initialSetEditClientID"></param>
            private void BindInitialSetEditUserQuickLinks(int initialSetEditUserID, int initialSetEditRoleID, int? initialSetEditClientID)
            {
                //clear quicklink listboxes, just in case
                lbEditQuickLinksTop.Items.Clear();
                lbEditQuickLinksBottom.Items.Clear();

                IEnumerable<QuickLink> availableQuickLinks = Enumerable.Empty<QuickLink>();

                //RAR: Commmented out for build
                //TODO: quicklinks are client and user based, so we need a client dropdown before we can edit quicklinks for a user
                ////bind the QuickLinks based on current role
                //availableQuickLinks = QuickLinksDataMapper.GetAllQuickLinksAvailableForUser(initialSetEditUserID, initialSetEditClientID, initialSetEditRoleID, false, false, false);

                //RAR: Commmented out for build
                //TODO: quicklinks are client and user based, so we need a client dropdown before we can edit quicklinks for a user
                ////bind available user quicklinks
                //BindAvailableUserQuickLinksLoop(availableQuickLinks, false, true);

                //RAR: Commmented out for build
                //TODO: quicklinks are client and user based, so we need a client dropdown before we can edit quicklinks for a user
                ////get and bind assigned user gauges.
                ////this is only if in user edit mode.
                //BindAssignedUserQuickLinksLoop(QuickLinksDataMapper.GetAllUserQuickLinksAssignedToUser(initialSetEditUserID));
            }


            /// <summary>
            /// Binds user quicklinks based on add/edit mode, role changed, client changed, and modules
            /// </summary>
            /// <param name="isAddUser"></param>
            /// <param name="isEditUser"></param>
            /// <param name="isRoleChanged"></param>
            /// <param name="isClientChanged"></param>
            private void BindQuickLinks(bool isAddUser, bool isEditUser, bool isRoleChanged, bool isClientChanged)
            {
                IEnumerable<QuickLink> availableQuickLinks = Enumerable.Empty<QuickLink>();
                IEnumerable<QuickLink> availableQuickLinksInlcudingAssociated = Enumerable.Empty<QuickLink>();

                if (isAddUser)
                {
                    //clear listboxes
                    lbAddQuickLinksTop.Items.Clear();
                    lbAddQuickLinksBottom.Items.Clear();

                    if (isRoleChanged)
                    {
                        //RAR: Commmented out for build
                        //TODO: quicklinks are client and user based, so we need a client dropdown before we can edit quicklinks for a user
                        ////rebind the quicklinks based on role selection                                                
                        //availableQuickLinks = QuickLinksDataMapper.GetAllQuickLinksAvailableForUser(null, Convert.ToInt32(Session["CID"].ToString()), Convert.ToInt32(ddlAddRole.SelectedValue), false, false, false);                                                    
                    }
                }
                else if (isEditUser)
                {
                    //clear listboxes
                    lbEditQuickLinksTop.Items.Clear();
                    lbEditQuickLinksBottom.Items.Clear();

                    if (isRoleChanged)
                    {
                        //rebind the quicklinks based on role selection                        
                        availableQuickLinks = QuickLinksDataMapper.GetAllQuickLinksAvailableForUser(Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), Convert.ToInt32(ddlEditRole.SelectedValue), false, false);
                        availableQuickLinksInlcudingAssociated = QuickLinksDataMapper.GetAllQuickLinksAvailableForUser(Convert.ToInt32(hdnEditID.Value), Convert.ToInt32(hdnCID.Value), Convert.ToInt32(ddlEditRole.SelectedValue), true, false);                                    
                    }

                    //RAR: Commmented out for build
                    //TODO: quicklinks are client and user based, so we need a client dropdown before we can edit quicklinks for a user
                    ////get and bind assigned user gauges.
                    ////this is only if in user edit mode.
                    ////for each assigned user gauge, format and bind                    
                    //BindAssignedUserQuickLinksLoop(QuickLinksDataMapper.GetAllQuickLinksAssignedToUserAndAvailable(availableQuickLinksInlcudingAssociated, Convert.ToInt32(hdnEditID.Value)));
                }

                //for each available user gauge, format and bind
                BindAvailableUserQuickLinksLoop(availableQuickLinks, isAddUser, isEditUser);
            }


            /// <summary>
            /// for each available user quicklink, format and bind
            /// </summary>
            /// <param name="availableUserQuickLinks"></param>
            /// <param name="isAddUser"></param>
            /// <param name="isEditUser"></param>
            private void BindAvailableUserQuickLinksLoop(IEnumerable<QuickLink> availableUserQuickLinks, bool isAddUser, bool isEditUser)
            {
                //for each available user QuickLink              
                using (IEnumerator<QuickLink> list = availableUserQuickLinks.GetEnumerator())
                {
                    int counter = 1;
                    while (list.MoveNext())
                    {
                        //current user QuickLink
                        QuickLink item = (QuickLink)list.Current;

                        ListItem listItem = new ListItem(item.QuickLinkName, item.QuickLinkID.ToString());

                        if (isAddUser)
                        {
                            lbAddQuickLinksTop.Items.Add(listItem);
                        }
                        else if (isEditUser)
                        {
                            lbEditQuickLinksTop.Items.Add(listItem);
                        }

                        counter++;
                    }
                }
            }

            /// <summary>
            /// this is only if in user edit mode.
            /// for each assigned user QuickLink, format and bind
            /// </summary>
            /// <param name="assignedUserQuickLinks"></param>
            private void BindAssignedUserQuickLinksLoop(IEnumerable<QuickLink> assignedUserQuickLinks)
            {
                //for each available user QuickLink              
                using (IEnumerator<QuickLink> list = assignedUserQuickLinks.GetEnumerator())
                {
                    int counter = 1;
                    while (list.MoveNext())
                    {
                        //current user QuickLink
                        QuickLink item = (QuickLink)list.Current;

                        ListItem listItem = new ListItem(item.QuickLinkName, item.QuickLinkID.ToString());

                        lbEditQuickLinksBottom.Items.Add(listItem);

                        counter++;
                    }
                }
            }
        

        #endregion

        #region Helper Methods

            private IEnumerable<UserDataMapper.GetUsersData> QueryUsers()
            {
                try
                {
                    //RAR
                    return null; // UserDataMapper.GetAllUsersByCID(siteUser.roleID, siteUser.CID);
                }
                catch (SqlException sqlEx)
                {
                    LogHelper.LogCWError("Error retrieving users in user administration.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogHelper.LogCWError("Error retrieving users in user administration.", ex);
                    return null;
                }
            }

            private IEnumerable<UserDataMapper.GetUsersData> QuerySearchedUsers(string[] searchText)
            {
                try
                {
                    //RAR
                    //get all user by client id
                    return null; // UserDataMapper.GetAllSearchedUsersByClientID(searchText, siteUser.roleID, siteUser.CID);
                }
                catch (SqlException sqlEx)
                {
                    LogHelper.LogCWError("Error retrieving users in user administration.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogHelper.LogCWError("Error retrieving users in user administration.", ex);
                    return null;
                }
            }

            private void BindUsers()
            {
                //query users
                IEnumerable<UserDataMapper.GetUsersData> users = QueryUsers();

                //maintain sort--- doesnt work!
                //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
                //gridUsers.Sort(GridViewSortExpression, tempSD);

                gridUsers.DataSource = users;

                // bind grid
                gridUsers.DataBind();

                SetGridCountLabel(users.Count());
            }

            private void BindSearchedUsers(string[] searchText)
            {
                //query users
                IEnumerable<UserDataMapper.GetUsersData> users = QuerySearchedUsers(searchText);

                //maintain sort--- doesnt work!
                //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
                //gridUsers.Sort(GridViewSortExpression, tempSD);

                gridUsers.DataSource = users;

                // bind grid
                gridUsers.DataBind();

                SetGridCountLabel(users.Count());
            }

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} user found", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} users found", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No users found";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            //***Linq to sql IEnumerable to DataTable conversion. Needed for sorting.****
            private DataTable ConvertIEnumerableToDataTable(IEnumerable dataSource)
            {
                System.Reflection.PropertyInfo[] propInfo = null;
                DataTable dt = new DataTable();

                foreach (object o in dataSource)
                {
                    propInfo = o.GetType().GetProperties();

                    for (int i = 0; i < propInfo.Length; i++)
                    {
                        dt.Columns.Add(propInfo[i].Name);
                    }
                    break;
                }

                foreach (object tempObject in dataSource)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < propInfo.Length; i++)
                    {
                        object t = tempObject.GetType().InvokeMember(propInfo[i].Name, BindingFlags.GetProperty, null, tempObject, new object[] { });
                        if (t != null)
                            dr[i] = t.ToString();
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }

        #endregion
    }
}

