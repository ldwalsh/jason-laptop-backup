﻿using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using Telerik.Web.UI;
using Telerik.Web.UI.Dock;

namespace CW.Website
{
    public partial class PerformanceDashboardNew : SitePage
    {
        #region event(s)

            void Page_FirstLoad()
            {
                var globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();

                litDashboardBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens
                (
                    siteUser.IsSchneiderTheme, 
                    globalSetting.PerformanceDashboardBody
                );

                SetClientInfoAndImages();

                foreach (var dock in FindControls<RadDock>())
				{
                    dock.DefaultCommands = DefaultCommands.ExpandCollapse;

                    dock.EnableRoundedCorners = true;

                    dock.Pinned = true;

                    dock.Resizable = false;

                    dock.CssClass = "radDockLarge";
                }
            }

            void Page_Load()
            {
                energyConsumption.SearchCriteria = searchCriteria;

                monthlyUtilityConsumption.SearchCriteria = searchCriteria;

                significantEnergyUsers.SearchCriteria = searchCriteria;
            }

        #endregion

        #region method(s)

            void SetClientInfoAndImages()
            {
                var client = DataMgr.ClientDataMapper.GetClient(siteUser.CID);

                if (client == null) return;

                imgHeaderClientImage.AlternateText = string.Empty;

                imgHeaderClientImage.Visible = true;

                imgHeaderClientImage.ImageUrl = HandlerHelper.ClientImageUrl(client.CID, client.ImageExtension);

                lblHeaderClientName.InnerText = siteUser.ClientName;
            }

        #endregion
    }
}