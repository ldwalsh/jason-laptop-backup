﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSEquipmentAdministration.aspx.cs" Inherits="CW.Website.KGSEquipmentAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/Equipment/ViewEquipment.ascx" tagname="ViewEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/AddEquipment.ascx" tagname="AddEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentVariables.ascx" tagname="EquipmentVariables" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentVariablesToMultipleEquipment.ascx" tagname="EquipmentVariablesToMultipleEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/AnalysesToAnEquipment.ascx" tagname="AnalysesToAnEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/AnalysisToMultipleEquipment.ascx" tagname="AnalysisToMultipleEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentToEquipment.ascx" tagname="EquipmentToEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/EquipmentStats.ascx" tagname="EquipmentStats" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Equipment/MoveEquipment.ascx" tagname="MoveEquipment" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <h1>KGS Equipment Administration</h1>

      <div class="richText">The kgs equipment administration area is used to view, add, and edit equipment.</div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>
      </div>

      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" CausesValidation="false" runat="server" SelectedIndex="0" MultiPageID="radMultiPage">
          <Tabs>
            <telerik:RadTab Text="View Equip." />
            <telerik:RadTab Text="Add Equip." />
            <telerik:RadTab Text="Equip. Vars To An Equip." />
            <telerik:RadTab Text="Equip. Var To Mult. Equip." />
            <telerik:RadTab Text="Analyses To An Equip." />
            <telerik:RadTab Text="Analysis To Mult. Equip." />
            <telerik:RadTab Text="Equip. To Equip." />
            <telerik:RadTab Text="Stats" />
            <telerik:RadTab Text="Move Equip." />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:ViewEquipment ID="ViewEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:AddEquipment ID="AddEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView3" runat="server">
            <CW:EquipmentVariables ID="EquipmentVariables" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView4" runat="server">   
            <CW:EquipmentVariablesToMultipleEquipment ID="EquipmentVariablesToMultipleEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView5" runat="server">   
            <CW:AnalysesToAnEquipment ID="AnalysesToAnEquipment" runat="server" />  
          </telerik:RadPageView> 

          <telerik:RadPageView ID="RadPageView6" runat="server">
            <CW:AnalysisToMultipleEquipment ID="AnalysisToMultipleEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView7" runat="server">
            <CW:EquipmentToEquipment ID="EquipmentToEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView8" runat="server">
            <CW:EquipmentStats ID="EquipmentStats" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView9" runat="server">
            <CW:MoveEquipment ID="MoveEquipment" runat="server" />
          </telerik:RadPageView>

        </telerik:RadMultiPage>
      </div>

</asp:Content>