﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.Master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemUploadBMSInfo.aspx.cs" Inherits="CW.Website.SystemUploadBMSInfo" %>
<%@ Register src="~/_controls/upload/UploadHeader.ascx" tagname="UploadHeader" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/UploadFormat.ascx" tagname="UploadFormat" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupsBMSInfoUpload.ascx" tagname="IDLookupsBMSInfoUpload" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/Upload.ascx" tagname="Upload" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <CW:UploadHeader ID="UploadHeader" runat="server" />
  <CW:UploadFormat ID="UploadFormat" runat="server" />  
  <CW:IDLookupsBMSInfoUpload ID="IDLookupsBMSInfoUpload" runat="server" />
  <CW:Upload ID="Upload" runat="server" />

</asp:Content>
