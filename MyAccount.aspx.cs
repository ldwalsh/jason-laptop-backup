﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

using CW.Business;
using CW.Data;
using CW.Common.Helpers;
using CW.Utility;
using CW.Website._framework;
using CW.Data.Models.User;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class MyAccount: SitePage
    {
        #region Properties

            //private User mUser;
            //private HttpCookie mCookie;
            public string email;

            const string userExists = "User already exists.";                        
            const string reassigningQuickLinksFailed = "Error reassigning quicklinks.";
            const string reassigningQuickLinksSuccessful = "Quick Links update was successful.";
            const string updateSuccessful = "Account update was successful.";
            const string updateFailed = "Account update failed. Please contact an administrator.";
            const string invalidPasswordFirstOrLast = "Your password cannot contain your first or last name.";
            const string invalidPasswordEmail = "Your password cannot contain parts of your email.";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                LoggedInCheck();

                //hide labels
                lblEditError.Visible = false;
                lblEditQuickLinkError.Visible = false;
                if (siteUser.IdentityManagedExternalProvider)
                {
                    externallyManaged.Style.Value = "display: block";
                }
                else
                {
                    internallyManaged.Style.Value = "display: block";
                }
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindCountries(ddlEditCountry);
                    BindCultures(ddlEditCulture);

                    var mUser = DataMgr.UserDataMapper.GetUser(siteUser.UID);                    

                    SetUserIntoEditForm(mUser);                     
                  
                    if(!String.IsNullOrEmpty(Request.QueryString["tab"]))
                        SetTab(Request.QueryString["tab"]);
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetUserIntoEditForm(User user)
            {
                //ID
                hdnEditID.Value = Convert.ToString(user.UID);
                //First Name
                txtEditFirstName.Text = user.FirstName;
                //Last Name
                txtEditLastName.Text = user.LastName;
                //Email
                txtEmail.InnerText = user.Email;
                //Address
                txtEditAddress.Text = user.Address;
                //City
                txtEditCity.Text = user.City;
                //Country Alpha2Code
                try
                {
                    ddlEditCountry.SelectedValue = user.CountryAlpha2Code;

                    ddlEditCountry_OnSelectedIndexChanged(null, null);

                    //set zip validators accordingly
                    editZipRequiredValidator.Enabled = editZipRegExValidator.Enabled = user.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                    txtEditZip.Mask = user.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa"; 

                    //StateID
                    try
                    {
                        ddlEditState.SelectedValue = Convert.ToString(user.StateID);
                    }
                    catch
                    {
                    }

                    //set state validator accordingly
                    editStateRequiredValidator.Enabled = user.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                }
                catch
                {
                }
                //Zip
                txtEditZip.Text = user.Zip;
                //Phone
                txtEditPhone.Text = user.Phone;
                //MobilePhone
                txtEditMobilePhone.Text = user.MobilePhone;
                //Culture
                ddlEditCulture.SelectedValue = Convert.ToString(user.LCID);
 
                //Role
                //not shown

                //Client
                //not shown

                //Active
                //not shown

                //ResetPassword
                txtEditNewPassword.Text = "";               
            }

        #endregion

        #region Load and Bind Fields

            private void BindCountries(DropDownList ddl)
            {
                ddl.DataTextField = "CountryName";
                ddl.DataValueField = "Alpha2Code";
                ddl.DataSource = DataMgr.CountryDataMapper.GetAllCountries();
                ddl.DataBind();
            }

            private void BindStates(DropDownList ddl, string alpha2Code)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code);
                ddl.DataBind();
            }

            private void BindCultures(DropDownList ddl)
            {
                ddl.DataTextField = "DisplayName";
                ddl.DataValueField = "LCID";
                ddl.DataSource = CultureHelper.FilterUserCultures(CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.DisplayName).ToArray());
                ddl.DataBind();
            }

        #endregion

        #region Load User

            protected void LoadEditFormIntoUser(User user)
            {
                //ID
                user.UID = Convert.ToInt32(hdnEditID.Value);
                //First Name
                user.FirstName = txtEditFirstName.Text;
                //Last Name
                user.LastName = txtEditLastName.Text;
                //Email
                user.Email = txtEmail.InnerText;                
                //Address
                user.Address = txtEditAddress.Text;
                //City
                user.City = txtEditCity.Text;
                //CountryAlpha2Code
                user.CountryAlpha2Code = ddlEditCountry.SelectedValue;
                //StateID
                user.StateID = ddlEditState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditState.SelectedValue) : null;
                //ZIP
                user.Zip = String.IsNullOrEmpty(txtEditZip.Text) ? null : txtEditZip.Text; 
                //Phone
                user.Phone = String.IsNullOrEmpty(txtEditPhone.Text) ? null : txtEditPhone.Text;
                //MobilePhone
                user.MobilePhone = String.IsNullOrEmpty(txtEditMobilePhone.Text) ? null : txtEditMobilePhone.Text;
                //Culture
                user.LCID = Convert.ToInt32(ddlEditCulture.SelectedValue);

                //Client
                //not edited

                //Role
                //not edited

                //New Password
                user.Password = user.Password = String.IsNullOrEmpty(txtEditNewPassword.Text) ? "" : Encryptor.getMd5Hash(txtEditNewPassword.Text);          

                //Active
                //not edited
            }

        #endregion

        #region Dropdown Events

            protected void ddlEditCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlEditState, ddlEditCountry.SelectedValue);

                editZipRequiredValidator.Enabled = editZipRegExValidator.Enabled = ddlEditCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtEditZip.Mask = ddlEditCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
                editStateRequiredValidator.Enabled = ddlEditState.Items.Count > 1;
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Update user Button on click.
            /// </summary>
            protected void updateUserButton_Click(object sender, EventArgs e)
            {   
                //create and load the form into the user
                User mUser = new User();
                LoadEditFormIntoUser(mUser);

                //check if the user email exists for another user if trying to change
                if (DataMgr.UserDataMapper.DoesUserExist(mUser.UID, mUser.Email))
                    LabelHelper.SetLabelMessage(lblEditError, userExists, lnkSetFocus);
                //update the user
                else
                {
                    try
                    {
                        //if new password was entered
                        if (!siteUser.IdentityManagedExternalProvider && !String.IsNullOrEmpty(txtEditNewPassword.Text))
                        {
                            string[] emailSplit = txtEmail.InnerText.Substring(0, txtEmail.InnerText.IndexOf("@")).Split('.');

                            foreach(string es in emailSplit)
                            {
                                if (es.Length >= 3 && txtEditNewPassword.Text.ToLower().Contains(es.ToLower()))
                                {
                                    LabelHelper.SetLabelMessage(lblEditError, invalidPasswordEmail, lnkSetFocus);
                                    return;
                                }
                            }
                               
                            if ((txtEditFirstName.Text.Length >= 3 && txtEditNewPassword.Text.ToLower().Contains(txtEditFirstName.Text.ToLower())) || (txtEditLastName.Text.Length >= 3 && txtEditNewPassword.Text.ToLower().Contains(txtEditLastName.Text.ToLower())))
                            {
                                LabelHelper.SetLabelMessage(lblEditError, invalidPasswordFirstOrLast, lnkSetFocus);
                                return;
                            }
                            
                            DataMgr.UserDataMapper.UpdateUser(mUser, true, false, false);
                        }
                        else
                        {
                            DataMgr.UserDataMapper.UpdateUser(mUser, false, false, false);
                        }

                        //set new sessions and cookies
                        SetNewSessionsAndCookies(mUser);
                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error updating user in my account: UserId={0}, UserEmail={1}, UserFirstName={2} UserLastName={3}.", mUser.UID, mUser.Email, mUser.FirstName, mUser.LastName), ex);
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocus);                    
                    } 
                }

                editConfirmNewPasswordRequiredValidator.IsValid = true;
            }

            /// <summary>
            /// Update user quick links Button on click for client and user logged in as.
            /// </summary>
            protected void updateUserQuickLinksButton_Click(object sender, EventArgs e)
            {
                bool quickLinkEditSuccess = true;
                int uid = siteUser.UID;

                //deletes all user quicklinks by cid and uid
                //keep this as deleting all, as opposed to the user administration pages, its a good way for users to self clean up quick links if they get messed up.
                DataMgr.QuickLinksDataMapper.DeleteAllUserClientQuickLinksByCIDAndUID(siteUser.CID, uid);

                //assign user quicklinks if appropriate role and/or client exists in listbox---
                if (lbEditQuickLinksBottom.Items.Count > 0)
                {
                    try
                    {
                        //inserting each UserQuickLink in the bottom listbox if they dont exist
                        foreach (ListItem item in lbEditQuickLinksBottom.Items)
                        {
                            //declare new Users_Clients_QuickLink
                            Users_Clients_QuickLink mUserClientQuickLink = new Users_Clients_QuickLink();

                            int quickLinkID = Convert.ToInt32(item.Value);

                            mUserClientQuickLink.QuickLinkID = quickLinkID;
                            mUserClientQuickLink.UID = uid;
                            mUserClientQuickLink.CID = siteUser.CID;
                            DataMgr.QuickLinksDataMapper.InsertUserClientQuickLink(mUserClientQuickLink);
                        }

                        ////deleting each quicklink in the top listbox if they exist
                        //foreach (ListItem item in lbEditQuickLinksTop.Items)
                        //{
                        //    //if the quicklink exists, delete
                        //    if (QuickLinksDataMapper.DoesQuickLinkExistForUser(Convert.ToInt32(item.Value), mUser.UserID))
                        //    {
                        //        QuickLinksDataMapper.DeleteUserQuickLinkByQuickLinkIDAndUserID(Convert.ToInt32(item.Value), mUser.UserID);
                        //    }
                        //}
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning quicklinks to user in my account.", sqlEx);
                        quickLinkEditSuccess = false;
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning quicklinks to user in my account.", ex);
                        quickLinkEditSuccess = false;
                    }
                }

                lblEditQuickLinkError.Text = quickLinkEditSuccess ? reassigningQuickLinksSuccessful : reassigningQuickLinksFailed;
                lblEditQuickLinkError.Visible = true;  
            }

            /// <summary>
            /// on edit QuickLinks button up click, add QuickLinks
            /// </summary>
            protected void btnEditQuickLinksUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditQuickLinksBottom.SelectedIndex != -1)
                {
                    lbEditQuickLinksTop.Items.Add(lbEditQuickLinksBottom.SelectedItem);
                    lbEditQuickLinksBottom.Items.Remove(lbEditQuickLinksBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit QuickLinks button down click, add QuickLinks
            /// </summary>
            protected void btnEditQuickLinksDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditQuickLinksTop.SelectedIndex != -1)
                {
                    lbEditQuickLinksBottom.Items.Add(lbEditQuickLinksTop.SelectedItem);
                    lbEditQuickLinksTop.Items.Remove(lbEditQuickLinksTop.SelectedItem);
                }
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// On Init for Tabs
            /// </summary>
            //protected void loadTabs(object sender, EventArgs e)
            //{
            //}

            /// <summary>
            /// Tab changed event, bind users grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    var mUser = DataMgr.UserDataMapper.GetUser(siteUser.UID);

                    SetUserIntoEditForm(mUser);  
                }
                else if (radTabStrip.MultiPage.SelectedIndex == 1)
                    BindInitialSetEditUserQuickLinks(); 
            }

        #endregion

        #region Bind QuickLinks

            /// <summary>
            /// Binds the initial set of user ganges when loaded for the first time into the form in edit mode.
            /// </summary>
            private void BindInitialSetEditUserQuickLinks()
            {
                //clear quicklink listboxes, just in case
                lbEditQuickLinksTop.Items.Clear();
                lbEditQuickLinksBottom.Items.Clear();

                //get based on user client modules restriction
                //get available quicklinks not assigned by uid, cid, and role
                IEnumerable<QuickLink> availableQuickLinks = DataMgr.QuickLinksDataMapper.GetAllQuickLinksAvailableForUser(siteUser.UID, siteUser.CID, siteUser.RoleID, false);

                //bind available user quicklinks
                BindAvailableUserQuickLinksLoop(availableQuickLinks);

                //get and bind assigned user quicklinks.
                //this is only if in user edit mode.
                BindAssignedUserQuickLinksLoop(DataMgr.QuickLinksDataMapper.GetAllQuickLinksAssignedToUser(siteUser.UID, siteUser.CID));
            }

            /// <summary>
            /// for each available user quicklink, format and bind
            /// </summary>
            /// <param name="availableUserQuickLinks"></param>
            /// <param name="isAddUser"></param>
            /// <param name="isEditUser"></param>
            private void BindAvailableUserQuickLinksLoop(IEnumerable<QuickLink> availableUserQuickLinks)
            {
                //for each available user QuickLink              
                using (IEnumerator<QuickLink> list = availableUserQuickLinks.GetEnumerator())
                {
                    int counter = 1;
                    while (list.MoveNext())
                    {
                        //current user QuickLink
                        QuickLink item = (QuickLink)list.Current;

                        ListItem listItem = new ListItem(item.QuickLinkName, item.QuickLinkID.ToString());

                        lbEditQuickLinksTop.Items.Add(listItem);                        

                        counter++;
                    }
                }
            }

            /// <summary>
            /// this is only if in user edit mode.
            /// for each assigned user QuickLink, format and bind
            /// </summary>
            /// <param name="assignedUserQuickLinks"></param>
            private void BindAssignedUserQuickLinksLoop(IEnumerable<QuickLink> assignedUserQuickLinks)
            {
                //for each available user QuickLink              
                using (IEnumerator<QuickLink> list = assignedUserQuickLinks.GetEnumerator())
                {
                    int counter = 1;
                    while (list.MoveNext())
                    {
                        //current user QuickLink
                        QuickLink item = (QuickLink)list.Current;

                        ListItem listItem = new ListItem(item.QuickLinkName, item.QuickLinkID.ToString());

                        lbEditQuickLinksBottom.Items.Add(listItem);

                        counter++;
                    }
                }
            }
        
        #endregion

        #region Helper Methods

            private void SetNewSessionsAndCookies(User mUser)
            {
                //set new sessions
                sessionState["UserEmail"] = mUser.Email;
                sessionState["UserFirstName"] = mUser.FirstName;
                sessionState["UserLastName"] = mUser.LastName;

                //set new cookies if exists
                //if (Request.Cookies["UserInfo"] != null)
                //{
                //    if (Request.Cookies["UserInfo"].Expires > DateTime.UtcNow)
                //    {
                //        mCookie = Request.Cookies["UserInfo"];
                //        try
                //        {
                //            mCookie["UserEmail"] = mUser.Email;
                //            mCookie["UserFirstName"] = mUser.FirstName;
                //            mCookie["UserLastName"] = mUser.LastName;
                //        }
                //        catch
                //        {
                //        }
                //    }
                //}
            }

            protected void LoggedInCheck()
            {
                //Check if logged in
                if (!String.IsNullOrWhiteSpace(siteUser.Email) && !siteUser.IsPublicOrKioskUser)
                    email = siteUser.Email;
                else
                    Response.Redirect("/Home.aspx");
            }

            protected void SetTab(string tab)
            {            
                int t;
                if (Int32.TryParse(tab, out t) && (t + 1 <= radTabStrip.MultiPage.PageViews.Count))
                {
                    radTabStrip.Tabs[t].Selected = true;
                    radTabStrip.MultiPage.SelectedIndex = t;
                    BindInitialSetEditUserQuickLinks();
                }
            }

        #endregion
    }
}