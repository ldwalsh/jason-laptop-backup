﻿using System;
using System.Collections.Generic;

namespace CW.Website.Kiosk.domain
{
    public class BuildingUtilities
    {
        public KeyValuePair<String,Double>[] BuildingUtilitiesTotals{get;set;}

        public String __type
        {
            get { return "Kiosk.domain.BuildingUtilities"; }
        }
    }
}