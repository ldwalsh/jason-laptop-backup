﻿using CW.Data.Models.Building;
using System;
using System.Collections.Generic;

namespace CW.Kiosk.domain
{
	public class PortfolioUtilities
	{
		public IDictionary<String,BuildingUtilities> BuildingUtilities;
		public IDictionary<String,String>			 PointTypeDisplayNameLookup;
		public IDictionary<String,Int32>			 PointTypeClassLookup;

		public String __type
		{
			get {return "Kiosk.domain.PortfolioUtilities";}
		}
	}
}