﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website.Kiosk.domain
{
    public class BuildingHourlyUtilities
    {
        public KeyValuePair<Int32,Double>[] ElectricTotals {get;set;}
        public KeyValuePair<Int32,Double>[] SteamTotals {get;set;}
        public KeyValuePair<Int32,Double>[] ChilledWaterTotals {get;set;}
        public KeyValuePair<Int32,Double>[] HotWaterTotals { get; set; }
        public KeyValuePair<Int32,Double>[] GasTotals {get;set;}
        public KeyValuePair<Int32,Double>[] WaterTotals {get;set;}
        public KeyValuePair<Int32,Double>[] Totals	{get;set;}

        public KeyValuePair<Int32,Double>[] ElectricYesterdayTotals {get;set;}
        public KeyValuePair<Int32,Double>[] SteamYesterdayTotals {get;set;}
        public KeyValuePair<Int32,Double>[] ChilledWaterYesterdayTotals {get;set;}
        public KeyValuePair<Int32,Double>[] HotWaterYesterdayTotals { get; set; }
        public KeyValuePair<Int32,Double>[] GasYesterdayTotals {get;set;}
        public KeyValuePair<Int32,Double>[] WaterYesterdayTotals {get;set;}
        public KeyValuePair<Int32,Double>[] YesterdayTotals {get;set;}

        public BuildingHourlyUtilities()
        {
            this.ElectricTotals					= new KeyValuePair<Int32,Double>[0];
            this.SteamTotals					= new KeyValuePair<Int32,Double>[0];
            this.ChilledWaterTotals				= new KeyValuePair<Int32,Double>[0];
            this.GasTotals						= new KeyValuePair<Int32,Double>[0];
            this.WaterTotals					= new KeyValuePair<Int32,Double>[0];
            this.HotWaterTotals                 = new KeyValuePair<Int32, Double>[0];
            this.Totals							= new KeyValuePair<Int32,Double>[0];

            this.ElectricYesterdayTotals		= new KeyValuePair<Int32,Double>[0];
            this.SteamYesterdayTotals			= new KeyValuePair<Int32,Double>[0];
            this.ChilledWaterYesterdayTotals	= new KeyValuePair<Int32,Double>[0];
            this.GasYesterdayTotals				= new KeyValuePair<Int32,Double>[0];
            this.WaterYesterdayTotals			= new KeyValuePair<Int32,Double>[0];
            this.HotWaterYesterdayTotals        = new KeyValuePair<Int32, Double>[0];
            this.YesterdayTotals				= new KeyValuePair<Int32,Double>[0];
        }

        public String __type
        {
            get { return "Kiosk.domain.BuildingHourlyUtilities"; }
        }
    }
}