﻿using System;
using System.Collections.Generic;

namespace CW.Website.Kiosk.domain
{
    public class BuildingStatistics
    {
        public KeyValuePair<string,int> TotalPoints { get; set; }
        public KeyValuePair<string, int> TotalEquipment { get; set; }
        public KeyValuePair<string, int>[] EquipmentClassTotals { get; set; }
        
        public String __type
        {
            get { return "Kiosk.domain.BuildingStatistics"; }
        }
    }
}