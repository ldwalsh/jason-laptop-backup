﻿using System;
using System.Runtime.Serialization;

namespace CW.Website.Kiosk
{
	[
	DataContract
	]
	public class ClientInfo
	{
		[
		DataMember
		]
		public Int32 defaultBID;

		[
		DataMember
		]
		public String headerImgSrc;
		
		[
		DataMember
		]
		public String clientImgSrc;

		[
		DataMember
		]
		public String kioskContent;

		[
		DataMember
		]
		public String kioskQRContent;

		[
		DataMember
		]
		public String kioskQRLink;

		[
		DataMember
		]
		public String[] chartColors;
	}
}