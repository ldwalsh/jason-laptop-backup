﻿using CW.Common.Config;
using CW.Website.DependencyResolution;
using JSAsset;
using System;

namespace CW.Website.Kiosk
{
	public class KioskAsset: EmbeddedAssetBase
	{
		public override String Name
		{
			get {return "Kiosk";}
		}

		public override String RelativePath
		{
			get {return "Kiosk/script/";}
		}

		public override String[] Dependencies
		{
			get {return new[]{"iCL"};}
		}

        public override String InitFunction
        {
            get {return "init";}
        }

        public override Int64 Version
        {
            get {return Int64.Parse(IoC.Resolve<IConfigManager>().GetConfigurationSetting("versionNumber", new DateTime().Ticks.ToString()).Replace(".", String.Empty));}
        }
    }
}