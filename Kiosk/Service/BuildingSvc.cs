﻿using CW.Business;
using CW.Business.Results;
using CW.Business.UtilityMeters;
using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Data.Models.Building;
using CW.Data.Models.Utility;
using CW.Kiosk.domain;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.Kiosk.Service
{
	public class BuildingSvc
	{
		#region field

			private readonly ISiteUser SiteUser;
            private DataManager mDataManager = null;

		#endregion
		
		#region constructor
		
			public BuildingSvc(ISiteUser siteUser) //should be using ISiteUser
			{
				SiteUser = siteUser;
                mDataManager = DataManager.Get(System.Web.HttpContext.Current.Session.SessionID);
			}

			public BuildingSvc()
			{
				SiteUser = CW.Website._framework.SiteUser.Current;
                mDataManager = DataManager.Get(System.Web.HttpContext.Current.Session.SessionID);
            }

		#endregion

		#region method

			private BuildingHourlyUtilities GetBuildingHourlyUtilities(int bid, string timeZoneID)
			{
				Func<Int32, DateTime, Int32, IEnumerable<RawData>>
					getRawDataByBIDAndDayAndPointTypeID = (buildingID, day, pointTypeID) =>
					{
                        return CW.Business.DataManager.Get(System.Web.HttpContext.Current.Session.SessionID).RawDataMapper.GetRawPlotDataForPoints(mDataManager.PointDataMapper.GetAllPointsByBIDAndPointTypeID(buildingID, pointTypeID), day, day.AddDays(1));
					};

                var udm = new UtilitySummaryManager(bid, timeZoneID, 7, getRawDataByBIDAndDayAndPointTypeID, mDataManager.BuildingDataMapper.GetBuildingSettingsByBID(bid).UnitSystem);
                
				#region Set Totals

				udm.SetElectricTotals(new[]{BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID, BusinessConstants.PointType.BuildingElectricPowerPointTypeID,}	);

				// Get from Submeters if no totals found
				if (!udm.HourlyTotals.ElectricTotals.Any())
				{
					udm.HourlyTotals.Usage.Remove(BusinessConstants.PointClass.BuildingElectricalEnergyPointClassID.ToString());
					udm.HourlyTotals.UsageComparison.Remove(BusinessConstants.PointClass.BuildingElectricalEnergyPointClassID.ToString());

					udm.SetElectricTotals
					(
						new[]{BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID, BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID,},
						true
					);
				}

				udm.SetSteamTotals(new[]{BusinessConstants.PointType.BuildingSteamEnergyPointTypeID,  BusinessConstants.PointType.BuildingSteamFlowPointTypeID,});

				// Get from Submeters if no totals found
				if (!udm.HourlyTotals.SteamTotals.Any())
				{
					udm.HourlyTotals.Usage.Remove(BusinessConstants.PointClass.BuildingSteamWeightPointClassID.ToString());
					udm.HourlyTotals.UsageComparison.Remove(BusinessConstants.PointClass.BuildingSteamWeightPointClassID.ToString());

					udm.SetSteamTotals
					(
						new int[]
							{
								BusinessConstants.PointType.SubmeterSteamEnergyPointTypeID, 
								BusinessConstants.PointType.SubmeterSteamFlowPointTypeID,
							},
						true
					);
				}

				udm.SetChilledWaterTotals(new[]{BusinessConstants.PointType.BuildingCHWCoolingEnergyPointTypeID, BusinessConstants.PointType.BuildingCHWCoolingRatePointTypeID,});

				udm.SetHotWaterTotals(new[]{BusinessConstants.PointType.BuildingHWHeatingEnergyPointTypeID, BusinessConstants.PointType.BuildingHWHeatingRatePointTypeID,});

				udm.SetGasTotals
				(
					new[]
					{
						BusinessConstants.PointType.BuildingGasEnergyPointTypeID, 
						BusinessConstants.PointType.BuildingGasFlowPointTypeID, 
						BusinessConstants.PointType.SubmeterGasEnergyPointTypeID, 
						BusinessConstants.PointType.SubmeterGasFlowPointTypeID, 
					}
				);

				// Get from Submeters if no totals found
				if (!udm.HourlyTotals.GasTotals.Any())
				{
					udm.HourlyTotals.Usage.Remove(BusinessConstants.PointClass.BuildingGasEnergyPointClassID.ToString());
					udm.HourlyTotals.UsageComparison.Remove(BusinessConstants.PointClass.BuildingGasEnergyPointClassID.ToString());

					udm.SetGasTotals(new[]{BusinessConstants.PointType.SubmeterGasEnergyPointTypeID, BusinessConstants.PointType.SubmeterGasFlowPointTypeID,}, 	true);
				}

				udm.SetWaterTotals(new[]{BusinessConstants.PointType.BuildingWaterVolumePointTypeID, BusinessConstants.PointType.BuildingWaterFlowPointTypeID,});

				// Get from Submeters if no totals found
				if (!udm.HourlyTotals.WaterTotals.Any())
				{
					udm.HourlyTotals.Usage.Remove(BusinessConstants.PointClass.BuildingFluidVolumePointClassID.ToString());
					udm.HourlyTotals.UsageComparison.Remove(BusinessConstants.PointClass.BuildingFluidVolumePointClassID.ToString());

					udm.SetWaterTotals(new[]{BusinessConstants.PointType.SubmeterWaterFlowPointTypeID,}, true);
				}

				// Call last after all individial sets
				udm.SetTotals();

				#endregion

				return udm.HourlyTotals;
			}

			public KioskBuilding[] GetBuildings()
			{
                var inputs = new KioskBuildingInputs() { CID = SiteUser.CID, ColorPointTypeIDs = BusinessConstants.PointType.KioskColorPointTypeIDs, EnergyUsePointTypeIDs = BusinessConstants.PointType.KioskEnergyUsePointTypeIDs, EnergyPointClassIDs = BusinessConstants.PointClass.KioskTopLevelPointClassIDs, PowerPointClassIDs = EnumHelper.ToArray<BusinessConstants.PointClass.PowerPointClassEnum>(), Creds = SiteUser };

				// check cache
                var kbrm = new KioskBuildingResultsManager(mDataManager, inputs, (cid, bid, imageExt) => { return HandlerHelper.BuildingImageUrl(cid, bid, imageExt); });

				var visibleBuildings = SiteUser.GetUpdatedVisibleBuidings().Select(_ => _.BID);
			
				return kbrm.Get().Where(b=> visibleBuildings.Contains(b.BID)).ToArray();
			}

			public IDictionary<String,BuildingStatistics> GetPortfolioStatistics() //Dictionary key must be string to be serializable (for now while using default serializer)
			{
                var inputs = new KioskBuildingStatsInputs(){Creds=SiteUser, EquipmentClassLabelDict=BusinessConstants.EquipmentClass.KioskEquipmentClassTotalLabelDictionary };

                var stats = new BuildingStatisticsResultsManager(mDataManager, inputs).Get();

				return SiteUser.GetUpdatedVisibleBuidings().ToDictionary(b=>b.BID.ToString(), b => stats.Where(bs=>bs.BID == b.BID).FirstOrDefault());
			}

			public PortfolioUtilities GetPortfolioUtilities()
			{
				var utcYesterday = DateTime.UtcNow.AddDays(-1);

                var inputs = new UtilityBuildingInputs(){Creds=SiteUser, StartDate=new DateTime(utcYesterday.Year, utcYesterday.Month, utcYesterday.Day)};
                
                var utilities =
                    new BuildingUtilitiesResultsManager(mDataManager, inputs).Get().Where(bu => SiteUser.GetUpdatedVisibleBuidings().Select(_ => _.BID).Contains(bu.BID)).ToDictionary(_ => _.BID.ToString(), _ => _);
					
				var pointTypeIDs = new List<Int32>();
			
				utilities.ToList().ForEach(_=>_.Value.BuildingUtilitiesTotals.ToList().ForEach(__=>{if (!pointTypeIDs.Contains(__.Key)){pointTypeIDs.Add(__.Key);}}));

                var pointTypes = mDataManager.PointTypeDataMapper.GetByIDs(pointTypeIDs).ToList();

				return
				new PortfolioUtilities
					{
						BuildingUtilities          = utilities,
						PointTypeDisplayNameLookup = pointTypes.ToDictionary(_=>_.PointTypeID.ToString(), _=>_.DisplayName),  //combine into one loop
						PointTypeClassLookup       = pointTypes.ToDictionary(_=>_.PointTypeID.ToString(), _=>_.PointClassID), //combine into one loop
					};
			}

			public IDictionary<String,BuildingHourlyUtilities> GetPortfolioHourlyUtilities()
			{
				return SiteUser.GetUpdatedVisibleBuidings().ToDictionary(_=>_.BID.ToString(), _=>GetBuildingHourlyUtilities(_.BID, _.TimeZone));
			}

			public IDictionary<String,BuildingHourlyUtilities> GetPortfolioHourlyUtilitiesByBuilding(Int32 bid)
			{
				var x = SiteUser.GetUpdatedVisibleBuidings().Where(_=>_.BID == bid).ToDictionary(_=>_.BID.ToString(), _=>GetBuildingHourlyUtilities(_.BID, _.TimeZone));

				return x;
			}

			public String GetHelp(Int32 id)
			{
				return
				TokenVariableHelper.FormatSpecialThemeBasedTokens
				(
                    SiteUser.IsSchneiderTheme, (String)typeof(GlobalSetting).GetProperty("KioskHelpQ" + id).GetValue(mDataManager.GlobalDataMapper.GetGlobalSettings())
				);
			}

		#endregion
	}
}