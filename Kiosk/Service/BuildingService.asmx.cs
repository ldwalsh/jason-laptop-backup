﻿using CW.Data.Models.Building;
using CW.Kiosk.domain;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Script.Services;
using System.Web.Services;

namespace CW.Website.Kiosk.Service
{
    [
    ScriptService
    ]
    public class BuildingService: WebService
    {
        [
        WebMethod(EnableSession=true),
		ScriptMethod(UseHttpGet=true),
		]
        public KioskBuilding[] GetBuildings()
        {
			return new BuildingSvc().GetBuildings();
        }

        [
        WebMethod(EnableSession=true),
        ScriptMethod(UseHttpGet=true),
        ]
        public IDictionary<String,BuildingStatistics> GetPortfolioStatistics() //Dictionary key must be string to be serializable
        {
            return new BuildingSvc().GetPortfolioStatistics();
        }

        [
        WebMethod(EnableSession=true),
        ScriptMethod(UseHttpGet=true),
        ]
		public PortfolioUtilities GetPortfolioUtilities()
        {
			return new BuildingSvc().GetPortfolioUtilities();
        }

        [
        WebMethod(EnableSession=true),
        ScriptMethod(UseHttpGet=true),
        ]
        public IDictionary<String,BuildingHourlyUtilities> GetPortfolioHourlyUtilities()
        {
			return new BuildingSvc().GetPortfolioHourlyUtilities();
        }

        [
        WebMethod(EnableSession=true),//, CacheDuration=Int32.MaxValue),
        ScriptMethod(UseHttpGet=true),
        ]
        public IDictionary<String,BuildingHourlyUtilities> GetPortfolioHourlyUtilitiesByBuilding(Int32 bid)
        {
//Context.Response.Cache.GetType().GetField("_maxAge", (BindingFlags.Instance|BindingFlags.NonPublic)).SetValue(Context.Response.Cache, TimeSpan.FromHours(10));

			return new BuildingSvc().GetPortfolioHourlyUtilitiesByBuilding(bid);
        }

        [
        WebMethod(EnableSession=true),//, CacheDuration=Int32.MaxValue),
        ScriptMethod(UseHttpGet=true),
        ]
        public String GetHelp(Int32 id)
        {			
			return new BuildingSvc().GetHelp(id);
        }
    }
}
