﻿
///
///<reference path="$iCL.js" />

///<reference path="Kiosk.js" />
///<reference path="Kiosk/Landing.js" />
///<reference path="Kiosk/BingMapsHelper.js" />
///<reference path="Kiosk/KeyValuePair.js" />
///<reference path="Kiosk/BuildingCoordinatesLookup.js" />
///<reference path="Kiosk/BuildingSelectorDialog.js" />
///<reference path="Kiosk/BuildingService.js" />
///<reference path="Kiosk/ClientInfo.js" />
///<reference path="Kiosk/DataManager.js" />
///<reference path="Kiosk/DataManagerForUtilities.js" />
///<reference path="Kiosk/ExpanderControl.js" />
///<reference path="Kiosk/HelpDialog.js" />
///<reference path="Kiosk/HelpWidget.js" />
///<reference path="Kiosk/KioskService.js" />
///<reference path="Kiosk/PresenterBase.js" />
///<reference path="Kiosk/ViewBase.js" />
///<reference path="Kiosk/ViewBaseWithChart.js" />

///<reference path="Kiosk/domain/Building.js" />
///<reference path="Kiosk/domain/BuildingHourlyUtilities.js" />
///<reference path="Kiosk/domain/BuildingStatistics.js" />
///<reference path="Kiosk/domain/BuildingUtilities.js" />
///<reference path="Kiosk/domain/PortfolioUtilities.js" />

///<reference path="Kiosk/presenters/BuildingProfilePresenter.js" />
///<reference path="Kiosk/presenters/BuildingUtilitiesPresenter.js" />
///<reference path="Kiosk/presenters/HeaderPresenter.js" />
///<reference path="Kiosk/presenters/MapPresenter.js" />
///<reference path="Kiosk/presenters/PortfolioProfilePresenter.js" />

///<reference path="Kiosk/utilityDataLoadStrategies/KioskUserUDLStrategy.js" />
///<reference path="Kiosk/utilityDataLoadStrategies/PortlUserUDLStrategy.js" />
///<reference path="Kiosk/utilityDataLoadStrategies/UDLStrategyBase.js" />

///<reference path="Kiosk/views/BuildingProfileView.js" />
///<reference path="Kiosk/views/BuildingUtilitiesView.js" />
///<reference path="Kiosk/views/HeaderView.js" />
///<reference path="Kiosk/views/MapView.js" />
///<reference path="Kiosk/views/PortfolioProfileView.js" />
