﻿///<reference path="../$Kiosk.js" />

Object.define
(
	{
		namespace: Kiosk,
		prototype: iCL.TimePollerBase,
	},

	function DataManager(pollTime)
	{
		///<param name="pollTime" type="iCL.Time" />
		
		this._buildings          = iCL.TArray.VAR(Kiosk.domain.Building)
		this._portfolioUtilities = Kiosk.domain.PortfolioUtilities.VAR;
		this._buildingStatistics = iCL.collections.Dictionary.VAR(Kiosk.domain.BuildingStatistics)
		this._bs                 = Kiosk.BuildingService.VAR;
		this.updated             = iCL.Event;		

		this.Cstr =
			function()
			{
				this._bs = Kiosk.BuildingService.instance;

				this.start()

				if (pollTime < Date.current) this._polling(true)
			}

		this._polling =
			function(initial)
			{
				if (!initial) return _rU(Kiosk.Landing.instance.reload())

				var cs = iCL.CallStack.NEW(this.updated.raise)

				var cc = function(f){return {success:f, failure:f}}

				this._bs.getBuildings()          .setOn = cc(cs.add(this._getBuildings_Callback))
				this._bs.getPortfolioUtilities() .setOn = cc(cs.add(this._getPortfolioUtilities_Callback))
				this._bs.getPortfolioStatistics().setOn = cc(cs.add(this._getBuildingStatistics_Callback))
				
				if (userInfo.isKioskUser) return;

				this.stop()
			}

		this._getBuildings_Callback =
			function(data, l)
			{
				if (data instanceof Error) return ((data instanceof iCL.AsyncRPCall.TimeoutError) ? (data.isLastAttempt ? undefined : _rU(l.fail = true)) : undefined)

				this._buildings = new iCL.TArray(Kiosk.domain.Building, data)
			}

		this._getPortfolioUtilities_Callback =
			function(data, l)
			{
				if (data instanceof Error) return ((data instanceof iCL.AsyncRPCall.TimeoutError) ? (data.isLastAttempt ? undefined : _rU(l.fail = true)) : undefined)

				this._portfolioUtilities = data;
			}

		this._getBuildingStatistics_Callback =
			function(data, l)
			{
				if (data instanceof Error) return (data.isLastAttempt ? undefined : _rU(l.fail = true))

				this._buildingStatistics = iCL.collections.Dictionary.NEW(Kiosk.domain.BuildingStatistics, data)
			}

		this.buildings =
			{
				get:function(){return this._buildings;}
			}

		this.portfolioUtilities =
			{
				get:function(){return this._portfolioUtilities;}
			}

		this.buildingStatistics =
			{
				get:function(){return this._buildingStatistics;}
			}
	}
)
