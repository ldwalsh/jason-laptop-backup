﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk,
	},

	function PresenterBase()
	{
		this.__view			 = $.ui.ViewBase.VAR;
		this._messageListner = $.MessageBus.MessageListener.VAR;

		this.Cstr =
			function()
			{
				this._messageListner = $.MessageBus.MessageListener.NEW(this._landing.mbus, this._receiveBusMsg)
			}

		this._wireToView =
			function()
			{
				Object.getDescriptors(this.__view).forEach //move to constructor?  //move to base?
				(
					function(d)
					{
						if (!(d.set && d.set.isDelegate)) return; //revise

						var n = d.name;

						var f = this["_view_" + n]

						if (!f)
						{
							f = this["_" + n.removeStart("_")] //revise: use dedicated ViewDelegate w/ special naming prefix!
						}
					
						if (!f) return;

						this.__view[n] = f;
			
					}.bind(this)
				)
			}

		this._receiveBusMsg = //remove?
			function()
			{
			}

		this._view_get =
			function()
			{
				return this.__view;
			}

		this._view_set =
			function(value)
			{
				this.__view = value;

				this._wireToView()
			}

		this._landing =
			{
				get:function(){return Kiosk.Landing.instance;}
			}
	}
)
