﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk,
		prototype:	$.ui.windows.DialogWindow,
		resources:	["resource/html/BuildingSelectorDialog.html", "resource/less/BuildingSelectorDialog.less",]
	},

	function BuildingSelectorDialog()
	{
		var $cstr = this.constructor;
		var $base = this.constructor.prototype;
		
		this._useId				= ("div" + $cstr.name) //move to base!
		this._reposition		= Function.empty; //overrides
		this.buildingSelected	= DELEGATE()

		this.Cstr =
			function()
			{
				this._contentElement = $cstr.type.definition.resources.html.cloneNode(true) //may not need to clone if .html was a getter!
				this._hasTitlebar	 = false;
			}

		this._showOverlay =
			function()
			{
				var f = this._overlayRemover = HTMLDivElement.createMask(Kiosk.Landing.instance._divKiosk)

				f.element.classList.add("SelectBuildingOverlay")
			}

		this._open =
			function()
			{
				document.body.appendChild(this._contentElement) //could move logic to base?

				$base._open.apply(this)

				this._windowDiv.style.top = ("calc(" + getComputedStyle(this._windowDiv).top + " + 100px" + ")") //revise?
				
				var lb = $.ui.controls.ListBox.CAST($.ui.ControlsGroup.NEW(this._contentElement, this).SET({render:null,}).getControlById("divBuildingSelector"))

				Kiosk.Landing.instance.dataManager.buildings.forEach
				(
					function(b)
					{
						lb.addItem
						(
							DocumentFragment.create([HTMLElement.create(HTMLSpanElement, {textContent:b.name,}), HTMLElement.create(HTMLSpanElement, {textContent:b.address,}),]), b.bid
						)
					}
				)
			}

		this._close =
			function()
			{
				$base._close.apply(this)

				this._contentElement.remove() //shoudl be handled by base?
			}

		this._divBuildingSelector_itemSelected =
			function(ea)
			{
				///<param name="ea" type="$.ui.controls.ListBox.ItemSelectedEventArgs" />
				
				this.close()

				this.buildingSelected(ea.item.value)
			}
	}
)
