﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk,
		prototype: $.ui.BindableControlBase,
		resources: ["resource/html/ExpanderControl.html", "resource/less/ExpanderControl.less",],
		statics:
		{
			Cstr:
			function()
			{
				var state = this._selectors.state;

				this._toggleSelectors = [state.collapsed, state.expanded]
			},
			_selectors:
			{
				content:	"Kiosk-Expander-Content",
				toggler:	"Kiosk-Expander-Toggler",				
				state:
				{
					notOverflowed:	"Kiosk-Expander-NotOverflowed",
					collapsed:		"Kiosk-Expander-Collapsed",
					expanded:		"Kiosk-Expander-Expanded",
				},
			},
			_toggleSelectors: Array.VAR,
			
			stateEnum:
			{
				notOverflowed:	0,
				collapsed:		1,
				expanded:		2,
			},
		}
	},

	function Expander(element) //implement DataBind "interface"
	{
		///<param name="element" type="HTMLDivElement" />

		var $cstr = this.constructor;
		
		this._divContent		= HTMLDivElement.VAR;
		this._divContentWrapper	= HTMLDivElement.VAR;
		this._divToggler		= HTMLDivElement.VAR;
		this.toggled			= $.Event;
		this.changed			= $.Event;

		this.render =
			function()
			{
				this._divContent		= this._element.getElementsByClassName($cstr._selectors.content)[0]
				this._divContentWrapper	= this._divContent.parentElement;
				this._divToggler		= this._element.getElementsByClassName($cstr._selectors.toggler)[0]

				this._divContent._dataBind = this.dataBind; //hacky! logic should be handled inside DataBinder?

				this._divToggler.event.attach.click(this._divToggler_Click) //revise?

				this._element.classList.add($cstr._selectors.state.notOverflowed)

				window.addResizeListener(this._divContentWrapper, this._reflow) //revise
			}

		this._reflow =
			function()
			{
				var _s  = $cstr._selectors.state; //use let instead when suppoorted
				var _sE = $cstr.stateEnum;				

				this._divContentWrapper.style.height = "100%"; //why? //revise

				if (this._isContentOverflow)
				{
					if (this.state == _sE.notOverflowed) this._element.classList.swap(_s.notOverflowed, _s.collapsed)
				}
				else
				{
					if ((this.state != _sE.expanded) && (!this.state == _sE.notOverflowed)) this._element.classList.swap(_s.collapsed, _s.notOverflowed)
				}

				this._divContentWrapper.style.height = ""; //revise
			}

		this.dataBind = //revise?
			function(data)
			{
				this.text = data;
			}

		this._divToggler_Click =
			function()
			{
				if (this.state == $cstr.stateEnum.notOverflowed) return;

				var args;

				if (this.isCollapsed)
				{
					args = $cstr._toggleSelectors;

					this._divToggler.classList.swap("arrows-down", "arrows-up") //revise
				}
				else
				{
					args = $cstr._toggleSelectors.concat().reverse()

					this._divToggler.classList.swap("arrows-up", "arrows-down") //revise
				}
				
				DOMTokenList.swap.invoke([this._element].concat(args), Function.invoke.byArray)

				this.toggled.raise({state:this.state,})
			}

		this.text =
			{
				get:function(){return this._divContent.textContent},
				set:function()
				{
					var s = arguments[0]

					this._divContent.textContent = s;

					this._element.style.display = (s ? "" : "none") //revise

					this._reflow()

					this.changed.raise()
				}
			}

		this._isContentOverflow =
			{
				get:function(){return (this._divContent.scrollHeight > this._divContentWrapper.clientHeight)}
			}

		this.state =
			{
				get:function()
				{
					var _s  = $cstr._selectors.state;
					var _cL = this._element.classList;
					var _sE	= $cstr.stateEnum;

					return (_cL.contains(_s.notOverflowed) ? _sE.notOverflowed : (this.isCollapsed ? _sE.collapsed : _sE.expanded))
				},
			}			

		this.isCollapsed =
			{
				get:function(){return this._element.classList.contains($cstr._selectors.state.collapsed)}
			}

		this.isExpanded =
			{
				get:function(){return this._element.classList.contains($cstr._selectors.state.expanded)}
			}
	}
)
