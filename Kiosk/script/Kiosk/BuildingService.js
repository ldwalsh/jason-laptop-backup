﻿///
///<reference path="../$Kiosk.js" />

Object.define
(
	{
		namespace: Kiosk,
		prototype: iCL.asp.ScriptServiceBase,
		statics:
		{
			instance_:
			function()
			{
				this.__instance = (this.__instance || this.NEW(document.baseUrl.endWith("/") + "Service/BuildingService.asmx"))

				return this.__instance;
			},
		},
	},

	function BuildingService(endpoint)
	{
		this.Cstr =
			function()
			{
				this._defineWebMethod("GetBuildings", null, {timeout:600,})
				this._defineWebMethod("GetPortfolioUtilities", null, {timeout:600,})
				this._defineWebMethod("GetPortfolioStatistics", null, {timeout:600,})
				this._defineWebMethod("GetPortfolioHourlyUtilities", null, {timeout:18000,}) //18000
				this._defineWebMethod("GetPortfolioHourlyUtilitiesByBuilding", ["bid"], {timeout:600,}) //,"hour"], /*headers:{"cache-control":"private"},*/})
			}

		this.getBuildings =
			function()
			{
				return this._methods.GetBuildings.invoke()
			}

		this.getPortfolioUtilities =
			function()
			{
				return this._methods.GetPortfolioUtilities.invoke()
			}

		this.getPortfolioStatistics =
			function()
			{
				return this._methods.GetPortfolioStatistics.invoke()
			}

		this.getPortfolioHourlyUtilities =
			function()
			{
				return this._methods.GetPortfolioHourlyUtilities.invoke()
			}

		this.getPortfolioHourlyUtilitiesByBuilding =
			function(bid)
			{
				return this._methods.GetPortfolioHourlyUtilitiesByBuilding.invoke(bid) //, Date.current.hours)
			}
	}
)
