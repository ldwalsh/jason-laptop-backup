﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk,
		resources:	["resource/html/HelpWidget.html",]
	},

	function HelpWidget(container, elements)
	{
		///<param name="elements" type="Array" elementType="HTMLElement" />
		///<param name="container" type="HTMLElement" />
				
		var $cstr = this.constructor;

		this._container	= HTMLElement.VAR;
		this._elements	= $.TArray.VAR(HTMLElement)
		this._imgs		= $.TArray.VAR(HTMLImageElement)
		this.isEnabled	= false;
		
		this.Cstr =
			function()
			{
				this._imgs = new $.TArray(HTMLImageElement)
			}

		this._highlightElement =
			function(element)
			{
				///<param name="element" type="HTMLElement" />

				//
				if (window.intellisense) return HTMLDivElement.VAR;
				//

				var referenceDiv = HTMLElement.create(HTMLDivElement, {parentNode:document.body, style:{position:"absolute",}.SET(element.getBoundingClientRect().getPlacement(), true),})

				return HTMLElement.create(HTMLDivElement, {parentNode:HTMLElement.create(HTMLDivElement, {parentNode:referenceDiv, classList:{add:"ODIV"},}), classList:{add:"IDIV"},})
			}

		this._body_click =
			function(event)
			{
				///<param name="event" type="Event" />
				
				var e = event.target;
				
				if (e.ownerElement)
				{
					var helpSrc = e.ownerElement.getData("help-widget--url")

					if (!helpSrc) return;
								
					Kiosk.HelpDialog.NEW(this).open(helpSrc) //reuse object
				}
				else this.disable()

				this._togglePointerCapture(false)
			}

		this._togglePointerCapture = //rename?
			function(add)
			{
				var b = document.body;
				
				Function.invoke((add ? b.addEventListener : b.removeEventListener).bind(b), ["click", this._body_click, true])
			}

		this.disable =
			function()
			{
				this._imgs.forEach(function(img){rU(Function.callEach(HTMLElement.prototype.remove, [img.targetRegion, img]))})

				this._container.toggleEnabled()

				this.Cstr() //revise

				this.isEnabled = false;
			}

		this.enable =
			function()
			{
				var div = this._container.toggleEnabled(false).SET({style:{opacity:0},})

				this._elements.forEach
				(
					function(e)
					{
						var div = this._highlightElement(e)
						
						var innerClass = e.getData("help-widget--inner-class")

						if (innerClass)
						{
							div.SET({classList:{add:[innerClass,],},})
						}

						var rect = e.getBoundingClientRect()

						var img = HTMLElement.create(HTMLImageElement, {src:"resource/image/help.png",})
						div.parentNode.insertBefore(img, div)
						
						this._imgs.push(img)

						//var hp = ((rect.left > (document.documentElement.clientWidth  - rect.right))  ? "left" : "right" )
						//var vp = ((rect.top  > (document.documentElement.clientHeight - rect.bottom)) ? "top"  : "bottom")

						//if (hp == "left")
						//{
						//	img.style.SET({left:(rect.left - (img.offsetWidth * .75)), top:(rect.top - (img.offsetHeight * .75)),})
						//}
						//else
						//{
						//	img.style.SET({left:(rect.left + rect.width - (img.offsetHeight * .25)), top:(rect.top - (img.offsetHeight * .75)),})
						//}
						
						img.ownerElement = e; //revise
						img.targetRegion = div.parentNode.parentNode; //revise
					
					}.bind(this)
				)

				this._togglePointerCapture(true)
				//document.body.addEventListener("click", this._body_click, true)
				//div.addEventListener("click", this._body_click, true)
				
				this.isEnabled = true;
			}
	}
)
