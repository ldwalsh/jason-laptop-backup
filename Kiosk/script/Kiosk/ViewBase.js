﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk,
		prototype: $.ui.ViewBase2, //should be called Portlet? and inherit from View?
	},

	function ViewBase()
	{
		this._headerDiv = HTMLDivElement.VAR;

		this._render =
			function()
			{
				this._headerDiv = this._div.parentElement.parentElement.getElementsByClassName("PortletHeader")[0]

				this.portletTitle = this._headerDiv.textContent;

				//this._adjustBodyForHeader() //revise?
				addResizeListener(this._headerDiv, this._adjustBodyForHeader.executeAndReturn())
			}

		this._adjustBodyForHeader =
			function()
			{
				this._div.parentNode.style.height = ("calc(100% - " + this._headerDiv.offsetHeight + "px" + ")") //move out to element resize handler
			}

		this._findElement =	//move to base?
			function(dataId)
			{
				//
				if (window.intellisense) return HTMLElement.VAR;
				//

				var e = this._div.getElmntById(dataId)

				if (e) return e;

				throw new Error(343434787277)
			}

		this._kioskDiv =
			{
				get:function(){return $gId("divKiosk")}
			}

		this.portletTitle =
			{
				get:function(){return this._headerDiv.textContent;},
				set:function(){this._headerDiv.textContent = arguments[0];}
			}
	}
)