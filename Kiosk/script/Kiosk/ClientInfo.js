﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk,
	},

	function ClientInfo()
	{
		this.defaultBID		= Number.VAR;
		this.headerImgSrc	= String.VAR;
		this.clientImgSrc	= String.VAR;
		this.kioskContent	= String.VAR;
		this.kioskQRContent	= String.VAR;
		this.kioskQRLink	= String.VAR;
		this.chartColors	= $.TArray.VAR(String)
	}
)
