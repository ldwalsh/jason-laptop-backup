﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk.views,
		prototype:	Kiosk.ViewBase,
		resources:
		[
			{
			    url: "https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1", //&s=1 only needed for ssl
				type:	"ecma",
				check:	function(){return !!(window.Microsoft && Microsoft.Maps && (Microsoft.Maps.Map && Microsoft.Maps.MapTypeId))}
			},
			"resource/html/views/MapView.html",
			"resource/less/views/MapView.less",
		],
		contracts:
		{
			_IEntity:			{bid:Number, pixelLeft:Number, pixelTop:Number,},
			_IMappedBuilding:	{anchor:HTMLAnchorElement, location:Object,},
			IBuildingInfo:		{bid:Number, name:String, street:String, city:String, state:String, coordinates:Kiosk.BuildingCoordinatesLookup.Coordinates, icon:String,
								 totalPerfColor:Number,},
		},
	},

	function MapView()
	{
		var $cstr = this.constructor;
		var $base = this.constructor.prototype;

		this._bingMap			= Object.VAR;
		this._buildingSelector	= $.ui.controls.ListBox.VAR;
		this._fullLocRect		= Object.VAR; //?
		this._currLocRect		= Object.VAR; //?
		this._entityLookup		= $.Lookup.VAR(HTMLAnchorElement)
		this.mapReady			= DELEGATE()
		this.buildingSelected	= DELEGATE("id")		

		this.Cstr =
			function()
			{
				var divMap = $gId("divMap")

				this._bingMap =
					new Microsoft.Maps.Map
					(
						divMap, 
						{
							credentials:			Kiosk.BingMapsHelper.key,
							mapTypeId:				Microsoft.Maps.MapTypeId.road,
							disablePanning:			true,
							disableZooming:			true,
							enableClickableLogo:	false,
							enableSearchLogo:		false,
							showDashboard:			false,
							showMapTypeSelector:	false,
							showCopyright:			false,
							showScalebar:			false,
						}
					)
			}

		this._getContainerDiv =
			function()
			{
				return this._kioskDiv.getElmntById("div" + this.constructor.type.name).SET({appendChild:[this.constructor.type.definition.resources.html]})
			}
		
		this._render =
			function()
			{
				$base._render.apply(this)

				$.ui.binders.ResizeBinder.NEW(divMap, divMap.parentNode)
			}

		this._divChooseBuilding_click =
			function(ea)
			{
				///<param name="ea" type="$.ui.controls.Button.ClickEventArgs" />
				
				this._buildingSelector.visible = true;
			}

		this._divBuildingSelector_itemSelected =
			function(ea)
			{
				///<param name="ea" type="$.ui.controls.ListBox.ItemSelectedEventArgs" />
				
				this._buildingSelector.visible = false;

				if (this._bingMap.getZoom() == 18) this._setMapZoom(this._fullLocRect, 15, this._setMapView.getArgsCaller([ea.item.value]))
				else this._setMapView(ea.item.value)
			}

		this._setMapZoom =
			function(rect, zoomLevel, callback)
			{
				///<param name="rect" type="Object" />
				///<param name="zoomLevel" type="Number" />
				///<param name="callback" type="Function" />
		
				this._currLocRect = rect;
				
				var o = {zoom:(zoomLevel || (rect.center ? 15 : 18)),}
				
				o[rect.center ? "bounds" : "center"] = rect;

				if (callback)
				{					
					var callList  = Function.makeCallListObject() //revise name!
					var handlerId = Microsoft.Maps.Events.addHandler(this._bingMap, "viewchangeend", callList)
					
					callList.addFunc([Microsoft.Maps.Events.removeHandler.getArgsCaller([handlerId]), callback])
				}

				this._findElement("imgZoom").toggleVisibility((o.zoom != 15), true)

				this._bingMap.setView(o)
			}

		this._setMapView =
			function(bid, isDrillDown)
			{
				///<param name="bid" type="Number" />
				///<param name="isDrillDown" type="Boolean" optional="true" />

				if (this._bingMap.getZoom() == 15)
				{
					if (Kiosk.BingMapsHelper.computeIsCrowded(this._bingMap, this._entityLookup[bid].anchor, 25))
						return rU(this._setMapZoom(this._entityLookup[bid].location, 18, this._setMapView.getArgsCaller([bid, true])))
				}
				else
				{
					if (!isDrillDown)
					{
						if (!Kiosk.BingMapsHelper.withinMapViewBounds(this._bingMap.getBounds(), this._entityLookup[bid].location))
							return rU(this._setMapZoom(this._fullLocRect, 15, this._setMapView.getArgsCaller([bid])))

						this._setMapZoom(this._entityLookup[bid].location, 18)
					}
				}

				this.buildingSelected(bid)
			}

		this.setMapView =
			function(bid)
			{
				this._setMapView(bid)
			}

		this._imgZoom_click =
			function()
			{
				this._setMapZoom(this._fullLocRect, 15)

				this._findElement("imgZoom").toggleVisibility(false)
			}

		this.addBuildings =
			function(buildingInfos)
			{
				///<param name="buildingInfos" type="Array" elementType="Kiosk.views.MapView.IBuildingInfo" />
				
				if (!this.isRendered) return rU(this.rendered.attach(this.addBuildings.getArgsCaller(Array.fromArrayLike(arguments)))) //need fromArrayLike here? nah
				
				var el = $.ExecutionList.NEW()
				
				buildingInfos.forEach //this should all be moved out to presenter
				(
					function(bi)
					{
						if (bi.coordinates) return;
						
						el.add
						(
							function(item)
							{
								///<param name="item" type="$.ExecutionList.ExecItem" />
								
								Kiosk.BingMapsHelper.geocodeFunc(bi, item.doContinue)
							}
						)
					}
				)

				this._entityLookup = $.Lookup.NEW($cstr.IBuildingInfo)

				el.execute
				(
					function()
					{
						var locations = []
						var div		  = HTMLElement.create(HTMLDivElement, {className:"BuildingIcon",})

						buildingInfos.forEach
						(
							function(bi)
							{					
								var loc = new Microsoft.Maps.Location().SET({latitude:bi.coordinates.latitude, longitude:bi.coordinates.longitude,})

								var pin =
									new Microsoft.Maps.Pushpin
									(
										loc,
										{
											htmlContent: div.clone().SET({classList:{add:["PerfRating-" + bi.totalPerfColor]}, setAttribute:["data-bid", bi.bid,]}).outerHTML,
											icon:		 "",
											height:		 "39",  //revise these dimensional hardcodings
											width:		 "32",	//revise these dimensional hardcodings
										}
									)

								Microsoft.Maps.Events.addHandler
								(
									pin, "click", function(event, isDrillDown){this._setMapView(parseInt(event.target.cm1002_er_etr.dom.children[0].getData("bid")))}.bind(this)
								)
								
								this._bingMap.entities.push(pin)

								this._entityLookup.add(bi.bid, {anchor:pin.cm1002_er_etr.dom, location:pin.getLocation(),})

								locations.push(loc)

								this._buildingSelector.addItem
								(
									DocumentFragment.create
									(
										[HTMLElement.create(HTMLSpanElement, {textContent:bi.name,}), HTMLElement.create(HTMLSpanElement, {textContent:bi.street,}),]
									),
									bi.bid
								)
								
							}.bind(this)
						)

						this._currLocRect = this._fullLocRect = Microsoft.Maps.LocationRect.fromLocations(locations)

						this._setMapZoom
						(
							this._fullLocRect,
							15,
							function()
							{
								$.ui.WindowResizeManager.instance.appendCallback(this._setMapZoom.getArgsCaller([this._currLocRect])) //revise?!

								this.mapReady()
							
							}.bind(this)
						)
					
					}.bind(this)
				)
			}

		this.highlightBuilding =
			function(bid)
			{
				///<param name="bid" type="Number" />
				
				var entities = this._bingMap.entities;
				var length	 = entities.getLength()
				var selected = null;
				
				for (var i=0; i<length; i++)
				{
					var a = HTMLAnchorElement.CAST(entities.get(i).cm1002_er_etr.dom)
					var e = a.children[0]
					
					a.classList[((e.getData("bid") == bid) ? a.classList.add : a.classList.remove).name]("Selected") //use .toggle here when supported
				}

				this._buildingSelector.selectedValue = bid;
			}
	}
)


//var M = {}


//M.Resource =
//iface
//(
//	{}
//)


//M.ResourceSet =
//iface
//(
//	{
//		estimatedTotal:	Number,
//		resources:		"$.TArray.VAR(M.Location)"
//	}
//)

//M.Response =
//iface
//(
//	{
//		statusCode:					Number,
//		statusDescription:			String,
//		authenticationResultCode:	String,
//		traceId:					String,
//		copyright:					String,
//		brandLogoUri:				String,
//		resourceSets:				"$.TArray.VAR(M.ResourceSet)",
//		errorDetails:				"$.TArray.VAR(String)",
//	}
//)



//M.Address =
//iface
//(
//	{
//		addressLine:		String,
//		locality:			String,
//		neighborhood:		String,
//		adminDistrict:		String,
//		adminDistrict2:		String,
//		formattedAddress:	String,
//		postalCode:			String,
//		countryRegion:		String,
//		landMark:			String,
//	}
//)


//M.Point = //implements shape
//iface
//(
//	{
//		type:				String,
//		point:				"$.TArray.VAR(Number)",
//		calculationMethod:	String,
//		usageTypes:			"$.TArray.VAR(String)",
//	}
//)



//M.Location =  //implements Resource
//iface
//(
//	{
//		name:				String,
//		point:				M.Point,
//		bbox:				Object,
//		entityType:			Object,
//		address:			M.Address,
//		confidence:			String,		//?
//		matchCodes:			String,		//?
//		queryParseValues:	Object,
//		geocodePoints:		"$.TArray.VAR(M.Point)",
//	}
//)


//
if (window.intellisense)
{
	window.Microsoft = {Maps:{Events:{addHandler:function(entity, eventName, func){func();},},},}
}
//