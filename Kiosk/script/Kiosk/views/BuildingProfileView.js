﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk.views,
		prototype:	Kiosk.ViewBase,
		resources:	["resource/html/views/BuildingProfileView.html?r=1", "resource/less/views/BuildingProfileView.less",],
	},

	function BuildingProfileView()
	{
		var $base = this.constructor.prototype;

		this._image			= $.ui.controls.Image.VAR;
		this._data			= Object.VAR;
		this._chart			= Object.VAR;
		this._t				= null;	//rename
		this.dataSelected	= DELEGATE()

		this._getContainerDiv =
			function()
			{
				return this._kioskDiv.getElmntById("div" + this.constructor.type.name).SET({appendChild:[this.constructor.type.definition.resources.html]})
			}

		this._render =
			function()
			{
				//encapsulte in control 'T'
				this._t	= HTMLElement.CAST(document.querySelector("[data-control-typ='T']"))
				var y	= HTMLElement.CAST(this._div.querySelector("div.Content"))
				var f	= function(){this._viewResize({contentElement:y,})}.bind(this)
				//

				addResizeListener(this._div, f) //move to base and enable via property
				addResizeListener(this._t, this._viewResize) //revise?

				$base._render.apply(this)

				var expander = Kiosk.Expander.CAST(this._controlsGroup.getControlById("divExpander"))
				
				expander.toggled.attach(f.getNoArgsCaller()) //revise
				expander.changed.attach(f.getNoArgsCaller()) //revise

				this._image = $.ui.controls.Image.CAST(this._controlsGroup.getControlById("imgBuilding")) //should be handled by base

				var divChartContainer = this._findElement("divChartContainer") //revise
				jQuery(divChartContainer).bind("plotclick", function(){if (arguments[2]) this.dataSelected(arguments[2].series.label)}.bind(this))
			}

		this._viewResize = //move to base? and enable via property?
			function(ea)
			{
				var e = this._t.children[0]
				var s = e.style;
				var h = parseInt(s.height)
				var v = (this._div.offsetHeight - e.nextElementSibling.offsetHeight - 10)

				if (h == v) return;

				s.height = v+"px";

				//var y = ea.contentElement; //revise
				//y.classList.toggle("HiddenAbsolutely", !((y.offsetHeight > 100) && (y.offsetWidth > 200))) //hook into reflow event

				if (this._data) this.setBuildingUtilityUsage(this._data);
			}
		
		this.bindFields =
			function(binderObj)
			{
				///<param name="binderObj" type="Object" />

				$.ui.DataBinder.NEW(this._div).bind
				(
					binderObj.SET
					(
						{
							websiteLink: (binderObj.websiteLink || "javascript:"),
							bindClass:
							function(value, cell)
							{
								cell.getParent(HTMLTableRowElement).toggleDisplay(!!value)

								return (value + ":")
							},
						},
						true /// TODO: remove not, should not be required
					)				
				)
			}

		this.loadImage =
			function(url)
			{
				this._image.url = url;
			}

		this.setBuildingUtilityUsage =
			function(data) //what is data type!!!!???
			{
				this._data = data;

				var d = []

				data.forEach(function(o){return rU(d.push({label:o, data:o.value, color:o.color}))})

				var div = HTMLElement.create(HTMLDivElement, {style:{fontSize:"1em", textAlign:"center", padding:"1px", color:"white",},})
				
				this._chart =
				jQuery.plot
				(
					this._findElement("divChartContainer"),
					d,
					{
						series:
						{
							pie:
							{
								show:		true,
								radius:		0.7,
								tilt:		0.75,
								combine:	{color:'#999', threshold:0.1,},
								label:
								{
									show:		true,
									radius:		0.7,
									formatter:	function(labelObj, series){return div.SET({textContent:labelObj.label||labelObj,}).outerHTML;},
									background:	{opacity:0.5, color:"#000"}
								},
							},
						},
						grid:	{clickable:true,},
						legend:	{show:false,}
					}
				)
			}
	}
)
