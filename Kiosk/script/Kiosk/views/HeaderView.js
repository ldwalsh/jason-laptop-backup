﻿///<reference path="../../$Kiosk.js" />

Object.define
(
	{
		namespace:	Kiosk.views,
		prototype:	Kiosk.ViewBase,
		resources:	["resource/html/views/HeaderView.html", "resource/less/views/HeaderView.less",],
	},

	function HeaderView()
	{
        var $ = iCL;

        this._helpWidget = Kiosk.HelpWidget.VAR;
		
		this._getContainerDiv =
			function()
			{
				return Kiosk.Landing.instance._divKiosk.getElmntById("divHeader").SET({appendChild:[HeaderView.type.definition.resources.html]})
			}

		this._imgHelp_click =
			function()
			{
				this._helpWidget = (this._helpWidget || Kiosk.Landing.instance.helperWidget) //since not loaded during Cstr

				this._helpWidget.enable()
			}

		this._portletResize =
			function(init)
			{
				if (!this._helpWidget || !this._helpWidget.isEnabled) return;

				this._helpWidget.disable()
			}

		this._createQRCode =
			function(value, element)
			{
				///<param name="value" type="String" />
				///<param name="element" type="HTMLElement" />
				
				if (value)
				{
					var size = (this._div.offsetHeight + 20)

					return ("url(\"http://chart.googleapis.com/chart?cht=qr&chs=" + size + "x" + size + "&chld=M&chl=" + value + "\")")
				}

				element.toggleDisplay(false)
			}

		this._render =
			function()
			{
                var ui = $.ui;

				ui.DataBinder.NEW(this._div).bind(Kiosk.Landing.instance.clientInfo.toCopy().SET({createQRCode:this._createQRCode,}, true)) //revise to be set by presenter

				ui.WindowResizeManager.instance.appendCallback(this._portletResize, true) //move this to ViewBase? or PortletBase?
			}
	}
)




Object.define
(
	{
		namespace: iCL.ui,
		statics:   {bindingAttributeName:"binding",},
		contracts: {ITest:{field:String, defaultValue:String, func:String, format:String},},
	},

	function DataBinder(container)
	{
		/// <param name="container" type="HTMLElemengt" />

		this._container = HTMLElement.VAR;

		this._parseDataBindingAttribute =
			function(binding)
			{
				///<param name="element" type="HTMLElement" />

				//
				if (window.intellisense) return DataBinder.ITest.VAR;
				//
				
				if (!binding) return {};
				
				if (!~binding.indexOf(":")) return {field:binding}

				return Object.parse(binding)
			}

		this._populateElement =
			function(element, value, binding) //combine with .bind
			{
				///<param name="element" type="HTMLElement" />

				if (element._dataBind) return rU(element._dataBind(value))

                var p = binding.property;
                
                if (p)
                {
                    Object.setProperty(element, p, value)
                }
                else
                {
                    element[SELECT(element.constructor, [[HTMLAnchorElement, "href"], [HTMLImageElement, "src"], [SELECT.default, "textContent"]])] = value;
                }
			}

		this.bind =
			function(binderObj, owner) //remove owner (no need)
			{
				this._container.getElements().forEach
				(
					function(e)
					{
						var binding = this._parseDataBindingAttribute(e.getData(DataBinder.bindingAttributeName))
						
						if (!binding.field) return;

						var value = binderObj[binding.field]

						value = ((value == null || (value.length == 0)) ? ((binding.defaultValue == null) ? null : binding.defaultValue) : value)
                        
						binding.func = //revise!!!
						Function.invoke
						(
							function()
							{
								if (!(e instanceof HTMLTableCellElement) || binding.func) return binding.func;

								var cols = e.getParent(HTMLTableElement).getElements(HTMLTableColElement)

								var col = cols[e.parentIndex]

								if (!col) return;

								var b = col.getData("binding")

								if (!b) return;

								var o = Object.parse(b)

								return (o.func || undefined)
							}
						)

						if (binding.func)
						{
							var func = (owner ? owner[binding.func] : binderObj[binding.func])
                            
                            if (!(func instanceof Function)) throw new Error(9363738223)

							value = func(value, e, binding)
						}
						//else if (binding.format)
						//{
						//    value = binding.format.replace("{0}", value)
						//}

						this._populateElement(e, value, binding)
						
					}.bind(this)
				)
			}
	}
)


Object.setProperty =
function(obj, x, value)
{
    var arr = x.split(".")

    for (var i=0; i<arr.length-1; i++)
    {
        obj = obj[arr[i]]
    }

    obj[arr[i]] = value;
}
