﻿///<reference path="../../$Kiosk.js" />

Object.define
(
	{
		namespace: Kiosk.views,
		prototype: Kiosk.ViewBaseWithChart,
		resources: ["resource/html/views/BuildingUtilitiesView.html", "resource/less/views/BuildingUtilitiesView.less",],
		contracts: {ISummaryData:{currentUse:Number, totalUsage:Number, peakUse:Number, peakTime:Number,}, IPerformanceData:{percent:Number, rating:Number,},},
		construct:
		{
			_globeColorLookup: ["neutral_grey", "dark_green_down", "light_green_down", "yellow_up", "red_up"],
			_arrowColorLookup: ["grey", "darkgreen", "lightgreen", "yellow", "red"],
		},
	},

	function BuildingUtilitiesView()
	{
        var $ = iCL;
		
		this._chartContainerName    = "buildingUtilitiesChartContainer";
		this._comparisonLineVisible = true;
		this._divChartArea          = HTMLDivElement.VAR;
		this._divButtonBarArea      = HTMLDivElement.VAR;
		this._fullButtonMode        = Boolean.VAR;
		this._fullButtonModeWidth   = Number.VAR;
		this.utilitySelected        = DELEGATE({utility:String,})

		this._buttonBar_onButtonClick =
			function(ea)
			{
				///<param name="ea" type="$.ui.controls.ButtonBar.ButtonClickArgs" />
				
				this.utilitySelected(ea.button.command)
			}

		this._lnkHideComparison_click =
			function(dE)
			{
				///<param name="dE" type="$.DomEvent" />
				
				this._comparisonLineVisible = !this._comparisonLineVisible;
				
				dE.target.textContent = ((this._comparisonLineVisible ? "Hide" : "Show") + " Comparison")

				this._repaintChart()
			}

		this._repaintChart =
			function()
			{
				var options = this._chartControl._chartObject.options;

				options.series[1].visible = this._comparisonLineVisible;

				options.transitions = true;
				
				this._chartControl.repaint()

				options.transitions = false;
			}

		this._portletResize =
			function()
			{
				var ul = this._divButtonBarArea.children[0]

				this._fullButtonModeWidth = ifFalsy(this._fullButtonModeWidth, ul.offsetWidth)

				var isCompact = (this._divButtonBarArea.pixelWidth < this._fullButtonModeWidth)

				if (isCompact == this._fullButtonMode) return;
					
				this._fullButtonMode = isCompact;

				exec(isCompact ? ul.classList.add : ul.classList.remove).bind(ul.classList)("Compact") //revise?

				ul.children.forEach(function(_){return _rU(_.title = (isCompact ? _.getElements(HTMLDivElement).item(1).textContent : ""))})

				this._divButtonBarArea.parentNode.children[0].style.bottom = (this._divButtonBarArea.offsetHeight + "px"); //revise

				this._adjustChartToFitContainer()
			}

		this._render =
			function()
			{
				BuildingUtilitiesView.prototype._render.apply(this)

				this._divChartArea = this._div.getElmntById("divChartArea")
				this._divButtonBarArea = this._findElement("divButtonBarArea") //revise to use ViewBase build-in capability
			}

		this.selectUtility =
			function(utility)
			{
				this._controlsGroup.getControlByElement(this._divButtonBarArea.children[0]).getButtonByCommand(utility)._elementClick()
			}
		
		this.setHourlyData =
			function(data)
			{
				this._chartControl.set_dataSource(data[0])

				this._chartControl.repaint() //required only on very first rendering so could move out?

				var d = this._chartControl._chartObject._sourceSeries[1].data;

				for (var i=0; i<24; i++)
				{
					d[i].value = data[1][i]
				}

				//Turning animation on before repainting the chart
				//RadHtmlChart1.set_transitions(true);

				this._repaintChart()
			}

		this.toggleLoadingNotifier =
			function(isLoading)
			{
				///<param name="isLoading" type="Boolean" />
				
				var div = this._div.toggleEnabled(!isLoading);

				if (!isLoading) return;

				div.classList.add("Loading")
			}

		this.toggleEnabled =
			function(isEnabled, div)
			{
				this._div.toggleEnabled(isEnabled)
			}

		this.setSummaryData =
			function(summary)
			{
				///<param name="summary" type="Kiosk.views.BuildingUtilitiesView.ISummaryData" />
				
				$.ui.DataBinder.NEW(this._div.getElmntById("tblSummary")).bind(summary, {format:function(value){return value.localeFormat("n0")},}) //revise
			}

		this.setPerformanceData = 
			function(performance)
			{
				///<param name="performance" type="Kiosk.views.BuildingUtilitiesView.IPerformanceData" />
				
				var imgArrow = this._findElement("imgArrow") //revise
				imgArrow.src = $.Url.NEW({pathname:imgArrow.src,}).SET({fileName:(BuildingUtilitiesView._globeColorLookup[performance.rating] + ".png"),})
				
				var imgEarth = HTMLImageElement.CAST(this._findElement("imgEarth"))	//revise
				imgEarth.src = $.Url.NEW({pathname:imgEarth.src,}).SET({fileName:("earth_" + BuildingUtilitiesView._arrowColorLookup[performance.rating] + ".png"),})

				var pp = performance.percent;

				this._findElement("divPercent").textContent = (isNum(pp) ? (pp.toNumeric().multiply(100)[(pp > 0) ? "ceil" : "floor"](.1) + "%") : pp)
			}
	}
)
