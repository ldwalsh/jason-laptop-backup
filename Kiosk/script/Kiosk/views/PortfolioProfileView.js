﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk.views,
		prototype:	Kiosk.ViewBase,
		resources:	["resource/html/views/PortfolioProfileView.html", "resource/less/views/PortfolioProfileView.less",],
	},

	function PortfolioProfileView()
	{
		var $base = this.constructor.prototype;
		
		this._data				= Object.VAR;
		this.displayByChanged	= DELEGATE()
		this.dataSelected		= DELEGATE()

		this._getContainerDiv =
			function()
			{
				return this._kioskDiv.getElmntById("div" + this.constructor.type.name).SET({appendChild:[this.constructor.type.definition.resources.html]})
			}

		this._ulBy_onButtonClick =
			function(args)
			{
				///<param name="args" type="$.ui.controls.ButtonBar.ButtonClickArgs" />

				this.displayByChanged
				(
					Kiosk.presenters.PortfolioProfilePresenter.DisplayByEnum[String.toCamelCase(args.button.element.textContent.split("By").item(1).removeWhiteSpace())]
				)
			}

		this._portletResize =
			function()
			{
				if (!this._data) return;

				this.setPortfolioData(this._data)
			}

		this._render =
			function()
			{
				$base._render.apply(this)
				
				var divChartContainer = this._findElement("divChartContainer") //revise

				jQuery(divChartContainer).bind("plotclick", function(){if (arguments[2]) this.dataSelected(arguments[2].series.label)}.bind(this))

				$.ui.WindowResizeManager.instance.appendCallback(this._portletResize, true) //move this to ViewBase? or PortletBase?
			}

		this.setPortfolioData =
			function(data)
			{
				this._data = data;
				
				var d = []

				data.forEach
				(
					function(o)
					{
						d.push({label:o, data:o.value, color:o.color})
					}
				)

				var innerRadius = 0.6;
				var outerRadius = 0.9;
				var fontEm		= 0.8;

				if (d.length < 10) //revise and use a mapping approach
				{
					if (d.length < 5)
					{
						innerRadius += 0.2;
						fontEm		+= 0.2;
					}
					else
					{
						innerRadius += 0.1;
						fontEm		+= 0.1;
					}
				}

				var div = HTMLElement.create(HTMLDivElement, {style:{fontSize:(fontEm + "em"), textAlign:"center", padding:"1px", color:"white",},})

				var divChartContainer = this._findElement("divChartContainer") //revise
				
				jQuery.plot
				(
					divChartContainer,
					d,
					{
						series:
						{
							pie:
							{
								show:	 true,
								radius:  innerRadius,
								tilt:	 0.75,
								combine: {color:'#999', threshold:0.025,},
								label:
								{
									show:		true,
									radius:		outerRadius,
									formatter:	function(labelObj, series){return div.SET({textContent:(labelObj.label||labelObj),}).outerHTML;},
									background:	{opacity:0.5, color:"#000"},
								},
							},
						},
						grid:	{clickable:true,},
						legend:	{show:false,},
					}
				)
			}
	}
)
