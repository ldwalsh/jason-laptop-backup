﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk,
		prototype: Kiosk.ViewBase,
	},

	function ViewBaseWithChart()
	{
		var $base = this.constructor.prototype;

		this._chartContainerName	= String.VAR;
		this._chartContainer		= HTMLDivElement.VAR;
		this._chart					= HTMLElement.VAR;
		this._chartControl			= Object.VAR;

		this.Cstr =
			function()
			{
				this._chart = this._kioskDiv.children.single({dataName:this._chartContainerName,}).children[0]
				
				this._chartContainer = this._div.getElmntById("divChartContainer").SET({appendChild:[this._chart,],})

				this._chartControl = this._chart.control; //remove?
			}

		this._getContainerDiv =
			function()
			{
				return this._kioskDiv.getElmntById("div" + this.constructor.type.name).SET({appendChild:[this.constructor.type.definition.resources.html]})
			}

		this._adjustChartToFitContainer =
			function()
			{
				var c = this._chartContainer;

				this._chart.pixelWidth	= (c.pixelWidth  - (c.children[0].offsetLeft * 2))
				this._chart.pixelHeight = (c.pixelHeight - (c.children[0].offsetTop  * 2))

				this._chart.control.repaint()
			}

		this._portletResize = //overwrite... change to viewBaseResize?
			function()
			{
			}

		this._render =
			function()
			{
				$base._render.apply(this)

				$.ui.WindowResizeManager.instance.appendCallback(this._adjustChartToFitContainer, true) //leave this here since specific to this base

				$.ui.WindowResizeManager.instance.appendCallback(this._portletResize, true) //move this to ViewBase? or PortletBase?
			}
	}
)
