﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk.presenters,
		prototype:	Kiosk.PresenterBase,
		statics:
		{
			_rangeArray:		Number.createRange(0, 23),
			_utilityNameLookup:	{12:"Electric", 51:"Steam", 49:"Gas", 11:"ChilledWater", 10:"HotWater", 50:"Water",},
		},
	},

	function BuildingUtilitiesPresenter()
	{
		var $cstr = this.constructor;

		this._view						= Kiosk.views.BuildingUtilitiesView.VAR;
		this._currentBuilding			= Kiosk.domain.Building.VAR;
		this._currentUtility			= String.VAR;
		this._udlStrategy				= Kiosk.utilityDataLoadStrategies.UDLStrategyBase.VAR; //use Interface instead since no need for base
		
		this.Cstr =
			function()
			{
				this._view = Kiosk.views.BuildingUtilitiesView.NEW().SET({utilitySelected:this._selectUtility,})

				this._messageListner.addSubscription("buildingSelected", this._msg_buildingSelected)

				//

				this._udlStrategy = (function(_){return (userInfo.isKioskUser ? _.KioskUserUDLStrategy : _.PortlUserUDLStrategy)}(Kiosk.utilityDataLoadStrategies)).NEW()

				//
				//below MessageBox use should be handled by view

				var closer = REF;

				this._udlStrategy.onSuccess.attach(Function.ifCall.getArgsCaller([closer]))
				this._udlStrategy.onFailure.attach(function(msg){return _rU($.ui.windows.MessageBox.open("ERROR: " + msg))}.bind(this))
				this._udlStrategy.onTimeout.attach(function(msg){return _rU(closer.value = closer.value || $.ui.windows.MessageBox.open(msg, {callback:function(){closer.value = null;}}))})
			}

		this._msg_buildingSelected =
			function(building)
			{
				///<param name="building" type="Kiosk.domain.Building" />

				if (this._currentBuilding == building) return;

				this._view.SET({portletTitle:(this._view.portletTitle.split("-")[0] + " - " + building.name), toggleLoadingNotifier:[true],})

				this._currentBuilding = building;

				this._udlStrategy.getBuildingData
				(
					building.bid,
					function(data, bid)
					{
						///<param name="data" type="$.collections.Dictionary" />

						if (this._currentBuilding.bid != bid) return;

						this._view.toggleLoadingNotifier(false)

						if (!data.count) return _rU(this._view.toggleEnabled(false))

						this.selectUtility()
					
					}.bind(this)
				)
			}

		this._selectUtility =
			function(utility)
			{
				///<param name="utility" type="String" optional="true" value="'steam'" />

				var utils = this._udlStrategy._portfolioHourlyUtilities;

				if (!utils) return; //Not loaded yet... should use a callback pattern instead
				
				this._currentUtility = utility = (utility || "Total")

				var summary			= {currentUse:$.Numeric.NEW(0), totalUsage:$.Numeric.NEW(0), peakUse:$.Numeric.NEW(0), peakTime:"",}				
				var performance		= {percent:0, rating:0,}
				var data			= [[],[]]
				var portfolioUtils	= REF;

				if (utils.tryGet(this._currentBuilding.bid, portfolioUtils)) // && portfolioUtils) // is the && portfolioUtils needed?
				{
					var pp = ((utility == "Total") ? "total" : String.toCamelCase($cstr._utilityNameLookup[utility]))
					
					performance.percent	= this._currentBuilding[pp + "PerfPct"  ]||"N/A";
					performance.rating  = this._currentBuilding[pp + "PerfColor"]

					var u = Kiosk.domain.BuildingHourlyUtilities.AS(portfolioUtils.value).usage.item(utility)

					$cstr._rangeArray.forEach
					(
						function(i)
						{
							var h = Date.hourIntToHourTime(i)
							
							if (!u[i.toString()]) return rU(data[0].push({hour:h, value:null,}))

							var v = $.Numeric.NEW(u[i] || 0)

							data[0].push({hour:h, value:v,})

							summary.totalUsage.add(v)
						
							if (!(v >= summary.peakUse)) return;

							summary.SET({peakTime:(h + "m"), peakUse:v,})
						}
					)
					
					if (Object.keys(u).length) //revise when 'u' is an actual dictionary object
					{
						summary.currentUse = (data[0][Object.keys(u).getLast()].value || summary.currentUse)
					}

					//summary.SET
					//(
					//	{totalUsage:summary.totalUsage.toNumeric().applyUnit(unit), peakUse:summary.peakUse.toNumeric().applyUnit(unit), currentUse:summary.currentUse.toNumeric().applyUnit(unit),}
					//)
					summary.totalUsage =
					summary.totalUsage.toNumeric().applyUnit
					(
						this._currentBuilding[String.toCamelCase($cstr._utilityNameLookup[utility] || "") + "EnergyUnitLabel"] || "kBTU"
					)

					var u = this._currentBuilding[String.toCamelCase($cstr._utilityNameLookup[utility] || "") + "PowerUnitLabel"] || "kBTU/hr"
					
					summary.peakUse	   = summary.peakUse   .toNumeric().applyUnit(u)
					summary.currentUse = summary.currentUse.toNumeric().applyUnit(u)

					u = portfolioUtils.value.usageComparison.item((utility == "Total") ? "ComparisonTotal" : utility)

					$cstr._rangeArray.forEach(function(i){return rU(data[1].push(u[i] || null))})
				}

				this._view.SET({setHourlyData:[data], setSummaryData:[summary], setPerformanceData:[performance],})
			}

		this.selectUtility =
			function(utility)
			{
				///<param name="utility" type="String" optional="true" />
				
				this._selectUtility(utility)

				if (!this._currentUtility) return;
				
				this._view.selectUtility(this._currentUtility)
			}
	}
)
