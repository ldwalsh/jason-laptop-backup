﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk.presenters,
		prototype: Kiosk.PresenterBase,
	},

	function BuildingProfilePresenter()
	{
		this._view = Kiosk.views.BuildingProfileView.VAR;
		
		this.Cstr =
			function()
			{
				this._view = Kiosk.views.BuildingProfileView.NEW()

				this._messageListner.addSubscription("buildingSelected", this._msg_buildingSelected)
			}

		this._msg_buildingSelected =
			function(building)
			{
				///<param name="building" type="Kiosk.domain.Building" />
				
				var bindingObj = building.toCopy()

				bindingObj.squareFootage = building.squareFootage.toNumeric().localeFormat("n0")

				var stats = this._landing.dataManager.buildingStatistics.item(building.bid)

				stats.equipmentClassTotals.forEach
				(
					function(kv)
					{
						var arr = kv.key.split("|")
						
						bindingObj["ClassID_"   + arr[0]] = $.Numeric.NEW(kv.value).localeFormat("n0")
						bindingObj["ClassName_" + arr[0]] = arr[1]
					}
				)

				bindingObj.SET({totalPoints:stats.totalPoints.toNumeric().localeFormat("n0"), totalEquipment:stats.totalEquipment.toNumeric().localeFormat("n0"),}, true)
				
				this._view.SET({portletTitle:(this._view.portletTitle.split("-")[0] + " - " + building.name), bindFields:[bindingObj], loadImage:[bindingObj.buildingImageURL],})

				//

				var data = []

				var pU = this._landing.dataManager.portfolioUtilities;

				pU.buildingUtilities.item(building.bid).buildingUtilitiesTotals.forEach
				(
					function(kv, index)
					{
						if (!kv.value) return;

						data.push
						(
							{
								id:		pU.pointTypeClassLookup[kv.key],
								label:	pU.pointTypeDisplayNameLookup[kv.key],
								value:	kv.value,
								color:	$.Color.getFromColorsArray(this._landing.clientInfo.chartColors, index),
							}
						)
					
					}.bind(this)
				)

				if (!data.length)
				{
					data.push({label:"No Data", value:1, color:this._landing.clientInfo.chartColors[0],})
				}

				this._view.setBuildingUtilityUsage(data)
			}

		this._view_dataSelected =
			function(dataPoint)
			{
				this._landing.buildingUtilitiesPresenter.selectUtility(dataPoint.id)
			}
	}
)
