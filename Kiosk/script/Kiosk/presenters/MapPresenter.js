﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk.presenters,
		prototype:	Kiosk.PresenterBase,
	},

	function MapPresenter()
	{
		this._view			  = Kiosk.views.MapView.VAR;
		this._currentBuilding = Kiosk.domain.Building.VAR;
				
		this.Cstr =
			function()
			{
				this._view = Kiosk.views.MapView.NEW()

				this._messageListner.addSubscription("dataManagerUpdated", this._msg_dataManagerUpdated)
			}

		this._view_buildingSelected =
			function(bid)
			{
				if (this._currentBuilding && (this._currentBuilding.bid == bid)) return;

				this._currentBuilding = this._landing.dataManager.buildings.singleOrNull({bid:bid,})
				
				this._view.highlightBuilding(bid)

				this._landing.mbus.send("buildingSelected", this._currentBuilding)
			}

		this._view_mapReady =
			function()
			{
				this.selectBuilding(this._landing.clientInfo.defaultBID)
			}

		this._msg_dataManagerUpdated =
			function()
			{
				var infos = [];

				this._landing.dataManager.buildings.forEach
				(
					function(b)
					{
						infos.push
						(
							{ //use Object.instantiate here (or similar)

								bid:			b.bid,
								name:			b.name,
								street:			b.address,
								city:			b.city,
								state:			b.state,
								coordinates:	Kiosk.BuildingCoordinatesLookup.Coordinates.NEW(b.latitude, b.longitude),
								totalPerfColor:	b.totalPerfColor
							}
						)
					}
				)

				this._view.addBuildings(infos)
			}

		this.selectBuilding =
			function(bid)
			{
				this._view._setMapView(bid)	
			}

		this.currentBuildingId =
			{
				get:function(){return (this._currentBuilding ? this._currentBuilding.bid : null)}
			}
	}
)
