﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk.presenters,
		prototype:	Kiosk.PresenterBase,
		contracts:	{IChartDataPoint:{id:Number, label:String, value:Number, color:String, isExploded:Boolean, percent:Number,},},
		nested:		{DisplayByEnum:{building:1, utility:2, buildingType:3,},},
		statics:	{_waterUseCaption:"Water Use",},
	},

	function PortfolioProfilePresenter()
	{
		var $cstr = this.constructor;

		this._view				= Kiosk.views.PortfolioProfileView.VAR;
		this._currentDisplayBy	= Number; //should be $cstr.DisplayByEnum.VAR

		this.Cstr =
			function()
			{
				this._view = Kiosk.views.PortfolioProfileView.NEW()

				this._messageListner.addSubscription("dataManagerUpdated", this._msg_dataManagerUpdated)

				this._view.displayByChanged = this._displayData;
			}

		this._msg_dataManagerUpdated =
			function()
			{
				this._displayData($cstr.DisplayByEnum.building)
			}

		this._view_dataSelected =
			function(dataPoint)
			{
				///<param name="dataPoint" type="Kiosk.presenters.PortfolioProfilePresenter.IChartDataPoint" />
				
				switch (this._currentDisplayBy)
				{
					case $cstr.DisplayByEnum.building: return (dataPoint.id ? rU(this._landing.mapPresenter.selectBuilding(dataPoint.id)) : undefined)
					case $cstr.DisplayByEnum.utility:  return rU(this._landing.buildingUtilitiesPresenter.selectUtility(dataPoint.utility)) //revise to conform with interface... no utility prop
				}

				throw new $.InvalidOperationError()
			}

		this._getChartColor =
			function(index)
			{
				return $.Color.getFromColorsArray(this._landing.clientInfo.chartColors, index)
			}

		this._displayData =
			function(displayBy)
			{
				///<param name="displayBy" type="Kiosk.presenters.PortfolioProfilePresenter.DisplayByEnum" />

				var getChartColor		= this._getChartColor;
				var dataManager			= this._landing.dataManager;
				var buildings			= dataManager.buildings;
				var buildingUtilities	= dataManager.portfolioUtilities.buildingUtilities;
				var data				= new $.TArray($cstr.IChartDataPoint).SET({otherCount:0,}, true)
				
				this._currentDisplayBy = displayBy;

				switch (displayBy)
				{
					case $cstr.DisplayByEnum.building:
					{
						buildingUtilities.forEach
						(																												  
							function(entry)
							{
								entry.value.buildingUtilitiesTotals.forEach
								(
									function(kvPair)
									{
										if ((kvPair.key == $cstr._waterUseCaption) || !kvPair.value) return;
								
										var bid = parseInt(entry.key)
										var d	= data.singleOrNull({id:bid,})
										
										if (!d)
										{
											d = {id:bid, value:0, label:(bid ? buildings.single({bid:bid,}).name : "_"), color:getChartColor(data.length),}

											data.push(d)
										}

										d.value += kvPair.value
									}
								)
							}
						)

						break;
					}
					case $cstr.DisplayByEnum.utility:
					{
						buildingUtilities.forEach
						(																												  
							function(entry)
							{
								entry.value.buildingUtilitiesTotals.forEach
								(
									function(kvPair)
									{
										if ((kvPair.key == $cstr._waterUseCaption) || !kvPair.value) return; //revise

										var label = dataManager.portfolioUtilities.pointTypeDisplayNameLookup[kvPair.key]

										var d = data.singleOrNull({label:label,})

										if (!d)
										{
											d = {utility:dataManager.portfolioUtilities.pointTypeClassLookup[kvPair.key], label:label, value:0, color:getChartColor(data.length),} //revise to conform to interface

											data.push(d)
										}

										d.value += kvPair.value;
									}
								)
							}
						)

						break;
					}
					case $cstr.DisplayByEnum.buildingType:
					{
						buildingUtilities.forEach
						(
							function(entry)
							{
								entry.value.buildingUtilitiesTotals.forEach
								(
									function(kvPair)
									{
										if ((kvPair.key == $cstr._waterUseCaption) || !kvPair.value) return;

										var buildingType = buildings.single({bid:parseInt(entry.key),}).buildingType;

										if (!buildingType)
										{
											data.otherCount++;
										}

										var d = data.singleOrNull({label:buildingType})

										if (!d)
										{
											d = {label:buildingType, value:0, color:getChartColor(data.length),}

											data.push(d)
										}

										d.value += kvPair.value;
									}
								)
							}
						)
					}
				}

				if (data.length)
				{
					if (data.otherCount)
					{
						data.first({label:"_",}).label = (data.otherCount + " Others")

						data.otherCount = 0;
					}

					var total = $.Numeric.NEW(0)

					buildingUtilities.forEach(function(entry){return entry.value.buildingUtilitiesTotals.forEach(total.add.t())})

					data.forEach(function(point){return _rU(point.label += (" (" + $.Numeric.NEW(point.value).divide(total).multiply(100).round(1, {min:.1}) + "%)"))})
				}
				else
				{
					data.push({label:"No Data", value:1, color:this._landing.clientInfo.chartColors[0],})
				}
				
				this._view.setPortfolioData(data)
			}
	}
)




Function.prototype.t = //rename/revise!
	function()
	{
		return function(kvPair)
		{
//debugger;

			this(kvPair.value)
		
		}.bind(this)
	}





//Function.prototype.t = //rename/revise!
//	function()
//	{
//debugger;
//		var f = this;
						
//		return function(kvPair)
//		{
//			f(kvPair.value)
//		}
//	}


