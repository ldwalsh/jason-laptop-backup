﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk.presenters,
		prototype: Kiosk.PresenterBase
	},

	function HeaderPresenter()
	{
		this._view = Kiosk.views.HeaderView.VAR;

		this.Cstr =
			function()
			{
				this._view = Kiosk.views.HeaderView.NEW()
			}
	}
)