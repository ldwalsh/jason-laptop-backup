﻿///
///<reference path="../../$Kiosk.js" />

$.define
(
	{
		namespace: Kiosk.domain,
	},

	function BuildingStatistics()
	{
		this.bid					= Number.VAR;
		this.totalPoints			= Number.VAR;
		this.totalEquipment			= Number.VAR;
		this.equipmentClassTotals	= $.TArray.VAR($.KeyValuePair)

		this.Cstr =
			function()
			{
				this.totalPoints			= Number.VAR;
				this.totalEquipment			= Number.VAR;
				this.equipmentClassTotals	= new $.TArray(Kiosk.KeyValuePair)
			}
	}
)
