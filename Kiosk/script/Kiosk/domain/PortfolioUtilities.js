﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk.domain,
		statics:
		{
			_deserialize:
			function(obj, instance)
			{
				///<param name="obj" type="Object" />
				///<param name="instance" type="this" />				

				instance.Cstr()
			},
		}
	},

	function PortfolioUtilities()
	{
		this.buildingUtilities			= $.collections.Dictionary.VAR(Kiosk.domain.BuildingUtilities)
		this.pointTypeDisplayNameLookup	= $.Lookup.VAR(String)
		this.pointTypeClassLookup		= $.Lookup.VAR(String)

		this.Cstr =
			function()
			{
				this.buildingUtilities			= $.collections.Dictionary.NEW(Kiosk.domain.BuildingUtilities)
				this.pointTypeDisplayNameLookup	= $.Lookup.NEW(String)
				this.pointTypeClassLookup		= $.Lookup.NEW(String)
			}
	}
)
