﻿///<reference path="../../$Kiosk.js" />

Object.define
(
	{
		namespace: Kiosk.domain,
	},

	function Building()
	{
		this.bid                         = Number.VAR;
		this.clientName                  = String.VAR;
		this.name                        = String.VAR;
		this.address                     = String.VAR;
		this.city                        = String.VAR;
		this.state                       = String.VAR;
		this.country                     = String.VAR;
		this.latitude                    = String.VAR;
		this.longitude                   = String.VAR;
		this.squareFootage               = Number.VAR;
		this.buildingType                = String.VAR;
		this.buildingImageURL            = String.VAR;
		this.kioskContent                = String.VAR;
		this.websiteLink                 = String.VAR;

        this.	 electricEnergyUnitLabel = String.VAR;
        this.		steamEnergyUnitLabel = String.VAR;
        this.chilledWaterEnergyUnitLabel = String.VAR;
        this.		  gasEnergyUnitLabel = String.VAR;
        this.		waterEnergyUnitLabel = String.VAR;
		this.	 hotWaterEnergyUnitLabel = String.VAR;

        this.	  electricPowerUnitLabel = String.VAR;
        this.		 steamPowerUnitLabel = String.VAR;
        this. chilledWaterPowerUnitLabel = String.VAR;
        this.		   gasPowerUnitLabel = String.VAR;
        this.		 waterPowerUnitLabel = String.VAR;
		this.	  hotWaterPowerUnitLabel = String.VAR;

		this.	 electricPerfColor       = Number.VAR;
		this.		steamPerfColor       = Number.VAR;
		this.chilledWaterPerfColor       = Number.VAR;
		this.		  gasPerfColor       = Number.VAR;
		this.		waterPerfColor       = Number.VAR;
		this.	 hotWaterPerfColor       = Number.VAR;
		this.		totalPerfColor       = Number.VAR;

		this.	 electricPerfPct         = Number.VAR;
		this.		steamPerfPct         = Number.VAR;
		this.chilledWaterPerfPct         = Number.VAR;
		this.		  gasPerfPct         = Number.VAR;
		this.		waterPerfPct         = Number.VAR;
		this.	 hotWaterPerfPct         = Number.VAR;
		this.		totalPerfPct         = Number.VAR;

        this._toCopy =
            function()
            {
                var o = {}

                for (var a in this)
                {
                    o[a] = (this[a]||"").valueOf()
                }

                return o;
            }
    }
)
