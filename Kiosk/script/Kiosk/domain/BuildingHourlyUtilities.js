﻿	///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk.domain,
		statics:
		{
			_deserialize:
			function(obj, instance)
			{
				///<param name="obj" type="Object" />
				///<param name="instance" type="this" />
				
				instance.Cstr()
			},
		},
	},

	function BuildingHourlyUtilities()
	{
		this.electricTotals					= $.TArray.VAR(Kiosk.KeyValuePair)
		this.steamTotals					= $.TArray.VAR(Kiosk.KeyValuePair)
		this.chilledWaterTotals				= $.TArray.VAR(Kiosk.KeyValuePair)
		this.hotWaterTotals					= $.TArray.VAR(Kiosk.KeyValuePair)
		this.gasTotals						= $.TArray.VAR(Kiosk.KeyValuePair)
		this.waterTotals					= $.TArray.VAR(Kiosk.KeyValuePair)
		this.totals							= $.TArray.VAR(Kiosk.KeyValuePair)

		this.electricComparisonTotals		= $.TArray.VAR(Kiosk.KeyValuePair)
		this.steamComparisonTotals			= $.TArray.VAR(Kiosk.KeyValuePair)
		this.chilledWaterComparisonTotals	= $.TArray.VAR(Kiosk.KeyValuePair)
		this.hotWaterComparisonTotals		= $.TArray.VAR(Kiosk.KeyValuePair)
		this.gasComparisonTotals			= $.TArray.VAR(Kiosk.KeyValuePair)
		this.waterComparisonTotals			= $.TArray.VAR(Kiosk.KeyValuePair)
		this.comparisonTotals				= $.TArray.VAR(Kiosk.KeyValuePair)

		this.usage							= $.collections.Dictionary.VAR(Object)
		this.usageComparison				= $.collections.Dictionary.VAR(Object)

		this.Cstr =
			function()
			{
				this.electricTotals					= new $.TArray(Kiosk.KeyValuePair)
				this.steamTotals					= new $.TArray(Kiosk.KeyValuePair)
				this.chilledWaterTotals				= new $.TArray(Kiosk.KeyValuePair)
				this.hotWaterTotals					= new $.TArray(Kiosk.KeyValuePair)
				this.gasTotals						= new $.TArray(Kiosk.KeyValuePair)
				this.waterTotals					= new $.TArray(Kiosk.KeyValuePair)
				this.totals							= new $.TArray(Kiosk.KeyValuePair)

				this.electricComparisonTotals		= new $.TArray(Kiosk.KeyValuePair)
				this.steamComparisonTotals			= new $.TArray(Kiosk.KeyValuePair)
				this.chilledWaterComparisonTotals	= new $.TArray(Kiosk.KeyValuePair)
				this.hotWaterComparisonTotals		= new $.TArray(Kiosk.KeyValuePair)
				this.gasComparisonTotals			= new $.TArray(Kiosk.KeyValuePair)
				this.waterComparisonTotals			= new $.TArray(Kiosk.KeyValuePair)
				this.comparisonTotals				= new $.TArray(Kiosk.KeyValuePair)

				this.usage							= $.collections.Dictionary.NEW(Object)
				this.usageComparison				= $.collections.Dictionary.NEW(Object)
			}
	}
)
