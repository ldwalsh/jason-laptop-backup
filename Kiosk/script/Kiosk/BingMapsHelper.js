﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk,
		//construct:	function BingMapHelper(){}, not supported by loadscript
		statics:
		{
			key:		"AgMstyaSTSlRUh0lo4t8ZTkYkGw8VoVz_6YhUoGLR9VkoblN6K_pQE6Y1wMPXgBD", //replace with key defined in CW.Utility->common constants
			//icons:		{building:"resource/image/building_icon.png", selectedBuilding:"resource/image/building_icon_selected.png",},

			geocodeFunc:
			function(buildingInfo, callback) //move!?
			{
				///<param name="buildingInfo" type="Kiosk.views.MapView.IBuildingInfo" />
				///<param name="callback" type="Function" />
				
				var url =
					(
						("https://dev.virtualearth.net/REST/v1/Locations?key=" + this.key) +
						(
							"&addressLine="		+ encodeURIComponent(buildingInfo.street) +
							"&locality="		+ encodeURIComponent(buildingInfo.city) +
							"&adminDistrict="	+ encodeURIComponent(buildingInfo.state)
						)
					)

				var retryCaller = this.geocodeFunc.getArgsCaller(arguments).getDelayCaller(1000)
						
				XMLHttpRequest.executeGet
				(
					("Dispatcher.ashx?url=" + encodeURIComponent(url)),
					function(xhr)
					{
						if (xhr.status != 200) return retryCaller()

						var resourceSets = JSON.parse(xhr.responseText).resourceSets;

						if (!resourceSets.length) return retryCaller()
						
						var resources = resourceSets[0].resources;

						if (!resources.length) return retryCaller()

						resources.forEach
						(
							function(loc)
							{
								buildingInfo.coordinates =
								Kiosk.BuildingCoordinatesLookup.Coordinates.NEW
									(loc.geocodePoints.FOR(function(point){return (point.usageTypes.contains("Display") ? point : undefined)}).coordinates)
							}
						)
								
						callback()
					}
				)
			},
			
			withinMapViewBounds:
			function(bounds, location)
			{
				return (bounds.contains(location))
			},

			computeIsCrowded:
			function(bingMap, anchor, threshold)
			{
				var entities		= bingMap.entities;
				var length			= entities.getLength()
				var bounds			= bingMap.getBounds()
				var arr				= []
				
				for (var i=0; i<length; i++)
				{
					var entity = entities.get(i)

					if (!this.withinMapViewBounds(bounds, entity.getLocation())) continue;

					var a = entity.cm1002_er_etr.dom;

					arr.push({pixelLeft:a.pixelLeft, pixelTop:a.pixelTop,})
				}

				var left	= anchor.pixelLeft;
				var top		= anchor.pixelTop;

				return arr.some
				(
					function(icon)
					{
						///<param name="icon" type="Kiosk.views.MapView._IEntity.VAR" />
					
						var h = Math.abs(left - icon.pixelLeft)
						var v = Math.abs(top  - icon.pixelTop )

						if (!h && !v) return;

						if ((h < threshold) && (v < threshold)) return true;
					}
				)
			}
		},
	},
	function BingMapsHelper(){}
)
