﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace:	Kiosk,
		prototype:	$.IntervalPollerBase,
		debugging:	{},
	},

	function DataManagerForUtilities()
	{
		var $base = this.constructor.prototype;

		this._failCount = Number.VAR;		
		
		this.onSuccess	= $.Event;
		this.onFailure	= $.Event;
		this.onTimeout	= $.Event;

		this.Cstr =
			function()
			{
				this._pollInterval = (15 * 60 * 1000) //(15 * 60 * 1000)
			}

		this.start =
			function()
			{
				this.failCount = 0;
				
				$base.start.apply(this, [true]) //revise
			}

		this._polling =
			function(completed)
			{
				///<param name="completed" type="Callback" />

				Kiosk.BuildingService.instance.getPortfolioHourlyUtilities().setOn =
					Function.bindAll
					(
						this,					
						{
							success:function(d)
							{
								completed()
								
								this.onSuccess.raise(d)
							
							}, //replace with Function.makeCallList()?

							failure:function(err)
							{
								if (this.failCount == 3) return _rU(this.onFailure.raise("Utility Performance could not be loaded."))

								this._polling.getArgsCaller([completed]).invokeDelay(10*1000)

								this.failCount++;
							},

							timeout:function(err)
							{
								///<param name="err" type="$.AsyncRPCall.TimeoutError" />

								this.onTimeout.raise("Please wait while Utility Performance is loaded.")

								err.continueWait = true;
							},
						}
					)
			}
	}
)



