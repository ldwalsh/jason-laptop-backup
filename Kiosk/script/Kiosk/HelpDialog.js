﻿///<reference path="../$Kiosk.js" />

Object.define
(
	{
		namespace: Kiosk,
		prototype: iCL.ui.windows.DialogWindow,
		resources: ["resource/html/HelpDialog.html", "resource/less/HelpDialog.less",],
		construct:
		{
			_helpEndpoint: document.baseUrl.endWith("/") + "Service/BuildingService.asmx/GetHelp?id=",
		}
	},

	function HelpDialog(helpWidget)
	{
		///<param name="helpWidget" type="Kiosk.HelpWidget" />
		
		var $base = HelpDialog.prototype;
		
		this._helpWidget = Kiosk.HelpWidget.VAR;
		this._reposition = Function.empty; //overrides
		this._useId      = ("div" + HelpDialog.name)
		this._helpUrl    = String.VAR;

		this._showOverlay =
			function()
			{
				var f = this._overlayRemover = HTMLDivElement.createMask(Kiosk.Landing.instance._divKiosk)

				f.element.classList.add("SelectBuildingOverlay")
			}

		this._open =
			function()
			{
				var html = HelpDialog.type.definition.resources.html; //revise
				
				this._contentElement = html.cloneNode(true).SET({innerHTML:JSON.parse(iCL.RPCall.NEW(HelpDialog._helpEndpoint + this._helpUrl).SET({deserializer:iCL.SimpleDeserializer.NEW(), verb:"GET"}).exec()).d || ""})

				$base._open.apply(this)

				this._windowDiv.style.top = ("calc(" + getComputedStyle(this._windowDiv).top + " + 100px" + ")") //revise?

				HTMLElement.create(HTMLDivElement, {parentNode:this.header,}).event.attach.click(this.close)

				this._windowDiv.classList.add("FixedSize")
			}
			
		this.open =
			function(helpUrl)
			{
				this._helpUrl = helpUrl;

				$base.open.apply(this)
			}

		this.close =
			function()
			{
				$base.close.apply(this)
				
				this._helpWidget._togglePointerCapture(true)
			}
	}
)
