﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk.utilityDataLoadStrategies,
		prototype: Kiosk.utilityDataLoadStrategies.UDLStrategyBase,
	},

	function PortlUserUDLStrategy()
	{
		this._loadingList			   = $.collections.List.VAR(Number)
		this._portfolioHourlyUtilities = $.collections.Dictionary.VAR(Kiosk.domain.BuildingHourlyUtilities)

		this.Cstr =
			function()
			{
				this._loadingList			   = $.collections.List.NEW(Number)				
				this._portfolioHourlyUtilities = $.collections.Dictionary.NEW(Kiosk.domain.BuildingHourlyUtilities)
			}

		this._getBuildingData =
			function(bid, callback, attempt)
			{
				///<param name="bid" type="Number" />
				///<param name="callback" type="Callback" />
				///<param name="attempt" type="Number" />
				
				var utils = this._portfolioHourlyUtilities;

				if (utils.hasKey(bid)) return callback(utils, bid) //revise the callback args

				if (this._loadingList.contains(bid)) return;
				
				this._loadingList.add(bid)

				Kiosk.BuildingService.instance.getPortfolioHourlyUtilitiesByBuilding(bid).setOn =
					Function.bindAll
					(
						this,
						{
							failure:function(err)
							{
								if (attempt > 2 && !confirm("The utility data could not be retrieved. Retry?")) return rU(callback(null, bid)) //replace with ui.MessageBox
									return rU(this._getBuildingData.invokeAsync([bid, callback, (attempt+1)], 10000))
							},

							success:function(data)
							{
								///<param name="data" type="Array" elementType="Kiosk.domain.BuildingHourlyUtilities" />

								utils.add(bid, data[bid])
						
								this._getBuildingData(bid, callback)
							},

							finally:function()
							{
								this._loadingList.remove(bid)
							},
						}
					)
			}

		this.getBuildingData =
			function(bid, callback)
			{
				///<param name="bid" type="Number" />
				///<param name="callback" type="Function" />

				this._getBuildingData(bid, callback, 1)
			}
	}
)
