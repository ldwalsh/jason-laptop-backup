﻿///
///<reference path="../../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk.utilityDataLoadStrategies,
		prototype: Kiosk.utilityDataLoadStrategies.UDLStrategyBase,
	},

	function KioskUserUDLStrategy()
	{
		var $cstr = this.constructor;

		this._portfolioHourlyUtilities	= $.collections.Dictionary.VAR(Kiosk.domain.BuildingHourlyUtilities)
		this._callbackWithArgs			= Function.VAR;

		this.Cstr =
			function()
			{
				Kiosk.DataManagerForUtilities.NEW().SET({start:Function.noArgs, onSuccess:this._onSuccess, onFailure:this._onFailure, onTimeout:this.onTimeout,})
			}

		this._onFailure =
			function(msg)
			{
				this._onSuccess(new Error())

				this.onFailure.raise(msg)
			}
		
		this._onSuccess =
			function(data)
			{
				this._portfolioHourlyUtilities = $.collections.Dictionary.NEW(Kiosk.domain.BuildingHourlyUtilities, data)

				if (!(data instanceof Error)) this.onSuccess.raise() //revise?

				if (!this._callbackWithArgs) return;
				
				this._callbackWithArgs(this._portfolioHourlyUtilities)
			}

		this.getBuildingData =
			function(bid, callback)
			{
				///<param name="bid" type="Number" />
				///<param name="callback" type="Function" />
				
				this._callbackWithArgs = callback.getArgsCaller([Function.getArgsCaller.firstPlaceHolder, bid]) //nullify previous callback

				if (!this._portfolioHourlyUtilities) return;

				callback(this._portfolioHourlyUtilities, bid)
			}
	}
)
