﻿///
///<reference path="../$Kiosk.js" />

$.class
(
	{
		namespace: Kiosk,
		prototype: $.htmlStorage.HTMLStorageObjectBase,
		nested:
		{
			Coordinates:
			function(arg0, arg1)
			{
				///<signature>
				/// <param name="latitude" type="Number" />
				///	<param name="longitude" type="Number" />
				///</signature>
				///<signature>
				/// <param name="coordinates" type="Array" elementType="Number" />
				///</signature>				

				this.latitude  = Number.VAR;
				this.longitude = Number.VAR;

				this.Cstr =
					function()
					{
						if (arg0 instanceof Array)
						{
							this.latitude  = arg0[0]
							this.longitude = arg0[1]
						}
						else
						{
							this.latitude  = arg0;
							this.longitude = arg1;
						}
					}
			},
		},
	},

	function BuildingCoordinatesLookup()
	{
		var $cstr = this.constructor;

		this.buildings = $.Lookup.VAR($cstr.Coordinates)

		this.Cstr =
			function()
			{
				this.buildings = $.Lookup.NEW($cstr.Coordinates)
			}
	}
)
