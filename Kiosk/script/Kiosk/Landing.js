﻿///<reference path="../$Kiosk.js" />

Object.define
(
	{
		namespace: Kiosk,
		resources: ["resource/less/Landing.less", "resource/less/LandingForPortalUser.less", "resource/less/LandingShared.less"],
		construct:
		{
			Cstr:
			function()
			{
				///<param name="error" type="Error" />
				
				iCL.onErrorCallback = this._onError;
			},

			_onError:
			function(error)
			{
				throw error;
			},

			instance_:
			function()
			{
				//
				if (window.intellisense) return Kiosk.Landing.VAR;
				//

				return this.__instance;
			},
		},
	},

	function Landing()
	{
		var _presenters = Kiosk.presenters;

		this._divKiosk           = HTMLDivElement.VAR;
		this._dataManager        = Kiosk.DataManager.VAR;
		this.sessionKeepAliveURL = String.VAR;
		this.clientInfo          = Kiosk.ClientInfo.VAR;
		this.mbus                = iCL.MessageBus.VAR;
		this.helperWidget        = Kiosk.HelpWidget.VAR;
		
		this.mapPresenter               = AutoProp(_presenters.MapPresenter)
		this.portfolioProfilePresenter  = AutoProp(_presenters.PortfolioProfilePresenter)
		this.buildingProfilePresenter   = AutoProp(_presenters.BuildingProfilePresenter)
		this.buildingUtilitiesPresenter = AutoProp(_presenters.BuildingUtilitiesPresenter)
		
		this.Cstr =
			function()
			{
				this.constructor.__instance = this; //revise

				this._divKiosk    = $gId("divKiosk");
				this.mbus         = iCL.MessageBus.NEW(this)
				this.clientInfo   = Kiosk.ClientInfo.NEW()				
				this.helperWidget = Kiosk.HelpWidget.NEW()
			}

		this.load =
			function()
			{
				var ska;
				
				if (userInfo.isKioskUser)
				{
					document.disableContextMenu()
					
					HTMLElement.create(HTMLMetaElement, {parentNode:document.head, name:"viewport", content:"width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;",})

					ska = iCL.SessionKeepAlive;
				}
				else
				{
					this._divKiosk.classList.add("ForPortalUser")

					ska = iCL.asp.SessionKeepAlive;
				}

				ska.NEW(this.sessionKeepAliveURL).enable()

				//
				//instantiate presenters
				
				_presenters.HeaderPresenter.NEW()

				this.mapPresenter               = _presenters.MapPresenter              .NEW()
				this.portfolioProfilePresenter  = _presenters.PortfolioProfilePresenter .NEW()
				this.buildingProfilePresenter   = _presenters.BuildingProfilePresenter  .NEW()
				this.buildingUtilitiesPresenter = _presenters.BuildingUtilitiesPresenter.NEW()

				var presenters = new iCL.TArray(Kiosk.PresenterBase, [this.mapPresenter, this.portfolioProfilePresenter, this.buildingProfilePresenter, this.buildingUtilitiesPresenter])

				//

				Function.invoke
				(
					function f() //await view rendering
					{
						if (presenters.FOR(function(p){if (!p._view.isRendered) return true;})) return _rU(f.bind(this).invokeAsync()) //use .some here

						document.scrollToBottom()
						
						this._toggleLoadingNotification(true)
						
						this._dataManager = Kiosk.DataManager.NEW(iCL.Time.NEW(4,00,00)).SET({updated:this._dataManager_updated,}) //polling stopped inside for Portal users

					}.bind(this)
				)

				this.helperWidget = Kiosk.HelpWidget.NEW(this._divKiosk, Array.fromArrayLike(document.querySelectorAll("[data-help-widget--url]")))
			}

		this.reload =
			function()
			{
				this._toggleLoadingNotification(true)

				XMLHttpRequest.executeGet
				(
					location.href,
					function(hro)
					{
						if (hro.status != 200) return _rU(this.reload.invokeDelay(30 * 1000))

						Navigator.load(location.href, {bid:Kiosk.Landing.instance.clientInfo.defaultBID, r:Date.now(),})
					
					}.bind(this),
					{forceRefresh:true,}
				)				
			}

		this._toggleLoadingNotification =
			function(state)
			{
				///<param name="state" type="Boolean" />

				var div = this._divKiosk.toggleEnabled(!state)

				if (!state) return;

				div.classList.add("Loading")
			}

		this._dataManager_updated =
			function()
			{
				this._toggleLoadingNotification(false)

				var msg =
					Function.invoke
					(
						function()
						{
							if (Array.isEmpty(this._dataManager.buildings))  return "The building list could not be retrieved. The server has not responded to the requests.";
							if (!this._dataManager.portfolioUtilities)       return "Portfolio Utilities data could not be retrieved. The server has not responded to the requests.";
							if (this._dataManager.buildingStatistics && !this._dataManager.buildingStatistics.count) return "Building Statistics data could not be retrieved. The server has not responded to the requests.";
						
						}.bind(this)
					)

				if (msg) return _rU(iCL.ui.windows.MessageBox.open(msg, {buttons:[{caption:"Reload Kiosk", func:this.reload,}]}))

				if (this.clientInfo.defaultBID) return _rU(this.mbus.send("dataManagerUpdated", null))

				Kiosk.BuildingSelectorDialog.NEW().SET({buildingSelected:this._defaultBuildingSelected, open:null,})
			}

		this._defaultBuildingSelected =
			function(bid)
			{
				this.clientInfo.defaultBID = bid;

				this._dataManager_updated()

				iCL.ui.InteractionMonitor.NEW().SET({enable:null, addInterval:[(30*60), this._dataManager_updated],})
			}

		this.dataManager =
			{
				get:function(){return this._dataManager;}
			}
	}
)


Array.isEmpty = function(arr){return (!arr || !arr.length)} //ADDED



////////////////////////


Object.define
(
	{
		namespace: iCL,
	},

	function CallStack(callback)
	{
		this._callback          = Function.VAR;
		this._remainingOpsCount = 0;

		this.add =
			function(func)
			{
				this._remainingOpsCount++;

				return function()
				{
					var x = {fail:false}

					func.apply(null, Array.fromAlike(arguments).concat([x]))

					if (x.fail) return;

					this._remainingOpsCount--;

					if (this._remainingOpsCount) return;

					this._callback()
					
				}.bind(this)
			}

		this.abort =
			function()
			{
				
			}
	}
)



Navigator.extends =
{
	load:
	function(href, qsArgs)
	{
		///<param name="href" type="String" />
		///<param name="qsArgs" type="Object" />
		
		var qs = "";
		
		if (qsArgs)
		{
			Object.forEach
			(
				qsArgs,
				function(value, key)
				{
					qs += ("&" + key + "=" + value)
				}
			)

			qs = ((location.href.contains("?") ? "&" : "?") + qs.substr(1))	
		}

		location.href = (href + qs)
	},
}



window.SELECT =
function(value, lookup)
{
	//
	if (window.intellisense) return lookup[Object.keys(lookup)[0]]
	//

	//if (!lookup["default"]) throw new Error("345345433443")

    if (!value) return null;

    if (lookup instanceof Array) return (lookup.find(function(_){return (_[0] == value)}) || lookup.find(function(_){return (_[0] == SELECT.default)}) || [,])[1]
    
    if (lookup instanceof Array) return (SELECT.X(lookup, value) || SELECT.X(lookup, SELECT.default) || [,])[1]

    var k = value.toString()
    var v = lookup[k]

    switch (v)
    {
        case window.SELECT.next:
        {
            var keys = Object.keys(lookup)

            return window.SELECT(keys[keys.indexOf(k)+1], lookup)
        }

        case window.SELECT.last: throw new Error(23423421422211)

        default: return (v || lookup["default"])
    }
}

window.SELECT.next    = "_NEXT_";
window.SELECT.last    = "_LAST_";
window.SELECT.default = "default";
window.SELECT.X       = function(lookup, value){return lookup.find(function(_){return (_[0] == value)})}
