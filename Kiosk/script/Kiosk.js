﻿///
///<reference path="$Kiosk.js" />

//var Kiosk = {} required becuase of bug in JSAsset

window.Namespace =
{
	create:
	function(obj)
	{
		var f =
			function(n)
			{
				for (var a in n)
				{
					var o = n[a]

					f(o)

					o.SET({__namespace:true, __name:a, parent:((n == obj) ? window : n), toString:function(){return this.__name;}}, true)
				}
			}

		f(obj)

		for (var a in obj)
		{
			window[a.toString()] = obj[a]
		}
	}
}

Namespace.create
(
	{
		Kiosk:
		{
			domain:						{},
			presenters:					{},
			utilityDataLoadStrategies:	{},
			views:						{},
		}
	}
)

