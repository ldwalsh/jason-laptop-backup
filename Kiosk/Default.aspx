﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Default.aspx.cs" Inherits="CW.Website.Kiosk.Default" MasterPageFile="~/_masters/Kiosk.Master" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ContentPlaceHolderID="plcCopy" runat="server">

	<script type="text/javascript" src="/_assets/scripts/jquery.flot.min.js"></script>
	<script type="text/javascript" src="/_assets/scripts/jquery.flot.pie.min.js"></script>

	<script type="text/javascript">

		function init()
		{
			var bid = /bid=[0-9]*/.exec(location.search.toLowerCase())

			clientInfo.defaultBID = ((bid && bid[0]) ? parseInt(bid[0].split("=")[1]) : 0)

			Kiosk.Landing.NEW().SET({clientInfo:clientInfo, sessionKeepAliveURL:("/_framework/httpHandlers/SessionKeepAliveHandler.ashx"),}).load()
		}

	</script>
	
	<div id="divKiosk">
		<div data-name="portfolioProfileChartContainer" style="display:none;">
			<telerik:RadHtmlChart ID="RadHtmlChart1" runat="server" Transitions="false" Width="50" Height="50" Legend-Appearance-Visible="false">
				<PlotArea>
					<Series>
						<telerik:PieSeries DataFieldY="value" ColorField="color" NameField="name">
							<LabelsAppearance DataField="name" />
						</telerik:PieSeries>
					</Series>
				</PlotArea>
			</telerik:RadHtmlChart>
		</div>
		<div data-name="buildingProfileChartContainer" style="display:none;">
			<telerik:RadHtmlChart ID="RadHtmlChart2" runat="server" Transitions="false" Width="50" Height="50" Legend-Appearance-Visible="false">
				<PlotArea>
					<Series>
						<telerik:PieSeries DataFieldY="value" ColorField="color" NameField="name">
							<LabelsAppearance DataField="name" />
						</telerik:PieSeries>
					</Series>
				</PlotArea>
			</telerik:RadHtmlChart>
		</div>
		<div data-name="buildingUtilitiesChartContainer" style="display:none;">
			<telerik:RadHtmlChart runat="server" ID="ColumnChart" Width="50" Height="50" Transitions="false">
				<ChartTitle>
					<Appearance Visible="false" />
				</ChartTitle>
				<Legend >
					<Appearance Visible="false" />
				</Legend>
				<PlotArea>
					<XAxis DataLabelsField="hour">
						<MajorGridLines Visible="false" />
						<MinorGridLines Visible="false" />
					</XAxis>
					<YAxis>
						<MinorGridLines Visible="false" />
					</YAxis>
					<Series>
						<telerik:ColumnSeries DataFieldY="value">
							<TooltipsAppearance Visible="false" />
							<LabelsAppearance Visible="false" />
						</telerik:ColumnSeries>
						<telerik:LineSeries DataFieldY="value">
							<TooltipsAppearance Visible="false" />
							<LabelsAppearance Visible="false" />
							<LineAppearance Width="0" />
							<SeriesItems>
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
								<telerik:CategorySeriesItem Y="0" />
							</SeriesItems>
						</telerik:LineSeries>
					</Series>
				</PlotArea>
			</telerik:RadHtmlChart>
		</div>
		<div data-id="divKioskArea">
			<div data-id="divHeader"></div>
			<div data-id="divBody">
				<div data-id="divTPane">						
					<div data-id="divLSection">
						<div class="Portlet">								
							<div class="PortletHeader">Portfolio Utility Performance</div>
							<div class="PortletBodyArea">
								<div	class="PortletBody"
										data-id="divMapView"
										data-help-widget--url="1"
										data-help-widget--inner-class="HelpWidget-BuildingUtilityPerformance">
								</div>
							</div>
						</div>								
					</div>
					<div data-id="divRSection">
						<div class="Portlet">
							<div class="PortletHeader">Portfolio Utility Breakdown</div>
							<div class="PortletBodyArea">
								<div	class="PortletBody"
										data-id="divPortfolioProfileView"
										data-help-widget--url="2"
										data-help-widget--inner-class="HelpWidget-BuildingUtilityPerformance">
								</div>
							</div>
						</div>							
					</div>						
				</div>
				<div data-id="divBPane">
					<div data-id="divLSection">
						<div class="Portlet">
							<div class="PortletHeader">Building Utility Performance</div>
							<div class="PortletBodyArea">
								<div	class="PortletBody"
										data-id="divBuildingUtilitiesView"
										data-help-widget--url="3"
										data-help-widget--inner-class="HelpWidget-BuildingUtilityPerformance">
								</div>
							</div>
						</div>
					</div>
					<div data-id="divRSection">
						<div class="Portlet">
							<div class="PortletHeader">Building Utility Breakdown</div>
							<div class="PortletBodyArea">
								<div	class="PortletBody"
										data-id="divBuildingProfileView"
										data-help-widget--url="4"
										data-help-widget--inner-class="HelpWidget-BuildingUtilityPerformance">
								</div>
							</div>
						</div>
					</div>						
				</div>
			</div>
		</div>
	</div>

</asp:Content>