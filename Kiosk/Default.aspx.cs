﻿using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using iCL;
using iCLib;
using JSAsset;
using System;
using System.IO;
using System.Runtime.Serialization.Json;

namespace CW.Website.Kiosk
{
	public partial class Default: SitePage
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			var cid	   = this.siteUser.CID;
			var client = DataMgr.ClientDataMapper.GetClient(cid, null);
			var stream = new MemoryStream();
			
			new DataContractJsonSerializer(typeof(ClientInfo)).WriteObject
			(
				stream,
				new ClientInfo
					{
						headerImgSrc	= ("../_assets/styles/themes/images/" + (SiteUser.Current.IsSchneiderTheme ? "se" : "cw") + "-header-secondary.png"),
						clientImgSrc	= ("/" + HandlerHelper.ClientImageUrl(cid, client.ImageExtension)),
						kioskContent	= client.KioskContent,
						kioskQRContent	= client.KioskQRContent,
						kioskQRLink		= client.KioskQRLink,
						chartColors		= ColorHelper.SEChartingColorsHex,
					}
			);

			stream.Position = 0;

			RegisterStringAsScriptBlock("window.clientInfo=" + new StreamReader(stream).ReadToEnd() + ";");
			RegisterStringAsScriptBlock("window.userInfo={isKioskUser:" + RoleHelper.IsKioskUser(SiteUser.Current.RoleID.ToString()).ToString().ToLower() + ",};");

			ScriptIncluder.GetInstance(this)
				.Add<core.coreClass>()
				.Add<core.asp.ScriptService>()
				.Add<core.Array>()
				.Add<core.CallbackHandler>()
				.Add<core.Color>()
				.Add<core.Date>()
				.Add<core.DateRange>()
				.Add<core.DatePrototype>()
				.Add<core.DOMTokenList>()
				.Add<core.DELEGATE>()	//remove
				.Add<core.Document>()
				.Add<core.DocumentFragment>()
				.Add<core.DOMRectPrototype>()
				.Add<core.ExecutionQueue>()
				.Add<core.HTMLCollectionPrototype>()
				.Add<core.htmlStorage.HTMLStorageObjectBase>()
				.Add<core.KeyedArray>()
				.Add<core.String>()
				.Add<core.Lookup>()
				.Add<core.MessageBus>()
				.Add<core.Navigator>()
				.Add<core.Numeric>()
				.Add<core.Time>()
				.Add<core.TimePollerBase>()
				.Add<core.asp.SessionKeepAlive>()
				.Add<core.ui.windows.WaitBox>()
				.Add<core.ui.DataBinder>()
				.Add<core.ui.windows.DialogWindow>()
				.Add<core.ui.controls.Button>()
				.Add<core.ui.controls.ButtonBar>()
				.Add<core.ui.controls.Image>()
				.Add<core.ui.controls.ListBox>()
				.Add<core.ui.controls.Test>()
				.Add<core.ui.InteractionMonitor>()
				.Add<core.ui.ViewBase>()
				.Add<core.ui.windows.MessageBox>()
				.Add<core.ui.WindowResizeManager>();

			JSAssetManager.GetInstance(this).Add<KioskAsset>();
		}

		private void Page_Init()
		{
			if (!RoleHelper.IsKioskUser(siteUser.RoleID.ToString())) return;

			this.Master.FindControl("updatePanelTop").Visible = false;
		}
	}
}