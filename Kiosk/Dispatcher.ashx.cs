﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CW.Kiosk
{
	public class Dispatcher: IHttpHandler
	{
		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			var url = context.Request.QueryString["url"];

			var x = System.Net.WebRequest.CreateHttp(url);

			var s = x.GetResponse().GetResponseStream();

            var reader = new StreamReader (s);
            // Read the content.
            string responseFromServer = reader.ReadToEnd ();

			s.Close();


			context.Response.ContentType = "text/plain";
			context.Response.Write(responseFromServer);
		}

		Boolean IHttpHandler.IsReusable
		{
			get	{return true;}
		}
	}
}