﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.Master" AutoEventWireup="false" CodeBehind="ProviderUploadPoints.aspx.cs" Inherits="CW.Website.ProviderUploadPoints" %>
<%@ Register src="~/_controls/upload/UploadPoints.ascx" tagname="UploadPoints" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
   <CW:UploadPoints ID="UploadPoints" runat="server" />
</asp:Content>