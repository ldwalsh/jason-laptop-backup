﻿using CW.Business;
using CW.Common.Constants;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Collections.Generic;

namespace CW.Website._diagnostics
{
    public sealed class DiagnosticsBinder
    {
        #region CLASS

            [Serializable()]
            public sealed class BindingParameters
            {
                public static Int32? ToInt(String value)
                {
                    if (value.ToLower() == "all" || value.ToLower() == "0" || value.ToLower() == "-1") return null;

                    return Int32.Parse(value);
                }

                public  Int32 cid;

                public  List<int> bids;

                public  Int32? equipmentClassID;

                public  Int32? equipmentID;

                public  Int32? analysisID;

                public  DateTime start;

                public  DateTime end;

                public  DataConstants.AnalysisRange range;

                public  String notesFilter;
            }

        #endregion


        private readonly SiteUser siteUser;

        private readonly DataManager dataManager;

        public DiagnosticsBinder(SiteUser siteUser, DataManager dataManager)
        {
            this.siteUser = siteUser;
            this.dataManager = dataManager;
        }

        public IEnumerable<DiagnosticsResult> BuildDiagnosticsResults(BindingParameters bp)
        {
            DateTime startDate;

            switch (bp.range)
            {
                case DataConstants.AnalysisRange.Daily:
                {
                    startDate = bp.start;

                    break;
                }
                case DataConstants.AnalysisRange.Weekly:
                {
                    startDate = DateTimeHelper.GetLastSunday(bp.start);
                    
                    break;
                }
                case DataConstants.AnalysisRange.Monthly:
                {
                    startDate = DateTimeHelper.GetFirstOfMonth(bp.start);

                    break;
                }
                case DataConstants.AnalysisRange.HalfDay:
                {
                    startDate = DateTime.UtcNow;
                    bp.end = DateTime.UtcNow;

                    break;
                }
                default:
                {
                    throw new InvalidOperationException("The AnalysisRange is not supported.");
                }
            }

            return
			dataManager.DiagnosticsDataMapper.GetDiagnosticsResults
            (
                new DiagnosticsResultsInputs()
                {
                    AnalysisRange = bp.range, StartDate = startDate, EndDate = bp.end, AID = bp.analysisID, CID = bp.cid, EID = bp.equipmentID, EquipmentClassID = bp.equipmentClassID, NotesFilter = bp.notesFilter
                },
                bp.bids
            );
        }

        public IEnumerable<DiagnosticsResult> BuildTopDiagnosticsResults(BindingParameters bp, int top)
        {
            DateTime startDate;

            switch (bp.range)
            {
                case DataConstants.AnalysisRange.Daily:
                {
                    startDate = bp.start;

                    break;
                }
                case DataConstants.AnalysisRange.Weekly:
                {
                    startDate = DateTimeHelper.GetLastSunday(bp.start);
                    
                    break;
                }
                case DataConstants.AnalysisRange.Monthly:
                {
                    startDate = DateTimeHelper.GetFirstOfMonth(bp.start);

                    break;
                }
                default:
                {
                    throw new InvalidOperationException("The AnalysisRange is not supported.");
                }
            }

            return dataManager.DiagnosticsDataMapper.GetTopDiagnosticsResults(bp.range,  startDate, bp.end, bp.cid, bp.bids, bp.equipmentClassID, bp.equipmentID, bp.analysisID, bp.notesFilter, top);
        }
    }
}