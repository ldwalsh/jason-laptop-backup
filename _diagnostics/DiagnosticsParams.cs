﻿using CW.Common.Constants;
using CW.Utility;
using CW.Utility.Web;
using System;

namespace CW.Website._diagnostics
{
	public sealed class DiagnosticsParamsObject: QueryString.IParamsObject
	{
		#region QueryString.IParamsObject

			Boolean QueryString.IParamsObject.RequirePropertyAttribute
			{
				get {return false;}
			}

		#endregion

		#pragma warning disable 0649

		public DataConstants.AnalysisRange rng;
            
		public DateTime sd;

		[
		QueryString.Property(Optional=true)
		]
		public DateTime? ed;
            
		public Int32 cid;

		[
		QueryString.Property(Requires="cid")
		]
		public Int32? bid;

		[
		QueryString.Property(Optional=true, Requires="bid")
		]
		public Int32? ecid;

		[
		QueryString.Property(Optional=true, Requires="ecid")
		]
		public Int32? eid;

		[
		QueryString.Property(Optional=true, Requires="eid")
		]
		public Int32? aid;

		[
		QueryString.Property(Optional=true)
		]
		public Boolean i;

		#pragma warning restore 0649
	}
}