﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSDataSourceVendorProductAdministration.aspx.cs" Inherits="CW.Website.KGSDataSourceVendorProductAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>KGS Data Source Vendor Products Administration</h1>                        
                <div class="richText">The kgs data source vendor products administration area is used to view, add, and edit data source vendor products.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Data Source Vendor Products"></telerik:RadTab>
                            <telerik:RadTab Text="Add Data Source Vendor Product"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                           <telerik:RadPageView ID="RadPageView1" runat="server">                                   
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p>    
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>        
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridDataSourceVendorProducts"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="VendorProductID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridDataSourceVendorProducts_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridDataSourceVendorProducts_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridDataSourceVendorProducts_Sorting"  
                                             OnSelectedIndexChanged="gridDataSourceVendorProducts_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridDataSourceVendorProducts_Editing"                                                                                                                                                                                   
                                             OnRowDeleting="gridDataSourceVendorProducts_Deleting"
                                             OnDataBound="gridDataSourceVendorProducts_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="VendorProductName" HeaderText="Vendor Product Name">  
                                                    <ItemTemplate><%# Eval("VendorProductName")%></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Vendor">  
                                                    <ItemTemplate><%# Eval("VendorName")%></ItemTemplate>                           
                                                </asp:TemplateField>                                                                                                
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this data source vendor product permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT DATA SOURCE VENDOR PRODUCT DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvVendorProduct" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Data Source Vendor Product Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Vendor Product Name: </strong>" + Eval("VendorProductName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorProductDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("VendorProductDescription") + "</li>"%>                                                                                                                     
                                                            <%# "<li><strong>Vendor: </strong>" + Eval("VendorName") + "</li>"%>                                                                                                                                                                                             	                                                                	                                                                                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT DATA SOURCE VENDOR PRODUCT PANEL -->                              
                                    <asp:Panel ID="pnlEditVendorProduct" runat="server" Visible="false" DefaultButton="btnUpdateVendorProduct">                                                                                             
                                        <div>
                                            <h2>Edit Data Source Vendor Product</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Vendor Product Name:</label>
                                                <asp:TextBox ID="txtEditVendorProductName" MaxLength="50" runat="server"></asp:TextBox>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>  
                                            <div class="divForm">   
                                                <label class="label">*Vendor:</label>    
                                                <asp:DropDownList ID="ddlEditVendor" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                </asp:DropDownList> 
                                            </div>                                                                                                                                              
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateVendorProduct" runat="server" Text="Edit Product"  OnClick="updateVendorProductButton_Click" ValidationGroup="EditVendorProduct"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editVendorProductNameRequiredValidator" runat="server"
                                        ErrorMessage="Vendor Product Name is a required field."
                                        ControlToValidate="txtEditVendorProductName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditVendorProduct">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editVendorProductNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editVendorProductNameRequiredValidatorExtender"
                                        TargetControlID="editVendorProductNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editVendorRequiredValidator" runat="server"
                                        ErrorMessage="Vendor is a required field." 
                                        ControlToValidate="ddlEditVendor"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditVendorProduct">
                                        </asp:RequiredFieldValidator>
                                      <ajaxToolkit:ValidatorCalloutExtender ID="editVendorRequiredValidatorExtender" runat="server"
                                        BehaviorID="editVendorRequiredValidatorExtender" 
                                        TargetControlID="editVendorRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                    </asp:Panel>                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">  
                                    <asp:Panel ID="pnlAddVendorProduct" runat="server" DefaultButton="btnAddVendorProduct">
                                    <h2>Add Data Source Vendor Product</h2> 
                                    <div>    
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                                
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Product Name:</label>
                                            <asp:TextBox ID="txtAddVendorProductName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div> 
                                        <div class="divForm">
                                             <label class="label">Description:</label>
                                             <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                             <div id="divAddDescriptionCharInfo"></div>
                                        </div>  
                                        <div class="divForm">   
                                            <label class="label">*Vendor:</label>    
                                            <asp:DropDownList ID="ddlAddVendor" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                         
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddVendorProduct" runat="server" Text="Add Product"  OnClick="addVendorProductButton_Click" ValidationGroup="AddVendorProduct"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addVendorProductNameRequiredValidator" runat="server"
                                        ErrorMessage="Vendor Product Name is a required field."
                                        ControlToValidate="txtAddVendorProductName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddVendorProduct">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addVendorProductNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addVendorProductNameRequiredValidatorExtender"
                                        TargetControlID="addVendorProductNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addVendorRequiredValidator" runat="server"
                                        ErrorMessage="Vendor is a required field." 
                                        ControlToValidate="ddlAddVendor"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddVendorProduct">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addVendorRequiredValidatorExtender" runat="server"
                                        BehaviorID="addVendorRequiredValidatorExtender" 
                                        TargetControlID="addVendorRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    </asp:Panel>
                            </telerik:RadPageView>                        
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>


                    
                  
