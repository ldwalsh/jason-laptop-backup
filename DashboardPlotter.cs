﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Controls.DataVisualization;
using System.Windows.Controls.DataVisualization.Charting;
using System.Collections.Generic;

namespace CW.Website.Silverlight
{
    public class DashboardPlotter
    {
        #region Data Classes

        public class StatPlot
        {
            public double val { get; set; }
            public DateTime? day { get; set; }
        }

        #endregion

        #region Plot Methods

        /// <summary>
        /// Plots a fault plot
        /// </summary>
        /// <param name="plotDate"></param>
        /// <param name="chart1"></param>
        /// <param name="monthData"></param>
        /// <param name="yearData"></param>
        public static void plotFaults(string plotDate, Chart chart1, System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<object>> monthData, System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<object>> yearData)
        {
            LineSeries faultSeries = (LineSeries)chart1.FindName("series1");
            faultSeries.Title = "Faults";
            if (chart1.FindName("series2") != null)
            {
                LineSeries series2 = (LineSeries)chart1.FindName("series2");
                LineSeries series3 = (LineSeries)chart1.FindName("series3");

                chart1.Series.Remove(series2);
                chart1.Series.Remove(series3);
            }

            List<StatPlot> f = new List<StatPlot>();
            chart1.DataContext = f;


            List<StatPlot> faults = new List<StatPlot>();

            switch (plotDate)
            {
                case "pastWeek":
                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Date >= DateTime.UtcNow.Date.AddDays(-7).Date)
                        {
                            faults.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[0]) });
                        }
                    }

                    faultSeries.DataContext = faults;
                    chart1.UpdateLayout();
                    break;
                case "past30":

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        faults.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[0]) });
                    }

                    faultSeries.DataContext = faults;
                    chart1.UpdateLayout();
                    break;
                case "currWeek":

                    DateTime start = DateTime.UtcNow.Date;
                    while (start.DayOfWeek != DayOfWeek.Monday)
                    {
                        start = start.AddDays(-1);
                    }

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Date >= start.Date)
                        {
                            faults.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[0]) });
                        }
                    }

                    faultSeries.DataContext = faults;
                    chart1.UpdateLayout();
                    break;
                case "currMonth":

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Month == DateTime.UtcNow.Month)
                        {
                            faults.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[0]) });
                        }
                    }

                    faultSeries.DataContext = faults;
                    chart1.UpdateLayout();
                    break;

                case "pastYear":

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in yearData)
                    {
                        faults.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[0]) });

                    }

                    faultSeries.DataContext = faults;
                    chart1.UpdateLayout();
                    break;

            }
        }

        /// <summary>
        /// Plots a cost savings plot
        /// </summary>
        /// <param name="plotDate"></param>
        /// <param name="chart1"></param>
        /// <param name="monthData"></param>
        /// <param name="yearData"></param>
        public static void plotCosts(string plotDate, Chart chart1, System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<object>> monthData, System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<object>> yearData)
        {
            LineSeries costsSeries = (LineSeries)chart1.FindName("series1");
            costsSeries.Title = "Cost";

            if (chart1.FindName("series2") != null)
            {
                LineSeries series2 = (LineSeries)chart1.FindName("series2");
                LineSeries series3 = (LineSeries)chart1.FindName("series3");

                chart1.Series.Remove(series2);
                chart1.Series.Remove(series3);
            }

            List<StatPlot> c = new List<StatPlot>();
            chart1.DataContext = c;


            List<StatPlot> costs = new List<StatPlot>();

            switch (plotDate)
            {
                case "pastWeek":
                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Date >= DateTime.UtcNow.Date.AddDays(-7).Date)
                        {
                            costs.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[1]) });
                        }
                    }

                    costsSeries.DataContext = costs;
                    chart1.UpdateLayout();
                    break;
                case "past30":

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        costs.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToInt32(i[1]) });
                    }

                    costsSeries.DataContext = costs;
                    chart1.UpdateLayout();
                    break;
                case "currWeek":

                    DateTime start = DateTime.UtcNow.Date;
                    while (start.DayOfWeek != DayOfWeek.Monday)
                    {
                        start = start.AddDays(-1);
                    }

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Date >= start.Date)
                        {
                            costs.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[1]) });
                        }
                    }

                    costsSeries.DataContext = costs;
                    chart1.UpdateLayout();
                    break;
                case "currMonth":

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Month == DateTime.UtcNow.Month)
                        {
                            costs.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[1]) });
                        }
                    }

                    costsSeries.DataContext = costs;
                    chart1.UpdateLayout();
                    break;

                case "pastYear":

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in yearData)
                    {
                        costs.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[1]) });

                    }

                    costsSeries.DataContext = costs;
                    chart1.UpdateLayout();
                    break;

            }
        }

        /// <summary>
        /// Plots a priority plot
        /// </summary>
        /// <param name="plotDate"></param>
        /// <param name="chart1"></param>
        /// <param name="monthData"></param>
        /// <param name="yearData"></param>
        public static void plotPriorities(string plotDate, Chart chart1, System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<object>> monthData, System.Collections.ObjectModel.ObservableCollection<System.Collections.ObjectModel.ObservableCollection<object>> yearData)
        {

            /*Style legendStyle = new System.Windows.Style(typeof(Legend));

            Thickness margin = new Thickness(15, 0, 15, 0);

            legendStyle.Setters.Add(new Setter(Legend.MarginProperty, margin));
            legendStyle.Setters.Add(new Setter(Legend.VerticalAlignmentProperty, VerticalAlignment.Center));
            legendStyle.Setters.Add(new Setter(Legend.HeightProperty, 50));

            chart1.LegendStyle = legendStyle;*/

            List<StatPlot> energy = new List<StatPlot>();
            List<StatPlot> comfort = new List<StatPlot>();
            List<StatPlot> maintenance = new List<StatPlot>();

            LineSeries maintenanceSeries = (LineSeries)chart1.FindName("series1");
            maintenanceSeries.Title = "Maintenance";
            LineSeries comfortSeries;
            LineSeries energySeries;

            if (chart1.FindName("series2") == null)
            {
                Style dataPointStyle1 = new System.Windows.Style(typeof(DataPoint));
                dataPointStyle1.Setters.Add(new Setter(DataPoint.BackgroundProperty, new SolidColorBrush(Colors.Red)));

                comfortSeries = new LineSeries();//(LineSeries)chart1.FindName("series2");
                comfortSeries.IndependentValueBinding = new System.Windows.Data.Binding("day");
                comfortSeries.DependentValueBinding = new System.Windows.Data.Binding("val");
                comfortSeries.ItemsSource = comfort;
                comfortSeries.DataPointStyle = dataPointStyle1;
                comfortSeries.Name = "series2";
                comfortSeries.Title = "Comfort";

                Style dataPointStyle2 = new System.Windows.Style(typeof(DataPoint));
                dataPointStyle2.Setters.Add(new Setter(DataPoint.BackgroundProperty, new SolidColorBrush(Colors.Yellow)));

                energySeries = new LineSeries();//(LineSeries)chart1.FindName("series3");
                energySeries.IndependentValueBinding = new System.Windows.Data.Binding("day");
                energySeries.DependentValueBinding = new System.Windows.Data.Binding("val");
                energySeries.ItemsSource = energy;
                energySeries.DataPointStyle = dataPointStyle2;
                energySeries.Name = "series3";
                energySeries.Title = "Energy";


                chart1.Series.Add(comfortSeries);
                chart1.Series.Add(energySeries);
            }
            else
            {
                comfortSeries = (LineSeries)chart1.FindName("series2");
                energySeries = (LineSeries)chart1.FindName("series3");
            }

            List<StatPlot> f = new List<StatPlot>();
            chart1.DataContext = f;



            switch (plotDate)
            {
                case "pastWeek":

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Date >= DateTime.UtcNow.Date.AddDays(-7).Date)
                        {
                            energy.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[2]) });
                            comfort.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[3]) });
                            maintenance.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[4]) });
                        }
                    }

                    comfortSeries.ItemsSource = comfort;
                    energySeries.ItemsSource = energy;
                    energySeries.DataContext = energy;
                    comfortSeries.DataContext = comfort;
                    maintenanceSeries.DataContext = maintenance;
                    //chart1.DataContext = faults;
                    chart1.UpdateLayout();
                    break;
                case "past30":

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        energy.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[2]) });
                        comfort.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[3]) });
                        maintenance.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[4]) });
                    }

                    comfortSeries.ItemsSource = comfort;
                    energySeries.ItemsSource = energy;
                    energySeries.DataContext = energy;
                    comfortSeries.DataContext = comfort;
                    maintenanceSeries.DataContext = maintenance;
                    chart1.UpdateLayout();
                    break;
                case "currWeek":

                    List<StatPlot> energy3 = new List<StatPlot>();
                    List<StatPlot> comfort3 = new List<StatPlot>();
                    List<StatPlot> maintenance3 = new List<StatPlot>();

                    DateTime start = DateTime.UtcNow.Date;
                    while (start.DayOfWeek != DayOfWeek.Monday)
                    {
                        start = start.AddDays(-1);
                    }

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Date >= start.Date)
                        {
                            energy.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[2]) });
                            comfort.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[3]) });
                            maintenance.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[4]) });
                        }
                    }

                    comfortSeries.ItemsSource = comfort;
                    energySeries.ItemsSource = energy;
                    energySeries.DataContext = energy;
                    comfortSeries.DataContext = comfort;
                    maintenanceSeries.DataContext = maintenance;
                    chart1.UpdateLayout();
                    break;
                case "currMonth":

                    List<StatPlot> energy4 = new List<StatPlot>();
                    List<StatPlot> comfort4 = new List<StatPlot>();
                    List<StatPlot> maintenance4 = new List<StatPlot>();

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in monthData)
                    {
                        if (Convert.ToDateTime(i[5]).Month == DateTime.UtcNow.Month)
                        {
                            energy.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[2]) });
                            comfort.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[3]) });
                            maintenance.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[4]) });
                        }
                    }

                    comfortSeries.ItemsSource = comfort;
                    energySeries.ItemsSource = energy;
                    energySeries.DataContext = energy;
                    comfortSeries.DataContext = comfort;
                    maintenanceSeries.DataContext = maintenance;
                    chart1.UpdateLayout();
                    break;

                case "pastYear":

                    List<StatPlot> energy5 = new List<StatPlot>();
                    List<StatPlot> comfort5 = new List<StatPlot>();
                    List<StatPlot> maintenance5 = new List<StatPlot>();

                    foreach (System.Collections.ObjectModel.ObservableCollection<object> i in yearData)
                    {
                        energy.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[2]) });
                        comfort.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[3]) });
                        maintenance.Add(new StatPlot() { day = Convert.ToDateTime(i[5]).Date, val = Convert.ToDouble(i[4]) });

                    }

                    comfortSeries.ItemsSource = comfort;
                    energySeries.ItemsSource = energy;
                    energySeries.DataContext = energy;
                    comfortSeries.DataContext = comfort;
                    maintenanceSeries.DataContext = maintenance;
                    chart1.UpdateLayout();
                    break;

            }
            chart1.UpdateLayout();
        }

        #endregion
    }
}
