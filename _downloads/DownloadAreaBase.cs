﻿using CW.Business;
using CW.Reporting._fileservices;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;

namespace CW.Website._downloads
{
    public abstract class DownloadAreaBase<TEntity, TGenerator> : SiteUserControl where TEntity : new() where TGenerator : IFileGenerator
    {
        #region properties

            #region overrides

                protected abstract Func<Dictionary<string, object>, IEnumerable<TEntity>> GetData { get; }
                protected abstract Dictionary<string, object> Parameters { get; }

            #endregion

            public DropDownList DropDownList { set; get; }
            public List<DropDownList> DropDownLists { set; get; }
            public string Criteria { get; set; }
            protected bool IsKGSUserAdminPageValue { get { return (IsKGSUserAdminPage.HasValue) ? IsKGSUserAdminPage.Value : false; } }
            public bool? IsKGSUserAdminPage { set; private get; }

            protected IFileGenerator FileGenerator
            { 
                get 
                {
                    var objectList = (IsKGSUserAdminPage != null) ? new object[] { GetData, Parameters, IsKGSUserAdminPage } : new object[] { GetData, Parameters };

                    return Activator.CreateInstance(typeof(TGenerator), objectList) as IFileGenerator; 
                } 
            }

        #endregion

        #region fields

            protected Label lblError;
            protected ImageButton imgDownload;
            protected LinkButton lnkDownload;

        #endregion

        #region events

            #region virtual

                protected virtual void Download_Click(object sender, EventArgs ea)
                {
                    try
                    {
                        var dm = DataManager.Get(HttpContext.Current.Session.SessionID);

                        var file = new FilePublishService
                        (
                            dm.ClientDataMapper.UpdateClientAccountSettings,
                            dm.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                            siteUser.IsKGSFullAdminOrHigher,
                            siteUser.IsSchneiderTheme,
                            siteUser.Email,
                            FileGenerator,
                            LogMgr
                        );

                        lblError.Visible = !file.AttachAsDownloadWithNoDataCheck(new DownloadRequestHandler.HandledFileAttacher());

                        if (lblError.Visible)
                        {
                            lblError.Text = "No data to download.";

                            return;
                        }
                    }
                    catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
                    {
                        lblError.Text = "Max monthly download limit reached.";

                        lblError.Visible = true;

                        return;
                    }
                }

            #endregion

        #endregion

        #region methods

            #region override

                protected override void OnInit(EventArgs e)
                {
                    base.OnInit(e);

                    lnkDownload.Click += Download_Click;

                    imgDownload.Click += Download_Click;
                }

            #endregion

        #endregion
    }
}