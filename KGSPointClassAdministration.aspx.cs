﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.PointClass;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSPointClassAdministration : SitePage
    {
        #region Properties

            private PointClass mPointClass;
            const string addPointClassSuccess = " point class addition was successful.";
            const string addPointClassFailed = "Adding point class failed. Please contact an administrator.";
            const string pointClassExists = "A point class with that name already exists.";
            const string updateSuccessful = "Point class update was successful.";
            const string updateFailed = "Point class update failed. Please contact an administrator.";
            protected string selectedValue;    
            const string deleteSuccessful = "Point class deletion was successful.";
            const string deleteFailed = "Point class deletion failed. Please contact an administrator.";
            const string pointTypeExists = "Cannot delete point class because point type is associated. Please disassociate from point type first.";    
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "PointClassName";
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindPointClasses();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindAllEngUnitsDropdowns();

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetPointClassIntoEditForm(CW.Data.PointClass pointClass)
            {
                //ID
                hdnEditID.Value = Convert.ToString(pointClass.PointClassID);
                //Point Class Name
                txtEditPointClassName.Text = pointClass.PointClassName;

                //IP Eng Unit
                ddlEditIPEngUnits.SelectedValue = Convert.ToString(pointClass.IPEngUnitID);
                //SI Eng Unit
                ddlEditSIEngUnits.SelectedValue = Convert.ToString(pointClass.SIEngUnitID);

                //Point Class Description
                txtEditDescription.Value = String.IsNullOrEmpty(pointClass.PointClassDescription) ? null : pointClass.PointClassDescription;
            }

        #endregion

        #region Load and Bind Fields
            
            private void BindAllEngUnitsDropdowns()
            {
                IEnumerable<EngUnit> engineeringUnits = DataMgr.EngineeringUnitsDataMapper.GetAllEngineeringUnits();

                ddlAddIPEngUnits.DataTextField = ddlAddSIEngUnits.DataTextField = ddlEditIPEngUnits.DataTextField = ddlEditSIEngUnits.DataTextField = "EngUnits";
                ddlAddIPEngUnits.DataValueField = ddlAddSIEngUnits.DataValueField = ddlEditIPEngUnits.DataValueField = ddlEditSIEngUnits.DataValueField = "EngUnitID";
                ddlAddIPEngUnits.DataSource = ddlAddSIEngUnits.DataSource = ddlEditIPEngUnits.DataSource = ddlEditSIEngUnits.DataSource = engineeringUnits;

                ddlAddIPEngUnits.DataBind();
                ddlAddSIEngUnits.DataBind();
                ddlEditIPEngUnits.DataBind();
                ddlEditSIEngUnits.DataBind();
            }

        #endregion

        #region Load Point Class

            protected void LoadAddFormIntoPointClass(PointClass pointClass)
            {
                //Point Class Name
                pointClass.PointClassName = txtAddPointClassName.Text;
                pointClass.IPEngUnitID = Convert.ToInt32(ddlAddIPEngUnits.SelectedValue);
                pointClass.SIEngUnitID = Convert.ToInt32(ddlAddSIEngUnits.SelectedValue);
                pointClass.PointClassDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                pointClass.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoPointClass(PointClass pointClass)
            {
                //ID
                pointClass.PointClassID = Convert.ToInt32(hdnEditID.Value);
                //Point Class Name
                pointClass.PointClassName = txtEditPointClassName.Text;
                pointClass.PointClassDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                pointClass.IPEngUnitID = Convert.ToInt32(ddlEditIPEngUnits.SelectedValue);
                pointClass.SIEngUnitID = Convert.ToInt32(ddlEditSIEngUnits.SelectedValue);
                pointClass.DateModified = DateTime.UtcNow;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add point class Button on click.
            /// </summary>
            protected void addPointClassButton_Click(object sender, EventArgs e)
            {  
                //check that point class does not already exist
                if (!DataMgr.PointClassDataMapper.DoesPointClassExist(txtAddPointClassName.Text))
                {
                    mPointClass = new PointClass();

                    //load the form into the point class
                    LoadAddFormIntoPointClass(mPointClass);

                    try
                    {
                        //insert new point class
                        DataMgr.PointClassDataMapper.InsertPointClass(mPointClass);

                        LabelHelper.SetLabelMessage(lblAddError, mPointClass.PointClassName + addPointClassSuccess, lnkSetFocusAdd);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding point class.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addPointClassFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding point class.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addPointClassFailed, lnkSetFocusAdd);
                    }
                }
                else
                {
                    //point class exists
                    LabelHelper.SetLabelMessage(lblAddError, pointClassExists, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update point class Button on click. Updates point class data.
            /// </summary>
            protected void updatePointClassButton_Click(object sender, EventArgs e)
            {
                PointClass mPointClass = new PointClass();

                //load the form into the point class
                LoadEditFormIntoPointClass(mPointClass);

                //check if the pointclass name exists for another pointclass if trying to change
                if (DataMgr.PointClassDataMapper.DoesPointClassExist(mPointClass.PointClassID, mPointClass.PointClassName))
                {
                    LabelHelper.SetLabelMessage(lblEditError, pointClassExists, lnkSetFocusView);
                }
                //update the point class
                else
                {
                    //try to update the point class            
                    try
                    {
                        DataMgr.PointClassDataMapper.UpdatePointClass(mPointClass);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                        //Bind point classes again
                        BindPointClasses();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating point class.", ex);
                    }
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindPointClasses();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindPointClasses();
            }

        #endregion

        #region Dropdown events

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind point class grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind point classes to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindPointClasses();
                }
            }

        #endregion

        #region Grid Events

            protected void gridPointClasses_OnDataBound(object sender, EventArgs e)
            {                           
            }

            protected void gridPointClasses_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }
            protected void gridPointClasses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditPointClass.Visible = false;
                dtvPointClass.Visible = true;
                
                int pointClassID = Convert.ToInt32(gridPointClasses.DataKeys[gridPointClasses.SelectedIndex].Values["PointClassID"]);

                //set data source
                dtvPointClass.DataSource = DataMgr.PointClassDataMapper.GetFullPointClassByPointClassID(pointClassID);
                //bind point class to details view
                dtvPointClass.DataBind();
            }
            protected void gridPointClasses_Editing(object sender, GridViewEditEventArgs e)
            {
                pnlEditPointClass.Visible = true;
                dtvPointClass.Visible = false;

                int pointClassID = Convert.ToInt32(gridPointClasses.DataKeys[e.NewEditIndex].Values["PointClassID"]);

                PointClass mPointClass = DataMgr.PointClassDataMapper.GetPointClass(pointClassID);

                //Set Point Class data
                SetPointClassIntoEditForm(mPointClass);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }
            protected void gridPointClasses_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows pointClassid
                int pointClassID = Convert.ToInt32(gridPointClasses.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a point class if no point type
                    //has been associated.                    

                    //check if point type is associated to that point class
                    bool hasPointType = DataMgr.PointTypeDataMapper.IsPointTypeAssociatedWithPointClass(pointClassID);

                    if (hasPointType)
                    {
                        lblErrors.Text = pointTypeExists;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //delete point class
                        DataMgr.PointClassDataMapper.DeletePointClass(pointClassID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting point class.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryPointClasses());
                gridPointClasses.PageIndex = gridPointClasses.PageIndex;
                gridPointClasses.DataSource = SortDataTable(dataTable as DataTable, true);
                gridPointClasses.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditPointClass.Visible = false;                
            }

            protected void gridPointClasses_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedPointClasses(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridPointClasses.DataSource = SortDataTable(dataTable, true);
                gridPointClasses.PageIndex = e.NewPageIndex;
                gridPointClasses.DataBind();
            }

            protected void gridPointClasses_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridPointClasses.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedPointClasses(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridPointClasses.DataSource = SortDataTable(dataTable, false);
                gridPointClasses.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetPointClassData> QueryPointClasses()
            {
                try
                {
                    //get all point classes 
                    return DataMgr.PointClassDataMapper.GetAllFullPointClasses();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving point classes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving point classes.", ex);
                    return null;
                }
            }

            private IEnumerable<GetPointClassData> QuerySearchedPointClasses(string searchText)
            {
                try
                {
                    //get all point classes 
                    return DataMgr.PointClassDataMapper.GetAllFullSearchedPointClasses(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving point classes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving point classes.", ex);
                    return null;
                }
            }

            private void BindPointClasses()
            {
                //query point classes
                IEnumerable<GetPointClassData> classes = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryPointClasses() : QuerySearchedPointClasses(txtSearch.Text);

                int count = classes.Count();

                gridPointClasses.DataSource = classes;

                // bind grid
                gridPointClasses.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedPointClasses(string[] searchText)
            //{
            //    //query point classes
            //    IEnumerable<GetPointClassData> classes = QuerySearchedPointClasses(searchText);

            //    int count = classes.Count();

            //    gridPointClasses.DataSource = classes;

            //    // bind grid
            //    gridPointClasses.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} point class found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} point classes found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No point classes found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

        #endregion
    }
}

