﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.Master" AutoEventWireup="false" CodeBehind="KGSSystemTest.aspx.cs" Inherits="CW.Website.KGSSystemTest" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/System/IndividualDataSourceCheck.ascx" tagname="IndividualDataSourceCheck" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/System/DataSourceCheck.ascx" tagname="DataSourceCheck" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/System/PointCheck.ascx" tagname="PointCheck" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/System/PointRangeCheck.ascx" tagname="PointRangeCheck" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/System/AnalysesCheck.ascx" tagname="AnalysesCheck" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <script type="text/javascript" src="_assets/scripts/dates.js"></script>

  <h1>KGS System Test</h1>

  <div class="richText">The kgs system test page is used to perform diagnosic tests on the system.</div>

  <div class="updateProgressDiv">
    <asp:UpdateProgress ID="updateProgressTop" runat="server">
      <ProgressTemplate>
        <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
      </ProgressTemplate>
    </asp:UpdateProgress>
  </div>

  <div class="administrationControls">
    <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
      <Tabs>
        <telerik:RadTab Text="Individual Data Source Check" />
        <telerik:RadTab Text="Data Sources Check" />
        <telerik:RadTab Text="Point Check" />
        <telerik:RadTab Text="Point Range Check" />
        <telerik:RadTab Text="Analyses Check" />
      </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

      <telerik:RadPageView ID="RadPageView1" runat="server">  
        <CW:IndividualDataSourceCheck ID="IndividualDataSourceCheck" runat="server" />
      </telerik:RadPageView>

      <telerik:RadPageView ID="RadPageView2" runat="server">
        <CW:DataSourceCheck ID="DataSourceCheck" runat="server" />
      </telerik:RadPageView>

      <telerik:RadPageView ID="RadPageView3" runat="server">
        <CW:PointCheck ID="PointCheck" runat="server" />
      </telerik:RadPageView>

      <telerik:RadPageView ID="RadPageView4" runat="server">
        <CW:PointRangeCheck ID="PointRangeCheck" runat="server" />
      </telerik:RadPageView>

      <telerik:RadPageView ID="RadPageView6" runat="server">
        <CW:AnalysesCheck ID="AnalysesCheck" runat="server" />
      </telerik:RadPageView>

    </telerik:RadMultiPage>
  </div>

</asp:Content>
