﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.EquipmentPoints;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._controls;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class PointAdministration : SitePage
    {
        #region Properties
           
            protected string selectedValue;
            const string updateSuccessful = "Point update was successful.";
            const string updateFailed = "Point update failed. Please contact an administrator.";    
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "PointName";

            const string referenceIDExistsChangeFailed = "Cannot change reference id because the same reference id already exists for this datasource.";

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //hide labels
                lblErrors.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {                    
                    dropDowns.BindBuildingList(siteUser.VisibleBuildings);               

                    //default view by
                    //dropDowns.GetDivContainer(_controls.DropDowns.ViewModeEnum.Equipment).Visible = false;

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litAdminPointsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.AdminPointsBody);

                    sessionState["Search"] = String.Empty;
                }

                PointDownloadArea.DropDownList = dropDowns.GetDDL(DropDowns.ViewModeEnum.Building);
            }
        
        #endregion

        #region Set Fields and Data

            protected void SetEquipmentPointIntoEditForm(GetEquipmentPointsData equipment_point)
            {
                //ID
                hdnEditID.Value = Convert.ToString(equipment_point.PID);
                //Point Name
                txtEditPointName.Text = equipment_point.PointName;
                //ReferenceID
                txtEditReferenceID.Text = equipment_point.ReferenceID.Trim();
                hdnEditReferenceID.Value = equipment_point.ReferenceID.Trim();
                hdnEditDataSourceID.Value = equipment_point.DSID.ToString();
            }

        #endregion

        #region Load and Bind Fields

        #endregion

        #region Load Point

            protected void LoadEditFormIntoPoint(Point point)
            {
                //ID
                point.PID = Convert.ToInt32(hdnEditID.Value);
                //Point Name
                point.PointName = txtEditPointName.Text;
                //ReferenceID
                point.ReferenceID = txtEditReferenceID.Text.Trim();                
            }

        #endregion

        #region Dropdown Events

            protected void dropDowns_OnBuildingChanged(object sender, EventArgs e)
            {
                var isBidSelected = dropDowns.BID != null;

                ControlHelper.ToggleVisibility(isBidSelected, new Control[] { PointDownloadArea });
                PointDownloadArea.FindControl("lblError").Visible = false;

                if (!isBidSelected)
                {
                    pnlSearch.Visible = false;

                    return;
                }

                dropDowns.BindEquipmentClassList(DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(dropDowns.BID.Value));

                BindPoints();
            }

            protected void dropDowns_OnEquipmentClassChanged(Object sender, EventArgs e)
            {
                if (dropDowns.EquipmentClassID == null)
                {
                    pnlSearch.Visible = false;

                    return;
                }

                dropDowns.BindEquipmentList(DataMgr.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(dropDowns.EquipmentClassID.Value, dropDowns.BID.Value));

                BindPoints();
            }

            protected void dropDowns_OnEquipmentChanged(Object sender, EventArgs e)
            {
                pnlSearch.Visible = (dropDowns.EID != null);

                if (dropDowns.EID == null) return;

                BindPoints();
            }

        #endregion

        #region Grid Events

            protected void gridPoints_OnDataBound(object sender, EventArgs e)
            {
                //check if super admin or higher, hide edit column if not
                gridPoints.Columns[1].Visible = siteUser.IsSuperAdminOrHigher;                                    
            }

            protected void gridPoints_OnSelectedIndexChanged(object sender, EventArgs e)
            {                
                dtvPoint.Visible = true;
                
                int epid = Convert.ToInt32(gridPoints.DataKeys[gridPoints.SelectedIndex].Values["EPID"]);

                //set data source
                dtvPoint.DataSource = DataMgr.EquipmentPointsDataMapper.GetIEnumerablePartialEquipmentPointByEPID(epid);
                //bind equipment_point to details view
                dtvPoint.DataBind();
            }

            protected void gridPoints_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }


            protected void gridPoints_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvPoint.Visible = false;
                pnlEditPoint.Visible = true;

                int epid = Convert.ToInt32(gridPoints.DataKeys[e.NewEditIndex].Values["EPID"]);

                //get equipment_point by pid
                GetEquipmentPointsData mEquipment_Point = DataMgr.EquipmentPointsDataMapper.GetPartialEquipmentPointByEPID(epid);

                //Set equipment_point data
                SetEquipmentPointIntoEditForm(mEquipment_Point);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridPoints_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedPoints(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridPoints.DataSource = SortDataTable(dataTable, true);
                gridPoints.PageIndex = e.NewPageIndex;
                gridPoints.DataBind();
            }

            protected void gridPoints_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridPoints.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedPoints(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridPoints.DataSource = SortDataTable(dataTable, false);
                gridPoints.DataBind();
            }
        
        #endregion

        #region Button Events

            /// <summary>
            /// Update Point Button on click.
            /// </summary>
            protected void updatePointButton_Click(object sender, EventArgs e)
            {
                //multiple points can have the same name.

                var mPoint = new Point();
           
                LoadEditFormIntoPoint(mPoint);
                
                //TEMP hidden DSID
                mPoint.DSID = Convert.ToInt32(hdnEditDataSourceID.Value);
                
                //check if reference id already exists for this data source
                if (mPoint.ReferenceID != hdnEditReferenceID.Value && DataMgr.PointDataMapper.DoesPointReferenceIDExistForDataSource(mPoint.DSID, mPoint.ReferenceID))
			    {
				    lblEditError.Text = referenceIDExistsChangeFailed;
                    lblEditError.Visible = true;
                    lnkSetFocusEdit.Focus();
                    return;
				}


                //try to update the point              
                try
                {
                    //update point
                    DataMgr.PointDataMapper.UpdatePartialAdminPoint(mPoint);

                    //update hidden variable to new values for multiple updates in a row
                    hdnEditReferenceID.Value = mPoint.ReferenceID;

                    lblEditError.Text = updateSuccessful;
                    lblEditError.Visible = true;
                    lnkSetFocusEdit.Focus();

                    //Bind points again
                    ReBindPoints();
                }
                catch (Exception ex)
                {
                    lblEditError.Text = updateFailed;
                    lblEditError.Visible = true;
                    lnkSetFocusEdit.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating point.", ex);
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindPoints();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindPoints();
            }

        #endregion

        #region Helper Methods

            private void BindPoints()
            {
                //query point
                IEnumerable<GetEquipmentPointsData> points = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryPoints() : QuerySearchedPoints(txtSearch.Text);

                int count = points.Count();

                gridPoints.DataSource = points;

                //bind grid
                gridPoints.DataBind();

                //set count label
                SetGridCountLabel(points.Count());
            }

            private void ReBindPoints()
            {
                //query point
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryPoints() : QuerySearchedPoints(txtSearch.Text));

                gridPoints.PageIndex = gridPoints.PageIndex;
                gridPoints.DataSource = SortDataTable(dataTable as DataTable, true);
                gridPoints.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //set search visibility. dont remove when last result from searched results in the grid is deleted.
                pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && dataTable.Rows.Count == 0) ? false : true;
            }

            private IEnumerable<GetEquipmentPointsData> QueryPoints()
            {
                try
                {
                    if (dropDowns.EID != null && dropDowns.EID.Value > 0)
                        return DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointsWithPartialDataByEID(dropDowns.EID.Value);

                    if (dropDowns.EquipmentClassID != null && dropDowns.EquipmentClassID.Value > 0)
                        return DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointsWithPartialDataByBIDAndEquipmentClassID(dropDowns.BID.Value, dropDowns.EquipmentClassID.Value);

                     if (dropDowns.BID != null && dropDowns.BID.Value > 0)
                         return DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointsWithPartialDataByBID(dropDowns.BID.Value);
                    
                    return null;
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment_points.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment_points.", ex);
                    return null;
                }
            }

            private IEnumerable<GetEquipmentPointsData> QuerySearchedPoints(string searchText)
            {
                try
                {
                    if (dropDowns.EID != null && dropDowns.EID > 0)
                        return DataMgr.EquipmentPointsDataMapper.SearchAllEquipmentPointsByEID(searchText, dropDowns.EID.Value);

                    if (dropDowns.EquipmentClassID != null && dropDowns.EquipmentClassID > 0)
                        return DataMgr.EquipmentPointsDataMapper.SearchAllEquipmentPointsByBIDAndEquipmentClassID(searchText, dropDowns.BID.Value, dropDowns.EquipmentClassID.Value);

                    if (dropDowns.BID != null && dropDowns.BID > 0)
                        return DataMgr.EquipmentPointsDataMapper.SearchAllEquipmentPointsByBID(searchText, dropDowns.BID.Value);

                    return null;
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving searched equipment_points.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving searched equipment_points.", ex);
                    return null;
                }
            }


            //private void BindSearchedPoints(string[] searchText)
            //{
            //    //query point
            //    IEnumerable<GetEquipmentPointsData> points = QuerySearchedPoints(searchText);
               
            //    gridPoints.DataSource = points;

            //    //bind grid
            //    gridPoints.DataBind();

            //    //set count label
            //    SetGridCountLabel(points.Count());
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} point found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} points found.", count);
                }
                else
                {
                    lblResults.Text = "No points found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
      
        #endregion
    }
}