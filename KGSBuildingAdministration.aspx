﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSBuildingAdministration.aspx.cs" Inherits="CW.Website.KGSBuildingAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/Buildings/ViewBuildings.ascx" tagname="ViewBuildings" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Buildings/AddBuilding.ascx" tagname="AddBuilding" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Buildings/BuildingSettings.ascx" tagname="BuildingSettings" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Buildings/BuildingVariables.ascx" tagname="BuildingVariables" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Buildings/BuildingVariablesToMultipleBuildings.ascx" tagname="BuildingVariablesToMultipleBuildings" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Buildings/BuildingCMMSSettings.ascx" TagName="BuildingCMMSSettings" TagPrefix="CW"  %>
<%@ Register src="~/_controls/admin/Buildings/BuildingStats.ascx" tagname="BuildingStats" tagprefix="CW" %>
<%@ Register src="~/_administration/building/BuildingGoalsSettingsTab.ascx" tagPrefix="CW" tagName="BuildingGoalsSettingsTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <script type="text/javascript" src="/_administration/_script/TabStateMonitor.js?v=_assets/"></script>

      <h1>KGS Building Administration</h1>

      <div class="richText">The kgs building administration area is used to view, add, and edit buildings.</div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>   
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div>

      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Buildings" />
            <telerik:RadTab Text="Add Building" />
            <telerik:RadTab Text="Building Settings" />
            <telerik:RadTab Text="Building Variables To A Building" />
            <telerik:RadTab Text="Building Variables To Multiple Buildings" />                     
            <telerik:RadTab Text="Building WO Settings" />
            <telerik:RadTab Text="Building Goal Settings" />
            <telerik:RadTab Text="Stats" />               
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:ViewBuildings ID="ViewBuildings" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:AddBuilding ID="AddBuilding" runat="server" />
          </telerik:RadPageView>  
                                                      
          <telerik:RadPageView ID="RadPageView3" runat="server">   
            <CW:BuildingSettings ID="BuildingSettings" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView4" runat="server">
            <CW:BuildingVariables ID="BuildingVariables" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView5" runat="server">
            <CW:BuildingVariablesToMultipleBuildings ID="BuildingVariablesToMultipleBuildings" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView6" runat="server">
            <CW:BuildingCMMSSettings ID="BuildingCMMSSettings" runat="server" />
          </telerik:RadPageView>
            
          <telerik:RadPageView ID="RadPageView7" runat="server">
            <CW:BuildingGoalsSettingsTab ID="BuildingGoalsSettingsTab" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView8" runat="server">
            <CW:BuildingStats ID="BuildingStats" runat="server" />
          </telerik:RadPageView>

        </telerik:RadMultiPage>
      </div>

</asp:Content>