﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSEquipmentClassAdministration: SitePage
    {
        #region Properties

            private EquipmentClass mEquipmentClass;
            const string addEquipmentClassSuccess = " equipment class addition was successful.";
            const string addEquipmentClassFailed = "Adding equipment class failed. Please contact an administrator.";
            const string equipmentClassExists = "A equipment class with that name already exists.";
            const string updateSuccessful = "Equipment class update was successful.";
            const string updateFailed = "Equipment class update failed. Please contact an administrator.";
            const string deleteSuccessful = "Equipment class deletion was successful.";
            const string deleteFailed = "Equipment class deletion failed. Please contact an administrator.";
            const string equipmentTypeExists = "Cannot delete equipment class because equipment type is associated. Please disassociate from equipment type first.";
            const string equipmentVariableExists = "Cannot delete equipment class because equipment variable is associated. Please disassociate from equipment variable first."; 
            const string equipmentManufacturerExists = "Cannot delete equipment class because equipment manufacturer is associated. Please disassociate from equipment manufacturer first.";
            const string cantDeleteEquipmentClassGroup = "Cannot delete equipment class 'Group' because it is permanent.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "EquipmentClassName";

            const string reassignVariablesUpdateSuccessful = "Equipment variables reassignment was successful.";
            const string reassignVariablesUpdateFailed = "Equipment variables reassignment failed. Please contact an administrator.";
            const string equipmentRequiresEquipmentVariable = "One or more equipment variables could not be unassigned because an associated equipment with that class requires them.";

            const string reassignManufacturersUpdateSuccessful = "Equipment manufacturers reassignment was successful.";
            const string reassignManufacturersUpdateFailed = "Equipment manufacturers reassignment failed. Please contact an administrator.";
            const string equipmentRequiresEquipmentManufacturer = "One or more equipment manufacturers could not be unassigned because an associated equipment with that class requires them.";

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    //bind grid
                    BindEquipmentClasses();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    //bind dropdowns
                    BindEquipmentClasses(ddlVariablesEquipmentClass, ddlManufacturersEquipmentClass);

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetEquipmentClassIntoEditForm(CW.Data.EquipmentClass equipmentClass)
            {
                //ID
                hdnEditID.Value = Convert.ToString(equipmentClass.EquipmentClassID);
                //Equipment Class Name
                txtEditEquipmentClassName.Text = equipmentClass.EquipmentClassName;
                //Equipment Variable Description
                txtEditDescription.Value = String.IsNullOrEmpty(equipmentClass.EquipmentClassDescription) ? null : equipmentClass.EquipmentClassDescription;
                //Is Group. Uneditable
                lblEditIsGroup.Text = equipmentClass.IsGroup.ToString();
            }

        #endregion

        #region Get Fields and Data
                   
        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Binds all dropdownlists with all equipment classes
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="ddl2"></param>
            private void BindEquipmentClasses(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<EquipmentClass> classes = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();

                ddl.DataTextField = "EquipmentClassName";
                ddl.DataValueField = "EquipmentClassID";
                ddl.DataSource = classes;
                ddl.DataBind();

                ddl2.DataTextField = "EquipmentClassName";
                ddl2.DataValueField = "EquipmentClassID";
                ddl2.DataSource = classes;
                ddl2.DataBind();
            }

            /// <summary>
            /// Binds top and bottom list boxes with equipment variables by equipmentClassID
            /// 
            /// top: all not yet associated to equipmentClassID
            /// bottom: all currently associated to equipmentClassID
            /// </summary>
            /// <param name="equipmentClassID"></param>
            private void BindEquipmentVariables(int equipmentClassID)
            {
                //get equipment variables associated to equipmentClassID 
                IEnumerable<EquipmentVariable> mEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesAssociatedToEquipmentClassID(equipmentClassID);

                if (mEquipmentVariables.Any())
                {
                    //bind equipment variables to bottom listbox
                    lbVariablesBottom.DataSource = mEquipmentVariables;
                    lbVariablesBottom.DataTextField = "EquipmentVariableDisplayName";
                    lbVariablesBottom.DataValueField = "EVID";
                    lbVariablesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbVariablesBottom.Items.Clear();
                }

                //get equipment variables not associated to equipment, but associated to
                //the equipment class
                mEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesNotAssociatedToEquipmentClassID(equipmentClassID);

                if (mEquipmentVariables.Any())
                {
                    //bind equipment variables to top listbox
                    lbVariablesTop.DataSource = mEquipmentVariables;
                    lbVariablesTop.DataTextField = "EquipmentVariableDisplayName";
                    lbVariablesTop.DataValueField = "EVID";
                    lbVariablesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbVariablesTop.Items.Clear();
                }

                equipmentVariables.Visible = true;
            }

            /// <summary>
            /// Binds top and bottom list boxes with equipment manufacturers by equipmentClassID
            /// 
            /// top: all not yet associated to equipmentClassID
            /// bottom: all currently associated to equipmentClassID
            /// </summary>
            /// <param name="equipmentClassID"></param>
            private void BindEquipmentManufacturers(int equipmentClassID)
            {
                //get equipment variables associated to equipmentClassID 
                IEnumerable<EquipmentManufacturer> mEquipmentManufacturers = DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturersAssociatedToEquipmentClassID(equipmentClassID);

                if (mEquipmentManufacturers.Any())
                {
                    //bind equipment manufacturers to bottom listbox
                    lbManufacturersBottom.DataSource = mEquipmentManufacturers;
                    lbManufacturersBottom.DataTextField = "ManufacturerName";
                    lbManufacturersBottom.DataValueField = "ManufacturerID";
                    lbManufacturersBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbManufacturersBottom.Items.Clear();
                }

                //get equipment manufacturers not associated to equipment, but associated to
                //the equipment class
                mEquipmentManufacturers = DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturersNotAssociatedToEquipmentClassID(equipmentClassID);

                if (mEquipmentManufacturers.Any())
                {
                    //bind equipment manufacturers to top listbox
                    lbManufacturersTop.DataSource = mEquipmentManufacturers;
                    lbManufacturersTop.DataTextField = "ManufacturerName";
                    lbManufacturersTop.DataValueField = "ManufacturerID";
                    lbManufacturersTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbManufacturersTop.Items.Clear();
                }

                equipmentManufacturers.Visible = true;
            }

        #endregion

        #region Load Equipment Class

            protected void LoadAddFormIntoEquipmentClass(EquipmentClass equipmentClass)
            {
                //Equipment Class Name
                equipmentClass.EquipmentClassName = txtAddEquipmentClassName.Text;
                equipmentClass.EquipmentClassDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                equipmentClass.IsGroup = chkAddIsGroup.Checked;
                equipmentClass.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoEquipmentClass(EquipmentClass equipmentClass)
            {
                //ID
                equipmentClass.EquipmentClassID = Convert.ToInt32(hdnEditID.Value);
                //Equipment Class Name
                equipmentClass.EquipmentClassName = txtEditEquipmentClassName.Text;
                equipmentClass.EquipmentClassDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                equipmentClass.DateModified = DateTime.UtcNow;

                //cannot change isgroup
            }

        #endregion
        
        #region Dropdown Events

            /// <summary>
            /// ddlVariablesEquipmentClass on selected index changed, binds equipment variables list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlVariablesEquipmentClass_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int equipmentClassID = Convert.ToInt32(ddlVariablesEquipmentClass.SelectedValue);

                if (equipmentClassID != -1)
                {
                    BindEquipmentVariables(equipmentClassID);                    
                }
                else
                {
                    //hide equipment variables
                    equipmentVariables.Visible = false;
                }

                //hide error message
                lblVariablesError.Visible = false;
            }

            /// <summary>
            /// ddlManufacturersEquipmentClass on selected index changed, binds equipment manufacturers list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManufacturersEquipmentClass_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int equipmentClassID = Convert.ToInt32(ddlManufacturersEquipmentClass.SelectedValue);

                if (equipmentClassID != -1)
                {
                    BindEquipmentManufacturers(equipmentClassID);
                }
                else
                {
                    //hide equipment manufacturers
                    equipmentManufacturers.Visible = false;
                }

                //hide error message
                lblManufacturersError.Visible = false;
            }
        
        #endregion

        #region Button Events

            /// <summary>
            /// Add Equipment class Button on click.
            /// </summary>
            protected void addEquipmentClassButton_Click(object sender, EventArgs e)
            {
                //check that equipment class does not already exist
                if (!DataMgr.EquipmentClassDataMapper.DoesEquipmentClassExist(txtAddEquipmentClassName.Text))
                {
                    mEquipmentClass = new EquipmentClass();

                    //load the form into the equipment class
                    LoadAddFormIntoEquipmentClass(mEquipmentClass);

                    try
                    {
                        //insert new equipment class
                        DataMgr.EquipmentClassDataMapper.InsertEquipmentClass(mEquipmentClass);

                        LabelHelper.SetLabelMessage(lblAddError, mEquipmentClass.EquipmentClassName + addEquipmentClassSuccess, lnkSetFocusAdd);;
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment class.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addEquipmentClassFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment class.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addEquipmentClassFailed, lnkSetFocusAdd);
                    }
                }
                else
                {
                    //equipment class exists
                    LabelHelper.SetLabelMessage(lblAddError, equipmentClassExists, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update equipment class Button on click. Updates equipment class data.
            /// </summary>
            protected void updateEquipmentClassButton_Click(object sender, EventArgs e)
            {
                EquipmentClass mEquipmentClass = new EquipmentClass();

                //load the form into the equipment class
                LoadEditFormIntoEquipmentClass(mEquipmentClass);

                //check if the equipmentclass name exists for another equipmentclass if trying to change
                if (DataMgr.EquipmentClassDataMapper.DoesEquipmentClassExist(mEquipmentClass.EquipmentClassID, mEquipmentClass.EquipmentClassName))
                {
                    LabelHelper.SetLabelMessage(lblEditError, equipmentClassExists, lnkSetFocusView);
                }
                //update the equipment class
                else
                {
                    //try to update the equipment class              
                    try
                    {
                        DataMgr.EquipmentClassDataMapper.UpdateEquipmentClass(mEquipmentClass);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                        //Bind equipment classes again
                        BindEquipmentClasses();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment class.", ex);
                    }
                }
            }

            /// <summary>
            /// Update equipment variables button click, reassigns variables to equipment class based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateEquipmentVariablesButton_Click(object sender, EventArgs e)
            {
                int equipmentClassID = Convert.ToInt32(ddlVariablesEquipmentClass.SelectedValue);
                int evid;
                bool equipVarInUse = false;

                try
                {
                    //inserting each equipment variable in the bottom listbox if they dont exist
                    foreach (ListItem item in lbVariablesBottom.Items)
                    {
                        //declare new equipment class variable
                        EquipmentClasses_EquipmentVariable mEquipmentVariable = new EquipmentClasses_EquipmentVariable();

                        evid = Convert.ToInt32(item.Value);
                        mEquipmentVariable.EquipmentClassID = equipmentClassID;
                        mEquipmentVariable.EVID = evid;             

                        //if the equipment variable doesnt already exist, insert
                        if (!DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableExistForEquipmentClass(equipmentClassID, evid))
                        {
                            DataMgr.EquipmentVariableDataMapper.InsertEquipmentVariable(mEquipmentVariable);
                        }
                    }

                    //deleting each equipment variable in the top listbox if they exist
                    foreach (ListItem item in lbVariablesTop.Items)
                    {
                        evid = Convert.ToInt32(item.Value);

                        //if the equipment variable exists, delete
                        if (DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableExistForEquipmentClass(equipmentClassID, evid))
                        {
                            //checks if the equipment variable is associated to any equipment with class id
                            //Note: On equipment to equipment variable page it checks if an analysis is assigned to the equipment that required that equipment variable.
                            if (!DataMgr.EquipmentVariableDataMapper.IsEquipmentVariableAssociatedToEquipmentByEquipmentClassID(evid, equipmentClassID))
                            {
                                DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariableByEquipmentClassIDAndEVID(equipmentClassID, evid);
                            }
                            else
                            {
                                equipVarInUse = true;
                            }
                        }
                    }

                    LabelHelper.SetLabelMessage(lblVariablesError, equipVarInUse ? equipmentRequiresEquipmentVariable : reassignVariablesUpdateSuccessful, lnkSetFocusVariables);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment variables to equipment class.", sqlEx);
                    LabelHelper.SetLabelMessage(lblVariablesError, reassignVariablesUpdateFailed, lnkSetFocusVariables);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment variables to equipment class.", ex);
                    LabelHelper.SetLabelMessage(lblVariablesError, reassignVariablesUpdateFailed, lnkSetFocusVariables);
                }
            }

            /// <summary>
            /// on button up click, remove equipment variables from bottom (unassociate to equip class)
            /// </summary>
            protected void btnVariablesUpButton_Click(object sender, EventArgs e)
            {
                while (lbVariablesBottom.SelectedIndex != -1)
                {
                    lbVariablesTop.Items.Add(lbVariablesBottom.SelectedItem);
                    lbVariablesBottom.Items.Remove(lbVariablesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add equipment variables to bottom (associate to equip class)
            /// </summary>
            protected void btnVariablesDownButton_Click(object sender, EventArgs e)
            {
                while (lbVariablesTop.SelectedIndex != -1)
                {
                    {
                        lbVariablesBottom.Items.Add(lbVariablesTop.SelectedItem);
                        lbVariablesTop.Items.Remove(lbVariablesTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// Update equipment manufacturers button click, reassigns manufacturers to equipment class based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateEquipmentManufacturersButton_Click(object sender, EventArgs e)
            {
                int equipmentClassID = Convert.ToInt32(ddlManufacturersEquipmentClass.SelectedValue);
                int manufacturerID;
                bool equipManufacturerInUse = false;

                try
                {
                    //inserting each equipment manufacturer in the bottom listbox if they dont exist
                    foreach (ListItem item in lbManufacturersBottom.Items)
                    {
                        //declare new equipment class manufacturer
                        EquipmentClasses_EquipmentManufacturer mEquipmentManufacturer = new EquipmentClasses_EquipmentManufacturer();

                        manufacturerID = Convert.ToInt32(item.Value);
                        mEquipmentManufacturer.EquipmentClassID = equipmentClassID;
                        mEquipmentManufacturer.ManufacturerID = manufacturerID;

                        //if the equipment manufacturer doesnt already exist, insert
                        if (!DataMgr.EquipmentManufacturerDataMapper.DoesEquipmentManufacturerExistForEquipmentClass(equipmentClassID, manufacturerID))
                        {
                            DataMgr.EquipmentManufacturerDataMapper.InsertEquipmentManufacturer(mEquipmentManufacturer);
                        }
                    }

                    //deleting each equipment manufacturer in the top listbox if they exist
                    foreach (ListItem item in lbManufacturersTop.Items)
                    {
                        manufacturerID = Convert.ToInt32(item.Value);

                        //if the equipment manufacturer exists, delete
                        if (DataMgr.EquipmentManufacturerDataMapper.DoesEquipmentManufacturerExistForEquipmentClass(equipmentClassID, manufacturerID))
                        {
                            //checks if the equipment manufacturer is associated to any equipment with class id                        
                            if (!DataMgr.EquipmentManufacturerDataMapper.IsEquipmentAssociatedWithEquipmentManufacturerByEquipmentClassID(manufacturerID, equipmentClassID))
                            {
                                DataMgr.EquipmentManufacturerDataMapper.DeleteEquipmentManufacturerByEquipmentClassIDAndManufacturerID(equipmentClassID, manufacturerID);
                            }
                            else
                            {
                                equipManufacturerInUse = true;
                            }
                        }
                    }

                    LabelHelper.SetLabelMessage(lblManufacturersError, equipManufacturerInUse ? equipmentRequiresEquipmentManufacturer : reassignManufacturersUpdateSuccessful, lnkSetFocusManufacturers);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment manufacturers to equipment class.", sqlEx);
                    LabelHelper.SetLabelMessage(lblManufacturersError, reassignManufacturersUpdateFailed, lnkSetFocusManufacturers);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment manufacturers to equipment class.", ex);
                    LabelHelper.SetLabelMessage(lblManufacturersError, reassignManufacturersUpdateFailed, lnkSetFocusManufacturers);
                }
            }

            /// <summary>
            /// on button up click, remove equipment manufacturers from bottom (unassociate to equip class)
            /// </summary>
            protected void btnManufacturersUpButton_Click(object sender, EventArgs e)
            {
                while (lbManufacturersBottom.SelectedIndex != -1)
                {
                    lbManufacturersTop.Items.Add(lbManufacturersBottom.SelectedItem);
                    lbManufacturersBottom.Items.Remove(lbManufacturersBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add equipment manufacturers to bottom (associate to equip class)
            /// </summary>
            protected void btnManufacturersDownButton_Click(object sender, EventArgs e)
            {
                while (lbManufacturersTop.SelectedIndex != -1)
                {
                    {
                        lbManufacturersBottom.Items.Add(lbManufacturersTop.SelectedItem);
                        lbManufacturersTop.Items.Remove(lbManufacturersTop.SelectedItem);
                    }
                }
            }
            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindEquipmentClasses();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindEquipmentClasses();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind equipment class grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind equipment classes to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindEquipmentClasses();
                }
            }

        #endregion

        #region Grid Events

            protected void gridEquipmentClasses_OnDataBound(object sender, EventArgs e)
            {
                //CANNOT DELETE CERTAIN CLASSES
                GridViewRowCollection rows = gridEquipmentClasses.Rows;

                foreach (GridViewRow row in rows)
                {
                    if ((row.RowState & DataControlRowState.Edit) == 0)
                    {
                        //hide delete link for undeletable equipment classes
                        try
                        {
                            //get rows datakey which right now is just one single key. pointtype id.
                            if (BusinessConstants.EquipmentClass.UnDeletableEquipmentClassesDictionary.ContainsValue(Convert.ToInt32(((GridView)sender).DataKeys[row.RowIndex].Value)))
                            {
                                //hide delete link
                                ((LinkButton)row.Cells[5].Controls[1]).Visible = false;
                            }
                        }
                        catch
                        {
                        }
                    }
                }         
            }

            protected void gridEquipmentClasses_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridEquipmentClasses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditEquipmentClass.Visible = false;
                dtvEquipmentClass.Visible = true;
                
                int equipmentClassID = Convert.ToInt32(gridEquipmentClasses.DataKeys[gridEquipmentClasses.SelectedIndex].Values["EquipmentClassID"]);

                //set data source
                dtvEquipmentClass.DataSource = DataMgr.EquipmentClassDataMapper.GetEquipmentClassByEquipmentClassID(equipmentClassID);
                //bind equipment variable to details view
                dtvEquipmentClass.DataBind();
            }

            protected void gridEquipmentClasses_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvEquipmentClass.Visible = false;
                pnlEditEquipmentClass.Visible = true;

                int equipmentClassID = Convert.ToInt32(gridEquipmentClasses.DataKeys[e.NewEditIndex].Values["EquipmentClassID"]);

                EquipmentClass mEquipmentClass = DataMgr.EquipmentClassDataMapper.GetEquipmentClass(equipmentClassID);

                //Set Equipment Variable data
                SetEquipmentClassIntoEditForm(mEquipmentClass);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridEquipmentClasses_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows equipmentClassid
                int equipmentClassID = Convert.ToInt32(gridEquipmentClasses.DataKeys[e.RowIndex].Value);

                try
                {
                    //DEPRICATED GROUP CHECK
                    //don't allow deletiong of equipment class 'group'
                    //if (mDataManager.EquipmentClassDataMapper.IsEquipmentClassGroup(equipmentClassID))
                    //{
                    //    lblErrors.Text = cantDeleteEquipmentClassGroup;
                    //    lblErrors.Visible = true;
                    //    lnkSetFocusView.Focus();
                    //}
                    //else
                    //{
                        //Only allow deletion of a equipment class if no equipment type
                        //has been associated.                    

                        //check if equipment type is associated to that equipment class
                        //equipment type deletion checks if associated with equipment before alowing deleteion of the type on the equipment type admin page
                        if (DataMgr.EquipmentTypeDataMapper.IsEquipmentTypeAssociatedWithEquipmentClass(equipmentClassID))
                        {
                            lblErrors.Text = equipmentTypeExists;
                        }
                        //checks if any variables are still associated to the class                        
                        else if (DataMgr.EquipmentVariableDataMapper.IsAnyEquipmentVariableAssociatedToEquipmentClass(equipmentClassID))
                        {
                            lblErrors.Text = equipmentVariableExists;
                        }
                        //checks if any manufacturers are still associated to the class                        
                        else if (DataMgr.EquipmentManufacturerDataMapper.IsAnyEquipmentManufacturerAssociatedToEquipmentClass(equipmentClassID))
                        {
                            lblErrors.Text = equipmentManufacturerExists;
                        }
                        else
                        {
                            //delete all equipment variables associated to equipment class
                            //EquipmentVariableDataMapper.DeleteEquipmentVariablesByEquipmentClassID(equipmentClassID);

                            //delete all equipment manufacturers associated to equipment class
                            //EquipmentVariableDataMapper.DeleteEquipmentManufacturersByEquipmentClassID(equipmentClassID);

                            //delete equipment class
                            DataMgr.EquipmentClassDataMapper.DeleteEquipmentClass(equipmentClassID);

                            lblErrors.Text = deleteSuccessful;
                        }

                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    //}
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting equipment class.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryEquipmentClasses());
                gridEquipmentClasses.PageIndex = gridEquipmentClasses.PageIndex;
                gridEquipmentClasses.DataSource = SortDataTable(dataTable as DataTable, true);
                gridEquipmentClasses.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditEquipmentClass.Visible = false;  
            }

            protected void gridEquipmentClasses_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEquipmentClasses(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridEquipmentClasses.DataSource = SortDataTable(dataTable, true);
                gridEquipmentClasses.PageIndex = e.NewPageIndex;
                gridEquipmentClasses.DataBind();
            }

            protected void gridEquipmentClasses_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridEquipmentClasses.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEquipmentClasses(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridEquipmentClasses.DataSource = SortDataTable(dataTable, false);
                gridEquipmentClasses.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<EquipmentClass> QueryEquipmentClasses()
            {
                try
                {
                    //get all equipment classes 
                    return DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment classes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment classes.", ex);
                    return null;
                }
            }

            private IEnumerable<EquipmentClass> QuerySearchedEquipmentClasses(string searchText)
            {
                try
                {
                    //get all equipment classes 
                    return DataMgr.EquipmentClassDataMapper.GetAllSearchedEquipmentClasses(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment classes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment classes.", ex);
                    return null;
                }
            }

            private void BindEquipmentClasses()
            {
                //query equipment classes
                IEnumerable<EquipmentClass> classes = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryEquipmentClasses() : QuerySearchedEquipmentClasses(txtSearch.Text);

                int count = classes.Count();

                gridEquipmentClasses.DataSource = classes;

                // bind grid
                gridEquipmentClasses.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedEquipmentClasses(string[] searchtxt)
            //{
            //    //query equipment classes
            //    IEnumerable<EquipmentClass> classes = QuerySearchedEquipmentClasses(searchtxt);

            //    int count = classes.Count();

            //    gridEquipmentClasses.DataSource = classes;

            //    // bind grid
            //    gridEquipmentClasses.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} equipment class found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} equipment classes found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No equipment classes found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
    
        #endregion
    }
}

