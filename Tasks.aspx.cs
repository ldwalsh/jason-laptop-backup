﻿using CW.Utility;
using CW.Website._framework;
using System.Globalization;
using System.Web.Services;

namespace CW.Website
{
    public partial class Tasks : SitePage
    {
        #region web method(s)

            [WebMethod]
            public static bool IsNumberValid(string num)
            {
                double val;

                var numStyles = NumberStyles.AllowLeadingSign | NumberStyles.AllowTrailingSign | NumberStyles.AllowThousands;

                return double.TryParse(num, numStyles, new CultureInfo(SiteUser.Current.CultureName), out val);
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                var globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                litTasksBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.TasksBody);
            }

            private void Page_Init() 
            { 
                SearchCriteria.ViewTasks = ViewTasks;
                ViewTasks.SearchCriteria = SearchCriteria;
            }

        #endregion
    }
}