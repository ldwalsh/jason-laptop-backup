﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using CW.Business;
using CW.Data;
using CW.Utility;
using CW.Website.Kiosk.Service;
using CW.Data.Extensions;
using System.Diagnostics;
using CW.Data.Models.Building;
using System.Collections.Generic;
using CW.Website._framework;

namespace CW.Website
{
    public partial class SystemInfo : SitePage
    {
        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //logged in security check in master page  

            //set system info by website assembly info
            AssemblyHelper.AssemblyInfo assemblyInfo = AssemblyHelper.GetAssemblyInfo("CW.Website");
            var versionNumberParts = ConfigurationManager.AppSettings["versionNumber"].Split('.');

            if (versionNumberParts.Count() == 4)
            {
                //lblMode.Text = "Beta"; 
                lblVersion.Text = String.Join(".", versionNumberParts.Take(2).ToArray()); // assemblyInfo.Version; 
                lblBuild.Text = versionNumberParts[2];  //assemblyInfo.Build;
                lblReleaseDate.Text = assemblyInfo.BuildDate.ToString();
            }

            litSpecs.Text = String.Format("DefaultCacheRepository:{0},ProcessedDataCacheRepository:{1}",
                                DataMgr.DefaultCacheRepository.GetType().ToString(),
                                DataMgr.ProcessedDataCacheRepository.GetType().ToString()
                                );
        }

        #endregion
    }
}
