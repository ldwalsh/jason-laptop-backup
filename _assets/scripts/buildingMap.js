﻿var locations = new Array();
var map;
var geocodeCredentialsKey;
var userDataforBuildingMap;
var userCulture;
var counter = 0;

//Bing map
function GetMap() {
    map = new Microsoft.Maps.Map(document.getElementById("mapDiv"), { credentials: Base64Decode(geocodeCredentialsKey), mapTypeId: Microsoft.Maps.MapTypeId.automatic });

    var infoboxLayer = new Microsoft.Maps.EntityCollection();
    map.entities.push(infoboxLayer);

    infobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), { visible: false });
    infoboxLayer.push(infobox);

    map.getCredentials(MakeGeocodeRequest);
}

//function to either use the user entered coorindates from the db, or pull from microsoft's BING MAP API if coordinates were not entered
function MakeGeocodeRequest(credentials) {
    for (var bid in userDataforBuildingMap) {
        var building = {
            address:                             userDataforBuildingMap[bid][0],
            city:                                userDataforBuildingMap[bid][1],
            state:                               userDataforBuildingMap[bid][2],
            countryAlpha2Code:                   userDataforBuildingMap[bid][3],
            buildingName:                        userDataforBuildingMap[bid][4],
            faults:                              userDataforBuildingMap[bid][5],
            costs:                               userDataforBuildingMap[bid][6],
            latitude:                            userDataforBuildingMap[bid][7],
            longitude:                           userDataforBuildingMap[bid][8],
            costThresholdValue:                  userDataforBuildingMap[bid][9],
            isProviderView:                      userDataforBuildingMap[bid][10],
            clientName:                          userDataforBuildingMap[bid][11],
            hasDiagnosticAndCommissioningAccess: userDataforBuildingMap[bid][12],
            hasProfileAccess:                    userDataforBuildingMap[bid][13]
        };

        userDataforBuildingMap[bid] = building;

        if (building.latitude && building.longitude)
            SetCoordinatesAndPushpin({ latitude: building.latitude, longitude: building.longitude }, bid);
        else {
            var geocodeRequest = "https://dev.virtualearth.net/REST/v1/Locations?query=" + [building.address, building.city, building.state, building.countryAlpha2Code].join() + "&key=" + credentials + "&jsonp=ParseResults&output=json&jsonso=" + bid;
            CallRestService(geocodeRequest, true);
        }
    }
}

//function called when data is returned from the rest service.  lat/long values are passed to the SetCoordinatesAndPushin function
function ParseResults(result, bid) {
    if (result && result.resourceSets && result.resourceSets.length > 0 && result.resourceSets[0].resources && result.resourceSets[0].resources.length > 0) {
        var coordinates = result.resourceSets[0].resources[0].point.coordinates;
        SetCoordinatesAndPushpin({ latitude: coordinates[0], longitude: coordinates[1] }, bid);
    }
}

//function that will place the locations on the map via coordinates (lat/long), then will set the pushpin information for each infobox
function SetCoordinatesAndPushpin(coordinates, bid) {

    var building = userDataforBuildingMap[bid];
    var location = new Microsoft.Maps.Location(coordinates.latitude, coordinates.longitude);
    
    counter++

    if (!CheckInArray(location, locations))
        locations[locations.length] = location;

    //PUSHPIN SECTION
    var pushPinIcon;
      
    if (building.costThresholdValue == 0)
        pushPinIcon = "/_assets/images/buildingicons/building_icon_blue_small.png";
    else if (building.costThresholdValue == 1)
        pushPinIcon = "/_assets/images/buildingicons/building_icon_medgreen_small.png";
    else if (building.costThresholdValue == 2)
        pushPinIcon = "/_assets/images/buildingicons/building_icon_yellow_small.png";
    else if (building.costThresholdValue == 3)
        pushPinIcon = "/_assets/images/buildingicons/building_icon_orange_small.png";
    else
        pushPinIcon = "/_assets/images/buildingicons/building_icon_red_small.png";
    
    var pushpin = new Microsoft.Maps.Pushpin(location, { icon: pushPinIcon });
    var title = (building.isProviderView && building.clientName.length > 0) ? (unescape((building.clientName.length <= 21) ? building.clientName : building.clientName.substring(0, 20) + "..."))
                                                                            : (unescape((building.buildingName.length <= 21) ? building.buildingName : building.buildingName.substring(0, 20) + "..."));

    pushpin.Title = title;

    var address = "<div>" + unescape((building.address.length <= 31) ? building.address : building.address.substring(0, 30) + "...") + "</div>";
    var description = (building.isProviderView && building.clientName.length > 0) ? "<div>" + (unescape((building.buildingName.length <= 31) ? building.buildingName : building.buildingName.substring(0, 30) + "...")) + "</div>" + address
                                                                                  : address;
    
    pushpin.Description = description;

    if (building.hasDiagnosticAndCommissioningAccess)        
        pushpin.Description += "<div>Yesterday's Fault Count: " + kendo.toString(building.faults, 'n0') + "<br />Yesterday's Cost Savings: " + building.costs + "</div><div><a href='CommissioningDashboard.aspx?bid=" + bid + "'>Commissioning Dashboard</a></div>";
        
    if (building.hasProfileAccess && !building.isProviderView)
        pushpin.Description += "<div><a href='BuildingProfile.aspx?bid=" + bid + "'>Building Profile</a></div>";
    
    // Add handler for the pushpin click event.
    Microsoft.Maps.Events.addHandler(pushpin, 'click', displayInfobox);

    map.entities.push(pushpin);
    //PUSHPIN 

    SetView(coordinates);
}

function SetView(coordinates) {
    //don't set the view of the map until all of the data points (pushpins) have been populated on the map
    if (counter == Object.keys(userDataforBuildingMap).length) {
        if (locations.length > 1) {

            //mapTypeId: Microsoft.Maps.MapTypeId.aerial,
            var allBounds = new Microsoft.Maps.LocationRect.fromLocations(locations);
            map.setView({ bounds: allBounds, labelOverlay: Microsoft.Maps.LabelOverlay.hidden });

            //after setview all locations. 
            //cant do it by zoom value because it hasnt binded yet and it zooms based on bounds, and no post binding events to hook into.
            if (allBounds.height < .002 && allBounds.width < .002) {                
                map.setView({
                    'zoom': 16,
                    'animate': false
                });
            }
        }
        else
            map.setView({ center: new Microsoft.Maps.Location(coordinates.latitude, coordinates.longitude), zoom: 16, labelOverlay: Microsoft.Maps.LabelOverlay.hidden });
    }
}



//HELPER FUNCTIONS
function CallRestService(request, attemptRetry) {
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", request);
    script.onerror =
        function () {
            if (attemptRetry) CallRestService(this.src, false)
            else {
                counter++;
                SetView(coordinates);
            }
        } //attempt to retry the failed request once more
    document.body.appendChild(script);
}

//function to check if lat/long have already beeen added to the array
function CheckInArray(val, vals) {
    for (var i = 0; i < vals.length; i++)
        if (vals[i].latitude === val.latitude && vals[i].longitude === val.longitude) return true;

    return false;
}

function Base64Decode(geocodeCredentialsKey) {
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    do {
        enc1 = keyStr.indexOf(geocodeCredentialsKey.charAt(i++));
        enc2 = keyStr.indexOf(geocodeCredentialsKey.charAt(i++));
        enc3 = keyStr.indexOf(geocodeCredentialsKey.charAt(i++));
        enc4 = keyStr.indexOf(geocodeCredentialsKey.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64)
            output = output + String.fromCharCode(chr2);
        if (enc4 != 64)
            output = output + String.fromCharCode(chr3);

    } while (i < geocodeCredentialsKey.length);

    return output;
}
//HELPER FUNCTIONS

//EVENT HANDLER FUNCTIONS
//info box
function displayInfobox(e) {
    if (e.targetType == 'pushpin') {
        infobox.setOptions({
            visible: true,
            title: e.target.Title,
            description: e.target.Description,
            offset: new Microsoft.Maps.Point(0, 25)
        });
        infobox.setLocation(e.target.getLocation());

        //A buffer limit to use to specify the infobox must be away from the edges of the map.
        var buffer = 2;

        var infoboxOffset = infobox.getOffset();
        var infoboxAnchor = infobox.getAnchor();
        var infoboxLocation = map.tryLocationToPixel(e.target.getLocation(), Microsoft.Maps.PixelReference.control);

        var dx = infoboxLocation.x + infoboxOffset.x - infoboxAnchor.x;
        var dy = infoboxLocation.y - 60 - infoboxAnchor.y;

        if (dy < buffer) {    //Infobox overlaps with top of map.
            dy *= -1;  //Offset in opposite direction.
            dy += buffer; //add buffer from the top edge of the map.
        } else
            dy = 0; //If dy is greater than zero than it does not overlap.

        if (dx < buffer) { //Check to see if overlapping with left side of map.
            dx *= -1; //Offset in opposite direction.
            dx += buffer; //add a buffer from the left edge of the map.
        } else { //Check to see if overlapping with right side of map.
            dx = map.getWidth() - [dx + infobox.getWidth() - 55];
 
            //If dx is greater than zero then it does not overlap.
            if (dx > buffer)
                dx = 0;
            else
                dx -= buffer; //add a buffer from the right edge of the map.
        }

        //Adjust the map so infobox is in view
        if (dx != 0 || dy != 0)
            map.setView({ centerOffset: new Microsoft.Maps.Point(dx, dy), center: map.getCenter() });
    }
}
//EVENT HANDLER FUNCTIONS