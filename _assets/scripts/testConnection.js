﻿window.KGS = window.KGS || {};

KGS.TestConnection = function() {

    var data = 
    { 
        id : jQuery('.ddlWorkOrderVendor').val().trim(),
        uri : jQuery('.txtWorkOrderBaseUrl').val().trim(),
        username : jQuery('.txtWorkOrderUserName').val().trim(),
        password : jQuery('.txtWorkOrderPassKey').val().trim(),
    };

    jQuery.ajax
    ({ 
        url : "TestConnection.ashx", 
        data : data, 
        contentType : "application/json; charset=utf-8",
        dataType : "json",
        cache : false,
        success : function(result) {
            alert(result);
        }
    });
}

