﻿window.KGS = window.KGS || {};

KGS.RefreshControls = function()
{
    ddlCost = jQuery(".ddlCost");
    ddlEnergy = jQuery(".ddlEnergy");
    ddlCarbon = jQuery(".ddlCarbon");
    ddlCarbonMetric = jQuery(".ddlCarbonMetric");

    costPct = jQuery(".costValPct");
    costAbs = jQuery(".costValAbs");
    energyPct = jQuery(".energyValPct");
    energyAbs = jQuery(".energyValAbs");
    carbonPct = jQuery(".carbonValPct");
    carbonAbs = jQuery(".carbonValAbs");

    costPctInput = jQuery(".costValPct > span > input");
    costAbsInput = jQuery(".costValAbs > span > input");
    energyPctInput = jQuery(".energyValPct > span > input");
    energyAbsInput = jQuery(".energyValAbs > span > input");
    carbonPctInput = jQuery(".carbonValPct > span > input");
    carbonAbsInput = jQuery(".carbonValAbs > span > input"); 

    lblCost = jQuery(".lblCost");
    lblEnergy = jQuery(".lblEnergy");
    lblOther = jQuery(".lblOther");
}

KGS.DisplayValueEditFields = function ($) {   
        
    if (ddlCost.val() === '2') costAbs.css('display', 'inline-block'); else costAbs.hide();
    if (ddlCost.val() === '3') costPct.css('display', 'inline-block'); else costPct.hide();
    if (ddlEnergy.val() === '2') energyAbs.css('display', 'inline-block'); else energyAbs.hide();
    if (ddlEnergy.val() === '3') energyPct.css('display', 'inline-block'); else energyPct.hide();
    if (ddlCarbon.val() === '2') carbonAbs.css('display', 'inline-block'); else carbonAbs.hide();
    if (ddlCarbon.val() === '3') carbonPct.css('display', 'inline-block'); else carbonPct.hide();

    KGS.SetUnitLabels();        
    return false;
}

KGS.SetUnitLabels = function ($) {
        
    var txtCost = KGS.GetCostTxt();
    var txtEnergy = KGS.GetEnergyTxt();
    var txtOther = KGS.GetOtherTxt();

    lblCost.text(txtCost);
    lblEnergy.text(txtEnergy);
    lblOther.text(txtOther);

    return false;
}


KGS.GetCostTxt = function ($) {
    
    var text = "";

    if (ddlCost.val() === "2") text = userCurrency;
    else if (ddlCost.val() === "3") text = "Percent";

    return text;
}

KGS.GetEnergyTxt = function ($) {

    var text = "";

    if (ddlEnergy.val() === "2") text = "kWh";
    else if (ddlEnergy.val() === "3") text = "Percent";

    return text;
}

KGS.GetOtherTxt = function ($) {

    var text = "";
    if (ddlCarbon.val() === "2" && ddlCarbonMetric.val() === "1") text = "Tons C02";
    else if (ddlCarbon.val() === "2" && ddlCarbonMetric.val() === "4") text = "Cars";
    else if (ddlCarbon.val() === "2" && ddlCarbonMetric.val() === "5") text = "Trees";
    else if (ddlCarbon.val() === "3") text = "Percent";

    return text;
}

KGS.ToggleGoalSettingsView = function ($) {

    KGS.RefreshControls();

    var goalsEnabled = jQuery(".chkEnableGoals > input").is(':checked');
    var goalEditView = jQuery(".goalSettings");

    var saveBtn = jQuery(".saveButton");

    var inheritDiv = jQuery(".inheritDiv");
    var isInherited = jQuery(".chkInheritGoals  > input").is(':checked');
    
    if (goalsEnabled)
    {
        inheritDiv.show();

        if (inheritDiv === null || !isInherited)
        {            
            goalEditView.show();
            saveBtn.show();
        }        

        if(isInherited)
        {
            saveBtn.hide();
        }
    }        
    else
    {
        goalEditView.hide();
        saveBtn.hide();
        inheritDiv.hide();
    }

    KGS.DisplayValueEditFields();    
}

KGS.ChkEnableClicked = function ($) {
    var chkEnabled = jQuery(".chkEnableGoals > input");
    var goalsEnabled = chkEnabled.is(':checked');
    var shouldToggleEnabled = true;

    if (!goalsEnabled)
    {
        var shouldToggleEnabled = (confirm('Are you sure you want to disable goals? This will delete all existing goals'));
    }

    if (shouldToggleEnabled)
    {
        //trigger postback                
        __doPostBack('chkEnable', goalsEnabled);        
    }
    else
    {        
        chkEnabled.prop('checked', !goalsEnabled);
    }    
}

KGS.ChkInheritClicked = function ($)
{
    var isInherited = jQuery(".chkInheritGoals >input").is(':checked');
    __doPostBack('chkInherit', isInherited);
}

//KGS.ChkInheritClicked = function ($)
//{    
//    var chkInherit = jQuery(".chkInheritGoals > input");
//    var isInherited = chkInherit.is(':checked');

//    alert(isInherited);
//    if (!isInherited)
//    {
//        var shouldToggleInherit = (confirm('Are you sure you want to override inherited client goals?'));
//    }

//    if (shouldToggleInherit) {
//        //trigger postback                
//        __doPostBack('chkInherit', isInherited);
//    }
//    else {
//        chkInherit.prop('chkInherit', !isInherited);
//    }
//}

/*
KGS.SaveButtonClick = function ($) {

    var costVal = ddlCost.val() === '1' ? costAbsInput.val() : costPctInput.val();
    var energyVal = ddlEnergy.val() === '1' ? energyAbsInput.val() : energyPctInput.val();
    var carbonVal = ddlCarbon.val() === '1' ? carbonAbsInput.val() : carbonPctInput.val();    

    var data =
    {
        costTypeID: ddlCost.val(),
        costValue: costVal,        
        energyTypeID: ddlEnergy.val(),
        energyValue: energyVal,        
        carbonTypeID: ddlCarbon.val(),
        carbonValue: carbonVal,        
        carbonMetricID: ddlCarbonMetric.val()
    };

    jQuery.ajax
    ({
        headers: { 'Cookie': document.cookie },
        url: "GoalViewHandler",
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (result) {
            alert(result);
        }
    });    
}
*/