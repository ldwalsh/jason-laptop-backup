﻿$(document).ready(function () {
    //this was put in place to fix the issue with the RadHtmlChart issue in FireFox (only), removing the href from the explicitly generated base tag fixed the issue.
    var baseTag = document.getElementsByTagName("base")[0];
    if (baseTag != null)
        baseTag.href = '';

    //this code is to auto resize the widget being processed to the appropriate size after the async request completes
    var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();
    pgRegMgr.add_beginRequest(BeginHandler);
    pgRegMgr.add_endRequest(EndHandler);

    function BeginHandler() {
        window.parent.EnableFilters(false);
        window.parent.EnableDockFunctionality(false);
    }

    function EndHandler() {
        var windowParent = window.parent;
        var frameStyle = window.frameElement.style;
        var placeholder = window.frameElement.placeholder;
        var placeholderStyle = placeholder.style;

        frameStyle.height = "0px"; //height needs to be set to 0px first with chrome, safari and opera for the resize to work

        var height = document.body.scrollHeight;
        frameStyle.height = height + "px";

        var width = document.body.scrollWidth;
        frameStyle.width = width + "px";

        placeholderStyle.height = height + "px";
        placeholderStyle.width = width + "px";

        windowParent.HideDDLFromWidget(window.frameElement);
        windowParent.AdjustIframeOverlays();

        //if there are still other widgets processing, even if this request completes, don't enable the filters yet.
        if (windowParent.AreWidgetsProcessing("async")) return;

        windowParent.EnableFilters(true);
        windowParent.EnableDockFunctionality(true);
    }
});