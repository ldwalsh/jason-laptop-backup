﻿var locations = new Array();
var map;
var geocodeCredentialsKey;
var userDataforBuildingMap;
var userCulture;
var counter = 0;

//Bing map
function GetMap() {
    var mapOptions = {
        credentials: Base64Decode(geocodeCredentialsKey),
        mapTypeId: Microsoft.Maps.MapTypeId.automatic,
    };

    map = new Microsoft.Maps.Map(document.getElementById("mapDiv"), mapOptions);

    var infoboxLayer = new Microsoft.Maps.EntityCollection();
    map.entities.push(infoboxLayer);

    infobox = new Microsoft.Maps.Infobox(new Microsoft.Maps.Location(0, 0), { visible: false });
    infoboxLayer.push(infobox);

    map.getCredentials(MakeGeocodeRequest);
}

//function to either use the user entered coorindates from the db, or pull from microsoft's BING MAP API if coordinates were not entered
function MakeGeocodeRequest(credentials) {
    for (var bid in userDataforBuildingMap) {
        var building = {
            buildingName: userDataforBuildingMap[bid][0],
            address: userDataforBuildingMap[bid][1],
            city: userDataforBuildingMap[bid][2],
            state: userDataforBuildingMap[bid][3],
            countryAlpha2Code: userDataforBuildingMap[bid][4],
            alarmCount: userDataforBuildingMap[bid][5],
            alarmPct: userDataforBuildingMap[bid][6],
            latitude: userDataforBuildingMap[bid][7],
            longitude: userDataforBuildingMap[bid][8],
            criticalAlarmThreshold: userDataforBuildingMap[bid][9],
            nonCriticalAlarmThreshold: userDataforBuildingMap[bid][10]
        };

        userDataforBuildingMap[bid] = building;

        if (building.latitude && building.longitude)
            SetCoordinatesAndPushpin({ latitude: building.latitude, longitude: building.longitude }, bid);
        else {
            var geocodeRequest = "https://dev.virtualearth.net/REST/v1/Locations?query=" + [building.address, building.city, building.state, building.countryAlpha2Code].join() + "&key=" + credentials + "&jsonp=ParseResults&output=json&jsonso=" + bid;
            CallRestService(geocodeRequest, true);
        }
    }
}

//function called when data is returned from the rest service.  lat/long values are passed to the SetCoordinatesAndPushin function
function ParseResults(result, bid) {
    if (result && result.resourceSets && result.resourceSets.length > 0 && result.resourceSets[0].resources && result.resourceSets[0].resources.length > 0) {
        var coordinates = result.resourceSets[0].resources[0].point.coordinates;
        SetCoordinatesAndPushpin({ latitude: coordinates[0], longitude: coordinates[1] }, bid);
    }
}

//function that will place the locations on the map via coordinates (lat/long), then will set the pushpin information for each infobox
function SetCoordinatesAndPushpin(coordinates, bid) {
    var building = userDataforBuildingMap[bid];
    var location = new Microsoft.Maps.Location(coordinates.latitude, coordinates.longitude);

    counter++

    //make sure the location doesn't already exist in the array, if it doesn't add it
    if (!CheckInArray(location, locations))
        locations[locations.length] = location;

    //PUSHPIN SECTION
    var pushPinIcon; 

    if (building.alarmPct >= building.criticalAlarmThreshold)
        pushPinIcon = "/_assets/images/buildingicons/building_icon_red_small.png";
    else if (building.alarmPct < building.criticalAlarmThreshold && building.alarmPct >= building.nonCriticalAlarmThreshold)
        pushPinIcon = "/_assets/images/buildingicons/building_icon_yellow_small.png";
    else
        pushPinIcon = "/_assets/images/buildingicons/building_icon_medgreen_small.png";

    var pushpin = new Microsoft.Maps.Pushpin(location, { icon: pushPinIcon, width: 40 });

    pushpin.Title = unescape((building.buildingName.length <= 30) ? building.buildingName : building.buildingName.substring(0, 29) + "...");
    pushpin.Description = unescape((building.address.length <= 30) ? building.address : building.address.substring(0, 29) + "...");
    pushpin.Description += "<div># Today's Total Active Alarms: " + kendo.toString(building.alarmCount, 'n0') + "<br />% Today's Total Active Alarms: " + kendo.toString(building.alarmPct, 'n2') + "%</div>";

    //If there is more than one building in the list, we need to add the Building Information link, otherwise it won't make sense to display as there is only one building
    if (Object.keys(userDataforBuildingMap).length > 1)
        pushpin.Description += "<div><a href='javascript:void(0)' onclick='PopulateAndRefreshWidgetsByBID(\"" + bid + "\");'>Building Alarm Information</a></div>";

    // Add handler for the pushpin click event.
    Microsoft.Maps.Events.addHandler(pushpin, 'click', displayInfobox);

    map.entities.push(pushpin);
    //PUSHPIN 

    SetView(coordinates);
}

function SetView(coordinates) {
    //don't set the view of the map until all of the data points (pushpins) have been populated on the map
    if (counter == Object.keys(userDataforBuildingMap).length) {
        if (locations.length > 1) {

            var allBounds = new Microsoft.Maps.LocationRect.fromLocations(locations);
            map.setView({ bounds: allBounds, labelOverlay: Microsoft.Maps.LabelOverlay.hidden });

            //after setview all locations. 
            //cant do it by zoom value because it hasnt binded yet and it zooms based on bounds, and no post binding events to hook into.
            if (allBounds.height < .0005 && allBounds.width < .002) {
                map.setView({
                    'zoom': 16,
                    'animate': false
                });
            }
        }
        else
            map.setView({ center: new Microsoft.Maps.Location(coordinates.latitude, coordinates.longitude), zoom: 16, labelOverlay: Microsoft.Maps.LabelOverlay.hidden });
    }
}

//HELPER FUNCTIONS
function CallRestService(request, attemptRetry) {
    var script = document.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", request);
    script.onerror =
        function () {
            if (attemptRetry) CallRestService(this.src, false)
            else {
                counter++;
                SetView(coordinates);
            }
        } //attempt to retry the failed request once more
    document.body.appendChild(script);
}

//function to check if lat/long have already beeen added to the array
function CheckInArray(val, vals) {
    for (var i = 0; i < vals.length; i++)
        if (vals[i].latitude === val.latitude && vals[i].longitude === val.longitude) return true;

    return false;
}

function Base64Decode(geocodeCredentialsKey) {
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    do {
        enc1 = keyStr.indexOf(geocodeCredentialsKey.charAt(i++));
        enc2 = keyStr.indexOf(geocodeCredentialsKey.charAt(i++));
        enc3 = keyStr.indexOf(geocodeCredentialsKey.charAt(i++));
        enc4 = keyStr.indexOf(geocodeCredentialsKey.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64)
            output = output + String.fromCharCode(chr2);
        if (enc4 != 64)
            output = output + String.fromCharCode(chr3);

    } while (i < geocodeCredentialsKey.length);

    return output;
}
//HELPER FUNCTIONS

//EVENT HANDLER FUNCTIONS
//info box
function displayInfobox(e) {
    if (e.targetType == 'pushpin') {
        infobox.setOptions({
            visible: true,
            title: e.target.Title,
            description: e.target.Description,
            offset: new Microsoft.Maps.Point(0, 25)
        });

        infobox.setLocation(e.target.getLocation());

        //A buffer limit to use to specify the infobox must be away from the edges of the map.
        var buffer = 2;

        var infoboxOffset = infobox.getOffset();
        var infoboxAnchor = infobox.getAnchor();
        var infoboxLocation = map.tryLocationToPixel(e.target.getLocation(), Microsoft.Maps.PixelReference.control);

        var dx = infoboxLocation.x + infoboxOffset.x - infoboxAnchor.x;
        var dy = infoboxLocation.y - 60 - infoboxAnchor.y;

        if (dy < buffer) {    //Infobox overlaps with top of map.
            dy *= -1;  //Offset in opposite direction.
            dy += buffer; //add buffer from the top edge of the map.
        } else
            dy = 0; //If dy is greater than zero than it does not overlap.

        if (dx < buffer) { //Check to see if overlapping with left side of map.
            dx *= -1; //Offset in opposite direction.
            dx += buffer; //add a buffer from the left edge of the map.
        } else { //Check to see if overlapping with right side of map.
            dx = map.getWidth() - [dx + infobox.getWidth() - 55];

            //If dx is greater than zero then it does not overlap.
            if (dx > buffer)
                dx = 0;
            else
                dx -= buffer; //add a buffer from the right edge of the map.
        }

        //Adjust the map so infobox is in view
        if (dx != 0 || dy != 0)
            map.setView({ centerOffset: new Microsoft.Maps.Point(dx, dy), center: map.getCenter() });
    }
}

//EVENT HANDLER FUNCTIONS
$(document).ready(function () {
    userDataforBuildingMap = JSON.parse(document.getElementById("userDataforBuildingMap").value);

    //If there are any buildings in the list, we need to display the map, otherwise hide it and display the no data message
    if (Object.keys(userDataforBuildingMap).length > 0) {
        document.getElementById('buildingMap').style.display = 'block';
        document.getElementById('noDataMessage').style.display = 'none';

        geocodeCredentialsKey = document.getElementById("geocodeCredentialsKey").value;
        userCulture = document.getElementById("userCulture").value;

        GetMap();
    }
    else {
        document.getElementById('buildingMap').style.display = 'none';
        document.getElementById('noDataMessage').style.display = 'block';
    }

    var frameStyle = window.frameElement.style;

    window.frameElement.height = "0px"; //for chrome, safari and opera fix
    
    var height = document.body.scrollHeight;
    frameStyle.height = height + "px";

    var width = document.body.scrollWidth;
    frameStyle.width = width + "px";
});

function PopulateAndRefreshWidgetsByBID(bid) {
    var windowParent = window.parent;
    var iframes = windowParent.document.getElementsByTagName('iframe');

    if (AreWidgetsProcessing(windowParent)) return;

    var widgetsCount = new Array();
    var counter = 0;
    var dockToUpdate = new Array();
    var ddlsToUpdate = new Array();

    for (var i = 0; i < iframes.length; i++) {
        var iframe = iframes[i];
        var iFrameContentWindow = iframe.contentWindow;

        var ddl = iFrameContentWindow.document.querySelector("select[data-buildingMap='attached']");

        //create a count of all widgets that use the buildings ddl for their data filtering
        if (ddl != null) {
            widgetsCount.push(iframe.id);

            //If the building ddl is found and the values is set to the new bid, increment the count.  Add the ddl to the list of ddls to update for later in code.
            //Also, if the raddock are collapsed we have to add the dock to another array to force them open in order to resize correctly.
            if (ddl.value == bid)
                counter++;

            dockToUpdate.push(windowParent.$find($(iframe.placeholder, windowParent.document).parents('.RadDock')[0].id));
            ddlsToUpdate.push(ddl);
        }
    } //end of for loop through iframes for checking ddl values

    //important code, if the number of widgets looped through is equal to the number of ddls with the bid value set, we don't want to reupdate them all, especially since the data has been cached to an hour prior anyways
    //otherwise loop through the ddls that need to be updated and set their bid, open their raddock if collapsed and force the async postback.
    if (widgetsCount.length == counter)
        return;
    else {
        for (var i = 0; i < ddlsToUpdate.length; i++) {
            if (dockToUpdate[i].get_collapsed())
                dockToUpdate[i].set_collapsed(false);

            ddlsToUpdate[i].value = bid;
            ddlsToUpdate[i].onchange();
        }
    }
}

function AreWidgetsProcessing(windowParent) {
    var alertMessage = 'Widgets are still processing.  Please wait for the widgets to complete before selecting building.';

    //if this is the initial execution of the page, don't allow them to process the widgets yet.
    if (windowParent.IsInitialLoad()) {
        alert(alertMessage);
        return;
    }

    //if there are still widgets processing, don't allow them to execute the code again yet.
    if (windowParent.AreWidgetsProcessing("asyncBuildingMap")) {
        alert(alertMessage);
        return;
    }
}