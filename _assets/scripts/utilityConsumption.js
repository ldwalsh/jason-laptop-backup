﻿window.KGS = window.KGS || {};

//when the page first loads and is ready, toggle filters based on the first
//value that was set.
jQuery(document).ready(function () {
 
    KGS.ToggleSubFilters();

});

//function used to toggle visibility of sub filters, based on selected
//analysis type value
KGS.ToggleSubFilters = function () {
    
    var ids = jQuery('#hdnIdsToHideFilter').val().split(',').map(Number);

    var selectedId = Number(jQuery('.ddlAnalysisTypes').val());

    var subFilters = jQuery('.divSubFilters');
    
    if (jQuery.inArray(selectedId, ids) == -1) {

        subFilters.show();

        return;
    }

    subFilters.hide();

    KGS.DeselectCheckBoxes();

    KGS.DeselectDropDowns();
}

//convenience function to deselect n number of check boxes
KGS.DeselectCheckBoxes = function () {

    var checkBoxes = ['#chkArea', '#chkWeather'];

    checkBoxes.forEach(function(checkBox) {

        jQuery(checkBox).prop('checked', false);

    });
}

//convenience function to deselect n number of drop downs (TODO)
KGS.DeselectDropDowns = function () {

    $('.ddlUtilities').each(function() {

        $(this).find('option:first').prop('selected', 'selected');

    });

}