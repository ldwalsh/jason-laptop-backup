﻿function OpenUrlInNewWindow(ddl) {
    if (ddl.value != -1) {
        window.open(ddl.value, '_blank')

        var options = ddl.options;

        for (var i = 0; i < options.length; i++)
            options[i].selected = false;

        ddl.blur();
    }
}