﻿
function PopulateRadDatePickerDates(startDateClientID, endDateClientID, amount, intervalLength, asUTC) {
    if (asUTC === undefined) asUTC = false;

    var d = new Date();

    if (asUTC) d = new Date(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(), d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds());

    SetDateRange(startDateClientID, endDateClientID, amount, intervalLength, d);
}

function SetDateRange(startDateClientID, endDateClientID, amount, intervalLength, d) {

    var interval = (intervalLength == "hours") ? { "hours": amount } : { "minutes": amount };
    var startDate = new Date((interval['minutes']) ? d.setMinutes(d.getMinutes() - parseInt(interval['minutes'])) : d.setHours(d.getHours() - parseInt(interval['hours'])));
    var endDate = new Date((interval['minutes']) ? d.setMinutes(d.getMinutes() + parseInt(interval['minutes'])) : d.setHours(d.getHours() + parseInt(interval['hours'])));
  
    $find(startDateClientID).set_selectedDate(kendo.parseDate(startDate));
    $find(endDateClientID).set_selectedDate(kendo.parseDate(endDate));
}

function SetDateRangeWithOffset(startDateClientID, endDateClientID, amount, intervalLength, offsetInMS) {

    var date = new Date();
    var utc = date.getTime() + (date.getTimezoneOffset() * 60000);
    var od = new Date(utc + offsetInMS);

    SetDateRange(startDateClientID, endDateClientID, amount, intervalLength, od);
}

//This comparer does something strange to the text fields for the dates (at least for the en-US culture format).  For some reason on a post-back, it deletes the AM/PM designation from the value in the textbox ???
var DatetimeComparer = function (sender, args) {
    var start = new Date(document.getElementById(sender.controltovalidate.replace("End", "Start")).value);
    var end = new Date(document.getElementById(sender.controltovalidate).value);

    args.IsValid = (end >= start);
};

var RadDatePickerDatetimeComparer = function (sender, args) {

    var start = $find(sender.controltovalidate.replace("End", "Start")).get_dateInput().get_selectedDate();
    var end = $find(sender.controltovalidate).get_dateInput().get_selectedDate();

    args.IsValid = (end >= start);
};