﻿function CopyGridView(panelName) {
    var div = document.getElementById(panelName);
    var controlRange;

    div.contentEditable = 'true';

    if (document.body.createControlRange) {
        controlRange = document.body.createControlRange();
        controlRange.addElement(div);
        controlRange.execCommand('Copy');
    }

    div.contentEditable = 'false';
}