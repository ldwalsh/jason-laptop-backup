﻿function OnEngUnitChange(ddl, infoid, textboxid, valfailureid) {
    if (null != engUnitInfo) {
        var engUnitDdl = $("#" + ddl.id);
        if (null != engUnitDdl) {
            var selectedValue = engUnitDdl.val();
            var info = engUnitInfo[selectedValue];
            if (null != info) {
                $("#" + infoid).html(info.engUnit);
                var failure = $("#" + valfailureid);
                if (null != failure) {
                    failure.html("");
                }
                var textbox = $("#" + textboxid);
                if (null != textbox) {
                    var htmlElement = textbox.get(0).tagName;
                    var newElement = null;
                    if (htmlElement == "TEXTAREA" && !info.isStringOrArray) {
                        var input = '<input name="' + textbox.get(0).name + '" class="textbox" id="' + textbox.get(0).id + '" type="text" size="1" maxlength="25" rows="1" cols="1"></input>';
                        newElement = $(input);
                    }
                    else if (htmlElement == "INPUT" && info.isStringOrArray) {
                        var textarea = '<textarea name="' + textbox.get(0).name + '" id="' + textbox.get(0).id + '" maxlength="2500" rows="5" cols="40"></textarea>';
                        newElement = $(textarea);
                    }
                    else {
                        // do nothing regarding html element
                    }

                    if (null != newElement) {
                        newElement.val(textbox.val());
                        textbox.replaceWith(newElement);
                    }
                }
            }
        }
    }
}
