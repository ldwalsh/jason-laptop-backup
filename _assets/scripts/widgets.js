﻿google.load("visualization", "1", { 'packages': ["corechart"] }); //this needs be called before setting data for the building data points pie chart

$.getScript('_assets/scripts/buildingMap.js');
$.getScript('_assets/scripts/drawChart.js');

$.ajaxSetup({
    cache: false, // Disable caching of AJAX responses, mostly for IE
    contentType: "text/html; charset=utf-8"
});

function redirectUrl(e) {
    document.location.href = e.target.getAttribute("data-url");
}

var widgetsLoaded = new Array();
var docks = new Array();
var fadeInTime = 2000;
var loadingImage = '<img src="_assets/images/loading.gif" alt="loading" />';
var href = "";

$(document).ready(function () {

    if ($('.lnk').length > 0) {
        href = $('.lnk').attr('href');

        //$('.lnk').attr('disabled', 'disabled');
        //$('.lnk').removeAttr('href');
        //$('.lnk').fadeTo("slow", 0.5);
    }

    if ($('#btnUILoad').val() == "LoadDefaultWidgets") {

        //javascript snippet used to override the default "draggable" behavior when links are placed within a radDock Title Bar
        $("[data-url]").each(
            function (i, e) {
                Sys.Application.add_load(function () {
                    $telerik.$($get(e.id)).on("mousedown", function (e) { $telerik.cancelRawEvent(e) });
                });
            });
        //javascript snippet used to override the default "draggable" behavior when links are placed within a radDock Title Bar

        $("#commodities").hide(); //hide the commodities widget to display the "appearance" of a loading widget

        PreloadDefaultWidgetsProcessingImage();
    }
    else
        PreloadProviderWidgetsProcessingImage();

    setTimeout(function () {

        $('#btnUILoad').click(function () {

            if ($('#btnUILoad').val() == "LoadDefaultWidgets")
                LoadDefaultWidgets();
            else
                LoadProviderWidgets();

        }).trigger('click'); //end of button click function
    }, 1000)

    function PreLoadWidgetProgressImage(widgetUpdateElement, isValidWidget) {
        $(widgetUpdateElement).html(loadingImage);

        if (isValidWidget && $(widgetUpdateElement).length > 0 && $('.lnk').length > 0)
            docks.push(widgetUpdateElement);
    }

    function DisplayLoadingProgressImage(widgetUpdateElement) {
        if ($(widgetUpdateElement).length > 0)
            $(widgetUpdateElement).show();
    }

    function PreloadDefaultWidgetsProcessingImage() {
        PreLoadWidgetProgressImage("#updateProgressBuildingMap", 1);
        PreLoadWidgetProgressImage("#updateProgressPast30DaysAvoidableCosts", 1);
        PreLoadWidgetProgressImage("#updateProgressLastMonthsDiagnosticSummary", 1);
        PreLoadWidgetProgressImage("#updateProgressYesterdaysDiagnosticSummary", 1);
        PreLoadWidgetProgressImage("#updateProgressLastMonthsDiagnosticResults", 1);
        PreLoadWidgetProgressImage("#updateProgressYesterdaysDiagnosticResults", 1);
        //PreLoadWidgetProgressImage("#updateProgressWeather", 1);
        PreLoadWidgetProgressImage("#updateProgressPast30DaysPortfolioPriorities", 1);
        PreLoadWidgetProgressImage("#updateProgressRecentTasks", 1);
        PreLoadWidgetProgressImage("#updateProgressTopProjects", 1);
        PreLoadWidgetProgressImage("#updateConsumptionComparison", 0);
        //PreLoadWidgetProgressImage("#updateProgressCommodities", 0);
        PreLoadWidgetProgressImage("#updateProgressQuickLinks", 1);
        PreLoadWidgetProgressImage("#updateProgressBuildingDataPointsPieChart", 1);
        PreLoadWidgetProgressImage("#updateProgressFaults", 1);        
    }

    function PreloadProviderWidgetsProcessingImage() {
        PreLoadWidgetProgressImage("#updateProgressBuildingMap", 1);
        PreLoadWidgetProgressImage("#updateProgressLastMonthsDiagnosticSummaryByClient", 1);
        PreLoadWidgetProgressImage("#updateProgressYesterdaysDiagnosticSummaryByClient", 1);
        PreLoadWidgetProgressImage("#updateProgressLastMonthsDiagnosticSummary", 1);
        PreLoadWidgetProgressImage("#updateProgressYesterdaysDiagnosticSummary", 1);
        PreLoadWidgetProgressImage("#updateProgressLastMonthsDiagnosticResults", 1);
        PreLoadWidgetProgressImage("#updateProgressYesterdaysDiagnosticResults", 1);
        PreLoadWidgetProgressImage("#updateProgressPast30DaysAvoidableCosts", 1);
        PreLoadWidgetProgressImage("#updateProgressPast30DaysPortfolioPriorities", 1);
    }

    function LoadDefaultWidgets() {
        DisplayLoadingProgressImage("#updateProgressBuildingMap");
        DisplayLoadingProgressImage("#updateProgressPast30DaysAvoidableCosts");
        DisplayLoadingProgressImage("#updateProgressLastMonthsDiagnosticSummary");
        DisplayLoadingProgressImage("#updateProgressYesterdaysDiagnosticSummary");
        DisplayLoadingProgressImage("#updateProgressLastMonthsDiagnosticResults");
        DisplayLoadingProgressImage("#updateProgressYesterdaysDiagnosticResults");
        //DisplayLoadingProgressImage("#updateProgressWeather");
        DisplayLoadingProgressImage("#updateProgressPast30DaysPortfolioPriorities");
        DisplayLoadingProgressImage("#updateProgressRecentTasks");
        DisplayLoadingProgressImage("#updateProgressTopProjects");
        //DisplayLoadingProgressImage("#updateProgressCommodities");
        DisplayLoadingProgressImage("#updateProgressQuickLinks");
        DisplayLoadingProgressImage("#updateProgressBuildingDataPointsPieChart");
        DisplayLoadingProgressImage("#updateProgressFaults");

        GetWidget("BuildingMap", "#updateProgressBuildingMap", "#buildingMap", "#contentToLoadBuildingMap", 0);
        GetWidget("Past30DaysAvoidableCosts", "#updateProgressPast30DaysAvoidableCosts", "#past30DaysAvoidableCosts", "#contentToLoadPast30DaysAvoidableCosts", 0);
        GetWidget("LastMonthsDiagnosticSummary", "#updateProgressLastMonthsDiagnosticSummary", "#lastMonthsDiagnosticSummary", "#contentToLoadLastMonthsDiagnosticSummary", 0);
        GetWidget("YesterdaysDiagnosticSummary", "#updateProgressYesterdaysDiagnosticSummary", "#yesterdaysDiagnosticSummary", "#contentToLoadYesterdaysDiagnosticSummary", 0);
        GetWidget("LastMonthsDiagnosticResults", "#updateProgressLastMonthsDiagnosticResults", "#lastMonthsDiagnosticResults", "#contentToLoadLastMonthsDiagnosticResults", 0);
        GetWidget("YesterdaysDiagnosticResults", "#updateProgressYesterdaysDiagnosticResults", "#yesterdaysDiagnosticResults", "#contentToLoadYesterdaysDiagnosticResults", 0);
        //GetWidget("Weather", "#updateProgressWeather", "#weather", "#contentToLoadWeather", 0);
        GetWidget("Past30DaysPortfolioPriorities", "#updateProgressPast30DaysPortfolioPriorities", "#past30DaysPortfolioPriorities", "#contentToLoadPast30DaysPortfolioPriorities", 0);
        GetWidget("RecentTasks", "#updateProgressRecentTasks", "#recentTasks", "#contentToLoadRecentTasks", 0);
        GetWidget("TopProjects", "#updateProgressTopProjects", "#topProjects", "#contentToLoadTopProjects", 0);
        GetWidget("FaultsSparkLine", "#updateProgressFaults", "#faults", "#contentToLoadFaults", 0);

        if (window.ConsumptionWidget)
            ConsumptionWidget.load()

        //setTimeout(function () {
        //    $("#updateProgressCommodities").hide();
        //    $("#commodities").fadeIn(fadeInTime);
        //}, 2000);

        GetWidget("QuickLinks", "#updateProgressQuickLinks", "#quickLinks", "#contentToLoadQuickLinks", 0);
        GetWidget("BuildingDataPointsPieChart", "#updateProgressBuildingDataPointsPieChart", "#buildingDataPointsPieChart", "#contentToLoadBuildingDataPointsPieChart", 0);
    }

    function LoadProviderWidgets() {
        DisplayLoadingProgressImage("#updateProgressBuildingMap");
        DisplayLoadingProgressImage("#updateProgressLastMonthsDiagnosticSummaryByClient");
        DisplayLoadingProgressImage("#updateProgressYesterdaysDiagnosticSummaryByClient");
        DisplayLoadingProgressImage("#updateProgressLastMonthsDiagnosticSummary");
        DisplayLoadingProgressImage("#updateProgressYesterdaysDiagnosticSummary");
        DisplayLoadingProgressImage("#updateProgressLastMonthsDiagnosticResults");
        DisplayLoadingProgressImage("#updateProgressYesterdaysDiagnosticResults");
        DisplayLoadingProgressImage("#updateProgressPast30DaysAvoidableCosts");
        DisplayLoadingProgressImage("#updateProgressPast30DaysPortfolioPriorities");

        GetWidget("BuildingMap", "#updateProgressBuildingMap", "#buildingMap", "#contentToLoadBuildingMap", 0, 0, 1);
        GetWidget("LastMonthsDiagnosticSummary", "#updateProgressLastMonthsDiagnosticSummaryByClient", "#lastMonthsDiagnosticSummaryByClient", "#contentToLoadLastMonthsDiagnosticSummary", 0, 0, 1, 1);
        GetWidget("YesterdaysDiagnosticSummary", "#updateProgressYesterdaysDiagnosticSummaryByClient", "#yesterdaysDiagnosticSummaryByClient", "#contentToLoadYesterdaysDiagnosticSummary", 0, 0, 1, 1);
        GetWidget("LastMonthsDiagnosticSummary", "#updateProgressLastMonthsDiagnosticSummary", "#lastMonthsDiagnosticSummary", "#contentToLoadLastMonthsDiagnosticSummary", 0, 0, 1);
        GetWidget("YesterdaysDiagnosticSummary", "#updateProgressYesterdaysDiagnosticSummary", "#yesterdaysDiagnosticSummary", "#contentToLoadYesterdaysDiagnosticSummary", 0, 0, 1);
        GetWidget("LastMonthsDiagnosticResults", "#updateProgressLastMonthsDiagnosticResults", "#lastMonthsDiagnosticResults", "#contentToLoadLastMonthsDiagnosticResults", 0, 0, 1);
        GetWidget("YesterdaysDiagnosticResults", "#updateProgressYesterdaysDiagnosticResults", "#yesterdaysDiagnosticResults", "#contentToLoadYesterdaysDiagnosticResults", 0, 0, 1);
        GetWidget("Past30DaysAvoidableCosts", "#updateProgressPast30DaysAvoidableCosts", "#past30DaysAvoidableCosts", "#contentToLoadPast30DaysAvoidableCosts", 0, 0, 1);
        GetWidget("Past30DaysPortfolioPriorities", "#updateProgressPast30DaysPortfolioPriorities", "#past30DaysPortfolioPriorities", "#contentToLoadPast30DaysPortfolioPriorities", 0, 0, 1);
    }

}); //end of document.ready

function GetWidget(widgetName, widgetUpdateElement, widgetElement, widgetContentToLoad, bid, data, isProviderView, isByClient) {
    if ($(widgetElement).length > 0) {
        //this can be hard-coded to the weather widget divs since it only applies to that section if bid is set
        //if (bid != 0) {
        //    $('#weather').hide();
        //    $("#updateProgressWeather").show();
        //}

        //the first urlToLoad value can be hard-coded to the weather widget user control since it only applies to that section if bid is set
        var urlToLoad = (bid != 0) ? '_controls/LoadUserControl.aspx?uc=Weather&bid=' + bid : '_controls/LoadUserControl.aspx?uc=' + widgetName + '';

        if (data)
            urlToLoad += ('&data=' + data)

        if (isProviderView)
            urlToLoad += '&providerView=1';

        if (isByClient)
            urlToLoad += '&isByClient=1';

        $.ajax({
            type: 'GET',
            url: urlToLoad,
            success: function (result) {
                $(widgetElement).html($(result).find(widgetContentToLoad).html());

                if (widgetName == "BuildingMap" && document.getElementById("geocodeCredentialsKey") != null) {
                    geocodeCredentialsKey = document.getElementById("geocodeCredentialsKey").value;
                    userDataforBuildingMap = JSON.parse(document.getElementById("userDataforBuildingMap").value);
                    userCulture = document.getElementById("userCulture").value;
                    GetMap();
                }

                if (widgetName == "BuildingDataPointsPieChart" && document.getElementById("buildingPointPieChartString") != null) {
                    buildingPointPieChartString = document.getElementById("buildingPointPieChartString").value.replace(/\|/g, "\"");

                    drawChart();
                }

                //if (widgetName == "Weather") {
                //    //this is for when the user chooses another building in the ddl for the weather widget
                //    $("#ddlWeatherBuilding").change(function () {
                //        GetWidget(widgetName, widgetUpdateElement, widgetElement, widgetContentToLoad, $(this).val());
                //    });
                //}

                $(widgetUpdateElement).hide();
                $(widgetElement).hide().fadeIn(fadeInTime);

                if ($('.lnk').length > 0) {
                    widgetsLoaded.push(widgetUpdateElement);

                    if (widgetsLoaded.length == docks.length) {
                        //$('.lnk').removeAttr('disabled');
                        //$('.lnk').attr('href', href);
                        //$('.lnk').fadeTo("slow", 1);

                        widgetsLoaded.length = 0
                    }
                }
            },
            error: function (message) {
                $(widgetUpdateElement).hide();
            }
        });
    }
}