﻿window.ConsumptionWidget =
{
    cmbBuilding_change:
	function (event) {
	    var e = event.target;

	    this.load(e.value, e.ownerDocument.querySelector("[id$='cmbUtility']").value)
	},

    cmbUtility_change:
	function (event) {
	    var e = event.target;

	    this.load(e.ownerDocument.querySelector("[id$='cmbBuilding']").value, e.value)
	},

    load:
	function (bid, utility) {
	    $("#divConsumptionComparison").hide()
	    $("#updateConsumptionComparison").show()

	    GetWidget
		(
			"ConsumptionComparison",
			"#updateConsumptionComparison",
			"#divConsumptionComparison",
			"#contentToLoadConsumptionComparison",
			0,
			((bid && utility) ? JSON.stringify({ bid: bid, utility: utility, }) : undefined)
		)
	}
}