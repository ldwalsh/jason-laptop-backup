﻿var buildingPointPieChartString;

function drawChart() {
    var data = google.visualization.arrayToDataTable(JSON.parse(buildingPointPieChartString));

    var options = {
        title: 'Building To Data Points Percentage',
        is3D: 'true',
    };

    var chart = new google.visualization.PieChart(document.getElementById('chart_div2'));
    chart.draw(data, options);
}