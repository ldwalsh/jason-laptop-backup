﻿var widgetsProcessed = new Array();

var avoidableEnergyCostsTitleByBuilding = "Building Avoidable Energy Costs for Past 30 Days";
var faultsTitleByBuilding = "Building Faults for Past 30 Days";
var avoidableEnergyCostsTitle = "Porfolio Avoidable Energy Costs for Past 30 Days";
var faultsTitle = "Porfolio Faults for Past 30 Days";
var averagePrioritiesTitle = "Porfolio Average Priorities for Past 30 Days";

var rangeText = 'Past 30 Days';
var plotRange = 'Past30Days';
var plotType = 'Portfolio';

var fadeInTime = 2000;
var loadingImage = '<img src="_assets/images/loading.gif" alt="loading" />';
var noDataMessage = "No data for {0}";

var ddlInterval = null;
var ddlRange = null;
var ddlBuildings = null;
var ddlEquipmentClass = null;
var ddlEquipment = null;

//object var for use with the diagnostics links attached to each point
var d = {
    cid: null,
    bid: null,
    interval: null,
    equipmentClassID: null,
    eid: null
};

jQuery(document).ready(function () {

    //pre-load the loading images
    PreLoadWidgetProgressImage(".updateProgressDivForWidgets");

    //nothing necessary here, just trigger the click (once the page loads and the DOM is ready) and the LoadCharts function handles the rest
    setTimeout(function () {
        jQuery('.imgBtn').click(function () { }).trigger('click');
    }, 1000)
});

//this function does the following, reset the processed widgets count for the next pass, display the loading image and hide the widgets data, disable the ddls and refresh button and lastly processes the ajax request for each widget
function LoadCharts() {
    widgetsProcessed.length = 0;  //need to reset the array with each call to this function, otherwise the disabling/enabling of controls code malfunctions

    SetPlotTypeAndRangeAndFormatChartTitles();

    DisplayLoadingProgressImage(".updateProgressDivForWidgets");
    HideProcessingWidgets(".processingWidgetDiv");

    jQuery('.dropdown').prop('disabled', true);

    jQuery('.imgBtn').prop("onclick", null);
    jQuery('.imgBtn').fadeTo("slow", 0.5);

    jQuery('.imgDashboardDownload').prop("disabled", true);
    jQuery('.imgDashboardDownload').fadeTo("slow", 0.5);

    ProcessAjaxRequest('#buildingAvoidableEnergyCostsByBuilding', '#updateProgressBuildingAvoidableEnergyCostsByBuilding', avoidableEnergyCostsTitleByBuilding, "Cost", "Cost", plotRange, plotType, "Building", "Bar");
    ProcessAjaxRequest('#buildingFaultsByBuilding', '#updateProgressBuildingFaultsByBuilding', faultsTitleByBuilding, "Faults", "Faults", plotRange, plotType, "Building", "Bar");
    ProcessAjaxRequest('#buildingAvoidableEnergyCosts', '#updateProgressBuildingAvoidableEnergyCosts', avoidableEnergyCostsTitle, "Cost", "Cost", plotRange, plotType, "Date", "Line");
    ProcessAjaxRequest('#buildingFaults', '#updateProgressBuildingFaults', faultsTitle, "Faults", "Faults", plotRange, plotType, "Date", "Line");
    ProcessAjaxRequest('#buildingPriorities', '#updateProgressBuildingPriorities', averagePrioritiesTitle, "Average Priorities", "Priorities", plotRange, plotType, "Date", "Line");
}

//this function takes in the div used for the loading image and sets the loading image for use later
function PreLoadWidgetProgressImage(widgetUpdateElementByClassName) {
    jQuery(widgetUpdateElementByClassName).html(loadingImage);
}

//this function takes in the div used for the loading image and displays it while the widget is processing
function DisplayLoadingProgressImage(widgetUpdateElementByClassName) {
    jQuery(widgetUpdateElementByClassName).show();
}

//this function takes in the div used for the processing widget (i.e. widget with the chart image) and hides it while the widget is processing
function HideProcessingWidgets(widgetProcessingElementByClassName) {
    jQuery(widgetProcessingElementByClassName).hide();
}

//this function takes in all the necessary items for processing the widget and processes it based off an ajax call to the ChartPage.aspx file (containing the radchart control)
//if successful it runs the processWidget function, if error, it processes the empty widget, hides the loading image, and display the no data message
function ProcessAjaxRequest(widgetElementForResult, widgetUpdateElement, chartTitle, YAxisTitle, chartType, plotRange, plotType, chartTypeGrouping, defaultType, isError) {
    
	var chartProps = jQuery.extend(true, {}, chartPropsTemplate);
	
	chartProps.ChartTitle.TextBlock.Text = chartTitle;
    chartProps.PlotArea.YAxis.AxisLabel.TextBlock.Text = YAxisTitle;
    chartProps.DefaultType = defaultType;

    if (chartTypeGrouping == "Date") {     
        chartProps.Width = "1034px";
        chartProps.PlotArea.XAxis.AutoScale = false;
        chartProps.PlotArea.XAxis.IsZeroBased = false;

        if (widgetElementForResult == "#buildingAvoidableEnergyCosts") {
            var div = jQuery("div.totalAvoidableCostSelectedDiv")[0];

            if (div != null) jQuery(div).remove();
        }
    }
    else {
        chartProps.Width = "504px";

		if (widgetElementForResult == "#buildingAvoidableEnergyCostsByBuilding")
		{
			chartProps.PlotArea.XAxis.Appearance.LabelAppearance = {RotationAngle: -45, Position: { AlignedPosition: "BottomLeft" }, }
		}

		chartProps.PlotArea.XAxis.Appearance.ValueFormat = "General";
		chartProps.PlotArea.XAxis.DataLabelsColumn = "#X";
    }

    //need to obtain the values here, otherwise all ddl objects would be null
    GetDropDownValues();

    //common variable items for the plotInfo and d object.  The latter is used with the diagnostics page
    var cidForRequest = document.getElementById('cid').value;
    var bidForRequest = (ddlBuildings != null && ddlBuildings.value != "-1") ? parseInt(ddlBuildings.value) : null;
    var equipmentClassIDForRequest = (ddlEquipmentClass != null && ddlEquipmentClass.value != "-1") ? parseInt(ddlEquipmentClass.value) : null;
    var eidForRequest = (ddlEquipment != null && ddlEquipment.value != "-1") ? parseInt(ddlEquipment.value) : null;

    plotInfo.ChartType = chartType;
    plotInfo.ChartTypeGrouping = chartTypeGrouping;
    plotInfo.PlotRange = plotRange;
    plotInfo.PlotType = "Portfolio";

    d.cid = cidForRequest;
    d.interval = (ddlInterval != null) ? ddlInterval.value : null;
    d.bid = bidForRequest;
    d.equipmentClassID = equipmentClassIDForRequest;
    d.eid = eidForRequest;

    if (chartTypeGrouping == "Date") {
        plotInfo.PlotType = plotType;
        plotInfo.BID = bidForRequest
        plotInfo.EquipmentClassID = equipmentClassIDForRequest;
        plotInfo.EID = eidForRequest;
    }

    //this is used only in cases where the image failed to load.  We append dateTime to the url querystring to force a reload vs. reloaded the error from cache
    var dateTime = new Date().getTime();

    jQuery.ajax({
        type: 'POST',
        url: "_dashboard/ChartPage.aspx" + ((isError) ? "?r=" + dateTime : ""),
		data:
		{
			chartProps: JSON.stringify(chartProps),
			plotInfo:	JSON.stringify(plotInfo),
			cid:		cidForRequest,
		},
        dataType: 'html',
        success: function (result) {
            ProcessWidget(widgetElementForResult, widgetUpdateElement, result, YAxisTitle, chartTitle, chartType, plotRange, plotType, chartTypeGrouping, defaultType);
        },
        error: function (message) {
            ProcessEmptyWidget(widgetElementForResult, widgetUpdateElement, chartTitle, rangeText, null);
        }
    });
}

//this function takes in all the necessary items to process the widget.  There is a lot this function must execute in order to catch and retry errors in loading the images
//1) Firstly, if there are no results back (i.e. completed but no data) we just have to show the div with no data message and hide the loading image
//2) Otherwise, modify the src attribute of the image to _src, this is incase of an error, it doesn't attempt to load the broken or missing image
//3) Next set the resulting markup to a temporary div, and pull out the chart and image element into a variable for later use
//4) Next remove the default onerror attribute for the image and replace it with our custom version.  This custom version will attempt to keep loading the image via the ajax call until successful
//4) Once the widget is finally loaded, hide the loading image and display the widget contents (i.e. the image), this is done by setting the src value back to what it was originally set to in the beginning
//5) Lastly, call the enable filters function incase all the widgets were fully processed we can enable the filters again
function ProcessWidget(widgetElementForResult, widgetUpdateElement, ajaxResult, YAxisTitle, chartTitle, chartType, plotRange, plotType, chartTypeGrouping, defaultType) {
    if (ajaxResult.length == 1) {
        jQuery(widgetElementForResult).addClass("dockMessageLabel");
        jQuery(widgetElementForResult).html(String.format(noDataMessage, chartTitle));
        jQuery(widgetUpdateElement).hide();

        DisplayWidget(widgetElementForResult, ajaxResult, chartTypeGrouping);
    }
    else if (ajaxResult.indexOf("You are in the wrong location") != -1) {
        jQuery(widgetElementForResult).addClass("dockMessageLabel");
        jQuery(widgetElementForResult).html(String.format(noDataMessage, chartTitle));
        jQuery(widgetUpdateElement).hide();

        jQuery(widgetElementForResult).fadeIn(fadeInTime);
    }
    else {
        ajaxResult = ajaxResult.replace("src='ChartImage.axd?UseSession", "_src='ChartImage.axd?UseSession");
        var div = document.createElement('div');

        div.innerHTML = ajaxResult;
        var chart = jQuery(div).find('div[id^="chart"]')[0];
        var img = jQuery(chart).find('img')[0];

        img.setAttribute('onerror', "");

        jQuery(img).error(function () {
            jQuery(widgetElementForResult).hide();
            ProcessAjaxRequest(widgetElementForResult, widgetUpdateElement, chartTitle, YAxisTitle, chartType, plotRange, plotType, chartTypeGrouping, defaultType, true);
        });

        jQuery(img).load(function () {
            jQuery(widgetUpdateElement).hide();

            DisplayWidget(widgetElementForResult, ajaxResult, chartTypeGrouping);
        });

        jQuery(widgetElementForResult).removeClass("dockMessageLabel");
        jQuery(widgetElementForResult).html("");
        jQuery(widgetElementForResult).append(chart);

        img.src = img.getAttribute("_src").replace("ChartImage.axd", "CustomChartImage.axd");
        img.removeAttribute("_src");
    }

    EnableFiltersIfWidgetsProcessed(widgetElementForResult);
}

//this function is only necessary because when an empty widget is processed (i.e. AJAX error), there still is a requirement to add the widget as "processed", so the filters will enable properly once all are processed
function ProcessEmptyWidget(widgetElementForResult, widgetUpdateElement, chartTitle, rangeText, ajaxResult) {
    jQuery(widgetElementForResult).addClass("dockMessageLabel");
    jQuery(widgetElementForResult).html(String.format(noDataMessage, chartTitle, rangeText));
    jQuery(widgetUpdateElement).hide();

    DisplayWidget(widgetElementForResult, ajaxResult);

    EnableFiltersIfWidgetsProcessed(widgetElementForResult);
}

//this function is used to display the widget when a successful ajax request has been received, and to load the chart image to the widget
//there is also some code to disable the default context menu and replace it with our custom context menu for each point.  These context menus load the diagnostics pages either in a new window or parent window,
//based on the selection from the custom context menu
function DisplayWidget(widgetElementForResult, ajaxResult, chartTypeGrouping) {
    //this code covers the requirement to load the "Total Avoidable Cost" monetary value for the Avoidable Costs chart as a separate (temporarily created) div
    if (widgetElementForResult == "#buildingAvoidableEnergyCosts") {

        if (ajaxResult != null) {
            if (ajaxResult.length > 1 && jQuery(ajaxResult).find('#hdnCostTotal')[0] != null) {

                var div = document.createElement('div');

                div.textContent = "";

                div.textContent = (jQuery(ajaxResult).find('#hdnCostTotal')[0].value != "") ? 'Total Avoidable Cost Selected: ' + jQuery(ajaxResult).find('#hdnCostTotal')[0].value : "";
                div.classList.add('totalAvoidableCostSelectedDiv');
                //div.classList.add('processingWidgetDiv');

                jQuery(widgetElementForResult).before(div);
                jQuery(div).fadeIn(fadeInTime);
            }
        }
    }

    jQuery(widgetElementForResult).fadeIn(fadeInTime);

    //override default context menu code
    if (ajaxResult != null) {
        if (ajaxResult.length > 1) {
            var map = jQuery(widgetElementForResult).find('map')[0].children;
            var cM = iCL.ui.controls.ContextMenu.NEW($gId("ulContextMenu").clone(), map).SET({ mouseButton: iCL.ui.controls.ContextMenu.ButtonEnum.Left })

            cM.onClick = ContextMenuClick;
        }
    }
}

//this function is used to re-enable all the widget filters once all widgets have processed (both successful and errored widgets).
function EnableFiltersIfWidgetsProcessed(widgetElementForResult) {
    widgetsProcessed.push(jQuery(widgetElementForResult));

    if (widgetsProcessed.length == jQuery('.updateProgressDivForWidgets').length) {
        jQuery('.dropdown').prop('disabled', false);

        jQuery('.imgBtn')[0].setAttribute('onclick', 'LoadCharts()');
        jQuery('.imgBtn').fadeTo("slow", 1);

        jQuery('.imgDashboardDownload').prop("disabled", false);
        jQuery('.imgDashboardDownload').fadeTo("slow", 1);
    }
}

//this function sets the plot type and range chart titles
function SetPlotTypeAndRangeAndFormatChartTitles() {
    SetPlotTypeAndRange();

    var plotTypeForTitle = (plotType == "EquipmentClass") ? "Equipment Class" : plotType;

    faultsTitleByBuilding = String.format("Building Faults for {0}", rangeText);
    avoidableEnergyCostsTitleByBuilding = String.format("Building Avoidable Energy Costs for {0}", rangeText);
    avoidableEnergyCostsTitle = String.format("{0} Avoidable Energy Costs for {1}", plotTypeForTitle, rangeText);
    faultsTitle = String.format("{0} Faults for {1}", plotTypeForTitle, rangeText);
    averagePrioritiesTitle = String.format("{0} Average Priorties for {1}", plotTypeForTitle, rangeText);
}

//this function sets the plot type and range based on the ddl values
function SetPlotTypeAndRange() {
    //need to obtain the values here, otherwise all ddl objects would be null
    GetDropDownValues();

    if (ddlRange != null) {
        rangeText = ddlRange.options[ddlRange.selectedIndex].text;
        plotRange = ddlRange.value;
    }

    if (ddlEquipment != null && ddlEquipment.value != "-1")
        plotType = 'Equipment';
    else if (ddlEquipmentClass != null && ddlEquipmentClass.value != "-1")
        plotType = 'EquipmentClass';
    else if (ddlBuildings != null && ddlBuildings.value != "-1")
        plotType = 'Building';
    else
        plotType = 'Portfolio';
}

//this function obtains the ddl values for the chart filter and diagnostics page values
function GetDropDownValues() {
    ddlInterval = document.querySelector("[id$='ddlInterval']");
    ddlRange = document.querySelector("[id$='ddlRange']");
    ddlBuildings = document.querySelector("[id$='ddlBuildings']");
    ddlEquipmentClass = document.querySelector("[id$='ddlEquipmentClass']");
    ddlEquipment = document.querySelector("[id$='ddlEquipment']");
}

//this function load the diagnostics page either in a new window or parent window, based on user context menu selection
function ContextMenuClick(eA) {
    var url = CreateDiagnosticsURL(eA.element.getAttribute("data"));

    if (eA.menuItem == "Open here")
        window.location.href = url;
    else
        window.open(url, null, "location=1,resizable=1,scrollbars=1,toolbar=1,height=768,width=1024");
}

//this function create the diagnostics url to load, based on the d object values set when each widget was processed
function CreateDiagnosticsURL(data) {
    var dataForUrl = data.split("|");
    var bid = dataForUrl[0];
    var startDate = dataForUrl[1];
    var endDate = dataForUrl[2];

    var url = "/Diagnostics.aspx?cid={0}&bid={1}&rng={2}&sd={3}&ed={4}";
    
    if (!bid) {
        bid = (d.bid) ? d.bid : 0;

        if (d.equipmentClassID)
            url += "&ecid={5}";

        if (d.eid)
            url += "&eid={6}";
    }
    
    return url.format(d.cid, bid, d.interval, startDate, endDate, d.equipmentClassID, d.eid)
}

function GetHtml(container, markup) {
    var radDocks = document.querySelectorAll('.RadDock');

    for (i = 0; i < radDocks.length; i++)
        radDocks[i].style.height = radDocks[i].offsetHeight + 'px';

    document.getElementById(container).value = '';
    document.getElementById(container).value = "<head>" + document.head.innerHTML + "</head><body style='background: none;'>" + document.getElementById(markup).innerHTML + "</body>";

    for (i = 0; i < radDocks.length; i++)
        radDocks[i].style.height = '';
}