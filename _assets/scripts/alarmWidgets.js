﻿var widgetsLoaded = new Array();
var docks = new Array();

var progressDivs;
var widgetDivs;
var iframes;
var iframesContainer;
var updateProgressTxt = "updateProgress";
var frameTxt = "Frame";

var dateFields;
var sd;
var ed;

var inDragState;
var radDocks;

var pgRegMgr = Sys.WebForms.PageRequestManager.getInstance();

//When the dom is ready, obtain all div and frame objects for widget modification, then set the initial html for the progress divs (loading gif) and set all frame sources (using temp function to ensure the calendars are rendered).
$(document).ready(function () {
    progressDivs = $.makeArray(document.getElementsByClassName('updateProgressDivForWidgets'));
    iframes = $.makeArray(document.getElementsByTagName('iframe'));
    widgetDivs = $.makeArray(document.getElementsByClassName('divForWidgets'));
    iframesContainer = document.getElementById('iframesContainer');
    radDocks = document.querySelectorAll(".radDockMedium, .radDockLarge");

    for (var i = 0; i < progressDivs.length; i++) progressDivs[i].innerHTML = "<img src='_assets/images/loading.gif' alt='loading' />";

    var f = function () {
        //TODO: figure out the best way to obtain the id more generically
        GetDataFields();
        
        if (!sd || !ed) return setTimeout(f);

        for (var i = 0; i < iframes.length; i++) iframes[i].src += GetQSForIframeWithDateRange(iframes[i], GetFullUTCMilliseconds(sd), GetFullUTCMilliseconds(ed));
    };

    f();

    EnableFilters(false);
});

//Important function.  When the browser window resizes, we need to ensure that the frames content remains inside its appropriate docks.  Otherwise the browser will resize and the frame content remains static.
$(window).resize(function () {
    for (var i = 0; i < widgetDivs.length; i++) {
        var offset = $('#' + widgetDivs[i].id).offset();
        var iframeStyle = iframes[i].style;

        iframeStyle.top = offset.top + 'px';
        iframeStyle.left = offset.left + 'px';
    }
});

//Get an instance of the page manager for setting functions if the page in processing a request.  When the request starts, disable dock functionality, if it's complete, enable them.
pgRegMgr.add_beginRequest(BeginHandler);
pgRegMgr.add_endRequest(EndHandler);

function BeginHandler() {
    EnableDockFunctionality(false);
}

function EndHandler() {
    GetDataFields();
    EnableDockFunctionality(true);
}

//This is required to autosize the iframes content on either an asycn postback or widget being initially loaded (initially thought it was unnecessary but is required for Chrome, Safari and Opera browsers).
function AutoResize(widget) {
    var newheight;
    var newwidth;
    
    var iframe = document.getElementById(widget + frameTxt);
    var placeholder = document.getElementById(widget);

    //this line helps correct a resizing issue for the Chrome, Safari and Opera browsers.  If the height isn't initially set to 0, those browsers add mysterious padding to the bottom of the frame.
    iframe.height = 0;

    if (iframe == null)
        throw new Error('Widget ' + frameId + ' was not found');

    if (iframe.contentDocument.body != null) {
        newheight = iframe.contentDocument.body.scrollHeight;
        newwidth = iframe.contentDocument.body.scrollWidth;

        var placeholderStyle = placeholder.style;
        var iframeStyle = iframe.style;
        
        placeholderStyle.height = (newheight) + "px";
        placeholderStyle.width = (newwidth) + "px";

        iframeStyle.height = (newheight) + "px";
        iframeStyle.width = (newwidth) + "px";
    }
}

//When the widget iframe is loaded, hide the spinner and show the content (relative to its parent dock) only if the frame src has been set, also toggle the filter fields based on initial load or reprocessed widgets.
function ShowIframe(widget) {
    var iframe = document.getElementById(widget + frameTxt);
    var updateProgressPlaceholder = document.getElementById(updateProgressTxt + widget.charAt(0).toUpperCase() + widget.slice(1));
    var placeholder = document.getElementById(widget);

    if (iframe.src == "about:blank")
        return;

    placeholder.iframe = iframe;
    iframe.placeholder = placeholder;

    widgetsLoaded.push(iframe.id);

    updateProgressPlaceholder.style.display = 'none';

    $(iframe).css('zIndex', 1) //have to set the z-index to some value other than "auto" so it will be placed over the dock.
    
    HideDDLFromWidget(iframe); //hide the ddl for buildings if one exists, and if there is only one building.

    if (widgetsLoaded.length == radDocks.length) { 
        EnableFilters(true);
        EnableDockFunctionality(true);
        widgetsLoaded.length = 0
    }

    iframe.style.visibility = 'visible';

    AutoResize(widget); //auto resize the frame and placeholder to make sure the content fits inside the dock.

    AdjustIframeOverlays();
}

function AdjustIframeOverlays() {
    for (var i = 0; i < widgetDivs.length; i++) {
        var iframeStyle = iframes[i].style;

        if (iframeStyle == 'none')
            continue;

        var offset = $(widgetDivs[i]).offset();

        iframeStyle.top = offset.top + 'px';
        iframeStyle.left = offset.left + 'px';
    }
}

function OnClientInitialize(dock, args) {
    dock.set_enableDrag(false);
    dock.get_commandsContainer().style.display = "none";

    docks.push(dock);
}

//When the widget is "dragged", we need to check if it's initially loading before allowing draggability.
function OnClientDragStart(dock, args) {
    //if they move all docks to another zone, the current zone is lost (bad Telerik!), so this conditional will check if there is only one dock left in the zone and disable draggability.
    if (dock.get_parent().get_docks().length == "1") {
        CancelRequest(dock, args, 'drag', true);
        return;
    }
    
    ////make sure to expand the widget before moving, so the iframe's connector will be visible for reattaching.
    if (dock.get_collapsed()) CancelRequest(dock, args, 'setCollapsed', false)

    iframesContainer.style.display = 'none';

    inDragState = true;

    $(window).mouseup(function () {
        OnClientDragEnd(dock, args);
    });    
}

function OnClientDragEnd(dock, args) {
    inDragState = false;
    $(window).unbind("mouseup");

    setTimeout(function () {
        iframesContainer.style.display = '';
        AdjustIframeOverlays();
    }, 500);
}

//When the widget is being "expanded/collapsed", we simply need to hide or show the iframe content based on the get_collapsed dock method.
function OnClientCommand(dock, args) {
    var placeholder = dock.get_contentContainer().getElementsByClassName('divForWidgets')[0];

    placeholder.iframe.style.display = (dock.get_collapsed()) ? 'none' : 'block';

    AdjustIframeOverlays();
}

//Function to cancel either the drag request or command request, the command request couldn't be "cancelled" per se, due to Telerik limitations, so had to get a bit fancy with their api to fix the issue
function CancelRequest(dock, args, event, boolean) {
    (event == "drag") ? args.set_cancel(boolean) : dock.set_collapsed(boolean);
}

//When the widget iframe is loaded, hide ddl if there is only one building to display.
function HideDDLFromWidget(iframe) {
    //for this section, we need to check if there was only one building for the client.  If so, we need to hide the containing div per widget.
    var iFrameContentWindow = iframe.contentWindow;
    var ddl = iFrameContentWindow.document.querySelector("select[lookup-id='ddlBuildings']"); //add data to the beginning of the attribute
    var divBuilding = iFrameContentWindow.document.getElementById('divBuilding');
    var parentDiv = iFrameContentWindow.document.querySelector("select[set-css-to='divDockFormFirst']");

    if (ddl != null && divBuilding != null) {
        if (ddl.length == 0) {
            divBuilding.style.display = 'none';

            //In the case of the search widget, change the css of the parent div to "divDockFormFirst", as the second ddl has now become the "first".
            if (parentDiv != null) {
                if (parentDiv.parentNode != null)
                    parentDiv.parentNode.className = "divDockFormFirst";
            }
        }
    }
}

//When the pdf generator button is clicked we simply take the docks content, place it in a temp div, hide the iframe (doesn't work with the pdf generator), and when done remove the temp div and show the iframe.
function GetHtml(container, markup) {
    var divArray = new Array();

    for (var i = 0; i < radDocks.length; i++) {
        var radDock = radDocks[i];

        radDock.style.height = radDock.offsetHeight + 'px';

        var placeholder = radDock.querySelectorAll('.divForWidgets')[0];
        var placeholderIframe = placeholder.iframe;
        var div = document.createElement('div');
            
        div.setAttribute('style', 'width: ' + placeholderIframe.clientWidth + 'px; height: ' + placeholderIframe.clientHeight + 'px;');
        div.innerHTML = placeholderIframe.contentWindow.document.head.innerHTML + placeholderIframe.contentWindow.document.body.innerHTML;

        divArray.push(div);
            
        radDock.querySelector('.rdContent').appendChild(div);
    }
    
    document.getElementById(container).value = '';

    dateFields[0].setAttribute('value', dateFields[0].value);
    dateFields[1].setAttribute('value', dateFields[1].value);

    document.getElementById(container).value = "<head>" + document.head.innerHTML + "</head><body style='background: none;'>" + document.getElementById(markup).innerHTML + "</body>";

    for (var i = 0; i < radDocks.length; i++) {
        var radDock = radDocks[i];
        var placeholder = radDock.querySelectorAll('.divForWidgets')[0];
        var placeholderIframe = placeholder.iframe;

        radDock.style.height = '';
        radDock.querySelector('.rdContent').removeChild(divArray[i]);
    }
}

//Function to ensure the date fields are valid and processes the widgets with the valid date (UTC) values selected, executed when the refresh data button is clicked.
function CheckDateFieldsAndProcess() {
    //have to reload the browser window if the user is timed out, otherwise reload the new data.
    $.ajax({
        type: 'POST',
        url: "_controls/LoadUserControl.aspx/CheckLoggedIn",
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (parseInt(response.d))
                document.location.reload();
            else {
                GetDataFields();

                if (AreFieldsEmpty(dateFields))
                    alert("Please enter both Start Date and End Date.");
                else if (GetFullUTCMilliseconds(sd) > GetFullUTCMilliseconds(ed))
                    alert("Start Date must be less than End Date.");
                else
                    ProcessWidgets(GetFullUTCMilliseconds(sd), GetFullUTCMilliseconds(ed));
            }
        }
    });
}

//Function to process the widgets which appends the start and end date values to the URL's query string, also disables the filters and hides the iframe while processing.
function ProcessWidgets(sd, ed) {
    EnableFilters(false);

    for (var i = 0; i < docks.length; i++) {
        docks[i].set_enableDrag(false);
        docks[i].get_commandsContainer().style.display = "none";
    }

    for (var i = 0; i < iframes.length; i++) {
        var iframe = iframes[i];
        var iframeStyle = iframe.style;
        var iframeSrc = iframe.src;

        if (iframe.getAttribute("data-skip-reload") == null) {
            iframeStyle.visibility = 'hidden';
            iframeStyle.height = 0;
            iframe.placeholder.style.height = 0;

            iframeSrc = iframeSrc.substring(0, iframeSrc.indexOf("?"));
            iframeSrc += GetQSForIframeWithDateRange(iframe, sd, ed);
            iframe.src = iframeSrc;
        }
        else
            widgetsLoaded.push(iframe.id);
    }

    for (var i = 0; i < progressDivs.length; i++) {
        var progressDiv = progressDivs[i];

        progressDiv.style.display = (progressDiv.getAttribute("data-skip-reload") == null) ? 'block' : "none";
    }
}

function EnableFilters(action) {
    if (action) {
        $(dateFields).prop("disabled", false);
        $(dateFields).fadeTo("slow", 1);

        $('.imgCal').removeAttr("disabled");
        $('.imgCal').fadeTo("slow", 1);

        $('.imgBtn')[0].setAttribute('onclick', 'CheckDateFieldsAndProcess()');
        $('.imgBtn').fadeTo("slow", 1);

        $('.imgDashboardDownload').prop("disabled", false);
        $('.imgDashboardDownload').fadeTo("slow", 1);        
    }
    else {
        $(dateFields).prop("disabled", true);
        $(dateFields).fadeTo("slow", 0.5);

        $('.imgCal').attr('disabled', 'disabled')
        $('.imgCal').fadeTo("slow", 0.5);

        $('.imgBtn')[0].setAttribute('onclick', null);
        $('.imgBtn').fadeTo("slow", 0.5);

        $('.imgDashboardDownload').prop("disabled", true);
        $('.imgDashboardDownload').fadeTo("slow", 0.5);
    }
}

function EnableDockFunctionality(action) {
    for (var i = 0; i < docks.length; i++) {
        if (action) {
            (i == docks.length - 1) ? docks[i].set_enableDrag(false) : docks[i].set_enableDrag(true);
            docks[i].get_commandsContainer().style.display = "block";
        }
        else {
            (i == docks.length - 1) ? docks[i].set_enableDrag(true) : docks[i].set_enableDrag(false);
            docks[i].get_commandsContainer().style.display = "none";
        }
    }
}

function AreFieldsEmpty(fields) {
    var emptyFieldValues = false;

    for (var i = 0; i < fields.length; i++) {
        if (fields[i].value == "" || fields[i].value == "__/__/____") { //check for "__/__/____" needs to be here, since the field can also contain this "empty" value
            emptyFieldValues = true;
            break;
        }
    }

    return emptyFieldValues;
}

function GetFullUTCMilliseconds(d) {
    var date = d.get_dateInput().get_selectedDate();

    return date.getTime() + (date.getTimezoneOffset() * 60000);
}

//Function called only when the widget is initally loaded or reprocessed via filters, creates the updated src with start and end dates, and appropriate control to load via the user defined attribute.
function GetQSForIframeWithDateRange(iframe, qsSD, qsED) {
    return "?uc=" + iframe.getAttribute('control-to-load') + "&sd=" + qsSD + "&ed=" + qsED;
}

function AreWidgetsProcessing(asyncType) {
    var widgetsProcessing = false;

    for (var i = 0; i < iframes.length; i++) {
        var iframe = iframes[i];
        var iFrameContentWindow = iframe.contentWindow;

        if (iFrameContentWindow.Sys != null) {

            var prm = iFrameContentWindow.Sys.WebForms.PageRequestManager.getInstance();

            if (prm != null) {
                if (prm.get_isInAsyncPostBack()) {

                    if (asyncType == "asyncBuildingMap") {
                        var ddl = iFrameContentWindow.document.querySelector("select[data-buildingMap='attached']");

                        if (ddl != null) {
                            widgetsProcessing = true;
                            break;
                        }
                    }
                    else {
                        widgetsProcessing = true;
                        break;
                    }

                }
            }
        }
    }

    return widgetsProcessing;
}

function IsInitialLoad() {
    var isInitialLoad = false;

    for (var i = 0; i < radDocks.length; i++) {
        var dockSpinner = radDocks[i].querySelector('.updateProgressDivForWidgets');
        var dockSpinnerStyleDisplay = dockSpinner.style.display;

        if (dockSpinnerStyleDisplay == "" || dockSpinnerStyleDisplay == "block") {
            isInitialLoad = true;
            break;
        }
    }

    return isInitialLoad;
}

function GetDataFields() {
    dateFields = document.querySelectorAll(".data-text-box");

    sd = $find('ctl00_plcCopy_txtStartDate');
    ed = $find('ctl00_plcCopy_txtEndDate');
}