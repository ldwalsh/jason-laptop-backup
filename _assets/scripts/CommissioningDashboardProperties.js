﻿var chartPropsTemplate =
{
    Appearance: { Border: { Width: 0 } },
    DefaultType: "Line",
    Height: "250px",
    Legend:
    {
        Appearance:
        {
            Dimensions:
            {
                Margins: { Top: 0, Right: 0 },
                Paddings: { Top: 0, Right: 0, Bottom: 0, Left: 0 }
            },
            ItemMarkerAppearance: { Figure: "Rectangle" },
            Overflow: "Row",
            Position: { AlignedPosition: "Bottom" }
        }
    },
    ChartTitle:
    {
        Appearance:
        {
            Dimensions: { Margins: { Top: 0 }, Paddings: { Top: 0, Bottom: 0, Left: -4 } },
            Position: { AlignedPosition: "TopLeft" }
        },
        TextBlock: { Text: "", Appearance: { TextProperties: { Color: "Black", Font: "Arial; 11; Bold" } } }
    },
    PlotArea:
    {
        Appearance: { Border: { Color: "DarkGray" }, Dimensions: { Margins: { Top: 26, Right: 1, Bottom: 55 } }, FillStyle: { MainColor: "White", SecondColor: "White" } },
        XAxis:
        {
            Appearance:
            {
                MajorGridLines: { Color: "DarkGray", PenStyle: "Solid" },
                TextAppearance: { TextProperties: { Color: "DimGray", Font: "Arial; 7.5; Regular" } },
                MajorTick: { Color: "DimGray" },
            },
            AxisLabel: { Visible: true, TextBlock: { Appearance: { TextProperties: { Color: "DimGray", Font: "Arial; 9; Bold" } }, Text: "" } },
        },
        YAxis:
        {
            AutoScale: false,
            Appearance:
            {
                MajorGridLines: { Color: "DarkGray" },
                MinorGridLines: { Color: "LightGray" },
                MinorTick: { Visible: false },
                TextAppearance: { TextProperties: { Color: "DimGray", Font: "Arial; 7.5; Regular" } },
                MajorTick: { Color: "DimGray" },
            },
            AxisLabel: { Visible: true, TextBlock: { Appearance: { TextProperties: { Color: "DimGray", Font: "Arial; 9; Bold" } }, Text: "" } },
        }
    }
}

var plotInfo =
{
    ChartType: "Cost",
    ChartTypeGrouping: "Date",
    PlotRange: "Past30Days",
    PlotType: "Portfolio",
    BID: null,
    EquipmentClassID: null,
    EID: null
}