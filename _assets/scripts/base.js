//JQUERY's READY EVENT
(function($){
$(function(){

	//SECONDARY NAV
	var hoverColour = "#000000";  //text hover color

	$("a.hoverBtn").show("fast", function() {
		$(this).wrap("<div class=\"hoverBtn\">");
		$(this).attr("class", "");
	});

	$("a.hoverBtnNoCursor").show("fast", function() {
		$(this).wrap("<div class=\"hoverBtn\">");

		//set no cursor
		$(this).attr("class", "noCursor");
	});

	//display the hover div
	$("div.hoverBtn").show("fast", function() {
		//append the background div
		$(this).append("<div></div>");

		//get link's size
		var wid = $(this).children("a").width();
		var hei = $(this).children("a").height();

		//set div's size
		$(this).width(wid);
		$(this).height(hei);
		$(this).children("div").width(wid);
		$(this).children("div").height(hei);

		//on link hover
		$(this).children("a").hover(function() {
			//store initial link colour
			if ($(this).attr("rel") == "") {
				$(this).attr("rel", $(this).css("color"));
			}
			//fade in the background
			$(this).parent().children("div")
					.stop()
					.css({ "display": "none", "opacity": "1" })
					.fadeIn("fast");
			//fade the colour
			$(this).stop()
					.css({ "color": $(this).attr("rel") })
					.animate({ "color": hoverColour }, 350);
		}, function() {
			//fade out the background
			$(this).parent().children("div")
					.stop()
					.fadeOut("slow");
			//fade the colour
			$(this).stop()
					.animate({ "color": $(this).attr("rel") }, 250);
		});
	});
});
})(jQuery);

//Fixed with AjaxControlToolkit v15.1.2.0
////ON CALANDER SHOW POP UP Z-INDEX FIX
//function onCalendarShown(sender, args) {
//	sender._popupBehavior._element.style.zIndex = 10002;
//}

//TEXT AREA CHAR LIMIT
function limitChars(textarea, limit, infodiv) {
    var info = document.getElementById(infodiv);
    if (null != info) { // Only process if info element exists
        var text = textarea.value;
        var textlength = text.length;
        if (textlength > limit) {
            info.innerHTML = 'You cannot write more than ' + limit + ' characters.';
            textarea.value = text.substr(0, limit);
            return false;
        }
        else {
            info.innerHTML = 'You have ' + (limit - textlength) + ' characters left.';
            return true;
        }
    }
}

window.KGS_ = //can be removed once EquipmentForm no longer use Namespace method (which it doesn't need to do)
{
	Namespace:
	function(namespaceString)
	{
	    ///<param name="namespaceString" type="String" />
		var n = namespaceString.split(".");
		var current = window;
		
		for (var i=0; i<n.length; i++)
		{
			var a = n[i];
			
			if (!current[a]) current[a] = {};

			current = current[a];
		}
	},
};

function sliderOnChange(sender, id) {
    document.getElementById(id).value = sender.value;
}

function confirmRequired(textbox, requiredValidator, requiredValidatorExtenderPopupTable) {
    var text = document.getElementById(textbox).value;
    var rv = document.getElementById(requiredValidator);
    var textlength = text.length;

    ValidatorEnable(rv, (textlength > 0));

    if (!(textlength > 0)) {
        var rve = document.getElementById(requiredValidatorExtenderPopupTable);
        if (rve != null) rve.style.visibility = "hidden";
    }
}