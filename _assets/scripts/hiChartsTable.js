﻿window.KGS = window.KGS || {}; //set the scope of this file

//Create the data table (if the chart exists) and add header and data rows if any
//note- container name does not inlcude the '_container' suffix
KGS.DrawTable = function (containerName, tableName) {

    var container = jQuery(containerName + '_container');

    if (!container || !container.highcharts()) return;

    var hiChart = container.highcharts();

    if (!hiChart || !hiChart.xAxis[0].categories || !hiChart.series) return;

    var series = hiChart.series;

    var categories = hiChart.xAxis[0].categories

    var table = $(tableName);

    if (table.length == 0) return;

    KGS.AddHeaderRow(table[0], series);

    for (i = 0; i < categories.length; i++)
        KGS.AddTableRow(table[0], categories, series, i, hiChart.tooltip);

    //if culture exists, add custom date and data parser (b/c tablesorter doesn't understand all culturized data)
    if (kendo && kendo.culture()) 
        KGS.SetCultureParsers(table, series, containerName);

};

//applies custom parsers to sort on tablesorter columns
KGS.GetHeadersWithSorters = function (numColumns) {

    var headers = [{ sorter: 'dateNorm' }];

    //add sorter for all other columns
    for (i = 1; i <= numColumns; i++)
        headers.push({ sorter: 'dataNorm' });
    
    return headers;

}

//adds header row to table
KGS.AddHeaderRow = function (table, series) {

    var header = table.createTHead();

    var tr = document.createElement('tr');

    var thDate = document.createElement('th'); //add date column header

    thDate.innerHTML = 'Date';

    $(thDate).removeClass('header');

    tr.appendChild(thDate);

    //add column header for each series
    $.each(series, function (index, seriesItem) {

        var th = document.createElement('th');

        th.innerHTML = seriesItem.name;

        tr.appendChild(th);

    });

    header.appendChild(tr);

};

//adds data row to the table
KGS.AddTableRow = function (table, categories, series, i, tooltip) {

    if (!table || !table.tBodies) return;

    var prefix = "";

    var suffix = "";

    if (tooltip && tooltip.options) {

        prefix = (tooltip.options.valuePrefix) ? tooltip.options.valuePrefix : prefix;

        suffix = (tooltip.options.valueSuffix) ? tooltip.options.valueSuffix : suffix;

    }

    var tbody = table.tBodies[0];

    var tr = document.createElement('tr'); //create new row

    var td1 = document.createElement('td'); //add category

    var category = document.createTextNode(categories[i]);

    td1.appendChild(category);

    tr.appendChild(td1);

    $.each(series, function (index, seriesItem) {

        var data = seriesItem.data[i].y;

        var formattedData = kendo.toString(data, "n2");

        var td = document.createElement('td');

        var txtNode = document.createTextNode((formattedData) ? (prefix + formattedData + suffix).trim() : "");

        $(td).addClass("td");

        td.appendChild(txtNode);

        tr.appendChild(td);
    });

    $(tr).addClass("tr");

    tbody.appendChild(tr);

};

//sets 'culturized' parsers
KGS.SetCultureParsers = function (table, series, containerName) {

    if (!table || !series) return;

    var cultureNormDataParser = KGS.GetNormDataParser();

    var cultureNormDateParser = KGS.GetNormDateParser(containerName);

    $.tablesorter.addParser(cultureNormDataParser);

    $.tablesorter.addParser(cultureNormDateParser);

    var headersWithSorters = KGS.GetHeadersWithSorters(series.length);

    $(table[0]).tablesorter({

        headers: headersWithSorters

    });
}

//gets data parser for tablesorter that accounts for "culturized" data
KGS.GetNormDataParser = function () {

    if (!kendo || !kendo.culture()) return null;

    var culture = kendo.culture();

    var parser = {
        id: 'dataNorm',
        is: function (s) {
            return false;
        },
        format: function (s) {

            //set the currency group and decimal separators
            var decimalSeparator = '.'; //default

            var groupSeparator = ','; //default            

            //override defaults if there's a culture object present
            if (culture) {

                var numberFormat = culture["numberFormat"];

                if (numberFormat) {

                    decimalSeparator = (numberFormat["."]) ? numberFormat["."] : decimalSeparator;

                    groupSeparator = (numberFormat[","]) ? numberFormat[","] : groupSeparator;

                }
            };            

            //set regex to extract data value
            var pattern = "-?([0-9]+" + groupSeparator + "?)+" + decimalSeparator + "?[0-9]+-?";

            var regex = new RegExp(pattern);

            var match = regex.exec(s);

            //if there's no match return original data
            if (!match || !match[0]) return s;

            var data = match[0];            

            //remove the number group separator
            data = data.replace(groupSeparator, '');

            //replace decimal separator with '.' for format tablesorter/js can understand
            data = data.replace(decimalSeparator, '.');                                                  

            //check if there is a negative value - need to do this since some culture encoding
            //places the negative symbol AFTER the number
            var isNeg = data.indexOf('-') !== -1;

            //remove any '-' characters
            data = data.replace(/-/g, '');            

            //try to parse the data into numeric format
            var dataNumeric = parseFloat(data);

            //if data is not parsable to numeric just return the data
            if (!dataNumeric || dataNumeric == 'NaN') return data;

            if (isNeg)
            {

                dataNumeric = dataNumeric * -1;

            }            

            return dataNumeric;
        },
        type: 'numeric'
    };

    return parser;

};


//gets date parser for tablesorter that accounts for "culturized" dates
KGS.GetNormDateParser = function (containerName) {

    if (!kendo || !kendo.culture()) return null;

    var culture = kendo.culture();

    var parser = {
        id: 'dateNorm',
        is: function (s) {
            return false;
        },
        format: function (s) {            

            //try to get the name of the dateformat variable (set server-side, based 
            //in part on the container name)
            if (containerName) {
                
                var dateFormatVarName = "dotnetDateFormat_" + containerName.substring(1);

                //try to retrieve the date format from the page
                var dotnetDateFormat = window[dateFormatVarName];
                
            }

            //if the dotnet format is available, first try to parse in that format
            return (dotnetDateFormat) ? kendo.parseDate(s, dotnetDateFormat) : kendo.parseDate(s);

        },
        type: 'numeric'
    };

    return parser;

}

KGS.DownloadTable = function (tableName) {

    var table = jQuery(tableName);

    if (!table) return;

    var csv = [];

    //push each row in table onto csv array
    table.find('tr').each(function () {

        var headers = $(this).find('th');

        var rowData;

        var rowType = (headers && headers.length > 0) ? 'th' : 'td';

        rowData = KGS.GetTableRowElement($(this), rowType);

        csv.push(rowData + '\r\n');

    });

    //encode csv and 'open' a hidden link to trigger browser download
    var csvContent = 'data:text/csv;charset=utf-8,' + csv.join('').trim();

    var encodedUri = encodeURI(csvContent);

    var link = document.createElement('a');

    link.setAttribute("href", encodedUri);

    link.setAttribute("download", "energy_data.csv");

    document.body.appendChild(link);

    link.click();

}

//gets a table row element
KGS.GetTableRowElement = function (row, element) {

    var rowString = '';

    var trData = row.find(element);

    trData.each(function (index, element) {

        rowString += "\"" + $(this).text() + "\"";

        if (index != trData.length - 1) rowString += ',';

    });

    return rowString;

}