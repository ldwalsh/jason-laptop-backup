﻿window.KGS = window.KGS || {}; //set the scope of this file

Sys.Application.add_load(LoadHandler);

/*
After each load (including postbacks), we must re-execute the javascript that
was previously created by the highcharts framework. Also we must execute the
"Toggle" function because the user may have selected the "table" view.
Otherwise the highchart will be lost after the postback. This has been included
in the javascript "load" event exposed by the ASP.NET framework seen above.
*/
function LoadHandler() {
    
    var zone = $find('radDockZone');

    var docks = new Array(); 
    
    docks = zone.get_docks(); 

    $.each(docks, function (index, dock) {

        dock.set_collapsed(false);

    });

    var scriptDivIds = 
    [
        "#energyConsumptionChartScriptDiv", 
        "#monthlyConsumptionChartScriptDiv", 
        "#SEUChartScriptDiv", 
        ".seuDisplayTypesDiv",
        ".SEUClientSideChartQueryParams"
    ];

    $.each(scriptDivIds, function (index, id) {

        var script = jQuery(id).text();

        eval(script);

    });

    KGS.DrawTable('#energyConsumptionChartContainer', '.energyConsumptionTable');

    //TODO, combine the ToggleTable calls from two into one
    KGS.ToggleTable('.ddlEnergyConsumptionTableToggle');

    KGS.ToggleTable('.ddlSEUToggle');    
}

/*
Function that simply hides all divs, retrieves the chart type (table or line chart)
selected by the user, and displays the appropriate chart type.
*/
KGS.ToggleTable = function (ddlToggleID) {
    
    if (!ddlToggleID) return false;
    
    var divIDs = KGS.GetDivIds(ddlToggleID);

    KGS.HideDivs(divIDs);

    var ddlToggle = jQuery(ddlToggleID);    

    var toggleVal = ddlToggle.val();    

    KGS.ShowDiv(toggleVal, divIDs);

}

//convenience function(s) section
KGS.GetDivIds = function (divID) {

    if (divID === ".ddlEnergyConsumptionTableToggle") {
        return [[".energyConsumptionChartDiv"], [".energyConsumptionTableDiv", ".tableDownloadDivEnergy"]];
    }
    else {
        return [[".SEUChartDiv"], [".SEUTableDiv", ".tableDownloadDivSEU"]];
    }

}

KGS.HideDivs = function (ids) {    
    
    //flatten ids
    var flatIds = $.map(ids, function (id, index) {

        return id;

    });

    $.each(flatIds, function (index, id) { 
        
        jQuery(id).hide();

    });

}

KGS.ShowDiv = function (toggleVal, allIDs) {    
    
    var showIds = allIDs[(toggleVal != "5") ? 0 : 1];
    
    $.each(showIds, function (index, id) {
        
        var div = $(id);

        div.css('display', 'inline-block');        

        if (!(id.indexOf(".tableDownloadDiv") !== -1 || !div || !div[0] || !div[0].style))
            div[0].style.width = '100%';

    });  
}
//convenience function(s) section