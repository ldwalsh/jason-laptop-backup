﻿window.KGS = window.KGS || {}; //set the scope of this file

//Refreshes the items in the display type ddl based on selected analysis type
KGS.RefreshSEUDisplayDDL = function () {

    var displayDDL = $(".ddlSEUToggle") ? $(".ddlSEUToggle")[0] : null;

    var analysisDDL = $(".ddlSEUAnalysisType") ? $(".ddlSEUAnalysisType")[0] : null;

    if (!displayDDL || !analysisDDL || !analysisDDL.value || !baselineDisplayIDs || !goalDisplayIDs) 
        return false;

    var analysisID = analysisDDL.value;

    var displayDict;

    if (goalDisplayTypes && goalDisplayIDs.indexOf(analysisID) !== -1)
    {
        KGS.ResetAndRefresh(displayDDL, goalDisplayTypes, analysisDDL)

        return;
    }

    if (baselineDisplayTypes && baselineDisplayIDs.indexOf(analysisID) !== -1)
    {
        KGS.ResetAndRefresh(displayDDL, baselineDisplayTypes, analysisDDL)

        return;
    }

    KGS.ResetAndRefresh(displayDDL, regularDisplayTypes, analysisDDL)

};

//resets the dropdown with the new items
KGS.ResetDDL = function (ddl, items) {
    
    if (!ddl || !ddl.options || !items) return false;

    ddl.options.length = 0; //remove current options

    //add new options
    $.each(Object.keys(items), function(index, value){
        
        var option = document.createElement("option");

        option.value = items[value];

        option.textContent = value;

        ddl.appendChild(option);
    });

    KGS.ToggleTable('.ddlSEUToggle');

}

//toggles visibility of SEU's byArea ddl based on analysis type
KGS.RefreshByAreaVisibility = function(analysisDDL) {

    var SEUByAreaDDL = $(".seuByArea") ? $(".seuByArea")[0] : null;
    
    if (!byAreaAnalysisTypes || !analysisDDL || !SEUByAreaDDL) return false;

    var style = byAreaAnalysisTypes.indexOf(analysisDDL.value) !== -1 ? "display:inline-block" 
                                                                      : "display:none";

    SEUByAreaDDL.setAttribute("style", style);

}

//Event handler when user clicks on SEU drilldown - 
//triggers AJAX if applicable to retrieve drilldown data
KGS.SEUDrilldownClick = function (e, chart) {

    if (!chart || !e || !e.point) return false;

    chart.showLoading("Retrieving data...");

    var queryParams = KGS.GetSEUDrilldownQueryParams(e, chart);
    
    if (!queryParams) {

        KGS.HandleBadOrNoData(chart);

        return false;
    }

    var success = KGS.GetDrilldownData(e, chart, queryParams);

    if(success === false){
    
        KGS.HandleBadOrNoData(chart);

    }       

};

//Event handler when user clicks on SEU drillup -
//pops the lowest level drilldown query params from
//the query params stack
KGS.SEUDrilluupClick = function (e, chart) {

    if(KGS.SEUQueryParamStack) {

        KGS.SEUQueryParamStack.pop();

        KGS.SEUQueryParamStack.pop();  //todo- remove once we figure out why scripts running 2x on page load
    }
}

//convenience function(s)
KGS.ResetAndRefresh = function(displayDDL, displayDict, analysisDDL) {

    KGS.ResetDDL(displayDDL, displayDict);

    KGS.RefreshByAreaVisibility(analysisDDL);

}                                

//gets the query params for a drilldown click and returns
//object that contains info from dropdowns and point that was clicked
KGS.GetSEUDrilldownQueryParams = function(e, chart){

    if(!KGS.SEUQueryParamStack) return null;

    var params = KGS.SEUQueryParamStack.peek();

    if(!params || !e || !e.point || !chart) return null;

    params.pointName = e.point.name;

    return params;
};

//makes ajax call to get data based onthe query parameters,
//then calls method that updates the chart data
KGS.GetDrilldownData = function(e, chart, queryParams){

    if(!queryParams || !e) return false;

    if(!e.drilldown){  

        jQuery.ajax({ 
        url : "ConsumptionMetricHandler", 
        data : queryParams, 
        contentType : "application/json; charset=utf-8",
        dataType : "json",
        cache : false,
        success : function(result) {

            if(result && result.SerializedQueryParams && KGS.SEUQueryParamStack){
                
                var queryParams = JSON.parse(result.SerializedQueryParams);

                if(queryParams)
                {
                    KGS.SEUQueryParamStack.push(queryParams);

                    KGS.SEUQueryParamStack.push(queryParams); //todo- remove once we figure out why scripts running 2x on page load
                }
            }

            KGS.SetChartDrillDownData(e, result, chart);

        },
        error: function(result) {
            
            KGS.HandleBadOrNoData(chart);

        } 
        });
    }
    else{

        chart.hideLoading();

    }
};

//sets the chart drilldown data with the dataset
KGS.SetChartDrillDownData = function(e, result, chart){

    if(!result || !result.Data || !e || !chart) return false;
    
    data = {
            name: e.point.name,
            data: result.Data
        };

    KGS.AddSeriesAndHideLoading(e, data, chart);
}

KGS.AddSeriesAndHideLoading = function(e, data, chart) {

    if(!e || !data || !chart)
      return false;

    chart.xAxis[0].setCategories(null);

    chart.addSeriesAsDrilldown(e.point, data);

    chart.hideLoading();
};

KGS.HandleBadOrNoData = function(chart){

    alert("Trouble getting data, please refresh page and try again.");

    if(chart){

        chart.hideLoading();

    }
}

//A module representing a stack of query params used by
//the SEU widget. Each object on the stack represents the 
//query params from the Search Criteria panel and also the
//SEU widget DDL. We need to maintain this stack on the client
//side in order to save the "breadcrumbs" of queries when the user
//drills down into the chart data.
KGS.SEUQueryParamStack = (function(){

    var stack = [];

    return{
        push: function(params){

            stack.push(params);

        },
        pop: function(){

            return stack.pop();

        },
        peek: function(){
        
            var params = stack.pop();

            stack.push(params);

            return params;

        }
    };
})();