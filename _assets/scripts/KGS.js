﻿
var K =
{
	_hasInterface:function(interfaceObj, obj){},
	Array:function(type, array){this.type=null;this.push=function(obj){};this.getLast=function(){};this.forEach=function(){};},
	asp:
	{
		webForm:
		{
			AsyncPostbackManager:function(){this.beginRequest=new K.Event(this);this.endRequest=new K.Event(this);this.initRequest=new K.Event(this);this.pageLoaded=new K.Event(this);this.pageLoading=new K.Event(this);},
			doPostback:function(targetElement, eventArgs){},
			setPostbackEventInfo:function(value, clearTime){},
			doPostbackWithInfo:function(eventTarget, eventArgument, eventData){},
			getServerControlID:function(control){}
		}
	},
	contains:function(obj, containsObj){},
	css:{findSheetByFile:function(name, doc){},findSelectorByName:function(name, sheet){},getSelectors:function(sheet){},toDimension:function(size,unit){},parseDisplay:function(bln){}},
	dom:{ALL:null,TagNames:{A:null,Span:null,IFrame:null,Input:null,Option:null,Div:null,Button:null,Img:null,Table:null,THead:null,TBody:null,TD:null,TH:null}},
	ErrorBase:function(err, message, additionalMessage, innerError){},
	Element:function(tag_OR_element, attribs, styles, events)
	{
		this.element=null;
		this.e=null;
		this.style=function(styleObj){};
		this.getEvent=function(){};
		this.appendTo=function(parent){};
		this.hide=function(){};
		this.display=function(){};
	},
	Enum:function(objectEnum)
	{
		this.toNumber = function(enumConstant){};
		this.toValue = function(enumValue){};
	},
	Event:function(eventSource){this.attach=function(func){};this.detach=function(func){};this.fire=function(eventArgs){};},
	forEach:function(obj, func){},
	hasInterface:function(interfaceObj, obj){},
	ifn:function(obj, value){},
	String:function(string)
	{
		this.prepend=function(str){};
		this.append=function(str){};
		this.trim=function(){};
		this.globalReplace=function(from, to){};
		this.toProperCase=function(){};
		this.insert=function(index, str){};
		this.substr=function(start, end){};
		this.fromInterCap=function(){};
	},
	NativeType:{Func:null, Obj:null, Bln:null, Num:null, Str:null, RegExpr:null},
	typeOf:function(obj){},
	Type:{Win:null,Func:null,Str:null,Num:null,Obj:null,Bln:null,Arr:null,Dte:null,Args:null,DOMDocument:null,DOMFragment:null,DOMNodeList:null,DOMTextNode:null,DOMComment:null,DOMUnknown:null,RegExpr:null,HTMLElement:null},
	
	vv:function(value, rules){},	
	vp:function(args, rules){},	
	vo:function(obj, def){}
};

K.T = K.Type;

K.Element.enable = function(element){};
K.Element.disable = function(element){};
K.Element.create = function(tag, parent, attribs, styles, events){};

K.Enum.is = function(obj){};

K.String.format = function(str, strParts){};
K.String.globalReplace = function(str, fromStr, toStr){};