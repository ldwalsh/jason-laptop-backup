﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemAPISubscriberAdministration.aspx.cs" Inherits="CW.Website.SystemAPISubscriberAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/APISubscribers/ViewAPISubscribers.ascx" tagname="ViewAPISubscribers" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/APISubscribers/AddAPISubscriber.ascx" tagname="AddAPISubscriber" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/APISubscribers/AddProduct.ascx" tagname="AddProduct" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/APISubscribers/ViewProducts.ascx" tagname="ViewProducts" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/APISubscribers/AddAPIContainer.ascx" tagname="AddAPIContainer" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/APISubscribers/ViewAPIContainers.ascx" tagname="ViewAPIContainers" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/APISubscribers/AddAPI.ascx" tagname="AddAPI" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/APISubscribers/ViewAPIs.ascx" tagname="ViewAPIs" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">                	                  	
      <script type="text/javascript">
        function checkThrottlingQuotaInput(textBox, minValue, maxValue) {
            var inputValue = textBox.value;
            if (inputValue < minValue) {
                textBox.value = minValue;
            }
            else if (inputValue > maxValue) {
                textBox.value = maxValue;
            }
        }
        function setEnabledBasedOnThrottleQuotaEnableState(checked, rateLimitId, renewalPeriodId, nRateLimitId, nRenewalPeriodId, bandwidthId) {
            // Input textbox
            var obj = $get(rateLimitId);
            if (null != obj) {
                obj.disabled = !checked;
            }

            // Numeric button up/down
            obj = $find(nRateLimitId);
            if (null != obj) {
                obj._bUp.disabled = !checked;
                obj._bDown.disabled = !checked;
            }

            // Input textbox
            obj = $get(renewalPeriodId);
            if (null != obj) {
                obj.disabled = !checked;
            }

            // Numeric button up/down
            obj = $find(nRenewalPeriodId);
            if (null != obj) {
                obj._bUp.disabled = !checked;
                obj._bDown.disabled = !checked;
            }

            // Input textbox
            if (null != bandwidthId) {
                obj = $get(bandwidthId);
                if (null != obj) {
                    obj.disabled = !checked;
                }
            }
        }
      </script>

      <h1>System API Subscriber Administration</h1>

      <div class="richText">The system API subscriber administration area is used to view, add, and edit API Subscribers used for external REST service authentication</div>
                                                                                                                                                                       
      <div class="updateProgressDiv">                                                                                                                                
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>                            
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div> 
       
      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" CausesValidation="false" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Products" />
            <telerik:RadTab Text="Add Product" />
            <telerik:RadTab Text="View API Containers" />
            <telerik:RadTab Text="Add API Container" />
            <telerik:RadTab Text="View APIs" />
            <telerik:RadTab Text="Add API" />
            <telerik:RadTab Text="View API Subscribers" />
            <telerik:RadTab Text="Add API Subscriber" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
            <telerik:RadPageView ID="radPageView1" runat="server"> 
                <CW:ViewProducts ID="ViewProducts" runat="server" />
            </telerik:RadPageView>                                       
            <telerik:RadPageView ID="radPageView2" runat="server"> 
                <CW:AddProduct ID="AddProduct" runat="server" />
            </telerik:RadPageView>                                       
            <telerik:RadPageView ID="radPageView3" runat="server"> 
                <CW:ViewAPIContainers ID="ViewAPIContainers" runat="server" />
            </telerik:RadPageView>                                       
            <telerik:RadPageView ID="radPageView4" runat="server"> 
                <CW:AddAPIContainer ID="AddAPIContainer" runat="server" />
            </telerik:RadPageView>                                       
            <telerik:RadPageView ID="radPageView5" runat="server"> 
                <CW:ViewAPIs ID="ViewAPIs" runat="server" />
            </telerik:RadPageView>                                       
            <telerik:RadPageView ID="radPageView6" runat="server"> 
                <CW:AddAPI ID="AddAPI" runat="server" />
            </telerik:RadPageView>                                       
            <telerik:RadPageView ID="radPageView7" runat="server">
                <CW:ViewAPISubscribers ID="ViewAPISubscribers" runat="server" />                                                                    
            </telerik:RadPageView>
            <telerik:RadPageView ID="radPageView8" runat="server"> 
                <CW:AddAPISubscriber ID="AddAPISubscriber" runat="server" />
            </telerik:RadPageView>                                       
        </telerik:RadMultiPage>                     
      </div>
       
</asp:Content>