﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="ProviderPointTypeAdministration.aspx.cs" Inherits="CW.Website.ProviderPointTypeAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/PointTypes/nonkgs/ViewPointTypes.ascx" tagname="ViewPointTypes" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <h1>Provider Point Type Administration</h1>

  <div class="richText">The provider point type administration area is used to view point types.</div>

  <div class="updateProgressDiv">
    <asp:UpdateProgress ID="updateProgressTop" runat="server">
      <ProgressTemplate>
      <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
      </ProgressTemplate>
    </asp:UpdateProgress>
  </div>

  <div class="administrationControls">
    <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage">
      <Tabs>
        <telerik:RadTab Text="View Point Types" />
      </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

      <telerik:RadPageView ID="RadPageView1" runat="server">
        <CW:ViewPointTypes ID="ViewPointTypes" runat="server" />
      </telerik:RadPageView>

    </telerik:RadMultiPage>
  </div>

</asp:Content>