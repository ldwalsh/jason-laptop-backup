﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DynamicRadContent.aspx.cs" Inherits="CW.Website.DynamicRadContent" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="/_assets/scripts/jquery-1.2.6.min.js"></script>
		<script type="text/javascript" src="/_assets/scripts/jquery-color.min.js"></script>
    
</head>
<body>
    <form id="form1" runat="server">

        <script type="text/javascript">
            $(document).ready(function () {

                var dock = $find("<%= radDockBuildingMap.ClientID%>");
        var content = dock.get_contentContainer();
        var text = $telerik.isIE ? content.childNodes[0].innerHTML : content.childNodes[1].innerHTML;
        alert(text);
    });
        </script>

        <telerik:RadScriptManager ID="RadScriptManager" EnableScriptGlobalization="true" AsyncPostBackTimeout="1200" runat="server" />
    <div>
    <telerik:RadDockLayout ID="radDockLayout1" runat="server">

    <telerik:RadDockZone ID="radDockZone1" BorderStyle="None" runat="server" FitDocks="false" Orientation="Horizontal">
                        
      <telerik:RadDock ID="radDockBuildingMap" DefaultCommands="ExpandCollapse" EnableAnimation="true" Title="Building Map" CssClass="radDock" runat="server" EnableRoundedCorners="true" DockMode="Docked" Resizable="false" Width="438px">
        <ContentTemplate>
          <asp:UpdatePanel ID="udpBuildingMap" runat="server">
            <ContentTemplate>
		      <div id="injectHere"></div>
            </ContentTemplate>
          </asp:UpdatePanel>
	    </ContentTemplate>
      </telerik:RadDock>

    </telerik:RadDockZone>

  </telerik:RadDockLayout>
    </div>
    </form>
</body>
</html>
