﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using CW.Business;
using CW.Data;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Data.Models.FAQ;
using CW.Utility;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class SystemFAQAdministration : SitePage
    {
        #region Properties

            private FAQSection mFAQSection;
            const string addFAQSectionSuccess = " faq section addition was successful.";
            const string addFAQSectionFailed = "Adding faq section failed. Please contact an administrator.";
            //const string faqSectionExists = "A faq section with that title already exists.";
            const string updateFAQSectionSuccessful = "FAQ section update was successful.";
            const string updateFAQSectionFailed = "FAQ section update failed. Please contact an administrator.";
            const string deleteFAQSectionSuccessful = "FAQ section deletion was successful.";
            const string deleteFAQSectionFailed = "FAQ section deletion failed. Please contact an administrator.";
            const string faqExists = "Cannot delete faq section because one or more faq's are associated. Please disassociate from section first.";    
            const string initialFAQSectionSortDirection = "ASC";
            const string initialFAQSectionSortExpression = "Title";

            private CW.Data.FAQ mFAQ;
            const string addFAQSuccess = "FAQ addition was successful.";
            const string addFAQFailed = "Adding faq failed. Please contact an administrator.";
            const string updateFAQSuccessful = "FAQ update was successful.";
            const string updateFAQFailed = "FAQ update failed. Please contact an administrator.";
            const string deleteFAQSuccessful = "FAQ deletion was successful.";
            const string deleteFAQFailed = "FAQ deletion failed. Please contact an administrator.";
            const string initialFAQSortDirection = "ASC";
            const string initialFAQSortExpression = "SortOrder";

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewFAQSectionSortDirection
            {
                get { return ViewState["FAQSectionSortDirection"] as string ?? string.Empty; }
                set { ViewState["FAQSectionSortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewFAQSectionSortExpression
            {
                get { return ViewState["FAQSectionSortExpression"] as string ?? string.Empty; }
                set { ViewState["FAQSectionSortExpression"] = value; }
            }

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewFAQSortDirection
            {
                get { return ViewState["FAQSortDirection"] as string ?? string.Empty; }
                set { ViewState["FAQSortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewFAQSortExpression
            {
                get { return ViewState["FAQSortExpression"] as string ?? string.Empty; }
                set { ViewState["FAQSortExpression"] = value; }
            }
        
        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //hide labels
                lblFAQErrors.Visible = false;
                lblAddFAQError.Visible = false;
                lblEditFAQError.Visible = false;
                lblFAQSectionErrors.Visible = false;
                lblAddFAQSectionError.Visible = false;
                lblEditFAQSectionError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindFAQSections();
                    
                    //Bound on tab change
                    //BindFAQs();

                    ViewState.Clear();
                    GridViewFAQSectionSortDirection = initialFAQSectionSortDirection;
                    GridViewFAQSectionSortExpression = initialFAQSectionSortExpression;
                    GridViewFAQSortDirection = initialFAQSortDirection;
                    GridViewFAQSortExpression = initialFAQSortExpression;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetFAQSectionIntoEditForm(FAQSection faqSection)
            {
                //ID
                hdnEditFAQSectionSectionID.Value = Convert.ToString(faqSection.SectionID);
                //Title
                txtEditFAQSectionTitle.Text = faqSection.Title;
                //SortOrder
                txtEditFAQSectionSortOrder.Text = Convert.ToString(faqSection.SortOrder);
            }

            protected void SetFAQIntoEditForm(CW.Data.FAQ faq)
            {
                //ID
                hdnEditFAQFAQID.Value = Convert.ToString(faq.FAQID);
                //Question
                txtEditFAQQuestion.Value = String.IsNullOrEmpty(faq.Question) ? null : faq.Question;
                //Answer
                txtEditFAQAnswer.Value = String.IsNullOrEmpty(faq.Answer) ? null : faq.Answer;
                //SectionID
                ddlEditFAQSection.SelectedValue = Convert.ToString(faq.SectionID);
                //SortOrder
                txtEditFAQSortOrder.Text = Convert.ToString(faq.SortOrder);
            }

        #endregion

        #region Load and Bind Fields

            private void BindFAQSections(DropDownList ddl)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "Title";
                ddl.DataValueField = "SectionID";
                ddl.DataSource = DataMgr.FAQDataMapper.GetAllFAQSections();
                ddl.DataBind();
            }

        #endregion

        #region Load FAQ Section

            protected void LoadAddFormIntoFAQSection(FAQSection faqSection)
            {
                //Title
                faqSection.Title = txtAddFAQSectionTitle.Text;
                //Sort Order
                faqSection.SortOrder = Convert.ToInt32(txtAddFAQSectionSortOrder.Text);

                faqSection.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoFAQSection(FAQSection faqSection)
            {
                //ID
                faqSection.SectionID = Convert.ToInt32(hdnEditFAQSectionSectionID.Value);
                //Title
                faqSection.Title = txtEditFAQSectionTitle.Text;
                //Sort Order
                faqSection.SortOrder = Convert.ToInt32(txtEditFAQSectionSortOrder.Text);

                faqSection.DateModified = DateTime.UtcNow;
            }

        #endregion

        #region Load FAQ

            protected void LoadAddFormIntoFAQ(CW.Data.FAQ faq)
            {
                //Question
                faq.Question = String.IsNullOrEmpty(txtAddFAQQuestion.Value) ? null : txtAddFAQQuestion.Value;
                //Answer
                faq.Answer = String.IsNullOrEmpty(txtAddFAQAnswer.Value) ? null : txtAddFAQAnswer.Value;
                //SectionID
                faq.SectionID = Convert.ToInt32(ddlAddFAQSection.SelectedValue);
                //Sort Order
                faq.SortOrder = Convert.ToInt32(txtAddFAQSortOrder.Text);

                faq.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoFAQ(CW.Data.FAQ faq)
            {
                //ID
                faq.FAQID = Convert.ToInt32(hdnEditFAQFAQID.Value);
                //Question
                faq.Question = String.IsNullOrEmpty(txtEditFAQQuestion.Value) ? null : txtEditFAQQuestion.Value;
                //Answer
                faq.Answer = String.IsNullOrEmpty(txtEditFAQAnswer.Value) ? null : txtEditFAQAnswer.Value;
                //SectionID
                faq.SectionID = Convert.ToInt32(ddlEditFAQSection.SelectedValue);
                //Sort Order
                faq.SortOrder = Convert.ToInt32(txtEditFAQSortOrder.Text);

                faq.DateModified = DateTime.UtcNow;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add faq section Button on click.
            /// </summary>
            protected void addFAQSectionButton_Click(object sender, EventArgs e)
            {
                mFAQSection = new FAQSection();

                //load the form into the faq section
                LoadAddFormIntoFAQSection(mFAQSection);

                //increment sort order for faq sections if placed in position that already exists
                if (DataMgr.FAQDataMapper.DoesSectionSortOrderExist(null, mFAQSection.SortOrder))
                {
                    foreach (FAQSection faqSection in DataMgr.FAQDataMapper.GetAllFAQSectionsWithSortOrderEqualOrGreaterThan(mFAQSection.SortOrder))
                    {
                        faqSection.SortOrder++;
                        DataMgr.FAQDataMapper.UpdateFAQSection(faqSection);
                    }
                }

                try
                {
                    //insert new faq section
                    DataMgr.FAQDataMapper.InsertFAQSection(mFAQSection);

                    LabelHelper.SetLabelMessage(lblAddFAQSectionError, mFAQSection.Title + addFAQSectionSuccess, lnkAddFAQSectionSetFocus); 
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding faq section.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddFAQSectionError, addFAQSectionFailed, lnkAddFAQSectionSetFocus); 
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding faq section.", ex);
                    LabelHelper.SetLabelMessage(lblAddFAQSectionError, addFAQSectionFailed, lnkAddFAQSectionSetFocus); 
                }
            }

            /// <summary>
            /// Update faq section Button on click. Updates faq section data.
            /// </summary>
            protected void updateFAQSectionButton_Click(object sender, EventArgs e)
            {
                mFAQSection = new FAQSection();

                //load the form into the faq section
                LoadEditFormIntoFAQSection(mFAQSection);

                //increment sort order for faq sections if placed in position that already exists
                if (DataMgr.FAQDataMapper.DoesSectionSortOrderExist(mFAQSection.SectionID, mFAQSection.SortOrder))
                {
                    foreach (FAQSection faqSection in DataMgr.FAQDataMapper.GetAllFAQSectionsWithSortOrderEqualOrGreaterThan(mFAQSection.SortOrder))
                    {
                        faqSection.SortOrder++;
                        DataMgr.FAQDataMapper.UpdateFAQSection(faqSection);
                    }
                }

                //try to update the faq section            
                try
                {
                    DataMgr.FAQDataMapper.UpdateFAQSection(mFAQSection);

                    LabelHelper.SetLabelMessage(lblEditFAQSectionError, updateFAQSectionSuccessful, lnkFAQSectionSetFocusView);

                    //Bind faq sections again
                    BindFAQSections();
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditFAQSectionError, updateFAQSectionFailed, lnkFAQSectionSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating faq section.", ex);
                }
            }

            /// <summary>
            /// Add faq Button on click.
            /// </summary>
            protected void addFAQButton_Click(object sender, EventArgs e)
            {
                mFAQ = new CW.Data.FAQ();

                //load the form into the faq
                LoadAddFormIntoFAQ(mFAQ);

                //increment sort order for faqs in that section if placed in position that already exists
                if (DataMgr.FAQDataMapper.DoesFAQSortOrderExist(null, mFAQ.SectionID, mFAQ.SortOrder))
                {
                    foreach (CW.Data.FAQ faq in DataMgr.FAQDataMapper.GetAllFAQsWithSortOrderEqualOrGreaterThan(mFAQ.SectionID, mFAQSection.SortOrder))
                    {
                        faq.SortOrder++;
                        DataMgr.FAQDataMapper.UpdateFAQ(faq);
                    }
                }

                try
                {
                    //insert new faq
                    DataMgr.FAQDataMapper.InsertFAQ(mFAQ);

                    LabelHelper.SetLabelMessage(lblAddFAQError, addFAQSuccess, lnkAddFAQSetFocus);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding faq.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddFAQError, addFAQFailed, lnkAddFAQSetFocus);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding faq.", ex);
                    LabelHelper.SetLabelMessage(lblAddFAQError, addFAQFailed, lnkAddFAQSetFocus);
                }
            }

            /// <summary>
            /// Update faq Button on click. Updates faq data.
            /// </summary>
            protected void updateFAQButton_Click(object sender, EventArgs e)
            {
                mFAQ = new CW.Data.FAQ();

                //load the form into the faq
                LoadEditFormIntoFAQ(mFAQ);

                //increment sort order for faqs in that section if placed in position that already exists
                if (DataMgr.FAQDataMapper.DoesFAQSortOrderExist(mFAQ.FAQID, mFAQ.SectionID, mFAQ.SortOrder))
                {
                    foreach (CW.Data.FAQ faq in DataMgr.FAQDataMapper.GetAllFAQsWithSortOrderEqualOrGreaterThan(mFAQ.SectionID, mFAQSection.SortOrder))
                    {
                        faq.SortOrder++;
                        DataMgr.FAQDataMapper.UpdateFAQ(faq);
                    }
                }

                //try to update the faq            
                try
                {
                    DataMgr.FAQDataMapper.UpdateFAQ(mFAQ);

                    LabelHelper.SetLabelMessage(lblEditFAQError, updateFAQSuccessful, lnkFAQSetFocusView);

                    //Bind faq again
                    BindFAQs();
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditFAQError, updateFAQFailed, lnkFAQSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating faq.", ex);
                }
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind appropriate grid after tab changed back from add tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind faq sections to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindFAQSections();
                }
                //else if active tab index is 2, bind faqs to grid
                //faq grid and edit
                else if (radTabStrip.MultiPage.SelectedIndex == 2)
                {
                    BindFAQs();

                    BindFAQSections(ddlEditFAQSection);
                }
                //else if add faq
                else if (radTabStrip.MultiPage.SelectedIndex == 3)
                {                    
                    BindFAQSections(ddlAddFAQSection);
                }
            }

        #endregion

        #region Grid Events

            protected void gridFAQSections_OnDataBound(object sender, EventArgs e)
            {                         
            }

            protected void gridFAQSections_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridFAQSections_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditFAQSection.Visible = false;
                dtvFAQSection.Visible = true;
                
                int sectionID = Convert.ToInt32(gridFAQSections.DataKeys[gridFAQSections.SelectedIndex].Values["SectionID"]);

                //set data source
                dtvFAQSection.DataSource = DataMgr.FAQDataMapper.GetFAQSectionByIDAsEnumerable(sectionID);
                //bind faq section to details view
                dtvFAQSection.DataBind();
            }

            protected void gridFAQSections_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvFAQSection.Visible = false;
                pnlEditFAQSection.Visible = true;

                int sectionID = Convert.ToInt32(gridFAQSections.DataKeys[e.NewEditIndex].Values["SectionID"]);

                mFAQSection = DataMgr.FAQDataMapper.GetFAQSectionByID(sectionID);

                //Set faq section data
                SetFAQSectionIntoEditForm(mFAQSection);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridFAQSections_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows sectionid
                int sectionID = Convert.ToInt32(gridFAQSections.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a faq section if no faqs have been associated.                    

                    //check if faq is associated to that building class
                    bool hasFAQs = DataMgr.FAQDataMapper.IsFAQAssociatedWithFAQSection(sectionID);

                    if (hasFAQs)
                    {
                        lblFAQSectionErrors.Text = faqExists;
                    }
                    else
                    {
                        //delete faq section
                        DataMgr.FAQDataMapper.DeleteFAQSection(sectionID);

                        lblFAQSectionErrors.Text = deleteFAQSectionSuccessful;
                    }  
                }
                catch (Exception ex)
                {
                    lblFAQSectionErrors.Text = deleteFAQSectionFailed;

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting faq section.", ex);
                }

                lblFAQSectionErrors.Visible = true;
                lnkFAQSectionSetFocusView.Focus();

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryFAQSections());
                gridFAQSections.PageIndex = gridFAQSections.PageIndex;
                gridFAQSections.DataSource = FAQSectionSortDataTable(dataTable as DataTable, true);
                gridFAQSections.DataBind();

                SetGridFAQSectionsCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditFAQSection.Visible = false;
            }

            protected void gridFAQSections_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryFAQSections());

                //maintain current sort direction and expresstion on paging
                gridFAQSections.DataSource = FAQSectionSortDataTable(dataTable, true);
                gridFAQSections.PageIndex = e.NewPageIndex;
                gridFAQSections.DataBind();
            }

            protected void gridFAQSections_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridFAQSections.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryFAQSections());

                GridViewFAQSectionSortExpression = e.SortExpression;

                gridFAQSections.DataSource = FAQSectionSortDataTable(dataTable, false);
                gridFAQSections.DataBind();
            }

            protected void gridFAQs_OnDataBound(object sender, EventArgs e)
            {
            }

            protected void gridFAQs_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridFAQs_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditFAQ.Visible = false;
                dtvFAQ.Visible = true;
                
                int faqID = Convert.ToInt32(gridFAQs.DataKeys[gridFAQs.SelectedIndex].Values["FAQID"]);

                //set data source
                dtvFAQ.DataSource = DataMgr.FAQDataMapper.GetFullFAQByIDAsEnumerable(faqID);
                //bind faq to details view
                dtvFAQ.DataBind();
            }

            protected void gridFAQs_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvFAQ.Visible = false;
                pnlEditFAQ.Visible = true;

                int faqID = Convert.ToInt32(gridFAQs.DataKeys[e.NewEditIndex].Values["FAQID"]);

                mFAQ = DataMgr.FAQDataMapper.GetFAQByID(faqID);

                //Set faq data
                SetFAQIntoEditForm(mFAQ);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridFAQs_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows faqid
                int faqID = Convert.ToInt32(gridFAQs.DataKeys[e.RowIndex].Value);

                try
                {
                    //delete faq
                    DataMgr.FAQDataMapper.DeleteFAQ(faqID);

                    lblFAQErrors.Text = deleteFAQSuccessful;
                }
                catch (Exception ex)
                {
                    lblFAQErrors.Text = deleteFAQFailed;

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting faq.", ex);
                }

                lblFAQErrors.Visible = true;
                lnkFAQSetFocusView.Focus();

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryFAQs());
                gridFAQs.PageIndex = gridFAQs.PageIndex;
                gridFAQs.DataSource = FAQSortDataTable(dataTable as DataTable, true);
                gridFAQs.DataBind();

                SetGridFAQsCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditFAQ.Visible = false;
            }

            protected void gridFAQs_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryFAQs());

                //maintain current sort direction and expresstion on paging
                gridFAQs.DataSource = FAQSortDataTable(dataTable, true);
                gridFAQs.PageIndex = e.NewPageIndex;
                gridFAQs.DataBind();
            }

            protected void gridFAQs_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridFAQs.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryFAQs());

                GridViewFAQSortExpression = e.SortExpression;

                gridFAQs.DataSource = FAQSortDataTable(dataTable, false);
                gridFAQs.DataBind();
            }
        #endregion

        #region Helper Methods

            private IEnumerable<FAQSection> QueryFAQSections()
            {
                try
                {
                    //get all faq sections
                    return DataMgr.FAQDataMapper.GetAllFAQSections();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving faq sections.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving faq sections.", ex);
                    return null;
                }
            }

            private IEnumerable<GetFAQData> QueryFAQs()
            {
                try
                {
                    //get all faqs
                    return DataMgr.FAQDataMapper.GetAllFullFAQs();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving faqs.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving faqs.", ex);
                    return null;
                }
            }

            private void BindFAQSections()
            {
                //query faq sections
                IEnumerable<FAQSection> sections = QueryFAQSections();

                int count = sections.Count();

                gridFAQSections.DataSource = sections;

                // bind grid
                gridFAQSections.DataBind();

                SetGridFAQSectionsCountLabel(count);
            }

            private void BindFAQs()
            {
                //query faqs
                IEnumerable<GetFAQData> faqs = QueryFAQs();

                int count = faqs.Count();

                gridFAQs.DataSource = faqs;

                // bind grid
                gridFAQs.DataBind();

                SetGridFAQsCountLabel(count);
            }

            private void SetGridFAQSectionsCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblFAQSectionResults.Text = String.Format("{0} faq section found.", count);
                }
                else if (count > 1)
                {
                    lblFAQSectionResults.Text = String.Format("{0} faq sections found.", count);
                }
                else
                {
                    lblFAQSectionResults.Text = "No faq sections found.";
                }
            }

            private void SetGridFAQsCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblFAQResults.Text = String.Format("{0} faq found.", count);
                }
                else if (count > 1)
                {
                    lblFAQResults.Text = String.Format("{0} faqs found.", count);
                }
                else
                {
                    lblFAQResults.Text = "No faqs found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetFAQSectionSortDirection()
            {
                //switch sorting directions
                switch (GridViewFAQSectionSortDirection)
                {
                    case "ASC":
                        GridViewFAQSectionSortDirection = "DESC";
                        ViewState["FAQSectionSortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewFAQSectionSortDirection = "ASC";
                        ViewState["FAQSectionSortDirection"] = "ASC";
                        break;
                }
                return GridViewFAQSectionSortDirection;
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetFAQSortDirection()
            {
                //switch sorting directions
                switch (GridViewFAQSortDirection)
                {
                    case "ASC":
                        GridViewFAQSortDirection = "DESC";
                        ViewState["FAQSortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewFAQSortDirection = "ASC";
                        ViewState["FAQSortDirection"] = "ASC";
                        break;
                }
                return GridViewFAQSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView FAQSectionSortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewFAQSectionSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewFAQSectionSortExpression, GridViewFAQSectionSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewFAQSectionSortExpression, GetFAQSectionSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView FAQSortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewFAQSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewFAQSortExpression, GridViewFAQSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewFAQSortExpression, GetFAQSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
     
        #endregion
    }
}

