﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Help.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="FAQ.aspx.cs" Inherits="CW.Website.FAQ" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                   	                  	        	
        	<h1>FAQ's</h1>                        
                <div class="richText">Faq's section.</div>   
                <br />
                <div class="faq">

                <extensions:GroupingRepeater ID="rptFAQs" runat="server">
                <GroupTemplate>
                     <div class="groupRepeaterHeader">
                     <h2><%# Eval("SectionTitle") %></h2>
                     </div>
                </GroupTemplate>
                <ItemTemplate>
                    <div class="groupRepeaterItem">
                    <h3><%# Eval("Question") %></h3>
                    <asp:HyperLink ID="lnk1" runat="server" CssClass="toggle"><asp:Label ID="lbl1" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                    <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender1" runat="Server"
                                TargetControlID="pnl1"
                                CollapsedSize="0"
                                Collapsed="true" 
                                ExpandControlID="lnk1"
                                CollapseControlID="lnk1"
                                AutoCollapse="false"
                                AutoExpand="false"
                                ScrollContents="false"
                                ExpandDirection="Vertical"
                                TextLabelID="lbl1"
                                CollapsedText="show answer"
                                ExpandedText="hide answer" 
                                />
                     <asp:Panel ID="pnl1" CssClass="richText" runat="server">
                        <%# Eval("Answer") %>
                     </asp:Panel>
                     </div>
                </ItemTemplate>
               </extensions:GroupingRepeater>
                                
               </div>                                 

</asp:Content>                