﻿<%@ Page Language="C#" EnableViewState="true" EnableEventValidation="false" AutoEventWireup="false" CodeBehind="BureauReport.aspx.cs" Inherits="CW.Website.BureauReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title id="title" runat="server">$product</title>   
    <base id="baseTag" runat="server" />
    <style type="text/css">
		    @import url("/_assets/styles/themes/bureau.css");
	</style>
</head>
<body id="body" runat="server">
    <form class="" id="form1" runat="server">
        
        <telerik:RadScriptManager ID="ScriptManager" EnableScriptCombine="true" EnableCdn="true" EnableScriptGlobalization="true" AsyncPostBackTimeout="600" runat="server" />
           
        <asp:UpdatePanel ID="updatePanel" ValidateRequestMode="Disabled" runat="server" UpdateMode="Conditional">
        <ContentTemplate>     

        <input type="hidden" runat="server" id="hdn_container" />

        <asp:Panel ID="pnlError" runat="server" Visible="false">
            <asp:Label ID="lblError" CssClass="errorMessage"  runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlReport" runat="server">
        <div class="reportHeader">
            <div class="headerLeft">
                <img id="headerImage" src="" alt="magnify" runat="server" />
            </div>
            <div class="headerMiddle">
                <div id="headerTitle" class="headerTitle" runat="server"></div>
                <div class="headerSecondaryTitle">Performance Report</div>
                <div id="headerBuilding" class="headerBuilding" runat="server"></div>
            </div>            
            <div id="divHeaderRight" class="headerRight" runat="server">             
                <div class="container">   
                    <div class="labelMediumBold">For the period of:</div>                
                    <asp:Literal ID="litHeaderPeriod" runat="server" ></asp:Literal>                   
                </div>
            </div>
        </div>
        <hr />
        <div class="section section1">
            <div class="section1Left">
                <div class="containerWithHeader"> 
                        <div id="containerHeaderOuter">
                        <div id="containerHeaderMiddle">
                        <div id="containerHeaderInner">
                            Avoidable Energy Cost
                        </div>
                        </div>
                        </div>
                    <br />
                    <asp:Literal ID="litAvoidableEnergyTotalValue"  runat="server"></asp:Literal>
                    <div class="labelLarge">Total This Period</div>
                    <br />
                    <asp:Literal ID="litAvoidableEnergyChangeValue" runat="server"></asp:Literal>
                    <div id="divAvoidableEnergyChangeString" class="labelLarge" runat="server"></div>
                    <br />
                </div>
            </div>
            <div class="section1Right richText">
                <asp:ImageButton ID="btnIntroduction" CssClass="editCircleGrey editCircleGreyMarginTopSmall" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="introductionButton_Click" runat="server" />
                <asp:Literal ID="litIntroduction" runat="server"></asp:Literal>
                <telerik:RadEditor ID="editorIntroduction" 
                                    runat="server"                                                                                                                                                
                                    CssClass="editorNewLine"                                                        
                                    Height="380px" 
                                    Width="650px"
                                    MaxHtmlLength="5000"
                                    NewLineMode="Div"
                                    ToolsWidth="652px"                                                                                                                                       
                                    ToolbarMode="ShowOnFocus"        
                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                                    Visible="false"
                                    EnableResize="false"
                                    > 
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                    </CssFiles>                                                                                                                             
                        </telerik:RadEditor>  
            </div>
        </div>
        <div class="section section2">
            <div class="section2Left">
                <div class="containerWithHeader"> 
                    <div class="containerSingleLineHeader">Period Trend Summary</div>
                    <div>
                        <img class="priorityIcon" src="_assets/styles/themes/customReportImages/energy.png" alt="energy" />
                        <img id="imgEnergyArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                        <div class="trendContent">
                            <span class="labelLargeBoldGray">Energy -</span><br />
                            <asp:Literal ID="litEnergyTrendSummary" runat="server"></asp:Literal>     
                        </div>
                    </div>
                    <hr class="containerSpacerGray" />
                    <div>
                        <img class="priorityIcon" src="_assets/styles/themes/customReportImages/maintenance.png" alt="maintenance" />
                        <img id="imgMaintenanceArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                        <div class="trendContent">
                            <span class="labelLargeBoldGray">Maintenance -</span><br />
                            <asp:Literal ID="litMaintenanceTrendSummary" runat="server"></asp:Literal>          
                        </div>
                    </div>                    
                    <hr class="containerSpacerGray" />
                    <div>
                        <img class="priorityIcon" src="_assets/styles/themes/customReportImages/comfort.png" alt="comfort" />
                        <img id="imgComfortArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                        <div class="trendContent">
                            <span class="labelLargeBoldGray">Comfort -</span><br />
                            <asp:Literal ID="litComfortTrendSummary" runat="server"></asp:Literal>  
                        </div>                        
                    </div>
                </div>
            </div>
            <div class="section2Right">
                <div class="containerWithHeader"> 
                    <div class="containerSingleLineHeader">Facility At A Glance</div>
                    <br />
                    <div class="buildingInfoFormat">
                        <label class="labelBoldColored">Customer:</label><br />
                        <span id="clientName" runat="server"></span>
                    </div>
                    <div class="buildingInfoFormat">
                        <label class="labelBoldColored">Building Name:</label><br />
                        <span id="buildingName" runat="server"></span>
                    </div>
                    <div class="buildingInfoFormat">
                        <label class="labelBoldColored">Location:</label><br />
                        <span id="buildingLocation" runat="server"></span>
                    </div>
                    <div class="buildingInfoFormat">
                        <label class="labelBoldColored">Building Type:</label><br />
                        <span id="buildingType" runat="server"></span>
                    </div>
                    <div class="buildingInfoFormat">
                        <label class="labelBoldColored">Year Built:</label><br />
                        <span id="buildingYear" runat="server"></span>
                    </div>
                    <div class="buildingInfoFormat">
                        <label class="labelBoldColored">Number of Buildings:</label><br />
                        <span id="buildingCount" runat="server"></span>
                    </div>
                    <div class="buildingInfoFormat">
                        <label class="labelBoldColored">Total Square Footage:</label><br />
                        <span id="buildingSqft" runat="server"></span>
                    </div>
                    <div class="containerSpacerColored">Prepared By:</div>
                    <br />
                    <div class="preparedByFormat"><strong id="preparedBy" runat="server"></strong></div>
                    <div id="userAddress" class="preparedByFormat" runat="server"></div>
                    <div id="userPhone" class="preparedByFormat" runat="server"></div>
                    <div id="userEmail" class="preparedByFormat" runat="server"></div>
                </div>
            </div>
        </div>
        <div class="bottomPage">
            <asp:Literal ID="litTagline" runat="server"></asp:Literal>
            <img id="imgBottomPage" alt="logo" src="" runat="server" />
        </div>

        <div style="page-break-before: always"></div>

        <div class="newPage">
            <h2 id="newPageTitle2" runat="server"></h2>
            <div id="newPageBuilding2" class="newPageBuilding" runat="server"></div>
            <img id="imgNewPageBuilding2" src="" alt="magnify" runat="server" />
        </div>
        <hr />
        <div class="section section3">
            <h2>Top 5 Issues</h2>

            <div id="gridTbl" class="gridEnergy"> 
                <div class="tblPreHeader">
                    <img src="_assets/styles/themes/customReportImages/energy-white.png" alt="energy" />
                    <span>Energy</span>
                    <asp:ImageButton ID="btnTopEnergy" CssClass="editCircleWhite" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="energyGridEditButton_Click" runat="server" />
                </div>
                <asp:GridView 
                    ID="gridEnergy"   
                    EnableViewState="true"           
                    runat="server"                                                                                                                                 
                    AutoGenerateColumns="false"                                              
                    HeaderStyle-CssClass="tblTitle" 
                    RowStyle-CssClass="tblCol1"
                    AlternatingRowStyle-CssClass="tblCol2"   
                    ShowHeaderWhenEmpty="true"
                    EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                 
                    >                         
                    <EmptyDataTemplate>
                        No Issues
                    </EmptyDataTemplate>              
                    <Columns>                        
                         <asp:TemplateField HeaderText="Building" ItemStyle-CssClass="buildingColumnWidth">                            
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("BuildingName"), 32)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="74px" Visible="false" Text='<%# StringHelper.TrimText(Eval("BuildingName"), 32)%>' MaxLength="32"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Equipment" ItemStyle-CssClass="equipmentColumnWidth">
                            <ItemTemplate>                                
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("EquipmentName"), 42)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="86px" Visible="false" Text='<%# StringHelper.TrimText(Eval("EquipmentName"), 42)%>' MaxLength="42"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Notes" ItemStyle-CssClass="notesColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 142)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="440px" Visible="false" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 142)%>' MaxLength="142"></asp:TextBox> 
                            </ItemTemplate>                            
                        </asp:TemplateField>                                                          
                        <asp:TemplateField HeaderText="Cost" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Eval("TotalCostSavings") %>'></asp:Label>
                                <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavings") %>' MaxLength="10"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Occurrences" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Eval("Occurrences") %>'></asp:Label>
                                <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences") %>' MaxLength="4"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>        
                </asp:GridView>
            </div>

            <div id="gridTbl" class="gridMaintenance">
                <div class="tblPreHeader">
                    <img src="_assets/styles/themes/customReportImages/maintenance-white.png" alt="maintenance" />
                    <span>Maintenance</span>
                    <asp:ImageButton ID="btnTopMaintenance" CssClass="editCircleWhite" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="maintenanceGridEditButton_Click" runat="server" />
                </div>
                <asp:GridView 
                    ID="gridMaintenance"   
                    EnableViewState="true"           
                    runat="server"                                                                                                             
                    AutoGenerateColumns="false"                                              
                    HeaderStyle-CssClass="tblTitle" 
                    RowStyle-CssClass="tblCol1"
                    AlternatingRowStyle-CssClass="tblCol2"        
                    ShowHeaderWhenEmpty="true"
                    EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                                      
                    > 
                    <EmptyDataTemplate>
                        No Issues
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="Building" ItemStyle-CssClass="buildingColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("BuildingName"), 32)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="74px" Visible="false" Text='<%# StringHelper.TrimText(Eval("BuildingName"), 32)%>' MaxLength="32"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Equipment" ItemStyle-CssClass="equipmentColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("EquipmentName"), 42)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="86px" Visible="false" Text='<%# StringHelper.TrimText(Eval("EquipmentName"), 42)%>' MaxLength="42"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField>                         
                        <asp:TemplateField HeaderText="Notes" ItemStyle-CssClass="notesColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 142)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="440px" Visible="false" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 142)%>' MaxLength="1426"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField>                                                          
                        <asp:TemplateField HeaderText="Priority" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Math.Round(Convert.ToDecimal(Eval("AverageMaintenancePriority")), 1)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Math.Round(Convert.ToDecimal(Eval("AverageMaintenancePriority")), 1)%>' MaxLength="4"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Occurrences" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Eval("Occurrences") %>'></asp:Label>
                                <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences") %>' MaxLength="4"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>        
                </asp:GridView>
            </div>
   
            <div id="gridTbl" class="gridComfort">
                <div class="tblPreHeader">
                    <img src="_assets/styles/themes/customReportImages/comfort-white.png" alt="comfort" />
                    <span>Comfort</span>
                    <asp:ImageButton ID="btnTopComfort" CssClass="editCircleWhite" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="comfortGridEditButton_Click" runat="server" />
                </div>
                <asp:GridView 
                    ID="gridComfort"   
                    EnableViewState="true"           
                    runat="server"                                                                                                             
                    AutoGenerateColumns="false"                                              
                    HeaderStyle-CssClass="tblTitle" 
                    RowStyle-CssClass="tblCol1"
                    AlternatingRowStyle-CssClass="tblCol2"      
                    ShowHeaderWhenEmpty="true"
                    EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                   
                    > 
                    <EmptyDataTemplate>
                        No Issues
                    </EmptyDataTemplate>
                    <Columns>
                         <asp:TemplateField HeaderText="Building" ItemStyle-CssClass="buildingColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("BuildingName"), 32)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="74px" Visible="false" Text='<%# StringHelper.TrimText(Eval("BuildingName"), 32)%>' MaxLength="32"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Equipment" ItemStyle-CssClass="equipmentColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("EquipmentName"), 42)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="86px" Visible="false" Text='<%# StringHelper.TrimText(Eval("EquipmentName"), 42)%>' MaxLength="42"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField>  
                        <asp:TemplateField HeaderText="Notes" ItemStyle-CssClass="notesColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 142)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="440px" Visible="false" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 142)%>' MaxLength="142"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField>                                             
                        <asp:TemplateField HeaderText="Priority" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Math.Round(Convert.ToDecimal(Eval("AverageComfortPriority")), 1)%>'></asp:Label>
                                <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Math.Round(Convert.ToDecimal(Eval("AverageComfortPriority")), 1)%>' MaxLength="4"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="Occurrences" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Eval("Occurrences") %>'></asp:Label>
                                <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences") %>' MaxLength="4"></asp:TextBox> 
                            </ItemTemplate>
                        </asp:TemplateField>                                                 
                    </Columns>        
                </asp:GridView>
            </div>
        </div>
        <div class="section section4">
            <h3>Recommended Actions</h3>
            <div class="section4Full richText">
                <asp:ImageButton ID="btnRecommendedActions" CssClass="editCircleGrey editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="recommendedActionsButton_Click" runat="server" />
                <asp:Literal ID="litRecommended" runat="server"></asp:Literal>               
                <telerik:RadEditor ID="editorRecommendedActions" 
                            runat="server" 
                            CssClass="editorNewLine"                                                  
                            Height="184px" 
                            Width="810px"
                            MaxHtmlLength="5000"
                            NewLineMode="Div"
                            ToolsWidth="812px"                                                                                                                                       
                            ToolbarMode="ShowOnFocus"        
                            ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                            Visible="false"
                            EnableResize="false"
                            >   
                            <CssFiles>
                                <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                            </CssFiles>                                                                                
                </telerik:RadEditor> 
            </div>
        </div>

        <div style="page-break-before: always"></div>

        <div class="newPage">
            <h2 id="newPageTitle3" runat="server"></h2>
            <div id="newPageBuilding3" class="newPageBuilding" runat="server"></div>
            <img id="imgNewPageBuidling3" src="" alt="magnify" runat="server" />
        </div>
        <hr />
        <div class="section section5">
            <div class="section5Left">
             <h3>Total Avoidable Energy Cost Trend</h3>

            <asp:Chart ID="energyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="446px" Height="248px">                              
                <Series></Series>
                <ChartAreas>
                    <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                        <AxisY LineColor="#666666" LineWidth="1" Title="Avoidable Cost" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                            <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                            <MajorGrid Enabled="false" /> 
                            <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                        </AxisY>
                        <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                            <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                            <MajorGrid Enabled="false" />  
                            <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
            </asp:Chart>
            </div>
            <div class="section5Right richText">
                <h3>Energy Cost Trend Analysis</h3> 
                <asp:ImageButton ID="btnEnergyTrend" CssClass="editCircleGrey editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="energyTrendButton_Click" runat="server" />
                <asp:Literal ID="litEnergy" runat="server"></asp:Literal>
                <telerik:RadEditor ID="editorEnergyTrend"
                            runat="server" 
                            CssClass="editorNewLine"                                                  
                            Height="258px" 
                            Width="340px"
                            MaxHtmlLength="5000"
                            NewLineMode="Div"
                            ToolsWidth="400px"                                                                                                                                       
                            ToolbarMode="ShowOnFocus"        
                            ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                            Visible="false"
                            EnableResize="false"                                               
                            >   
                            <CssFiles>
                                <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                            </CssFiles>                                                                                
                </telerik:RadEditor>
            </div>
        </div>
        <hr class="hrDotted" />
        <div class="section section6">
            <div class="section6Left">
            <h3>Total Maintenance Incidents Trend</h3> 

            <asp:Chart ID="maintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="500px" Height="286px">                            
                <Series></Series>                
                <ChartAreas>
                    <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                        <AxisY LineColor="#666666" LineWidth="1" Title="Total Incidents" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                            <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                            <MajorGrid Enabled="false" /> 
                            <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                        </AxisY>
                        <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="Average Priority" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                            <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                            <MajorGrid Enabled="false" /> 
                            <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                        </AxisY2>
                        <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                            <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                            <MajorGrid Enabled="false" />  
                            <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                        </AxisX>                        
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend Font="Arial, 8.25pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                    <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                </Legends>
            </asp:Chart>
            </div>                    
            <div class="section6Right richText">
                <h3>Maintenance Trend Analysis</h3>
                <asp:ImageButton ID="btnMaintenanceTrend" CssClass="editCircleGrey editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="maintenanceTrendButton_Click" runat="server" /> 
                <asp:Literal ID="litMaintenance" runat="server"></asp:Literal> 
                <telerik:RadEditor ID="editorMaintenanceTrend" 
                            runat="server" 
                            CssClass="editorNewLine"                                                  
                            Height="258px" 
                            Width="340px"
                            MaxHtmlLength="5000"
                            NewLineMode="Div"
                            ToolsWidth="400px"                                                                                                                                       
                            ToolbarMode="ShowOnFocus"        
                            ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" 
                            Visible="false"
                            EnableResize="false"                                               
                            > 
                            <CssFiles>
                                <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                            </CssFiles>                                                                                  
                </telerik:RadEditor>               
            </div>
        </div>
        <hr class="hrDotted" />
        <div class="section section7">
            <div class="section7Left">
            <h3>Total Comfort Incidents Trend</h3>

            <asp:Chart ID="comfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="500px" Height="286px">                             
                <Series></Series>
                <ChartAreas>
                    <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                        <AxisY LineColor="#666666" LineWidth="1" Title="Total Incidents" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                            <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                            <MajorGrid Enabled="false" /> 
                            <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                        </AxisY>
                        <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="Average Priority" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                            <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                            <MajorGrid Enabled="false" /> 
                            <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                        </AxisY2>
                        <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                            <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                            <MajorGrid Enabled="false" />  
                            <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                        </AxisX>
                    </asp:ChartArea>
                </ChartAreas>
                <Legends>
                    <asp:Legend Font="Arial, 8.25pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                    <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                </Legends>
            </asp:Chart>
            </div>
            <div class="section7Right richText">
                <h3>Comfort Trend Analysis</h3> 
                <asp:ImageButton ID="btnComfortTrend" CssClass="editCircleGrey editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="comfortTrendButton_Click" runat="server" />
                <asp:Literal ID="litComfort" runat="server"></asp:Literal>
                <telerik:RadEditor ID="editorComfortTrend" 
                            runat="server" 
                            CssClass="editorNewLine"                                                  
                            Height="258px" 
                            Width="340px"
                            MaxHtmlLength="5000"
                            NewLineMode="Div"
                            ToolsWidth="400px"                                                                                                                                       
                            ToolbarMode="ShowOnFocus"        
                            ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                            Visible="false"
                            EnableResize="false"                                                
                            >   
                            <CssFiles>
                                <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                            </CssFiles>                                                                                
                </telerik:RadEditor>
            </div>
        </div>
        <div class="bottomPage">
            <img id="imgBottomPage2" alt="logo" src="" runat="server" />
        </div>
        </asp:Panel>

        <asp:UpdatePanel ID="updatePanelNested" EnableViewState="false" runat="server" UpdateMode="Conditional">
            <ContentTemplate>             
                <div id="divDownload" class="divDownload" runat="server">
                    <asp:Button ID="btnDownload" runat="server" OnClientClick="refreshHtml();" OnClick="btnDownloadButton_Click" CausesValidation="false" Text="Download"></asp:Button> - Please wait a moment while your pdf report is prepared for download.
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <script language="javascript" type="text/javascript">
            function refreshHtml() {
                document.getElementById('<%= hdn_container.ClientID %>').value = document.head.innerHTML + document.body.innerHTML;
            }

            document.getElementById('<%= hdn_container.ClientID %>').value = document.head.innerHTML + document.body.innerHTML;
        </script>

        </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
