﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="true" CodeBehind="KGSAdministration.aspx.cs" Inherits="CW.Website.KGSAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
             
                 	                  	        	
    <h1>KGS Building Technology Administration</h1>                          
    <div class="richText">
        The above KGS building technology administration pages are used to view, update, add, delete, assign, and schedule everything on the building science side of the system.
    </div>                      
          
    
</asp:Content>                