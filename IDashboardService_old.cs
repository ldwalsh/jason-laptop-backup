﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using CW.Data;
using CW.Business;
using CW.Data.AzureStorage.Models;
using System.Windows.Controls;

namespace CW.Website
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    //(SessionMode=SessionMode.Required)
    [ServiceContract]
	public interface IDashboardService
	{
        [OperationContract]
        IEnumerable<RawDataMapper.GetRawPlotData> GetLiveData(int userID, int buildingID);

        [OperationContract]
        IEnumerable<Building> GetBuildingData();

        [OperationContract]
        List<object> GetBuildingImage(int buidlingID, string buildingExtension, string buildingName);

        //DEPRICATED RAR
        //[OperationContract]
        //List<List<object>> GetAllClientData(string userRoleID);

        //DEPRICATED RAR
        //[OperationContract]
        //List<List<object>> GetAllProviderAdminClientData(int userID);

        [OperationContract]
        BuildingDataMapper.GetBuildingsData GetFullBuildingData(int buildingID);

        [OperationContract]
        IEnumerable<EquipmentDataMapper.GetEquipmentData> GetFullEquipmentData(int equipmentID);

        [OperationContract]
        IEnumerable<EquipmentVariableDataMapper.GetEquipmentEquipmentVariableData> GetAllEquipmentVariablesByEID(int equipmentID);

        [OperationContract]
        List<List<object>> GetAllEquipmentClassData();

        [OperationContract]
        List<List<object>> GetEquipmentClassInBuildingData(int buildingID, int equimentClassID);

        [OperationContract]
        List<EngUnit> GetPointEngUnit(List<int> PIDs);

        [OperationContract]
        List<string[]> GetPointInfo(List<int> PIDs);

        [OperationContract]
        List<string[]> GetPointInfoByEID(int eid);

        [OperationContract]
        List<List<double>> GetStatistics(int bid, int userID, string roleID, int clientID, DateTime? startDate);

        [OperationContract]
        List<List<object>> GetPastMonthStatistics(int bid, int userID, string roleID, int clientID, DateTime? startDate, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser);

        [OperationContract]
        List<List<object>> GetPastMonthEquipmentClassStatistics(int buildingID, int userID, string roleID, int clientID, DateTime? startDate, int classID, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser);

        [OperationContract]
        List<List<object>> GetPastMonthEquipmentStatistics(int BID, int EID, int userID, string roleID, int clientID, DateTime? startDate, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser);


        [OperationContract]
        List<List<object>> GetPastYearStatistics(int bid, int userID, string roleID, int clientID, DateTime startDate, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser);

        [OperationContract]
        List<List<object>> GetPastYearEquipmentClassStatistics(int buildingID, int userID, string roleID, int clientID, DateTime startDate, int classID, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser);

        [OperationContract]
        List<List<object>> GetPastYearEquipmentStatistics(int bid, int eid, int userID, string roleID, int clientID, DateTime startDate, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser);

        //DEPRICATED RAR
        //[OperationContract]
        //Client GetClientData(string clientID);

        [OperationContract]
        IEnumerable<Point> GetAllPointsByEID(int eid);

        [OperationContract]
        IEnumerable<RawDataMapper.GetRawPlotData> GetLiveDataForPIDs(List<int> pids);

        [OperationContract]
        IEnumerable<RawDataMapper.GetRawPlotData> GetPast30DataForPIDs(IEnumerable<int> pids, DateTime startDate);

        [OperationContract]
        List<RawDataMapper.GetRawPlotData> UpdateLiveDataForPIDs(List<int> pids);

        [OperationContract]
        string GetDashboardContent();
    }
}
