﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._extensions;
using CW.Website._framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public abstract class UserAdministrationBase: SitePage
    {
        #region  properties

        protected User mUser;
        protected const string userExists = "User already exists.";
        protected const string addUserSuccess = " user addition was successful.";
        protected const string addUserFailed = "Adding user failed. Please contact an administrator.";
        protected const string primaryClientMissing = "Cannot add users until a primary client is added for this organization.";
        protected const string addingAuditForUserFailed = "Error adding audit for user.";
        protected const string clientRequired = "At least one client is required to be assigned for this role.";
        protected const string assigningClientsFailed = "Error assigning clients to the user.";
        protected const string reassigningClientsFailed = "Error reassigning clients to the user.";
        protected const string publicOrKioskClientRestriction = "Cannot have more than one client associated to a user with this role.";
        protected const string publicOrKioskUserExists = "User with this role already exists for this client.";       
        protected const string updateSuccessful = " user update was successful.";
        protected const string updateFailed = "User update failed. Please contact an administrator.";
        protected const string updateRoleSuccessful = "User client role update was successful.";
        protected const string updateRoleFailed = "User client role update failed. Please contact an administrator.";
        protected const string updateQuickLinksSuccessful = "User client quicklinks update was successful.";
        protected const string updateQuickLinksFailed = "User client quicklinks update failed. Please contact an administrator.";
        protected const string updateBuildingsSuccess = "User client buildings and building groups update was successful.";
        protected const string updateBuildingsFailed = " User client buildings and building groups update failed. Please conteact an administrator.";
        protected const string updateModulesSuccess = "User client modules update was successful.";
        protected const string updateModulesFailed = " User client modules update failed. Please conteact an administrator.";
        protected const string deleteSuccessful = "User deletion was successful.";
        protected const string deleteFailed = "User deletion failed. Please contact an administrator.";
        protected const string unlockSuccessful = "User unlock was successful.";
        protected const string unlockFailed = "User unlock failed. Please contact an administrator.";
        protected const string invalidPasswordFirstOrLast = "User password cannot contain their first or last name.";
        protected const string invalidPasswordEmail = "User password cannot contain parts of their email.";
        protected const string tempPasswordSendSuccess = "Temporary password sent successfully.";
        protected const string tempPasswordSendFail = "Temporary password send failed. Please contact an administrator.";
        protected const string tempPasswordDBFail = "Temporary password update failed. Please contact an administrator.";

        //const string buildingGroupsExist = "Cannot delete user because user is associated with one or more building groups. Please remove building group associations first."; 
        //const string buildingsExist = "Cannot delete user because user is associated with one or more buildings. Please remove building associations first.";
        //const string clientsExist = "Cannot delete user because user is associated with one or more clients. Please remove client associations first.";        
        protected const string initialSortDirection = "ASC";
        protected const string initialSortExpression = "Email";

        protected const string initialUsersAuditSortDirection = "DESC";
        protected const string initialUsersAuditSortExpression = "LoginSuccessTotalCount";
        protected bool AnyAuthzChanges { get; set; }


        //gets and sets the gridview viewstate sort direction for the dataview
        protected string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? string.Empty; }
            set { ViewState["SortDirection"] = value; }
        }

        //gets and sets the gridview viewstate sort expression for the dataview
        protected string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        //gets and sets the gridview viewstate sort direction for the dataview
        protected string GridViewUsersAuditSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? string.Empty; }
            set { ViewState["SortDirection"] = value; }
        }

        //gets and sets the gridview viewstate sort expression for the dataview
        protected string GridViewUsersAuditSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        #endregion

        #region field

        #endregion

        #region constructor

            protected UserAdministrationBase()
            {
                AnyAuthzChanges = false; // Ensure that state is set to false so that not notification to DA is made
            }

        #endregion

        #region Load and Bind Fields

        protected void BindSecondaryRoles(DropDownExtension ddl, bool excludeKGSRoles, bool excludeAdminRoles, int? maxRole = null)
            {
                BindRoles(ddl, excludeKGSRoles, excludeAdminRoles, true, true, true, maxRole);
            }

            protected void BindPrimaryRoles(DropDownExtension ddl, bool excludeKGSRoles, bool excludeAdminRoles, bool excludeRestrictedRoles, bool excludePublicAndKioskRoles)
            {
                BindRoles(ddl, excludeKGSRoles, excludeAdminRoles, excludeRestrictedRoles, excludePublicAndKioskRoles);
            }

            private void BindRoles(DropDownExtension ddl, bool excludeKGSRoles, bool excludeAdminRoles, bool excludeRestrictedRoles, bool excludePublicAndKioskRoles, bool isSecondary = false, int? maxRole = null)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "RoleName";
                ddl.DataValueField = "RoleID";

                IEnumerable<UserRole> userRoles = Enumerable.Empty<UserRole>();

                userRoles = DataMgr.UserRoleDataMapper.GetAllRoles();

                if (isSecondary && maxRole != null)
                {
                    userRoles = RoleHelper.FilterRolesGreaterThanUserRole(userRoles, (int)maxRole);
                }
                else if (!siteUser.IsKGSSystemAdmin)
                {
                    userRoles = RoleHelper.FilterRolesGreaterThanOrEqualToUserRole(userRoles, siteUser.RoleID);
                }

                if (excludePublicAndKioskRoles)
                    userRoles = RoleHelper.FilterPublicAndKioskRoles(userRoles);

                if (excludeRestrictedRoles)
                    userRoles = RoleHelper.FilterRestrictedRoles(userRoles);

                if (excludeAdminRoles)
                    userRoles = RoleHelper.FilterAdminRoles(userRoles);

                if(excludeKGSRoles)
                    userRoles = RoleHelper.FilterKGSRoles(userRoles);

                ddl.DataSource = userRoles;
                ddl.DataBind();

                //populate decription in dropdown items by added a title attribute
                int counter = ddl.Items.Count > userRoles.Count() ? 1 : 0;
                foreach (UserRole ur in userRoles)
                {
                    ddl.Items[counter].Attributes.Add("title", ur.RoleDescription);
                    counter++;
                }
            }

            protected void BindCountries(DropDownList ddlAdd, DropDownList ddlEdit)
            {
                IEnumerable<Country> countries = DataMgr.CountryDataMapper.GetAllCountries();

                ddlAdd.DataTextField = "CountryName";
                ddlAdd.DataValueField = "Alpha2Code";
                ddlAdd.DataSource = countries;
                ddlAdd.DataBind();

                ddlEdit.DataTextField = "CountryName";
                ddlEdit.DataValueField = "Alpha2Code";
                ddlEdit.DataSource = countries;
                ddlEdit.DataBind();
            }

            protected void BindStates(DropDownList ddl, string alpha2Code)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code);
                ddl.DataBind();
            }

            protected void BindOrganizations(DropDownList ddl, DropDownList ddl2)
            {
                IList<Organization> orgs = DataMgr.OrganizationDataMapper.GetAllOrganizations();

                ddl.DataTextField = "OrganizationName";
                ddl.DataValueField = "OID";
                ddl.DataSource = orgs;
                ddl.DataBind();

                ddl2.DataTextField = "OrganizationName";
                ddl2.DataValueField = "OID";
                ddl2.DataSource = orgs;
                ddl2.DataBind();
            }

            protected void BindClients(DropDownList ddl, string roleID, string uid, string orgID)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "ClientName";
                ddl.DataValueField = "CID";

                //bind all clients for the org with provider clients
                if (!String.IsNullOrEmpty(orgID))
                {
                    ddl.DataSource = DataMgr.ClientDataMapper.GetAllActiveClientsByOrgWithProviderClients(Convert.ToInt32(orgID));
                    ddl.DataBind();
                }
                //bind based on user and role
                else if (!String.IsNullOrEmpty(roleID))
                {
                    ddl.DataSource = RoleHelper.IsKGSFullAdminOrHigher(roleID) ? DataMgr.ClientDataMapper.GetAllActiveClients() : DataMgr.ClientDataMapper.GetAllActiveClientsAssociatedToUID(Convert.ToInt32(uid), RoleHelper.IsRestricted(roleID));
                    ddl.DataBind();
                }
                //bind based on just restriced user
                else
                {
                    ddl.DataSource = DataMgr.ClientDataMapper.GetAllActiveClientsAssociatedToUID(Convert.ToInt32(uid), true);
                    ddl.DataBind();
                }
            }

            protected void BindBuildingGroups(Int32 cid, Int32 oid, ListBox lbEditClientBuildingGroupsTop, ListBox lbEditClientBuildingGroupsBottom, HiddenField hdnEditClientBuildingsUID, CheckBox chkIsBuildingRestricted)
            {
                var buildingGroups = DataMgr.BuildingGroupDataMapper.GetAllBuildingGroupsByCID(cid);

                //if client is for users organization, get building groups just for the organization client
                if (DataMgr.ClientDataMapper.IsClientForOrganization(cid, oid))
                {
                    //just get distinct buildinggroups. apply the guild for the first buildinggroup row.
                    var usersClientsList = DataMgr.UserClientDataMapper.GetUsersClients(Convert.ToInt32(hdnEditClientBuildingsUID.Value), cid).Where(c => c.BuildingGroupID != 0)
                                                                        .GroupBy(g => new { g.BuildingGroupID })
                                                                        .Select(s => new UsersClients
                                                                        {
                                                                            BuildingGroupID = s.First().BuildingGroupID,
                                                                            Guid = s.First().Guid
                                                                        });

                    var availableAssignedBuildingGroupList = from bg in buildingGroups
                                                             join c in usersClientsList on bg.BuildingGroupID equals c.BuildingGroupID
                                                             select Tuple.Create<string, string>(bg.BuildingGroupName, bg.BuildingGroupID.ToString() + "_" + c.Guid.ToString());


                    var formatedBuildingGroupsList = from bg in buildingGroups
                                                     select Tuple.Create<string, string>(bg.BuildingGroupName, bg.BuildingGroupID.ToString());

                    var availableUnassignedBuildingGroupList = formatedBuildingGroupsList.Except(from a in availableAssignedBuildingGroupList select Tuple.Create<string, string>(a.Item1, a.Item2.Split('_').First()));

                    if (availableAssignedBuildingGroupList.Any())
                    {
                        chkIsBuildingRestricted.Checked = true;
                    }

                    // Bind Top List Box - Available and Unassigned
                    ControlHelper.BindListBoxWithTuple(lbEditClientBuildingGroupsTop, availableUnassignedBuildingGroupList);
                    // Bind Bottom List Box - Available and Assigned
                    ControlHelper.BindListBoxWithTuple(lbEditClientBuildingGroupsBottom, availableAssignedBuildingGroupList);
                }
                //else we are getting buildings groups based on the provided client relationship
                else
                {
                    //NOTE: This section will get all building groups for a client based on all Provider clinet relationships.
                    //Meaning if this organization has 2 or more providers, it will combine and take the highest access levels.
                    //Thus is one provider has access to all building groups, and another does not, then the use will have all building groups available.
                      
                    List<int> providerIDs = DataMgr.ProviderDataMapper.GetAllProvidersIDsByOrg(oid);

                    var providersClientsList = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByProviderIDs(providerIDs).Where(pc => pc.CID == cid);

                    //just get distinct buildinggroups. apply the guild for the first buildinggroup row.
                    //var providersClientsList = mDataManager.ProviderClientDataMapper.GetProvidersClientsList(providerID, cid).Where(pc => pc.BuildingGroupID != 0);
                    //var providersClientsListBuildingGroups = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByProviderIDsFlattenedByCID(providerIDs).Where(pc => pc.BuildingGroupID != 0)
                    //                                                    .GroupBy(g => new { g.BuildingGroupID })
                    //                                                    .Select(s => new ProvidersClients
                    //                                                    {
                    //                                                        BuildingGroupID = s.First().BuildingGroupID,
                    //                                                        Guid = s.First().Guid
                    //                                                    });
                    var providersClientsListBuildingGroups = providersClientsList.Where(pc => pc.BuildingGroupID != 0)
                                                                    .GroupBy(g => new { g.BuildingGroupID })
                                                                    .Select(s => new ProvidersClients
                                                                    {
                                                                        BuildingGroupID = s.First().BuildingGroupID,
                                                                        Guid = s.First().Guid
                                                                    });


                    //just get distinct buildings. apply the guid for the first building row.
                    //var providersClientsList = mDataManager.ProviderClientDataMapper.GetProvidersClientsList(providerID, cid).Where(pc => pc.BID != 0);
                    //var providersClientsListBuildings = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByProviderIDsFlattenedByCID(providerIDs).Where(pc => pc.BID != 0)
                    //                                                        .GroupBy(g => new { g.BID })
                    //                                                        .Select(s => new ProvidersClients
                    //                                                        {
                    //                                                            BID = s.First().BID,
                    //                                                            Guid = s.First().Guid
                    //                                                        });
                    var providersClientsListBuildings = providersClientsList.Where(pc => pc.BID != 0)
                                                                            .GroupBy(g => new { g.BID })
                                                                            .Select(s => new ProvidersClients
                                                                            {
                                                                                BID = s.First().BID,
                                                                                Guid = s.First().Guid
                                                                            });


                    var usersClientsList = DataMgr.UserClientDataMapper.GetUsersClients(Convert.ToInt32(hdnEditClientBuildingsUID.Value), cid).Where(c => c.BuildingGroupID != 0)
                                                                            .GroupBy(g => new { g.BuildingGroupID })
                                                                            .Select(s => new UsersClients
                                                                            {
                                                                                BuildingGroupID = s.First().BuildingGroupID,
                                                                                Guid = s.First().Guid
                                                                            });

                    var availableAssignedBuildingGroupList = from bg in buildingGroups
                                                             join uc in usersClientsList on bg.BuildingGroupID equals uc.BuildingGroupID
                                                             select Tuple.Create<string, string>(bg.BuildingGroupName, bg.BuildingGroupID.ToString() + "_" + uc.Guid.ToString());


                    IEnumerable<Tuple<string, string>> formatedAvailableUnassignedBuildingGroupList = Enumerable.Empty<Tuple<string, string>>();

                    //if anything is restricted
                    if (providersClientsListBuildingGroups.Any() || providersClientsListBuildings.Any())
                    {
                        //if buildings groups are restricted, else leave empty
                        if (providersClientsListBuildingGroups.Any())
                        {
                            formatedAvailableUnassignedBuildingGroupList = (from bg in buildingGroups
                                                                            join pc in providersClientsListBuildingGroups on bg.BuildingGroupID equals pc.BuildingGroupID
                                                                            select Tuple.Create<string, string>(bg.BuildingGroupName, bg.BuildingGroupID.ToString()));
                        }
                    }
                    //else all building groups
                    else {
                        formatedAvailableUnassignedBuildingGroupList = (from bg in buildingGroups                                                                        
                                                                        select Tuple.Create<string, string>(bg.BuildingGroupName, bg.BuildingGroupID.ToString()));

                    }

                    var availableUnassignedBuildingGroupList = formatedAvailableUnassignedBuildingGroupList.Except(from a in availableAssignedBuildingGroupList select Tuple.Create<string, string>(a.Item1, a.Item2.Split('_').First()));

                    if (availableAssignedBuildingGroupList.Any())
                    {
                        chkIsBuildingRestricted.Checked = true;
                    }

                    // Bind Top List Box - Available and Unassigned
                    ControlHelper.BindListBoxWithTuple(lbEditClientBuildingGroupsTop, availableUnassignedBuildingGroupList);
                    // Bind Bottom List Box - Available and Assigned
                    ControlHelper.BindListBoxWithTuple(lbEditClientBuildingGroupsBottom, availableAssignedBuildingGroupList);
                }
            }

            protected void BindBuildings(Int32 cid, Int32 oid, ListBox lbEditClientBuildingsTop, ListBox lbEditClientBuildingsBottom, HiddenField hdnEditClientBuildingsUID, CheckBox chkIsBuildingRestricted)
            {
                //TODO: maybe eventually allow for a provider dropdown here based off the users_providers access

                var buildings = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(cid);

                //if client is for users organization, get buildings just for the organization client
                if (DataMgr.ClientDataMapper.IsClientForOrganization(cid, oid))
                {
                    //just get distinct buildings. apply the guild for the first building row.
                    var usersClientsList = DataMgr.UserClientDataMapper.GetUsersClients(Convert.ToInt32(hdnEditClientBuildingsUID.Value), cid).Where(c => c.BID != 0)
                                                                        .GroupBy(g => new { g.BID })
                                                                        .Select(s => new UsersClients
                                                                        {
                                                                            BID = s.First().BID,
                                                                            Guid = s.First().Guid
                                                                        });

                    var availableAssignedBuildingList = from b in buildings
                                                        join c in usersClientsList on b.BID equals c.BID
                                                        select Tuple.Create<string, string>(b.BuildingName, b.BID.ToString() + "_" + c.Guid.ToString());


                    var formatedBuildingsList = from b in buildings
                                                select Tuple.Create<string, string>(b.BuildingName, b.BID.ToString());

                    var availableUnassignedBuildingList = formatedBuildingsList.Except(from a in availableAssignedBuildingList select Tuple.Create<string, string>(a.Item1, a.Item2.Split('_').First()));

                    if (availableAssignedBuildingList.Any())
                    {
                        chkIsBuildingRestricted.Checked = true;
                    }

                    // Bind Top List Box - Available and Unassigned
                    ControlHelper.BindListBoxWithTuple(lbEditClientBuildingsTop, availableUnassignedBuildingList);
                    // Bind Bottom List Box - Available and Assigned
                    ControlHelper.BindListBoxWithTuple(lbEditClientBuildingsBottom, availableAssignedBuildingList);
                }
                //else we are getting buidlings based on the provided client relationship
                else
                {
                    //NOTE: This section will get all buildings for a client based on all Provider clinet relationships.
                    //Meaning if this organization has 2 or more providers, it will combine and take the highest access levels.
                    //Thus is one provider has access to all buildings, and another does not, then the use will have all buildings available.
                                    
                    List<int> providerIDs = DataMgr.ProviderDataMapper.GetAllProvidersIDsByOrg(oid);

                    var providersClientsList = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByProviderIDs(providerIDs).Where(pc => pc.CID == cid);

                    //just get distinct buildinggroups. apply the guild for the first buildinggroup row.
                    //var providersClientsList = mDataManager.ProviderClientDataMapper.GetProvidersClientsList(providerID, cid).Where(pc => pc.BuildingGroupID != 0);
                    //var providersClientsListBuildingGroups = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByProviderIDsFlattenedByCID(providerIDs).Where(pc => pc.BuildingGroupID != 0)
                    //                                                    .GroupBy(g => new { g.BuildingGroupID })
                    //                                                    .Select(s => new ProvidersClients
                    //                                                    {
                    //                                                        BuildingGroupID = s.First().BuildingGroupID,
                    //                                                        Guid = s.First().Guid
                    //                                                    });
                    var providersClientsListBuildingGroups = providersClientsList.Where(pc => pc.BuildingGroupID != 0)
                                                                    .GroupBy(g => new { g.BuildingGroupID })
                                                                    .Select(s => new ProvidersClients
                                                                    {
                                                                        BuildingGroupID = s.First().BuildingGroupID,
                                                                        Guid = s.First().Guid
                                                                    });


                    //just get distinct buildings. apply the guild for the first building row.
                    //var providersClientsList = mDataManager.ProviderClientDataMapper.GetProvidersClientsList(providerID, cid).Where(pc => pc.BID != 0);
                    //var providersClientsListBuildings = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByProviderIDsFlattenedByCID(providerIDs).Where(pc => pc.BID != 0)
                    //                                                        .GroupBy(g => new { g.BID })
                    //                                                        .Select(s => new ProvidersClients
                    //                                                        {
                    //                                                            BID = s.First().BID,
                    //                                                            Guid = s.First().Guid
                    //                                                        });
                    var providersClientsListBuildings = providersClientsList.Where(pc => pc.BID != 0)
                                                            .GroupBy(g => new { g.BID })
                                                            .Select(s => new ProvidersClients
                                                            {
                                                                BID = s.First().BID,
                                                                Guid = s.First().Guid
                                                            });


                    var usersClientsList = DataMgr.UserClientDataMapper.GetUsersClients(Convert.ToInt32(hdnEditClientBuildingsUID.Value), cid).Where(c => c.BID != 0)
                                                                        .GroupBy(g => new { g.BID })
                                                                        .Select(s => new UsersClients
                                                                        {
                                                                            BID = s.First().BID,
                                                                            Guid = s.First().Guid
                                                                        });

                    var availableAssignedBuildingList = from b in buildings
                                                        join c in usersClientsList on b.BID equals c.BID
                                                        select Tuple.Create<string, string>(b.BuildingName, b.BID.ToString() + "_" + c.Guid.ToString());


                    IEnumerable<Tuple<string, string>> formatedAvailableUnassignedBuildingList = Enumerable.Empty<Tuple<string, string>>();

                    //if anything is restricted
                    if (providersClientsListBuildingGroups.Any() || providersClientsListBuildings.Any())
                    {
                        //if buildings are restricted, else leave empty
                        if (providersClientsListBuildings.Any())
                        {
                            formatedAvailableUnassignedBuildingList = (from b in buildings
                                                                       join pc in providersClientsListBuildings on b.BID equals pc.BID                                                                       
                                                                       select Tuple.Create<string, string>(b.BuildingName, b.BID.ToString()));
                        }
                    }
                    //else all buildings
                    else
                    {
                        formatedAvailableUnassignedBuildingList = (from b in buildings
                                                                        select Tuple.Create<string, string>(b.BuildingName, b.BID.ToString()));

                    }

                    var availableUnassignedBuildingList = formatedAvailableUnassignedBuildingList.Except(from a in availableAssignedBuildingList select Tuple.Create<string, string>(a.Item1, a.Item2.Split('_').First()));


                    if (availableAssignedBuildingList.Any())
                    {
                        chkIsBuildingRestricted.Checked = true;
                    }

                    // Bind Top List Box - Available and Unassigned
                    ControlHelper.BindListBoxWithTuple(lbEditClientBuildingsTop, availableUnassignedBuildingList);
                    // Bind Bottom List Box - Available and Assigned
                    ControlHelper.BindListBoxWithTuple(lbEditClientBuildingsBottom, availableAssignedBuildingList);
                }
            }

            protected void BindModules(Int32 cid, ListBox lbEditClientModulesTop, ListBox lbEditClientModulesBottom, HiddenField hdnEditClientModulesUID, CheckBox chkIsModuleRestricted)
            {
                //TODO: maybe eventually allow for a provider dropdown here based off the users_providers access

                var modules = DataMgr.ModuleDataMapper.GetAllActiveModulesAssociatedToClientID(cid);

                var availableAssignedModulesList = DataMgr.ModuleDataMapper.GetAllActiveRestrictedModulesAssociatedToCIDAndUIDInListboxFormat(cid, Convert.ToInt32(hdnEditClientModulesUID.Value), modules);

                var formatedModulesList = from m in modules
                                          select Tuple.Create<string, string>(m.ModuleName, m.ModuleID.ToString());

                var availableUnassignedModulesList = formatedModulesList.Except(from a in availableAssignedModulesList select Tuple.Create<string, string>(a.Item1, a.Item2.Split('_').First()));

                if (availableAssignedModulesList.Any())
                {
                    chkIsModuleRestricted.Checked = true;
                }

                // Bind Top List Box - Available and Unassigned
                ControlHelper.BindListBoxWithTuple(lbEditClientModulesTop, availableUnassignedModulesList);
                // Bind Bottom List Box - Available and Assigned
                ControlHelper.BindListBoxWithTuple(lbEditClientModulesBottom, availableAssignedModulesList);
            }

            protected void BindCultures(DropDownList ddlAdd, DropDownList ddlEdit)
            {
                CultureInfo[] cultures = CultureHelper.FilterUserCultures(CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.DisplayName).ToArray());

                ddlAdd.DataTextField = "DisplayName";
                ddlAdd.DataValueField = "LCID";
                ddlAdd.DataSource = cultures;
                ddlAdd.DataBind();
                ddlAdd.SelectedValue = Convert.ToString(BusinessConstants.Culture.DefaultCultureLCID);

                ddlEdit.DataTextField = "DisplayName";
                ddlEdit.DataValueField = "LCID";
                ddlEdit.DataSource = cultures;
                ddlEdit.DataBind();
            }

        #endregion

		#region Update Associations Methods

            protected void UpdateAssociationsBuildings(int uid, int cid, ListBox lbEditClientBuildingsTop, ListBox lbEditClientBuildingsBottom, HiddenField hdnEditClientBuildingsPrimaryRoleID, HiddenField hdnEditClientBuildingsSecondaryRoleID, HiddenField hdnEditClientBuildingsUnrestricted)
            {
                int[] moduleIds = null;
                var userClientRows = new List<UsersClients>();
                var insertList = new List<UsersClients>();
                var deleteList = new List<UsersClients>();
                
                //get primary role form hidden field
                int primaryRoleID = Convert.ToInt32(hdnEditClientBuildingsPrimaryRoleID.Value);

                // get secondary role from hidden field
                int secondaryRoleID = Convert.ToInt32(hdnEditClientBuildingsSecondaryRoleID.Value);

                //get regardless if any
                moduleIds = DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).Where(c => c.ModuleID != 0).Select(m => m.ModuleID).Distinct().ToArray();

                // add association if it doesn't already exist
                foreach (ListItem item in lbEditClientBuildingsBottom.Items)
                {
                    int bid;
                    string[] itemValues = item.Value.Split('_');

                    // has no value or has a Guid in the value, therefore relationship already exists
                    if (itemValues.Count() == 0 || itemValues.Count() > 1) { continue; }

                    // parse bid
                    if (!Int32.TryParse(itemValues[0], out bid)) { continue; }

                    //if module restricted then add for each module
                    if (moduleIds.Any())
                    {
                        //for each module 
                        foreach (int moduleID in moduleIds)
                        {
                            // add to insert list
                            insertList.Add(new UsersClients() { CID = cid, UID = uid, BID = bid, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID, ModuleID = moduleID });
                        }
                    }
                    else
                    {
                        // add to insert list
                        insertList.Add(new UsersClients() { CID = cid, UID = uid, BID = bid, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID });
                    }
                }
                
                // delete unrestricted user rows (could be multple beacuse of modules)
                if (insertList.Count != 0 && hdnEditClientBuildingsUnrestricted.Value == "true")
                {
                    var unrestrictedUserClient = DataMgr.UserClientDataMapper.GetUnrestrictedBuildingsUsersClients(uid, cid);
                    DataMgr.UserClientDataMapper.DeleteUsersClients(unrestrictedUserClient);
                    hdnEditClientBuildingsUnrestricted.Value = "";
                }

                // insert associations
                DataMgr.UserClientDataMapper.AddUsersClients(insertList);
                
                if (lbEditClientBuildingsTop.Items.Count > 0)
                    userClientRows = DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).ToList();

                // remove association if it currently exist
                foreach (ListItem item in lbEditClientBuildingsTop.Items)
                {
                    int bid;
                    Guid guid;
                    string[] itemValues = item.Value.Split('_');

                    // no Guid, relationship does not exist, therefore delete is not neccessry
                    if (itemValues.Count() < 2) { continue; }

                    // parse bid and guid
                    if (!Int32.TryParse(itemValues[0], out bid)) { continue; }
                    if (!Guid.TryParse(itemValues[1], out guid)) { continue; }

                    //can be multiple now because of modules, get all to delete for each guid
                    var userClientBuildingRows = userClientRows.Where(c => c.BID == bid);

                    foreach (UsersClients uc in userClientBuildingRows)
                    {
                        // add to delete list
                        deleteList.Add(new UsersClients() { CID = cid, UID = uid, Guid = uc.Guid });
                    }
                }

                // delete associations
                DataMgr.UserClientDataMapper.DeleteUsersClients(deleteList);

                //if we deleted last building and/or buildign group, but modules are still restricted, add rows back for each module with unrestriced buildings
                if (moduleIds.Any() && !DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).Any())
                {
                    insertList = new List<UsersClients>();

                    //for each module 
                    foreach (int moduleID in moduleIds)
                    {
                        // add to insert list
                        insertList.Add(new UsersClients() { CID = cid, UID = uid, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID, ModuleID = moduleID });
                    }

                    DataMgr.UserClientDataMapper.AddUsersClients(insertList);
                }
            }

            protected void UpdateAssociationsBuildingsGroups(int uid, int cid, ListBox lbEditClientBuildingGroupsTop, ListBox lbEditClientBuildingGroupsBottom,  HiddenField hdnEditClientBuildingsPrimaryRoleID, HiddenField hdnEditClientBuildingsSecondaryRoleID, HiddenField hdnEditClientBuildingsUnrestricted)
            {
                int[] moduleIds = null;
                var userClientRows = new List<UsersClients>();
                var insertList = new List<UsersClients>();
                var deleteList = new List<UsersClients>();

                //get primary role form hidden field
                int primaryRoleID = Convert.ToInt32(hdnEditClientBuildingsPrimaryRoleID.Value);

                // get secondary role from hidden field
                int secondaryRoleID = Convert.ToInt32(hdnEditClientBuildingsSecondaryRoleID.Value);

                //get regardless if any
                moduleIds = DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).Where(c => c.ModuleID != 0).Select(m => m.ModuleID).Distinct().ToArray();

                // add association if it doesn't already exist
                foreach (ListItem item in lbEditClientBuildingGroupsBottom.Items)
                {
                    int buildingGroupID;
                    string[] itemValues = item.Value.Split('_');

                    // has no value or has a Guid in the value, therefore relationship already exists
                    if (itemValues.Count() == 0 || itemValues.Count() > 1) { continue; }

                    // parse bid
                    if (!Int32.TryParse(itemValues[0], out buildingGroupID)) { continue; }

                    //if module restricted then add for each module
                    if (moduleIds.Any())
                    {
                        //for each module 
                        foreach (int moduleID in moduleIds)
                        {
                            // add to insert list
                            insertList.Add(new UsersClients() { CID = cid, UID = uid, BuildingGroupID = buildingGroupID, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID, ModuleID = moduleID });
                        }
                    }
                    else
                    {
                        // add to insert list
                        insertList.Add(new UsersClients() { CID = cid, UID = uid, BuildingGroupID = buildingGroupID, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID });
                    }
                }

                // delete unrestricted user rows (could be multple beacuse of modules)
                if (insertList.Count != 0 && hdnEditClientBuildingsUnrestricted.Value == "true")
                {
                    var unrestrictedUserClient = DataMgr.UserClientDataMapper.GetUnrestrictedBuildingsUsersClients(uid, cid);
                    DataMgr.UserClientDataMapper.DeleteUsersClients(unrestrictedUserClient);
                    hdnEditClientBuildingsUnrestricted.Value = "";
                }

                // insert associations
                DataMgr.UserClientDataMapper.AddUsersClients(insertList);

                if (lbEditClientBuildingGroupsTop.Items.Count > 0)
                    userClientRows = DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).ToList();


                // remove association if it currently exist
                foreach (ListItem item in lbEditClientBuildingGroupsTop.Items)
                {
                    int buildingGroupID;
                    Guid guid;
                    string[] itemValues = item.Value.Split('_');

                    // no Guid, relationship does not exist, therefore delete is not neccessry
                    if (itemValues.Count() < 2) { continue; }

                    // parse bid and guid
                    if (!Int32.TryParse(itemValues[0], out buildingGroupID)) { continue; }
                    if (!Guid.TryParse(itemValues[1], out guid)) { continue; }

                    //can be multiple now because of modules, get all to delete for each guid
                    var userClientBuildingGroupRows = userClientRows.Where(c => c.BuildingGroupID == buildingGroupID);

                    foreach (UsersClients uc in userClientBuildingGroupRows)
                    {
                        // add to delete list
                        deleteList.Add(new UsersClients() { CID = cid, UID = uid, Guid = uc.Guid });
                    }
                }

                // delete associations
                DataMgr.UserClientDataMapper.DeleteUsersClients(deleteList);

                //if we deleted last building and/or buildign group, but modules are still restricted, add rows back for each module with unrestriced buildings
                if (moduleIds.Any() && !DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).Any())
                {
                    insertList = new List<UsersClients>();

                    //for each module 
                    foreach (int moduleID in moduleIds)
                    {
                        // add to insert list
                        insertList.Add(new UsersClients() { CID = cid, UID = uid, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID, ModuleID = moduleID });
                    }

                    DataMgr.UserClientDataMapper.AddUsersClients(insertList);
                }
            }

            protected void UpdateAssociationsModules(int uid, int cid, ListBox lbEditClientModulesTop, ListBox lbEditClientModulesBottom, HiddenField hdnEditClientModulesPrimaryRoleID, HiddenField hdnEditClientModulesSecondaryRoleID, HiddenField hdnEditClientModulesUnrestricted)
            {
                int[] buildingIds = null;
                int[] buildingGroupIds = null;
                var userClientRows = new List<UsersClients>();
                var insertList = new List<UsersClients>();
                var deleteList = new List<UsersClients>();

                //get primary role form hidden field
                int primaryRoleID = Convert.ToInt32(hdnEditClientModulesPrimaryRoleID.Value);

                // get secondary role from hidden field
                int secondaryRoleID = Convert.ToInt32(hdnEditClientModulesSecondaryRoleID.Value);

                //get regardless if any
                buildingIds = DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).Where(c => c.BID != 0).Select(b => b.BID).Distinct().ToArray();
                buildingGroupIds = DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).Where(c => c.BuildingGroupID != 0).Select(bg => bg.BuildingGroupID).Distinct().ToArray();

                // add association if it doesn't already exist
                foreach (ListItem item in lbEditClientModulesBottom.Items)
                {
                    int moduleID;
                    string[] itemValues = item.Value.Split('_');

                    // has no value or has a Guid in the value, therefore relationship already exists
                    if (itemValues.Count() == 0 || itemValues.Count() > 1) { continue; }

                    // parse moduleID
                    if (!Int32.TryParse(itemValues[0], out moduleID)) { continue; }

                    //if biulding or building group restricted
                    if (buildingIds.Any() || buildingGroupIds.Any())
                    {
                        //if building restricted then add for each building
                        if (buildingIds.Any())
                        {
                            //for each building 
                            foreach (int bid in buildingIds)
                            {
                                // add to insert list
                                insertList.Add(new UsersClients() { CID = cid, UID = uid, BID = bid, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID, ModuleID = moduleID });
                            }
                        }

                        //if building restricted then add for each building
                        if (buildingIds.Any())
                        {
                            //for each buildinggroup 
                            foreach (int buildingGroupId in buildingGroupIds)
                            {
                                // add to insert list
                                insertList.Add(new UsersClients() { CID = cid, UID = uid, BuildingGroupID = buildingGroupId, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID, ModuleID = moduleID });
                            }
                        }
                    }
                    else
                    {
                        // add to insert list
                        insertList.Add(new UsersClients() { CID = cid, UID = uid, ModuleID = moduleID, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID });
                    }
                }

                // delete unrestricted user rows (could be multple beacuse of builidngs or building groups)
                if (insertList.Count != 0 && hdnEditClientModulesUnrestricted.Value == "true")
                {
                    var unrestrictedUserClient = DataMgr.UserClientDataMapper.GetUnrestrictedModulesUsersClients(uid, cid);
                    DataMgr.UserClientDataMapper.DeleteUsersClients(unrestrictedUserClient);
                    hdnEditClientModulesUnrestricted.Value = "";
                }

                // insert associations
                DataMgr.UserClientDataMapper.AddUsersClients(insertList);

                if (lbEditClientModulesTop.Items.Count > 0)
                    userClientRows = DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).ToList();

                // remove association if it currently exist
                foreach (ListItem item in lbEditClientModulesTop.Items)
                {
                    int moduleID;
                    Guid guid;
                    string[] itemValues = item.Value.Split('_');

                    // no Guid, relationship does not exist, therefore delete is not neccessry
                    if (itemValues.Count() < 2) { continue; }

                    // parse bid and guid
                    if (!Int32.TryParse(itemValues[0], out moduleID)) { continue; }
                    if (!Guid.TryParse(itemValues[1], out guid)) { continue; }

                    //can be multiple now because of biuldings and building groups, get all to delete for each guid
                    var userClientModuleRows = userClientRows.Where(c => c.ModuleID == moduleID);

                    foreach (UsersClients uc in userClientModuleRows)
                    {
                        // add to delete list
                        deleteList.Add(new UsersClients() { CID = cid, UID = uid, Guid = uc.Guid });
                    }

                    //delete quicklinks for user, client, and module
                    DataMgr.QuickLinksDataMapper.DeleteUserClientQuickLinksByCIDAndUIDAndModuleID(cid, uid, moduleID);

                    //if analysis builder delete use client views
                    if (moduleID == (Int32)BusinessConstants.Module.Modules.AnalysisBuilder)
                    {
                        DataMgr.AnalysisBuilderDataMapper.DeleteViewsByCIDAndUID(cid, uid);
                    }
                }

                // delete associations
                DataMgr.UserClientDataMapper.DeleteUsersClients(deleteList);

                //if we deleted last module, but building or building groups are still restricted, add rows back for each with unrestriced modules
                if (!DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).Any())
                {
                    insertList = new List<UsersClients>();

                    if (buildingIds.Any())
                    {
                        //for each building 
                        foreach (int buildingID in buildingIds)
                        {
                            // add to insert list
                            insertList.Add(new UsersClients() { CID = cid, UID = uid, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID, BID = buildingID });
                        }
                    }
                    if (buildingGroupIds.Any())
                    {
                        //for each buildingGroup
                        foreach (int buildingGroupId in buildingGroupIds)
                        {
                            // add to insert list
                            insertList.Add(new UsersClients() { CID = cid, UID = uid, PrimaryRoleID = primaryRoleID, SecondaryRoleID = (int?)secondaryRoleID, BuildingGroupID = buildingGroupId });
                        }
                    }

                    DataMgr.UserClientDataMapper.AddUsersClients(insertList);
                }
            }

            /// <summary>
            /// Only used for clean up for users on role change
            /// </summary>
            /// <param name="uid"></param>
            /// <param name="cid"></param>
            /// <param name="changedRole"></param>
            /// <param name="isPrimaryRoleChange"></param>
            protected void CleanUpTablesByRoleChange(int uid, int? cid, string changedRole, bool isPrimaryRoleChange)
            {
                if (RoleHelper.IsRestricted(changedRole))
                {
                    //TODO: Doesnt exist yet. delete any building modules for user
                    //TODO: Doesnt exist yet. delete any client modules for user           

                    //No need to delete usersclients restricted buildings and building groups.

                    //No need to delete usersfrom user groups as they are not role based anymore.

                    //get and delete all user clients for user and changed role id
                    if (isPrimaryRoleChange)
                    {
                        //deletes quicklinks for user
                        DataMgr.QuickLinksDataMapper.DeleteAllUserClientQuickLinksByUIDAndChangedRoleID(uid, Convert.ToInt32(changedRole));

                        if(RoleHelper.IsPublicUser(changedRole)){
                            //deletes analysisbuildiner view for user
                            DataMgr.AnalysisBuilderDataMapper.DeleteViewsByUID(uid);
                        }

                        DataMgr.UserClientDataMapper.DeleteUsersClients(DataMgr.UserClientDataMapper.GetUsersClientsByUID(uid).ToList());
                    }
                    else
                    {
                        //deletes quicklinks for user and client and chagned role id
                        DataMgr.QuickLinksDataMapper.DeleteAllUserQuickLinksByUIDAndCIDAndChangedRoleID(uid, (int)cid, Convert.ToInt32(changedRole));

                        if (RoleHelper.IsPublicUser(changedRole))
                        {
                            //deletes analysisbuildiner view for user and client
                            DataMgr.AnalysisBuilderDataMapper.DeleteViewsByCIDAndUID((int)cid, uid);
                        }
                    }
                }
                else
                {
                    //TODO: Doesnt exist yet. delete any building modules for user

                    //No need to delete usersfrom user groups as they are not role based anymore.
                
                    //get and delete all user clients for user
                    if (isPrimaryRoleChange)
                    {
                        DataMgr.UserClientDataMapper.DeleteUsersClients(DataMgr.UserClientDataMapper.GetUsersClientsByUID(uid).ToList());
                    }
                }
            }

            protected void SetUserBackToFullMode(int uid, int cid, int primaryRoleID, int secondaryRoleID)
            {
                if (!DataMgr.UserClientDataMapper.GetUsersClients(uid, cid).Any())
                    DataMgr.UserClientDataMapper.AddUsersClients(new UsersClients() { CID = cid, UID = uid, Guid = new Guid(), BID = 0, BuildingGroupID = 0, ModuleID = 0, PrimaryRoleID = primaryRoleID, SecondaryRoleID = secondaryRoleID, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow });
            }

        #endregion

        #region Grid Helper Methods

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            protected string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            protected void SetGridCountLabel(int count, Label lblResults)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} user found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} users found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No users found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            protected string GetUsersAuditSortDirection()
            {
                //switch sorting directions
                switch (GridViewUsersAuditSortDirection)
                {
                    case "ASC":
                        GridViewUsersAuditSortDirection = "DESC";
                        ViewState["UsersAuditSortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewUsersAuditSortDirection = "ASC";
                        ViewState["UsersAuditSortDirection"] = "ASC";
                        break;
                }
                return GridViewUsersAuditSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView UsersAuditSortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewUsersAuditSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewUsersAuditSortExpression, GridViewUsersAuditSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewUsersAuditSortExpression, GetUsersAuditSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            protected void SetGridUsersAuditCountLabel(int count, Label lblUsersAuditResults)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblUsersAuditResults.Text = String.Format("{0} users audits found.", count);
                }
                else if (count > 1)
                {
                    lblUsersAuditResults.Text = String.Format("{0} users audits found.", count);
                }
                else
                {
                    lblUsersAuditResults.Text = "No users audits found.";
                }
            }

        #endregion

        #region Helper Methods

            protected void ClearListbox(ListBox lb)
            {
                lb.Items.Clear();
            }

            protected void DisableClientRoleRestrictedControls(CheckBox chkEditClientRolesRole, DropDownList ddlEditClientRolesRole)
            {
                chkEditClientRolesRole.Checked = false;
                ddlEditClientRolesRole.Items.Clear();
            }

            protected void ToggleBuildingRestrictedControls(bool isRestricted, HtmlGenericControl divEditBuildingGroupsListBoxs, HtmlGenericControl divEditBuildingsListBoxs)
            {
                // show / hide  buildings / building groups list boxes
                divEditBuildingGroupsListBoxs.Visible = isRestricted;
                divEditBuildingsListBoxs.Visible = isRestricted;
            }

            protected void ToggleModuleRestrictedControls(bool isRestricted, HtmlGenericControl divEditModulesListBoxs)
            {
                // show / hide modules list boxes
                divEditModulesListBoxs.Visible = isRestricted;
            }    

            protected void SendTemporaryPassword(Int32 uid, Label lblResults, Label lblErrors, HtmlAnchor lnkSetFocusView)
            {
                var user = DataMgr.UserDataMapper.GetUser(uid);
                var email = user.Email;
                var randomPassword = Membership.GeneratePassword(12, 2);
                var newHashedPassword = Encryptor.getMd5Hash(randomPassword);

                try
                {
                    Email.SendRegeneratedPasswordEmail(user, randomPassword, siteUser.IsSchneiderTheme);
                    
                    try
                    {
                        user.Password = newHashedPassword;
                        DataMgr.UserDataMapper.UpdateUserPassword(user);
                        SetMessage(tempPasswordSendSuccess, lblResults, lnkSetFocusView);
                    }
                    catch (Exception ex)
                    {
                        SetMessage(tempPasswordDBFail, lblErrors, lnkSetFocusView);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Password update failed for user: UserEmail={0}.", email), ex);
                    }
                }
                catch (Exception ex)
                {
                    SetMessage(tempPasswordSendFail, lblErrors, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Regenerated password email was not sent for user: UserEmail={0}.", email), ex);
                }
            }

            private void SetMessage(String message, Label lbl, HtmlAnchor lnkSetFocusView)
            {
                lbl.Text = message;
                lbl.Visible = true;

                lnkSetFocusView.Focus();
        }

        protected void RegisterPostClick(params LinkButton[] buttons)
        {
            foreach (LinkButton button in buttons)
            {
                button.Click += Button_PostClick;
            }
        }

        /// <summary>
        /// This method is exclusively used to send a notification to DA to purge user's AuthZs cached in Azure.
        /// Valid only for users that are API subscribers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_PostClick(object sender, EventArgs e)
        {
            if (null != mUser && AnyAuthzChanges)
            {
                _controls.ApiSubscriberHelper ash = new _controls.ApiSubscriberHelper(mUser.UID);
                ash.SignalAuthorizationsChangedAsync(DataMgr).ContinueWith(_ => { });
            }
        }
    #endregion
    }
}