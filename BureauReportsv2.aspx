﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="BureauReportsv2.aspx.cs" Inherits="CW.Website.BureauReportsv2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="System.Linq" %>
<%@ Import Namespace="CW.Website._framework.httpmodules" %>
<%@ Import Namespace="CW.Common.Constants" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <script type="text/javascript">

            var ListBoxCustomValidator = function (sender, args) {
            var listBox = document.getElementById(sender.controltovalidate);
        
            //.disabled changed in framework 4.5, .classList doesnt work in ie9
            if (!$(listBox).hasClass('aspNetDisabled')) {

                var listBoxCnt = 0;

                for (var x = 0; x < listBox.options.length; x++) {
                    if (listBox.options[x].selected) listBoxCnt++;
                }

                args.IsValid = (listBoxCnt > 0)
            }
            else {
                args.IsValid = true;
            }
            };

            function chkIncludeTopDiagnosticsDetials_OnClick() {
                if (document.getElementById('ctl00_plcCopy_chkIncludeTopDiagnosticsDetails').checked) {
                    document.getElementById('ctl00_plcCopy_chkIncludeTopDiagnosticsFigures').disabled = false;
                }
                else {
                    document.getElementById('ctl00_plcCopy_chkIncludeTopDiagnosticsFigures').checked = false;
                    document.getElementById('ctl00_plcCopy_chkIncludeTopDiagnosticsFigures').disabled = true;
                }
            }
            function chkEditIncludeTopDiagnosticsDetials_OnClick() {
                if (document.getElementById('ctl00_plcCopy_chkEditIncludeTopDiagnosticsDetails').checked) {
                    document.getElementById('ctl00_plcCopy_chkEditIncludeTopDiagnosticsFigures').disabled = false;
                }
                else {
                    document.getElementById('ctl00_plcCopy_chkEditIncludeTopDiagnosticsFigures').checked = false;
                    document.getElementById('ctl00_plcCopy_chkEditIncludeTopDiagnosticsFigures').disabled = true;
                }
            }

            function ValidateMultipleValidationGroupsScheduled() {
                var isValid = false;
                isValid = Page_ClientValidate('Common');
                if (isValid) {
                    isValid = Page_ClientValidate('AddBureauReport');
                }

                return isValid;
            }


            function ValidateMultipleValidationGroupsGenerate() {
                var isValid = false;
                isValid = Page_ClientValidate('Generate');
                if (isValid) {
                    isValid = Page_ClientValidate('Common');
                }

                return isValid;
            }

            
    </script>

             <h1>Bureau Reports</h1>
             <div class="richText">
                <p>The bureau reports page is used to enter dynamic input to generate a bureau report based on a provided date range.</p>
                <br />                
                <p>Follow the instructions below to generate and download as a pdf:</p>
                <ol>
                    <li>Make your selections and optionally fill out any content ahead of time.</li>
                    <li>Click generate below and a new page will open up with the report.</li>
                    <li>To edit content, click the dots in the right hand corner of any editable area.</li>
                    <li>Click download at the bottom of the generated report.</li>
                    <!--<li>Open this page in <strong>Chrome Version 23</strong> or higher.</li>
                    <li><strong>Produce content in document outside of the browser</strong> in case of session timeouts or the need to go back after sumbitting. Paste and format in form when ready to sumbit in entirety. Do not use the back button in the browser as it will not retain entered content.</li>
                    <li>Once generated, make sure you are at the default browser <strong>Zoom setting of 100%</strong>. You can check from the right hand corner of the browser window.</li> 
                    <li>Select to <strong>"Print"</strong> from the browser window.</li>
                    <li>Within the print screen, change the desitination to <strong>"Save as PDF"</strong>.</li>
                    <li>Within the print screen, uncheck the <strong>"Headers and footers"</strong> option.</li>
                    <li>Click <strong>"Save"</strong>.</li>-->
                </ol>
             </div>
             <div class="updateProgressDiv">
                 <asp:UpdateProgress ID="updateProgressTop" runat="server">
                     <ProgressTemplate>
                        <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                     </ProgressTemplate>
                 </asp:UpdateProgress>
             </div>
             <div class="administrationControls">
                <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="Generate/Schedule Bureau Reports"></telerik:RadTab>
                            <telerik:RadTab Text="View Scheduled Bureau Reports"></telerik:RadTab>                   
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                        <telerik:RadPageView ID="RadPageView1" runat="server">                              

                                <h2>Bureau Current Report Period</h2>
                                <a id="lnkSetFocusAdd" runat="server"></a>                                                  
                                <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>   
                            
                                <input type="hidden" id="hdnCID" name="hdnCID" runat="server" />
                                <input type="hidden" id="hdnUID" name="hdnUID" runat="server" />
                                <input type="hidden" id="hdnOID" name="hdnOID" runat="server" />
                                <input type="hidden" id="hdnUserCulture" name="hdnUserCulture" runat="server" />

                                <div class="divForm">    
                                    <label class="label">*Start Date:</label>
                                    <telerik:RadDatePicker ID="txtStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                    
                                    <asp:RequiredFieldValidator ID="dateStartRequiredValidator" runat="server"
                                                    CssClass="errorMessage" 
                                                    ErrorMessage="Date is a required field."
                                                    ControlToValidate="txtStartDate"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="Generate">
                                                    </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="dateStartRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="dateStartRequiredValidatorExtender" 
                                        TargetControlID="dateStartRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>    
                               </div>
                               <div class="divForm">
                                    <label class="label">*End Date:</label>                     
                                    <telerik:RadDatePicker ID="txtEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                                                                   
                                    <asp:RequiredFieldValidator ID="dateEndRequiredValidator" runat="server"
                                                    CssClass="errorMessage" 
                                                    ErrorMessage="Date is a required field."
                                                    ControlToValidate="txtEndDate"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="Generate">
                                                    </asp:RequiredFieldValidator>
                                   <ajaxToolkit:ValidatorCalloutExtender ID="dateEndRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="dateEndRequiredValidatorExtender" 
                                        TargetControlID="dateEndRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:CompareValidator ID="dateCompareValidator" Display="None" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Invalid range."
                                        ControlToValidate="txtEndDate"
                                        ControlToCompare="txtStartDate"
                                        Type="Date"
                                        Operator="GreaterThanEqual"
                                        ValidationGroup="Generate"
                                        SetFocusOnError="true"
                                        >
                                        </asp:CompareValidator>  
                                   <ajaxToolkit:ValidatorCalloutExtender ID="dateCompareValidatorExtender" runat="server" 
                                        BehaviorID="dateCompareValidatorExtender" 
                                        TargetControlID="dateCompareValidator" 
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175" />  
                                   <p>(Note: Dates are not used for scheduled reports.)</p>                                                                 
                                </div>

                                <hr />
                                <h2>Bureau Previous Report Period</h2>
                                <p>
                                    Previous report comparison is most accurate when dates are accross the same interval. If this is the first report, or you would not like to do a comparison, enter the same dates as the current report period.
                                </p>
                                <div class="divForm">    
                                    <label class="label">*Previous Start Date:</label>
                                    <telerik:RadDatePicker ID="txtPreviousStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                                         
                                    <asp:RequiredFieldValidator ID="datePreviousStartRequiredValidator" runat="server"
                                                    CssClass="errorMessage" 
                                                    ErrorMessage="Date is a required field."
                                                    ControlToValidate="txtPreviousStartDate"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="Generate">
                                                    </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="datePreviousStartRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="datePreviousStartRequiredValidatorExtender" 
                                        TargetControlID="datePreviousStartRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                               </div>
                               <div class="divForm">
                                    <label class="label">*Previous End Date:</label>  
                                    <telerik:RadDatePicker ID="txtPreviousEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                                               
                                    <asp:RequiredFieldValidator ID="datePreviousEndRequiredValidator" runat="server"
                                                    CssClass="errorMessage" 
                                                    ErrorMessage="Date is a required field."
                                                    ControlToValidate="txtPreviousEndDate"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="Generate">
                                                    </asp:RequiredFieldValidator>       
                                   <ajaxToolkit:ValidatorCalloutExtender ID="datePreviousEndRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="datePreviousEndRequiredValidatorExtender" 
                                        TargetControlID="datePreviousEndRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                               
                                    <asp:CompareValidator ID="previousDateCompareValidator" Display="None" runat="server"
                                        CssClass="errorMessage"
                                        ErrorMessage="Invalid range."
                                        ControlToValidate="txtPreviousEndDate"
                                        ControlToCompare="txtPreviousStartDate"
                                        Type="Date"
                                        Operator="GreaterThanEqual"
                                        ValidationGroup="Generate"
                                        SetFocusOnError="true"
                                        >
                                        </asp:CompareValidator> 
                                   <ajaxToolkit:ValidatorCalloutExtender ID="previousDateCompareValidatorExtender" runat="server" 
                                        BehaviorID="previousDateCompareValidatorExtender" 
                                        TargetControlID="previousDateCompareValidator" 
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175" />    
                                   <p>(Note: Dates are not used for scheduled reports.)</p>                                                                                                                                                                                         
                                </div>
                                <!--<div class="divForm">
                                        <label class="label">*Comparison Threshold Percent:</label>
                                        <asp:DropDownList CssClass="dropdown" ID="ddlThreshold" runat="server" >  
                                            <asp:ListItem Value="0" Selected="True">0%</asp:ListItem>
                                            <asp:ListItem Value="1">1%</asp:ListItem>
                                            <asp:ListItem Value="2">2%</asp:ListItem>
                                            <asp:ListItem Value="3">3%</asp:ListItem>
                                            <asp:ListItem Value="4">4%</asp:ListItem>
                                            <asp:ListItem Value="5">5%</asp:ListItem>
                                            <asp:ListItem Value="6">6%</asp:ListItem>
                                            <asp:ListItem Value="7">7%</asp:ListItem>
                                            <asp:ListItem Value="8">8%</asp:ListItem>
                                            <asp:ListItem Value="9">9%</asp:ListItem>
                                            <asp:ListItem Value="10">10%</asp:ListItem>
                                        </asp:DropDownList>
                                </div>-->
                                <hr />
                                <h2>Bureau Settings</h2>
                                <!--<div id="divClient" class="divForm" runat="server">   
                                    <label class="label">*Client:</label>    
                                    OnSelectedIndexChanged="ddlClient_OnSelectedIndexChanged"
                                    <asp:DropDownList ID="ddlClient" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server">
                                            <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                    </asp:DropDownList> 
                                </div>--> 
                                <div class="divForm">                                                        
                                        <label class="label">*Buildings:</label>                                                                                                                                         
                                        <asp:ListBox ID="lbBuildings" CssClass="listbox" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                        <asp:CustomValidator ID="buildingsCustomValidator" runat="server"  ErrorMessage="Buildings is a required field." ControlToValidate="lbBuildings" SetFocusOnError="true" ValidationGroup="Common" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None" ></asp:CustomValidator> 
                                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingsCustomValidatorExtender" runat="server" BehaviorID="buildingsCustomValidatorExtender" TargetControlID="buildingsCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                </div>
                                <div class="divForm">
                                        <label class="label">*Theme:</label>
                                        <asp:DropDownList CssClass="dropdown" ID="ddlTheme" runat="server">  
                                        </asp:DropDownList>
                                </div>
                                <div class="divForm">
                                    <label class="label">*Language:</label>
                                    <asp:DropDownList ID="ddlLanguage" CssClass="dropdown" OnSelectedIndexChanged="ddlLanguage_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
                                </div>
                                <div class="divForm">
                                    <label class="label">*Culture Format:</label>
                                    <asp:DropDownList ID="ddlCulture" CssClass="dropdown" runat="server" />
                                </div>
                                <div class="divForm">
                                    <label class="label">Show Client Logos:</label>
                                    <asp:CheckBox ID="chkShowClientLogos" CssClass="checkbox" Checked="true" runat="server" />                                                                                        
                                </div> 
                                <div class="divForm">
                                    <label class="label">Include Cover Page:</label>
                                    <asp:CheckBox ID="chkIncludeCoverPage" CssClass="checkbox" runat="server" />                                                                                      
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Table Of Contents Page:</label>
                                    <asp:CheckBox ID="chkIncludeContentsPage" CssClass="checkbox" runat="server" />                                                                                      
                                </div>        
                                <div class="divForm">
                                    <label class="label">Include Expert Summary Page And Inputs:</label>
                                    <asp:CheckBox ID="chkIncludeExpertSummary" CssClass="checkbox" runat="server" />  
                                    <p>(Note: Expert summary page and inputs are not included in scheduled reports.)</p>                                                                                       
                                </div> 
                                <div class="divForm">
                                    <label class="label">Include Building Summary Report:</label>
                                    <asp:CheckBox ID="chkIncludeBuildingSummaryReport" CssClass="checkbox" Checked="true" runat="server" />                                                                                      
                                </div> 
                                <div class="divForm">
                                    <label class="label">Include Equipment Class Summary Report:</label>
                                    <asp:CheckBox ID="chkIncludeEquipmentClassSummaryReport" CssClass="checkbox" Checked="true" runat="server" />                                                                                      
                                </div> 
                                <div class="divForm">
                                    <label class="label">Include Building Trends Summary Report:</label>
                                    <asp:CheckBox ID="chkIncludeBuildingTrendsSummaryReport" CssClass="checkbox" Checked="true" runat="server" />                                                                                      
                                </div> 
                                <div class="divForm">
                                    <label class="label">Include Top Diagnostic Issues Summary Report:</label>
                                    <asp:CheckBox ID="chkIncludeBuildingTopIssuesSummaryReport" CssClass="checkbox" Checked="true" runat="server" />                                                                                      
                                </div> 
                                <div class="divForm">
                                    <label class="label">Include Ventilation Equipment Report:</label>
                                    <asp:CheckBox ID="chkIncludeVentilationEquipmentReport" CssClass="checkbox" Checked="true" runat="server" /> 
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">Top:</label>
                                    <asp:DropDownList ID="ddlTopVentilation" CssClass="dropdownNarrower" runat="server">
                                        <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                        <asp:ListItem Value="50" Text="50" selected="True"></asp:ListItem>  
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>  
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem>     
                                    </asp:DropDownList>                                                                                                             
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Heating Equipment Report:</label>
                                    <asp:CheckBox ID="chkIncludeHeatingEquipmentReport" CssClass="checkbox" Checked="true" runat="server" />
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">Top:</label>
                                    <asp:DropDownList ID="ddlTopHeating" CssClass="dropdownNarrower" runat="server">
                                        <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                        <asp:ListItem Value="50" Text="50" selected="True"></asp:ListItem>  
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>   
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem> 
                                    </asp:DropDownList>  
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Cooling Equipment Report:</label>
                                    <asp:CheckBox ID="chkIncludeCoolingEquipmentReport" CssClass="checkbox" Checked="true" runat="server" />           
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">Top:</label>
                                    <asp:DropDownList ID="ddlTopCooling" CssClass="dropdownNarrower" runat="server">
                                        <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                        <asp:ListItem Value="50" Text="50" selected="True"></asp:ListItem>  
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>   
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem> 
                                    </asp:DropDownList>                                                                             
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Plant Equipment Report:</label>
                                    <asp:CheckBox ID="chkIncludePlantEquipmentReport" CssClass="checkbox" Checked="true" runat="server" />  
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">Top:</label>
                                    <asp:DropDownList ID="ddlTopPlant" CssClass="dropdownNarrower" runat="server">
                                        <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                        <asp:ListItem Value="50" Text="50" selected="True"></asp:ListItem>  
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>  
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem>  
                                    </asp:DropDownList>                                                                                      
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Zone Equipment Report:</label>
                                    <asp:CheckBox ID="chkIncludeZoneEquipmentReport" CssClass="checkbox" Checked="true" runat="server" />  
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">Top:</label>
                                    <asp:DropDownList ID="ddlTopZone" CssClass="dropdownNarrower" runat="server">                            
							            <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                        <asp:ListItem Value="50" Text="50" selected="True"></asp:ListItem>  
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>   
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem> 
                                    </asp:DropDownList>                                                                                      
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Specialty Equipment Report:</label>
                                    <asp:CheckBox ID="chkIncludeSpecialtyEquipmentReport" CssClass="checkbox" runat="server" />   
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">Top:</label>
                                    <asp:DropDownList ID="ddlTopSpecialty" CssClass="dropdownNarrower" runat="server">
                                        <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                        <asp:ListItem Value="50" Text="50" selected="True"></asp:ListItem>  
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem>    
                                    </asp:DropDownList>                                                                                     
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Performance Report (Requires Perf. Module):</label>
                                    <asp:CheckBox ID="chkIncludePerformanceReport" CssClass="checkbox" Enabled="false" runat="server" />                                                                                      
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Alarm Report (Requires Alarms Module):</label>
                                    <asp:CheckBox ID="chkIncludeAlarmReport" CssClass="checkbox" Enabled="false" runat="server" />                                                                                      
                                </div>
                                <div class="divForm">
                                    <label class="label">Include Projects Report (Requires Projects Module):</label>
                                    <asp:CheckBox ID="chkIncludeProjectReport" CssClass="checkbox" Enabled="false" runat="server" />    
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">Top:</label>
                                    <asp:DropDownList ID="ddlTopProjects" CssClass="dropdownNarrower" runat="server">
                                        <asp:ListItem Value="5" Text="5"></asp:ListItem>  
                                        <asp:ListItem Value="10" Text="10" selected="True"></asp:ListItem>  
                                        <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                        <asp:ListItem Value="50" Text="50"></asp:ListItem>    
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                    </asp:DropDownList>                                                                                  
                                </div> 
                                <div class="divForm">
                                    <label class="label">Include Tasks Report (Requires Tasks Module):</label>
                                    <asp:CheckBox ID="chkIncludeTaskReport" CssClass="checkbox" Enabled="false" runat="server" />    
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">Top:</label>
                                    <asp:DropDownList ID="ddlTopTasks" CssClass="dropdownNarrower" runat="server">
                                        <asp:ListItem Value="5" Text="5"></asp:ListItem>  
                                        <asp:ListItem Value="10" Text="10"></asp:ListItem>  
                                        <asp:ListItem Value="25" Text="25" selected="True"></asp:ListItem>
                                        <asp:ListItem Value="50" Text="50"></asp:ListItem>    
                                        <asp:ListItem Value="100" Text="100"></asp:ListItem>
                                        <asp:ListItem Value="500" Text="500"></asp:ListItem>
                                    </asp:DropDownList>                                                                                  
                                </div> 
                                <div class="divForm">
                                    <label class="label">Include Top Recent Weekly Diagnostic Issues Details:</label>
                                    <input class="checkbox" type="checkbox" id="chkIncludeTopDiagnosticsDetails" onclick="chkIncludeTopDiagnosticsDetials_OnClick();" runat="server" />  
                                    &nbsp;&nbsp;|&nbsp;&nbsp;
                                    <label class="labelLeft">And Figures:</label>
                                    <input class="checkbox" type="checkbox" id="chkIncludeTopDiagnosticsFigures" disabled="disabled" runat="server" />                                                                                                               
                                </div> 
                                <!--<div class="divForm">
                                    <label class="label">Include Hidden Issue:</label>
                                    <asp:CheckBox ID="chkIncludeHiddenIssues" CssClass="checkbox" runat="server" />                                                                                      
                                </div>-->
                    

                                <hr />
                                <h2>Bureau Content</h2>
                                <div class="richText">
                                    Custom token variables are available for the rich text editors. They include the following:
                                    <ul>
                                        <li>Company Name (KGS Buildings or Schneider Electric) = $company</li>
                                        <li>Company Abbreviation (KGS or SE) = $companyShort</li>
                                        <li>Product Name (Clockworks or Building Analytics) = $product</li>
                                        <li>Product Abbreviation (CW or BA) = $productShort</li>
                                        <li>Start Date = $startDate</li>
                                        <li>End Date = $endDate</li>
                                        <li>Previous Start Date = $previousStartDate</li>
                                        <li>Previous End Date = $previousEndDate</li>
                                        <li>Total Avoidable Cost Percentage Changed = $totalCostPercent</li>
                                        <li>Previous Total Avoidable Cost = $previousCost</li>                                 
                                        <li>Interval Average Avoidable Cost = $intervalCost</li>                           
                                        <li>Interval Average Avoidable Cost Percentage Changed = $intervalCostPercent</li>                     
                                        <li>Interval Average Energy Incidents = $intervalEnergyIncidents</li>
                                        <li>Interval Average Energy Priority Precentage Changed = $intervalEnergyPercent</li>                           
                                        <li>Interval Average Maintenance Incidents = $intervalMaintenanceIncidents</li>
                                        <li>Interval Average Maintenance Priority Precentage Changed = $intervalMaintenancePercent</li>                                                
                                        <li>Interval Average Comfort Incidents = $intervalComfortIncidents</li>  
                                        <li>Interval Average Comfort Priority Precentage Changed = $intervalComfortPercent</li>      
                                    </ul>
                                 </div>
                                <div class="divForm">
                                    <label class="label">Title:</label>
                                    <asp:TextBox  ID="txtUserTitle" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                </div>
                                <div class="divFormLeftmost">
                                    <label class="labelLeft">Summary (Max HTML Characters = 10000):</label>
                                    <telerik:RadEditor ID="editorAutomatedSummary"
                                                runat="server"                                                                                                                                                
                                                CssClass="editorNewLine"                                                        
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="10000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                                                > 
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                                                             
                                    </telerik:RadEditor>                                 
                                </div>
                                <div class="divFormLeftmost">
                                    <label class="labelLeft">
                                    Expert Summary (Max HTML Characters = 50000):</label>
                                    <telerik:RadEditor ID="editorExpertSummary" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="50000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                    </telerik:RadEditor>
                                </div>
                                             
                                <div class="divFormLeftmost">
                                    <label class="labelLeft">
                                    Expert Energy Cost Trend Analysis (Max HTML Characters = 500):</label>
                                    <telerik:RadEditor ID="editorEnergyTrend"
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="175px" 
                                                Width="674px"
                                                MaxHtmlLength="500"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                    </telerik:RadEditor>
                                </div>
                                <div class="divFormLeftmost">
                                    <label class="labelLeft">
                                    Expert Maintenance Trend Analysis (Max HTML Characters = 500):</label>
                                    <telerik:RadEditor ID="editorMaintenanceTrend" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="175px" 
                                                Width="674px"
                                                MaxHtmlLength="500"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                                > 
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                  
                                    </telerik:RadEditor>
                                </div>
                                <div class="divFormLeftmost">
                                    <label class="labelLeft">
                                    Expert Comfort Trend Analysis (Max HTML Characters = 500):</label>
                                    <telerik:RadEditor ID="editorComfortTrend" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="175px" 
                                                Width="674px"
                                                MaxHtmlLength="500"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                    </telerik:RadEditor>
                                </div>
          
                                <hr />
                                <h2>Generate or Save/Schedule This Report</h2>
                                <div class="richText">
                                    You may generate this report now to provide expert opinions, alter content, and download as a pdf.
                                    Alternatively you can save and schedule this report to run on an automated reoccuring basis.
                                </div>

                                <table style="width: 100%;">
                                    <tr>
                                        <td style="width: 50%;">
                                            <br />
                                            <div class="divFormTwoColumn">
                                                <label class="labelTwoColumn">Generate Report Now:</label>
                                                <asp:LinkButton CssClass="lnkButton" ID="btnGenerate" runat="server" Text="Generate Now" PostBackUrl="~/BureauReportv2.aspx" OnClientClick="ValidateMultipleValidationGroupsGenerate(); window.document.forms[0].target='_blank';"></asp:LinkButton>
                                            </div>
                                        </td>
                                        <td style="width: 50%; border-left-style: solid; border-left-width: 2px; border-left-color: #eaeaea;">
                                            <div class="divFormTwoColumn">
                                                <label class="labelTwoColumn">*Report Name:</label>
                                                <asp:TextBox ID="txtReportName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                            </div>
                                            <div class="divFormTwoColumn">
                                                <label class="labelTwoColumn">Description:</label>
                                                <textarea name="txtDescription" id="txtDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divDescriptionCharInfo')" runat="server"></textarea>
                                                <div id="divDescriptionCharInfo"></div>                                                         
                                            </div>                                            
                                            <div class="divFormTwoColumn">
                                                <label class="labelTwoColumn">*Run Monthly:</label>
                                                <asp:CheckBox ID="chkRunMonthly" CssClass="checkbox" runat="server" />            
                                                <p>(Note: Monthly is the 1st of every month.)</p>                                                                                                
                                            </div>
                                            <div class="divFormTwoColumn">
                                                <label class="labelTwoColumn">*Run Quarterly:</label>
                                                <asp:CheckBox ID="chkRunQuarterly" CssClass="checkbox" runat="server" />                                                                                                            
                                                <p>(Note: Quarterly is the 1st of every yearly calander quarter.)</p>                                                                                                
                                            </div>
                                            <div class="divFormTwoColumn">
                                                <label class="labelTwoColumn">*Compare Previous:</label>
                                                <asp:CheckBox ID="chkComparePrevious" CssClass="checkbox" runat="server" />   
                                                <p>(Note: Previous comparison is the previous month or quarter.)</p>                                                                                                           
                                            </div>  
                                            <div class="divFormTwoColumn">                                                                                                                               
                                                <div>                                       
                                                    <label class="labelTwoColumn">Available User Groups:</label> 
                                                    <asp:ListBox ID="lbUserGroupsTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                    </asp:ListBox>
                                                    <div class="divArrows">
                                                    <asp:ImageButton CssClass="up-arrow" PostBackUrl="~/BureauReportsv2.aspx" ID="btnUp"  OnClick="btnUserGroupsUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                    <asp:ImageButton CssClass="down-arrow" PostBackUrl="~/BureauReportsv2.aspx" ID="btnDown"  OnClick="btnUserGroupsDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                    </div>
                                                    <label class="labelTwoColumn">Assigned User Groups:</label>
                                                    <asp:ListBox ID="lbUserGroupsBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                    </asp:ListBox> 
                                                </div>   
                                                <a target="_blank" href="/<%=PathRewriteModule.UserGroupAdministration%>">Create User Groups</a>                                               
                                            </div>                                          
                                            <br />
                                            <div class="divFormTwoColumn">
                                               <label class="labelTwoColumn">Save and Schedule:</label>
                                               <asp:LinkButton CssClass="lnkButton" PostBackUrl="~/BureauReportsv2.aspx" ID="btnAddBureauReport" runat="server" Text="Schedule Report" OnClick="addBureauReportButton_Click" OnClientClick="return ValidateMultipleValidationGroupsScheduled()"></asp:LinkButton>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                                <!--Ajax Validators-->      
                                <asp:RequiredFieldValidator ID="addReportNameRequiredValidator" runat="server"
                                    ErrorMessage="Report Name is a required field."
                                    ControlToValidate="txtReportName"          
                                    SetFocusOnError="true"
                                    Display="None"
                                    ValidationGroup="AddBureauReport">
                                    </asp:RequiredFieldValidator>
                                <ajaxToolkit:ValidatorCalloutExtender ID="addReportNameRequiredValidatorExtender" runat="server"
                                    BehaviorID="addReportNameRequiredValidatorExtender"
                                    TargetControlID="addReportNameRequiredValidator"
                                    HighlightCssClass="validatorCalloutHighlight"
                                    Width="175">
                                    </ajaxToolkit:ValidatorCalloutExtender>
                                
                        </telerik:RadPageView>                                              
                        <telerik:RadPageView ID="RadPageView2" runat="server">   

                                <h2>View Scheduled Bureau Reports</h2>                                                                                                                                   
                                <p>
                                    <a id="lnkSetFocusView" href="#" runat="server"></a>
                                    <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblErrors" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>                                            
                                </p>   
                                   
                                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>      
                                                                                                                
                                <div id="gridTbl">                                        
                                        <asp:GridView 
                                            ID="gridBureauReports"   
                                            EnableViewState="true"           
                                            runat="server"  
                                            DataKeyNames="SBRID"  
                                            GridLines="None"                                      
                                            PageSize="20" PagerSettings-PageButtonCount="20"
                                            OnRowCreated="gridBureauReports_OnRowCreated" 
                                            AllowPaging="true"  OnPageIndexChanging="gridBureauReports_PageIndexChanging"
                                            AllowSorting="true"  OnSorting="gridBureauReports_Sorting"   
                                            OnSelectedIndexChanged="gridBureauReports_OnSelectedIndexChanged"                                                                                                             
                                            OnRowDeleting="gridBureauReports_Deleting"
                                            OnRowEditing="gridBureauReports_Editing"
                                            OnDataBound="gridBureauReports_OnDataBound"
                                            AutoGenerateColumns="false"                                              
                                            HeaderStyle-CssClass="tblTitle" 
                                            RowStyle-CssClass="tblCol1"
                                            AlternatingRowStyle-CssClass="tblCol2"   
                                            RowStyle-Wrap="true"                                                                                   
                                            > 
                                            <Columns>
                                            <asp:CommandField ShowSelectButton="true" />
                                            <asp:TemplateField>
                                                <ItemTemplate>                                                
                                                        <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                                                                    
                                            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ReportName" HeaderText="Report Name">  
                                                <ItemTemplate><%# Eval("ReportName") %></ItemTemplate>
                                            </asp:TemplateField> 
                                            <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Client">  
                                                <ItemTemplate><%# Eval("ClientName") %></ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Buildings">  
                                                <ItemTemplate><%# StringHelper.ParseStringArrayToCSVString((string[])Eval("BuildingNames")) %></ItemTemplate>
                                            </asp:TemplateField>   
                                            <asp:TemplateField SortExpression="RunMonthly" HeaderText="Run Monthly">  
                                                <ItemTemplate><%# Eval("RunMonthly") %></ItemTemplate>
                                            </asp:TemplateField>  
                                            <asp:TemplateField SortExpression="RunQuarterly" HeaderText="Run Quarterly">                                                     
                                                <ItemTemplate><%# Eval("RunQuarterly") %></ItemTemplate>
                                            </asp:TemplateField>    
                                            <asp:TemplateField SortExpression="CompareToPreviousPeriod" HeaderText="Previous Comparison">                                                     
                                                <ItemTemplate><%# Eval("CompareToPreviousPeriod") %></ItemTemplate>
                                            </asp:TemplateField>                                                
                                            <asp:TemplateField HeaderText="Language">                                                     
                                                <ItemTemplate><%# CultureHelper.GetCultureDisplayName(CultureHelper.GetCultureInfo(Eval("LanguageCultureName").ToString()).LCID) %></ItemTemplate>
                                            </asp:TemplateField>                                                  
                                            <asp:TemplateField SortExpression="DateCreated" ItemStyle-Wrap="true" HeaderText="Start Date">  
                                                <ItemTemplate><%# Eval("DateCreated")%></ItemTemplate>                                                  
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>                                                
                                                        <asp:LinkButton ID="LinkButton2" Runat="server" CausesValidation="false"
                                                            OnClientClick="return confirm('Are you sure you wish to delete this scheduled report permanently?\n\nAlternatively you can deactivate run monthly or quarterly.');"
                                                            CommandName="Delete">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                                                                                                                                          
                                            </Columns>        
                                        </asp:GridView> 
                                                                          
                                </div>                                    
                                <br /><br /> 
                                <div>                                                            
                                <!--SELECT BUREAU REPORT DETAILS VIEW -->
                                <asp:DetailsView ID="dtvBureauReport" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                    <Fields>
                                        <asp:TemplateField  
                                        ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                        ItemStyle-BorderWidth="0" 
                                        ItemStyle-BorderColor="White" 
                                        ItemStyle-BorderStyle="none"
                                        ItemStyle-Width="100%"
                                        HeaderStyle-Width="1" 
                                        HeaderStyle-BorderWidth="0" 
                                        HeaderStyle-BorderColor="White" 
                                        HeaderStyle-BorderStyle="none"
                                        >
                                            <ItemTemplate>
                                                <div>
                                                    <h2>Bureau Report Details</h2>                                                                                                                                            
                                                    <ul class="detailsListWide">
                                                        <%# "<li><strong>Report Name: </strong>" + Eval("ReportName") + "</li>"%> 
                                                        <%# String.IsNullOrEmpty(Convert.ToString(Eval("ReportDescription"))) ? "" : "<li><strong>Report Description: </strong>" + Eval("ReportDescription") + "</li>"%>
                                                        <%# "<li><strong>Client: </strong>" + Eval("ClientName") + "</li>"%> 
                                                        <%# ((string[])Eval("BuildingNames")).Count() == 0 ? "" : "<li><strong>Buildings: </strong>" + StringHelper.ParseStringArrayToCSVString((string[])Eval("BuildingNames")) + "</li>"%>
                                                        <%# String.IsNullOrEmpty(Convert.ToString(Eval("PreparedByFullName"))) ? "" : "<li><strong>Prepared By: </strong>" + Eval("PreparedByFullName") + "</li>"%>                                                          
                                                        <%# "<li><strong>Run Monthly: </strong>" + Eval("RunMonthly") + "</li>"%>
                                                        <%# "<li><strong>Run Quarterly: </strong>" + Eval("RunQuarterly") + "</li>"%>                                                        
                                                        <%# "<li><strong>Previous Comparison: </strong>" + Eval("CompareToPreviousPeriod") + "</li>"%>                                                         
                                                        <%# String.IsNullOrEmpty(Convert.ToString(Eval("Title"))) ? "" : "<li><strong>Title: </strong>" + Eval("Title") + "</li>"%> 
                                                        <%# "<li><strong>Theme: </strong>" + BusinessConstants.Theme.ReportThemes.Where(t => t.Key == Convert.ToInt32(Eval("Theme").ToString())).FirstOrDefault().Value + "</li>"%>   
                                                        <%# "<li><strong>Language: </strong>" + CultureHelper.GetCultureDisplayName(CultureHelper.GetCultureInfo(Eval("LanguageCultureName").ToString()).LCID) + "</li>"%>  
                                                        <%# "<li><strong>Culture Format: </strong>" + CultureHelper.GetCultureDisplayName(Convert.ToInt32(Eval("PageLCID"))) + "</li>"%>                                                           
                                                        <%# "<li><strong>Show Client Logos: </strong>" + Eval("ShowClientsLogos") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Cover Page: </strong>" + Eval("IncludeCoverPage") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Table Of Contents: </strong>" + Eval("IncludeTableOfContents") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Building Summary Report: </strong>" + Eval("IncludeBuildingSummaryReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Equipment Class Summary Report: </strong>" + Eval("IncludeEquipmentClassSummaryReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Building Trends Summary Report: </strong>" + Eval("IncludeBuildingTrendsSummaryReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Diagnostic Issues Summary Report: </strong>" + Eval("IncludeDiagnosticIssuesSummaryReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Ventilation Report: </strong>" + Eval("IncludeVentilationReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Top Ventilation: </strong>" + Eval("TopVentilation") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Heating Report: </strong>" + Eval("IncludeHeatingReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Top Heating: </strong>" + Eval("TopHeating") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Cooling Report: </strong>" + Eval("IncludeCoolingReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Top Cooling: </strong>" + Eval("TopCooling") + "</li>"%> 
                                                        <%# "<li><strong>Include Plant Report: </strong>" + Eval("IncludePlantReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Top Plant: </strong>" + Eval("TopPlant") + "</li>"%> 
                                                        <%# "<li><strong>Include Zone Report: </strong>" + Eval("IncludeZoneReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Top Zone: </strong>" + Eval("TopZone") + "</li>"%> 
                                                        <%# "<li><strong>Include Specialty Report: </strong>" + Eval("IncludeSpecialtyReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Top Specialty: </strong>" + Eval("TopSpecialty") + "</li>"%> 
                                                        <%# "<li><strong>Include Performance Report: </strong>" + Eval("IncludePerformanceReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Alarm Report: </strong>" + Eval("IncludeAlarmReport") + "</li>"%> 
                                                        <%# "<li><strong>Include Projects Report: </strong>" + Eval("IncludeProjectsReport") + "</li>"%>                                                             
                                                        <%# "<li><strong>Top Projects: </strong>" + Eval("TopProjects") + "</li>"%> 
                                                        <%# "<li><strong>Include Tasks Report: </strong>" + Eval("IncludeTasksReport") + "</li>"%>
                                                        <%# "<li><strong>Top Tasks: </strong>" + Eval("TopTasks") + "</li>"%> 
                                                        <%# "<li><strong>Include Top Weekly Diagnostic Issues: </strong>" + Eval("IncludeTopWeeklyDiagnosticIssues") + "</li>"%>                                                             
                                                        <%# "<li><strong>Include Top Weekly Diagnostic Figures: </strong>" + Eval("IncludeTopWeeklyDiagnosticFigures") + "</li>"%> 
                                                        <%# "<li><strong>Start Date: </strong>" + Eval("DateCreated") + "</li>"%>                                                             
                                                     </ul>                                                        
                                                </div>
                                            </ItemTemplate>                                             
                                        </asp:TemplateField>
                                    </Fields>
                                </asp:DetailsView>   
                                    
                                </div>                                     
                                                                     
                                <!--EDIT BUREAU REPORT PANEL -->                              
                                <asp:Panel ID="pnlEditBureauReport" runat="server" Visible="false" DefaultButton="btnUpdateBureauReport">                                                                                             
                                                                                                                                                                                                           
                                        <div>
                                            <h2>Edit Bureau Report</h2>
                                        </div>  
                                        <div> 
                                            <a id="lnkSetFocusEdit" runat="server"></a>                                                     
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Report Name:</label>
                                                <asp:TextBox ID="txtEditReportName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                            </div>
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" runat="server"></textarea>
                                                <div id="divEditDescriptionCharInfo"></div>                                                         
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Client:</label>
                                                <asp:Label CssClass="labelContent" ID="lblEditClient" runat="server"></asp:Label>                                                        
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Buildings:</label>                                                                                                                                     
                                                <asp:ListBox ID="lbEditBuildings" CssClass="listbox" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                                <asp:CustomValidator ID="editBuildingsCustomValidator" runat="server"  ErrorMessage="Buildings is a required field." ControlToValidate="lbEditBuildings" SetFocusOnError="true" ValidationGroup="EditBureauReport" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None" ></asp:CustomValidator> 
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingsCustomValidatorExtender" runat="server" BehaviorID="editBuildingsCustomValidatorExtender" TargetControlID="editBuildingsCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                            </div>
                                            <div class="divForm">
                                                <label class="label">Prepared By:</label>
                                                <asp:Label ID="lblEditPreparedByFullName" CssClass="labelContent" MaxLength="100" runat="server"></asp:Label>                                                    
                                                <p>(Note: Updates as current user.)</p>  
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Run Monthly:</label>
                                                <asp:CheckBox ID="chkEditRunMonthly" CssClass="checkbox" runat="server" />      
                                                <p>(Note: Monthly is the 1st of every month.)</p>                                                                                                        
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Run Quarterly:</label>
                                                <asp:CheckBox ID="chkEditRunQuarterly" CssClass="checkbox" runat="server" />     
                                                <p>(Note: Quarterly is the 1st of every yearly calander quarter.)</p>                                                                                                        
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Compare Previous:</label>
                                                <asp:CheckBox ID="chkEditComparePrevious" CssClass="checkbox" runat="server" /> 
                                                <p>(Note: Previous comparison is the previous month or quarter.)</p>                                                                                                            
                                            </div>
                                            <div class="divForm">                                                                                                                               
                                                <div>                                       
                                                    <label class="label">Available User Groups:</label> 
                                                    <asp:ListBox ID="lbEditUserGroupsTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                    </asp:ListBox>
                                                    <div class="divArrows">
                                                    <asp:ImageButton CssClass="up-arrow"  ID="ImageButton1"  OnClick="btnEditUserGroupsUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                    <asp:ImageButton CssClass="down-arrow" ID="ImageButton2"  OnClick="btnEditUserGroupsDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                    </div>
                                                    <label class="label">Assigned User Groups:</label>
                                                    <asp:ListBox ID="lbEditUserGroupsBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                    </asp:ListBox> 
                                                </div>   
                                                <a target="_blank" href="/<%=PathRewriteModule.UserGroupAdministration%>">Create User Groups</a>
                                                <asp:HiddenField ID="hdnEditGUID" runat="server" Visible="false" />
                                            </div>
                                            <div class="divForm">
                                                <label class="label">Title:</label>
                                                <asp:TextBox ID="txtEditTitle" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                                                                        
                                            </div>                                                                                                                                                                                                                                                                                           
                                            <div class="divForm">   
                                                <label class="label">*Theme:</label>                                              
                                                <asp:DropDownList CssClass="dropdown" ID="ddlEditTheme" runat="server">  
                                                </asp:DropDownList>
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Language:</label>
                                                <asp:DropDownList ID="ddlEditLanguage" CssClass="dropdown" runat="server" />
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Culture Format:</label>
                                                <asp:DropDownList ID="ddlEditCulture" CssClass="dropdown" runat="server" />
                                            </div>                                                                                                                                                                                                    
                                            <div class="divForm">
                                                <label class="label">*Show Client Logos:</label>
                                                <asp:CheckBox ID="chkEditShowClientLogos" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Cover Page:</label>
                                                <asp:CheckBox ID="chkEditIncludeCoverPage" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Table Of Contents:</label>
                                                <asp:CheckBox ID="chkEditIncludeContentsPage" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Building Summary Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeBuildingSummaryReport" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Equipment Class Summary Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeEquipmentClassSummaryReport" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Building Trends Summary Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeBuildingTrendsSummaryReport" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Top Diagnostic Issues Summary Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeBuildingTopIssuesSummaryReport" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Ventilation Equipment Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeVentilationEquipmentReport" CssClass="checkbox" runat="server" />                                                                                                            
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">Top:</label>   
                                                <asp:DropDownList ID="ddlEditTopVentilation" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>  
                                                    <asp:ListItem Value="100" Text="100"></asp:ListItem>  
                                                    <asp:ListItem Value="500" Text="500"></asp:ListItem>   
                                                </asp:DropDownList>
                                            </div>                                                                                                                                 
                                                <div class="divForm">
                                                <label class="label">*Include Heating Equipment Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeHeatingEquipmentReport" CssClass="checkbox" runat="server" />                                                                                                            
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">Top:</label>  
                                                <asp:DropDownList ID="ddlEditTopHeating" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>  
                                                    <asp:ListItem Value="100" Text="100"></asp:ListItem>  
                                                    <asp:ListItem Value="500" Text="500"></asp:ListItem>   
                                                </asp:DropDownList>
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Include Cooling Equipment Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeCoolingEquipmentReport" CssClass="checkbox" runat="server" />                                                                                                            
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">Top:</label>     
                                                <asp:DropDownList ID="ddlEditTopCooling" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>  
                                                    <asp:ListItem Value="100" Text="100"></asp:ListItem>  
                                                    <asp:ListItem Value="500" Text="500"></asp:ListItem>    
                                                </asp:DropDownList>
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Include Plant Equipment Report:</label>
                                                <asp:CheckBox ID="chkEditIncludePlantEquipmentReport" CssClass="checkbox" runat="server" />                                                                                                            
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">Top:</label>    
                                                <asp:DropDownList ID="ddlEditTopPlant" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>  
                                                    <asp:ListItem Value="100" Text="100"></asp:ListItem>  
                                                    <asp:ListItem Value="500" Text="500"></asp:ListItem>    
                                                </asp:DropDownList>
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Include Zone Equipment Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeZoneEquipmentReport" CssClass="checkbox" runat="server" />                                                                                                            
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">Top:</label>  
                                                <asp:DropDownList ID="ddlEditTopZone" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>  
                                                    <asp:ListItem Value="100" Text="100"></asp:ListItem>  
                                                    <asp:ListItem Value="500" Text="500"></asp:ListItem>    
                                                </asp:DropDownList>
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Include Specialty Equipment Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeSpecialtyEquipmentReport" CssClass="checkbox" runat="server" />                                                                                                            
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">Top:</label>  
                                                <asp:DropDownList ID="ddlEditTopSpecialty" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>  
                                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>  
                                                    <asp:ListItem Value="100" Text="100"></asp:ListItem>  
                                                    <asp:ListItem Value="500" Text="500"></asp:ListItem>    
                                                </asp:DropDownList>
                                            </div>    
                                            <div class="divForm">
                                                <label class="label">*Include Performance Report:</label>
                                                <asp:CheckBox ID="chkEditIncludePerformanceReport" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Alarm Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeAlarmReport" CssClass="checkbox" runat="server" />                                                                                                            
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Include Pojects Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeProjectsReport" CssClass="checkbox" runat="server" />                                                                                                            
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">Top:</label>  
                                                <asp:DropDownList ID="ddlEditTopProjects" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>  
                                                    <asp:ListItem Value="10" Text="10" selected="True"></asp:ListItem>  
                                                    <asp:ListItem Value="25" Text="25"></asp:ListItem>
                                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>    
                                                    <asp:ListItem Value="100" Text="100"></asp:ListItem> 
                                                </asp:DropDownList>
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Include Tasks Report:</label>
                                                <asp:CheckBox ID="chkEditIncludeTasksReport" CssClass="checkbox" runat="server" />                                                                                                            
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">Top:</label>  
                                                <asp:DropDownList ID="ddlEditTopTasks" CssClass="dropdownNarrower" runat="server">
                                                    <asp:ListItem Value="5" Text="5"></asp:ListItem>  
                                                    <asp:ListItem Value="10" Text="10"></asp:ListItem>  
                                                    <asp:ListItem Value="25" Text="25" selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="50" Text="50"></asp:ListItem>    
                                                    <asp:ListItem Value="100" Text="100"></asp:ListItem> 
                                                </asp:DropDownList>
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Include Top Recent Weekly Diagnostic Issues Detials:</label>
                                                <input class="checkbox" type="checkbox" id="chkEditIncludeTopDiagnosticsDetails" onclick="chkEditIncludeTopDiagnosticsDetials_OnClick();" runat="server" />                                                           
                                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                                <label class="labelLeft">And Figures:</label>
                                                <input class="checkbox" type="checkbox" id="chkEditIncludeTopDiagnosticsFigures" disabled="disabled" runat="server" />
                                            </div> 
                                                                                                                                         
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBureauReport" runat="server" Text="Update Report"  OnClick="updateBureauReportButton_Click" ValidationGroup="EditBureauReport"></asp:LinkButton>    
                                        </div>
                                                
                                        <!--Ajax Validators-->      
                                        <asp:RequiredFieldValidator ID="editReportNameRequiredValidator" runat="server"
                                            ErrorMessage="Report Name is a required field."
                                            ControlToValidate="txtEditReportName"          
                                            SetFocusOnError="true"
                                            Display="None"
                                            ValidationGroup="EditBureauReport">
                                            </asp:RequiredFieldValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="editReportNameRequiredValidatorExtender" runat="server"
                                            BehaviorID="editReportNameRequiredValidatorExtender"
                                            TargetControlID="editReportNameRequiredValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                                                                               
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                                </asp:Panel>
                                                                                                                                    
                            </telerik:RadPageView>  
                        </telerik:RadMultiPage>
             </div>

</asp:Content>                