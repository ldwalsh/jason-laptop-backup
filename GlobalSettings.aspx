﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="GlobalSettings.aspx.cs" Inherits="CW.Website.GlobalSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

             <h1>Global Settings</h1>
             <div class="richText">
                The global settings pages is used to configure global settings within the system. Such as the following:
                 <ul>
                     <li>Page Header's.</li>
                     <li>Page Body's.</li>
                     <li>Page Error Message's.</li>
                 </ul>
             </div>
             <div class="richText">
                Custom token variables are available for the rich text editors as well. Including the following:
                <ul>
                    <li>Company Name (KGS Buildings or Schneider Electric) = $company</li>
                    <li>Company Abbreviation (KGS or SE) = $companyShort</li>
                    <li>Product Name (Clockworks or Building Analytics) = $product</li>
                    <li>Product Abbreviation (CW or BA) = $productShort</li>
                    <li>KGS Only HTML Class = $kgsOnly</li>
                    <li>Schneider Only HTML Class = $seOnly</li>
                </ul>
             </div>
             <div class="updateProgressDiv">
                 <asp:UpdateProgress ID="updateProgressTop" runat="server">
                     <ProgressTemplate>
                        <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                     </ProgressTemplate>
                 </asp:UpdateProgress>
             </div>
             <div class="administrationControls">
                 
                 <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="Home Content"></telerik:RadTab>
                            <telerik:RadTab Text="Admin Content"></telerik:RadTab>                            
                            <telerik:RadTab Text="Module Content"></telerik:RadTab>
                            <telerik:RadTab Text="Help Content"></telerik:RadTab>
                            <telerik:RadTab Text="Kiosk Help Content"></telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                        <telerik:RadPageView ID="RadPageView1" runat="server">     
              
                         <asp:Panel ID="pnl1" runat="server" DefaultButton="btnUpdateHomeContent">
                             <h2>Home Content</h2>
                             <div>
                                 <a id="lnkSetFocusHome" runat="server"></a>
                                 <asp:Label ID="lblHomeContentError" CssClass="errorMessage" Visible="false" 
                                     runat="server"></asp:Label>
                             </div>
                             <div class="divFormLeftmost">
                                 <label class="textareaWideLabel">
                                 Welcome Page Header:</label>
                                 <textarea class="textareaWide" id="txtWelcomePageHeader" cols="80" 
                                     onkeyup="limitChars(this, 100, 'divWelcomePageHeaderCharInfo')" rows="5" 
                                     runat="server"></textarea>
                                 <div class="textareaWideCharacterDiv" id="divWelcomePageHeaderCharInfo">
                                 </div>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Welcome Page Body (Max HTML Characters = 5000):</label>
                                 <telerik:RadEditor ID="editorWelcomePageBody" 
                                                runat="server"                                                                                                                                                
                                                CssClass="editorNewLine"                                                        
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="5000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" 
                                                > 
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                                                             
                                 </telerik:RadEditor>
                                 <!-- OnTextChanged="limitEditorChars('ctl00_plcCopy_TabContainer1_TabPanel1_editorWelcomePageBody', 5000, 'divEditorWelcomePageBody')"-->
                             </div>
                             <div class="divFormLeftmost">
                                 <label class="textareaWideLabel">
                                 Home Page Header:</label>
                                 <textarea class="textareaWide" id="txtHomePageHeader" cols="80" 
                                     onkeyup="limitChars(this, 100, 'divHomePageHeaderCharInfo')" rows="5" 
                                     runat="server"></textarea>
                                 <div class="textareaWideCharacterDiv" id="divHomePageHeaderCharInfo">
                                 </div>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Home Page Body (Max HTML Characters = 5000):</label>
                                 <telerik:RadEditor ID="editorHomePageBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="5000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                 </telerik:RadEditor>
                             </div>
                             <asp:LinkButton CssClass="lnkButton" ID="btnUpdateHomeContent" runat="server" Text="Update" OnClick="updateHomeContentButton_Click" ValidationGroup="UpdateHomeContent"></asp:LinkButton>
                         </asp:Panel>
                         
                    </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server">

                         <asp:Panel ID="pnl2" runat="server" DefaultButton="btnUpdateAdminContent">
                             <h2>Admin Content</h2>
                             <div>
                                 <a id="lnkSetFocusAdmin" runat="server"></a>
                                 <asp:Label ID="lblAdminContentError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Body (Max HTML Characters = 5000):</label>
                                 <telerik:RadEditor ID="editorAdminBody"
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="5000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Data Sources Body (Max HTML Characters = 5000):</label>
                                 <telerik:RadEditor ID="editorAdminDataSourcesBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="5000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                > 
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                  
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Building Groups Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminBuildingGroupsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Buildings Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminBuildingsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >  
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                 
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Equipment Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminEquipmentBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >  
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                 
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Points Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminPointsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >  
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                 
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Analyses Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminAnalysesBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Scheduled Analyses Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminScheduledAnalysesBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                > 
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                  
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Users Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminUsersBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Settings Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminSettingsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >     
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                              
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Organiation Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminOrganizationBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >     
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                              
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Admin Client Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAdminClientBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >     
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                              
                                 </telerik:RadEditor>
                             </div>
                             <asp:LinkButton CssClass="lnkButton" ID="btnUpdateAdminContent" runat="server" Text="Update" OnClick="updateAdminContentButton_Click" ValidationGroup="UpdateAdminContent"></asp:LinkButton>
                         </asp:Panel>
                     
                     </telerik:RadPageView>            
                        <telerik:RadPageView ID="RadPageView3" runat="server">

                         <asp:Panel ID="pnl3" runat="server" DefaultButton="btnUpdateModuleContent">
                             <h2>Module Content</h2>
                             <div>
                                 <a id="lnkSetFocusModule" runat="server"></a>
                                 <asp:Label ID="lblModuleContentError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Alarms Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAlarmsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Analysis Builder Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorAnalysisBuilderBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>                             
                             <div class="divFormLeftmost">
                                 <label>
                                 Commissioning Dashboard Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorCommissioningDashboardBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>                             
                             <div class="divFormLeftmost">
                                 <label>
                                 Diagnostics Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorDiagnosticsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Documents Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorDocumentsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div> 
                             <div class="divFormLeftmost">
                                 <label>
                                 Energy Dashboard Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorEnergyDashboardBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>                           
                             <div class="divFormLeftmost">
                                 <label>
                                 Operations Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorOperationsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Performance Dashboard Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorPerformanceDashboardBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Profiles Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorProfilesBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Projects Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorProjectsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>

                             <div class="divFormLeftmost">
                                 <label>
                                 Reports Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorReportsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >      
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                             
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Tasks Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorTasksBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >      
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                             
                                 </telerik:RadEditor>
                             </div>                                
                             <div class="divFormLeftmost">
                                 <label>
                                 Utility Billing Body (Max HTML Characters = 1000):</label>
                                 <telerik:RadEditor ID="editorUtilityBillingBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="1000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >    
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                               
                                 </telerik:RadEditor>
                             </div>
                             <asp:LinkButton CssClass="lnkButton" ID="btnUpdateModuleContent" runat="server" Text="Update" OnClick="updateModuleContentButton_Click" ValidationGroup="UpdateModuleContent"></asp:LinkButton>
                         </asp:Panel>
                     
                    </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView4" runat="server">
                         <asp:Panel ID="pnl4" runat="server" DefaultButton="btnUpdateHelpContent">
                             <h2>Help Content</h2>
                             <div>
                                 <a id="lnkSetFocusHelp" runat="server"></a>
                                 <asp:Label ID="lblHelpContentError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Help Body (Max HTML Characters = 5000):</label>
                                 <telerik:RadEditor ID="editorHelpBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="5000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                > 
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                  
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Privacy Policy Body (Max HTML Characters = 20000):</label>
                                 <telerik:RadEditor ID="editorPrivacyPolicyBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="20000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >  
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                 
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Terms and Conditions Body (Max HTML Characters = 20000):</label>
                                 <telerik:RadEditor ID="editorTermsAndConditionsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="20000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                 </telerik:RadEditor>
                             </div>
                             <asp:LinkButton CssClass="lnkButton" ID="btnUpdateHelpContent" runat="server" Text="Update" OnClick="updateHelpContentButton_Click" ValidationGroup="UpdateHelpContent"></asp:LinkButton>
                         </asp:Panel>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView5" runat="server">
                         <asp:Panel ID="pnl5" runat="server" DefaultButton="btnUpdateKioskHelpContent">
                             <h2>Kiosk Help Content</h2>
                             <div>
                                 <a id="lnkSetFocusKioskHelp" runat="server"></a>
                                 <asp:Label ID="lblKioskHelpContentError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Kiosk Help Q1 (Max HTML Characters = 20000):</label>
                                 <telerik:RadEditor ID="editorKioskHelpQ1" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="20000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                > 
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                  
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Kiosk Help Q2 (Max HTML Characters = 20000):</label>
                                 <telerik:RadEditor ID="editorKioskHelpQ2" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="20000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >  
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                 
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Kiosk Help Q3 (Max HTML Characters = 20000):</label>
                                 <telerik:RadEditor ID="editorKioskHelpQ3" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="20000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                 </telerik:RadEditor>
                             </div>
                             <div class="divFormLeftmost">
                                 <label>
                                 Kiosk Help Q4 (Max HTML Characters = 20000):</label>
                                 <telerik:RadEditor ID="editorKioskHelpQ4" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="20000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsFull.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                 
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                 </telerik:RadEditor>
                             </div>
                             <asp:LinkButton CssClass="lnkButton" ID="btnUpdateKioskHelpContent" runat="server" Text="Update" OnClick="updateKioskHelpContentButton_Click" ValidationGroup="UpdateKioskHelpContent"></asp:LinkButton>
                         </asp:Panel>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
             </div>

</asp:Content>                