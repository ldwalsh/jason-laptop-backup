﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.EquipmentTypes.nonkgs;
using System;

namespace CW.Website
{
    public partial class ProviderEquipmentTypeAdministration : AdminSitePageTemp
    {
        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewEquipmentTypes).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.PROVIDER; } }

            #endregion

        #endregion
    }
}