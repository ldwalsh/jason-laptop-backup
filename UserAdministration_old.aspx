﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="true" CodeBehind="UserAdministration_old.aspx.cs" Inherits="CW.Website.UserAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="CW.Website" Namespace="CW.Website._extensions" TagPrefix="extensions" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
    
        <div id="intro" class="pod">
        	<div class="top"> </div>
            <div id="middle" class="middle">                    	                  	
            	<h1>User Administration</h1>                        
                <div class="richText">
                    <asp:Literal ID="litAdminUsersBody" runat="server"></asp:Literal> 
                </div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                Loading...
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <ajaxToolkit:TabContainer ID="TabContainer1"                  
                            runat="server" 
                            OnLoad="loadTabs" 
                            OnActiveTabChanged="onActiveTabChanged"
                            AutoPostBack="true"                            
                            Width="100%">
                            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" 
                                HeaderText="View Users">
                                <ContentTemplate>
                                   
                                    <p>
                                            <a id="lnkSetFocusView" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>           

                                    <div class="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </div>   
                                    <!-- NOTE: Do not move controls around in the grid as on row databound uses column and control positions to show and hide controls. -->                                                                              
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridUsers"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="UID"  
                                             GridLines="None"                                      
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridUsers_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridUsers_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridUsers_Sorting"   
                                             OnSelectedIndexChanged="gridUsers_OnSelectedIndexChanged"                                                                                                             
                                             OnRowDeleting="gridUsers_Deleting"
                                             OnRowEditing="gridUsers_Editing"
                                             OnDataBound="gridUsers_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Email" HeaderText="Email">  
                                                    <ItemTemplate><%# Eval("Email").ToString().Length <= 25 ? Eval("Email") : Eval("Email").ToString().Remove(25, (Eval("Email").ToString().Length - 25)) + "..." %></ItemTemplate>                                                                                                                                       
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FirstName" HeaderText="First Name">  
                                                    <ItemTemplate><%# Eval("FirstName").ToString().Length <= 15 ? Eval("FirstName") : Eval("FirstName").ToString().Remove(15, (Eval("FirstName").ToString().Length - 15)) + "..."%></ItemTemplate>                                    
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="LastName" HeaderText="Last Name">  
                                                    <ItemTemplate><%# Eval("LastName").ToString().Length <= 20 ? Eval("LastName") : Eval("LastName").ToString().Remove(20, (Eval("LastName").ToString().Length - 20)) + "..."%></ItemTemplate>                           
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Role" HeaderText="Role">  
                                                    <ItemTemplate><%# Eval("Role")%>
                                                        <asp:HiddenField ID="hdnRoleID" runat="server" Visible="true" Value='<%# Eval("RoleID")%>' />
                                                    </ItemTemplate>                                               
                                                </asp:TemplateField>   
                                                <asp:TemplateField SortExpression="Active" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this user permanently?\n\nAlternatively you can deactivate a user temporarily instead.');"
                                                               CommandName="Delete">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                                                          
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT USER DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvUser" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>User Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# Eval("FirstName") == null && Eval("LastName") == null ? "" : "<li><span class=\"\"><strong>Name: </strong>" + (String.IsNullOrEmpty(Convert.ToString(Eval("FirstName"))) ? "" : Eval("FirstName") + " ") + (String.IsNullOrEmpty(Convert.ToString(Eval("LastName"))) ? "" : Eval("LastName")) + "</span></li>"%> 
                                                            <%# "<li><strong>Email: </strong><a href=\"mailto:" + Eval("Email") + "\">" + Eval("Email") + "</a></li>"%>   
                                                            <%# "<li><strong>Role: </strong>" + Eval("Role") + "</li>"%>
                                                            <%# "<li><strong>Address: </strong>" + Eval("Address") + "</li>"%> 
                                                            <%# "<li><strong>City: </strong>" + Eval("City") + "</li>"%> 
                                                            <%# "<li><strong>State: </strong>" + Eval("StateName") + "</li>"%> 
                                                            <%# "<li><strong>Zip: </strong>" + Eval("Zip") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Phone"))) ? "" : "<li><strong>Phone: </strong>" + Eval("Phone") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("MobilePhone"))) ? "" : "<li><strong>Mobile Phone: </strong>" + Eval("MobilePhone") + "</li>"%>                                                                        
                                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>                                                	                                                                	                                                                
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>                                     
                                                                     
                                    <!--EDIT USER PANEL -->                              
                                    <asp:Panel ID="pnlEditUsers" runat="server" Visible="false">                                                                                             
                                              <div>
                                                    <h2>Edit User</h2>
                                              </div>  
                                              <div>                                                    
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <asp:HiddenField ID="hdnCID" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*First Name:</label>
                                                        <asp:TextBox ID="txtEditFirstName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                        <asp:RequiredFieldValidator ID="editFirstNameRequiredValidator" runat="server"
                                                            ErrorMessage="First Name is a required field."
                                                            ControlToValidate="txtEditFirstName"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editFirstNameRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editFirstNameRequiredValidatorExtender"
                                                            TargetControlID="editFirstNameRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender> 
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Last Name:</label>
                                                        <asp:TextBox ID="txtEditLastName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                                                                        
                                                        <asp:RequiredFieldValidator ID="editLastNameRequiredValidator" runat="server"
                                                            ErrorMessage="Last Name is a required field."
                                                            ControlToValidate="txtEditLastName"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editLastNameRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editLastNameRequiredValidatorExtender"
                                                            TargetControlID="editLastNameRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Email:</label>
                                                        <asp:TextBox ID="txtEditEmail" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="editEmailRequiredValidator" runat="server"
                                                            ErrorMessage="Email is a required field." 
                                                            ControlToValidate="txtEditEmail"  
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editEmailRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editEmailRequiredValidatorExtender" 
                                                            TargetControlID="editEmailRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                        <asp:RegularExpressionValidator ID="editEmailRegExValidator" runat="server"
                                                            ErrorMessage="Invalid Email Format."
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ControlToValidate="txtEditEmail"
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditUser">
                                                            </asp:RegularExpressionValidator>                         
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editEmailRegExValidatorExtender" runat="server"
                                                            BehaviorID="editEmailRegExValidatorExtender" 
                                                            TargetControlID="editEmailRegExValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>                                                        
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Confirm Email:</label>
                                                        <asp:TextBox ID="txtEditConfirmEmail" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="editConfirmEmailRequiredValidator" runat="server"
                                                            ErrorMessage="Confirm Email is a required field." 
                                                            ControlToValidate="txtEditConfirmEmail"  
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmEmailRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editConfirmEmailRequiredValidatorExtender" 
                                                            TargetControlID="editConfirmEmailRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                        <asp:RegularExpressionValidator ID="editConfirmEmailRegExValidator" runat="server"
                                                            ErrorMessage="Invalid Email Format."
                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                            ControlToValidate="txtEditConfirmEmail"
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditUser">
                                                            </asp:RegularExpressionValidator>                         
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmEmailRegExValidatorExtender" runat="server"
                                                            BehaviorID="editConfirmEmailRegExValidatorExtender" 
                                                            TargetControlID="editConfirmEmailRegExValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                        <asp:CompareValidator ID="editConfirmEmailCompareValidator" runat="server"
                                                            ErrorMessage="The emails do not match." 
                                                            ControlToValidate="txtEditConfirmEmail"
                                                            ControlToCompare="txtEditEmail" 
                                                            Operator="Equal"
                                                            Display="None"
                                                            ValidationGroup="EditUser">
                                                            </asp:CompareValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmEmailCompareValidatorExtender" runat="server"
                                                            BehaviorID="editConfirmEmailCompareValidatorExtender" 
                                                            TargetControlID="editConfirmEmailCompareValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender> 
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Address:</label>
                                                        <asp:TextBox ID="txtEditAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                                        <asp:RequiredFieldValidator ID="editAddressRequiredValidator" runat="server"
                                                            ErrorMessage="Address is a required field."
                                                            ControlToValidate="txtEditAddress"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editAddressRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editAddressRequiredValidatorExtender"
                                                            TargetControlID="editAddressRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                    </div>                                                                                                                                                                                           
                                                    <div class="divForm">
                                                         <label class="label">*City:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditCity" runat="server" MaxLength="100"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="editCityRequiredValidator" runat="server"
                                                            ErrorMessage="City is a required field."
                                                            ControlToValidate="txtEditCity"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editCityRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editCityRequiredValidatorExtender"
                                                            TargetControlID="editCityRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>                                                           
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">*State:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlEditState" AppendDataBoundItems="true" runat="server" >                                                                                          
                                                         </asp:DropDownList>
                                                         <asp:RequiredFieldValidator ID="editStateRequiredValidator" runat="server"
                                                            ErrorMessage="State is a required field." 
                                                            ControlToValidate="ddlEditState"  
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            InitialValue="-1"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editStateRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editStateRequiredValidatorExtender" 
                                                            TargetControlID="editStateRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">*Zip Code:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditZip" runat="server" MaxLength="5" ></asp:TextBox>   
                                                         <ajaxToolkit:MaskedEditExtender ID="editZipMaskedEditExtender" runat="server"
                                                                TargetControlID="txtEditZip" 
                                                                Mask="99999"
                                                                AutoComplete="true"                                
                                                                MaskType="Number" 
                                                                InputDirection="LeftToRight"                                
                                                                />     
                                                        <asp:RequiredFieldValidator ID="editZipRequiredValidator" runat="server"
                                                            ErrorMessage="Zip Code is a required field."
                                                            ControlToValidate="txtEditZip"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editZipRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editZipRequiredValidatorExtender"
                                                            TargetControlID="editZipRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender> 
                                                    </div>                                                    
                                                    <div class="divForm">
                                                         <label class="label">Phone:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditPhone" runat="server" MaxLength="15" ></asp:TextBox>   
                                                         <ajaxToolkit:MaskedEditExtender ID="editPhoneMaskedEditExtender" runat="server"
                                                            TargetControlID="txtEditPhone" 
                                                            Mask="(999)999-9999"
                                                            AutoComplete="true"                                
                                                            MaskType="Number" 
                                                            InputDirection="LeftToRight"                                 
                                                            />
                                                    </div>  
                                                    <div class="divForm">
                                                         <label class="label">Mobile Phone:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditMobilePhone" runat="server" MaxLength="15" ></asp:TextBox>   
                                                         <ajaxToolkit:MaskedEditExtender ID="editMobilePhoneMaskedEditExtender" runat="server"
                                                            TargetControlID="txtEditMobilePhone" 
                                                            Mask="(999)999-9999"
                                                            AutoComplete="true"                                
                                                            MaskType="Number" 
                                                            InputDirection="LeftToRight"                                 
                                                            /> 
                                                    </div>  
                                                    <div class="divForm">   
                                                        <label class="label">*User Role:</label>    
                                                        <extensions:DropDownExtension ID="ddlEditRole" CssClass="dropdown" OnSelectedIndexChanged="ddlEditRole_OnSelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true" runat="server">                                                                
                                                        </extensions:DropDownExtension>
                                                        <p>(Warning: Changing the users role MAY clear all buildings associated with that user, if any.")
                                                        </p> 
                                                        <asp:RequiredFieldValidator ID="editRoleRequiredValidator" runat="server"
                                                            ErrorMessage="User Role is a required field." 
                                                            ControlToValidate="ddlEditRole"  
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            InitialValue="-1"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editRoleRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editRoleRequiredValidatorExtender" 
                                                            TargetControlID="editRoleRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div>                                                                                                                                                                                                                                                                  
                                                    <div class="divForm">
                                                        <label class="label">*Active:</label>
                                                        <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                        
                                                    </div>                   
                          
                                                    <hr />
                                                    <h2>Reset Password</h2>
                                                    <p>Enter a new password only if you wish to reset it.</p>
                                                    <div class="divForm">                                                          
                                                        <label class="label">New Password:</label>                                                                                          
                                                        <asp:TextBox ID="txtEditNewPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>                                            
                                                        <asp:RegularExpressionValidator ID="editNewPasswordRegExValidator" runat="server"
                                                            ErrorMessage="Password must be 6-12 characters."
                                                            ValidationExpression=".{6,12}"
                                                            ControlToValidate="txtEditNewPassword"
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditUser">
                                                            </asp:RegularExpressionValidator>                         
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editNewPaswordRegExValidatorExtender" runat="server"
                                                            BehaviorID="editNewPasswordRegExValidatorExtender" 
                                                            TargetControlID="editNewPasswordRegExValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                    </div>                                       
                                                      
                                                    <div id="divEditBuildingGroupsListBoxs" runat="server" visible="false">
                                                        <hr />
                                                        <h2>Assign Building Groups</h2>
                                                        <p>Building Groups are shown/available based on role and client selected above.</p>
                                                        <div class="divForm">                                                        
                                                            <label class="label">Building Group Access:</label>                                                                                                                                       
                                                            <asp:ListBox ID="lbEditBuildingGroupsTop" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>
                                                            <div class="divArrows">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnEditBuildingGroupsUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditBuildingGroupsUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnEditBuildingGroupsDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditBuildingGroupsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                            </div>
                                                            <asp:ListBox ID="lbEditBuildingGroupsBottom" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>                                                    
                                                        </div>
                                                    </div>  
                                                                                                                             
                                                    <div id="divEditBuildingsListBoxs" runat="server" visible="false">
                                                        <hr />
                                                        <h2>Assign Buildings</h2>
                                                        <p>Buildings are shown/available based on role and client selected above.</p>
                                                        <div class="divForm">                                                        
                                                            <label class="label">Building Access:</label>                                                                                                                                       
                                                            <asp:ListBox ID="lbEditBuildingsTop" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>
                                                            <div class="divArrows">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnEditBuildingsUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditBuildingsUpButton_Click"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnEditBuildingsDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditBuildingsDownButton_Click"></asp:ImageButton>                                                    
                                                            </div>
                                                            <asp:ListBox ID="lbEditBuildingsBottom" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>                                                    
                                                        </div>
                                                    </div>
                                                        <!--TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard                                              
                                                        <hr />
                                                        <h2>Assign Preset Gauges</h2>
                                                        <p>Gauges are available based on role, client, and buildings selected above.</p>
                                                        <div class="divForm">                                                        
                                                            <label class="label">Preset Gauges:</label>                                                                                                                                       
                                                            <asp:ListBox ID="lbEditGaugesTop" Height="100" Width="480" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>
                                                            <div class="divArrowsWide">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnEditGaugesUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditGaugesUpButton_Click"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnEditGaugesDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditGaugesDownButton_Click"></asp:ImageButton>                                                    
                                                            </div>
                                                            <asp:ListBox ID="lbEditGaugesBottom" Height="100" Width="480" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>  
                                                        </div>                                                                                           
                                                        -->                                           
                                                        <hr />
                                                        <h2>Assign Quick Links</h2>
                                                        <p>Quick Links are available based on role, and client selected above. Also based on the associated clients modules.</p>
                                                        <div class="divForm">                                                        
                                                            <label class="label">Quick Links:</label>                                                                                                                                       
                                                            <asp:ListBox ID="lbEditQuickLinksTop" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>
                                                            <div class="divArrows">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnEditQuickLinksUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditQuickLinksUpButton_Click"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnEditQuickLinksDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditQuickLinksDownButton_Click"></asp:ImageButton>
                                                            </div>
                                                            <asp:ListBox ID="lbEditQuickLinksBottom" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>                                                    
                                                        </div>
                                                 
                                                                                                        
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateUser" runat="server" Text="Update User"  OnClick="updateUserButton_Click" ValidationGroup="EditUser"></asp:LinkButton>
                                                </div>
                                                
                                                <!--Ajax Validators are next to each form element, because they are not always visiable/in use-->
                                    </asp:Panel>                                                                                                         
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" 
                                HeaderText="Add User">
                                <ContentTemplate>  
                                    <h2>Add User</h2> 
                                    <div>            
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                        
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*First Name:</label>
                                            <asp:TextBox ID="txtAddFirstName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                            <asp:RequiredFieldValidator ID="addFirstNameRequiredValidator" runat="server"
                                                ErrorMessage="First Name is a required field."
                                                ControlToValidate="txtAddFirstName"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addFirstNameRequiredValidatorExtender" runat="server"
                                                BehaviorID="addFirstNameRequiredValidatorExtender"
                                                TargetControlID="addFirstNameRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Last Name:</label>
                                            <asp:TextBox ID="txtAddLastName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                                                                        
                                            <asp:RequiredFieldValidator ID="addLastNameRequiredValidator" runat="server"
                                                ErrorMessage="Last Name is a required field."
                                                ControlToValidate="txtAddLastName"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addLastNameRequiredValidatorExtender" runat="server"
                                                BehaviorID="addLastNameRequiredValidatorExtender"
                                                TargetControlID="addLastNameRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>  
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Email:</label>
                                            <asp:TextBox ID="txtAddEmail" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="addEmailRequiredValidator" runat="server"
                                                ErrorMessage="Email is a required field." 
                                                ControlToValidate="txtAddEmail"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addEmailRequiredValidatorExtender" runat="server"
                                                BehaviorID="addEmailRequiredValidatorExtender" 
                                                TargetControlID="addEmailRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addEmailRegExValidator" runat="server"
                                                ErrorMessage="Invalid Email Format."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ControlToValidate="txtAddEmail"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addEmailRegExValidatorExtender" runat="server"
                                                BehaviorID="addEmailRegExValidatorExtender" 
                                                TargetControlID="addEmailRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Confirm Email:</label>
                                            <asp:TextBox ID="txtAddConfirmEmail" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="addConfirmEmailRequiredValidator" runat="server"
                                                ErrorMessage="Confirm Email is a required field." 
                                                ControlToValidate="txtAddConfirmEmail"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmEmailRequiredValidatorExtender" runat="server"
                                                BehaviorID="addConfirmEmailRequiredValidatorExtender" 
                                                TargetControlID="addConfirmEmailRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addConfirmEmailRegExValidator" runat="server"
                                                ErrorMessage="Invalid Email Format."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ControlToValidate="txtAddConfirmEmail"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmEmailRegExValidatorExtender" runat="server"
                                                BehaviorID="addConfirmEmailRegExValidatorExtender" 
                                                TargetControlID="addConfirmEmailRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:CompareValidator ID="addConfirmEmailCompareValidator" runat="server"
                                                ErrorMessage="The emails do not match." 
                                                ControlToValidate="txtAddConfirmEmail"
                                                ControlToCompare="txtAddEmail" 
                                                Operator="Equal"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:CompareValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmEmailCompareValidatorExtender" runat="server"
                                                BehaviorID="addConfirmEmailCompareValidatorExtender" 
                                                TargetControlID="addConfirmEmailCompareValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Address:</label>
                                            <asp:TextBox ID="txtAddAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                            <asp:RequiredFieldValidator ID="addAddressRequiredValidator" runat="server"
                                                ErrorMessage="Address is a required field."
                                                ControlToValidate="txtAddAddress"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addAddressRequiredValidatorExtender" runat="server"
                                                BehaviorID="addAddressRequiredValidatorExtender"
                                                TargetControlID="addAddressRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>                                                                                                                                                                                           
                                        <div class="divForm">
                                             <label class="label">*City:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddCity" runat="server" MaxLength="100"></asp:TextBox>
                                             <asp:RequiredFieldValidator ID="addCityRequiredValidator" runat="server"
                                                ErrorMessage="City is a required field."
                                                ControlToValidate="txtAddCity"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addCityRequiredValidatorExtender" runat="server"
                                                BehaviorID="addCityRequiredValidatorExtender"
                                                TargetControlID="addCityRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>     
                                        </div>
                                        <div class="divForm">
                                             <label class="label">*State:</label>
                                             <asp:DropDownList CssClass="dropdown" ID="ddlAddState" AppendDataBoundItems="true" runat="server" >  
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                             
                                             </asp:DropDownList>
                                             <asp:RequiredFieldValidator ID="addStateRequiredValidator" runat="server"
                                                ErrorMessage="State is a required field." 
                                                ControlToValidate="ddlAddState"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addStateRequiredValidatorExtender" runat="server"
                                                BehaviorID="addStateRequiredValidatorExtender" 
                                                TargetControlID="addStateRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>                                                
                                        </div>
                                        <div class="divForm">
                                             <label class="label">*Zip Code:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddZip" runat="server" MaxLength="5" ></asp:TextBox>   
                                             <ajaxToolkit:MaskedEditExtender ID="addZipMaskedEditExtender" runat="server"
                                                    TargetControlID="txtAddZip" 
                                                    Mask="99999"
                                                    AutoComplete="true"                                
                                                    MaskType="Number" 
                                                    InputDirection="LeftToRight"                                 
                                                    />     
                                            <asp:RequiredFieldValidator ID="addZipRequiredValidator" runat="server"
                                                ErrorMessage="Zip Code is a required field."
                                                ControlToValidate="txtAddZip"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addZipRequiredValidatorExtender" runat="server"
                                                BehaviorID="addZipRequiredValidatorExtender"
                                                TargetControlID="addZipRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>  
                                        </div>                                                    
                                        <div class="divForm">
                                             <label class="label">Phone:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddPhone" runat="server" MaxLength="15" ></asp:TextBox>   
                                             <ajaxToolkit:MaskedEditExtender ID="addPhoneMaskedEditExtender" runat="server"
                                                TargetControlID="txtAddPhone" 
                                                Mask="(999)999-9999"
                                                AutoComplete="true"                                
                                                MaskType="Number" 
                                                InputDirection="LeftToRight"                                 
                                                />  
                                        </div>
                                        <div class="divForm">
                                             <label class="label">MobilePhone:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddMobilePhone" runat="server" MaxLength="15" ></asp:TextBox>   
                                             <ajaxToolkit:MaskedEditExtender ID="addMobilePhoneMaskedEditExtender" runat="server"
                                                TargetControlID="txtAddMobilePhone" 
                                                Mask="(999)999-9999"
                                                AutoComplete="true"                                
                                                MaskType="Number" 
                                                InputDirection="LeftToRight"                                 
                                                />   
                                        </div>
                                        <div class="divForm">   
                                            <label class="label">*User Role:</label>    
                                            <extensions:DropDownExtension ID="ddlAddRole" OnSelectedIndexChanged="ddlAddRole_OnSelectedIndexChanged" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1" >Select one...</asp:ListItem>                           
                                            </extensions:DropDownExtension> 
                                            <asp:RequiredFieldValidator ID="addRoleRequiredValidator" runat="server"
                                                ErrorMessage="User Role is a required field." 
                                                ControlToValidate="ddlAddRole"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addRoleRequiredValidatorExtender" runat="server"
                                                BehaviorID="addRoleRequiredValidatorExtender" 
                                                TargetControlID="addRoleRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>                                                                                                                                                                                                                       
                                        <div class="divForm">  
                                            <label class="label">*Password:</label>                                                                                          
                                            <asp:TextBox ID="txtAddInitialPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="addInitialPasswordRequiredValidator" runat="server"
                                                ErrorMessage="Password is a required field." 
                                                ControlToValidate="txtAddInitialPassword"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addInitialPasswordRequiredValidatorExtender" runat="server"
                                                BehaviorID="addInitialPasswordRequiredValidatorExtender" 
                                                TargetControlID="addInitialPasswordRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addInitialPasswordRegExValidator" runat="server"
                                                ErrorMessage="Password must be 6-12 characters."
                                                ValidationExpression=".{6,12}"
                                                ControlToValidate="txtAddInitialPassword"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addInitialPaswordRegExValidatorExtender" runat="server"
                                                BehaviorID="addInitialPasswordRegExValidatorExtender" 
                                                TargetControlID="addInitialPasswordRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Confirm Password:</label>                                                                                          
                                            <asp:TextBox ID="txtAddConfirmPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="addConfirmPasswordRequiredValidator" runat="server"
                                                ErrorMessage="Confirm Password is a required field." 
                                                ControlToValidate="txtAddConfirmPassword"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmPasswordRequiredValidatorExtender" runat="server"
                                                BehaviorID="addConfirmPasswordRequiredValidatorExtender" 
                                                TargetControlID="addConfirmPasswordRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addConfirmPasswordRegExValidator" runat="server"
                                                ErrorMessage="Confirm Password must be 6-12 characters."
                                                ValidationExpression=".{6,12}"
                                                ControlToValidate="txtAddConfirmPassword"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmPasswordRegExValidatorExtender" runat="server"
                                                BehaviorID="addConfirmPasswordRegExValidatorExtender" 
                                                TargetControlID="addConfirmPasswordRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:CompareValidator ID="addConfirmPasswordCompareValidator" runat="server"
                                                ErrorMessage="The passwords do not match. Passwords are case sensitive." 
                                                ControlToValidate="txtAddConfirmPassword"
                                                ControlToCompare="txtAddInitialPassword" 
                                                Operator="Equal"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:CompareValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmPasswordCompareValidatorExtender" runat="server"
                                                BehaviorID="addConfirmPasswordCompareValidatorExtender" 
                                                TargetControlID="addConfirmPasswordCompareValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>   
                                        </div>
                                        
                                        <div id="divAddBuildingGroupsListBoxs" runat="server" visible="false">
                                            <hr />
                                            <h2>Assign Building Groups</h2>
                                            <p>Building Groups are shown/available based on role and client selected above.</p>
                                            <div class="divForm">                                            
                                                <label class="label">Building Group Access:</label>                                                                                                                                       
                                                <asp:ListBox ID="lbAddBuildingGroupsTop" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnAddBuildingGroupsUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnAddBuildingGroupsUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnAddBuildingGroupsDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnAddBuildingGroupsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                </div>
                                                <asp:ListBox ID="lbAddBuildingGroupsBottom" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>                                                    
                                            </div>
                                        </div> 

                                        <div id="divAddBuildingsListBoxs" runat="server" visible="false">
                                            <hr />
                                            <h2>Assign Buildings</h2>
                                            <p>Buildings are shown/available based on role and client selected above.</p>
                                            <div class="divForm">                                            
                                                <label class="label">Building Access:</label>                                                                                                                                       
                                                <asp:ListBox ID="lbAddBuildingsTop" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnAddBuildingsUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnAddBuildingsUpButton_Click"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnAddBuildingsDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnAddBuildingsDownButton_Click"></asp:ImageButton>                                                    
                                                </div>
                                                <asp:ListBox ID="lbAddBuildingsBottom" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>                                                    
                                            </div>
                                        </div> 
                                            <!--TEMP: temporaritly disable gauges as they are not currently in user via teh dashboard                           
                                            <hr />
                                            <h2>Assign Preset Gauges</h2>
                                            <p>Gauges are available based on role, client, and buildings selected above.</p>
                                            <div class="divForm">                                            
                                                <label class="label">Preset Gauges:</label>                                                                                                                                       
                                                <asp:ListBox ID="lbAddGaugesTop" Height="100" Width="480" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrowsWide">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnAddGaugesUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnAddGaugesUpButton_Click"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnAddGaugesDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnAddGaugesDownButton_Click"></asp:ImageButton>                                                    
                                                </div>
                                                <asp:ListBox ID="lbAddGaugesBottom" Height="100" Width="480" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>                                                    
                                            </div>
                                            -->                                                                          
                                            <hr />
                                            <h2>Assign Quick Links</h2>
                                            <p>Quick Links are available based on role, and client selected above. Also based on the associated clients modules.</p>
                                            <div class="divForm">                                                                                        
                                                <label class="label">Quick Links:</label>                                                                                                                                       
                                                <asp:ListBox ID="lbAddQuickLinksTop" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnAddQuickLinksUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnAddQuickLinksUpButton_Click"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnAddQuickLinksDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnAddQuickLinksDownButton_Click"></asp:ImageButton>                                                    
                                                </div>
                                                <asp:ListBox ID="lbAddQuickLinksBottom" Height="100" Width="246" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>                                                    
                                            </div>                                                
                                    
                                                                                                                                                                                                
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddUser" runat="server" Text="Add User"  OnClick="addUserButton_Click" ValidationGroup="AddUser"></asp:LinkButton>                                                                                   
                                    </div>
                                    
                                    <!--Ajax Validators are next to each form element, because they are not always visiable/in use-->    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                                </ContentTemplate>                  
                            </ajaxToolkit:TabPanel>                                                                                                                        
                        </ajaxToolkit:TabContainer>
                   </div>                                                                 
             </div>
        </div>                
</asp:Content>


                    
                  
