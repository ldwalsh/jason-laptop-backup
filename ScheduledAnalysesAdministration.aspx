﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="ScheduledAnalysesAdministration.aspx.cs" Inherits="CW.Website.ScheduledAnalysesAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/ScheduledAnalyses/ViewScheduledAnalyses.ascx" tagname="ViewScheduledAnalyses" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <h1>Scheduled Analyses Administration</h1>                        
      <div class="richText">
        <asp:Literal ID="litAdminScheduledAnalysesBody" runat="server"></asp:Literal> 
      </div>     

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>                                      
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div>
        
      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" CausesValidation="false" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Sch. Analyses" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:ViewScheduledAnalyses ID="ViewScheduledAnalyses" runat="server" />
          </telerik:RadPageView>
        
        </telerik:RadMultiPage>
      </div>

</asp:Content>