﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using CW.Business;
using CW.Data.Models.FAQ;
using CW.Website._framework;

namespace CW.Website
{
    public partial class ReleaseNotes : SitePage
    {
        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    //bind release notes repeater
                    rptReleaseNotes.DataSource = DataMgr.ReleaseNoteDataMapper.GetAllReleaseNotes();
                    rptReleaseNotes.DataBind();
                }
            }

        #endregion
    }
}
