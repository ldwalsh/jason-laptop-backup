﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemAdministration.aspx.cs" Inherits="CW.Website.SystemAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                 	                  	        	
    <h1>System Administration</h1>                        
    <div class="richText">
        The above system administration pages are used to perform diagnostic tests on the system, enable and disable functionaily, and configure global settings.
    </div>                                
    
</asp:Content>                