﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.BMSInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class SystemBMSInfoAdministration : AdminSitePageTemp
    {
        private void Page_FirstLoad()
        {
            TabStateMonitor.ChangeState(new Enum[] { SystemBMSInfoAdministration.TabMessages.Init });
        }

        private void Page_Init()
        {

        }
        public enum TabMessages
        {
            Init,
            AddBMSInfo,
            EditBMSInfo,
            DeleteBMSInfo
        }

        #region properties

        #region overrides

        protected override String DefaultControl { get { return typeof(ViewBMSInfo).Name; } }

        public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.KGS; } }

        #endregion

        #endregion
    }
}