﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSEquipmentManufacturerModelAdministration.aspx.cs" Inherits="CW.Website.KGSEquipmentManufacturerModelAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
      
      <h1>KGS Equipment Manufacturer Models Administration</h1>
                             
      <div class="richText">The kgs equipment manufacturer models administration area is used to view, add, and edit equipment manufacturer models.</div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>                                       
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div> 

      <div class="administrationControls">

        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Equipment Manufacturer Models" />
            <telerik:RadTab Text="Add Equipment Manufacturer Model" />
          </Tabs>
        </telerik:RadTabStrip>

        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">

            <h2>View Equipment Manufacturer Models</h2> 
          
            <p>
              <a id="lnkSetFocusView" href="#" runat="server"></a>
              <asp:Label ID="lblResults" runat="server" Text="" />
              <asp:Label ID="lblErrors" CssClass="errorMessage" runat="server" Visible="false" />
            </p>

            <div class="divForm">
              <label class="label">Select Manufacturer:</label>

              <asp:DropDownList ID="ddlManufacturerList" CssClass="dropdown" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="BindManufacturerDataForDDL_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Value="-1">View All</asp:ListItem>                             
              </asp:DropDownList>
            </div> 
          
            <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
              
              <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />

              <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
              <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click" />

            </asp:Panel>    
                                      
            <div id="gridTbl">
                                        
              <asp:GridView ID="gridEquipmentManufacturerModels"   
                            EnableViewState="true"           
                            runat="server"  
                            DataKeyNames="ManufacturerModelID" 
                            GridLines="None"                                           
                            PageSize="20"
                            PagerSettings-PageButtonCount="20"
                            OnRowCreated="gridEquipmentManufacturerModels_RowCreated" 
                            AllowPaging="true"  
                            OnPageIndexChanging="gridEquipmentManufacturerModels_PageIndexChanging"
                            AllowSorting="true"  
                            OnSorting="gridEquipmentManufacturerModels_Sorting"  
                            OnSelectedIndexChanged="gridEquipmentManufacturerModels_SelectedIndexChanged"                                                                                                             
                            OnRowEditing="gridEquipmentManufacturerModels_Editing"
                            OnRowDeleting="gridEquipmentManufacturerModels_Deleting"
                            OnDataBound="gridEquipmentManufacturerModels_DataBound"
                            AutoGenerateColumns="false"                                              
                            HeaderStyle-CssClass="tblTitle" 
                            RowStyle-CssClass="tblCol1"
                            AlternatingRowStyle-CssClass="tblCol2" 
                            RowStyle-Wrap="true"> 
                <Columns> 
                  <asp:CommandField ShowSelectButton="true" />

                  <asp:TemplateField>
                    <ItemTemplate>                                                
                      <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                    </ItemTemplate>
                  </asp:TemplateField>
                   
                  <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ManufacturerModelName" HeaderText="Manufacturer Model Name">  
                    <ItemTemplate><%# Eval("ManufacturerModelName")%></ItemTemplate>
                  </asp:TemplateField>
                
                  <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ManufacturerName" HeaderText="Manufacturer">  
                    <ItemTemplate><%# Eval("ManufacturerName")%></ItemTemplate>                           
                  </asp:TemplateField>
                                                                                                                
                  <asp:TemplateField>
                    <ItemTemplate>                                                
                      <asp:LinkButton Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this equiupment manufacturer model permanently?');" CommandName="Delete">Delete</asp:LinkButton>
                    </ItemTemplate>
                  </asp:TemplateField>                                                                                                                                                                          
                </Columns>        
              </asp:GridView>                                      

            </div>
            <br />
            <br /> 
         
            <div>                                      
                                    
              <!--SELECT EQUIPMENT MANUFACTURER MODEL DETAILS VIEW -->
              <asp:DetailsView ID="dtvManufacturerModel" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                <Fields>
                  <asp:TemplateField ControlStyle-BorderWidth="0"
                                     ControlStyle-BorderColor="White" 
                                     ControlStyle-BorderStyle="none" 
                                     ItemStyle-BorderWidth="0" 
                                     ItemStyle-BorderColor="White" 
                                     ItemStyle-BorderStyle="none"
                                     ItemStyle-Width="100%"
                                     HeaderStyle-Width="1" 
                                     HeaderStyle-BorderWidth="0" 
                                     HeaderStyle-BorderColor="White" 
                                     HeaderStyle-BorderStyle="none">
                    <ItemTemplate>
                      <div>
                        <h2>Equipment Manufacturer Model Details</h2>

                        <ul class="detailsList">
                          <%# "<li><strong>Product Name: </strong>" + Eval("ManufacturerModelName") + "</li>"%> 
                          <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerModelDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("ManufacturerModelDescription") + "</li>"%>
                          <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerName"))) ? "" : "<li><strong>Manufacturer: </strong>" + Eval("ManufacturerName") + "</li>"%>
                        </ul>
                      </div>
                    </ItemTemplate>                                             
                  </asp:TemplateField>
                </Fields>
              </asp:DetailsView>   

            </div>
                                    
            <!--EDIT EQUIPMENT PANEL -->                              
            <asp:Panel ID="pnlEditManufacturerModel" runat="server" Visible="false" DefaultButton="btnUpdateManufacturerModel">

              <div>
                <h2>Edit Equipment Manufacturer Model</h2>
              </div>  
            
              <div>

                <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server" />
                <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />

                <div class="divForm">
                  <label class="label">*Model Name:</label>
                  <asp:TextBox ID="txtEditManufacturerModelName" MaxLength="50" runat="server" />
                  <asp:HiddenField ID="hdnEditManufacturerModelName" runat="server" Visible="false" />                                    
                </div> 

                <div class="divForm">
                  <label class="label">Description:</label>
                  <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>
                  <div id="divEditDescriptionCharInfo"></div>
                </div>  

                <div class="divForm">   
                  <label class="label">*Manufacturer:</label>    
                  <asp:DropDownList ID="ddlEditManufacturer" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
                 <asp:HiddenField ID="hdnEditManufacturer" runat="server" Visible="false" /> 
                </div>
                                                                                                                                               
                <asp:LinkButton CssClass="lnkButton" ID="btnUpdateManufacturerModel" runat="server" Text="Edit Model"  OnClick="updateManufacturerModelButton_Click" ValidationGroup="EditManufacturerModel" />
                     
              </div>
                                    
              <!--Ajax Validators-->      
              <asp:RequiredFieldValidator ID="editManufacturerModelNameRequiredValidator" 
                                          runat="server" 
                                          ErrorMessage="Manufacturer Model Name is a required field." 
                                          ControlToValidate="txtEditManufacturerModelName" 
                                          SetFocusOnError="true" 
                                          Display="None" 
                                          ValidationGroup="EditManufacturerModel" />
              <ajaxToolkit:ValidatorCalloutExtender ID="editManufacturerModelNameRequiredValidatorExtender" 
                                                    runat="server" 
                                                    BehaviorID="editManufacturerModelNameRequiredValidatorExtender" 
                                                    TargetControlID="editManufacturerModelNameRequiredValidator" 
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175" />

              <asp:RequiredFieldValidator ID="editManufacturerRequiredValidator"
                                          runat="server"
                                          ErrorMessage="Manufacturer is a required field." 
                                          ControlToValidate="ddlEditManufacturer"  
                                          SetFocusOnError="true" 
                                          Display="None" 
                                          InitialValue="-1"
                                          ValidationGroup="EditManufacturerModel" />
              <ajaxToolkit:ValidatorCalloutExtender ID="editManufacturerRequiredValidatorExtender" 
                                                    runat="server"
                                                    BehaviorID="editManufacturerRequiredValidatorExtender" 
                                                    TargetControlID="editManufacturerRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175" />                                                                                                                                                                                                                       
            </asp:Panel>
                                                                                     
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">

            <asp:Panel ID="pnlAddManufacturerModel" runat="server" DefaultButton="btnAddManufacturerModel">

              <h2>Add Equipment Manufacturer Model</h2> 
            
              <div> 
  
                <a id="lnkSetFocusAdd" runat="server"></a>                                              
                <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server" />               
                                        
                <div class="divForm">
                  <label class="label">*Model Name:</label>
                  <asp:TextBox ID="txtAddManufacturerModelName" CssClass="textbox" Columns="50" MaxLength="50" runat="server" />                                      
                </div> 

                <div class="divForm">
                  <label class="label">Description:</label>
                  <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>
                  <div id="divAddDescriptionCharInfo"></div>
                </div> 
                   
                <div class="divForm">   
                  <label class="label">*Manufacturer:</label>    
                  <asp:DropDownList ID="ddlAddManufacturer" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                    <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                  </asp:DropDownList> 
                </div>
                                                                                                                                                        
                <asp:LinkButton CssClass="lnkButton" ID="btnAddManufacturerModel" runat="server" Text="Add Model"  OnClick="addManufacturerModelButton_Click" ValidationGroup="AddManufacturerModel" />

              </div>
                                    
              <!--Ajax Validators-->      
              <asp:RequiredFieldValidator ID="addManufacturerModelNameRequiredValidator" 
                                          runat="server"
                                          ErrorMessage="Manufacturer Model Name is a required field."
                                          ControlToValidate="txtAddManufacturerModelName"          
                                          SetFocusOnError="true"
                                          Display="None"
                                          ValidationGroup="AddManufacturerModel" />
              <ajaxToolkit:ValidatorCalloutExtender ID="addManufacturerModelNameRequiredValidatorExtender" 
                                                    runat="server"
                                                    BehaviorID="addManufacturerModelNameRequiredValidatorExtender"
                                                    TargetControlID="addManufacturerModelNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175" />

              <asp:RequiredFieldValidator ID="addManufacturerRequiredValidator" 
                                          runat="server"
                                          ErrorMessage="Manufacturer is a required field." 
                                          ControlToValidate="ddlAddManufacturer"  
                                          SetFocusOnError="true" 
                                          Display="None" 
                                          InitialValue="-1"
                                          ValidationGroup="AddManufacturerModel" />
              <ajaxToolkit:ValidatorCalloutExtender ID="addManufacturerRequiredValidatorExtender" 
                                                    runat="server"
                                                    BehaviorID="addManufacturerRequiredValidatorExtender" 
                                                    TargetControlID="addManufacturerRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175" />

            </asp:Panel>

          </telerik:RadPageView>
                                     
        </telerik:RadMultiPage>

      </div> <!--end of administrationControls div-->                                                            
      
</asp:Content>