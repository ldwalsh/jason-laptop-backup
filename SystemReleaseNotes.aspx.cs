﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using CW.Business;
using CW.Data;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Utility;
using Telerik.Web.UI;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class SystemReleaseNotes : SitePage
    {
        #region Properties

            private ReleaseNote mReleaseNote; 
            const string addReleaseNoteSuccess = " release note addition was successful.";
            const string addReleaseNoteFailed = "Adding release note failed. Please contact an administrator.";
            const string releaseNoteExists = "A release note with that build number already exists.";
            const string updateReleaseNoteSuccessful = "Release note update was successful.";
            const string updateReleaseNoteFailed = "Release note update failed. Please contact an administrator.";
            const string deleteReleaseNoteSuccessful = "Release note deletion was successful.";
            const string deleteReleaseNoteFailed = "Release note deletion failed. Please contact an administrator.";
            const string initialReleaseNotesSortDirection = "DESC";
            const string initialReleaseNotesSortExpression = "BuildNumber";

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewReleaseNotesSortDirection
            {
                get { return ViewState["ReleaseNotesSortDirection"] as string ?? string.Empty; }
                set { ViewState["ReleaseNotesSortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewReleaseNotesSortExpression
            {
                get { return ViewState["ReleaseNotesSortExpression"] as string ?? string.Empty; }
                set { ViewState["ReleaseNotesSortExpression"] = value; }
            }         
        
        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs system admin.
                if (!siteUser.IsKGSSystemAdmin)
                    Response.Redirect("/Home.aspx");   

                //hide labels
                lblReleaseNotesErrors.Visible = false;
                lblAddReleaseNotesError.Visible = false;
                lblEditReleaseNotesError.Visible = false;               
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindReleaseNotesTree();
                    
                    //Bound on tab change
                    //BindFAQs();

                    ViewState.Clear();
                    GridViewReleaseNotesSortDirection = initialReleaseNotesSortDirection;
                    GridViewReleaseNotesSortExpression = initialReleaseNotesSortExpression;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetReleaseNoteIntoEditForm(ReleaseNote releaseNote)
            {
                //ID
                hdnEditReleaseNotesReleaseNoteID.Value = Convert.ToString(releaseNote.ReleaseNoteID);
                //Major
                ddlEditReleaseNotesMajorVersion.SelectedValue = releaseNote.MajorVersion.ToString();
                //Minor
                ddlEditReleaseNotesMinorVersion.SelectedValue = releaseNote.MinorVersion.ToString();
                //BuildNumber
                txtEditReleaseNotesBuildNumber.Text = Convert.ToString(releaseNote.BuildNumber);
                //ReleaseDate
                txtEditReleaseDate.SelectedDate = releaseNote.ReleaseDate;
                //ReleaseNotes
                editorEditReleaseNotesReleaseNotes.Content = releaseNote.ReleaseNotes;
                //Comments
                editorEditReleaseNotesComments.Content = releaseNote.Comments;
            }

        #endregion

        #region Load and Bind Fields

        #endregion

        #region Load Release Note

            protected void LoadAddFormIntoReleaseNote(ReleaseNote releaseNote)
            {
                //Major
                releaseNote.MajorVersion = Convert.ToInt32(ddlAddReleaseNotesMajorVersion.SelectedValue);
                //Minor
                releaseNote.MinorVersion = Convert.ToInt32(ddlAddReleaseNotesMinorVersion.SelectedValue);
                //BuildNumber
                releaseNote.BuildNumber = Convert.ToInt32(txtAddReleaseNotesBuildNumber.Text);
                //ReleaseDate
                releaseNote.ReleaseDate = (DateTime)txtAddReleaseDate.SelectedDate;
                //ReleaseNotes
                releaseNote.ReleaseNotes = StringHelper.ClearRadEditorFirefoxBR(editorAddReleaseNotesReleaseNotes.Content); 
                //Comments
                releaseNote.Comments = StringHelper.ClearRadEditorFirefoxBR(editorAddReleaseNotesComments.Content); 

                releaseNote.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoReleaseNote(ReleaseNote releaseNote)
            {                         
                //ID
                releaseNote.ReleaseNoteID = Convert.ToInt32(hdnEditReleaseNotesReleaseNoteID.Value);
                //Major
                releaseNote.MajorVersion = Convert.ToInt32(ddlEditReleaseNotesMajorVersion.SelectedValue);
                //Minor
                releaseNote.MinorVersion = Convert.ToInt32(ddlEditReleaseNotesMinorVersion.SelectedValue);
                //BuildNumber
                releaseNote.BuildNumber = Convert.ToInt32(txtEditReleaseNotesBuildNumber.Text);
                //ReleaseDate
                releaseNote.ReleaseDate = (DateTime)txtEditReleaseDate.SelectedDate;
                //ReleaseNotes
                releaseNote.ReleaseNotes = StringHelper.ClearRadEditorFirefoxBR(editorEditReleaseNotesReleaseNotes.Content);
                //Comments
                releaseNote.Comments = StringHelper.ClearRadEditorFirefoxBR(editorEditReleaseNotesComments.Content);

                releaseNote.DateModified = DateTime.UtcNow;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add release note Button on click.
            /// </summary>
            protected void addReleaseNotesButton_Click(object sender, EventArgs e)
            {
                mReleaseNote = new ReleaseNote();

                //load the form into the release note
                LoadAddFormIntoReleaseNote(mReleaseNote);

                //check if build number exists
                if (!DataMgr.ReleaseNoteDataMapper.DoesReleaseNoteExist(null, mReleaseNote.BuildNumber))
                {
                    try
                    {
                        //insert new release note
                        DataMgr.ReleaseNoteDataMapper.InsertReleaseNote(mReleaseNote);

                        LabelHelper.SetLabelMessage(lblAddReleaseNotesError, String.Format("{0}.{1}.{2} {3}", mReleaseNote.MajorVersion, mReleaseNote.MinorVersion, mReleaseNote.BuildNumber, addReleaseNoteSuccess), lnkAddReleaseNotesSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding release note.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddReleaseNotesError, addReleaseNoteFailed, lnkAddReleaseNotesSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding release note.", ex);
                        LabelHelper.SetLabelMessage(lblAddReleaseNotesError, addReleaseNoteFailed, lnkAddReleaseNotesSetFocus);
                    }
                }
				else
				{
					//release note exists
                    LabelHelper.SetLabelMessage(lblAddReleaseNotesError, releaseNoteExists, lnkAddReleaseNotesSetFocus);
				}             
            }

            /// <summary>
            /// Update release note Button on click. Updates release note data.
            /// </summary>
            protected void updateReleaseNotesButton_Click(object sender, EventArgs e)
            {
                mReleaseNote = new ReleaseNote();

                //load the form into the release note
                LoadEditFormIntoReleaseNote(mReleaseNote);

                //check if build number exists
                if (!DataMgr.ReleaseNoteDataMapper.DoesReleaseNoteExist(mReleaseNote.ReleaseNoteID, mReleaseNote.BuildNumber))
                {
                    //try to update the release note
                    try
                    {
                        DataMgr.ReleaseNoteDataMapper.UpdateReleaseNote(mReleaseNote);

                        LabelHelper.SetLabelMessage(lblEditReleaseNotesError, updateReleaseNoteSuccessful, lnkEditReleaseNotesSetFocus);

                        //Bind release notes again
                        BindReleaseNotes();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditReleaseNotesError, updateReleaseNoteFailed, lnkEditReleaseNotesSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating faq section.", ex);
                    }
                }
                else
                {
                    //release note exists
                    LabelHelper.SetLabelMessage(lblEditReleaseNotesError, releaseNoteExists, lnkEditReleaseNotesSetFocus);
                }
            }          

        #endregion

        #region Tree Methods and Events

            /// <summary>
            /// Binds data to documents tree
            /// </summary>
            private void BindReleaseNotesTree()
            {
                releaseNotesTree.AppendDataBoundItems = true;

                //get release notes
                IEnumerable<ReleaseNote> releaseNotes = DataMgr.ReleaseNoteDataMapper.GetAllReleaseNotes();

                //major
                List<int> majorList = releaseNotes.Select(r => r.MajorVersion).Distinct().OrderByDescending(o => o).ToList();

                foreach (var major in majorList)
                {
                    var majorNode = new RadTreeNode(major.ToString(), null);
                    majorNode.PostBack = false;
                    
                    releaseNotesTree.Nodes.Add(majorNode);
                    

                    List<int> minorList = releaseNotes.Where(m => m.MajorVersion == major).Select(r => r.MinorVersion).Distinct().OrderByDescending(o => o).ToList();

                    foreach (var minor in minorList)
                    {
                        var minorNode = new RadTreeNode(minor.ToString(), null);
                        minorNode.PostBack = false;

                        majorNode.Nodes.Add(minorNode);
                        
                        List<int> buildList = releaseNotes.Where(m => m.MajorVersion == major && m.MinorVersion == minor).Select(r => r.BuildNumber).OrderByDescending(o => o).ToList();

                        foreach (var build in buildList)
                        {
                            //buildnumber is unique
                            var buildNode = new RadTreeNode(build.ToString(), releaseNotes.Where(r => r.BuildNumber == build).Select(s => s.ReleaseNoteID).First().ToString());

                            minorNode.Nodes.Add(buildNode);
                        }
                    }
                }
            }

            private void SelectNode(RadTreeNode node)
            {
                releaseNotesTreeNodeClick(releaseNotesTree, new RadTreeNodeEventArgs(node));

                node.Selected = true;

                node.Focus();
            }

            /// <summary>
            /// on reposity tree node click
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void releaseNotesTreeNodeClick(object sender, RadTreeNodeEventArgs e)
            {
                if (!String.IsNullOrEmpty(e.Node.Value))
                {
                    releaseNotesDetails.DataSource = DataMgr.ReleaseNoteDataMapper.GetReleaseNoteByIDAsEnumerable(Convert.ToInt32(e.Node.Value));
                    releaseNotesDetails.DataBind();
                }
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind appropriate grid after tab changed back from add tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind release notes to tree
                //if (radTabStrip.MultiPage.SelectedIndex == 0)
                //{
                    //BindReleaseNotesTree();
                //}

                //if active tab index is 1, bind release notes to grid
                if (radTabStrip.MultiPage.SelectedIndex == 1)
                {
                    BindReleaseNotes();
                }
            }

        #endregion

        #region Grid Events

            protected void gridReleaseNotes_OnDataBound(object sender, EventArgs e)
            {                         
            }

            protected void gridReleaseNotes_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridReleaseNotes_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditReleaseNotes.Visible = false;
                dtvReleaseNotes.Visible = true;

                int releaseNoteID = Convert.ToInt32(gridReleaseNotes.DataKeys[gridReleaseNotes.SelectedIndex].Values["ReleaseNoteID"]);

                //set data source
                dtvReleaseNotes.DataSource = DataMgr.ReleaseNoteDataMapper.GetReleaseNoteByIDAsEnumerable(releaseNoteID);
                //bind release note to details view
                dtvReleaseNotes.DataBind();
            }

            protected void gridReleaseNotes_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvReleaseNotes.Visible = false;
                pnlEditReleaseNotes.Visible = true;

                int releaseNoteID = Convert.ToInt32(gridReleaseNotes.DataKeys[e.NewEditIndex].Values["ReleaseNoteID"]);

                mReleaseNote = DataMgr.ReleaseNoteDataMapper.GetReleaseNoteByID(releaseNoteID);

                //Set release note data
                SetReleaseNoteIntoEditForm(mReleaseNote);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridReleaseNotes_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows sectionid
                int releaseNoteID = Convert.ToInt32(gridReleaseNotes.DataKeys[e.RowIndex].Value);

                try
                {
                    //delete release note
                    DataMgr.ReleaseNoteDataMapper.DeleteReleaseNote(releaseNoteID);

                    lblReleaseNotesErrors.Text = deleteReleaseNoteSuccessful;
                }
                catch (Exception ex)
                {
                    lblReleaseNotesErrors.Text = deleteReleaseNoteFailed;

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting release note.", ex);
                }

                lblReleaseNotesErrors.Visible = true;
                lnkReleaseNotesSetFocusView.Focus();

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryReleaseNotes());
                gridReleaseNotes.PageIndex = gridReleaseNotes.PageIndex;
                gridReleaseNotes.DataSource = ReleaseNoteSortDataTable(dataTable as DataTable, true);
                gridReleaseNotes.DataBind();

                SetGridReleaseNotesCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditReleaseNotes.Visible = false;
            }

            protected void gridReleaseNotes_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryReleaseNotes());

                //maintain current sort direction and expresstion on paging
                gridReleaseNotes.DataSource = ReleaseNoteSortDataTable(dataTable, true);
                gridReleaseNotes.PageIndex = e.NewPageIndex;
                gridReleaseNotes.DataBind();
            }

            protected void gridReleaseNotes_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridReleaseNotes.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryReleaseNotes());

                GridViewReleaseNotesSortExpression = e.SortExpression;

                gridReleaseNotes.DataSource = ReleaseNoteSortDataTable(dataTable, false);
                gridReleaseNotes.DataBind();
            }
       
        #endregion

        #region Helper Methods

            private IEnumerable<ReleaseNote> QueryReleaseNotes()
            {
                try
                {
                    //get all release notes
                    return DataMgr.ReleaseNoteDataMapper.GetAllReleaseNotes();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving release notes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving release notes.", ex);
                    return null;
                }
            }

            private void BindReleaseNotes()
            {
                //query release notes
                IEnumerable<ReleaseNote> releaseNotes = QueryReleaseNotes();

                int count = releaseNotes.Count();

                gridReleaseNotes.DataSource = releaseNotes;

                // bind grid
                gridReleaseNotes.DataBind();

                SetGridReleaseNotesCountLabel(count);
            }
        

            private void SetGridReleaseNotesCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblReleaseNotesResults.Text = String.Format("{0} release note found.", count);
                }
                else if (count > 1)
                {
                    lblReleaseNotesResults.Text = String.Format("{0} release notes found.", count);
                }
                else
                {
                    lblReleaseNotesResults.Text = "No release notes found.";
                }
            }
        

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetReleaseNotesSortDirection()
            {
                //switch sorting directions
                switch (GridViewReleaseNotesSortDirection)
                {
                    case "ASC":
                        GridViewReleaseNotesSortDirection = "DESC";
                        ViewState["ReleaseNotesSortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewReleaseNotesSortDirection = "ASC";
                        ViewState["ReleaseNotesSortDirection"] = "ASC";
                        break;
                }
                return GridViewReleaseNotesSortDirection;
            }

        
            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView ReleaseNoteSortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewReleaseNotesSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewReleaseNotesSortExpression, GridViewReleaseNotesSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewReleaseNotesSortExpression, GetReleaseNotesSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

        #endregion
    }
}

