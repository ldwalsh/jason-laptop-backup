﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="ProviderEquipmentTypeAdministration.aspx.cs" Inherits="CW.Website.ProviderEquipmentTypeAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/EquipmentTypes/nonkgs/ViewEquipmentTypes.ascx" tagname="ViewEquipmentTypes" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <h1>Provider Equipment Type Administration</h1>

  <div class="richText">The provider equipment type administration area is used to view equipment types.</div>

  <div class="updateProgressDiv">
    <asp:UpdateProgress ID="updateProgressTop" runat="server">
      <ProgressTemplate>
      <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
      </ProgressTemplate>
    </asp:UpdateProgress>
  </div>

  <div class="administrationControls">
    <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage">
      <Tabs>
        <telerik:RadTab Text="View Equipment Types" />
      </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

      <telerik:RadPageView ID="RadPageView1" runat="server">
        <CW:ViewEquipmentTypes ID="ViewEquipmentTypes" runat="server" />
      </telerik:RadPageView>

    </telerik:RadMultiPage>
  </div>

</asp:Content>