﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemReleaseNotes.aspx.cs" Inherits="CW.Website.SystemReleaseNotes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>System Release Notes</h1>                        
                <div class="richText">The system release notes area is used to view, add, and edit release notes.</div>                                 
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>                                       
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="Browse Release Notes"></telerik:RadTab>
                            <telerik:RadTab Text="View Release Notes"></telerik:RadTab>
                            <telerik:RadTab Text="Add Release Notes"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>Browse Release Notes</h2>
                                    <p>
                                    </p>

                                    <telerik:RadAjaxLoadingPanel ID="radAjaxLoadingBrowser" runat="server" Skin="Default" />
                                    <telerik:RadAjaxPanel ID="RadAjaxPanel3" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional" LoadingPanelID="radAjaxLoadingBrowser">                                  
                                                                                                                                                                                 
                                    <div class="releaseNotesTree">
                                        <telerik:RadTreeView runat="server" ID="releaseNotesTree" OnNodeClick="releaseNotesTreeNodeClick">                                                        
                                        </telerik:RadTreeView>
                                    </div>
                                    <div class="releaseNotesDetails">
                                        <asp:DetailsView ID="releaseNotesDetails" AutoGenerateRows="false" runat="server" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                                            <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div> 
                                                    <h3>Release Date: </h3><br />      
                                                    <div><%# Convert.ToDateTime(Eval("ReleaseDate")).ToShortDateString() %></div>
                                                    <br />
                                                    <hr />
                                                    <br />
                                                    <h3>Release Notes:</h3><br />                                                                                                        
                                                    <div class="richText"><asp:Literal ID="litReleaseNotes" runat="server" Text='<%# String.IsNullOrEmpty(Convert.ToString(Eval("ReleaseNotes"))) ? "" : Eval("ReleaseNotes").ToString()  %>'></asp:Literal></div> 
                                                    <br />
                                                    <hr />
                                                    <br />
                                                    <h3>Comments:</h3><br />   
                                                    <div class="richText"><asp:Literal ID="litComments" runat="server" Text='<%# String.IsNullOrEmpty(Convert.ToString(Eval("Comments"))) ? "" : Eval("Comments").ToString() %>'></asp:Literal></div> 
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            </Fields>
                                        </asp:DetailsView>
                                    </div>
                                    
                                    </telerik:RadAjaxPanel>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                    <h2>View Release Notes</h2>
                                    <p>
                                            <a id="lnkReleaseNotesSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblReleaseNotesResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblReleaseNotesErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>        
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridReleaseNotes"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="ReleaseNoteID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridReleaseNotes_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridReleaseNotes_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridReleaseNotes_Sorting"                                                                                                               
                                             OnSelectedIndexChanged="gridReleaseNotes_OnSelectedIndexChanged"
                                             OnRowEditing="gridReleaseNotes_Editing"     
                                             OnRowDeleting="gridReleaseNotes_Deleting"
                                             OnDataBound="gridReleaseNotes_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Major Version">  
                                                    <ItemTemplate><%# Eval("MajorVersion")%></ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Minor Version">  
                                                    <ItemTemplate><%# Eval("MinorVersion")%></ItemTemplate>
                                                </asp:TemplateField>                                                 
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Build Number">  
                                                    <ItemTemplate><%# Eval("BuildNumber")%></ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Release Date">  
                                                    <ItemTemplate><%# Convert.ToDateTime(Eval("ReleaseDate")).ToShortDateString() %></ItemTemplate>
                                                </asp:TemplateField>                                                                                                  
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this Release Notes permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br />       
                                    <div>                                                  
                                    <!--SELECT RELEASE NOTES DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvReleaseNotes" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>System Release Notes Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Major Version: </strong>" + Eval("MajorVersion") + "</li>"%> 
                                                            <%# "<li><strong>Minor Version: </strong>" + Eval("MinorVersion") + "</li>"%>
                                                            <%# "<li><strong>Build Number: </strong>" + Eval("BuildNumber") + "</li>"%>
                                                            <%# "<li><strong>Release Date: </strong>" + Convert.ToDateTime(Eval("ReleaseDate")).ToShortDateString() + "</li>"%>
                                                            <li><strong>Release Notes: </strong><div class="richText"><asp:Literal runat="server" Text='<%# String.IsNullOrEmpty(Convert.ToString(Eval("ReleaseNotes"))) ? "" : Eval("ReleaseNotes").ToString() %>'></asp:Literal></div></li>
                                                            <li><strong>Comments: </strong><div class="richText"><asp:Literal runat="server" Text='<%# String.IsNullOrEmpty(Convert.ToString(Eval("Comments"))) ? "" : Eval("Comments").ToString() %>'></asp:Literal></div></li>                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT RELEASE NOTE PANEL -->                              
                                    <asp:Panel ID="pnlEditReleaseNotes" runat="server" Visible="false" DefaultButton="btnUpdateReleaseNotes">                                                                                             
                                        <div>
                                            <h2>Edit Release Notes</h2>
                                        </div>  
                                        <div>      
                                            <a id="lnkEditReleaseNotesSetFocus" runat="server"></a>                                                  
                                            <asp:Label ID="lblEditReleaseNotesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditReleaseNotesReleaseNoteID" runat="server" Visible="false" />
                                            <div class="divFormWidest">
                                                <label class="labelNarrowest">*Major Version:</label>
                                                <asp:DropDownList ID="ddlEditReleaseNotesMajorVersion" CssClass="dropdown" runat="server">
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>                       
                                            </div> 
                                            <div class="divFormWidest">
                                                <label class="labelNarrowest">*Minor Version:</label>
                                                <asp:DropDownList ID="ddlEditReleaseNotesMinorVersion" CssClass="dropdown" runat="server">
                                                    <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                </asp:DropDownList>                                      
                                            </div> 
                                            <div class="divFormWidest">
                                                <label class="labelNarrowest">*Build Number:</label>
                                                <asp:TextBox ID="txtEditReleaseNotesBuildNumber" CssClass="textbox" MaxLength="4" runat="server"></asp:TextBox>                                  
                                            </div>
                                            <div class="divFormWidest">
                                                <label class="labelNarrowest">*Release Date:</label>
                                                <telerik:RadDatePicker ID="txtEditReleaseDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                
                                            </div>
                                            <div class="divFormWidest">
                                                <label class="labelNarrowest">*Release Notes:</label>
                                                <telerik:RadEditor ID="editorEditReleaseNotesReleaseNotes" 
                                                    runat="server" 
                                                    CssClass="editor"                                                  
                                                    Height="475px" 
                                                    Width="674px"
                                                    MaxHtmlLength="5000"
                                                    NewLineMode="Div"
                                                    ToolsWidth="676px"                                                                                                                                       
                                                    ToolbarMode="ShowOnFocus"        
                                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                                    >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                                </telerik:RadEditor>
                                            </div>
                                            <div class="divFormWidest">
                                                <label class="labelNarrowest">Comments:</label>
                                                <telerik:RadEditor ID="editorEditReleaseNotesComments" 
                                                    runat="server" 
                                                    CssClass="editor"                                                  
                                                    Height="475px" 
                                                    Width="674px"
                                                    MaxHtmlLength="30000"
                                                    NewLineMode="Div"
                                                    ToolsWidth="676px"                                                                                                                                       
                                                    ToolbarMode="ShowOnFocus"        
                                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                               
                                                    >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles> 
                                                </telerik:RadEditor>
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateReleaseNotes" runat="server" Text="Update Release Notes"  OnClick="updateReleaseNotesButton_Click" ValidationGroup="EditReleaseNotes"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editReleaseNotesMajorVersionRequiredValidator" runat="server"
                                        ErrorMessage="Major Version is a required field."
                                        ControlToValidate="ddlEditReleaseNotesMajorVersion"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditReleaseNotes">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editReleaseNotesMajorVersionRequiredValidatorExtender" runat="server"
                                        TargetControlID="editReleaseNotesMajorVersionRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                    <asp:RequiredFieldValidator ID="editReleaseNotesMinorVersionRequiredValidator" runat="server"
                                        ErrorMessage="Minor Version is a required field."
                                        ControlToValidate="ddlEditReleaseNotesMinorVersion"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditReleaseNotes">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editReleaseNotesMinorVersionRequiredValidatorExtender" runat="server"
                                        TargetControlID="editReleaseNotesMinorVersionRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                            
                                    <ajaxToolkit:FilteredTextBoxExtender ID="editReleaseNotesBuildNumberFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtEditReleaseNotesBuildNumber"         
                                            FilterType="Numbers"
                                            />   
                                    <asp:RequiredFieldValidator ID="editReleaseNotesBuildNumberRequiredValidator" runat="server"
                                        ErrorMessage="Build Number is a required field."
                                        ControlToValidate="txtEditReleaseNotesBuildNumber"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditReleaseNotes">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editReleaseNotesBuildNumberRequiredValidatorExtender" runat="server"
                                        TargetControlID="editReleaseNotesBuildNumberRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                    
                                    <asp:RequiredFieldValidator ID="editReleaseNotesDateRequiredValidator" runat="server"
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtEditReleaseDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditReleaseNotes">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editReleaseNotesDateRequiredValidatorExtender" runat="server"
                                        TargetControlID="editReleaseNotesDateRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                        
                                    </asp:Panel>                                                                                               
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView3" runat="server">
                                <asp:Panel ID="pnlAddReleaseNotes" runat="server" DefaultButton="btnAddReleaseNotes">
                                    <h2>Add Release Notes</h2> 
                                    <div>        
                                        <a id="lnkAddReleaseNotesSetFocus" runat="server"></a>                                            
                                        <asp:Label ID="lblAddReleaseNotesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divFormWidest">
                                                <label class="labelNarrowest">*Major Version:</label>
                                                <asp:DropDownList ID="ddlAddReleaseNotesMajorVersion" CssClass="dropdown" runat="server">
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>                                                   
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>                       
                                        </div> 
                                        <div class="divFormWidest">
                                                <label class="labelNarrowest">*Minor Version:</label>
                                                <asp:DropDownList ID="ddlAddReleaseNotesMinorVersion" CssClass="dropdown" runat="server">
                                                    <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                    <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                    <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                    <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                    <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                    <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                    <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                    <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                </asp:DropDownList>                                      
                                        </div> 
                                        <div class="divFormWidest">
                                            <label class="labelNarrowest">*Build Number:</label>
                                            <asp:TextBox ID="txtAddReleaseNotesBuildNumber" CssClass="textbox" MaxLength="4" runat="server"></asp:TextBox>                                  
                                        </div>                                        
                                        <div class="divFormWidest">
                                            <label class="labelNarrowest">*Release Date:</label>
                                            <telerik:RadDatePicker ID="txtAddReleaseDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                                                            
                                        </div>
                                        <div class="divFormWidest">
                                            <label class="labelNarrowest">*Release Notes:</label>
                                            <telerik:RadEditor ID="editorAddReleaseNotesReleaseNotes" 
                                                    runat="server" 
                                                    CssClass="editor"                                                  
                                                    Height="475px" 
                                                    Width="674px"
                                                    MaxHtmlLength="5000"
                                                    NewLineMode="Div"
                                                    ToolsWidth="676px"                                                                                                                                       
                                                    ToolbarMode="ShowOnFocus"        
                                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                                    >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                                            </telerik:RadEditor>
                                        </div>
                                        <div class="divFormWidest">
                                            <label class="labelNarrowest">Comments:</label>
                                            <telerik:RadEditor ID="editorAddReleaseNotesComments" 
                                                    runat="server" 
                                                    CssClass="editor"                                                  
                                                    Height="475px" 
                                                    Width="674px"
                                                    MaxHtmlLength="30000"
                                                    NewLineMode="Div"
                                                    ToolsWidth="676px"                                                                                                                                       
                                                    ToolbarMode="ShowOnFocus"        
                                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                                    >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles> 
                                            </telerik:RadEditor>
                                        </div>                                                 
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddReleaseNotes" runat="server" Text="Add Release Notes"  OnClick="addReleaseNotesButton_Click" ValidationGroup="AddReleaseNotes"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addReleaseNotesMajorVersionRequiredValidator" runat="server"
                                        ErrorMessage="Major Version is a required field."
                                        ControlToValidate="ddlAddReleaseNotesMajorVersion"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddReleaseNotes">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addReleaseNotesMajorVersionRequiredValidatorExtender" runat="server"
                                        TargetControlID="addReleaseNotesMajorVersionRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                    <asp:RequiredFieldValidator ID="addReleaseNotesMinorVersionRequiredValidator" runat="server"
                                        ErrorMessage="Minor Version is a required field."
                                        ControlToValidate="ddlAddReleaseNotesMinorVersion"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddReleaseNotes">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addReleaseNotesMinorVersionRequiredValidatorExtender" runat="server"
                                        TargetControlID="addReleaseNotesMinorVersionRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                          
                                    <ajaxToolkit:FilteredTextBoxExtender ID="addReleaseNotesBuildNumberFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtAddReleaseNotesBuildNumber"         
                                            FilterType="Numbers"
                                            />   
                                    <asp:RequiredFieldValidator ID="addReleaseNotesBuildNumberRequiredValidator" runat="server"
                                        ErrorMessage="Build Number is a required field."
                                        ControlToValidate="txtAddReleaseNotesBuildNumber"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddReleaseNotes">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addReleaseNotesBuildNumberRequiredValidatorExtender" runat="server"
                                        TargetControlID="addReleaseNotesBuildNumberRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                    
                                    <asp:RequiredFieldValidator ID="addReleaseNotesDateRequiredValidator" runat="server"
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtAddReleaseDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddReleaseNotes">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addReleaseNotesDateRequiredValidatorExtender" runat="server"
                                        TargetControlID="addReleaseNotesDateRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                              

                                </asp:Panel>                                                                                                                                                                  
                            </telerik:RadPageView>                           
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>