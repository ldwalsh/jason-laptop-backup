﻿<%@ Page Language="C#" Async="true" AsyncTimeout="300000" EnableEventValidation="false" MasterPageFile="~/_masters/Main.master" AutoEventWireup="false" CodeBehind="Home.aspx.cs" Inherits="CW.Website.Home" %>
<%@ Register src="~/_controls/LeftNavResponsive.ascx" tagname="LeftNavResponsive" tagprefix="CW" %>
<%@ Register src="~/_controls/DefaultWidgets.ascx" tagname="DefaultWidgets" tagprefix="CW" %>
<%@ Register src="~/_controls/ProviderWidgets.ascx" tagname="ProviderWidgets" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <asp:Panel ID="pnlLoggedOut" runat="server">    
    <div class="containerHomeLoggedOut" runat="server">
      <div class="contentLoggedOut">
        <h1 id="h1HomePageHeaderLoggedOut" runat="server"></h1>        
        <div class="richText">
          <asp:Literal ID="litHomePageBodyLoggedOut" runat="server" />
        </div>
      </div>
    </div>
  </asp:Panel>

  <asp:Panel ID="pnlLoggedIn" runat="server">
         
    <div class="containerHome" runat="server">

      <CW:LeftNavResponsive ID="LeftNavResponsive" runat="server" />

      <div class="divSecondaryLogo"></div>

      <div class="content"> 
               
        <div class="top"></div>
            
        <div class="middle">        
            
              <asp:Panel  ID="pnlHomePageContent" runat="server">
                <h1 id="h1HomePageHeader" runat="server" />
                <div class="richText">
                  <asp:Literal ID="litHomePageBody" runat="server" />
                </div>
                <hr />
              </asp:Panel>

              <script type="text/javascript" src="_assets/scripts/widgets.js"></script>

              <asp:MultiView ID="mvHomePage" runat="server" ActiveViewIndex="0">
                <asp:View ID="viewDefault" runat= "server">
                  <div class="providerClientViewLink">
                    <asp:LinkButton ID="lnkProviderView" runat="server" CommandArgument="viewProvider" CommandName="SwitchViewByID" Text="switch to provider view" Visible="false" CssClass="lnk" OnCommand="SetContentVisibility" />
                  </div>
                  <CW:DefaultWidgets ID="DefaultWidgets" runat="server" />
                </asp:View>

                <asp:View ID="viewProvider" runat= "server">
                  <div class="providerClientViewLink">
                    <asp:LinkButton ID="lnkClientView" runat="server" CommandArgument="viewDefault" CommandName="SwitchViewByID" Text="switch to client view" Visible="false" CssClass="lnk" OnCommand="SetContentVisibility" />
                  </div>
                  <CW:ProviderWidgets ID="ProviderWidgets" runat="server" />
                </asp:View>
              </asp:MultiView>

             <div style="display: block; clear: both;"></div>  

        </div> <!--end of (outer) middle div-->

      </div> <!--end of content div-->

    </div> <!--end of containerHome div--> 
 
  </asp:Panel>

</asp:Content>