﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemCache.aspx.cs" Inherits="CW.Website.SystemCache" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Data.Models.Alarm" %>
<%@ Import Namespace="CW.Data.Models.Diagnostics" %>
<%@ Import Namespace="CW.Website" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

             <h1>System Cache</h1>
             <div class="richText">
                The system cache page is used to manage system cache. Below is a grid showing the cache status of diagnostics by building.  Click "Refresh" to reload analyses from storage into cache for a building.
             </div>
             <div class="updateProgressDiv">
                 <asp:UpdateProgress ID="updateProgressTop" runat="server">
                     <ProgressTemplate>
                         <img src="_assets/images/Pik4F3300.gif" alt="loading" />
                         <span>Loading...</span>
                     </ProgressTemplate>
                 </asp:UpdateProgress>
             </div>
             <div class="administrationControls">
                 <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage">
                     <Tabs>
                         <telerik:RadTab Text="Diagnostics Cache">
                         </telerik:RadTab>
                         <telerik:RadTab Text="Kiosk Cache">
                         </telerik:RadTab>
                         <telerik:RadTab Text="Alarm Cache">
                         </telerik:RadTab>
                         <telerik:RadTab Text="Interpreter Cache">
                         </telerik:RadTab>
                         <telerik:RadTab Text="Cache Performance">
                         </telerik:RadTab>
                     </Tabs>
                 </telerik:RadTabStrip>
                 <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                     <telerik:RadPageView ID="RadPageView1" runat="server">
                         <p>
                             <a id="lnkSetFocusView" href="#" runat="server"></a>
                             <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                             <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                         </p>
                         <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                             <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewDiagnosticsAll_Click"></asp:LinkButton>
                             <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                             <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchDiagonsticsButton_Click"/>
                         </asp:Panel>
                         <div id="gridTbl">
                             <asp:GridView ID="gridBuildingDiagnosticsCache" 
                        EnableViewState="true" 
                        runat="server" 
                        DataKeyNames="CID,ClientName,BID,BuildingName,StartDate,EndDate,AnalysisRange" 
                        GridLines="None"
                        PageSize="20" PagerSettings-PageButtonCount="20"   
                        OnRowCreated="gridBuildingDiagnosticsCache_OnRowCreated"                                                                                                                                                 
                        AllowPaging="true"  OnPageIndexChanging="gridBuildingDiagnosticsCache_PageIndexChanging"
                        AllowSorting="true"  OnSorting="gridBuildingDiagnosticsCache_Sorting"  
                        OnRowDeleting="gridBuildingDiagnosticsCache_Deleting"
                        OnDataBound="gridBuildingDiagnosticsCache_OnDataBound"
                        AutoGenerateColumns="false" 
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1" 
                        AlternatingRowStyle-CssClass="tblCol2" 
                        RowStyle-Wrap="true" 
                        onselectedindexchanged="gridBuildingDiagnosticsCache_SelectedIndexChanged">
                                 <Columns>
                                     <asp:CommandField ShowSelectButton="true" SelectText="Display Count"  />
                                     <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<BuildingDiagnosticsCacheStatus>(d=>d.ClientName)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<BuildingDiagnosticsCacheStatus>(d=>d.BuildingName)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Analysis Range">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<BuildingDiagnosticsCacheStatus>(d=>d.AnalysisRange)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Start Date">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<BuildingDiagnosticsCacheStatus>(d=>d.StartDate)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="End Date">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<BuildingDiagnosticsCacheStatus>(d=>d.EndDate)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <%--<asp:TemplateField HeaderText="Has Data?">  
                                <ItemTemplate><%# Eval("HasData")%></ItemTemplate>                                                  
                            </asp:TemplateField>--%>
                                     <asp:TemplateField>
                                         <ItemTemplate>
                                             <asp:LinkButton ID="LinkButton2" Runat="server" CausesValidation="false"
                                            OnClientClick="return confirm('Are you sure you wish to refresh the cache?');"
                                            CommandName="Delete">Refresh</asp:LinkButton>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                 </Columns>
                             </asp:GridView>
                         </div>
                     </telerik:RadPageView>
                     <telerik:RadPageView ID="RadPageView2" runat="server">
                         <p>
                             <asp:Label ID="lblKioskResults" runat="server" Text=""></asp:Label>
                             <asp:Label ID="lblKioskErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                         </p>
                         <div id="gridTbl">
                             <asp:GridView ID="gridKioskCache" 
                        EnableViewState="true" 
                        runat="server" 
                        DataKeyNames="CID,ClientName,TypeID" 
                        GridLines="None"
                        PageSize="20" PagerSettings-PageButtonCount="20"   
                        OnRowCreated="gridKioskCache_OnRowCreated"                                                                                                                                                 
                        AllowPaging="true"  OnPageIndexChanging="gridKioskCache_PageIndexChanging"
                        AllowSorting="true"  OnSorting="gridKioskCache_Sorting"  
                        OnRowDeleting="gridKioskCache_Deleting"
                        OnDataBound="gridKioskCache_OnDataBound"
                        AutoGenerateColumns="false" 
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1" 
                        AlternatingRowStyle-CssClass="tblCol2" 
                        RowStyle-Wrap="true" 
                        onselectedindexchanged="gridKioskCache_SelectedIndexChanged">
                                 <Columns>
                                     <asp:CommandField ShowSelectButton="true" SelectText="Display Count"  />
                                     <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Label" HeaderText="Label">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<BuildingDiagnosticsCacheStatus>(d=>d.Label)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<BuildingDiagnosticsCacheStatus>(d=>d.ClientName)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField>
                                         <ItemTemplate>
                                             <asp:LinkButton ID="LinkButton2" Runat="server" CausesValidation="false"
                                            OnClientClick="return confirm('Are you sure you wish to refresh the cache?');"
                                            CommandName="Delete">Refresh</asp:LinkButton>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                 </Columns>
                             </asp:GridView>
                         </div>
                     </telerik:RadPageView>
                     <telerik:RadPageView ID="RadPageView3" runat="server">
                         <p>
                             <asp:Label ID="lblAlarmResults" runat="server" Text=""></asp:Label>
                             <asp:Label ID="lblAlarmErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                         </p>
                         <asp:Panel ID="pnlAlarmSearch" runat="server" DefaultButton="searchAlarmBtn" CssClass="divSearch">
                             <asp:LinkButton id="viewAllAlarm" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAlarmAll_Click"></asp:LinkButton>
                             <asp:TextBox ID="txtAlarmSearch" runat="server" CssClass="textbox"></asp:TextBox>
                             <asp:LinkButton CssClass="lnk-search-button" ID="searchAlarmBtn" runat="server" ValidationGroup="Search" OnClick="searchAlarmButton_Click"/>
                         </asp:Panel>
                         <div id="gridTbl">
                             <asp:GridView ID="gridAlarmCache" 
                        EnableViewState="true" 
                        runat="server" 
                        DataKeyNames="CID,ClientName,StartDate,EndDate,Duration" 
                        GridLines="None"
                        PageSize="20" PagerSettings-PageButtonCount="20"   
                        OnRowCreated="gridBuildingDiagnosticsCache_OnRowCreated"                                                                                                                                                 
                        AllowPaging="true"  OnPageIndexChanging="gridAlarmCache_PageIndexChanging"
                        AllowSorting="true"  OnSorting="gridAlarmCache_Sorting"  
                        OnRowDeleting="gridAlarmCache_Deleting"
                        OnDataBound="gridAlarmCache_OnDataBound"
                        AutoGenerateColumns="false" 
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1" 
                        AlternatingRowStyle-CssClass="tblCol2" 
                        RowStyle-Wrap="true" 
                        onselectedindexchanged="gridAlarmCache_SelectedIndexChanged">
                                 <Columns>
                                     <asp:CommandField ShowSelectButton="true" SelectText="Display Count"  />
                                     <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<AlarmCacheStatus>(a=>a.ClientName)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Duration">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<AlarmCacheStatus>(a=>a.Duration)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Start Date">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<AlarmCacheStatus>(a=>a.StartDate)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="End Date">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<AlarmCacheStatus>(a=>a.EndDate)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField>
                                         <ItemTemplate>
                                             <asp:LinkButton ID="LinkButton2" Runat="server" CausesValidation="false"
                                            OnClientClick="return confirm('Are you sure you wish to refresh the cache?');"
                                            CommandName="Delete">Refresh</asp:LinkButton>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                 </Columns>
                             </asp:GridView>
                         </div>
                     </telerik:RadPageView>
                     <telerik:RadPageView ID="RadPageView4" runat="server">
                         <h2>Interpreter Cache</h2>
                         <p>
                             <asp:Label ID="lblInterpreterResults" runat="server" Text=""></asp:Label>
                             <asp:Label ID="lblInterpreterError" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>
                         </p>
                         <div class="divForm">
                             <label class="label">
                             Select Client:</label>
                             <asp:DropDownList CssClass="dropdown" id="ddlClients" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlClients_SelectedIndexChanged" AutoPostBack="true">
                                 <asp:ListItem Value="-1">Select...</asp:ListItem>
                             </asp:DropDownList>
                         </div>
                         <div class="divForm">
                             <label class="label">
                             Select Data Source:</label>
                             <asp:DropDownList CssClass="dropdown" id="ddlDataSources" AppendDataBoundItems="true" runat="server">
                                 <asp:ListItem Value="-1">All</asp:ListItem>
                             </asp:DropDownList>
                         </div>
                         <asp:LinkButton CssClass="lnkButton" runat="server" ID="btnInterpreterSubmit" Text="Submit" OnClick="btnInterpreterCacheSubmit_Click" />
                     </telerik:RadPageView>
                     <telerik:RadPageView ID="RadPageView5" runat="server">
                         <h2>Cache Performance</h2>
                         <p>
                             <asp:Label ID="lblViewResults" runat="server" Text=""></asp:Label>
                             <asp:Label ID="lblViewError" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>
                         </p>
                         <div class="divForm">
                             <label class="label">
                             Select Building:</label>
                             <asp:DropDownList CssClass="dropdown" id="ddlBuildings" AppendDataBoundItems="true" runat="server">
                                 <asp:ListItem Value="-1">All</asp:ListItem>
                             </asp:DropDownList>
                         </div>
                         <div class="divForm">
                             <label class="label">
                             Cache Type:</label>
                             <asp:DropDownList CssClass="dropdown" id="ddlCacheTypes" AppendDataBoundItems="true" runat="server">
                                 <asp:ListItem Value="-1">Select...</asp:ListItem>
                             </asp:DropDownList>
                         </div>
                         <asp:LinkButton CssClass="lnkButton" runat="server" ID="btnSubmit" Text="Submit" OnClick="btnViewCacheSubmit_Click" />
                         <%--<asp:Panel ID="pnlViewCacheSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                        <asp:LinkButton id="lnkViewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewCacheAll_Click"></asp:LinkButton>
                        <asp:TextBox ID="txtViewSearch" runat="server" CssClass="textbox"></asp:TextBox>
                        <asp:LinkButton CssClass="lnk-search-button" ID="lnkSearchBtn" runat="server" ValidationGroup="Search" OnClick="searchCacheButton_Click"/>   
                    </asp:Panel>  --%>
                         <div id="gridTbl" style="padding-top:5px">
                             <asp:GridView ID="gridViewCache" 
                        EnableViewState="true" 
                        runat="server" 
                        DataKeyNames="ClientName, CID, BuildingName, BID" 
                        GridLines="None"
                        PageSize="20" PagerSettings-PageButtonCount="20"   
                        OnRowCreated="gridBuildingDiagnosticsCache_OnRowCreated"                                                                                                                                                 
                        AllowPaging="false"  OnPageIndexChanging="gridViewCache_PageIndexChanging"
                        AllowSorting="false"  OnSorting="gridViewCache_Sorting"  
                        OnDataBound="gridViewCache_OnDataBound"
                        AutoGenerateColumns="false" 
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1" 
                        AlternatingRowStyle-CssClass="tblCol2" 
                        RowStyle-Wrap="true" >
                                 <Columns>
                                     <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<CacheOperation>(a=>a.BuildingName)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Cache Type">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<CacheOperation>(a=>a.CacheType)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Range">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<CacheOperation>(a=>a.Range)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Time Elasped (in ms)">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<CacheOperation>(a=>a.TimeElasped)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Size (in bytes)">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<CacheOperation>(a=>a.Size))%>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Record Count">
                                         <ItemTemplate>
                                             <%# Eval(PropHelper.G<CacheOperation>(a=>a.Rows)) %>
                                         </ItemTemplate>
                                     </asp:TemplateField>
                                 </Columns>
                             </asp:GridView>
                         </div>
                     </telerik:RadPageView>
                 </telerik:RadMultiPage>
             </div>
    
</asp:Content>                