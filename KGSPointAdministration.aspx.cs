﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.Points;
using System;

namespace CW.Website
{
	public partial class KGSPointAdministration: AdminSitePageTemp
    {
        public enum TabMessages
        {
            AddPoint,
            EditPoint,
            DeletePoint
        }

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewPoints).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.KGS; } }

            #endregion

        #endregion
    }
}