﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.FileType;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSFileTypeAdministration : SitePage
    {
        #region Properties

            private FileType mFileType;
          
            const string addFileTypeSuccess = " file type addition was successful.";
            const string addFileTypeFailed = "Adding file type failed. Please contact an administrator.";
            const string fileTypeExists = "A file type with that name already exists.";
            protected string selectedValue;
            const string updateSuccessful = "File type update was successful.";
            const string updateFailed = "File type update failed. Please contact an administrator.";
            const string deleteSuccessful = "File type deletion was successful.";
            const string deleteFailed = "File type deletion failed. Please contact an administrator.";
            const string fileExists = "Cannot delete file type because one or more files are associated. Please disassociate from files first.";    
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "FileTypeName";
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //datamanger in sitepage

                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindFileTypes();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindFileClasses(ddlAddFileClass, ddlEditFileClass);

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetFileTypeIntoEditForm(FileType fileType)
            {
                //ID
                hdnEditID.Value = Convert.ToString(fileType.FileTypeID);
                //File Type Name
                txtEditFileTypeName.Text = fileType.FileTypeName;
                //File Class
                ddlEditFileClass.SelectedValue = Convert.ToString(fileType.FileClassID);

                //File Variable Description
                txtEditDescription.Value = String.IsNullOrEmpty(fileType.FileTypeDescription) ? null : fileType.FileTypeDescription;
            }

       #endregion

        #region Load and Bind Fields

            private void BindFileClasses(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<FileClass> classes = DataMgr.FileClassDataMapper.GetAllFileClasses();

                ddl.DataTextField = "FileClassName";
                ddl.DataValueField = "FileClassID";
                ddl.DataSource = classes;
                ddl.DataBind();

                ddl2.DataTextField = "FileClassName";
                ddl2.DataValueField = "FileClassID";
                ddl2.DataSource = classes;
                ddl2.DataBind();
            }

        #endregion

        #region Load File Type

            protected void LoadAddFormIntoFileType(FileType fileType)
            {
                //File Type Name
                fileType.FileTypeName = txtAddFileTypeName.Text;
                //File Class
                fileType.FileClassID = Convert.ToInt32(ddlAddFileClass.SelectedValue);
                //Description
                fileType.FileTypeDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                fileType.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoFileType(FileType fileType)
            {
                //ID
                fileType.FileTypeID = Convert.ToInt32(hdnEditID.Value);
                //File Type Name
                fileType.FileTypeName = txtEditFileTypeName.Text;
                fileType.FileTypeDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                fileType.FileClassID = Convert.ToInt32(ddlEditFileClass.SelectedValue);
                fileType.DateModified = DateTime.UtcNow;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add file type Button on click.
            /// </summary>
            protected void addFileTypeButton_Click(object sender, EventArgs e)
            {
                //check that file type does not already exist
                if (!DataMgr.FileTypeDataMapper.DoesFileTypeExist(txtAddFileTypeName.Text))
                {
                    mFileType = new FileType();

                    //load the form into the file type
                    LoadAddFormIntoFileType(mFileType);

                    try
                    {
                        //insert new file type
                        DataMgr.FileTypeDataMapper.InsertFileType(mFileType);

                        LabelHelper.SetLabelMessage(lblAddError, mFileType.FileTypeName + addFileTypeSuccess, lnkSetFocusAdd);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding file type", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addFileTypeFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding file type", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addFileTypeFailed, lnkSetFocusAdd);
                    }
                }
                else
                {
                    //file type exists
                    LabelHelper.SetLabelMessage(lblAddError, fileTypeExists, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update file type Button on click. Updates file type data.
            /// </summary>
            protected void updateFileTypeButton_Click(object sender, EventArgs e)
            {
                FileType mFileType = new FileType();

                //load the form into the file type
                LoadEditFormIntoFileType(mFileType);

                //check if the filetype name exists for another filetype if trying to change
                if (DataMgr.FileTypeDataMapper.DoesFileTypeExist(mFileType.FileTypeID, mFileType.FileTypeName))
                {
                    LabelHelper.SetLabelMessage(lblEditError, fileTypeExists, lnkSetFocusView);
                }
                //update the file type
                else
                {
                    //try to update the file type              
                    try
                    {
                        DataMgr.FileTypeDataMapper.UpdateFileType(mFileType);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                        //Bind file types again
                        BindFileTypes();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating file type.", ex);
                    }
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindFileTypes();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindFileTypes();
            }

        #endregion

        #region Dropdown events

            /// <summary>
            /// On initialize of the file dropdown, bind all file classes
            /// Do not reload on autopostback.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlEditFileClass_OnInit(object sender, EventArgs e)
            {
                DropDownList ddl = (DropDownList)sender;

                ddl.DataTextField = "FileClassName";
                ddl.DataValueField = "FileClassID";
                ddl.DataSource = DataMgr.FileClassDataMapper.GetAllFileClasses();
                ddl.DataBind();

                ddl.SelectedValue = selectedValue;
            }

         #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind file type grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind file classes to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindFileTypes();
                }
            }

        #endregion

        #region Grid Events

            protected void gridFileTypes_OnDataBound(object sender, EventArgs e)
            {                           
            }

            protected void gridFileTypes_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }
            protected void gridFileTypes_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditFileType.Visible = false;
                dtvFileType.Visible = true;
                
                int fileTypeID = Convert.ToInt32(gridFileTypes.DataKeys[gridFileTypes.SelectedIndex].Values["FileTypeID"]);

                //set data source
                dtvFileType.DataSource = DataMgr.FileTypeDataMapper.GetFullFileTypeByFileTypeID(fileTypeID);
                //bind file type to details view
                dtvFileType.DataBind();
            }
            protected void gridFileTypes_Editing(object sender, GridViewEditEventArgs e)
            {
                pnlEditFileType.Visible = true;
                dtvFileType.Visible = false;

                int fileTypeID = Convert.ToInt32(gridFileTypes.DataKeys[e.NewEditIndex].Values["FileTypeID"]);

                FileType mFileType = DataMgr.FileTypeDataMapper.GetFileTypeWithFileClass(fileTypeID);

                //Set File Type data
                SetFileTypeIntoEditForm(mFileType);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridFileTypes_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows FileClassid
                int fileTypeID = Convert.ToInt32(gridFileTypes.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a file type if no file has been associated.                    

                    //check if a file is associated to that file type
                    bool hasFile = DataMgr.FileDataMapper.IsFileAssociatedWithFileType(fileTypeID);

                    if (hasFile)
                    {
                        lblErrors.Text = fileExists;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //delete file type
                        DataMgr.FileTypeDataMapper.DeleteFileType(fileTypeID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting file type.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryFileTypes());
                gridFileTypes.PageIndex = gridFileTypes.PageIndex;
                gridFileTypes.DataSource = SortDataTable(dataTable as DataTable, true);
                gridFileTypes.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditFileType.Visible = false;
            }

            protected void gridFileTypes_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedFileTypes(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridFileTypes.DataSource = SortDataTable(dataTable, true);
                gridFileTypes.PageIndex = e.NewPageIndex;
                gridFileTypes.DataBind();
            }

            protected void gridFileTypes_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridFileTypes.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedFileTypes(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridFileTypes.DataSource = SortDataTable(dataTable, false);
                gridFileTypes.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetFileTypeData> QueryFileTypes()
            {
                try
                {
                    //get all file types 
                    return DataMgr.FileTypeDataMapper.GetAllFullFileTypes();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving file types.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving file types.", ex);
                    return null;
                }
            }

            private IEnumerable<GetFileTypeData> QuerySearchedFileTypes(string searchText)
            {
                try
                {
                    //get all file types 
                    return DataMgr.FileTypeDataMapper.GetAllFullSearchedFileTypes(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving file types.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving file types.", ex);
                    return null;
                }
            }

            private void BindFileTypes()
            {
                //query file types
                IEnumerable<GetFileTypeData> types = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryFileTypes() : QuerySearchedFileTypes(txtSearch.Text);

                int count = types.Count();

                gridFileTypes.DataSource = types;

                // bind grid
                gridFileTypes.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedFileTypes(string[] searctText)
            //{
            //    //query file types
            //    IEnumerable<GetFileTypeData> types = QuerySearchedFileTypes(searctText);

            //    int count = types.Count();

            //    gridFileTypes.DataSource = types;

            //    // bind grid
            //    gridFileTypes.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} file type found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} file types found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No file types found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
       
        #endregion
    }
}

