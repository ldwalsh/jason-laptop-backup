﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;
using CW.Data.AzureStorage;
using CW.Data.AzureStorage.DataContexts.Blob;
using CW.Diagnostics.Helpers;
using CW.Utility;
using CW.Website._framework;
using Microsoft.Samples.ServiceHosting.AspProviders;
using Microsoft.WindowsAzure;

namespace CW.Website
{
    public partial class KGSClientAdministration : ModulePage
    {
        #region Properties

            private Client mClient;
            private ClientSetting mClientSettings;
            const string clientExists = "Client already exists.";
            const string addClientSuccess = " client addition was successful.";
            const string faliedTransaction = "This transaction has failed. Please contact your system admin for more help.";            
            const string updateSuccessful = "Client update was successful.";
            const string updateFailed = "Client update failed. Please contact an administrator.";
            const string reassignModulesUpdateSuccessful = "Modules reassignment was successful.";
            const string reassignModulesUpdateFailed = "Modules reassignment failed. Please contact an administrator.";
            const string updateSettingsSuccessful = "Client settings update was successful.";
            const string updateSettingsFailed = "Client settings update failed. Please contact an administrator.";
            const string deleteSuccessful = "Client deletion was successful.";
            const string deleteFailed = "Client deletion failed. Please contact an administrator.";
            const string usersExist = "Cannot delete client because client is associated with one or more users. Please remove user association first.";
            const string buildingsExist = "Cannot delete building because client is associated with one or more buildings. Please remove building association first."; 
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "ClientName";
            private CloudStorageAccount mAccount = null;

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                mAccount = CloudConfiguration.GetStorageAccount(CommonConstants.ATS_STORAGE_KEY);

                //logged in security check in master page

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
                lblContentSettingsError.Visible = false;
                lblAccountSettingsError.Visible = false;

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindClients();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindStates(ddlEditState);
                    BindStates(ddlAddState);

                    BindClients(ddlModulesClients);
                    BindClients(ddlContentSettingsClients);
                    BindClients(ddlAccountSettingsClients);

                    moduleSession["Search"] = String.Empty;
                }

                //set enter key to submit search
                txtSearch.Attributes.Add("onKeyPress", "javascript:if (event.keyCode == 13) __doPostBack('" + searchBtn.UniqueID + "','')");
            }

        #endregion

        #region Set Fields and Data

            protected void SetClientIntoEditForm(Client client)
            {
                //ID
                hdnEditID.Value = Convert.ToString(client.CID);
                //Name
                txtEditClientName.Text = client.ClientName;
                //Address
                txtEditAddress.Text = client.ClientAddress;
                //City
                txtEditCity.Text = client.ClientCity;
                //State
                ddlEditState.SelectedValue = Convert.ToString(client.ClientStateID);
                //Zip
                txtEditZip.Text = client.ClientZip;
                //Phone
                txtEditPhone.Text = client.ClientPhone;
                //Fax
                txtEditFax.Text = client.ClientFax;
                //Primary Contact
                txtEditPrimaryContact.Text = client.ClientPrimaryContactName;
                //Primary Contact Email
                txtEditPrimaryEmail.Text = client.ClientPrimaryContactEmail;
                //Primary Contact Phone
                txtEditPrimaryPhone.Text = client.ClientPrimaryContactPhone;
                //Secondary Contact
                txtEditSecondaryContact.Text = client.ClientSecondaryContactName;
                //Secondary Contact Email
                txtEditSecondaryEmail.Text = client.ClientSecondaryContactEmail;
                //Secondary Contact Phone
                txtEditSecondaryPhone.Text = client.ClientSecondaryContactPhone;

                //Active
                chkEditActive.Checked = client.IsActive;
            }

            protected void SetClientContentSettingsIntoEditForm(ClientSetting clientSettings)
            {
                //HomePageHeader                
                    txtHomePageHeader.Value = clientSettings.HomePageHeader;
                    //txtEditHomePageHeader.Text = clientSettings.HomePageHeader;
                //HomePageBody
                //Make sure to decode from ascii to html 
                    editorHomePageBody.Content = Server.HtmlDecode(clientSettings.HomePageBody);
                    //txtEditHomePageBody.Value = clientSettings.HomePageBody;
                //NewsBody
                //Make sure to decode from ascii to html 
                    editorNewsBody.Content = Server.HtmlDecode(clientSettings.NewsBody);                
            }

            protected void SetClientAccountSettingsIntoEditForm(ClientSetting clientSettings)
            {
                //MaxMonthlyReportDownloads
                sliderSettingsMaxMonthlyReportDownloads.Text = clientSettings.MaxMonthlyReportDownloads.ToString();
                //CurrentMonthlyReportDownloads
                lblSettingsCurrentMonthlyReportDownloads.Text = clientSettings.CurrentMonthlyReportDownloads.ToString();
                //CurrentTotalReportDownloads
                lblSettingsCurrentTotalReportDownloads.Text = clientSettings.CurrentTotalReportDownloads.ToString();
                //MaxMonthlyReportEmails
                sliderSettingsMaxMonthlyReportEmails.Text = clientSettings.MaxMonthlyReportEmails.ToString();
                //CurrentMonthlyReportEmails
                lblSettingsCurrentMonthlyReportEmails.Text = clientSettings.CurrentMonthlyReportEmails.ToString();
                //CurrentTotalReportEmails
                lblSettingsCurrentTotalReportEmails.Text = clientSettings.CurrentTotalReportEmails.ToString();

                //MaxMonthlyRepositoryDownloads
                sliderSettingsMaxMonthlyRepositoryDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.MaxMonthlyRepositoryDownloads, 0).ToString();
                //CurrentMonthlyRepositoryDownloads
                lblSettingsCurrentMonthlyRepositoryDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentMonthlyRepositoryDownloads, 1).ToString();
                //CurrentTotalRepositoryDownloads
                lblSettingsCurrentTotalRepositoryDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentTotalRepositoryDownloads, 1).ToString();
                //MaxMonthlyRepositoryUploads
                sliderSettingsMaxMonthlyRepositoryUploads.Text = MathHelper.ConvertBytesToMB(clientSettings.MaxMonthlyRepositoryUploads, 0).ToString();
                //CurrentMonthlyRepositoryUploads
                lblSettingsCurrentMonthlyRepositoryUploads.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentMonthlyRepositoryUploads, 1).ToString();
                //CurrentTotalRepositoryUploads
                lblSettingsCurrentTotalRepositoryUploads.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentTotalRepositoryUploads, 1).ToString();
                //MaxTotalRepositoryStorage
                sliderSettingsMaxTotalRepositoryStorage.Text = MathHelper.ConvertBytesToMB(clientSettings.MaxTotalRepositoryStorage, 0).ToString();
                //CurrentTotalRepositoryStorage
                lblSettingsCurrentTotalRepositoryStorage.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentTotalRepositoryStorage, 1).ToString();

                //MaxMonthlyRawDataDownloads
                sliderSettingsMaxMonthlyRawDataDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.MaxMonthlyRawDataDownloads, 0).ToString();
                //CurrentMonthlyRawDataDownloads
                lblSettingsCurrentMonthlyRawDataDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentMonthlyRawDataDownloads, 1).ToString();
                //CurrentTotalRawDataDownloads
                lblSettingsCurrentTotalRawDataDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentTotalRawDataDownloads, 1).ToString();

                //MaxMonthlyPortfolioDownloads
                sliderSettingsMaxMonthlyPortfolioDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.MaxMonthlyPortfolioDownloads, 0).ToString();
                //CurrentMonthlyPortfolioDownloads
                lblSettingsCurrentMonthlyPortfolioDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentMonthlyPortfolioDownloads, 1).ToString();
                //CurrentTotalPortfolioDownloads
                lblSettingsCurrentTotalPortfolioDownloads.Text = MathHelper.ConvertBytesToMB(clientSettings.CurrentTotalPortfolioDownloads, 1).ToString();
            }

        #endregion

        #region Load and Bind Fields

            private void BindStates(DropDownList ddl)
            {
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = StateDataMapper.GetAllStates();
                ddl.DataBind();
            }

            /// <summary>
            /// Binds a dropdown list with all clients
            /// </summary>
            /// <param name="ddl"></param>
            private void BindClients(DropDownList ddl)
            {
                ddl.DataTextField = "ClientName";
                ddl.DataValueField = "CID";
                ddl.DataSource = ClientDataMapper.GetAllClients();
                ddl.DataBind();

            }

            /// <summary>
            /// Binds top and bottom list boxes with modules by clientID
            /// 
            /// top: all not yet associated to clientID
            /// bottom: all currently associated to clientID
            /// </summary>
            /// <param name="clientID"></param>       
            private void BindModules(int clientID)
            {
                //get modules associated to client
                IEnumerable<CW.Data.Module> mModules = ModuleDataMapper.GetAllModulesAssociatedToClientID(clientID);

                if (mModules.Count() > 0)
                {
                    //bind modules to bottom listbox
                    lbModulesBottom.DataSource = mModules;
                    lbModulesBottom.DataTextField = "ModuleName";
                    lbModulesBottom.DataValueField = "ModuleID";
                    lbModulesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbModulesBottom.Items.Clear();
                }

                //get modules not assiciated to analysis
                mModules = ModuleDataMapper.GetAllModulesNotAssociatedToClientID(clientID);

                if (mModules.Count() > 0)
                {
                    //bind modules to top listbox
                    lbModulesTop.DataSource = mModules;
                    lbModulesTop.DataTextField = "ModuleName";
                    lbModulesTop.DataValueField = "ModuleID";
                    lbModulesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbModulesTop.Items.Clear();
                }

                modules.Visible = true;
            }
       
        #endregion

        #region Load Client, ClientSettings

            protected void LoadAddFormIntoClient(Client client)
            {
                //Name
                client.ClientName = txtAddClientName.Text;

                //Address
                client.ClientAddress = txtAddAddress.Text;

                //City
                client.ClientCity = txtAddCity.Text;

                //State
                client.ClientStateID = Convert.ToInt32(ddlAddState.SelectedValue);

                //Zip
                client.ClientZip = txtAddZip.Text;

                //Phone
                client.ClientPhone = txtAddPhone.Text;

                //Fax
                client.ClientFax = txtAddFax.Text;
                //Primary Contact
                client.ClientPrimaryContactName = txtAddPrimaryContact.Text;
                //Primary Contact Email
                client.ClientPrimaryContactEmail = txtAddPrimaryEmail.Text;
                //Primary Contact Phone
                client.ClientPrimaryContactPhone = txtAddPrimaryPhone.Text;
                //Secondary Contact
                client.ClientSecondaryContactName = txtAddSecondaryContact.Text;
                //Secondary Contact Email
                client.ClientSecondaryContactEmail = txtAddSecondaryEmail.Text;
                //Secondary Contact Phone
                client.ClientSecondaryContactPhone = txtAddSecondaryPhone.Text;

                client.IsActive = true;
                client.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoClient(Client client)
            {
                //ID
                client.CID = Convert.ToInt32(hdnEditID.Value);

                //Name
                client.ClientName = txtEditClientName.Text;

                //Address
                client.ClientAddress = txtEditAddress.Text;

                //City
                client.ClientCity = txtEditCity.Text;

                //State
                client.ClientStateID = Convert.ToInt32(ddlEditState.SelectedValue);

                //Zip
                client.ClientZip = txtEditZip.Text;

                //Phone
                client.ClientPhone = txtEditPhone.Text;

                //Fax
                client.ClientFax = txtEditFax.Text;
                //Primary Contact
                client.ClientPrimaryContactName = txtEditPrimaryContact.Text;
                //Primary Contact Email
                client.ClientPrimaryContactEmail = txtEditPrimaryEmail.Text;
                //Primary Contact Phone
                client.ClientPrimaryContactPhone = txtEditPrimaryPhone.Text;
                //Secondary Contact
                client.ClientSecondaryContactName = txtEditSecondaryContact.Text;
                //Secondary Contact Email
                client.ClientSecondaryContactEmail = txtEditSecondaryEmail.Text;
                //Secondary Contact Phone
                client.ClientSecondaryContactPhone = txtEditSecondaryPhone.Text;

                client.IsActive = true;
            }
        
            /// <summary>
            /// Loads the editor form data for client content settings for update.
            /// 
            /// Also removes extra characters over max html limits, and then encodes the html.
            /// </summary>
            /// <param name="clientSettings"></param>
            protected void LoadEditFormIntoClientContentSettings(ClientSetting clientSettings)
            {
                //ID
                clientSettings.CID = Convert.ToInt32(ddlContentSettingsClients.SelectedValue);

                //HomePageHeader                                                               
                    clientSettings.HomePageHeader = String.IsNullOrEmpty(txtHomePageHeader.Value) ? null : txtHomePageHeader.Value;
                    //txtEditHomePageHeader.Text;

                //HomePageBody
                    //if editor's html is greater then defined max html length, 
                    //then remove extra characters.
                    //Make sure to html encode before putting in the database. From html to ascii.
                    clientSettings.HomePageBody = Server.HtmlEncode(editorHomePageBody.Content); 
                    //txtEditHomePageBody.Value;
                    clientSettings.NewsBody = Server.HtmlEncode(editorNewsBody.Content); 
            }

            /// <summary>
            /// Loads the editor form data for client account settings 
            /// </summary>
            /// <param name="clientSettings"></param>
            protected void LoadEditFormIntoClientAccountSettings(ClientSetting clientSettings)
            {
                //ID
                clientSettings.CID = Convert.ToInt32(ddlAccountSettingsClients.SelectedValue);

                //MaxMonthlyReportDownloads
                clientSettings.MaxMonthlyReportDownloads = Convert.ToInt32(sliderSettingsMaxMonthlyReportDownloads.Text);
                //MaxMonthlyReportEmails
                clientSettings.MaxMonthlyReportEmails = Convert.ToInt32(sliderSettingsMaxMonthlyReportEmails.Text);
                //MaxMonthlyRepositoryDownloads
                clientSettings.MaxMonthlyRepositoryDownloads = MathHelper.ConvertMBToBytes(Convert.ToInt32(sliderSettingsMaxMonthlyRepositoryDownloads.Text));
                //MaxMonthlyRepositoryUploads
                clientSettings.MaxMonthlyRepositoryUploads = MathHelper.ConvertMBToBytes(Convert.ToInt32(sliderSettingsMaxMonthlyRepositoryUploads.Text));
                //MaxTotalRepositoryStorage
                clientSettings.MaxTotalRepositoryStorage = MathHelper.ConvertMBToBytes(Convert.ToInt32(sliderSettingsMaxTotalRepositoryStorage.Text));
                //MaxMonthlyRawDataDownloads
                clientSettings.MaxMonthlyRawDataDownloads = MathHelper.ConvertMBToBytes(Convert.ToInt32(sliderSettingsMaxMonthlyRawDataDownloads.Text));
                //MaxMonthlyPortfolioDownloads
                clientSettings.MaxMonthlyPortfolioDownloads = MathHelper.ConvertMBToBytes(Convert.ToInt32(sliderSettingsMaxMonthlyPortfolioDownloads.Text));
            }

        #endregion

        #region Dropdown Events

            /// <summary>
            /// ddlModulesClients on selected index changed, binds modules list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlModulesClients_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int clientID = Convert.ToInt32(ddlModulesClients.SelectedValue);

                if (clientID != -1)
                {
                    BindModules(clientID);
                }
                else
                {
                    //hide modules
                    modules.Visible = false;
                }

                //hide error message
                lblModulesError.Visible = false;
            }
        
            /// <summary>
            /// ddlContentSettingsClients on selected index changed, set settings into conetent edit form
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlContentSettingsClients_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int clientID = Convert.ToInt32(ddlContentSettingsClients.SelectedValue);

                if (clientID != -1)
                {
                    //gets client settings
                    ClientSetting mClientSettings = ClientDataMapper.GetClientSettingsByClientID(clientID);

                    //set client conetent setting into edit form
                    SetClientContentSettingsIntoEditForm(mClientSettings);

                    //show settings div
                    contentSettings.Visible = true;
                }
                else
                {
                    //hide settings
                    contentSettings.Visible = false;
                }

                //hide error message
                lblContentSettingsError.Visible = false;
            }

            /// <summary>
            /// ddlAccountSettingsClients on selected index changed, set settings into edit form
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlAccountSettingsClients_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int clientID = Convert.ToInt32(ddlAccountSettingsClients.SelectedValue);

                if (clientID != -1)
                {
                    //gets client settings
                    ClientSetting mClientSettings = ClientDataMapper.GetClientSettingsByClientID(clientID);

                    //set client setting into edit form
                    SetClientAccountSettingsIntoEditForm(mClientSettings);

                    //show settings div
                    accountSettings.Visible = true;
                }
                else
                {
                    //hide settings
                    accountSettings.Visible = false;
                }

                //hide error message
                lblAccountSettingsError.Visible = false;
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Add client Button on click.
            /// </summary>
            protected void addClientButton_Click(object sender, EventArgs e)
            {
                //check that client does not already exist
                if (!ClientDataMapper.DoesClientExist(txtAddClientName.Text))
                {
                    mClient = new Client();

                    //load the form into the client
                    LoadAddFormIntoClient(mClient);

                    mClientSettings = new ClientSetting();
                    mClientSettings.DateModified = DateTime.UtcNow;
                    
                    try
                    {
                        //inserts new client, returns newly created client id, 
                        //and sets the client settings client id
                        mClientSettings.CID = ClientDataMapper.InsertClientAndReturnClientID(mClient);

                        //insert new client setting. default values are set on db side.
                        //ClientDataMapper.InsertClientSettings(mClientSettings);

                        //Repository container for client is created on first file upload, not here.

                        lblAddError.Text = mClient.ClientName + addClientSuccess;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                    catch (SqlException sqlEx)
                    {
                        LogHelper.LogCWError("Error adding client.", sqlEx);
                        lblAddError.Text = faliedTransaction;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();

                    }
                    catch (Exception ex)
                    {
                        LogHelper.LogCWError("Error adding client", ex);
                        lblAddError.Text = faliedTransaction;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                }
                else
                {
                    //Client exists
                    lblAddError.Text = clientExists;
                    lblAddError.Visible = true;
                    lnkSetFocusAdd.Focus();
                }
            }

            /// <summary>
            /// Update client Button on click.
            /// </summary>
            protected void updateClientButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the client
                Client mClient = new Client();
                LoadEditFormIntoClient(mClient);

                //check if the clientname exists for another Client if trying to change
                if (ClientDataMapper.DoesClientExist(mClient.CID, mClient.ClientName))
                {
                    lblEditError.Text = clientExists;
                    lblEditError.Visible = true;
                    lnkSetFocusView.Focus();
                }
                //update the client
                else
                {
                    try
                    {
                        ClientDataMapper.UpdateClient(mClient);

                        lblEditError.Text = updateSuccessful;
                        lblEditError.Visible = true;
                        lnkSetFocusView.Focus();

                        //Bind clients again
                        BindClients();
                    }
                    catch (Exception ex)
                    {
                        lblEditError.Text = updateFailed;
                        lblEditError.Visible = true;
                        lnkSetFocusView.Focus();

                        LogHelper.LogCWError("Error updating client.", ex);
                    }
                }
            }

            /// <summary>
            /// Update modules button click, reassigns modules to client based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateModulesButton_Click(object sender, EventArgs e)
            {
                int clientID = Convert.ToInt32(ddlModulesClients.SelectedValue);
                int moduleID;

                //declare new Client_Module
                Clients_Module mModule = new Clients_Module();

                try
                {
                    //inserting each module in the bottom listbox if they dont exist
                    foreach (ListItem item in lbModulesBottom.Items)
                    {
                        moduleID = Convert.ToInt32(item.Value);
                        mModule.CID = clientID;
                        mModule.ModuleID = moduleID;

                        //if the module doesnt already exist, insert
                        if (!ModuleDataMapper.DoesModuleExistForClient(clientID, moduleID))
                        {
                            ModuleDataMapper.InsertClientModule(mModule);
                        }
                    }

                    //deleting each module in the top listbox if they exist
                    foreach (ListItem item in lbModulesTop.Items)
                    {
                        moduleID = Convert.ToInt32(item.Value);

                        //if the module exists, delete
                        if (ModuleDataMapper.DoesModuleExistForClient(clientID, moduleID))
                        {
                            //delete quick links for users with client and module ids
                            QuickLinksDataMapper.DeleteAllUserClientQuickLinksByClientIDAndModuleID(clientID, moduleID);

                            //then delete the module for the client
                            ModuleDataMapper.DeleteModuleByClientIDAndModuleID(clientID, moduleID);                            
                        }
                    }


                    lblModulesError.Text = reassignModulesUpdateSuccessful;
                    lblModulesError.Visible = true;
                    lnkSetFocusModule.Focus();
                }
                catch (SqlException sqlEx)
                {
                    LogHelper.LogCWError("Error reassigning modules to client.", sqlEx);
                    lblModulesError.Text = reassignModulesUpdateFailed;
                    lblModulesError.Visible = true;
                    lnkSetFocusModule.Focus();
                }
                catch (Exception ex)
                {
                    LogHelper.LogCWError("Error reassigning modules to client.", ex);
                    lblModulesError.Text = reassignModulesUpdateFailed;
                    lblModulesError.Visible = true;
                    lnkSetFocusModule.Focus();
                }
            }

            /// <summary>
            /// Update client content settings Button on click.
            /// </summary>
            protected void updateContentSettingsButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the client settings
                ClientSetting mClientSettings = new ClientSetting();
                LoadEditFormIntoClientContentSettings(mClientSettings);

                try
                {
                    ClientDataMapper.UpdateClientContentSettings(mClientSettings);

                    lblContentSettingsError.Text = updateSettingsSuccessful;
                    lblContentSettingsError.Visible = true;
                    lnkSetFocusContentSettings.Focus();

                }
                catch (Exception ex)
                {
                    lblContentSettingsError.Text = updateSettingsFailed;
                    lblContentSettingsError.Visible = true;
                    lnkSetFocusContentSettings.Focus();

                    LogHelper.LogCWError("Unable to update client content settings.", ex);
                }
            }

            /// <summary>
            /// Update client account settings Button on click.
            /// </summary>
            protected void updateAccountSettingsButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the client settings
                ClientSetting mClientSettings = new ClientSetting();
                LoadEditFormIntoClientAccountSettings(mClientSettings);

                try
                {
                    ClientDataMapper.UpdateClientAccountSettings(mClientSettings);

                    lblAccountSettingsError.Text = updateSettingsSuccessful;
                    lblAccountSettingsError.Visible = true;
                    lnkSetFocusAccountSettings.Focus();

                }
                catch (Exception ex)
                {
                    lblAccountSettingsError.Text = updateSettingsFailed;
                    lblAccountSettingsError.Visible = true;
                    lnkSetFocusAccountSettings.Focus();

                    LogHelper.LogCWError("Unable to update client account settings.", ex);
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                moduleSession["Search"] = txtSearch.Text;
                string[] searchText = txtSearch.Text.Split(' ');
                BindSearchedClients(searchText);
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                moduleSession["Search"] = String.Empty;
                BindClients();
            }

        #endregion

        #region ListBox Up/Down Button Events

            /// <summary>
            /// on button up click, remove modules from bottom (unassociate to client)
            /// </summary>
            protected void btnModulesUpButton_Click(object sender, EventArgs e)
            {
                while (lbModulesBottom.SelectedIndex != -1)
                {
                    lbModulesTop.Items.Add(lbModulesBottom.SelectedItem);
                    lbModulesBottom.Items.Remove(lbModulesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add modules to bottom (associate to client)
            /// </summary>
            protected void btnModulesDownButton_Click(object sender, EventArgs e)
            {
                while (lbModulesTop.SelectedIndex != -1)
                {
                    {
                        lbModulesBottom.Items.Add(lbModulesTop.SelectedItem);
                        lbModulesTop.Items.Remove(lbModulesTop.SelectedItem);
                    }
                }
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// On Init for Tabs
            /// </summary>
            protected void loadTabs(object sender, EventArgs e)
            {        
            }

            /// <summary>
            /// Tab changed event, bind clients grid after tab changed back from add Client tab
            /// </summary>
            protected void onActiveTabChanged(object sender, EventArgs e)
            {
                if (TabContainer1.ActiveTabIndex == 0)
                {
                    BindClients();
                }
            }

        #endregion

        #region Grid Events

            protected void gridClients_OnDataBound(object sender, EventArgs e)
            {
                
            }

            protected void gridClients_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditClients.Visible = false;
                dtvClient.Visible = true;
                IOrderedDictionary dka = gridClients.DataKeys[gridClients.SelectedIndex].Values;

                int cid = Convert.ToInt32(dka["CID"]);

                //set data source
                dtvClient.DataSource = ClientDataMapper.GetFullClientByID(cid);
                //bind user to details view
                dtvClient.DataBind();
            }

            protected void gridClients_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridClients_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvClient.Visible = false;
                pnlEditClients.Visible = true;

                IOrderedDictionary dka = gridClients.DataKeys[e.NewEditIndex].Values;

                int cid = Convert.ToInt32(dka["CID"]);

                Client mClient = ClientDataMapper.GetClient(cid);

                SetClientIntoEditForm(mClient);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridClients_Deleting(object sender, GridViewDeleteEventArgs e)
            {                
                //get deleting rows clientid
                int clientID = Convert.ToInt32(gridClients.DataKeys[e.RowIndex].Value);

                try
                {
                    //check if users are assigned to a client regardless of clients isActive state
                    if (UserDataMapper.AreUsersAssociatedToClient(clientID))
                    {
                        lblErrors.Text = usersExist;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    //check if buildings are assigned to a client regardless of clients isActive state
                    else if (BuildingDataMapper.AreBuildingsAssociatedToClient(clientID))
                    {
                        lblErrors.Text = buildingsExist;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {                        
                        //delete client repository container from blob storage
                        //all buildings should have been deleted already so all files should be deleted already from the 
                        RepositoryBlobDataContext blobContext = new RepositoryBlobDataContext(mAccount, clientID);
                        blobContext.DeleteContainer();

                        //delete settings
                        ClientDataMapper.DeleteClientSettings(clientID);

                        //delete modules
                        ModuleDataMapper.DeleteModulesByClientID(clientID);
                        
                        //delete client
                        ClientDataMapper.DeleteClient(clientID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();

                        pnlEditClients.Visible = false;
                        dtvClient.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogHelper.LogCWError("Error deleting client.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = ConvertIEnumerableToDataTable(QueryClients());
                gridClients.PageIndex = gridClients.PageIndex;
                gridClients.DataSource = SortDataTable(dataTable as DataTable, true);
                gridClients.DataBind();

                SetGridCountLabel(dataTable.ExtendedProperties.Count);

                //hide edit form if shown
                pnlEditClients.Visible = false;
            }

            protected void gridClients_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = ConvertIEnumerableToDataTable(QuerySearchedClients(moduleSession["Search"].Split(' ')));

                //maintain current sort direction and expresstion on paging
                gridClients.DataSource = SortDataTable(dataTable, true);
                gridClients.PageIndex = e.NewPageIndex;
                gridClients.DataBind();
            }

            protected void gridClients_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridClients.EditIndex = -1;

                DataTable dataTable = ConvertIEnumerableToDataTable(QuerySearchedClients(moduleSession["Search"].Split(' ')));

                GridViewSortExpression = e.SortExpression;

                gridClients.DataSource = SortDataTable(dataTable, false);
                gridClients.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<ClientDataMapper.GetClientsData> QueryClients()
            {
                try
                {
                    //get all Clients 
                    return ClientDataMapper.GetAllClientData();
                }
                catch (SqlException sqlEx)
                {
                    LogHelper.LogCWError("Error retrieving clients.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogHelper.LogCWError("Error retrieving clients.", ex);
                    return null;
                }
            }

            private IEnumerable<ClientDataMapper.GetClientsData> QuerySearchedClients(string[] searchText)
            {
                try
                {
                    //get all Clients 
                    return ClientDataMapper.GetAllSearchedClientData(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogHelper.LogCWError("Error retrieving clients.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogHelper.LogCWError("Error retrieving clients.", ex);
                    return null;
                }
            }

            private void BindClients()
            {
                //query Clients
                IEnumerable<ClientDataMapper.GetClientsData> clients = QueryClients();

                int count = clients.Count();

                //maintain sort--- doesnt work!
                //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
                //gridClients.Sort(GridViewSortExpression, tempSD);

                gridClients.DataSource = clients;

                // bind grid
                gridClients.DataBind();

                SetGridCountLabel(count);
            }

            private void BindSearchedClients(string[] searchText)
            {
                //query Clients
                IEnumerable<ClientDataMapper.GetClientsData> clients = QuerySearchedClients(searchText);

                int count = clients.Count();

                //maintain sort--- doesnt work!
                //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
                //gridClients.Sort(GridViewSortExpression, tempSD);

                gridClients.DataSource = clients;

                // bind grid
                gridClients.DataBind();

                SetGridCountLabel(count);
            }

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} client found", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} clients found", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No clients found";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            //***Linq to sql IEnumerable to DataTable conversion. Needed for sorting.****
            private DataTable ConvertIEnumerableToDataTable(IEnumerable dataSource)
            {
                System.Reflection.PropertyInfo[] propInfo = null;
                DataTable dt = new DataTable();

                foreach (object o in dataSource)
                {
                    propInfo = o.GetType().GetProperties();

                    for (int i = 0; i < propInfo.Length; i++)
                    {
                        dt.Columns.Add(propInfo[i].Name);
                    }
                    break;
                }

                foreach (object tempObject in dataSource)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < propInfo.Length; i++)
                    {
                        object t = tempObject.GetType().InvokeMember(propInfo[i].Name, BindingFlags.GetProperty, null, tempObject, new object[] { });
                        if (t != null)
                            dr[i] = t.ToString();
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }

        #endregion
    }
}

