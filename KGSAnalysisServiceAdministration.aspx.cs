﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Threading;
using CW.Business;

namespace CW.Website
{
    public partial class KGSAnalysisServiceAdministration : System.Web.UI.Page
    {
        #region Properties

        const string DEBUG_OVERRIDE_SPECIFIED_ANALYSIS_DESCRIPTION = "PRODUCTION/DEBUG USE: Purges existing data then runs a specified analysis in debug mode, and inserts the analyized data into the db. No matlab logging. Logs .net exceptions, logs sql exceptions. Returns detailed success/failure message. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";
        const string DEBUG_SCHEDULED_ANALYSES_DESCRIPTION = "PRODUCTION/DEBUG USE: Runs the scheduled analyses in debug mode, and inserts the analyized data into the db. No matlab logging. Logs .net exceptions, logs sql exceptions. Returns detailed success/failure messages up until a matlab error. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";
        const string DEBUG_SPECIFIED_ANALYSIS_DESCRIPTION = "DEBUG USE: Runs the specified analysis in debug mode, and inserts the analyized data into the db. No matlab logging. Logs .net exception, logs sql exception. Returns detailed success/failure message. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";
        const string OVERRIDE_SPECIFIED_ANALYSIS_DESCRIPTION = "PRODUCTION USE ONLY: Purges existing data then runs a specified analysis, and inserts the analyized data into the db. Logs matlab error, logs .net exception, logs sql exception, and returns detailed success/failure message. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";
        const string PURGE_ALL_ANALYSES_DESCRIPTION = "PRODUCTION USE ONLY: Purges all existing analysis data for a date. Logs .net exceptions, logs sql exceptions.Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";
        const string PURGE_SPECIFIED_ANALYSIS_DESCRIPTION = "PRODUCTION USE ONLY: Purges existing data for a specified analysis. Logs .net exceptions, logs sql exceptions. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";
        const string SCHEDULED_ANALYSES_DESCRIPTION = "PRODUCTION USE ONLY: Runs a specified analysis, and inserts the analyized data into the db. Logs matlab error, logs .net exception, logs sql exception, and returns detailed success/failure message. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";
        const string TEST_SCHEDULED_ANALYSES_DESCRIPTION = "Runs the scheduled analyses in test mode, without db insertion. No matlab logging. Logs .net exceptions, logs sql exceptions. Returns detailed success/failure message. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";
        const string SPECIFIED_ANALYSIS_DESCRIPTION = "PRODUCTION USE ONLY: Runs a specified analysis, and inserts the analyized data into the db. Logs matlab error, logs .net exception, logs sql exception, and returns detailed success/failure message. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended. ";
        const string TEST_SPECIFIED_ANALYSIS_DESCRIPTION = "TEST USE: Runs the specified analysis in test mode, without db insertion. No matlab logging. Logs .net exception, logs sql exception. Returns detailed success/failure message. Date Format: MM/DD/YYYY. Range Format: DAILY,WEEKLY, or MONTHLY. Daily is date provided to the next day. Weekly is the sunday to sunday for whatever the date provided falls within. Monthly is the month for whatever the date provided falls withing. When manually providing a date, it must fall within a range of complete existing data. Ex: Inputting the current friday will not work because the current week sunday-sunday has not yet ended.";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page  

                if (!Page.IsPostBack)
                {
                    BindClients(ddlClient);
                }
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Binds a dropdown list with all clients
            /// </summary>
            /// <param name="ddl"></param>
            private void BindClients(DropDownList ddl)
            {
                ddl.DataTextField = "ClientName";
                ddl.DataValueField = "ClientID";
                ddl.DataSource = ClientDataMapper.GetAllClients();
                ddl.DataBind();
            }

            private void BindBuildings(DropDownList ddl, int clientID)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "BuildingName";
                ddl.DataValueField = "BID";
                ddl.DataSource = BuildingDataMapper.GetAllBuildingsByClientID(clientID);
                ddl.DataBind();
            }

            private void BindEquipment(DropDownList ddl, int bid)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "EquipmentName";
                ddl.DataValueField = "EID";
                ddl.DataSource = EquipmentDataMapper.GetAllEquipmentByBuildingID(bid);
                ddl.DataBind();
            }

            private void BindAnalysis(DropDownList ddl, int eid)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "AnalysisName";
                ddl.DataValueField = "AID";
                ddl.DataSource = AnalysisDataMapper.GetAllActiveAnalysesByEquipmentID(eid);
                ddl.DataBind();
            }

            #endregion

        #region Dropdown Events

            protected void ddlWebServices_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlWebServices.SelectedValue != "-1")
                {
                    string webservice = "";

                    switch (ddlWebServices.SelectedValue)
                    {
                        case "DebugOverrideSpecifiedAnalysis":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("1");
                            webServiceDescription.InnerText = DEBUG_OVERRIDE_SPECIFIED_ANALYSIS_DESCRIPTION;
                            break;
                        case "DebugScheduledAnalyses":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("3");
                            webServiceDescription.InnerText = DEBUG_SCHEDULED_ANALYSES_DESCRIPTION;
                            break;
                        case "DebugSpecifiedAnalysis":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("1");
                            webServiceDescription.InnerText = DEBUG_SPECIFIED_ANALYSIS_DESCRIPTION;
                            break;
                        case "OverrideSpecifiedAnalysis":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("1");
                            webServiceDescription.InnerText = OVERRIDE_SPECIFIED_ANALYSIS_DESCRIPTION;
                            break;
                        case "PurgeAllAnalyses":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("3");
                            webServiceDescription.InnerText = PURGE_ALL_ANALYSES_DESCRIPTION;
                            break;
                        case "PurgeSpecifiedAnalysis":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("1");
                            webServiceDescription.InnerText = PURGE_SPECIFIED_ANALYSIS_DESCRIPTION;
                            break;
                        case "ScheduledAnalyses":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("3");
                            webServiceDescription.InnerText = SCHEDULED_ANALYSES_DESCRIPTION;
                            break;
                        case "SpecifiedAnalysis":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("1");
                            webServiceDescription.InnerText = SPECIFIED_ANALYSIS_DESCRIPTION;
                            break;
                        case "TestScheduledAnalyses":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("3");
                            webServiceDescription.InnerText = TEST_SPECIFIED_ANALYSIS_DESCRIPTION;
                            break;
                        case "TestSpecifiedAnalysis":
                            webservice = ddlWebServices.SelectedValue;
                            VisibiltyHelper("1");
                            webServiceDescription.InnerText = TEST_SPECIFIED_ANALYSIS_DESCRIPTION;
                            break;
                    }

                    //clear all fields
                    txtManualStartDate.Text = "";
                    ddlDateRange.SelectedIndex = -1;
                    ddlClient.SelectedIndex = -1;
                }
                else
                {
                    VisibiltyHelper("2");
                    webServiceDescription.InnerText = String.Empty;
                }
            }

            protected void ddlClient_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindBuildings(ddlBuilding, Convert.ToInt32(ddlClient.SelectedValue));
            }

            protected void ddlBuilding_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindEquipment(ddlEquipment, Convert.ToInt32(ddlBuilding.SelectedValue));
            }

            protected void ddlEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindAnalysis(ddlAnalysis, Convert.ToInt32(ddlEquipment.SelectedValue));
            }

            protected void ddlAnalysis_OnSelectedIndexChanged(object sender, EventArgs e)
            {                
            }
        

        #endregion

        #region Button Click Events

            protected void btnSubmit_Click(object sender, EventArgs e)
            {
                    string result = "";
                    AnalysisServiceReference.AnalysisService service = new AnalysisServiceReference.AnalysisService();
                    service.Timeout = Timeout.Infinite;
                    service.UseDefaultCredentials = true;
                    btnSubmit.Enabled = false;
                
                    switch (ddlWebServices.SelectedValue)
                    {
                        case "DebugOverrideSpecifiedAnalysis":
                            result = service.DebugOverrideSpecifiedAnalysis(txtManualStartDate.Text, ddlDateRange.SelectedValue, ddlEquipment.SelectedValue, ddlAnalysis.SelectedValue);
                            break;
                        case "DebugScheduledAnalyses":
                            result = service.DebugScheduledAnalyses(txtManualStartDate.Text, ddlDateRange.SelectedValue);
                            break;
                        case "DebugSpecifiedAnalysis":
                            result = service.DebugSpecifiedAnalysis(txtManualStartDate.Text, ddlDateRange.SelectedValue, ddlEquipment.SelectedValue, ddlAnalysis.SelectedValue);
                            break;
                        case "OverrideSpecifiedAnalysis":
                            result = service.OverrideSpecifiedAnalysis(txtManualStartDate.Text, ddlDateRange.SelectedValue, ddlEquipment.SelectedValue, ddlAnalysis.SelectedValue);
                            break;
                        case "PurgeAllAnalyses":
                            result = service.PurgeAllAnalyses(txtManualStartDate.Text, ddlDateRange.SelectedValue);
                            break;
                        case "PurgeSpecifiedAnalysis":
                            result = service.PurgeSpecifiedAnalysis(txtManualStartDate.Text, ddlDateRange.SelectedValue, ddlEquipment.SelectedValue, ddlAnalysis.SelectedValue);
                            break;
                        case "ScheduledAnalyses":
                            result = service.ScheduledAnalyses(txtManualStartDate.Text, ddlDateRange.SelectedValue);
                            break;
                        case "SpecifiedAnalysis":
                            result = service.SpecifiedAnalysis(txtManualStartDate.Text, ddlDateRange.SelectedValue, ddlEquipment.SelectedValue, ddlAnalysis.SelectedValue);
                            break;
                        case "TestScheduledAnalyses":
                            result = service.TestScheduledAnalyses(txtManualStartDate.Text, ddlDateRange.SelectedValue);
                            break;
                        case "TestSpecifiedAnalysis":
                            result = service.TestSpecifiedAnalysis(txtManualStartDate.Text, ddlDateRange.SelectedValue, ddlEquipment.SelectedValue, ddlAnalysis.SelectedValue);
                            break;
                    }

                    litWebServiceResponse.Visible = true;
                    litWebServiceResponse.Text = result;

                    btnSubmit.Enabled = true;
            }

        #endregion

        #region Helper Methods

            /// <summary>
            /// Helps set panel visiblity.
            /// show all =  "1"
            /// hide all = "2"
            /// show all except pnlIds = "3"
            /// </summary>
            /// <param name="visibilityLevel"></param>
            private void VisibiltyHelper(string visibilityLevel)
            {
                switch (visibilityLevel)
                {
                    //show all
                    case "1":
                        pnlDates.Visible = true;
                        pnlIds.Visible = true;
                        pnlButtons.Visible = true;
                        break;
                    //hide all
                    case "2":
                        pnlDates.Visible = false;
                        pnlIds.Visible = false;
                        pnlButtons.Visible = false;
                        break;
                    //show all except pnlIds
                    case "3":
                        pnlDates.Visible = true;
                        pnlIds.Visible = false;
                        pnlButtons.Visible = true;
                        break;
                }
            }

        #endregion
    }
}
