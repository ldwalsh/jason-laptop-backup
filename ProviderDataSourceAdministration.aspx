﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="ProviderDataSourceAdministration.aspx.cs" Inherits="CW.Website.ProviderDataSourceAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/DataSources/ViewDataSources.ascx" tagname="ViewDataSources" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/DataSources/AddDataSource.ascx" tagname="AddDataSource" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/DataSources/BulkEditDataSources.ascx" tagname="BulkEditDataSources" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/DataSources/DataSourceStats.ascx" tagname="DataSourceStats" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <h1>Provider Data Source Administration</h1>
      <div class="richText">The provider data source administration area is used to view, add, and edit data sources.</div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div> 

      <div class="administrationControls">
        <asp:HiddenField ID="hdnPageViews" runat="server" />
        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Data Sources" />
            <telerik:RadTab Text="Add Data Source" />
            <telerik:RadTab Text="Bulk Edit Data Sources" />
            <telerik:RadTab Text="Stats" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
            
          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:ViewDataSources ID="ViewDataSources" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:AddDataSource ID="AddDataSource" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView3" runat="server">  
            <CW:BulkEditDataSources ID="BulkEditDataSources" runat="server" />
          </telerik:RadPageView>
        
          <telerik:RadPageView ID="RadPageView4" runat="server">  
            <CW:DataSourceStats ID="DataSourceStats" runat="server" />
          </telerik:RadPageView>

        </telerik:RadMultiPage>
      </div>

</asp:Content>