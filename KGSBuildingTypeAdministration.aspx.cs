﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.BuildingType;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSBuildingTypeAdministration : SitePage
    {
        #region Properties

            private BuildingType mBuildingType;   
            const string addBuildingTypeSuccess = " building type addition was successful.";
            const string addBuildingTypeFailed = "Adding building type failed. Please contact an administrator.";
            const string buildingTypeExists = "A building type with that name already exists.";
            protected string selectedValue;
            const string updateSuccessful = "Building type update was successful.";
            const string updateFailed = "Building type update failed. Please contact an administrator.";
            const string deleteSuccessful = "Building type deletion was successful.";
            const string deleteFailed = "Building type deletion failed. Please contact an administrator.";
            const string buildingExists = "Cannot delete building type because one or more buildings are associated. Please disassociate from buildings first.";    
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "BuildingTypeName";
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //datamanger in sitepage

                //logged in security check in master page
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindBuildingTypes();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindBuildingClasses(ddlAddBuildingClass, ddlEditBuildingClass);

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetBuildingTypeIntoEditForm(CW.Data.BuildingType buildingType)
            {
                //ID
                hdnEditID.Value = Convert.ToString(buildingType.BuildingTypeID);
                //Building Type Name
                txtEditBuildingTypeName.Text = buildingType.BuildingTypeName;
                //Building Class
                ddlEditBuildingClass.SelectedValue = Convert.ToString(buildingType.BuildingClassID);

                //Building Variable Description
                txtEditDescription.Value = String.IsNullOrEmpty(buildingType.BuildingTypeDescription) ? null : buildingType.BuildingTypeDescription;
            }

        #endregion

        #region Get Fields and Data
                   
        #endregion

        #region Load and Bind Fields

            private void BindBuildingClasses(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<BuildingClass> classes = DataMgr.BuildingClassDataMapper.GetAllBuildingClasses();

                ddl.DataTextField = "BuildingClassName";
                ddl.DataValueField = "BuildingClassID";
                ddl.DataSource = classes; 
                ddl.DataBind();

                ddl2.DataTextField = "BuildingClassName";
                ddl2.DataValueField = "BuildingClassID";
                ddl2.DataSource = classes;
                ddl2.DataBind();
            }

        #endregion

        #region Load Building Type

            protected void LoadAddFormIntoBuildingType(BuildingType buildingType)
            {
                //Building Type Name
                buildingType.BuildingTypeName = txtAddBuildingTypeName.Text;
                //Building Class
                buildingType.BuildingClassID = Convert.ToInt32(ddlAddBuildingClass.SelectedValue);
                //Description
                buildingType.BuildingTypeDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                buildingType.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoBuildingType(BuildingType buildingType)
            {
                //ID
                buildingType.BuildingTypeID = Convert.ToInt32(hdnEditID.Value);
                //Building Type Name
                buildingType.BuildingTypeName = txtEditBuildingTypeName.Text;
                buildingType.BuildingTypeDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                buildingType.BuildingClassID = Convert.ToInt32(ddlEditBuildingClass.SelectedValue);
                buildingType.DateModified = DateTime.UtcNow;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add building type Button on click.
            /// </summary>
            protected void addBuildingTypeButton_Click(object sender, EventArgs e)
            {
                //check that building type does not already exist
                if (!DataMgr.BuildingTypeDataMapper.DoesBuildingTypeExist(txtAddBuildingTypeName.Text))
                {
                    mBuildingType = new BuildingType();

                    //load the form into the building type
                    LoadAddFormIntoBuildingType(mBuildingType);

                    try
                    {
                        //insert new building type
                        DataMgr.BuildingTypeDataMapper.InsertBuildingType(mBuildingType);

                        LabelHelper.SetLabelMessage(lblAddError, mBuildingType.BuildingTypeName + addBuildingTypeSuccess, lnkSetFocusAdd);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building type.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addBuildingTypeFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building type.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addBuildingTypeFailed, lnkSetFocusAdd);
                    }
                }
                else
                {
                    //building type exists
                    LabelHelper.SetLabelMessage(lblAddError, buildingTypeExists, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update building type Button on click. Updates building type data.
            /// </summary>
            protected void updateBuildingTypeButton_Click(object sender, EventArgs e)
            {
                BuildingType mBuildingType = new BuildingType();

                //load the form into the building type
                LoadEditFormIntoBuildingType(mBuildingType);

                //check if the buildingtype name exists for another buildingtype if trying to change
                if (DataMgr.BuildingTypeDataMapper.DoesBuildingTypeExist(mBuildingType.BuildingTypeID, mBuildingType.BuildingTypeName))
                {
                    LabelHelper.SetLabelMessage(lblEditError, buildingTypeExists, lnkSetFocusEdit);
                }
                //update the building type
                else
                {
                    //try to update the building type              
                    try
                    {
                        DataMgr.BuildingTypeDataMapper.UpdateBuildingType(mBuildingType);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusEdit);

                        //Bind building types again
                        BindBuildingTypes();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building type.", ex);
                    }
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindBuildingTypes();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindBuildingTypes();
            }


        #endregion

        #region Dropdown events

            /// <summary>
            /// On initialize of the building dropdown, bind all building classes
            /// Do not reload on autopostback.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlEditBuildingClass_OnInit(object sender, EventArgs e)
            {
                DropDownList ddl = (DropDownList)sender;

                ddl.DataTextField = "BuildingClassName";
                ddl.DataValueField = "BuildingClassID";
                ddl.DataSource = DataMgr.BuildingClassDataMapper.GetAllBuildingClasses();
                ddl.DataBind();

                ddl.SelectedValue = selectedValue;
            }

         #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind building type grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind building classes to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindBuildingTypes();
                }
            }

        #endregion

        #region Grid Events

            protected void gridBuildingTypes_OnDataBound(object sender, EventArgs e)
            {                          
            }

            protected void gridBuildingTypes_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }
            protected void gridBuildingTypes_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditBuildingType.Visible = false;
                dtvBuildingType.Visible = true;
                
                int buildingTypeID = Convert.ToInt32(gridBuildingTypes.DataKeys[gridBuildingTypes.SelectedIndex].Values["BuildingTypeID"]);

                //set data source
                dtvBuildingType.DataSource = DataMgr.BuildingTypeDataMapper.GetFullBuildingTypeByBuildingTypeID(buildingTypeID);
                //bind building type to details view
                dtvBuildingType.DataBind();
            }
            protected void gridBuildingTypes_Editing(object sender, GridViewEditEventArgs e)
            {
                pnlEditBuildingType.Visible = true;
                dtvBuildingType.Visible = false;

                int buildingTypeID = Convert.ToInt32(gridBuildingTypes.DataKeys[e.NewEditIndex].Values["BuildingTypeID"]);

                BuildingType mBuildingType = DataMgr.BuildingTypeDataMapper.GetBuildingType(buildingTypeID);

                //Set Building Type data
                SetBuildingTypeIntoEditForm(mBuildingType);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridBuildingTypes_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows buildingClassid
                int buildingTypeID = Convert.ToInt32(gridBuildingTypes.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a building class if no building type
                    //has been associated.                    

                    //check if a building is associated to that building type
                    bool hasBuilding = DataMgr.BuildingDataMapper.IsBuildingAssociatedWithBuildingType(buildingTypeID);

                    if (hasBuilding)
                    {
                        lblErrors.Text = buildingExists;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //delete building type
                        DataMgr.BuildingTypeDataMapper.DeleteBuildingType(buildingTypeID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting building type.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryBuildingTypes());
                gridBuildingTypes.PageIndex = gridBuildingTypes.PageIndex;
                gridBuildingTypes.DataSource = SortDataTable(dataTable as DataTable, true);
                gridBuildingTypes.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditBuildingType.Visible = false; 
            }

            protected void gridBuildingTypes_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBuildingTypes(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridBuildingTypes.DataSource = SortDataTable(dataTable, true);
                gridBuildingTypes.PageIndex = e.NewPageIndex;
                gridBuildingTypes.DataBind();
            }

            protected void gridBuildingTypes_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridBuildingTypes.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBuildingTypes(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridBuildingTypes.DataSource = SortDataTable(dataTable, false);
                gridBuildingTypes.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetBuildingTypeData> QueryBuildingTypes()
            {
                try
                {
                    //get all building types 
                    return DataMgr.BuildingTypeDataMapper.GetAllFullBuildingTypes();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building types.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building types.", ex);
                    return null;
                }
            }

            private IEnumerable<GetBuildingTypeData> QuerySearchedBuildingTypes(string searchText)
            {
                try
                {
                    //get all building types 
                    return DataMgr.BuildingTypeDataMapper.GetAllFullSearchedBuildingTypes(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building types.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building types.", ex);
                    return null;
                }
            }

            private void BindBuildingTypes()
            {
                //query building types
                IEnumerable<GetBuildingTypeData> types = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryBuildingTypes() : QuerySearchedBuildingTypes(txtSearch.Text);

                int count = types.Count();

                gridBuildingTypes.DataSource = types;

                // bind grid
                gridBuildingTypes.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedBuildingTypes(string[] searchText)
            //{
            //    //query building types
            //    IEnumerable<GetBuildingTypeData> types = QuerySearchedBuildingTypes(searchText);

            //    int count = types.Count();

            //    gridBuildingTypes.DataSource = types;

            //    // bind grid
            //    gridBuildingTypes.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} building type found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} building types found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No building types found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }      

        #endregion
    }
}