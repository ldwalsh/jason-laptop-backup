﻿using System;

namespace CW.Website._fileservices
{
    public enum AllowanceTypeEnum
    {
        AdminDownload, //admin grid downloads: equpment schedules.
        ModuleDownload, //module grid downloads: diagnostics, alarms.
        DocumentDownload, //document downloads: documents.
        DocumentUpload, //document uploads: documents.
        RawDataDownload, //raw data downloads: reporting.
        ReportDownload, //pdf report downloads: reporting, diagnostics, utility billing.
        ReportEmail, //pdf report emails: reporting, diagnostics, utility billing.
    }
}