﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;

using CW.Business;
using CW.Data.AzureStorage.Models;
using CW.Data.AzureStorage;
using CW.Data;
using CW.Data.Models.VAndVPre;
using CW.Common.Constants;

namespace CW.Website._fileservices
{
    public abstract class VDataFileGeneratorBase
    {
        public static IFileGenerator Create<T>(DataManager dataManager, IEnumerable<PointTypeDisplayNameAndVPID> vpoints, DateTime startDate, DateTime endDate, DataConstants.AnalysisRange analysisRange) where T : VDataFileGeneratorBase
        {
            return Activator.CreateInstance(typeof(T), new Object[] { dataManager, vpoints, startDate, endDate, analysisRange }) as IFileGenerator;
        } 

        protected   readonly
                    DataManager
                    dataManager;

        protected   readonly
                    IEnumerable<PointTypeDisplayNameAndVPID>
                    vpoints;

        protected   readonly
                    DateTime
                    startDate;

        protected   readonly
                    DateTime
                    endDate;

        protected readonly
                    DataConstants.AnalysisRange
                    analysisRange;

        protected VDataFileGeneratorBase(DataManager dataManager, IEnumerable<PointTypeDisplayNameAndVPID> vpoints, DateTime startDate, DateTime endDate, DataConstants.AnalysisRange analysisRange)
        {
            this.dataManager = dataManager; 
            this.vpoints = vpoints;
            this.startDate = startDate;
            this.endDate = endDate;
            this.analysisRange = analysisRange;
        }

        protected DataTable BuildTable(DataManager dataManager, IEnumerable<PointTypeDisplayNameAndVPID> vpoints, DateTime startDate, DateTime endDate, DataConstants.AnalysisRange analysisRange)
        {
            var datetimeColumn = new List<DateTime>();
                
            var start = startDate;
                
            while (start < endDate)
            {
                datetimeColumn.Add(start);

                switch (analysisRange)
                {
                    case DataConstants.AnalysisRange.Daily:
                        start = start.AddDays(1);
                        break;
                    case DataConstants.AnalysisRange.Weekly:
                        start = start.AddDays(7);
                        break;
                    case DataConstants.AnalysisRange.Monthly:
                        start = start.AddMonths(1);
                        break;
                    case DataConstants.AnalysisRange.HalfDay:
                        start = start.AddDays(1);
                        break;
                }
                
            }

            var dt = new DataTable();

            new DataSet().Tables.Add(dt);

            dt.Columns.Add(new DataColumn("DateTime"));
                
            var vpointDetails = new List<List<Object>>();

            var vData = dataManager.VAndVPreDataMapper.GetConvertedVDataPlotDataForVPIDs(vpoints.Select(vp => Convert.ToInt32(vp.VPID)).ToList(), startDate, endDate, analysisRange);

            foreach (var vpoint in vpoints) 
            {
                vpointDetails.Add(new List<Object> { Convert.ToInt32(vpoint.VPID), vpoint.DisplayName, vData.Where(p => p.VPID == Convert.ToInt32(vpoint.VPID)).ToList() });

                dt.Columns.Add(new DataColumn(vpoint.DisplayName));
            }

            foreach (var c in datetimeColumn)
            {
                var dr = dt.NewRow();

                dr["DateTime"] = c;

                foreach (var vpoint in vpointDetails)
                {
                    var data = ((IEnumerable<VDataFull>)vpoint[2]).Where(p => p.StartDate.ToShortDateString() == c.ToShortDateString());

                    dr[Convert.ToString(vpoint[1])] = ((data.Count() == 0) ? String.Empty : data.First().RawValue);
                }

                // add row to dataset now
                foreach (var point in vpointDetails)
                {
                    if (String.IsNullOrEmpty(dr[Convert.ToString(point[1])].ToString())) continue;
                        
                    dt.Rows.Add(dr);
                        
                    break;
                }
            }

            return dt;
        }
    }
}