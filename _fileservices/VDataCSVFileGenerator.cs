﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data.AzureStorage;
using CW.Data;
using CW.Data.Models.VAndVPre;
using CW.Common.Constants;

namespace CW.Website._fileservices
{
    public class VDataCSVFileGenerator : VDataFileGeneratorBase, IFileGenerator
    {
        public VDataCSVFileGenerator(DataManager dataManager, IEnumerable<PointTypeDisplayNameAndVPID> vpoints, DateTime startDate, DateTime endDate, DataConstants.AnalysisRange analysisRange)
            : base(dataManager, vpoints, startDate, endDate, analysisRange)
        {
        }

        public Byte[] Generate()
        {
            var dt = BuildTable(dataManager, vpoints, startDate, endDate, analysisRange);

            var sb = new StringBuilder();
                
            foreach (DataColumn column in dt.Columns)
            {
                sb.AppendFormat("{0},", column.ColumnName);
            }

            sb.Remove((sb.Length-1), 1);
            sb.Append("\r\n");

            foreach (DataRow row in dt.Rows)
            {
                for (var i=0; i<dt.Columns.Count; i++)
                {
                    sb.AppendFormat(((row[i].ToString() == "&nbsp;") ? String.Empty : row[i].ToString()));
                    sb.Append(",");
                }

                sb.Remove((sb.Length-1), 1);
                sb.Append("\r\n");
            }

            return Encoding.UTF8.GetBytes(sb.ToString());
        }

        public Byte[] Generate(out String fileName)
        {
            throw new NotImplementedException();
        }

        public AllowanceTypeEnum AllowanceType
        {
            get {return AllowanceTypeEnum.RawDataDownload;}
        }
    }
}