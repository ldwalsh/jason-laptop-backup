﻿using CW.Business;
using CW.Common.Config;
using CW.Common.Constants;
using CW.Common.Instrumentation;
using CW.Data;
using CW.Utility;
using CW.Website._administration.equipment;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using CW.Website.DependencyResolution;
using System;
using System.IO;
using System.Web;
using System.Web.UI;

namespace CW.Website._fileservices
{
	public class FilePublishService //TODO: this class probaly could be decomposed into 2+. This one handles to many types of scenarios.
	{
		#region field

			private readonly DataManager dataManager;
			private readonly ClientDataMapper clientDataMapper;        
			private readonly IFileGenerator fileGenerator;
			private readonly ISiteUser siteUser;
			private readonly ISectionLogManager logger;
			//

			private readonly ClientSetting settings;           

		#endregion

		#region constructor

			public FilePublishService(DataManager dataManager, ISiteUser siteUser, IFileGenerator fileGenerator, ISectionLogManager logger)
			{
				this.dataManager      = dataManager;
				this.clientDataMapper = new ClientDataMapper(dataManager);
				this.fileGenerator    = fileGenerator;
				this.siteUser         = siteUser;
				this.logger           = logger;

				settings = clientDataMapper.GetClientSettingsByClientID(siteUser.CID);
			}

			public FilePublishService(ISiteUser siteUser, IFileGenerator fileGenerator, ISectionLogManager logger, DataManager dm): this(dm, siteUser, fileGenerator, logger)
			{
			}

			public FilePublishService(ISiteUser siteUser, IFileGenerator fileGenerator)
			{
				var container = new IoC();

				this.dataManager      = siteUser.DataMgr;
				this.clientDataMapper = new ClientDataMapper(dataManager);
				this.fileGenerator    = fileGenerator;
				this.siteUser         = siteUser;
				this.logger           = container.Resolve<ISectionLogManager>();

				settings = clientDataMapper.GetClientSettingsByClientID(siteUser.CID);
			}

			public FilePublishService(ISiteUser siteUser, ISectionLogManager logger, DataManager dm)
			{
				this.logger       = logger;
				this.dataManager = dm;
			}

		#endregion

		public void IncrementAllowance(Byte[] content, AllowanceTypeEnum allowanceType)
		{
			if (siteUser.IsKGSFullAdminOrHigher) return;

			var monthly = new ReflectionHelper.Property<Int64>(settings, String.Format("CurrentMonthly{0}s", allowanceType));
			var max     = new ReflectionHelper.Property<Int64>(settings, String.Format("MaxMonthly{0}s", allowanceType));
			var total   = new ReflectionHelper.Property<Int64>(settings, String.Format("CurrentTotal{0}s", allowanceType));

			if ((monthly.Value + content.LongLength) < max.Value)
			{
				monthly.Value += content.LongLength;
				total  .Value += content.LongLength;

				clientDataMapper.UpdateClientAccountSettings(settings);

				return;
			}

			throw new MonthlyDownloadAllowanceReachedException(settings.CID, fileGenerator.AllowanceType, logger);
		}

		public Boolean SendInEmail(String subject, String body)
		{
			if (HasReachedMonthlyReportEmails)
				throw new MonthlyReportEmailAllowanceReachedException(settings.CID, fileGenerator.AllowanceType, logger);

			var byteArray = fileGenerator.Generate();

			try
			{
				using (var ms = new MemoryStream(byteArray))
				{
					Email.SendReportEmail(siteUser.Email, null, subject, body, ms);
				}
			}
			catch(Exception ex)
			{
				logger.Log(DataConstants.LogLevel.ERROR, "Error sending report email.", ex);

				return false;
			}

			if (HasNoAllowances) return true;
            
			IncrementMonthlyReportsEmailed();

			return true;
		}

		public void AttachAsDownload()
		{
			AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher());
		}
		//
		public void AttachAsDownload(String fileName, Byte[] byteArray)
		{
			new DownloadRequestHandler.HandledFileAttacher().AttachForDownload(fileName, byteArray);
		}
		//
		//public void AttachAsDownload(IFileAttacher fileAttacher, String fileName)
		//{
		//	var bytes = fileGenerator.Generate();
			
		//	IncrementAllowance(bytes, fileGenerator.AllowanceType);

		//	fileAttacher.AttachForDownload(fileName, bytes);
		//}
		//
		public void AttachAsDownload(IFileAttacher fileAttacher)
		{
			byte[] byteArray;
			String fileName;

			if (HasDataCheck(fileAttacher, out byteArray, out fileName))
			{
				IncrementAllowance(byteArray, fileGenerator.AllowanceType);

				fileAttacher.AttachForDownload(fileName, byteArray);
			}
		}

		public Boolean AttachAsDownloadWithNoDataCheck()
		{
			return AttachAsDownloadWithNoDataCheck(new DownloadRequestHandler.HandledFileAttacher());
		}
		//
		public Boolean AttachAsDownloadWithNoDataCheck(IFileAttacher fileAttacher)
		{
			byte[] byteArray;
			String fileName;

			if (HasDataCheck(fileAttacher, out byteArray, out fileName))
			{
				IncrementAllowance(byteArray, fileGenerator.AllowanceType);

				fileAttacher.AttachForDownload(fileName, byteArray);

				return true;
			}
            
			return false;
		}

		public Boolean HasDataCheck()
		{
			byte[] ignoreByteArray;
			string ignoreFileName;
			return HasDataCheck(new DownloadRequestHandler.HandledFileAttacher(), out ignoreByteArray, out ignoreFileName);
		}
		//
		public Boolean HasDataCheck(IFileAttacher fileAttacher, out byte[] byteArray, out String fileName)
		{
			byteArray = fileGenerator.Generate(out fileName);

			return (byteArray.Length > 0);
		}

		public Boolean AttachAsDownloadWithFileName(String fileName)
		{
			return AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), fileName);
		}

		public Boolean AttachAsDownload(IFileAttacher fileAttacher, String fileName)
		{
			Byte[] byteArray = null;

			var reachedAllowance = false;

			if (fileGenerator is RawFileGeneratorBase)
			{
				byteArray = fileGenerator.Generate();

				reachedAllowance = WillReachMonthlyRawDataDownloads(byteArray.Length);
			}
			else if (fileGenerator is DiagnosticsXLSFileGenerator)
			{
				byteArray = fileGenerator.Generate();
                
				reachedAllowance = WillReachMonthlyDiagnosticsDownloads(byteArray.Length);
			}
			else if (fileGenerator is ReportFileGenerator)
			{
				reachedAllowance = HasReachedMonthlyReportDownloads;
			}
			else if (fileGenerator is EquipmentScheduleGenerator)
			{
				reachedAllowance = WillReachMonthlyAdminDownloads(byteArray.Length);
			}
			//else other file not handled

			if (reachedAllowance) throw new MonthlyDownloadAllowanceReachedException(settings.CID, fileGenerator.AllowanceType, logger);

			if (byteArray == null)
			{
				byteArray = fileGenerator.Generate();
			}

			SetDefaultFileNameIfEmpty(ref fileName);

			try
			{
				//if (response != null)
				//{
				//    response.Clear();
				//    new ContentAttacher(response).Attach(byteArray, fileName);
				//}
				//else if(page != null)
				//{
				if (byteArray.Length > 0)
				{
					HttpContext.Current.Response.Clear(); //not sure what this does... hopefully don't need
					fileAttacher.AttachForDownload(fileName, byteArray);
				}
				else
				{
					//no data
					return false;
				}
				//}
			}
			catch (Exception ex)
			{
				if (fileGenerator is RawFileGeneratorBase)
				{
					logger.Log(DataConstants.LogLevel.ERROR, "Error downloading excel or csv on diagnostics or reports page.", ex);
				}
				else if (fileGenerator is DiagnosticsXLSFileGenerator)
				{
					logger.Log(DataConstants.LogLevel.ERROR, "Error downloading diagnostics excel grid.", ex);
				}
				else if (fileGenerator is ReportFileGenerator)
				{
					logger.Log(DataConstants.LogLevel.ERROR, "Error downloading report on diagnostics or reports page.", ex);
				}
				else if (fileGenerator is EquipmentScheduleGenerator)
				{
					logger.Log(DataConstants.LogLevel.ERROR, "Error downloading equipment schedule.", ex);
				}

				return false;
			}

			if (HasNoAllowances) return true;

			if (fileGenerator is RawFileGeneratorBase)
			{
				IncrementMonthlyRawDataDownloadedSize(byteArray.Length);
			}
			else if (fileGenerator is DiagnosticsXLSFileGenerator)
			{
				IncrementMonthlyDiagnosticsDownloadedSize(byteArray.Length);
			}
			else if (fileGenerator is ReportFileGenerator)
			{
				IncrementMontlyReportsDownloaded();
			}
			else if (fileGenerator is EquipmentScheduleGenerator)
			{
				IncrementMonthlyAdminDownloadedSize(byteArray.Length);
			}

			return true;
		}

		#region Admin

		public Boolean HasReachedMonthlyAdminDownloads
		{
			get
			{
				if (HasNoAllowances) return false;

				return !(settings.CurrentMonthlyAdminDownloads < settings.MaxMonthlyAdminDownloads);
			}
		}

		public Boolean WillReachMonthlyAdminDownloads(long size)
		{
			if (HasNoAllowances) return false;

			return !((settings.CurrentMonthlyAdminDownloads + size) < settings.MaxMonthlyAdminDownloads);
		}

		public void IncrementMonthlyAdminDownloadedSize(long size)
		{
			if (HasNoAllowances) return;

			settings.CurrentMonthlyAdminDownloads = (settings.CurrentMonthlyAdminDownloads + size);
			settings.CurrentTotalAdminDownloads = (settings.CurrentTotalAdminDownloads + size);

			dataManager.ClientDataMapper.UpdateClientAccountSettings(settings);
		}

		#endregion

		#region Emails

		public Boolean HasReachedMonthlyReportEmails
		{
			get
			{
				if (HasNoAllowances) return false;

				return !(settings.CurrentMonthlyReportEmails < settings.MaxMonthlyReportEmails);
			}
		}

		public void IncrementMonthlyReportsEmailed()
		{
			if (HasNoAllowances) return;

			settings.CurrentMonthlyReportEmails++;
			settings.CurrentTotalReportEmails++;

			dataManager.ClientDataMapper.UpdateClientAccountSettings(settings);
		}

		#endregion

		#region Reports

		public Boolean HasReachedMonthlyReportDownloads
		{
			get
			{
				if (HasNoAllowances) return false;

				return !(settings.CurrentMonthlyReportDownloads < settings.MaxMonthlyReportDownloads);
			}
		}

		public void IncrementMontlyReportsDownloaded()
		{
			if (HasNoAllowances) return;

			settings.CurrentMonthlyReportDownloads++;
			settings.CurrentTotalReportDownloads++;

			dataManager.ClientDataMapper.UpdateClientAccountSettings(settings);
		}

		#endregion

		#region Raw Data

		public Boolean HasReachedMonthlyRawDataDownloads
		{
			get
			{
				if (HasNoAllowances) return false;

				return !(settings.CurrentMonthlyRawDataDownloads < settings.MaxMonthlyRawDataDownloads);
			}
		}

		public Boolean WillReachMonthlyRawDataDownloads(long size)
		{
			if (HasNoAllowances) return false;

			return !((settings.CurrentMonthlyRawDataDownloads + size) < settings.MaxMonthlyRawDataDownloads);
		}

		public void IncrementMonthlyRawDataDownloadedSize(long size)
		{
			if (HasNoAllowances) return;

			settings.CurrentMonthlyRawDataDownloads = (settings.CurrentMonthlyRawDataDownloads + size);
			settings.CurrentTotalRawDataDownloads = (settings.CurrentTotalRawDataDownloads + size);

			dataManager.ClientDataMapper.UpdateClientAccountSettings(settings);
		}

		#endregion

		#region Diagnostics

		public Boolean HasReachedMonthlyDiagnosticsDownloads
		{
			get
			{
				if (HasNoAllowances) return false;

				return !(settings.CurrentMonthlyModuleDownloads < settings.MaxMonthlyModuleDownloads);
			}
		}

		public Boolean WillReachMonthlyDiagnosticsDownloads(long size)
		{
			if (HasNoAllowances) return false;

			return !((settings.CurrentMonthlyModuleDownloads + size) < settings.MaxMonthlyModuleDownloads);
		}

		public void IncrementMonthlyDiagnosticsDownloadedSize(long size)
		{
			if (HasNoAllowances) return;

			settings.CurrentMonthlyModuleDownloads = (settings.CurrentMonthlyModuleDownloads + size);
			settings.CurrentTotalModuleDownloads = (settings.CurrentTotalModuleDownloads + size);

			dataManager.ClientDataMapper.UpdateClientAccountSettings(settings);
		}

		#endregion

		private Boolean HasNoAllowances
		{
			get {return siteUser.IsKGSFullAdminOrHigher;}
		}

		private void SetDefaultFileNameIfEmpty(ref String fileName)
		{
			if (!String.IsNullOrWhiteSpace(fileName)) return;

				fileName = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productFile", true);
		}

		#region custom exceptions

			public class MonthlyReportEmailAllowanceReachedException: ExceptionBase
			{
				public MonthlyReportEmailAllowanceReachedException(Int32 CID, AllowanceTypeEnum allowanceType, ISectionLogManager logger)
				{
					var m =
					String.Format
					(
						"The monthly '{0}' email allowance for client {1} has been reached.",
						new Object[]{CID, allowanceType}
					);

					SetMessage(m);

					logger.Log(DataConstants.LogLevel.INFO, m);
				}
			}

			public class MonthlyDownloadAllowanceReachedException: ExceptionBase
			{
				public MonthlyDownloadAllowanceReachedException(Int32 CID, AllowanceTypeEnum allowanceType, ISectionLogManager logger)
				{
					var m =
					String.Format
					(
						"The monthly '{0}' download allowance for client {1} has been reached.",
						new Object[]{allowanceType, CID}
					);

					SetMessage(m);

					logger.Log(DataConstants.LogLevel.INFO, m);
				}
			}

		#endregion
	}
}