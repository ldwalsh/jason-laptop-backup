﻿using System;
using System.IO;
using CW.Common.Constants;
using CW.Common.Helpers;
using CW.Utility;
using CW.Website._framework;
using CW.Common.Instrumentation;

namespace CW.Website._fileservices
{
    public sealed class ReportFileGenerator: IFileGenerator
    {

        private Byte[] byteArray;

        public static ReportFileGenerator Create(SiteUser siteUser, ISectionLogManager logger, ITextSharpHelper.AnalysisPDFReport analysisPDFReport, ITextSharpHelper.AnalysisPDFReport analysisPDFReport2, ITextSharpHelper.RawPDFReport rawPDFReport, ITextSharpHelper.RawPDFReport rawPDFReport2, ITextSharpHelper.CalculatedPDFReport calculatedPDFReport, ITextSharpHelper.CalculatedPDFReport calculatedPDFReport2, string reportComments)
        {               
            var reportTitle = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product Report for ");
            var reportHeader = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product Report for ");

            reportTitle = ITextSharpHelper.GenerateReportTitle(reportTitle, analysisPDFReport, analysisPDFReport2, rawPDFReport, rawPDFReport2);
            reportHeader = ITextSharpHelper.GenerateReportHeader(reportHeader, analysisPDFReport, analysisPDFReport2, rawPDFReport, rawPDFReport2);

            var memoryStream = new MemoryStream();

            Byte[] byteArray;

            try
            {
                ITextSharpHelper.GeneratePDF(memoryStream, reportTitle, reportHeader, siteUser.FullName, analysisPDFReport, analysisPDFReport2, rawPDFReport, rawPDFReport2, calculatedPDFReport, calculatedPDFReport2, reportComments, siteUser.IsSchneiderTheme);

                byteArray = memoryStream.ToArray();
            }
            catch(Exception ex)
            {
                logger.Log(DataConstants.LogLevel.ERROR, "Error generating report pdf.", ex);

                return null;
            }
            finally
            {
                memoryStream.Close();
            }

            return new ReportFileGenerator(byteArray);
        }

        private ReportFileGenerator(Byte[] byteArray)
        {
            //future implementation of ReportGenerator can handle caching of reports

            //Get report memorystream session, and convert to byte array                
            //byteArray = (ITextSharpHelper.AddComments((MemoryStream)Session["Report"], reportComments)).ToArray();                        
            //byteArray = ((MemoryStream)Session["Report"]).ToArray();
            //byte[] byteArray = memoryStream.ToArray();

            this.byteArray = byteArray;
        }

        public AllowanceTypeEnum AllowanceType
        {
            get {return AllowanceTypeEnum.ReportDownload;}
        }

        public Byte[] Generate()
        {
            return byteArray;
        }

        public Byte[] Generate(out String fileName)
        {
            throw new NotImplementedException();
        }
    }
}