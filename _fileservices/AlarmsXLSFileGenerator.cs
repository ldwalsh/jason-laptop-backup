﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace CW.Website._fileservices
{
    public class AlarmsXLSFileGenerator: GridViewXLSFileGeneratorBase, IFileGenerator
    {
        public AlarmsXLSFileGenerator(GridView gv, bool currentPageOnly, int[] removeColumns, bool showLinks)
            : base(gv, currentPageOnly, removeColumns, showLinks)
        {
        }

        public AllowanceTypeEnum AllowanceType
        {
            get { return AllowanceTypeEnum.ModuleDownload; }
        }
    }
}