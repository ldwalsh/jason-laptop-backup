﻿using System;
using System.IO;
using System.Web;

namespace CW.Website._fileservices
{
	public class GenericResponseStreamAttacher: IFileAttacher
	{
		private String getContentType(HttpResponse response, String fileName, Object fileContent)
		{
			switch (new FileInfo(fileName).Extension.ToLower())
			{
				case ".pdf": return "application/pdf";
				case ".csv": return "application/csv";
				
				default: throw new NotSupportedException();
			}
		}

		public void AttachForDownload(String fileName, Byte[] fileContent)
		{
			var res = HttpContext.Current.Response;

			res.Clear();

			res.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			res.AddHeader("Content-Length", fileContent.LongLength.ToString());

			res.BinaryWrite(fileContent);

			res.ContentType = getContentType(res, fileName, fileContent);

			res.Flush();
		}
	}
}