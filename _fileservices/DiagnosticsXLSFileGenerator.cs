﻿using System;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Utility;
using CW.Website._framework;

namespace CW.Website._fileservices
{
    public class DiagnosticsXLSFileGenerator: GridViewXLSFileGeneratorBase, IFileGenerator
    {
        public DiagnosticsXLSFileGenerator(GridView gv, bool currentPageOnly, int[] removeColumns, bool showLinks)
            : base(gv, currentPageOnly, removeColumns, showLinks)
        {
        }

        public AllowanceTypeEnum AllowanceType
        {
            get { return AllowanceTypeEnum.ModuleDownload; }
        }
    }
}