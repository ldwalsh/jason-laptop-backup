﻿using System;
using System.Web.UI;

namespace CW.Website._fileservices
{
    public interface IFileAttacher
    {
        void AttachForDownload(String fileName, Byte[] fileContent);
    }
}