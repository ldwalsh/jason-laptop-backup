﻿using System;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Utility;
using CW.Website._framework;

namespace CW.Website._fileservices
{
    public class GridViewXLSFileGeneratorBase
    {
        private readonly GridView gv;
        private readonly bool currentPageOnly;
        private readonly int[] removeColumns;
        private readonly bool showLinks;

        public GridViewXLSFileGeneratorBase(GridView gv, bool currentPageOnly, int[] removeColumns, bool showLinks)
        {
            this.gv = gv;
            this.currentPageOnly = currentPageOnly;
            this.removeColumns = removeColumns;
            this.showLinks = showLinks;
        }

        public Byte[] Generate()
        {
            Table tbl = GridViewHelper.PrepareForExport(gv, currentPageOnly, removeColumns, SiteUser.Current.IsSchneiderTheme, showLinks);

            var stringWriter = new StringWriter();
            
            tbl.RenderControl(new HtmlTextWriter(stringWriter));

            var sb = new StringBuilder(stringWriter.ToString());

            stringWriter.Close();

            return Encoding.UTF8.GetBytes(sb.ToString());
        }

        public Byte[] Generate(out String fileName)
        {
            throw new NotImplementedException();
        }
    }
}