﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;
using CW.Utility;

namespace CW.Website._fileservices
{
    public class RawCSVFileGenerator: RawFileGeneratorBase, IFileGenerator
    {
        public RawCSVFileGenerator(DataManager dataManager, IEnumerable<Point> points, DateTime startDate, DateTime endDate, Boolean isSyncronized):base(dataManager, points, startDate, endDate, isSyncronized)
        {
        }

        public Byte[] Generate()
        {
            DataTable dt = new DataTable();

            //TODO: Do we even need to build tables for csv?
            if (isSyncronized)
            {
                dt = BuildTableSynchronized(dataManager, points, startDate, endDate);

                if (dt.Rows.Count == 0) return new Byte[0];
            }
            else
            {
                dt = BuildTable(dataManager, points, startDate, endDate);
            }
 
            var sb = new StringBuilder();
                
            foreach (DataColumn column in dt.Columns)
            {
                sb.AppendFormat("{0},", column.ColumnName);
            }

            sb.Remove((sb.Length-1), 1);
            sb.Append("\r\n");

            foreach (DataRow row in dt.Rows)
            {
                for (var i=0; i<dt.Columns.Count; i++)
                {
                    sb.AppendFormat(StringHelper.EscapeCurlyBracesForStringBuilder((row[i].ToString() == "&nbsp;") ? String.Empty : row[i].ToString()));
                    sb.Append(",");
                }

                sb.Remove((sb.Length-1), 1);
                sb.Append("\r\n");
            }

            return Encoding.UTF8.GetBytes(sb.ToString());
        }

        public Byte[] Generate(out String fileName)
        {
            throw new NotImplementedException();
        }

        public AllowanceTypeEnum AllowanceType
        {
            get {return AllowanceTypeEnum.RawDataDownload;}
        }
    }
}