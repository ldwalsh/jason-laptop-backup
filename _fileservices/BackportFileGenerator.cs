﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;
using CW.Utility;

namespace CW.Website._fileservices
{
    public class BackportFileGenerator: IFileGenerator
    {
        private readonly DataManager mDataManager;         
        private readonly string mBPID;
        private readonly int mCID;  
        
        public BackportFileGenerator(DataManager dataManager, int cid, string bpid)
        {
            mDataManager = dataManager;
            mBPID = bpid;
            mCID = cid;
        }

        public Byte[] Generate()
        {
            var blob = mDataManager.BackportDataMapper.GetBackportJobBlob(mCID, mBPID);

            if(blob == null || blob.Asset == null)
            {
                return new byte[]{};
            }
            return blob.Asset;
        }

        public Byte[] Generate(out String fileName)
        {
            var b = mDataManager.BackportDataMapper.GetBackportJobEntity(mCID, mBPID);

            fileName = b.FromTableName + b.StartDate.ToShortDateString() + "-" + b.EndDate.ToShortDateString() + ".csv";

            return Generate();
        }

        public AllowanceTypeEnum AllowanceType
        {
            get {return AllowanceTypeEnum.AdminDownload;}
        }
    }
}