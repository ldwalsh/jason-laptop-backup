﻿using System;

namespace CW.Website._fileservices
{
    public interface IFileGenerator
    {
        AllowanceTypeEnum AllowanceType{get;}

        Byte[] Generate();
        Byte[] Generate(out String fileName);
    }
}