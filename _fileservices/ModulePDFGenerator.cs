﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._fileservices
{
    public sealed class ModulePDFGenerator: IFileGenerator
    {
         private Byte[] byteArray;

         public ModulePDFGenerator(Byte[] byteArray)
         {
             this.byteArray = byteArray;
         }

         public AllowanceTypeEnum AllowanceType
         {
             get {return AllowanceTypeEnum.ModuleDownload;}
         }

         public Byte[] Generate()
         {
             return byteArray;
         }

         public Byte[] Generate(out String fileName)
         {
             throw new NotImplementedException();
         }
    }
}