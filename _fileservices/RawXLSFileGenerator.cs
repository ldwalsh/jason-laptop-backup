﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data;
using System.Diagnostics;

namespace CW.Website._fileservices
{
    public class RawXLSFileGenerator: RawFileGeneratorBase, IFileGenerator
    {
        public RawXLSFileGenerator(DataManager dataManager, IEnumerable<Point> points, DateTime startDate, DateTime endDate, Boolean isSyncronized):base(dataManager, points, startDate, endDate, isSyncronized)
        {
        }

        public Byte[] Generate()
        {
            GridView gv;

            if (isSyncronized)
            {
                Stopwatch rawDataPull2 = new Stopwatch();
                rawDataPull2.Start();
                var dt = BuildTableSynchronized(dataManager, points, startDate, endDate);
                rawDataPull2.Stop();

                if (dt.Rows.Count == 0) return new Byte[0];

                gv = new GridView { DataSource = dt };

                gv.DataBind();
            }
            else
            {
                Stopwatch rawDataPull = new Stopwatch();
                rawDataPull.Start();
                gv = BuildGridView(dataManager, points, startDate, endDate);
                rawDataPull.Stop();
            }

            var stringWriter = new StringWriter();
            
            gv.RenderControl(new HtmlTextWriter(stringWriter));

            var sb = new StringBuilder(stringWriter.ToString());

            stringWriter.Close();

            return Encoding.UTF8.GetBytes(sb.ToString());
        }

        public Byte[] Generate(out String fileName)
        {
            throw new NotImplementedException();
        }

        public AllowanceTypeEnum AllowanceType
        {
            get {return AllowanceTypeEnum.RawDataDownload;}
        }
    }
}