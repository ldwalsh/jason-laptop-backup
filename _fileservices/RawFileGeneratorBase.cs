﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data.AzureStorage.Models;
using CW.Data.Models.Raw;
using CW.Data;
using CW.Data.Helpers;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using CW.Data.Models.Reporting;
using System.Diagnostics;
using System.Threading;

using CW.Utility;
namespace CW.Website._fileservices
{
    public abstract class RawFileGeneratorBase
    {
        public static IFileGenerator Create<T>(DataManager dataManager, IEnumerable<Point> points, DateTime startDate, DateTime endDate, Boolean isSyncronized) where T: RawFileGeneratorBase
        {
            return Activator.CreateInstance(typeof(T), new Object[]{dataManager, points, startDate, endDate, isSyncronized}) as IFileGenerator;
        } 

        protected   readonly
                    DataManager
                    dataManager;

        protected   readonly
                    IEnumerable<Point>
                    points;

        protected   readonly
                    DateTime
                    startDate;

        protected   readonly
                    DateTime
                    endDate;

        protected readonly
                    Boolean
                    isSyncronized;

        protected readonly
                    int
                    displayInterval;

        protected RawFileGeneratorBase(DataManager dataManager, IEnumerable<Point> points, DateTime startDate, DateTime endDate, Boolean isSyncronized)
        {
            this.dataManager = dataManager;
            this.points = points;
            this.startDate = startDate;
            this.endDate = endDate;
            this.isSyncronized = isSyncronized;
            this.displayInterval = Utility.MathHelper.GCD(points.Select(p => p.SamplingInterval));
        }

        protected GridView BuildGridView(DataManager dataManager, IEnumerable<Point> points, DateTime startDate, DateTime endDate)
        {
            var rawData = dataManager.RawDataMapper.GetRawDataForPIDs(points.Select(p => p.PID).ToList(), startDate, endDate, true)
                .OrderBy(r => r.PointName).ThenBy(r => r.PID).ThenBy(r => r.DateLoggedLocal);

            GridView gv = new GridView();
            gv.AutoGenerateColumns = false;

            BoundField bf = new BoundField();
            bf.HeaderText = bf.DataField = "PointName";
            gv.Columns.Add(bf);

            bf = new BoundField();
            bf.HeaderText = bf.DataField = "DateLoggedLocal";
            gv.Columns.Add(bf);

            bf = new BoundField();
            bf.HeaderText = bf.DataField = "RawValue";
            gv.Columns.Add(bf);

            gv.DataSource = rawData;
            gv.DataBind();

            return gv;
        }
        protected DataTable BuildTable(DataManager dataManager, IEnumerable<Point> points, DateTime startDate, DateTime endDate)
        {
            var rawData = dataManager.RawDataMapper.GetRawDataForPIDs(points.Select(p => p.PID).ToList(), startDate, endDate, true)
                .OrderBy(r => r.PointName).ThenBy(r => r.PID).ThenBy(r => r.DateLoggedLocal);

            var dt = new DataTable();

            new DataSet().Tables.Add(dt);

            dt.Columns.Add("PointName", typeof(String));
            dt.Columns.Add("DateLoggedLocal", typeof(DateTime));
            dt.Columns.Add("RawValue", typeof(String));

            foreach (GetFullRawData r in rawData)
            {
                dt.Rows.Add(new object[] { r.PointName, r.DateLoggedLocal, r.RawValue });
            }
       
            return dt;
        }
        protected DataTable BuildTableSynchronized(DataManager dataManager, IEnumerable<Point> points, DateTime startDate, DateTime endDate)
        {                
            var pointDetails = new List<List<Object>>();

            var rawData = dataManager.RawDataMapper.GetRawDataForPIDs(points.Select(p => p.PID).ToList(), startDate, endDate, true);

            PointHelper.IncrementDuplicatePointNames(points);

            // Create Report Table
            ReportTable reportTable = new ReportTable();

            // Add Columns
            Dictionary<int, IEnumerable<GetFullRawData>> lookup = SetColumns(points, rawData, reportTable);

            var datetimeCells = new List<DateTime>();
            var start = startDate;
            
            while (start < endDate)
            {
                datetimeCells.Add(start);

                start = start.AddMinutes(this.displayInterval);
            }

            
            // Add Rows
            AddRowsParallel(datetimeCells, lookup, reportTable);

            // Convert ReportTable to DataTable
            DataTable dt = CreateDataTable(reportTable.Columns, reportTable.Rows);

            // Sort DataTable by DateTime column
            if (dt.Rows.Count > 0)
            {
                DataView dv = dt.DefaultView;
                dv.Sort = "DateTime";
                dt = dv.ToTable();
            }

            return dt;
        }

        #region ReportTable Adapter Methods
        
        private DataTable CreateDataTable(IEnumerable<ReportColumn> columns, IEnumerable<ReportRow> rows)
        {
            var dt = new DataTable();

            //new DataSet().Tables.Add(dt);

            foreach (ReportColumn reportColumn in columns)
            {
                if (reportColumn.DataValueType == null)
                {
                    throw new ArgumentNullException(reportColumn.DataValueType.Name);
                }

                dt.Columns.Add(reportColumn.Name, reportColumn.DataValueType);
            }

            foreach (ReportRow reportRow in rows)
            {
                DataRow dr = dt.NewRow();

                foreach (ReportRowCell rowCell in reportRow.RowCells)
                {
                    dr[rowCell.Column.Name] = rowCell.CellValue;
                }

                dt.Rows.Add(dr);
            }

            return dt;
        }

        #endregion
        
        #region ReportTable Builder Methods
        private void AddRows(List<DateTime> datetimeColumns, Dictionary<int,IEnumerable<GetFullRawData>> rawDataLookup, ReportTable reportTable)
        {
            List<ReportRow> rows = new List<ReportRow>();

            foreach (DateTime dtc in datetimeColumns)
            {
                ReportRow row = CreateReportRow(
                    dtc,
                    rawDataLookup,
                    reportTable.Columns);

                if (row != null) { rows.Add(row); }
            }

            // Set Rows
            reportTable.Rows = rows.AsEnumerable();
        }

        #region Parallel Code

        private void AddRowsParallel(List<DateTime> datetimeColumns, Dictionary<int, IEnumerable<GetFullRawData>> rawDataLookup, ReportTable reportTable)
        {
            // Query Results Queue
            var queryQueue = new ConcurrentQueue<DateTime>();
            var resultsQueue = new ConcurrentQueue<ReportRow>();

            foreach (DateTime dtc in datetimeColumns)
            {
                queryQueue.Enqueue(dtc);
            }

            // Create Action delegates
            Action action = () =>
            {
                DateTime localValue;
                while (queryQueue.TryDequeue(out localValue))
                {
                    ReportRow row = CreateReportRow(
                        localValue,
                        rawDataLookup,
                        reportTable.Columns);

                    if (row != null) { resultsQueue.Enqueue(row); }

                    // Sleep to throttle parallel processing
                    Thread.Sleep(2);
                }
            };

            // Start 4 concurrent consuming actions.
            Parallel.Invoke(action, action, action, action);

            // Gather results
            List<ReportRow> rows = new List<ReportRow>();
            ReportRow resultValue;
            while (resultsQueue.TryDequeue(out resultValue))
            {
                rows.Add(resultValue);
            }

            // Set Rows
            reportTable.Rows = rows.AsEnumerable();
        }
        #endregion

        private Dictionary<int, IEnumerable<GetFullRawData>> SetColumns(IEnumerable<Point> points, IEnumerable<GetFullRawData> rawData, ReportTable reportTable)
        {
            List<ReportColumn> columns = new List<ReportColumn>();
            var lookup = new Dictionary<int, IEnumerable<GetFullRawData>>();

            columns.Add(new ReportColumn() { Name = "DateTime", DataValueType = typeof(DateTime) });

            foreach (var point in points)
            {
                lookup.Add(point.PID, rawData.Where(d => d.PID == point.PID).ToList());

                columns.Add(new ReportColumn() { ID = point.PID, Name = point.PointName, DataValueType = typeof(String) });
            }

            reportTable.Columns = columns.AsEnumerable();

            return lookup;
        }

        private ReportRow CreateReportRow(DateTime rowInterval, Dictionary<int,IEnumerable<GetFullRawData>> rawDataLookup, IEnumerable<ReportColumn> columns)
        {
            ReportRow row = null;
            List<ReportRowCell> cells = new List<ReportRowCell>();

            cells.Add(new ReportRowCell() { CellValue = rowInterval, Column = columns.Where(c => c.Name == "DateTime").First() });

            bool hasRowData = false;
            
            foreach (var column in columns)
            {
                if (column.ID == 0)
                {
                    continue;
                }

                double intervalInSeconds = (this.displayInterval / 2.0) * 60;

                var cellRawData = rawDataLookup[column.ID]
                    .Where(p => p.DateLoggedLocal.CompareTo(rowInterval.AddSeconds(intervalInSeconds)) <= 0 &&
                        p.DateLoggedLocal.CompareTo(rowInterval.AddSeconds(-intervalInSeconds)) >= 0);


                if (!cellRawData.Any())
                {
                    cells.Add(new ReportRowCell() { CellValue = String.Empty, Column = column });
                }
                else
                {
                    cells.Add(new ReportRowCell() { CellValue = cellRawData.First().RawValue, Column = column });
                    hasRowData = true;
                }
            }

            if (hasRowData)
            {
                row = new ReportRow();
                row.RowCells = cells;
            }

            return row;
        }        
        
        #endregion
    }
}