﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CW.Business.Query;
using CW.Data;
using CW.Utility.OpenDocument.Spreadsheet;
using CW.Website._fileservices;

namespace CW.Website._administration.equipment
{
    public sealed class EquipmentScheduleGenerator: IFileGenerator
    {
        #region STRUCT
        
            public struct BuildingInfo
            {
                public readonly Int32  BID;
                public readonly String Name;

                public BuildingInfo(Int32 bid, String name):this()
                {
                    BID  = bid;
                    Name = name;
                }
            }
        
        #endregion
        
        #region field

            private readonly BuildingInfo buildingInfo;

        #endregion

        #region constructor

            public EquipmentScheduleGenerator(BuildingInfo buildingInfo)
            {
                this.buildingInfo = buildingInfo;
            }
        
        #endregion

        #region IFileGenerator

            #region method

                Byte[] IFileGenerator.Generate(out String fileName)
                {
                    fileName = String.Format("Equipment Schedule for building {0}.xlsx", buildingInfo.Name);

                    var allEquipVarVals =
                        new Equipment_EquipmentVariableQuery(null)
							.LoadWith<Equipment, EquipmentType>(_=>_.Equipment, _=>_.EquipmentType, _=>_.EquipmentClass)
							.LoadWith(_=>_.EquipmentVariable)
							.FinalizeLoadWith
                            .All
                            .ByBuilding(buildingInfo.BID)
                            .Sort(_=>_.Equipment.EquipmentType.EquipmentClass.EquipmentClassName)
                            .ToList();

                    //

                    if (allEquipVarVals.Count() == 0) return new Byte[0];

                    var data = new Dictionary<EquipmentClass,Dictionary<EquipmentVariable,IList<Equipment_EquipmentVariable>>>();

                    foreach (var val in allEquipVarVals)
                    {
                        var equipClass = val.Equipment.EquipmentType.EquipmentClass;

                        Dictionary<EquipmentVariable,IList<Equipment_EquipmentVariable>> valsByVar;

                        if (!data.TryGetValue(equipClass, out valsByVar))
                        {
                            valsByVar = new Dictionary<EquipmentVariable,IList<Equipment_EquipmentVariable>>();

                            data.Add(equipClass, valsByVar);
                        }

                        IList<Equipment_EquipmentVariable> vals;

                        if (!valsByVar.TryGetValue(val.EquipmentVariable, out vals))
                        {
                            vals = new List<Equipment_EquipmentVariable>();
                    
                            valsByVar.Add(val.EquipmentVariable, vals);
                        }

                        vals.Add(val);
                    }

                    using (var ssd = new SpreadsheetDoc())
                    {
                        foreach (var d in data)
                        {
                            var ws = ssd.CreateWorksheet(d.Key.EquipmentClassName);

                            //
                            //build header row

                            var headerRow = ws.AddRow(new[]{"EquipmentName", "EquipmentType"});

                            var equipments = new List<Equipment>();

                            foreach (var ev in d.Value)
                            {
                                headerRow.AddCell(ev.Key.EquipmentVariableDisplayName);

                                equipments.AddRange(ev.Value.Select(e_ev => e_ev.Equipment));
                            }

                            equipments = equipments.Distinct().OrderBy(_=>_.EquipmentName).ToList();

                            //

                            foreach (var e in equipments)
                            {
                                var row = ws.AddRow(new[]{e.EquipmentName, e.EquipmentType.EquipmentTypeName});

                                foreach (var ev in d.Value)
                                {
                                    Object val;

                                    var e_ev = ev.Value.FirstOrDefault(_=>_.EID == e.EID);

                                    if (e_ev == null)
                                    {
                                        val = String.Empty;
                                    }
                                    else
                                    {
                                        Double dbl;

                                        if (Double.TryParse(e_ev.Value, out dbl))
                                        {
                                            val = dbl;
                                        }
                                        else
                                        {
                                            val = e_ev.Value;
                                        }
                                    }

                                    row.AddCell(val);
                                }
                            }
                        }

                        //

                        MemoryStream ms;
                
                        ssd.Save(out ms);

                        return ms.ToArray();
                    }
                }
                //
                Byte[] IFileGenerator.Generate()
                {
                    throw new NotImplementedException();
                }

            #endregion

            #region property

                AllowanceTypeEnum IFileGenerator.AllowanceType
                {
                    get {return AllowanceTypeEnum.AdminDownload;}
                }

            #endregion

        #endregion
    }
}