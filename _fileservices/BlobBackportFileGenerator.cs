﻿using CW.Business;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._fileservices
{
    public class BlobBackportFileGenerator : IFileGenerator
    {
        private readonly DataManager mDataManager;
        private readonly string mBPID;
        private readonly DateTime mDateCreated;

        public BlobBackportFileGenerator(DataManager dataManager, DateTime dateCreated, string bpid)
        {
            mDataManager = dataManager;
            mBPID = bpid;
            mDateCreated = dateCreated;
        }

        public Byte[] Generate()
        {
            var blob = mDataManager.BlobBackportDataMapper.GetBlobBackportJobBlob(mDateCreated, mBPID);

            if (blob == null)
            {
                return new byte[] { };
            }
            return blob;
        }

        public Byte[] Generate(out String fileName)
        {
            var b = mDataManager.BlobBackportDataMapper.GetBlobBackportJobEntity(mDateCreated, mBPID);

            fileName = b.DateCreatedUTC.ToShortDateString() + ".csv";

            return Generate();
        }

        public AllowanceTypeEnum AllowanceType
        {
            get { return AllowanceTypeEnum.AdminDownload; }
        }
    }
}