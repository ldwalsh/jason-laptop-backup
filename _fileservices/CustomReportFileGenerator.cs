﻿using System;
using System.IO;
using CW.Common.Helpers;
using CW.Utility;
using CW.Website._framework;

namespace CW.Website._fileservices
{
    public sealed class CustomReportFileGenerator: IFileGenerator
    {
        private Byte[] byteArray;

        public static CustomReportFileGenerator Create(Byte[] byteArray)
        {
            return new CustomReportFileGenerator(byteArray);
        }

        private CustomReportFileGenerator(Byte[] byteArray)
        {
            this.byteArray = byteArray;
        }

        public AllowanceTypeEnum AllowanceType
        {
            get { return AllowanceTypeEnum.AdminDownload; }
        }

        public Byte[] Generate()
        {
            return byteArray;
        }

        public Byte[] Generate(out String fileName)
        {
            throw new NotImplementedException();
        }
    }
}