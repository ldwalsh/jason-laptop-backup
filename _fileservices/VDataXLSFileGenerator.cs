﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Business;
using CW.Data.AzureStorage;
using CW.Data;
using CW.Data.Models.VAndVPre;
using CW.Common.Constants;

namespace CW.Website._fileservices
{
    public class VDataXLSFileGenerator : VDataFileGeneratorBase, IFileGenerator
    {
        public VDataXLSFileGenerator(DataManager dataManager, IEnumerable<PointTypeDisplayNameAndVPID> vpoints, DateTime startDate, DateTime endDate, DataConstants.AnalysisRange analysisRange)
            : base(dataManager, vpoints, startDate, endDate, analysisRange)
        {
        }

        public Byte[] Generate()
        {
            var dt = BuildTable(dataManager, vpoints, startDate, endDate, analysisRange);

            var gv = new GridView{DataSource = dt};

            gv.DataBind();

            var stringWriter = new StringWriter();
            
            gv.RenderControl(new HtmlTextWriter(stringWriter));

            var sb = new StringBuilder(stringWriter.ToString());

            stringWriter.Close();

            return Encoding.UTF8.GetBytes(sb.ToString());
        }

        public Byte[] Generate(out String fileName)
        {
            throw new NotImplementedException();
        }

        public AllowanceTypeEnum AllowanceType
        {
            get {return AllowanceTypeEnum.RawDataDownload;}
        }
    }
}