﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="Administration.aspx.cs" Inherits="CW.Website.Administration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
       	                  	        	
    <h1>Administration</h1>                        
    <div class="richText">
        <asp:Literal ID="litAdminBody" runat="server"></asp:Literal>                    
    </div>                            
    
</asp:Content>                