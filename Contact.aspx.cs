﻿using System;
using CW.Business;
using CW.Data;
using CW.Common.Helpers;
using CW.Utility;
using CW.Website._framework;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class Contact : SitePage
    {
        #region Properties

            const string contactSuccessful = "Contact email was successfully sent.";
            const string contactFailed = "Contact email failed to send. Please contact an administrator directly.";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //hide labels
                lblErrors.Visible = false;
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Submit contact button on click.
            /// </summary>
            protected void submitButton_Click(object sender, EventArgs e)
            {
                //get user data
                User mUser = DataMgr.UserDataMapper.GetUser(siteUser.Email);

                //get support override if exists
                string supportOverrideEmail = DataMgr.ClientDataMapper.GetSupportOverrideEmailIfExists(siteUser.CID);

                //try to send email
                try
                {
                    Email.SendContactEmail(mUser, "$product " + ddlSubject.SelectedValue, txtDescription.Value, siteUser.IsSchneiderTheme, supportOverrideEmail);

                    lblErrors.Text = contactSuccessful;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                catch (Exception ex)
                {
                    lblErrors.Text = contactFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Contact email was not sent for user: UserEmail={0}.", mUser.Email), ex);
                }

            }

        #endregion
    }
}
