﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSBuildingVariableAdministration.aspx.cs" Inherits="CW.Website.KGSBuildingVariableAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                <asp:Literal ID="litScripts" runat="server" />
                  	                  	
            	<h1>KGS Building Variables Administration</h1>                        
                <div class="richText">The kgs building variables administration area is used to view, add, and edit building variables.</div>                                                 
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Building Variables"></telerik:RadTab>
                            <telerik:RadTab Text="Add Building Variable"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Building Variables</h2> 
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p>  
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>          
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridBuildingVariables"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="BVID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridBuildingVariables_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridBuildingVariables_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridBuildingVariables_Sorting"  
                                             OnSelectedIndexChanged="gridBuildingVariables_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridBuildingVariables_Editing"                                                                                                                                                                                   
                                             OnRowDeleting="gridBuildingVariables_Deleting"
                                             OnDataBound="gridBuildingVariables_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingVariableName" HeaderText="Building Variable Name">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("BuildingVariableName"),30) %></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingVariableDisplayName" HeaderText="Building Variable Display Name">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("BuildingVariableDisplayName"),30) %></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="IP Eng Units">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("IPEngUnits"),12) %></ItemTemplate>                          
                                                </asp:TemplateField>
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="SI Eng Units">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("SIEngUnits"),12) %></ItemTemplate>                          
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="User Editable">                                                      
                                                    <ItemTemplate><%# Eval("UserEditable") %></ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="IP Default Val">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("IPDefaultValue"),14) %></ItemTemplate>
                                                </asp:TemplateField>                                                    
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="SI Default Val">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("SIDefaultValue"),14) %></ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this building variable permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT BUILDING VARIABLE DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvBuildingVariable" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Building Variable Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Build. Variable Name: </strong>" + Eval("BuildingVariableName") + "</li>"%> 
                                                            <%# "<li><strong>Build. Var. Display Name: </strong>" + Eval("BuildingVariableDisplayName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("BuildingVariableDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("BuildingVariableDescription") + "</li>"%>                                                           
                                                            <%# "<li><strong>IP Engineering Units: </strong>" + Eval("IPEngUnits") + "</li>"%>
                                                            <%# "<li><strong>SI Engineering Units: </strong>" + Eval("SIEngUnits") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("UserEditable"))) ? "" : "<li><strong>User Editable: </strong>" + Eval("UserEditable") + "</li>"%>
                                                            <!--Default value can be an empty string-->
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("IPDefaultValue"))) ? "" : "<li><strong>IP Default Value: </strong><div class='divContentPreWrap'>" + Eval("IPDefaultValue") + "</div></li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("SIDefaultValue"))) ? "" : "<li><strong>SI Default Value: </strong><div class='divContentPreWrap'>" + Eval("SIDefaultValue") + "</div></li>"%>
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT EQUIPMENT PANEL -->                              
                                    <asp:Panel ID="pnlEditBuildingVariable" runat="server" Visible="false" DefaultButton="btnUpdateBuildingVariable">                                                                                             
                                        <div>
                                            <h2>Edit Building Variable</h2>
                                        </div>  
                                        <div>    
                                            <a id="lnkSetFocusEdit" runat="server"></a>                                                 
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Building Variable Name:</label>
                                                <asp:TextBox ID="txtEditBuildingVariableName" MaxLength="50" runat="server"></asp:TextBox>
                                                <p>(Warning: Editing a building variable name may cause multiple analyses to stop working.)</p>
                                                <asp:HiddenField ID="hdnBuildingVariableName" runat="server" Visible="false" />
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Building Variable Display Name:</label>
                                                <asp:TextBox ID="txtEditBuildingVariableDisplayName" MaxLength="50" runat="server"></asp:TextBox>                                    
                                                <asp:HiddenField ID="hdnBuildingVariableDisplayName" runat="server" Visible="false" />
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 500, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>  
                                            <div class="divForm">   
                                                <label class="label">*IP Engineering Units:</label>    
                                                <asp:DropDownList ID="ddlEditIPEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">*SI Engineering Units:</label>    
                                                <asp:DropDownList ID="ddlEditSIEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*User Editable:</label> 
                                                <asp:CheckBox CssClass="checkbox" ID="chkEditUserEditable" runat="server" />
                                            </div>
                                            <div class="divForm">
                                                <label class="label">IP Default Value:</label>
                                                <asp:TextBox ID="txtEditIPDefaultValue" MaxLength="25" runat="server" TextMode="SingleLine" Rows="1" Columns="1" CssClass="textbox"></asp:TextBox>&nbsp;<span id="spEditIPInfo" runat="server"></span>
                                                <div id='valFailureEditIPDefaultValue' runat="server" class="errorMessage" style="display:none"></div>
                                            </div>
                                            <div class="divForm">
                                                <label class="label">SI Default Value:</label>
                                                <asp:TextBox ID="txtEditSIDefaultValue" MaxLength="25" runat="server" TextMode="SingleLine" Rows="1" Columns="1" CssClass="textbox"></asp:TextBox>&nbsp;<span id="spEditSIInfo" runat="server"></span>
                                                <div id='valFailureEditSIDefaultValue' runat="server" class="errorMessage" style="display:none"></div>
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBuildingVariable" runat="server" Text="Update Variable"  OnClick="updateBuildingVariableButton_Click" ValidationGroup="EditBuildingVariable"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editBuildingVariableNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Variable Name is a required field."
                                        ControlToValidate="txtEditBuildingVariableName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditBuildingVariable">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingVariableNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editBuildingVariableNameRequiredValidatorExtender"
                                        TargetControlID="editBuildingVariableNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editBuildingVariableDisplayNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Variable Display Name is a required field."
                                        ControlToValidate="txtEditBuildingVariableDisplayName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditBuildingVariable">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingVariableDisplayNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editBuildingVariableDisplayNameRequiredValidatorExtender"
                                        TargetControlID="editBuildingVariableDisplayNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="editIPEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="IP Engineering Units is a required field." 
                                        ControlToValidate="ddlEditIPEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditBuildingVariable">
                                        </asp:RequiredFieldValidator>
                                      <ajaxToolkit:ValidatorCalloutExtender ID="editIPEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="editIPEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="editIPEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="editSIEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="SI Engineering Units is a required field." 
                                        ControlToValidate="ddlEditSIEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditBuildingVariable">
                                        </asp:RequiredFieldValidator>
                                      <ajaxToolkit:ValidatorCalloutExtender ID="editSIEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="editSIEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="editSIEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                    </asp:Panel>
                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">  
                                <asp:Panel ID="pnlAddBuildingVariable" runat="server" DefaultButton="btnAddBuildingVariable"> 
                                    <h2>Add Building Variable</h2>
                                    <div>          
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                          
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Building Variable Name:</label>
                                            <asp:TextBox ID="txtAddBuildingVariableName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">*Building Variable Display Name:</label>
                                            <asp:TextBox ID="txtAddBuildingVariableDisplayName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div>
                                        <div class="divForm">
                                             <label class="label">Description:</label>
                                             <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 500, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                             <div id="divAddDescriptionCharInfo"></div>
                                        </div>  
                                        <div class="divForm">   
                                            <label class="label">*IP Engineering Units:</label>    
                                            <asp:DropDownList ID="ddlAddIPEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div>
                                        <div class="divForm">   
                                            <label class="label">*SI Engineering Units:</label>    
                                            <asp:DropDownList ID="ddlAddSIEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div>   
                                        <div class="divForm">
                                            <label class="label">IP Default Value:</label>
                                            <asp:TextBox ID="txtAddIPDefaultValue" MaxLength="25" runat="server" TextMode="SingleLine" Rows="1" Columns="1" CssClass="textbox"></asp:TextBox>&nbsp;<span id="spAddIPInfo" runat="server"></span>
                                            <div id='valFailureAddIPDefaultValue' runat="server" class="errorMessage" style="display:none"></div>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">SI Default Value:</label>
                                            <asp:TextBox ID="txtAddSIDefaultValue" MaxLength="25" runat="server" TextMode="SingleLine" Rows="1" Columns="1" CssClass="textbox"></asp:TextBox>&nbsp;<span id="spAddSIInfo" runat="server"></span>
                                            <div id='valFailureAddSIDefaultValue' runat="server" class="errorMessage" style="display:none"></div>
                                        </div>  
                                        <div class="divForm">
                                            <label class="label">*User Editable:</label> 
                                            <asp:CheckBox CssClass="checkbox" ID="chkAddUserEditable" runat="server" />
                                        </div>                                                                                                 
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddBuildingVariable" runat="server" Text="Add Variable"  OnClick="addBuildingVariableButton_Click" ValidationGroup="AddBuildingVariable"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addBuildingVariableNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Variable Name is a required field."
                                        ControlToValidate="txtAddBuildingVariableName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddBuildingVariable">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addBuildingVariableNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addBuildingVariableNameRequiredValidatorExtender"
                                        TargetControlID="addBuildingVariableNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addBuildingVariableDisplayNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Variable Display Name is a required field."
                                        ControlToValidate="txtAddBuildingVariableDisplayName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddBuildingVariable">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addBuildingVariableDisplayNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addBuildingVariableDisplayNameRequiredValidatorExtender"
                                        TargetControlID="addBuildingVariableDisplayNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addIPEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="IP Engineering Units is a required field." 
                                        ControlToValidate="ddlAddIPEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddBuildingVariable">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addIPEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="addIPEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="addIPEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="addSIEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="SI Engineering Units is a required field." 
                                        ControlToValidate="ddlAddSIEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddBuildingVariable">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addSIEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="addSIEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="addSIEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                </asp:Panel>                                                                                                                                                                
                            </telerik:RadPageView>                        
                        </telerik:RadMultiPage>
                   </div>                                                                 
               
</asp:Content>


                    
                  
