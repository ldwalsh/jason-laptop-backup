﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Error404.master" EnableViewState="true" AutoEventWireup="true" CodeBehind="Error404.aspx.cs" Inherits="CW.Website.Error404" %>

<asp:Content ID="plcCopyContentPage" ContentPlaceHolderID="plcCopyPage" runat="server">
  <div style="text-align: center;">
    <h1 runat="server" id="headerErrorPage"></h1>
    <br />
    <p><asp:Literal ID="litOperationIdMessage" runat="server" /></p>
  </div>
</asp:Content>

<asp:Content ID="plcCopyContentIframe" ContentPlaceHolderID="plcCopyIframe" runat="server">
  <div style="text-align: center;">
    <div runat="server" id="headerErrorIframe"></div>
  </div>
</asp:Content>