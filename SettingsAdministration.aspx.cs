﻿using System;
using CW.Business;
using CW.Data;
using CW.Common.Helpers;
using CW.Utility;
using CW.Website._framework;
using CW.Common.Constants;
using CW.Common.Instrumentation;

namespace CW.Website
{
    public partial class SettingsAdministration: SitePage
    {
        #region Properties
            
            const string noClientSpecified = "No client is specfied for a KGS Admin or higher.";                        
            const string updateSettingsSuccessful = "Client settings update was successful.";
            const string updateSettingsFailed = "Client settings update failed. Please contact an administrator.";

        #endregion

        #region Page Events

            private void Page_PreInit()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher, or super admin non provider, or super admin provider proxy.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && !siteUser.IsLoggedInUnderProviderClient) || (siteUser.IsSuperAdmin && siteUser.IsProxyClient && siteUser.IsLoggedInUnderProviderClient)))
                    Response.Redirect("/Home.aspx");
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                    //get client by user
                    var client = DataMgr.ClientDataMapper.GetClient(siteUser.CID);

                    //gets client settings
                    ClientSetting mClientSettings = DataMgr.ClientDataMapper.GetClientSettingsByClientID(client.CID);

                    //set client setting into edit form
                    SetClientSettingsIntoEditForm(mClientSettings);

                    //show settings div
                    settings.Visible = true;

                    //hide error message
                    lblSettingsError.Visible = false;

                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litAdminSettingsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.AdminSettingsBody);                              
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetClientSettingsIntoEditForm(ClientSetting clientSettings)
            {
                //NewsBody
                //Make sure to decode from ascii to html 
                editorNewsBody.Content = clientSettings.NewsBody;
            }

        #endregion

        #region Load Client, ClientSettings
        
            /// <summary>
            /// Loads the editor form data for client settings for update.
            /// 
            /// Also removes extra characters over max html limits, and then encodes the html.
            /// </summary>
            /// <param name="clientSettings"></param>
            protected void LoadEditFormIntoClientSettings(ClientSetting clientSettings)
            {
                //get client by user
                var client = DataMgr.ClientDataMapper.GetClient(siteUser.CID);

                //ID
                clientSettings.CID = client.CID;

                //NewsBody
                    //if editor's html is greater then defined max html length, 
                    //then remove extra characters.
                    //Make sure to html encode before putting in the database. From html to ascii.
                clientSettings.NewsBody = editorNewsBody.Content;
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Update client settings Button on click.
            /// </summary>
            protected void updateSettingsButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the client settings
                ClientSetting mClientSettings = new ClientSetting();
                LoadEditFormIntoClientSettings(mClientSettings);

                try
                {
                    DataMgr.ClientDataMapper.UpdateClientNewsSettings(mClientSettings);

                    LabelHelper.SetLabelMessage(lblSettingsError, updateSettingsSuccessful, lnkSetFocus);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblSettingsError, updateSettingsFailed, lnkSetFocus);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Unable to update client settings.", ex);
                }
            }

        #endregion
    }
}

