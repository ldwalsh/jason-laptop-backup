﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.DataSourceVendorProduct;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSDataSourceVendorProductAdministration: SitePage
    {
        #region Properties

            private DataSourceVendorProduct mVendorProduct;
            const string addVendorProductSuccess = " vendor product addition was successful.";
            const string addVendorProductFailed = "Adding vendor product failed. Please contact an administrator.";
            const string addVendorProductNameExists = "Cannot add vendor product because the name already exists."; 
            const string updateSuccessful = "Vendor product update was successful.";
            const string updateFailed = "Vendor product update failed. Please contact an administrator.";
            const string updateVendorProductNameExists = "Cannot update vendor product name because the name already exists.";     
            const string deleteSuccessful = "Vendor product deletion was successful.";
            const string deleteFailed = "Vendor product deletion failed. Please contact an administrator.";            
            const string associatedToDataSource = "Cannot delete vendor product becuase a data source requires it.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "VendorProductName";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindVendorProducts();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindVendors(ddlAddVendor, ddlEditVendor);

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetVendorProductIntoEditForm(DataSourceVendorProduct vendorProduct)
            {
                //ID
                hdnEditID.Value = Convert.ToString(vendorProduct.VendorProductID);
                //Vendor Product Name
                txtEditVendorProductName.Text = vendorProduct.VendorProductName;
                //Equipment Variable Description
                txtEditDescription.Value = String.IsNullOrEmpty(vendorProduct.VendorProductDescription) ? null : vendorProduct.VendorProductDescription;
                
                //Vendor
                ddlEditVendor.SelectedValue = Convert.ToString(vendorProduct.VendorID);                
            }

            #endregion

        #region Load and Bind Fields

            private void BindVendors(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<DataSourceVendor> vendors = DataMgr.DataSourceVendorDataMapper.GetAllVendors();

                ddl.DataTextField = "VendorName";
                ddl.DataValueField = "VendorID";
                ddl.DataSource = vendors;
                ddl.DataBind();

                ddl2.DataTextField = "VendorName";
                ddl2.DataValueField = "VendorID";
                ddl2.DataSource = vendors;
                ddl2.DataBind();
            }

        #endregion

        #region Load Vendor Product

            protected void LoadAddFormIntoVendorProduct(DataSourceVendorProduct vendorProduct)
            {
                //Vendor Product Name
                vendorProduct.VendorProductName = txtAddVendorProductName.Text;
                //Equipment Variable Description
                vendorProduct.VendorProductDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                //Vendor
                vendorProduct.VendorID = Convert.ToInt32(ddlAddVendor.SelectedValue);

                vendorProduct.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoVendorProduct(DataSourceVendorProduct vendorProduct)
            {
                //ID
                vendorProduct.VendorProductID = Convert.ToInt32(hdnEditID.Value);
                //Vendor Product Name
                vendorProduct.VendorProductName = txtEditVendorProductName.Text;
                //Equipment Variable Description
                vendorProduct.VendorProductDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //Vendor
                vendorProduct.VendorID = Convert.ToInt32(ddlEditVendor.SelectedValue);                
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add vendor product Button on click.
            /// </summary>
            protected void addVendorProductButton_Click(object sender, EventArgs e)
            {
                mVendorProduct = new DataSourceVendorProduct();

                //load the form into the vendor product
                LoadAddFormIntoVendorProduct(mVendorProduct);

                try
                {
                    //insert new vendor product
                    DataMgr.DataSourceVendorProductDataMapper.InsertVendorProduct(mVendorProduct);

                    LabelHelper.SetLabelMessage(lblAddError, mVendorProduct.VendorProductName + addVendorProductSuccess, lnkSetFocusAdd);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding data source vendor product.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addVendorProductFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding data source vendor product.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addVendorProductFailed, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update vendor product Button on click. Updates vendor product data.
            /// </summary>
            protected void updateVendorProductButton_Click(object sender, EventArgs e)
            {
                DataSourceVendorProduct mVendorProduct = new DataSourceVendorProduct();

                //load the form into the vendor product
                LoadEditFormIntoVendorProduct(mVendorProduct);

                //try to update the vendor products              
                try
                {
                    DataMgr.DataSourceVendorProductDataMapper.UpdateVendorProduct(mVendorProduct);

                    LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                    //Bind vendor products again
                    BindVendorProducts();
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating data source vendor product.", ex);
                }
            }

            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindVendorProducts();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindVendorProducts();
            }

        #endregion

        #region Dropdown events

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind vendor products grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind vendor products to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindVendorProducts();
                }
            }

        #endregion

        #region Grid Events

            protected void gridDataSourceVendorProducts_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridDataSourceVendorProducts_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditVendorProduct.Visible = false;
                dtvVendorProduct.Visible = true;
                
                int vendorProductID = Convert.ToInt32(gridDataSourceVendorProducts.DataKeys[gridDataSourceVendorProducts.SelectedIndex].Values["VendorProductID"]);

                //set data source
                dtvVendorProduct.DataSource = DataMgr.DataSourceVendorProductDataMapper.GetFullDataSourceVendorProductByID(vendorProductID);
                //bind vendor product to details view
                dtvVendorProduct.DataBind();
            }

            protected void gridDataSourceVendorProducts_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridDataSourceVendorProducts_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvVendorProduct.Visible = false;
                pnlEditVendorProduct.Visible = true;

                int vendorProductID = Convert.ToInt32(gridDataSourceVendorProducts.DataKeys[e.NewEditIndex].Values["VendorProductID"]);

                DataSourceVendorProduct mVendorProduct = DataMgr.DataSourceVendorProductDataMapper.GetVendorProduct(vendorProductID);

                //Set Equipment Variable data
                SetVendorProductIntoEditForm(mVendorProduct);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridDataSourceVendorProducts_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows equipmentVariableid
                int vendorProductID = Convert.ToInt32(gridDataSourceVendorProducts.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a vendor product if it as not been assocaited with
                    //a data source

                    //check if vendor product is associated to any data source
                    if (DataMgr.DataSourceVendorProductDataMapper.AreDataSourcesAssignedToDataSourceVendorProduct(vendorProductID))
                    {
                        lblErrors.Text = associatedToDataSource;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }

                    else
                    {
                        //delete equipment variable
                        DataMgr.DataSourceVendorProductDataMapper.DeleteDataSourceVendorProduct(vendorProductID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting data source vendor product.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryVendorProducts());
                gridDataSourceVendorProducts.PageIndex = gridDataSourceVendorProducts.PageIndex;
                gridDataSourceVendorProducts.DataSource = SortDataTable(dataTable as DataTable, true);
                gridDataSourceVendorProducts.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditVendorProduct.Visible = false;  
            }

            protected void gridDataSourceVendorProducts_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedVendorProducts(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridDataSourceVendorProducts.DataSource = SortDataTable(dataTable, true);
                gridDataSourceVendorProducts.PageIndex = e.NewPageIndex;
                gridDataSourceVendorProducts.DataBind();
            }

            protected void gridDataSourceVendorProducts_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridDataSourceVendorProducts.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedVendorProducts(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridDataSourceVendorProducts.DataSource = SortDataTable(dataTable, false);
                gridDataSourceVendorProducts.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetDataSourceVendorProductData> QueryVendorProducts()
            {
                try
                {
                    //get all equipment variables 
                    return DataMgr.DataSourceVendorProductDataMapper.GetAllDataSourceVendorProductsWithPartialData();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor products.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor products.", ex);
                    return null;
                }
            }

            private IEnumerable<GetDataSourceVendorProductData> QuerySearchedVendorProducts(string searchText)
            {
                try
                {
                    //get all equipment variables 
                    return DataMgr.DataSourceVendorProductDataMapper.GetAllSearchedDataSourceVendorProductsWithPartialData(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor products.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor products.", ex);
                    return null;
                }
            }

            private void BindVendorProducts()
            {
                //query equipment variables
                IEnumerable<GetDataSourceVendorProductData> variables = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryVendorProducts() : QuerySearchedVendorProducts(txtSearch.Text);

                int count = variables.Count();

                gridDataSourceVendorProducts.DataSource = variables;

                // bind grid
                gridDataSourceVendorProducts.DataBind();

                SetGridCountLabel(count);

                //set search visibility. dont remove when last result from searched results in the grid is deleted.
                pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && count == 0) ? false : true;
            }

            //private void BindSearchedVendorProducts(string[] searchText)
            //{
            //    //query equipment variables
            //    IEnumerable<GetDataSourceVendorProductData> variables = QuerySearchedVendorProducts(searchText);

            //    int count = variables.Count();

            //    gridDataSourceVendorProducts.DataSource = variables;

            //    // bind grid
            //    gridDataSourceVendorProducts.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} data source vendor product found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} data source vendor products found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No data source vendor products found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

        #endregion
    }
}

