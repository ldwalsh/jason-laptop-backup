﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSEngUnitsAdministration.aspx.cs" Inherits="CW.Website.KGSEngUnitsAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                 	                  	
            	<h1>KGS Eng Units Administration</h1>                        
                <div class="richText">The kgs eng units administration area is used to view, add, and edit eng units.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
                </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Eng Units"></telerik:RadTab>
                            <telerik:RadTab Text="Add Engineering Unit"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Eng Units</h2>
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>       
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>       
                                                                                                                
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridEngUnits"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="EngUnitID"  
                                             GridLines="None"                                      
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridEngUnits_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridEngUnits_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridEngUnits_Sorting"   
                                             OnSelectedIndexChanged="gridEngUnits_OnSelectedIndexChanged"                                                                                                             
                                             OnRowDeleting="gridEngUnits_Deleting"
                                             OnRowEditing="gridEngUnits_Editing"
                                             OnDataBound="gridEngUnits_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EngUnits" HeaderText="Eng Unit Name">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("EngUnits"),30) %></ItemTemplate> 
                                                </asp:TemplateField> 
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Description">                                                      
                                                     <ItemTemplate><label title="<%# Eval("EngUnitsDescription") %>"><%# StringHelper.TrimText(Eval("EngUnitsDescription"),150) %></label></ItemTemplate>    
                                                </asp:TemplateField>  
                                                 <asp:TemplateField ItemStyle-Wrap="true" SortExpression="MinValue" HeaderText="Min Value">                                                      
                                                    <ItemTemplate><%# Eval("MinValue") %></ItemTemplate> 
                                                </asp:TemplateField>
                                                 <asp:TemplateField ItemStyle-Wrap="true" SortExpression="MaxValue" HeaderText="Max Value">                                                      
                                                    <ItemTemplate><%# Eval("MaxValue") %></ItemTemplate> 
                                                </asp:TemplateField>                                                
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this eng unit permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                                                          
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT ENG UNITS DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvEngUnit" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>                                                    
                                                    <div>
                                                        <h2>Eng Unit Details</h2>                                                                                                                       
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Eng Unit: </strong>" + Eval("EngUnits") + "</li>"%> 
                                                            <%# "<li><strong>Description: </strong>" + Eval("EngUnitsDescription") + "</li>"%>
                                                            <%# "<li><strong>Min Value: </strong>" + Eval("MinValue") + "</li>"%>
                                                            <%# "<li><strong>Max Value: </strong>" + Eval("MaxValue") + "</li>"%>                                                                                                                                                  	                                                                	                                                                
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>                                     
                                                                     
                                    <!--EDIT ENG UNITS PANEL -->                              
                                    <asp:Panel ID="pnlEditEngUnit" runat="server" Visible="false" DefaultButton="btnUpdateEngUnit">                                                                                             
                                              <div>
                                                    <h2>Edit Eng Unit</h2>
                                              </div>  
                                              <div>                                                    
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*Eng Unit:</label>
                                                        <asp:TextBox ID="txtEditEngUnit" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                                    </div>                                                
                                                    <div class="divForm">
                                                         <label class="label">Description:</label>
                                                         <textarea name="txtEditDescription" id="txtEditDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" runat="server"></textarea>
                                                         <div id="divEditDescriptionCharInfo"></div>                                                           
                                                    </div> 
                                                    <div class="divForm">
                                                         <label class="label">*Min Value (Range Checks):</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditMinValue" runat="server" MaxLength="10"></asp:TextBox>
                                                    </div>                                                     
                                                    <div class="divForm">
                                                         <label class="label">*Max Value (Range Checks):</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditMaxValue" runat="server" MaxLength="10"></asp:TextBox>
                                                    </div>                                                                                                                                             
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEngUnit" runat="server" Text="Update Eng Unit"  OnClick="updateEngUnitButton_Click" ValidationGroup="EditEngUnit"></asp:LinkButton>    
                                                </div>
                                                
                                                <!--Ajax Validators-->      
                                                <asp:RequiredFieldValidator ID="editEngUnitsRequiredValidator" runat="server"
                                                    ErrorMessage="Eng Unit is a required field."
                                                    ControlToValidate="txtEditEngUnit"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditEngUnit">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editEngUnitsRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editEngUnitsRequiredValidatorExtender"
                                                    TargetControlID="editEngUnitsRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                           
                                                <asp:RequiredFieldValidator ID="editDescriptionequiredValidator" runat="server"
                                                    ErrorMessage="Description is a required field."
                                                    ControlToValidate="txtEditDescription"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditEngUnit">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editDescriptionequiredValidatorExtender" runat="server"
                                                    BehaviorID="editDescriptionequiredValidatorExtender"
                                                    TargetControlID="editDescriptionequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                               <ajaxToolkit:FilteredTextBoxExtender ID="editMinValueFilteredTextBoxExtender" runat="server"
                                                        TargetControlID="txtEditMinValue"         
                                                        FilterType="Custom, Numbers"
                                                        ValidChars="-." />
                                               <asp:RequiredFieldValidator ID="editMinValueRequiredValidator" runat="server"
                                                    ErrorMessage="Min Value is a required field."
                                                    ControlToValidate="txtEditMinValue"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditEngUnit">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editMinValueRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editMinValueRequiredValidatorExtender"
                                                    TargetControlID="editMinValueRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>   
                                                <asp:RegularExpressionValidator ID="editMinValueRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Numeric Format."
                                                    ValidationExpression="^(\d|-)*\.?\d*$"
                                                    ControlToValidate="txtEditMinValue"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="EditEngUnit">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editMinValueRegExValidatorExtender" runat="server"
                                                    BehaviorID="editMinValueRegExValidatorExtender" 
                                                    TargetControlID="editMinValueRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                
                                                <ajaxToolkit:FilteredTextBoxExtender ID="editMaxValueFilteredTextBoxExtender" runat="server"
                                                    TargetControlID="txtEditMaxValue"         
                                                    FilterType="Custom, Numbers"
                                                    ValidChars="-." />
                                                <asp:RequiredFieldValidator ID="editMaxValueRequiredValidator" runat="server"
                                                    ErrorMessage="Max Value is a required field."
                                                    ControlToValidate="txtEditMaxValue"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditEngUnit">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editMaxValueRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editMaxValueRequiredValidatorExtender"
                                                    TargetControlID="editMaxValueRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>    
                                                <asp:RegularExpressionValidator ID="editMaxValueRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Numeric Format."
                                                    ValidationExpression="^(\d|-)*\.?\d*$"
                                                    ControlToValidate="txtEditMaxValue"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="EditEngUnit">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editMaxValueRegExValidatorExtender" runat="server"
                                                    BehaviorID="editMaxValueRegExValidatorExtender" 
                                                    TargetControlID="editMaxValueRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>         
                                                <asp:CompareValidator ID="editValueCompareValidator" runat="server"
                                                     ErrorMessage="Invalid Range."
                                                     ControlToValidate="txtEditMaxValue"
                                                     ControlToCompare="txtEditMinValue"
                                                     Operator="GreaterThan"
                                                     Type="Double" 
                                                     SetFocusOnError="true"
                                                     Display="None"
                                                     ValidationGroup="EditEngUnit">
                                                    </asp:CompareValidator>
                                                 <ajaxToolkit:ValidatorCalloutExtender ID="editValueCompareValidatorExtender" runat="server"
                                                    BehaviorID="editValueCompareValidatorExtender" 
                                                    TargetControlID="editValueCompareValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                    </asp:Panel>
                                                                                                         
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                    <asp:Panel ID="pnlAddEngUnit" runat="server" DefaultButton="btnAddEngUnit"> 
                                              <h2>Add Engineering Unit</h2> 
                                              <div>  
                                                    <a id="lnkSetFocusAdd" runat="server"></a>                                                  
                                                    <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <div class="divForm">
                                                        <label class="label">*Eng Unit:</label>
                                                        <asp:TextBox ID="txtAddEngUnit" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                                    </div> 
                                                    <div class="divForm">
                                                         <label class="label">Description:</label>
                                                         <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 500, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                         <div id="divAddDescriptionCharInfo"></div>
                                                    </div>    
                                                  <div class="divForm">
                                                         <label class="label">*Min Value (Range Checks):</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtAddMinValue" runat="server" MaxLength="10"></asp:TextBox>
                                                    </div>                                                     
                                                    <div class="divForm">
                                                         <label class="label">*Max Value (Range Checks):</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtAddMaxValue" runat="server" MaxLength="10"></asp:TextBox>
                                                    </div>                                                                                            
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnAddEngUnit" runat="server" Text="Add Eng Unit"  OnClick="addEngUnitButton_Click" ValidationGroup="AddEngUnit"></asp:LinkButton>    
                                                </div>
                                                
                                                <!--Ajax Validators-->      
                                                <asp:RequiredFieldValidator ID="addEngUnitRequiredValidator" runat="server"
                                                    ErrorMessage="Eng Unit is a required field."
                                                    ControlToValidate="txtAddEngUnit"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddEngUnit">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addEngUnitRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addEngUnitRequiredValidatorExtender"
                                                    TargetControlID="addEngUnitRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>  
                                                <asp:RequiredFieldValidator ID="addDescriptionRequiredValidator" runat="server"
                                                    ErrorMessage="Description is a required field."
                                                    ControlToValidate="txtAddDescription"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddEngUnit">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addDescriptionRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addDescriptionRequiredValidatorExtender"
                                                    TargetControlID="addDescriptionRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                        
                                                <ajaxToolkit:FilteredTextBoxExtender ID="addMinValueFilteredTextBoxExtender" runat="server"
                                                    TargetControlID="txtAddMinValue"         
                                                    FilterType="Custom, Numbers"
                                                    ValidChars="-." />
                                                <asp:RequiredFieldValidator ID="addMinValueRequiredValidator" runat="server"
                                                    ErrorMessage="Min Value is a required field."
                                                    ControlToValidate="txtAddMinValue"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddEngUnit">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addMinValueRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addMinValueRequiredValidatorExtender"
                                                    TargetControlID="addMinValueRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>   
                                                <asp:RegularExpressionValidator ID="addMinValueRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Numeric Format."
                                                    ValidationExpression="^(\d|-)*\.?\d*$"
                                                    ControlToValidate="txtAddMinValue"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="AddEngUnit">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addMinValueRegExValidatorExtender" runat="server"
                                                    BehaviorID="addMinValueRegExValidatorExtender" 
                                                    TargetControlID="addMinValueRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                
                                                <ajaxToolkit:FilteredTextBoxExtender ID="addMaxValueFilteredTextBoxExtender" runat="server"
                                                    TargetControlID="txtAddMaxValue"         
                                                    FilterType="Custom, Numbers"
                                                    ValidChars="-." />
                                                <asp:RequiredFieldValidator ID="addMaxValueRequiredValidator" runat="server"
                                                    ErrorMessage="Max Value is a required field."
                                                    ControlToValidate="txtAddMaxValue"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddEngUnit">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addMaxValueRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addMaxValueRequiredValidatorExtender"
                                                    TargetControlID="addMaxValueRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>    
                                                <asp:RegularExpressionValidator ID="addMaxValueRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Numeric Format."
                                                    ValidationExpression="^(\d|-)*\.?\d*$"
                                                    ControlToValidate="txtAddMaxValue"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="AddEngUnit">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addMaxValueRegExValidatorExtender" runat="server"
                                                    BehaviorID="addMaxValueRegExValidatorExtender" 
                                                    TargetControlID="addMaxValueRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>         
                                                <asp:CompareValidator ID="addValueCompareValidator" runat="server"
                                                     ErrorMessage="Invalid Range."
                                                     ControlToValidate="txtAddMaxValue"
                                                     ControlToCompare="txtAddMinValue"
                                                     Operator="GreaterThan"
                                                     Type="Double"
                                                     SetFocusOnError="true"
                                                     Display="None"
                                                     ValidationGroup="AddEngUnit">
                                                    </asp:CompareValidator>
                                                 <ajaxToolkit:ValidatorCalloutExtender ID="addValueCompareValidatorExtender" runat="server"
                                                    BehaviorID="addValueCompareValidatorExtender" 
                                                    TargetControlID="addValueCompareValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                    </asp:Panel>
                            </telerik:RadPageView>                          
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>


                    
                  
