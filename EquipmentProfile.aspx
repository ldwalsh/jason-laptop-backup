﻿<%@ Page Language="C#" MasterPageFile="~/_masters/EquipmentProfile.master" EnableEventValidation="false" EnableViewState="true" AutoEventWireup="false" CodeBehind="EquipmentProfile.aspx.cs" Inherits="CW.Website.EquipmentProfile" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="cwcontrols" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register Assembly="CW.Website" Namespace="CW.Website._extensions" TagPrefix="extensions" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                 	                  	        	
    <div class="equipmentProfilesModuleIcon">
        <h1 id="header" runat="server">Equipment Profile</h1>                          
    </div>
    <div class="richText">           
    </div>  
    <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
            <ProgressTemplate>
                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
            </ProgressTemplate>
        </asp:UpdateProgress>  
    </div> 

    <asp:Panel ID="pnlInitialSelection" class="divInitialSelection" runat="server" Visible="false">
            <!--Building Selection-->                               
            <div class="divForm">   
                <label class="label">*Select Building:</label> 
                <extensions:DropDownExtension ID="ddlBuildingsInitial" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildingsInitial_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                    <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                </extensions:DropDownExtension> 
            </div>                            
                            
            <!--Equipment Selection-->                        
            <div class="divForm">
                <label class="label">*Select Equipment Class:</label>
                <asp:DropDownList ID="ddlEquipmentClassesInitial" CssClass="dropdown" AppendDataBoundItems="true"
                    OnSelectedIndexChanged="ddlEquipmentClassesInitial_OnSelectedIndexChanged" AutoPostBack="true"
                    runat="server">
                    <asp:ListItem Value="-1">Select building first...</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="equipmentClassInitialRequiredValidator" runat="server" ErrorMessage="Equipment Class is a required field."
                    ControlToValidate="ddlEquipmentClassesInitial" SetFocusOnError="true" Display="None" InitialValue="-1"
                    ValidationGroup="Profile">
                </asp:RequiredFieldValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassInitialRequiredValidatorExtender" runat="server"
                    BehaviorID="equipmentClassInitialRequiredValidatorExtender" TargetControlID="equipmentClassInitialRequiredValidator"
                    HighlightCssClass="validatorCalloutHighlight" Width="175">
                </ajaxToolkit:ValidatorCalloutExtender>
            </div>

            <div class="divForm">   
                <label class="label">*Equipment:</label>
                <extensions:DropDownExtension ID="ddlEquipmentInitial" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentInitial_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                    <asp:ListItem Value="-1" >Select equipment class first...</asp:ListItem>                           
                </extensions:DropDownExtension>
            </div>                                                                                                                                                     
        
    </asp:Panel>

    <asp:Panel id="pnlProfile" runat="server">

        <!--Profile Left Column--> 
        <div class="profileLeftColumn">

            <!--Profile Top-->     
            <div>                             
                <div class="divProfileImage">  
                    <img id="imgProfile" runat="server" class="profileImage" />
                </div> 
                <div class="profileTeaser">
                    <!--Equipment teaser -->                            
                    <asp:Literal ID="litTeaser" runat="server"></asp:Literal>
                </div>
                <!--<div class="divProfileInteractive">
                    <div><a href="#">Check in</a></div>
                    <br />
                    <div><a href="#">Like</a></div>
                </div>-->
            </div>
   
            <div class="block">
                <div class="blockHeader">
                    <span class="blockTitle">Details</span>
                    <asp:HyperLink ID="lnkDetails" runat="server" CssClass="toggle"><asp:Label ID="lblDetails" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                </div>
                <div class="blockBody">
                <ajaxToolkit:CollapsiblePanelExtender ID="detailsCollapsiblePanelExtender" runat="Server"
                            TargetControlID="pnlDetails"
                            CollapsedSize="0"
                            Collapsed="false" 
                            ExpandControlID="lnkDetails"
                            CollapseControlID="lnkDetails"
                            AutoCollapse="false"
                            AutoExpand="false"
                            ScrollContents="false"
                            ExpandDirection="Vertical"
                            TextLabelID="lblDetails"
                            CollapsedText="show details"
                            ExpandedText="hide details" 
                            />
                    <asp:Panel ID="pnlDetails" CssClass="blockCollapsiblePanel" runat="server">
                    <!--EQUIPMENT DETAILS VIEW -->
                            <asp:DetailsView ID="dtvEquipment" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                <Fields>
                                    <asp:TemplateField  
                                    ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                    ItemStyle-BorderWidth="0" 
                                    ItemStyle-BorderColor="White" 
                                    ItemStyle-BorderStyle="none"
                                    ItemStyle-Width="100%"
                                    HeaderStyle-Width="1" 
                                    HeaderStyle-BorderWidth="0" 
                                    HeaderStyle-BorderColor="White" 
                                    HeaderStyle-BorderStyle="none"
                                    >
                                        <ItemTemplate>
                                            <div>                                                                                                                                        
                                                <ul class="detailsListNarrow">                                                                
                                                    <%# "<li><strong>Equipment Name: </strong>" + Eval("EquipmentName") + "</li>"%>                                                             
                                                    <%# "<li><strong>Building Name: </strong>" + Eval("BuildingName") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerName"))) ? "" : "<li><strong>Manufacturer Name: </strong>" + Eval("ManufacturerName") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerModelName"))) ? "" : "<li><strong>Model Name: </strong>" + Eval("ManufacturerModelName") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("SerialNumber"))) ? "" : "<li><strong>Serial Number: </strong>" + Eval("SerialNumber") + "</li>"%>                       
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentLocation"))) ? "" : "<li><strong>Equipment Location: </strong><div class='divContentPreWrap'>" + Eval("EquipmentLocation") + "</div></li>"%>                                                                                                   
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Description"))) ? "" : "<li><strong>Equipment Description: </strong><div class='divContentPreWrap'>" + Eval("Description") + "</div></li>"%> 
                                                    <%# "<li><strong>Equipment Class Name: </strong>" + Eval("EquipmentClassName") + "</li>"%> 	                                                                            
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentClassDescription"))) ? "" : "<li><strong>Class Description: </strong>" + Eval("EquipmentClassDescription") + "</li>"%> 
                                                    <%# "<li><strong>Equipment Type Name: </strong>" + Eval("EquipmentTypeName") + "</li>"%> 	                                                                                                                                        
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentTypeDescription"))) ? "" : "<li><strong>Type Description: </strong>" + Eval("EquipmentTypeDescription") + "</li>"%>
                                                                
                                                    <!-- no need to display equipment image since its already on the profile -->

                                                    <!--TODO: Admins only
                                                    String.IsNullOrEmpty(Convert.ToString(Eval("PrimaryContactName"))) ? "" : "<li><strong>Primary Contact: </strong><a href=\"/KGSEquipmentManufacturerContactAdministration.aspx?cid=" + Eval("PrimaryContactID") + "\">" + Eval("PrimaryContactName") + "</a></li>
                                                    String.IsNullOrEmpty(Convert.ToString(Eval("SecondaryContactName"))) ? "" : "<li><strong>Secondary Contact: </strong><a href=\"/KGSEquipmentManufacturerContactAdministration.aspx?cid=" + Eval("SecondaryContactID") + "\">" + Eval("SecondaryContactName") + "</a></li>
                                                    -->
                                                                
                                                    <asp:Literal Visible='<%# !(String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentNotes")))) %>' ID="litAdminBuildingsBody" Text='<%# "<li><strong>Equipment Notes: </strong><div>" + Convert.ToString(Eval("EquipmentNotes")) + "</div></li>" %>' runat="server"></asp:Literal>
                                                    </ul>  
                                            </div>             
                                        </ItemTemplate>                                             
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>   
                                    
                    </asp:Panel>
                    </div>
            </div>
            <div class="block">
                <div class="blockHeader">
                    <span class="blockTitle">Service Details</span>
                    <asp:LinkButton ID="lnkEditServicedDetails" Text="edit service details" runat="server" CssClass="blockEdit" OnClick="showEditEquipmentServicedPanelButton_Click" ValidationGroup="ShowEditEquipmentServicedPanel"></asp:LinkButton>
                    <asp:HyperLink ID="lnkServicedDetails" runat="server" CssClass="toggle"><asp:Label ID="lblServicedDetails" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                </div>
                <div class="blockBody">
                <ajaxToolkit:CollapsiblePanelExtender ID="servicedDetailsCollapsiblePanelExtender" runat="Server"
                            TargetControlID="pnlServicedDetails"
                            CollapsedSize="0"
                            Collapsed="true" 
                            ExpandControlID="lnkServicedDetails"
                            CollapseControlID="lnkServicedDetails"
                            AutoCollapse="false"
                            AutoExpand="false"
                            ScrollContents="false"
                            ExpandDirection="Vertical"
                            TextLabelID="lblServicedDetails"
                            CollapsedText="show service details"
                            ExpandedText="hide service details" 
                            />
                    <asp:Panel ID="pnlServicedDetails" CssClass="blockCollapsiblePanel" runat="server">                                
                    <!--EQUIPMENT SERVICE DETAILS VIEW -->
                            <asp:DetailsView ID="dtvEquipmentServiced" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                <Fields>
                                    <asp:TemplateField  
                                    ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                    ItemStyle-BorderWidth="0" 
                                    ItemStyle-BorderColor="White" 
                                    ItemStyle-BorderStyle="none"
                                    ItemStyle-Width="100%"
                                    HeaderStyle-Width="1" 
                                    HeaderStyle-BorderWidth="0" 
                                    HeaderStyle-BorderColor="White" 
                                    HeaderStyle-BorderStyle="none"
                                    >
                                        <ItemTemplate>
                                            <div>                                            
                                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedDate"))) && String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedBy"))) && String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedCompany"))) ? "<span>No service details provided.</span>" : "<ul class='detailsListNarrow'>" + (String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedDate"))) ? "" : "<li><strong>Last Serviced Date:</strong><div>" + Convert.ToDateTime(Eval("LastServicedDate")).ToShortDateString() + "</div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedBy"))) ? "" : "<li><strong>Last Serviced By:</strong><div>" + Eval("LastServicedBy") + "</div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("LastServicedCompany"))) ? "" : "<li><strong>Last Serviced Company:</strong><div>" + Eval("LastServicedCompany") + "</div></li>") + "</ul>"%>
                                            </div>             
                                        </ItemTemplate>                                          
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>   
                            <asp:Panel ID="pnlEditEquipmentServiced" runat="server" Visible="false">
                                <div class="divFormWide">
                                    <label class="labelNarrow">Last Serviced Date:</label>
                                    <telerik:RadDatePicker ID="txtEditLastServicedDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>
                                </div>
                                <div class="divFormWide">
                                    <label class="labelNarrow">Last Serviced By:</label>
                                    <asp:TextBox ID="txtEditLastServicedBy" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                </div>
                                <div class="divFormWide">
                                    <label class="labelNarrow">Last Serviced Company:</label>
                                    <asp:TextBox ID="txtEditLastServicedCompany" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                </div>
                                <br /><br />
                                <asp:LinkButton CssClass="lnkButtonInline" ID="btnUpdateServiced" runat="server" Text="Update"  OnClick="updateServicedButton_Click" ValidationGroup="EditServiced"></asp:LinkButton>    
                                <asp:LinkButton CssClass="lnkButtonInline" ID="btnCancelServiced" runat="server" Text="Cancel"  OnClick="cancelServicedButton_Click" ValidationGroup="CancelServiced"></asp:LinkButton>    
                                <br /><br /><br />
                            </asp:Panel>
                    </asp:Panel>
                    </div>
            </div>  
            <div class="block">
                <div class="blockHeader">
                    <span class="blockTitle">Additional Details</span>
                    <asp:HyperLink ID="lnkAdditionalDetails" runat="server" CssClass="toggle"><asp:Label ID="lblAdditionalDetails" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                </div>
                <div class="blockBody">
                <ajaxToolkit:CollapsiblePanelExtender ID="additionalDetailsCollapsiblePanelExtender" runat="Server"
                            TargetControlID="pnlAdditionalDetails"
                            CollapsedSize="0"
                            Collapsed="true" 
                            ExpandControlID="lnkAdditionalDetails"
                            CollapseControlID="lnkAdditionalDetails"
                            AutoCollapse="false"
                            AutoExpand="false"
                            ScrollContents="false"
                            ExpandDirection="Vertical"
                            TextLabelID="lblAdditionalDetails"
                            CollapsedText="show additional details"
                            ExpandedText="hide additional details" 
                            />
                    <asp:Panel ID="pnlAdditionalDetails" CssClass="blockCollapsiblePanel" runat="server">
                    <!--EQUIPMENT ADDITONAL DETAILS VIEW -->
                            <asp:DetailsView ID="dtvEquipmentAdditional" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                <Fields>
                                    <asp:TemplateField  
                                    ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                    ItemStyle-BorderWidth="0" 
                                    ItemStyle-BorderColor="White" 
                                    ItemStyle-BorderStyle="none"
                                    ItemStyle-Width="100%"
                                    HeaderStyle-Width="1" 
                                    HeaderStyle-BorderWidth="0" 
                                    HeaderStyle-BorderColor="White" 
                                    HeaderStyle-BorderStyle="none"
                                    >
                                        <ItemTemplate>
                                            <div>                                            
                                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("ExternalEquipmentTypeName"))) && String.IsNullOrEmpty(Convert.ToString(Eval("CMMSReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("CMMSLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("BIMReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("BIMLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("DMSReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("DMSLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("GISReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("GISLink"))) ? "<span>No additional details provided.</span>" : "<ul class='detailsListSeperateLines'>" + (String.IsNullOrEmpty(Convert.ToString(Eval("ExternalEquipmentTypeName"))) ? "" : "<li><strong>External Equipment Type Name:</strong><div>" + Eval("ExternalEquipmentTypeName") + "</div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("CMMSReferenceID"))) ? "" : "<li><strong>CMMS (Computerized Maintenance Management System) ReferenceID:</strong><div>" + Eval("CMMSReferenceID") + "</div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("CMMSLink"))) ? "" : "<li><strong>CMMS (Computerized Maintenance Management System) Link:</strong><div><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("CMMSLink").ToString()) + "'>" + Eval("CMMSLink") + "</a></div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("BIMReferenceID"))) ? "" : "<li><strong>BIM (Building Information Management) ReferenceID:</strong><div>" + Eval("BIMReferenceID") + "</div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("BIMLink"))) ? "" : "<li><strong>BIM (Building Information Management) Link:</strong><div><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("BIMLink").ToString()) + "'>" + Eval("BIMLink") + "</a></div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("DMSReferenceID"))) ? "" : "<li><strong>DMS (Document Management Software) ReferenceID:</strong><div>" + Eval("DMSReferenceID") + "</div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("DMSLink"))) ? "" : "<li><strong>DMS (Document Management Software) Link:</strong><div><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("DMSLink").ToString()) + "'>" + Eval("DMSLink") + "</a></div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("GISReferenceID"))) ? "" : "<li><strong>GIS (Geographical Information System) ReferenceID:</strong><div>" + Eval("GISReferenceID") + "</div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("GISLink"))) ? "" : "<li><strong>GIS (Geographical Information System) Link:</strong><div><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("GISLink").ToString()) + "'>" + Eval("GISLink") + "</a></div></li>") + "</ul>"%>                                                           
                                            </div>             
                                        </ItemTemplate>                                             
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>   
                                    
                    </asp:Panel>
                    </div>
            </div>                     
            <div class="block">
                <div class="blockHeader">
                    <span class="blockTitle">Equipment Variables</span>                                                               
                    <a id="lnkEditVars" class="blockEdit" target="_blank" title="Edit Equipment Variables" href="EquipmentVariables" runat="server">edit variables</a>
                    <asp:HyperLink ID="lnkVars" runat="server" CssClass="toggle"><asp:Label ID="lblVars" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                </div>
                <div class="blockBody">
                <ajaxToolkit:CollapsiblePanelExtender ID="varsCollapsiblePanelExtender" runat="Server"
                            TargetControlID="pnlVars"
                            CollapsedSize="0"
                            Collapsed="true" 
                            ExpandControlID="lnkVars"
                            CollapseControlID="lnkVars"
                            AutoCollapse="false"
                            AutoExpand="false"
                            ScrollContents="false"
                            ExpandDirection="Vertical"
                            TextLabelID="lblVars"
                            CollapsedText="show variables"
                            ExpandedText="hide variables" 
                            />
                    <asp:Panel ID="pnlVars" CssClass="blockCollapsiblePanel" runat="server">
                    <div id="equipmentVariables" visible="false" runat="server">                                                                  
                        <div>
                            <asp:Label ID="lblEquipmentVarsEmpty" Visible="false" runat="server"></asp:Label>                  
                        </div>                                                                                                                                                                                                                                                                                                                 
                            <asp:Repeater ID="rptEquipmentVars" Visible="false" runat="server">
                                <HeaderTemplate>                                                        
                                    <ul class="detailsListProperties">
                                </HeaderTemplate>
                                <ItemTemplate>                                                                                                                                                                               
                                        <%# "<li><strong>" + Eval("EquipmentVariableDisplayName") + (String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentVariableDescription"))) ? ":</strong>" + (String.IsNullOrEmpty(Convert.ToString(Eval("Value"))) ? "" : "<dl><dt>Value: </dt><dd>" + Eval("Value").ToString() + "</dd><dt>Default Value: </dt><dd>" + Eval("BuildingSettingBasedDefaultValue").ToString()) + "</dd></dl>" : ":</strong>" + (String.IsNullOrEmpty(Convert.ToString(Eval("Value"))) ? "" : "<dl><dt>Value: </dt><dd>" + Eval("Value").ToString() + "</dd>") + "<dt>Default Value: </dt><dd>" + Eval("BuildingSettingBasedDefaultValue").ToString() + "</dd><dt>Description: </dt><dd>" + Eval("EquipmentVariableDescription")) + "</dd></dl></li>"%>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </ul>  
                                </FooterTemplate>                                                   
                            </asp:Repeater>                                                                                                                                        
                    </div>   
                    </asp:Panel>
                    </div>
            </div>  

        </div>
                 
        <!--Profile Right Column-->    
        <div class="profileRightColumn">

            <div class="block">
                <div class="blockHeader"><span class="blockTitle">Profile Selection</span><asp:HyperLink ID="lnkBack" runat="server">back to building</asp:HyperLink></div>
                        
                <!--Building Selection-->     
                <asp:Panel ID="pnlBuildings" runat="server">                               
                        <!--<h2>Building Information</h2>-->
                        <div class="divFormThreeColumn">   
                            <label class="labelThreeColumn">*Building:</label> 
                            <extensions:DropDownExtension ID="ddlBuildings" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                            </extensions:DropDownExtension> 
                        </div>                            
                </asp:Panel>                                  

                <!--Equipment Selection-->
                <asp:Panel ID="pnlEquipment" runat="server">                                 
                        <!--<h2>Equipment Information</h2>-->

                        <div class="divFormThreeColumn">
                            <label class="labelThreeColumn">*Equip. Class:</label>
                            <asp:DropDownList ID="ddlEquipmentClasses" CssClass="dropdownNarrow" AppendDataBoundItems="true"
                                OnSelectedIndexChanged="ddlEquipmentClasses_OnSelectedIndexChanged" AutoPostBack="true"
                                runat="server">
                                <asp:ListItem Value="-1">Select building first...</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="equipmentClassRequiredValidator" runat="server" ErrorMessage="Equipment Class is a required field."
                                ControlToValidate="ddlEquipmentClasses" SetFocusOnError="true" Display="None" InitialValue="-1"
                                ValidationGroup="Profile">
                            </asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassRequiredValidatorExtender" runat="server"
                                BehaviorID="equipmentClassRequiredValidatorExtender" TargetControlID="equipmentClassRequiredValidator"
                                HighlightCssClass="validatorCalloutHighlight" Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>
                        </div>

                        <div class="divFormThreeColumn">   
                            <label class="labelThreeColumn">*Equipment:</label>
                            <extensions:DropDownExtension ID="ddlEquipment" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                <asp:ListItem Value="-1" >Select equip. class first...</asp:ListItem>                           
                            </extensions:DropDownExtension>
                        </div>                                                                                                                                                     
                </asp:Panel>              

                </div>

            <!--TODO: show blocks based on module access level-->
            <div id="divDiagnosticsBlock" class="block" runat="server">
                <div class="blockHeader"><span class="blockTitle">Diagnostics</span><a id="lnkDiagnostics" target="_blank" href="Diagnostics.aspx" runat="server">go</a></div>
                <div id='divDiagnosticsTop10' runat='server' class="blockBody">
                    <span>Yesterday's Top Results</span>
                    <ul>
                    <asp:ListView ID="lvDiagnostics" runat="server">
                        <ItemTemplate>
                            <li><a target="_blank" href="Diagnostics.aspx?cid=<%# Eval("CID") %>&bid=<%# Eval("BID") %>&ecid=<%# Eval("EquipmentClassID") %>&eid=<%# Eval("EID") %>&aid=<%# Eval("AID") %>&rng=<%# Eval("AnalysisRange") %>&sd=<%# Eval("StartDate") %>" class='blockLink'><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval("CostSavings"))) %></a> - <%# Eval("NotesSummary")%></li>
                        </ItemTemplate>
                    </asp:ListView>
                    </ul>
                </div>
            </div>
            <div id="divDocumentsBlock" class="block" runat="server">
                <div class="blockHeader"><span class="blockTitle">Documents</span><a id="lnkDocuments" target="_blank" href="Documents.aspx" runat="server">go</a></div>
                <div id='divDocumentsTop10' runat='server' class="blockBody">
                    <span>Top Viewed</span>
                    <ul>
                    <asp:ListView ID="lvDocuments" runat="server">
                        <ItemTemplate>
                            <li><a target="_blank" href="Documents.aspx?bid=<%# Eval("BID") %>&eid=<%# Eval("EID") %>" class='blockLink'><%# Eval("DisplayName") %></a></li>
                        </ItemTemplate>
                    </asp:ListView>
                    </ul>
                </div>
            </div>
            <div id="divProjectsBlock" class="block" runat="server">
                <div class="blockHeader"><span class="blockTitle">Projects</span><a id="lnkProjects" target="_blank" href="Projects.aspx" runat="server">go</a></div>
                <div id='divProjectsTop10' runat='server' class="blockBody">
                    <span>Top Open</span>
                    <ul>
                    <asp:ListView ID="lvProjects" runat="server">
                        <ItemTemplate>
                                <li><a href="Projects.aspx?cid=<%# Eval("CID") %>&bid=0&sfd=<%# Eval("TargetStartDate") %>" target="_blank" class='blockLink'><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval("ProjectedSavings")))%></a> - <%# Eval("ProjectName") %></li>
                        </ItemTemplate>
                    </asp:ListView>
                    </ul>
                </div>
            </div>  
            <div id="divTasksBlock" class="block" runat="server" visible="false">
              <div class="blockHeader"><span class="blockTitle">Tasks</span><a id="lnkTasks" target="_blank" href="Tasks.aspx" runat="server">go</a></div>
              <div id='divTasksTop10' runat='server' class="blockBody">
                <span>Recent Open</span>
                <ul>
                  <asp:ListView ID="lvTasks" runat="server" OnItemDataBound="lvTasks_ItemDataBound">
                    <ItemTemplate>
                      <li><asp:HyperLink ID="lnkTaskRecord" runat="server"></asp:HyperLink> - <asp:Label ID="lblSummary" runat="server" AssociatedControlID="lnkTaskRecord" CssClass="labelCustom" /></li>
                    </ItemTemplate>
                  </asp:ListView>
                </ul>
              </div>
            </div>
            <div id="divReportsBlock" class="block" runat="server">
                <div class="blockHeader"><span class="blockTitle">Reports</span><a id="lnkReports" href="Reports.aspx" target="_blank" runat="server">go</a></div>
                <div class="blockBody">                                
                </div>
            </div>
            <div class="block">
                <div class="blockHeader"><span class="blockTitle">External Links</span></div>
                <div class="blockBody">
                    <!--<span>Some secondary header</span>
                        <span class='spanCentered'>Some secondary header centered</span>
                        <ul>
                            <li><label></label><a class='blockLink'>Test</a></li>
                            <li><label></label><a class='blockLink'>Test</a></li>
                            <li><label></label><a class='blockLink'>Test</a></li>
                            <li><label></label><a class='blockLink'>Test</a></li>
                        </ul>-->
                    <asp:DetailsView ID="dtvExternalLinks" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                            <Fields>
                                <asp:TemplateField  
                                ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                ItemStyle-BorderWidth="0" 
                                ItemStyle-BorderColor="White" 
                                ItemStyle-BorderStyle="none"
                                ItemStyle-Width="100%"
                                HeaderStyle-Width="1" 
                                HeaderStyle-BorderWidth="0" 
                                HeaderStyle-BorderColor="White" 
                                HeaderStyle-BorderStyle="none"
                                >
                                    <ItemTemplate>                                          
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("CMMSLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("BIMLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("DMSLink"))) && String.IsNullOrEmpty(Convert.ToString(Eval("GISLink"))) ? "<span class='spanCentered'>No external links provided.</span>" : "<ul>" + (String.IsNullOrEmpty(Convert.ToString(Eval("CMMSLink"))) ? "" : "<li><label>CMMS Link:</label><a class='blockLink' target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("CMMSLink").ToString()) + "'>" + StringHelper.TrimText(Eval("CMMSLink").ToString(), 25) + "</a></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("BIMLink"))) ? "" : "<li><label>BIM Link:</label><a class='blockLink' target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("BIMLink").ToString()) + "'>" + StringHelper.TrimText(Eval("BIMLink").ToString(), 25) + "</a></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("DMSLink"))) ? "" : "<li><label>DMS Link:</label><a class='blockLink' target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("DMSLink").ToString()) + "'>" + StringHelper.TrimText(Eval("DMSLink").ToString(), 25) + "</a></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("GISLink"))) ? "" : "<li><label>GIS Link:</label><a class='blockLink' target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("GISLink").ToString()) + "'>" + StringHelper.TrimText(Eval("GISLink").ToString(), 25) + "</a></li>") + "</ul>"%>
                                    </ItemTemplate>                                             
                                </asp:TemplateField>
                            </Fields>
                        </asp:DetailsView>  
                </div>
            </div>

        </div>  

        <div style="display: block; clear: both;"></div>

    </asp:Panel>

</asp:Content>                