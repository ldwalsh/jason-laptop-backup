﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.Buildings;
using System;
using Telerik.Web.UI;

namespace CW.Website
{
    public partial class ProviderBuildingAdministration : AdminSitePageTemp
    {
        #region event(s)       

            void Page_Init() => BuildingVariables.AdminMode = BuildingVariables.AdminModeEnum.PROVIDER;

        #endregion

        public enum TabMessages
        {
            Init,
            AddBuilding,
            EditBuilding,
            DeleteBuilding
        }

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewBuildings).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.PROVIDER; } }

            #endregion

        #endregion

        #region method(s)

            #region override(s)

                protected override void SetVisibleTabs()
                {
                    var addBuildingTab = radTabStrip.Tabs[1];
                    var cmmsSettingsTab = radTabStrip.Tabs[4];
                    var goalsTab = radTabStrip.Tabs[5];

                    addBuildingTab.Visible = siteUser.IsKGSFullAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient);
                    cmmsSettingsTab.Visible = goalsTab.Visible = siteUser.IsKGSFullAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient && siteUser.IsProxyClient);

                    //delete in descending order so tab indexes do not change
                    DeleteTabIfNotVisible(goalsTab, 5);
                    DeleteTabIfNotVisible(cmmsSettingsTab, 4);
                    DeleteTabIfNotVisible(addBuildingTab, 1);                                                                                                    
                }

                protected override void SetChangeState() => TabStateMonitor.ChangeState(new Enum[] { KGSBuildingAdministration.TabMessages.Init });

            #endregion

            void DeleteTabIfNotVisible(RadTab tab, int tabIndex)
            {
                //for the tab to be completed "removed", remove the inner control, the page view AND the tab
                if (!tab.Visible)
                {
                    radTabStrip.Tabs[tabIndex].PageView.Controls.RemoveAt(1);

                    radMultiPage.PageViews.RemoveAt(tabIndex);

                    radTabStrip.Tabs.RemoveAt(tabIndex);
                }
            }

        #endregion
    }
}