﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSBuildingClassAdministration.aspx.cs" Inherits="CW.Website.KGSBuildingClassAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                  	                  	
            	<h1>KGS Building Class Administration</h1>                        
                <div class="richText">The kgs building class administration area is used to view, add, and edit building classes.</div>                                 
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Building Classes"></telerik:RadTab>
                            <telerik:RadTab Text="Add Building Class"></telerik:RadTab>
                            <telerik:RadTab Text="Building Variables to a Class"></telerik:RadTab>
                            <telerik:RadTab Text="Building Variable to Multiple Classes"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">   
                                    <h2>View Building Classes</h2> 
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>    
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>        
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridBuildingClasses"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="BuildingClassID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridBuildingClasses_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridBuildingClasses_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridBuildingClasses_Sorting"                                                                                                               
                                             OnSelectedIndexChanged="gridBuildingClasses_OnSelectedIndexChanged"
                                             OnRowEditing="gridBuildingClasses_Editing"     
                                             OnRowDeleting="gridBuildingClasses_Deleting"
                                             OnDataBound="gridBuildingClasses_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingClassName" HeaderText="Building Class">  
                                                    <ItemTemplate><%# Eval("BuildingClassName")%></ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Building Class Description">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("BuildingClassDescription"), 200) %></ItemTemplate>
                                                </asp:TemplateField>                                                   
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this building class permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br />       
                                    <div>                                                  
                                    <!--SELECT BUILDING CLASS DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvBuildingClass" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Building Class Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Building Class Name: </strong>" + Eval("BuildingClassName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("BuildingClassDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("BuildingClassDescription") + "</li>"%>                                                                                                                                                                                                             	                                                                	                                                                                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT BUIILDING CLASS PANEL -->                              
                                    <asp:Panel ID="pnlEditBuildingClass" runat="server" Visible="false" DefaultButton="btnUpdateBuildingClass">                                                                                             
                                        <div>
                                            <h2>Edit Building Class</h2>
                                        </div>  
                                        <div>
                                            <a id="lnkSetFocusEdit" runat="server"></a>                                                        
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Building Class Name:</label>
                                                <asp:TextBox ID="txtEditBuildingClassName" MaxLength="50" runat="server"></asp:TextBox>                                  
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBuildingClass" runat="server" Text="Edit Class"  OnClick="updateBuildingClassButton_Click" ValidationGroup="EditBuildingClass"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editBuildingClassNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Class Name is a required field."
                                        ControlToValidate="txtEditBuildingClassName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditBuildingClass">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingClassNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editBuildingClassNameRequiredValidatorExtender"
                                        TargetControlID="editBuildingClassNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                    </asp:Panel>         
                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">                                 
                                <asp:Panel ID="pnlAddBuildingClass" runat="server" DefaultButton="btnAddBuildingClass">
                                    <h2>Add Buildinng Class</h2> 
                                    <div>        
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                            
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Building Class Name:</label>
                                            <asp:TextBox ID="txtAddBuildingClassName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                                        </div>  
                                        <div class="divForm">
                                            <label class="label">Description:</label>
                                            <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                            <div id="divAddDescriptionCharInfo"></div>
                                        </div>                                                                                             
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddBuildingClass" runat="server" Text="Add Class"  OnClick="addBuildingClassButton_Click" ValidationGroup="AddBuildingClass"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="buildingClassNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Class Name is a required field."
                                        ControlToValidate="txtAddBuildingClassName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddBuildingClass">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="buildingClassNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="buildingClassNameRequiredValidatorExtender"
                                        TargetControlID="buildingClassNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>                  
                            </telerik:RadPageView> 
                            <telerik:RadPageView ID="RadPageView3" runat="server">  
                                <asp:Panel ID="pnlUpdateBuildingVariables" runat="server" DefaultButton="btnUpdateBuildingVariables">                                                                                                                                                                                                    
                                        <h2>Building Variables to a Class</h2>
                                        <p>
                                            Please select a building class in order to select and assign building variables.
                                        </p>
                                        <div>
                                            <a id="lnkSetFocusVariables" runat="server"></a>
                                            <asp:Label ID="lblVariablesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Building Class:</label>    
                                            <asp:DropDownList ID="ddlVariablesBuildingClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVariablesBuildingClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                          
                                        <div id="buildingVariables" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                Building variables assiged to a class will be available to assign to a building with that class.
                                            </p>
                                            <p>
                                                Note: For performance dashboard assign variables (,,,).
                                            </p>                                            
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbVariablesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnUp"  OnClick="btnVariablesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnDown"  OnClick="btnVariablesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbVariablesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBuildingVariables" runat="server" Text="Reassign"  OnClick="updateBuildingVariablesButton_Click" ValidationGroup="UpdateBuildingVariables"></asp:LinkButton>    
                                        </div> 
                                  </asp:Panel>                                                 
                            </telerik:RadPageView>  
                            <telerik:RadPageView ID="RadPageView4" runat="server">  
                                <asp:Panel ID="pnlUpdateBuildingVariable"  runat="server" DefaultButton="btnUpdateBuildingVariable">                                                                                                                                                                                                    
                                        <h2>Building Variable to Multiple Classes</h2>
                                        <p>
                                            Please select a building variable in order to select and assign building classes.
                                        </p>
                                        <div>
                                            <a id="lnkSetFocusVariable" runat="server"></a>
                                            <asp:Label ID="lblVariableError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Building Variable:</label>    
                                            <asp:DropDownList ID="ddlVariableBuildingVariable" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVariableBuildingVariables_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                          
                                        <div id="buildingClasses" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                Building variables assiged to a class will be available to assign to a building with that class.
                                            </p>
                                            <p>
                                                Note: For performance dashboard assign variables (,,,).
                                            </p>                                            
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbClassesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="ImageButton1"  OnClick="btnClassesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="ImageButton2"  OnClick="btnClassesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbClassesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBuildingVariable" runat="server" Text="Reassign"  OnClick="updateBuildingVariableButton_Click" ValidationGroup="UpdateBuildingVariable"></asp:LinkButton>    
                                        </div> 
                                  </asp:Panel>                                                 
                            </telerik:RadPageView>                                                        
                        </telerik:RadMultiPage>
                   </div>                                                                                
</asp:Content>


                    
                  
