﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using CW.Business;
using CW.Data;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Data.Models.DataSourceVendorProduct;
using CW.Utility;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class SystemDataSourceVendorProductsRetrievalMethodsAdministration: SitePage
    {
        #region Properties

            private DataSourceVendorProducts_DataSourceRetrievalMethod mVendorProductRetrievalMethod;
            const string addVendorProductRetrievalMethodSuccess = "Vendor product retrieval method addition was successful.";
            const string addVendorProductRetrievalMethodFailed = "Adding vendor product retrieval method failed. Please contact an administrator.";
            const string addVendorProductRetrievalMethodConfigVariableExists = "Cannot add vendor product retrieval method because the config variable already exists."; 
            const string updateSuccessful = "Vendor product retrieval method update was successful.";
            const string updateFailed = "Vendor product retrieval method update failed. Please contact an administrator.";
            const string updateVendorConfigVariableExists = "Cannot update vendor product retrieval method because the config variable already exists.";     
            const string deleteSuccessful = "Vendor product retrieval method deletion was successful.";
            const string deleteFailed = "Vendor product retrieval method deletion failed. Please contact an administrator.";            
            const string associatedToDataSource = "Cannot delete vendor product retrieval method becuase a data source requires it.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "VendorName";
            const string configVarExists = "A config variable with that name already exists.";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs system admin.
                if (!siteUser.IsKGSSystemAdmin)
                    Response.Redirect("/Home.aspx");   

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindVendorProductsRetrievalMethods();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindVendors(ddlAddVendor);

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetVendorProductRetrievalMethodIntoEditForm(GetDataSourceVendorProductRetrievalMethodData vendorProductRetrievalMethod)
            {
                //ID
                hdnEditID.Value = Convert.ToString(vendorProductRetrievalMethod.VendorProductRetrievalMethodID);
                //Vendor Name
                lblEditVendorName.Text = vendorProductRetrievalMethod.VendorName;
                //Vendor Product Name
                lblEditVendorProductName.Text = vendorProductRetrievalMethod.VendorProductName;
                //Retrieval Method
                lblEditRetrievalMethod.Text = vendorProductRetrievalMethod.RetrievalMethod;
                //Config Variable
                txtEditConfigVariable.Text = vendorProductRetrievalMethod.ConfigVariable;
                
            }

            #endregion

        #region Load and Bind Fields
            
            private void BindVendors(DropDownList ddl)
            {
                ddl.DataTextField = "VendorName";
                ddl.DataValueField = "VendorID";
                ddl.DataSource = DataMgr.DataSourceVendorDataMapper.GetAllVendors();
                ddl.DataBind();
            }

            private void BindVendorProducts(DropDownList ddl, int vendorID)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "VendorProductName";
                ddl.DataValueField = "VendorProductID";
                ddl.DataSource = DataMgr.DataSourceVendorProductDataMapper.GetAllVendorProductsByVendorID(vendorID);
                ddl.DataBind();
            }

            private void BindRetrievalMethods(DropDownList ddl, int vendorProductID)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "RetrievalMethod";
                ddl.DataValueField = "RetrievalMethodID";
                ddl.DataSource = DataMgr.DataSourceRetrievalMethodDataMapper.GetAvailableRetrievalMethodsByProductID(vendorProductID);
                ddl.DataBind();
            }

        #endregion

        #region Load Vendor Product

            protected void LoadAddFormIntoVendorProductRetrievalMethod(DataSourceVendorProducts_DataSourceRetrievalMethod vendorProductRetrievalMethod)
            {
                //Vendor Product ID
                vendorProductRetrievalMethod.VendorProductID = Convert.ToInt32(ddlAddVendorProduct.SelectedValue);
                //Retrieval Method ID
                vendorProductRetrievalMethod.RetrievalMethodID = Convert.ToInt32(ddlAddRetrievalMethod.SelectedValue);
                //Config Variable
                vendorProductRetrievalMethod.ConfigVariable = txtAddConfigVariable.Text;

                vendorProductRetrievalMethod.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoVendorProductRetrievalMethod(DataSourceVendorProducts_DataSourceRetrievalMethod vendorProductRetrievalMethod)
            {
                //ID
                vendorProductRetrievalMethod.VendorProductRetrievalMethodID = Convert.ToInt32(hdnEditID.Value);
                //Config Variable
                vendorProductRetrievalMethod.ConfigVariable = txtEditConfigVariable.Text;

                //Cannot edit vendor product, vendor, or retrievalmethod.
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add vendor product retrieval method Button on click.
            /// </summary>
            protected void addVendorProductRetrievalMethodButton_Click(object sender, EventArgs e)
            {
                //check that config variable does not already exist
                if (!DataMgr.DataSourceVendorProductDataMapper.DoesDataSourceVendorProductRetrievalMethodConfigVarExist(txtAddConfigVariable.Text))
                {
                    mVendorProductRetrievalMethod = new DataSourceVendorProducts_DataSourceRetrievalMethod();

                    //load the form into the vendor product retrieval method
                    LoadAddFormIntoVendorProductRetrievalMethod(mVendorProductRetrievalMethod);

                    try
                    {
                        //insert new vendor product retrieval method
                        DataMgr.DataSourceVendorProductDataMapper.InsertDataSourceVendorProductRetrievalMethod(mVendorProductRetrievalMethod);

                        lblAddError.Text = addVendorProductRetrievalMethodSuccess;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding data source vendor product retrieval method.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addVendorProductRetrievalMethodFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding data source vendor product retrieval method.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addVendorProductRetrievalMethodFailed, lnkSetFocusAdd);      
                    }
                }
                else
                {
                    //config var exists
                    lblAddError.Text = configVarExists;
                    lblAddError.Visible = true;
                    lnkSetFocusAdd.Focus();
                }
                
            }

            /// <summary>
            /// Update vendor product retrieval method Button on click. Updates vendor product retrieval method data.
            /// </summary>
            protected void updateVendorProductRetrievalMethodButton_Click(object sender, EventArgs e)
            {
                DataSourceVendorProducts_DataSourceRetrievalMethod mVendorProductRetrievalMethod = new DataSourceVendorProducts_DataSourceRetrievalMethod();

                //load the form into the vendor product retrieval method
                LoadEditFormIntoVendorProductRetrievalMethod(mVendorProductRetrievalMethod);

                //try to update the vendor product retrieval method
                try
                {
                    //TODO: make config var hdn variables so we can remove this query
                    DataSourceVendorProducts_DataSourceRetrievalMethod originalVendorProductRetrievalMethod = DataMgr.DataSourceVendorProductDataMapper.GetDataSourceVendorProductRetrievalMethodByID(mVendorProductRetrievalMethod.VendorProductRetrievalMethodID);

                    //if the configvar name changed, check that is doenst already exist for another vendor product retrieval method                          
                    //check if the configvariable name exists for another vendor product retrieval method if trying to change
                    if (originalVendorProductRetrievalMethod.ConfigVariable != mVendorProductRetrievalMethod.ConfigVariable && DataMgr.DataSourceVendorProductDataMapper.DoesDataSourceVendorProductRetrievalMethodConfigVarExist(mVendorProductRetrievalMethod.VendorProductRetrievalMethodID, mVendorProductRetrievalMethod.ConfigVariable))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, configVarExists, lnkSetFocusView); 
                        return;
                    }

                    DataMgr.DataSourceVendorProductDataMapper.UpdateDataSourceVendorProductRetrievalMethod(mVendorProductRetrievalMethod);

                    LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView); 

                    //Bind vendor products retrieval methods again
                    BindVendorProductsRetrievalMethods();
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView); 
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating data source vendor product retrieval method.", ex);
                }
             
            }

            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            //protected void searchButton_Click(object sender, EventArgs e)
            //{
            //    sessionState["Search"] = txtSearch.Text;
            //    string[] searchText = txtSearch.Text.Split(' ');
            //    BindSearchedVendorProducts(searchText);
            //}

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            //protected void viewAll_Click(object sender, EventArgs e)
            //{
            //    txtSearch.Text = "";
            //    sessionState["Search"] = String.Empty;
            //    BindVendorProductsRetrievalMethods();
            //}

        #endregion

        #region Dropdown events

            protected void ddlAddVendor_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindVendorProducts(ddlAddVendorProduct, Convert.ToInt32(ddlAddVendor.SelectedValue));
                BindRetrievalMethods(ddlAddRetrievalMethod, Convert.ToInt32(ddlAddVendorProduct.SelectedValue));
            }
            protected void ddlAddVendorProduct_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindRetrievalMethods(ddlAddRetrievalMethod, Convert.ToInt32(ddlAddVendorProduct.SelectedValue));
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind vendor product retrieval methods grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind vendor product retrieval methods to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindVendorProductsRetrievalMethods();
                }
            }

        #endregion

        #region Grid Events

            protected void gridDataSourceVendorProductsRetrievalMethods_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridDataSourceVendorProductsRetrievalMethods_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditVendorProductRetrievalMethod.Visible = false;
                dtvVendorProductRetrievalMethod.Visible = true;

                int vendorProductRetrievalMethodID = Convert.ToInt32(gridDataSourceVendorProductsRetrievalMethods.DataKeys[gridDataSourceVendorProductsRetrievalMethods.SelectedIndex].Values["VendorProductRetrievalMethodID"]);

                //set data source
                dtvVendorProductRetrievalMethod.DataSource = DataMgr.DataSourceVendorProductDataMapper.GetFullVendorProductRetrievalMethodByIDAsEnumerable(vendorProductRetrievalMethodID);
                //bind vendor product retrieval method to details view
                dtvVendorProductRetrievalMethod.DataBind();
            }

            protected void gridDataSourceVendorProductsRetrievalMethods_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridDataSourceVendorProductsRetrievalMethods_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvVendorProductRetrievalMethod.Visible = false;
                pnlEditVendorProductRetrievalMethod.Visible = true;

                int vendorProductRetrievalMethodID = Convert.ToInt32(gridDataSourceVendorProductsRetrievalMethods.DataKeys[e.NewEditIndex].Values["VendorProductRetrievalMethodID"]);

                GetDataSourceVendorProductRetrievalMethodData mVendorProductRetrievalMethod = DataMgr.DataSourceVendorProductDataMapper.GetFullVendorProductRetrievalMethodByID(vendorProductRetrievalMethodID);

                //Set Vendor product retireval method data
                SetVendorProductRetrievalMethodIntoEditForm(mVendorProductRetrievalMethod);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridDataSourceVendorProductsRetrievalMethods_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows id
                int vendorProductRetrievalMethodID = Convert.ToInt32(gridDataSourceVendorProductsRetrievalMethods.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a vendor product retrieval method if it as not been assocaited with
                    //a data source

                    //check if vendor product retrieval method is associated to any data source
                    if (DataMgr.DataSourceVendorProductDataMapper.AreDataSourcesAssignedToDataSourceVendorProductRetrievalmethod(vendorProductRetrievalMethodID))
                    {
                        lblErrors.Text = associatedToDataSource;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }

                    else
                    {
                        //delete vendor product retrieval method
                        DataMgr.DataSourceVendorProductDataMapper.DeleteDataSourceVendorProductRetrievalMethod(vendorProductRetrievalMethodID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting data source vendor product retrieval method.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryVendorProductsRetrievalMethods());
                gridDataSourceVendorProductsRetrievalMethods.PageIndex = gridDataSourceVendorProductsRetrievalMethods.PageIndex;
                gridDataSourceVendorProductsRetrievalMethods.DataSource = SortDataTable(dataTable as DataTable, true);
                gridDataSourceVendorProductsRetrievalMethods.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditVendorProductRetrievalMethod.Visible = false;  
            }

            protected void gridDataSourceVendorProductsRetrievalMethods_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryVendorProductsRetrievalMethods());

                //maintain current sort direction and expresstion on paging
                gridDataSourceVendorProductsRetrievalMethods.DataSource = SortDataTable(dataTable, true);
                gridDataSourceVendorProductsRetrievalMethods.PageIndex = e.NewPageIndex;
                gridDataSourceVendorProductsRetrievalMethods.DataBind();
            }

            protected void gridDataSourceVendorProductsRetrievalMethods_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridDataSourceVendorProductsRetrievalMethods.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryVendorProductsRetrievalMethods());

                GridViewSortExpression = e.SortExpression;

                gridDataSourceVendorProductsRetrievalMethods.DataSource = SortDataTable(dataTable, false);
                gridDataSourceVendorProductsRetrievalMethods.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetDataSourceVendorProductRetrievalMethodData> QueryVendorProductsRetrievalMethods()
            {
                try
                {
                    //get all equipment variables 
                    return DataMgr.DataSourceVendorProductDataMapper.GetAllFullDataSourceVendorProductsRetrievalMethods();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor products retrieval methods.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor products retrieval methods.", ex);
                    return null;
                }
            }

            //private IEnumerable<GetDataSourceVendorProductRetrievalMethodData> QuerySearchedVendorProductsRetrievalMethods(string[] searchText)
            //{
            //    try
            //    {
            //        //get all equipment variables 
            //        return mDataManager.DataSourceVendorProductDataMapper.GetAllSearchedDataSourceVendorProductsWithPartialData(searchText);
            //    }
            //    catch (SqlException sqlEx)
            //    {
            //        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor products retrieval methods.", sqlEx);
            //        return null;
            //    }
            //    catch (Exception ex)
            //    {
            //        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor products retrieval methods.", ex);
            //        return null;
            //    }
            //}

            private void BindVendorProductsRetrievalMethods()
            {
                //query equipment variables
                IEnumerable<GetDataSourceVendorProductRetrievalMethodData> variables = QueryVendorProductsRetrievalMethods();

                int count = variables.Count();

                gridDataSourceVendorProductsRetrievalMethods.DataSource = variables;

                // bind grid
                gridDataSourceVendorProductsRetrievalMethods.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedVendorProducts(string[] searchText)
            //{
            //    //query equipment variables
            //    IEnumerable<GetDataSourceVendorProductRetrievalMethodData> variables = QuerySearchedVendorProductsRetrievalMethods(searchText);

            //    int count = variables.Count();

            //    gridDataSourceVendorProductsRetrievalMethods.DataSource = variables;

            //    // bind grid
            //    gridDataSourceVendorProductsRetrievalMethods.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} data source vendor products retrieval methods found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} data source vendor products retrieval methods found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No data source vendor products retrieval methods found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

        #endregion
    }
}

