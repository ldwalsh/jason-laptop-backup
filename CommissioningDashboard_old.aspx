﻿<%@ Page Language="C#" EnableEventValidation="false" EnableSessionState="true" MasterPageFile="~/_masters/CommissioningDashboard.master" AutoEventWireup="false" CodeBehind="CommissioningDashboard.aspx.cs" Inherits="CW.Website.CommissioningDashboard" Async="true" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
		<div id="commissioningDashboard" class="commissioningDashboard">	   
			<div id="dashboardHeader" class="dashboardHeader"> 
				<div class="commissioningModuleIcon">
				</div>
				<!--cw dashboard logo-->
				<div class="dashboardHeaderLogo">
					Commissioning<br />Dashboard
				</div>
				<div class="headerSpacer"></div>
				<div class="headerClientInfo"> 
					<div class="headerClientImage">     
						<asp:Image ID="imgHeaderClientImage" CssClass="imgHeader" runat="server" />
					</div>          
					<div class="headerClientName">
						<label id="lblHeaderClientName" runat="server"></label>
					</div>
				</div>
				<div class="headerSpacer"></div>
				<div class="headerBuildingInfo">         
					<div class="headerBuildingImage">          
						<asp:Image ID="imgHeaderBuildingImage" CssClass="imgHeader" runat="server" Visible="false" />
					</div>
					<div class="headerBuildingName">
						<!--<label id="lblHeaderBuildingTypeName" runat="server"></label>-->
						<label id="lblHeaderBuildingName" runat="server"></label>
					</div>
				</div>
				<div class="headerBuildingSelect">
					<label class="labelLargeBold">Select Building:</label>
					<asp:DropDownList CssClass="dropdown" ID="ddlBuilding" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
						<asp:ListItem Value="-1">Select one...</asp:ListItem>
						<asp:ListItem Value="all">View All</asp:ListItem>
					</asp:DropDownList>
				</div>
			</div>
			<div id="dashboardBody" class="dashboardBody">
					<div class="dashboardBodyLeftColumn">
						<ul class="dashboardNav">
							<li><asp:LinkButton CssClass="lnkBtnDashboardActive" ID="btnHome" runat="server" Text="Home" OnClientClick="" OnClick="homeButton_Click"></asp:LinkButton></li>
							<li><asp:LinkButton CssClass="lnkBtnDashboard" ID="btnPerformance" runat="server" Text="Performance Data" OnClick="performanceButton_Click"></asp:LinkButton></li>
							<!--<li><asp:LinkButton Visible="false" CssClass="lnkBtnDashboard" ID="btnClientStatistics" runat="server" Text="Client Statistics" OnClick="clientStatisticsButton_Click"></asp:LinkButton></li>-->
							<!--<li><asp:LinkButton Visible="false" CssClass="lnkBtnDashboard" ID="btnBuildingStatistics" runat="server" Text="Building Statistics" OnClick="buildingStatisticsButton_Click"></asp:LinkButton></li>-->
						</ul>
						<hr />
						<div class="dashboardSeondarySelections">
							<asp:MultiView ID="mvSecondary" ActiveViewIndex="0" runat="server">
								<asp:View ID="vSecondaryHome" runat="server" />
								<asp:View ID="vSecondaryPerformance" runat="server">
									<asp:UpdatePanel runat="server" UpdateMode="Conditional">
										<ContentTemplate>
											<div class="divSecondaryForm">
												<label class="labelBold">Equipment Class:</label>  
												<asp:DropDownList ID="ddlPerformanceEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPerformanceEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
													<asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
												</asp:DropDownList> 
											</div>
											<div id="divEquipmentFilter" ClientIDMode="Static" class="divSecondaryForm" runat="server">
												<label class="labelBold">Equipment:</label>  
												<asp:DropDownList ID="ddlPerformanceEquipment" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server">
													<asp:ListItem Value="-1">Select equipment class first...</asp:ListItem>                                 
												</asp:DropDownList> 
											</div>
										</ContentTemplate>
									</asp:UpdatePanel>
                                    <div id="divCost" class="divBodyBlock" runat="server">
                                        <div class="divBodyBlockHeader">
                                            <span>Total Avoidable Cost In Displayed Range</span>
                                        </div>
                                        <div class="divBodyBlockBody">
                                            <span id="spnCostTotal" class="blockValueBlue"></span>
                                        </div>                                    
                                    </div>                                                                       
								</asp:View>
								<asp:View ID="vSecondaryClient" runat="server" />
								<asp:View ID="vSecondaryBuilding" runat="server" />
							</asp:MultiView>

						</div>
					</div>
				<div class="dashboardBodyRightColumn"> 
					<asp:MultiView ID="mvMain" ActiveViewIndex="0" runat="server">
						<asp:View ID="vMainHome" runat="server">
							<ajaxToolkit:RoundedCornersExtender ID="rceHome" runat="server" TargetControlID="divHomeInner" Radius="6" Corners="All" />
							<div id="divHomeInner" class="dashboardHomeBodyInner" runat="server">
                            <h1>Commissioning Dashboard</h1>                                       
							<div class="richText">
								<asp:Literal ID="litDashboardBody" runat="server"></asp:Literal>                                
							</div>  
							<div>                                
								<div id="divSelectedBuilding" class="divSelectedBuilding" runat="server">                                  
								</div>                            
								<telerik:RadRotator ID="rrBuildingScroller" runat="server" 
									CssClass="radRotaterBuildingScroller"
									Width="736px" Height="380px"
									RotatorType="CoverFlowButtons"
									ItemHeight="204" ItemWidth="272"
									OnItemClick="radRotaterBuildingScroller_OnItemClick"
									>                                
									<ItemTemplate>
										 <asp:HiddenField Value='<%# Eval("bid") %>' runat="server" />
                                         <asp:Image ID="imgBuildingImage" ImageUrl='<%# Eval("src") %>' Visible='<%# Eval("imgVisible") %>' AlternateText='<%# Eval("alt") %>' runat="server" />
										 <div class="radRotaterTeaser"><%# Eval("alt") %></div>
									</ItemTemplate>
								</telerik:RadRotator>
							</div> 
                            <div id="divViewAll" class="divViewAll"><asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnViewAll" runat="server" Visible="false" Text="View All" OnClick="btnViewAll_Click" /></div>
                            </div>                                                         
					   </asp:View>
					   <asp:View ID="vMainPerformance" runat="server">
							<ajaxToolkit:RoundedCornersExtender ID="rcePerformance" runat="server" TargetControlID="divPerformanceInner" Radius="6" Corners="All" />
							<div id="divPerformanceInner" class="dashboardBodyInner" runat="server">
								<div class="divChartTypeButtons">
									<span>Display:</span>
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnCostSavings" runat="server" Text="Cost" OnClientClick=";return false;" />
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnFaults" runat="server" Text="Faults" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPriorities" runat="server" Text="Priorities" OnClientClick=";return false;" />									
								</div>
								<div class="divPlotTypeButtons">
									<span>Plot Type:</span>
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnBuilding" runat="server" Text="Building" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnEquipmentClass" runat="server" Text="Equipment Class" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnEquipment" runat="server" Text="Equipment" OnClientClick=";return false;" />
								</div>
								<div class="divDisplayIntervalButtons">
									<span>Interval:</span>
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnDaily" runat="server" Text="Daily" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnWeekly" runat="server" Text="Weekly" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnMonthly" runat="server" Text="Monthly" OnClientClick=";return false;" />
								</div>
								<div id="divPlotRangeButtonsDaily" class="divPlotRangeButtons">
									<span>Plot Range:</span>
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast30Days" runat="server" Text="Past 30 Days" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast60Days" runat="server" Text="Past 60 Days" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast90Days" runat="server" Text="Past 90 Days" OnClientClick=";return false;" />
								</div>
								<div id="divPlotRangeButtonsWeekly" class="divPlotRangeButtons" style="display:none;">
									<span>Plot Range:</span>
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast10Weeks" runat="server" Text="Past 10 Weeks" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast25Weeks" runat="server" Text="Past 25 Weeks" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast52Weeks" runat="server" Text="Past 52 Weeks" OnClientClick=";return false;" />
								</div>
								<div id="divPlotRangeButtonsMonthly" class="divPlotRangeButtons" style="display:none;">
									<span>Plot Range:</span>
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast12Months" runat="server" Text="Past 12 Months" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast24Months" runat="server" Text="Past 24 Months" OnClientClick=";return false;" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPast36Months" runat="server" Text="Past 36 Months" OnClientClick=";return false;" />
								</div>
								<!--performance chart iframe new-->
								<div id="divPerformanceChartIFrames" style="margin-top:5px;">
									<div id="divEmptyDataSet" style="display:none; padding:10px">No commissioning results found.</div>
								</div>
							</div>
					   </asp:View>
					   <asp:View ID="vMainClient" runat="server">
							<ajaxToolkit:RoundedCornersExtender ID="rceClient" runat="server" TargetControlID="divClientInner" Radius="6" Corners="All"></ajaxToolkit:RoundedCornersExtender>  
							<div id="divClientInner" class="dashboardBodyInner" runat="server">
							
							 client statistics

							</div>
					   </asp:View> 
					   <asp:View ID="vMainBuilding" runat="server">
							<ajaxToolkit:RoundedCornersExtender ID="rceBuilding" runat="server" TargetControlID="divBuildingInner" Radius="6" Corners="All"></ajaxToolkit:RoundedCornersExtender>  
							<div id="divBuildingInner" class="dashboardBodyInner" runat="server">
							
							 building statistics

							</div>
					   </asp:View>                           
				   </asp:MultiView>
			   </div> 
			</div>
			<!--dashboard tabs-->
			<div id="dashboardBottomTabs" class="dashboardBottomTabs">

				<!--        OnLoad="loadTabs" 
							OnActiveTabChanged="onActiveTabChanged"
							-->
				<ajaxToolkit:TabContainer ID="TabContainer1"                  
							runat="server"  BackColor="Red"
							AutoPostBack="true"                            
							Width="100%"
							Visible="false" Enabled="false">
							<ajaxToolkit:TabPanel ID="TabPanel1" runat="server" 
								HeaderText="Performance">
								<ContentTemplate>
									
									<div class="dashboardBottomBodyLeftColumn">
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody">
																						
												<!--TODO: RadScheduler for the calendar based chart widget-->

											</div>
										</div>
									</div>
									<div class="dashboardBottomBodyRightColumn"> 
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
									</div> 

								</ContentTemplate>
							</ajaxToolkit:TabPanel>                            
							<ajaxToolkit:TabPanel ID="TabPanel2" runat="server" 
								HeaderText="Statistics">
								<ContentTemplate>
								
								<!-- TODO: RadDock for each widget-->

									<div class="dashboardBottomBodyLeftColumn">
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget
											</div>
											<div class="widgetBody"></div>
										</div>
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
									</div>
									<div class="dashboardBottomBodyRightColumn"> 
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
									</div>

								</ContentTemplate>
							</ajaxToolkit:TabPanel>  
				</ajaxToolkit:TabContainer>
			</div>

			<div id="dashboardFooter" class="dashboardFooter">             
			</div>
		</div>
        
        <ul id="ulContextMenu" class="commissionDashboardPointContextMenu">
            <li>Open in new window</li>
            <li>Open here</li>
        </ul>
			
		<script type="text/javascript">
			
            function loadCommissioningDashboard()
            {
				document.disableContextMenu()

				new $.asp.SessionKeepAlive(document.baseUrl.endWith("/") + "_framework/httphandlers/SessionKeepAliveHandler.ashx").enable()

            	new CommissioningDashboard().init()
            }

		</script>
					
</asp:Content>
