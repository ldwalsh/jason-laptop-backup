﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Help.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="Documentation.aspx.cs" Inherits="CW.Website.Documentation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                 	                  	        	
        	<h1>Documentation</h1>                          
            <div class="richText">Documentation section.</div>   
            <br />
            <div class="documentation" runat="server">

            <asp:Repeater ID="rptDocumentation" OnItemCommand="downloadButton_Click" runat="server">
                <HeaderTemplate>
                <div class="groupRepeaterHeader">
                    <h2>Manuals</h2>
                </div>
                </HeaderTemplate>            
                <ItemTemplate>
                    <div class="groupRepeaterItem">
                        <h3 id="headerFileName" runat="server"><%# Eval("FileName") %></h3>
                        <asp:LinkButton ID="btnDownload" Text="download" CommandArgument='<%# Eval("FilePath").ToString() %>' runat="server"></asp:LinkButton>
                    </div>
                </ItemTemplate>
            </asp:Repeater>      
                                   
            </div>   
    
</asp:Content>                