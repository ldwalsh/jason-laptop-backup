﻿using CW.Website._framework;
using iCL;
using iCLib;
using JSAsset;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace CW.Website.AnalysisBuilder
{
	public partial class Default: SitePage, SitePage.IClientPage
	{
		private void Page_Init()
		{
			var browser = Request.Browser;

			if ((browser.Browser == "IE") && (Single.Parse(browser.Version, CultureInfo.InvariantCulture) < 9))
			{
				divLoadingAnalysisBuilder.InnerHtml = "The Analysis Builder module requires Internet Explorer 9.0 or higher.";

				divLoadingAnalysisBuilder.Attributes.Remove("class");

				divLoadingAnalysisBuilder.Style.Add("margin", "25px");

				return;
			}

            divGlobalSettingsMsg.InnerHtml = DataMgr.GlobalDataMapper.GetGlobalSettings().AnalysisBuilderBody;

			ScriptIncluder.GetInstance(this)
                .Require("window.Type")
				.Add<core.coreEnum>()
				.Add<core.Event>()		//remove
				.Add<core.DELEGATE>()	//remove
				.Add<core.Array>()
				.Add<core.Char>()
				.Add<core.RegExpPrototype>()
				.Add<core.Boolean>()
				.Add<core.Function>()
				.Add<core.String>()
				.Add<core.RegExp>()
				.Add<core.Date>()
				.Add<core.DatePrototype>()
				.Add<core.Downloader>()
				.Add<core.MessageBus>()
				.Add<core.KeyedArray>()
				.Add<core.Lookup>()
				.Add<core.CallbackHandler>()
				.Add<core.asp.DataStore>()
				.Add<core.Time>()
				.Add<core.DateRange>()
				.Add<core.ExecutionQueue>()
				.Add<core.Math>()
				.Add<core.Navigator>()
				.Add<core.CSSStyleDeclaration>()
				.Add<core.HTML.HTMLFormElement>()
				.Add<core.HTMLCollectionPrototype>()
				.Add<core.HTMLFormPrototype>()
				.Add<core.HTMLInputElement>()
				.Add<core.HTMLScriptElement>()
				.Add<core.HTMLSelectElement>()
				.Add<core.HTMLTextAreaElement>()
				.Add<core.Location>()
				.Add<core.ui.ControlsGroup>()
				.Add<core.ui.controls.Accordion>()
				.Add<core.ui.controls.ButtonBar>()
				.Add<core.ui.controls.MenuBar>()
				.Add<core.ui.controls.Repeater>()
				.Add<core.ui.controls.Table>()
				.Add<core.ui.controls.TableView>()
				.Add<core.ui.controls.TabStrip>()
				.Add<core.ui.controls.TreeView>()
				.Add<core.ui.controls.TTT>()
				.Add<core.ui.controls.ViewToggle>()
				.Add<core.ui.controls.ContextMenu>()
				//.Add<core.ui.ObjectDropTarget>()
				.Add<core.ui.TemplatedElement>()
				.Add<core.ui.Validator>()
				.Add<core.ui.ViewBase>()
				.Add<core.ui.WindowGroup>()
				.Add<core.ui.WindowPinner>()
				.Add<core.ui.windows.DialogWindow>()
				.Add<core.ui.windows.InputBox>()
				.Add<core.ui.windows.MessageBox>()
				.Add<core.ui.windows.ToolWindow>()
				.Add<core.ui.windows.WaitBox>()
				.Add<core.ui.controls.Button>()
				.Add<core.Url>();

			JSAssetManager.GetInstance(this).Add<UserAnalysisAsset>();

            //var x = Linking.Generate(1, 36, 16, new[]{new Linking.PointInfo(516, new DateTime(2015, 2, 17), new DateTime(2015, 2, 18))});
		}


		#region IClientPage

			Dictionary<String,String> IClientPage.Properties
			{
				get
				{
					return new Dictionary<String,String>
					{
						{"uid", siteUser.UID.ToString()}, {"oid", siteUser.ClientOID.ToString()}, {"cid", siteUser.CID.ToString()}, {"roleId", siteUser.RoleID.ToString()}, {"culture", "\"" + siteUser.CultureName + "\""}
					};
				}
			}

			Dictionary<String,String> IClientPage.AsyncProperties
			{
				get {return null;}
			}

		#endregion
	}
}