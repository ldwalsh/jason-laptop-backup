﻿using CW.Website._framework;
using CW.Website.AnalysisBuilder.domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.AnalysisBuilder
{
	public abstract class ScriptObjectBase
	{
		#region STATIC

			private readonly static ILookup<Type,String> typeLookup;

			static ScriptObjectBase()
			{
				typeLookup = //todo: revise not to use this approach?
				new Dictionary<Type,String>
				{
					{typeof(EquipmentVariableValue), "UA.evaluation.EquipmentVariableValue"},
				
				}.ToLookup(_=>_.Key, _=>_.Value);
			}

		#endregion

		private readonly String scriptType;

		public ScriptObjectBase()
		{
			var t = GetType();

			//scriptType = (typeLookup[t].SingleOrDefault() ?? ("UA." + t.Name));

			__type = (typeLookup[t].SingleOrDefault() ?? ("UA." + t.Name));
		}

		public virtual String __type
		{
			private set;
			get;

			//get {return scriptType;}
		}

		//private SiteUser _siteUser;

		//protected SiteUser siteUser
		//{
		//	get {return _siteUser ?? SiteUser.Current;}
		//}
	}
}