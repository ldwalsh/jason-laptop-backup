﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="Default.aspx.cs" Inherits="CW.Website.AnalysisBuilder.Default" MasterPageFile="~/_masters/AnalysisBuilder.Master" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

	<script type="text/javascript">

		//"use strict";

		if (!window.NamedNodeMap)
		{
			if (window.MozNamedAttrMap)
			{
				window.NamedNodeMap = MozNamedAttrMap;
			}
			else throw new Error(71972132)
		}

		function loadIDE()
		{
			ctl00_Login.showedRenewModalCallbackArray.push
			(
				function()
				{
					var style;
					
					style			= $gId("mpeRenewSessionBehavior_backgroundElement").style;
					style.zIndex	= iCL.ui.LayerManager.layerGroups.sysModal.min;
					style.position	= "absolute";

					style			= $gId("ctl00_Login_pnlRenewSession").style;
					style.zIndex	= iCL.ui.LayerManager.layerGroups.sysModal.min+1;
					style.position	= "absolute";
				}
			)

			//$.asp.SessionKeepAlive.NEW(document.baseUrl.endWith("/") + "../_framework/httphandlers/SessionKeepAliveHandler.ashx").enable()

			UA.BuilderEnviornment.createInstance($gId("IDE"), UA.SiteUser.NEW(page.uid, page.oid, page.cid, page.roleId, page.culture), $gId("ctl00_plcCopy_divLoadingAnalysisBuilder"))
	}

	</script>
	
	<div id="UAB">
		<div id="divLoadingAnalysisBuilder" runat="server" class="AnalysisBuilderModuleIcon" style="visibility:hidden;">
            <div>Loading Analysis Builder...</div>
            <div id="divGlobalSettingsMsg" runat="server"></div> 
        </div>
		<div id="IDE" style="display:none;">
			<div id="divLPane">
				<div id="divHeader">
					<div id="divTitle" class="AnalysisBuilderModuleIcon">
						<span>Analysis Builder</span>
					</div>
				</div>
				<div id="divEquipmentTree"></div>
			</div>
			<div id="divRPane">
				<div id="divTabs"></div>
				<div id="divChartPane"></div>
			</div>
		</div>
	</div>

</asp:Content>