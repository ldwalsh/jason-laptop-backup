﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;

namespace CW.Website.AnalysisBuilder.Domain
{
    public enum DataTypeEnum
    {
        Boolean,
        String,
        Number,
        DateTime,
    }

    public enum FunctionCategoryEnum
    {
        Operator,
		Conditional,
        Fault,
        Physics,
		Numeric,
    }

    public abstract class FunctionObjBase
    {
        #region property

			public String __type //should be private but wont be serialized
			{
				get {return "UA.FunctionObjBase";}
			}

            public FunctionCategoryEnum Category
            {
                protected set;
                get;
            }

			[
			ScriptIgnore
			]
            public String Symbol
            {
                protected set;
                get;
            }

			public String Name
			{
				protected set;
				get;
			}

            public String Label
            {
                protected set;
                get;
            }

            public String Description
            {
                protected set;
                get;
            }

			[
			ScriptIgnore
			]
            public IList<IList<DataTypeEnum>> Inputs
            {
                protected set;
                get;
            }

			[
			ScriptIgnore
			]
            public DataTypeEnum Output
            {
                protected set;
                get;
            }

        #endregion
    }

    public abstract class FunctionCallBase
    {
        #region method

            public abstract Object Call(IList<Object> arguments);

        #endregion
    }
}