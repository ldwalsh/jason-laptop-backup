﻿using System;

namespace CW.Website.AnalysisBuilder.Domain
{
	public class VisuProperties
	{
		public Int32    VisuId;
		public String   Name;
		public String   Description;
		public DateTime Modified;
		public Boolean  SharedRead;
		public Boolean  SharedWrite;
		public String   Notes;
	}
}