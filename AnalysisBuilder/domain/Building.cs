﻿using System;

namespace CW.Website.AnalysisBuilder.Domain
{
	public class Building: ScriptObjectBase
	{
		public Int32  BuildingId;
		public Int32  ClientId;
		public String Name;
	}
}