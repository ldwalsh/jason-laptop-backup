﻿using CW.Website.AnalysisBuilder;
using System;

namespace CW.Website.AnalysisBuilder.domain
{
	public class EquipmentObj: ScriptObjectBase
	{
		public Int32	Id;
		public Int32	BuildingId;
		public Int32	ClassId;
		public String	Name;
	}
}