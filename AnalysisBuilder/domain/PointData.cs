﻿using System;
using System.Collections.Generic;

namespace CW.Website.AnalysisBuilder.Domain
{
	public class PointData: ScriptObjectBase
    {
        #region static

            public class Comparer: IEqualityComparer<PointData>
            {
                public Boolean Equals(PointData a, PointData b)
                {
                    return ((a.PointId == b.PointId) && (a.Logged == b.Logged));
                }

                public Int32 GetHashCode(PointData pd)
                {
                    return (pd.PointId.Value ^ pd.Logged.GetHashCode());
                }
            }

        #endregion

        public Int32? PointId;
        public String Logged;
		public Double Value;
	}
}