﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace CW.Website.UserAnalysisBuilder.Domain
{
	public class DWInfo
	{
		#region STATIC

			#region method

				public static void save(DWInfo dwInfo)
				{
					var db = DBContext.GetInstance();

					var _dwInfo = DWInfo.GetByWindowID(dwInfo.WindowID);

					if (_dwInfo == null)
					{
						dwInfo.UID = 0; //use SiteUser

						db.DWInfo.Add(dwInfo);
					}
					else
					{
						_dwInfo.X = dwInfo.X;
						_dwInfo.Y = dwInfo.Y;
						_dwInfo.W = dwInfo.W;
						_dwInfo.H = dwInfo.H;
					}

					db.SaveChanges();					
				}

				public static DWInfo GetByWindowID(Int32 windowID)
				{
					var db = DBContext.GetInstance();

					return (from dwi in db.DWInfo where (dwi.UID == 0) && (dwi.WindowID == windowID) select dwi).FirstOrDefault();
				}

			#endregion

		#endregion

		#region property

			[
			Key,
			ScriptIgnore
			]
			public Int32 ID
			{
				get;
				set;
			}

			[
			Required,
			ScriptIgnore
			]
			public Int32 UID
			{
				get;
				set;
			}

			[
			Required
			]
			public Int32 WindowID
			{
				get;
				set;
			}

			[
			Required
			]
			public Int32 X
			{
				get;
				set;
			}

			[
			Required
			]
			public Int32 Y
			{
				get;
				set;
			}

			public Int32 W
			{
				get;
				set;
			}

			public Int32 H
			{
				get;
				set;
			}

			//

			public String __type
			{
				get {return "UA.views.DWInfo";}
			}

		#endregion
	}
}