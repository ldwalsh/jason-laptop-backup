﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Linq;

namespace CW.Website.UserAnalysisBuilder.Domain
{
	public class EquipmentAssociationProfile
	{
		#region STATIC

			public static EquipmentAssociationProfile Create(Int32 analysisObjId, String name)
			{
				var db = DBContext.GetInstance();

				if ((from eap in db.EquipmentAssociationProfile where (eap.AnalysisObj.ID == analysisObjId) && (eap.Name == name) select eap).Any())
					throw new Exception("EquipmentAssociationProfile object already exists with the given name");

				var obj =
					new EquipmentAssociationProfile
					{
						AnalysisObj		= db.AnalysisObj.Find(analysisObjId), //AnalysisObj.GetByID(analysisObjId),
						DateCreated		= DateTime.Now,
						DateModified	= DateTime.Now,
						Name			= name,
					};

				db.EquipmentAssociationProfile.Add(obj);

				db.SaveChanges();

				return obj;
			}

		#endregion

        #region property

			[
			NotMapped
			]
			public String __type
			{
				get {return "UA.EquipmentAssociationProfile";}
			}
			
			[
			Key
			]
            public Int32 ID
            {
                set;
                get;
            }

			[
			Required
			]
            public DateTime DateCreated
            {
                set;
                get;
            }

			[
			Required
			]
            public DateTime DateModified
            {
                set;
                get;
            }
						
			[
			Required
			]
            public String Name
            {
                set;
                get;
            }

			//

			[
			ScriptIgnore
			]
			public AnalysisObj AnalysisObj
			{
				set;
				get;
			}

			public IList<EquipmentAssociation> EquipmentAssociations
			{
				set;
				get;
			}

        #endregion
	}
}