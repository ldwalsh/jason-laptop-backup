﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Linq;

namespace CW.Website.UserAnalysisBuilder.Domain
{
	public class EquipmentAssociation
	{
		#region STATIC

			public static EquipmentAssociation Create(Int32 profileId, String alias, Int32 equipmentId)
			{
				var db = DBContext.GetInstance();

				var q =
					(
						from ea in db.EquipmentAssociation.Include("EquipmentAssociationProfile")
						where (ea.EquipmentAssociationProfile.ID == profileId) && (ea.Alias == alias)
						select ea
					);

				if (q.Any()) throw new Exception("EquipmentAssociationProfile object already exists with the given name");

				var obj =
					new EquipmentAssociation
					{
						Alias						= alias,
						DateCreated					= DateTime.Now,
						DateModified				= DateTime.Now,
						EquipmentAssociationProfile	= db.EquipmentAssociationProfile.Find(profileId),
						EquipmentId					= equipmentId,
					};

				db.EquipmentAssociation.Add(obj);

				db.SaveChanges();

				return obj;
			}

			public static EquipmentAssociation Update(Int32 associationId, String alias, Int32 equipmentId)
			{
				var db = DBContext.GetInstance();

				var obj	= db.EquipmentAssociation.Find(associationId);

				if (obj == null) throw new Exception("Association could not be updated because it does not exist");

				obj.DateModified = DateTime.Now;
				obj.Alias		 = alias;
				obj.EquipmentId	 = equipmentId;

				db.SaveChanges();

				return obj;
			}

			public static void Delete(Int32 associationId)
			{
				var db = DBContext.GetInstance();

				db.EquipmentAssociation.Remove(db.EquipmentAssociation.Find(associationId));

				db.SaveChanges();
			}

		#endregion

		#region field

			private Int32? equipmentClassId;

		#endregion

        #region property

			[
			NotMapped
			]
			public String __type
			{
				get {return "UA.EquipmentAssociation";}
			}
			
			[
			Key
			]
            public Int32 ID
            {
                set;
                get;
            }

			[
			Required
			]
            public DateTime DateCreated
            {
                set;
                get;
            }

			[
			Required
			]
            public DateTime DateModified
            {
                set;
                get;
            }
						
			[
			Required
			]
            public String Alias
            {
                set;
                get;
            }

			[
			NotMapped
			]
			public Int32 EquipmentClassId
			{
				get
				{
					if (equipmentClassId == null)
					{
						equipmentClassId = new CW.Business.DataManager(true).EquipmentDataMapper.GetEquipmentClassIDByEID(this.EquipmentId);
					}

					return equipmentClassId.Value;
				}
			}

			[
			Required
			]
            public Int32 EquipmentId
            {
                set;
                get;
            }

			//

			[
			ScriptIgnore
			]
			public EquipmentAssociationProfile EquipmentAssociationProfile
			{
				set;
				get;
			}

        #endregion
	}
}