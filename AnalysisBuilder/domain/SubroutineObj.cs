﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Linq;

namespace CW.Website.UserAnalysisBuilder.Domain
{
	public class SubroutineObj
	{
		#region STATIC

			#region method

				public static SubroutineObj Create(String name)
				{
					return new SubroutineObj{Name=name};
				}

				public static SubroutineObj GetByID(Int32 id)
				{
					return DBContext.GetInstance().SubroutineObj.Include("Equipment").Where(_=>(_.ID==id)).Single();

					//return (from so in DBContext.GetInstance().SubroutineObj where (so.ID == id) select so).ToList()[0];
				}

				public static SubroutineObj[] GetAll()
				{
					return DBContext.GetInstance().SubroutineObj.ToArray();

					//return (from so in DBContext.GetInstance().SubroutineObj select so).ToArray();
				}

				public static SubroutineObj[] GetByAnalysisObjID(Int32 analysisObjID)
				{
					return DBContext.GetInstance().SubroutineObj.Include("Equipment").Where(so=>so.AnalysisObjs.Any(ao=>(ao.ID == analysisObjID))).ToArray();
				}

				public static void UpdateEquipment(Int32 id, SubroutineObj_EquipmentClass subroutineEquipment)
				{
					var dc = DBContext.GetInstance();

					var list = dc.SubroutineObj_EquipmentClasses.Where(_=>(_.SubroutineObj.ID == id)).ToList();

					foreach (var l in list)
					{
						dc.SubroutineObj_EquipmentClasses.Remove(l);
					}

					dc.SaveChanges();
					
					
															
				}

			#endregion

		#endregion

		#region property

			[
			NotMapped
			]
			public String __type
			{
				get {return "UA.SubroutineObj";}
			}

			[
			Key
			]
			public Int32 ID
			{
				get;
				set;
			}
			
			[
			Required
			]
			public String Name
			{
				get;
				set;
			}

			public Int32 InputEngUnit
			{
				get;
				set;
			}


			public Int32 OutputEngUnit
			{
				get;
				set;
			}

			public String Definition
			{
				get;
				set;
			}

			public IList<SubroutineObj_EquipmentClass> Equipment
			{
				set;
				get;
			}

			[
			ScriptIgnore
			]
			public IList<AnalysisObj> AnalysisObjs
			{
				set;
				get;
			}

		#endregion
	}
}