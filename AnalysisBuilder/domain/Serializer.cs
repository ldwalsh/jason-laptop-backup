﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace CW.Website.AnalysisBuilder.Domain
{
    public class Serializer<T>
    {
        private readonly FileInfo fileInfo;

        public Serializer(FileInfo fileInfo)
        {
            this.fileInfo = fileInfo;
        }
             
        public void Serialize(T obj)
        {
            var stream = File.Open(fileInfo.FullName, FileMode.Create);

            new BinaryFormatter().Serialize(stream, obj);
       
            stream.Close();
        }
 
        public T Deserialize()
        {
            if (!fileInfo.Exists) return default(T);

            var stream = File.Open(fileInfo.FullName, FileMode.Open);

            if (stream.Length == 0)
            {
                stream.Close();

                return default(T);
            }
       
            var obj = (T)new BinaryFormatter().Deserialize(stream);
       
            stream.Close();
       
            return obj;
        }
    }
}