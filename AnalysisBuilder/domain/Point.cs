﻿using CW.Website.AnalysisBuilder;
using System;

namespace CW.Website.AnalysisBuilder.Domain
{
	public sealed class Point: ScriptObjectBase
	{
		public Int32  Id;		
		public Int32  ClassId;
		public Int32  TypeId;
		public Int32  EngUnitId;		
		public String Name;
		public Int32  Sampling;
	}
}