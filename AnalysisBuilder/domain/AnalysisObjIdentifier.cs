﻿using CW.Business;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace CW.Website.AnalysisBuilder.Domain
{
	public class VisuIdentifier
	{			
		#region static

			public static IEnumerable<Object> GetAll(SiteUser siteUser)
			{
				return new DataManagerSQL(ConfigurationManager.ConnectionStrings["CWConnectionString"].ConnectionString).AnalysisBuilderDataMapper.GetOpenViewList(siteUser.CID, siteUser.UID);          
			}

		#endregion

		#region field

			public Int32    ID;
			public String   Description;
			public String   Name;
			public DateTime Created;
			public DateTime Modified;
			public Int32    OwnerId;
			public String   Owner;
			public Boolean  SharedRead;
			public Boolean  SharedWrite;

		#endregion
	}
}