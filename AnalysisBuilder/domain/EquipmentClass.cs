﻿using CW.Website.AnalysisBuilder;
using System;

namespace CW.Website.AnalysisBuilder.domain
{
	public class EquipmentClass: ScriptObjectBase
	{
		public Int32  Id;
		public String Name;
	}
}