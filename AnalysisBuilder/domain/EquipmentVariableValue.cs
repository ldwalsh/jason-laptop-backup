﻿using CW.Website.AnalysisBuilder;
using System;

namespace CW.Website.AnalysisBuilder.domain
{
	public class EquipmentVariableValue: ScriptObjectBase
	{
		public String Value;
		public Int32  EngUnitID;
	}
}