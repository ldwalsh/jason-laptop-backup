﻿using CW.Business;
using CW.Website._framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Security;

namespace CW.Website.AnalysisBuilder.Domain
{
    [
    TableAttribute("AnalysisBuilderViewObj")
    ]
    public class Visu : ScriptObjectBase
    {
        #region STATIC

        #region method

        //public static AnalysisObj Create(String name, String plots, Boolean isPrivate)
        //{
        //	var db = DBContext.GetInstance();

        //	if ((from ao in db.AnalysisObj where ao.Name == name select ao).Any()) return null;

        //	var now = DateTime.Now;

        //	var o =	new AnalysisObj{Name=name, Plots=plots, IsPrivate=isPrivate,Created=now, Modified=now,};

        //	db.AnalysisObj.Add(o);

        //	db.SaveChanges();

        //	return o;
        //}

        public static void Save(Visu visu, SiteUser siteUser)
        {
            if (visu.ClientId != siteUser.CID) throw new SecurityException();

            if (visu.SharedWrite && !visu.SharedRead) throw new InvalidOperationException();

            visu.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public).ToList().ForEach
            (
                (PropertyInfo info) =>
                {
                    var a = info.GetCustomAttribute<MaxLengthAttribute>(true);

                    if (a == null) return;

                    if (info.GetValue(visu).ToString().Length > a.Length) throw new ValidationException();
                }
            );

            //validate any expression names & expressions here

            var db = DBContext.GetInstance();

            if (visu.Id == 0)
            {
                if (visu.OwnerId != siteUser.UID) throw new SecurityException();

                db.Visulizations.Add(visu);
            }
            else
            {
                var ao = db.Visulizations.FirstOrDefault(_ => (_.Id == visu.Id));

                if (ao == null) throw new InvalidOperationException("already deleted"); //message used client side to identify error... will revise

                if ((visu.OwnerId != siteUser.UID) && !ao.SharedWrite) throw new SecurityException();

                if (ao.Modified == visu.Modified) throw new InvalidOperationException();
                if ((ao.OwnerId != siteUser.UID) && (!ao.SharedWrite)) throw new SecurityException();

                db.Entry(ao).State = System.Data.Entity.EntityState.Detached;

                db.Visulizations.Attach(visu);

                db.Entry(visu).State = System.Data.Entity.EntityState.Modified;
            }

            db.SaveChanges();
        }

        public static void SaveProperties(VisuProperties visuProps, SiteUser siteUser)
        {
            if (visuProps.VisuId == 0) throw new InvalidOperationException();
            if (visuProps.SharedWrite && !visuProps.SharedRead) throw new InvalidOperationException();

            var db = DBContext.GetInstance();

            var ao = db.Visulizations.FirstOrDefault(_ => (_.Id == visuProps.VisuId));

            if (ao == null) throw new InvalidOperationException("already deleted"); //message used client side to identify error... will revise

            if (ao.ClientId != siteUser.CID) throw new SecurityException();

            if ((ao.OwnerId != siteUser.UID) && !ao.SharedWrite) throw new SecurityException();

            if (ao.Modified == visuProps.Modified) throw new InvalidOperationException();

            typeof(Visu).GetProperties(BindingFlags.Instance | BindingFlags.Public).ToList().ForEach
            (
                (PropertyInfo info) =>
                {
                    var a = info.GetCustomAttribute<MaxLengthAttribute>(true);

                    if (a == null) return;

                    if (visuProps.GetType().GetField(info.Name).GetValue(visuProps).ToString().Length > a.Length) throw new ValidationException();
                }
            );

            ao.Name = visuProps.Name;
            ao.Description = visuProps.Description;
            ao.Modified = visuProps.Modified;
            ao.SharedRead = visuProps.SharedRead;
            ao.SharedWrite = visuProps.SharedWrite;
            ao.Notes = visuProps.Notes;

            db.SaveChanges();
        }

        public static void Delete(Int32 id, SiteUser siteUser)
        {
            if (id <= 0) throw new ArgumentException();

            var db = DBContext.GetInstance();

            var ao = db.Visulizations.First(_ => _.Id == id);

            if (ao.ClientId != siteUser.CID) throw new SecurityException();
            if ((ao.OwnerId != siteUser.UID) && !ao.SharedWrite) throw new SecurityException();

            db.Visulizations.Remove(ao);

            db.SaveChanges();
        }

        public static Visu GetById(Int32 id, DataManager dm)
        {
            if (id < 1) throw new ArgumentException("Invalid Visulization ID.");

            var visu = DBContext.GetInstance().Visulizations.Where(_ => _.Id == id).Single();

            var converter = new FractionalSecondsConverter();

            // NOTE: JSON.NET deserializes all integers to Int64. Cast as appropriate...
            dynamic plots = JsonConvert.DeserializeObject<ExpandoObject>(visu.Plots.Replace("__type", "TYPE__"), new ExpandoObjectConverter());

            foreach (dynamic plot in plots.v)
            {
                if (plot.TYPE__ != "UA.charting.RawDataPlot") continue; //remove this hard-coding somehow

                plot.equipmentName = dm.EquipmentDataMapper.GetEquipmentNameByEID((Int32)plot._equipmentId); //get latest equipmentName

                var point = dm.PointDataMapper.GetPointByPID((Int32)plot.point.id);
                var pointType = dm.PointTypeDataMapper.GetPointType(point.PointTypeID);
                var pointClass = dm.PointClassDataMapper.GetPointClass(pointType.PointClassID);

                var p = plot.point;

                p.name = point.PointName;
                p.typeId = point.PointTypeID;
                p.classId = pointClass.PointClassID;
                p.engUnitId = (dm.BuildingDataMapper.GetBuildingSettingsByBID((Int32)plot._buildingId).UnitSystem ? pointClass.SIEngUnitID : pointClass.IPEngUnitID);
                p.sampling = point.SamplingInterval;
            }

            visu.Plots = ((String)JsonConvert.SerializeObject(plots, converter))
                                                .Replace("\"v\":{}", ("\"v\":" + JsonConvert.SerializeObject(plots.v)));

            // NOTE: JSON.NET deserializes all arrays to Lists. Using Count rather than Length...
            for (var i = 0; i < plots.v.Count; i++)
            {
                var p = plots.v[i];

                // NOTE: ExpandoObject wil throw if you try to access a key that does not exist.
                // see:  http://stackoverflow.com/questions/2839598/how-to-detect-if-a-property-exists-on-an-expandoobject
                if (( (IDictionary<String, object>)p ).ContainsKey("_annotations") == false)
                {
                    continue;
                }

                var annotations = p._annotations;

                var pe = (String)JsonConvert.SerializeObject(p, converter);

                visu.Plots = visu.Plots.Replace(pe, pe.Replace("\"v\":{}", ("\"v\":" + (String)JsonConvert.SerializeObject(annotations.v, converter))));
            }

            visu.Plots = visu.Plots.Replace("TYPE__", "__type");

            return visu;
        }


        //				public static AnalysisObj Create(String name)
        //				{
        //					var db = DBContext.GetInstance();

        //					if ((from ao in db.AnalysisObj where ao.Name == name select ao).Any()) return null;

        //					var analysisObj =
        //						new AnalysisObj
        //						{
        //							DateCreated	= DateTime.Now,
        //							Name		= name
        //						};

        //					db.AnalysisObj.Add(analysisObj);

        //					db.SaveChanges();

        //					return analysisObj;
        //				}

        //				public static SubroutineObj AddSubroutine(Int32 analysisObjId, String subroutineName)
        //				{
        //					var analysisObj = AnalysisObj.GetByID(analysisObjId);

        //					if (analysisObj == null) throw new Exception();

        //					var subroutineObj = SubroutineObj.Create(subroutineName);

        //					analysisObj.AddSubroutine(subroutineObj);

        //					DBContext.GetInstance().SaveChanges();

        //					return subroutineObj;
        //				}

        //				public static void ImportSubroutine(Int32 analysisObjID, Int32 subroutineObjID)
        //				{
        //					var analysisObj = AnalysisObj.GetByID(analysisObjID);

        //					if (analysisObj == null) throw new Exception();

        //					var subroutineObj = SubroutineObj.GetByID(subroutineObjID);

        //					analysisObj.SubroutineObjs.Add(subroutineObj);

        //					DBContext.GetInstance().SaveChanges();
        //				}

        //				public void RemoveSubroutine(Int32 analysisObjID, Int32 subroutineObjID)
        //				{
        //					var analysisObj = AnalysisObj.GetByID(analysisObjID);

        //					if (analysisObj == null) throw new Exception();

        //					var subroutineObj = analysisObj.SubroutineObjs.Where(_=>_.ID == subroutineObjID).FirstOrDefault();

        //					if (subroutineObj == null) throw new Exception();

        //					analysisObj.SubroutineObjs.Remove(subroutineObj);

        //					DBContext.GetInstance().SaveChanges();
        //				}

        //				//public static void Update(AnalysisObj analysisObj)
        //				//{
        //					//db.AnalysisObj.Local.Add(analysisObj);
        //					//db.AnalysisObj.Attach(analysisObj);

        //					//var s = db.ChangeTracker.Entries<AnalysisObj>().Single(_=>_.Entity == analysisObj).State;

        //					//db.ChangeTracker.Entries<AnalysisObj>().Single(_=>_.Entity == analysisObj).State = System.Data.EntityState.Modified;

        //					//var ss = db.ChangeTracker.Entries<AnalysisObj>().Single(_=>_.Entity == analysisObj).State;

        //					//db.ChangeTracker.DetectChanges();

        //					//new System.Data.Objects.ObjectStateManager(db.Database).ChangeObjectState(analysisObj, System.Data.EntityState.Added);

        //					//DBContext.GetInstance().SaveChanges();
        //				//}

        //				public static IEnumerable<AnalysisIdentifier> GetAllIdentifiers()
        //				{
        //try
        //{
        //	return DBContext.GetInstance().AnalysisObj.Select(_=> new AnalysisIdentifier{ID=_.ID, Name=_.Name, DateCreated=_.DateCreated}).ToList();
        //}
        //catch (Exception e)
        //{

        //}

        //return Enumerable.Empty<AnalysisIdentifier>();

        //				}

        //				public static AnalysisObj GetByID(Int32 id)
        //				{
        //					System.Threading.Thread.Sleep(1000);

        //					return new AnalysisObj{ID=1, Name="NewAnalysis", DateCreated=DateTime.Now,};

        //					//var x =
        //					//	DBContext
        //					//	.GetInstance()
        //					//	.AnalysisObj
        //					//	.Include("SubroutineObjs")
        //					//	.Include("SubroutineObjs.Equipment")
        //					//	.Include("EquipmentAssociationProfiles")
        //					//	.Include("EquipmentAssociationProfiles.EquipmentAssociations")
        //					//	.Where(_=>_.ID == id).Single();

        //					//return x;

        //					//var x =  (from ao in DBContext.GetInstance().AnalysisObj.Include("SubroutineObjs") where ao.ID == id select ao).Single();
        //					// //DBContext.GetInstance().AnalysisObj.First(_=> _.ID == id);
        //				}

        #endregion

        #endregion

        #region method

        //public void AddSubroutine(SubroutineObj subroutineObj)
        //{
        //	this.SubroutineObjs.Add(subroutineObj);
        //}

        #endregion

        #region constructor

        public Visu()
        {
        }

        #endregion

        #region property

        [NotMapped]
        public String __type //revise to use base
        {
            get { return "UA.VisuObj"; }
        }

        [Key]
        [Column("ID")]
        public Int32 Id
        {
            set;
            get;
        }

        [Required]
        [MaxLength(50)]
        public String Name
        {
            get;
            set;
        }

        [MaxLength(100)]
        public String Description
        {
            get;
            set;
        }

        [Required]
        [Column("DateCreated")]
        public DateTime Created
        {
            set;
            get;
        }

        [Required]
        [Column("DateModified")]
        public DateTime Modified
        {
            set;
            get;
        }

        [Required]
        [Column("CID")]
        public Int32 ClientId
        {
            set;
            get;
        }

        [Required]
        [Column("OwnerID")]
        public Int32 OwnerId
        {
            set;
            get;
        }

        [Required]
        public String Plots
        {
            set;
            get;
        }

        [Column("XPlotID")]
        public Int32? XPlotId
        {
            set;
            get;
        }

        [Required]
        public Boolean SharedRead
        {
            set;
            get;
        }

        [Required]
        public Boolean SharedWrite
        {
            set;
            get;
        }

        [MaxLength(255)]
        public String Notes
        {
            set;
            get;
        }

        [Required]
        public Int16 ViewMode
        {
            set;
            get;
        }

        //public Int32 MainSubroutineObjID
        //{
        //	set;
        //	get;
        //}

        //public String UserVariables
        //{
        //	set;
        //	get;
        //}

        //

        //public IList<SubroutineObj> SubroutineObjs
        //{
        //	set;
        //	get;
        //}

        //public IList<EquipmentAssociationProfile> EquipmentAssociationProfiles
        //{
        //	set;
        //	get;
        //}

        #endregion
    }


    public static class DynamicJsonObject_
    {
        public static Boolean RemoveMember(this JObject obj, params String[] name)
        {
            var dic = (IDictionary<String, Object>)((Object)obj).GetType().GetField("_values", (BindingFlags.Instance | BindingFlags.NonPublic)).GetValue(obj);

            foreach (var m in name)
            {
                dic.Remove(m);
            }

            return true;
        }
    }

    public class FractionalSecondsConverter : JsonConverter
    {

        private static readonly JsonConverter Converter = new IsoDateTimeConverter { DateTimeFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fff'Z'" };

        public override bool CanRead
        {
            get
            {
                return false; // Deny read so incoming DateTime's can have more fractional seconds
            }
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var date = (DateTime)value;
            var json = JsonConvert.SerializeObject(date, Converter);
            writer.WriteRawValue(json);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException("Unnecessary because CanRead is false. The type will skip the converter.");
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }
    }

}