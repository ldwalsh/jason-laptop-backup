﻿using System;
using JSAsset;
using CW.Website.DependencyResolution;
using CW.Common.Config;

namespace CW.Website.AnalysisBuilder
{
	public class UserAnalysisAsset: EmbeddedAssetBase
	{
		public override String Name
		{
			get {return "UA";}
		}

		public override String RelativePath
		{
			get {return "AnalysisBuilder/script/";}
		}

		public override String[] Dependencies
		{
			get {return new[]{"iCL"};}
		}

        public override String InitFunction
        {
            get {return "loadIDE";}
        }

        public override Int64 Version
        {
            get {return Int64.Parse(IoC.Resolve<IConfigManager>().GetConfigurationSetting("versionNumber", new DateTime().Ticks.ToString()).Replace(".", String.Empty));}
        }
	}
}