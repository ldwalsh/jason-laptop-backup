﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
	public class GreaterThanFunction: FunctionObjBase
	{
        public GreaterThanFunction()
        {
			Name		= "greaterThan";
            Category    = FunctionCategoryEnum.Numeric;
            Symbol      = ">";
            Label       = "GreaterThan";
            Description = "Returns true if the left hand number is grater than the right.";
            Inputs      = new[]{new[]{DataTypeEnum.Number, DataTypeEnum.Number}};
            Output      = DataTypeEnum.Boolean;
        }
	}
}