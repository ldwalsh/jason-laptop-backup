﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
    public class ExponentFunction: FunctionObjBase
    {
        public ExponentFunction()
        {
			Name		= "exp";
            Category    = FunctionCategoryEnum.Operator;
            Symbol      = "^";
            Label       = "Exponent";
            Description = "Exponents stuff";
            Inputs      = new[]{new[]{DataTypeEnum.Number}};
            Output      = DataTypeEnum.Number;
        }
    }
}