﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
    public class MinContinousDurationFunction: FunctionObjBase
    {
        public MinContinousDurationFunction()
        {
			Name		= "MinContinousDurationFunction";
            Category    = FunctionCategoryEnum.Physics;
            Symbol      = "F";
            Label       = "Min Continous Duration Function";
            Description = "Adds stuff";
            Inputs      = new[]{new[]{DataTypeEnum.Number}, new[]{DataTypeEnum.Number}};
            Output      = DataTypeEnum.Number;
        }
    }
}