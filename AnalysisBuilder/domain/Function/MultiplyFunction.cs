﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
    public class MultiplyFunction: FunctionObjBase
    {
        public MultiplyFunction()
        {
			Name		= "multiply";
            Category    = FunctionCategoryEnum.Operator;
            Symbol      = "×";
            Label       = "Multiply";
            Description = "Multiplies ...";
            Inputs      = new[]{new[]{DataTypeEnum.Number}, new[]{DataTypeEnum.Number}};
            Output      = DataTypeEnum.Number;
        }
    }
}