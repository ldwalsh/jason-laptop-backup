﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
    public class SubtractFunction: FunctionObjBase
    {
        public SubtractFunction()
        {
			Name		= "subtract";
            Category    = FunctionCategoryEnum.Operator;
            Symbol      = "-";
            Label       = "Subtraction";
            Description = "Subtract stuff";
            Inputs      = new[]{new[]{DataTypeEnum.Number}, new[]{DataTypeEnum.Number}};
            Output      = DataTypeEnum.Number;
        }
    }
}