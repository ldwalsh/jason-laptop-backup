﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
    public class AddFunction: FunctionObjBase
    {
		public AddFunction()
		{
			Name		= "add";
			Category    = FunctionCategoryEnum.Operator;
			Symbol      = "+";
			Label       = "Addition";
			Description = "Adds stuff";
			Inputs      = new[]{new[]{DataTypeEnum.Number}, new[]{DataTypeEnum.Number}};
			Output      = DataTypeEnum.Number;
		}
    }
}