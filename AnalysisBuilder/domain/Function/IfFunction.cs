﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
	public class IfFunction: FunctionObjBase
	{
        public IfFunction()
        {
			Name		= "if";
            Category    = FunctionCategoryEnum.Conditional;
            Symbol      = "";
            Label       = "If";
            Description = "";
            Inputs      = new[]{new[]{DataTypeEnum.Number}, new[]{DataTypeEnum.Number}};
            Output      = DataTypeEnum.Number;
        }
	}
}