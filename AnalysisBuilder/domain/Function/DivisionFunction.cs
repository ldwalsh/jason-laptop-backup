﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
    public class DivisionFunction: FunctionObjBase
    {
        public DivisionFunction()
        {
			Name		= "divide";
            Category    = FunctionCategoryEnum.Operator;
            Symbol      = "÷";
            Label       = "Division";
            Description = "Divides";
            Inputs      = new[]{new[]{DataTypeEnum.Number}, new[]{DataTypeEnum.Number}};
            Output      = DataTypeEnum.Number;
        }
    }
}