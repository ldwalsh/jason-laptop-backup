﻿
namespace CW.Website.AnalysisBuilder.Domain.Function
{
	public class LessThanFunction: FunctionObjBase
	{
        public LessThanFunction()
        {
			Name		= "lessThan";
            Category    = FunctionCategoryEnum.Numeric;
            Symbol      = "<";
            Label       = "LessThan";
            Description = "Returns true if the left hand number is grater than the right.";
            Inputs      = new[]{new[]{DataTypeEnum.Number, DataTypeEnum.Number}};
            Output      = DataTypeEnum.Boolean;
        }
	}
}