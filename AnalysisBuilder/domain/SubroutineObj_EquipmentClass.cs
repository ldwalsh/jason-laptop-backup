﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Script.Serialization;
using System.Linq;

namespace CW.Website.UserAnalysisBuilder.Domain
{
	public class SubroutineObj_EquipmentClass
	{
		#region field

			private DateTime dateCreated;

		#endregion

		#region property

			[
			NotMapped
			]
			public String __type
			{
				get {return "UA.SubroutineObj_EquipmentClass";}
			}

			[
			Key
			]
            public Int32 ID
            {
                set;
                get;
            }

			[
			Required
			]
            public DateTime DateCreated
            {
                set
				{
					dateCreated = value;
				}
                get
				{
					if (dateCreated == DateTime.MinValue) return DateTime.Now;

					return dateCreated;
				}
            }
			
			[
			Required,
			ScriptIgnore,
			]			
			public SubroutineObj SubroutineObj
            {
                set;
                get;
            }

			[
			Required,
			]
			public Int32 EquipmentClassID
            {
                set;
                get;
            }

			[
			Required,
			]
			public String Alias
			{
				get;
				set;
			}

		#endregion
	}
}