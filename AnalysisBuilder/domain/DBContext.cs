﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web;

namespace CW.Website.AnalysisBuilder.Domain
{
    public class DBContext: DbContext
    {
		public static DBContext GetInstance()
		{
			var dbContext = HttpContext.Current.Items["dbContext"];
			
			if (dbContext != null) return (DBContext)dbContext;

			try
			{
				dbContext = new DBContext();
			}
			catch (Exception)
			{
				throw;
			}
			
			HttpContext.Current.Items.Add("dbContext", dbContext);

			return (DBContext)dbContext;
		}

		private DBContext(): base("CWConnectionString") //CWConnectionString //DBContext
		{
			//((IObjectContextAdapter)this).ObjectContext.ContextOptions.ProxyCreationEnabled = false; 

			this.Configuration.LazyLoadingEnabled = false;

		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			Database.SetInitializer<DBContext>(null); // <--- This is what i needed

			//Database.SetInitializer<DBContext>(new CreateDatabaseIfNotExists<DBContext>());

			base.OnModelCreating(modelBuilder);
		}

		public DbSet<Visu> Visulizations {get; set;}

		//public DbSet<AnalysisObj>					AnalysisObj						{get; set;}
		//public DbSet<DWInfo>						DWInfo							{get; set;}
		//public DbSet<EquipmentAssociation>			EquipmentAssociation			{get; set;}
		//public DbSet<EquipmentAssociationProfile>	EquipmentAssociationProfile		{get; set;}
		//public DbSet<SubroutineObj>					SubroutineObj					{get; set;}
		//public DbSet<SubroutineObj_EquipmentClass>	SubroutineObj_EquipmentClasses	{get; set;}
    }
}