using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using CW.Website.AnalysisBuilder.FileGenerators;
using System;
using System.IO;
using System.Web;
using System.Web.SessionState;

namespace CW.Website.AnalysisBuilder.handler
{
	public class ExportHandler: IHttpHandler, IReadOnlySessionState
	{
		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			var fileName = context.Request.Form["fileName"];

            new FilePublishService(getGenerator(fileName)).AttachAsDownload(new GenericResponseStreamAttacher(), fileName);
		}

		private IFileGenerator getGenerator(String fileName)
		{
			switch (new FileInfo(fileName).Extension)
			{
				case ".csv": return Activator.CreateInstance<CSVGenerator>();
				case ".pdf": return Activator.CreateInstance<PDFGenerator>();

				default: throw new NotImplementedException();
			}
		}

		Boolean IHttpHandler.IsReusable
		{
			get {return false;}
		}
	}
}
