﻿///
///<reference path="../../_analysisBuilder/script/$UA.js" />

function construct_Expression_variablesRecognized()
{
	function t(expr, vars)
	{
		var e = new UA.expression.Expression(expr)

		assert.areEqual(vars.length, e.variables.length)
		
		vars		= vars.sort()
		e.variables = e.variables.sort()

		for (var i=0; i<vars.length; i++)
		{
			assert.areEqual(vars[i], e.variables[i])
		}
	}

	t("a", ["a"])
	t("+a", ["a"])
	t("-a", ["a"])

	t("(b-a)", ["a", "b"])
	t("(b-(a+b))", ["a","b"])
	t("(b-(a+c))", ["a","b", "c"])

	t("a > 50 | a<10", ["a"])
	t("z > 50 | y<10", ["y","z"])

	t("sum(a, b)", ["a","b"])
	t("sum(a, b, sum(a, z))", ["a","b","z"])

	t("sum(a, b, (1-c))", ["a","b","c"])
}