﻿using CW.Website.UserAnalysisBuilder;
using iCLib;
using jsTestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UserAnalysisBuilder._analysisBuilderTest.expression
{
	[
	TestClass
	]
	public class ExpressionTreeNodeTest: JSTestClassBase
	{
		public ExpressionTreeNodeTest(): base("_analysisBuilderTest\\expression\\")
		{
			JS.AddCode(ScriptIncluder.GetTestingInstance().AddAll().GetSource());			
			JS.AddCode(new UserAnalysisAsset());

			JS.Generate();
		}

		[
		TestMethod
		]
		public void evaluate_ValidTreeNode_evaluated()
		{
			Assert.IsTrue(JS.Invoke());
		}

		[
		TestMethod, 
		]
		public void evaluate_InvalidTreeNode_throwError()
		{
			Assert.IsTrue(JS.Invoke());
		}
	}
}