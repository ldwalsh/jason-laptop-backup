﻿using CW.Website.UserAnalysisBuilder;
using iCLib;
using jsTestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.UserAnalysisBuilder._analysisBuilderTest.expression
{
	[
	TestClass
	]
	public class ExpressionTest: JSTestClassBase
	{
		public ExpressionTest(): base("_analysisBuilderTest\\expression\\")
		{
			JS.AddCode(ScriptIncluder.GetTestingInstance().AddAll().GetSource());			
			JS.AddCode(new UserAnalysisAsset());

			JS.Generate();
		}

		[
		TestMethod
		]
		public void construct_Expression_variablesRecognized()
		{
			Assert.IsTrue(JS.Invoke());
		}
	}
}