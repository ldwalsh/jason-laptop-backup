﻿///
///<reference path="../../_analysisBuilder/script/$UA.js" />

function construct_fragment_fromValidExpressionString()
{
	function t(expr, value, operator, isFunctionCall, isGrouping, canEvaluate)
	{
		var ef = new UA.expression.ExpressionFragment(expr)

		assert.areEqual(value, ef.value)
		assert.areEqual(operator, ef.operator)

		assert.areEqual(isFunctionCall, ef.isFunctionCall)
		assert.areEqual(isGrouping, ef.isGrouping)
		assert.areEqual(canEvaluate, ef.canEvaluate)
	}

	t("/3", "3", "/",		false, false, true)
	t("+2", "2", "+",		false, false, true)
	t(" + 2 ", "2", "+",	false, false, true)
	t("-2", "-2", "+",		false, false, true)
	t(" - 2 ", "-2", "+",	false, false, true)
	t("-+2", "2", "-",		false, false, true)
	t("+- 2 ", "-2", "+",	false, false, true)
	t("/3", "3", "/",		false, false, true)
	t(" / 3 ", "3", "/",	false, false, true)
	t("/-5", "-5", "/",		false, false, true)
	t("/+5", "5", "/",		false, false, true)
	
	t("(1+2 + z)", "(1+2+z)", "+",		false, true, false)
	t(" + (1+2 + z)", "(1+2+z)", "+",	false, true, false)
	t(" - (1+2 + z)", "-(1+2+z)", "+",	false, true, false) //?

	t("sum(1, 2,3   )", "sum(1,2,3)", "+",			true, false, false)
	t("-sum(a,1)", "-sum(a,1)", "+",				true, false, false)
	t("someFunction(a)", "someFunction(a)", "+",	true, false, false)

	t("(sum( 1, x ) - 2)", "(sum(1,x)-2)", "+",		false, true, false)
}

function construct_error_invalidExpressionString()
{
	function t(expr, errCstr)
	{
		assert.throws(function(){new UA.expression.ExpressionFragment(expr)}, errCstr)
	}

	t("//5", {message:"Sign must be a negative or positive"})
	t("((1+1)", {message:"Missing matching brace"})
	t("1---1", {message:"Identifier expected instead of sign"})
}

function z()
{
	function t(expr, args)
	{
		var f = new UA.expression.ExpressionFragment(expr)

		assert.areElementsEqual(args, f.getArguments())
	}
	
	t("sum(a, b, c)", ["a", "b", "c",])
	t("sum(a, b, sum(a,c))", ["a", "b", "sum(a,c)",])
}
