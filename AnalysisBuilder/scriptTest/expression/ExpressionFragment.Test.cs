﻿using CW.Website.UserAnalysisBuilder;
using iCLib;
using jsTestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UserAnalysisBuilder._analysisBuilderTest.expression
{
	[
	TestClass
	]
	public class ExpressionFragmentTest: JSTestClassBase
	{
		public ExpressionFragmentTest(): base("_analysisBuilderTest\\expression\\")
		{
			JS.AddCode(ScriptIncluder.GetTestingInstance().AddAll().GetSource());			
			JS.AddCode(new UserAnalysisAsset());

			JS.Generate();
		}

		[
		TestMethod
		]
		public void construct_fragment_fromValidExpressionString()
		{
			Assert.IsTrue(JS.Invoke());
		}

		[
		TestMethod //, ExpectedException(typeof(Exception))
		]
		public void construct_error_invalidExpressionString()
		{
			Assert.IsTrue(JS.Invoke());
		}

		[
		TestMethod
		]
		public void z()
		{
			Assert.IsTrue(JS.Invoke());
		}
	}
}