﻿///
///<reference assembly="jsTestHost" name="jsTestHost.script.UT.js" />
///<reference path="../../_analysisBuilder/script/$UA.js" />


function evaluate_ValidTreeNode_evaluated()
{
	function t(expr, expected, args)
	{
		var ef = new UA.expression.ExpressionFragment(expr)
		var tn = new UA.expression.ExpressionTreeNode(ef)

		var val = tn.evaluate(args)

		assert.areEqual(expected, val)
	}

	//
	//numeric constant

	t("5", 5)
	t("-5", -5)

	//
	//addition/subtraction

	t("2+3", 5)
	t("2 + 3", 5)
	t("3-2", 1)
	t("2-3", -1)
	t("2 - 3", -1)

	//
	//decimal point
	
	t("2.0 + 3.0", 5)
	t("2.0 + 3.0-1.5", 3.5)

	//
	//multiple operations

	t(" 4/ 2+2", 4)
	t(" 4*2 -2", 6) 
	t(" 4*2 +-2", 6)	
	t(" 4*2+2", 10)
	t("4/2/1*2", 4)

	//
	//grouping

	t("(1 + 1)", 2)
	t("(1 - 1)", 0)
	t("-(1 + 1)", -2)
	t("-(1 - 2)", 1)
	t("-(1 - 1)", 0)
	t("(2 - (1 - 1))", 2)
	t("8 / (1+3)", 2)

	//
	//operation with sign

	t("1+2+-3", 0)
	t("+2 - 3+1-1", -1)
	t("+2 - 3", -1)
	t("-2 - 3", -5)
	t("-2 + 2", 0)
	t("+10/2", 5)
	
	//
	//division

	t("5/-2", -2.5)
	t("-5/2", -2.5)
	t("-5/-2", 2.5)

	//
	//function call

	t("sum(1,2,3)", 6)
	t("product(-1, 4)", -4)
	t("difference(5,-2)", -3)

	//
	//function addition/subtraction
	
	t("sum(1,2,3) + sum(1,2) /2", 7.5)
	t("sum(1,2,3) + -sum(1,2)", 3)
	t("-sum(1,2,3)  - sum(1,2)", -9)
	t("sum(1,2,3) + sum(1,2)/-2", 4.5)

	//
	//nested function calls

	t("sum(1,2, sum(2,3))", 8)
	t("sum(1,2, sum(2,product(2,2)))", 9)
	t("sum(5,2, product(-2, 1))", 5)
	t("difference(5,-2, max(5,4))", -8)
	t("difference(max(5,4), -min(9,8))", 3)
	
	//
	//nested function and grouping

	t("sum(1,2, sum(2,product(2,(2-1))))", 7)

	//
	//variables

	t("(a+b)", 9, {a:5, b:4})
	t("(a-b)", 1, {a:5, b:4})

	//
	//equality

	t("1=1", true)
	t("(2 = 1)", false)
	t("(2 = 1) + 1", 1)
	t("(1 = 1) + 1", 2)
	t("(2 = 1+1)", true)
	t(" ((1 == 1) -1)", 0)

	//
	//greater/less than

	t("(2 > 1+1)", false)	
	t(" ((1 > 1) -1)", -1)
	t(" (1 < 2)", true)

	//
	//or and operation

	t("a = 1 | a = 2", true, {a:1})
	t("(a = 1 | (a = 2))", true, {a:1})
	t("a=1|(a = 2)", false, {a:5})
	t("(a = 1 & (b = 2))", true, {a:1,b:2})
	t("(a = 1 & (b = 3))", false, {a:1,b:2})
	t("a = 1 || a = 2", true, {a:1})
	t("(a = 1 || (a = 2))", true, {a:1})
	t("a=1||(a = 2)", false, {a:5})
	t("(a == 1 && (b == 2))", true, {a:1,b:2})
	t("(a == 1 && (b == 3))", false, {a:1,b:2})
}

function evaluate_InvalidTreeNode_throwError()
{
	function t(expr, expected, args)
	{
		var ef = new UA.expression.ExpressionFragment(expr)
		var tn = new UA.expression.ExpressionTreeNode(ef)

		tn.evaluate(args)
	}

	assert.throws(function(){t("x", 1, {})}, {message:"Missing argument"})
}

