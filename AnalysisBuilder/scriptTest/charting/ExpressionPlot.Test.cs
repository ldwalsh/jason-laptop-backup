﻿using CW.Website.UserAnalysisBuilder;
using iCLib;
using jsTestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CW.UserAnalysisBuilder._analysisBuilderTest.charting
{
	[
	TestClass
	]
	public class ExpressionPlotTest: JSTestClassBase
	{
		public ExpressionPlotTest(): base("_analysisBuilderTest\\charting\\")
		{
			JS.AddCode(ScriptIncluder.GetTestingInstance().AddAll().GetSource());			
			JS.AddCode(new UserAnalysisAsset());

			JS.Generate();
		}

		[
		TestMethod
		]
		public void x()
		{
			Assert.IsTrue(JS.Invoke());
		}
	}
}