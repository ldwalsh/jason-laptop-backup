﻿using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._framework;
using EO.Pdf;
using System;
using System.Drawing;
using System.Web;
using CW.Utility.Web;

namespace CW.Website.AnalysisBuilder.FileGenerators
{
    public sealed class PDFGenerator: IFileGenerator
    {
		public AllowanceTypeEnum AllowanceType
		{
			get {return AllowanceTypeEnum.ModuleDownload;}
		}

		public Byte[] Generate()
		{
			var eoh = new EssentialObjectsHelper(null, null);
            bool IsSchneiderTheme = SiteUser.Current.IsSchneiderTheme;
            eoh.CreatePdfHeader(IsSchneiderTheme, .7);
            eoh.CreatePdfFooter(IsSchneiderTheme, "Analysis Builder Report", .7);

			return eoh.ConvertHtmlAndReturnBytes(HttpContext.Current.Request.Form["content"]);
		}

		public Byte[] Generate(out String fileName)
		{
			throw new NotImplementedException();
		}
    }
}