﻿using CW.Reporting._fileservices;
using System;
using System.Text;
using System.Web;

namespace CW.Website.AnalysisBuilder.FileGenerators
{
    public sealed class CSVGenerator: IFileGenerator
    {
		public AllowanceTypeEnum AllowanceType
		{
			get {return AllowanceTypeEnum.ModuleDownload;}
		}

		public Byte[] Generate()
		{
			var str = HttpContext.Current.Request.Form["content"].Replace("|", "\n");

			//var bytes = new Byte[str.Length * sizeof(Char)];
			//Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
			//return bytes;

			return Encoding.UTF8.GetBytes(str.ToString());
		}

		public Byte[] Generate(out String fileName)
		{
			throw new NotImplementedException();
		}
    }
}