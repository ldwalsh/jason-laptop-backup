﻿using CW.Business;
using CW.Common.Config;
using CW.Common.Constants;
using CW.Data.Collections;
using CW.Data.Models;
using CW.Website._framework;
using CW.Website.DependencyResolution;
using System;
using System.Reflection;
using System.Web;

namespace CW.Website.AnalysisBuilder.Service
{
	public class ServiceBase
	{
		#region STATIC

            public static void SetCachingPolicy(HttpCachePolicy cache, Int32 minutes)
			{
				cache.SetCacheability(HttpCacheability.Private);

                cache.GetType().GetField("_maxAge", (BindingFlags.Instance | BindingFlags.NonPublic)).SetValue(cache, new TimeSpan(0, 0, minutes, 0, 0));

				//cache.SetExpires(DateTime.Now.AddYears(1));
			}

            public static void SetCachingPolicy(Int32 minutes)
            {
                SetCachingPolicy(HttpContext.Current.Response.Cache, minutes);
            }

		#endregion

		#region field
		
			private DataManager _dataManager;
			private SiteUser    _siteUser;

		#endregion

		#region method

			protected DataManager GetOrgSpecificDataManager()
			{
				return GetOrgSpecificDataManager(siteUser.ClientOID);
			}
		
			protected DataManager GetOrgSpecificDataManager(Int32 oid)
			{
                dataManager.SetOrgBasedStorageAccounts
				(
					new StorageAccountCriteria
						{
							OID              = siteUser.ClientOID,
							IsCommon         = false,
							CWEnvironment    = EnvironmentType.FromValue<EnvironmentType>(IoC.Resolve<IConfigManager>().GetConfigurationSetting(DataConstants.ConfigKeyCWEnv, "", true)),
							StorageAcctLevel = StorageAccountLevel.Primary
						}
				);

				return dataManager;
			}

		#endregion

		#region property

			protected DataManager dataManager
			{
				get {return (_dataManager = (_dataManager ?? DataManager.Get(httpContext.Session.SessionID)));}
			}

            protected SiteUser siteUser
            {
                get {return (_siteUser = (_siteUser ?? SiteUser.Current));}
            }

            protected HttpContext httpContext
            {
                get {return HttpContext.Current;}
            }

		#endregion
	}
}