﻿using CW.Business.Query;
using CW.Data;
using CW.Website.AnalysisBuilder.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.AnalysisBuilder.Service
{
    public class PointSvc: ServiceBase
    {
        private static Domain.Point ConvertToPoint(Data.Point point)
        {
            var pointClass = point.PointType.PointClass;

            return
            new Domain.Point
            {
                Id        = point.PID,
                ClassId   = pointClass.PointClassID,
                TypeId    = point.PointTypeID,
                EngUnitId = (point.Equipment_Points.First().Equipment.Building.BuildingSettings.First().UnitSystem ? pointClass.SIEngUnitID : pointClass.IPEngUnitID),
                Name      = point.PointName,
                Sampling  = point.SamplingInterval,
            };
        }

        public IEnumerable<Domain.Point> GetByEquipmentId(Int32 equipId)
        {
            return
            new PointQuery(dataManager.ConnectionString, siteUser) //credentials not actually used for security yet					
                .LoadWith<PointType>(_ => _.PointType, _ => _.PointClass)
                .LoadWith<Equipment_Point, Data.Equipment, Data.Building>(_ => _.Equipment_Points, _ => _.Equipment, _ => _.Building, _ => _.BuildingSettings)
                .FinalizeLoadWith
                .GetVisibleOnly()
                .ByEquipment(equipId)
                .ToList()
                .Select(ConvertToPoint);
        }

        public IEnumerable<PointData> GetData(Int32 oid, Int32[] pointIds, String from, String to)
        {
            var points =
                dataManager.RawDataMapper.GetConvertedRawPlotDataForPoints(pointIds.ToList(), DateTime.Parse(from), DateTime.Parse(to).AddDays(1))
                 .Select(_ => new PointData{PointId=_.PID, Logged=("d-" + _.DateLoggedLocal), Value=_.ConvertedRawValue}); //.Distinct(new PointData.Comparer());

            ServiceBase.SetCachingPolicy(Int32.Parse(httpContext.Request.Headers["App-CacheFor"]));

            ////for debugging/dev... to be removed
            //var c1 = points.Count();
            
            //var p = points.Distinct(new PointData.Comparer());

            //if (c1 != p.Count())
            //{
            //    var x = 2;
            //}
            ////

            return points;
        }
    }
}