﻿using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;

namespace CW.Website.AnalysisBuilder.Service
{
    [
    ScriptService
    ]
    public class FunctionService: WebService
    {
		private readonly FunctionSvc svc = new FunctionSvc();

		[
        WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public IEnumerable<Object> GetEngUnits()
		{
			return svc.GetEngUnits();
		}

        //[
        //WebMethod(true)
        //]
        //public FunctionObjBase[] GetFunctionTypes()
        //{
        //    var functions =
        //        new FunctionObjBase[]
        //            {
        //                new AddFunction(),
        //                new DivisionFunction(),
        //                new ExponentFunction(),
        //                new GreaterThanFunction(),
        //                new IfFunction(),
        //                new LessThanFunction(),
        //                new MinContinousDurationFunction(),
        //                new MultiplyFunction(),
        //                new SubtractFunction(),
        //            };

        //    var r = new List<FunctionObjBase>();

        //    foreach (var f in functions)
        //    {
        //        r.Add(f);
        //    }

        //    return r.ToArray();
        //}
    }
}
