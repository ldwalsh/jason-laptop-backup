﻿using CW.Website._framework;
using CW.Website.AnalysisBuilder.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.AnalysisBuilder.Service
{
	public class BuildingSvc
	{
		public IEnumerable<Building> GetForCurrentClient()
		{
            return SiteUser.Current.VisibleBuildings.Select
                (_=>new Building{BuildingId=_.BID, ClientId=_.CID, Name=_.BuildingName, /*TzOffset=TimeZoneInfo.FindSystemTimeZoneById(_.TimeZone).BaseUtcOffset.Hours*/});
        }
	}
}