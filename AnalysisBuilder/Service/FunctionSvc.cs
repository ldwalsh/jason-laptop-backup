﻿using CW.Business;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.AnalysisBuilder.Service
{
	public class FunctionSvc: ServiceBase
	{
		public IEnumerable<Object> GetEngUnits()
		{
			//return new DataManagerSQL().EngineeringUnitsDataMapper.GetAllEngineeringUnits().Select(_=> new{__type="UA.EngUnit", EngUnitId=_.EngUnitID, Name=_.EngUnits});
			return GetOrgSpecificDataManager().EngineeringUnitsDataMapper.GetAllEngineeringUnits().Select(_=> new{__type="UA.EngUnit", EngUnitId=_.EngUnitID, Name=_.EngUnits});
		}
	}
}