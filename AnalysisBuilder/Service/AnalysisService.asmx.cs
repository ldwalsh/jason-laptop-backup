﻿using CW.Website.AnalysisBuilder.Domain;
using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;

namespace CW.Website.AnalysisBuilder.Service
{
    [
    ScriptService
    ]
    public class AnalysisService: WebService
    {
		private readonly AnalysisSvc svc = new AnalysisSvc();

		//[
		//WebMethod(true)
		//]
		//public AnalysisObj Create(String name, String plots, Boolean isPrivate)
		//{
		//	return AnalysisObj.Create(name, plots, isPrivate);
		//}

		[
		WebMethod(true)
		]
		public Int32 Save(Visu visu)
		{
			return svc.Save(visu);
		}

		[
		WebMethod(true)
		]
		public void SaveProperties(VisuProperties visuProps)
		{
			svc.SaveProperties(visuProps);
		}

		[
		WebMethod(true),
		]
		public void Delete(Int32 id)
		{
			svc.Delete(id);
		}

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public IEnumerable<Object> GetAllIdentifiers()
		{
			return svc.GetAllIdentifiers();
		}

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public Visu GetById(Int32 id)
		{
			return svc.GetById(id);
		}




		//[
		//WebMethod(true)
		//]
		//public IEnumerable<AnalysisIdentifier> GetAllIdentifiers()
		//{
		//	return AnalysisObj.GetAllIdentifiers();
		//}

		//[
		//WebMethod(true)
		//]
		//public AnalysisObj Create(String name)
		//{
		//	return AnalysisObj.Create(name);
		//}

		//[
		//WebMethod(true)
		//]
		//public AnalysisObj GetByID(Int32 id)
		//{
		//	return AnalysisObj.GetByID(id);
		//}

		//[
		//WebMethod
		//]
		//public Boolean SaveAnalysisObj(UpdateObj updateObj)
		//{
		//	var ao = AnalysisObj.GetByID(updateObj.id);

		//	foreach (var property in updateObj.properties)
		//	{
		//		var p = ao.GetType().GetProperty(property.Key);

		//			p.SetValue(ao, property.Value, null);
		//	}
			
		//	AnalysisObj.Update(ao);

		//	return true;
		//}

		//[
		//WebMethod(true)
		//]
		//public SubroutineObj AddSubroutine(Int32 analysisObjId, String subroutineName)
		//{
		//	return AnalysisObj.AddSubroutine(analysisObjId, subroutineName);
		//}

		//[
		//WebMethod(true)
		//]
		//public void ImportSubroutine(Int32 analysisObjID, Int32 subroutineObjID)
		//{
		//	AnalysisObj.ImportSubroutine(analysisObjID, subroutineObjID);
		//}

		//[
		//WebMethod(true)
		//]
		//public void RemoveSubroutine(Int32 analysisObjID, Int32 subroutineObjID)
		//{
		//	var analysisObj = AnalysisObj.GetByID(analysisObjID);

		//	if (analysisObj == null) throw new Exception();

		//	var subroutineObj = analysisObj.SubroutineObjs.Where(_=>_.ID == subroutineObjID).FirstOrDefault();

		//	if (subroutineObj == null) throw new Exception();

		//	analysisObj.SubroutineObjs.Remove(subroutineObj);

		//	DBContext.GetInstance().SaveChanges();
		//}

		////
		////
		////
		////
		////

		//[
		//WebMethod(true)
		//]
		//public SubroutineObj[] SubroutineObj_GetByAnalysisObjID(Int32 analysisObjId)
		//{
		//	return SubroutineObj.GetByAnalysisObjID(analysisObjId);
		//}

		//[
		//WebMethod(true)
		//]
		//public SubroutineObj[] SubroutineObj_GetAll()
		//{
		//	return SubroutineObj.GetAll();
		//}

		//[
		//WebMethod(true)
		//]
		//public SubroutineObj SubroutineObj_GetByID(Int32 id)
		//{
		//	return SubroutineObj.GetByID(id);
		//}

		//[
		//WebMethod(true)
		//]
		//public SubroutineObj SubroutineObj_Create(String name)
		//{
		//	return SubroutineObj.Create(name);
		//}

		//[
		//WebMethod(true)
		//]
		//public void SubroutineObj_UpdateEquipment(Int32 subroutineObjID, SubroutineObj_EquipmentClass[] equipment)
		//{
			
		//}

		////
		////
		////
		////
		////

		//[
		//WebMethod(true)
		//]
		//public Boolean Update(String objType, Int32 objID, IDictionary<String,Object> objProps)
		//{
		//	var type = Type.GetType(objType.Replace("UA.", "UserAnalysisBuilder.domain."));

		//	var GetByID = type.GetMethod("GetByID", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

		//	var obj = GetByID.Invoke(null, new Object[]{objID});

		//	foreach (var prop in objProps)
		//	{
		//		var propName = prop.Key[0].ToString().ToUpper() + prop.Key.Substring(1);

		//		var p = type.GetProperty(propName);

		//		if (prop.Value is System.Collections.ICollection)
		//		{
		//			var c = new Conv();

		//			var o = c.Deserialize((System.Collections.Generic.IDictionary<String,Object>)((Object[])prop.Value)[1], typeof(SubroutineObj_EquipmentClass), new System.Web.Script.Serialization.JavaScriptSerializer());

		//			//((SubroutineObj)obj).Equipment = new[]{(SubroutineObj_EquipmentClass)o};

		//			((SubroutineObj)obj).Equipment = null;
					
					
					

		//			//.Add((SubroutineObj_EquipmentClass)o);

					
					
		//			//var s = new System.Web.Script.Serialization.JavaScriptConverter();

		//			//var x = s.Deserialize(((System.Collections.Generic.IDictionary<String,Object>)prop.Value), typeof(SubroutineObj_EquipmentClass), new System.Web.Script.Serialization.JavaScriptSerializer());




		//		}
		//		else
		//		{
		//			p.SetValue(obj, prop.Value, null);
		//		}
		//	}

		//	//var Update = updateObj.Type.GetMethod("Update", System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);

		//	//	Update.Invoke(null, new Object[]{obj});

		//	try
		//	{
		//		DBContext.GetInstance().SaveChanges();
		//	}
		//	catch (Exception e)
		//	{
		//		throw;
		//	}

		//	return true;
		//}
    }
}

//public class Conv: System.Web.Script.Serialization.JavaScriptConverter
//{
//	public override object Deserialize(IDictionary<String,Object> dictionary, Type type, System.Web.Script.Serialization.JavaScriptSerializer serializer)
//	{
//		if (type == typeof(SubroutineObj_EquipmentClass))
//		{
//			return
//			new SubroutineObj_EquipmentClass
//				{
//					//ID				 = 1,
//					EquipmentClassID = System.Convert.ToInt32(dictionary["equipmentClassID"]),
//					Alias			 = System.Convert.ToString(dictionary["alias"]),
//				};


//		}

//		throw new Exception();
//	}

//	public override IDictionary<String,Object> Serialize(Object obj, System.Web.Script.Serialization.JavaScriptSerializer serializer)
//	{
//		return null;
//	}

//	public override IEnumerable<Type> SupportedTypes
//	{
//		get
//		{
//			return null;
//		}
//	}
//}
