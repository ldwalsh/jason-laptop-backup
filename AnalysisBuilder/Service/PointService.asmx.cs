﻿using CW.Website.AnalysisBuilder.Domain;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

namespace CW.Website.AnalysisBuilder.Service
{
	[
	ScriptService
	]
	public class PointService: WebService
	{
		private readonly PointSvc svc = new PointSvc();

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public IEnumerable<Domain.Point> GetByEquipmentId(Int32 equipId)
		{
			return svc.GetByEquipmentId(equipId);
		}

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public IEnumerable<PointData> GetData(Int32 oid, Int32[] pointIds, String from, String to)
		{
            //if (cacheFor > 0)
            //{
            //    var cache = HttpContext.Current.Response.Cache;

            //    cache.GetType().GetField("_maxAge", (BindingFlags.Instance | BindingFlags.NonPublic)).SetValue(cache, new TimeSpan(0, cacheFor, 0));
            //}

            return svc.GetData(oid, pointIds, from, to);
		}
	}
}
