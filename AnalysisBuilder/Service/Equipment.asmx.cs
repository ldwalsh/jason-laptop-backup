﻿using CW.Website.AnalysisBuilder.domain;
using System;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;

namespace CW.Website.AnalysisBuilder.Service
{
	[
	ScriptService
	]
	public class Equipment: WebService
	{
		private readonly EquipmentSvc svc = new EquipmentSvc();

		[
        WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
        public IEnumerable<EquipmentClass> GetClasses()
        {
            return svc.GetClasses();
        }

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public IEnumerable<EquipmentClass> GetClassesByBuildingId(Int32 buildingId)
		{
			return svc.GetClassesByBuildingId(buildingId);
		}

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public IEnumerable<EquipmentObj> GetByBuildingAndClass(Int32 bid, Int32 classID)
		{
			return svc.GetByBuildingAndClass(bid, classID);
		}

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public IEnumerable<EquipmentObj> GetAssociatedEquipmentByEquipment(Int32 eid)
		{
			return svc.GetAssociatedEquipmentByEquipment(eid);
		}

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public EquipmentVariableValue GetVariableValue(Int32 eid, Int32 evid)
		{
			return svc.GetVariableValue(eid, evid);
		}

		//[
		//WebMethod
		//]
		//public EquipmentAssociationProfile CreateEquipmentAssociationProfile(Int32 analysisObjId, String name)
		//{
		//	return EquipmentAssociationProfile.Create(analysisObjId, name);
		//}

		//[
		//WebMethod
		//]
		//public EquipmentAssociation CreateEquipmentAssociation(Int32 profileId, String alias, Int32 equipmentId)
		//{
		//	return EquipmentAssociation.Create(profileId, alias, equipmentId);
		//}

		//[
		//WebMethod
		//]
		//public EquipmentAssociation UpdateEquipmentAssociation(Int32 associationId, String alias, Int32 equipmentId)
		//{
		//	return EquipmentAssociation.Update(associationId, alias, equipmentId);
		//}

		//[
		//WebMethod
		//]
		//public void DeleteEquipmentAssociation(Int32 associationId)
		//{
		//	EquipmentAssociation.Delete(associationId);
		//}
	}
}
