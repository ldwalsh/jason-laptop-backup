﻿using CW.Website.AnalysisBuilder.Domain;
using System;
using System.Collections.Generic;
using System.Security;

namespace CW.Website.AnalysisBuilder.Service
{
	public class AnalysisSvc: ServiceBase
	{
		public Int32 Save(Visu visu)
		{
			if (siteUser.RoleID >= 900) throw new SecurityException(); //move to a security module?

			Visu.Save(visu, siteUser);

			return visu.Id;
		}

		public void SaveProperties(VisuProperties visuProps)
		{
            if (siteUser.RoleID >= 900) throw new SecurityException(); //move to a security module?

			Visu.SaveProperties(visuProps, siteUser);
		}

		public void Delete(Int32 id)
		{
            if (siteUser.RoleID >= 900) throw new SecurityException(); //move to a security module?

			Visu.Delete(id, siteUser);
		}

		public IEnumerable<Object> GetAllIdentifiers()
		{
            if (siteUser.RoleID >= 900) throw new SecurityException(); //move to a security module?

			return VisuIdentifier.GetAll(siteUser);
		}

		public Visu GetById(Int32 id)
		{
            if (siteUser.RoleID >= 900) throw new SecurityException(); //move to a security module?

			return Visu.GetById(id, dataManager);
		}
	}
}