﻿using CW.Website._framework;
using CW.Website.AnalysisBuilder.domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;

namespace CW.Website.AnalysisBuilder.Service
{
	public class EquipmentSvc: ServiceBase
	{
		public IEnumerable<EquipmentClass> GetClasses() //rename to get GetAllClasses?
		{
			return GetOrgSpecificDataManager().EquipmentClassDataMapper.GetAllEquipmentClasses().Select(_=>new EquipmentClass{Id=_.EquipmentClassID, Name=_.EquipmentClassName});
		}

		public IEnumerable<EquipmentClass> GetClassesByBuildingId(Int32 buildingId)
		{
			if (!SiteUser.Current.VisibleBuildings.Any(_=>(_.BID == buildingId))) throw new SecurityException(); //security should be handled by Query.EquipmentClass object
						
			return GetOrgSpecificDataManager().EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(buildingId).Select(_=>new EquipmentClass{Id=_.EquipmentClassID, Name=_.EquipmentClassName});
		}

		public IEnumerable<EquipmentObj> GetByBuildingAndClass(Int32 bid, Int32 classID)
		{
			if (SiteUser.Current.VisibleBuildings.SingleOrDefault(_=>(_.BID == bid)) == null) throw new SecurityException();

			//
			//above security should be handled by Query.Equipment object

			return
			GetOrgSpecificDataManager().EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(classID, bid, (_=>_.EquipmentType))
				.Where(_=>_.IsActive && _.IsVisible) //revise?
				.Select(_=>new EquipmentObj{Id=_.EID, BuildingId=_.BID, ClassId=_.EquipmentType.EquipmentClassID, Name=_.EquipmentName});
		}

		public IEnumerable<EquipmentObj> GetAssociatedEquipmentByEquipment(Int32 eid)
		{
            var dm = GetOrgSpecificDataManager();
				
			var equip = dm.EquipmentDataMapper.GetEquipmentByEID(eid, false, false, false, false, false, false);

			if (!SiteUser.Current.VisibleBuildings.Any(_=>(_.BID == equip.BID))) throw new SecurityException(); //above security should be handled by Query.Equipment object

			//
			//above security should be handled by Query.EquipmentClass object

			return dm.EquipmentDataMapper.GetAllVisibleEquipmentAssociatedByPEID(eid, true).Select(_=>new EquipmentObj{Id=_.EID, BuildingId=_.BID, ClassId=_.EquipmentType.EquipmentClassID, Name=_.EquipmentName});

			//if (eid != 32111) return new EquipmentObj[]{};
						
			//return new[]
			//{
			//	new EquipmentObj(){BuildingId=equip.BID, ClassId=1, Id=324324234, Name="Igors_Equip"},
			//	new EquipmentObj(){BuildingId=equip.BID, ClassId=1, Id=33324234, Name="OtherEquip"},
			//};
		}

		public EquipmentVariableValue GetVariableValue(Int32 eid, Int32 evid)
		{
			//
			//should check eEv.Equipment.BID for security?

            var eEv = GetOrgSpecificDataManager().EquipmentVariableDataMapper.GetEquipmentVariableByEIDAndEVID(eid, evid, (_=>_.EquipmentVariable));

			//TODO: ip or si based off buildingsetting
			return new EquipmentVariableValue{Value=eEv.Value, EngUnitID=eEv.EquipmentVariable.IPEngUnitID};
		}
	}
}