﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using CW.Website.UserAnalysisBuilder.Domain;

namespace CW.Website.UserAnalysisBuilder.Service
{
    [
    ScriptService
    ]
    public class WindowService: WebService
    {
        #region STRUCT

            [
            Serializable
            ]
            public struct Location
            {
                public Int32 x;
                public Int32 y;

				public String __type
				{
					get {return null;}
				}
            }

			//[
			//Serializable
			//]
			//public struct WindowInfo
			//{
			//	public Int32 x;
			//	public Int32 y;
			//	public Int32 w;
			//	public Int32 h;

			//	public String __type
			//	{
			//		get {return "UA.views.DisplayWindow.WindowInfo";}
			//	}
			//}

        #endregion

        #region static

            #region field

                private static readonly Serializer<Dictionary<String,Object>> s;
                private static readonly Dictionary<String,Object> db;

            #endregion

            #region constructor

                static WindowService()
                {
                    s = new Serializer<Dictionary<String,Object>>(new FileInfo(HttpContext.Current.Request.PhysicalApplicationPath + "db.bin"));

                    db = s.Deserialize() ?? new Dictionary<String,Object>();
                }

            #endregion

        #endregion

        #region method

            [
            WebMethod(true)
            ]
            public void SetDWInfo(DWInfo dwInfo)
            {
                //db[String.Format("WindowLocation[{0}]", windowId)] = location;

                //s.Serialize(db);

				DWInfo.save(dwInfo);
            }

            [
            WebMethod(true)
            ]
            public DWInfo GetDWInfo(Int32 windowId)
            {
                //Object l;

                //if (!db.TryGetValue(String.Format("WindowLocation[{0}]", windowId), out l))
                //{
                //    l = new Location{x=0, y=0};
                //}

                //return (Location)l;

				var dwInfo = DWInfo.GetByWindowID(windowId);

				if (dwInfo == null)
				{
					dwInfo = new DWInfo{WindowID=windowId, X=1, Y=1};
				}

				return dwInfo;
            }

            [
            WebMethod(true)
            ]
            public Location GetWindowLocationnn(Int32 windowId)
            {
                Object l;

                if (!db.TryGetValue(String.Format("WindowLocation[{0}]", windowId), out l))
                {
                    l = new Location{x=0, y=0};
                }

                return (Location)l;
            }
        #endregion
    }
}
