﻿using CW.Website.AnalysisBuilder.Domain;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Services;

namespace CW.Website.AnalysisBuilder.Service
{
	[
	ScriptService
	]
	public class BuildingService: WebService
	{
		private readonly BuildingSvc svc = new BuildingSvc();

		[
		WebMethod(true),
		ScriptMethod(UseHttpGet=true)
		]
		public IEnumerable<Building> GetForCurrentClient()
		{
			return svc.GetForCurrentClient();
		}
	}
}
