﻿///
///<reference path="$UA.js" />

//var UA = {} required becuase of bug in JSAsset

window.Namespace =
{
	create:
	function(obj)
	{
		var f =
			function(n)
			{
				for (var a in n)
				{
					var o = n[a]

					f(o)

					o.SET({__namespace:true, __name:a, parent:((n == obj) ? window : n), toString:function(){return this.__name;}}, true)
				}
			}

		f(obj)

		for (var a in obj)
		{
			window[a.toString()] = obj[a]
		}
	}
}

Namespace.create
(
	{
		UA:
		{
			bus:        {},
			charting:   {adaptors:{flot:{auxilary:{},},},},
			evaluation: {},
			expression: {},
			functions:  {},
			presenters: {},
            repository: {strategy:{}},
			service:    {},
			views:      {controls:{},},
		}
	}
)


