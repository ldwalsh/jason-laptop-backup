/*
Copyright (c) 2013, Igor Zhan

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

///<reference assembly="iCLib" name="MicrosoftAjax.js" />

///<reference assembly="iCLib" name="iCLib.iCL.core.js" />

///<reference assembly="iCLib" name="iCLib.iCL.RegExp.js" />
///<reference assembly="iCLib" name="iCLib.iCL.RegExpPrototype.js" />

///<reference assembly="iCLib" name="iCLib.iCL.Navigator.js" />
///<reference assembly="iCLib" name="iCLib.iCL.NamedNodeMapPrototype.js" />

///<reference assembly="iCLib" name="iCLib.iCL.css.js" />
///<reference assembly="iCLib" name="iCLib.iCL.css.Style.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ArrayPrototype.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Number.js" />

///<reference assembly="iCLib" name="iCLib.iCL.coreClass.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Color.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Lookup.js" />

///<reference assembly="iCLib" name="iCLib.iCL.KeyedArray.js" />
///<reference assembly="iCLib" name="iCLib.iCL.TypedArray.js" />

///<reference assembly="iCLib" name="iCLib.iCL.Time.js" />
///<reference assembly="iCLib" name="iCLib.iCL.TimePollerBase.js" />

///<reference assembly="iCLib" name="iCLib.iCL.Event.js" />
///<reference assembly="iCLib" name="iCLib.iCL.domEvent.js" />
///<reference assembly="iCLib" name="iCLib.iCL.domEvent.EventMetabase.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Location.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Numeric.js" />
///<reference assembly="iCLib" name="iCLib.iCL.domEvent.DomEvent.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Coordinates.js" />
///<reference assembly="iCLib" name="iCLib.iCL.css.ClassObj.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Size.js" />
///<reference assembly="iCLib" name="iCLib.iCL.String.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTMLElement.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTMLElementPrototype.js" />
///<reference assembly="iCLib" name="iCLib.iCL.StringPrototype.js" />

///<reference assembly="iCLib" name="iCLib.iCL.htmlStorage.js" />
///<reference assembly="iCLib" name="iCLib.iCL.htmlStorage.HTMLStorageContext.js" />
///<reference assembly="iCLib" name="iCLib.iCL.htmlStorage.HTMLStorageObjectBase.js" />
 
///<reference assembly="iCLib" name="iCLib.iCL.HTMLLinkElement.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTMLScriptElement.js" />

///<reference assembly="iCLib" name="iCLib.iCL.HTML.HTMLDocument.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTML.HTMLFormElement.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTML.HTMLTableElement.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTML.HTMLTableElementPrototype.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTML.HTMLTableRowElementPrototype.js" />

///<reference assembly="iCLib" name="iCLib.iCL.css.Styles.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Math.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Math.Bounds.js" />
///<reference assembly="iCLib" name="iCLib.iCL.collection.js" />
///<reference assembly="iCLib" name="iCLib.iCL.collection.List.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.LayerManager.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Window.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Document.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.DragHandler.js" />
///<reference assembly="iCLib" name="iCLib.iCL.coreEnum.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Array.js" />
///<reference assembly="iCLib" name="iCLib.iCL.DocumentFragment.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.js" />
///<reference assembly="iCLib" name="iCLib.iCL.webService.js" />
///<reference assembly="iCLib" name="iCLib.iCL.webService.deserializers.js" />
///<reference assembly="iCLib" name="iCLib.iCL.webService.deserializers.C2JSerializer.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Function.js" />
///<reference assembly="iCLib" name="iCLib.iCL.FunctionPrototype.js" />
///<reference assembly="iCLib" name="iCLib.iCL.collection.DictionaryEntry.js" />
///<reference assembly="iCLib" name="iCLib.iCL.collection.Dictionary.js" />
///<reference assembly="iCLib" name="iCLib.iCL.MediaTypes.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ServerExceptionError.js" />
///<reference assembly="iCLib" name="iCLib.iCL.RPCall.js" />
///<reference assembly="iCLib" name="iCLib.iCL.AsyncRPCall.js" />
///<reference assembly="iCLib" name="iCLib.iCL.RPCFactory.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.WebMethodExec.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.WebMethod.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.ScriptService.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.DataStore.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Url.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.SessionKeepAlive.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.webForm.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.webForm.AsyncPostbackManager.js" />
///<reference assembly="iCLib" name="iCLib.iCL.asp.webForm.DynamicFragment.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Boolean.js" />
///<reference assembly="iCLib" name="iCLib.iCL.CallbackHandler.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Char.js" />
///<reference assembly="iCLib" name="iCLib.iCL.collection.DynamicList.js" />
///<reference assembly="iCLib" name="iCLib.iCL.css.Sheet.js" />
///<reference assembly="iCLib" name="iCLib.iCL.CSSStyleDeclaration.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Date.js" />
///<reference assembly="iCLib" name="iCLib.iCL.DatePrototype.js" />
///<reference assembly="iCLib" name="iCLib.iCL.DateRange.js" />
///<reference assembly="iCLib" name="iCLib.iCL.DELEGATE.js" />
///<reference assembly="iCLib" name="iCLib.iCL.domEvent.EventTypes.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ExecutionQueue.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Guid.js" />

///<refernece name="iCLib.iCL.Location.js" />

/// 
///<reference assembly="iCLib" name="iCLib.iCL.HTMLCollectionPrototype.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTMLFormPrototype.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTMLInputElement.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTMLSelectElement.js" />
///<reference assembly="iCLib" name="iCLib.iCL.HTMLTextAreaElement.js" />
 
///<reference assembly="iCLib" name="iCLib.iCL.MessageBus.js" />
///<reference assembly="iCLib" name="iCLib.iCL.Object.js" />
///<reference assembly="iCLib" name="iCLib.iCL.QueryString.js" />
///<reference assembly="iCLib" name="iCLib.iCL.structure.js" />
///<reference assembly="iCLib" name="iCLib.iCL.structure.Table.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.DataBinder.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.Globalizer.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.WindowBase.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.DraggableWindowBase.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.ResizeHandler.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.ContentWindowBase.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.ControlsCollection.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.ControlBase.js" />

///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.Accordion.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.Button.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.ButtonBar.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.ContextMenu.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.Image.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.FileUploader.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.ListBox.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.MenuBar.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.Repeater.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.Scroller.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.Slider.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.Table.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.TableView.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.TabStrip.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.ToggleImage.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.ToolTip.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.TreeView.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.controls.ViewToggle.js" />

///<reference assembly="iCLib" name="iCLib.iCL.ui.ControlsGroup.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.HoverTarget.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.InteractionMonitor.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.ObjectDragHandler.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.ObjectDropTarget.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.TemplatedElement.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.Validator.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.ViewBase.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.WindowGroup.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.WindowPinner.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.WindowResizeManager.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.windows.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.windows.DialogWindow.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.windows.InputBox.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.windows.MessageBox.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.windows.ToolWindow.js" />
///<reference assembly="iCLib" name="iCLib.iCL.ui.windows.WaitBox.js" />
///<reference assembly="iCLib" name="iCLib.iCL.XMLHttpRequest.js" />
///<reference assembly="iCLib" name="iCLib.intellisense.js" />
