﻿
///
///<reference path="$iCL.js" />

///
///<reference path="UA.js" />
///<reference path="UA/ServiceBase.js" />
///<reference path="UA/ViewModesEnum.js" />
///<reference path="UA/AnalysisObjIdentifier.js" />
///<reference path="UA/AnalysisObj.js" />
///<reference path="UA/AnalysisService.js" />
///<reference path="UA/BuilderEnviornment.js" />
///<reference path="UA/Building.js" />
///<reference path="UA/BuildingService.js" />
///<reference path="UA/PrintRenderer.js" />

///<reference path="UA/bus/AnalysisBM.js" />
///<reference path="UA/bus/BMTypeEnum.js" />
///<reference path="UA/bus/SubroutineBM.js" />

///<reference path="UA/AnalysisDisplayPresenterManager.js" />

///<reference path="UA/charting/adaptors/ChartAdaptorBase.js" />
///<reference path="UA/charting/adaptors/FlotExtenderBase.js" />
///<reference path="UA/charting/adaptors/FlotAdaptorBase.js" />
///<reference path="UA/charting/adaptors/FlotChartAdaptor.js" />
///<reference path="UA/charting/adaptors/FlotContextMenuExtender.js" />

///<reference path="UA/charting/adaptors/FlotHoverExtender.js" />
///<reference path="UA/charting/adaptors/FlotMultiHoverExtender.js" />
///<reference path="UA/charting/adaptors/FlotScatterPlotAdaptor.js" />
///<reference path="UA/charting/adaptors/FlotSelectionExtender.js" />
///<reference path="UA/charting/adaptors/FlotStepZoomExtender.js" />
///<reference path="UA/charting/adaptors/GoogleChartAdaptor.js" />
///<reference path="UA/charting/adaptors/TableAdaptor.js" />

///<reference path="UA/charting/AdaptorManager.js" />
///<reference path="UA/charting/PlotBase.js" />
///<reference path="UA/charting/ExpressionPlot.js" />
///<reference path="UA/charting/PlotAggregator.js" />
///<reference path="UA/charting/RawDataPlot.js" />

///<reference path="UA/PresenterManager.js" />
///<reference path="UA/EngUnit.js" />

///<reference path="UA/EquipmentService.js" />
///<reference path="UA/EquipmentClass.js" />
///<reference path="UA/EquipmentObj.js" />


///<reference path="UA/expression/AliasUtility.js" />
///<reference path="UA/expression/Expression.js" />
///<reference path="UA/expression/ExpressionFragment.js" />
///<reference path="UA/expression/ExpressionTreeNode.js" />
///<reference path="UA/expression/FragmentBase.js" />


///<reference path="UA/Point.js" />
///<reference path="UA/PointData.js" />
///<reference path="UA/PointDataRepository.js" />
///<reference path="UA/PointDateRange.js" />
///<reference path="UA/PointService.js" />
/// 



///<reference path="UA/presenters/PresenterBase.js" />

///<reference path="UA/presenters/AnalysisDisplayManagerPresenter.js" />
 
///<reference path="UA/presenters/AnalysisDisplayPresenter.js" />
 
///<reference path="UA/presenters/VisuDisplayPresenter/ViewHandler.js" />
 
///<reference path="UA/presenters/AnalysisPropertiesPresenter.js" />
///<reference path="UA/presenters/EquipmentTreePresenter.js" />
///<reference path="UA/presenters/ExpressionBuilderPresenter.js" />
///<reference path="UA/presenters/OpenAnalysisPresenter.js" />


/// 
///<reference path="UA/SiteUser.js" />


///<reference path="UA/VisuDisplay.js" />




///<reference path="UA/views/ViewBase.js" />
///<reference path="UA/views/WindowedViewBase.js" />
///<reference path="UA/views/AnalysisPropertiesView.js" />
///<reference path="UA/views/AnalysisDisplayManagerWView.js" />
///<reference path="UA/views/AnalysisDisplayWView.js" />
///<reference path="UA/views/DisplayWindow.js" />
///<reference path="UA/views/DWInfo.js" />
///<reference path="UA/views/EquipmentTreeWView.js" />
///<reference path="UA/views/ExpressionBuilderView.js" />
///<reference path="UA/views/OpenAnalysisView.js" />
///<reference path="UA/views/WindowService.js" />
