﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace: UA,
	},

	function SubroutineObj(analysisObj, id, name)
	{
		this._definition	= String.VAR;
		this.id				= String.VAR;
		this.name			= String.VAR;
		this.inputEngUnit	= Number.VAR;
		this.outputEngUnit	= Number.VAR;
		this._equipment		= $.TArray.VAR(UA.EquipmentClass)
	
		this.Cstr =
			function()
			{
				if (!(/\w+/.test(name))) throw new $.InvalidOperationError("The subroutine name is invalid.")
					
				this.id   = id;
				this.name = name;
			}

		this.definition_get =
			function()
			{
				return this._definition;
			}

		this.definition_set =
			function(value)
			{
				this._definition = value;
				
				UA.ObjectService.instance.update(this, ["definition"])
			}

		this.equipment_get =
			function()
			{
				return this._equipment;
			}

	
		$.class.instantiate(this)
	}
)
