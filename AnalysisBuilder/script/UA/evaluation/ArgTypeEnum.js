﻿///
///<reference path="../../$UA.js" />

$.enum
(
	{
		namespace: UA.evaluation,
		name:		"ArgTypeEnum",
	},

	function()
	{
		/// <field name="Constant" />
		/// <field name="CallingStatementResult" />
		/// <field name="UserVariable" />
		/// <field name="EquipmentVariable" />
		/// <field name="NestedFunctionResult" />
		/// <field name="SubroutineResult" />
	},
	[
		"Constant", "CallingStatementResult", "UserVariable", "EquipmentVariable", "NestedFunctionResult", "SubroutineResult",
	]
)
