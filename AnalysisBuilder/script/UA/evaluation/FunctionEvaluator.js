﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.evaluation,
	},

	function FunctionEvaluator(evaluationEngine, statementEvaluator, funcCall)
	{
		///<param name="evaluationEngine" type="UA.evaluation.EvaluationEngine" />
		///<param name="statementEvaluator" type="UA.evaluation.StatementEvaluator" />
		///<param name="funcCall" type="UA.evaluation.FuncCall" />

		//

		this._evaluationEngine		= UA.evaluation.EvaluationEngine.VAR;
		this._statementEvaluator	= UA.evaluation.StatementEvaluator.VAR;
		this.funcCall				= UA.evaluation.FuncCall.VAR;

		this._map					= null;
		this._callback				= Function.VAR;
		

		//

		this.Cstr =
			function()
			{
				this._map =
					Array.toObjectMap
					(
						[
							[UA.evaluation.ArgTypeEnum.CallingStatementResult,	this._getCallingStatementResult	],
							[UA.evaluation.ArgTypeEnum.Constant,				this._getConstant				],
							[UA.evaluation.ArgTypeEnum.EquipmentVariable,		this._getEquipmentVariable		],
							[UA.evaluation.ArgTypeEnum.UserVariable,			this._getUserVariable			],
							[UA.evaluation.ArgTypeEnum.NestedFunctionResult,	this._getFunctionResult			],
						]
					)
			}

		this.evaluate =
			function(callback)
			{
				///<param name="callback" type="Function" />

				this._callback = callback;

				//

				var el = $.ExecutionList.NEW()

				for (var i=0; i<this.funcCall.argsList.length; i++)
				{
					var arg = this.funcCall.argsList.item(i)

					with ({arg:arg})
					{
						el.add(this._map[arg.type], [arg])
					}
				}

				el.execute(this._evaluate)
			}

		this._evaluate =
			function()
			{
				try
				{
					var result = this.funcCall.invoke()
				}
				catch (e)
				{
					switch (e.message)
					{
						case "x1":
						{
							this._evaluationEngine.outputStream
							(
								"The function '" + this.funcCall.funcName + "' in subroutine '" + this._statementEvaluator.subroutineEvaluator.subroutineObj.name + "' was not found."
							)

							return;
						}
						default:
						{
							this._evaluationEngine.outputStream("Unknown Error")

							return;
						}
					}
				}

				this._statementEvaluator.funcResults[this.funcCall.rawStatement] = result;

				Function.ifCall(this._callback, [this.funcCall, result])
			}

		this._getConstant =
			function(funcArg)
			{
				///<param name="funcArg" type="UA.evaluation.FuncArg" />

				funcArg.value = ((funcArg.prefix == "#") ? parseFloat(funcArg.body) : funcArg.body) //constant value in body

				//arguments.callee.return = true;
			}

		this._getUserVariable =
			function(funcArg)
			{
				///<param name="funcArg" type="UA.evaluation.FuncArg" />

				var uV = evaluationEngine.analysisObj.userVariables.singleOrNull({"name":funcArg.body})

				if (!uV)
				{
					this.onException("The user variable '" + rawValue.substr(1) + "' does not exist.")

					//arguments.callee.return = false;

					return;
				}

				funcArg.value = uV.value;

				//arguments.callee.return = true;
			}

		this._getEquipmentVariable =
			function(funcArg)
			{
				///<param name="funcArg" type="UA.evaluation.FuncArg" />

				var f = arguments.callee;

				UA.EquipmentService.instance.getVariableValue(funcArg.body.split(":")[0], funcArg.body.split(":")[1]).onReturn.attach
				(
					function(equipVarVal)
					{
						///<param name="equipVarVal" type="UA.evaluation.EquipmentVariableValue" />

						funcArg.value = equipVarVal.value;

						//f.return = true;
					}
				)
			}

		this._getEquipmentDataPoints =
			function()
			{
			}

		this._getCallingStatementResult =
			function(funcArg)
			{
				///<param name="funcArg" type="UA.evaluation.FuncArg" />

				funcArg.value = this._statementEvaluator.subroutineEvaluator.statementResults.getLast().val;

				//arguments.callee.return = true;
			}

		this._getFunctionResult =
			function(funcArg)
			{
				///<param name="funcArg" type="UA.evaluation.FuncArg" />

				funcArg.value =	this._statementEvaluator.funcResults[funcArg.body]

				//arguments.callee.return = true;
			}
	}
)
