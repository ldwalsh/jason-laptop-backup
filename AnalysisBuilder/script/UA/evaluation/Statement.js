﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.evaluation,
	},

	function Statement(statementStr)
	{
		///<param name="statementStr" type="String" />

		this.Cstr =
			function()
			{
				this.statementStr = statementStr;
				this.func		  = UA.evaluation.FuncCall.NEW(statementStr)
			}

		this.getFuncCallList =
			function()
			{
				return this.func.getFuncCallList()
			}
	}
)
