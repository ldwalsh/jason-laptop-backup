﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.evaluation,
		statics:
		{
			prefixToArgType:
			{
				".":	UA.evaluation.ArgTypeEnum.UserVariable,
				"!":	UA.evaluation.ArgTypeEnum.EquipmentVariable,
				"_":	UA.evaluation.ArgTypeEnum.CallingStatementResult,
				"#":	UA.evaluation.ArgTypeEnum.Constant,
				"$":	UA.evaluation.ArgTypeEnum.Constant,
				"ƒ":	UA.evaluation.ArgTypeEnum.NestedFunctionResult,
			},
		},
	},

	function FuncArg(rawArg, order)
	{
		///<param name="rawArg" type="String" />
		///<param name="order" type="Number" />

		this.rawArg = rawArg;

		this.prefix = rawArg.charAt(0)
		
		this.type = this.constructor.prefixToArgType[this.prefix]
		this.body = this.rawArg.substr(1)

		this.order = order;
		
		this.value = null;
	}
)
