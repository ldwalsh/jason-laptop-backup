﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.evaluation,
		statics:
		{
			parseArgs:
			function(argString)
			{
				///<param name="argString" type="String" />

				var argList = new $.TArray(String)

				var currentArg = "";
				var inFunction = 0;

				for (var i=0; i<argString.length; i++)
				{
					var char = argString.charAt(i)

					switch (char)
					{
						case ",":
						{
							if (inFunction > 0) break;

							argList.push(currentArg)

							currentArg = "";
							
							continue;
						}
						case "(":
						{
							inFunction++;

							break;
						}
						case ")":
						{
							inFunction--;

							//if (inFunction == 0)
						}
						//case "ƒ":
						//{
						//	inFunction
						//}
					}

					currentArg += char;
				}

				argList.push(currentArg)

				return argList;
			},

			_split:
			function(str)
			{
				///<param name="str" type="String" />

				var openParam = str.indexOf("(")

				return [str.substring(0, openParam), UA.evaluation.FuncCall.parseArgs(str.substring((openParam+1), str.length-1))]
			},
		},
	},

	function FuncCall(rawStatement)
	{
		///<param name="rawStatement" type="String" />

		this.Cstr =
			function()
			{
				this._split				= UA.evaluation.FuncCall._split(rawStatement)

				this.rawStatement		= rawStatement;

				this.funcName			= this._split[0]
				this.argsList			= this._getArgsList()
				
				this.nestedFuncCalls	= this._getNestedFuncCalls()
			}

		this.getFuncCallList =
			function()
			{
				var callList = new $.TArray(this.constructor)

				for (var i=0; i<this.nestedFuncCalls.length; i++)
				{
					var func = this.nestedFuncCalls.item(i)

					var list = func.getFuncCallList()

					for (var ii=0; ii<list.length; ii++)
					{
						callList.push(list.item(ii))
					}
				}

				callList.push(this)
				
				return callList;
			}

		this.invoke =
			function()
			{
				var func = Function.CAST(UA.functions[this.funcName])

				if (!func) throw new Error("x1")

				var args = []

				for (var i=0; i<this.argsList.length; i++)
				{
					args.push(this.argsList.item(i).value)
				}

				return func.apply(window, args)
			}

		this._getArgsList =
			function()
			{
				var args = new $.TArray(UA.evaluation.FuncArg)

				for (var i=0; i<this._split[1].length; i++)
				{
					var arg = this._split[1].item(i)

					args.push(UA.evaluation.FuncArg.NEW(arg, i+1))
				}

				return args;
			}

		this._getNestedFuncCalls =
			function()
			{
				var funcs = new $.TArray(this.constructor)

				for (var i=0; i<this.argsList.length; i++)
				{
					var arg = this.argsList.item(i)

					if (arg.prefix != "ƒ") continue;

					funcs.push(this.constructor.NEW(arg.rawArg.substring(1)))
				}

				return funcs;
			}
	}
)
