﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.evaluation,
		statics:
		{
			fromString:
			function(str)
			{
				///<param name="str" type="String" />

				str = str.split("=")

				return UA.evaluation.UserVariableValue.NEW(str[0], str[1])
			},
		},
	},

	function UserVariableValue(name, value)
	{
		this.name  = String.VAR;
		this.value = String.VAR;

		this.Cstr =
			function()
			{
				this.name	= (this.name  || "")
				this.value	= (this.value || "")
			}
	}
)

