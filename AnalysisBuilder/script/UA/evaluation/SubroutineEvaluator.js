﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.evaluation,
	},

	function SubroutineEvaluator(evaluationEngine, subroutineObj, subroutineResults)
	{
		///<param name="evaluationEngine" type="UA.evaluation.EvaluationEngine" />
		///<param name="subroutineObj" type="UA.SubroutineObj" />

		//

		this._evaluationEngine	= UA.evaluation.EvaluationEngine.VAR;
		this.subroutineObj		= UA.SubroutineObj.VAR;

		this._funcCallList		= new $.TArray(UA.evaluation.FunctionObjCall)

this.statementResults = $.collection.Dictionary.NEW(Object)


		this.onCompleted		= DELEGATE;
		this.onException		= DELEGATE;

		this.evaluate =
			function()
			{
				if (!this.subroutineObj.definition) //throw new UA.evaluation.EvaluationEngine.EvaluationError("The subroutine '" + this._subroutineObj.name + "' does not have a definition.");
				{
					this.onException("The subroutine '" + this.subroutineObj.name + "' does not have a definition.")

					return;
				}

				//
				
				this._buildFuncCallStack()
			}

		this._buildFuncCallStack =
			function()
			{
				var statementStack = new $.TArray(UA.evaluation.Statement)

				new $.TArray(String, this.subroutineObj.definition.split("➔")).forEach
					(
						function(rawStatement)
						{
							statementStack.push(UA.evaluation.Statement.NEW(rawStatement))
						}
					)

				//
				
				//var cq = new $.CallbackQueue()

var $this = this;

				var el = $.ExecutionList.NEW()

				for (var i=0; i<statementStack.length; i++)
				{
					var statement = statementStack.item(i)

					with ({se:UA.evaluation.StatementEvaluator.NEW(this._evaluationEngine, this, statement)})
					{
						el.add
						(
							function()
							{
								var f = arguments.callee;

								se.evaluate
								(
									function(statement, result)
									{
										$this._statementEvaluatedCallback(statement, result)

										//f.return = true;

									}
								)

							},
							[]
						)
					}

					//cq.add(statement.statementStr, this._statementEvaluatedCallback)

					//UA.evaluation.StatementEvaluator.NEW(this._evaluationEngine, this, statement).evaluate(this._statementEvaluatedCallback)
				}

				el.execute(this._subroutineEvaluated)

				//cq.wait(this._subroutineEvaluated)
			}

		this._statementEvaluatedCallback =
			function(statement, result, $token)
			{
				///<param name="statement" type="UA.evaluation.Statement" />
				///<param name="result" type="Object" />

				this.statementResults.add(statement.statementStr, result)

				//arguments.callee.return(statement.statementStr)
			}

		this._subroutineEvaluated =
			function()
			{
				this.onCompleted("Subroutine evaluated to: " + this.statementResults.getLast().val)
			}
	}
)

//  ➔  ƒ
