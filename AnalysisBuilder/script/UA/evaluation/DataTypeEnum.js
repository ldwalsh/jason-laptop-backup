﻿///
///<reference path="../../$UA.js" />

$.enum
(
	{
		namespace: UA.evaluation,
		name:		"DataTypeEnum"
	},		 
	function()
	{
		/// <field name="Boolean" />
		/// <field name="String" />
		/// <field name="Number" />
		/// <field name="DateTime" />
	},
	[
		"Boolean", "String", "Number", "DateTime"
	]
)