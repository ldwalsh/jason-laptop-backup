﻿///
///<reference path="../../$UA.js" />

$.enum
(
	{
		namespace: UA.evaluation,
		name:		"FunctionCategoryEnum"
	},
	function()
	{
		/// <field name="Operator" />
		/// <field name="Fault" />
		/// <field name="Physics" />
		/// <field name="Conditional" />
		/// <field name="Numeric" />
	},
	[
		"Operator", "Fault", "Physics", "Conditional", "Numeric"
	]
)
