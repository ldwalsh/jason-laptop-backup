﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.evaluation,
	},

	function StatementEvaluator(evaluationEngine, subroutineEvaluator, statement)
	{
		///<param name="evaluationEngine" type="UA.evaluation.EvaluationEngine" />
		///<param name="subroutineEvaluator" type="UA.AnalysisObj2" />
		///<param name="statement" type="UA.evaluation.Statement" />	

		this._callback	 = Function.VAR;
		this.funcResults = {}

		this.evaluate =
			function(callback)
			{
				this._callback = callback;

				//

				var fcl = this.statement.getFuncCallList()

				var cq = $.CallbackQueue.NEW()

				for (var i=0; i<fcl.length; i++)
				{
					var funcCall = fcl.item(i)

					cq.add(funcCall.rawStatement, this._functionEvaluatedCallback)
					
					UA.evaluation.FunctionEvaluator.NEW(this.evaluationEngine, this, funcCall).evaluate(this._functionEvaluatedCallback)
				}

				cq.wait(this._statementEvaluated)
			}

		this._functionEvaluatedCallback =
			function(funcCall, result)
			{
				///<param name="funcCall" type="UA.evaluation.FuncCall" />

				this.funcResults[funcCall.rawStatement] = result;

				this.subroutineEvaluator.onCompleted("Function '" + funcCall.rawStatement + "' evaluated to: " + result)

				//arguments.callee.return(funcCall.rawStatement)
			}

		this._statementEvaluated =
			function()
			{
				this._callback(this.statement, this.funcResults[this.statement.statementStr])
			}
	}
)