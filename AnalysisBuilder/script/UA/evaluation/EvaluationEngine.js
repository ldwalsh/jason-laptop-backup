﻿///
///<reference path="../../$UA.js" />

///dependency: FunctionObjBase

$.class
(
	{
		namespace: UA.evaluation,
		//dependency:	[UA.FunctionObjBase]
	},

	function EvaluationEngine(analysisObj)
	{
		///<param name="analysisObj" type="UA.AnalysisObj2" />

		this._analysisObj	= UA.AnalysisObj2.VAR;
		this.outputStream	= new DELEGATE; //$.Event

		this.run =
			function()
			{
				if (!this._analysisObj.mainSubroutineObj)
				{
					this.outputStream("The given AnalysisObj does not have a main subroutine")

					return;
				}

				this.outputStream("Running analysis.")

				var se = UA.evaluation.SubroutineEvaluator.NEW(this, this._analysisObj.mainSubroutineObj)

					se.onException = this.outputStream;
					se.onCompleted = this.outputStream;

					se.evaluate()
			}
		
		this.analysisObj_get =
			function()
			{
				return this._analysisObj;
			}

	}
	//,
	//{
	//	registerFunctions:
	//	function()
	//	{
	//		UA.FunctionService.instance.getFunctionTypes().setOnReturn
	//		(
	//			function(types)
	//			{
	//				///<param name="types" type="Array" />

	//				//revise... types should be a $.TArray AND should be able to just set it to .functions =

	//				for (var i=0; i<types.length; i++)
	//				{
	//					UA.evaluation.EvaluationEngine.functions.push(types[i]);
	//				}

	//				//Function.ifCall(callback, [UA.evaluation.EvaluationEngine.functions]);
	//			}
	//		)
	//	},

	//	getEngUnits:
	//	function()
	//	{
	//		UA.FunctionService.instance.getEngUnits().setOnReturn
	//		(
	//			function(engUnits)
	//			{
	//				///<param name="engUnits" type="Array" elementType="UA.EngUnit" />
					
	//				for (var i=0; i<engUnits.length; i++)
	//				{
	//					UA.evaluation.EvaluationEngine.engUnits.push(engUnits[i]);
	//				}
	//			}
	//		)
	//	},

	//	functions:	new $.TArray(UA.FunctionObjBase),
	//	engUnits:	new $.TArray(UA.EngUnit), //replace with $.KeyedArray(UA.EngUnit)
	//}
)






$.class
(
	{
		namespace:	UA.evaluation.EvaluationEngine,
		name:		"EvaluationError",
	},
	function()
	{
		typeof UA.evaluation.EvaluationEngine.EvaluationError;
	
		return new $._ErrorBase(this, "")
	}
)

$.class
(
	{
		namespace:	UA.evaluation.EvaluationEngine,
		name:		"SubroutineNotDefinedError",
	},

	function(subroutineObj)
	{
		///<param name="subroutineObj" type="UA.SubroutineObj" />

		typeof $.ArgValueError;
	
		return new $._ErrorBase(this, ("The SubroutineObj has no definition. [" + subroutineObj.name + "]"))
	}
)