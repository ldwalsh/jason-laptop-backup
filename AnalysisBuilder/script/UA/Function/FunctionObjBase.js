﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA,
	},

	function FunctionObjBase()
	{
		this.name			= String.VAR;
		this.category		= UA.evaluation.FunctionCategoryEnum;
		this.label			= String.VAR;
		this.description	= String.VAR;

		this.evaluate_get =
			function()
			{
				return Function.CAST(UA.functions[this.name]);
			}
	}
)

/*

$.class
(
)


var _ns =
	{
		add:
		function(a, b, z)
		{
			var result = parseFloat(arguments[0]);

			for (var i=1; i<arguments.length; i++)
			{
				var val = parseFloat(arguments[i]);

				result += val;
			}

			return result;
		},

		divide:
		function(a, b, z)
		{
			var result = parseFloat(arguments[0]);

			for (var i=1; i<arguments.length; i++)
			{
				var val = parseFloat(arguments[i]);

				result /= val;
			}

			return result;
		},

		exp:
		function(b, n)
		{
			return Math.pow(b, n);
		},

		greaterThan:
		function(x, y)
		{
			return (parseFloat(x) > parseFloat(y));
		},

		lessThan:
		function(x, y)
		{
			return (parseFloat(x) < parseFloat(y));
		},

		subtract:
		function(a, b, z)
		{
			var result = parseFloat(arguments[0]);

			for (var i=1; i<arguments.length; i++)
			{
				var val = parseFloat(arguments[i]);

				result -= val;
			}

			return result;
		},

		multiply:
		function(a, b, z)
		{
			var result = 1;

			for (var i=0; i<arguments.length; i++)
			{
				var val = parseFloat(arguments[i]);

				result *= val;
			}

			return result;
		},

		//

		if:
		function(bool, ifTrue, ifFalse)
		{
			return (bool ? ifTrue : ifFalse);
		}

	};


	UA.functions = _ns;

*/