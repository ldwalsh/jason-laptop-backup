﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		prototype: iCL.asp.ScriptServiceBase,
		construct:
		{
			instance_:
			function()
			{
				this.__instance = (this.__instance || this.NEW(document.baseUrl.endWith("/") + "Service/AnalysisService.asmx"))
				
				return this.__instance;
			},
		},
	},

	function VisuService(endpoint)
	{
		this.Cstr =
			function()
			{
				this._defineWebMethod("Save", ["visu"], {verb:"POST",})
				this._defineWebMethod("SaveProperties", ["visuProps"], {verb:"POST",})
				this._defineWebMethod("Delete", ["id"], {verb:"POST",})
				this._defineWebMethod("GetAllIdentifiers")
				this._defineWebMethod("GetById", ["id"])
			}
		
		this.getAllIdentifiers =
			function()
			{
				return this._methods.GetAllIdentifiers.invoke()
			}

		this.getById =
			function(id)
			{
				return this._methods.GetById.invoke(id)
			}

		this.save =
			function(visu)
			{
				///<param name="visu" type="UA.VisuObj" />
				
				var v = Constructor.serialize(visu.SET({modified:Date.current,}), true)
				
				Object.keys(v).forEach //this logic should be moved out to ScriptServiceBase!
				(
					function(k)
					{
						if (k[0] == "_")
						{
							v["_" + k[1].toUpperCase() + k.substr(2)] = v[k]
						}
						else
						{
							v[k[0].toUpperCase() + k.substr(1)] = v[k]
						}

						delete v[k]
					}
				)
				
				return this._methods.Save.invoke(v)
			}

		this.saveProperties =
			function(visuProps)
			{
				///<param name="visuProps" type="Object" />

				return this._methods.SaveProperties.invoke
				(
					{visuId:visuProps.id, name:visuProps.name, Description:visuProps.description, Modified:Date.current, SharedRead:visuProps.sharedRead, SharedWrite:visuProps.sharedWrite, Notes:visuProps.notes,}
				)
			}

		this.delete =
			function(id)
			{
				return this._methods.Delete.invoke(id)
			}
	}
)
