﻿///
///<reference path="../../$UA.js" />


Enum.define
(
	{
		namespace: UA.bus,
	},

	function BMTypeEnum()
	{
		VisuOpened,
		AnalysisUnloaded,

		RenameAnalysis,

		EvaluateAnalysis,

		ViewAnalysisProps,
		ViewAnalysisEquipmentAssociation,

		CreateSubroutine,
		ImportSubroutine,
		RemoveSubroutine,
		RenameSubroutine,

		SetSubroutineAsMain,

		ViewSubroutine,
		ViewSubroutineProps,

		NewChartDisplay
	}
)


/*
$.enum
(
	{
		namespace: UA.bus,
		name:		"BMTypeEnum"
	},
	function()
	{
		///<field name="VisuOpened" />
		///<field name="AnalysisUnloaded" />

		///<field name="RenameAnalysis" />

		///<field name="EvaluateAnalysis" />

		///<field name="ViewAnalysisProps" />
		///<field name="ViewAnalysisEquipmentAssociation" />

		///<field name="CreateSubroutine" />
		///<field name="ImportSubroutine" />
		///<field name="RemoveSubroutine" />
		///<field name="RenameSubroutine" />

		///<field name="SetSubroutineAsMain" />

		///<field name="ViewSubroutine" />
		///<field name="ViewSubroutineProps" />

		///<field name="NewChartDisplay" />
	},
	[
		"VisuOpened",
		"AnalysisUnloaded",

		"RenameAnalysis",

		"EvaluateAnalysis",

		"ViewAnalysisProps",
		"ViewAnalysisEquipmentAssociation",

		"CreateSubroutine",
		"ImportSubroutine",
		"RemoveSubroutine",
		"RenameSubroutine",

		"SetSubroutineAsMain",

		"ViewSubroutine",
		"ViewSubroutineProps",

		"NewChartDisplay",
	]
)
*/