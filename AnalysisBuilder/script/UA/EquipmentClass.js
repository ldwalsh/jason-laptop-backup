﻿///
///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		reference: function(){return [UA.EquipmentService,]},		
		statics:
		{
			classes: null,

			Cstr:
			function()
			{
				this.classes = new $.TArray(this)
			}, 
		},
	},

	function EquipmentClass()
	{
		this.id   = Number.VAR;
		this.name = String.VAR;
	}
)
