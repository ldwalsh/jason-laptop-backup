﻿///<reference path="../$UA.js"/>

Object.define
(
	{
		namespace: UA,
	},

	function Point() //rename to PointObj
	{
		this.id        = Number.VAR;		
		this.name      = String.VAR;
		this.typeId    = Number.VAR;
		this.classId   = Number.VAR;		
		this.engUnitId = Number.VAR;		
		this.sampling  = Number.VAR;
	}
)