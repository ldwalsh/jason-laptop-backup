﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		construct: {defaultVisuName_:function(){return "Visual";}, defaultVisuNameRegex_:function(){return "^" + this.defaultVisuName + " [0-9]+$";},},
	},

	function VisuDisplay(visu)
	{
		var $charting    = UA.charting;

		this._visu          = UA.VisuObj.VAR;
this._viewMode      = UA.ViewModesEnum.Line;
		this._isLoaded      = false;
		this._plots         = iCL.collections.List.VAR($charting.PlotBase)
		this._changeCounter = 0; //move out to VisuDisplay
		this._visuLastSaved = 0;

		this.loadErrors = null;

		this.Cstr =
			function()
			{
				this._plots = iCL.collections.List.NEW($charting.PlotBase)

				this.loadErrors = []

				//

				var buildings = UA.BuilderEnviornment.instance.siteUser.buildings;
				
				var plots = this._visu.plots;

				plots.filter //move out to AnalysisObjManager
				(
					function(p)
					{
						var RawDataPlot  = $charting.RawDataPlot;

						if (p instanceof $charting.ExpressionPlot)
						{
							if (!p.pointAliases.values.length) return true; //revise

							return !!$charting.ExpressionPlot.CAST(p).pointAliases.filter
							(
								function(alias)
								{
									var p2 = plots.ofType(RawDataPlot).singleOrNull(function(_){return (_.point.id == alias.value)})

									if (p2) return buildings.hasKey(p2.buildingId)
									
									this.loadErrors.push(VisuDisplay.ExpressionAliaseNotFoundError.NEW(p, alias.key))

									return true;
								
								}.bind(this)
							
							).length;
						}
						else return buildings.hasKey(RawDataPlot.CAST(p).buildingId)						
					
					}.bind(this)
				
				).sort(Array.createSortFunction("_order", Math.negate)).forEach(this._plots.add) //.forEach(this._addPlotToLegend.getArgsCaller([Function.getArgsCaller.firstPlaceHolder]))				
			}

		this.createPlot =
			function(point, dateRange, buildingId, equipmentId)
			{
				///<signature>
				///	<param name="point" type="o.Point" />
				///	<param name="dateRange" type="Object" />
				///	<param name="buildingId" type="Object" />
				///	<param name="equipmentId" type="Object" />
				///</signature>

				var plot = ((arguments.length == 1) ? arguments[0] : $charting.RawDataPlot.NEW(dateRange||this._visu.dateRange, point, buildingId, equipmentId, UA.EngUnit.engUnits)) //revise

				this._plots.add(plot)
                this._visu.addPlot(plot)   //this._visu.SET({addPlot:[plot], xPlot:ifNull(this._visu.xPlot, plot),})

				this._changeCounter++;

				return plot;
			}

		this.removePlot =
			function(plot)
			{
				var plots = this._visu._plots; //revise
				
				//var plot = this._plots.single({id:id})

				var id = plot.id;
								
				if ($charting.RawDataPlot.AS(plot)) //dependenents/dependencies of RawDataPlot should be maintained/computed by AnalysisObjManager or directly by AnalysisObj
				{
					var errMsg =
						plots.FOR //use ofType here first!?
						(
							function(p)
							{
								p = $charting.ExpressionPlot.AS(p) //should be handled by .ofType

								return ((p && p.pointAliases.values.contains(plot.point.id)) ? ("The point cannot be deleted because it is required by the expression: " + p.title) : undefined)
							}
						)

					if (errMsg) return errMsg;
				}

				this._plots.remove(plot)

				plots.remove(plots.single({id:id}))

				plots.toArray().sort(Array.createSortFunction("order")).forEach(function(p, index){rU(p.order=++index)})  //add .sort to list and remove the .toArray! //Plot ordering willl be managed by Visu

				//if (plot != this._visu.xPlot) return;
				
				//this._.visu.xPlot = (this._plots.count ? this._plots.item(0) : null)
			}

		this.loadData =
			function(dateRange, callback)
			{
				this._isLoaded = false;

				this._visu.dateRange = ((dateRange && !this._visu.dateRange.equalTo(dateRange)) ? dateRange : this._visu.dateRange) //revise??
				
				UA.charting.PlotAggregator.NEW(this.plots).aggregate(function(){Function.invoke([callback, function(){this._isLoaded = true}.bind(this)])}.bind(this)) //revise
			}

		this.export = //rename to exportVisu? /move logic out to UA.VisuExporter!!!
			function(adaptor, errCallback)
			{
				if (!this.isLoaded)     return new Error("The visualization is still loading.")
				if (!this._plots.count) return new Error("The visualization has no points.")

				//

				var extension;
				var content;

				if (this.viewMode == UA.ViewModesEnum.Table)
				{
					var t = adaptor.tableControl;

					extension = "csv";
					content   = (String.build(t.headerArray, Function.passThrough, ",") + "|" + String.build(t.rows, function(r){return String.build(r.cells, function(_){return ("\"" + _.textContent + "\"")}, ",")}, "|"))
				}
				else
				{
					extension = UA.ExportTypeEnum.PDF.toString().toLowerCase()
					content   = UA.PrintRenderer.NEW(this._visu, this._plots.toArray(), adaptor.constructor).render()
				}

				iCL.Downloader.NEW(this._visu.name.removeWhiteSpace(), "handler/ExportHandler.ashx", errCallback).initiate({fileName:(this._visu.name + "." + extension), content:content})
			}

		this.visu =
			{
				get:function(){return this._visu;}
			}

		this.hasHiddenPlots =
			{
				get:function(){return (this._visu.plots.length != this._plots.count)}
			}			

		this.visuNeedsSaving =
			{
				get:function(){return (this._changeCounter != this._visuLastSaved)}
			}

		this.viewMode =
			{
				get:function( ){return this._viewMode;},
				set:function(v){this._viewMode = this._visu.viewMode = v}
			}			

		this.isLoaded =
			{
				get:function(){return this._isLoaded;}
			}

		this.plots =
            {
                get:function(){return this._plots.toArray()}
            }			
	}
)



Object.define
(
	{
		namespace: UA.VisuDisplay,
		prototype: Error,
	},

	function ExpressionAliaseNotFoundError(expressionPlot, alias)
	{
		this.expressionPlot = UA.charting.ExpressionPlot.VAR;
		this.alias          = String.VAR;
	}
)




Array.prototype.ofType =
function(cstr)
{
	//
	if (window.intellisense) return this;
	//

	return this.filter(function(_){return (_ instanceof cstr)})
}
