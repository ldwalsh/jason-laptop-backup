﻿///<reference path="../$UA.js"/>

Object.define
(
	{
		namespace: UA,
	},

	function PointData() //rename to PointSample
	{
        this.pointId = Number.VAR;
		this.logged  = Date.VAR;
		this.value   = String.VAR;
	}
)
