﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"MenuHandler",
	},

	function(builderEnviornment, menuUL)
	{
		/// <param name="builderEnviornment" type="UA.BuilderEnviornment" />
		/// <param name="menuUL" type="HTMLUListElement" />

		this._builderEnviornment = UA.BuilderEnviornment.VAR;
		this._menuUL			 = HTMLUListElement.VAR;
		this._mainMenuBar		 = $.ui.controls.MenuBar.VAR;

		this.Cstr =
			function()
			{
				this._mainMenuBar				= new $.ui.controls.MenuBar(this._menuUL)
				this._mainMenuBar.menuHandler	= new this._getMenuHandler(this._builderEnviornment) //this._menuHandler;

				$.ui.ControlBase.buildControl(this._mainMenuBar)

				new $.MessageBus.MessageListener(this._builderEnviornment.mbus, this._receiveBusMsg)
			}

		this._receiveBusMsg =
			function(msg)
			{
				///<param name="msg" type="$.MessageBus.Message" />

				switch (msg.type)
				{
					case UA.bus.BMTypeEnum.VisuOpened:
					{
						this._mainMenuBar.getMenuByCaption("View").toggleEnabled(true)

						break;
					}
					case UA.bus.BMTypeEnum.AnalysisUnloaded:
					{
						this._mainMenuBar.getMenuByCaption("View").toggleEnabled(false)

						break;
					}
				}				
			}

		this._getBuilderEnviornment =
			function()
			{
				return this._builderEnviornment;
			}

		this._getMenuHandler =
			function(builderEnviornment)
			{
				///<param name="builderEnviornment" type="UA.BuilderEnviornment" />
				
				var r =
					{
						"Analysis":
						{
							"New":
							function()
							{
								UA.presenters.PresenterBase.create(new UA.presenters.NewAnalysisPresenter(builderEnviornment)).show()
							},

							"Open":
							function()
							{
								UA.presenters.PresenterBase.create(new UA.presenters.OpenAnalysisPresenter(builderEnviornment)).show()
							},

							"Close":
							function()
							{
								alert("Not Supported")
							}
						},

						"View":
						{
							"New Chart Display":
							function()
							{
								builderEnviornment.chartDisplayPresenterManager.createChartDisplay()
							},

							"*":
							function(menu)
							{
								///<param name="menu" type="String" />

								var wg = UA.views.WindowedViewBase.windowGroup;

								var w =
									wg.windows.FILTER
									(
										function(e)
										{
											return (e.windowedView.constructor == UA.views[(menu.replace(/ /g, "") + "WView")])
										}

									).getLast()

								w.SET({left:10, top:10})

								wg.activateWindow(w)
							}
						}
					}
				
				return r;
			}


		$.class.instantiate2(this)
	}
)
