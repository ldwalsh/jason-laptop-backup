﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function AnalysisExplorerPresenter(builderEnviornment, viewCstr)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />
		///<param name="viewCstr" type="Object" />
		
		this._view = UA.views.AnalysisExplorerWView.NEW()

		this._init =
			function()
			{
				this._view.setSubroutineAsMain	= builderEnviornment.analysisManager.setSubroutineAsMain;
				
				this._view.removeSubroutine		= builderEnviornment.analysisManager.removeSubroutine;
				this._view.createSubroutine		= builderEnviornment.analysisManager.createSubroutine;
				
				this._view.nameCriteria = UA.AnalysisManager.nameCriteria;

				//

				this._messageListner.subscriptions.add(UA.bus.BMTypeEnum.VisuOpened)
				this._messageListner.subscriptions.add(UA.bus.BMTypeEnum.RenameSubroutine)
				this._messageListner.subscriptions.add(UA.bus.BMTypeEnum.SetSubroutineAsMain)
				this._messageListner.subscriptions.add(UA.bus.BMTypeEnum.RenameAnalysis)

				this._messageListner.wireTo(UA.presenters.AnalysisExplorerPresenter_.MessageEndpoint.NEW(this._view))
			}

		this._receiveBusMsg =
			function(msg)
			{
				///<param name="msg" type="$.MessageBus.Message" />

				switch (msg.type)
				{
					case UA.bus.BMTypeEnum.CreateSubroutine:
					{
						var subroutineObj = msg.subroutineObj;
						
						this._view.addSubroutineToList(subroutineObj.name, subroutineObj.id)

						break;
					}
					case UA.bus.BMTypeEnum.RemoveSubroutine:
					{
						var subroutineObj = msg.subroutineObj;

						this._view.removeSubroutineFromList(subroutineObj.id)

						break;
					}
					case UA.bus.BMTypeEnum.ImportSubroutine:
					{		
						var subroutineObj = msg.subroutineObj;

						this._view.addSubroutineToList(subroutineObj.name, subroutineObj.id)
					}
				}
			}

		this._runAnalysis =
			function()
			{
				
				if (!this._analysisObj.mainSubroutineObj)
				{
					$.ui.windows.MessageBox.open("The currently opened analysis does not have a main subroutine defined.")

					return;
				}

				var ee = UA.evaluation.EvaluationEngine.NEW(this._analysisObj)

				builderEnviornment.mbus.send($.MessageBus.Message.NEW(UA.bus.BMTypeEnum.EvaluateAnalysis, ee))

				ee.run()
			}

		this._viewAnalysisProps =
			function()
			{
				builderEnviornment.mbus.send(UA.bus.AnalysisBM.NEW(UA.bus.BMTypeEnum.ViewAnalysisProps, this._analysisObj))
			}

		this._viewAnalysisEquipmentAssociation =
			function()
			{
				builderEnviornment.presenterManager.analysisEquipmentAssociationPresenter.show()
			}

		this._importSubroutine =
			function()
			{											
				UA.presenters.ImportSubroutinePresenter.NEW(builderEnviornment).show()
			}
	
		this._viewSubroutine =
			function(subroutineId)
			{
				builderEnviornment.mbus.send(UA.bus.SubroutineBM.NEW(UA.bus.BMTypeEnum.ViewSubroutine, this._analysisObj.getSubroutineById(subroutineId)))
			}

		this._viewSubroutineProps =										
			function(subroutineId)
			{
				builderEnviornment.mbus.send(UA.bus.SubroutineBM.NEW(UA.bus.BMTypeEnum.ViewSubroutineProps, this._analysisObj.getSubroutineById(subroutineId)))
			}
	}
)


UA.presenters.AnalysisExplorerPresenter_ = {}