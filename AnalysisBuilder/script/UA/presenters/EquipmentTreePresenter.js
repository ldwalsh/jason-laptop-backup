﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
		
		statics:
		{
			createUniquePointPath:function(equipId, pointId){return (equipId + "." + pointId)},
		}
	},

	function EquipmentTreePresenter(builderEnviornment)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />

		this._view   = UA.views.EquipmentTreeWView.VAR;
		this._points = iCL.collections.Dictionary.VAR(UA.Point)

		this.Cstr =
			function()
			{
				this._view   = UA.views.EquipmentTreeWView.NEW()
				this._points = iCL.collections.Dictionary.NEW(UA.Point)

				this._messageListner.addSubscription("ideLoaded", this._msg_ideLoaded)
			}

		this._getEquipmentObj = //move logic to service layer
			function(e)
			{
				var list  = UA.BuilderEnviornment.instance._equipment;
				var found = list[e.id]

				if (!found)
				{
					list[e.id] = found = e;
				}

				return found;
			}

		this._view_buildingExpanded =
			function(buildingId)
			{
				UA.EquipmentService.instance.getClassesByBuildingId(buildingId).setOn =
				Function.bindAll
				(
					this,
					{
						success:function(classes)
						{
							///<param name="classes" type="Array" elementType="UA.EquipmentClass" />
						
							classes.forEach(function(c){this._view.addEquipmentClass(buildingId, c.name, c.id)}.bind(this))

							this._view.addEquipmentClass(buildingId) //revise this method of setting isLoading=false						
						},
						failure:function(err)
						{
							this._view.promptError("An error occured retrieving building classes.")

							//err.caller.exec()
						}
					}
				)
			}

		this._view_equipClassExpanded =
			function(buildingId, classId)
			{
				UA.EquipmentService.instance.getByBuildingAndClass(buildingId, classId).setOn =
				Function.bindAll
				(
					this,
					{
						success:function(equipment)
						{
							///<param name="equipment" type="o.TArray" elementType="UA.EquipmentObj" />

							equipment.forEach
							(
								function(e)
								{
									//UA.BuilderEnviornment.instance._equipment[e.id] = e;

									e = this._getEquipmentObj(e)
									
									this._view.addEquipment(buildingId, classId, e.name, e.id)

								}.bind(this))

							this._view.addEquipment(buildingId, classId) //revise this method of setting isLoading=false
						},

						failure:function(err)
						{
							this._view.promptError("Equipment could not be loaded")
						}					
					}
				)
			}

		this._view_equipExpanded =
			function(buildingId, classId, equipmentId) //, isAssociatedEqpmnt)
			{
				UA.EquipmentService.instance.getAssociatedEquipmentByEquipment(equipmentId).setOn =
				Function.bindAll
				(
					this,
					{
						success:function(equipment)
						{
							///<param name="equipment" type="o.TArray" elementType="UA.EquipmentObj" />

							equipment.forEach
							(
								function(e)
								{
									//UA.BuilderEnviornment.instance._equipment[e.id] = e;

									e = this._getEquipmentObj(e)
									
									this._view.addAssociatedEquipment(buildingId, classId, equipmentId, e.id, e.name)
								
								}.bind(this)
							)
						},
						failure:this._view.promptNotification.getArgsCaller(["Associated equipment could not be loaded if any exists."])						
					}
				)
				
				UA.PointService.instance.getByEquipmentId(equipmentId).setOn =
				Function.bindAll
				(
					this,
					{
						failure:function(err)
						{
							var msg = "An error occurred retrieving the points."

							var ref = REF;
							var inDebug = iCL.Url.NEW(document.location).query.tryGet("inDebug", ref)
							if (inDebug) {err.message;}

							this._view.promptError(msg)					
						},
						success:function(points)
						{
							///<param name="points" type="Array" elementType="UA.Point" />

							points.forEach
							(
								function(p)
								{
									this._addPoint(equipmentId, p)
									
									this._view.addPoint(buildingId, classId, equipmentId, p.name, p.id)
								
								}.bind(this)
							)

							this._view.addPoint(buildingId, classId, equipmentId) //revise this method of setting isLoading=false
						}
					}
				)
			}

		this._view_eqpmntAssociatedExpanded =
			function(buildingId, equipmentClassId, parentEquipmentId, equipmentId)
			{
				UA.PointService.instance.getByEquipmentId(equipmentId).setOn =
				Function.bindAll
				(
					this,
					{
						failure:function(err)
						{
							var msg = "An error occurred retrieving the points."

							var ref = REF;
							var inDebug = iCL.Url.NEW(document.location).query.tryGet("inDebug", ref)
							if (inDebug) {err.message;}

							this._view.promptError(msg)					
						},
						success:function(points)
						{
							///<param name="points" type="Array" elementType="UA.Point" />

							points.forEach
							(
								function(point)
								{
									this._addPoint(equipmentId, point)
									
									this._view.addAssociatedPoint(buildingId, equipmentClassId, parentEquipmentId, equipmentId, point.id, point.name)
								
								}.bind(this)
							)

							this._view.addAssociatedPoint(buildingId, equipmentClassId, parentEquipmentId, equipmentId) //revise this method of setting isLoading=false
						}
					}
				)
			}

		this._addPoint = //revise
			function(equipmentId, point)
			{
				var key = EquipmentTreePresenter.createUniquePointPath(equipmentId, point.id)

				this._points.hasKey(key)||this._points.add(key, point) //only need to check hasKey because of Associated-Equipment?
			}


		this._view_pointSelected =
			function(buildingId, equipmentId, pointId, autoSelect)
			{
				var point = this._points.item(EquipmentTreePresenter.createUniquePointPath(equipmentId, pointId))

				var dateRange;
				
				if (autoSelect)
				{
					var target = document.location.querystring["target"] //revise

					var p = target.points.first({id:pointId})

					if (p.f && p.t)
					{
						dateRange = iCL.DateRange.NEW(new Date(parseInt(/[0-9]+/.exec(p.f)[0])), new Date(parseInt(/[0-9]+/.exec(p.f)[0])))
					}
				}
				
				//this._builderEnviornment.visuDisplayPresenterManager.activePresenter.addRawPoint(point, this._builderEnviornment.presenterManager.visuDisplayManagerPresenter.dateRange, buildingId, equipmentId)
                this._builderEnviornment.visuDisplayPresenterManager.activePresenter.addRawPoint(point, null, buildingId, equipmentId)
			}

		this._msg_ideLoaded =
			function()
			{
				//
				//revise

				var sortedBuildings = []
				var x = {}

				this._builderEnviornment.siteUser.buildings.forEach
				(
					function(de)
					{
						sortedBuildings.push(de.value.name)

						x[de.value.name] = de.value;
					}
				)

				sortedBuildings = sortedBuildings.sort()

				for (var i=0; i<sortedBuildings.length; i++)
				{
					var building = x[sortedBuildings[i]]

					this._view.addBuilding(building.name, building.buildingId, ((i == (sortedBuildings.length-1)) ? true : undefined))
				}

				//

				var target = document.location.querystring["target"]

				if (!target) return;
				
				this._view.selectPoints(target.bid, target.classId, target.eid, target.points)
			}
	}
)


//var x = ["target=" +  encodeURIComponent(JSON.stringify(   {bid:1,classId:36,eid:16,points:[{id:516,f:"\/Date(1424149200000)\/",t:"\/Date(1424235600000)\/"}]}   )   )  ]
//var x = ["target=" +  encodeURIComponent(JSON.stringify(   {bid:1,classId:36,eid:16,}   )   )  ]
