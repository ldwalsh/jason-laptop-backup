﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function SubroutinePropsPresenter(builderEnviornment)
	{
		var $base = this.constructor.prototype;

		//

		this._view					= UA.views.SubroutinePropsWView.NEW()
		this._currentSubroutineObj	= UA.SubroutineObj.VAR;
		
		//

		this._init =
			function()
			{
				this._messageListner.subscriptions.add(UA.bus.BMTypeEnum.ViewSubroutineProps, this.show)

				//this._wireToView()

				UA.EquipmentService.instance.getClasses().wireTo(this) //revise to use UA.EquipmentClass.classes

				this._view.addEngUnits(UA.evaluation.EvaluationEngine.engUnits)
			}

		this._getClassesCallback =
			function(equipClasses)
			{
				///<param name="equipClasses" type="Array" elementType="UA.EquipmentClass" />

				this._view.addEquipClassToList("", -1)

				for (var i=0; i<equipClasses.length; i++)
				{
					var ec = equipClasses[i]

					this._view.addEquipClassToList(ec.name, ec.id)
				}
			}

		this.show =
			function(msg)
			{
				///<param name="msg" type="UA.bus.SubroutineBM" />

				var subroutineObj = msg.subroutineObj;
				
				this._currentSubroutineObj = subroutineObj;
				
				this._view.title			= subroutineObj.name+" Properties";
				this._view.subroutineName	= subroutineObj.name;
				this._view.inputUnit		= subroutineObj.inputEngUnit;
				this._view.outputUnit		= subroutineObj.outputEngUnit;

				this._view.equipment		= subroutineObj.equipment;

				//

				$base.show.apply(this)
			}

		this._saveProps =
			function()
			{
				//var equipment = this._view.equipment;

				//var changed = false;

				//for (var i=0; i<equipment.length; i++)
				//{
				//	var e = equipment.item(i)

				//	if (this._currentSubroutineObj.equipment.singleOrNull({"alias":e.value})) continue;

				//	changed = true;
				//}

				//if (changed)
				//{
				//	var x = []

				//	for (var i=0; i<equipment.length; i++)
				//	{
				//		var e = equipment.item(i)

				//		var so_ec = new UA.SubroutineObj_EquipmentClass()

				//			so_ec.alias = e.value;
				//			so_ec.equipmentClassID = 3;

				//		x.push(so_ec)
				//	}
				//}

				//

				UA.ObjectUpdater.instantiate(this._currentSubroutineObj).update
				(
					{
						name:				this._view.subroutineName,
						inputEngUnit:		this._view.inputUnit,
						outputEngUnit:		this._view.outputUnit,
					}
					
				).setOnReturn(this._updateCallback)

				


//debugger;
//				UA.SubroutineService.instance.updateEquipment(this._currentSubroutineObj.id, x)


			}

		this._updateCallback =
			function(props)
			{
				if (props.name)
				{
					builderEnviornment.mbus.send(UA.bus.SubroutineBM.NEW(UA.bus.BMTypeEnum.RenameSubroutine, this._currentSubroutineObj))
				}
			}
	}
)
