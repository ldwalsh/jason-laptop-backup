﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
		globalize: {},
	},

	function VisuDisplayManagerPresenter(builderEnviornment) //rename
	{
		this._view            = UA.views.VisuDisplayManagerWView.VAR;
		this.presenterManager = UA.VisuDisplayPresenterManager.VAR;
		this.dateRange        = iCL.DateRange.VAR; //duplicate of View?

		this.Cstr =
			function()
			{
				this._view			  = UA.views.VisuDisplayManagerWView.NEW()
				this.presenterManager = this._builderEnviornment.visuDisplayPresenterManager;
				
				this._messageListner.addSubscription(UA.bus.BMTypeEnum.VisuOpened, this._msg_visuOpened.getArgsCaller([Function.prototype.getArgsCaller.placeHolder, true]))
				this._messageListner.addSubscription("VisuRenamed", function(data){this._view.visuRenamed(data.name)}.bind(this))
			}
		
		this._msg_visuOpened =
			function(visu)
			{
				///<param name="visu" type="UA.VisuObj" />

				var p = this.presenterManager.createPresenter(visu)

				this._view_selectVisu(this.presenterManager.presenters.length-1)

				p.loaded = this._updateView;//p.loaded.attach(this._updateView) /////////////////////////
				
				this._view.visuOpened(visu.name, visu.dateRange)
			}

		this._createVisu = //move some of this logic out?
			function()
			{
				var presenters = this.presenterManager.presenters;

				var increment = 1;

				presenters.forEach //move this logic to VisuDisplayPresenterManager!
				(
					function(p)
					{
						var n = p.visuDisplay.visu.name;

						if (!n.match(UA.VisuDisplay.defaultVisuNameRegex)) return;

						var i = parseInt(n.split(" ")[1])

						increment = ((i >= increment) ? (i+1) : increment)
					}
				)

				this._msg_visuOpened(UA.VisuObj.NEW((UA.VisuDisplay.defaultVisuName + " " + increment), this._builderEnviornment.siteUser.clientId, this._builderEnviornment.siteUser.id))
			}

		this._closeVisu =
			function()
			{
				var p = this.presenterManager.activePresenter; //assuming only activePresenter can be closed!
				var v = p.visuDisplay.visu;

				if ((v.isSaved && p.visuNeedsSaving) || (!v.isSaved && v.hasPlots))
				{
					Function.invoke
					(
						function()
						{
							this._view.promptQuestion
							(
								"Close the visualization without saving changes?", function(option){return ((option == "Continue") ? _rU(this._doCloseVisu(p)) : undefined)}.bind(this), ["Continue", "Cancel"]
							)

						}.bind(this)
					)
				}
				else this._doCloseVisu(p)
			}

		this._doCloseVisu =
			function(presenter)
			{
				///<param name="presenter" type="o.VisuDisplayManagerPresenter" />
				
				this._view.visuClosed()
				
				this.presenterManager.removePresenter(presenter)
	
				if (this.presenterManager.presenters.length) return _rU(this._view.visuSelected(0))
				
				this._createVisu() //visuOpened
			}

		this._view_selectVisu =
			function(index)
			{
				this.presenterManager.activePresenter = this.presenterManager.presenters.item(index)
				
				this._updateView()
			}

		this._updateView =
			function()
			{
				var canWork = (this._builderEnviornment.siteUser.roleId < UA.SiteUser.roles.PublicUser)

                this._view.updateViewState(this.presenterManager.activePresenter.getStateInfo().SET({canSave:canWork, canOpen:canWork, canEdit:canWork,}))
			}
		
		this._saveVisu =
			function()
			{
				var v = this.presenterManager.activePresenter.visuDisplay.visu;
				
				if ((v.ownerId != this._builderEnviornment.siteUser.id) && (!v.sharedWrite)) return _rU(this._view.promptNotification("The visualization is not shared for write access by its owner."))

				if (!v.hasPlots) return _rU(this._view.promptNotification("The current visualization has no plots to save."))
				
				var save =
					function()
					{
						var WaitBox = iCL.ui.windows.WaitBox;

						WaitBox.open("Saving: " + v.name) //move to view!

						var vd = this.presenterManager.activePresenter.visuDisplay;vd._visuLastSaved = vd._changeCounter; //hack
						
						UA.VisuService.instance.save(v).setOn =
						Function.bindAll
						(
							this,
							{
								success:function(id){return _rU(v.id = id)},
								failure:function(err)
								{
									if (err instanceof iCL.ServerExceptionError)
									{
										if (err.message == "already deleted")
										{
											return this._view.promptQuestion
											(
												"The visualization was deleted by another user since being opened. Would you like to re-create this visualization.",
												{Yes:function(){v.SET({id:0, ownerId:this._builderEnviornment.siteUser.id, sharedWrite:false,});save()}.bind(this),},
												["Yes", "Cancel"]
											)
										}
									}
									
									this._view.promptError("An error occurred saving the visualization.")
								},
								finally:iCL.ui.windows.WaitBox.close,
							}
						)

					}.bind(this)
				
				if (v.isSaved || !v.name.match(UA.VisuDisplay.defaultVisuNameRegex)) return _rU(save())

				this._view.promptQuestion(("Save this visualization with the default name '" + v.name + "'?"), {"Save":save}, ["Save", "Cancel"])
			} //move out to dedicated VisuSaver class

		this._view_renamVisu =
			function(name)
			{
				///<param name="name" type="String" />
				
				//validate and return false if name invalid... notify view
				
				this.presenterManager.activePresenter.visuDisplay.visu.name = name;

				return true;
			}

		this._manageAction = //rename manageCommand
			function(command)
			{
                var p = this.presenterManager.activePresenter;

                Function.invoke
                (
                    UA.presenters.VisuDisplayManagerPresenter.ActionsEnum.SELECT
                    (
                        (command=UA.presenters.VisuDisplayManagerPresenter.ActionsEnum[command]),
                        {
                            Close:          this._closeVisu,
                            New:            this._createVisu,
                            Save:           this._saveVisu,
                            Open:           function(){UA.presenters.OpenVisuPresenter.NEW(this._builderEnviornment).SET({show:null})}.bind(this),
                            Props:          function(){UA.presenters.AnalysisPropertiesPresenter.NEW(this._builderEnviornment, p.visuDisplay.visu).show()}.bind(this),
                            ExportPDF:      p.export.bindArgs(UA.ExportTypeEnum.PDF),
                            ExportCSV:      p.export.bindArgs(UA.ExportTypeEnum.CSV),
                            CreateExpr:     p._viewHandler.createExpression, //revise to route through presenter NOT directly to view                            
                            ViewLine:       SELECT.next,
                            ViewTable:      SELECT.next,
                            ViewScatter:    function(){p.viewMode = UA.ViewModesEnum[command.toString().substring(4)]},                            
                            RangeChange:    function(){this.presenterManager.activePresenter.loadData(this.dateRange=this._view.dateRange)}.bind(this),
                            DrillOut:       SELECT.next,
                            DrillIn:        SELECT.next,
                            AnnotationsOff: SELECT.next,
                            AnnotationsOn:  SELECT.next,
                            PanOn:          SELECT.next,
                            PanOff:         p.managerCommand.bindArgs(command),                            
                            default:        Function.empty
                        }
                    )
                )
				
				this._updateView() //best approach?
			}
	}
)







Enum.define
(
	{
		namespace: UA.presenters.VisuDisplayManagerPresenter,
	},

	function ActionsEnum()
	{
		Close, New, Save, Open, Props, ExportPDF, ExportCSV, CreateExpr, ViewLine, ViewTable, ViewScatter, DrillOut, DrillIn, RangeChange, PanOn, PanOff, AnnotationsOn, AnnotationsOff
	}
)



UA.presenters.VisuDisplayManagerPresenter.ActionsEnum.defineProp2
(    
    {
        SELECT:
        {
            configurable:false,
            enumerable:false,
            value:
            function(value, selects)
            {
                if (!selects.__validated)
                {
                    var keys = Object.keys(this)

                    Error.ifThrow(Object.keys(selects).for(function(_, ix){return ((_ == "default") || keys.contains(_)) ? undefined : new Error("The Enum does not have a value: " + Object.keys(selects)[ix])}))

                    this.defineProp2({__validated:{configurable:false, enumerable:false, value:true}})
                }

                return SELECT(value, selects)
            }
        }
    }
)


Error.ifThrow = function(r){if (r instanceof Error) throw r;}

Array.prototype.for =
function(predicate)
{
    for (var i=0; i<this.length; i++)
    {
        var r = predicate(this[i], i)

        if (r != undefined) return r;
    }
}



Enum.define
(
	{
		namespace: UA,
	},

	function ExportTypeEnum()
	{
		PDF, CSV
	}
)

