﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function AnalysisEquipmentAssociationPresenter(bE)
	{
		var $base = this.constructor.prototype;

		//

		this._view			   = UA.views.AnalysisEquipmentAssociationWView.NEW() //should be handled by base
		this._currentProfileId = Number.VAR;

		//

		this._init =
			function()
			{
				this._messageListner.wireTo(this)
				
				//
				
				var arr = [] //revise below to use Array.transform+Object.transform
				for (var i=0; i<UA.EquipmentClass.classes.length; i++)
				{
					var klass = UA.EquipmentClass.classes.item(i)
					arr.push(HTMLSelectElement.OptionObject.NEW(klass.name, klass.id))
				}
				this._view.populateEquipmentClasses(arr)
			}

		this.show =
			function()
			{
				this._populateAssociationProfiles()

				$base.show.call(this)
			}

		this._addProfile =
			function(name)
			{
				UA.EquipmentService.instance.createEquipmentAssociationProfile(this._analysisObj.id, name).onReturn.attach
				(
					function(obj)
					{
						this._analysisObj.equipmentAssociationProfiles.push(obj)
						
						this._populateAssociationProfiles()

					}.bind(this)
				)
			}

		this._populateAssociationProfiles =
			function()
			{
				var optionObjs = []

				optionObjs.push(HTMLSelectElement.OptionObject.empty)

				for (var i=0; i<this._analysisObj.equipmentAssociationProfiles.length; i++)
				{
					var profile = this._analysisObj.equipmentAssociationProfiles.item(i)

					optionObjs.push(HTMLSelectElement.OptionObject.NEW(profile.name, profile.id))
				}

				this._view.populateAssociationProfiles(optionObjs)
			}

		this._profileSelected =
			function(profileId)
			{
				this._currentProfileId = profileId;

				this._view.clearEquipmentAssociations()

				if (profileId == -1) return;

				var profile = this._analysisObj.equipmentAssociationProfiles.single({id:profileId})

				for (var i=0; i<profile.equipmentAssociations.count; i++)
				{
					var assoc = profile.equipmentAssociations.item(i)

					this._view.addEquipmentAssociation(assoc.id, assoc.alias, assoc.equipmentClassId, assoc.equipmentId)
				}
			}

		this._buildEquipmentList =
			function(classId)
			{
				var optionObjs = new $.TArray(HTMLSelectElement.OptionObject)

				for (var i=0; i<bE.equipmentObjs.length; i++)
				{
					var eObj = bE.equipmentObjs.item(i)

					if (eObj.classId != classId) continue;

					optionObjs.push(HTMLSelectElement.OptionObject.NEW(eObj.name, eObj.id))
				}

				return optionObjs;
			}

		this._associationChanged =
			function(assocId, alias, equipmentId)
			{
				///<param name="assocId" type="Number" />
				///<param name="alias" type="String" />
				///<param name="equipmentClassId" type="Number" />
				///<param name="equipmentId" type="Number" />

				if (assocId == 0)
				{
					var assoc			  = UA.EquipmentAssociation.NEW()
						assoc.alias		  = alias;
						assoc.equipmentId = equipmentId

					this._analysisObj.equipmentAssociationProfiles.single({id:this._currentProfileId}).equipmentAssociations.add(assoc).onReturn.attach
					(
						function()
						{	
							this._profileSelected(this._currentProfileId)

						}.bind(this)
					)
				}
				

				//if (!alias.trim() || (equipmentId == -1)) return;

				//if (associationId == 0)
				//{
				//	UA.EquipmentService.instance.createEquipmentAssociation(this._currentProfileId, alias, equipmentId).setOnSuccess
				//	(
				//		function(equipAssoc)
				//		{
				//			///<param name="equipmentAssociation" type="UA.EquipmentAssociation" />
							
				//			this._analysisObj.equipmentAssociationProfiles.single({id:this._currentProfileId}).equipmentAssociations.push(equipAssoc)

				//			this._profileSelected(this._currentProfileId)

				//		}.bind(this)
				//	)
				//}
				//else
				//{
				//	UA.EquipmentService.instance.updateEquipmentAssociation(associationId, alias, equipmentId).setOnSuccess
				//	(
				//		function()
				//		{
				//			var assoc = this._analysisObj.equipmentAssociationProfiles.single({id:this._currentProfileId}).equipmentAssociations.single({id:associationId})

				//			assoc.alias		  = alias;
				//			assoc.equipmentId = equipmentId;

				//		}.bind(this)
				//	)
				//}
			}

		this._deleteAssociation =
			function(assocId)
			{
				this._analysisObj.equipmentAssociationProfiles.single({id:this._currentProfileId}).equipmentAssociations.remove(assocId).setOnReturn
				(
					function(r)
					{
						if (r instanceof Error)
						{
							$.ui.windows.MessageBox.open(Error.CAST(r).message)

							return;
						}

						this._view.removeEquipmentAssociation(assocId)

					}.bind(this)
				)
			}
	}
)
