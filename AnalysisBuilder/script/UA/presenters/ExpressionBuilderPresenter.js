﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
		statics:
		{
			defaultExpressionName:		"MyExpression",
			defaultExpressionNameRegEx: /^MyExpression[1-9][0-9]*$/,
		},
	},

	function ExpressionBuilderPresenter(builderEnviornment)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />

		this._view           = UA.views.ExpressionBuilderView.VAR;
		this.plots           = iCL.TArray.VAR(UA.charting.PlotBase)
		this.expressionPlot  = UA.charting.ExpressionPlot.VAR;			//when editing existing
		this.expressionSaved = DELEGATE("expressionPlot")
		this._usedAliases    = Array.VAR;

		this.Cstr =
			function()
			{
				this._usedAliases = []

				this._view = UA.views.ExpressionBuilderView.NEW()
			}

		this.show =
			function()
			{
				ExpressionBuilderPresenter.prototype.show.apply(this, arguments)

				var tempAddedPoints = []

				if (this.expressionPlot)
				{
					this._view.title      = this.expressionPlot.title;
                    this._view.expression = this.expressionPlot.expression.text;
					
					for (var i=0; i<this.plots.length; i++)
					{
						var plot = this.plots.item(i)

						if ((plot == this.expressionPlot) || (plot instanceof UA.charting.ExpressionPlot)) continue;

						plot = UA.charting.RawDataPlot.CAST(plot);
						
						if (tempAddedPoints.contains(plot.point.id)) continue;

						var alias =
							this.expressionPlot.pointAliases.FOR
							(
								function(de)
								{
									this._usedAliases.push(de.key)

									if (de.value == plot.point.id) return de.key;

								}.bind(this)
							)

						if (!alias)
						{
							alias = (plot.alias || this._makeupAlias())
						}

						this._view.addPlotAlias(plot.point.id, plot.displayName, alias, plot.color, plot.equipmentName)

						tempAddedPoints.push(plot.point.id)
					}
				}
				else
				{
					var nameIncrement = 1;

					for (var i=0; i<this.plots.length; i++)
					{
						var plot = this.plots.item(i)

						if (plot instanceof UA.charting.ExpressionPlot)
						{
							//
							//handle default title of new expression

							if (ExpressionBuilderPresenter.defaultExpressionNameRegEx.test(plot.title))
							{
								var x = parseInt(plot.title.split(ExpressionBuilderPresenter.defaultExpressionName)[1])

								if (x >= nameIncrement)
								{
									nameIncrement = (x + 1)
								}
							}
						
							continue;
						}
						
						plot = UA.charting.RawDataPlot.CAST(plot)

						if (tempAddedPoints.contains(plot.point.id)) continue;

						this._view.addPlotAlias(plot.point.id, plot.displayName, (plot.alias || this._makeupAlias()), plot.color, plot.equipmentName)

						tempAddedPoints.push(plot.point.id)
					}

					this._view.title = (ExpressionBuilderPresenter.defaultExpressionName + nameIncrement)
				}

				//

				//if (!ExpressionBuilderPresenter.functionsAdded)
				//{
					
					this._view.addFunction("Math", "sum", "sum(x, y [, z, ... n])", "Adds numeric arguments and returns the result.")
					this._view.addFunction("Math", "product", "product(x, y [, z, ... n])", "Multiplies numeric arguments and returns the result.")
					this._view.addFunction("Math", "quotient", "quotient(numerator, denominator)", "Divides the numerator by denominator and returns the result.")
					
					this._view.addFunction("Math", "pow" /*exp, power*/, "power(number, power)", "Returns the result of a number raised to a given power.")

					this._view.addFunction("Math", "mod", "mod(number, divisor)", "Returns the remainder after the number is divided by the divisor.")

					this._view.addFunction("Math", "max", "max(x, y [, z, ... n])", "Returns the largest value from the numeric arguments.")
					this._view.addFunction("Math", "min", "min(x, y [, z, ... n])", "Returns the smallest value from the numeric arguments.")

					this._view.addFunction("Math", "average" /*mean*/, "average(x, y [, z, ... n])", "Returns the average (arithmetic mean) of the numeric arguments.")
					this._view.addFunction("Math", "median", "median(x, y [, z, ... n])", "Returns the median value of the numeric arguments.")
					//this._view.addFunction("Math", "MODE", "median(x, y [, z, ... n])", "Returns the median value of the numeric arguments.")

					this._view.addFunction("Math", "floor", "floor(x)", "Returns the numeric argument rounded down to the nearest integer.")
					this._view.addFunction("Math", "ceiling", "ceiling(x)", "Returns the numeric argument rounded up to the nearest integer.")

					this._view.addFunction("Math", "abs", "abs(x)", "Returns the absolute value of the numeric argument.")

					//this._view.addFunction("EVEN", this._createTooltip("min(x, y [, z, ... n])", "Returns the smallest value from the numeric arguments."))


					this._view.addFunction("Conditional", "and", "and(condition1, condition2 [, condition3, ... n])", "Returns true if ALL conditions are true, otherwise returns false.")
					this._view.addFunction("Conditional", "or", "or(condition1, condition2 [, condition3, ... n])", "Returns true if ANY conditions are true, otherwise returns false.")
					this._view.addFunction("Conditional", "xor", "xor(condition1, condition2 [, condition3, ... n])", "Returns true if ANY conditions are true. Returns false if ALL or no conditions are true.")
					this._view.addFunction("Conditional", "if", "if(condition, true_value, false_value)", "Returns the true value if condition is true, otherwise returns false.")
					//this._view.addFunction("SWITCH", this._createTooltip("if(condition, true_value, false_value)", "Returns the true value if condition is true, otherwise returns false."))

					//this._view.addFunction("SMALL", this._createTooltip("if(condition, true_value, false_value)", "Returns the true value if condition is true, otherwise returns false."))
					//this._view.addFunction("SIGN" /*SGN*/, this._createTooltip("if(condition, true_value, false_value)", "Returns the true value if condition is true, otherwise returns false."))

					//this._view.addFunction("ROUNDUP" /*SGN*/, this._createTooltip("if(condition, true_value, false_value)", "Returns the true value if condition is true, otherwise returns false."))
					//this._view.addFunction("ROUNDDOWN" /*SGN*/, this._createTooltip("if(condition, true_value, false_value)", "Returns the true value if condition is true, otherwise returns false."))
					//this._view.addFunction("ROUND" /*SGN*/, this._createTooltip("if(condition, true_value, false_value)", "Returns the true value if condition is true, otherwise returns false."))

					//PI
					

					//ExpressionBuilderPresenter.functionsAdded = true;
				//}
			}

		this._makeupAlias = //move to UA.expression.AliasUtility
			function()
			{
				var alias, keyCode=64;

				while (keyCode < 90)
				{
					alias = String.fromCharCode(++keyCode).toLowerCase()

					if (this.plots.search({alias:alias}).length || this._usedAliases.contains(alias)) continue;

					this._usedAliases.push(alias)

					return alias;
				}

				throw new Error(8645533333333333)
			}

		this._saveExpression =
			function(title, expression, plotAliases)
			{
				///<param name="title" type="String" />
				///<param name="expression" type="String" />
				///<param name="plotAliases" type="iCL.collections.Dictionary" />

				//
				if (window.intellisense)
				{
					plotAliases._cstr = Number;
				}
				//

				var plotWithSameTitle = (this.plots.singleOrNull({title:title}))
				
				if (plotWithSameTitle && (plotWithSameTitle != this.expressionPlot)) return "A plot with the same title already exists."; //return error code instead not actual msg!

				var refMsg = REF;

				if (plotAliases.FOR(function(de){if (UA.expression.AliasUtility.invalidatePointAlias(de.key, refMsg)) return true;})) return refMsg.value;

				//

				var range = this._builderEnviornment.presenterManager.visuDisplayManagerPresenter.presenterManager.activePresenter.visuDisplay.visu.dateRange; //revise!
                var plot  = (this.expressionPlot ? this.expressionPlot : UA.charting.ExpressionPlot.NEW(range, title)) //move to VisuDisplay!!!

				//var points = iCL.collections.Dictionary.NEW(UA.Point)


//var interval = 0;

//this.plots.FOR
//(
//	function(plot)
//	{
//		if (!(plot instanceof UA.charting.RawDataPlot)) return;

//		//

//		//point = UA.charting.RawDataPlot.CAST(plot).point;

//		//points.add(point.id, point)

//		//

//		if (!plot.data) return false; //in-case of opened expression... revise

//		if (!plot.data.item(1) || !plot.data.item(0)) return false; //in-case datasource empty

//		var ival = ((plot.data.item(1)[0] - plot.data.item(0)[0]) / 60000) //use concrete type here
//		if (interval && (ival <= interval)) return;
//		interval = ival;
//	}
//)

				try //revise this overbearing try... also should test expression before instanting ExpressionPlot so can pass in interval and other params
				{
					var testExpression = UA.expression.Expression.NEW(expression, true)
				}
				catch(e)
				{
					var err = "The expression is invalid.";
					
					if (e instanceof UA.expression.InvalidFragmentError)
					{
						err += ("<br /><br />Error:&nbsp;" + "Division by 0")
					}

					return err;
				}

				//

				plot.pointAliases.clear()

				var failedAliasCheck =
					testExpression.variables.FOR //revise FOR
					(
						function(alias)
						{
							var refPointId = REF;

							if (!plotAliases.tryGet(alias, refPointId)) return ("The point alias '" + alias +"' used in the expression is not defined")

							//if (!plot.pointAliases.hasKey(alias))
							//{
							//	plot.pointAliases.add(alias, refPointId.value)
							//}

							if (plot.pointAliases.hasKey(alias)) return;
							
							plot.pointAliases.add(alias, refPointId.value)
						}
					)

				if (failedAliasCheck) return failedAliasCheck;

				plot.title		= title; //reduntant if new expression was created using constructor... revise?
				plot.expression = testExpression;

				//plot._interval	= interval; //hack should not access private member but pass instead!

				this.expressionSaved(plot)
			}
	}
)
