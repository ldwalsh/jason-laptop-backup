﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase
	},

	function OutputPresenter(builderEnviornment)
	{
		this._view = UA.views.OutputWView.NEW()

		//

		this._init =
			function()
			{
				this._messageListner.subscriptions.add(UA.bus.BMTypeEnum.EvaluateAnalysis, this._msgEvaluateAnalysis)
			}

		this._msgEvaluateAnalysis =
			function(msg)
			{
				///<param name="msg" type="$.MessageBus.Message" />

				var evaluationEngine = msg.getData(UA.evaluation.EvaluationEngine)

				evaluationEngine.outputStream = this._view.writeOutput;

				this.show()
			}
	}
)

