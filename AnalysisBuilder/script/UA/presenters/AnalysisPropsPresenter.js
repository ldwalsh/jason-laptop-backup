﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function AnalysisPropsPresenter(bE, viewCstr)
	{
		///<param name="bE" type="UA.BuilderEnviornment" />

		this._view = UA.views.AnalysisPropsView.NEW()

		//

		this._init =
			function()
			{
				//this._wireToView();

				this._messageListner.subscriptions.add(UA.bus.BMTypeEnum.ViewAnalysisProps)
				this._messageListner.wireTo(this, true)
			}

		this._saveProps =
			function()
			{
				var userVariables = "";
				
				for (var i=0; i<this._view.userVariables.length; i++)
				{
					userVariables += (UA.AnalysisObj2.userVariableDelimeter + this._view.userVariables[i].text)
				}

				userVariables = userVariables.substr(3)
				
				UA.ObjectUpdater.instantiate(bE.analysisManager.currentAnalysisObj).setOnReturn(this._updateCallback).update
				(
					{
						"name":			 this._view.analysisName,
						"userVariables": userVariables,
					}
				)
			}

		this._updateCallback =
			function(props)
			{	
				if (props.name)
				{
					bE.mbus.send($.MessageBus.Message.NEW(UA.bus.BMTypeEnum.RenameAnalysis, props.name))
				}
			}

		this._msg_viewAnalysisProps =
			function()
			{
				var analysisObj = bE.analysisManager.currentAnalysisObj;

				this._view.clearView()

				this._view.analysisName  = analysisObj.name;
				this._view.userVariables = analysisObj.userVariables;

				this.show()
			}
	}
)
