﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters,
	},

	function PresenterBase(builderEnviornment)
	{
		this.__view              = iCL.ui.ViewBase.VAR;
		this._builderEnviornment = UA.BuilderEnviornment.VAR;
		this._messageListner     = iCL.MessageBus.MessageListener.VAR;

		this.Cstr =
			function()
			{
				this._messageListner = iCL.MessageBus.MessageListener.NEW(this._builderEnviornment.mbus, this._receiveBusMsg)
			}

		this.Dispose =
			function()
			{
				this._view.Dispose()
			}

		this._wireToView =
			function()
			{
				if (this._viewHandler) //cleanup/consildate
				{
					Object.getDescriptors(this.__view).forEach //move to constructor?  //move to base?
					(
						function(d)
						{
							if (!(d.set && d.set.isDelegate)) return; //revise

							var n = d.name;

							var f = this._viewHandler[n]

							if (!f) return;

							this.__view[n] = f;
			
						}.bind(this)
					)
				}
				else
				{
					Object.getDescriptors(this.__view).forEach //move to constructor?  //move to base?
					(
						function(d)
						{
							if (!(d.set && d.set.isDelegate)) return; //revise

							var n = d.name;

							var f = this["_view_" + n]

							if (!f) //remove?!
							{
								f = this["_" + n.removeStart("_")] //revise: use dedicated ViewDelegate w/ special naming prefix!

								//f = function()
								//{
								//	this["_" + n.removeStart("_")].invoke(arguments)

								//}.bind(this)
							}
							
							if (!f) return;

							this.__view[n] = f;							
							//this.__view[n] = function(){return this[f.methodName].apply(null, arguments)}.bind(this)
							//this.__view[n] = function(){return f.THIS[f.methodName].apply(null, arguments)}
			
						}.bind(this)
					)
				}
			}

		this._receiveBusMsg = //remove?
			function()
			{
			}

		this.show =
			function()
			{
				//this._wireToView()
				
				this._view.show()
			}

		this.hide =
			function()
			{
				this._view.hide()
			}

		this._view_get =
			function()
			{
				return this.__view;
			}

		this._view_set =
			function(value)
			{
				this.__view = value;

				this._wireToView()
			}
	}
)
