﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function SubroutineEditorPresenter(buildingEnviornment, subroutineObj)
	{
		///<param name="buildingEnviornment" type="UA.BuilderEnviornment" />
		///<param name="subroutineObj" type="UA.SubroutineObj" />

		this._view = UA.views.SubroutineEditorWView.NEW(subroutineObj.name, subroutineObj.id)

		//

		this._init =
			function()
			{
				this._wireToView()

				this._view.populateDefinition(subroutineObj.definition)
			}

		this._subroutineChanged =
			function(definition)
			{
				//validate definition

				subroutineObj.definition = definition;
			}
	}
)
