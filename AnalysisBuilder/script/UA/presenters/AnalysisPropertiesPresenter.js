﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function AnalysisPropertiesPresenter(builderEnviornment, visu)
	{
		this._view    = UA.views.AnalysisPropertiesView.VAR;
		this._visu    = UA.VisuObj.VAR;
		this._canEdit = Boolean.VAR;
		this._sharing = Boolean.VAR;
		this._fields  = iCL.TArray.VAR(Object.AttributeDescriptor) //rename
		this._fields2 = iCL.TArray.VAR(Object.AttributeDescriptor) //rename

		this.Cstr =
			function()
			{
				this._view = UA.views.AnalysisPropertiesView.NEW()

				this._canEdit = (!this._visu.id || (this._visu.ownerId == this._builderEnviornment.siteUser.id) || this._visu.sharedWrite)
				this._sharing = ((this._visu.ownerId == this._builderEnviornment.siteUser.id) || !this._visu.ownerId)

				//
				
				var ds = Object.getDescriptors(UA.VisuObj.NEW()).lookup; //should use cached
				
				this._fields  = [ds.id, ds.name, ds.description, ds.created, ds.modified, ds.sharedRead, ds.sharedWrite, ds.notes, ds.isSaved]

				this._fields2 = null;
				
				if (!this._canEdit) return;

				this._fields2 = [ds.name, ds.description, ds.notes]

				if (!this._sharing) return;
				
				this._fields2.push(ds.sharedRead )
				this._fields2.push(ds.sharedWrite)
			}

		this.show =
			function()
			{	
				this._fields.forEach //move out to iCL
				(
					function(d)
					{
						///<param name="d" type="Object.AttributeDescriptor" />
						
						var n = d.name;

						if (!n) throw new Error(324776353)

						this._view.populateField(n, this._visu[n], (d.name.startsWith("shared") ? this._sharing : this._canEdit))
					
					}.bind(this)
				)

				AnalysisPropertiesPresenter.prototype.show.apply(this)
			}

		this._propsApplied =
			function(valuesObject)
			{
				///<param name="valuesObject" type="Object" />

				if (valuesObject.name != this._visu.name)
				{
					this._builderEnviornment.mbus.send("VisuRenamed", {name:valuesObject.name,})
				}

				this._fields2.forEach
				(
					function(d)
					{
						var p= {Number:parseFloat, Boolean:Boolean.parse,}[d.cstr.name], n= d.name, v= valuesObject[n];

						this._visu[n] = (p ? p(v) : v)
					
					}.bind(this)
				)

				if (!this._visu.isSaved) return;

				var _WaitBox = iCL.ui.windows.WaitBox

				_WaitBox.open("Saving properties")
				
				UA.VisuService.instance.saveProperties(this._visu).setOnSuccess
				(
					function(error)
					{
						_WaitBox.close()

						if (error instanceof Error) return _rU(this._view.promptError("Could not save visualization properties. Please check your connection and try again."))

					}.bind(this)
				)	
			}
	}
)