﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
		attribute: ["NonPrototype"], //future feature
	},

	function OpenVisuPresenter(builderEnviornment)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />

		this._view = UA.views.OpenAnalysisView.VAR;

		this.Cstr =
			function()
			{
				this._view = UA.views.OpenAnalysisView.NEW()
				
				this._view._siteUserId = this._builderEnviornment.siteUser.id; //remove hack
			}

		this.show =
			function()
			{
				this.constructor.prototype.show.apply(this) // may need to move back out if delay noticeable ... also uses this.constructor which is unsafe (see NonPrototype above)
				
				this._addVisus()
			}

		this._addVisus =
			function()
			{
				UA.VisuService.instance.getAllIdentifiers().setOn =
				Function.bindAll
				(
					this,
					{
						success:function(identifiers)
						{
							var arr = [];this._builderEnviornment.visuDisplayPresenterManager.presenters.forEach(function(p){arr.push(p.visuDisplay.visu.id)}) //todo: combine

							var alreadyOpened = 0;

							identifiers.sort(function(a, b){return a.name.localeCompare(b.name)}).forEach
							(
								function(o)
								{
									///<param name="o" type="UA.AnalysisObjIdentifier" />
								
									if (arr.contains(o.id)) return rU(++alreadyOpened)

									this._view.addVisuToList(o.id, o.name, o.created, o.modified, o.description, o.ownerId, o.owner, o.sharedRead, o.sharedWrite, (o.ownerId == this._view._siteUserId))
								
								}.bind(this)
							)

							this._view._endList(alreadyOpened)
						},

						failure:function(error)
						{
							this.hide()
						
							this._view.promptError("Could not retrieve view list. Please check your connection and try again.")					
						},
					}
				)
			}

		this._view_visuSelected =
			function(id)
			{
				///<param name="id" type="Number" />
				
				var p = this._builderEnviornment.visuDisplayPresenterManager.presenters.firstOrNull({id:id,})
				
				if (p) return rU(this._view.promptError("The visualization '" + p.visuDisplay.visu.name + "' is already opened.")) //revise: throw error and create/show message in view?
				iCL.ui.windows.WaitBox.open("Opening visualization...") //move to view?
				
				UA.VisuService.instance.getById(id).setOn =
				Function.bindAll
				(
					this,
					{
						success:function(visu)
						{
							this._builderEnviornment.mbus.send(UA.bus.BMTypeEnum.VisuOpened, visu) //call async and use a callback when ready to hide the WaitBox
						},

						failure:function(err)
						{
							var msg = "The visualization failed to open.";
							
							msg += ((err instanceof iCL.AsyncRPCall.TimeoutError) ? " Please check your connection and try again." : "")

							this._view.promptError(msg)
						},

						finally:iCL.ui.windows.WaitBox.close.bindArgs([]),
					}
				)
			}

		this._view_visuDeleted =
			function(id)
			{
				this._view.promptQuestion
				(
					"Are you sure you want to delete the selected visualization?",
					{
						Yes:function(r)
						{
							iCL.ui.windows.WaitBox.open("Deleting visualization...") //move to view?

							UA.VisuService.instance.delete(id).setOnSuccess
							(
								function()
								{
									this._view.clearVisus()

									this._addVisus()

									iCL.ui.windows.WaitBox.close() //move to view?
								
								}.bind(this)
							)
						
						}.bind(this),
					},
					["Yes", "Cancel"]
				)
			}
	}
)
