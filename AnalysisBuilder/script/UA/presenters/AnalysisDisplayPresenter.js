﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function VisuDisplayPresenter(builderEnviornment, visuDisplay) //pass in VisuDisplay here
	{
		///<param name="visuDisplay" type="o.VisuDisplay" />

		var $charting    = UA.charting;

        this._chart = Object.VAR;  //rename to _view/_currentView		
		this._view           = UA.views.VisuDisplayWView.VAR;
		this.charts = iCL.Lookup.VAR; //this._adaptorManager = $charting.AdaptorManager.VAR;
		this._viewHandler    = null; //rename/revise	
		this.visuDisplay     = UA.VisuDisplay.VAR;
this.loaded = Function.VAR;// iCL.Event;

		this.Cstr =
			function()
			{
				this._viewHandler    = VisuDisplayPresenter.ViewHandler.NEW(this) //revise placement? pass into view?
				this._view           = UA.views.VisuDisplayWView.NEW()
				
                this._charts = iCL.Lookup.NEW(UA.charting.ChartBase) //this._adaptorManager = $charting.AdaptorManager.NEW()

				//
				
				if (this.visuDisplay.hasHiddenPlots) this._view.promptNotification("The opened view has plots associated to restricted buildings. These plots will not be displayed.")

				this.visuDisplay.plots.forEach(this._addPlotToLegend.getArgsCaller([Function.getArgsCaller.firstPlaceHolder]))
			}

		this._shown = false; //revise
		this.show =
			function()
			{
				if (this._shown) return _rU(this._view.show())
				
				this.constructor.prototype.show.apply(this)

				this._shown = true;
			}

		this.addRawPoint =
			function(point, dateRange, buildingId, equipmentId)
			{
				this._addPlotToLegend(this.visuDisplay.createPlot.apply(null, arguments)) //revise?
			}

		this._expressionSaved = //revise to use messgebus
			function(plot)
			{
				///<param name="plot" type="o.ExpressionPlot" />
										
				if (this.visuDisplay.plots.singleOrNull({id:plot.id})) return _rU(Function.invoke([this._view.changeLegend.bindArgs([plot]), this.loadData]))

				this.visuDisplay.createPlot(plot) //revise to pass in info to create plot

				this._addPlotToLegend(plot)
			}

		this._addPlotToLegend =
			function(plot)
			{
				Function.invoke([this._view.addPlot.bindArgs([plot, (this.visuDisplay.visu.xPlot == plot)]), this.loadData])
			}

		this.export =
			function()
			{
				if (!this.visuDisplay.plots.length) return _rU(this._view.promptNotification("The visualization has no points."   ))
                if (!this.visuDisplay.isLoaded)     return _rU(this._view.promptNotification("The visualization is still loading."))
                
                var extension;
                var content;

                var adaptor = this._getAdaptor()
                
                if (this.viewMode == UA.ViewModesEnum.Table)
                {
					var t = this._chart._adaptor.tableControl;

					extension = "csv";
					content   = (String.build(t.headerArray, Function.passThrough, ",") + "|" + String.build(t.rows, function(r){return String.build(r.cells, function(_){return ("\"" + _.textContent + "\"")}, ",")}, "|"))
                }
                else
                {
                    extension = "pdf";                    
                    
                    content =
                    UA.PrintRenderer.NEW
                    (
                        adaptor,
                        this.visuDisplay.plots.map(function(_){return UA.charting.ChartBase.ggg(this.viewMode)(_, this.visuDisplay.visu)}.bind(this)),
                        Object.Tr(this.visuDisplay.visu).clone(['dateRange', 'name->title', 'description->desc'])
                    
                    ).render()
                }
                
                iCL.Downloader.NEW(this.visuDisplay.visu.name.removeWhiteSpace() /**/, "handler/ExportHandler.ashx", function(err)
                    {
                        this._view.promptNotification(((err.name == "MonthlyDownloadAllowanceReachedException") ? "Your monthly download allowance has been reached." : null) || "Your document could not be downloaded due to an unexpected error.")
                
                    }.bind(this)
                
                ).initiate({fileName:(this.visuDisplay.visu.name + "." + extension), content:content})

                //var err =
                //    this.visuDisplay.export(function(err){this._view.promptNotification(((err.name == "MonthlyDownloadAllowanceReachedException") ? "Your monthly download allowance has been reached." : null) || "Your document could not be downloaded due to an unexpected error.")}.bind(this))

				//if (err) return;
				
				//this._view.promptNotification(err.message)
			}

		this.loadData =
			function(dateRange)
			{
				if (dateRange && dateRange.equalTo(this.visuDisplay.visu.dateRange)) return; //may not need if handled by date range control??
                
                this._view.setLoadingState(true)

				this.visuDisplay.loadData
				(
					dateRange,
					function(err)
					{
						if (err) return _rU(Function.invoke(this._view.promptError.bindArgs(["Point data could not be retrieved. Please check your connection."]), this._view.setLoadingState.bindArgs([false])))
						
						this._loadAndOrDisplayCurrentAdaptor()
					
					}.bind(this)
				)

				if (!dateRange) return;
				
                this.visuDisplay.plots.forEach(this._view.changeLegend.getArgsCaller([Function.getArgsCaller.firstPlaceHolder, UA.charting.PlotChangeEnum.Range]))
			}

		this._getAdaptor =
            function()
            {
                return SELECT(this.viewMode, [[UA.ViewModesEnum.Line, UA.charting.LineGrah],[UA.ViewModesEnum.Scatter, UA.charting.ScatterPlot], [UA.ViewModesEnum.Table, UA.charting.TableView]])
            }
        
        this._loadAndOrDisplayCurrentAdaptor =
			function()
			{
				//should be moved to the adaptor!!
				if ((this.viewMode == UA.ViewModesEnum.Scatter) && (this.visuDisplay.plots.length == 1))
                {
                    this._view.promptNotification("Graphing a scatter plot requires at least two points.")
                }
				//

				if (!this._chart)
				{
                    this._charts.add(this.viewMode, this._chart=this._getAdaptor().NEW(this._view.chartContainer.firstElementChild) /*revise?*/)

                    this._chart.interaction = function(){this.loaded(this)}.bind(this) //c.interaction = function(){this.loaded.raise(this)}.bind(this)
				}

                this._chart.bind(this.visuDisplay.plots.map(function(_){return UA.charting.ChartBase.ggg(this.viewMode)(_, this.visuDisplay.visu)}.bind(this)))

                this._view.setLoadingState(false)

                this.loaded(this) //this.loaded.raise(this)
			}

		this._rrr =
			function(plots, action)
			{
                ex(this._chart||{draw:Function.empty}).draw(plots.map(function(_){return UA.charting.ChartBase.ggg(this.viewMode)(_, this.visuDisplay.visu)}.bind(this)), action) //do not need to do for some actions?
			}

		this.managerCommand = //dispatchChartCommand
			function(command)
			{
                if (!this._chart) return;

                this._chart.dispatchCommand(command)
			}

		this.getStateInfo =
			function()
			{
				return {viewMode:(this._chart ? this.viewMode : null), isLoaded:this.visuDisplay.isLoaded, dateRange:this.visuDisplay.visu.dateRange}.SET(this._chart ? this._chart.getStateInfo() : null)
			}

		this.viewMode =
			{
				get:function( ){return this.visuDisplay.viewMode;},
                set:function(v)
                {
                    this._chart = this._charts[v]

                    Function.invoke([this._view.switchMode.bindArgs([this.visuDisplay.viewMode=v]), this._loadAndOrDisplayCurrentAdaptor])
                }
			}

this.isLoaded =	//revise   vs visuDisplay?
	{
		get:function(){this._chart.isLoaded;}
	}


	}
)







Object.define
(
    {
        namespace: UA.charting,
        construct:
        {
            ggg:function(viewMode){return SELECT(viewMode, [[UA.ViewModesEnum.Line, UA.charting.ChartLinePlot], [UA.ViewModesEnum.Table, UA.charting.ChartLinePlot], [UA.ViewModesEnum.Scatter, UA.charting.ChartScatterPlot]]).fromPlotBase;} //move???
        }
    },

    function ChartBase(div)
    {
        this._div        = HTMLDivElement.VAR;
        this._adaptor    = UA.charting.adaptors.ChartAdaptorBase.VAR;
        this._forPrint   = Boolean.VAR;
        this._getAdaptor = Function.VAR;

        this.Cstr =
            function()
            {
                this._forPrint = (this._div.data("forPrint").value == true)
                this._adaptor  = this._getAdaptor()
            }

        this.bind =
            function(plots)
            {
                this._adaptor.bind(plots)
            }
        
        this.draw =
            function(plots, action)
            {
                this._adaptor.draw(plots, action)
            }

        this.getStateInfo =
            function()            
            {
                return this._adaptor.getStateInfo()
            }

        this.dispatchCommand = //rename to doCommand / or??
            function(command)
            {
                this._adaptor.dispatchCommand(command)
            }

        this.interaction = //should be event!!!
            {
                set:function(v){this._adaptor.interaction = v;}
            }

        this.container =
            {
                get:function( ){return this._div;}
            }

        this.forPrint =
            {
                get:function( ){return this._forPrint;}
            }
    }
)






Object.define
(
    {
        namespace: UA.charting,
        prototype: UA.charting.ChartBase,
    },

    function LineGrah(div)
    {
        this._getAdaptor =
            function()
            {
                return UA.charting.adaptors.FlotChartAdaptor.NEW(this)
            }
    }
)

Object.define
(
    {
        namespace: UA.charting,
        prototype: UA.charting.ChartBase,
    },

    function TableView(div)
    {
        this._getAdaptor =
            function()
            {
                return UA.charting.adaptors.TableAdaptor.NEW(this)
            }
    }
)




Object.define
(
    {
        namespace: UA.charting,
        prototype: UA.charting.ChartBase,
    },

    function ScatterPlot(div)
    {
        this._getAdaptor =
            function()
            {
                return UA.charting.adaptors.FlotScatterPlotAdaptor.NEW(this)
            }
    }
)













Object.define
(
    {
        namespace: UA.charting,
        construct:
        {
            getEngUnit:
            function(plot) //move out
            {
                var RawDataPlot = UA.charting.RawDataPlot;

                return ((plot instanceof RawDataPlot) ? UA.EngUnit.engUnits.single({engUnitId:plot.point.engUnitId}).name : "")
            }
        }
    },

    function ChartPlotBase(plotId, label, unit, data)
    {
        this.plotId = Number.VAR;

        this.label   = String.VAR;
        this.unit    = String.VAR;
        this.data    = Array.VAR;
        
        this.visible = Boolean.VAR;
        this.xAxis   = Number.VAR;
        this.yAxis   = Number.VAR;
        this.color   = String.VAR;
    }
)



Object.define
(
    {
        namespace: UA.charting,
        prototype: UA.charting.ChartPlotBase,
        construct:
        {
            fromPlotBase:
            function(plot, visu)
            {
                return this.NEW(plot.id, plot.displayName, UA.charting.ChartPlotBase.getEngUnit(plot), plot.data).SET({visible:plot.isVisible, xAxis:plot.xAxis, yAxis:plot.yAxis, color:plot.color, filled:plot.isFilled,})
            },
        },
    },

    function ChartLinePlot(plotId, label, unit, data)
    {        
        this.filled = Boolean.VAR;
    }
)





Object.define
(
    {
        namespace: UA.charting,
        prototype: UA.charting.ChartPlotBase,
        construct:
        {
            fromPlotBase:
            function(plot, visu)
            {
                return this.NEW(plot.id, plot.displayName, UA.charting.ChartPlotBase.getEngUnit(plot), plot.data).SET({visible:plot.isVisible, xAxis:plot.xAxis, yAxis:plot.yAxis, color:plot.color, xPlot:(visu.xPlot==plot),})                        
            },
        },
    },

    function ChartScatterPlot(plotId, label, unit, data)
    {
        this.xPlot = Boolean.VAR;
    }
)

