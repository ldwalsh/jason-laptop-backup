﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function NewAnalysisPresenter(builderEnviornment)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />

		this._view = UA.views.NewAnalysisView.NEW()

		//

		this._init =
			function()
			{
				this._view.nameCriteria = UA.AnalysisManager.nameCriteria;

				this._view.closed.set(this._newAnalysis)
			}

		this._newAnalysis =
			function(value)
			{
				if (!value) return;

				builderEnviornment.analysisManager.loadAnalysis(UA.AnalysisObjIdentifier.NEW(0, null, value))
			}
	}
)
