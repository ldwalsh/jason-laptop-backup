﻿///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters.VisuDisplayPresenter,
	},

	function ViewHandler(presenter) //to be reintegrated back into the Presenter
	{
		var PlotChangeEnum = UA.charting.PlotChangeEnum;

		this._presenter = UA.presenters.VisuDisplayPresenter.VAR;

		this.legendHover =
			function(plot, entering)
			{
                this._presenter._rrr([this._presenter.visuDisplay.plots.single({id:plot.id})], (entering ? PlotChangeEnum.Highlight : PlotChangeEnum.Unhighlight))
			}

		this.changedIsXAxis =
			function(plot)
			{
				var d = this._presenter.visuDisplay;

                d.visu.xPlot = d.plots.single({id:plot.id})

                this._presenter._rrr(d.plots, PlotChangeEnum.IsXAxis) //causes double draw... need to refine before using this
			}

		this.changedColor =
			function(plot, color)
			{
				this._presenter._rrr([this._presenter.visuDisplay.plots.single({id:plot.id}).SET({color:color})], PlotChangeEnum.Color)
			}

		this.changedVisibility =
			function(plot, isVisible)
			{
				this._presenter._rrr([this._presenter.visuDisplay.plots.single({id:plot.id}).SET({isVisible:isVisible})], PlotChangeEnum.Visible)
			}

		this.changedFill =
			function(plot, isFilled)
			{
				this._presenter._rrr([this._presenter.visuDisplay.plots.single({id:plot.id}).SET({isFilled:isFilled})], PlotChangeEnum.Fill)
			}

		this.changedYAxis =
			function(plot, yAxis)
			{
				this._presenter._rrr([this._presenter.visuDisplay.plots.single({id:plot.id}).SET({yAxis:yAxis,})], PlotChangeEnum.YAxis)
			}

		this.removePlot =
			function(plot)
			{
                plot = this._presenter.visuDisplay.plots.single({id:plot.id})
                
                var xPlotId = this._presenter.visuDisplay.visu.xPlot.id;

				var errMsg = this._presenter.visuDisplay.removePlot(plot)

				if (errMsg) return errMsg;

                var xPlot = this._presenter.visuDisplay.visu.xPlot;

                if (xPlot && (xPlot.id != xPlotId))
                {
                    this._presenter._view.xAxisPlotId = xPlot.id;
                }

                this._presenter._rrr([plot], PlotChangeEnum.Delete)
			}

		this.loadData =
			function()
			{
				//this._presenter._loadData.invokeDeferred(1000)
				this._presenter._loadData()
			}

this.createExpression =
	function(expressionPlot)
	{
		///<param name="expressionPlot" type="o.ExpressionPlot" />

		UA.presenters.ExpressionBuilderPresenter.NEW(this._presenter._builderEnviornment)
			.SET({plots:this._presenter.visuDisplay.plots, expressionPlot:expressionPlot, expressionSaved:this._presenter._expressionSaved}).show()
	}

this.editExpressionPlot =
	function(plot)
	{
		var plot = this._presenter.visuDisplay.plots.single({id:plot.id})

		if (!(plot instanceof UA.charting.ExpressionPlot)) throw new Error(843789348943)

		this.createExpression(plot)
	}
	}
)
