﻿///
///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.presenters.AnalysisExplorerPresenter,
	},

	function MessageEndpoint(view)
	{
		///<param name="view" type="UA.views.AnalysisExplorerWView" />

		this._view = UA.views.AnalysisExplorerWView.VAR;

		this.analysisLoaded =
			function(msg)
			{
				///<param name="msg" type="UA.bus.AnalysisBM" />
				
				var visu = msg.visu;

				this._view.analysisTitle = visu.name;

				if (!visu.subroutineObjs.length) return;

				for (var i=0; i<visu.subroutineObjs.length; i++)
				{
					var subroutineObj = visu.subroutineObjs.item(i)

					this._view.addSubroutineToList(subroutineObj.name, subroutineObj.id)
				}

				if (visu.mainSubroutineObj)
				{
					this._view.markAsMainSubroutine(visu.mainSubroutineObj.id)
				}
			}
					
		this.renameSubroutine =
			function(msg)
			{
				///<param name="msg" type="UA.bus.SubroutineBM" />
				
				var subroutineObj = msg.subroutineObj;

				this._view.renameSubroutineInList(subroutineObj.id, subroutineObj.name)
			}

		this.setSubroutineAsMain =
			function(msg)
			{
				///<param name="msg" type="UA.bus.SubroutineBM" />

				this._view.markAsMainSubroutine(msg.subroutineObj.id)
			}

		this.renameAnalysis =
			function(msg)
			{
				///<param name="msg" type="$.MessageBus.Message" />

				this._view.analysisTitle = msg.data;
			}
	}
)

