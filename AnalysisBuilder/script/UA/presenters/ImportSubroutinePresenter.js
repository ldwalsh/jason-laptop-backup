﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function ImportSubroutinePresenter(builderEnviornment)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />

		this._view = UA.views.ImportSubroutineView.NEW()

		//

		this._init =
			function()
			{
				//this._wireToView()

				this._getAllIdentifiersNotifier = this._view.loadingAnalysisThrobToggle;

				UA.VisuService.instance.getAllIdentifiers().wireTo(this)
			}

		this._analysisChanged =
			function(analysisObjID)
			{
				this._view.clearSubroutineList()

				var wme = ((analysisObjID == -1) ? UA.SubroutineService.instance.getAll() : UA.SubroutineService.instance.getByAnalysisObjId(analysisObjID))

					wme.onReturn	= this._addSubroutines;
					//wme.setNotifier(this._view.loadingSubroutineThrobToggle)
			}

		this._importSubroutine =
			function(subroutineObjID)
			{
				this._view.removeSubroutineFromList(subroutineObjID)
				
				this._builderEnviornment.analysisManager.importSubroutine(subroutineObjID)
			}

		this._getAllIdentifiersCallback =
			function(analysisIdentifiers)
			{
				///<param name="analysisIdentifiers" type="Array" elementType="UA.AnalysisObjIdentifier" />

				this._view.addVisuToList("All", "-1")

				analysisIdentifiers.forEach
				(
					function(ai)
					{
						if (ai.id == this._builderEnviornment.analysisManager.currentAnalysisObj.id) return;

						this._view.addVisuToList(ai.name, ai.id)
						
					}.bind(this)
				)

				this._analysisChanged(-1)
			}

		this._addSubroutines = //need to use SubroutineIdentifier instead!
			function(subroutineObjs)
			{
				///<param name="subroutineObjs" type="Array" elementType="UA.SubroutineObj" />

				subroutineObjs.forEach
				(
					function(value)
					{
						if (this._builderEnviornment.analysisManager.currentAnalysisObj.subroutineObjs.search({"id":value.id}).length) return;

						this._view.addSubroutineToList(value.name, value.id)
					
					}.bind(this)
				)
			}
	}
)

