﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.presenters,
		prototype: UA.presenters.PresenterBase,
	},

	function FunctionBoxPresenter(builderEnviornment)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />

		this._view = UA.views.FunctionBoxWView.NEW()

		//

		this.addFunctions =
			function(functionObjs)
			{
				///<param name="functionObjs" type="$.TArray" elementType="UA.FunctionObjBase" />

				for (var i=0; i<functionObjs.length; i++)
				{
					var fo = functionObjs[i]

					this._view.addFunction(fo.category.toString(), fo.label)
				}
			}
		
		//this._loadedFunctionType =
		//	function(types)
		//	{
		//		for (var i=0; i<types.length; i++)
		//		{
		//			var fType = types[i]

		//			this._view.addFunction(fType.category.toString(), fType.label)
		//		}
		//	}

		this._functionDragEnd =
			function()
			{
				//builderEnviornment.TEST.
			}

		this._functionDragStart =
			function(f)
			{
					
			}
	}
)
