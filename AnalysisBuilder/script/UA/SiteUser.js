﻿///
///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
        construct: {roles:{PublicUser:900,}}
	},

	function SiteUser(id, orgId, clientId, roleId, culture)
	{
		///<param name="id" type="Number" />
		///<param name="orgId" type="Number" />
		///<param name="clientId" type="Number" />
		///<param name="roleId" type="Number" />

		this.id        = Number.VAR;
		this.orgId     = Number.VAR;
		this.clientId  = Number.VAR;
		this.roleId    = Number.VAR;
		this.culture   = String.VAR;
		this.buildings = $.collections.Dictionary.VAR(UA.Building)	
		
		this.Cstr =
			function()
			{
				this.buildings = $.collections.Dictionary.NEW(UA.Building)
			}
	}
)


