﻿///
///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
	},

	function AnalysisObjIdentifier(id, name, created, modified, description)
	{
		this.id          = Number.VAR;
		this.name        = String.VAR;
		this.description = String.VAR;
		this.created     = Date.VAR;
		this.modified    = Date.VAR;
		this.ownerId     = Number.VAR;
		this.owner       = String.VAR;
		this.sharedRead  = Boolean.VAR;
		this.sharedWrite = Boolean.VAR;
	}
)
