﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace: UA,
		statics:
		{
			nameCriteria: "^[A-Za-z]([A-Za-z]|\$|[0-9])+$",
		},
	},
	function AnalysisManager(builderEnviornment)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />
		
		this._currentAnalysisObj	= UA.AnalysisObj2.VAR;	
		this.onAnalysisLoad			= $.Event;
		this.onAnalysisUnload		= $.Event;
	
		//this.createAnalysis =
		//	function(name)
		//	{
		//		///<param name="name" type="String">Name for new analysis to be created.</param>

		//		//if (this._currentAnalysisObj) throw new $.InvalidOperationError("An analysis is currently loaded.") //replace with a disabled New

		//		//UA.VisuService.instance.create(name).success.let(this._analysisLoaded)
		//	}

		this.createSubroutine =
			function(name)
			{
				//check name of subroutine does not exist

				//if (!this._currentAnalysisObj) throw new $.InvalidOperationError("No current analysis to close.")
				
				UA.VisuService.instance.addSubroutine(this._currentAnalysisObj.id, name).OnReturn.attach
				(
					function(subroutineObj)
					{
						this._currentAnalysisObj.subroutineObjs.push(subroutineObj)

						builderEnviornment.mbus.send(UA.bus.SubroutineBM.NEW(UA.bus.BMTypeEnum.CreateSubroutine, subroutineObj))
					
					}.bind(this)
				)
			}

		this.importSubroutine =
			function(subroutineObjId)
			{
				var $this = this;

				var wme = UA.VisuService.instance.importSubroutine(this._currentAnalysisObj.id, subroutineObjId)
				
					wme.onReturn.attach
					(
						function()
						{
							var wme2 = UA.SubroutineService.instance.getByID(subroutineObjId)

								wme2.onReturn.attach
								(
									function(subroutineObj)
									{
										$this._currentAnalysisObj.subroutineObjs.push(subroutineObj)

										builderEnviornment.mbus.send(UA.bus.SubroutineBM.NEW(UA.bus.BMTypeEnum.ImportSubroutine, subroutineObj))
									}
								)
						}
					)
			}

		this.removeSubroutine =
			function(subroutineId)
			{
				///<param name="subroutineId" type="Number" />

				var rpcExec = UA.VisuService.instance.removeSubroutine(this._currentAnalysisObj.id, subroutineId)

				var subroutineObj = this._currentAnalysisObj.getSubroutineById(subroutineId)

				rpcExec.onReturn.attach
				(
					function()
					{
						builderEnviornment.mbus.send(UA.bus.SubroutineBM.NEW(UA.bus.BMTypeEnum.RemoveSubroutine, subroutineObj))
					}
				)

				//this._currentAnalysisObj.subroutineObjs.remove(subroutineObj)
				this._currentAnalysisObj.subroutineObjs.splice(this._currentAnalysisObj.subroutineObjs.indexOf(subroutineObj, 1))

					//revise because subroutineObjs must be readonly!!!
					//remove should not exist on array obj
					//should use readonly List obj here
			}

		this.setSubroutineAsMain =
			function(subroutineObjId)
			{
				var $this = this;
				
				UA.ObjectService.instance.update(this._currentAnalysisObj, {"MainSubroutineObjID":subroutineObjId}).onReturn.attach
				(
					function()
					{
						$this.currentAnalysisObj._mainSubroutineObjID = subroutineObjId;

						builderEnviornment.mbus.send(UA.bus.SubroutineBM.NEW(UA.bus.BMTypeEnum.SetSubroutineAsMain, $this.currentAnalysisObj.mainSubroutineObj))

					}.bind(this)
				)
			}
				
		this.loadAnalysis =
			function(analysisIdentifier)
			{
				///<param name="analysisIdentifier" type="UA.AnalysisObjIdentifier" />

				if (this._currentAnalysisObj) throw new $.InvalidOperationError("An analysis is currently loaded.") //replace with a disabled New

				//if (analysisIdentifier.id)
				//{
				//	UA.VisuService.instance.getById(analysisIdentifier.id).onReturn = this._analysisLoaded;
				//}
				//else
				//{
				//	UA.VisuService.instance.create(analysisIdentifier.name).onReturn = this._analysisLoaded;
				//}

				//new UA.VisuObj()

				
			}
	
		this.unloadAnalysis =
			function()
			{
				if (!this._currentAnalysisObj) throw new $.InvalidOperationError("No current analysis to close.")

				UA.presenters.PresenterBase.prototype._analysisObj = null;
			}

		//this._analysisLoaded =
		//	function(analysisObj)
		//	{
		//		///<param name="analysisObj" type="UA.AnalysisObj2" />

		//		this._currentAnalysisObj = analysisObj;

		//		UA.presenters.PresenterBase.prototype._analysisObj = analysisObj; //MOVE!!!!!
	
		//		this.onAnalysisLoad.raise(analysisObj)
		//	}
			
		this.currentAnalysisObj_get =
			function()
			{
				return this._currentAnalysisObj;
			}
	}
)
