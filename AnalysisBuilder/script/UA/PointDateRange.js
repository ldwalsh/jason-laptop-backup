///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		prototype: iCL.DateRange,
	},

	function PointDateRange(point, from, to) //revise to take $.DateRange instead of dateFrom & dateTo
	{
		///<param name="point" type="UA.Point" />
		///<param name="from" type="Date" />
		///<param name="to" type="Date" />

		this.point = UA.Point.VAR;

        this._toCopy =
            function()
            {
                return iCL.DateRange._toCopy(this)
            }
	}
)
