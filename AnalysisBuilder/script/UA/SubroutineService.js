﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"SubroutineService",
		statics:
		{
			instance_:
			function()
			{
				this.__instance = this.__instance || new this(document.baseUrl.endWith("/") + document.base.getData("modulePath") + "Service/AnaylsisService.asmx")

				return this.__instance;
			},
		},
	},

	function(serviceEndpoint)
	{
		this._ss = new $.asp.ScriptService(serviceEndpoint)

		//this._ss.defaultNotifier = new $.DD(document).toggle;

		this._ss.defineMethod("SubroutineObj_GetByID", ["id"])
		this._ss.defineMethod("SubroutineObj_GetAll")
		this._ss.defineMethod("SubroutineObj_GetByAnalysisObjID", ["analysisObjId"])
		this._ss.defineMethod("SubroutineObj_Create", ["name"])
		this._ss.defineMethod("SubroutineObj_UpdateEquipment", ["subroutineObjID","equipment"]) //MAY NOT NEED if used ObjectService.update

		//

		this.getByID =
			function(id)
			{
				return this._ss.methods.SubroutineObj_GetByID.invoke(id)
			}

		this.getAll =
			function()
			{
				return this._ss.methods.SubroutineObj_GetAll.invoke()
			}

		this.getByAnalysisObjId =
			function(analysisObjId)
			{
				return this._ss.methods.SubroutineObj_GetByAnalysisObjID.invoke(analysisObjId)
			}

		this.create =
			function(name)
			{
				return this._ss.methods.SubroutineObj_Create.invoke(name)
			}

		this.updateEquipment =
			function(subroutineObjID, equipment)
			{
				return this._ss.methods.SubroutineObj_UpdateEquipment.invoke(subroutineObjID, equipment)
			}


		$.class.instantiate(this)
	}
)
