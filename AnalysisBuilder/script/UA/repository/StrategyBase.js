﻿
Object.define
(
    {
        namespace: UA.repository,
    },
    
    function StrategyBase()
    {
        this.__pointService = UA.PointService.instance;
        this._cacheFor      = 31536000; //move out to const

        this._getData =
            function(points, range, callback)
            {
                points = Array.wrapObject(points)
                
                this.__pointService.getData(points.map(function(_){return _.id}), range.from, range.to, this._cacheFor).setOn =
                {
                    binding:this,
                    success:function(r){callback(this._adjustSamples(points, r))}
                }
            }

        this._adjustSamples =
            function(points, samples)
            {
                var point;

                var last = {}
                var next = {}

                var data = []

                samples.forEach(function(s, i){return _rU(s.logged.roundToMinute(((point && (point.id == s.pointId)) ? point : points.single({id:s.pointId})).sampling))}.bind(this))

                samples.forEach
                (
                    function(s, i)
                    {
                        point = ((point && (point.id == s.pointId)) ? point : points.single({id:s.pointId}))

                        for (var j=i+1; j<samples.length; j++)
                        {
                            var s2 = samples[j]

                            if (s2.pointId == point.id)
                            {
                                next[point.id] = s2

                                break;
                            }
                        }

                        if (this._adjustSample((1000 * 60 * point.sampling), s.logged, (last[point.id]||{logged:null}).logged, (next[point.id]||{logged:null}).logged))
                        {
                            data.push(s)
                        }

                        last[point.id] = s;
                        next[point.id] = null;
                    
                    }.bind(this)
                )

                return data;
            }

        this._adjustSample =
            function(sampling, sample, last, next)
            {                
                if (!last) return true;
                if (!next) return false;

                if (sample > last)
                {
                    if (sample < next) return true;

                    if (sample.equalTo(next) && (last.toCopy().addMinutes(5) < sample))
                    {
                        sample.addMinutes(-5)
                    }

                    return true;
                }
                else
                {    
                    if (sample < next.toCopy().addMinutes(-5)) return _rT(sample.addMinutes(5))
                    
                    return false;
                }
            }
    }
)
