﻿
Object.define
(
    {
        namespace: UA.repository,
        construct: {timeout:3000,}
    },
    
    function RetrieveInstance(strategies, callback)
    {
        this._strategies = Array.VAR;
        this._callback   = Function.VAR;
        this._timerId    = Number.getRandom(1000000, 9999999)
        this._points     = Array.VAR;
        this._range      = iCL.DateRange.VAR;
        this._started    = false;

        this.retrieve =
            function(points, range)
            {
                if (this._started) throw new iCL.InvalidOperationError("The RetrieveInstance object is in a started stated.")

                if (this._points && this._points.length)
                {
                    this._range.from = ((range.from < this._range.from) ? range.from : this._range.from)
                    this._range.to   = ((range.to   > this._range.to  ) ? range.to   : this._range.to  )

                    this._points = this._points.concat(points).distinct(".id")
                }
                else
                {
                    this._points = points.toCopy()
                    this._range  = range.toCopy() 
                }

                this._doRetrieve.invokeDeferred(RetrieveInstance.timeout, [], this._timerId) //revise to not use 3 args but 2
            }

        this._doRetrieve =
            function()
            {
                this._started = true;

                var dates = this._range.getDates()
                var a     = []
                                
                this._points.forEach
                (
                    function(p)
                    {
                        a.push([p.id, dates.map(function(_){return [_.toCopy(), null]})])

                        this._strategies.forEach(function(_){_.is(a)})
                    
                    }.bind(this)
                )

                var strategy, dayDates=[], called=0, aggregated={}

                a[0][1].concat([[,]]).forEach // concat in a blank pair <-revise
                (
                    function(d)
                    {
                        if (strategy != d[1])
                        {
                            if (strategy && dayDates.length)
                            {
                                called++;

                                strategy.retrieve
                                (
                                    this._points,
                                    iCL.DateRange.NEW(dayDates[0], dayDates.getLast()),
                                    function(r)
                                    {
                                        aggregated = Object.Transform(aggregated).merge(r) //merge(aggregated, r)

                                        if (--called) return;

                                        var o;

                                        this.points.forEach(function(_){dates.forEach(function(__){exec(o = aggregated[_.id] = (aggregated[_.id] || {}))[__.value] = (o[__.value] || [])})})

                                        this._callback(this, aggregated)

                                    }.bind(this)
                                )
                            }
                                    
                            strategy = d[1]
                            dayDates = []
                        }

                        dayDates.push(d[0])
                    
                    }.bind(this)
                )
            }

        this.points =
            {
                get:function(){return this._points;}
            }

        this.range =
            {
                get:function(){return this._range;}
            }

        this.started =
            {
                get:function(){return this._started;}
            }
    }
)
