﻿
Object.define
(
    {
        namespace: UA.repository,
    },
    
    function Repository(strategies)
    {
        var $repository = UA.repository;

        this._strategies = Array.VAR;
        this._requests   = Array.VAR;
        this._pending    = Array.VAR;

        this.Cstr =
            function()
            {
                this._requests = []
                this._pending  = []
            }

        this.retrieve =
            function(points, range, callback)
            {
                points = Array.wrapObject(points)
                range  = range.toCopy()

                this._requests.push($repository.RetrieveArgs.NEW(points, range, callback))
                
                var ri = (this._pending.find(function(_){return this._findRetriever(_, points, range)}.bind(this)) || this._pending.pushAndReturn($repository.RetrieveInstance.NEW(this._strategies, this._retrieveDone)))

                ri.retrieve(points, range)
            }

        this._findRetriever =
            function(retrieveInstance, points, range)
            {
                if (retrieveInstance.started) return false;

                var rr = retrieveInstance.range.toCopy()
                var r = iCL.DateRange.NEW(rr.from.addDays(-2), rr.to.addDays(2)) //the 2 days padding is arbitrary

                return (r.includes(range.from) || r.includes(range.to))
            }

        this._retrieveDone =
            function(instance, r)
            {
                this._requests.forEach
                (
                    function(o, i)
                    {
                        if (!o) return;

                        var points = o.points.map(function(_){return _.id})

                        if (!Object.keys(r).map(Number.parse).containsAll(points)) return;

                        if (!instance.range.includes(o.range)) return;
                                    
                        this._requests[i] = null;

                        var x={}, dates=o.range.getDates().map(function(_){return _.getTime()})

                        points.forEach(function(_){x[_] = Object.Transform(r[_]).reduce(dates)}) //Object.reduce(r[_], dates)})

                        Function.ifCall(o.callback, [x])
                                
                    }.bind(this)
                )
            }
    }
)
