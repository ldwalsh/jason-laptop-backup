﻿
Object.define
(
    {
        namespace: UA.repository.strategy,
        prototype: UA.repository.StrategyBase,
    },
    
    function PastWeekStrategy()
    {
        this.is =
            function(a)
            {
                var current = Date.yesterday;

                while (current.getDay() != Date.days.Saturday)
                {
                    current = current.toCopy().addDays(-1)
                }

                a.forEach(function(_){_[1].forEach(function(__){__[1] = (__[1] || ((__[0] <= current) ? this : __[1]))}.bind(this))}.bind(this))
            }

        this.retrieve =
            function(points, range, callback)
            {
                var d;
                var ranges = []

                while (!d || range.from < d)
                {
                    d = (d ? d.toCopy().addDays(-7) : range.to.toCopy().addDays(-6))

                    ranges.push(iCL.DateRange.NEW(d.toCopy(), d.toCopy().addDays(+6)))
                }

                var remaining = (ranges.length * points.length)
                var result    = {}

                points.forEach
                (
                    function(p)
                    {
                        var id = p.id;
                        var o  = (result[id] = (result[id] || {}))

                        ranges.forEach
                        (
                            function(r)
                            {
                                this._getData
                                (
                                    p,
                                    r,
                                    function(s)
                                    {
                                        var d, arr;

                                        s.forEach
                                        (
                                            function(i)
                                            {
                                                var t = i.logged.today.getTime()
                                        
                                                arr = ((d == t) ? arr : (arr = (o[d=t] = [])))

                                                arr.push(i)
                                            }                                
                                        )

                                        if (--remaining) return;

                                        callback(result)
                                    }
                                )

                            }.bind(this)
                        )
                    
                    }.bind(this)                
                )
            }

        //this._adjustSamples =
        //    function(points, pointData)
        //    {
        //        var point;

        //        for (var i=0; i<pointData.length; i++)
        //        {
        //            var ps = pointData[i]
                            
        //            point = ((point && (point.id == ps.pointId)) ? point : points.single({id:ps.pointId}))

        //            ps.logged.roundToMinute(point.sampling)

        //            if ((i > 0) && (pointData[i-1].logged.value == ps.logged.value))
        //            {
        //                //if (pointData[i+1])
        //                //{
        //                //    pointData[i+1].logged.roundToMinute(point.sampling)

        //                //    if ()
        //                //}

        //                pointData[i-1].logged.addMinutes(-5)
        //            }
        //        }

        //        return pointData;
        //    }
    }
)
