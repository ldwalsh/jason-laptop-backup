﻿
Object.define
(
    {
        namespace: UA.repository.strategy,
        prototype: UA.repository.StrategyBase,
    },
    
    function CurrentDayStrategy()
    {
        this._store    = []
        this._cacheFor = 10;

        this.is =
            function(a)
            {
                var today = Date.today;

                a.forEach(function(_){_[1].forEach(function(__){__[1] = (__[0].equalTo(today) ? this : __[1])}.bind(this))}.bind(this))
            }

        this.retrieve =
            function(points, range, callback)
            {                
                var s = this._store;

                var missingPoints = []
                        
                for (var i=0; i<points.length; i++)
                {
                    var p=points[i], id=p.id;

                    if (s[id]) continue;

                    missingPoints.push(p)
                }

                if (!missingPoints.length)
                {
                    var obj = {}

                    points.map(function(_){return _.id}).forEach(function(i){return _rU(obj[i] = {}.SET(range.from.getTime(), s[i]))}.bind(this))

                    callback(obj)

                    return;
                }

                var result = {}

                this._getData
                (
                    missingPoints,
                    range,
                    function(d)
                    {
                        var o, d, a, id;

                        d.forEach
                        (
                            function(i)
                            {
                                var t = i.logged.today.value;

                                if (t > range.to.value) return;
                                
                                o = ((i.pointId == id) ? result[id] : (result[i.pointId] = result[i.pointId]||{}))                                
                                        
                                a = ((i.pointId == id) && (d == t) ? a : (a = (o[d = t] || (o[d] = []))))

                                a.push(i)

                                id = i.pointId;
                            }                                
                        )
                        
                        callback(result)

                        //if (d.length)
                        //{
                        //    for (var i=0; i<d.length; i++)
                        //    {
                        //        var p=d[i], id=p.pointId;

                        //        //exec(s[id] = (s[id] || [])).push(p)
                        //    }
                        //}
                        //else
                        //{
                        //    for (var i=0; i<missingPoints.length; i++)
                        //    {
                        //        //s[missingPoints[i].id] = []
                        //    }
                        //}

                        //this.retrieve(points, range, callback)
                    
                    }.bind(this)
                )
            }
    }
)
