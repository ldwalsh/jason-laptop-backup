﻿
Object.define
(
    {
        namespace: UA.repository.strategy,
        prototype: UA.repository.StrategyBase,
    },
    
    function CurrentWeekStrategy()
    {
        this.is =
            function(a)
            {
                var arr=[], current=Date.yesterday;

                while (current.getDay() != Date.days.Saturday)
                {
                    arr.push(current)

                    current = current.toCopy().addDays(-1)
                }

                a.forEach(function(_){_[1].forEach(function(__){__[1] = (__[1] || (arr.has(__[0]) ? this : __[1]))}.bind(this))}.bind(this))
            }

        this.retrieve =
            function(points, range, callback)
            {
                this._cacheFor = Math.floor((Date.today.addDays(1) - new Date()) / 1000 / 60)

                var r = range.toCopy()
                
                while (r.from.getDay() != Date.days.Sunday)
                {
                    r.from = r.from.addDays(-1)
                }

                var result = {}

                this._getData
                (
                    points,
                    r,
                    function(d)
                    {
                        var o, d, a, id;

                        d.forEach
                        (
                            function(i)
                            {
                                var t = i.logged.today.value;

                                if (t > range.to.value) return;
                                
                                o = ((i.pointId == id) ? result[id] : (result[i.pointId] = result[i.pointId]||{}))                                
                                        
                                a = ((i.pointId == id) && (d == t) ? a : (a = (o[d = t] || (o[d] = []))))

                                a.push(i)

                                id = i.pointId;
                            }                                
                        )

                        callback(result)
                                
                    }.bind(this)
                )
            }
    }
)
