﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"SubroutineEditorPresenterManager",
	},

	function(builderEnviornment)
	{
		///<param name="builderEnviornment" type="UA.BuilderEnviornment" />

		this._presenters = new $.collection.Dictionary(UA.presenters.SubroutineEditorPresenter);

		this.activePresenter = UA.presenters.SubroutineEditorPresenter.VAR;

		this.Cstr =
			function()
			{
				this._builderEnviornment = arguments[0]

				//

				var ml = new $.MessageBus.MessageListener(this._builderEnviornment.mbus);

				ml.subscriptions.add(UA.bus.BMTypeEnum.ViewSubroutine, this._msgViewSubroutine);
			}

		this._msgViewSubroutine =
			function(msg)
			{
				///<param name="msg" type="UA.bus.SubroutineBM" />

				if (this._presenters.hasKey(msg.subroutineObj.id))
				{
					this.activePresenter = this._presenters.get(msg.subroutineObj.id)

					return;

					//alert("opened");

					//var presenter = UA.presenters.SubroutineEditorPresenter.CAST(this._presenters.get(message.subroutineObj.id));

					//presenter.show()
				}
				
				this._presenters.add(msg.subroutineObj.id, new UA.presenters.SubroutineEditorPresenter(this._builderEnviornment, msg.subroutineObj));

				this.activePresenter = this._presenters.getLast();
			}


		$.class.instantiate(this)
	}
)
