﻿///<reference path="../../$UA.js" />

Type.define
(
	{
		namespace: UA.expression,
		construct:
		{
			operators:		["+", "-", "*", "/", "%", "=", ">", "<", "&", "|", "!"],

			variableRegex:	"[a-zA-Z]+[a-zA-Z0-9]*",
			constantRegex: "[0-9]*",

			fragmentRegex: {numeric:"\\d*\\.?\\d*", variable:"[a-zA-Z]+", functionCall:"[a-zA-Z]+\\(.+\\)", grouping:"\(.+\)"},

			valueRegEx_: function(){return ("(" + this.variableRegex + "|" + this.constantRegex + ")")},

            getFragmentInstance:
            function(ft)
            {
                var getRegEx = function(x){return new RegExp(("^" + operators + x + "$"), "g")}

                var operators = "([+-/*%=><|&!]?[+-]?)"

                var rx = this.fragmentRegex;

                if (getRegEx(rx.numeric     ).test(ft)) return UA.expression.NumericFragment.NEW(ft)                
                if (getRegEx(rx.variable    ).test(ft)) return UA.expression.VariableFragment.NEW(ft)
                if (getRegEx(rx.functionCall).test(ft)) return UA.expression.FunctionCallFragment.NEW(ft, [";"])    
                if (getRegEx(rx.grouping    ).test(ft)) return UA.expression.GroupingFragment.NEW(ft, UA.expression.ExpressionTreeNode.ns.Expression.operators)
            }
		},
	},

	function Expression(expressionStr, cultureSpecific, symbolInfo)
	{
		///<param name="expressionStr" type="String" />
		
		this._expressionStr = String.VAR;
        this._symbolInfo    = UA.expression.CultureSymbolInfo.VAR;
		this.rootNode       = Expression.ns.ExpressionTreeNode.VAR;
		this.variables      = $.TArray.VAR(String)

        this.valueOf = function(){return ("[object " + this.constructor.type.fullName + "]")}
		
		this.Cstr =
			function()
			{
                var s = this._expressionStr;

                if ((/\(/g).getMatchCount(s) != (/\)/g).getMatchCount(s)) throw new Error("Missing matching brace")
                
                if (cultureSpecific)
                {
                    var ug = UA.BuilderEnviornment.instance.globalizer;                    
                    var groupSymbol = ug.cldr.main(UA.Globalize.cldrPath.latin.numbers.group)                    
                    ;(s.match(new RegExp("[0-9]{1,3}(\\" + groupSymbol + "[0-9]{3})+", "g"))||[]).forEach(function(m){s = s.replace(m, m.replace(new RegExp("\\"+groupSymbol, "g"), ""))})

                    var decimalSymbol = ug.cldr.main(UA.Globalize.cldrPath.latin.numbers.decimal)                    
                    ;(s.match(new RegExp(("[0-9]+\\" + decimalSymbol + "[0-9]+"), "g"))||[])
                        .forEach
                        (
                            //function(m){s = s.replace(m, m.replace(new RegExp("\\"+decimalSymbol, "g"), UA.BuilderEnviornment.instance.globalizerIC.cldr.main(UA.Globalize.cldrPath.latin.numbers.decimal)))}
                            function(m){s = s.replace(m, m.replace(new RegExp("\\"+decimalSymbol, "g"), "."))}
                        )
                }
                
                s = s.replace(/ /g, "").replace(/==/g, "=").replace(/&&/g, "&").replace(/\|\|/g, "|").replace(/if\(/g, "iif(") //

                this.rootNode  = Expression.ns.ExpressionTreeNode.NEW(this, Expression.getFragmentInstance(s))

				this.variables = new $.TArray(String)
				
				this._searchVariables(this.rootNode)
			}

		this._addFoundVariable =
			function(variable)
			{
				if (this.variables.contains(variable)) return;

				this.variables.push(variable)
			}

		this._searchVariables =
			function(node)
			{
				///<param name="node" type="UA.expression.ExpressionTreeNode" />
				
				var f = node._fragment;
                
                if (f instanceof UA.expression.CompositeFragmentBase) return _rU(node.childNodes.forEach(this._searchVariables))
                if (f instanceof UA.expression.VariableFragment)      return _rU(this._addFoundVariable(f.body))
			}

        this.toString =
            function()
            {
                return this.rootNode.toString().replace(/iif\(/g, "if(")
            }

		this.evaluate =
			function(variables)
			{
				return this.rootNode.evaluate(variables)
			}

        this.text =
            {
                get:function(){return this.rootNode.text.replace(/iif\(/g, "if(")}
            }

        this.symbolInfo =
            {
                get:function(){return Object.Tr(this._symbolInfo).extract(["decimal", "group", "seperator"])} //revise?
            }
	}
)


Type.define
(
    {
        namespace: UA.expression,
    },

    function CultureSymbolInfo(decimal, group, seperator)
    {
        this.decimal   = String.VAR;
        this.group     = String.VAR;
        this.seperator = String.VAR;
    }
)
