﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.expression,
		statics:
		{
			Cstr:
			function()
			{
				//var x = "[a-zA-Z0-9"
				//Math.arithmeticOperators
				//var regEx = new RegExp()
			},

			getOperatorIndexes: //rename
			function(arr, expr)
			{
				///<param name="arr" type="Array" />
				///<param name="expr" type="String" />

				//var isGrouping = (expr.startsWith("(") && expr.endsWith(")"))

				var operatorIndexes = []
				var signIndexes		= []

				var groupingDepth = 0;

				for (var i=0; i<expr.length; i++)
				{
					var chr = new Char(expr[i])

					switch ("" + chr)
					{
						case "(":
						{
							groupingDepth++;

							break;
						}
						case ")":
						{
							groupingDepth--;

							break;
						}
						default:
						{
							//if (isGrouping)
							//{
							//	if (groupingDepth > 1) break;
							//}
							//else
							//{
							//	if (groupingDepth) break;
							//}

							if (groupingDepth) break;

							var op = ("" + chr);

							if (arr.contains(op))
							{
								if (operatorIndexes.length && ((operatorIndexes.getLast()+1) == i))
								{
									signIndexes.push(i)

									break;
								}

								if (signIndexes.length && (signIndexes.getLast()+1) == i) throw new Error("Identifier expected instead of sign")

								operatorIndexes.push(i)
							}
						}
					}
				}

				if (!operatorIndexes[0])
				{
					operatorIndexes.shift()
				}

				return operatorIndexes;
			},
		},
	},

	function ExpressionFragment(value)
	{
		///<param name="value" type="String" />

		this._value          = value||String.VAR;
		
		this._operator       = String.VAR;
		this._sign           = String.VAR;

		this._isFunctionCall = false;
		this._isGrouping     = false;

		this._canEvaluate    = false;//rename

		this.Cstr =
			function()
			{
				if (!window.intellisense)
				{
					this._value = arguments[0]
				}

				//

				var ops_ = UA.expression.Expression.operators;

				var expr = this._value.replace(/ /g, "").replace(/==/g, "=").replace(/&&/g, "&").replace(/\|\|/g, "|")

				//
				//move to ExpressionTreeNode? /////////////

				//for (var i=0; i<expr.length; i++)
				//{
				//	var chr = expr.charAt(i)
				//}

				if ((/\(/g).getMatchCount(expr) != (/\)/g).getMatchCount(expr)) throw new Error("Missing matching brace")

				
				//////////////
				
				//
				//test for invalid syntax here

				if (ops_.contains(expr[0]))
				{
					this._operator = expr[0]

					if (ops_.contains(expr[1]))
					{
						if (!["-", "+"].contains(expr[1])) throw new Error("Sign must be a negative or positive")

						this._sign = expr[1]

						expr = expr.substr(2)
					}
					else
					{
						this._sign = "+";

						if (this._operator == "-")
						{
							this._operator = "+";

							this._sign = "-";
						}

						expr = expr.substr(1)
					}
				}
				else
				{
					this._operator = this._sign = "+";
				}

				var indexes = ExpressionFragment.getOperatorIndexes(ExpressionFragment.namespace.Expression.operators, this._value)

				this._isFunctionCall = (/^[a-zA-Z]+\(/.test(expr) && (!indexes.length)) //revise				

				if (this._isFunctionCall)
				{
					expr = expr.replaceRecursive(", ", ",").replaceRecursive("( ", "(").replaceRecursive(" )", ")") //revise
				}
				else if (isNum(expr) || RegExp.asStartEnd(/*"-?" +*/ ExpressionFragment.namespace.Expression.variableRegex).test(expr))
				{
					this._canEvaluate = true;
				}
				else if (/^\(.+\)$/.test(expr))
				{
					this._isGrouping = true;

					//if (this._sign == "+")
					//{
					//	this._sign = "";
					//}
				}

				this._value = expr;
			}

		this.getArguments =
			function()
			{
				if (!this._isFunctionCall) throw new Error()

				var x = this._value.substr(this._value.indexOf("(") + 1).removeEnd(")")

				return x.splitByIndexes(ExpressionFragment.getOperatorIndexes([","], x))

				//+sum ( 1, 2, sum(a, b, c))
			}

		this.value_get =
			function()
			{
				if (this._value.substr(0,2).toLowerCase() == "if") //revise to check for opening \s*(
				{
					this._value = ("iif" + this._value.substr(2))
				}

				return (this._sign + this._value);
			}

		this.operator_get =
			function()
			{
				return this._operator;
			}

		this.sign_get =
			function()
			{
				return this._sign;
			}

		this.isFunctionCall_get =
			function()
			{
				return this._isFunctionCall;
			}

		this.isGrouping_get =
			function()
			{
				return this._isGrouping;
			}

		this.canEvaluate_get =
			function()
			{
				return this._canEvaluate;
			}
	}
)
