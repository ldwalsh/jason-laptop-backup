﻿///<reference path="../../$UA.js" />

Type.define
(
	{
		namespace: UA.expression,
		construct:
		{
			invalidatePointAlias:
			function(alias, refMsg)
			{
				/// <param name="alias" type="String" />
				/// <param name="refMsg" type="REFF" />

				if (!isNaN(alias))
				{
					refMsg.value = "Point alias cannot be a number.";
				}

				if (!alias)
				{
					refMsg.value = "Point must have an assigned alias.";
				}

				if (window.ecmaKeywords[alias.toLowerCase()])
				{
					refMsg.value = ("The assigned name '" + alias + "' is not allowed.");
				}
				
				return !!refMsg.value;
			},
		},
	},
	
	function AliasUtility(){}
)