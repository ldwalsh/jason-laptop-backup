﻿///<reference path="../../$UA.js" />

Type.define
(
    {
        namespace: UA.expression,
        construct:
        {        
			getOperatorIndexes: //rename/move
			function(arr, expr)
			{
				///<param name="arr" type="Array" />
				///<param name="expr" type="String" />

				//var isGrouping = (expr.startsWith("(") && expr.endsWith(")"))

				var operatorIndexes = []
				var signIndexes		= []

				var groupingDepth = 0;

				for (var i=0; i<expr.length; i++)
				{
					var chr = $.Char.NEW(expr[i])

					switch ("" + chr)
					{
						case "(":
						{
							groupingDepth++;

							break;
						}
						case ")":
						{
							groupingDepth--;

							break;
						}
						default:
						{
							if (groupingDepth) break;

							var op = ("" + chr)

							if (arr.contains(op))
							{
								if (operatorIndexes.length && ((operatorIndexes.getLast()+1) == i))
								{
									signIndexes.push(i)

									break;
								}

								if (signIndexes.length && (signIndexes.getLast()+1) == i) throw new Error("Identifier expected instead of sign")

								operatorIndexes.push(i)
							}
						}
					}
				}

				if (!operatorIndexes[0])
				{
					operatorIndexes.shift()
				}

				return operatorIndexes;
			}       
        }
    },

    function FragmentBase(fragment)
    {
        this._fragment = String.VAR;
        this._body     = String.VAR;
		this._operator = String.VAR;
        this._sign     = String.VAR;

        this.Cstr =
            function()
            {
				if (!/^(([+-/*%=><&|!])?([+-])?(\(.+\)||[a-zA-Z]+||([0-9]+(\.[0-9]+)?)||([a-zA-Z]+\(.*\))))*$/.test(this._fragment)) throw new Error("The expression is invalid")
            }

        this.toString =
            function()
            {
                return (this._operator + this._sign + this._body)
            }

        this.body =
            {
                get:function(){return this._body;}
            }

        this.operator =
            {
                get:function(){return this._operator;}
            }

        this.sign =
            {
                get:function(){return this._sign;}
            }

        this.isNegative =
            {
                get:function(){return (this.operator == "-")}
            }
    }
)


Type.define
(
    {
        namespace: UA.expression,
        prototype: UA.expression.FragmentBase
    },

    function CompositeFragmentBase()
    {
        this.Cstr =
            function()
            {
                var s = this._body = this._fragment;

                this._operator = this._sign = "+";

                if (!UA.expression.FragmentBase.getOperatorIndexes(this._seps, s).length)
                {
                    var ops = UA.expression.Expression.operators;
                
			        if (ops.contains(s[0]))
			        {
				        if (["-", "+"].contains(s[1]))
                        {
                            if ((s[0] != "-") || (s[1] != "-"))
                            {
                                this._operator = s[0]
                                this._sign     = s[1]
                            }

                            s = s.substr(2)
                        }
                        else
                        {
                            this._operator = s[0]

                            s = s.substr(1)
                        }
			        }

                    this._body = s;
                }
            }

        this.getComponents =
            function()
            {
                var s = this._innerBody;
                
                return s.splitByIndexes(UA.expression.FragmentBase.getOperatorIndexes(this._seps, s), this._k)
            }

        this._seps =
            {
                get:function(){return UA.expression.ExpressionTreeNode.ns.Expression.operators}
            }

        this._innerBody =
            {
                get:function()
                {
                    var s = this._body;

                    var arr = UA.expression.FragmentBase.getOperatorIndexes(this._seps, s)

                    if (arr.length) return s;
                    
                    return (s.endsWith(")") ? s.substr(s.indexOf("(")+1).removeEnd(")") : s) //revise?!
                }
            }

        this._k =
            {
                get:function(){return true;}
            }
    }
)



Type.define
(
    {
        namespace: UA.expression,
        prototype: UA.expression.CompositeFragmentBase,
    },

    function FunctionCallFragment(fragment)
    {
        this._seps =
            {
                get:function(){return [","]}
            }

        this._k =
            {
                get:function(){return false;}
            }

        this._funcName =
            {
                get:function()
                {
                    var s = this._body;
                    
                    return s.substr(0, s.indexOf("("))
                }
            }

        this.getText =
            function(inner)
            {
                return (this._funcName + "(" + inner + ")")
            }
    }
)


Type.define
(
    {
        namespace: UA.expression,
        prototype: UA.expression.CompositeFragmentBase,
    },

    function GroupingFragment(fragment)
    {
        this.getText =
            function(inner)
            {
                return ("(" + inner + ")")
            }
    }
)

Type.define
(
    {
        namespace: UA.expression,
        prototype: UA.expression.FragmentBase,
    },

    function EvaluatableFragmentBase(fragment)
    {
        this.Cstr =
            function()
            {
                var s = this._fragment;

                var ops = UA.expression.Expression.operators;

                this._operator = this._sign = "+";
                
			    if (ops.contains(s[0]))
			    {
				    if (["-", "+"].contains(s[1]))
                    {
                        if ((s[0] != "-") || (s[1] != "-"))
                        {
                            this._operator = s[0]
                            this._sign     = s[1]
                        }

                        s = s.substr(2)
                    }
                    else
                    {
                        this._operator = s[0]

                        s = s.substr(1)
                    }
			    }

                this._body = s;
            }
    }
)



Type.define
(
    {
        namespace: UA.expression,
        prototype: UA.expression.EvaluatableFragmentBase,
    },

    function NumericFragment(fragment)
    {
    }
)

Type.define
(
    {
        namespace: UA.expression,
        prototype: UA.expression.EvaluatableFragmentBase,
    },

    function VariableFragment(fragment)
    {
    
    }
)





Type.define
(
	{
		namespace: UA.expression,
		prototype: Error,
	},

	function InvalidFragmentError()
	{		
	}
)



Enum.define
(
    {
        namespace: UA.expression,
    },

    function Operators()
    {
        Add, Subtract, Multiply, Divide, Module, EqualTo, GreaterThan, LessThan, And, Or
    }
)


Enum.define
(
    {
        namespace: UA.expression,
    },

    function ComparisonOperators()
    {
        Equal, NotEqual, GreaterThan, LessThan, GreaterThanOrEqual, LessThanOrEqual
    }
)

Enum.define
(
    {
        namespace: UA.expression,
    },

    function LogicOperators()
    {
        And, Or, Not
    }
)
