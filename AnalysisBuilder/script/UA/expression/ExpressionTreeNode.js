﻿///<reference path="../../$UA.js" />

Type.define
(
	{
		namespace: UA.expression,
	},

	function ExpressionTreeNode(expression, fragment, parentNode)
	{
		///<param name="fragment" type="UA.expression.FragmentBase" />
		///<param name="parentNode" type="Object" />

        var $=iCL, FragmentBase=ExpressionTreeNode.ns.FragmentBase;
		
		this._expression = UA.expression.Expression.VAR;
        this._fragment   = UA.expression.FragmentBase.VAR;
		this.parentNode  = parentNode;
		this.childNodes  = $.collections.List.VAR(ExpressionTreeNode)
		
		this.Cstr =
			function()
			{
				this.childNodes = $.collections.List.NEW(ExpressionTreeNode)

				var f = this._fragment;

                if (!(f instanceof UA.expression.CompositeFragmentBase)) return;
                
                f.getComponents().forEach(function(_){return _rU(this.childNodes.add(ExpressionTreeNode.NEW(this._expression, UA.expression.Expression.getFragmentInstance(_), this)))}.bind(this)) //use .map and addRange here!!!
			}

        this._getText =
            function(ggg)
            {
                var nodes = this.childNodes;
                var f     = this._fragment;

                var s;
                    
                if (nodes.count)
                {
                    var s2 = nodes.toArray().map(function(_){return _._getText(ggg)}).join((f instanceof UA.expression.FunctionCallFragment) ? ", " : "")

                    s = ((f instanceof UA.expression.FunctionCallFragment) ? f.getText(s2) : ((this._expression.rootNode == this && (this._fragment.operator == "+")) ? s2 : f.getText(s2)))
                }
                else
                {
                    s = (((f instanceof UA.expression.NumericFragment) && ggg) ? ggg.format("{0:n}", parseFloat(f.body)) : f.body)
                }

                var op = f.operator + f.sign;
                    
                s = (((["+", "-"].contains(f.operator)) ? ((op[0] == op[1]) ? "+" : "-") : ((f.sign == "+") ? f.operator : op)) + s)

                return (((!this.parentNode || (this.parentNode.childNodes.item(0) == this) || (this.parentNode._fragment instanceof UA.expression.FunctionCallFragment)) && (s[0] == "+")) ? s.substr(1) : s)
            }

        this.toString =
            function()
            {
                return this._getText()
            }

        this._getOp =
            function(f)
            {
                var op = (f.operator + f.sign)

                return (["+", "-"].contains(f.operator)) ? ((op[0] == op[1]) ? "+" : "-") : ((f.sign == "+") ? f.operator : op)
            }

        this._getOpForEvaluatable = //revise
            function(f)
            {
                return (this.parentNode ? "" : this._getOp(f))
            }

        this._getOps = //merge with _getOp?
            function(val, node)
            {
                var val = (this._getOp(node._fragment) + val)

                if (val.substr(0, 2) == "--") return val.substr(1)

                return val;
            }

		this.evaluate =
			function(args)
			{
				var f = this._fragment;

                var nodes = this.childNodes;
				
				if (!nodes.count)
				{
					var s = f.body;
					
					if (f instanceof UA.expression.NumericFragment) return parseFloat(this._getOpForEvaluatable(f) + s)

                    if (f instanceof UA.expression.VariableFragment)
                    {
					    if (!(s in args))    throw new Error("Missing argument")
					    if (args[s] == null) throw new Error("Missing argument value") //check to make sure it is numeric?

                        return parseFloat(this._getOpForEvaluatable(f) + args[s])
                    }

                    throw iCL.InvalidOperationError()
				}
				
				var str = "";
                
				if (f instanceof UA.expression.GroupingFragment)
				{
                    str += (this._getOp(f) + "(")
				}
				else if (f instanceof UA.expression.FunctionCallFragment)
				{
					var s = f.body; //combine
                    str += (this._getOp(f) + ("UA.expression.Math." + s.substr(0, s.indexOf("("))) + "(") //use string insert/splice instead here
				}

                nodes.forEach(function(_){return _rU(str += (this._getOps(_.evaluate(args), _) + ((f instanceof UA.expression.FunctionCallFragment) ? "," : "")))}.bind(this))

                str = ((f instanceof UA.expression.CompositeFragmentBase) ? ((f instanceof UA.expression.FunctionCallFragment) ? str.substr(0, str.length-1) : str) + ")" : str)

                str = ((this.parentNode && str[0] == f.operator) ? str.substr(1) : str)

                return eval(str.replace(/=/g, "==").replace(/&/g, "&&").replace(/\|/g, "||")) ///0 returns infinity
			}

        this.text =
            {
                get:function(){return this._getText(UA.BuilderEnviornment.instance.globalizer)}
            }
	}
)


Type.define
(
	{
		namespace: UA.expression,
		construct:
		{
			sum:		Math.sum,
			product:	Math.product,
			quotient:	Math.quotient,
			pow:		Math.pow,
			mod:		Math.mod,

			max:		Math.max,
			min:		Math.min,
			average:	Math.average,
			median:		Math.median,

			floor:		Math.floor,
			ceiling:	Math.ceil,
			abs:		Math.abs,

			and:		Math.and,
			or:			Math.or,
			xor:		Math.xor,

			iif:		Math.iif,
		},
	},

    function Math(){}
)

//Type.define
//(
//	{
//		namespace: UA.expression,
//		construct:
//        function Math()
//        {
//			this.sum      = Math.sum
//			this.product  = Math.product
//			this.quotient =	Math.quotient
//			this.pow      = Math.pow
//			this.mod      = Math.mod
//			this.max      = Math.max
//			this.min      = Math.min
//			this.average  = Math.average
//			this.median   = Math.median
//			this.floor    = Math.floor
//			this.ceiling  = Math.ceil
//			this.abs      = Math.abs
//            this.and      = Math.and,
//			this.or       = Math.or
//			this.xor      = Math.xor
//			this.iif      = Math.iif
//        },
//	}
//)
