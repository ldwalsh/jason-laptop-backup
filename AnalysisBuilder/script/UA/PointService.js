﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		prototype: UA.ServiceBase,
		construct:
		{
			instance_:
			function() //should be using patern here
			{
				this.__instance = (this.__instance || this.NEW(document.baseUrl.endWith("/") + "Service/PointService.asmx"))

				return this.__instance;
			},
		},
	},

	function PointService(endpoint)
	{
		this.Cstr =
			function()
			{
				this._defineWebMethod("GetByEquipmentId", ["equipId"])
				this._defineWebMethod("GetData", ["oid", "pointIds", "from", "to"]) //use {oid:Number, pointIds:Array, from:Date,... etc}
			}

		this.getByEquipmentId =
			function(equipId)
			{
				return this._doCall("Objects.Points", this._methods["GetByEquipmentId"], [equipId])

				//var ce = iCL.CallExec.NEW()

				//var ls = iCL.LocalStorageContext.NEW("Objects.Points")

				//var points = ls.get()

				//if (points)
				//{
				//	ce.onSuccess.raiseAsync([points])
				//}
				//else
				//{
				//	this._methods.GetByEquipmentId.invoke(equipId).setOn =
				//	Function.bindAll
				//	(
				//		this,
				//		{
				//			success:function(points)
				//			{
				//				ls.add(points, iCL.Time.NEW(0, 20).ms)

				//				ce.onSuccess.raise(points)
				//			},
				//			failure:function(err){ce.onFailure.raise(err)},
				//		}
				//	)
				//}

				//return ce;
			}

		this.getData =
			function(pointIds, from, to, cacheFor)
			{
				///<param name="pointIds" type="Array" elementType="Number" />
				///<param name="from" type="Date" />
				///<param name="to" type="Date" />

				pointIds = Array.wrapObject(pointIds)

				var format = "mm/dd/yyyy"; //get from elsewhere?                

				//return this._methods.GetData.invoke(UA.BuilderEnviornment.instance.siteUser.orgId, pointIds.sort(), from.format(format), to.format(format), cacheFor)
                return this._methods.GetData.setOptions({headers:{"App-CacheFor":cacheFor}}).invoke(UA.BuilderEnviornment.instance.siteUser.orgId, pointIds.sort(), from.format(format), to.format(format))
			}
	}
)
