﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		resources: ["resource/html/print/PdfTemplate.html",],
        versioned: Date.now(),
	},

	function PrintRenderer(chartCstr, plots, details)
	{
		this._chartCstr = Constructor.VAR;
        this._plots     = iCL.TArray.VAR(UA.charting.PlotBase)
        this._details   = Object.VAR

		this._bindToTR =
			function(tr, data)
			{
				///<param name="tr" type="HTMLTableRowElement" />
				///<param name=data" type="Object" />

				Object.forEach2
				(
					data,
					function(v, k)
					{
						var el = tr.querySelector("[data-field=" + k + "]")

						if (!el) return;
			
						Function.invoke((Function.IS(v) ? v : function(el){el.textContent = v;}), [el]) //use .setTextContent here

						el.removeAttribute("data-field")
					}
				)
			}

		this.render =
			function()
			{
				var div = HTMLElement.CAST(PrintRenderer.type.definition.resources.html.cloneNode(true))

				//

				div.querySelector("[data-field='title']").textContent = this._details.title; div.querySelector("[data-field='desc']" ).textContent = this._details.desc; //use data binding instead
				
				//

				var d = HTMLElement.create(HTMLDivElement, {data:{forPrint:true}, style:{height:500, width:720, visibility:"hidden",},})
				document.body.appendChild(d) //required because axis labels don't show otherwise!!! shoulden't need to add to dom otherwise... FLOT bug?			
                var canvas = this._chartCstr.NEW(d).SET({bind:[this._plots],})._adaptor.getCanvas()
                d.remove()
                
                div.querySelector("img").SET({src:canvas.toDataURL()})

				//				

				var table = div.querySelector("table")
				
				var trTemplate = HTMLTableElement.CAST(table).tBodies[0].firstElementChild.removeElement()
				
				this._plots.forEach
				(
					function(p)
					{
						if (!p.visible) return;
								
						var trt = trTemplate.cloneNode(true), dr = this._details.dateRange;

						this._bindToTR
						(
							trt,
							{
								spnIcon:     "", //p.icon,
								txtColor:    function(_){_.style.backgroundColor=p.color},
								spnPlotName: p.label,
								dateRange:   function(_){_.textContent = UA.BuilderEnviornment.instance.globalizer.format("{0/date:short} - {1/date:short}", [dr.from, dr.to])},
                                cmbYAxis:    p.yAxis,
								fill:        function(_){this._renderFill(_, p)}.bind(this), //use bindArgs here
								xaxis:       function(_){this._renderXAxis(_, p)}.bind(this), //use bindArgs here
							}
						)
						
						table.tBodies[0].appendChild(trt)
					
					}.bind(this)
				)

				return div.outerHTML;
			}
			
		this._renderFill =
			function(el, plot)
			{
				///<param name="el" type="HTMLElement" />

				if (!("filled" in plot)) return rU(el.style.display = "none")

				el.firstElementChild.style.backgroundColor = (plot.isFilled ? "gray": "")
			}

		this._renderXAxis =
			function(el, plot)
			{
				if (!("isXAxis" in plot)) return rU(el.style.display = "none")

				el.style.visibility = (plot.isXAxis ? "" : "hidden")
			}
	}
)
