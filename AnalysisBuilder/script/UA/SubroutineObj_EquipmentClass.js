﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"SubroutineObj_EquipmentClass",
	},

	function()
	{
		this.id					= Number.VAR;
		this.dateCreated		= Date  .VAR;
		this.equipmentClassID	= Number.VAR;
		this.alias				= String.VAR;
	}
)
