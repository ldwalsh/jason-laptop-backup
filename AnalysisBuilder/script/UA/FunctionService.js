﻿///
///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		prototype: UA.ServiceBase,
		construct:
		{
			instance_:
			function()
			{
				this.__instance = (this.__instance || this.NEW(document.baseUrl.endWith("/") + "Service/FunctionService.asmx"))

				return this.__instance;
			},
		},
	},

	function FunctionService(endpoint)
	{
		this.Cstr =
			function()
			{
				this._defineWebMethod("GetEngUnits")
			}

		this.getEngUnits =
			function()
			{
				return this._doCall("Objects.EngUnits", this._methods["GetEngUnits"])
			}

		this._timeout =
			{
				get:function(){return iCL.Time.NEW(1, 0, 0).ms;}
			}
			
	}
)
