﻿///
///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		prototype: UA.ServiceBase,
		statics:
		{
			instance_:
			function()
			{
				this.__instance = (this.__instance || this.NEW(document.baseUrl.endWith("/") + "Service/Equipment.asmx"))

				return this.__instance;
			},
		},
	},

	function EquipmentService(endpoint)
	{
		this.Cstr =
			function()
			{
				this._defineWebMethod("GetClasses")
				this._defineWebMethod("GetClassesByBuildingId", ["buildingId"])
				this._defineWebMethod("GetByBuildingAndClass", ["bid", "classID"])
				this._defineWebMethod("GetAssociatedEquipmentByEquipment", ["eid"])
				this._defineWebMethod("GetVariableValue", ["eid","evid"])
			}

		this.getClasses =
			function()
			{
				return this._methods.GetClasses.invoke()
			}
		
		this.getClassesByBuildingId =
			function(buildingId)
			{
				return this._doCall(("Objects.EquipmentClasses[" + buildingId + "]"), this._methods["GetClassesByBuildingId"], [buildingId])
			}

		this.getByBuildingAndClass =
			function(bid, classId)
			{
				return this._doCall(("Objects.Equipment[" + bid + "][" + classId + "]"), this._methods["GetByBuildingAndClass"], [bid, classId])
			}

		this.getAssociatedEquipmentByEquipment =
			function(eid)
			{
				return this._doCall(("Objects.AssociatedEquipment[" + eid + "]"), this._methods["GetAssociatedEquipmentByEquipment"], [eid])
			}

		//this.getVariableValue =
		//	function(eid, evid)
		//	{
		//		return this._methods.GetVariableValue.invoke(eid, evid)
		//	}
	}
)
