﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
	},

	function VisuDisplayPresenterManager(builderEnviornment)
	{
		var Presenter = UA.presenters.VisuDisplayPresenter;

		this._builderEnviornment = AutoProp(UA.BuilderEnviornment)
		this._presenters         = iCL.collections.List.VAR(Presenter)
		this._activePresenter    = Presenter.VAR;
		this._messageListner     = iCL.MessageBus.MessageListener.VAR;

		this.Cstr =
			function()
			{
				this._presenters     = iCL.collections.List.NEW(Presenter)
				this._messageListner = iCL.MessageBus.MessageListener.NEW(this._builderEnviornment.mbus)
			}		

		this.createPresenter =
			function(visu)
			{
				///<param name="visu" type="UA.VisuObj" />
				
				return this._presenters.addAndReturn(Presenter.NEW(this._builderEnviornment, UA.VisuDisplay.NEW(visu)))
			}

		this.removePresenter =
			function(presenter)
			{
				///<param name="presenter" type="o.VisuDisplayPresenter" />

				this._activePresenter = ((this._activePresenter == presenter) ? null : this._activePresenter)

				this._presenters.remove(presenter)

				presenter.Dispose()
			}

		this.presenters =
			{
				get:function(){return this._presenters.toArray()}
			}

		this.activePresenter =
			{
				get:function( ){return this._activePresenter;},
				set:function(v)
				    {
					    ///<param name="v" type="o.VisuDisplayPresenter" />

					    var p = this._activePresenter;

					    if (p == v) return;

					    if (p) //ifX(p, p.hide)
					    {
						    p.hide()
					    }

					    this._activePresenter = v;
				
					    if (!v) return;

					    v.show()
				    }
			}
	}
)
