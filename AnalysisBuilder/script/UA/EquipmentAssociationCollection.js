﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"EquipmentAssociationCollection",
		inherit:	$.collection.List,
	},

	function(associationProfile)
	{
		///<param name="associationProfile" type="UA.EquipmentAssociationProfile" />

		var cstr = UA.EquipmentAssociationCollection;
		var base = cstr.prototype;

		this._associationProfile = associationProfile;

		this.Cstr =
			function()
			{
				//this._associationProfile = associationProfile;
			}

		this.add =
			function(assoc)
			{
				///<param name="assoc" type="UA.EquipmentAssociation" />

				var exec =
					UA.EquipmentService.instance.createEquipmentAssociation(this._associationProfile.id, assoc.alias, assoc.equipmentId).onReturn.attach
					(
						function(assoc)
						{
							///<param name="assoc" type="UA.EquipmentAssociation" />

							this._items.push(assoc)
														
						}.bind(this)
					)
				
				return exec;
			}

		this.remove =
			function(assocId)
			{
				var exec =
					UA.EquipmentService.instance.deleteEquipmentAssociation(assocId).onReturn.attach
					(
						function()
						{
							var x = this.single({id:assocId})

							base.remove.apply(this, [x])

						}.bind(this)
					)

				return exec;
			}

		this.associationProfile_get =
			function()
			{
				return this._associationProfile;
			}


		$.class.instantiate(this, [UA.EquipmentAssociation])
	}
)
