﻿///
///<reference path="../$UA.js" />

Enum.define
(
	{
		namespace: UA,
	},

	function ViewModesEnum()
	{
		Line, Table, Scatter
	}
)