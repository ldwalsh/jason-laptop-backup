﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		prototype: iCL.asp.ScriptServiceBase,
		
		construct: {_timeout: iCL.Time.NEW(0, 0, 0).ms,}
	},

	function ServiceBase()
	{
		this._doCall =
			function(key, webMethod, args)
			{
				var ce  = iCL.CallExec.NEW(null)
				var ls  = iCL.LocalStorageContext.NEW(key)
				var lso = ls.get(this._timeout)

				if (lso)
				{
					ce.onSuccess.raise.invokeAsync([lso.obj])
				}								 
				else
				{
					webMethod.invoke.invoke((args ? args : undefined), Function.invoke.withArray).setOn = 
					Function.bindAll
					(
						this,
						{
							success:function(obj)
							{
								ls.add(obj, ServiceBase._timeout)

								return ce.onSuccess.raise(obj)	
							},
							failure:function(err){ce.onFailure.raise(err)}, //wire up automagically
						}
					)
				}

				return ce;
			}

		this._timeout =
			{
				get:function(){return ServiceBase._timeout;}
			}
	}
)
