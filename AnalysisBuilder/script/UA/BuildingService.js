﻿///
///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		prototype: UA.ServiceBase,
		statics:
		{
			instance_:
			function()
			{
				this.__instance = this.__instance || this.NEW(document.baseUrl.endWith("/") + "Service/BuildingService.asmx")

				return this.__instance;
			},
		},
	},

	function BuildingService(endpoint)
	{
		this.Cstr =
			function()
			{
				this._defineWebMethod("GetForCurrentClient")
			}
		
		this._addToBuildingsDict =
			function(buildings)
			{
				this.buildings.pushArray(buildings)
			}

		this.getForCurrentClient =
			function()
			{
				return this._doCall("Objects.Buildings", this._methods["GetForCurrentClient"])
			}
	}
)
