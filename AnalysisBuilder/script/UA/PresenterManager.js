﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
	},

	function PresenterManager(builderEnviornment)
	{
		this._builderEnviornment = AutoProp(UA.BuilderEnviornment)
		this.presenters			 = iCL.TArray.VAR(UA.presenters.PresenterBase)

		this.equipmentTreePresenter			= UA.presenters.EquipmentTreePresenter.VAR;
		this.visuDisplayManagerPresenter	= UA.presenters.VisuDisplayManagerPresenter.VAR;

		this.Cstr =
			function()
			{
				var $presenters = UA.presenters;

				this.presenters = new iCL.TArray($presenters.PresenterBase)

				//var presenters =
				//	[
						//$presenters.AnalysisExplorerPresenter,
						//$presenters.FunctionBoxPresenter,
						//$presenters.SubroutinePropsPresenter,
						//$presenters.AnalysisPropsPresenter,
						//$presenters.OutputPresenter,
						//$presenters.EquipmentTreePresenter,
						//$presenters.AnalysisEquipmentAssociationPresenter,
				//	]

				//for (var i=0; i<presenters.length; i++)
				//{
				//	var cstr = presenters.item(i)
					
				//	$presenters.PresenterBase.create(new cstr(this._builderEnviornment))
				//}
				
				//this.analysisExplorerPresenter	= $presenters.PresenterBase.create(new $presenters.AnalysisExplorerPresenter(this._builderEnviornment))
				//this.functionBoxPresenter		= $presenters.PresenterBase.create(new $presenters.FunctionBoxPresenter(this._builderEnviornment))

				this.equipmentTreePresenter      = $presenters.EquipmentTreePresenter     .NEW(this._builderEnviornment)
				this.visuDisplayManagerPresenter = $presenters.VisuDisplayManagerPresenter.NEW(this._builderEnviornment)
				
				//new UA.SubroutineEditorPresenterManager(this._builderEnviornment)
			}

		this.showDisplays =
			function()
			{					
				//this.analysisExplorerPresenter.show()
				//this.functionBoxPresenter.show()

				this.equipmentTreePresenter.show()
				this.visuDisplayManagerPresenter.show()
			}
	
		this._toggleWindowDisplay =
			function(menuItem)
			{
				var w = this.displays.keys[menuItem.menuCaption.replace(/ /g, "")]._window;
					
				w.toggleVisibility(!w.isVisible)
					
				menuItem.toggleChecked() //move up
			}
	}
)




