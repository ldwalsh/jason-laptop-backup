﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"EquipmentAssociation",
	},

	function()
	{
		this.id					= Number.VAR;
		this.dateCreated		= Date  .VAR;
		this.dateModified		= Date  .VAR;
		this.alias				= String.VAR;
		this.equipmentClassId	= Number.VAR;
		this.equipmentId		= Number.VAR;


		$.class.instantiate2(this)
	}
)
