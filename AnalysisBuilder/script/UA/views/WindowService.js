﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
		statics:
		{
			instance_:
			function()
			{
				this.__instance = this.__instance || this.NEW(document.baseUrl.endWith("/") + "Service/WindowService.asmx")

				return this.__instance;
			},
		},
	},

	function WindowService()
	{
		this.Cstr =
			function(serviceEndpoint)
			{
				this._ss = $.asp.ScriptService.NEW(serviceEndpoint)

				//this._ss.defaultNotifier = new $.Document.DD($.Document.current).toggle;

				this._ss.defineMethod("GetDWInfo", ["windowId"])

				this._ss.defineMethod("SetDWInfo", ["dwInfo"])
			}

		this.getDWInfo =
			function(windowId)
			{
				return this._ss.methods.GetDWInfo.invoke(windowId)
			}

		this.setDWInfo =
			function(dwInfo)
			{
				return this._ss.methods.SetDWInfo.invoke(dwInfo)
			}
	}
)
