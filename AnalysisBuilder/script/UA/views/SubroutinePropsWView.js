﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.views,
		prototype: UA.views.WindowedViewBase,
	},

	function SubroutinePropsWView()
	{
		this._elmntIDs				= ["_cmbInputUnit","_cmbOutputUnit","_txtName","_txtEquipAlias","_cmbEquipClass","_lstEquipment","_btnAddEquipAlias","_btnRemoveEquipAlias"]

		this._listPattern			= $.ui.patterns.List.VAR;

		this.addEquipClassToList	= Function.VAR;
		this.saveProps				= DELEGATE()

		this.Cstr =
			function()
			{
				this.addEquipClassToList = this._cmbEquipClass.addOption;
				
				//
				
				this._listPattern		= $.ui.patterns.List.NEW(this._lstEquipment, this._btnAddEquipAlias, this._btnRemoveEquipAlias)
				this._listPattern.owner = this;
			}

		this.show =
			function()
			{
				if (this._window.isShown)
				{
					this._window.attention() //move to WindowBase

					return;
				}

				this._window.show()
			}

		this.addEngUnits =
			function(engUnits)
			{
				///<param name="engUnits" type="Array" />

				this._cmbOutputUnit.addOption("", 0)
				this._cmbInputUnit .addOption("", 0)

				engUnits.forEach
				(
					function(engUnit)
					{
						this._cmbInputUnit .addOption(engUnit.name, engUnit.id)
						this._cmbOutputUnit.addOption(engUnit.name, engUnit.id)

					}.bind(this)
				)
			}

		this._init =
			function()
			{
				$.ui.ControlsGroup.NEW(this._div).render()

				this._window = UA.views.DisplayWindow.NEW(this, this._div, "")

				//this._window.open()
			}

		//this._btnAdd_Click =
		//	function()
		//	{
		//		if (!new $.ui.Validator([this._txtEquipAlias,this._cmbEquipClass]).validate()) return;
				
		//		this._lstEquipment.addOption(this._txtEquipAlias.value, this._cmbEquipClass.value)
		//	}

		this._listPatternOnSelectChange =
			function(optionObj)
			{
				///<param name="optionObj" type="HTMLSelectElement.OptionObject" />

				var t = optionObj.text.split("(")

				this._txtEquipAlias.value = t[0].trim()

				this._cmbEquipClass.setText(t[1].subStr(0, -1))
			}

		this._listPatternOnAdd =
			function()
			{
				return {text:(this._txtEquipAlias.value + " (" + this._cmbEquipClass.getText() + ")"), value:this._txtEquipAlias.value}
			}

		this._listPatternOnRemove =
			function()
			{
				this._txtEquipAlias.value = "";
				this._lstEquipment.value  = -1;
			}

		this._btnApply_click =
			function()
			{
				this.saveProps();
			}

		this.title_set =
			function(v)
			{
				this._window.title = v;
			}

		this.subroutineName_get =
			function()
			{
				return this._txtName.value;
			}

		this.subroutineName_set =
			function(value)
			{
				this._txtName.value = value;
			}

		this.inputUnit_get =
			function()
			{
				return this._cmbInputUnit.numericValue;
			}

		this.inputUnit_set =
			function(value)
			{
				this._cmbInputUnit.value = value;
			}

		this.outputUnit_get =
			function()
			{
				return this._cmbOutputUnit.numericValue;
			}

		this.outputUnit_set =
			function(value)
			{
				this._cmbOutputUnit.value = value;
			}

		this.equipment_get =
			function()
			{
				return this._listPattern.value;

				//var arr = [] //new $.TArray(UA.SubroutineObj_EquipmentClass)

				//for (var i=0; i<this._lstEquipment.options.length; i++)
				//{
				//	var option = HTMLOptionElement.CAST(this._lstEquipment.options[i])

				//	arr.push($.set(new UA.SubroutineObj_EquipmentClass, {equipmentClassID:option.numericValue, alias:option.text}))
				//}

				//return arr;
			}
			
		this.equipment_set =
			function(value)
			{
				///<param name="value" type="Array" elementType="UA.SubroutineObj_EquipmentClass" />

				//should not be passing in UA.SubroutineObj_EquipmentClass !!!!?????????

				this._lstEquipment.clearOptions()

				var optionsArray = this._cmbEquipClass.optionsToArray()

				value.forEach
				(
					function(e)
					{
						this._lstEquipment.addOption((e.alias + " (" + optionsArray.single({value:e.equipmentClassID.toString()}).text + ")"), e.alias)
					},
					this
				)
			}
	}
)
