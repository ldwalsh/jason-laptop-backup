﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
        versioned: Date.now(),
		prototype: UA.views.WindowedViewBase,
		resources: ["resource/html/views/VisuDisplayManagerWView.html", "resource/less/views/VisuDisplayManagerWView.less"],

		construct: //remove
		{
			noCSS:true,
		},
	},

	function VisuDisplayManagerWView()
	{
		var $controls = iCL.ui.controls;

        var ActionsEnum = UA.presenters.VisuDisplayManagerPresenter.ActionsEnum;

        this._elmntIDs     = ["_txtTitle", "_btnRemoveTab", "_cmbChartType", "_divDateRange"]
		this._tabStrip     = $controls.TabStrip.VAR;
		this._dateRangeCtl = Object.VAR;
		this.selectVisu    = DELEGATE("index")		
		this.manageAction  = DELEGATE()		

		this._init =
			function()
			{
				UA.BuilderEnviornment.instance._div.appendChild(this._div)

				this._window = UA.views.DisplayWindow.NEW(this, this._div).SET({hasTitlebar:false, onOpened:function(){iCL.ui.binders.ResizeBinder.NEW(this._window.windowDiv, $gId("divTabs"))}.bind(this)})  //revise

				this._dateRangeCtl =
				iCL.ui.extenders.InputDateRangeExtender.fromDiv(this._divDateRange, {rangeLimit:iCL.DateRange.NEW(new Date(2010, 1, 1), new Date().addDays(+1)),})
					.SET({changed:this._date_changed, editing:this._date_editing, buttonImage:"../_assets/images/cal.png", culture:UA.BuilderEnviornment.instance.siteUser.culture, attach:[]})
			}

		this._validateDateRange =  //move out to presenter!!! or dedicated
			function(range, from, to)
			{
				var VisuDisplayWView = UA.views.VisuDisplayWView
				var _classes = VisuDisplayWView._classes;

				DOMTokenList.remove([from, to], [_classes.InvalidDate, _classes.InvalidRange])

				var f = range.from;

                if (!f) return _rO(from.classList.add(_classes.InvalidDate), "Please enter a valid start date.")

				if (f.year < 2010) return _rO(from.classList.add(_classes.InvalidDate), "The start date must be after 2010.")

				var t = range.to;

				if (!t) return _rO(to.classList.add(_classes.InvalidDate), "Please enter a valid end date.")

				if (t < f) return _rO(DOMTokenList.add([from, to], _classes.InvalidRange), "The start date cannot be after the end date.")

				if (this._getDayDiff(range) > 90) return _rO(DOMTokenList.add([from, to], _classes.InvalidRange), "Date range cannot exceed 90 days.") //ALL THIS LOGIC DOES NOT BELONG HERE! (will be moved to VisuDisplay)                
                //if (range.delta >= (90*24*60*60*1000))
                //if (iCL.TimeSpan.NEW(range.delta).days > 90)  <-- use something like this??

				return "";
			}

        this._getDayDiff =
            function(dateRange) //temp until I get delta to work on dateRange... probably not .to inclusive
            {
                var from = dateRange.from;                
                var days = 0;                
                while (from <= dateRange.to)
                {
                    from.addDays(1)
                    days++;
                }
                return days;
            }

		this._date_editing = //move out to presenter
			function(eA)
			{
				this._validateDateRange(eA.range, eA.extender.inputFrom, eA.extender.inputTo)
			}

		this._date_changed = //move out to presenter
			function(eA)
			{
			    var failed = this._validateDateRange(eA.range, eA.extender.inputFrom, eA.extender.inputTo)

                if (failed) return _rU((String.IS(failed) ? this.promptNotification(failed) : undefined))

				this.manageAction(ActionsEnum.RangeChange)
			}

		this._render =
			function()
			{
				this._tabStrip = $controls.TabStrip.CAST(this._controlsGroup.getControlById("divTabStrip"))
			}

		this.visuOpened =
			function(name, dateRange)
			{
				this._dateRangeCtl.dateRange = dateRange;
				
				var tab = this._tabStrip.addTab(name, this._tabStrip.tabs.count)

				var span = HTMLElement.create(HTMLSpanElement, {parentNode:tab.li, className:"AnalysisBuilderIconsOriginal Icon-01x02",}) //should be done using template!
					span.event.attach.click(this.manageAction.bindArgs([ActionsEnum.Close]))

				this._tabStrip.selectedIndex = (this._tabStrip.tabs.count-2)
			}

		this.visuSelected =
			function(index)
			{
				this.selectVisu(this._tabStrip.selectedIndex = index)
			}

		this.visuRenamed =
			function(name)
			{
				this._tabStrip.selectedTab.caption = name;
			}

		this.visuClosed =
			function()
			{
				///<param name="index" type="Number" />
				
				var ts = this._tabStrip;

				ts.removeTab(ts.selectedIndex)
				
				if ((ts.tabs.count + 1) != this._tabStrip.maxTabCount) return;
				
				this._tabStrip.getTab(this._tabStrip.tabs.count-1).visible = true;
			}

		this._tabStrip_tabSelected =
			function(ea)
			{
				///<param name="ea" type="$.ui.controls.TabStrip.TabSelectEA" />
					
				var tab = ea.tab;

				if (tab.name == "NewView") //revise string check
				{
					this.manageAction(ActionsEnum.New)

					var ts = this._tabStrip;

					if (ts.tabs.count == ts.maxTabCount) {ts.getTab(ts.tabs.count-1).visible = false;} //revise

					return;
				}

				this.selectVisu(tab.index)
			}

		this._toolBarClick =
			function(ea)
			{
				this.manageAction(String.toPascalCase(ea.button.command))
			}

		this.updateViewState =
			function(state)
			{
                var hasViewMode      = !state.viewMode;
                var getControlByName = this._controlsGroup.getControlByName;

                var bars = iCL.Lookup.fromObj(["visuAux", "lineExport", "tableExport", "scatterExport", "drill"].lookup(function(_){return getControlByName(_ + "ButtonBar")}))

                bars.forEach(function(k, _){_.disabled = hasViewMode})

                if (!state.viewMode) return;

                getControlByName("ExportVT").toggle(new Number(state.viewMode))

                var bb = bars.item("visuAux")
                
                bb.getButtons().find(function(_){return _.command.startsWith("Annotations")}).toggled = !state.annotationsDisabled;

                bb.getButtons().find(function(_){return _.command.startsWith("Pan")}).toggled  = !state.panningDisabled;
                bb.getButtons().find(function(_){return _.command.startsWith("Pan")}).disabled = !state.drillOut;

                bb.getButtonByCommand("view" + state.viewMode.toString()).toggled = true;

                //

                var bb = getControlByName("visuButtonBar")
                bb.getButtonByCommand("props").disabled = !state.canEdit; bb.getButtonByCommand("open").disabled = !state.canOpen; bb.getButtonByCommand("save").disabled = !state.canSave; //combine

                bars.item("visuAux").getButtonByCommand("view" + state.viewMode.toString()).toggled = true;

                var bb = bars.item("drill"); bb.getButtonByCommand("drillOut").disabled = !state.drillOut; bb.getButtonByCommand("drillIn" ).disabled = !state.drillIn; //combine

			    this._dateRangeCtl.dateRange = state.dateRange;
			}

		this.disableFeature =
			function(arg0)
			{
				///<signature>
				///	<param name="featureName" type="String" />
				///</signature>
				///<signature>
				///	<param name="featureNames" type="Array" elementType="String" />
				///</signature>
				
				if (arg0 instanceof Array) return rU(Array.CAST(arg0).forEach(this.disableFeature))
				
				switch (arg0) //revise... use enums, revise duplication of code
				{
					case "props":
					case "open":
					case "save":
					{
						$controls.ButtonBar.CAST(this._controlsGroup.getControlByName("VisuToolbar")).getButtonByCommand(arg0).display = false;

						break;
					}
					case "export": //?
					{
						//$controls.ButtonBar.CAST(this._controlsGroup.getControlByName("TableToolbar")).getButtonByName(arg0).display = false;

						break;
					}
				}
			}

		this.dateRange =
			{
				get:function(){return this._dateRangeCtl.dateRange;}
			}			
	}
)


Array.prototype.lookup = //revise
function(func)
{
    var map = {}

    for (var i=0; i<this.length; i++)
    {
        var o = this[i]

        map[o] = func(o)
    }

    return map;
}
