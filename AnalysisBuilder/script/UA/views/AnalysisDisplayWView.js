﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
        versioned: Date.now(),
		prototype: UA.views.WindowedViewBase,
		resources: ["resource/html/views/VisuDisplayWView.html", "resource/less/views/VisuDisplayWView.less" /*"3rdParty/miniColors/jquery.miniColors.js", "3rdParty/jquery.maskedinput-1.3.js"*/],
		construct:
		{
			noCSS:true,

			_invalidDateMsg: "Please enter a valid date.",

			_messages: {InvalidDate:"Please enter a valid date.", InvalidRange:"Please enter a valid date range.",},
			_classes:  {InvalidDate:"InvalidDate", InvalidRange:"InvalidDateRange",}
		},
	},

	function VisuDisplayWView()
	{
		var _base = VisuDisplayWView.prototype;

		var $controls = iCL.ui.controls;

		this._elmntIDs          = ["_divDataViewToggle", /*"divChart",*/]
		this._pointTitles       = iCL.collections.List.VAR(String) //use KeyedArray?
		this.removePlot         = DELEGATE("plotId")
		
		this.editExpressionPlot = DELEGATE("plotId")
		this.legendHover        = DELEGATE()

		this.changedColor       = DELEGATE("plot", "color")
		this.changedVisibility  = DELEGATE("plot", "isVisible")
		this.changedYAxis       = DELEGATE("plot", "yAxis")
		this.changedFill        = DELEGATE("plot", "isFilled")
		this.changedIsXAxis     = DELEGATE("plot", "isXAxis")

        this.isXAxisGN          = String.VAR;

if (window.intellisense){this._div.getElmntsById(this._elmntIDs, this)}

		this.Dispose =
			function()
			{
				_base.Dispose.apply(this, arguments)

				this._div.removeElement()
			}

		this._beenShown = false;

		this.show =
			function()
			{
				_base.show.apply(this)

				if (Navigator.agent == Navigator.agents.Chrome) //remove in future
				{
					//this.divChart.toggleDisplay(true)
					//this.divChart.style.visibility = "";
				}

				//

				if (this._beenShown) return;

				this._beenShown = true;

				if (window.intellisense)
				{
					HTMLElement.prototype.querySelectorAll = function(){return [HTMLDivElement.VAR]}
				}

				//revise
				Array.fromAlike(HTMLElement.CAST(this._div).querySelectorAll("[data-control-type='TII']")).reverse().forEach(function(e){iCL.ui.controls.TII.NEW(e).draw()})			
				var slider = this._div.querySelector("[data-control-t='SLIDER']")
				slider.addEventListener
				(
					"mousedown",
					function(e)
					{
						var y = e.clientY;

						var n = slider.nextElementSibling;

						document.body.style.cursor = "ns-resize";

						//HTMLElement.CAST(this._div.querySelector("[data-id='divDataViewToggle']")).createMaskingDiv().style.SET({bottom:-100, opacity:.5, backgroundColor:"yellow",})

						function fm(e)
						{
							n.style.height = (parseInt(getComputedStyle(n).height) + (slider.getBoundingClientRect().top - e.clientY) + "px")

							slider.parentElement.controlInstance.draw()
						}

						document.addEventListener("mousemove", fm)

						var fy =
							function(e)
							{
								//HTMLElement.CAST(this._div.querySelector("[data-id='divDataViewToggle']")).removeMaskingDiv()

								document.body.style.cursor = "";

								document.removeEventListener("mouseup", fy)
								document.removeEventListener("mousemove", fm)
						
							}.bind(this)

						document.addEventListener("mouseup", fy)
					
					}.bind(this)
				)
			}

		this.hide =
			function()
			{
				_base.hide.apply(this)

				if (Navigator.agent == Navigator.agents.Chrome) //remove in future
				{
					//this.divChart.toggleDisplay(false)
					//this.divChart.style.visibility = "hidden";
				}
			}

		this._init =
			function()
			{
				UA.BuilderEnviornment.instance._div.appendChild(this._div)

				this._window = UA.views.DisplayWindow.NEW(this, this._div, "Chart Display").SET({/*resizeable:true,*/ hasTitlebar:false,})
				
				this._window.onOpened = function(){iCL.ui.binders.ResizeBinder.NEW(this._window.windowDiv, $gId("divChartPane"))}.bind(this)
			}

		this._removePlot =
			function(plot, row)
			{
				var errMsg = this.removePlot(plot)

				if (errMsg) return _rU(this.promptNotification(errMsg))

				$controls.TableView.CAST(this._controlsGroup.getControlById("tblPlotsTableView")).rows.single(function(_){return (parseInt(_.getField("plotId").value) == plot.id)}).remove()
			}

		this.addPlot =
			function(plot, isXAxis) //pass in individual plot props as needed
			{
				///<param name="plot" type="UA.charting.PlotBase" />
				
				var row = $controls.TableView.CAST(this._controlsGroup.getControlById("tblPlotsTableView")).SET({globalizer:UA.BuilderEnviornment.instance.globalizer,}).insertRow(0)
				
				row.element.event.attach.mouseEnter(function(){this.legendHover(plot, true )}.bind(this))
				row.element.event.attach.mouseLeave(function(){this.legendHover(plot, false)}.bind(this))
				
				row.classList.add(plot.constructor.type.name) //??

				row.getField("delete")        .SET({clicked:this._removePlot.bindArgs([plot])})
				row.getField("plotId")        .SET({value:plot.id})
				row.getField("icon")          .SET({value:plot.icon, tooltip:((plot instanceof UA.charting.ExpressionPlot) ? "Expression" : "Point")})				
				row.getField("plotName")      .SET({value:plot.displayName})
				row.getField("editExpression").SET({visible:(plot instanceof UA.charting.ExpressionPlot), clicked:function(){this.editExpressionPlot(plot)}.bind(this),})
				row.getField("range")         .SET({value:[plot.dateRange.from, plot.dateRange.to]})
				row.getField("shown")         .SET({value:plot.isVisible, changed:function(ea, sender){this.changedVisibility(plot, sender.checked)}.bind(this)})				
				row.getField("color")         .SET({value:plot.color})				
				row.getField("fill")          .SET({value:plot.isFilled, changed:function(ea, sender){this.changedFill(plot, sender.checked)}.bind(this)})
				row.getField("yAxis")         .SET({value:(plot.yAxis + "L"), changed:function(ea){this.changedYAxis(plot, parseInt(HTMLSelectElement.CAST(ea.sourceAsElmnt).value))}.bind(this)})
				row.getField("isXAxis")       .SET({value:isXAxis, changed:this.changedIsXAxis.bindArgs([plot]).bind(this)})

                row.getField("isXAxis").element.setAttribute("name", (this.isXAxisGN||(this.isXAxisGN=("g"+Date.now()))))

				//

				var txtColor = row.getField("color").element;
				var t;				
				jQuery(txtColor) //wrap in iCL.ui.control.ColorPicker
					.miniColors
					(
						{
							//changeDelay: 5000,
							letterCase:"uppercase",
							change:
							function(hex, rgb)
							{
								if (t) clearTimeout(t) //could use ifCall here with .cancel

								t = this.changedColor.invokeDelay(1000, [plot, hex]).clearId
								
							}.bind(this)
						}
					)				
				txtColor.nextElementSibling.style.backgroundColor = txtColor.value = plot.color;
			}

		this.changeLegend =
			function(plot, change)
			{
                change = ifNotNull(change, function(){return Array.wrapObject(change)})

				var row = $controls.TableView.CAST(this._controlsGroup.getControlById("tblPlotsTableView")).rows.single(function(_){return (parseInt(_.getField("plotId").value) == plot.id)})

				if (change)
				{
					if (change.contains(UA.charting.PlotChangeEnum.Range)) //revise
					{
						row.getField("range").SET({value:[plot.dateRange.from, plot.dateRange.to]})
					}
				}

				if (!(plot instanceof UA.charting.ExpressionPlot)) return;
				
row.getField("plotName").SET({value:plot.title, element:{tooltip:plot.expression.text,}})
			}

		this._updateWindowTitle =
			function()
			{
				var windowTitle = "Chart Display - ";

				var enumerator = this._pointTitles.getEnumerator() //revise enumerator?

				for (var a in enumerator)
				{
					windowTitle += (enumerator[a] + ", ")
				}
				
				this._window.title = windowTitle;
			}

		this.setLoadingState =
			function(state)
			{
				if (!(state ^ this._divDataViewToggle.isMasked)) return;

				if (state) this._divDataViewToggle.toggleEnabled(false).classList.add("Loading"); else this._divDataViewToggle.removeMaskingDiv()
			},

		this.switchMode =
			function(mode)
			{
				//if (!this._divDataViewToggle.isMasked) this._divDataViewToggle.toggleEnabled(false).classList.add("Loading")

				$controls.TableView.CAST(this._controlsGroup.getControlById("tblPlotsTableView")).element.data("view-mode").value = mode.toString()

				$controls.ViewToggle.CAST(this._controlsGroup.getControlById("divDataViewToggle")).toggle(mode.valueOf())
			}

		this.xAxisPlotId =
			{
                get:function( ){ },
				set:function(v)
				{
					//$controls.TableView.CAST(this._controlsGroup.getControlById("tblPlotsTableView")).rows.forEach(function(_){return _rU(_.getField("isXAxis").value = (_.getField("plotId").value == v))})

                    $controls.TableView.CAST(this._controlsGroup.getControlById("tblPlotsTableView")).rows
                     .forEach(function(_){  return ((_.getField("plotId").value == v) ? _rU(_.getField("isXAxis").value = true) : undefined)})
                }
            }

		this.displayID_get =
			function()
			{																					   
				return null;
			}

		this.chartContainer_get =
			function()
			{
				return $controls.ViewToggle.CAST(this._controlsGroup.getControlById("divDataViewToggle")).currentView;
			}
	}
)
