﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace:	UA.views,
		prototype:	UA.views.WindowedViewBase,
	},

	function FunctionBoxWView()
	{
		var $base = this.constructor.prototype;

		this._accordion			= $.ui.controls.Accordion.VAR;
		this.functionDragStart	= DELEGATE
		this.functionDragEnd	= DELEGATE
	
		this.addFunction =
			function(categoryName, name)
			{
				var header = this._accordion.getHeaderByID(categoryName)

				if (header == null)
				{
					header = this._accordion.createAndAppendHeader(categoryName, categoryName)

					this._controlsGroup.render()
				}

				var buttonBar = this._controlsGroup.getControlByElement(header.contentDiv.childNodes[0], $.ui.controls.ButtonBar)
				
				var button = buttonBar.createAndAppendButton(name, name)

				var d = D.NEW(button.li, (categoryName + "." + name))

		//d.onDragStart.register(this.functionDragStart);
		//d.onDragEnd  .register(this.functionDragEnd);

				//button.li_GET().dE.attach.mouseDown

				//this._makeDraggable(button.li_GET());
			}
		
		this.show =
			function()
			{
				this._accordion.render()

				$base.show.apply(this)
			}

		this._init =
			function()
			{
				var divFunctionBoxWView = $.ui.TemplatedElement.NEW($gId("divFunctionBoxWView"))

				//

				this._controlsGroup = $.ui.ControlsGroup.NEW(divFunctionBoxWView.element, {"accordion":[divFunctionBoxWView.templates.item("FunctionsButtonBar")]})

				this._controlsGroup.render()

				this._accordion = this._controlsGroup.getControlById("accordion", $.ui.controls.Accordion)

				//
					
				this._window = UA.views.DisplayWindow.NEW(this, divFunctionBoxWView.element, "Functions")

				this._window.resizeable = true;
			}

		this._windowResizing =
			function()
			{
				this._accordion.resize()
			}
	}
)







$.class
(
	{
		namespace: window,
	},

	function D(element, obj)
	{
		///<param name="element" type="HTMLElement" />
		///<param name="obj" type="Object" />

		this._element		= HTMLElement.VAR;
		this._obj			= null;
		this.onDragStart	= $.Event;
		this.onDragEnd		= $.Event;
		this._dragElement	= null;

		this.Cstr =
			function()
			{
				this._element.event.attach.mouseDown(this._mouseDown)
			}

		this._mouseDown =
			function(domEvent)
			{
				this.onDragStart.raise(obj)

				this._dragElement = this._element.clone()

				var position = this._element.offsetLocation;
				
				$.set //TODO: use Elmnt.set
				(
					this._dragElement,
					{
						style:
						{
							cursor:		$.css.Style.Cursor.Copy,
							position:	$.css.Style.Position.Absolute,
							left:		position.left,
							opacity:	0.5,
							top:		position.top,
							zIndex:		$.ui.LayerManager.maxZIndex
						}
					}
				)

				document.body.appendChild(this._dragElement)

				var odh = $.ui.ObjectDragHandler.NEW(this._element, this._dragElement)
				
				odh.onDragEnd.register(this._endDrag)
			
				odh.startDrag(domEvent) //revise not to pass in domEvent!
			}

		this._endDrag =
			function()
			{
try
{
	this._dragElement.removeElement()
}
catch (e)
{
	return;
}

				this.onDragEnd.raise()
			}
	}
)
