﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
		prototype: UA.views.ViewBase,
		resources: ["resource/html/views/ExpressionBuilderView.html", "resource/less/views/ExpressionBuilderView.less"],

		construct: //remove
		{
			noCSS:true,
		},
	},

	function ExpressionBuilderView()
	{
		var $=iCL, $ui=$.ui, $controls=$.ui.controls, $extenders=$ui.extenders, $DialogWindow=$ui.windows.DialogWindow;

		this._dw             = $DialogWindow.VAR;
		this._saveExpression = DELEGATE("title", "expression", "plotAliases")

this._elmntIDs = ["_txtTitle", "_txtExpression", "_tblPlotAliases", "_divTools"]; if (window.intellisense){this._div.getElmntsById(this._elmntIDs, this)}

		this.Cstr =
			function()
			{
				$extenders.InputCharFilter.NEW(this._txtTitle, /^[a-zA-Z]+[a-zA-Z0-9]*$/).attach() //get regex from presenter
			}

		this.Dispose =
			function()
			{
				$base.Dispose.apply(this, arguments)

				this._div.removeElement()

				this._div = null;
			}

		this._preRender =
			function()
			{
				UA.BuilderEnviornment.instance._div.appendChild(this._div) //move out
				
				this._dw = $DialogWindow.NEW(this._div, "Create Expression").SET({open:[true],})
			}

		this.show = //move to base
			function()
			{
				this._dw.show()
			}

		this._createControlGroup =
			function()
			{
				return $ui.ControlsGroup.NEW(this._dw.contentElement, this)
			}

		this.addPlotAlias =
			function(id, name, alias, color, equipmentName)
			{
				///<param name="id" type="Number" />
				///<param name="name" type="String" />
				///<param name="alias" type="String" />
				///<param name="color" type="String" />

				var $TreeView = $controls.TreeView;

				//
				//add to list view

				var getField = $controls.TableView.CAST(this._controlsGroup.getControlByElement(this._tblPlotAliases)).addRow().getField;

				var txtAlias = getField("alias")

				getField("id"   ).value=id; getField("name").value=(equipmentName + " - " + name); txtAlias.value=alias; getField("color").element.style.backgroundColor = color; //revise!!! use wireup

				$extenders.InputCharFilter.NEW(txtAlias.element, RegExp.asStartEnd(UA.expression.Expression.variableRegex)).attach() //get regex from presenter

				//

				//var treeView = $TreeView.CAST(this._controlsGroup.getControlById("ulA")) //rename ulA!

				//var node = treeView.getNodeByID(equipmentName)

				//var pointsNode = $TreeView.TreeNode.VAR;

				//if (node)
				//{
				//	var n = treeView.getNodeByID(equipmentName)

				//	pointsNode = n.subNodes.item(1)
				//}
				//else
				//{
				//	var n = treeView.createAndAppendNode(null, equipmentName, equipmentName)

				//	treeView.createAndAppendNode(n, "Variables")
					
				//	pointsNode = treeView.createAndAppendNode(n, "Points")
				//}

				//
				
				//var textInput = HTMLElement.create(HTMLInputElement, {type:"text",})

				//$extenders.TextInputMirror.NEW(textInput).SET({targetInput:txtAlias,}).attach()

				//var node = treeView.createNode(name).SET({rightFloatingHTML:textInput})
				
				//treeView.appendNode(node, pointsNode)
			}

		this._b_click =
			function(ea)
			{
				///<param name="ea" type="$.ui.controls.ButtonBar.ButtonClickArgs" />

				$controls.ViewToggle.CAST(this._controlsGroup.getControlById("divPlotAliases")).toggle(ea.button.index)
			}

		this._btnSave_click =
			function()
			{
				//
				//refine... use auto validation... move out to presenter!  //move validation logic to presenter
				if (!this._txtTitle.value.trim()) return _rU(this.promptNotification("Please enter a title.")) //cant even use spaces anyways!?
				if (this._txtTitle.value.length > 25) return _rU(this.promptError("The title cannot be longer than 25 characters."))
				if (!this._txtExpression.value.trim()) return _rU(this.promptNotification("Please enter an expression."))
				//

				var plotAliases = $.collections.Dictionary.NEW(Number) //combine and use a .toDictionary?
				$controls.TableView.CAST
				(
					this._controlsGroup.getControlByElement(this._tblPlotAliases)).rows.forEach(function(row){return _rU(plotAliases.add(row.getField("alias").value, parseInt(row.getField("id").value)))}
				)

				var errMsg = this._saveExpression(this._txtTitle.value, this._txtExpression.value, plotAliases)
				
				if (errMsg) return _rU(this.promptNotification(errMsg))

				this._btnCancel_click()
			}

		this._createTooltip =
			function(usage, description)
			{
				return ("Usage:  " + usage + "\n\nDescription:\n" + description)
			}

		this.addFunction =
			function(category, name, usage, description)
			{
                var buttonBar = $controls.ButtonBar.CAST(this._controlsGroup.getControlById("ul" + category))
                //var button = buttonBar.addButton(name)
                var button = $controls.ButtonBar.MomentaryButton.NEW(buttonBar, HTMLElement.create(HTMLLIElement, {textContent:name,}))
				buttonBar.appendButton(button)
                button.element.tooltip = this._createTooltip(usage, description)
			}

		this.title_set =
			function(value)
			{
				this._txtTitle.value = value;
			}

		this.expression_set =
			function(value)
			{
				this._txtExpression.value = value;
			}

		this._btnCancel_click =
			function()
			{
				this._dw.close()
			}
	}
)
