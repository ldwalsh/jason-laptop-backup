﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace:	UA.views,
		prototype:	UA.views.WindowedViewBase,
	},

	function AnalysisExplorerWView()
	{
		var controlGroup;

				var popupMenu;	//remove when popup menu is .ControlBase
				var popupMenuAnalysis;

		//

		this._elmntIDs							= ["_divAnalysisTitle", "_cm", "_pmAnalysis"]

		this._mainSubroutineId					= Number.VAR;

		this.nameCriteria						= "";

		this.runAnalysis						= DELEGATE()
		this.viewAnalysisProps					= DELEGATE()
		this.viewAnalysisEquipmentAssociation	= DELEGATE()

		this.createSubroutine					= DELEGATE()
		this.importSubroutine					= DELEGATE()

		this.viewSubroutine						= DELEGATE()
		this.viewSubroutineProps				= DELEGATE()
		this.removeSubroutine					= DELEGATE()
		this.setSubroutineAsMain				= DELEGATE()

		this.clearSubroutines =
			function()
			{
				//var tv = controlGroup.getControlById("tv", $.ui.controls.TreeView)
			}
	
		this.addSubroutineToList =
			function(subroutineName, subroutineID)
			{
				var tv = controlGroup.getControlById("tv", $.ui.controls.TreeView)

				var subroutineNode = tv.createAndAppendNode(tv.getNodeByID("subroutines"), subroutineName, subroutineID) //revise using subroutineID as ID for element!!!

				subroutineNode.onDblClick.attach(this._nodeDblClick)

				popupMenu.addTargetElement(subroutineNode.li)
			}

		this.removeSubroutineFromList =
			function(subroutineId)
			{
				var tv = controlGroup.getControlById("tv", $.ui.controls.TreeView)

					tv.removeNode(tv.getNodeByID(subroutineId))
			}

		this.renameSubroutineInList =
			function(subroutineID, newName)
			{
				var tv = controlGroup.getControlById("tv", $.ui.controls.TreeView)

				var node = tv.getNodeByID(subroutineID)

				node.text = newName;
			}

		this.markAsMainSubroutine =
			function(subroutineObjId)
			{
				var tv = controlGroup.getControlById("tv", $.ui.controls.TreeView)

				if (this._mainSubroutineId)
				{
					tv.getNodeByID(this._mainSubroutineId).li.classList.remove("MainSubroutine")
				}

				tv.getNodeByID(subroutineObjId).li.classList.add("MainSubroutine")

				this._mainSubroutineId = subroutineObjId;

			}

		this._init =
			function()
			{
				controlGroup = $.ui.ControlsGroup.NEW(this._div) //use ContentBase to render ControlsGroup automagically!

				controlGroup.render()
								
				controlGroup.getControlById("buttonBar", $.ui.controls.ButtonBar).onButtonClick = this._barButtonClick;
				
				//
				
				this._window = UA.views.DisplayWindow.NEW(this, this._div, "Analysis Explorer")
				
				this._window.resizeable = true;
				
				//this._window.open() handled by base

				//

				popupMenu = $.ui.PopupMenu.NEW(this._cm)
				popupMenu.onClick.register(this._subroutinePopupMenuAction)

				popupMenuAnalysis = $.ui.PopupMenu.NEW(this._pmAnalysis, this._divAnalysisTitle)
				popupMenuAnalysis.onClick.register(this._analysisPopupMenuAction)
			}
			
		this._barButtonClick =
			function(eventArgs)
			{
				///<param name="eventArgs" type="$.ui.controls.ButtonBar.ButtonClickArgs" />

				var o =
					{
						"add":
						function()
						{
							$.ui.windows.InputBox.NEW("Enter name for new subroutine.", new RegExp(this.nameCriteria)).show
							(
								function(value)
								{
									if (!value) return;
										
									this.createSubroutine(value)
										
								}.bind(this)
							)

						}.bind(this),

						"import": this.importSubroutine,
						"run":	  this.runAnalysis
					}

				o[eventArgs.button.name]()
			}

		this._analysisPopupMenuAction = //rename, revise!
			function(eventArgs)
			{
				///<param name="eventArgs" type="$.ui.controls.ContextMenu.ClickEventArgs" />
				
				Function.ifCall
				(
					{
						"Run":						this.runAnalysis,
						"Properties":				this.viewAnalysisProps,
						"Equipment Association":	this.viewAnalysisEquipmentAssociation,
					
					}[eventArgs.menuItem]
				)
			}

		this._subroutinePopupMenuAction = //rename, revise!
			function(eventArgs)
			{
				///<param name="eventArgs" type="$.ui.controls.ContextMenu.ClickEventArgs" />

				var id = parseInt(eventArgs.element.dataID)
				
				switch (eventArgs.menuItem)
				{
					case "Properties":
					{
						this.viewSubroutineProps(id)

						return;
					}
					case "Remove":
					{
						this.removeSubroutine(id)
												
						return;
					}
					case "Set As Main":
					{
						this.setSubroutineAsMain(id)

						return;
					}
				}

			}

		this._nodeDblClick =
			function(eventArgs)
			{
				///<param name="eventArgs" type="$.ui.controls.TreeView.NodeClickEventArgs" />

				this.viewSubroutine(parseInt(eventArgs.treeNode.id))
			}

		this._analysisTitle_get = //DONT ACTUALLY NEED BUT contruct2 requires a GET for property creation
			function(value)
			{
				return this._divAnalysisTitle.innerHTML;
			}
			
		this._analysisTitle_set =
			function(value)
			{
				this._divAnalysisTitle.innerHTML = value;
			}
	}
)
