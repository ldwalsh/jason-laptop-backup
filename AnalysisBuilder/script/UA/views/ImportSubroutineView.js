﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.views,
		prototype: UA.views.ViewBase,
	},

	function ImportSubroutineView()
	{
		var cstr = UA.views.ImportSubroutineView;
		var base = cstr.prototype;

		this._elmntIDs = ["cmbAnalysis", "imgLoadingAnalysis", "cmbSubroutines", "imgLoadingSubroutines"]

		this.analysisChanged	= DELEGATE()
		this.importSubroutine	= DELEGATE()

		this.Cstr =
			function()
			{
				this.loadingAnalysisThrobToggle		= this._imgLoadingAnalysis.toggleVisibility;
				this.loadingSubroutineThrobToggle	= this._imgLoadingSubroutines.toggleVisibility;

				//

				this.clearView()

				this.window = $.ui.windows.DialogWindow.NEW(this._div, "Import Subroutine"); //move to _init
				this.window.open(true)
			}

		this.show =
			function()
			{
				this.window.show()
			}

		this.addVisuToList =
			function(analysisName, analysisID)
			{
				this._cmbAnalysis.addOption(analysisName, analysisID)
			}

		this.clearSubroutineList =
			function()
			{
				this._cmbSubroutines.options.length = 0;
			}

		this.addSubroutineToList =
			function(subroutineName, subroutineID)
			{
				this._cmbSubroutines.addOption(subroutineName, subroutineID)
			}

		this.removeSubroutineFromList =
			function(subroutineID)
			{
				this._cmbSubroutines.removeOptionByValue(subroutineID)
			}

		this._cmbAnalysis_change =
			function()
			{
				this.analysisChanged(parseInt(this._cmbAnalysis.value))
			}

		this._btnImport_click =
			function()
			{
				if (!this._cmbSubroutines.value)
				{
					debugger;return;
				}

				this.importSubroutine(parseInt(this._cmbSubroutines.value))
			}
	}
)

