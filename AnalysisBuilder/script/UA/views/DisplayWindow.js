﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
		prototype: iCL.ui.windows.ToolWindow,
	},

	function DisplayWindow(windowedView, contentElement, title)
	{
		///<param name="windowedView" type="UA.views.WindowedViewBase" />
		///<param name="contentElement" type="HTMLDivElement" />
		///<param name="title" type="String" />

		var $base = this.constructor.prototype;

		this._windowedView	= UA.views.WindowedViewBase.VAR;
		this._dwInfo		= UA.views.DWInfo.VAR;
			
		this.open =
			function(dontShow)
			{
				$base.open.apply(this, [ifNull(dontShow, true)])
			}

		this._show =
			function()
			{
				$base._show.apply(this, arguments)

				if (!this._windowedView.displayID) return;

				UA.views.WindowService.instance.getDWInfo(this._windowedView.displayID).onSuccess.attach
				(
					function(dwInfo)
					{
						///<param name="dwInfo" type="UA.views.DWInfo" />

						this._dwInfo = dwInfo;

						this.left = dwInfo.x;
						this.top  = dwInfo.y;

						if (this._resizeable)
						{
							this.width  = dwInfo.w;
							this.height = dwInfo.h;

							//revise location
							//this._elmnts.divBody.pixelHeight = Math.round(parseFloat(this._windowDiv.computedStyle.height)) - this._elmnts.divTitleBar.pixelHeight;
						}

						//this.onWindowInfo.raise()

					}.bind(this)
				)
			}

		this._dragEnd =
			function(eA)
			{
				if (this._windowedView.displayID)
				{
					this._dwInfo.x = eA.endLocation.x;
					this._dwInfo.y = eA.endLocation.y;

					UA.views.WindowService.instance.setDWInfo(this._dwInfo)
				}
			}
		
		this._resized =
			function()
			{
				if (this._windowedView.displayID)
				{
					var size = this._windowDiv.size;

					this._dwInfo.w = size.w;
					this._dwInfo.h = size.h;

					UA.views.WindowService.instance.setDWInfo(this._dwInfo)
				}

				this._windowedView._resized() //change design?
			}

		this.windowClass_get =
			function()
			{
				return iCL.getCstrName(this.constructor.baseConstructor)
			}

		this.windowedView_get =
			function()
			{
				return this._windowedView;
			}
	}
)
