﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.views,
	},

	function NewAnalysisView()
	{
		this.closed		  = DELEGATE()
		this.nameCriteria = "";

		this.show =
			function()
			{
				$.ui.windows.InputBox.NEW("Enter new analysis name.", new RegExp(this.nameCriteria)).show(this.closed)
			}
	}
)
