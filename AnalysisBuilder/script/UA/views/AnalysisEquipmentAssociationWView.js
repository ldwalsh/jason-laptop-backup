﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace:	UA.views,
		prototype:	UA.views.WindowedViewBase,
	},

	function AnalysisEquipmentAssociationWView()
	{
		this._elmntIDs			= ["_cmbProfiles", "_btnCreateProfile", "_btnDeleteProfile", "_btnCreateAssociation"]
		this._controlsGroup		= $.ui.ControlsGroup.VAR;
		this._repeater			= $.ui.controls.Repeater.VAR;

		this.addProfile			= DELEGATE()
		this.profileSelected	= DELEGATE()
		this.buildEquipmentList	= DELEGATE()
		this.associationChanged = DELEGATE()
		this.deleteAssociation	= DELEGATE()

		//

		this.Cstr =
			function()
			{
				this._controlsGroup = $.ui.ControlsGroup.NEW(this._div)
				this._controlsGroup.render()

				this._repeater = this._controlsGroup.getControlById("divEquipmentAssociations", $.ui.controls.Repeater)
			}

		this._init =
			function()
			{
				//should be handled by base
				this._window = UA.views.DisplayWindow.NEW(this, this._div, "Analysis Equipment Association")
				//this._window.open()
				//

				this._btnDeleteProfile.visible = this._btnCreateAssociation.visible = false;
			}

		this.populateAssociationProfiles =
			function(optionObjs)
			{
				///<param name="optionObjs" type="Array" elementType="HTMLSelectElement.OptionObject" />

				this._cmbProfiles.clearOptions()
				
				for (var i=0; i<optionObjs.length; i++)
				{
					this._cmbProfiles.addOption(optionObjs[i])
				}
			}

		this.populateEquipmentClasses =
			function(optionObjs)
			{
				///<param name="optionObjs" type="Array" elementType="HTMLSelectElement.OptionObject" />

				var cmb = this._repeater.getTemplateChildElement("cmbEquipmentClass")

				cmb.addOption(HTMLSelectElement.OptionObject.empty)

				optionObjs.forEach(function(value){cmb.addOption(value)})
			}

		this.clearEquipmentAssociations =
			function()
			{
				this._repeater.clear()
			}

		this.addEquipmentAssociation =
			function(assocId, alias, equipmentClassId, equipmentId, note)
			{
				var ri = this._repeater.createRI()

				var imgDelete			= ri.getChildElement("imgDelete")
				var hdnAssociationId	= ri.getChildElement("hdnAssociationId")
				var txtEquipmentAlias	= ri.getChildElement("txtEquipmentAlias")
				var cmbEquipmentClass	= ri.getChildElement("cmbEquipmentClass")
				var cmbEquipment		= ri.getChildElement("cmbEquipment")

				hdnAssociationId.value	= assocId;
				txtEquipmentAlias.value = alias;
				cmbEquipmentClass.value = equipmentClassId;

				this._populateEquipment(cmbEquipmentClass, cmbEquipment)

				cmbEquipment.value		= equipmentId;

				cmbEquipmentClass.event.attach.change
				(
					function(domEvent)
					{
						this._populateEquipment(cmbEquipmentClass, cmbEquipment)
							
					}.bind(this)
				)

				cmbEquipment.event.attach.change
				(
					function()
					{
						this.associationChanged(hdnAssociationId.numericValue, txtEquipmentAlias.value, cmbEquipment.numericValue)

					}.bind(this)
				)

				imgDelete.event.attach.click
				(
					function()
					{
						this.deleteAssociation(hdnAssociationId.numericValue)

					}.bind(this)
				)	
			}

		this.removeEquipmentAssociation =
			function(assocId)
			{
				this._repeater.repeats.forEach
				(
					function(ri)
					{
						///<param name="ri" type="$.ui.controls.Repeater.RepeatInstance" />

						if (ri.getChildElement("hdnAssociationId").numericValue != assocId) return;

						this._repeater.removeRI(ri)

						return false;

					}.bind(this)
				)
			}

		this._populateEquipment =
			function(cmbEquipmentClass, cmbEquipment)
			{
				///<param name="cmbEquipmentClass" type="HTMLSelectElement" />
				///<param name="cmbEquipment" type="HTMLSelectElement" />

				cmbEquipment.clearOptions()

				if (cmbEquipmentClass.numericValue == -1) return;

				var equipment = this.buildEquipmentList(cmbEquipmentClass.numericValue)

				cmbEquipment.addOption(HTMLSelectElement.OptionObject.empty)

				for (var i=0; i<equipment.length; i++)
				{
					cmbEquipment.addOption(equipment[i].text, equipment[i].value)
				}
			}

		this._btnCreateProfile_click =
			function()
			{
				var ib = $.ui.windows.InputBox.NEW("Enter name of new profile.")

				ib.show(this.addProfile)
			}

		this._btnDeleteProfile_click =
			function()
			{	
			}

		this._btnCreateAssociation_click =
			function()
			{
				if (this._repeater.repeats.length)
				{
					var ri = this._repeater.repeats.getLast()

					if (!ri.getChildElement("hdnAssociationId").numericValue) return;
				}

				this.addEquipmentAssociation(0, "", -1, -1)
			}

		this._cmbProfiles_change =
			function()
			{
				this.profileSelected(this._cmbProfiles.numericValue)

				this._btnDeleteProfile.visible = this._btnCreateAssociation.visible = (this._cmbProfiles.numericValue > -1)
			}
	}
)
