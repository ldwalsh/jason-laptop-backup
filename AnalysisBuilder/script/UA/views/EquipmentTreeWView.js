﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
		prototype: UA.views.WindowedViewBase,
		resources: ["resource/html/views/EquipmentTreeWView.html", "resource/less/views/EquipmentTreeWView.less"],
		construct: //remove
		{
			noCSS:true,
		},
	},

	function EquipmentTreeWView()
	{
		var BldngIdnt       = EquipmentTreeWView.BldngIdnt
		var EqpmntClassIdnt = EquipmentTreeWView.EqpmntClassIdnt
		var EqpmntIdnt      = EquipmentTreeWView.EqpmntIdnt
		var PntIdnt         = EquipmentTreeWView.PntIdnt

		this._elmntIDs                = ["_ulTree", "_divTree"]
		this._tree                    = iCL.ui.controls.TreeView.VAR;
		this.buildingExpanded         = DELEGATE("buildingId")
		this.equipClassExpanded       = DELEGATE()
		this.equipExpanded            = DELEGATE()
		this.eqpmntAssociatedExpanded = DELEGATE()
		this.pointSelected            = DELEGATE("pointId", "equipmentName")
		this.displayID_get            = function(){return null;}		

		this._init =
			function()
			{
				UA.BuilderEnviornment.instance._div.appendChild(this._div)

				this._window =
				UA.views.DisplayWindow.NEW(this, this._div, "Equipment Tree").SET
				(
					{
						hasTitlebar:false,
						onOpened:
						function()
						{
							iCL.ui.binders.ResizeBinder.NEW(this._window.windowDiv, $gId("divEquipmentTree")) //revise

							this._window.windowDiv.getElmntById("divOuterBody")

						}.bind(this),
					}
				)
			}

		this._render =
			function()
			{
				this._tree = this._controlsGroup.getControlById(this._ulTree.dataID, iCL.ui.controls.TreeView)
			}

		this._buildingExpandedCaller =
			function(ea)
			{
				///<param name="ea" type="o.NodeClickEventArgs" />

				ea.treeNode.isLoading = true;

				this.buildingExpanded(parseInt(ea.treeNode.id))
			}

		this._equipmentClassExpandedCaller =
			function(ea)
			{
				///<param name="ea" type="o.NodeClickEventArgs" />

				var split = ea.treeNode.SET({isLoading:true,}).id.split(".").transform(parseInt)
				
				this.equipClassExpanded(split[0], split[1])
			}

		this._equipmentExpandedCaller =
			function(ea)
			{
				///<param name="ea" type="o.TreeView.NodeClickEventArgs" />

				var idSplit = ea.treeNode.SET({isLoading:true,}).id.split(".").transform(parseInt);

				var idnt = EqpmntIdnt.NEW(EqpmntClassIdnt.NEW.invoke(idSplit), idSplit[2])
		
				this.equipExpanded(idnt.bldngId, idnt.eqpmntClassId, idnt.eqpmntId, idnt.isAssociated)
			}

		this.addBuilding =
			function(name, id, isLast)
			{
				///<param name="name" type="String" />
				///<param name="id" type="String" />
				///<param name="isLast" type="Boolean" optional="true" />

				var node = this._tree.createAndAppendNode(null, name, BldngIdnt.NEW(id)).SET({isExpanded:false, onFirstExpand:this._buildingExpandedCaller,})

				if (!isLast || (this._tree.nodes.count > 1)) return;
				
				node.isExpanded = true;
			}

		this.selectPoints =
			function(bldngId, eqpmntClassId, eqpmntId, points)
			{
				///<param name="bldngId" type="Number" />
				///<param name="eqpmntClassId" type="Number" />
				/// <param name="name" type="String">Description</param>

				this._loadNode(bldngId)

				MethodIntercept
				(
					this.addEquipmentClass,
					function(f, args, cancelIntercept)
					{
						f.apply(null, args)

						if (args[1]) return;

						this._loadNode(bldngId, eqpmntClassId)

						cancelIntercept()
					}
				)

				MethodIntercept
				(
					this.addEquipment,
					function(f, args, cancelIntercept)
					{
						f.apply(null, args)

						if (args[2]) return;

						this._loadNode(bldngId, eqpmntClassId, eqpmntId)

						cancelIntercept()
					}
				)

				if (!points) return;

				MethodIntercept
				(
					this.addPoint,
					function(f, args, cancelIntercept)
					{
						f.apply(null, args)
						
						if (args[3]) return;
						
						this._selectPoints(bldngId, eqpmntClassId, eqpmntId, points)

						cancelIntercept()					
					}
				)
			}

		this._loadNode =
			function(bldngId, eqpmntClassId, eqpmntId)
			{
				if (window.intellisense) return;

				var Idnt;

				if (!eqpmntClassId)
				{
					Idnt = BldngIdnt;
				}
				else if (!eqpmntId)
				{
					Idnt = EqpmntClassIdnt
				}
				else
				{
					Idnt = EqpmntIdnt;
				}

				this._tree.getNodeByID(Idnt.NEW.apply(null, arguments)).expand()
			}

		this._selectPoints =
			function(bldngId, eqpmntClassId, eqpmntId, pntIds)
			{
				pntIds.forEach(function(p){return _rU(this.pointSelected(bldngId, eqpmntId, p.id, true))}.bind(this))
			}

		this.addEquipmentClass =
			function(buildingId, name, id)
			{
				var bldngIdnt = BldngIdnt.NEW(buildingId)

				if (!name) return rU(this._tree.getNodeByID(bldngIdnt).isLoading = false)
				
				this._tree.createAndAppendNode(bldngIdnt, name, EqpmntClassIdnt.NEW(buildingId, id)).SET({isExpanded:false, onFirstExpand:this._equipmentClassExpandedCaller,})
			}

		this.addEquipment =
			function(buildingId, equipmentClassId, name, id)
			{
				var eqpmntClassIdnt = EqpmntClassIdnt.NEW(buildingId, equipmentClassId)

			    if (!name) return _rU(this._tree.getNodeByID(eqpmntClassIdnt).SET({isLoading:false, /*isExpanded:!n.subNodes.length,*/}))

				this._tree.createAndAppendNode(eqpmntClassIdnt, name, EqpmntIdnt.NEW(eqpmntClassIdnt, id)).SET({isExpanded:false, onFirstExpand:this._equipmentExpandedCaller,})
			}

		this.addAssociatedEquipment =
			function(buildingId, equipmentClassId, equipmentId, id, name)
			{
				var t = this._tree;

				var eqpmntIdnt = EqpmntIdnt.NEW(buildingId, equipmentClassId, equipmentId)
				
				var g = t.getNodeByID(eqpmntIdnt)

				eqpmntIdnt.isAssociated = true;
				
				t.createAndAppendNode
				(
					(g.subNodes.singleOrNull({id:/Associated$/}) || t.createAndAppendNode(g, "Associated", eqpmntIdnt)), name, EquipmentTreeWView.PntIdnt.NEW(eqpmntIdnt, id)
				
				).SET({isExpanded:false, onFirstExpand:this.eqpmntAssociatedExpanded.getArgsCaller([buildingId, equipmentClassId, equipmentId, id]),})
			}

		this.addPoint =
			function(buildingId, equipmentClassId, equipmentId, name, id)
			{
				var t = this._tree;

				var eqpmntIdnt = EqpmntIdnt.NEW(buildingId, equipmentClassId, equipmentId);
				
				var n = t.getNodeByID(eqpmntIdnt) //.SET({isLoading:false,}) //revise because no need to keep resetting isLoading:false
				
				if (!name) return _rU(n.isLoading = false)

				this._configureN(t.createAndAppendNode(n, name, EquipmentTreeWView.PntIdnt.NEW(eqpmntIdnt, id)), buildingId, equipmentId, id, name)

				//n._li.tooltip = ("ID: " + id) //revise
				//n.onDblClick.register(this.pointSelected.getArgsCaller([equipmentId, id, n.text]))
			}

		this.addAssociatedPoint =
			function(buildingId, equipmentClassId, parentEquipmentId, equipmentId, id, name)
			{
				var t = this._tree;
				
				var associatedEqpmntIdnt = EquipmentTreeWView.AssociatedEqpmntIdnt.NEW(buildingId, equipmentClassId, parentEquipmentId, equipmentId)

				var n = t.getNodeByID(associatedEqpmntIdnt) //.SET({isLoading:false,}) //revise because no need to keep resetting isLoading:false

				if (!name) return;

				//var n = t.createAndAppendNode(n, name, (associatedEqpmntIdnt + "." + id))

				this._configureN(t.createAndAppendNode(n, name, (associatedEqpmntIdnt + "." + id)), buildingId, equipmentId, id, name)

				//n._li.tooltip = ("ID: " + id) //revise				
				//n.onDblClick.register(this.pointSelected.getArgsCaller([equipmentId, id, n.text]))
			}

		this._configureN =
			function(node, buildingId, equipmentId, id, name)
			{
				node._li.tooltip = name;

				node.onDblClick.register(this.pointSelected.getArgsCaller([buildingId, equipmentId, id]))
			}
	}
)



function MethodIntercept(f1, f2)
{
	//
	if (window.intellisense) return Function.VAR;
	//

	//validate arguments are compatible (name, type?)

	var methodName = f1.methodName;
	
	f1.THIS[methodName] = function(){return f2.apply(f1.THIS, [f1, arguments, function(){return _rU(f1.THIS[methodName] = f1)}])}
}

	

Object.define
(
	{
		namespace: UA.views.EquipmentTreeWView,
		prototype: String,
	},

	function BldngIdnt(bldngId)
	{
		this.toString = function(){return this.valueOf()}
		this.valueOf  = function(){return bldngId.toString()}
	}
)

Object.define
(
	{
		namespace: UA.views.EquipmentTreeWView,
		prototype: String,
	},

	function EqpmntClassIdnt(bldngId, eqpmntClassId)
	{
		this.bldngId       = Number.VAR;
		this.eqpmntClassId = Number.VAR;
		
		this.toString = function(){return this.valueOf()}
		this.valueOf  = function(){return [this.bldngId, this.eqpmntClassId].join(".")}
	}
)

Object.define
(
	{
		namespace: UA.views.EquipmentTreeWView,
		prototype: String,
	},

	function EqpmntIdnt(bldngId, eqpmntClassId, eqpmntId, isAssociated)
	{
		///<signature>
		/// <param name="bldngId" type="Number" />
		/// <param name="eqpmntClassId" type="Number" />
		/// <param name="eqpmntId" type="Number" />
		/// <param name="isAssociated" type="Boolean" optinoal="true" />
		///</signature>
		///<signature>
		/// <param name="eqpmntClassIdnt" type="o.EqpmntClassIdnt" />
		/// <param name="eqpmntId" type="Number" />
		/// <param name="isAssociated" type="Boolean" optinoal="true" />
		///</signature>

		this.bldngId       = Number.VAR;
		this.eqpmntClassId = Number.VAR;
		this.eqpmntId      = Number.VAR;
		this.isAssociated  = Boolean.VAR;
		
		this.Cstr =
			function()
			{
				if (arguments[0] instanceof UA.views.EquipmentTreeWView.EqpmntClassIdnt)
				{
					this.Cstr_A.apply(this, arguments)
				}
				else
				{
					this.Cstr_B.apply(this, arguments)
				}
			}

		this.Cstr_A =
			function(eqpmntClassIdnt, eqpmntId, isAssociated)
			{
				///<param name="eqpmntClassIdnt" type="o.EqpmntClassIdnt" />
				///<param name="eqpmntId" type="Number" />
				///<param name="isAssociated" type="Boolean" optinoal="true" />

				this.bldngId       = eqpmntClassIdnt.bldngId;
				this.eqpmntClassId = eqpmntClassIdnt.eqpmntClassId;
				this.eqpmntId      = eqpmntId;
				this.isAssociated  = (isAssociated || false)
			}

		this.Cstr_B =
			function(bldngId, eqpmntClassId, eqpmntId, isAssociated)
			{
				///<param name="bldngId" type="Number" />
				///<param name="eqpmntClassId" type="Number" />
				///<param name="eqpmntId" type="Number" />
				///<param name="isAssociated" type="Boolean" optinoal="true" />

				this.isAssociated = (this.isAssociated || false)
			}

		this.toString = function(){return this.valueOf()}
		this.valueOf  = function(){return [this.bldngId, this.eqpmntClassId, this.eqpmntId,].concat(this.isAssociated ? ["Associated"] : []).join(".")}
	}
)

Object.define
(
	{
		namespace: UA.views.EquipmentTreeWView,
		prototype: String,
	},

	function AssociatedEqpmntIdnt(bldngId, eqpmntClassId, parentEqpmntId, eqpmntId)
	{
		this.bldngId        = Number.VAR;
		this.eqpmntClassId  = Number.VAR;
		this.parentEqpmntId = Number.VAR;
		this.eqpmntId       = Number.VAR;

		this.toString = function(){return this.valueOf()}
		this.valueOf  = function(){return [this.bldngId, this.eqpmntClassId, this.parentEqpmntId, "Associated", this.eqpmntId].join(".")}		
	}
)


Object.define
(
	{
		namespace: UA.views.EquipmentTreeWView,
		prototype: String,
	},

	function PntIdnt(bldngId, eqpmntClassId, eqpmntId, pntId)
	{
		this.bldngId       = Number.VAR;
		this.eqpmntClassId = Number.VAR;
		this.eqpmntId      = Number.VAR;
		this.pntId         = Number.VAR;

		this.isAssociated  = Boolean.VAR;

		this.Cstr =
			function()
			{
				if (arguments[0] instanceof UA.views.EquipmentTreeWView.EqpmntIdnt)
				{
					this.Cstr_A.apply(this, arguments)
				}
				else
				{
					this.Cstr_B.apply(this, arguments)
				}
			}

		this.Cstr_A =
			function(eqpmntIdnt, pntId)
			{
				///<param name="eqpmntIdnt" type="o.EqpmntIdnt" />

				this.bldngId       = eqpmntIdnt.bldngId;
				this.eqpmntClassId = eqpmntIdnt.eqpmntClassId;
				this.eqpmntId      = eqpmntIdnt.eqpmntId;
				this.pntId         = pntId;

				this.isAssociated  = eqpmntIdnt.isAssociated;
			}

		this.Cstr_B =
			function()
			{
				///<param name="eqpmntIdnt" type="o.EqpmntIdnt" />

				this.bldngId       = bldngId;
				this.eqpmntClassId = eqpmntClassId;
				this.eqpmntId      = eqpmntId;
				this.pntId         = pntId;
			}

		this.toString = function(){return this.valueOf()}
		this.valueOf  = function(){return [this.bldngId, this.eqpmntClassId, this.eqpmntId,].concat(this.isAssociated ? ["Associated"] : []).concat([this.pntId]).join(".")}
	}
)
