﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
		prototype: UA.views.ViewBase,
		resources: ["resource/html/views/OpenAnalysisView.html", "resource/less/views/OpenAnalysisView.less"],
		construct: {noCSS:true, _cells:["Id", "Name", "Created", "Modified", "Description", "OwnerId", "Owner", "Read", "Write", ""],}, //remove
	},

	function OpenAnalysisView()
	{
		var $ui = iCL.ui;

		this._title          = "Open Visualization"; //move to base
		this._dw             = $ui.windows.DialogWindow.VAR;
		this._controlsGroup  = $ui.ControlsGroup.VAR;
		this._table          = $ui.controls.Table.VAR;
		this._alreadyViewing = Number.VAR;
		this.visuSelected    = DELEGATE("id")
		this.visuDeleted     = DELEGATE("id")
		this._siteUserId     = Number.VAR; //remove this hack
						
		this.Cstr =
			function()
			{
				UA.BuilderEnviornment.instance._div.appendChild(this._div) //move out
				
				this._dw = $ui.windows.DialogWindow.NEW(this._div, this._title) //move to base

				this._createControlGroup = Function.returnNull;
			}

		this.show =
			function()
			{
				this._dw.open() //move to base?

				this._controlsGroup = $ui.ControlsGroup.NEW(this._dw.contentElement, this).SET({render:null,}) //handled by ViewBase?

				this._table = $ui.controls.Table.CAST(this._controlsGroup.getControlById("divTable"))
				
				this._table.appendRow(true).appendCells(OpenAnalysisView._cells)
				
				this._dw.__h = addEventHandler("keypress", function(x){if (x.keyCode == 27) this.hide()}.bind(this)) //future feature tryout... need to integrate w/ DialogWindow
			}

		this.hide =
			function()
			{
				this._dw.__h() //realize .__h func pointer still exists... revise?

				this._dw.close()
			}

		this.clearVisus = //should replace with remove of deleted item instead
			function()
			{
				this._table.clear(true)

				//should also clear labels?
			}

		this.addVisuToList =
			function(id, name, created, modified, description, ownerId, owner, sharedRead, sharedWrite, isDeleteable)
			{
				///<param name="id" type="Number" />
				///<param name="name" type="String" />
				///<param name="created" type="Date" />
				///<param name="modified" type="Date" />
				///<param name="description" type="String" />
				///<param name="sharedWrite" type="Boolean" />
				///<param name="isDeleteable" type="Boolean" optional="true" />

				var f = "m/d/yy hh:mm"

				var deleteLink = "";

				var create = HTMLElement.create;

				if (isDeleteable) //revise
				{
					deleteLink = create(HTMLAnchorElement, {href:"javascript:", textContent:"delete",}); deleteLink.event.attach.click(this.visuDeleted.getArgsCaller([id]))
				}

				this._table.addRow("visus").appendCells
				(	
					[
						id,
						create(HTMLTableCellElement, {textContent:this._shortenName(name, 25), title:((name.length > 25) ? name : ""),}),
						created.format(f),
						modified.format(f),
						description,
						ownerId,
						create(HTMLTableCellElement, {textContent:this._shortenName(owner, 25), title:((owner.length > 25) ? owner : ""),}),
						create(HTMLInputElement, {type:HTMLInputElement.types.CHECKBOX, checked:sharedRead , disabled:"true",}), //pass in boolean true
						create(HTMLInputElement, {type:HTMLInputElement.types.CHECKBOX, checked:sharedWrite, disabled:"true",}), //pass in boolean true
						deleteLink,
					]
				)				
			}

		this._shortenName =
			function(str, length, suffix)
			{
				///<param name="str" type="String" />
				///<param name="length" type="Number" />
				///<param name="suffix" type="String" optional="true" />
					
				if (!str.trim()) return "";
					
				if (str.length <= length) return str;

				return (str.substr(0, length).trim() + (suffix || "..."))
			}

		this._endList =
			function(alreadyOpenedCount) //this method need not exist?
			{
				///<param name="alreadyOpenedCount" type="Number" />

				this._alreadyViewing = this._div.getElmntById("spnViewing").textContent = alreadyOpenedCount;

				this._table.addRowDone("visus")

				this._txtFilter_keyUp()

				this._table.element.classList.remove("ajaxLoad-Pik500078")
			}

		this._chkAllUsers_click =
			function()
			{
				this._txtFilter_keyUp()
			}

		this._txtFilter_keyUp = //move filter logic to presenter and repop instead!!! View should not have any filtering/sorting logic!!!
			function()
			{
				var txtFilter = this._div.getElmntById("txtFilter")

				var foundCount = 0;

				var Row = $ui.controls.Table.Row;

				this._table.rows.forEach
				(
					function(row)
					{
						///<param name="row" type="$.ui.controls.Table.RowBase" />
						
						if ((row instanceof Row) && row.isHeader) return; //to do: remove

						var found = (!((row.cells[5].textContent != this._siteUserId) && !this._showAllUsers))

						if (found)
						{
							found =
							row.cells.some
							(
								function(cell)
								{
									cell = Row.Cell.AS(cell)

									if (!cell.index) return;

									if (cell.textContent.toLowerCase().contains(txtFilter.value.toLowerCase())) return true;
								}
							)
						}

						if (found) {foundCount++;}

						row.rowElement.toggleDisplay(!!found)
					
					}.bind(this)
				)

				this._div.getElmntById("spnFound").textContent = (foundCount + this._alreadyViewing)
			}

		this._btnOpen_click =
			function()
			{
				if (!this._table._selectedRow) return rU(this.promptNotification("Select a visualization to open first."))

				this.visuSelected(this._table._selectedRow.cells[0].textContent.asValue())
				
				this.hide()
			}

		this._btnCancel_click =
			function()
			{
				this.hide()
			}

		this._showAllUsers =
			{
				get:function(){return this._div.getElmntById("chkAllUsers").checked;}
			}
	}
)
