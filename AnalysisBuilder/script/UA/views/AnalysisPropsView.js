﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace:	UA.views,
		prototype:	UA.views.ViewBase,
	},

	function AnalysisPropsView()
	{
		this._elmntIDs				= ["_cmbVariables", "_txtVariableName", "_txtVariableValue", "_txtName", "_btnAdd", "_btnRemove"]
		this._dw					= null; //required beause not inherited
		this._listPattern			= null;
		this._controlsGroup			= null;
		
		this.saveProps				= DELEGATE()

		this.Cstr =
			function()
			{
				this._dw = $.ui.windows.DialogWindow.NEW(this._div, "Analysis Properties")
				
				this._controlsGroup = $.ui.ControlsGroup.NEW(this._div)
				this._controlsGroup.render()
				
				//
				
				this._dw.open(true)
				
				//

				this._listPattern		= $.ui.patterns.List.NEW(this._cmbVariables, this._btnAdd, this._btnRemove)
				this._listPattern.owner = this;
			}

		this.show =
			function() //required beause not inherited from base
			{
				this._dw.show()
			}

		this.hide =
			function() //required beause not inherited from base
			{
				this._dw.hide()
			}

		this._listPatternOnAdd =
			function()
			{
				var name  = this._txtVariableName .value = this._txtVariableName .value.trim()
				var value = this._txtVariableValue.value = this._txtVariableValue.value.trim()

				if (!name)
				{
					$.ui.windows.MessageBox.open("Please enter a name for the user variable.")
				
					return;
				}
				
				if (!value)
				{
					$.ui.windows.MessageBox.open("Please enter a value for the user variable.")

					return;
				}

				return {text:(name + "=" + value), value:name}
			}

		this._listPatternOnRemove =
			function()
			{
				this._txtVariableName.value  = "";
				this._txtVariableValue.value = "";
			}

		this._listPatternOnSelectChange =
			function(optionObj)
			{
				///<param name="optionObj" type="HTMLSelectElement.OptionObject" />

				var t = optionObj.text.split("=")

				this._txtVariableName.value  = t[0]
				this._txtVariableValue.value = t[1]
			}

		this.userVariables_get =
			function()
			{
				return this._listPattern.value;
			}

		this.userVariables_set =
			function(value)
			{
				if (!value.length) return;

				for (var i=0; i<value.length; i++)
				{
					var uV = value[i]

					this._cmbVariables.addOption((uV.name + "=" + uV.value), uV.name)
				}
			}

		this.analysisName_get =
			function()
			{
				return this._txtName.value;
			}

		this.analysisName_set =
			function(value)
			{
				this._txtName.value = value;
			}
	}
)