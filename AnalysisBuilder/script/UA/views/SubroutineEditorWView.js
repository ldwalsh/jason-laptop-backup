﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.views,
		prototype: UA.views.WindowedViewBase,
		statics:
		{
			noCSS:true,
		},
	},

	function SubroutineEditorWView(subroutineName, subroutineId)
	{
		///<param name="subroutineName" type="String" />
		///<param name="subroutineId" type="Number" />

		this._templateDiv		= $gId("divSubroutineEditorWView")
		this._elmntIDs			= ["txtDefinition"]
		this._div				= this._templateDiv.clone()
		
		this.subroutineChanged	= DELEGATE()
		this.populateDefinition = Function.VAR;

		//

		this.Cstr =
			function()
			{
				//this._div				= new $.Elmnt(this._templateDiv.cloneNode(true))
				//this.subroutineChanged	= DELEGATE()
				//this.populateDefinition = this._div.getById("txtDefinition", HTMLInputElement).setValue;

				this.populateDefinition = this._txtDefinition.setValue;
			}

		this._init =
			function()
			{
				/*
				var tblSlots = this._div.getById("tblSlots")

				var row = HTMLTableRowElement.CAST(tblSlots.rows[0])

				for (var i=0; i<row.cells.length; i++)
				{
					var cell = row.cells[i]

					//cell.dE.attach.mouseOver(window.alert)
				}

				$.Document.current.body.dE.attach.mouseMove
				(
					function(domEvent)
					{
						row.cells[0].style.backgroundColor = "gray";

						if (!row.getBounds().within(domEvent.cursorLocation.x, domEvent.cursorLocation.y)) return;

						row.cells[0].style.backgroundColor = "yellow";

					}
				)
				*/

				//

				this._txtDefinition.event.attach.keyPress(this._txtDefinition_keyPress)

				//
				
				this._templateDiv.parentNode.appendChild(this._div)

				this._window = new UA.views.DisplayWindow(this, this._div, "Subroutine - " + subroutineName)

				this._window.onClosed.register(this._onClosed) //move to base

				//this._window.open(false)
			}

		this._onClosed =//move to base
			function()
			{
				this._div.removeElement()
			}

		this._txtDefinition_keyPress =
			function(domEvent)
			{
				dp.action()
			}

		this._x =
			function()
			{
				this.subroutineChanged(this._elmnts.txtDefinition.value)
			}

		this.displayID_get =
			function()
			{
				return ($.typeOf(this) + subroutineId).getHashCode()
			}


		$.class.instantiate(this, [])

var dp = new DP(this._x)


	}
)




function DP(actionFunc)
{
	this._stID = null;

	//this._caller =
	//	function()
	//	{
	//		actionFunc()

	//		this.start()
	//	}

	this.action =
		function()
		{
			if (this._stID)
			{
				window.clearTimeout(this._stID)

				this._stID = null;
			}

			this._stID = window.setTimeout(actionFunc, 1000)
		}

	this.stop =
		function()
		{
			this.action = function(){}
		}
}
