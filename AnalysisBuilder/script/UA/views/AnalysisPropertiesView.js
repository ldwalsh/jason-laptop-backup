﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
		prototype: UA.views.ViewBase,
		resources: ["resource/html/views/AnalysisPropertiesView.html", "resource/less/views/AnalysisPropertiesView.less"],
		statics: //remove
		{
			noCSS:true,
		},
	},

	function AnalysisPropertiesView()
	{
		this._title           = "Visualization Properties"; //move to base
		this._dw              = iCL.ui.windows.DialogWindow.VAR;
		this._populatedFields = iCL.TArray.VAR(String)
		this._fieldLookup     = iCL.KeyedArray.VAR(HTMLElement)
		this._canApply        = false;
		this.propsApplied     = DELEGATE()

		this.Cstr =
			function()
			{
				UA.BuilderEnviornment.instance._div.appendChild(this._div) //move out
				
				this._dw = iCL.ui.windows.DialogWindow.NEW(this._div, this._title).SET({open:[true]})

				this._populatedFields = new iCL.TArray(String)
				this._fieldLookup     = iCL.KeyedArray.NEW(HTMLElement)

				//

				this._div.getElements(HTMLTableElement).first().getElements().FOR //move to base?
				(
					function(element)
					{
						var id = element.dataID;

						if (!id) return;

						var i;

						for (i=0; i<id.length; i++) //revise
						{
							var c = iCL.Char.NEW(id[i])
							if (!c.isUpper) continue;
							break;
						}

						this._fieldLookup.push(String.toCamelCase(id.substr(i)), element)
					
					}.bind(this)
				)
			}

		this.show = //move to base
			function()
			{
				if (!this._canApply) this._div.getElmntById("btnApply").toggleDisplay(false) //revise

				this._dw.show()
			}

		this.populateField =
			function(name, value, canEdit)
			{
				var element = HTMLElement.AS(this._fieldLookup.keys[name])

				if (!element) return;

				if (name == "isSaved")
				{
					value = (value ? "*Saved" : "")
				}

				element.content = value;

				if (!canEdit)
				{
					switch (element.constructor)
					{
						case HTMLInputElement:
						{
							switch (element.type)
							{
								case "text":
								{
									element.createMaskingDiv().SET({style:{opacity:.25,}})

									break;
								}
								case "checkbox":
								{
									element.disabled = true;

									break;
								}
							}
						}
						case HTMLTextAreaElement:
						{
							element.createMaskingDiv().SET({style:{opacity:.25,}})

							break;
						}
					}
				}

				if ((name == "sharedWrite") && canEdit && (HTMLElement.AS(this._fieldLookup.keys["sharedRead"]).checked == true)) //revise?
				{
					element.disabled = false;
				}

				this._populatedFields.push(name)

				if (!canEdit) return;
				
				this._canApply = true;
			}

		this._chkSharedRead_click =
			function(dE)
			{
				///<param name="dE" type="$.DomEvent" />
				
				var checked = HTMLInputElement.CAST(dE.targetAsElmnt).checked;

				var chkSharedWrite = this._div.getElmntById("chkSharedWrite")

				chkSharedWrite.checked  = (checked ? chkSharedWrite.checked : false)
				chkSharedWrite.disabled = !checked;
			}

		this._btnApply_click =
			function()
			{
				var props = {}

				this._populatedFields.forEach
				(
					function(name)
					{
						props[name] = this._fieldLookup.keys[name].content; //.asValue()
						
					}.bind(this)
				)				

				this.propsApplied(props)

				this._dw.close()
			}

		this._btnClose_click =
			function()
			{
				this._dw.close()
			}
	}
)
