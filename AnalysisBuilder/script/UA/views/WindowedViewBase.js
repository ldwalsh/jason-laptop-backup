﻿///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
		prototype: UA.views.ViewBase,
		construct: {windowGroup:iCL.ui.WindowGroup.NEW(),},
	},

	function WindowedViewBase()
	{
		this._window = iCL.ui.ContentWindowBase.VAR;

		this.Cstr =
			function()
			{
				this._init()

				//TODO: check this._window is not null... populated by this._build

				if (this._window == null) throw new Error(4656546456456)

				this._window.onResizing = this._windowResizing

				this._window.open(true)
				
				WindowedViewBase.windowGroup.addWindow(this._window)
			}

		this.Dispose =
			function()
			{
				this._window.close()
				
				WindowedViewBase.windowGroup.removeWindow(this._window)
			}

		this.show =
			function()
			{
				this._window.show()

				WindowedViewBase.windowGroup.activateWindow(this._window)
			}
	
		this.hide =
			function()
			{
				this._window.hide()
			}
	
		this._init =
			function()
			{
				throw iCL.NotImplementedError.NEW("The EnviornmentWindow._build method must be implemented.")
			}

		this._windowResizing =
			function() //virtual
			{}

		this._resized =
			function()
			{}

		this.displayID_get =
			function()
			{
				return null; //return iCL.typeOf(this).getHashCode();
			}
	}
)
