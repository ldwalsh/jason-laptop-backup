﻿///
///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.views.controls,
		prototype: $.ui.controls.Accordion,
	},

	function FunctionsAccordion(div, functionsButtonBarTemplate)
	{
		///<param name="div" type="HTMLDivElement" />
		///<param name="functionsButtonBarTemplate" type="HTMLElement" />

		this._createContentDiv =
			function()
			{
				return HTMLElement.create (HTMLDivElement, {parentNode:this._accordionDiv, childNodes:[functionsButtonBarTemplate.cloneNode(true)]})
			}
	}
)
