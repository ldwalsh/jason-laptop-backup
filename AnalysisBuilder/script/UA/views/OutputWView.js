﻿///
///<reference path="../../$UA.js" />

$.class
(
	{
		namespace: UA.views,
		prototype: UA.views.WindowedViewBase,
	},

	function OutputWView()
	{
		this._elmntIDs = ["_txaOutput"]

		this._init =
			function()
			{
				controlGroup = $.ui.ControlsGroup.NEW(this._div)

				controlGroup.render()

				controlGroup.getControlByName("toolbar", $.ui.controls.ButtonBar).getButtonByName("clear").onButtonClick = this._txaOutput.setValue.getNoArgsCaller()

				//

				this._window = UA.views.DisplayWindow.NEW(this, this._div, "Output")

				this._window.resizeable   = true;
				this._window.onResizing	  = this._windowResizing;
				//this._window.onWindowInfo = this._windowResizing;
				
				//this._window.open(true)
			}

		this.writeOutput =
			function(output)
			{
				this._txaOutput.value += ("\n" + output)
			}

		this._windowResizing =
			function()
			{
				this._txaOutput.width  = this._window.windowDiv.clientWidth - 10;
				this._txaOutput.height = this._window.windowDiv.offsetHeight - 52;
			}
	}
)
