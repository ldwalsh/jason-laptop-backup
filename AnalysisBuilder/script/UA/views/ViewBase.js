///
///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.views,
		prototype: iCL.ui.ViewBase2,
		
		construct:
		{
			prototypeCstr:
			function()
			{
				if (this == UA.views.WindowedViewBase) return; //revise?
				if (this.noCSS) return;

				//alert(this.type.name)
				
				//HTMLLinkElement.addLESS("resource/less/views/" + this.type.name + ".less" + "?r=" + UA.BuilderEnviornment.version)
			},
		},
	},

	function ViewBase()
	{
		this.promptNotification =
			function(message, callback)
			{
				iCL.ui.windows.MessageBox.open(message, {parentNode:$gId("UAB"), callback:callback,})
			}

		this.promptError =
			function(message)
			{
				iCL.ui.windows.MessageBox.open(message, {parentNode:$gId("UAB")})
			}

		this.promptQuestion =
			function(message, arg1, options)
			{
				///<signature>
				///	<param name="message" type="String" />
				///	<param name="callback" type="Function" />
				///	<param name="options" type="Array" elementType="String" />
				///</signature>
				///<signature>
				///	<param name="message" type="String" />
				///	<param name="lookup" type="Function" />
				///	<param name="options" type="Array" elementType="String" />
				///</signature>				
				
				var o;
				
				if (options)
				{
					o = {buttons:[], parentNode:$gId("UAB")}

					var f =
						function()
						{
							if (arg1 instanceof Function)
							{
								Function.ifCall(arg1, [arguments[0].caption])
							}
							else if (arg1.constructor == Object)
							{
								Function.ifCall(arg1[arguments[0].caption])
							}
							else throw new Error(9327327329232)
						}

					options.forEach(function(option){return rU(o.buttons.push({caption:option, func:f,}))})
				}
				
				iCL.ui.windows.MessageBox.open(message, o)
			}
	}
)
