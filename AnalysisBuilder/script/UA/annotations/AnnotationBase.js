﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
	},

	function AnnotationBase()
	{
		this._text_ = String.VAR;
        
        this.text =
            {
                set:function(v){this._text_ = v.trim()},
                get:function( ){return this._text_}
            }
            
	}
)



Object.define
(
	{
		namespace: UA,
        prototype: UA.AnnotationBase,
	},

	function PointAnnotation(xValue, text)
	{
		this.xValue = Number.VAR;
	}
)
