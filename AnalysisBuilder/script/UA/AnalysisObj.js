﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		construct:
		{
			_serialize:
			function(instance, obj)
			{
				///<param name="instance" type="this" />
				///<param name="obj" type="Object" />

				delete obj._plots;
				delete obj._xPlotId;
				delete obj._dateRange;

				obj.plots = Constructor.serialize(instance._plots)
				obj.xPlotId = instance._xPlotId;
			},
			
			_deserialize:
			function(obj, instance)
			{
				///<param name="obj" type="Object" />
				///<param name="instance" type="this" />

				obj._plots   = JSON.parse(obj.plots)
				obj._xPlotId = obj.xPlotId;

				delete obj.plots;
				delete obj.xPlotId;

				obj._dateRange = ifFalsy(obj._dateRange, obj._plots.v[0]._dateRange)
			},
		},
	},

	function VisuObj(name, clientId, ownerId) //rename to Visu or Visulization
	{
		///<param name="name" type="String" />
		///<param name="ownerId" type="Number" />
		
		this._plots      = iCL.collections.List.VAR(UA.charting.PlotBase) //use AutoProp w/ privateSetter
		this._xPlotId    = Number.VAR; //chart specific!!!
		this._dateRange  = iCL.DateRange.VAR;
		this.id          = 0;
		this.name        = String.empty;
		this.description = String.empty;
		this.created     = Date.VAR;
		this.modified    = Date.VAR;
		this.clientId    = 0;
		this.ownerId     = 0;
		this.sharedRead  = false;
		this.sharedWrite = false;
		this.notes       = String.empty;
		this.viewMode    = 0;
		
		this.Cstr =
			function()
			{
				this._plots = iCL.collections.List.NEW(UA.charting.PlotBase)

				this.created  = new Date()
				this.modified = this.created.toCopy()

				this._dateRange = iCL.DateRange.NEW(new Date) //pass in dateRange?  ...(this._dateRange || (this._plots.count ? visu._plots.first().dateRange : iCL.DateRange.NEW(new Date)))
			}

this.hasPlots_get = //remove
	function()
	{
		return !!this._plots.count;
	}

		this.addPlot =
			function(plot)
			{
				///<param name="plot" type="UA.charting.PlotBase" />				
				
				this._plots.forEach(function(p){return _rU(p._order++)})

				var arr=[];this._plots.forEach(function(p){arr.push(p.id)});//TODO: replace with .select!
				
				this._plots.add(plot.SET({_plotId:((Math.max.apply(Math, arr)||0)+1), _order:1,}))
			}

		this.plots =
            {
                get:function(){return this._plots.toArray()}
            }			

		this.xPlot =
			{
				get:function( )
                {
                    if (!this._plots.count) return null;

                    return (this._plots.singleOrNull({id:this._xPlotId}) || this._plots.single({id:(this._xPlotId = this._plots.item(0).id)}))
                },
				set:function(v)
                    {
                        this._xPlotId = v.id;
                    }
			}

		this.isSaved =
			{
				get:function(){return !!this.id;}
			}

		this.dateRange =
			{
				get:function( ){return this._dateRange.toCopy()},
				set:function(v)
				{
					if (this._dateRange.equalTo(v)) return;

					this._dateRange = v.toCopy()

					this._plots.forEach(function(_){_.dateRange = v}.bind(this))
				}
			}
	}
)



