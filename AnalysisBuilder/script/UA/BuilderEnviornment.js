﻿///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		resources: ["resource/less/UAB.less",],
		statics:
		{
			_instance: null,
			version:  2.2,

			instance_:
			function()
			{
				//
				if (window.intellisense) return UA.BuilderEnviornment.VAR;
				//
				
				return this._instance;
			},

			createInstance:
			function(div, siteUser, loadingElement)
			{
				///<param name="div" type="HTMLDivElement" />
				///<param name="siteUser" type="UA.SiteUser" />
				///<param name="loadingElement" type="HTMLElement" />

				return (this._instance = this.NEW(div, siteUser, loadingElement))
			},
		},
	},

	function BuilderEnviornment(div, siteUser, loadingElement)
	{
		///<param name="div" type="HTMLDivElement" />
		///<param name="siteUser" type="UA.SiteUser" />
		///<param name="loadingElement" type="HTMLElement" />

		this._div                        = HTMLDivElement.VAR;
		this.siteUser                    = UA.SiteUser.VAR;
		this._loadingElement             = HTMLElement.VAR;
		this.presenterManager            = AutoProp(UA.PresenterManager)
		this.visuDisplayPresenterManager = AutoProp(UA.VisuDisplayPresenterManager)
		this.mbus                        = iCL.MessageBus.VAR;
		this.globalizer                  = Object.VAR;
        //this.globalizerIC                = Object.VAR;

        this._repository = UA.repository.Repository.VAR;

		this._equipment = {} //hack for equipment lookup

		this.Cstr =
			function()
			{
				this._div.addEventListener("contextmenu", function(ea){ea.preventDefault()})

                document.addEventListener
                (
                    "contextmenu",
                    function(ea)
                    {
                        if ((ea.target.tagName == "UL") || (ea.target && (ea.target.constructor == HTMLCanvasElement))) ea.preventDefault()
                    }
                )
                
                Function.invoke([document.disableBackspaceNavigation.bind(document)])
				document.disableBackspaceNavigation.bind(document)
				
				document.getElementsByTagName("form")[0].disablePostOnEnter() //revise/move

				this.mbus = iCL.MessageBus.NEW(this)

				UA.FunctionService .instance.getEngUnits().setOn = {success:UA.EngUnit.engUnits.pushArray,       failure:alert.bindArgs(["Engineering units collection could not be loaded."])}
				UA.EquipmentService.instance.getClasses ().setOn = {success:UA.EquipmentClass.classes.pushArray, failure:alert.bindArgs(["Equipment classes collection could not be loaded."])}
				
				var el = iCL.ExecutionList.NEW();el.add(this._verifyEquipmentClassesLoaded);el.add(this._getUserBuildings);
					el.execute(this._loadDefaultAnalysis)

				iCL.ui.WindowPinner.NEW(this._div)

                Function.invoke
                (
                    function f()
                    {
                        if (!window.Globalize || !window.Globalize._numberSymbol) return _rU(f.bind(this).invokeDelay(250))

                        //this.globalizerIC = new UA.Globalize.NEW()
                        this.globalizer   = new UA.Globalize.NEW(this.siteUser.culture)
                    
                    }.bind(this)
                )


                //

                var $repository=UA.repository, $strategy=$repository.strategy;

                this._repository = $repository.Repository.NEW([$strategy.CurrentDayStrategy.NEW(), $strategy.PastWeekStrategy.NEW(), $strategy.CurrentWeekStrategy.NEW()])
			}

		this._verifyEquipmentClassesLoaded =
			function(execItem)
			{
				///<param name="execItem" type="$.ExecutionList.ExecItem" />
				
				if (!UA.EquipmentClass.classes.length) return rU(this._verifyEquipmentClassesLoaded.applyAsync(this, arguments))

				execItem.CONTINUE;
			}

		this._getUserBuildings =
			function(execItem)
			{
				///<param name="execItem" type="$.ExecutionList.ExecItem" />

				UA.BuildingService.instance.getForCurrentClient().setOn =
				Function.bindAll
				(
					this,
					{
						success:function(buildings){this.siteUser.buildings = iCL.collections.Dictionary.fromArray(UA.Building, buildings, "buildingId");execItem.CONTINUE;}.bind(this), ////combine
						failure:alert.bindArgs(["Buildings could not be loaded."])
					}
				)
			}

		this._loadDefaultAnalysis =
			function()
			{
				this._div.toggleDisplay()
				
				//check querystring for what to load ...OR

				this._analysisLoaded(UA.VisuObj.NEW((UA.VisuDisplay.defaultVisuName + " 1"), this.siteUser.clientId, this.siteUser.id)) //revise to not pass in id here? //load via manager not here! MOVE!
			}

		this._analysisLoaded =
			function(visu)
			{
				HTMLElement.prototype.removeElement.applyIfExists(this._loadingElement)

				this.visuDisplayPresenterManager = UA.VisuDisplayPresenterManager.NEW(this)

				this.presenterManager = UA.PresenterManager.NEW(this).SET({showDisplays:Function.noArgs,}) //must be here because calls open? must be before sending message

				this.mbus.send(iCL.MessageBus.Message.NEW("ideLoaded"))
				this.mbus.send(iCL.MessageBus.Message.NEW(UA.bus.BMTypeEnum.VisuOpened, visu))

				window.triggerResize()
			}

        this.repository =
            {
                get:function(){return this._repository;}
            }
	}
)


Type.define
(
    {
        namespace: iCL,
    },

    function Request()
    {
        this._urls    = Array.VAR;
        this._r       = null;
        this._working = false;

        this._checkIsWorking =
            function()
            {
                if (this._working) throw new Error(4366456423)
            }

        this.add =
            function(url)
            {
                this._checkIsWorking()

                return this.SET({_urls:Array.wrapObject(url)})
            }

        this.get =
            function(callback, errCallback)
            {
                this._checkIsWorking()

                var el = iCL.ExecutionList.NEW()

                this._urls.forEach(function(_){el.add(function(ei){return _rU(this._doGet(_, ei))}.bind(this))}.bind(this))

                this._r = []

                el.execute
                (
                    function()
                    {
                        callback(this._r)

                        this._working = false;

                        return;
                    
                    }.bind(this)
                )
            }

        this._doGet =
            function(url, b, forceRefresh)
            {
                XMLHttpRequest.executeGet
                (
                    url,
                    function(xhr)
                    {
                        if (xhr.status == 200)
                        {
                            this._r.push(JSON.parse(xhr.responseText))
                            
                            b.CONTINUE;

                            return;
                        }

                        this._doGet.invokeDelay(100, [url, b, true])
                        
                    }.bind(this),
                    {forceRefresh:forceRefresh}
                )
            }
    }    
)


Type.define
(
    {
        namespace: UA,
        resources:
        [
            //{base:"/resource/dist/", all:["/resource/dist/cldr.js", {url:"/resource/dist/cldr/event.js", req:function(){return window.Cldr;}}]},
            //\_assets\scripts\cldr
            {url:"../_assets/scripts/cldr/cldr.js",},
            {url:"../_assets/scripts/cldr/cldr/event.js", req:function(){return window.Cldr;}},
            {url:"https://cdnjs.cloudflare.com/ajax/libs/globalize/1.0.0/globalize.min.js", req:function(){return window.Cldr;}},
            {url:"https://cdnjs.cloudflare.com/ajax/libs/globalize/1.0.0/globalize/number.min.js", req:function(){return window.Globalize;}},
            {url:"https://cdnjs.cloudflare.com/ajax/libs/globalize/1.0.0/globalize/currency.min.js", req:function(){return window.Globalize;}},
            {url:"https://cdnjs.cloudflare.com/ajax/libs/globalize/1.0.0/globalize/date.js", req:function(){return (window.Globalize && window.Globalize._numberSymbol);}},
        ],
        construct:
        {
           _cldrPath:    "/_assets/scripts/cldr/",
           _formatRegEx: {_rgx0:(/{(\w+:\w+|)}/g), _rgxX:(/{([0-9])\/(\w+:\w+|)}/)},
           _supplement:  {state:0, files:["likelySubtags.json", "timeData.json", "weekData.json"],},
            cldrPath:    {latin:{numbers:{decimal:"numbers/symbols-numberSystem-latn/decimal", group:"numbers/symbols-numberSystem-latn/group", list:"numbers/symbols-numberSystem-latn/list"}}},
            
            load:
            function(arr, base, callback)
            {
                if (!window.Globalize) return _rU(this.load.invokeDelay(1000, arguments))

                iCL.Request.NEW().add(arr.map(function(_){return base+_;})).get
                (
                    function(_)
                    {
                        _.forEach(function(__){Globalize.load(__)})// revise

                        callback()
                    }
                )
            }
        }
    },

    function Globalize(cultureCode)
    {
        this._cultureCode = String.VAR;
		this._jqg         = Object.VAR;

        this.Cstr =
            function()
            {
                this._cultureCode = (this._cultureCode || "en-US")

                var s = Globalize._supplement;

                switch (s.state) //revise this switch
                {
                    case 0:
                    {
                        s.state = 1;
                    
                        Globalize.load
                        (
                            s.files,
                            "/_assets/scripts/cldr/supplemental/",
                            function()
                            {
                                s.state = 2;
                            
                                this._l()
                        
                            }.bind(this)
                        )

                        break;
                    }
                    case 1:
                    {
                        Function.invoke
                        (
                            function f()
                            {
                                if (s.state == 2) return _rU(this._l())

                                setTimeout(f.bind(this), 1000)

                            }.bind(this)
                        )

                        break;
                    }
                    case 2:
                    {
                        this._l()

                        break;
                    }
                }
            }



        this._l =
            function()
            {
                Globalize.load(this._getG(), Globalize._cldrPath, this._ready)
            }

        this._ready =
            function()
            {
                this._jqg = new window.Globalize(this._cultureCode)
            }

        this._getG =
            function()
            {
                return ["main/{culture}/numbers.json", "main/{culture}/currencies.json", "main/{culture}/ca-gregorian.json",].map(function(_){return _.replace("{culture}", this._cultureCode)}.bind(this))
            }

		this.format =
			function(text, value)
			{
				var m;

				value = Array.wrapObject(value)

				while ((m = Globalize._formatRegEx._rgx0.exec(text))) //revise?
				{
					text = text.replace(m[0], ("{0/" + m[1] + "}"))
				}
								
				while ((m = Globalize._formatRegEx._rgxX.exec(text)))
				{
					var v = value[parseInt(m[1])]

					if (v == null) //todo: revise this unnecessary if block
					{
						text = text.replace(m[0], "")
					}
					else if (v instanceof Date)
					{
						text = text.replace(m[0], this._jqg.formatDate(v, Object.fromArray([m[2].split(":")])))
					}
					else if (isNum(v))
					{
						text = text.replace(m[0], this._jqg.formatNumber(v))
					}
					else throw new Error("")
				}

				return text;
			}

        this.cldr =
            {
                get:function(){return this._jqg.cldr;}
            }
    }
)




//function X()
//{
//    var s = "";

//    var x = {}
//    x.extends =
//    {
//        decimal_:function(){return "decimal";},
//        group_:function(){return "group";},
//    }

//    var t = {}
//    t.extends =
//    {
//        numbers_:
//        function()
//        {
//            return x;
//        }
//    }

//    var o = {}
//    o.extends =
//    {
//        latin_:
//        function()
//        {
//            return t;
//        }
//    }

//    return o;
//}


/*
document.dc =
function(element)
{
	if (!this._l)
	{
		this._l = []

		document.addEventListener
		(
			"contextmenu",
			function(ea)
			{
				this._l.forEach
				(
					function(_)
					{
						return ((_ == ea.target) ? _rT(ea.preventDefault()) : undefined)
					}
				)

			}.bind(this)
		)
	}

	this._l.push(element)
}
*/


Enum.define
(
	{
		namespace: UA,
	},

	function MainMsgBusEnum()
	{
		IDELoaded, VisuOpened
	}
)







Object.define
(
	{
		namespace: iCL,
	},

	function IoC(objs)
	{
		this._objs = Object.VAR;

		this.Cstr =
			function()
			{
				this._objs = (this._objs || {})
			}

		this.g =
			function()
			{
				return {}.SET({getData:jjj,}, true)
			}

		this.find =
			function(name)
			{
				return this._objs[name]()
			}

		this.register =
			function(name, obj)
			{
				this._objs[name] = obj;
			}
	}
)




function destroyLessCache(pathToCss) { // e.g. '/css/' or '/stylesheets/'

  if (!window.localStorage || !less || less.env !== 'development') {
    return;
  }
  var host = window.location.host;
  var protocol = window.location.protocol;
  var keyPrefix = protocol + '//' + host + pathToCss;
  
  for (var key in window.localStorage) {
    if (key.indexOf(keyPrefix) === 0) {
      delete window.localStorage[key];
    }
  }
}


