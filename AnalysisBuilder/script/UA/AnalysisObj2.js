﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"AnalysisObj2",
		statics:
		{
			userVariableDelimeter: "|||",
		},
	},

	function(name)
	{
		///<param name="name" type="String" />

		var $cstr = this.constructor;

		//

		this._mainSubroutineObjID			= Number.VAR;
		this._userVariables					= String.VAR;

		this.id								= Number.VAR;
		this.dateCreated					= Date  .VAR;
		this.name							= String.VAR;

		this._$_ID = {}

		this.definedEquipment				= new $.collection.List(UA.EquipmentAlias)
		
		this.subroutineObjs					= null; //new $.TArray(UA.SubroutineObj) //use ReadOnlyArray
		this.equipmentAssociationProfiles	= null; //new $.TArray(UA.EquipmentAssociationProfile)

		this.getSubroutineById =
			function(id)
			{
				return this.subroutineObjs.singleOrNull({"id":id})
			}

		this.mainSubroutineObj_get =
			function()
			{
				return this.getSubroutineById(this._mainSubroutineObjID)
			}

		this.userVariables_get =
			function()
			{
				var uVs = new $.TArray(UA.evaluation.UserVariableValue)

				if (!this._userVariables) return uVs;

				var x = this._userVariables.split($cstr.userVariableDelimeter)

				for (var i=0; i<x.length; i++)
				{
					uVs.push(UA.evaluation.UserVariableValue.fromString(x[i]))
				}

				return uVs;
			}

		this.userVariables_set =
			function(value)
			{
				this._userVariables = value;
			}

	
		$.class.instantiate2(this)
	}
)


