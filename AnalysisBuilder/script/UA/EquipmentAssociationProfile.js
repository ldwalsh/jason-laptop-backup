﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"EquipmentAssociationProfile",
		CAST:
		function(o)
		{
			///<returns type="UA.EquipmentAssociationProfile" />

			return o;
		}
	},

	function()
	{
		this.id						= Number.VAR;
		this.dateCreated			= Date  .VAR;
		this.dateModified			= Date  .VAR;
		this.name					= String.VAR;		
		this.equipmentAssociations	= UA.EquipmentAssociationCollection.VAR;

		this.Cstr =
			function()
			{
				this.equipmentAssociations = new UA.EquipmentAssociationCollection(this)
			}


		$.class.instantiate2(this)
	}
)

