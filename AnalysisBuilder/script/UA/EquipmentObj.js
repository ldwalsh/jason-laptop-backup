﻿///
///<reference path="../$UA.js"/>

Object.define
(
	{
		namespace: UA,
	},

	function EquipmentObj()
	{
		this.id         = Number.VAR;
		this.buildingId = Number.VAR;
		this.classId    = Number.VAR;
		this.name       = String.VAR;
	}
)