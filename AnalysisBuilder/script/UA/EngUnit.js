﻿///
///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		statics:
		{
			engUnits: null, //AutoProp(function(){return UA.EngUnit}),

			Cstr:
			function()
			{
				this.engUnits = new $.TArray(this)
			}
		}
	},

	function EngUnit(engUnitId, name, desc)
	{
		///<param name="engUnitId" type="Number" />
		///<param name="name" type="String" />
		///<param name="desc" type="String" />

		this.engUnitId = Number.VAR;
		this.name      = String.VAR;
		this.desc      = String.VAR;
	}
)
