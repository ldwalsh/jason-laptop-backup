﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting,
	},

	function PlotAggregator(plots)
	{
		///<param name="plots" type="Array" elementType="UA.charting.PlotBase" />
		
		this.plots = iCL.TArray.VAR(UA.charting.PlotBase)

		this.aggregate =
			function(callback)
			{
				///<param name="callback" type="Function" />

				//
				if (window.intellisense) return rU(Function.ifCall(callback)) //remove and use el.execute intellisense feature?
				//
				
				var el = iCL.ExecutionList.NEW()

				this.plots.forEach(function(p){el.add(this._aggregatePlotData.getArgsCaller([p, Function.prototype.getArgsCaller.placeHolder]))}.bind(this))

				el.execute(callback)
			}

		this._aggregatePlotData =
			function(plot, execItem)
			{
				///<param name="plot" type="UA.charting.PlotBase" />
				///<param name="execItem" type="$.ExecutionList.ExecItem" />

				plot.getData
				(
					function(error)
					{
						if (error) return rU(execItem.doBreak(error))

						execItem.CONTINUE;
					}
				)
			}
	}
)




//Math.roundTo =
//	function(value, place)
//	{
//		return (this.round(value * place) / place);
//	}

//function downSample(arr, keyIndex, valIndex)
//{
//	///<param name="arr" type="$.KeyedArray" elementType="Array" />
//	///<param name="keyIndex" type="Number" />
//	///<param name="valIndex" type="Number" />

//	keyIndex = ifNull(keyIndex, 0);
//	valIndex = ifNull(valIndex, 1);

//	var arrDel = [];

//	//for (var i=1; i<(arr.length-1); i++)
//	//{
//	//	var val = arr[i][valIndex];

//	//	if ((val == arr[i-1][valIndex]) && (val == arr[i+1][valIndex]))
//	//	{
//	//		arrDel.push(i);
//	//	}
//	//}

//	var currentVal = Math.roundTo(arr[0][valIndex], 100);

//	for (var i=1; i<(arr.length-1); i++)
//	{
//		var val = Math.roundTo(arr[i][valIndex], 100);

//		if (currentVal == val)
//		{
//			arrDel.push(i);

//			continue;
//		}
		
//		currentVal = val;
//	}

//	var offset = 0;

//	for (var i=0; i<arrDel.length; i++)
//	{
//		arr[arrDel[i]][1] = null;

//		//var index = (arrDel[i] - offset);

//		//var key = arr[index];

//		//arr.splice([arr[index][0]], index);

//		//offset++;
//	}
//}