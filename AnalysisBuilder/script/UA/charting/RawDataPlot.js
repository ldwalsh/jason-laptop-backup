﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting,
		prototype: UA.charting.PlotBase,
		construct:
		{
			_serialize:
			function(instance, obj)
			{
				delete obj._pointDateRange;
				delete obj._cachedRange;
				delete obj.equipmentName;
				delete obj.data; //?? //should be handled by base!!!
				
				delete obj._engUnitsLookup; //will be moved out
			},
			
			_deserialize:
			function(obj, instance)
			{
				///<param name="obj" type="Object" />
				///<param name="instance" type="this" optional="true" />

				if (!obj._dateRange) return;
				
				obj._engUnitsLookup = UA.EngUnit.engUnits; //UA.IoC.find("EngUnit").engUnits; //revise

				var dateRange = obj._dateRange;

				delete obj._dateRange;

                obj._annotations = (obj._annotations || iCL.collections.List.NEW(UA.PointAnnotation))

				return Constructor.deserialize(obj, this.NEW("__SD__").SET({point:UA.Point.NEW("__SD__"),})).SET({dateRange:Constructor.deserialize(dateRange.SET({__type:iCL.DateRange.type.fullName,}, true))})
			},
		},
	},
	
	function RawDataPlot(dateRange, point, buildingId, equipmentId, engUnitsLookup)
	{
		///<param name="dateRange" type="$.DateRange" />
		///<param name="point" type="UA.Point" />
		///<param name="equipmentName" type="String" />

		this._buildingId     = Number.VAR
		this._equipmentId    = Number.VAR;
		this._pointDateRange = UA.PointDateRange.VAR;
		this._cachedRange    = iCL.DateRange.VAR;
		this._engUnitsLookup = iCL.TArray.VAR(UA.EngUnit) //needs to be removed
		this.point           = UA.Point.VAR;
		this.equipmentName   = String.VAR;
		
		this.Cstr =
			function()
			{
				var r = {hours:0, minutes:0, seconds:0, milliseconds:0,} //revise
				
				this._dateRange = iCL.DateRange.NEW(this._dateRange.from.SET(r), this._dateRange.to.SET(r))

				this.SET({dateRange:this._dateRange, equipmentName:UA.EquipmentObj.CAST(UA.BuilderEnviornment.instance._equipment[this._equipmentId]).name,}) //revise and pass in equipment list
			}

		this.getData =
			{
				//get:function(){return iCL.IoC.NEW().g().getData;}
                
                get:function(){return jj.bind(this)}
			}

		this.displayName_get =
			function()
			{
				return (this.equipmentName + "." + this.point.name + " (" + this._engUnitsLookup.single({engUnitId:this.point.engUnitId}).name + ")")
			}

		this.displayNameShort_get =
			function()
			{
				return String.shorten(this.point.name.limit, 20)
			}

		this.dateRange_set =
			function(value)
			{
				///<param name="value" type="$.DateRange" />

				if (value.equalTo(this._dateRange) && this.data) return;

				RawDataPlot.prototype.dateRange_set.apply(this, arguments)

				this._pointDateRange = UA.PointDateRange.NEW(this.point, this._dateRange.from, this._dateRange.to)

				this.getData()
			}

		this.buildingId =
			{
				get:function(){return this._buildingId;}
			}
			
		this.equipmentId =
			{
				get:function(){return this._equipmentId;}
			}
	}
)


function jj(callback) //this needs to be revised and distributed among Repository objects and a new dedicated object
{
    if (this._cachedRange && this._cachedRange.equalTo(this.dateRange)) return _rU(Function.ifCall(callback))

    UA.BuilderEnviornment.instance.repository.retrieve
    (
        this.point,
        this._pointDateRange,

        function(raw)
        {
            var ka = iCL.KeyedArray.NEW()

			var currentDay = this.dateRange.from;

            raw = raw[this.point.id]
            
            var keys = Object.keys(raw)
						
			for (var i=0; i<keys.length; i++)
			{						
                var dateKey = keys[i]

				var d = raw[dateKey];

				var to     = currentDay.toCopy().addDays(1)							
				var sample = currentDay.toCopy()
						
				if (d.length)
				{
					var ii = 0;
														
					while (sample < to)
					{
						var value = null;
								
						if (ii < d.length)
						{
							var row = d[ii]

							if (sample.equalTo(row.logged))
							{
								value = parseFloat(row.value)

								ii++;
							}
                            else if (sample > row.logged)
                            {
                                ii++;

                                continue;
                            }
						}

                        ka.push(sample.format(UA.PointDataRepository.dateStoreFormat), [sample.toCopy(), value]) //use concrete PointData instead!

						sample.minutes += this.point.sampling;
					}
				}
				else
				{
					while (sample < to)
					{
						ka.push(sample.format(UA.PointDataRepository.dateStoreFormat), [sample.toCopy(), null]) //use concrete PointData instead!

						sample.minutes += this.point.sampling;
					}
				}

				currentDay.day++;
			}

			this._cachedRange = this.dateRange.toCopy()
            this.data         = ka;

            jj.apply(this, [callback])        
        
        }.bind(this)
    )
}
