﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting,
		prototype: UA.charting.PlotBase,
		statics:
		{		
			_serialize:
			function(instance, obj)
			{
				///<param name="instance" type="this" />
				///<param name="obj" type="Object" />
				
				//Object.attrib.remove(obj, ["__interval"])
				delete obj["data"]
				Object.attrib.append(obj, ["_interval"])

				obj._expressionObj = instance.expression.toString()
			},

			_deserialize:
			function(obj, instance)
			{
				///<param name="obj" type="Object" />
				///<param name="instance" type="this" />

				instance.expression = UA.expression.Expression.NEW(obj._expressionObj) //obj._expressionObj;
				instance.dateRange  = Constructor.deserialize(obj._dateRange.SET({__type:iCL.DateRange.type.fullName,}, true))

				delete obj._expressionObj;
				delete obj._dateRange;
			},
		},
	},

	function ExpressionPlot(dateRange, title)
	{
		///<param name="dateRange" type="iCL.DateRange" />
		///<param name="title" type="String" />

		this._expressionObj = UA.expression.Expression.VAR;
		this._callback      = Function.VAR;
		this._pointAliases  = iCL.collections.Dictionary.VAR(Number)
		this.__interval     = Number.VAR;

		this.Cstr =
			function()
			{
				this._yAxis        = 2;
				this._pointAliases = iCL.collections.Dictionary.NEW(Number)
			}

		this.getData =
			function(callback)
			{
				///<param name="callback" type="Function" />

				this._callback = callback;

				if (!this._pointAliases.count) return rU(this._aggregate())

				var pointDataRangeArray = []

				this._pointAliases.forEach
				(
                    function(_){return _rU(pointDataRangeArray.push(UA.PointDateRange.NEW(/*interface*/{id:_.value, sampling:this._interval,}/*interface*/, this.dateRange.from, this.dateRange.to)))}.bind(this)
				)
				
                UA.BuilderEnviornment.instance.repository.retrieve(pointDataRangeArray.map(function(_){return _.point;}), this.dateRange, this._aggregate) //revise retrieve to just take PointDateRange obj?
			}

		this._createStatement =
			function(lkup, interval)
			{
				var pointDataValues = {}

				var isDataMissing =
					this._pointAliases.FOR //use find here instead of .FOR
					(
						function(de)
						{                            
                            var sample = lkup[de.value + "." + interval.value]

                            if (!sample) return true;

                            pointDataValues[de.key] = sample.value;
						}
					)

				if (isDataMissing) return null;

				return this._expressionObj.evaluate(pointDataValues)
			}

		Array.prototype.lastOrDefault = function(def){return (this.length ? this.getLast() : def)}

		this._aggregate =
			function(timeSeriesDataArray)
			{
				///<param name="timeSeriesDataArray" type="Array" elementType="UA.PointDataRepository.TimeSeriesData" />

                var lkup = {}

                for (var id in timeSeriesDataArray)
                {
                    for (var d in timeSeriesDataArray[id])
                    {
                        timeSeriesDataArray[id][d].forEach(function(_){return _rU(lkup[id + "." + _.logged.value] = _)})
                    }
                }

				var data            = iCL.KeyedArray.NEW()
                var to              = this.dateRange.to;
                var currentInterval = this.dateRange.from;

                var last = 0;

                if (timeSeriesDataArray)
                {
                    Object.forEach(timeSeriesDataArray, function(o){return _rU(last = Math.min(last||Number.MAX_VALUE, o[Object.keys(o).getLast()].lastOrDefault({logged:0}).logged))})
                }

				to = ((to.equalTo(Date.today)) ? (last ? last : Date.current.addMinutes(-this._interval)) : to.toCopy().addDays(1))                    				

				while (currentInterval <= to)
				{
                    try //probably can remove this try/catch now but will leave in for further testing
					{
                    	var value = this._createStatement(lkup, currentInterval)

						if (Boolean.IS(value)) {value = (value * 1)} //may not need this conversion!?
					}
					catch (e)
					{
						iCL.ui.windows.MessageBox.open("The expression '" + this.displayName + "' failed to plot because the expression is invalid.") //revise to push message upto presenter

						data = null;

						break;
					}
					
					data.push(currentInterval.format(UA.PointDataRepository.dateStoreFormat), [currentInterval.toCopy(), value])

					currentInterval.minutes += this._interval;
				}

				this.data = data;

				Function.ifCall(this._callback)
			}

		this._interval =
			{
				get:function( ){return (this.__interval || 5)}, //revise!
				set:function(_){this.__interval = _;}
			}

		this.displayName_get =
			function()
			{
				return this.displayNameShort;
			}

		this.displayNameShort_get =
			function()
			{
				return this.title;
			}

		this.expression =
			{
				get:function( ){return this._expressionObj;},
				set:function(_){this._expressionObj = ((_ instanceof UA.expression.Expression) ? _ : UA.expression.Expression.NEW(_))} //revise!!!
			}

		this.pointAliases =
			{
				get:function( ){return this._pointAliases;},
				set:function(_){this._pointpointAliases = _;}
			}
	}
)


//Object.extends =
//{
//    find:
//    function(obj, func)
//    {
//		for (var a in obj) //replace with Object.keys so no need for hasOwnProperty
//		{
//			if (!obj.hasOwnProperty(a)) continue;

//			var r = func(obj[a], a)

//            if (r == undefined) continue;

//            return r;
//		}
//    }
//}


Object.extends =
{
    find:
    function(obj, func)
    {
        return Object.keys(obj).find(function(k, i){return func(obj[k], k, i)})
    },

    forEach:
    function(obj, func)
    {
        return Object.keys(obj).forEach(function(k, i){return _rU(func(obj[k], k, i))})
    },
}
