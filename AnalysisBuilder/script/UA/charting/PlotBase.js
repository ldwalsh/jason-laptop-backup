﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting,
		construct:
		{
			_colors:["#FFFFFF", "#000000"],

			generateDefaultColor:
			function()
			{
				var hex = "#";

				for (var i=0; i<3; i++)
				{
					var h = Number.getRandom(0, 15).toString(16).toUpperCase()
				
					hex += (h + h)
				}

				if (this._colors.contains(hex)) return this.generateDefaultColor.call(this)

				this._colors.push(hex)

				return hex;
			},

            _deserialize:
            function(obj, instance)
            {
                debugger;
            }
		},
	},

	function PlotBase() //cannot be instantiated
	{
		this._plotId	= Number.VAR;
		this._order		= -1;
		this._dateRange	= iCL.DateRange.VAR;		
		this._color		= String.VAR;
		this._xAxis		= 1;
		this._yAxis		= 1;
		this._yAxisSide	= String.VAR;
		this.title		= String.VAR;
		this.isVisible	= true;
		this.isFilled	= false;
		this.data		= iCL.KeyedArray.VAR(UA.PointData)
		this.alias		= String.VAR;
		
		this._annotations = iCL.collections.List.VAR(UA.PointAnnotation)

		this.Cstr =
			function()
			{
				this._annotations = iCL.collections.List.NEW(UA.PointAnnotation)
			}
		
		this.getData = //rename to getSeries???
			function(callback)
			{
				throw iCL.NotImplementedError.NEW()
			}

		this.displayName_get =
			function()
			{
				throw new Error()
			}

		this.displayNameShort_get =
			function()
			{
				throw new Error()
			}

		this.icon =
			{
				get:function(){return ((this instanceof UA.charting.ExpressionPlot) ? "E" : "P")}
			}

		//order responsibility will be moved to Visu... which will manage and update ordering
		this.order_get = function( ){return this._order;}
		this.order_set = function(v){this._order = v}

		this.id_get = //to keep or not to keep?
			function()
			{
				return this._plotId;
			}

		this.plotId_get =
			function()
			{
				return this._plotId;
			}
	
		this.dateRange_get = function( ){return this._dateRange.toCopy()},
		this.dateRange_set = function(v){this._dateRange = v.toCopy()}

		this.color =
			{
				get:function( ){return (this._color || (this._color = PlotBase.generateDefaultColor()))},
				set:function(v){this._color = v}
			}

		this.xAxis =
			{
				get:function( ){return this._xAxis;},
				set:function(v){this._xAxis = v}
			}

		this.yAxis =
			{
				get:function( ){return this._yAxis;},
				set:function(v){this._yAxis = v}
			}

		this.yAxisSide =
			{
                get:function( ){return (this._yAxisSide || (this._yAxisSide = PlotBase.YAxisSideEnum.Left))},
				set:function(v){this._yAxisSide = v}
			}
	}
)





Enum.define
(
    {
		namespace: UA.charting.PlotBase,
    },

    function YAxisSideEnum()
    {
        "Left", "Right"
    }
)

