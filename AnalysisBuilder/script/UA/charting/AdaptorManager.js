﻿///<reference path="../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting,
	},

	function AdaptorManager()
	{
		this._adaptors	= iCL.Lookup.VAR(o.ChartAdaptorBase) //$.TArray.VAR(o.ChartAdaptorBase)
		this._x			= iCL.Lookup.VAR(Number);

		this.Cstr =
			function()
			{
				this._adaptors = iCL.Lookup.NEW(o.ChartAdaptorBase) //new $.TArray(o.ChartAdaptorBase)
				this._x        = iCL.Lookup.NEW(Number)
			}

		this.add =
			function(adaptorBase, changeReference)
			{
				///<param name="adaptorBase" type="o.ChartAdaptorBase" />

				if (this.getAdaptor(adaptorBase.constructor, changeReference)) throw new Error()

				var n = adaptorBase.constructor.type.name;
				  
				this._adaptors.add(n, adaptorBase) //this._adaptors.push(adaptorBase)

				this._x.add(n, changeReference)
			}

		this.getAdaptor =
			function(cstr, changeReference)
			{
				///<param name="cstr" type="Constructor" />
				///<param name="changeReference" type="Number" />

				//
				if (window.intellisense) return cstr.VAR;
				//

				return (this._adaptors.item(cstr.type.name) || null)
			}

		//this.getAdaptorCstrByMode =
		//	function(mode)
		//	{
		//		var $adaptors = UA.charting.adaptors;

		//		switch (mode)
		//		{
		//			case UA.ViewModesEnum.Line:    return $adaptors.FlotChartAdaptor;
		//			case UA.ViewModesEnum.Scatter: return $adaptors.FlotScatterPlotAdaptor;
		//			case UA.ViewModesEnum.Table:   return $adaptors.TableAdaptor;
		//		}

		//		throw new iCL.InvalidOperationError()
		//	}

		this.clear =
			function()
			{
				this._adaptors.clear()
				this._x.clear()
			}

		this.hasAny =
			{
				get:function(){return !!this._adaptors.count;}
			}
	}
)