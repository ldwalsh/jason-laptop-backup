﻿///<reference path="~/AnalysisBuilder/script/$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotAdaptorBase,
	},

	function FlotChartAdaptor(chart) //rename to FlotLineChartAdaptor
	{
        this.__renderer = FlotChartAdaptor.Renderer.VAR;

		this._loadExtenders =
			function()
			{
				var $adaptors = UA.charting.adaptors;

                if (this._chart.forPrint) return [$adaptors.FlotContextMenuExtender.NEW(this, true)]

				return [
				
					$adaptors.FlotSelectionExtender.NEW(this), $adaptors.FlotMultiHoverExtender.NEW(this, this._multiHoverTipTextCreator, this._mutliHoverDateCreator), $adaptors.FlotHoverExtender.NEW(this, this._hoverTipTextCreator),
					$adaptors.FlotStepZoomExtender.NEW(this), $adaptors.FlotPanExtender.NEW(this), $adaptors.FlotDoubleClickExtender.NEW(this), $adaptors.FlotContextMenuExtender.NEW(this),
				] //future extender?: //http://www.codicode.com/art/jquery_flot_animator.aspx
			}

		this._createPlot =
			function(plot)
			{
				return {plotId:plot.plotId, plotName:plot.label, customUnit:plot.unit, data:plot.data, color:plot.color, xaxis:plot.xAxis, yaxis:plot.yAxis, lines:{fill:plot.filled, show:plot.visible},}
			}

		this._configFlotOptions =
			function(options)
			{
				options.SET({series:{lines:{show:true, lineWidth:1, shadow:0,},}, xaxis:{tickFormatter:this._formatDate, mode:"time",}, yaxis:{tickFormatter:this._formatValue,},})
			}

		this._formatValue =
			function(val, axis) //revise to increase performance
			{
				return this._globalizer.format("{}", val)
			}

		this._formatDate =
			function(val, axis) //revise to increase performance
			{
				return this._globalizer.format("{skeleton:Md}<br />{time:short}", new Date(val))
			}

		this._hoverTipTextCreator =
			function(item)
			{
				var series = item.series;
				
				var p = item.datapoint;

				var $f = this._globalizer.format; //revise
				return ($f((series.plotName + ": {datetime:short} <span style='font-weight:bold;'>"), new Date(p[0])) + $f(("{} (" + series.customUnit + ")</span>"), p[1]))
			}

		this._multiHoverTipTextCreator =
			function(series, dataPoint)
			{
				return this._globalizer.format((series.plotName + ": {} " + series.customUnit), dataPoint[UA.charting.adaptors.FlotAdaptorBase._valueIx])
			}

		this._mutliHoverDateCreator =
			function(dateStr)
			{
				return this._globalizer.format("{datetime:short}", dateStr)
			}

        this.gh =
            function(x)
            {
                return x;
            }

        this.ghh =
            function(x)
            {
                return x;
            }

        this._renderer_get =
            function()
            {
                return (this.__renderer||(this.__renderer = FlotChartAdaptor.Renderer.NEW(this)))
            }
	}
)





Object.define
(
    {
        namespace: UA.charting.adaptors.flot,
    },

    function FlotRendererBase(flotAdaptor)
    {
        this._flotAdaptor = Object.VAR;
        this._flot        = Object.VAR;

        this.Cstr =
            function()
            {
                this._flot = this._flotAdaptor.flot;
            }
    }
)





Object.define
(
    {
        namespace: UA.charting.adaptors.FlotChartAdaptor,
        prototype: UA.charting.adaptors.flot.FlotRendererBase,
    },

    function Renderer(flotAdaptor)
    {
        var PCE = UA.charting.PlotChangeEnum;

        this.render =
            function(plots, action)
            {
                if (!action) return _rU(this._flot.draw())

                var p = plots[0], fp = this._flot.getData().singleOrNull({plotId:p.plotId})

                if (!fp) return;
                
                if (this["_render_" + String.toCamelCase(action.toString())](fp, p) === false) return

                this._flot.draw()
            }

        this._render_visible =
            function(fp, p)
            {
                fp.lines.show = p.visible;
            }

        this._render_color =
            function(fp, p)
            {
                fp.color = p.color;
            }

        this._render_highlight =
            function(fp, p)
            {
				fp.lines.lineWidth = 2;
            }

        this._render_unhighlight =
            function(fp, p)
            {
                fp.lines.lineWidth = 1;
            }

        this._render_fill =
            function(fp, p)
            {
                this._flot.setData(this._flotAdaptor._plots.map(this._flotAdaptor._createPlot))

                this._flot.setupGrid()
            }

        this._render_yAxis =
            function(fp, p)
            {
                this._flot.setData(this._flotAdaptor._plots.map(this._flotAdaptor._createPlot))

                this._flot.setupGrid()
            }

        this._render_delete =
            function(fp, p)
            {
                this._flot.setData(this._flotAdaptor._plots.map(this._flotAdaptor._createPlot))

                this._flot.setupGrid()
            }

        this.supportedActions =
            {
                get:function(){return [PCE.Highlight, PCE.Unhighlight, PCE.Color, PCE.Visible, PCE.Fill, PCE.YAxis, PCE.Delete]}
            }
    }
)