﻿///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotExtenderBase,
	},

	function FlotContextMenuExtender(flotAdaptor, forPrint) //rename to FlotAnnotationCMenuExtender???
	{
        this._annotationManager = Object.VAR;
        this._forPrint          = Boolean.VAR;

		this._init =
			function()
			{
                FlotContextMenuExtender.ContextMenu.NEW(this._jqo[0], (this._annotationManager = UA.charting.AnnotationManager.NEW(this._flotAdaptor, this._draw)))

                this.enable()
			}

        this._draw =
            function(plots, action)
            {
                if (this._disabled) return;

                if (!action) this._annotationManager.sync()
                
                this._do_draw(plots, action)
            }

        this._do_draw =
            function(plots, action)
            {
                var PCE = UA.charting.PlotChangeEnum;

                var an = this._annotationManager.annotations;
                var fa = this._flotAdaptor;
                
                an.keys.filter(function(_){return !fa.plots.find(function(__){return (__.plotId == _)})}.bind(this)).forEach(function(_){Object.Tr(an).attrib.pop(_).forEach(function(__){__.remove()})}.bind(this)) //move out

                plots = ([PCE.YAxis, PCE.Fill].contains(action) ? fa.plots : (plots || fa.plots)) //not-coincidently these require a flot setupGrid                

                plots.forEach
                (
                    function(p)
                    {
                        if (!(an[p.plotId]||Array.empty).length) return; 

                        var flot   = this._flot;                       
                        var data   = flot.getData()
                        var offset = flot.getPlotOffset()
                        var axes   = flot.getAxes()
                        
                        an[p.plotId].forEach
                        (
                            function(a)
                            {
                                if (a.state == "removed") return _rU(a.remove()) //can remove 'a' from collection as well here?
                                        
                                if ((a.visible = (p.visible && !p.xPlot)) && (a.ranged = Math.isBound(fa.gh(a.annotation.xValue), axes.xaxis, true)))
                              //if ((a.visible = p.visible)               && (a.ranged = Math.isBound(fa.gh(a.annotation.xValue), axes.xaxis, true))) <--- use this when works
                                {
                                    var fp = data.single({plotId:p.plotId})
                                    var dp = p.data.find(function(_){return (_[0].valueOf() == a.annotation.xValue)}) //move this logic out!!!

                                    if (this._forPrint) return _rU(this._drawPrint(a.annotation.text, fp, dp, offset))

                                    a.position = {left:fp.xaxis.p2c(fa.gh(dp[0]))+offset.left+3, top:fp.yaxis.p2c(dp[1])+offset.top}
                                }

                                a.draw(plots, action)
                    
                            }.bind(this)
                        )

                    }.bind(this)
                )
            }

        this._drawPrint =
            function(text, fp, dp, offset)
            {
                this._flotAdaptor.getCanvas().getContext("2d").SET({font:"", fillText:[text, fp.xaxis.p2c(this._flotAdaptor.gh(dp[0]))+offset.left+3, fp.yaxis.p2c(dp[1])+offset.top]})
            }

		this.getStateInfo =
			function()
			{
				return {annotationsDisabled:this._disabled,}
			}

        this.receiveCommand = //virtual
            function(command)
            {
                var AE = UA.presenters.VisuDisplayManagerPresenter.ActionsEnum

                switch (command)
                {
                    case AE.AnnotationsOff: return _rU(this.disable())
                    case AE.AnnotationsOn:  return _rU(this. enable())
                }
            }

        this._suspended = false; //rename/revise

        this._interactionDetected =
            function(args)
            {
                if (this._disabled) return;

                Function.ifCall(SELECT(args.type, {RequestControl:this._disable, ReleaseControl:this._do_draw,}))
            }

		this._enable =
			function()
			{
                if (!this._disabled) return;

				this._do_draw()
			}

		this._disable =
			function()
			{
                this._annotationManager.annotations.forEach2(function(_){_.forEach(function(__){__.hide()})}) //replace with hiding the whole annotation layer? (when pointer-events browser supported)
			}
	}
)




Object.define
(
    {
        namespace: UA.charting,
    },

    function AnnotationManager(flotAdaptor, drawCallback)
    {
        var AnnotationInfo = UA.charting.adaptors.FlotContextMenuExtender.AnnotationInfo;
        
        this._flotAdaptor  = Function.VAR;
        this._drawCallback = Function.VAR;
        this._lastItm      = Object.VAR;

        this._aaa = iCL.Lookup.VAR(Object)

        this.Cstr =
            function()
            {
                this._aaa = iCL.Lookup.NEW(Object)
            }

        this.sync =
            function()
            {
                var container;
                
                this._visuPlots.forEach
                (
                    function(p)
                    {
                        var arr   = this._aaa[p.id] = (this._aaa[p.id] || [])                        
                        var anno_ = p._annotations;

                        if (!anno_ || !anno_.count) return; //revise... .annotations should be always be set?

                        anno_.forEach(function(_){return arr.singleOrNull({annotation:_}) ? undefined : _rU(arr.push(AnnotationInfo.NEW((container||(container=this._flotAdaptor.getPlaceholder())), _, p.id)))}.bind(this))

                        arr.forEach(function(_){return anno_.contains(_.annotation) ? undefined : _rU(_.state = "removed")})

                    }.bind(this)
                )
            }

        this.isAddMode =
            function()
            {
                var o = this._lastItm = this._flotAdaptor.a1.item;

                if (!o) return false;

                var an = this._aaa[o.series.plotId]
                
                if (an) return !an.singleOrNull(function(_){return (!_.state && (_.annotation.xValue == this._flotAdaptor.gh(o.datapoint[0])))}.bind(this))

                return true;
            }

        this.showMenu =
            function()
            {
                return !!this._flotAdaptor.a1.item;
            }

        this.addAnnotation =
            function()
            {
				var t = this._promptName()

				if (!t) return;

                var plot = this._visuPlots.single(this._lastItm.series.plotId) //revise

                exec(this._aaa[plot.id]=this._aaa[plot.id]||[]).push
                    (
                        AnnotationInfo.NEW(this._flotAdaptor.getPlaceholder(), plot._annotations.addAndReturn(UA.PointAnnotation.NEW(this._flotAdaptor.ghh(this._lastItm.datapoint[0]), t)), plot.id)
                    )
                
                this._drawCallback([this._flotAdaptor.plots.single({plotId:plot.id})])
            }

        this._getAFromI =
            function()
            {
                var o=this._lastItm, gh=this._flotAdaptor.gh;

                return this._aaa[o.series.plotId].single(function(_){return (_.annotation.xValue == gh(o.datapoint[0]))})
            }

        this._promptName =
            function(text, title)
            {
                title = (title || "Please enter annotation text.")

                var t = (prompt(title, text||"") || "")

                return ((t.length > 150) ? this._promptName(t, "Please enter annotation text under 150 characters.") : t)
            }

        this.removeAnnotation =
            function(annotationInfo)
            {
                annotationInfo = (annotationInfo || this._getAFromI())
                
                this._visuPlots.single(annotationInfo.plotId)._annotations.remove(annotationInfo.annotation)

                annotationInfo.state = "removed";

                this._drawCallback([this._flotAdaptor.plots.single({plotId:annotationInfo.plotId})])                   
            }

        this.editAnnotation =
            function(annotationInfo)
            {
                annotationInfo = (annotationInfo || this._getAFromI())

                var t = this._promptName(annotationInfo.annotation.text)

                if (!t) return;

                annotationInfo.SET({annotation:{text:t,}, draw:[],})
            }

        this._visuPlots =
            {
                get:function(){return UA.BuilderEnviornment.instance.visuDisplayPresenterManager.activePresenter.visuDisplay.visu.plots}
            }

        this.annotations =
            {
                get:function(){return this._aaa;}
            }
    }
)


Object.define
(
    {
        namespace: UA.charting.adaptors.FlotContextMenuExtender,
        resources: ["resource/html/views/VisuDisplayWView/AnnotationContextMenu.html", "resource/less/views/VisuDisplayWView/AnnotationContextMenu.less"],
    },

    function ContextMenu(container, annotationManager)
    {
        this._container         = HTMLElement.VAR;
        this._annotationManager = UA.charting.AnnotationManager.VAR;

        this.Cstr =
            function()
            {
                this._cm = iCL.ui.controls.ContextMenu.NEW(this.constructor.type.definition.resources.html.clone(), [this._container]).SET({clicked:this._cm_selected, showing:this._cm_showing,})
            }

        this._disableItems =
            function(items)
            {
                var IDN = iCL.ui.controls.ContextMenu.ItemDattributeNames;

                Array.fromAlike(this._cm._ul.children).filter(function(_){return (_.getData(IDN.Type) != "header")}).forEach(function(_){_.data(IDN.Disabled).value = items.contains(_.getData(IDN.Command))})
            }

		this._cm_showing =
			function(target, ei)
			{
                if (!target.annotationInfo && !this._annotationManager.showMenu()) return _rU(ei.cancel());

                this._disableItems(this._annotationManager.isAddMode() ? ["edit", "delete"] : ["add"])
			}

		this._cm_selected =
			function(ea)
			{
                var AOE = ContextMenu.ns.AnnotationOperationEnum;
                
                switch (AOE[String.toPascalCase(ea.command)])
                {
                    case AOE.Add:    return _rU(this._annotationManager.addAnnotation())
                    case AOE.Delete: return _rU(this._annotationManager.removeAnnotation(ea.element.annotationInfo))
                    case AOE.Edit:   return _rU(this._annotationManager.editAnnotation(ea.element.annotationInfo))
                }
            }
    }
)



//function getEmSize(el){return Number(getComputedStyle(el, "").fontSize.match(/(\d+)px/)[1]);}



Object.define
(
    {
        namespace: UA.charting.adaptors.FlotContextMenuExtender,
        resources: ["resource/html/views/VisuDisplayWView/Annotation.html"]
    },

    function AnnotationInfo(container, annotation, plotId)
    {
        var PCE = UA.charting.PlotChangeEnum;
        
        this._container = Object.VAR;
        this.annotation = Object.VAR;
        this.plotId     = Number.VAR;

        this._inner     = HTMLDivElement.VAR;
        this._outer     = HTMLDivElement.VAR;

        this._visible   = true;
        this._ranged    = true;

        this.Cstr =
            function()
            {
                this._outer = AnnotationInfo.type.definition.resources.html.cloneNode(true).SET({parentNode:this._container,})
                this._inner = this._outer.children[0].SET({annotationInfo:this,}, true)
                
                this._inner.addEventListener("mouseenter", this._el_mouseenter) //combine above line
                this._inner.addEventListener("mouseleave", this._el_mouseleave) //combine above line
            }

        this._getD =
            function()
            {
                return String.shorten(this.annotation.text, 25)
            }

        this._el_mouseenter =
            function()
            {
                this._outer.classList.add("hover")

                this._text = this.annotation.text;
            }

        this._el_mouseleave =
            function()
            {
                this._outer.classList.remove("hover")

                this.draw()
            }

        this.draw =
            function(plots, action)
            {
                switch (action)
                {
                    case PCE.Highlight:   return _rU(this._outer.classList.add   ("hover"))
                    case PCE.Unhighlight: return _rU(this._outer.classList.remove("hover"))
                }
                
                var b = (this._ranged && this._visible)

                if (b) this._text = this._getD()

                this._outer.style.display = (b ? "" : "none")
            }

        this.remove =
            function()
            {
                if (!this._outer.parentNode) return;

                this._outer.removeElement()
            }

        this.hide =
            function()
            {
                this._outer.style.display = "none";
            }

        this._text =
            {
                set:function(v){this._inner.textContent = v;}
            }

        this.position =
            {
                set:function(v){this._outer.style.SET({left:v.left-4, top:v.top-4,})}
            }

        this.visible =
            {
                set:function(v){this._visible=v;},
                get:function( ){return this._visible;}
            }

        this.ranged =
            {
                set:function(v){this._ranged=v;},
                get:function( ){return this._ranged;}
            }

        this.element =
            {
                get:function( ){return this._outer;}
            }
    }
)



Enum.define
(
	{
		namespace: UA.charting.adaptors.FlotContextMenuExtender,
	},

	function AnnotationOperationEnum()
	{
		Add, Edit, Delete
	}
)




//var x = this._flot.p2c(this._lastPos)

//var ctx = this._flot.getCanvas().getContext("2d");

//ctx.font="10pt Georgia";
//ctx.fillText(t, x.left, x.top);

//ctx.font="30px Verdana";
//// Create gradient
//var gradient=ctx.createLinearGradient(0,0,c.width,0);
//gradient.addColorStop("0","magenta");
//gradient.addColorStop("0.5","blue");
//gradient.addColorStop("1.0","red");
//// Fill with gradient
//ctx.fillStyle=gradient;
//ctx.fillText("Big smile!",10,90); 




		//ctx.beginPath();

		//ctx.moveTo(x.left, x.top);

		//ctx.lineTo(150,150);

		//ctx.stroke();
