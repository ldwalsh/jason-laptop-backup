﻿///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.ChartAdaptorBase,
		construct:
        {_stampIx:0, _valueIx:1,},
	},

	function FlotAdaptorBase()
	{
		this._flot         = Object.VAR;		
		this._configFlotOptions = new Function("options", "")		        
		this.a1        = UA.charting.adaptors.flot.auxilary.A1.VAR;
		this._renderer = UA.charting.adaptors.flot.ChartRendererBase.VAR;

		this.Cstr =
			function()
			{
				this.a1 = UA.charting.adaptors.flot.auxilary.A1.NEW(this)
                                
                //this._extenders.forEach(function(_){_.SET({interaction:this.aaaa, enable:[],})}.bind(this))  //.interaction = this.aaaa;}.bind(this)); //revise... move to base!!!

                this._extenders.forEach(function(_){_.SET({interaction:this.aaaa,})}.bind(this))  //.interaction = this.aaaa;}.bind(this)); //revise... move to base!!!
			}

        this._init =
            function()
            {
                this._flot = jQuery.plot(this._div, Array.empty, this._configFlotOptionsInternal({canvas:true, grid:{hoverable:true, clickable:true,},}))
            }

		this.aaaa = //rename and move to ChartAdaptorBase
			function(args)
			{
				this._extenders.forEach
                (
                    function(_)
                    {
                        //if (["RequestControl", "ReleaseControl"].contains(args.type) && _._disabled) return;

                        _._interactionDetected(args)
                    }                
                )

                this.interaction.invoke(args) //this.interaction.raise(args)
			}

		this._configFlotOptionsInternal =
			function(options)
			{
				this._configFlotOptions(options)

				this._extenders.forEach(function(_){_._configFlotOptions(options)}) //revise

				return options;
			}

		this._binding =
			function(flotPlots)
			{               
                this._flot.setData(flotPlots)

                this._flot.setupGrid() //needed always? prob not
			}

        this._drawing =
            function(plots, action)
            {
                if (action)
                {
                    if (action == UA.charting.PlotChangeEnum.Delete) //move all if logic to base!!
                    {
                        this._plots.splice(this._plots.findIndex(function(_){return (_.plotId == plots[0].plotId)}), 1)
                    }
                    else
                    {
                        plots.forEach(function(_){this.plots.splice(this._plots.findIndex(function(__){return (_.plotId == __.plotId)}), 1, _)}.bind(this))
                    }
                }

                this._renderer.render(plots, action)
            }

        this.clear = //should be handled by renderer?
            function()
            {
                this._flot.unhighlight()
            }

		this.getCanvas = //may be able to make private
			function(type)
			{
                var CTE = UA.charting.CanvasTypeEnum;

			    return Function.invoke(SELECT(type||CTE.Main, [[CTE.Main, function(){return this._flot.getCanvas()}], [CTE.Overlay, function(){return this._div.querySelector(".flot-overlay")}]]).bind(this))
			}

        this.getPlugin =
            function(name)
            {
                return jQuery.plot.plugins.find(function(_){return (_.name == name)}).instances.find(function(_){return (_.instance == this._flot)}.bind(this))
            }

this.getPlaceholder =
    function()
    {
        return this._flot.getPlaceholder()[0]
    }

        this._renderer_get =
            function()
            {
                throw iCL.NotImplementedError.NEW()
            }

		this.container = //move to base
			{
				get:function(){return this._div;}
			}

this.hoverable =
	{
		set:function(v){this._flot.getOptions().grid.hoverable = v;}
	}

this.flot =
    {
        get:function(){return this._flot;}
    }


	}
)








Enum.define
(
	{
		namespace: UA.charting,
	},

	function CanvasTypeEnum()
	{
		Main, Overlay
	}
)





Object.define
(
	{
		namespace: UA.charting.adaptors.flot.auxilary,
	},

	function A1(flotAdaptor) //to be renamed
	{
		this._flotAdaptor = Object.VAR;

		this.position = Object.VAR;
		this.item     = Object.VAR;

		this.Cstr =
			function()
			{
				this._flotAdaptor.flot.getPlaceholder().bind("plothover", this._plotHover)
			}

		this._plotHover =
			function(event, position, item)
			{
				this.item     = item; //((!item && this.item && (document.elementFromPoint(position.pageX, position.pageY) != this._canvas)) ? this.item : item) //revise, use parallel mousemove event??? worried about performance
				this.position = position;
			}
	}
)





Object.define
(
    {
        namespace: UA.charting.adaptors.flot,
    },

    function ChartRendererBase(flotAdaptor)
    {
        this.render =
            function()
            {
            }
    }
)






Array.filterPredicates =
{
    notNull:            function(_){return (_ !=  null)},
    notNullStrict:      function(_){return (_ !== null)},

    notUndefined:       function(_){return (_ !=  undefined)},
    notUndefinedStrict: function(_){return (_ !== undefined)},
}






Object.define
(
	{
		namespace: UA.charting.adaptors,
	},

	function ActionArgs(cstr, type, data) //rename to InteractionArgs
	{
		this.cstr = Constructor.VAR;
		this.type = String.VAR;
		this.data = Object.VAR;
	}
)


Enum.define
(
	{
		namespace: UA.charting,
	},

	function PlotChangeEnum()
	{
		None, Highlight, Unhighlight, IsXAxis, Color, Visible, Fill, YAxis, Delete, Range
	}
)
