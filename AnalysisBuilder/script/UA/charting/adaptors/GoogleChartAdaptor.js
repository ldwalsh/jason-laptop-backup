﻿///
///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.ChartAdaptorBase,
	},

	function GoogleChartAdaptor(containerDiv, data)
	{
		///<param name="containerDiv" type="HTMLDivElement" />
		///<param name="data" type="Array" elementType="$.KeyedArray" />

		this.Cstr =
			function()
			{
				var d = this._data;

				var arr = d[0]

				for (var i=1; i<data.length; i++)
				{
					series = data[i]

					for (var key in arr.keys)
					{
						var row = arr.keys[key]

						var foundRow = series.keys[key]

						row.push(foundRow ? foundRow[1] : null)
					}
				}

				this._data = arr;
			}
		
		this._load =
			function(colorArray)
			{
				///<param name="colorArray" type="Array" elementType="String" />

				var chart = new google.visualization.LineChart(this._containerDiv)

				var containerHeight = this._containerDiv.offsetHeight;
				var containerWidth  = this._containerDiv.offsetWidth;
				
				chart.draw
				(
					google.visualization.arrayToDataTable(this._data),
					{
						height: (containerHeight - 3),
						width:  (containerWidth - 3),
						colors: colorArray,
						chartArea:
						{
							height:	(containerHeight - 80),
							width:	(containerWidth - 80),
						},
						fontSize:"10",
						legend:{position:"none",},

						hAxis:
						{
							slantedText:false,
							//slantedTextAngle:90,
						}
					}
				)

			}
	}
)
