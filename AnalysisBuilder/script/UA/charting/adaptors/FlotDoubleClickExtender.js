﻿///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotExtenderBase,
	},

	function FlotDoubleClickExtender(flotAdaptor)
	{
		this._pos = Object.VAR;

        this._init =
            function()
            {
                this.enable()
            }

		this._enable =
			function()
			{
				this._bindFlot(["plotclick", "dblclick"])
			}

		this._disable =
			function()
			{
				this._unbindFlot(["plotclick", "dblclick"])
			}

		this._flot_plotclick =
			function(ea, pos)
			{
				this._pos = pos;
			}

		this._flot_dblclick =
			function()
			{
				var xa = this._flotAdaptor.flot.getAxes().xaxis;

				var d = ((xa.max - xa.min) *.2)

				var c = {min:xa.min+d, max:xa.max-d}
				
				this.interaction(UA.charting.adaptors.ActionArgs.NEW(this.constructor, "selected", {xMin:c.min, xMax:c.max}))
			}
	}
)