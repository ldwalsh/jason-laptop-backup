﻿///																													  ///
///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotExtenderBase,
	},

	function FlotHoverExtender(flotAdaptor, tipTextCreator)
	{
		this._tipTextCreator = String.VAR;
		this._tooltipDiv     = HTMLDivElement.VAR;

        this._init =
            function()
            {
                this.enable()
            }

		this._enable =
			function()
			{
				this._bindFlot(["plothover"])
			}

		this._disable =
			function()
			{
				this._unbindFlot(["plothover"])
			}

		this.clear =
			function()
			{
				this._flot.unhighlight() //possible duplicate should be in a dedicated _plotHover?

				this._tooltipDiv = rU(HTMLElement.removeElement(this._tooltipDiv)) //use rN?
			}

		this._flot_plothover =
			function(event, pos, item)
			{
				this.clear()

				if (!item) return;

				this._tooltipDiv = this._createTooltip(this._tipTextCreator.bindArgs([item]), item.pageX, item.pageY, 20)

				this._flot.highlight(item.series, item.datapoint)
			}

		this._createTooltip = //duplcate... move to base
			function(getTextFunc, x, y, cursorOffset)
			{
				cursorOffset = (cursorOffset || 20)

				var tooltip = HTMLElement.create(HTMLDivElement, {innerHTML:getTextFunc(), parentNode:document.body, style:{zIndex:iCL.ui.LayerManager.layerGroups.appMenu.max,},})

				tooltip.classList.add("FlotTooltip") //revise			

				var offset = tooltip.offsetWidth;

				tooltip.style.SET({top:((y)+"px"), left:((((innerWidth - x) >= (offset + cursorOffset)) ? ((x + cursorOffset)) : ((x - offset - cursorOffset)))  + "px"),})

				return tooltip;
			}
	}
)