﻿///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotExtenderBase,
	},

	function FlotStepZoomExtender(flotAdaptor)
	{
		this._zoomHistory = Array.VAR
		this._currentZoom = Number.VAR

		this.Cstr =
			function()
			{
				this._zoomHistory = [{xMin:null, xMax:null}]
				this._currentZoom = 0;
			}

        this._init =
            function()
            {
                this.enable()
            }

		this._interactionDetected =
			function(args)			
			{
                if (args.type != "selected") return;
				
				this._zoom(+1, args.data)
			}

		this.getStateInfo =
			function()
			{
				return {drillIn:(this._zoomHistory.length-1 > this._currentZoom), drillOut:(this._currentZoom > 0),}
			}

        this.receiveCommand =
            function(command)
            {
                var AE = UA.presenters.VisuDisplayManagerPresenter.ActionsEnum

				switch (command)
				{
					case AE.DrillOut: return _rU(this._zoom(-1))
					case AE.DrillIn:  return _rU(this._zoom(+1))
				}
            }

		this._zoom =
			function(level, range)
			{
				var t = function(_){return ((_ < 0) ? 0 : ((_ > this._zoomHistory.length-1 && !range) ? this._zoomHistory.length-1 : _))}.bind(this)(this._currentZoom + level)

				if (range)
                {
					this._zoomHistory.splice(t, (this._zoomHistory.length-t), range)
				}

				if (this._currentZoom == t) return;

                var z = this._zoomHistory[this._currentZoom = t]

                this._flot.getXAxes().forEach(function(_){_.options.SET({min:z.xMin, max:z.xMax})}.bind(this))
                
                this._flot.setupGrid(); //should be handled by .draw in the future                
                this._flotAdaptor.draw()

                this.interaction(UA.charting.adaptors.ActionArgs.NEW(this.constructor, "zoomed", {zoomLevel:this._currentZoom}))
			}

		//this.zoom =
		//	function(level)
		//	{
		//		this._zoom(level)
		//	}
	}
)
