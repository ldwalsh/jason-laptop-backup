﻿///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.ChartAdaptorBase,
	},

	function TableAdaptor(chart)
	{
		this._tableControl = iCL.ui.controls.Table.VAR;
        this._plots        = null;

		this.Cstr =
			function()
			{
				this._tableControl = this._chart.container.controlInstance;
			}

        this._init =
            function()
            {
            }

		this._draw =
            function()
            {
            }

        this._createPlot =
            function(plot)
            {
                return plot;
            }
        
        this._binding =
			function(plots, callback)
			{
				///<param name="plots" type="Array" elementType="UA.charting.PlotBase" />
				///<param name="callback" type="Function" />

				this._tableControl.clear()
				
                this._plots = plots;

				plots = plots.reverse().search({visible:true}) //use .filter

                var interval = 0;
                
                plots.forEach
				(
					function(p)
					{
						if (!p.data.length) return;

						var ival = ((p.data.item(1)[0] - p.data.item(0)[0]) / 60000) //use concrete type here
						if (interval && (ival >= interval)) return;
						interval = ival;
					}
				)

				//
				//create header row

				var row = this._tableControl.appendRow(true)
				
				row.appendCell("Interval")

				plots.forEach
				(
					function(p)									
					{
						var s;

						//switch (p.constructor)
						//{
						//	case o.ChartLinePlot:
						//	{
						//		var p = o.RawDataPlot.CAST(p).point;

						//		s = (p.name + " (" + UA.EngUnit.engUnits.single({engUnitId:p.engUnitId}).name + ")")

						//		break;
						//	}
						//	case o.ExpressionPlot:
						//	{
						//		s = o.ExpressionPlot.CAST(p).displayName;

						//		break;
						//	}
						//}

                        s = p.label;

						row.appendCell(s).SET({style:{borderBottomColor:p.color,},})
					}
				)

				if (!interval) return rU(Function.ifCall(callback))

				//

				var bucketName      = "theBucket";
				var currentInterval = Math.min.invoke(plots.map(function(_){return _.data[0][0]})).toCopy()
				var dateTo          = Math.max.invoke(plots.map(function(_){return _.data.getLast()[0]})).toCopy().addDays(1)

				$f = this._globalizer.format;
				
				while (currentInterval < dateTo)
				{
					var row = this._tableControl.addRow(bucketName)

					row.appendCell($f("{datetime:short}", currentInterval))

					plots.forEach(function(_){row.appendCell(function(pd){return (pd && pd[1] != null ? $f("{}", pd[1]) : "")}(_.data.keys[currentInterval.format(UA.PointDataRepository.dateStoreFormat)]))});

					currentInterval.minutes += interval;
				}

				this._tableControl.addRowDone(bucketName)

				Function.ifCall(callback)
			}

        this.draw =
            function(plots, action)
            {
                return;

                if (plots.length > 1) return; //because _bind does all the work so need to revise
                
                var PCE = UA.charting.PlotChangeEnum

                var p = this._plots.single({plotId:plots[0].plotId})

                //var p = this._plots.single(plots[0].meta.plotId)

                switch (action)
                {
                    case PCE.Color:
                    {
                        p.color = plots[0].color

                        return _rU(this._binding(this._plots))
                    }
                    
                    case PCE.Visible:
                    {
                        p.viisble = plots[0].visible;

                        return _rU(this._binding(this._plots))
                    }
                }
            }

		this.tableControl =
			{
				get:function(){return this._tableControl;}
			}
	}
)
