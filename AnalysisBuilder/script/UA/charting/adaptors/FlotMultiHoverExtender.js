﻿///<reference path="~/AnalysisBuilder/script/$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotHoverExtender,
        resources: [{url:"/3rdParty/flot/jquery.flot.crosshair.js",}],
	},

	function FlotMultiHoverExtender(flotAdaptor, tipTextCreator, dateTitleCreator)
	{
		var FlotAdaptorBase = UA.charting.adaptors.FlotAdaptorBase;

		this._dateTitleCreator = Function.VAR;

		this._divOutput = HTMLDivElement.VAR;

		this._init =
			function()
			{
				this._divOutput = HTMLElement.create(HTMLDivElement, {parentNode:this._flotAdaptor.container, data:{name:"PointValues",},})

                this._bindFlot("plothover")
			}

		this.clear =
			function()
			{
				this._flot.unhighlight()

				this._divOutput.toggleDisplay(false)
			}

		this._configFlotOptions =
			function(options)
			{
				options.SET({crosshair:{mode:"x",},})
			}

		this._flot_plothover =
			function(event, pos, item)
			{
				this.clear()

				if (item) return;

				this._divOutput.style.display = "block";
				
				this._divOutput.clearHTML()

				this._autoSelectPoints.invokeDeferred(10, [pos])
			}

		this._autoSelectPoints =
			function(pos)
			{
				//this._autoSelectPoints._busy = true; //to be implemented

				var axes = this._flot.getAxes();
								
				if ((pos.x < axes.xaxis.min) || (pos.x > axes.xaxis.max) || (pos.y < axes.yaxis.min) || (pos.y > axes.yaxis.max)) return; //revise and use inRange
			
				var dataset = this._flot.getData();

				var first = (dataset[0] || {data:{length:null}}).data

				if (!first.length) return;

				var ix;

				for (ix=0; ix<first.length; ++ix) // Find the nearest points, x-wise
				{
					if (first[ix][FlotAdaptorBase._stampIx] > pos.x) break;
				}

				var date;
				
				if (!first[ix] || !(date = first[ix][FlotAdaptorBase._stampIx])) return;

				HTMLElement.create(HTMLDivElement, {parentNode:this._divOutput, textContent:this._dateTitleCreator(date), data:{id:"divDate",},})
			
				for (var i=0; i< dataset.length; ++i)
				{
					var series = dataset[i];

					if (!series.lines.show) continue;

					var dp = series.data[ix];

					if ((dp == null) || (dp[FlotAdaptorBase._valueIx] == null)) continue;
					
					HTMLElement.create(HTMLDivElement, {parentNode:this._divOutput, textContent:this._tipTextCreator(series, dp),})
				}

				//this._autoSelectPoints._busy = false; //to be implemented
			}
	}
)
