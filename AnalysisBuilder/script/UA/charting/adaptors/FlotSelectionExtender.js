﻿///<reference path="~/AnalysisBuilder/script/$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotExtenderBase,
        resources: [{url:"/3rdParty/flot/jquery.flot.selection.js",}],
		
		construct: {_selectingEvents:["plotselecting", "plotunselected", "plotselected"],}
	},

	function FlotSelectionExtender(flotAdaptor)
	{
		this._inSelectionMode = false;

        this._init =
            function()
            {
                this.enable()
            }
        
        this._enable =
			function()
			{
				this._bindFlot(FlotSelectionExtender._selectingEvents)

                if (!this._disabled) return;

                this._flotAdaptor.getPlugin("selection").options.disabled = false;
                this._flotAdaptor.getPlugin("crosshair").options.disabled = false;
			}

		this._disable =
			function()
			{
				this._unbindFlot(FlotSelectionExtender._selectingEvents)

                this._flotAdaptor.getPlugin("selection").options.disabled = true;
                this._flotAdaptor.getPlugin("crosshair").options.disabled = true;
			}

		this._flot_plotselecting =
			function(event, ranges)
			{
				this._flot.unhighlight() //revise and call disable on other extenders

				this._inSelectionMode = true;
				//this._flotAdaptor.hoverable = false;///////////
			}

		//this._flot_plotunselected =
		//	function(event, ranges)
		//	{
		//		Function.invokeAsync(function(){return _rU(this._flotAdaptor.hoverable = !(this._inSelectionMode = false))}.bind(this)) //revise?
		//	}

		this._flot_plotselected =
			function(event, ranges)
			{
				this._flot.clearSelection()
				
				if (!ranges.xaxis) return;
				
				//this._flot_plotunselected()

				var from = new Date(ranges.xaxis.from)
				var to   = new Date(ranges.xaxis.to  )

				var fromIx, toIx;

				var d = this._flot.getData()[0].data;				
				//var d = this._plots[0].data; //REVISE... first element not reliable

				for (var i=0; i<d.length; i++)
				{
					if (fromIx)
					{
						if (to > d[i][0]) continue;

						toIx = i;

						break;
					}
						
					if (from > d[i][0]) continue;
						
					fromIx = i;
				}

				var xa = ranges.xaxis;

				this.interaction(UA.charting.adaptors.ActionArgs.NEW(this.constructor, "selected", {xMin:xa.from, xMax:xa.to}))
			}

        this._interactionDetected =
            function(args)
            {
                if (args.type == "zoomed" && !args.data.zoomLevel)
                {
                    if (this._disabled)
                    {
                        this.enable()
                    }

                    return;
                }
            }

        this.receiveCommand = //virtual
            function(command)
            {
                var ActionsEnum = UA.presenters.VisuDisplayManagerPresenter.ActionsEnum

                if (![ActionsEnum.PanOn, ActionsEnum.PanOff].contains(command)) return;

                //var op = this._flot.getOptions()
                
                //clearSelection also!!!

                Function.ifCall(SELECT(command.toString(), {PanOn:this.disable, PanOff:this.enable,}))//this.disable()
            }

		this._configFlotOptions =
			function(options)
			{
				options.SET({selection:{mode:"x",},})
			}
	}
)
