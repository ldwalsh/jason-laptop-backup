﻿///<reference path="~/AnalysisBuilder/script/$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
	},

	function FlotExtenderBase(flotAdaptor)
	{
		this._disabled    = false;
        this._flotAdaptor = Object.VAR;
		this._flot        = Object.VAR;
		this._jqo         = HTMLDivElement.VAR;
		this.interaction  = Function.VAR; //DELEGATE()

        this._isInit = false;

		this.init =
			function()
			{
				this._flot = this._flotAdaptor.flot;
				this._jqo  = this._flot.getPlaceholder()
				
				this._init()

                this._isInit = true;
			}

		this._init =
			function() //override
			{}

        this.draw =
            function(plots, action)
            {
                this._draw(plots, action)
            }

        this._draw =
			function() //override
			{}

		this._bindFlot =
			function(events)
			{
				Array.wrapObject(events).forEach(function(e){return _rU(this._jqo.bind(e, (this["_flot_" + e])))}.bind(this))
			}

		this._unbindFlot =
			function(events)
			{
				Array.wrapObject(events).forEach(function(e){return _rU(this._jqo.unbind(e, (this["_flot_" + e])))}.bind(this))
			}

		this._interactionDetected =
			function(actionArgs) //override
			{}

		this.clear =
			function() //overridden
			{}

		this._enable =
			function() //overriden
			{}

		this.enable =
			function()
			{
                if (!this._disabled && this._isInit) debugger;

                this._enable()

                this._disabled = false;
			}

		this.disable =
			function()
			{
				this._disable()

                this._disabled = true;
			}

		//this._flot_loaded = new Function()

		this._configFlotOptions =
			function(options)
			{
			}

		this.getStateInfo =
			function()
			{}

        this.receiveCommand = //virtual
            function(command)
            {
            }

		//this._flot =
		//	{
		//		get:function(){return this._flotAdaptor.flot;}
		//	}

		//this._jqo =
		//	{
		//		get:function(){return this._flot.getPlaceholder()}
		//	}
	}
)
