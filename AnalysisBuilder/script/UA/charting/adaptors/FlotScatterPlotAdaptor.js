﻿///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotAdaptorBase,
	},

	function FlotScatterPlotAdaptor(chart)
	{
		///<param name="div" type="HTMLDivElement" />

		this._xPlot     = Object.VAR;
        this._xPlotData = Array.VAR;

		this._loadExtenders =
			function()
			{
				var $a = UA.charting.adaptors;

				return [$a.FlotSelectionExtender.NEW(this), $a.FlotHoverExtender.NEW(this, this._hoverTipTextCreator), $a.FlotStepZoomExtender.NEW(this), $a.FlotPanExtender.NEW(this), $a.FlotContextMenuExtender.NEW(this),]

				//future extender?: //http://www.codicode.com/art/jquery_flot_animator.aspx
			}

		this._configFlotOptions =
			function(options)
			{
				//options.SET({series:{points:{show:true, radius:2,},}, xaxis:{tickFormatter:this._formatValue,}, yaxis:{tickFormatter:this._formatValue,},})
                options.SET({series:{points:{show:true,},}, xaxis:{tickFormatter:this._formatValue,}, yaxis:{tickFormatter:this._formatValue,},})
			}

		this._formatValue =
			function(val, axis) //revise to increase performance
			{
				return this._globalizer.format("{}", val)
			}

        this._creatingPlots =
            function(plots)
            {
                var p = this._xPlot = plots.singleOrNull({xPlot:true})

                if (!p) return;

                this._xPlotData = Object.fromArray(p.data) //could be a performance gain if fromArray can be avoided
            }

		this._createPlot =
			function(plot)
			{
                if (plot == this._xPlot) return _rN(plot.visible = false)

				var d   = this._xPlotData;
                var arr = plot.data.map(function(_){return ((!_[0] || (d[_[0]] == null)) ? undefined : [d[_[0]], _[1]])}).filter(function(_){return !!_})

				return {data:arr, lines:{show:false}, points:{show:plot.visible, lineWidth:1, radius:2, fill:false,}, xaxis:plot.xAxis, yaxis:plot.yAxis, color:plot.color, customUnit:plot.unit, plotId:plot.plotId,}
			}

		this._hoverTipTextCreator =
			function(item)
			{
				var format = this._globalizer.format;
				var dp     = item.datapoint;

				return (format(("{} (" + this._xPlot.unit + ") "), dp[0]) + format(("{} (" + item.series.customUnit + ")"), dp[1]))
			}

        this.gh =
            function(x)
            {
                return this._xPlotData[new Date(x)]
            }

        this.ghh =
            function(x)
            {
                return this._xPlot.data.find(function(_){return (_[1] == x)})[0].valueOf() // move valueOf to prop setter?
            }

        this._renderer_get =
            function()
            {
                return (this.__renderer || (this.__renderer = FlotScatterPlotAdaptor.Renderer.NEW(this)))
            }
	}
)



Object.define
(
    {
        namespace: UA.charting.adaptors.FlotScatterPlotAdaptor,
        prototype: UA.charting.adaptors.flot.FlotRendererBase,
    },

    function Renderer(flotAdaptor)
    {
        var PCE = UA.charting.PlotChangeEnum;

        this.render =
            function(plots, action)
            {
                if (!action) return _rU(this._flot.draw())                
                
                if (plots.length == 1)
                {
                    var p = plots[0]

                    var fp = this._flot.getData().singleOrNull({plotId:p.plotId})

                    if (!fp) return;

                    if (this["_render_" + String.toCamelCase(action.toString())](fp, p) === false) return;
                }
                else
                {
                    if (this["_render_" + String.toCamelCase(action.toString())]() === false) return
                }

                this._flot.draw()
            }

        this._render_delete =
            function(fp, p)
            {
                this._flot.setData(this._flotAdaptor._plots.map(this._flotAdaptor._createPlot).filter(function(_){return !!_}))

                this._flot.setupGrid()
            }

        this._render_isXAxis =
            function(fp, p)
            {
                this._flotAdaptor.bind(this._flotAdaptor._plots, PCE.IsXAxis) //use .rebind? //revise!!!

                //this._flotAdaptor.drawExtenders(this._flotAdaptor._plots, PCE.IsXAxis)
            }

        this._render_visible =
            function(fp, p)
            {
                if (p.xPlot) return false;

                fp.points.show = p.visible;
            }

        this._render_color =
            function(fp, p)
            {
                if (p.xPlot) return false;

                fp.color = p.color;
            }

        this._render_highlight =
            function(fp, p)
            {
                if (p.xPlot) return false;

				fp.points.radius = 3;
            }

        this._render_unhighlight =
            function(fp, p)
            {
                if (p.xPlot) return false;

                fp.points.radius = 2;
            }

        this._render_yAxis =
            function(fp, p)
            {
                this._flot.setData(this._flotAdaptor._plots.map(this._flotAdaptor._createPlot).filter(function(_){return !!_}))

                this._flot.setupGrid()
            }

        this.supportedActions =
            {
                get:function(){return [PCE.Highlight, PCE.Unhighlight, PCE.Color, PCE.Visible, PCE.Fill, PCE.YAxis]}
            }
    }
)