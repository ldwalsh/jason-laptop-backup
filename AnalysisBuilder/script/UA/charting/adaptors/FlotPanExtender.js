﻿///<reference path="~/AnalysisBuilder/script/$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
		prototype: UA.charting.adaptors.FlotExtenderBase,
        resources: [{url:"/3rdParty/flot/jquery.flot.navigate.js"}]
	},

	function FlotPanExtender(flotAdaptor)
	{
		this._init =
			function()
			{
                this._flotAdaptor.bound.attach(this._adaptor_bound) //move to init?

                //this._flotAdaptor.getPlugin("navigate").detach() //move to init?

                this.disable()
			}

        this._adaptor_bound =
            function()
            {
                this._flot.getXAxes()[0].options.panRange = [this._flot.getXAxes()[0].min, this._flot.getXAxes()[0].max]

                this._flot.getYAxes().forEach(function(_){_.options.panRange = [_.min, _.max]})
            }

        this._draw =
            function(plots, action)
            {
                if (action != UA.charting.PlotChangeEnum.YAxis) return;

                this._adaptor_bound()
            }

		this._configFlotOptions =
			function(options)
			{
				options.SET({pan:{interactive:true,},})
			}

		this.getStateInfo =
			function()
			{
				return {panningDisabled:this._disabled,}
			}
        this.receiveCommand = //virtual
            function(command)
            {
                var ActionsEnum = UA.presenters.VisuDisplayManagerPresenter.ActionsEnum

                if (![ActionsEnum.PanOn, ActionsEnum.PanOff].contains(command)) return;

                this[(ActionsEnum.PanOn == command) ? "enable" : "disable"]()
            }

		this._interactionDetected =
			function(args)			
			{
				if (args.type != "zoomed") return;
				
				if (args.data.zoomLevel == 0)
                {
                    this.disable()
                }
			}

        this._flot_plotpan =
            function()
            {
                if (!this._isPanning)
                {
                    this._isPanning = true;
                    
                    this.interaction(UA.charting.adaptors.ActionArgs.NEW(this.constructor, "RequestControl"))
                }
                
                this._ddd.invokeDeferred(100)
            }

        this._isPanning = false;

        this._ddd =
            function()
            {
                this._isPanning = false;

                this.interaction(UA.charting.adaptors.ActionArgs.NEW(this.constructor, "ReleaseControl"))
            }

        this._enable =
            function()
            {
                this._flotAdaptor.getPlugin("navigate").attach()

                this._bindFlot("plotpan")
            }

        this._disable =
            function()
            {
                this._flotAdaptor.getPlugin("navigate").detach()

                this._unbindFlot("plotpan")
            }
	}
)
