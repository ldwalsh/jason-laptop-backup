﻿///<reference path="../../../$UA.js" />

Object.define
(
	{
		namespace: UA.charting.adaptors,
	},

	function ChartAdaptorBase(chart) //rename to AdaptorBase
	{
		this._chart      = Object.VAR;
        this._div        = HTMLDivElement.VAR;
        this._plots      = Array.VAR;
        this._extenders  = Array.VAR;
		this._isLoaded   = false;
		this.interaction = Function.VAR;
		this._globalizer = iCL.Globalizer.VAR;

        this.creatingPlot  = iCL.Event;

        this.bound = iCL.Event;

		this.Cstr =
			function()
			{
                this._div = this._chart.container;

                this._globalizer = UA.BuilderEnviornment.instance.globalizer;

                this._extenders = (this._loadExtenders() || Array.empty)
                
                this._init()
                
                this._extenders.forEach(function(_){_.init()});
			}

        this._creatingPlots = new Function()

		this._binding    = function(){throw iCL.NotImplementedError.NEW()}
        this._drawing    = function(){throw iCL.NotImplementedError.NEW()}
        this._createPlot = function(){throw iCL.NotImplementedError.NEW()}

		this._loadExtenders =
			function() //abstract/virtual ???
			{}
		
		this.bind =
			function(plots)
			{
				this._isLoaded = false;

                this._creatingPlots(this._plots = plots)
				
                this._binding(this._plots.map(function(p, ix){return (this.creatingPlot.raise(p, ix).cancelled ? null : this._createPlot(p))}.bind(this)).filter(Array.filterPredicates.notNull))

                this.bound.raise()

                this.draw(plots)				
			}

        this.draw =
            function(plots, action)
            {
                Function.invoke([this._drawing, this.drawExtenders], [plots||this._plots, action])

                this._isLoaded = true;
            }

        this.drawExtenders =
            function(plots, action)
            {
                this._extenders.forEach(function(_){_.draw(plots, action)})
            }

        this.dispatchCommand =
            function(command)
            {
                this._extenders.forEach(function(_){_.receiveCommand(command)})
            }

		this.getStateInfo =
			function()
			{
				var state = {}

				this._extenders.forEach(function(_){state.SET(_.getStateInfo())})

				return state;
			}

		this.isLoaded =
			{
				get:function(){return this._isLoaded;}
			}

        this.plots =
            {
                get:function(){return this._plots;}
            }			
	}
)
