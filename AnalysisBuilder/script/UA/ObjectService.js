﻿///
///<reference path="../$UA.js" />

$.class
(
	{
		namespace:	UA,
		name:		"ObjectService",
		statics:
		{
			instance_:
			function()
			{
				this.__instance = this.__instance || new this(document.baseUrl.endWith("/") + document.base.getData("modulePath") + "Service/AnaylsisService.asmx")

				return this.__instance;
			},
		},
	},

	function(serviceEndpoint)
	{
		this._ss = new $.asp.ScriptService(serviceEndpoint)

		//this._ss.defaultNotifier = new $.DD(document).toggle;

		this._ss.defineMethod("Update", ["objType", "objID", "objProps"])

		this.update =
			function(obj, props)
			{
				if (props instanceof Array)
				{
					props = Object.extract(obj, props)
				}

				props = Object.translate(props, String.toPascalCase)

				return this._ss.methods.Update.invoke($.getCstrFullName(obj.constructor), obj.id, props)
			}


		$.class.instantiate2(this)
	}
)
