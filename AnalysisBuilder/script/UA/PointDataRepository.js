﻿///
///<reference path="../$UA.js" />

Object.define
(
	{
		namespace: UA,
		statics:
		{
			getInstance:
			function()
			{
				this._instance = (this._instance || this.NEW(UA.PointService.instance))

				return this._instance;
			},
		
			dateStoreFormat:  "m/d/yyyy.HH:MM",
			//pointCountForDay: 287,

			_createUniqueKey:
			function(pointId, date)
			{
				///<param name="pointId" type="Number" />
				///<param name="date" type="Date" />

				return (pointId + "|" + date.format("m/d/yyyy"))
			}
		},
	},

	function PointDataRepository(pointService)
	{
		///<param name="pointService" type="UA.PointService" optional="true"/>

		this._pointService = UA.PointService.VAR;
		this._repository   = $.collections.Dictionary.VAR($.KeyedArray)
		this._lastPointLkp = $.Lookup.VAR;

		this.Cstr =
			function()
			{
				this._repository   = $.collections.Dictionary.NEW(Object) //$.collections.Dictionary.NEW($.KeyedArray)
				this._lastPointLkp = $.Lookup.NEW(Date)
				
				//this._pointService = (this._pointService || UA.PointService.instance) //use IoC here?
			}

		this.getPointData =
			function(pointDateRangeArray)
			{
				///<signature>
				///	<param name="pointDateRange" type="UA.PointDateRange" />
				///</signature>
				///<signature>
				///	<param name="pointDateRangeArray" type="Array" elementType="UA.PointDateRange" />
				///</signature>
				
				var el			   = $.ExecutionList.NEW()
				var pointDataArray = $.KeyedArray   .NEW() //new $.TArray(UA.PointData)

				pointDateRangeArray = Array.wrapObject(pointDateRangeArray)

				for (var i=0; i<pointDateRangeArray.length; i++)
				{
					var pointDateRange = UA.PointDateRange.CAST(pointDateRangeArray[i])

					var currentDate = pointDateRange.from;

					while (currentDate <= pointDateRange.to)
					{
						var key		= PointDataRepository._createUniqueKey(pointDateRange.point.id, currentDate)
						var args	= [pointDataArray, pointDateRange.point, currentDate.toCopy(), Function.prototype.bindArgs.placeHolder]
						var isToday = currentDate.equalTo(Date.today)

						var retrieveFromFunc = Function.VAR;

						if (isToday)
						{
							if (this._repository.hasKey(key))
							{
								var lastGet = Date.CAST(this._lastPointLkp[pointDateRange.point.id]).toCopy()

								if (lastGet.addMinutes(5) > Date.current)
								{
									retrieveFromFunc = this._retrieveFromRepository;
								}
								else
								{
									this._repository.set(key, null)

									this._lastPointLkp[pointDateRange.point.id] = Date.current;

									retrieveFromFunc = this._retrieveFromService
								}
							}
							else
							{
								this._repository.add(key, null);

								this._lastPointLkp[pointDateRange.point.id] = Date.current;

								retrieveFromFunc = this._retrieveFromService;
							}							
						}
						else
						{
							if (this._repository.hasKey(key))
							{
								retrieveFromFunc = this._retrieveFromRepository;
							}
							else
							{
								this._repository.add(key, null)

								retrieveFromFunc = this._retrieveFromService;
							}
						}

						el.add(retrieveFromFunc.bindArgs(args))

						currentDate.day += 1;
					}
				}

				var ch = $.CallbackHandler.NEW()
				
				el.execute(function(error){return rU(ch.callback(error||pointDataArray))})
				
				return ch;
			}

		this._retrieveFromRepository =
			function(pointDataArray, point, date, execItem)
			{
				///<param name="pointDataArray" type="$.KeyedArray" elementType="UA.PointData" />
				///<param name="key" type="String" />
				///<param name="execItem" type="$.ExecutionList.ExecItem" />
				
				var key = PointDataRepository._createUniqueKey(point.id, date)
				
				var data = this._repository.item(key)

				if (!data) return _rU(this._waitFor(key, this._retrieveFromRepository.bindArgs(arguments)))

				if (data instanceof Error) return _rU(execItem.doBreak(data))

				pointDataArray.push(key, data)

				execItem.CONTINUE;
			}

		this._retrieveFromService =
			function(pointDataArray, point, date, execItem, bypassCache)
			{
				///<param name="pointDataArray" type="$.KeyedArray" elementType="UA.PointData" />
				///<param name="point" type="UA.Point" />
				///<param name="date" type="Date" />
				///<param name="execItem" type="$.ExecutionList.ExecItem" optional="true" />
				///<param name="bypassCache" type="Boolean" value="true" />
				
				var args = arguments;

				bypassCache = (date.equalTo(Date.today) ? true : bypassCache)

				this._pointService.getData(point.id, date, date.tommorow).SET({bypassCache:(bypassCache||false),}).setOn =
					Function.bindAll
					(
						this,
						{
							success:function(data)
							{
								///<param name="data" type="Array" elementType="UA.PointData" />
								
								var key = PointDataRepository._createUniqueKey(point.id, date)
						
								data.sort(Array.createSortFunction("logged", Date.parse))

								if (!bypassCache && !data.length) return rU(this._retrieveFromService.apply(this, Array.fromArrayLike(args).concat([true])))
												
								var keyedArray = $.KeyedArray.NEW(UA.PointData)

								data.forEach
								(
									function(pointData)
									{
										var interval = new Date(pointData.logged)

										interval.roundToMinute(point.sampling)

										var intervalString = interval.format(PointDataRepository.dateStoreFormat);
								
										if (keyedArray.keys[intervalString]) return;

										keyedArray.push(intervalString, pointData.SET({logged:interval}))
									}
								)

								//

								var o = {pointId:point.id, date:date, data:keyedArray}
						
								//if (this._repository.hasKey(key)) this._repository.set(key, o) //required if because today is never stored... refine to store but reload for latest interval
								this._repository.set(key, o)

								pointDataArray.push(key, o)

								execItem.CONTINUE;
							},

							failure:function(error)
							{
								//this._repository.set(PointDataRepository._createUniqueKey(point.id, date), null)

								execItem.doBreak(error)
							}
						}
					)
			}

		this._waitFor =
			function(key, callback)
			{
				if (this._repository.item(key)) return rU(callback())

				this._waitFor.applyAsync(this, arguments)
			}
	}
)





//setTimeout
//(

//function()
//{

//return;


//$strategy = UA.repository.strategy;

//var r = UA.repository.Repository.NEW([$strategy.CurrentDayStrategy.NEW(), $strategy.PastWeekStrategy.NEW(), $strategy.CurrentWeekStrategy.NEW()])


//var P = UA.Point;



//r.retrieve
//(
//    [P.NEW().SET({id:152, sampling:5}),],
//    iCL.DateRange.NEW(Date.today.addDays(-2), Date.today),
//    function(r)
//    {
//debugger;

//    }
//)


//return;


//r.retrieve
//(
//    [P.NEW().SET({id:152, sampling:5}), P.NEW().SET({id:420, sampling:5}), P.NEW().SET({id:421, sampling:5}),],
//    iCL.DateRange.NEW(Date.today.addDays(-12), Date.today),
//    function(r)
//    {
//debugger;

//    }
//)


//r.retrieve
//(
//    [P.NEW().SET({id:152, sampling:5}), P.NEW().SET({id:420, sampling:5}), P.NEW().SET({id:421, sampling:5}),],
//    iCL.DateRange.NEW(Date.today.addDays(-20), Date.today.addDays(-16)),
//    function(r)
//    {
//debugger;

//    }
//)


//r.retrieve
//(
//    [P.NEW().SET({id:152, sampling:5}),],
//    iCL.DateRange.NEW(Date.today.addDays(-30), Date.today.addDays(-22)),
//    function(r)
//    {
//debugger;

//    }
//)

//r.retrieve
//(
//    [152, 420, 421], //[182,183],
//    iCL.DateRange.NEW(Date.yesterday, Date.yesterday),
//    function(r)
//    {
//debugger;

//    }
//)

//r.retrieve
//(
//    [183],
//    iCL.DateRange.NEW(Date.today.addDays(-12), Date.today),
//    function(r)
//    {
//debugger;

//    }
//)



/*


[pointId, [date, [interval]]

or

[pointId, [date, requestId]]




*/



//},

//6000
//)