﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;

using CW.Data;
using CW.Business;
using CW.Utility;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Common.Constants;

namespace CW.Website
{
    public partial class BureauReports : SitePage
    {
        #region Properties
        #endregion

        #region Page Events

            private void Page_Init()
            {
                //logged in security check in master page.

                //secondary security check specific to this page.                
                if (!(siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Diagnostics)))
                {
                    Response.Redirect("/Home.aspx");
                }
            }

            private void Page_FirstLoad()
            {
                //datamanger in sitepage

                BindThemes(ddlTheme);

                //BindClients(ddlClient);

                BindBuildings(lbBuildings, siteUser.CID);
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                }
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Binds a dropdown list with all clients
            /// </summary>
            /// <param name="ddl"></param>
            //private void BindClients(DropDownList ddl)
            //{
            //    ddl.DataTextField = "ClientName";
            //    ddl.DataValueField = "CID";
            //    ddl.DataSource = mDataManager.ClientDataMapper.GetAllClients(); 
            //    ddl.DataBind();
            //}

            private void BindBuildings(ListBox lb, int clientID)
            {
                //clear just in case
                lbBuildings.Items.Clear();

                lbBuildings.DataSource = siteUser.VisibleBuildings;
                lbBuildings.DataTextField = "BuildingName";
                lbBuildings.DataValueField = "BID";
                lbBuildings.DataBind();
            }

            private void BindThemes(DropDownList ddl)
            {
                ddl.Items.Clear();

                Dictionary<int, string> themes = Common.Constants.BusinessConstants.Theme.ReportThemes;

                if (!siteUser.IsKGSFullAdminOrHigher && !siteUser.IsSchneiderTheme)
                    themes = themes.Where(t => t.Key != 1).ToDictionary(t => t.Key, t => t.Value);
                else if (!siteUser.IsKGSFullAdminOrHigher && siteUser.IsSchneiderTheme)
                    themes = themes.Where(t => t.Key != 0).ToDictionary(t => t.Key, t => t.Value);

                ddl.DataValueField = "Key";
                ddl.DataTextField = "Value";
                ddl.DataSource = themes;
                ddl.DataBind();
            }

        #endregion

        #region Dropdown Events

            //protected void ddlClient_OnSelectedIndexChanged(object sender, EventArgs e)
            //{
            //    BindBuildings(lbBuildings, Convert.ToInt32(ddlClient.SelectedValue));
            //}

        #endregion

        #region Button Events

            /// <summary>
            /// Update home content Button on click.
            /// </summary>
            //protected void generateButton_Click(object sender, EventArgs e)
            //{
                //List<int> bidList = new List<int>();

                //foreach (ListItem building in lbBuildings.Items)
                //{
                //    if (!building.Selected) continue;

                //    bidList.Add(Convert.ToInt32(building.Value));
                //}

                //if (bidList.Any())
                //{
                //    //check if buildings are all of same culture
                //    if (mDataManager.BuildingDataMapper.AreBuildingSettingsCulturesTheSame(bidList))
                //    {
                //        //Introduction
                //        string introduction = Server.HtmlEncode(editorIntroduction.Content);

                //        //Recommended Actions
                //        string recommendedActions = Server.HtmlEncode(editorRecommendedActions.Content);

                //        //Energy Trend
                //        string energyTrend = Server.HtmlEncode(editorEnergyTrend.Content);

                //        //Maintenance Trend
                //        string maintenanceTrend = Server.HtmlEncode(editorMaintenanceTrend.Content);

                //        //Comfort Trend
                //        string comfortTrend = Server.HtmlEncode(editorComfortTrend.Content);

                //        //KGSAdminHomePageHeader   
                //        // = String.IsNullOrEmpty(txtKGSAdminHomePageHeader.Value) ? null : txtKGSAdminHomePageHeader.Value;

                //        lblError.Visible = false;
                //    }
                //    else
                //    {
                //        lblError.Text = "Selected buildings must be of the same culture.";
                //        lblError.Visible = true;
                //        lnkSetFocus.Focus();
                //    }
                //}
                //else
                //{
                //    lblError.Text = "No buildings were selected.";
                //    lblError.Visible = true;
                //    lnkSetFocus.Focus();
                //}
            //}

        #endregion
        
    }
}
