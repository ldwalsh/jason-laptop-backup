﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSEquipmentClassAdministration.aspx.cs" Inherits="CW.Website.KGSEquipmentClassAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>KGS Equipment Class Administration</h1>                        
                <div class="richText">The kgs equipment class administration area is used to view, add, and edit equipment classes.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Equipment Classes"></telerik:RadTab>
                            <telerik:RadTab Text="Add Equipment Class"></telerik:RadTab>
                            <telerik:RadTab Text="Equipment Variables"></telerik:RadTab>
                            <telerik:RadTab Text="Equipment Manufacturers"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Equipment Classes</h2> 
                                    <p>
                                        Equipment classes in the grid that do not have a delete button available, are special classes used for pre ploted charts in the live data dashboard.
                                    </p>
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                                            <asp:ValidationSummary ID="valSummary" ShowSummary="true" ValidationGroup="EditEquipmentClasses" runat="server" />
                                    </p>           
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>
                                    
                                    <!--please check the gridEquipmentClasses_OnDataBound method if any cells are moved-->       
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridEquipmentClasses"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="EquipmentClassID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridEquipmentClasses_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridEquipmentClasses_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridEquipmentClasses_Sorting" 
                                             OnSelectedIndexChanged="gridEquipmentClasses_OnSelectedIndexChanged"                                                                                                              
                                             OnRowEditing="gridEquipmentClasses_Editing"  
                                             OnRowDeleting="gridEquipmentClasses_Deleting"
                                             OnDataBound="gridEquipmentClasses_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentClassName" HeaderText="Equipment Class">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("EquipmentClassName"), 30)%></ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Equipment Class Description">  
                                                    <ItemTemplate><label title="<%# Eval("EquipmentClassDescription") %>"><%# StringHelper.TrimText(Eval("EquipmentClassDescription"),100) %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="IsGroup" HeaderText="Is Group">  
                                                    <ItemTemplate><%# Eval("IsGroup")%></ItemTemplate>                                                  
                                                </asp:TemplateField>                                                         
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this equipment class permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <div>                                                  
                                    <!--SELECT EQUIPMENT CLASS DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvEquipmentClass" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Equipment Class Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Equipment Class Name: </strong>" + Eval("EquipmentClassName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentClassDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("EquipmentClassDescription") + "</li>"%>
                                                            <%# "<li><strong>Group: </strong>" + Eval("IsGroup") + "</li>"%>
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT EQUIPMENT PANEL -->                              
                                    <asp:Panel ID="pnlEditEquipmentClass" runat="server" Visible="false" DefaultButton="btnUpdateEquipmentClass">                                                                                             
                                        <div>
                                            <h2>Edit Equipment Class</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Equipment Class Name:</label>
                                                <asp:TextBox ID="txtEditEquipmentClassName" MaxLength="50" runat="server"></asp:TextBox>                                  
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>  
                                            <div class="divForm">
                                                <label class="label">*Group:</label>
                                                <asp:Label ID="lblEditIsGroup" CssClass="labelContent" runat="server"></asp:Label>  
                                                <p>(Note: You cannot change this setting after the class is in use.)</p>                                                                                                
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentClass" runat="server" Text="Edit Class"  OnClick="updateEquipmentClassButton_Click" ValidationGroup="EditEquipmentClass"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editEquipmentClassNameRequiredValidator" runat="server"
                                        ErrorMessage="Equipment Class Name is a required field."
                                        ControlToValidate="txtEditEquipmentClassName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditEquipmentClass">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editEquipmentClassNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editEquipmentClassNameRequiredValidatorExtender"
                                        TargetControlID="editEquipmentClassNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                    </asp:Panel>                                       
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddEquipmentClass" runat="server" DefaultButton="btnAddEquipmentClass"> 
                                    <h2>Add Equipment Class</h2> 
                                    <div>                            
                                        <a id="lnkSetFocusAdd" runat="server"></a>                        
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Equipment Class Name:</label>
                                            <asp:TextBox ID="txtAddEquipmentClassName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Description:</label>
                                            <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                            <div id="divAddDescriptionCharInfo"></div>
                                        </div>      
                                        <div class="divForm">
                                            <label class="label">*Group:</label>
                                            <asp:CheckBox ID="chkAddIsGroup" CssClass="checkbox" runat="server" />  
                                            <p>(Note: Group allows multiple of the same point types to be added to an equipment of this types class.)</p>                                                      
                                            <p>(Warning: You cannot change this setting later.)</p>                                                        
                                        </div>                                                                                        
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddEquipmentClass" runat="server" Text="Add Class"  OnClick="addEquipmentClassButton_Click" ValidationGroup="AddEquipmentClass"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="equipmentClassNameRequiredValidator" runat="server"
                                        ErrorMessage="Equipment Class Name is a required field."
                                        ControlToValidate="txtAddEquipmentClassName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddEquipmentClass">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="equipmentClassNameRequiredValidatorExtender"
                                        TargetControlID="equipmentClassNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>                                                                                                                                                                   
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView3" runat="server">  
                                <asp:Panel ID="pnlUpdateEquipmentVariables" runat="server" DefaultButton="btnUpdateEquipmentVariables">                                                                                                                                                                                                    
                                        <h2>Equipment Variables</h2>
                                        <p>
                                            Please select a equipment class in order to select and assign equipment variables.
                                        </p>
                                        <div>
                                            <a id="lnkSetFocusVariables" runat="server"></a>
                                            <asp:Label ID="lblVariablesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Equipment Class:</label>    
                                            <asp:DropDownList ID="ddlVariablesEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVariablesEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                          
                                        <div id="equipmentVariables" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                Equipment variables assiged to a class will be available to assign toward a piece of equipment with that class.
                                            </p>
                                            <p>
                                                Note: Always assign variables (electricitycost,fuelcost,pricepoint).
                                            </p>                                            
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbVariablesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnUp"  OnClick="btnVariablesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnDown"  OnClick="btnVariablesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbVariablesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentVariables" runat="server" Text="Reassign"  OnClick="updateEquipmentVariablesButton_Click" ValidationGroup="UpdateEquipmentVariables"></asp:LinkButton>    
                                        </div> 
                                  </asp:Panel>                                                 
                            </telerik:RadPageView> 
                            <telerik:RadPageView ID="RadPageView4" runat="server">
                                <asp:Panel ID="pnlUpdateEquipmentManufacturers" runat="server" DefaultButton="btnUpdateEquipmentManufacturers">                                                                                                                                                                                                       
                                        <h2>Equipment Manufacturers</h2>
                                        <p>
                                            Please select a equipment class in order to select and assign equipment manufacturers.
                                        </p>
                                        <div>
                                            <a id="lnkSetFocusManufacturers" runat="server"></a>
                                            <asp:Label ID="lblManufacturersError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Equipment Class:</label>    
                                            <asp:DropDownList ID="ddlManufacturersEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlManufacturersEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                          
                                        <div id="equipmentManufacturers" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                Equipment Manufacturers assiged to a class will be available to assign toward a piece of equipment with that class.
                                            </p>                                           
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbManufacturersTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="ImageButton1"  OnClick="btnManufacturersUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="ImageButton2"  OnClick="btnManufacturersDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbManufacturersBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentManufacturers" runat="server" Text="Reassign"  OnClick="updateEquipmentManufacturersButton_Click" ValidationGroup="UpdateEquipmentManufacturers"></asp:LinkButton>    
                                        </div>   
                                 </asp:Panel>                                               
                            </telerik:RadPageView>                                                                                 
                        </telerik:RadMultiPage>
                   </div>                                                                 
               
</asp:Content>


                    
                  
