﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Drawing;
using CW.Business;
using CW.Data;
using CW.Data.AzureStorage;
using CW.Data.AzureStorage.DataContexts.Blob;
using CW.Data.AzureStorage.Helpers;
using CW.Data.AzureStorage.Models.Blob;
using CW.Utility;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using CW.Website._reports;
using CW.Website._documents;
using CW.Website._diagnostics;
using CW.Website._controls;
using CW.Data.Models.Building;
using CW.Data.Models.Projects;
using CW.Common.Helpers;
using CW.Common.Constants;
using Telerik.Web.UI;

using Microsoft.WindowsAzure.Storage;
using CW.Data.Models.Equipment;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class BuildingProfile : SitePage
    {
        #region Properties

            public byte[] buildingImage;
            public string buildingImageUrl, buildingImageAlternateText;
            const string noBuildingImage = "no-building-image.png";
            private DateTime mStartDate, mEndDate;
            IEnumerable<BuildingAdministrationStatistics> BuildingStats;
            //IEnumerable<EquipmentAdministrationStatistics> EquipmentStats;

        #endregion

        #region Page Events

            protected void Page_Init(object sender, EventArgs e)
            {
                DateTimeHelper.GenerateDefaultDatesYesterday(out mStartDate, out mEndDate, siteUser.EarliestBuildingTimeZoneID);
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    int bid;
                    //bool isKGSAdminOrHigher = RoleHelper.IsKGSAdminOrHigher(Session["UserRoleID"].ToString());
                    //bool isProviderAdmin = (siteUser.AsProvider && siteUser.IsSuperAdmin);
                    //bool isProviderAdminOrHigher = (siteUser.AsProvider && siteUser.IsSuperAdmin);

                    BindBuildingDropdownList(ddlBuildingsInitial);
                    BindBuildingDropdownList(ddlBuildings);

                    //if bid querystring value exists, bind profile
                    if (!String.IsNullOrEmpty(Request.QueryString["bid"]) && Int32.TryParse(Request.QueryString["bid"], out bid))
                    {
                        //check if building is associated with client
                        if (DataMgr.BuildingDataMapper.IsBuildingAssociatedWithCient(bid, siteUser.CID))
                        {
                            PanelVisibilityHelper(true);

                            ddlBuildings.SelectedValue = bid.ToString();
                            ddlBuildings_OnSelectedIndexChanged(ddlBuildings, null);                            
                        }
                        else
                        {
                            PanelVisibilityHelper(false);
                        }
                    }
                    else if (ddlBuildings.Items.Count == 2)
                    {
                        PanelVisibilityHelper(true);

                        ddlBuildings.SelectedIndex = 1;
                        ddlBuildings_OnSelectedIndexChanged(ddlBuildings, null);
                    }
                    else
                    {
                        PanelVisibilityHelper(false);

                        CreateBuildingTiles();
                    }

                    ModuleBlockVisibilityHelper();
                }

                if (ViewState["buildingStats"] != null)
                    BuildingStats = (IEnumerable<BuildingAdministrationStatistics>)ViewState["buildingStats"];
            }

        #endregion

        #region Load and Bind Fields

         private void BindBuildingDropdownList(DropDownList ddl)
        {
            ListItem lItem = new ListItem();
            lItem.Text = "Select one...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddl.Items.Clear();
            ddl.Items.Add(lItem);
            ddl.DataTextField = "BuildingName";
            ddl.DataValueField = "BID";

            //get all visible buildings by client id. get all regardless of active, as they once may have been active.           
            IEnumerable<Building> buildings = siteUser.VisibleBuildings; //to get LoadWithState... BuildingDataMapper.GetAllVisibleBuildingsAssociatedToUserID(siteUser.UID, Convert.ToInt32(siteUser.CID), siteUser.IsRestricted);
            ddl.DataSource = buildings;
            ddl.DataBind();
           
            //populate address in dropdown items by added a title attribute
            int counter = 1;
            foreach (Building b in buildings)
            {
                //state not in visiblebiuldings
                string fullAddress = String.Format("{0}, {1}", b.Address, b.City);

                //need to temporarily enable if disabled
                ddl.Enabled = true;

                ddl.Items[counter].Attributes.Add("title", fullAddress);
                counter++;
            }
        }

        #endregion

        #region Button Events

         protected void btnBack_Click(object sender, EventArgs e)
         {
             PanelVisibilityHelper(false);

             SetHeader();

             CreateBuildingTiles(BuildingStats);
         }

        #endregion

        #region Dropdown Events

         protected void ddlBuildingsInitial_OnSelectedIndexChanged(object sender, EventArgs e)
         {
             if (ddlBuildingsInitial.SelectedValue == "-1") return;

             int bid = Convert.ToInt32(ddlBuildingsInitial.SelectedValue);

             ddlBuildings.SelectedValue = bid.ToString();

             PanelVisibilityHelper(true);

             BindProfile(siteUser.CID, bid);
         }

        protected void ddlBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            if (ddlBuildings.SelectedValue == "-1") return;

            BindProfile(siteUser.CID, Convert.ToInt32(ddlBuildings.SelectedValue));
        }

        #endregion

        #region Profile Events

            private void BindProfile(int cid, int bid)
            {
                CreateEquipmentTiles(bid);

                //do buildings based binds
                DoBuildingBasedBinds(bid);

                DateTime sd;
                DateTime ed;

                DateTimeHelper.GenerateDefaultDatesYesterday(out sd, out ed, siteUser.EarliestBuildingTimeZoneID);

                BindModuleLinks(cid, bid, sd, ed);

                BindModuleLists(cid, bid, sd, ed);
            }

            /// <summary>
            /// Do Binds by bid           
            /// </summary>
            /// <param name="bid"></param>
            private void DoBuildingBasedBinds(int bid)
            {
                IEnumerable<GetBuildingsData> building = DataMgr.BuildingDataMapper.GetFullBuildingByBID(bid);

                SetHeader(building.First().BuildingName);

                BindBuildingTeaser(building);

                BindBuildingDetails(building, bid);

                BindAdditionalDetails(building);

                BindExternalLinks(building);
            }


            /// <summary>
            /// Binds building teaser  
            /// </summary>
            /// <param name="building"></param>
            private void BindBuildingTeaser(IEnumerable<GetBuildingsData> building)
            {
                GetBuildingsData b = building.First();

                //literal
                litTeaser.Text = String.Format("<div>Building <strong class='profileTeaserColor'>{0}</strong> of Type <strong class='profileTeaserColor'>{1}</strong> located at <strong class='profileTeaserColor'>{2}</strong></div>", b.BuildingName, b.BuildingTypeName, b.Address);
                litTeaser.Text += String.IsNullOrEmpty(b.Description) ? "" : "<br /><div><strong>Description: </strong>" + b.Description + "</div>";
            }
                    
            private void BindModuleLinks(int cid, int bid, DateTime sd, DateTime ed)
            {
                //lnkReports.HRef =
                //    QueryString.Create
                //    (
                //        "~/Reports.aspx?",
                //        new ReportsParamsObject
                //        {
                //            cid = cid,
                //            bid = bid,  
                //        }
                //    );

                lnkDocuments.HRef =
                    QueryString.Create
                    (
                        "~/Documents.aspx?",
                        new DocumentsParamsObject
                        {
                            //cid = cid,
                            bid = bid,
                        }
                    );

                lnkDiagnostics.Attributes.Add("href", "Diagnostics.aspx?cid=" + cid + "&bid=" + bid + "&rng=" + DataConstants.AnalysisRange.Daily + "&sd=" + sd + "&ed=" + ed);
                lnkReports.Attributes.Add("href", "Reports.aspx?cid=" + cid + "&bid=" + bid);
                //lnkDocuments.Attributes.Add("href", "Documents.aspx?cid=" + cid + "&bid=" + bid);       
                lnkProjects.Attributes.Add("href", "Projects.aspx?cid=" + cid + "&bid=" + bid);
            }

            private void BindModuleLists(int cid, int bid, DateTime sd, DateTime ed)
            {
                if (divDiagnosticsBlock.Visible)
                {
                    //Diagnostics top 10
                    var pb = new DiagnosticsBinder(siteUser, DataMgr);
                    var bp = new _diagnostics.DiagnosticsBinder.BindingParameters();
                    bp.cid = cid;
                    bp.bids = new List<int>(new int[] { bid });
                    bp.range = DataConstants.AnalysisRange.Daily;
                    bp.start = sd;
                    bp.end = ed;

                    var results = pb.BuildTopDiagnosticsResults(bp, Convert.ToByte("10"));

                    if (results.Any())
                    {
                        lvDiagnostics.DataSource = results;
                        lvDiagnostics.DataBind();
                    }

                    divDiagnosticsTop10.Visible = results.Any();
                }

                if (divDocumentsBlock.Visible)
                {
                    //Documents top 10 viewed
                    var results2 = DataMgr.FileDataMapper.GetAllTopViewedFilesByBID(bid, 10);

                    if (results2.Any())
                    {
                        lvDocuments.DataSource = results2;
                        lvDocuments.DataBind();
                    }

                    divDocumentsTop10.Visible = results2.Any();
                }

                if (divProjectsBlock.Visible)
                {
                    //projects top 10
                    var results3 = DataMgr.ProjectDataMapper.GetTopGroupedProjectAssociations(new ProjectsInputs(){ CID = cid, BID = bid }, 10);

                    if (results3.Any())
                    {
                        lvProjects.DataSource = results3;
                        lvProjects.DataBind();
                    }

                    divProjectsTop10.Visible = results3.Any();
                }
            }

            /// <summary>
            /// Binds building details          
            /// </summary>
            /// <param name="building"></param>
            private void BindBuildingDetails(IEnumerable<GetBuildingsData> building, int bid)
            {
                GetBuildingsData b = building.First();

                imgProfile.Visible = true;
                imgProfile.Alt = b.BuildingName;
				imgProfile.Src = HandlerHelper.BuildingImageUrl(b.CID, bid, b.ImageExtension);

                //// Before doing a blob lookup, check if image extension exists in relational database
                //// Save on transactions ($$$)
                //if (!String.IsNullOrEmpty(b.ImageExtension))
                //{
                //    Func<byte[]> lookup = () =>
                //    {
                //        var blob = mDataManager.BuildingDataMapper.GetBuildingImage(b.BID, b.ImageExtension);
                        
                //        return blob != null ? blob.Image : null;
                //    };

                //    radImgProfile.ImageLoadFunction = lookup;

                //    //var blob = mDataManager.BuildingDataMapper.GetBuildingImage(b.BID, b.ImageExtension);

                //    //if (blob == null)
                //    //{
                //    //    DisplayNoBuildingImage();
                //    //}
                //    //else
                //    //{
                //        //imgProfile.Src = TempFileHandler.GetURL(sessionState, TempFileHandler.CreateTempFile(sessionState, String.Empty, blob.Image));
                //        //imgProfile.Alt = b.BuildingName;
                //    //}
                //}

                //set data source
                dtvBuilding.DataSource = building;
                //bind Building to details view
                dtvBuilding.DataBind();
            }

            //private void DisplayNoBuildingImage()
            //{
            //    imgProfile.Src = ConfigurationManager.AppSettings["ImageAssetPath"] + "no-building-image.png";
            //    imgProfile.Alt = "no building image";
            //}

            /// <summary>
            /// Binds additional building details       
            /// </summary>
            /// <param name="building"></param>
            private void BindAdditionalDetails(IEnumerable<GetBuildingsData> building)
            {
                //set data source
                dtvBuildingAdditional.DataSource = building;
                //bind building to details view
                dtvBuildingAdditional.DataBind();
            }

            /// <summary>
            /// Binds external links     
            /// </summary>
            /// <param name="building"></param>
            private void BindExternalLinks(IEnumerable<GetBuildingsData> building)
            {
                //set data source
                dtvExternalLinks.DataSource = building;
                //bind building to details view
                dtvExternalLinks.DataBind();
            }

        #endregion

        #region Helper Methods

            protected void SetHeader(string buildingName = "")
            {
                header.InnerText = "Building Profile";
        
                if(String.IsNullOrEmpty(buildingName)) 
                    header.InnerText += ": " + buildingName;
            }

            protected void PanelVisibilityHelper(bool hideInitialState)
            {
                pnlProfile.Visible = hideInitialState;
                pnlInitialSelection.Visible = !hideInitialState;
            }

            protected void ModuleBlockVisibilityHelper()
            {
                divDiagnosticsBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Diagnostics));
                divReportsBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Reporting));
                divDocumentsBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Documents));
                divProjectsBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Projects));
            }

            private Int16 CostThresholdValue(Double? costSavings, Int32? bid, Int32? eid)
            {
                if (costSavings == null)
                    return 0;

                Double? costThreshold = 0;

                if (bid != null)
                {
                    var bv = DataMgr.BuildingVariableDataMapper.GetBuildingVariableByBIDAndBVID((Int32)bid, BusinessConstants.BuildingVariable.AnnualBuildingCostThresholdBVID);

                    if (bv != null) costThreshold = Convert.ToDouble(bv.Value) / 365;
                }
                else if (eid != null)
                {
                    var ev = DataMgr.EquipmentVariableDataMapper.GetEquipmentVariableByEIDAndEVID((Int32)eid, BusinessConstants.EquipmentVariable.AnnualEquipmentCostThresholdEVID);

                    if (ev != null) costThreshold = Convert.ToDouble(ev.Value) / 365;
                }

                if (costThreshold == 0)
                    return 0;
                if (costSavings == 0)
                    return 1;
                if (costSavings > 0 && costSavings < (costThreshold / 2))
                    return 2;
                if (costSavings < costThreshold && costSavings >= (costThreshold / 2))
                    return 3;
                if (costSavings >= costThreshold)
                    return 4;

                return 0;
            }

        #endregion
        
        #region Tile Methods

            public void CreateBuildingTiles(IEnumerable<BuildingAdministrationStatistics> stats = null)
            {
                IEnumerable<Building> buildings = siteUser.VisibleBuildings;
                IEnumerable<BuildingType> types = DataMgr.BuildingTypeDataMapper.GetAllBuildingTypes();

                //bug workaround, required after async postback
                BuildingRadTileList.Groups.Clear();

                if(stats == null)
                    stats = DataMgr.BuildingDataMapper.GetBuildingAdministrationStatisticsByBuildings(siteUser.CID, buildings, mStartDate, true);
                
                //bug workaround, set temporariliy
                BuildingRadTileList.AppendDataBoundItems = true;

                //filter and order
                types = types.Where(t => buildings.Select(b => b.BuildingTypeID).Distinct().Contains(t.BuildingTypeID)).OrderBy(t => t.BuildingTypeName);

                //Create tile groups
                foreach (BuildingType type in types)
                {
                    TileGroup tg = new TileGroup();
                    tg.Name = type.BuildingTypeName;
                                
                    RadTextTile tt = new RadTextTile();
                    tt.Text = tg.Name;
                    tt.AutoPostBack = false;
                    tt.Width = 160;
                    tt.EnableSelection = false;
                    tt.ForeColor = Color.White;
                    tt.Font.Size = FontUnit.Large;
                    tt.Font.Bold = true;
                    tt.BackColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.DarkGray);

                    //workaround, so the group tiles are not clickable.
                    tt.Enabled = false;

                    tg.Tiles.Add(tt);

                    BuildingRadTileList.Groups.Add(tg);
                }

                //merge stats and buildings
                IEnumerable<BuildingAdministrationStatisticsFull> list = buildings.Select(b => new BuildingAdministrationStatisticsFull
                                                                        {
                                                                            BID = b.BID,
                                                                            BuildingName = b.BuildingName,
                                                                            BuildingTypeName = types.Where(t => t.BuildingTypeID == b.BuildingTypeID).FirstOrDefault().BuildingTypeName,
                                                                            Address = b.Address,
                                                                            City = b.City,
                                                                            Sqft = b.Sqft,
                                                                            YearBuilt = b.YearBuilt,
                                                                            OperatingHours = b.OperatingHours,
                                                                            EquipmentCount = stats.Where(s => s.BID == b.BID).FirstOrDefault().EquipmentCount,
                                                                            PointCount = stats.Where(s => s.BID == b.BID).FirstOrDefault().PointCount,
                                                                            BuildingVariablesCount = stats.Where(s => s.BID == b.BID).FirstOrDefault().BuildingVariablesCount,
                                                                            YesterdayTotalCost = stats.Where(s => s.BID == b.BID).FirstOrDefault().YesterdayTotalCost,
                                                                            YesterdayEnergyScore = stats.Where(s => s.BID == b.BID).FirstOrDefault().YesterdayEnergyScore,
                                                                            YesterdayMaintenanceScore = stats.Where(s => s.BID == b.BID).FirstOrDefault().YesterdayMaintenanceScore,
                                                                            YesterdayComfortScore = stats.Where(s => s.BID == b.BID).FirstOrDefault().YesterdayComfortScore,                                                                            
                                                                            CostThresholdValue = CostThresholdValue(stats.Where(s => s.BID == b.BID).FirstOrDefault().YesterdayTotalCost, b.BID, null),
                                                                        }).ToList().OrderBy(b => b.BuildingName);

                BuildingRadTileList.DataSource = list;
                BuildingRadTileList.DataBind();

                //bug workaround, set back
                BuildingRadTileList.AppendDataBoundItems = false;

                ViewState.Add("buildingStats", stats);
            }

            public void CreateEquipmentTiles(int bid, IEnumerable<EquipmentAdministrationStatistics> stats = null)
            {
                IEnumerable<Equipment> equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid, (e => e.EquipmentType));
                IEnumerable<EquipmentClass> classes = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();
                
                //bug workaround, required after async postback
                EquipmentRadTileList.Groups.Clear();

                if(stats == null)
                    stats = DataMgr.EquipmentDataMapper.GetEquipmentAdministrationStatisticsByBID(siteUser.CID, bid, mStartDate, true);

                //bug workaround, set temporariliy
                EquipmentRadTileList.AppendDataBoundItems = true;

                //filter and order
                classes = classes.Where(c => equipment.Select(e => e.EquipmentType.EquipmentClassID).Distinct().Contains(c.EquipmentClassID)).OrderBy(c => c.EquipmentClassName);

                //Create tile groups
                foreach (EquipmentClass c in classes)
                {
                    TileGroup tg = new TileGroup();
                    tg.Name = c.EquipmentClassName;

                    RadTextTile tt = new RadTextTile();
                    tt.Text = tg.Name;
                    tt.AutoPostBack = false;
                    tt.Width = 160;
                    tt.EnableSelection = false;
                    tt.ForeColor = Color.White;
                    tt.Font.Size = FontUnit.Large;
                    tt.Font.Bold = true;
                    tt.BackColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.DarkGray);

                    //workaround, so the group tiles are not clickable.
                    tt.Enabled = false;

                    tg.Tiles.Add(tt);

                    EquipmentRadTileList.Groups.Add(tg);
                }

                //merge stats and equipment
                IEnumerable<EquipmentAdministrationStatisticsFull> list = equipment.Select(e => new EquipmentAdministrationStatisticsFull
                {
                    BID = e.BID,
                    EID = e.EID,
                    EquipmentName = e.EquipmentName,
                    EquipmentClassName = classes.Where(c => c.EquipmentClassID == e.EquipmentType.EquipmentClassID).FirstOrDefault().EquipmentClassName,
                    EquipmentLocation = e.EquipmentLocation,
                    LastServicedBy = e.LastServicedBy,
                    LastServicedDate = e.LastServicedDate,
                    AssociatedEquipmentCount = stats.Where(s => s.EID == e.EID).FirstOrDefault().AssociatedEquipmentCount,
                    PointCount = stats.Where(s => s.EID == e.EID).FirstOrDefault().PointCount,
                    EquipmentVariablesCount = stats.Where(s => s.EID == e.EID).FirstOrDefault().EquipmentVariablesCount,
                    YesterdayTotalCost = stats.Where(s => s.EID == e.EID).FirstOrDefault().YesterdayTotalCost,
                    YesterdayEnergyScore = stats.Where(s => s.EID == e.EID).FirstOrDefault().YesterdayEnergyScore,
                    YesterdayMaintenanceScore = stats.Where(s => s.EID == e.EID).FirstOrDefault().YesterdayMaintenanceScore,
                    YesterdayComfortScore = stats.Where(s => s.EID == e.EID).FirstOrDefault().YesterdayComfortScore,                    
                    CostThresholdValue = CostThresholdValue(stats.Where(s => s.EID == e.EID).FirstOrDefault().YesterdayTotalCost, null, e.EID),
                }).ToList().OrderBy(e => e.EquipmentName);

                EquipmentRadTileList.DataSource = list;
                EquipmentRadTileList.DataBind();

                //bug workaround, set back
                EquipmentRadTileList.AppendDataBoundItems = false;

                //ViewState.Add("equipmentStats", stats);
            }

        #endregion

        #region Tile Events

            /// <summary>
            /// Shared for buildingtilelist and equipmenttilelist
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void RadTileList_OnTileDataBound(object sender, TileListEventArgs e)
            {
                //common settings for all tiles created on bind
                e.Tile.Width = 160;
                //e.Tile.EnableSelection = false;
                //e.Tile.AutoPostBack = true;
                e.Tile.PeekTemplateSettings.ShowInterval = 0;
                e.Tile.PeekTemplateSettings.CloseDelay = 0;
                e.Tile.PeekTemplateSettings.ShowPeekTemplateOnMouseOver = true;
                e.Tile.PeekTemplateSettings.HidePeekTemplateOnMouseOut = true;
                e.Tile.PeekTemplateSettings.Animation = PeekTemplateAnimation.Slide;
                e.Tile.PeekTemplateSettings.AnimationDuration = 1500;
            }

            protected void BuildingRadTileList_OnTileClick(object sender, TileListEventArgs e)
            {
                ddlBuildings.SelectedValue = e.Tile.Name;

                PanelVisibilityHelper(true);

                BindProfile(siteUser.CID, Convert.ToInt32(e.Tile.Name));
            }

            protected void EquipmentRadTileList_OnTileClick(object sender, TileListEventArgs e)
            {
               // ddlBuildings.SelectedValue = e.Tile.Name;

               // PanelVisibilityHelper(true);

               // BindProfile(siteUser.CID, Convert.ToInt32(e.Tile.Name));

                Response.Redirect("/EquipmentProfile.aspx?eid=" + e.Tile.Name);
            }

            #endregion
    }
}
