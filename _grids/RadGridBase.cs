﻿using CW.Business;
using CW.Utility;
using CW.Website._administration;
using CW.Website._framework;
using CW.Website.DependencyResolution;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._grids
{
    public sealed class RadGridBase<T> where T : new()
    {
        #region fields

            RadGrid _radGrid;
            GridCacher _gridCacher;
            int pageSize = 25;

        #endregion

        #region delegates

            public delegate void GridLoader();
            public delegate IEnumerable GetEntitiesForGridDelegate(IEnumerable<string> criteria);
            public delegate IEnumerable<T> TransformData(IEnumerable<GridCacher.GridSet.Row> rows);
            public delegate void ItemCountMessage();

        #endregion

        #region properties

            public GridLoader GetGridLoader { get; set; }
            public GetEntitiesForGridDelegate GetEntitiesForGrid { get; set; }
            public TransformData GetTransformData { get; set; }    
            IEnumerable<GridCacher.GridSet.Row> Rows { get; set; }
            public IEnumerable<string> GridColumnNames { get; set; }
            public string NameField { get; set; }
            public string IdColumnName { get; set; }
            public Type Entity { get; set; }
            public string Name { get; set; }
            public CultureInfo CultureInfo { private get; set; }
            public string DefaultNumVal { private get; set; }
            
            GridCacher GridCacher
            {
                get
                {
                    var cacheStore = IoC.Resolve<DataManagerCommonStorage>().DefaultCacheRepository;

                    var cacheName = new List<string> { Name, ".", Entity.Name };

                    var createCacheKeyInfo = new GridCacher.CacheKeyInfo(string.Concat(cacheName), () => { return CacheHashKey; }, () => { return null; });

                    _gridCacher =
                    (
                        _gridCacher ?? new GridCacher
                        (   
                            Entity, 
                            cacheStore, 
                            createCacheKeyInfo, 
                            new Func<IEnumerable<string>, 
                            IEnumerable>(GetEntitiesForGrid), 
                            new GridCacher.ColumnInfo(IdColumnName, GridColumnNames, null)
                        ) { AlwaysForceRefresh = AlwaysForceGridCacherRefresh }
                    );

                    return _gridCacher;
                }
            }
    
            public string CacheHashKey { get; set; }
            public int PageSize { get { return pageSize; } set { pageSize = value; } }
            public bool AlwaysForceGridCacherRefresh => true;
            public bool Visible { get { return _radGrid.Visible; } set { _radGrid.Visible = value; } }
            public GridDataItemCollection Items => _radGrid.Items;
            public int RowCount { get; set; }
            public ItemCountMessage GetItemCountMessage { get; set; }
            public GridCommandEventHandler RadGridItemCommandHandler { get; set; }
            public GridCommandEventHandler RadGridDeleteCommandHandler { get; set; }
            public GridGroupsChangingEventHandler RadGridGroupsChangingEventHandler { get; set; }
            public GridItemEventHandler RadGridItemEventHander { get; set; }
            public bool EnableGrouping { get; set; }
            public bool EnableItemDataBound { get; set; }
            public bool ShowFooter { get; set; }

        #endregion

        #region constructor

            public RadGridBase(TemplateControl templateControl, RadGrid radGridObj)
            {
                _radGrid = radGridObj;

                templateControl.Init += OnTemplateControlInit;

                //TODO, revise so that the object casting is done properly (TemplateControl can be either a SitePage or SiteUserControl)
                ((SiteUserControl)templateControl).FirstLoad += OnTemplateControlFirstLoad;
            }

        #endregion

        #region events

            void OnTemplateControlInit(object sender, EventArgs e)
            {
                _radGrid.NeedDataSource += radGrid_NeedDataSource;

                _radGrid.ItemCommand += radGrid_ItemCommand;

                _radGrid.DeleteCommand += radGrid_DeleteCommand;

                if (EnableGrouping) _radGrid.GroupsChanging += radGrid_GroupsChanging;

                if (EnableItemDataBound) _radGrid.ItemDataBound += radGrid_ItemDataBound;
            }

            void OnTemplateControlFirstLoad(object sender, EventArgs e)
            {
                _radGrid.Visible = _radGrid.AutoGenerateColumns = false;

                _radGrid.AllowSorting = _radGrid.AllowPaging = _radGrid.ClientSettings.ReorderColumnsOnClient = _radGrid.ClientSettings.AllowColumnsReorder = true;

                _radGrid.MasterTableView.DataKeyNames = new[] { IdColumnName };

                _radGrid.GridLines = GridLines.None;

                _radGrid.PageSize = PageSize;

                _radGrid.PagerStyle.Mode = GridPagerMode.NextPrevAndNumeric;

                _radGrid.PagerStyle.PageSizeControlType = PagerDropDownControlType.None;

                if (EnableGrouping)
                {
                    _radGrid.MasterTableView.GroupLoadMode = GridGroupLoadMode.Client;

                    _radGrid.MasterTableView.HierarchyLoadMode = GridChildLoadMode.Client;

                    _radGrid.ShowGroupPanel = _radGrid.ClientSettings.AllowDragToGroup = _radGrid.ClientSettings.AllowExpandCollapse = _radGrid.GroupingSettings.ShowUnGroupButton = _radGrid.MasterTableView.ShowGroupFooter = true;
                }

                _radGrid.ShowFooter = ShowFooter;

                GetGridLoader();

                GetItemCountMessage();
            }

            #region grid events

                void radGrid_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
                {
                    GetData();

                    SetDataSource();
                }

                void radGrid_ItemCommand(object sender, GridCommandEventArgs e)
                {
                    switch (e.CommandName)
                    {
                        case "Select":
                        case "Edit":

                            CancelGridEvent(e);

                            SetSelectedItem(e);

                            break;
                    }

                    RadGridItemCommandHandler(sender, e);
                }

                void radGrid_DeleteCommand(object sender, GridCommandEventArgs e) => RadGridDeleteCommandHandler(sender, e);

                void radGrid_GroupsChanging(object sender, GridGroupsChangingEventArgs e) 
                {
                    ClearEditItems();

                    RadGridGroupsChangingEventHandler(sender, e);
                }

                void radGrid_ItemDataBound(object sender, GridItemEventArgs e) => RadGridItemEventHander(sender, e);

            #endregion

        #endregion

        #region methods

            public void SetColumnAttribute(string columnName, string headerText, bool isSortable = true, bool isGroupable = false)
            {
                //explanation of GetColumnSafe http://docs.telerik.com/devtools/aspnet-ajax/controls/grid/how-to/using-the--getitems-getcolumn-and-getcolumnsafe-methods
                var column = _radGrid.MasterTableView.GetColumnSafe(columnName);
                
                if (column == null) throw new NullReferenceException($"{columnName} is not a valid column");
                
                if (isSortable) column.SortExpression = columnName;

                column.HeaderText = headerText;

                if (isGroupable) column.GroupByExpression = $"{columnName} {column.UniqueName} Group By {columnName}";
                
                column.Groupable = isGroupable; //need to explictly set this each time, otherwise it defaults to true (thanks Teleirk!)
            }

            public void SetColumnFooter(string columnName, GridAggregateFunction aggregate, string footerText)
            {
                //explanation of GetColumnSafe http://docs.telerik.com/devtools/aspnet-ajax/controls/grid/how-to/using-the--getitems-getcolumn-and-getcolumnsafe-methods
                var column = _radGrid.MasterTableView.GetColumnSafe(columnName);

                if (column == null) throw new NullReferenceException($"{columnName} is not a valid column");

                if (column.ColumnType == nameof(GridTemplateColumn))
                {
                    var boundColumn = column as GridTemplateColumn;

                    boundColumn.Aggregate = aggregate;

                    boundColumn.FooterAggregateFormatString = footerText;
                }                                                                
            }

        	public int GetId(GridCommandEventArgs e) => int.Parse(_radGrid.MasterTableView.DataKeyValues[e.Item.ItemIndex][IdColumnName].ToString());

            public void GetData(bool forceRefresh = false)
            {
                var gs = GridCacher.Get(forceRefresh);

                Rows = gs.Rows;

                if (Rows != null) RowCount = Rows.Count();
            }

            public void BindData()
            {
                if (Rows != null)
                {
                    SetDataSource();

                    _radGrid.DataBind();
                }
            }

            public void GetAndBindData()
            {
                GetData();

                BindData();
            }

            public string GetRowCountMsg()
            {
                var formattedName = LanguageHelper.GetFormattedName(Name, (RowCount == 1));

                return (RowCount == 0) ? $"No {formattedName} found." : $"{RowCount} {formattedName} found.";
            }

            void SetDataSource() => _radGrid.DataSource = GetTransformData(Rows).ToList();

            void SetSelectedItem(GridCommandEventArgs e) => ((GridDataItem)(GridItem)_radGrid.MasterTableView.Items[e.Item.ItemIndex]).Edit = true;

            void CancelGridEvent(GridCommandEventArgs e) => e.Canceled = true;

            public void ClearEditItems() => _radGrid.MasterTableView.ClearEditItems();

            public void ResetGrouping() => _radGrid.MasterTableView.GroupByExpressions.Clear();

            public void ResetSorting() => _radGrid.MasterTableView.SortExpressions.Clear();

            public void ResetPaging() => _radGrid.MasterTableView.CurrentPageIndex = 0;

            public bool GroupedItemWasSet(GridGroupHeaderItem header, string headerText, Func<string, string> getValue, string column, string displayText = null)
            {
                var containsField = headerText.Contains(column);

                if (containsField)
                {
                    var val = headerText.Replace($"{column}: ", string.Empty); //we just need the value here, not the column name

                    var text = displayText ?? column;

                    header.DataCell.Text = $"{text}: {getValue(val)}";
                }

                return containsField;
            }

            public string GetFormattedNumValue(int? val, CultureInfo ci) => (val != null) ? FormatValue(val.Value, ci) : DefaultNumVal;

            public string GetFormattedNumValue(string val, CultureInfo ci) => (!string.IsNullOrWhiteSpace(val)) ? FormatValue(int.Parse(val), ci) : DefaultNumVal;

            string FormatValue(int val, CultureInfo ci) => val.ToString("n0", ci);

        #endregion
    }
}