﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using CW.Utility;
using CW.Business.Blob;
using CW.Business.Blob.Images;
using CW.Common.Constants;

namespace CW.Website
{
    public partial class KGSAdministration : Page
    {
        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page
            }

        #endregion
    }
}
