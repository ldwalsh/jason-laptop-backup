﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;

using CW.Data;
using CW.Data.Models.Building;
using CW.Data.Models.Client;
using CW.Utility;
using CW.Website._framework;
using CW.Website.Dashboard;
using iCL;
using iCLib;
using JSAsset;
using Telerik.Web.UI;

namespace CW.Website
{
	public partial class CommissioningDashboard: SitePage, SitePage.IClientPage
	{
		#region Properties

			private int uid;
			private int cid;
			const string noClientImage = "no-client-image.png";
			private int roleID;
			private bool isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser;
			//private string siteAddress;

			//building
			//public string serializedBuildings;
			//public byte[] buildingImage = null;
			//public string buildingImageUrl = "", buildingImageAlternateText = "";
			const string noBuildingImage = "no-building-image.png";

			private Dictionary<String,String> asyncProperties = new Dictionary<String,String>();

		#endregion

		#region Page Events

			private void Page_FirstInit()
			{
				ScriptIncluder.GetInstance(this) // wont be necessary to specifiy each reauired iCL resource in future
					.Add<core.coreClass>()
					.Add<core.coreEnum>()
					.Add<core.ArrayPrototype>()
					.Add<core.String>()
					.Add<core.asp.SessionKeepAlive>()
					.Add<core.Document>()
					.Add<core.Event>()
					.Add<core.TypedArray>()
					.Add<core.asp.webForm.AsyncPostbackManager>()
					.Add<core.HTMLAnchorPrototype>()
					.Add<core.HTMLCollectionPrototype>()
					.Add<core.HTMLSelectElement>()
					.Add<core.Function>() //will be removed in future
					.Add<core.ui.controls.ContextMenu>()
					.Add<core.Url>();

				JSAssetManager.GetInstance(this).Add<CommissioningDashboardAsset>();
			}

			protected override void OnLoad(EventArgs e)
			{
				//if the page is not a postback
				if (!Page.IsPostBack)
				{
					//set admin global settings
					//Make sure to decode from ascii to html 
					GlobalSetting globalSetting = mDataManager.GlobalDataMapper.GetGlobalSettings();
					litDashboardBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme,Server.HtmlDecode(globalSetting.CommissioningDashboardBody));

					//get and bind buildings
					BindBuildings(ddlBuilding, siteUser.VisibleBuildings);

					//disable nav buttons
					btnPerformance.Enabled = false;
					btnClientStatistics.Enabled = false;
					btnBuildingStatistics.Enabled = false;

					SetClientInfoAndImages();
				}   

				//set global info
				uid = siteUser.UID;
				roleID = siteUser.RoleID;
				cid = siteUser.CID;
				//siteAddress = ConfigurationManager.AppSettings["SiteAddress"];            
				//serializedBuildings = GetParam("Buildings");
				isKGSFullAdminOrHigher = siteUser.IsKGSFullAdminOrHigher;
				isSuperAdminOrFullAdminOrFullUser = siteUser.IsSuperAdminOrFullAdminOrFullUser;
			}

		#endregion

		#region Load and Bind Fields

			/// <summary>
			/// 
			/// </summary>
			/// <param name="ddl"></param>
			/// <param name="buildings"></param>
			private void BindBuildings(DropDownList ddl, IEnumerable<Building> buildings)
			{
				ddl.DataTextField = "BuildingName";
				ddl.DataValueField = "BID";
				ddl.DataSource = buildings;
				ddl.DataBind();

				//bind building image scroller
				BindBuildingScroller(buildings);
			}

			/// <summary>
			/// Binds a dropdownlist with all equipment classes for the building
			/// </summary>
			/// <param name="ddl"></param>
			private void BindEquipmentClasses(DropDownList ddl, int? bid)
			{
				//TODO:  this.client.GetEquipmentClassInBuildingDataAsync(buildingID, Convert.ToInt32(selected.Tag));

				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				var classes =
					((bid == null) ? mDataManager.EquipmentClassDataMapper.GetAllEquipmentClasses() : mDataManager.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid.Value));

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "EquipmentClassName";
				ddl.DataValueField = "EquipmentClassID";
				ddl.DataSource = classes;
				ddl.DataBind();
			}

			/// <summary>
			/// Binds a dropdownlist with all equipment
			/// </summary>
			/// <param name="ddl"></param>
			private void BindEquipment(DropDownList ddl, int bid, int equipmentClassID)
			{
				ListItem lItem = new ListItem();
				lItem.Text = "Select one...";
				lItem.Value = "-1";
				lItem.Selected = true;

				ddl.Items.Clear();
				ddl.Items.Add(lItem);
				ddl.DataTextField = "EquipmentName";
				ddl.DataValueField = "EID";
				ddl.DataSource = mDataManager.EquipmentDataMapper.GetAllActiveEquipmentByBuildingIDAndEquipmentClassID(bid, equipmentClassID);
				ddl.DataBind();
			}

		#endregion

		#region Set Fields and Data

			private void SetClientInfoAndImages()
			{
                GetClientsData client = mDataManager.ClientDataMapper.GetFullClientByIDAsEnumberable(siteUser.CID).First();

                //radHeaderClientImage.AlternateText = siteUser.ClientName;
                //radHeaderClientImage.Visible = true;
                //radHeaderClientImage.DefaultImageVirtualPath = "~/" + ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;

                imgHeaderClientImage.AlternateText = siteUser.ClientName;
                imgHeaderClientImage.Visible = true;
                imgHeaderClientImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;

                // Before doing a blob lookup, check if image extension exists in relational database
                // Save on transactions ($$$)
                if (!String.IsNullOrEmpty(client.ImageExtension))
                {
                    //var blob = mDataManager.ClientDataMapper.GetClientImage(client.First().CID, client.First().ImageExtension);

                    imgHeaderClientImage.ImageUrl = HandlerHelper.ClientImageUrl(client.CID, client.ImageExtension);

                    //radHeaderClientImage.DataValue = blob != null ? blob.Image : null;
                }

                lblHeaderClientName.InnerText = StringHelper.TrimText(siteUser.ClientName, 22);
			}

			private void SetBuildingInfoAndImages(int? bid)
			{
                if (!bid.HasValue)
                {
                    lblHeaderBuildingName.InnerText = String.Empty;
                    imgHeaderBuildingImage.Visible = false;
                    return;
                }

                GetBuildingsData building = mDataManager.BuildingDataMapper.GetFullBuildingByBID(bid.Value).FirstOrDefault();

                imgHeaderBuildingImage.AlternateText = building.ClientName;
                imgHeaderBuildingImage.Visible = true;
                imgHeaderBuildingImage.ImageUrl = HandlerHelper.BuildingImageUrl(building.BID, building.ImageExtension); 
                lblHeaderBuildingName.InnerText = StringHelper.TrimText(building.BuildingName, 22);
			}

		#endregion

		#region Dropdown Events

            protected void ddlBuilding_OnSelectedIndexChanged(object sender, EventArgs e)
            {
				var index = 0;

				foreach (RadRotatorItem rri in rrBuildingScroller.Items)
				{
					if (((HiddenField)rri.Controls[1]).Value == ddlBuilding.SelectedValue)
					{
						index = rri.Index;
					}
				}

                //bind based on building select
                BindOnBuildingSelect((ddlBuilding.SelectedValue == "all") ? null : (Int32?)Convert.ToInt32(ddlBuilding.SelectedValue), index);
            }


			/// <summary>
			/// ddlPerformanceEquipmentClass_OnSelectedIndexChanged, binds equipment dropdown by selected class, and allows for equipment class dropdown to be visable
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			protected void ddlPerformanceEquipmentClass_OnSelectedIndexChanged(object sender, EventArgs e)
			{
				if (ddlBuilding.SelectedValue == "all") return;

				BindEquipment(ddlPerformanceEquipment, Convert.ToInt32(ddlBuilding.SelectedValue), Convert.ToInt32(ddlPerformanceEquipmentClass.SelectedValue));
			}

			/// <summary>
			/// ddlPerformanceEquipment_OnSelectedIndexChanged, binds points lb by eid
			/// </summary>
			/// <param name="sender"></param>
			/// <param name="e"></param>
			//protected void ddlPerformanceEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
			//{
			//	if (ddlPerformanceEquipment.SelectedIndex > 0)
			//	{
			//		btnEquipment.Enabled = true;
			//	}
			//	else
			//	{
			//		btnEquipment.Enabled = false;
			//	}
			//}

		#endregion

		#region Main Nav Button Click Events

		/// <summary>
		/// Home view button click
		/// </summary>
		protected void homeButton_Click(object sender, EventArgs e)
		{
			mvMain.ActiveViewIndex = 0;
			mvSecondary.ActiveViewIndex = 0;

			SetActiveNavButton(btnHome);
		}

		/// <summary>
		/// Performance view button click
		/// </summary>
		protected void performanceButton_Click(object sender, EventArgs e)
		{
			mvMain.ActiveViewIndex = 1;
			mvSecondary.ActiveViewIndex = 1;

			SetActiveNavButton(btnPerformance);

			ddlPerformanceEquipment		.SelectedIndex = 0;
			ddlPerformanceEquipmentClass.SelectedIndex = 0;

			//TODO: maybe user service client side
			//dashboardService.GetAllEquipmentClassData();
		}

		/// <summary>
		/// Client statistics view button click
		/// </summary>
		protected void clientStatisticsButton_Click(object sender, EventArgs e)
		{
			mvMain.ActiveViewIndex = 2;
			mvSecondary.ActiveViewIndex = 2;

			SetActiveNavButton(btnClientStatistics);
		}

		/// <summary>
		/// Building statistics view button click
		/// </summary>
		protected void buildingStatisticsButton_Click(object sender, EventArgs e)
		{
			mvMain.ActiveViewIndex = 3;
			mvSecondary.ActiveViewIndex = 3;

			SetActiveNavButton(btnBuildingStatistics);
		}

		protected void btnViewAll_Click(object sender, EventArgs e)
		{
			BindOnBuildingSelect(null);
		}

		#endregion

		#region Binding Methods

			private void BindBuildingScroller(IEnumerable<Building> buildings)
			{
				if (buildings.Any())
				{
					List<BuildingImage> buildingImages =  new List<BuildingImage>();

					foreach (Building b in buildings)
					{
						BuildingImage img = new BuildingImage();

                        img.src = HandlerHelper.BuildingImageUrl(b.BID, b.ImageExtension);
                        img.imgVisible = true;
						img.alt = b.BuildingName;
						img.bid = b.BID;

						buildingImages.Add(img);
					}

					rrBuildingScroller.DataSource = buildingImages;
					rrBuildingScroller.DataBind();

					rrBuildingScroller.InitialItemIndex = siteUser.VisibleBuildings.Count() > 1 ? Convert.ToInt32(Math.Ceiling((double)(siteUser.VisibleBuildings.Count() / 2))) - 1 : 0;
				}
				else
				{
					divSelectedBuilding.InnerText = "No buildings available.";
				}
			}

			/// <summary>
			/// 
			/// </summary>
			/// <param name="bid"></param>
			private void BindOnBuildingSelect(int? bid, int? scrollerIndex=null)
			{
				if (ddlBuilding.Items[0].Value == "-1")
				{
					ddlBuilding.Items.RemoveAt(0);
				}
				
                //TODO: get currency hex from diagnosticresults data and show in series legend for each cost savings series. No need to show on axis anymore.
				
				//add currency hex as client side prop (if 1 building selected)
				if (bid != null)
				{
					asyncProperties.Add("buildingCurrencyHex", CultureHelper.GetCurrencySymbol(mDataManager.BuildingDataMapper.GetBuildingSettingsByBID((int)bid).LCID));
				}

				//enable nav buttons
				btnHome.Enabled = true;
				btnPerformance.Enabled = true;
				btnClientStatistics.Enabled = true;
				btnBuildingStatistics.Enabled = true;

				divSelectedBuilding.InnerText = ddlBuilding.SelectedItem.Text;

				//set buidling scroller building item
				if (scrollerIndex != null)
				{
					rrBuildingScroller.InitialItemIndex = scrollerIndex.Value;
				}

				//set buildinginfo and images
			    SetBuildingInfoAndImages(bid);				

				BindEquipmentClasses(ddlPerformanceEquipmentClass, bid);

				//
				//clear equipment

				ListItem lItem = new ListItem();
				lItem.Text = "Select equipment class first...";
				lItem.Value = "-1";
				lItem.Selected = true;
				ddlPerformanceEquipment.Items.Clear();
				ddlPerformanceEquipment.Items.Add(lItem);

				//

				divEquipmentFilter.Visible = (bid != null);

				performanceButton_Click(null, null);
			}

		#endregion

		#region Building Scroller

			/// <summary>
			/// building item click
			/// </summary>
			protected void radRotaterBuildingScroller_OnItemClick(object sender, RadRotatorEventArgs e)
			{
				HiddenField hdnBID = (HiddenField)e.Item.Controls[1];
				ddlBuilding.SelectedValue = hdnBID.Value;

				//BindOnBuildingSelect(Convert.ToInt32(hdnBID.Value), e.Item.Index);
				BindOnBuildingSelect(Convert.ToInt32(hdnBID.Value));
			}

			private class BuildingImage
			{
				public byte[] image { get; set; }
				public string src { get; set; }
				public string alt { get; set; }
				public bool radVisible { get; set; }
				public bool imgVisible { get; set; }
				public int bid { get; set; }
			}

		#endregion

		#region Helper Methods

		private void SetActiveNavButton(LinkButton lb)
		{
			btnHome.CssClass = "lnkBtnDock";
			btnPerformance.CssClass = "lnkBtnDock";
			btnClientStatistics.CssClass = "lnkBtnDock";
			btnBuildingStatistics.CssClass = "lnkBtnDock";
		
			lb.CssClass = "lnkBtnDockActive";
		}

		#endregion

		#region IClientPage

			Dictionary<String,String> IClientPage.Properties
			{
				get {return new Dictionary<String,String>{{"cid", siteUser.CID.ToString()}};}
			}

			Dictionary<String,String> IClientPage.AsyncProperties
			{
				get {return asyncProperties;}
			}

		#endregion
	}
}