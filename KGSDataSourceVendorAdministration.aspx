﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSDataSourceVendorAdministration.aspx.cs" Inherits="CW.Website.KGSDataSourceVendorAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                   	                  	
            	<h1>KGS Data Source Vendor Administration</h1>                        
                <div class="richText">The kgs data source vendor administration area is used to view, add, and edit data source vendors.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
                </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Data Source Vendors"></telerik:RadTab>
                            <telerik:RadTab Text="Add Data Source Vendor"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Data Source Vendors</h2> 
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>    
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>        
                                                                                                                
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridDataSourceVendors"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="VendorID"  
                                             GridLines="None"                                      
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridDataSourceVendors_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridDataSourceVendors_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridDataSourceVendors_Sorting"   
                                             OnSelectedIndexChanged="gridDataSourceVendors_OnSelectedIndexChanged"                                                                                                             
                                             OnRowDeleting="gridDataSourceVendors_Deleting"
                                             OnRowEditing="gridDataSourceVendors_Editing"
                                             OnDataBound="gridDataSourceVendors_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="VendorName" HeaderText="Vendor">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("VendorName"),30) %></ItemTemplate>       
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Phone">  
                                                    <ItemTemplate><%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorPhone"))) ? "" : Eval("VendorPhone")%></ItemTemplate>                                    
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Email">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("VendorEmail"),30) %></ItemTemplate>       
                                                </asp:TemplateField> 
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this data source vendor permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                                                          
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT DATA SOURCE VENDOR DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvDataSourceVendor" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>                                                    
                                                    <div>
                                                        <h2>Data Source Vendor Details</h2>                                                                                                                       
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Vendor: </strong>" + Eval("VendorName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorPhone"))) ? "" : "<li><strong>Phone: </strong>" + Eval("VendorPhone") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorEmail"))) ? "" : "<li><strong>Email: </strong>" + Eval("VendorEmail") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("VendorDescription") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorAddress"))) ? "" : "<li><strong>Address: </strong>" + Eval("VendorAddress") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorCity"))) ? "" : "<li><strong>City: </strong>" + Eval("VendorCity") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorStateName"))) ? "" : "<li><strong>State: </strong>" + Eval("VendorStateName") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorZip"))) ? "" : "<li><strong>Zip: </strong>" + Eval("VendorZip") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VendorCountryName"))) ? "" : "<li><strong>Country: </strong>" + Eval("VendorCountryName") + "</li>"%>                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>                                     
                                                                     
                                    <!--EDIT DATASOURCE VENDOR PANEL -->                              
                                    <asp:Panel ID="pnlEditDataSourceVendor" runat="server" Visible="false" DefaultButton="btnUpdateDataSourceVendor">                                                                                             
                                              <div>
                                                    <h2>Edit Data Source Vendor</h2>
                                              </div>  
                                              <div>                                                    
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*Vendor:</label>
                                                        <asp:TextBox ID="txtEditVendorName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Phone:</label>
                                                        <telerik:RadMaskedTextBox ID="txtEditVendorPhone" CssClass="textbox" runat="server"
                                                            Mask="(###)###-####" ValidationGroup="EditDataSourceVendor"></telerik:RadMaskedTextBox>                                                                                                                                                                 
                                                    </div>                                                                                                                                                                                           
                                                    <div class="divForm">
                                                         <label class="label">Email:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditVendorEmail" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>                                                                                                                                                                                                   
                                                    <div class="divForm">
                                                         <label class="label">Description:</label>
                                                         <textarea name="txtEditDescription" id="txtEditDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" runat="server"></textarea>
                                                         <div id="divEditDescriptionCharInfo"></div>                                                           
                                                    </div>            
                                                    <div class="divForm">
                                                        <label class="label">Address:</label>
                                                        <asp:TextBox ID="txtEditVendorAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                                                                              
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">City:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditVendorCity" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">Country:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlEditVendorCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditVendorCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                         <asp:ListItem Value="-1">Select one...</asp:ListItem>
                                                         </asp:DropDownList>
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">State:</label>
                                                        <asp:DropDownList ID="ddlEditVendorState" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                        <asp:ListItem Value="-1">Select country first...</asp:ListItem>
                                                        </asp:DropDownList>                                                                                                                                                              
                                                    </div>   
                                                    <div class="divForm">
                                                        <label class="label">Zip:</label>
                                                        <telerik:RadMaskedTextBox ID="txtEditVendorZip" CssClass="textbox" runat="server" 
                                                                Mask="aaaaaaaaaa" PromptChar="" ValidationGroup="EditDataSourceVendor"></telerik:RadMaskedTextBox>    
                                                    </div>                                                                                                                                                                                                                                      
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateDataSourceVendor" runat="server" Text="Update Vendor"  OnClick="updateDataSourceVendorButton_Click" ValidationGroup="EditDataSourceVendor"></asp:LinkButton>    
                                                </div>
                                                
                                                <!--Ajax Validators-->      
                                                <asp:RequiredFieldValidator ID="editDataSourceVendorNameRequiredValidator" runat="server"
                                                    ErrorMessage="Data Source Vendor Name is a required field."
                                                    ControlToValidate="txtEditVendorName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditDataSourceVendor">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editDataSourceVendorNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editDataSourceVendorNameRequiredValidatorExtender"
                                                    TargetControlID="editDataSourceVendorNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                                <asp:RegularExpressionValidator ID="editVendorPhoneRegExValidator" runat="server"
                                                        ErrorMessage="Invalid Phone Format."
                                                        ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                                        ControlToValidate="txtEditVendorPhone"
                                                        SetFocusOnError="true" 
                                                        Display="None" 
                                                        ValidationGroup="EditDataSourceVendor">
                                                        </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editVendorPhoneRegExValidatorExtender" runat="server"
                                                    BehaviorID="editVendorPhoneRegExValidatorExtender" 
                                                    TargetControlID="editVendorPhoneRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RegularExpressionValidator ID="editDataSourceVendorEmailRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Email Format."
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ControlToValidate="txtEditVendorEmail"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="EditDataSourceVendor">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editDataSourceVendorEmailRegExValidatorExtender" runat="server"
                                                    BehaviorID="editDataSourceVendorEmailRegExValidatorExtender" 
                                                    TargetControlID="editDataSourceVendorEmailRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                
                                                <asp:RegularExpressionValidator ID="editVendorZipRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Zip Format."
                                                    ValidationExpression="\d{5}"
                                                    ControlToValidate="txtEditVendorZip"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="EditDataSourceVendor"
                                                    Enabled="false">
                                                    </asp:RegularExpressionValidator>  
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editVendorZipRegExValidatorExtender" runat="server"
                                                    BehaviorID="editVendorZipRegExValidatorExtender"
                                                    TargetControlID="editVendorZipRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                                    </asp:Panel>
                                                                                                         
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server"> 
                                    <asp:Panel ID="pnlAddDataSourceVendor" runat="server" DefaultButton="btnAddDataSourceVendor"> 
                                              <h2>Add Data Source Vendor</h2> 
                                              <div>          
                                                    <a id="lnkSetFocusAdd" runat="server"></a>                                            
                                                    <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <div class="divForm">
                                                        <label class="label">*Vendor:</label>
                                                        <asp:TextBox ID="txtAddVendorName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Phone:</label>
                                                        <telerik:RadMaskedTextBox ID="txtAddVendorPhone" CssClass="textbox" runat="server"
                                                            Mask="(###)###-####" ValidationGroup="AddDataSourceVendor"></telerik:RadMaskedTextBox>                                                                                                        
                                                    </div>                                                                                                                                                                                           
                                                    <div class="divForm">
                                                         <label class="label">Email:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtAddVendorEmail" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>                                                    
                                                    <div class="divForm">
                                                         <label class="label">Description:</label>
                                                         <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                         <div id="divAddDescriptionCharInfo"></div>
                                                    </div>                                                                                                                                                                                                          
                                                    <div class="divForm">
                                                        <label class="label">Address:</label>
                                                        <asp:TextBox ID="txtAddVendorAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">City:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtAddVendorCity" runat="server" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">Country:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlAddVendorCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddVendorCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                         <asp:ListItem Value="-1">Select one...</asp:ListItem>
                                                         </asp:DropDownList>
                                                    </div>                                                                                                                                                                                         
                                                    <div class="divForm">
                                                         <label class="label">State:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlAddVendorState" AppendDataBoundItems="true" runat="server">
                                                         <asp:ListItem Value="-1">Select country first...</asp:ListItem>
                                                         </asp:DropDownList>
                                                    </div>       
                                                    <div class="divForm">
                                                        <label class="label">Zip:</label>
                                                        <telerik:RadMaskedTextBox ID="txtAddVendorZip" CssClass="textbox" runat="server" 
                                                            Mask="aaaaaaaaaa" PromptChar="" ValidationGroup="AddDataSourceVendor"></telerik:RadMaskedTextBox>
                                                    </div>                                                            
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnAddDataSourceVendor" runat="server" Text="Add Vendor"  OnClick="addDataSourceVendorButton_Click" ValidationGroup="AddDataSourceVendor"></asp:LinkButton>    
                                                </div>
                                                
                                                <!--Ajax Validators-->   
                                                <asp:RequiredFieldValidator ID="addDataSourceVendorNameRequiredValidator" runat="server"
                                                    ErrorMessage="Data Source Vendor Name is a required field."
                                                    ControlToValidate="txtAddVendorName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddDataSourceVendor">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addDataSourceVendorNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addDataSourceVendorNameRequiredValidatorExtender"
                                                    TargetControlID="addDataSourceVendorNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                                <asp:RegularExpressionValidator ID="addVendorPhoneRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Phone Format."
                                                    ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                                    ControlToValidate="txtAddVendorPhone"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="AddDataSourceVendor">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="AddVendorPhoneRegExValidatorExtender" runat="server"
                                                    BehaviorID="addVendorPhoneRegExValidatorExtender" 
                                                    TargetControlID="addVendorPhoneRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RegularExpressionValidator ID="addDataSourceVendorEmailRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Email Format."
                                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ControlToValidate="txtAddVendorEmail"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="AddDataSourceVendor">
                                                    </asp:RegularExpressionValidator>                         
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addDataSourceVendorEmailRegExValidatorExtender" runat="server"
                                                    BehaviorID="addDataSourceVendorEmailRegExValidatorExtender" 
                                                    TargetControlID="addDataSourceVendorEmailRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RegularExpressionValidator ID="addVendorZipRegExValidator" runat="server"
                                                    ErrorMessage="Invalid Zip Format."
                                                    ValidationExpression="\d{5}"
                                                    ControlToValidate="txtAddVendorZip"
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    ValidationGroup="AddDataSourceVendor"
                                                    Enabled="false">
                                                    </asp:RegularExpressionValidator> 
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addVendorZipRegExValidatorExtender" runat="server"
                                                    BehaviorID="addVendorZipRegExValidatorExtender"
                                                    TargetControlID="addVendorZipRegExValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                    </asp:Panel>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                            </telerik:RadPageView>                          
                        </telerik:RadMultiPage>
                   </div>                                                                 
               
</asp:Content>


                    
                  
