﻿<%@ Page Language="C#" EnableViewState="true" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="BureauReportv2.aspx.cs" Inherits="CW.Website.BureauReportv2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Utility.Web" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title id="title" runat="server">$product</title>   
    <style type="text/css">
		    @import url("/_assets/styles/themes/bureau2.css");
	</style>
</head>
<body id="body" runat="server">
    <form class="" id="form1" runat="server">
        
        <telerik:RadScriptManager ID="ScriptManager" EnableScriptCombine="true" EnableCdn="true" EnableScriptGlobalization="true" AsyncPostBackTimeout="600" runat="server" />
           
        <asp:UpdatePanel ID="updatePanel" ValidateRequestMode="Disabled" runat="server" UpdateMode="Conditional">
        <ContentTemplate>     

        <input type="hidden" runat="server" id="hdn_container" />
        <asp:HiddenField ID="hdnIsSETheme" runat="server" />
        <asp:HiddenField ID="hdnCID" runat="server" />
        <asp:HiddenField ID="hdnLanguageCultureName" runat="server" />
        <asp:HiddenField ID="hdnShowClientLogos" runat="server" />
        <asp:HiddenField ID="hdnIncludeTopDiagnosticsFigures" runat="server" />
        <asp:HiddenField ID="hdnIsScheduled" runat="server" />

        <asp:Panel ID="pnlError" runat="server" Visible="false">
            <asp:Label ID="lblError" CssClass="errorMessage"  runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlReport" runat="server">                 

        <div id="divCoverPage" runat="server">
        
            <div class="reportHeader">
                <div class="headerLeft">
                    <img id="headerImage1" src="" alt="magnify" runat="server" />
                </div>
                <div class="headerMiddle">
                    <div id="headerTitle1" class="headerTitle" runat="server"></div>
                    <div id="headerSecondaryTitle1" class="headerSecondaryTitle" runat="server"></div>
                    <div class="headerClientAndUserTitle">
                        <span id="headerClient1" runat="server"></span>
                        <span id="headerUserTitle1" runat="server"></span>
                    </div>
                </div>            
                <div class="headerRight">             
                    <div class="container">   
                        <div id="divPeriodHeader1" class="labelMediumBold" runat="server">:</div>                
                        <asp:Literal ID="litHeaderPeriod1" runat="server"></asp:Literal>     
                        <br />
                        <div id="divComparedTo1" class="labelMediumBold" runat="server" visible="false">:</div>                
                        <asp:Literal ID="litHeaderPreviousPeriod1" runat="server"></asp:Literal>                  
                    </div>
                </div>
            </div>
            <hr />
            
            <div class="section sectionCoverLogos">
                <img id="imgCoverThemed" alt="logo" src="" runat="server" />                
                <br /><br /><br /><br />
                <asp:HiddenField ID="hdnCoverClientByteArray" runat="server" />
                <telerik:RadBinaryImage ID="radCoverClient" runat="server" AlternateText="logo" />                
            </div>

            <div class="bottomPage">
                <asp:Literal ID="litTagline1" runat="server"></asp:Literal>
                <img id="imgBottomPage1" alt="logo" src="" runat="server" />
            </div>

            <div style="page-break-before: always"></div>
        </div>
        
        <div id="divContentsPage" runat="server">
        
            <div class="newPage">
                <h2 id="newPageTitle1" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient1" runat="server"></span>
                    <span id="newPageUserTitle1" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding1" src="" alt="magnify" runat="server" />
            </div>
            <hr />
            
            <div class="section section00" runat="server">
                <div id="divContentsPortfolioSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblPortfolioSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsExpertSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblExpertSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsBuildingSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblBuildingSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsEquipmentClassSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblEquipmentClassSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsBuildingTrendsPage" runat="server">
                    <label class="contentsLabel" id="lblBuildingTrendsPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsBuildingTopIssuesPage" runat="server">
                    <label class="contentsLabel" id="lblBuildingTopIssuesPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsVentilationSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblVentilationSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsHeatingSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblHeatingSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsCoolingSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblCoolingSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsPlantSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblPlantSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsZoneSummaryPage" runat="server">
                    <label class="contentsLabel" id="lblZoneSummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsSpecialtySummaryPage" runat="server">
                    <label class="contentsLabel" id="lblSpecialtySummaryPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsAlarms" runat="server">
                    <label class="contentsLabel" id="lblAlarms" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsProjects" runat="server">
                    <label class="contentsLabel" id="lblProjects" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsTasks" runat="server">
                    <label class="contentsLabel" id="lblTasks" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsTopDiagnostics" runat="server">
                    <label class="contentsLabel" id="lblTopDiagnostics" runat="server"></label>                  
                </div>
            </div>

            <div style="page-break-before: always"></div>

        </div>

        <div id="divPortfolioSummaryPage" runat="server">

            <div class="reportHeader">
                <div class="headerLeft">
                    <img id="headerImage2" src="" alt="magnify" runat="server" />
                </div>
                <div class="headerMiddle">
                    <div id="headerTitle2" class="headerTitle" runat="server"></div>
                    <div id="headerSecondaryTitle2" class="headerSecondaryTitle" runat="server"></div>
                    <div class="headerClientAndUserTitle">
                        <span id="headerClient2" runat="server"></span>
                        <span id="headerUserTitle2" runat="server"></span>
                    </div>
                </div>            
                <div class="headerRight" runat="server">             
                    <div class="container">   
                        <div id="divPeriodHeader2" class="labelMediumBold" runat="server">:</div>                
                        <asp:Literal ID="litHeaderPeriod2" runat="server"></asp:Literal>     
                        <br />
                        <div id="divComparedTo2" class="labelMediumBold" runat="server" visible="false">:</div>                
                        <asp:Literal ID="litHeaderPreviousPeriod2" runat="server"></asp:Literal>                  
                    </div>
                </div>
            </div>
            <hr />

            <div class="section section0">
                <div class="containerWithLeftHeader"> 
                    <div id="divFacilityTitle" class="containerLeftHeader" runat="server"></div>
                    <div class="containerTableFull">
                        <table class="containerTable">
                            <tr>
                                <td>
                                    <div class="buildingInfoFormat">
                                        <label id="lblFacilityCustomer" class="labelBoldColored" runat="server">:</label><br />
                                        <span id="clientName" runat="server"></span>
                                    </div>
                                </td>
                                <td>
                                    <div class="buildingInfoFormat">
                                        <label id="lblFacilityLocation" class="labelBoldColored" runat="server">:</label><br />
                                        <span id="buildingLocation" runat="server"></span>
                                    </div>
                                </td>
                                <td>
                                    <div class="buildingInfoFormat">
                                        <label id="lblFacilityYear" class="labelBoldColored" runat="server">:</label><br />
                                        <span id="buildingYear" runat="server"></span>
                                    </div>
                                </td>
                                <td>
                                    <div class="buildingInfoFormat">
                                        <label id="lblFacilityArea" class="labelBoldColored" runat="server">:</label><br />
                                        <span id="buildingArea" runat="server"></span>
                                    </div>  
                                </td>
                                <td>
                                    <div class="buildingInfoFormat">
                                        <label id="lblFacilityBuildingName" class="labelBoldColored" runat="server">:</label><br />
                                        <span id="buildingName" runat="server"></span>
                                    </div>  
                                </td>
                                <td>                                  
                                    <div class="buildingInfoFormat">
                                        <label id="lblFacilityBuildingType" class="labelBoldColored" runat="server">:</label><br />
                                        <span id="buildingType" runat="server"></span>
                                    </div>                    
                                </td>
                                <td>
                                    <div class="buildingInfoFormat">
                                        <label id="lblFacilityNumBuildings" class="labelBoldColored" runat="server">:</label><br />
                                        <span id="buildingCount" runat="server"></span>
                                    </div>    
                                </td>
                            </tr>
                        </table>
                    </div>                                 
                </div>
            </div>
            
            <div class="section section1">
                <div class="section1Left">
                    <div id="divUtilityContainer" runat="server">
                        <div class="containerWithHeader"> 
                            <div id="divTotalUtilityTitle" class="containerSingleLineHeader" runat="server"></div>
                            <div class="containerInner">
                                <table class="tableContainerLayout">
                                    <tr>
                                        <td class="tableThreeColumn40Left">
                                            <asp:Literal ID="litTotalUtilityCost"  runat="server"></asp:Literal>       
                                            <div id="divTotalUtilityTotalLabel1" class="labelLarge" runat="server"></div>                            
                                        </td>
                                        <td class="tableThreeColumn40Middle">  
                                            <asp:Literal ID="litTotalUtilityCostPercentage" runat="server"></asp:Literal>  
                                            <div id="divTotalUtilityCostChangeString" class="labelLarge" runat="server"></div>                                                                  
                                        </td>
                                        <td class="tableThreeColumn20Right">      
                                            <img id="imgUtilityCostsArrow" class="arrowIcon" src="" alt="arrow" runat="server" />                              
                                        </td>
                                    </tr>
                                </table>
                                <hr class="containerSpacerGray">
                                <table class="tableContainerLayout">
                                    <tr>
                                        <td class="tableThreeColumn40Left">
                                            <asp:Literal ID="litTotalUtilityConsumption"  runat="server"></asp:Literal>                                
                                            <div id="divTotalUtilityTotalLabel2" class="labelLarge" runat="server"></div>  
                                        </td>
                                        <td class="tableThreeColumn40Middle">  
                                            <asp:Literal ID="litTotalUtilityConsumptionPercentage" runat="server"></asp:Literal>   
                                            <div id="divTotaleUtilityConsumptionChangeString" class="labelLarge" runat="server"></div>                               
                                        </td>
                                        <td class="tableThreeColumn20Right">       
                                            <img id="imgUtilityConsumptionArrow" class="arrowIcon" src="" alt="arrow" runat="server" />                             
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <br />
                    </div>                

                    <div class="containerWithHeader"> 
                        <div id="divAvoidableUtilityTitle" class="containerSingleLineHeader" runat="server">                            
                        </div>
                        <div class="containerInner">
                            <table class="tableContainerLayout">
                                <tr>
                                    <td class="tableThreeColumn40Left">
                                        <asp:Literal ID="litAvoidableCostTotalValue"  runat="server"></asp:Literal>   
                                        <div id="divAvoidableUtilityTotalLabel1" class="labelLarge" runat="server"></div>                                                                                  
                                    </td>
                                    <td class="tableThreeColumn40Middle">  
                                        <asp:Literal ID="litAvoidableCostChangeValue" runat="server"></asp:Literal>
                                        <div id="divAvoidableCostChangeString" class="labelLarge" runat="server"></div>                                   
                                    </td>
                                    <td class="tableThreeColumn20Right">   
                                        <img id="imgAvoidableCostsArrow" class="arrowIcon" src="" alt="arrow" runat="server" />                                                                     
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                   
                </div>
                <div class="section1Right">
                    <div class="containerWithHeader"> 
                        <div id="divPeriodTrendSummaryTitle" class="containerSingleLineHeader" runat="server"></div>
                        <div class="containerInner">
                        <div>    
                            <img class="priorityIcon" src="_assets/styles/themes/customReportImages/cost.png" alt="cost" />                    
                            <img id="imgCostArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                            <div class="trendContent">
                                <span id="spanPeriodAvoidableCost" class="labelLargeBoldGray" runat="server"> -</span><br />
                                <span id="spanCostTrendSummary" class="labelLarge" runat="server"></span>                                
                            </div>
                        </div>
                        <hr class="containerSpacerGray" />
                        <div>
                            <img class="priorityIcon" src="_assets/styles/themes/customReportImages/energy.png" alt="energy" />
                            <img id="imgEnergyArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                            <div class="trendContent">
                                <span id="spanPeriodEnergy" class="labelLargeBoldGray" runat="server"> -</span><br />
                                <span id="spanEnergyTrendSummary" class="labelLarge" runat="server"></span>                                
                            </div>
                        </div>
                        <hr class="containerSpacerGray" />
                        <div>
                            <img class="priorityIcon" src="_assets/styles/themes/customReportImages/maintenance.png" alt="maintenance" />
                            <img id="imgMaintenanceArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                            <div class="trendContent">
                                <span id="spanPeriodMaintenance" class="labelLargeBoldGray" runat="server"> -</span><br />
                                <span id="spanMaintenanceTrendSummary" class="labelLarge" runat="server"></span>                                
                            </div>
                        </div>                    
                        <hr class="containerSpacerGray" />
                        <div>
                            <img class="priorityIcon" src="_assets/styles/themes/customReportImages/comfort.png" alt="comfort" />
                            <img id="imgComfortArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                            <div class="trendContent">
                                <span id="spanPeriodComfort" class="labelLargeBoldGray" runat="server"> -</span><br />
                                <span id="spanComfortTrendSummary" class="labelLarge" runat="server"></span>    
                            </div>                        
                        </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section section2">
                <div class="section2Full">
                    <h3 id="summaryHeader" runat="server"></h3>
                    <div class="richText">
                        <asp:ImageButton ID="btnAutomatedSummary" CssClass="editCircle editCircleGreyMarginTopSmall" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="automatedSummaryButton_Click" runat="server" />
                        <asp:Literal ID="litAutomatedSummary" runat="server"></asp:Literal>
                        <telerik:RadEditor ID="editorAutomatedSummary" 
                                            runat="server"                                                                                                                                                
                                            CssClass="editorNewLine"                                                        
                                            Height="280px" 
                                            Width="810px"
                                            MaxHtmlLength="10000"
                                            NewLineMode="Div"
                                            ToolsWidth="812px"                                                                                                                                       
                                            ToolbarMode="ShowOnFocus"        
                                            ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                                            Visible="false"
                                            EnableResize="false"
                                            > 
                                            <CssFiles>
                                                <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                            </CssFiles>                                                                                                                             
                                </telerik:RadEditor>  
                    </div>
                </div>
            </div>

            <div class="bottomPage">
                <asp:Literal ID="litTagline2" runat="server"></asp:Literal>
                <img id="imgBottomPage2" alt="logo" src="" runat="server" />
            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divExpertSummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle2" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient2" runat="server"></span>
                    <span id="newPageUserTitle2" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding2" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section section3">
                <div class="containerWithLeftHeader"> 
                    <div id="divPrepartedByTitle" class="containerLeftHeader" runat="server">:</div>
                    <div class="containerTableFull">
                        <table class="containerTable">
                            <tr>
                                <td>
                                    <div><strong id="preparedBy" runat="server"></strong></div>                            
                                </td>
                                <td>
                                    <div id="userAddress" runat="server"></div>                                                   
                                </td>
                                <td>
                                    <div id="userPhone" runat="server"></div>
                                    <div id="userEmail" runat="server"></div>
                                </td>
                           </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div id="divExpertSummary" class="section section4" runat="server">
                <h3 id="divExpertSummaryHeader" runat="server"></h3>
                <div class="section4Full richText">
                    <asp:ImageButton ID="btnExpertSummary" CssClass="editCircle editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="expertSummaryActionsButton_Click" runat="server" />
                    <asp:Literal ID="litExpertSummary" runat="server"></asp:Literal>               
                    <telerik:RadEditor ID="editorExpertSummary" 
                                runat="server" 
                                CssClass="editorNewLine"                                                  
                                Height="790px" 
                                Width="810px"
                                MaxHtmlLength="50000"
                                NewLineMode="Div"
                                ToolsWidth="812px"                                                                                                                                       
                                ToolbarMode="ShowOnFocus"        
                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                                Visible="false"
                                EnableResize="false"
                                >   
                                <CssFiles>
                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                </CssFiles>                                                                                
                    </telerik:RadEditor> 
                </div>
            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divBuildingSummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle3" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient3" runat="server"></span>
                    <span id="newPageUserTitle3" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding3" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section section5" runat="server">
                <div class="section5Pie" style="page-break-inside: avoid">
                    <asp:Chart ID="buildingSummaryPieChart" CssClass="trendChart" runat="server" ImageType="Png" EnableViewState="true" Width="836px" Height="252px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="buildingSummaryPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>                        
                    </asp:Chart>                    
                    <%--<telerik:RadHtmlChart ID="buildingSummaryPieChart2" runat="server" Height="280px" Width="814px">
                        <PlotArea>
                            <Series>
                                <telerik:PieSeries DataFieldY="TotalCostSavings" NameField="BuildingName">
                                    <LabelsAppearance Visible="false" />                            
                                    <TooltipsAppearance DataFormatString="{0}" />
                                </telerik:PieSeries>
                            </Series>
                        </PlotArea>
                        <ChartTitle Text="Buildings With Avoidable Cost">
                        </ChartTitle>
                        <Legend><Appearance Position="Right"></Appearance></Legend>
                    </telerik:RadHtmlChart>--%>
                </div>

                <div class="section5Column" style="page-break-inside: avoid">
                    
                    <asp:Chart ID="buildingSummaryStackedColumnChart" CssClass="trendChart" runat="server" ImageType="Png" EnableViewState="true" Width="836px" Height="252px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Name="buildingSummaryStackedColumnChartArea" BackColor="White">
                                <AxisY Maximum="100" LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX Interval="1" IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="true">
                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>  
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>

                </div>

                <div id="gridTbl" class="gridBuildingSummary">
                    <div class="tblPreHeader">                    
                        <span id="spanBuildingSummary" runat="server"></span>
                        <asp:ImageButton ID="btnBuildingSummary" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="buildingSummaryGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridBuildingSummary"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"     
                        ShowFooter="true"                                                                                                                                            
                        >
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox ID="TextBox1" runat="server" Width="100px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# Eval("EquipmentCount")%>'></asp:Label>
                                    <asp:TextBox ID="TextBox2" runat="server" Width="40px" Visible="false" Text='<%# Eval("EquipmentCount")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalEquipmentCount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>                         
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentWithFaultsCount")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("EquipmentWithFaultsCount")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalEquipmentWithFaultsCount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox>                                     
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("TotalCostSavingsPerDay")), (double?)Eval("PreviousTotalCostSavingsPerDay")) %>
                                </ItemTemplate>  
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalCostSavingsCultureFormated" runat="server"></asp:Label>
                                </FooterTemplate>                          
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PercentEquipmentWithEnergyFaults").ToString() + "%" %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("PercentEquipmentWithEnergyFaults").ToString() + "%" %>' MaxLength="4"></asp:TextBox>                                     
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("PercentEquipmentWithEnergyFaults")), (double?)Eval("PreviousPercentEquipmentWithEnergyFaults")) %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PercentEquipmentWithMaintenanceFaults").ToString() + "%" %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("PercentEquipmentWithMaintenanceFaults").ToString() + "%" %>' MaxLength="4"></asp:TextBox>
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("PercentEquipmentWithMaintenanceFaults")), (double?)Eval("PreviousPercentEquipmentWithMaintenanceFaults")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PercentEquipmentWithComfortFaults").ToString()  %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("PercentEquipmentWithComfortFaults").ToString() + "%" %>' MaxLength="4"></asp:TextBox>                                     
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("PercentEquipmentWithComfortFaults")), (double?)Eval("PreviousPercentEquipmentWithComfortFaults")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>     
                    </asp:GridView>
                </div>           
            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divEquipmentClassSummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle4" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient4" runat="server"></span>
                    <span id="newPageUserTitle4" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding4" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section section6" runat="server">
                <div class="section6Pie" style="page-break-inside: avoid">
                    <asp:Chart ID="equipmentClassSummaryPieChart" CssClass="trendChart" runat="server" ImageType="Png" EnableViewState="true" Width="836px" Height="252px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="equipmentClassSummaryPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>                    
                    <%--<telerik:RadHtmlChart ID="equipmentClassSummaryPieChart2" runat="server" Height="280px" Width="406px">
                        <PlotArea>                           
                            <Series>
                                <telerik:PieSeries DataFieldY="TotalCostSavings" NameField="EquipmentClassName">
                                    <LabelsAppearance Visible="false" />
                                    <TooltipsAppearance DataFormatString="{0}" />
                                </telerik:PieSeries>
                            </Series>
                        </PlotArea>
                        <ChartTitle Text="Equipment Class Total Cost">
                        </ChartTitle>
                    </telerik:RadHtmlChart>--%>
                </div>

                <div class="section6Column">
                    <asp:Chart ID="equipmentClassSummaryStackedColumnChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="836px" Height="252px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Name="equipmentClassSummaryStackedColumnChartArea" BackColor="White">           
                                <AxisY Maximum="100" LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX Interval="1" IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="true">
                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>             

                <div id="gridTbl" class="gridEquipmentClassSummary">
                    <div class="tblPreHeader">                    
                        <span id="spanEquipmentClassSummary" runat="server"></span>
                        <asp:ImageButton ID="btnEquipmentClassSummary" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="equipmentClassSummaryGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridEquipmentClassSummary"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"   
                        ShowFooter="true"                                                                                                                                                     
                        > 
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentClassColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentClassName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("EquipmentClassName")%>' MaxLength="50"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentCount")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("EquipmentCount")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalEquipmentCount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>                         
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentWithFaultsCount")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("EquipmentWithFaultsCount")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalEquipmentWithFaultsCount" runat="server"></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("TotalCostSavingsPerDay")), (double?)Eval("PreviousTotalCostSavingsPerDay")) %>
                                </ItemTemplate> 
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalCostSavingsCultureFormated" runat="server"></asp:Label>
                                </FooterTemplate>                           
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PercentEquipmentWithEnergyFaults").ToString() + "%" %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("PercentEquipmentWithEnergyFaults").ToString() + "%" %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("PercentEquipmentWithEnergyFaults")), (double?)Eval("PreviousPercentEquipmentWithEnergyFaults")) %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PercentEquipmentWithMaintenanceFaults").ToString() + "%" %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("PercentEquipmentWithMaintenanceFaults").ToString() + "%" %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("PercentEquipmentWithMaintenanceFaults")), (double?)Eval("PreviousPercentEquipmentWithMaintenanceFaults")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PercentEquipmentWithComfortFaults").ToString() + "%" %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("PercentEquipmentWithComfortFaults").ToString() + "%" %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("PercentEquipmentWithComfortFaults")), (double?)Eval("PreviousPercentEquipmentWithComfortFaults")) %>
                                </ItemTemplate>
                            </asp:TemplateField>   
                        </Columns>        
                    </asp:GridView>
                </div>
            </div>

            <div style="page-break-before: always"></div>
        </div>        

        <div id="divBuildingTrendsPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle5" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient5" runat="server"></span>
                    <span id="newPageUserTitle5" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding5" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section section7">
                <h3 id="totalEnergyTrendsHeader" runat="server"></h3>
                <div class="section7Left">            
                <asp:Chart ID="energyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  OnPrePaint="ChartEnergy_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                              
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="section7Right">
                    <asp:Chart ID="energyPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="energyPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
                <div id="divEnergyTrendAnalysis" class="section7Bottom richText" runat="server">
                    <!--<h3>Energy Cost Trend Analysis</h3>--> 
                    <asp:ImageButton ID="btnEnergyTrend" CssClass="editCircle editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="energyTrendButton_Click" runat="server" />
                    <asp:Literal ID="litEnergy" runat="server"></asp:Literal>
                    <telerik:RadEditor ID="editorEnergyTrend"
                                runat="server" 
                                CssClass="editorNewLine"                                                  
                                Height="80px" 
                                Width="810px"
                                MaxHtmlLength="500"
                                NewLineMode="Div"
                                ToolsWidth="812px"                                                                                                                                       
                                ToolbarMode="ShowOnFocus"        
                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" 
                                Visible="false"
                                EnableResize="false"                                               
                                >   
                                <CssFiles>
                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                </CssFiles>                                                                                
                    </telerik:RadEditor>
                </div>
            </div>
            <hr class="hrDotted" />
            <div class="section section8">
                <h3 id="totalMaintenanceTrendsHeader" runat="server"></h3> 
                <div class="section8Left">
                <asp:Chart ID="maintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                            
                    <Series></Series>                
                    <ChartAreas>
                        <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>                        
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>                    
                <div class="section8Right">
                    <asp:Chart ID="maintenancePeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="maintenancePeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
                <div id="divMaintenanceTrendAnalysis" class="section8Bottom richText" runat="server">
                    <!--<h3>Maintenance Trend Analysis</h3>-->
                    <asp:ImageButton ID="btnMaintenanceTrend" CssClass="editCircle editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="maintenanceTrendButton_Click" runat="server" /> 
                    <asp:Literal ID="litMaintenance" runat="server"></asp:Literal> 
                    <telerik:RadEditor ID="editorMaintenanceTrend" 
                                runat="server" 
                                CssClass="editorNewLine"                                                  
                                Height="80px" 
                                Width="810px"
                                MaxHtmlLength="500"
                                NewLineMode="Div"
                                ToolsWidth="812px"                                                                                                                                       
                                ToolbarMode="ShowOnFocus"        
                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" 
                                Visible="false"
                                EnableResize="false"                                               
                                > 
                                <CssFiles>
                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                </CssFiles>                                                                                  
                    </telerik:RadEditor>               
                </div>
            </div>
            <hr class="hrDotted" />
            <div class="section section9">
                <h3 id="totalComfortTrendsHeader" runat="server"></h3>
                <div class="section9Left">
                <asp:Chart ID="comfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                             
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title=""  TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" />
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="section9Right">
                    <asp:Chart ID="comfortPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="comfortPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
                <div id="divComfortTrendAnalysis" class="section9Bottom richText" runat="server">
                    <!--<h3>Comfort Trend Analysis</h3> -->
                    <asp:ImageButton ID="btnComfortTrend" CssClass="editCircle editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="comfortTrendButton_Click" runat="server" />
                    <asp:Literal ID="litComfort" runat="server"></asp:Literal>
                    <telerik:RadEditor ID="editorComfortTrend" 
                                runat="server" 
                                CssClass="editorNewLine"                                                  
                                Height="80px" 
                                Width="810px"
                                MaxHtmlLength="500"
                                NewLineMode="Div"
                                ToolsWidth="812px"                                                                                                                                       
                                ToolbarMode="ShowOnFocus"        
                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                                Visible="false"
                                EnableResize="false"                                                
                                >   
                                <CssFiles>
                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                </CssFiles>                                                                                
                    </telerik:RadEditor>
                </div>
            </div>

            <div style="page-break-before: always"></div>
       </div>

        <div id="divBuildingTopIssuesPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle6" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient6" runat="server"></span>
                    <span id="newPageUserTitle6" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding6" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section section10">
                <h3 id="topDiagnosticIssuesHeader" runat="server"></h3>

                <div id="gridTbl" class="gridEnergy"> 
                    <div class="tblPreHeader">
                        <img src="_assets/styles/themes/customReportImages/energy-white.png" alt="energy" />
                        <span id="spanTopEnergyIssuesTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnTopEnergy" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="energyGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridEnergy"   
                        EnableViewState="true"           
                        runat="server"                                                                                                                                 
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"   
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                 
                        >                                      
                        <Columns>                        
                             <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">                            
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>                                
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="notesColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 1500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="440px" Visible="false" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 1500)%>' MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>                            
                            </asp:TemplateField>   
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="actionsColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server"></asp:Label>
                                    <asp:TextBox runat="server" Width="440px" Visible="false" MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>                            
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="E" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="M" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="C" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>        
                    </asp:GridView>
                </div>

                <div id="gridTbl" class="gridMaintenance">
                    <div class="tblPreHeader">
                        <img src="_assets/styles/themes/customReportImages/maintenance-white.png" alt="maintenance" />
                        <span id="spanTopMaintenanceIssuesTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnTopMaintenance" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="maintenanceGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridMaintenance"   
                        EnableViewState="true"           
                        runat="server"                                                                                                             
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                                      
                        > 
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                         
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="notesColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 1500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="440px" Visible="false" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 1500)%>' MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="actionsColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server"></asp:Label>
                                    <asp:TextBox runat="server" Width="440px" Visible="false" MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>                            
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="E" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="M" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="C" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>        
                    </asp:GridView>
                </div>
   
                <div id="gridTbl" class="gridComfort">
                    <div class="tblPreHeader">
                        <img src="_assets/styles/themes/customReportImages/comfort-white.png" alt="comfort" />
                        <span id="spanTopComfortIssuesTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnTopComfort" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="comfortGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridComfort"   
                        EnableViewState="true"           
                        runat="server"                                                                                                             
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"      
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                   
                        > 
                        <Columns>
                             <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="notesColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 1500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="440px" Visible="false" Text='<%# StringHelper.TrimText(Eval("NotesSummary"), 1500)%>' MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="actionsColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server"></asp:Label>
                                    <asp:TextBox runat="server" Width="440px" Visible="false" MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>                            
                            </asp:TemplateField>                                            
                            <asp:TemplateField HeaderText="E" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="M" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="C" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences") %>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                 
                        </Columns>        
                    </asp:GridView>
                </div>
            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divVentilationSummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle7" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient7" runat="server"></span>
                    <span id="newPageUserTitle7" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding7" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section sectionEquipmentPie" style="page-break-inside: avoid" runat="server">
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="ventilationSummaryAirHandlersPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="ventilationSummaryAirHandlersPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="ventilationSummaryFansPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                                                    
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="ventilationSummaryFansPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="ventilationSummaryVentilationSystemsPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="ventilationSummaryVentilationSystemsPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
            </div>

            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="ventilationEnergyHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">            
                <asp:Chart ID="ventilationEnergyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  OnPrePaint="ChartVentilationEnergy_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                              
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="ventilationEnergyPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ventilationEnergyPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="ventilationMaintenanceHeader" runat="server"></h3> 
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="ventilationMaintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartVentilationMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                            
                    <Series></Series>                
                    <ChartAreas>
                        <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>                        
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>                    
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="ventilationMaintenancePeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ventilationMaintenancePeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="ventilationComfortHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="ventilationComfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartVentilationComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                             
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title=""  TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" />
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="ventilationComfortPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ventilationComfortPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>

            <div style="page-break-before: always"></div>

            <div id="gridTbl" class="gridEquipmentSummary">
                    <div class="tblPreHeader">                    
                        <span id="spanVentilationTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnVentilationSummary" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="ventilationSummaryGridEditButton_Click"  runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridVentilationSummary"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"        
                        ShowFooter="true"                                                                                                                                              
                        > 
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="analysisColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AnalysisName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("AnalysisName")%>' MaxLength="150"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                    
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("TotalCostSavingsPerDay")), (double?)Eval("PreviousTotalCostSavingsPerDay")) %>
                                </ItemTemplate>    
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalCostSavingsCultureFormated" runat="server"></asp:Label>
                                </FooterTemplate>                         
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageEnergyPriority")), (double?)Eval("PreviousAverageEnergyPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label ID="Label7" runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox ID="TextBox7" runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageMaintenancePriority")), (double?)Eval("PreviousAverageMaintenancePriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageComfortPriority")), (double?)Eval("PreviousAverageComfortPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>   
                        </Columns>        
                    </asp:GridView>
                </div>

             <div style="page-break-before: always"></div>
        </div>
        
        <div id="divHeatingSummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle8" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient8" runat="server"></span>
                    <span id="newPageUserTitle8" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding8" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section sectionEquipmentPie" style="page-break-inside: avoid" runat="server">
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="heatingSummaryBoilersPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                                                    
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="heatingSummaryBoilersPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
		        <div class="sectionEquipmentPieChart">                    
                    <asp:Chart ID="heatingSummaryHeatingSystemsPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="ventilationSummaryVentilationSystemsPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
            </div>

            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="heatingEnergyHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">            
                <asp:Chart ID="heatingEnergyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  OnPrePaint="ChartHeatingEnergy_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                              
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="heatingEnergyPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="heatingEnergyPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="heatingMaintenanceHeader" runat="server"></h3> 
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="heatingMaintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartHeatingMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                            
                    <Series></Series>                
                    <ChartAreas>
                        <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>                        
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>                    
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="heatingMaintenancePeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="heatingMaintenancePeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="heatingComfortHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="heatingComfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartHeatingComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                             
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title=""  TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" />
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="heatingComfortPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="heatingComfortPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>

            <div style="page-break-before: always"></div>

            <div id="gridTbl" class="gridEquipmentSummary">
                    <div class="tblPreHeader">                    
                        <span id="spanHeatingTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnHeatingSummary" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="heatingSummaryGridEditButton_Click"  runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridHeatingSummary"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"       
                        ShowFooter="true"                                                                                                                                               
                        > 
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="analysisColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AnalysisName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("AnalysisName")%>' MaxLength="150"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                    
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("TotalCostSavingsPerDay")), (double?)Eval("PreviousTotalCostSavingsPerDay")) %>
                                </ItemTemplate> 
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalCostSavingsCultureFormated" runat="server"></asp:Label>
                                </FooterTemplate>                            
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageEnergyPriority")), (double?)Eval("PreviousAverageEnergyPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageMaintenancePriority")), (double?)Eval("PreviousAverageMaintenancePriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageComfortPriority")), (double?)Eval("PreviousAverageComfortPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>   
                        </Columns>        
                    </asp:GridView>
                </div>

             <div style="page-break-before: always"></div>
        </div>

        <div id="divCoolingSummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle9" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient9" runat="server"></span>
                    <span id="newPageUserTitle9" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding9" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section sectionEquipmentPie" style="page-break-inside: avoid" runat="server">
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="coolingSummaryChillersPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="coolingSummaryChillersPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="coolingSummaryCoolingSystemsPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                                                    
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="coolingSummaryCoolingSystemsPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="coolingSummaryHeatRejectionPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="coolingSummaryHeatRejectionPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
            </div>

            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="coolingEnergyHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">            
                <asp:Chart ID="coolingEnergyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  OnPrePaint="ChartCoolingEnergy_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                              
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="coolingEnergyPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="coolingEnergyPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="coolingMaintenanceHeader" runat="server"></h3> 
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="coolingMaintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartCoolingMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                            
                    <Series></Series>                
                    <ChartAreas>
                        <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>                        
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>                    
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="coolingMaintenancePeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="coolingMaintenancePeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="coolingComfortHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="coolingComfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartCoolingComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                             
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title=""  TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" />
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="coolingComfortPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="coolingComfortPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>

            <div style="page-break-before: always"></div>

            <div id="gridTbl" class="gridEquipmentSummary">
                    <div class="tblPreHeader">                    
                        <span id="spanCoolingTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnCoolingSummary" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="coolingSummaryGridEditButton_Click"  runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridCoolingSummary"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"      
                        ShowFooter="true"                                                                                                                                                
                        > 
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="analysisColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AnalysisName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("AnalysisName")%>' MaxLength="150"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                    
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("TotalCostSavingsPerDay")), (double?)Eval("PreviousTotalCostSavingsPerDay")) %>
                                </ItemTemplate> 
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalCostSavingsCultureFormated" runat="server"></asp:Label>
                                </FooterTemplate>                            
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageEnergyPriority")), (double?)Eval("PreviousAverageEnergyPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageMaintenancePriority")), (double?)Eval("PreviousAverageMaintenancePriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageComfortPriority")), (double?)Eval("PreviousAverageComfortPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>   
                        </Columns>        
                    </asp:GridView>
                </div>

             <div style="page-break-before: always"></div>
        </div>

        <div id="divPlantSummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle10" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient10" runat="server"></span>
                    <span id="newPageUserTitle10" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding10" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section sectionEquipmentPie" style="page-break-inside: avoid" runat="server">
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="plantSummaryDomesticWaterSystemsPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="plantSummaryDomesticWaterSystemsPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="plantSummaryDualTemperatureWaterSystemsPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                                                    
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="plantSummaryDualTemperatureWaterSystemsPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="plantSummaryHeatExchangerPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="plantSummaryHeatExchangerPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="plantSummaryHeatRecoverySystemsPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="plantSummaryHeatRecoverySystemsPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="plantSummaryPumpsPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                                                    
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="plantSummaryPumpsPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="plantSummaryStoragePieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="plantSummaryStoragePieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
            </div>
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="plantEnergyHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">            
                <asp:Chart ID="plantEnergyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  OnPrePaint="ChartPlantEnergy_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                              
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="plantEnergyPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="plantEnergyPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="plantMaintenanceHeader" runat="server"></h3> 
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="plantMaintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartPlantMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                            
                    <Series></Series>                
                    <ChartAreas>
                        <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>                        
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>                    
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="plantMaintenancePeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="plantMaintenancePeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="plantComfortHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="plantComfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartPlantComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                             
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title=""  TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" />
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="plantComfortPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="plantComfortPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>

            <div style="page-break-before: always"></div>

            <div id="gridTbl" class="gridEquipmentSummary">
                    <div class="tblPreHeader">                    
                        <span id="spanPlantTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnPlantSummary" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="plantSummaryGridEditButton_Click"  runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridPlantSummary"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"  
                        ShowFooter="true"                                                                                                                                                    
                        > 
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="analysisColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AnalysisName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("AnalysisName")%>' MaxLength="150"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                    
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("TotalCostSavingsPerDay")), (double?)Eval("PreviousTotalCostSavingsPerDay")) %>
                                </ItemTemplate>    
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalCostSavingsCultureFormated" runat="server"></asp:Label>
                                </FooterTemplate>                         
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageEnergyPriority")), (double?)Eval("PreviousAverageEnergyPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageMaintenancePriority")),(double?)Eval("PreviousAverageMaintenancePriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox>
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageComfortPriority")), (double?)Eval("PreviousAverageComfortPriority")) %> 
                                </ItemTemplate>
                            </asp:TemplateField>   
                        </Columns>        
                    </asp:GridView>
                </div>

             <div style="page-break-before: always"></div>
        </div>

        <div id="divZoneSummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle11" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient11" runat="server"></span>
                    <span id="newPageUserTitle11" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding11" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section sectionEquipmentPie" style="page-break-inside: avoid" runat="server">
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="zoneSummaryZoneEquipmentPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="zoneSummaryZoneEquipmentPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="zoneSummaryZoneGroupsPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                                                    
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="zoneSummaryZoneGroupsPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
                        <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>
            </div>
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="zoneEnergyHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">            
                <asp:Chart ID="zoneEnergyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  OnPrePaint="ChartZoneEnergy_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                              
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="zoneEnergyPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="zoneEnergyPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="zoneMaintenanceHeader" runat="server"></h3> 
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="zoneMaintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartZoneMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                            
                    <Series></Series>                
                    <ChartAreas>
                        <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>                        
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>                    
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="zoneMaintenancePeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="zoneMaintenancePeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="zoneComfortHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="zoneComfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartZoneComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                             
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title=""  TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" />
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="zoneComfortPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="zoneComfortPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>

            <div style="page-break-before: always"></div>

            <div id="gridTbl" class="gridEquipmentSummary">
                    <div class="tblPreHeader">                    
                        <span id="spanZoneTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnZoneSummary" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="zoneSummaryGridEditButton_Click"  runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridZoneSummary"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"  
                        ShowFooter="true"                                                                                                                                                    
                        > 
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="analysisColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AnalysisName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("AnalysisName")%>' MaxLength="150"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                    
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("TotalCostSavingsPerDay")), (double?)Eval("PreviousTotalCostSavingsPerDay")) %>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalCostSavingsCultureFormated" runat="server"></asp:Label>
                                </FooterTemplate>                           
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageEnergyPriority")), (double?)Eval("PreviousAverageEnergyPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageMaintenancePriority")), (double?)Eval("PreviousAverageMaintenancePriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageComfortPriority")), (double?)Eval("PreviousAverageComfortPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>   
                        </Columns>        
                    </asp:GridView>
                </div>

             <div style="page-break-before: always"></div>
        </div>

        <div id="divSpecialtySummaryPage" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle12" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient12" runat="server"></span>
                    <span id="newPageUserTitle12" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding12" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="section sectionEquipmentPie" style="page-break-inside: avoid" runat="server">
                <div class="sectionEquipmentPieChart">
                    <asp:Chart ID="specialtySummarySpecialtyEquipmentPieChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Height="156px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Area3DStyle-Enable3D="true" Name="specialtySummarySpecialtyEquipmentPieChartArea" BackColor="White">                                                                                 
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Right" Enabled="false" BorderColor="#999999"></asp:Legend>
                        </Legends>
			            <Titles>
                            <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                        </Titles>
                    </asp:Chart>
                </div>              
            </div>
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="specialtyEnergyHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">            
                <asp:Chart ID="specialtyEnergyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  OnPrePaint="ChartSpecialtyEnergy_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                              
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="specialtyEnergyPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="specialtyEnergyPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="specialtyMaintenanceHeader" runat="server"></h3> 
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="specialtyMaintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartSpecialtyMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                            
                    <Series></Series>                
                    <ChartAreas>
                        <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>                        
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>                    
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="specialtyMaintenancePeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="specialtyMaintenancePeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>
            <hr class="hrDotted" />
            <div class="section sectionEquipmentChart" style="page-break-inside: avoid">
                <h3 id="specialtyComfortHeader" runat="server"></h3>
                <div class="sectionEquipmentChartLeft">
                <asp:Chart ID="specialtyComfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartSpecialtyComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="510px" Height="252px">                             
                    <Series></Series>
                    <ChartAreas>
                        <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                            <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY>
                            <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title=""  TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid Enabled="false" /> 
                                <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                            </AxisY2>
                            <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                <MajorGrid LineColor="#CCCCCC" Enabled="true" />
                                <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                            </AxisX>
                        </asp:ChartArea>
                    </ChartAreas>
                    <Legends>
                        <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                        <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                    </Legends>
                </asp:Chart>
                </div>
                <div class="sectionEquipmentChartRight">
                    <asp:Chart ID="specialtyComfortPeriodChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler"  runat="server" ImageType="Png" EnableViewState="true" Width="342px" Height="252px">                              
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="specialtyComfortPeriodChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>                            
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                    </asp:Chart>
                </div>                
            </div>

            <div style="page-break-before: always"></div>

            <div id="gridTbl" class="gridEquipmentSummary">
                    <div class="tblPreHeader">                    
                        <span id="spanSpecialtyTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnSpecialtySummary" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="specialtySummaryGridEditButton_Click"  runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridSpecialtySummary"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"  
                        ShowFooter="true"                                                                                                                                                    
                        > 
                        <Columns>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="buildingColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("BuildingName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="equipmentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("EquipmentName")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="analysisColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AnalysisName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("AnalysisName")%>' MaxLength="150"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Occurrences")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("Occurrences")%>' MaxLength="4"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                    
                            <asp:TemplateField HeaderText="" FooterStyle-CssClass="tblFooter gridAlignCenter numberColumnWidth" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("TotalCostSavingsCultureFormated") %>'></asp:Label>
                                    <asp:TextBox  runat="server" Width="60px" Visible="false" Text='<%# Eval("TotalCostSavingsCultureFormated") %>' MaxLength="10"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("TotalCostSavingsPerDay")), (double?)Eval("PreviousTotalCostSavingsPerDay")) %>
                                </ItemTemplate>  
                                <FooterTemplate>
                                    <asp:Label ID="lblGridTotalCostSavingsCultureFormated" runat="server"></asp:Label>
                                </FooterTemplate>                           
                            </asp:TemplateField>                                                        
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageEnergyPriority") %>'></asp:Label>
                                    <asp:TextBox  runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageEnergyPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageEnergyPriority")), (double?)Eval("PreviousAverageEnergyPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageMaintenancePriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageMaintenancePriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageMaintenancePriority")), (double?)Eval("PreviousAverageMaintenancePriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("AverageComfortPriority") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="40px" Visible="false" Text='<%# Eval("AverageComfortPriority") %>' MaxLength="4"></asp:TextBox> 
                                    <%# MathHelper.ComparerDoublesToAsciiArrows(Convert.ToDouble(Eval("AverageComfortPriority")), (double?)Eval("PreviousAverageComfortPriority")) %>
                                </ItemTemplate>
                            </asp:TemplateField>   
                        </Columns>        
                    </asp:GridView>
                </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divAlarms" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle13" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient13" runat="server"></span>
                    <span id="newPageUserTitle13" runat="server"></span>
                </div>                
                <div class="newPageRight" runat="server">             
                    <div class="container">   
                        <div id="divPeriodHeader3" class="labelMediumBold" runat="server">:</div>                
                        <asp:Literal ID="litHeaderPeriodAlarm" runat="server"></asp:Literal>                      
                    </div>
                </div>
            </div>
            <hr />

            <div class="sectionAlarms">
                <div id="gridTbl" class="gridAlarms">
                    <div class="tblPreHeader">                    
                        <span id="spanTopMostFrequentAlarmsTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnFrequentAlarms" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="frequentAlarmsGridEditButton_Click"  runat="server" />
                    </div>                                 
                    <asp:GridView 
                            ID="gridTopTenMostFrequentAlarms"  
                            EnableViewState="true"           
                            runat="server"                                                                                                          
                            AutoGenerateColumns="false"                                              
                            HeaderStyle-CssClass="tblTitle" 
                            RowStyle-CssClass="tblCol1"
                            AlternatingRowStyle-CssClass="tblCol2"        
                            ShowHeaderWhenEmpty="true"
                            EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                                    
                            > 
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PointName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("PointName")%>' MaxLength="50"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("EquipmentName") %>' MaxLength="50"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField> 
				                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("BuildingName") %>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="notesColumnWidth">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text=''></asp:Label>
                                        <asp:TextBox runat="server" Width="440px" Visible="false" Text='' MaxLength="1500"></asp:TextBox> 
                                    </ItemTemplate>                            
                                </asp:TemplateField>                                          
                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("NumOfOccurrences") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("NumOfOccurrences") %>' MaxLength="15"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField>                                                                                                                                                                                                          
                            </Columns>        
                    </asp:GridView> 
                </div>
            </div>

            <div class="sectionAlarms">
                <div id="gridTbl" class="gridAlarms">
                    <div class="tblPreHeader">                    
                        <span id="spanTopTotalTimeInAlarmTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnTotalAlarms" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="totalAlarmsGridEditButton_Click"  runat="server" />
                    </div>                                 
                    <asp:GridView 
                            ID="gridTopTenTotalTimeInAlarm"  
                            EnableViewState="true"           
                            runat="server"                                                                                                          
                            AutoGenerateColumns="false"                                              
                            HeaderStyle-CssClass="tblTitle" 
                            RowStyle-CssClass="tblCol1"
                            AlternatingRowStyle-CssClass="tblCol2"        
                            ShowHeaderWhenEmpty="true"
                            EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                                    
                            > 
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("PointName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("PointName")%>' MaxLength="50"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("EquipmentName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("EquipmentName") %>' MaxLength="50"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField> 
				                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("BuildingName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# Eval("BuildingName") %>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="notesColumnWidth">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text=''></asp:Label>
                                        <asp:TextBox runat="server" Width="440px" Visible="false" Text='' MaxLength="1500"></asp:TextBox> 
                                    </ItemTemplate>                            
                                </asp:TemplateField>                                          
                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("SelectedRangeTimeInAlarm") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("SelectedRangeTimeInAlarm") %>' MaxLength="20"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField>                                                                                                                                                                                                          
                            </Columns>        
                    </asp:GridView> 
                </div>
            </div>


            <div style="page-break-before: always"></div>
            
        </div>
           
        <div id="divProjects" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle14" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient14" runat="server"></span>
                    <span id="newPageUserTitle14" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding14" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="sectionProjects">
                <div id="gridTbl" class="gridProjects">
                    <div class="tblPreHeader">                    
                        <span id="spanTopOpenProjectsTableHeader" runat="server"></span>
                        <asp:ImageButton ID="btnProjects" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="projectsGridEditButton_Click"  runat="server" />
                    </div>                                 
                    <asp:GridView 
                            ID="gridTopOpenProjects"  
                            EnableViewState="true"           
                            runat="server"                                                                                                           
                            AutoGenerateColumns="false"                                              
                            HeaderStyle-CssClass="tblTitle" 
                            RowStyle-CssClass="tblCol1"
                            AlternatingRowStyle-CssClass="tblCol2"        
                            ShowHeaderWhenEmpty="true"
                            EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                                      
                            > 
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("ProjectName") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("ProjectName")%>' MaxLength="50"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(String.Join(", ",((string[])Eval("AnalysisEquipmentNameArray"))),497) %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="140px" Visible="false" Text='<%# StringHelper.TrimText(String.Join(", ",((string[])Eval("AnalysisEquipmentNameArray"))),497) %>' MaxLength="500"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField>  
                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="notesColumnWidth">
                                    <ItemTemplate>
                                        <asp:Label runat="server" Text=''></asp:Label>
                                        <asp:TextBox  runat="server" Width="440px" Visible="false" Text='' MaxLength="1500"></asp:TextBox> 
                                    </ItemTemplate>                            
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# String.IsNullOrEmpty(Convert.ToString(Eval("TargetStartDate"))) ? "" : Convert.ToDateTime(Eval("TargetStartDate")).ToShortDateString() %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# String.IsNullOrEmpty(Convert.ToString(Eval("TargetStartDate"))) ? "" : Convert.ToDateTime(Eval("TargetStartDate")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField>         
                                <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("ProjectedSavings") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("ProjectedSavings") %>' MaxLength="15"></asp:TextBox> 
                                </ItemTemplate>
                                </asp:TemplateField>                                                                                                                                                                                                          
                            </Columns>        
                    </asp:GridView> 
                </div>
            </div>

            <div style="page-break-before: always"></div>
            
        </div>


        <div id="divTasks" runat="server">
            <div class="newPage">
                <h2 id="newPageTitle15" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient15" runat="server"></span>
                    <span id="newPageUserTitle15" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding15" src="" alt="magnify" runat="server" />
            </div>
            <hr />

            <div class="sectionTasks">

                <div id="lv" class="lvTasks">

                    <asp:ListView runat="server" ID="lvTopOpenTasks" ClientIDMode="Predictable" ClientIDRowSuffix="TaskID"
                        OnItemDataBound="lvTopOpenTasks_OnItemDataBound"
                        OnLayoutCreated="lvTopOpenTasks_OnLayoutCreated">
                        <LayoutTemplate>
                            <div class="lvPreHeader">                    
                                <span id="spanTopOpenTasksTableHeader" runat="server"></span>
                                <asp:ImageButton ID="btnOpenTasks" ClientIDMode="Static" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="openTasksLVEditButton_Click" runat="server" />
                            </div>
                            <tr runat="server" id="itemPlaceholder" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <table class="tbl1" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><span runat="server">Building</span></td>
                                    <td>
                                       <asp:Label runat="server" Text='<%# Eval("Analyses_Equipment.Equipment.Building.BuildingName") %>'></asp:Label>
                                       <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval("Analyses_Equipment.Equipment.Building.BuildingName") %>' MaxLength="50"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">EquipmentName</span></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Analyses_Equipment.Equipment.EquipmentName") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval("Analyses_Equipment.Equipment.EquipmentName") %>' MaxLength="50"></asp:TextBox> 
                                    </td>    
                                </tr>
                                <tr>
                                    <td><span runat="server">Summary</span></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Summary") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Summary")%>' MaxLength="250"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Assignee</span></td>           
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval(PropHelper.G<TaskRecord, User>(_ => _.User1, _ => _.Email)) %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval(PropHelper.G<TaskRecord, User>(_ => _.User1, _ => _.Email)) %>' MaxLength="50"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">DateCreated</span></td>            
                                    <td>
                                        <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("DateCreated")).ToShortDateString() %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Convert.ToDateTime(Eval("DateCreated")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">DiagnosticDate</span></td>            
                                    <td>
                                        <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("AnalysisStartDate")).ToShortDateString() %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Convert.ToDateTime(Eval("AnalysisStartDate")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Description</span></td>                            
                                    <td>
                                        <asp:Label runat="server" Text='<%#  Eval("Description") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Description") %>' MaxLength="5000"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Recommendations</span></td>                           
                                     <td>
                                        <asp:Label runat="server" Text='<%#  Eval("Recommendations") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Recommendations") %>' MaxLength="5000"></asp:TextBox>                          
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Actions</span></td>
                                    <td>
                                       <asp:Label runat="server" Text='<%#  Eval("Actions") %>'></asp:Label>
                                       <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Actions") %>' MaxLength="5000"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                           <table class="tbl2" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><span runat="server">Building</span></td>
                                    <td>
                                       <asp:Label runat="server" Text='<%# Eval("Analyses_Equipment.Equipment.Building.BuildingName") %>'></asp:Label>
                                       <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval("Analyses_Equipment.Equipment.Building.BuildingName") %>' MaxLength="50"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">EquipmentName</span></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Analyses_Equipment.Equipment.EquipmentName") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval("Analyses_Equipment.Equipment.EquipmentName") %>' MaxLength="50"></asp:TextBox> 
                                    </td>    
                                </tr>
                                <tr>
                                    <td><span runat="server">Summary</span></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Summary") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Summary")%>' MaxLength="250"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Assignee</span></td>           
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval(PropHelper.G<TaskRecord, User>(_ => _.User1, _ => _.Email)) %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval(PropHelper.G<TaskRecord, User>(_ => _.User1, _ => _.Email)) %>' MaxLength="50"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">DateCreated</span></td>            
                                    <td>
                                        <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("DateCreated")).ToShortDateString() %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Convert.ToDateTime(Eval("DateCreated")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>                        
                                    <td><span runat="server">DiagnosticDate</span></td>            
                                    <td>
                                        <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("AnalysisStartDate")).ToShortDateString() %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Convert.ToDateTime(Eval("AnalysisStartDate")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Description</span></td>                            
                                    <td>
                                        <asp:Label runat="server" Text='<%#  Eval("Description") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Description") %>' MaxLength="5000"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Recommendations</span></td>                           
                                     <td>
                                        <asp:Label runat="server" Text='<%#  Eval("Recommendations") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Recommendations") %>' MaxLength="5000"></asp:TextBox>                          
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Actions</span></td>
                                    <td>
                                       <asp:Label runat="server" Text='<%#  Eval("Actions") %>'></asp:Label>
                                       <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Actions") %>' MaxLength="5000"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </AlternatingItemTemplate>
                        <EmptyDataTemplate>
                            <div class="divEmptyTemplate" runat="server"></div>
                        </EmptyDataTemplate>
                    </asp:ListView>

                </div>

                <div id="lv" class="lvTasks">

                    <asp:ListView runat="server" ID="lvTopCompletedTasks" ClientIDMode="Predictable" ClientIDRowSuffix="TaskID"
                        OnItemDataBound="lvTopCompletedTasks_OnItemDataBound"
                        OnLayoutCreated="lvTopCompletedTasks_OnLayoutCreated">
                        <LayoutTemplate>
                            <div class="lvPreHeader">                    
                                <span id="spanTopCompletedTasksTableHeader" runat="server"></span>
                                <asp:ImageButton ID="btnCompletedTasks" ClientIDMode="Static" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-white.png" OnClick="completedTasksLVEditButton_Click"  runat="server" />
                            </div>
                            <tr runat="server" id="itemPlaceholder" />
                        </LayoutTemplate>
                        <ItemTemplate>
                            <table class="tbl1" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><span runat="server">Building</span></td>
                                    <td>
                                       <asp:Label runat="server" Text='<%# Eval("Analyses_Equipment.Equipment.Building.BuildingName") %>'></asp:Label>
                                       <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval("Analyses_Equipment.Equipment.Building.BuildingName") %>' MaxLength="50"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">EquipmentName</span></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Analyses_Equipment.Equipment.EquipmentName") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval("Analyses_Equipment.Equipment.EquipmentName") %>' MaxLength="50"></asp:TextBox> 
                                    </td>    
                                </tr>
                                <tr>
                                    <td><span runat="server">Summary</span></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Summary") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Summary")%>' MaxLength="250"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Assignee</span></td>           
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval(PropHelper.G<TaskRecord, User>(_ => _.User1, _ => _.Email)) %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval(PropHelper.G<TaskRecord, User>(_ => _.User1, _ => _.Email)) %>' MaxLength="50"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">DateCreated</span></td>            
                                    <td>
                                        <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("DateCreated")).ToShortDateString() %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Convert.ToDateTime(Eval("DateCreated")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">DateCompleted</span></td>            
                                    <td>
                                        <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("DateCompleted")).ToShortDateString() %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Convert.ToDateTime(Eval("DateCompleted")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Description</span></td>                            
                                    <td>
                                        <asp:Label runat="server" Text='<%#  Eval("Description") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Description") %>' MaxLength="5000"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Recommendations</span></td>                           
                                     <td>
                                        <asp:Label runat="server" Text='<%#  Eval("Recommendations") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Recommendations") %>' MaxLength="5000"></asp:TextBox>                          
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Actions</span></td>
                                    <td>
                                       <asp:Label runat="server" Text='<%#  Eval("Actions") %>'></asp:Label>
                                       <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Actions") %>' MaxLength="5000"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                           <table class="tbl2" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><span runat="server">Building</span></td>
                                    <td>
                                       <asp:Label runat="server" Text='<%# Eval("Analyses_Equipment.Equipment.Building.BuildingName") %>'></asp:Label>
                                       <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval("Analyses_Equipment.Equipment.Building.BuildingName") %>' MaxLength="50"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">EquipmentName</span></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Analyses_Equipment.Equipment.EquipmentName") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval("Analyses_Equipment.Equipment.EquipmentName") %>' MaxLength="50"></asp:TextBox> 
                                    </td>    
                                </tr>
                                <tr>
                                    <td><span runat="server">Summary</span></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Summary") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Summary")%>' MaxLength="250"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Assignee</span></td>           
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval(PropHelper.G<TaskRecord, User>(_ => _.User1, _ => _.Email)) %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# Eval(PropHelper.G<TaskRecord, User>(_ => _.User1, _ => _.Email)) %>' MaxLength="50"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">DateCreated</span></td>            
                                    <td>
                                        <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("DateCreated")).ToShortDateString() %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Convert.ToDateTime(Eval("DateCreated")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>                        
                                    <td><span runat="server">DateCompleted</span></td>            
                                    <td>
                                        <asp:Label runat="server" Text='<%# Convert.ToDateTime(Eval("DateCompleted")).ToShortDateString() %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Convert.ToDateTime(Eval("DateCompleted")).ToShortDateString() %>' MaxLength="10"></asp:TextBox> 
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Description</span></td>                            
                                    <td>
                                        <asp:Label runat="server" Text='<%#  Eval("Description") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Description") %>' MaxLength="5000"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Recommendations</span></td>                           
                                     <td>
                                        <asp:Label runat="server" Text='<%#  Eval("Recommendations") %>'></asp:Label>
                                        <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Recommendations") %>' MaxLength="5000"></asp:TextBox>                          
                                    </td>
                                </tr>
                                <tr>
                                    <td><span runat="server">Actions</span></td>
                                    <td>
                                       <asp:Label runat="server" Text='<%#  Eval("Actions") %>'></asp:Label>
                                       <asp:TextBox runat="server" Width="674px" Visible="false" Text='<%# Eval("Actions") %>' MaxLength="5000"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </AlternatingItemTemplate>
                        <EmptyDataTemplate>
                            <div class="divEmptyTemplate" runat="server"></div>
                        </EmptyDataTemplate>
                    </asp:ListView>

                </div>
                
            </div>

            <div style="page-break-before: always"></div>
            
        </div>

        <div id="divTopDiagnostics" runat="server">     
             <div class="newPage">
                <h2 id="newPageTitle16" runat="server"></h2>
                <div class="newPageClientAndUserTitle">
                    <span id="newPageClient16" runat="server"></span>
                    <span id="newPageUserTitle16" runat="server"></span>
                </div>
                <img id="imgNewPageBuilding16" src="" alt="magnify" runat="server" />
            </div>
            <hr />            
            <asp:Repeater ID="rptTopDiagnostics" runat="server" OnItemDataBound="rptTopDiagnostics_OnItemDataBound">              
            <ItemTemplate>                                                                                                                                                              
                <div class="analysisInfoHeaderAndFooter">
                    <h3 id="topRecentWeeklyDiagnosticIssueHeader" runat="server"></h3>
                    <hr />                     
                </div>                                                                                                                             
                <div class="analysisInfoFormat">                    
                    <label id="lblDiagnosticIssueClientName" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblClientName" runat="server" Text=""><%# Eval("ClientName") %></asp:Label>
                </div>
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueBuildingName" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblBuildingName" runat="server" Text=""><%# Eval("BuildingName") %></asp:Label>
                </div>
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueEquipmentName" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblEquipmentName" runat="server" Text=""><%# Eval("EquipmentName") %></asp:Label>
                </div>
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueAnalysisName" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblAnalysisName" runat="server" Text=""><%# Eval("AnalysisName") %></asp:Label>
                </div>
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueStartDate" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblStartDate" runat="server" Text=""><%# Convert.ToDateTime(Eval("StartDate")).ToShortDateString() %></asp:Label>
                </div>   
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueAnalysisRange" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblDisplayInterval" runat="server" Text=""><%# Eval("AnalysisRange") %></asp:Label>
                </div>   
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueCostSavings" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblCost" runat="server" Text=""><%# Eval("TotalCostSavingsCultureFormated") %></asp:Label>
                </div>   
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueEnergy" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblEnergy" runat="server" Text=""><%# Eval("AverageEnergyPriority") %></asp:Label>
                </div>   
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueMaintenance" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblMaintenance" runat="server" Text=""><%# Eval("AverageMaintenancePriority") %></asp:Label>
                </div> 
                <div class="analysisInfoFormat">
                    <label id="lblDiagnosticIssueComfort" runat="server" class="labelBoldColored">:</label>
                    <asp:Label CssClass="labelContent" ID="lblComfort" runat="server" Text=""><%# Eval("AverageComfortPriority") %></asp:Label>
                </div>        
                <div class="analysisInfoFormat" runat="server">
                    <label id="lblDiagnosticIssueNotes" runat="server" class="labelBoldColored">:</label>
                    <span class="richText labelContent">
                        <asp:Literal runat="server" Text='<%# Eval("Notes") %>'></asp:Literal>
                    </span>
                </div> 
                                                      
                <asp:HiddenField ID="imgFigureByteArray" Value='<%# Eval("FigureByteArray") != null && !isScheduled ? Convert.ToBase64String((byte[])Eval("FigureByteArray")) : null %>' runat="server" />
                <!--DataValue must be after ImageUrl in placement order of the radbinaryimage properties-->
                <telerik:RadBinaryImage CssClass="imgFigure" ID="imgFigure" Visible='<%# GetInclude() %>' ImageUrl='<%# isScheduled ? "" : (GetInclude() ? HandlerHelper.FigureImageUrl(Convert.ToInt32(Eval("CID")), Convert.ToInt32(Eval("EID")), Convert.ToInt32(Eval("AID")), Convert.ToDateTime(Eval("StartDate")), (CW.Common.Constants.DataConstants.AnalysisRange)Eval("AnalysisRange"), Path.GetExtension(Eval("FigureBlobExt").ToString())) : "" ) %>' DataValue='<%# isScheduled ? (byte[])Eval("FigureByteArray") : null %>' runat="server" />            

                <div class="analysisInfoHeaderAndFooter" style="page-break-before: always;"></div>
            </ItemTemplate>                                                
            </asp:Repeater>

        </div>


        <!--<div class="bottomPage">
            <img id="imgBottomPageFinal" alt="logo" src="" runat="server" />
        </div>-->

        </asp:Panel>

        <asp:UpdatePanel ID="updatePanelNested" EnableViewState="false" runat="server" UpdateMode="Conditional">
            <ContentTemplate>             
                <div id="divDownload" class="divDownload" runat="server">                    
                    <asp:Label ID="lblDownloadError" Visible="false" CssClass="errorMessage" runat="server"></asp:Label>
                    <br />
                    <asp:Button ID="btnDownload" runat="server" OnClientClick="refreshHtml();" OnClick="btnDownloadButton_Click" CausesValidation="false" Text=""></asp:Button> - <span id="spanDownload" runat="server"></span>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <script language="javascript" type="text/javascript">
            function refreshHtml() {
                document.getElementById('<%= hdn_container.ClientID %>').value = document.head.innerHTML + document.body.innerHTML;
            }

            document.getElementById('<%= hdn_container.ClientID %>').value = document.head.innerHTML + document.body.innerHTML;
        </script>

        </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
