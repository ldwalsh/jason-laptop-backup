﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSUploadPoints.aspx.cs" Inherits="CW.Website.KGSUploadPoints" %>
<%@ Register src="~/_controls/upload/UploadPoints.ascx" tagname="UploadPoints" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
   <CW:UploadPoints ID="UploadPoints" runat="server" />
</asp:Content>     