﻿<%@ Page Language="C#" EnableViewState="true" AutoEventWireup="false" CodeBehind="Connect.aspx.cs" Inherits="CW.Website.Connect" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title id="title" runat="server">$product</title>   
    <style type="text/css" >    
        @import url("/_assets/styles/connect.css");
    </style>
</head>
<body>
    <form id="form1" runat="server">
    
    <asp:Panel ID="pnlIndividual" runat="server" Visible="false">
    <fieldset class="connect-container-small">
        <legend id="legendIndividualSmall" runat="server"></legend>
        <div class="connect-logo-small">
            <img id="logoIndividualSmall" runat="server" src="" alt="" />
        </div>
        <div class="connect-code-small">
            <img id="codeIndividualSmall" src="" alt="" runat="server" />
        </div>
        <div class="connect-info-small">
            <label id="lblEquipmentNameSmall" runat="server"></label>
            <label id="lblExternalEquipmentTypeNameSmall" runat="server"></label>
            <label id="lblCMMSCodeSmall" runat="server"></label>
            <label id="lblBuidlingNameSmall" runat="server"></label>
        </div>
    </fieldset>
    <label id="lblContentSmall" class="connect-content-small" runat="server"></label>

    <br />

    <fieldset class="connect-container-medium">
        <legend id="legendIndividualMedium" runat="server"></legend>
        <div class="connect-logo-medium">
            <img id="logoIndividualMedium" runat="server" src="" alt="" />
        </div>
        <div class="connect-code-medium">
            <img id="codeIndividualMedium" src="" alt="" runat="server" />
        </div>
        <div class="connect-info-medium">
            <label id="lblEquipmentNameMedium" runat="server"></label>
            <label id="lblExternalEquipmentTypeNameMedium" runat="server"></label>
            <label id="lblCMMSCodeMedium" runat="server"></label>
            <label id="lblBuidlingNameMedium" runat="server"></label>
        </div>
    </fieldset>
    <label id="lblContentMedium" class="connect-content-medium" runat="server"></label>

    </asp:Panel>
    <asp:Panel ID="pnlAll" runat="server" CssClass="connect-repeater" Visible="false">
    
        <asp:Repeater ID="rptAll" runat="server" OnItemDataBound="Repeater_ItemDataBound">
            <ItemTemplate>
                <div class="connect-left">
                <fieldset class="connect-container-small">
                    <legend id="legendAllLeft" runat="server"></legend>
                    <div class="connect-logo-small">
                        <img id="logoAllLeft" runat="server" src="" alt="" />
                    </div>
                    <div class="connect-code-small">
                      <img id="codeAllLeft" runat="server" src='<%# "https://chart.googleapis.com/chart?cht=qr&chl=" +  GetUrl() + "EquipmentProfile.aspx?eid=" + Eval("EID").ToString() + "&chs=120x120&chld=M" %>' alt="" />
                    </div>
                    <div class="connect-info-small">
                        <label id="lblEquipmentNameLeft" runat="server"><%# Eval("EquipmentName") %></label>
                        <label id="lblExternalEquipmentTypeNameLeft" runat="server"><%# Eval("ExternalEquipmentTypeName")%></label>
                        <label id="lblCMMSCodeLeft" runat="server"><%# Eval("CMMSReferenceID")%></label>
                        <label id="lblBuidlingNameLeft" runat="server"><%# Eval("Building.BuildingName")%></label>
                    </div>
                </fieldset>
                <label id="lblContentLeft" class="connect-content-small" runat="server"></label>
                </div>
            </ItemTemplate>
            <AlternatingItemTemplate>            
                <div class="connect-right">
                <fieldset class="connect-container-small">
                    <legend id="legendAllRight" runat="server"></legend>
                    <div class="connect-logo-small">
                        <img id="logoAllRight" runat="server" src="" alt="" />
                    </div>
                    <div class="connect-code-small">
                      <img id="codeAllRight" runat="server" src='<%# "https://chart.googleapis.com/chart?cht=qr&chl=" +  GetUrl() + "EquipmentProfile.aspx?eid=" + Eval("EID").ToString() + "&chs=120x120&chld=M" %>' alt="" />
                    </div>
                    <div class="connect-info-small">
                        <label id="lblEquipmentNameRight" runat="server"><%# Eval("EquipmentName") %></label>
                        <label id="lblExternalEquipmentTypeNameRight" runat="server"><%# Eval("ExternalEquipmentTypeName")%></label>
                        <label id="lblCMMSCodeRight" runat="server"><%# Eval("CMMSReferenceID")%></label>
                        <label id="lblBuidlingNameRight" runat="server"><%# Eval("Building.BuildingName")%></label>
                    </div>
                </fieldset>
                <label id="lblContentRight" class="connect-content-small" runat="server"></label>
                </div>
                <br />
            </AlternatingItemTemplate>
        </asp:Repeater>

    </asp:Panel>
    
    </form>
</body>
</html>
