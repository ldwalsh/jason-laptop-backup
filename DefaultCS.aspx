﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultCS.aspx.cs" Inherits="Telerik.Web.Examples.Dock.MyPortal.DefaultCS" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<html>

    <head>
        <title>test</title>
    </head>
    <body>

        <form runat="server">



<telerik:RadScriptManager ID="RadScriptManager" EnableScriptGlobalization="true" AsyncPostBackTimeout="1200" runat="server" />
    <script type="text/javascript">
        var currentLoadingPanel = null;
        var currentUpdatedControl = null;

        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(BeginRequestHandler);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function BeginRequestHandler(sender, args) {
            currentLoadingPanel = $find("<%= RadAjaxLoadingPanel1.ClientID %>");
            currentUpdatedControl = "<%= Panel1.ClientID %>";
            currentLoadingPanel.show(currentUpdatedControl);
        }
        function EndRequestHandler(sender, args) {
            if (currentLoadingPanel != null)
                currentLoadingPanel.hide(currentUpdatedControl);
            currentUpdatedControl = null;
            currentLoadingPanel = null;
        }
    </script>
    <telerik:RadAjaxLoadingPanel runat="server" ID="RadAjaxLoadingPanel1"></telerik:RadAjaxLoadingPanel>
    <telerik:RadFormDecorator ID="DemoFormDecorator" runat="server" DecoratedControls="All"
        ControlsToSkip="Zone" EnableRoundedCorners="false" />

        <table style="width: 100%">
            <tr>
                <td style="width: 160px">
                    <label for="DroptDownWidget">
                        Select Module:</label>
                </td>
                <td style="width: 140px">
                    <asp:DropDownList runat="server" ID="DroptDownWidget" Width="150">
                        <asp:ListItem Text="Commodities.ascx" Value="~/_controls/Commodities.ascx"
                            Selected="true"></asp:ListItem>
                        <asp:ListItem Text="BuildingMap.ascx" Value="~/_controls/BuildingMap.ascx"></asp:ListItem>
                        <asp:ListItem Text="TelerikBlogs.ascx" Value="~/Dock/Common/TelerikBlogs.ascx"></asp:ListItem>
                        <asp:ListItem Text="HoroscopesCS.ascx" Value="~/Dock/Common/HoroscopesCS.ascx"></asp:ListItem>
                        <asp:ListItem Text="WeatherCS.ascx" Value="~/Dock/Common/WeatherCS.ascx"></asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
                <td style="width: 100px">
                    <asp:Button runat="server" ID="ButtonPostBack" Text="Make PostBack" OnClick="ButtonPostBack_Click"></asp:Button>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="DropDownZone">
                        Select Docking Zone:</label>
                </td>
                <td>
                    <asp:DropDownList ID="DropDownZone" runat="server" DataSource="<%#GetZones() %>"
                        DataTextField="ID" DataValueField="ClientID" Width="150">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Button runat="server" ID="ButtonAddDock" Text="Add Dock (AJAX)" OnClick="ButtonAddDock_Click"></asp:Button>
                </td>
                <td>
                    <asp:Button runat="server" ID="ButtonClear" Text="Clear Dock State" OnClick="ButtonClear_Click"></asp:Button>
                </td>
            </tr>
        </table>

    <asp:UpdatePanel runat="server" ID="UpdatePanel2" ChildrenAsTriggers="false" UpdateMode="Conditional">
        <ContentTemplate>
            <br />
            <asp:Panel ID="Panel1" runat="server" Width="820px">
                <telerik:RadDockLayout runat="server" ID="RadDockLayout1" OnSaveDockLayout="RadDockLayout1_SaveDockLayout"
                    OnLoadDockLayout="RadDockLayout1_LoadDockLayout">
                    <telerik:RadDockZone runat="server" ID="RadDockZone1" Width="250" MinHeight="200"
                        Style="float: left; margin-right: 15px">
                    </telerik:RadDockZone>
                    <telerik:RadDockZone runat="server" ID="RadDockZone2" Width="250" MinHeight="200"
                        Style="float: left; margin-right: 15px; background: #f5f4e8;">
                    </telerik:RadDockZone>
                    <telerik:RadDockZone runat="server" ID="RadDockZone3" Width="250" MinHeight="200"
                        Style="background: #d5f0fa; float: left;">
                    </telerik:RadDockZone>
                    <br style="clear: both;" />
                </telerik:RadDockLayout>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ButtonAddDock" EventName="Click"></asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <div style="width: 0px; height: 0px; overflow: hidden; position: absolute; left: -10000px;">
        Hidden UpdatePanel, which is used to help with saving state when minimizing, moving
          and closing docks. This way the docks state is saved faster (no need to update the
          docking zones).
          <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        </asp:UpdatePanel>
    </div>
        </form>
    </body>
</html>