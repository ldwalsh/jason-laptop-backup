﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="BureauReports.aspx.cs" Inherits="CW.Website.BureauReports" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <script type="text/javascript">

            var ListBoxCustomValidator = function (sender, args) {
            var listBox = document.getElementById(sender.controltovalidate);
        
            //.disabled changed in framework 4.5, .classList doesnt work in ie9
            if (!$(listBox).hasClass('aspNetDisabled')) {

                var listBoxCnt = 0;

                for (var x = 0; x < listBox.options.length; x++) {
                    if (listBox.options[x].selected) listBoxCnt++;
                }

                args.IsValid = (listBoxCnt > 0)
            }
            else {
                args.IsValid = true;
            }
            };

    </script>


             <h1>Bureau Reports</h1>
             <div class="richText">
                <p>The bureau reports page is used to enter dynamic input to generate a bureau report based on a provided date range.</p>
                <br />                
                <p>Follow the instructions below to generate and download as a pdf:</p>
                <ol>
                    <li>Make your selections and optionally fill out any content ahead of time.</li>
                    <li>Click generate below and a new page will open up with the report.</li>
                    <li>To edit content, click the dots in the right hand corner of any editable area.</li>
                    <li>Click download at the bottom of the generated report.</li>
                    <!--<li>Open this page in <strong>Chrome Version 23</strong> or higher.</li>
                    <li><strong>Produce content in document outside of the browser</strong> in case of session timeouts or the need to go back after sumbitting. Paste and format in form when ready to sumbit in entirety. Do not use the back button in the browser as it will not retain entered content.</li>
                    <li>Once generated, make sure you are at the default browser <strong>Zoom setting of 100%</strong>. You can check from the right hand corner of the browser window.</li> 
                    <li>Select to <strong>"Print"</strong> from the browser window.</li>
                    <li>Within the print screen, change the desitination to <strong>"Save as PDF"</strong>.</li>
                    <li>Within the print screen, uncheck the <strong>"Headers and footers"</strong> option.</li>
                    <li>Click <strong>"Save"</strong>.</li>-->
                </ol>
             </div>
             <div class="updateProgressDiv">
                 <asp:UpdateProgress ID="updateProgressTop" runat="server">
                     <ProgressTemplate>
                        <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                     </ProgressTemplate>
                 </asp:UpdateProgress>
             </div>
             <div class="administrationControls">

                    <h2>Bureau Current Report Period</h2>
                    <div class="divForm">    
                        <label class="label">*Start Date:</label>
                        <telerik:RadDatePicker ID="txtStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>  
                        <br /> 
                        <asp:RequiredFieldValidator ID="dateStartRequiredValidator" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtStartDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="Generate">
                                        </asp:RequiredFieldValidator>
                   </div>
                   <div class="divForm">
                        <label class="label">*End Date:</label>                     
                        <telerik:RadDatePicker ID="txtEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                          
                        <br />                        
                        <asp:RequiredFieldValidator ID="dateEndRequiredValidator" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtEndDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="Generate">
                                        </asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="dateCompareValidator" Display="Dynamic" runat="server"
                            CssClass="errorMessage" 
                            ErrorMessage="Invalid range."
                            ControlToValidate="txtEndDate"
                            ControlToCompare="txtStartDate"
                            Type="Date"
                            Operator="GreaterThanEqual"
                            ValidationGroup="Generate"
                            SetFocusOnError="true"
                            >
                            </asp:CompareValidator>
                    </div>

                    <hr />
                    <h2>Bureau Previous Report Period</h2>
                    <p>
                        Previous report comparison is most accurate when dates are accross the same interval. If this is the first report, or you would not like to do a comparison, enter the same dates as the current report period.
                    </p>
                    <div class="divForm">    
                        <label class="label">*Previous Start Date:</label>
                        <telerik:RadDatePicker ID="txtPreviousStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                         
                        <br />   
                        <asp:RequiredFieldValidator ID="datePreviousStartRequiredValidator" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtPreviousStartDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="Generate">
                                        </asp:RequiredFieldValidator>
                   </div>
                   <div class="divForm">
                        <label class="label">*Previous End Date:</label>                     
                        <telerik:RadDatePicker ID="txtPreviousEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                         
                        <br />                         
                        <asp:RequiredFieldValidator ID="datePreviousEndRequiredValidator" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtPreviousEndDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="Generate">
                                        </asp:RequiredFieldValidator>                                                                                                                           
                        <asp:CompareValidator ID="previousDateCompareValidator" Display="Dynamic" runat="server"
                            CssClass="errorMessage"
                            ErrorMessage="Invalid range."
                            ControlToValidate="txtPreviousEndDate"
                            ControlToCompare="txtPreviousStartDate"
                            Type="Date"
                            Operator="GreaterThanEqual"
                            ValidationGroup="Generate"
                            SetFocusOnError="true"
                            >
                            </asp:CompareValidator>
                    </div>
                    <!--<div class="divForm">
                            <label class="label">*Comparison Threshold Percent:</label>
                            <asp:DropDownList CssClass="dropdown" ID="ddlThreshold" runat="server" >  
                                <asp:ListItem Value="0" Selected="True">0%</asp:ListItem>
                                <asp:ListItem Value="1">1%</asp:ListItem>
                                <asp:ListItem Value="2">2%</asp:ListItem>
                                <asp:ListItem Value="3">3%</asp:ListItem>
                                <asp:ListItem Value="4">4%</asp:ListItem>
                                <asp:ListItem Value="5">5%</asp:ListItem>
                                <asp:ListItem Value="6">6%</asp:ListItem>
                                <asp:ListItem Value="7">7%</asp:ListItem>
                                <asp:ListItem Value="8">8%</asp:ListItem>
                                <asp:ListItem Value="9">9%</asp:ListItem>
                                <asp:ListItem Value="10">10%</asp:ListItem>
                            </asp:DropDownList>
                    </div>-->
                    <hr />
                    <h2>Bureau Settings</h2>
                    <!--<div id="divClient" class="divForm" runat="server">   
                        <label class="label">*Client:</label>    
                        OnSelectedIndexChanged="ddlClient_OnSelectedIndexChanged"
                        <asp:DropDownList ID="ddlClient" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server">
                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                        </asp:DropDownList> 
                    </div>--> 
                    <div class="divForm">                                                        
                            <label class="label">*Buildings:</label>                                                                                                                                         
                            <asp:ListBox ID="lbBuildings" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                            </asp:ListBox>
                            <asp:CustomValidator ID="buildingsCustomValidator" runat="server"  ErrorMessage="Buildings is a required field." ControlToValidate="lbBuildings" SetFocusOnError="true" ValidationGroup="Generate" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None" ></asp:CustomValidator> 
                            <ajaxToolkit:ValidatorCalloutExtender ID="buildingsCustomValidatorExtender" runat="server" BehaviorID="buildingsCustomValidatorExtender" TargetControlID="buildingsCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                    </div>
                    <div class="divForm">
                            <label class="label">*Theme:</label>
                            <asp:DropDownList CssClass="dropdown" ID="ddlTheme" runat="server">  
                            </asp:DropDownList>
                    </div>
                    <div class="divForm">
                        <label class="label">*Show Clients Organization Logo:</label>
                        <asp:CheckBox ID="chkShowLogo" CssClass="checkbox" Checked="true" runat="server" />                                                                                        
                    </div>

                    <hr />
                    <h2>Bureau Content</h2>
                    <div class="divFormLeftmost">
                        <label>
                        Introduction (Max HTML Characters = 5000):</label>
                        <telerik:RadEditor ID="editorIntroduction" 
                                    runat="server"                                                                                                                                                
                                    CssClass="editorNewLine"                                                        
                                    Height="475px" 
                                    Width="674px"
                                    MaxHtmlLength="5000"
                                    NewLineMode="Div"
                                    ToolsWidth="676px"                                                                                                                                       
                                    ToolbarMode="ShowOnFocus"        
                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                                    > 
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                    </CssFiles>                                                                                                                             
                        </telerik:RadEditor>                                 
                    </div>
                    <div class="divFormLeftmost">
                        <label>
                        Recommended Actions (Max HTML Characters = 5000):</label>
                        <telerik:RadEditor ID="editorRecommendedActions" 
                                    runat="server" 
                                    CssClass="editorNewLine"                                                  
                                    Height="475px" 
                                    Width="674px"
                                    MaxHtmlLength="5000"
                                    NewLineMode="Div"
                                    ToolsWidth="676px"                                                                                                                                       
                                    ToolbarMode="ShowOnFocus"        
                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                    >   
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                    </CssFiles>                                                                                
                        </telerik:RadEditor>
                    </div>
                                             
                    <div class="divFormLeftmost">
                        <label>
                        Energy Cost Trend Analysis (Max HTML Characters = 5000):</label>
                        <telerik:RadEditor ID="editorEnergyTrend"
                                    runat="server" 
                                    CssClass="editorNewLine"                                                  
                                    Height="475px" 
                                    Width="674px"
                                    MaxHtmlLength="5000"
                                    NewLineMode="Div"
                                    ToolsWidth="676px"                                                                                                                                       
                                    ToolbarMode="ShowOnFocus"        
                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                    >   
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                    </CssFiles>                                                                                
                        </telerik:RadEditor>
                    </div>
                    <div class="divFormLeftmost">
                        <label>
                        Maintenance Trend Analysis (Max HTML Characters = 5000):</label>
                        <telerik:RadEditor ID="editorMaintenanceTrend" 
                                    runat="server" 
                                    CssClass="editorNewLine"                                                  
                                    Height="475px" 
                                    Width="674px"
                                    MaxHtmlLength="5000"
                                    NewLineMode="Div"
                                    ToolsWidth="676px"                                                                                                                                       
                                    ToolbarMode="ShowOnFocus"        
                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                    > 
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                    </CssFiles>                                                                                  
                        </telerik:RadEditor>
                    </div>
                    <div class="divFormLeftmost">
                        <label>
                        Comfort Trend Analysis (Max HTML Characters = 5000):</label>
                        <telerik:RadEditor ID="editorComfortTrend" 
                                    runat="server" 
                                    CssClass="editorNewLine"                                                  
                                    Height="475px" 
                                    Width="674px"
                                    MaxHtmlLength="5000"
                                    NewLineMode="Div"
                                    ToolsWidth="676px"                                                                                                                                       
                                    ToolbarMode="ShowOnFocus"        
                                    ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"                                                
                                    >   
                                    <CssFiles>
                                        <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                    </CssFiles>                                                                                
                        </telerik:RadEditor>
                    </div>
          
                    <asp:LinkButton CssClass="lnkButton" ID="btnGenerate" runat="server" Text="Generate" PostBackUrl="~/BureauReport.aspx" OnClientClick="window.document.forms[0].target='_blank';" ValidationGroup="Generate"></asp:LinkButton>
                        
             </div>

</asp:Content>                