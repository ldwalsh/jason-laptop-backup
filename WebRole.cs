﻿using CW.Common.Config;
using CW.Common.Constants;
using CW.Logging;
using CW.Data.AzureStorage;
using CW.Data.AzureStorage.Config;
using CW.Data.AzureStorage.Helpers;
using CW.Data.Instrumentation;
using CW.Website.DependencyResolution;
using Microsoft.Web.Administration;
using Microsoft.WindowsAzure.ServiceRuntime;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;


namespace WebRole1
{
    public class WebRole : RoleEntryPoint
    {
        private static IConfigManager CM { get { return new AzureConfigManager(); } }
        public override bool OnStart()
        {
            if (!RoleEnvironment.IsEmulated)
            {
                using (ServerManager serverManager = new ServerManager())
                {
                    foreach (var app in serverManager.Sites.SelectMany(x => x.Applications))
                    {
                        app["preloadEnabled"] = true;
                    }
                    foreach (var appPool in serverManager.ApplicationPools)
                    {
                        appPool.AutoStart = true;
                        appPool["startMode"] = "AlwaysRunning";
                        appPool.ProcessModel.IdleTimeout = TimeSpan.FromHours(CM.GetConfigurationSetting("AppPool.IdleTimeoutInHours", 0));
                        appPool.Recycling.PeriodicRestart.Time = TimeSpan.FromHours(CM.GetConfigurationSetting("AppPool.RestartInHours", 0));
                    }
                    serverManager.CommitChanges();
                }
            }

            bool hasConfigurationRestart = CM.GetConfigurationSetting("Configuration.HasRestartEvent", false);

            if (hasConfigurationRestart)
            {
                // For information on handling configuration changes
                // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
                RoleEnvironment.Changing += RoleEnvironmentChanging;
                RoleEnvironment.Changed += RoleEnvironmentChanged;
            }

            return base.OnStart();
        }

        private void RoleEnvironmentChanged(object sender, RoleEnvironmentChangedEventArgs e)
        {
            // Get the list of configuration changes
            var settingChanges = e.Changes.OfType<RoleEnvironmentConfigurationSettingChange>();

            foreach (var settingChange in settingChanges)
            {
                Trace.WriteLine("Setting: " + settingChange.ConfigurationSettingName, "Information");
            }
        }

        private void RoleEnvironmentChanging(object sender, RoleEnvironmentChangingEventArgs e)
        {
            // If a configuration setting is changing
            if (e.Changes.Any(change => change is RoleEnvironmentConfigurationSettingChange))
            {
                // Set e.Cancel to true to restart this role instance
                e.Cancel = true;
            }
        }
    }
}
