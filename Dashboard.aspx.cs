﻿using System;
using System.Configuration;
using System.Web.UI;
using CW.Business;
using CW.Data;
using CW.Website._framework;
using System.Xml;

namespace CW.Website
{
    public partial class Dashboard: SitePage
    {
        #region Properties


        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            initParams.Attributes.Add("value", "UserID=" + siteUser.UID + ", UserRoleID=" + siteUser.RoleID + ", ClientID=" + siteUser.CID + ", Buildings=" + Utility.SerializeHelper.SerializeToXMLString(siteUser.VisibleBuildings) + ", SiteAddress=" + ConfigurationManager.AppSettings["SiteAddress"] + ", IsKGSFullAdminOrHigher=" + siteUser.IsKGSFullAdminOrHigher + ", IsSuperAdminOrFullAdminOrFullUser=" + siteUser.IsSuperAdminOrFullAdminOrFullUser);
            //initParams.Attributes.Add("value", );
            //initParams.Attributes.Add("value", "ClientID=" + Session["CID"].ToString());

            //if the page is not a postback
            if (!Page.IsPostBack)
            {
                //set admin global settings
                //Make sure to decode from ascii to html 
                GlobalSetting globalSetting = GlobalDataMapper.GetGlobalSettings();
                litDashboardBody.Text = Server.HtmlDecode(globalSetting.DashboardBody);
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
        }

        #endregion

        #region Load and Bind Fields

        #endregion

        #region Dropdown Events

        #endregion

        #region Button Click Events

        protected void generateButton_Click(object sender, EventArgs e)
        {
        }

        #endregion

        #region Helper Methods


        #endregion
    }
}