﻿using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class Help : SiteMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            if (siteUser.IsAnonymous || siteUser.IsPublicOrKioskUser) Response.Redirect("/Home.aspx");

            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "jqueryColor", "base");
        }
    }
}