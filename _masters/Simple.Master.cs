﻿using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class Simple : SiteMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            if (siteUser.IsAnonymous) Response.Redirect("/Home.aspx");
        }
    }
}