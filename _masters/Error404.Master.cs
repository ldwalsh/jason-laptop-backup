﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace CW.Website._masters
{
    public partial class Error404 : MasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            var IsSchneider = Request.Url.AbsoluteUri.Contains(ConfigurationManager.AppSettings["SchneiderElectricDomain"]);
            var head = Page.Header;
            var link = new HtmlLink();

            link.Attributes.Add("href", IsSchneider ? Page.ResolveClientUrl("/_assets/styles/themes/ba.css") : Page.ResolveClientUrl("/_assets/styles/themes/cw.css"));
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("rel", "stylesheet");
            head.Controls.Add(link);

            link = new HtmlLink();
            link.Attributes.Add("href", IsSchneider ? Page.ResolveClientUrl("/_assets/styles/themes/images/se-favicon.ico") : Page.ResolveClientUrl("/_assets/styles/themes/images/cw-favicon.ico"));
            link.Attributes.Add("type", "image/x-icon");
            link.Attributes.Add("rel", "shortcut icon");
            head.Controls.Add(link);

            var title = (HtmlTitle)FindControl("title");

            title.Text = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(IsSchneider, title.Text).ToUpper();
        }
    }
}