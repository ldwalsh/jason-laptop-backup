﻿using CW.Common.Constants;
using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class Documents: SiteModuleMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            litScripts.Text = ReferenceSetter.Get("jquery", "kendo");          
        }

        public Documents()
        {
            base.mModule = BusinessConstants.Module.Modules.Documents;
        }
    }
}