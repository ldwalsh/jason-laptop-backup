﻿using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class Secondary : SiteMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            if (siteUser.IsAnonymous) Response.Redirect("/Home.aspx");

            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "base");
        }
    }
}