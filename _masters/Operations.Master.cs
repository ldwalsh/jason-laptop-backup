﻿using CW.Common.Constants;
using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class Operations: SiteModuleMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "highStock");
        }

        public Operations()
        {
            //OperationsDashboard
            mModule = BusinessConstants.Module.Modules.Operations;
        }
    }
}