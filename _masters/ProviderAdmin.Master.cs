﻿using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class ProviderAdmin : SiteMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {            
            //Check if logged in, is kgs full admin or higher, or full admin provider.
            if (siteUser.IsAnonymous || !(siteUser.IsKGSFullAdminOrHigher || (siteUser.IsFullAdminOrHigher && siteUser.IsLoggedInUnderProviderClient))) 
                Response.Redirect("/Home.aspx");

            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "jqueryColor", "base");
        }
    }
}
