﻿using System;
using CW.Website._framework;

namespace CW.Website._masters
{
    public partial class Admin: SiteMasterPage
    {
        protected void Page_Init(Object sender, EventArgs ea)
        {
            //Check if logged in and restricted admin or higher
            //restricted and full admins can view admin pages. super admin can edit certain extra pages/info.

            //Check if is restricted(aka full for this client) admin or higher
            if (!siteUser.IsFullAdminOrHigher) 
                Response.Redirect("/Home.aspx");

            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "jqueryColor", "base");
        }
    }
}