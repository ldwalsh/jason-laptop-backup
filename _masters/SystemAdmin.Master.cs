﻿using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class SystemAdmin : SiteMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            //Check is kgs super admin
            if (!siteUser.IsKGSSuperAdminOrHigher) 
                Response.Redirect("/Home.aspx");

            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "jqueryColor", "base");
        }
    }
}