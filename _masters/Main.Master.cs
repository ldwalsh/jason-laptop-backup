﻿using CW.Website._framework;
using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._masters
{
    public partial class Main : MasterPage
    {
        public bool isSEDomain = false;

        protected void Page_Init(object sender, EventArgs e)
        {
            isSEDomain = Request.Url.AbsoluteUri.Contains(ConfigurationManager.AppSettings["SchneiderElectricDomain"]);

            //dynamically add themed css and favicons based on se theme check
            HtmlHead head = (HtmlHead)Page.Header;
            HtmlLink link = new HtmlLink();
            link.Attributes.Add("href", isSEDomain ? Page.ResolveClientUrl("/_assets/styles/themes/ba.css") : Page.ResolveClientUrl("/_assets/styles/themes/cw.css"));
            link.Attributes.Add("type", "text/css");
            link.Attributes.Add("rel", "stylesheet");
            head.Controls.Add(link);
            
            link = new HtmlLink();
            link.Attributes.Add("href", isSEDomain ? Page.ResolveClientUrl("/_assets/styles/themes/images/se-favicon.ico") : Page.ResolveClientUrl("/_assets/styles/themes/images/cw-favicon.ico"));
            link.Attributes.Add("type", "image/x-icon");
            link.Attributes.Add("rel", "shortcut icon");
            head.Controls.Add(link);

            title.Text = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(isSEDomain, title.Text).ToUpper();

            var siteUser = SiteUser.Current;

            //Check if logged in
            if (!siteUser.IsAnonymous)
            {
                ClientScriptManager cs = Page.ClientScript;
                Type cstype = this.GetType();

                if (!String.IsNullOrEmpty(siteUser.OrganizationHeaderBGColor) || !String.IsNullOrEmpty(siteUser.OrganizationHeaderTextColor))
                {
                    //--Dynamically generated internal style sheet hack---------------------------------      
                    Literal dynamicallyGeneratedStyleSheetHack = (Literal)this.FindControl("dynamicallyGeneratedStyleSheetHack");
                    dynamicallyGeneratedStyleSheetHack.Text = "<style> ";

                    //set organization custom theming if enabled
                    //set header bg color if exists
                    if (!String.IsNullOrEmpty(siteUser.OrganizationHeaderBGColor))
                    {
                        HtmlControl header = (HtmlControl)this.FindControl("header");
                        if (header != null) header.Style.Add("background", siteUser.OrganizationHeaderBGColor);

                        dynamicallyGeneratedStyleSheetHack.Text += ".divLeftNav, .divLeftNav a{background-color: " + siteUser.OrganizationHeaderBGColor + " !important;}";
                    }

                    if (!String.IsNullOrEmpty(siteUser.OrganizationHeaderTextColor))
                    {
                        dynamicallyGeneratedStyleSheetHack.Text += ".header .divHeaderBar .divLogin a, .header .divLogin,.header .divPrimaryNav,.header .divPrimaryNav a,.header .divSelectClient,.header .divSelectModule{color: " + siteUser.OrganizationHeaderTextColor + " !important;}";
                        dynamicallyGeneratedStyleSheetHack.Text += ".header .divHeaderBar ul#utility li{border-right: 1px solid " + siteUser.OrganizationHeaderTextColor + " !important;}";
                        dynamicallyGeneratedStyleSheetHack.Text += ".header .divHeaderBar ul#utility li.last{border-right: none !important;}";
                        dynamicallyGeneratedStyleSheetHack.Text += ".header .divSelectClient,.header .divSelectModule{border-left: 1px solid " + siteUser.OrganizationHeaderTextColor + " !important;}";
                        dynamicallyGeneratedStyleSheetHack.Text += ".divLeftNav a{color: " + siteUser.OrganizationHeaderTextColor + " !important;}";
                    }

                    dynamicallyGeneratedStyleSheetHack.Text += " </style>";
                    //----------------------------------------------------------------------------------
                }

                //set provider custom theming if enabled            

                //dynmaically load users culture based kendo culture script.
                HtmlGenericControl cultureInclude = new HtmlGenericControl("script");
                cultureInclude.Attributes.Add("src", "/_assets/scripts/kendocultures/kendo.culture." + siteUser.CultureName + ".js");
                cultureInclude.Attributes.Add("type", "text/javascript");
                head.Controls.Add(cultureInclude);

                //register kendo startup script
                string kendoInitializeScript = "<script type=\"text/javascript\"> kendo.culture(\"" + siteUser.CultureName + "\"); </script>";
                cs.RegisterStartupScript(cstype, "Kendo Culture Intialize", kendoInitializeScript, false);

                litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "jsapi", "map","highStock");
            }
            else
            {
                litScripts.Text = ReferenceSetter.Get("jquery");
            }                            
        }
    }
}