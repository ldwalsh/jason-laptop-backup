﻿using CW.Common.Constants;
using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class AnalysisBuilder: SiteModuleMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
			litScripts.Text = ReferenceSetter.Get("jquery 1.8.3", "kendo", "jqueryUI", "jqueryMiniColor");
        }

        public AnalysisBuilder()
        {
            mModule = BusinessConstants.Module.Modules.AnalysisBuilder;
        }

        protected override Boolean UseFullBasePath
        {
            get {return true;}
        }
    }
}