﻿using CW.Business;
using CW.Website._framework;
using System;
using CW.Common.Constants;

namespace CW.Website._masters
{
    public partial class CommissioningDashboard: SiteModuleMasterPage
    {
        public CommissioningDashboard()
        {
            mModule = BusinessConstants.ModuleConstants.Module.CommissioningDashboard;
        }

        protected override Boolean UseFullBasePath
        {
            get {return true;}
        }
    }
}
