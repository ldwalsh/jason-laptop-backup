﻿using CW.Common.Constants;
using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class EnergyDashboard : SiteModuleMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            litScripts.Text = ReferenceSetter.Get("jquery", "kendo");
        }

        public EnergyDashboard()
        {
            mModule = BusinessConstants.Module.Modules.EnergyDashboard;
        }
    }
}