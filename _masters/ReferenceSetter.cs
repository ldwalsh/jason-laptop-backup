﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace CW.Website._masters
{
    public class ReferenceSetter
    {
        #region Properties

            static Dictionary<String, IEnumerable<String>> references { get; set; }

        #endregion

        #region Constructor

            static ReferenceSetter()
            {
                if (references == null) references = new Dictionary<String, IEnumerable<String>>();

                references.Add("jquery", new String[] { "https://code.jquery.com/jquery-2.1.4.min.js", "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js", "/_assets/scripts/jquery-2.1.4.min.js" });
                references.Add("jquery 1.8.3", new String[] { "https://code.jquery.com/jquery-1.8.3.min.js", "https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js", "/_assets/scripts/jquery-1.8.3.min.js" });
                references.Add("jqueryUI", new String[] { "https://code.jquery.com/ui/1.9.2/jquery-ui.min.js", "https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js", "/_assets/scripts/jquery-ui-1.9.2.min.js" });
                references.Add("jqueryColor", new String[] { "https://code.jquery.com/color/jquery.color-2.1.2.min.js", "/_assets/scripts/jquery-color.min.js" });
                references.Add("jqueryMiniColor", new String[] { "3rdParty/miniColors/jquery.miniColors.js" });
                references.Add("base", new String[] { "/_assets/scripts/base.min.js" });
                references.Add("kendo", new String[] { "https://da7xgjtj801h2.cloudfront.net/2015.1.318/js/kendo.core.min.js", "/_assets/scripts/kendo.core.min.js" });
                references.Add("flot", new String[] { "3rdParty/flot/jquery.flot.min.js" });
                references.Add("flotResize", new String[] { "3rdParty/flot/jquery.flot.resize.min.js" });
                references.Add("flotTime", new String[] { "3rdParty/flot/jquery.flot.time.min.js" });
                references.Add("map", new String[] { "https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1" });
                references.Add("jsapi", new String[] { "https://www.google.com/jsapi" });
                references.Add("widgetMaster", new String[] { "/_assets/scripts/WidgetMaster.js" });
                references.Add("highStock", new String[] { "https://code.highcharts.com/stock/highstock.js" });
                references.Add("highStockExport", new String[] { "https://code.highcharts.com/stock/modules/exporting.js" });
                //references.Add("highCharts", new String[] { "https://code.highcharts.com/highcharts.js" });
                

                //references.Add("cldr", new String[] { "https://cdnjs.cloudflare.com/ajax/libs/cldrjs/0.4.1/cldr.min.js" });
                //references.Add("globalize", new String[] { "https://cdnjs.cloudflare.com/ajax/libs/globalize/1.0.0/globalize.min.js" });
                references.Add("buildingequipmentvariables", new String[] { "/_assets/scripts/buildingequipmentvariables.js" });
            }

        #endregion

        #region Methods

            public static String Get(params String[] keys)
            {
                var scriptTemplate = "<script type=\"text/javascript\" src=\"{0}\"></script>";
                var scripts = new StringBuilder().AppendLine(); //create instance and append a blank line for formatting.

                foreach (String key in keys)
                {
                    var values = references[key];

                    foreach (String value in values)
                    {
                        if (value.Contains("http"))
                        {
                            var request = (HttpWebRequest)WebRequest.Create(value);
                            request.Method = "HEAD";

                            try
                            {
                                using (var response = (HttpWebResponse)request.GetResponse())
                                    if (response.StatusCode != HttpStatusCode.OK) continue;

                                scripts.AppendLine(String.Format(scriptTemplate, value));
                                break;
                            }
                            catch (Exception) { continue; } //there wasn't any response, so go to the next value
                        }

                        scripts.AppendLine(String.Format(scriptTemplate, value));
                    }   
                }

                return scripts.ToString();
            }

        #endregion
    }
}