﻿using CW.Common.Constants;
using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class Tasks : SiteModuleMasterPage
    {
        #region events

            private void Page_Init(Object sender, EventArgs e) { litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "base"); }

        #endregion

        #region constructor

            public Tasks() { mModule = BusinessConstants.Module.Modules.Tasks; }

        #endregion
    }
}