﻿using CW.Common.Constants;
using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class Kiosk : SiteModuleMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "jqueryUI");
        }

        public Kiosk()
        {
            mModule = BusinessConstants.Module.Modules.Kiosk;
        }

        protected override Boolean UseFullBasePath
        {
            get {return true;}
        }
    }
}