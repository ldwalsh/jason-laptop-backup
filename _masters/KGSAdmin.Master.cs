﻿using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class KGSAdmin : SiteMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {            
            //Check if is kgs full (aka restricted) admin or higher            
            if (!siteUser.IsKGSFullAdminOrHigher) 
                Response.Redirect("/Home.aspx");

            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "jqueryColor", "base");
        }
    }
}