﻿using CW.Common.Constants;
using CW.Website._framework;
using System;

namespace CW.Website._masters
{
    public partial class Diagnostics : SiteModuleMasterPage
    {
        protected void Page_Init(Object sender, EventArgs e)
        {
            litScripts.Text = ReferenceSetter.Get("jquery", "kendo", "base");
        }

        public Diagnostics()
        {
            mModule = BusinessConstants.Module.Modules.Diagnostics;
        }
    }
}