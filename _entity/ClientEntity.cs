﻿using System;
using CW.Business;
using CW.Website._framework;

namespace CW.Website.entity
{
    public class ClientEntity
    {
        public  readonly
                Int32
                ID;

        public  readonly
                String
                name;

        public ClientEntity(Int32 ID, String name)
        {
            //security check here

            this.ID   = ID;
            this.name = name;
        }
    }
}