﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Help.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="ReleaseNotes.aspx.cs" Inherits="CW.Website.ReleaseNotes" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                  	                  	        	
        	<h1>Release Notes</h1>                        
                <div class="richText">Release notes section.</div>   
                <br />
                <div class="release">

                <asp:Repeater ID="rptReleaseNotes" runat="server">            
                    <ItemTemplate>
                    <div class="groupRepeaterItem">
                        <h3 runat="server">Version <%# Eval("MajorVersion") %>.<%# Eval("MinorVersion") %>.<%# Eval("BuildNumber") %> (<%# Convert.ToDateTime(Eval("ReleaseDate")).ToShortDateString() %>)</h3>                            
                        <asp:HyperLink ID="lnk1" runat="server" CssClass="toggle"><asp:Label ID="lbl1" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                        <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender1" runat="Server"
                                    TargetControlID="pnl1"
                                    CollapsedSize="0"
                                    Collapsed="false" 
                                    ExpandControlID="lnk1"
                                    CollapseControlID="lnk1"
                                    AutoCollapse="false"
                                    AutoExpand="false"
                                    ScrollContents="false"
                                    ExpandDirection="Vertical"
                                    TextLabelID="lbl1"
                                    CollapsedText="show notes"
                                    ExpandedText="hide notes" 
                                    />
                         <asp:Panel ID="pnl1" CssClass="richText" runat="server">
                            <asp:Literal ID="litReleaseNotes" runat="server" Text='<%# String.IsNullOrEmpty(Convert.ToString(Eval("ReleaseNotes"))) ? "" : Eval("ReleaseNotes").ToString()  %>'></asp:Literal>
                         </asp:Panel>
                     </div>
                    </ItemTemplate>
                </asp:Repeater>      

               </div>                                 

</asp:Content>                