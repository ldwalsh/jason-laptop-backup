﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using CW.Business;
using CW.Data;
using CW.Website._framework;
using CW.Data.AzureStorage;
using System.Configuration;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class Connect : SitePage
    {
        #region Properties

        private const string contentEquipment = "Scan this QR code to access control data and equipment information.";
        private const string contentBuilding = "Scan this QR code to access control data and building information.";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //datamanger in sitepage

            //Check if logged in
            if (siteUser.IsAnonymous)
            {
                Response.Redirect("/Home.aspx");

                return;
            }

            //if the page is not a postback, clear the viewstate and set the initial viewstate settings
            if (!Page.IsPostBack)
            {
                string siteUrl = GetUrl();

                //if page not postback and eid querystring value exists bind profile
                if (!String.IsNullOrEmpty(Request.QueryString["eid"]))
                {
                    int eid = Convert.ToInt32(Request.QueryString["eid"]);

                    //check if equipment is associated with client or is kgsFullAdmin role or higher
                    if (siteUser.IsKGSFullAdminOrHigher || DataMgr.EquipmentDataMapper.IsEquipmentAssociatedWithClient(eid, siteUser.CID))
                    {
                        codeIndividualSmall.Src = "https://chart.googleapis.com/chart?cht=qr&chl=" + siteUrl + "EquipmentProfile.aspx?eid=" + Request.QueryString["eid"].ToString() + "&chs=120x120&chld=M";
                        codeIndividualMedium.Src = "https://chart.googleapis.com/chart?cht=qr&chl=" + siteUrl + "EquipmentProfile.aspx?eid=" + Request.QueryString["eid"].ToString() + "&chs=200x200&chld=M";
                    }
                    else
                    {
                        Response.Redirect("/Home.aspx");
                    }

                    Equipment equipment = DataMgr.EquipmentDataMapper.GetEquipmentByEID(eid, false, true, false, false, false, false);
                    lblEquipmentNameSmall.InnerText = lblEquipmentNameMedium.InnerText = equipment.EquipmentName;
                    lblExternalEquipmentTypeNameSmall.InnerText = lblExternalEquipmentTypeNameMedium.InnerText = equipment.ExternalEquipmentTypeName;
                    lblCMMSCodeSmall.InnerText = lblCMMSCodeMedium.InnerText = equipment.CMMSReferenceID;
                    lblBuidlingNameSmall.InnerText = lblBuidlingNameMedium.InnerText = equipment.Building.BuildingName;

                    lblContentSmall.InnerText = lblContentMedium.InnerText = contentEquipment;

                    SetIndividualConnectTheming();

                    pnlIndividual.Visible = true;
                }
                //else if page not postback and bid querystring value exists bind profile
                else if (!String.IsNullOrEmpty(Request.QueryString["bid"]))
                {
                    int bid = Convert.ToInt32(Request.QueryString["bid"]);

                    //check if building is associated with client or is kgsFullAdmin role or higher
                    if (siteUser.IsKGSFullAdminOrHigher || DataMgr.BuildingDataMapper.IsBuildingAssociatedWithCient(bid, siteUser.CID))
                    {
                        if (!String.IsNullOrEmpty(Request.QueryString["all"]))
                        {
                            bool all = Convert.ToBoolean(Request.QueryString["all"]);

                            rptAll.DataSource = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid, (eq => eq.Building));
                            rptAll.DataBind();

                            pnlAll.Visible = true;
                        }
                        else
                        {
                            codeIndividualSmall.Src = "https://chart.googleapis.com/chart?cht=qr&chl=" + siteUrl + "BuildingProfile.aspx?bid=" + Request.QueryString["bid"].ToString() + "&chs=120x120&chld=M";
                            codeIndividualMedium.Src = "https://chart.googleapis.com/chart?cht=qr&chl=" + siteUrl + "BuildingProfile.aspx?bid=" + Request.QueryString["bid"].ToString() + "&chs=200x200&chld=M";

                            Building building = DataMgr.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false);
                            lblEquipmentNameSmall.Visible = lblEquipmentNameMedium.Visible = false;
                            lblBuidlingNameSmall.InnerText = lblBuidlingNameMedium.InnerText = building.BuildingName;

                            lblContentSmall.InnerText = lblContentMedium.InnerText = contentBuilding;

                            SetIndividualConnectTheming();

                            pnlIndividual.Visible = true;
                        }
                    }
                    else
                    {
                        Response.Redirect("/Home.aspx");
                    }
                }
                else
                {
                    Response.Redirect("/Home.aspx");
                }

                //global page themeing
                HtmlTitle title = (HtmlTitle)this.FindControl("title");
                title.Text = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, title.Text).ToUpper();

                //dynamically add themed css and favicons based on se theme check
                HtmlHead head = (HtmlHead)Page.Header;
                HtmlLink link = new HtmlLink();
                link = new HtmlLink();
                link.Attributes.Add("href", siteUser.IsSchneiderTheme ? Page.ResolveClientUrl("/_assets/styles/themes/images/se-favicon.ico") : Page.ResolveClientUrl("/_assets/styles/themes/images/cw-favicon.ico"));
                link.Attributes.Add("type", "image/x-icon");
                link.Attributes.Add("rel", "shortcut icon");
                head.Controls.Add(link);
            }
        }

        /// <summary>
        /// Used to get Bulk/All Connect themeing
        /// </summary>
        /// <param name="Sender"></param>
        /// <param name="e"></param>
        protected void Repeater_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                string side = e.Item.ItemType == ListItemType.Item ? "Left" : "Right";

                ((HtmlGenericControl)(e.Item.FindControl("legendAll" + side))).InnerHtml = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productShort-CONNECT").ToUpper() + (siteUser.IsSchneiderTheme ? "" : "<sup>&reg;</sup>");

                ((HtmlImage)(e.Item.FindControl("codeAll" + side))).Alt = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productShort-connect");
                ((HtmlImage)(e.Item.FindControl("logoAll" + side))).Alt = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productShort-gears");
                ((HtmlImage)(e.Item.FindControl("logoAll" + side))).Src = ConfigurationManager.AppSettings["ThemeImageAssetPath"] + (siteUser.IsSchneiderTheme ? "se-small.png" : "cw-gears.png");

                ((HtmlGenericControl)(e.Item.FindControl("lblContent" + side))).InnerText = contentEquipment;
            }
        }

        private void SetIndividualConnectTheming()
        {
            //individuals
            legendIndividualSmall.InnerHtml = legendIndividualMedium.InnerHtml =
                Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productShort-CONNECT").ToUpper() + (siteUser.IsSchneiderTheme ? "" : "<sup>&reg;</sup>");

            codeIndividualSmall.Alt = codeIndividualMedium.Alt = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productShort-connect");
            logoIndividualSmall.Alt = logoIndividualMedium.Alt = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productShort-gears");
            logoIndividualSmall.Src = logoIndividualMedium.Src = ConfigurationManager.AppSettings["ThemeImageAssetPath"] + (siteUser.IsSchneiderTheme ? "se-small.png" : "cw-gears.png");
        }

        protected string GetUrl()
        {
            return (siteUser.IsSchneiderTheme ? ConfigurationManager.AppSettings["SchneiderElectricSiteAddress"] : ConfigurationManager.AppSettings["SiteAddress"]).ToString();
        }
    }
}