﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class BuildingGroupAdministration: SitePage
    {
        #region Properties

            private BuildingGroup mBuildingGroup;

            const string addBuildingGroupSuccess = " building group addition was successful.";
            const string addBuildingGroupFailed = "Adding building group failed. Please contact an administrator.";
            const string buildingsRequired = "Two or more buildings are required for a building group.";
            const string updateSuccessful = "Building group update was successful.";
            const string updateFailed = "Building group update failed. Please contact an administrator.";
            const string deleteSuccessful = "Building group deletion was successful.";
            const string deleteFailed = "Building group deletion failed. Please contact an administrator.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "BuildingGroupName";
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            private void Page_Init()
            {
                //logged in security check in master page
                //secondary security check 
                //Check kgs full admin or higher, or a super admin.
                if (!(siteUser.IsKGSFullAdminOrHigher || siteUser.IsSuperAdmin))
                    Response.Redirect("/Home.aspx");
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                //hide labels
                lblErrors.Visible = false;

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindBuildingGroups();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litAdminBuildingGroupsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.AdminBuildingGroupsBody);

                    sessionState["Search"] = String.Empty;

                    BindAddBuildingGroups();
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetBuildingGroupIntoEditForm(CW.Data.BuildingGroup buildingGroup)
            {
                //ID
                hdnEditID.Value = Convert.ToString(buildingGroup.BuildingGroupID);
                //Buiding Group Name
                txtEditBuildingGroupName.Text = buildingGroup.BuildingGroupName;
            }

        #endregion

        #region Load Building Groups

            protected void LoadAddFormIntoBuildingGroup(BuildingGroup buildingGroup)
            {
                //Building Group Name
                buildingGroup.BuildingGroupName = txtAddBuildingGroupName.Text;
                //ClientID
                buildingGroup.CID = siteUser.CID;

                buildingGroup.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoBuildingGroup(CW.Data.BuildingGroup buildingGroup)
            {
                //ID
                buildingGroup.BuildingGroupID = Convert.ToInt32(hdnEditID.Value);
                //Building Group Name
                buildingGroup.BuildingGroupName = txtEditBuildingGroupName.Text;
            }

        #endregion

        #region Button Events

            /// <summary>
            /// on add button up click, add buildings not in group
            /// </summary>
            protected void btnAddBuildingsUpButton_Click(object sender, EventArgs e)
            {
                while (lbAddBottom.SelectedIndex != -1)
                {
                    lbAddTop.Items.Add(lbAddBottom.SelectedItem);
                    lbAddBottom.Items.Remove(lbAddBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on add button down click, add buildings not in group
            /// </summary>
            protected void btnAddBuildingsDownButton_Click(object sender, EventArgs e)
            {
                while (lbAddTop.SelectedIndex != -1)
                {
                    {
                        lbAddBottom.Items.Add(lbAddTop.SelectedItem);
                        lbAddTop.Items.Remove(lbAddTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on edit button down click, add buildings not in group
            /// </summary>
            protected void btnEditBuildingsDownButton_Click(object sender, EventArgs e)
            {
                //ListBox tempTopListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbTop");
                //ListBox tempBottomListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbBottom");

                while (lbEditTop.SelectedIndex != -1)
                {
                    {
                        lbEditBottom.Items.Add(lbEditTop.SelectedItem);
                        lbEditTop.Items.Remove(lbEditTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on edit button up click, add buildings not in group
            /// </summary>
            protected void btnEditBuildingsUpButton_Click(object sender, EventArgs e)
            {
                //ListBox tempTopListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbTop");
                //ListBox tempBottomListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbBottom");

                while (lbEditBottom.SelectedIndex != -1)
                {
                    lbEditTop.Items.Add(lbEditBottom.SelectedItem);
                    lbEditBottom.Items.Remove(lbEditBottom.SelectedItem);
                }
            }

            /// <summary>
            /// Add building group Button on click.
            /// </summary>
            protected void addBuildingGroupButton_Click(object sender, EventArgs e)
            {
                int buildingGroupID;

                mBuildingGroup = new BuildingGroup();

                try
                {
                    //two or more
                    if (lbAddBottom.Items.Count > 1)
                    {
                        //load the form into the building group
                        LoadAddFormIntoBuildingGroup(mBuildingGroup);

                        //insert new building group
                        buildingGroupID = DataMgr.BuildingGroupDataMapper.InsertBuildingGroup(mBuildingGroup);

                        try
                        {                           
                            //addes each buildingsgroup_building 
                            //no need to check if they already exists since its the first addition
                            foreach (ListItem item in lbAddBottom.Items)
                            {
                                //declare new buildinggroup_building association
                                BuildingGroups_Building mBuildingGroup_Building = new BuildingGroups_Building();

                                //set values                                
                                mBuildingGroup_Building.BID = Convert.ToInt32(item.Value);
                                mBuildingGroup_Building.BuildingGroupID = buildingGroupID;

                                //insert
                                DataMgr.BuildingGroupDataMapper.InsertBuildingGroupBuilding(mBuildingGroup_Building);
                            }
                            lblAddError.Text = mBuildingGroup.BuildingGroupName + addBuildingGroupSuccess;
                            lblAddError.Visible = true;
                            lnkSetFocusAdd.Focus();
                        }
                        catch (SqlException sqlEx)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding buildinggroup_buildings for user admin page.", sqlEx);
                            LabelHelper.SetLabelMessage(lblAddError, addBuildingGroupFailed, lnkSetFocusAdd);
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding buildinggroup_buildings for user admin page.", ex);
                            LabelHelper.SetLabelMessage(lblAddError, addBuildingGroupFailed, lnkSetFocusAdd);
                        }
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblAddError, buildingsRequired, lnkSetFocusAdd);
                    }
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building group.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addBuildingGroupFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building group", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addBuildingGroupFailed, lnkSetFocusAdd);
                }
            }


            /// <summary>
            /// Update Building Group Button on click.
            /// </summary>
            protected void updateBuildingGroupButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the bulding group
                CW.Data.BuildingGroup mBuildingGroup = new CW.Data.BuildingGroup();

                //try to update the building group              
                try
                {
                    //two or more
                    if (lbEditBottom.Items.Count > 1)
                    {
                        LoadEditFormIntoBuildingGroup(mBuildingGroup);

                        //update building group
                        DataMgr.BuildingGroupDataMapper.UpdatePartialBuildingGroup(mBuildingGroup);

                        //deletes all building group buildings
                        DataMgr.BuildingGroupDataMapper.DeleteAllBuidingGroupBuildingsByBuildingGroupID(mBuildingGroup.BuildingGroupID);

                        try
                        {
                            //updating each buildings group id in the bottom listbox
                            foreach (ListItem item in lbEditBottom.Items)
                            {
                                //declare new buildinggroup_building association and 
                                BuildingGroups_Building mBuildingGroup_Building = new BuildingGroups_Building();

                                //set values                            
                                mBuildingGroup_Building.BID = Convert.ToInt32(item.Value);
                                mBuildingGroup_Building.BuildingGroupID = mBuildingGroup.BuildingGroupID;

                                //insert
                                DataMgr.BuildingGroupDataMapper.InsertBuildingGroupBuilding(mBuildingGroup_Building);
                            }

                            //lbBottom = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbTop");

                            //DERPICATED
                            //updating each buildings group id in the top listbox
                            //foreach (ListItem item in lbTop.Items)
                            //{
                            //    BuildingDataMapper.UpdatePartialBuilding(Convert.ToInt32(item.Value), null);
                            //}

                            LabelHelper.SetLabelMessage(lblErrors, updateSuccessful, lnkSetFocusView);

                            //Bind buildings again
                            BindBuildingGroups();
                        }
                        catch (SqlException sqlEx)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building group buidings for user admin page.", sqlEx);
                            LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building group buildings for user admin page.", ex);
                            LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                        }                    
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblEditError, buildingsRequired, lnkSetFocusView);
                    }
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building group.", ex);
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindBuildingGroups();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindBuildingGroups();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// On Init for Tabs
            /// </summary>
            protected void loadTabs(object sender, EventArgs e)
            {
                //if not client or provider proxy super admin or higher hide the add building group tab
                radTabStrip.Tabs[1].Visible = siteUser.IsKGSFullAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient && siteUser.IsProxyClient) || (siteUser.IsSuperAdmin && !siteUser.IsLoggedInUnderProviderClient);

                //disable add buttons only, for enter key search submit action. edit buttons already disable within view tabs panel
                //btnAddUp.Enabled = false;
                //btnAddDown.Enabled = false;
            }

            /// <summary>
            /// Tab changed event, bind building group grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind building groups to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindBuildingGroups();

                    //disable add buttons only, for enter key search submit action. edit buttons already disable within view tabs panel
                    //btnAddUp.Enabled = false;
                    //btnAddDown.Enabled = false;
                }
                //bind available buildings to listbox on add building group page
                //else
                //{
                //    btnAddUp.Enabled = true;
                //    btnAddDown.Enabled = true;

                //    //clear listboxs
                //    lbAddTop.Items.Clear();
                //    lbAddBottom.Items.Clear();
                //}
            }

        #endregion

        #region Grid Events

            protected void gridBuildingGroups_OnDataBound(object sender, EventArgs e)
            {
                GridViewRowCollection rows = gridBuildingGroups.Rows;

                //hide edit and delete links if user role is is not client or provider proxy super admin
                gridBuildingGroups.Columns[1].Visible = gridBuildingGroups.Columns[4].Visible = siteUser.IsKGSFullAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient && siteUser.IsProxyClient) || (siteUser.IsSuperAdmin && !siteUser.IsLoggedInUnderProviderClient);

                foreach (GridViewRow row in rows)
                {
                    int buildingGroupID = Convert.ToInt32(gridBuildingGroups.DataKeys[row.RowIndex].Values["BuildingGroupID"]);

                    //Bind buildings for group
                    IEnumerable<CW.Data.Building> bldgs = DataMgr.BuildingDataMapper.GetAllBuildingsByBuildingGroupID(buildingGroupID);
                    //if edit bind to literal
                    if (((row.RowState & DataControlRowState.Edit) == 0) && bldgs.Any())
                    {
                        StringBuilder s = new StringBuilder();
                        s.Append("<ul>");

                        //Optimized loop                         
                        using (IEnumerator<CW.Data.Building> list = bldgs.GetEnumerator())
                        {
                            while (list.MoveNext())
                            {
                                CW.Data.Building bldg = (CW.Data.Building)list.Current;
                                s.Append("<li>" + bldg.BuildingName + "</li>");
                            }
                        }

                        s.Append("</ul>");

                        //Add list of buidings to literal
                        try
                        {
                            ((Literal)row.Cells[3].Controls[0]).Text = s.ToString();
                        }
                        catch
                        {
                        }
                    }

                    //OLD GRID EDITING----
                    //else if(!((row.RowState & DataControlRowState.Edit) == 0))
                    //{
                    //    ListBox lbTemp = new ListBox();
                    //    if (bldgs.Any())
                    //    {
                    //        //bind buildings in group to bottom listbox
                    //        lbTemp = ((ListBox)row.Cells[2].FindControl("lbBottom"));
                    //        lbTemp.DataSource = bldgs;
                    //        lbTemp.DataTextField = "BuildingName";
                    //        lbTemp.DataValueField = "BID";
                    //        lbTemp.DataBind();
                    //    }

                    //    //get buildings not in a group
                    //    IEnumerable<CW.Data.Building> bldgs2 = BuildingDataMapper.GetAllBuildingsNotInAGroupByClientID(Convert.ToInt32(Session["CID"].ToString()), buildingGroupID);
                        
                    //    if (bldgs2.Any())
                    //    {
                    //        //bind building not in a group to top listbox
                    //        lbTemp = ((ListBox)row.Cells[2].FindControl("lbTop"));
                    //        lbTemp.DataSource = bldgs2;
                    //        lbTemp.DataTextField = "BuildingName";
                    //        lbTemp.DataValueField = "BID";
                    //        lbTemp.DataBind();
                    //    }
                    //}
                }
            }

            protected void gridBuildingGroups_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditBuildingGroup.Visible = false;
                dtvBuildingGroup.Visible = true;

                int buildingGroupID = Convert.ToInt32(gridBuildingGroups.DataKeys[gridBuildingGroups.SelectedIndex].Values["BuildingGroupID"]);                

                //set data source
                dtvBuildingGroup.DataSource = DataMgr.BuildingGroupDataMapper.GetIEnumerableFullBuildingGroupByID(buildingGroupID);
                //bind building group to details view
                dtvBuildingGroup.DataBind();
            }

            protected void gridBuildingGroups_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            //OLD GRID EDITING-----
            //protected void gridBuildingGroups_Editing(object sender, GridViewEditEventArgs e)
            //{
            //    gridBuildingGroups.EditIndex = e.NewEditIndex;               

            //    //get selected dropdown value to set selected value after bound again
            //    //try
            //    //{
            //    //    //selectedValue = gridUsers.Rows[e.NewEditIndex].Cells[5].Text;
            //    //    //selectedValue = ((DataBoundLiteralControl)gridUsers.Rows[e.NewEditIndex].Cells[4].Controls[0]).Text;
            //    //    selectedValue = ((HiddenField)gridBuildingGroups.Rows[e.NewEditIndex].Cells[4].Controls[1]).Value;
            //    //}
            //    //catch
            //    //{
            //    //}

            //    //Bind data again keeping current page and sort mode
            //    DataTable dataTable = ConvertIEnumerableToDataTable(QueryBuildingGroups());
            //    gridBuildingGroups.PageIndex = gridBuildingGroups.PageIndex;
            //    gridBuildingGroups.DataSource = SortDataTable(dataTable as DataTable, true);
            //    gridBuildingGroups.DataBind();     
            //}

            protected void gridBuildingGroups_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvBuildingGroup.Visible = false;
                pnlEditBuildingGroup.Visible = true;

                int buildingGroupID = Convert.ToInt32(gridBuildingGroups.DataKeys[e.NewEditIndex].Values["BuildingGroupID"]);

                BuildingGroup mBuildingGroup = DataMgr.BuildingGroupDataMapper.GetBuildingGroupByID(buildingGroupID);

                //Set buildings data
                SetBuildingGroupIntoEditForm(mBuildingGroup);

                //Bind buildings for group
                IEnumerable<Building> bldgs = DataMgr.BuildingDataMapper.GetAllBuildingsByBuildingGroupID(buildingGroupID);

                //clear listboxes just incase no buildigns to bind
                lbEditBottom.Items.Clear();
                lbEditTop.Items.Clear();

                ListBox lbTemp = new ListBox();
                if (bldgs.Any())
                {
                    //bind buildings in group to bottom listbox
                    lbEditBottom.DataSource = bldgs;
                    lbEditBottom.DataTextField = "BuildingName";
                    lbEditBottom.DataValueField = "BID";
                    lbEditBottom.DataBind();
                }

                //get buildings not in a group
                IEnumerable<CW.Data.Building> bldgs2 = DataMgr.BuildingDataMapper.GetAllBuildingsNotInAGroupByClientID(siteUser.CID, buildingGroupID);

                if (bldgs2.Any())
                {
                    //bind building not in a group to top listbox
                    lbEditTop.DataSource = bldgs2;
                    lbEditTop.DataTextField = "BuildingName";
                    lbEditTop.DataValueField = "BID";
                    lbEditTop.DataBind();
                }

                //Cancels the edit auto grid selected. Fixes the select link diasspearing in selected mode, and the edit link performing the delete method.
                e.Cancel = true;
            }

            //OLD GRID UPDATING-----
            //protected void gridBuildingGroups_Updating(object sender, GridViewUpdateEventArgs e)
            //{
            //    //get updating row
            //    GridViewRow row = gridBuildingGroups.Rows[gridBuildingGroups.EditIndex];

            //    int buildingGroupID = Convert.ToInt32(gridBuildingGroups.DataKeys[e.RowIndex].Value);
            //    string buildingGroupName = ((TextBox)row.Cells[1].Controls[1]).Text;
            //    ListBox tempListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbBottom");

            //    //try to update the building group
            //    try
            //    {                    
            //        BuildingGroupDataMapper.UpdateBuildingGroup(buildingGroupID, buildingGroupName);

            //        try
            //        {
            //            //updating each buildings group id in the bottom listbox
            //            foreach (ListItem item in tempListBox.Items)
            //            {
            //                BuildingDataMapper.UpdatePartialBuilding(Convert.ToInt32(item.Value), buildingGroupID);
            //            }

            //            tempListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbTop");

            //            //updating each buildings group id in the top listbox
            //            foreach (ListItem item in tempListBox.Items)
            //            {
            //                BuildingDataMapper.UpdatePartialBuilding(Convert.ToInt32(item.Value), null);
            //            }
                        
            //            lblErrors.Text = updateSuccessful;
            //            lblErrors.Visible = true;
            //        }
            //        catch (SqlException sqlEx)
            //        {
            //            SectionLogManager.Log(DataConstants.LogLevel.ERROR, "Error updating each buildings client building group.", sqlEx);
            //            lblErrors.Text = updateFailed;
            //            lblErrors.Visible = true;

            //        }
            //        catch (Exception ex)
            //        {
            //            SectionLogManager.Log(DataConstants.LogLevel.ERROR, "Error updating each buildings client building group.", ex);
            //            lblErrors.Text = updateFailed;
            //            lblErrors.Visible = true;
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        lblErrors.Text = updateFailed;
            //        lblErrors.Visible = true;

            //        SectionLogManager.Log(DataConstants.LogLevel.ERROR, "Error updating client building group.", ex);
            //    }

            //    //Reset the edit index.
            //    gridBuildingGroups.EditIndex = -1;

            //    //Bind data again keeping current page and sort mode
            //    DataTable dataTable = ConvertIEnumerableToDataTable(QueryBuildingGroups());
            //    gridBuildingGroups.PageIndex = gridBuildingGroups.PageIndex;
            //    gridBuildingGroups.DataSource = SortDataTable(dataTable as DataTable, true);
            //    gridBuildingGroups.DataBind();
            //}

            protected void gridBuildingGroups_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows buildinggroupid
                int buildingGroupID = Convert.ToInt32(gridBuildingGroups.DataKeys[e.RowIndex].Value);

                try
                {
                    //delete all buildinggroups_buildings associations first.
                    //foreign key will restrict deleting the building group first.
                    DataMgr.BuildingGroupDataMapper.DeleteAllBuidingGroupBuildingsByBuildingGroupID(buildingGroupID);
                    DataMgr.BuildingGroupDataMapper.DeleteBuildingGroup(buildingGroupID);


                    lblErrors.Text = deleteSuccessful;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting building group.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryBuildingGroups());
                gridBuildingGroups.PageIndex = gridBuildingGroups.PageIndex;
                gridBuildingGroups.DataSource = SortDataTable(dataTable as DataTable, true);
                gridBuildingGroups.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //clear edit for if exists
                pnlEditBuildingGroup.Visible = false;
            }

            //OLD GRID CANCELING-----
            //protected void gridBuildingGroups_Canceling(object sender, GridViewCancelEditEventArgs e)
            //{   
            //    //Reset the edit index.
            //    gridBuildingGroups.EditIndex = -1;

            //    //Bind data again keeping current page and sort mode
            //    DataTable dataTable = ConvertIEnumerableToDataTable(QueryBuildingGroups());
            //    gridBuildingGroups.PageIndex = gridBuildingGroups.PageIndex;
            //    gridBuildingGroups.DataSource = SortDataTable(dataTable as DataTable, true);
            //    gridBuildingGroups.DataBind();   
            //}

            protected void gridBuildingGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBuildingGroups(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridBuildingGroups.DataSource = SortDataTable(dataTable, true);
                gridBuildingGroups.PageIndex = e.NewPageIndex;
                gridBuildingGroups.DataBind();
            }

            protected void gridBuildingGroups_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridBuildingGroups.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBuildingGroups(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridBuildingGroups.DataSource = SortDataTable(dataTable, false);
                gridBuildingGroups.DataBind();
            }
        
        #endregion

        #region Dropdown and Checkbox events

            /// <summary>
            /// On initialize of the edit role dropdown, bind all roles greater then current users.
            /// Do not reload on autopostback.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            //protected void ddlEdit_OnInit(object sender, EventArgs e)
            //{
            //    DropDownList ddl = (DropDownList)sender;              

            //    ddl.DataTextField = "Role";
            //    ddl.DataValueField = "RoleID";                
            //    ddl.DataSource = UserRoleDataMapper.GetAllRolesGreaterThan(Convert.ToInt32(Session["UserRoleID"]));
            //    ddl.DataBind();
                
            //    ddl.SelectedValue = selectedValue;
            //}

        #endregion

        #region Helper Methods

            private IEnumerable<BuildingGroup> QueryBuildingGroups()
            {
                try
                {
                    //get all building groups by client id                    
                    return DataMgr.BuildingGroupDataMapper.GetAllBuildingsGroupsByCID(siteUser.CID);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving client building groups.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving client building groups.", ex);
                    return null;
                }
            }

            private IEnumerable<BuildingGroup> QuerySearchedBuildingGroups(string searchText)
            {
                try
                {
                    //get all building groups by client id                    
                    return DataMgr.BuildingGroupDataMapper.GetAllSearchedBuildingsGroupsByCID(searchText, siteUser.CID);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving client building groups.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving client building groups.", ex);
                    return null;
                }
            }

            private void BindBuildingGroups()
            {
                //query groups
                IEnumerable<BuildingGroup> groups = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryBuildingGroups() : QuerySearchedBuildingGroups(txtSearch.Text);

                int count = groups.Count();

                //maintain sort--- doesnt work!
                //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
                //gridUsers.Sort(GridViewSortExpression, tempSD);

                gridBuildingGroups.DataSource = groups;

                // bind grid
                gridBuildingGroups.DataBind();

                SetGridCountLabel(count);

                //set search visibility. dont remove when last result from searched results in the grid is deleted.
                pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && count == 0) ? false : true;
            }

            //private void BindSearchedBuildingGroups(string[] searchText)
            //{
            //    //query groups
            //    IEnumerable<BuildingGroup> groups = QuerySearchedBuildingGroups(searchText);

            //    int count = groups.Count();

            //    //maintain sort--- doesnt work!
            //    //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
            //    //gridUsers.Sort(GridViewSortExpression, tempSD);

            //    gridBuildingGroups.DataSource = groups;

            //    // bind grid
            //    gridBuildingGroups.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void BindAddBuildingGroups()
            {
                //get buildings by client ID
                IEnumerable<CW.Data.Building> bldgs2 = siteUser.VisibleBuildings;

                //bind building not in a group to top listbox
                lbAddTop.DataSource = bldgs2;
                lbAddTop.DataTextField = "BuildingName";
                lbAddTop.DataValueField = "BID";
                lbAddTop.DataBind();
            }

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} building group found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} building groups found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No building groups found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }  

        #endregion
    }
}