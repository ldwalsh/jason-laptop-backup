﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using CW.Business;
using CW.Data;
using CW.Website._framework;
using CW.Utility;

namespace CW.Website
{
    public partial class Privacy : SitePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //logged in security check in master page

            //set admin global settings
            //Make sure to decode from ascii to html 
            GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
            litPrivacyPolicyBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.PrivacyPolicyBody);
        }
    }
}