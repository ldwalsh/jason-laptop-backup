﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.DataSourceRetrievalMethod;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class SystemRetrievalMethodAdministration : SitePage
    {
        #region Properties

            private DataSourceRetrievalMethod mRetrievalMethod;
            const string addRetrievalMethodSuccess = " retrieval method addition was successful.";
            const string addRetrievalMethodFailed = "Adding retrieval method failed. Please contact an administrator.";
            const string addRetrievalMethodNameExists = "Cannot add retrieval method because the name already exists."; 
            const string updateSuccessful = "Retrieval method update was successful.";
            const string updateFailed = "Retrieval method update failed. Please contact an administrator.";
            const string updateRetrievalMethodNameExists = "Cannot update retrieval method name because the name already exists.";     
            const string deleteSuccessful = "Retrieval method deletion was successful.";
            const string deleteFailed = "Retrieval method deletion failed. Please contact an administrator.";
            const string associatedToDataSource = "Cannot delete retrieval method becuase a data source requires it.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "RetrievalMethod";
            const string retrievalMethodExists = "Retrieval method with provided RetrievalMethodID already exists.";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs system admin.
                if (!siteUser.IsKGSSystemAdmin)
                    Response.Redirect("/Home.aspx");   

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindRetrievalMethods();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetRetrievalMethodIntoEditForm(CW.Data.DataSourceRetrievalMethod retrievalMethod)
            {
                //ID
                hdnEditID.Value = Convert.ToString(retrievalMethod.RetrievalMethodID);
                //Retrieval Method Name
                txtEditRetrievalMethod.Text = retrievalMethod.RetrievalMethod;
                //Equipment Variable Description
                txtEditDescription.Value = String.IsNullOrEmpty(retrievalMethod.RetrievalMethodDescription) ? null : retrievalMethod.RetrievalMethodDescription;
                //Is Active
                chkEditActive.Checked = retrievalMethod.IsActive;

            }

        #endregion

        #region Load Retrieval Method

            protected void LoadAddFormIntoRetrievalMethod(DataSourceRetrievalMethod retrievalMethod)
            {
                //Vendor Product Name
                retrievalMethod.RetrievalMethod = txtAddRetrievalMethod.Text;
                //Equipment Variable Description
                retrievalMethod.RetrievalMethodDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                //Is Active
                retrievalMethod.IsActive = true;

                retrievalMethod.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoRetrievalMethod(DataSourceRetrievalMethod retrievalMethod)
            {
                //ID
                retrievalMethod.RetrievalMethodID = Convert.ToInt32(hdnEditID.Value);
                //Vendor Product Name
                retrievalMethod.RetrievalMethod = txtEditRetrievalMethod.Text;
                //Equipment Variable Description
                retrievalMethod.RetrievalMethodDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //Is Active
                retrievalMethod.IsActive = chkEditActive.Checked;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add retrieval method Button on click.
            /// </summary>
            protected void addRetrievalMethodButton_Click(object sender, EventArgs e)
            {
                mRetrievalMethod = new DataSourceRetrievalMethod();

                //load the form into the vendor product
                LoadAddFormIntoRetrievalMethod(mRetrievalMethod);

                try
                {
                    //insert new vendor product
                    DataMgr.DataSourceRetrievalMethodDataMapper.InsertRetrievalMethod(mRetrievalMethod);

                    LabelHelper.SetLabelMessage(lblAddError, mRetrievalMethod.RetrievalMethod + addRetrievalMethodSuccess, lnkSetFocusAdd);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding retrieval method.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addRetrievalMethodFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding retrieval method.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addRetrievalMethodFailed, lnkSetFocusAdd);
                }
                
            }

            /// <summary>
            /// Update retrieval method Button on click. Updates retrieval method data.
            /// </summary>
            protected void updateRetrievalMethodButton_Click(object sender, EventArgs e)
            {
                DataSourceRetrievalMethod mRetrievalMethod = new DataSourceRetrievalMethod();

                //load the form into the retrieval method
                LoadEditFormIntoRetrievalMethod(mRetrievalMethod);

                //try to update the retrieval methods             
                try
                {
                    DataMgr.DataSourceRetrievalMethodDataMapper.UpdateRetrievalMethod(mRetrievalMethod);

                    LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                    //Bind vendor products again
                    BindRetrievalMethods();
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating retrieval method.", ex);
                }
             
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindRetrievalMethods();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindRetrievalMethods();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind equipment variable grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind equipment variables to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindRetrievalMethods();
                }
            }

        #endregion

        #region Grid Events

            protected void gridRetrievalMethods_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridRetrievalMethods_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditRetrievalMethod.Visible = false;
                dtvRetrievalMethod.Visible = true;
                
                int retrievalMethodID = Convert.ToInt32(gridRetrievalMethods.DataKeys[gridRetrievalMethods.SelectedIndex].Values["RetrievalMethodID"]);

                //set data source
                dtvRetrievalMethod.DataSource = DataMgr.DataSourceRetrievalMethodDataMapper.GetFullRetrievalMethodByID(retrievalMethodID);
                //bind vendor product to details view
                dtvRetrievalMethod.DataBind();
            }

            protected void gridRetrievalMethods_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridRetrievalMethods_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvRetrievalMethod.Visible = false;
                pnlEditRetrievalMethod.Visible = true;

                int retrievalMethodID = Convert.ToInt32(gridRetrievalMethods.DataKeys[e.NewEditIndex].Values["RetrievalMethodID"]);

                DataSourceRetrievalMethod mRetrievalMethod = DataMgr.DataSourceRetrievalMethodDataMapper.GetRetrievalMethod(retrievalMethodID);

                //Set retrieval method data
                SetRetrievalMethodIntoEditForm(mRetrievalMethod);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridRetrievalMethods_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows equipmentVariableid
                int retrievalMethodID = Convert.ToInt32(gridRetrievalMethods.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a retrieval method if it as not been assocaited with
                    //a data source

                    //check if any vendor product is associated with this retrieval method
                    if (DataMgr.DataSourceVendorProductDataMapper.IsVendorProductRetrievalMethodAssociatedWithAnyVendorProducts(retrievalMethodID))
                    {
                        lblErrors.Text = associatedToDataSource;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }

                    else
                    {
                        //delete equipment variable
                        DataMgr.DataSourceRetrievalMethodDataMapper.DeleteDataSourceRetrievalMethod(retrievalMethodID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting retrieval method.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryRetrievalMethods());
                gridRetrievalMethods.PageIndex = gridRetrievalMethods.PageIndex;
                gridRetrievalMethods.DataSource = SortDataTable(dataTable as DataTable, true);
                gridRetrievalMethods.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditRetrievalMethod.Visible = false;
            }

            protected void gridRetrievalMethods_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedRetrievalMethods(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridRetrievalMethods.DataSource = SortDataTable(dataTable, true);
                gridRetrievalMethods.PageIndex = e.NewPageIndex;
                gridRetrievalMethods.DataBind();
            }

            protected void gridRetrievalMethods_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridRetrievalMethods.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedRetrievalMethods(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridRetrievalMethods.DataSource = SortDataTable(dataTable, false);
                gridRetrievalMethods.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetRetrievalMethodData> QueryRetrievalMethods()
            {
                try
                {
                    //get all equipment variables 
                    return DataMgr.DataSourceRetrievalMethodDataMapper.GetAllDataSourceRetrievalMethodsWithPartialData();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving retrieval methods.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving retrieval methods.", ex);
                    return null;
                }
            }

            private IEnumerable<GetRetrievalMethodData> QuerySearchedRetrievalMethods(string searchText)
            {
                try
                {
                    //get all equipment variables 
                    return DataMgr.DataSourceRetrievalMethodDataMapper.GetAllSearchedDataSourceRetrievalMethodsWithPartialData(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving retrieval methods.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving retrieval methods.", ex);
                    return null;
                }
            }

            private void BindRetrievalMethods()
            {
                //query equipment variables
                IEnumerable<GetRetrievalMethodData> variables = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryRetrievalMethods() : QuerySearchedRetrievalMethods(txtSearch.Text);

                int count = variables.Count();

                gridRetrievalMethods.DataSource = variables;

                // bind grid
                gridRetrievalMethods.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedRetrievalMethods(string[] searchText)
            //{
            //    //query equipment variables
            //    IEnumerable<GetRetrievalMethodData> variables = QuerySearchedRetrievalMethods(searchText);

            //    int count = variables.Count();

            //    gridRetrievalMethods.DataSource = variables;

            //    // bind grid
            //    gridRetrievalMethods.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} retrieval method found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} retrieval methods found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No retrieval methods found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
      
        #endregion
    }
}

