﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.Equipment;
using System;

namespace CW.Website
{
    public partial class ProviderEquipmentAdministration : AdminSitePageTemp
    {
        #region events

            private void Page_Init()
            {
                EquipmentVariables.AdminMode = EquipmentVariables.AdminModeEnum.PROVIDER;
            }

            private void Page_FirstLoad()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs full admin or higher., or superadmin provider            
                radTabStrip.Tabs[1].Visible = siteUser.IsKGSFullAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient);            
            }

        #endregion

        public enum TabMessages
        {
            AddEquipment,
            EditEquipment,
            DeleteEquipment
        }

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewEquipment).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.PROVIDER; } }

            #endregion

        #endregion
    }
}