﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSFileClassAdministration.aspx.cs" Inherits="CW.Website.KGSFileClassAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>KGS File Class Administration</h1>                        
                <div class="richText">The kgs file class administration area is used to view, add, and edit file classes.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View File Classes"></telerik:RadTab>
                            <telerik:RadTab Text="Add File Class"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View File Classes</h2>
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                                            <asp:ValidationSummary ID="valSummary" ShowSummary="true" ValidationGroup="EditFileClasses" runat="server" />
                                    </p>           
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridFileClasses"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="FileClassID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridFileClasses_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridFileClasses_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridFileClasses_Sorting" 
                                             OnSelectedIndexChanged="gridFileClasses_OnSelectedIndexChanged"                                                                                                              
                                             OnRowEditing="gridFileClasses_Editing"  
                                             OnRowDeleting="gridFileClasses_Deleting"
                                             OnDataBound="gridFileClasses_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FileClassName" HeaderText="File Class">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("FileClassName"),50) %></ItemTemplate>                          
                                                </asp:TemplateField> 
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="File Class Description">                        
                                                     <ItemTemplate><label title="<%# Eval("FileClassDescription") %>"><%# StringHelper.TrimText(Eval("FileClassDescription"),100) %></label></ItemTemplate>   
                                                </asp:TemplateField>                                                     
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this file class permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                                  
                                    <!--SELECT REPOSITORY FILE CLASS DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvFileClass" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>File Class Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>File Class Name: </strong>" + Eval("FileClassName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("FileClassDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("FileClassDescription") + "</li>"%>                                                                                                                                                                                                             	                                                                	                                                                                                                            
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    <!--EDIT REPOSITORY FILE PANEL -->                              
                                    <asp:Panel ID="pnlEditFileClass" runat="server" Visible="false" DefaultButton="btnUpdateFileClass">                                                                                             
                                        <div>
                                            <h2>Edit File Class</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*File Class Name:</label>
                                                <asp:TextBox ID="txtEditFileClassName" MaxLength="50" runat="server"></asp:TextBox>                                  
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateFileClass" runat="server" Text="Edit Class"  OnClick="updateFileClassButton_Click" ValidationGroup="EditFileClass"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editFileClassNameRequiredValidator" runat="server"
                                        ErrorMessage="File Class Name is a required field."
                                        ControlToValidate="txtEditFileClassName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditFileClass">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editFileClassNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editFileClassNameRequiredValidatorExtender"
                                        TargetControlID="editFileClassNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                    </asp:Panel>                                       
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddFileClass" runat="server" DefaultButton="btnAddFileClass">  
                                    <h2>Add File Class</h2>
                                    <div>                            
                                        <a id="lnkSetFocusAdd" runat="server"></a>                        
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*File Class Name:</label>
                                            <asp:TextBox ID="txtAddFileClassName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Description:</label>
                                            <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                            <div id="divAddDescriptionCharInfo"></div>
                                        </div>                                                                                              
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddFileClass" runat="server" Text="Add Class"  OnClick="addFileClassButton_Click" ValidationGroup="AddFileClass"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="fileClassNameRequiredValidator" runat="server"
                                        ErrorMessage="File Class Name is a required field."
                                        ControlToValidate="txtAddFileClassName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddFileClass">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="fileClassNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="fileClassNameRequiredValidatorExtender"
                                        TargetControlID="fileClassNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>                                                                                                                                                                   
                            </telerik:RadPageView>                                                  
                        </telerik:RadMultiPage>
                   </div>                                                                 
                   
</asp:Content>


                    
                  
