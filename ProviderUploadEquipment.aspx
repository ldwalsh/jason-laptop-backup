﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.Master" AutoEventWireup="false" CodeBehind="ProviderUploadEquipment.aspx.cs" Inherits="CW.Website.ProviderUploadEquipment" %>
<%@ Register src="~/_controls/upload/UploadHeader.ascx" tagname="UploadHeader" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/UploadFormat.ascx" tagname="UploadFormat" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupsEquipmentUpload.ascx" tagname="IDLookupsEquipmentUpload" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/Upload.ascx" tagname="Upload" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <CW:UploadHeader ID="UploadHeader" runat="server" />
  <CW:UploadFormat ID="UploadFormat" runat="server" />
  <CW:IDLookupsEquipmentUpload ID="IDLookupsEquipmentUpload" runat="server" />
  <CW:Upload ID="Upload" runat="server" />

</asp:Content>