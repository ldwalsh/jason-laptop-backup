﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using CW.Utility;
using CW.Data;
using CW.Business;
using CW.Website._framework;

namespace CW.Website
{
    public partial class Profiles : SitePage
    {
        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //if the page is not a postback, set initial global settings
                if (!Page.IsPostBack)
                {
                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litProfilesBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.ProfilesBody);
                }
            }

        #endregion
    }
}
