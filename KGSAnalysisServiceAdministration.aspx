﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="true" CodeBehind="KGSAnalysisServiceAdministration.aspx.cs" Inherits="CW.Website.KGSAnalysisServiceAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

     <div id="intro" class="pod">
    	<div class="top"> </div>
        <div id="middle" class="middle">                    	                  	        	
        	<h1>Analysis Service</h1>                        
            <div class="richText">
                The analysis service page is designed to select and perform web service actions on the system.
            </div>
            <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>                                       
                            Loading...
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
            </div>     
                <div>
                    <p>
                        <a id="lnkSetFocus" runat="server"></a>
                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                    </p>    
                    <div class="divForm">
                         <label class="label">Analysis Service Method:</label>
                         <asp:DropDownList CssClass="dropdown" ID="ddlWebServices" AppendDataBoundItems="true" runat="server" 
                                OnSelectedIndexChanged="ddlWebServices_OnSelectedIndexChanged" AutoPostBack="true">  
                            <asp:ListItem Value="-1">Select one...</asp:ListItem>   
                            <asp:ListItem Value="DebugOverrideSpecifiedAnalysis">Debug Override Specified Analysis</asp:ListItem> 
                            <asp:ListItem Value="DebugScheduledAnalyses">Debug Scheduled Analyses</asp:ListItem> 
                            <asp:ListItem Value="DebugSpecifiedAnalysis">Debug Specified Analysis</asp:ListItem> 
                            <asp:ListItem Value="OverrideSpecifiedAnalysis">Override Specified Analysis</asp:ListItem> 
                            <asp:ListItem Value="PurgeAllAnalyses">Purge All Analyses</asp:ListItem> 
                            <asp:ListItem Value="PurgeSpecifiedAnalysis">Purge Specified Analysis</asp:ListItem> 
                            <asp:ListItem Value="ScheduledAnalyses">Scheduled Analyses</asp:ListItem> 
                            <asp:ListItem Value="SpecifiedAnalysis">Specified Analysis</asp:ListItem> 
                            <asp:ListItem Value="TestScheduledAnalyses">Test Scheduled Analyses</asp:ListItem> 
                            <asp:ListItem Value="TestSpecifiedAnalysis">Test Specified Analysis</asp:ListItem> 
                         </asp:DropDownList>                                              
                    </div>      
                
                    <p id="webServiceDescription" runat="server">
                    </p>
                
                    <asp:Panel runat="server" ID="pnlDates" Visible="false">
                        <div class="divForm">
                            <label class="label">Manual Start Date:</label>
                            <asp:TextBox ID="txtManualStartDate" CssClass="textbox" runat="server"></asp:TextBox>
                            <asp:ImageButton ID="btnStartCal" CssClass="imgCal" ImageUrl="_assets/images/cal.png" runat="server" CausesValidation="false" />   
                            <ajaxToolkit:CalendarExtender ID="startCalExtender" runat="server"
                                TargetControlID="txtManualStartDate"        
                                Format="MM/dd/yyyy"
                                PopupPosition="Right"  
                                PopupButtonID="btnStartCal"   
                                OnClientShown="onCalendarShown"                         
                                />                         
                            <ajaxToolkit:MaskedEditExtender ID="startMaskedEditExtender" runat="server"
                                    TargetControlID="txtManualStartDate" 
                                    Mask="99/99/9999"
                                    AutoComplete="true"                                
                                    MaskType="Date" 
                                    InputDirection="LeftToRight"                                 
                                    />     
                            <ajaxToolkit:MaskedEditValidator ID="startMaskedEditValidator" runat="server"                                 
                                    ControlExtender="startMaskedEditExtender" 
                                    ControlToValidate="txtManualStartDate" 
                                    InvalidValueMessage="Invalid Date"  
                                    EmptyValueMessage="Date is required" 
                                    IsValidEmpty="False" 
                                    ValidationGroup="WebServices" 
                                    SetFocusOnError="true"
                                    >
                                    </ajaxToolkit:MaskedEditValidator>
                        </div>
                        <div class="divForm">
                            <label class="label">Date Range:</label>
                            <asp:DropDownList ID="ddlDateRange" CssClass="dropdown" runat="server">
                                    <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                    <asp:ListItem Value="daily">Daily</asp:ListItem>                                 
                                    <asp:ListItem Value="weekly">Weekly</asp:ListItem>                                 
                                    <asp:ListItem Value="monthly">Monthly</asp:ListItem>                                 
                            </asp:DropDownList> 
                        </div>
                        <asp:RequiredFieldValidator ID="dateRangeRequiredValidator" runat="server"
                            ErrorMessage="Date Range is a required field." 
                            ControlToValidate="ddlDateRange"  
                            SetFocusOnError="true" 
                            Display="None" 
                            InitialValue="-1"
                            ValidationGroup="WebServices">
                            </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="dateRangeRequiredValidatorExtender" runat="server"
                            BehaviorID="dateRangeRequiredValidatorExtender" 
                            TargetControlID="dateRangeRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>  
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlIds" Visible="false">
                        <div class="divForm">   
                            <label class="label">Select Client:</label>    
                            <asp:DropDownList ID="ddlClient" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlClient_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                    <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                            </asp:DropDownList> 
                        </div>                                                  
                        <div class="divForm">   
                            <label class="label">Select Building:</label>    
                            <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                    <asp:ListItem Value="-1">Select client first...</asp:ListItem>                                 
                            </asp:DropDownList> 
                        </div>                                                                                                         
                        <div class="divForm">   
                            <label class="label">Select Equipment:</label>    
                            <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                                                                
                                <asp:ListItem Value="-1">Select building first...</asp:ListItem>                             
                            </asp:DropDownList> 
                        </div>  

                        <div class="divForm">   
                            <label class="label">Select Analysis:</label>    
                            <asp:DropDownList ID="ddlAnalysis" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalysis_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                                                                
                                <asp:ListItem Value="-1">Select equipment first...</asp:ListItem>                             
                            </asp:DropDownList> 
                        </div>  

                        <asp:RequiredFieldValidator ID="clientRequiredValidator" runat="server"
                            ErrorMessage="Client is a required field." 
                            ControlToValidate="ddlClient"  
                            SetFocusOnError="true" 
                            Display="None" 
                            InitialValue="-1"
                            ValidationGroup="WebServices">
                            </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="clientRequiredValidatorExtender" runat="server"
                            BehaviorID="clientRequiredValidatorExtender" 
                            TargetControlID="clientRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>      
                        <asp:RequiredFieldValidator ID="buildingRequiredValidator" runat="server"
                            ErrorMessage="Building is a required field." 
                            ControlToValidate="ddlBuilding"  
                            SetFocusOnError="true" 
                            Display="None" 
                            InitialValue="-1"
                            ValidationGroup="WebServices">
                            </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingRequiredValidatorExtender" runat="server"
                            BehaviorID="buildingRequiredValidatorExtender" 
                            TargetControlID="buildingRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>         
                        <asp:RequiredFieldValidator ID="equipmentRequiredValidator" runat="server"
                            ErrorMessage="Equipment is a required field." 
                            ControlToValidate="ddlEquipment"  
                            SetFocusOnError="true" 
                            Display="None" 
                            InitialValue="-1"
                            ValidationGroup="WebServices">
                            </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="equipmentRequiredValidatorExtender" runat="server"
                            BehaviorID="equipmentRequiredValidatorExtender" 
                            TargetControlID="equipmentRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                  
                        <asp:RequiredFieldValidator ID="analysisRequiredValidator" runat="server"
                            ErrorMessage="Analysis is a required field." 
                            ControlToValidate="ddlAnalysis"  
                            SetFocusOnError="true" 
                            Display="None" 
                            InitialValue="-1"
                            ValidationGroup="WebServices">
                            </asp:RequiredFieldValidator>
                        <ajaxToolkit:ValidatorCalloutExtender ID="analysisRequiredValidatorExtender" runat="server"
                            BehaviorID="analysisRequiredValidatorExtender" 
                            TargetControlID="analysisRequiredValidator"
                            HighlightCssClass="validatorCalloutHighlight"
                            Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>      
                    </asp:Panel>
                    <asp:Panel  runat="server" ID="pnlButtons" Visible="false">
                        <asp:LinkButton CssClass="lnk-button" ValidationGroup="WebServices" runat="server" Text="Submit" ID="btnSubmit" onclick="btnSubmit_Click" />
                    </asp:Panel>

                    <asp:Literal ID="litWebServiceResponse" Visible="false" runat="server" />                                                        
                </div>                 
          </div>                      
    </div>         
    
</asp:Content>                