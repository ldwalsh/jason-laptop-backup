﻿using AjaxControlToolkit;
using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage;
using CW.Data.AzureStorage.Models.Queue;
using CW.Data.Helpers;
using CW.Data.Models.Point;
using CW.Data.Models.VAndVPre;
using CW.Utility;
using CW.Utility.Web;
using CW.Reporting._fileservices;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using CW.Website.entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Telerik.Charting;


namespace CW.Website.Reports
{
    public partial class ReportCriteria : SiteUserControl, SiteUserControl.IClientControl
    {
        #region ENUM

        public enum ReportType
        {
            Raw,
            Analyzed,
            Calculated
        }

        public enum ChartMode
        {
            Simple,
            Advanced,
        }

        public enum DisplayRange
        {
            HalfDay,
            Daily,
            Weekly,
            Monthly,
        }

        #endregion

        #region IClientControl 

        Control IClientControl.ContainerControl
        {
            get { return pnlReportCriteria; }
        }

        #endregion

        private DropDownSequencer dropDownSequence;
        private ClientEntity _clientEntity;
        private const int rawSynchronizedDataMax = 3200; //32days * 100pts
        private const int rawDataMax = 6400; //64days * 100pts
        private const int vDataMax = 10950; //365days * 30vpts

        #region fields

        private Char[] delimeter = new Char[] { DataConstants.DefaultDelimiter };
        private Func<ListItem, Boolean> criteria = (_ => _.Selected);

        #endregion

        #region Page Events

        private void Page_Init()
        {
            dropDownSequence =
            new DropDownSequencer
            (
                new[] { ddlBuildings, ddlEquipmentClasses, ddlEquipment, ddlAnalysis }//,
            );
        }

        private void Page_FirstInit()
        {
            if (ID.EndsWith("Right")) //revise how CSS class determined based on ID (no good) instead look at position in document flow
            {
                pnlReportCriteria.CssClass = "reportRightColumn";

                foreach (var extendor in FindControls<ValidatorCalloutExtender>())
                {
                    extendor.PopupPosition = ValidatorCalloutPosition.BottomRight;
                }
            }
            else
            {
                pnlReportCriteria.CssClass = "reportLeftColumn";
            }

            ControlHelper.RadioButtonControl.CheckOption(rblReportType, ReportType.Raw.ToString());
            ControlHelper.RadioButtonControl.CheckOption(rblChartMode, ChartMode.Simple.ToString());

            BindBuildingDropdownList();

            dropDownSequence.ResetSubDropDownsOf(ddlBuildings);
        }

        private void Page_Load() { AttachToolTips(); }

        #endregion

        #region Load and Bind Fields

        private void BindBuildingDropdownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlBuildings, true, false, false);

            ddlBuildings.DataTextField = "BuildingName";
            ddlBuildings.DataValueField = "BID";
            ddlBuildings.DataSource = siteUser.VisibleBuildings;

            ddlBuildings.DataBind();
        }

        private void BindEquipmentClassesDropDownList()
        {
            if (reportType == ReportType.Raw)
            {
                DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipmentClasses, true, true, false);
            }
            else
            {
                DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipmentClasses, true, false, false);
            }

            ddlEquipmentClasses.DataTextField = "EquipmentClassName";
            ddlEquipmentClasses.DataValueField = "EquipmentClassID";
            ddlEquipmentClasses.DataSource = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(Convert.ToInt32(ddlBuildings.SelectedValue));

            ddlEquipmentClasses.DataBind();
        }

        private void BindEquipmentDropdownList()
        {
            //get all visible equipment, regardless of its active state because we stll need to plot if it was once active.
            var equipment = Convert.ToInt32(ddlEquipmentClasses.SelectedValue) == 0 ? DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBIDWithClassAndType(Convert.ToInt32(ddlBuildings.SelectedValue)) : DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBIDAndEquipmentClassIDWithClassAndType(Convert.ToInt32(ddlBuildings.SelectedValue), Convert.ToInt32(ddlEquipmentClasses.SelectedValue));

            if (reportType == ReportType.Raw)
            {
                //clear equipment
                lbEquipment.Items.Clear();
                lbEquipment.DataTextField = "EquipmentName";
                lbEquipment.DataValueField = "EID";
                lbEquipment.DataSource = equipment;
                lbEquipment.DataBind();

                //populate class and type in dropdown items by added a title attribute
                var counter = 0;
                foreach (var e in equipment)
                {
                    lbEquipment.Items[counter].Attributes.Add("title", String.Format("{0}, {1}", e.EquipmentType.EquipmentClass.EquipmentClassName, e.EquipmentType.EquipmentTypeName));
                    counter++;
                }
            }
            else
            {
                DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipment, true, false, false);

                ddlEquipment.DataTextField = "EquipmentName";
                ddlEquipment.DataValueField = "EID";
                ddlEquipment.DataSource = equipment;
                ddlEquipment.DataBind();

                //populate class and type in dropdown items by added a title attribute
                var counter = 0;
                foreach (var e in equipment)
                {
                    ddlEquipment.Items[counter].Attributes.Add("title", String.Format("{0}, {1}", e.EquipmentType.EquipmentClass.EquipmentClassName, e.EquipmentType.EquipmentTypeName));
                    counter++;
                }
            }
        }

        private void BindAnalysisDropdownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlAnalysis, true, false, false);

            ddlAnalysis.DataTextField = "AnalysisName";
            ddlAnalysis.DataValueField = "AID";

            //get all visible analyses, regardless of its active state because we stll need to plot if it was once active.
            var analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByEID(Convert.ToInt32(ddlEquipment.SelectedValue));
            ddlAnalysis.DataSource = analyses;
            ddlAnalysis.DataBind();

            //populate analysis teaser in dropdown items by added a title attribute
            int counter = 1;
            foreach (var a in analyses)
            {
                ddlAnalysis.Items[counter].Attributes.Add("title", a.AnalysisTeaser);
                counter++;
            }
        }

        private void BindAssociatedEquipmentListBox(ListBox lbAssociatedEquipment, ListBox equipmentListBox)
        {
            //clear assocaitedEquipment
            lbAssociatedEquipment.Items.Clear();
            lbAssociatedEquipment.DataTextField = "PointName";
            lbAssociatedEquipment.DataValueField = "PID";

            bool isMultipleEquipmentSelected = equipmentListBox.Items.Count > 1 ? true : false;

            //for multiple equipment add equiment in pointlistbox like the groups method below does
            foreach (ListItem equipment in equipmentListBox.Items)
            {
                if (!equipment.Selected) continue;

                BindAssociatedEquipmentFromEquipment(lbAssociatedEquipment, equipment);
            }
        }

        private void BindAssociatedEquipmentFromEquipment(ListBox lb, ListItem equipment)
        {
            //get all visible associated equipment, regardless of its active state because we stll need to plot if it was once active.
            var associatedEquip = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentAssociatedByPEID(Convert.ToInt32(equipment.Value), true);

            //populate associated equipment name, and primary equipment name, and equiptype in listbox items by adding a title attribute
            var counter = 0;
            foreach (var ae in associatedEquip)
            {
                var item = new ListItem();

                item.Text = ae.EquipmentName + " (" + equipment.Text + ")";
                item.Value = ae.EID.ToString();

                lb.Items.Add(item);

                lb.Items[counter].Attributes.Add("title", String.Format("{0}, {1}, {2}", ae.EquipmentName, ae.EquipmentType.EquipmentTypeName, equipment.Text));
                counter++;
            }
        }

        private void BindPointsListBox(ListBox lbPoints, ListBox equipmentListBox, ListBox associatedEquipmentListBox, DropDownList equipmentDropdownList = null)
        {
            //clear points
            lbPoints.Items.Clear();
            lbPoints.DataTextField = "PointName";
            lbPoints.DataValueField = "PID";

            if (equipmentDropdownList == null)
            {
                ListItem[] fullEquipmentList = (equipmentListBox.Items.Cast<ListItem>().ToArray().Concat(associatedEquipmentListBox.Items.Cast<ListItem>()).ToArray());

                BindPointsFromEquipment(lbPoints, fullEquipmentList);
            }
            else
            {
                BindPointsFromEquipment(lbPoints, new ListItem[] { equipmentDropdownList.SelectedItem });
            }
        }

        private void BindPointsFromEquipment(ListBox lb, ListItem[] fullEquipmentList)
        {
            var counter = 0;

            //for multiple equipment add equiment in pointlistbox like the groups method below does
            foreach (ListItem equipment in fullEquipmentList)
            {
                if (!equipment.Selected) continue;

                //TEMP:  All classes are currenly groups. Remove if we remove non group functionality.
                //if (!DataMgr.EquipmentDataMapper.IsEquipmentAssociatedWithClassGroup(Convert.ToInt32(equipment.Value)))
                //{
                //get all visible points, regardless of its active state because we stll need to plot if it was once active.
                var points = DataMgr.PointDataMapper.GetAllVisiblePointsByEID(Convert.ToInt32(equipment.Value), new Expression<Func<CW.Data.Point, Object>>[] { (p => p.PointType) });

                //populate point name, and type in listbox items by added a title attribute
                foreach (var p in points)
                {
                    var item = new ListItem();

                    item.Text = p.PointName + " (" + equipment.Text + ")";
                    item.Value = CreateListItemPointValue(p, Convert.ToInt32(equipment.Value), equipment.Text);

                    lb.Items.Add(item);

                    lb.Items[counter].Attributes.Add("title", String.Format("{0}, {1}, {2}", equipment.Text, p.PointName, p.PointType.DisplayName));
                    counter++;
                }
                //}
                //else
                //{
                //foreach (var p in DataMgr.PointDataMapper.GetAllVisiblePointsByEID(Convert.ToInt32(equipment.Value), (p => p.PointType)))
                //{
                //    var equipmentNames = DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointEquipmentNamesNotOfClassGroupByPID(p.PID);
                //    var item = new ListItem();

                //    item.Text = (equipmentNames.Count() >= 1) ? p.PointName + " (" + equipment.Text + " - " + equipmentNames[0] + ")" : p.PointName + " (" + equipment.Text + ")";
                //    item.Value = CreateListItemPointValue(p);

                //    lb.Items.Add(item);

                //    //populate point name, and type in listbox items by added a title attribute
                //    lb.Items[counter].Attributes.Add("title", String.Format("{0}, {1}, {2}", equipment.Text, p.PointName, p.PointType.PointTypeName));
                //    counter++;
                //}
                //}
            }
        }

        private void BindEquipmentListBox()
        {
            var equipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBIDAndEquipmentClassIDWithClassAndType(Convert.ToInt32(ddlBuildings.SelectedValue), Convert.ToInt32(ddlEquipmentClasses.SelectedValue));

            ClearAndSetListBox(lbCalculatedEquipment, PropHelper.G<Equipment>(_ => _.EquipmentName), PropHelper.G<Equipment>(_ => _.EID), equipment);
        }

        private void BindAnalysisListBox()
        {
            var eids = GetSelectedIds(lbCalculatedEquipment);
            var analysesEquipmentFromQuery = new AnalysesEquipmentQuery(DataMgr.ConnectionString, siteUser).LoadWith(_ => _.Analyse).LoadWith(_ => _.Equipment).FinalizeLoadWith.GetAll().ByBuilding(Convert.ToInt32(ddlBuildings.SelectedValue)).ByEquipment(eids).ToList();
            var analyses = analysesEquipmentFromQuery.Select(_ => new { AID = CreateValueFieldsForAnalysesListBox(_), DisplayName = String.Format("{0} ({1})", _.Analyse.AnalysisName, _.Equipment.EquipmentName) }).ToList();

            ClearAndSetListBox(lbCalculatedAnalyses, PropHelper.G<PointTypeDisplayNameAndVPID>(_ => _.DisplayName), PropHelper.G<Analyse>(_ => _.AID), analyses);
        }

        private void BindPointTypesListBox()
        {
            var aids = GetSelectedIds(lbCalculatedAnalyses, 0);
            var eids = GetSelectedIds(lbCalculatedAnalyses, 1);
            var vpointsFromQuery = new VPointQuery(DataMgr.ConnectionString, siteUser).LoadWith(_ => _.Analyse).LoadWith(_ => _.Equipment).LoadWith(_ => _.PointType).FinalizeLoadWith.GetAll().ByAnalysis(aids).ByEquipment(eids).ToList();
            var pointTypes = vpointsFromQuery.Select(_ => new { VPID = CreateValueFieldsForPointTypesListBox(_), DisplayName = String.Format("{0} ({1}, {2})", _.PointType.DisplayName, _.Equipment.EquipmentName, _.Analyse.AnalysisName) }).OrderBy(pt => pt.DisplayName).ToList();

            ClearAndSetListBox(lbCalculatedPoints, PropHelper.G<PointTypeDisplayNameAndVPID>(_ => _.DisplayName), PropHelper.G<PointTypeDisplayNameAndVPID>(_ => _.VPID), pointTypes);
        }

        private void BindChart()
        {
            IEnumerable<String> charts = null;

            switch (chartMode)
            {
                case ChartMode.Simple:
                    {
                        charts =
                        new[]
                        {
                        SeriesChartType.Area,
                        SeriesChartType.Bar,
                        SeriesChartType.BoxPlot,
                        //SeriesChartType.Candlestick,
                        SeriesChartType.Column,
                        //SeriesChartType.FastLine,
                        //SeriesChartType.FastPoint,
                        SeriesChartType.Line,
                        SeriesChartType.Point,
                        //SeriesChartType.Spline,
                        //SeriesChartType.SplineArea,
                        SeriesChartType.SplineRange,
                        SeriesChartType.StepLine,
                        SeriesChartType.Range,
                        SeriesChartType.RangeBar,
                        SeriesChartType.RangeColumn

                        }.Select(c => c.ToString());

                        break;
                    }
                case ChartMode.Advanced:
                    {
                        charts =
                        new[]
                        {
                        ChartSeriesType.Area,
                        ChartSeriesType.Bar,
                        //ChartSeriesType.Bezier,
                        ChartSeriesType.Bubble,
                        //ChartSeriesType.CandleStick,
                        //ChartSeriesType.Gantt,
                        ChartSeriesType.Line,
                        ChartSeriesType.Pie,
                        ChartSeriesType.Point,
                        //ChartSeriesType.Spline,
                        //ChartSeriesType.SplineArea,
                        ChartSeriesType.StackedArea,
                        ChartSeriesType.StackedBar,
                        ChartSeriesType.StackedLine,
                            //ChartSeriesType.StackedSpline,
                            //ChartSeriesType.StackedSplineArea

                        }.Select(c => c.ToString());

                        break;
                    }
            }

            ddlChartType.Items.Clear();

            ddlChartType.Items.AddRange(charts.Select(c => new ListItem(c, c)).ToArray());

            ddlChartType.SelectedValue = "Line";
        }

        #endregion

        #region Events

        protected void ddlBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            SetBuildingOffset(ddlBuildings.SelectedValue);
            dropDownSequence.ResetSubDropDownsOf(ddlBuildings);

            if (ddlBuildings.SelectedIndex == -1) return;

            BindEquipmentClassesDropDownList();
        }

        protected void ddlEquipmentClasses_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequence.ResetSubDropDownsOf(ddlEquipmentClasses);

            if (ddlEquipmentClasses.SelectedIndex == -1) return;

            if (reportType == ReportType.Calculated)
            {
                BindEquipmentListBox();
                return;
            }

            BindEquipmentDropdownList();
        }

        protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequence.ResetSubDropDownsOf(ddlEquipment);

            if (ddlEquipment.SelectedIndex == -1) return;

            BindAnalysisDropdownList();

            if (reportType == ReportType.Calculated) return;

            BindPointsListBox(lbPoints, null, null, ddlEquipment);

            //handles duplicate point names for charting, in the case that more than one point has the same name and
            //is of point type unassigned or is used for a group and happens to have the same name and is from the same equipment.
            ControlHelper.IncrementDuplicateTextValues(lbPoints.Items);
        }

        protected void lbEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            if (reportType != ReportType.Raw) return;

            BindAssociatedEquipmentListBox(lbAssociatedEquipment, lbEquipment);
            BindPointsListBox(lbPoints, lbEquipment, lbAssociatedEquipment);

            //handles duplicate point names for charting, in the case that more than one point has the same name and
            //is of point type unassigned or is used for a group and happens to have the same name and is from the same equipment.
            ControlHelper.IncrementDuplicateTextValues(lbPoints.Items);
        }

        protected void lbCalculatedEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            if (reportType != ReportType.Calculated || lbCalculatedEquipment.Items.Count == 0) return;

            lbCalculatedPoints.Items.Clear();

            BindAnalysisListBox();

            //handles duplicate point names for charting, in the case that more than one point has the same name and
            //is of point type unassigned or is used for a group and happens to have the same name and is from the same equipment.
            ControlHelper.IncrementDuplicateTextValues(lbCalculatedPoints.Items);
        }

        protected void lbCalculatedAnalyses_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            if (reportType != ReportType.Calculated || lbCalculatedEquipment.Items.Count == 0 || lbCalculatedAnalyses.Items.Count == 0) return;

            BindPointTypesListBox();

            //handles duplicate point names for charting, in the case that more than one point has the same name and
            //is of point type unassigned or is used for a group and happens to have the same name and is from the same equipment.
            ControlHelper.IncrementDuplicateTextValues(lbCalculatedPoints.Items);
        }

        protected void lbAssociatedEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            if (reportType != ReportType.Raw) return;
            BindPointsListBox(lbPoints, lbEquipment, lbAssociatedEquipment);

            //handles duplicate point names for charting, in the case that more than one point has the same name and
            //is of point type unassigned or is used for a group and happens to have the same name and is from the same equipment.
            ControlHelper.IncrementDuplicateTextValues(lbPoints.Items);
        }

        protected void rblReportType_OnSelectIndexChanged(Object sender, EventArgs e)
        {
            var isRawMode = reportType == ReportType.Raw;
            var isAnalyzeMode = reportType == ReportType.Analyzed;
            var isCalculatedMode = reportType == ReportType.Calculated;

            divRawChartOrCalculatedSelection.Visible = isRawMode || isCalculatedMode;
            divAnalyzedSelectionOnly.Visible = isAnalyzeMode;
            divCalcuatedSelectionOnly.Visible = isCalculatedMode;
            divAnalysisRange.Visible = !isRawMode;
            divDataDownloads.Visible = divDateRange.Visible = !isAnalyzeMode;
            divRawSelectionOnly.Visible = isRawMode;
            divAnalysisDate.Visible = isAnalyzeMode;
            divDownload.Visible = isRawMode || isCalculatedMode;

            if (isCalculatedMode)
            {
                divSynchronized.Style[HtmlTextWriterStyle.Display] = "none";
                chkSynchronized.Checked = true;
                chkSynchronized.Enabled = false;
            }
            else
            {
                divSynchronized.Style[HtmlTextWriterStyle.Display] = "";
                chkSynchronized.Enabled = true;
            }

            if (!String.IsNullOrEmpty(ddlBuildings.SelectedValue)) ddlEquipmentClasses_OnSelectedIndexChanged(null, null);
        }

        protected void rblChartMode_OnSelectIndexChanged(object sender, EventArgs e)
        {
            switch (chartMode)
            {
                case ChartMode.Simple:
                    {
                        BindChart();

                        divRawChartOrCalculatedSelection.FindControl("lbl3D").Visible = true;
                        divRawChartOrCalculatedSelection.FindControl("chk3D").Visible = true;

                        break;
                    }
                case ChartMode.Advanced:
                    {
                        BindChart();

                        divRawChartOrCalculatedSelection.FindControl("lbl3D").Visible = false;
                        divRawChartOrCalculatedSelection.FindControl("chk3D").Visible = false;

                        break;
                    }
            }
        }

        private void SetValidatorEnabledState()
        {
            bool bEnable = "1" == rblDataset.SelectedValue;
            buildingRequiredValidator.Enabled = bEnable;
            equipmentClassRequiredValidator.Enabled = bEnable;
            equipmentRequiredValidator.Enabled = bEnable;
            pointsCustomValidator.Enabled = bEnable;
            equipmentRequiredValidator.Enabled = bEnable;
            analysisRequiredValidator.Enabled = bEnable;
        }

        protected void downloadExcelButton_Click(object sender, EventArgs e) { AttachFileAndSetValidatorState("excel", typeof(VDataXLSXFileGenerator), typeof(RawXLSXFileGenerator)); }

        protected void downloadCSVButton_Click(object sender, EventArgs e) { AttachFileAndSetValidatorState("csv", typeof(VDataCSVFileGenerator), typeof(RawCSVFileGenerator)); }

        protected void btnPrior_OnCommand(object sender, CommandEventArgs e)
        {
            //txtEndDate.Text = CultureHelper.FormatDateTimeGeneralShort(siteUser.CultureName, DateTime.Now);
            //txtStartDate.Text = CultureHelper.FormatDateTimeGeneralShort(siteUser.CultureName, DateTime.Now.AddHours(-1));
            //txtStartDate.Text = CultureHelper.FormatDateTimeString(siteUser.CultureName, DateTime.Now.AddHours(-1));  //DateTime.Now.ToString("MM/dd/yyyy hh:ss tt").ToString(CultureHelper.GetCultureInfo(siteUser.CultureName));
            //txtEndDate.Text = CultureHelper.FormatDateTimeString(siteUser.CultureName, DateTime.Now); //DateTime.Now.AddHours(-1).ToString("MM/dd/yyyy hh:ss tt").ToString(CultureHelper.GetCultureInfo(siteUser.CultureName));

            DateTime sd, ed;

            DateTimeHelper.GenerateDefaultDatesEndingTomrrowAndStartingXDaysAgo(out sd, out ed, siteUser.UserTimeZoneOffset, Convert.ToInt32(e.CommandArgument));

            txtEndDate.SelectedDate = ed;
            txtStartDate.SelectedDate = sd;
        }

        #endregion

        #region Helper Methods

        #region Static

        public static PointSelection ParseListItemPointValue(ListItem li, string buildingName, int bid)
        {
            var valueParts = li.Value.Split('|');
            return new PointSelection(bid, Convert.ToInt32(valueParts[0]), Convert.ToInt32(valueParts[1]), buildingName, valueParts[3], valueParts[4], Convert.ToInt32(valueParts[2]));
            //p.PID = ParseListItemPID(li);
            //p.SamplingInterval = Convert.ToInt32(li.Value.Split('|')[1]);
            //p.PointName = li.Text;

            //return p;
        }

        public static int ParseListItemPID(ListItem li)
        {
            return ParsePIDFromListItemValue(li.Value);
        }

        public static int ParseListItemEID(ListItem li)
        {
            return ParseEIDFromListItemValue(li.Value);
        }
        public static int ParseEIDFromListItemValue(String value)
        {
            return Convert.ToInt32(value.Split('|')[0]);
        }

        public static int ParsePIDFromListItemValue(String value)
        {
            return Convert.ToInt32(value.Split('|')[1]);
        }

        private static string CreateListItemPointValue(Point p, int eid, string equipName)
        {
            return String.Join("|", eid, p.PID, p.SamplingInterval, equipName, p.PointName);
            //return String.Format("{0}{1}{2}", p.PID, '|', p.SamplingInterval);
        }

        #endregion

        private void SetBuildingOffset(string buildingID)
        {
            if (String.IsNullOrWhiteSpace(buildingID))
                return;

            int bid = Convert.ToInt32(buildingID);

            if (bid > 0)
            {
                var tzi = TimeZoneInfo.FindSystemTimeZoneById(DataMgr.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false).TimeZone);
                var script = String.Format("{0}.buildingOffset = {1};", this.ClientID, tzi.GetUtcOffset(DateTime.UtcNow).TotalMilliseconds);
                ScriptManager.RegisterClientScriptBlock(Page, typeof(Page), this.ClientID, script, true);
            }
        }

        private void AttachVDataFile<TReportGeneratorType>(String filename) where TReportGeneratorType : VDataFileGeneratorBase
        {
            Boolean result;

            var selectedVPoints = GetVPoints();

            if (selectedVPoints.Count == 0)
            {
                lblDownloadError.Visible = true;
                lblDownloadError.Text = "No data exists for your selection.";
                return;
            }

            //check vpt range max
            DataConstants.AnalysisRange range = AnalysisHelper.GetAnalysisRange(displayRange.ToString());

            //half day is only for current day so its irrelivant to check

            if (HandleWhenDownloadDatasetTooLarge(typeof(TReportGeneratorType), selectedVPoints, vDataMax, filename, range))
            {
                return;
            }
            try
            {
                var generator = VDataFileGeneratorBase.Create<TReportGeneratorType>
                (
                    DataMgr.VAndVPreDataMapper.GetConvertedVDataPlotDataForVPIDs,
                    selectedVPoints,
                    startDate,
                    endDate,
                    range
                );

                var publisher = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    generator,
                    LogMgr

                );

                result = publisher.AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), filename);

                if (result == false)
                {
                    lblDownloadError.Visible = true;
                    lblDownloadError.Text = "No data exists for your selection.";
                    return;
                }

                lblDownloadError.Visible = false;
            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                lblDownloadError.Visible = true;
                lblDownloadError.Text = "Cannot download, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in the help area to request an increase in download limits.";
                return;
            }

            if (result) return;

            //Either no data exists or an error occured.
            ////OLD MESSAGE - An error occured attempting to download. Please try again later, contact your provider, or use the contact us page for a large data download request.
            lblDownloadError.Visible = true;
            lblDownloadError.Text = "No data exists for your selection.";
        }

        private void AttachRawDataFile<TReportGeneratorType>(String filename, bool formatted) where TReportGeneratorType : RawFileGeneratorBase
        {
            Boolean result;

            List<PointSelection> selectedPoints = GetPoints();
            if (0 == selectedPoints.Count)
            {
                lblDownloadError.Visible = true;
                lblDownloadError.Text = "No data exists for your selection.";
                return;
            }
            else
            {
                //check pt day max
                if (HandleWhenDownloadDatasetTooLarge(typeof(TReportGeneratorType), selectedPoints, (formatted ? rawSynchronizedDataMax : rawDataMax), filename))
                {
                    return;
                }
            }

            try
            {
                var publisher = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    RawFileGeneratorBase.Create<TReportGeneratorType>
                    (
                        DataMgr.RawDataMapper.GetRawDataForPointSelectionSet,
                        selectedPoints,
                        startDate,
                        endDate,
                        formatted
                    ),
                    LogMgr

                );

                result = publisher.AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), filename);

                if (result == false)
                {
                    lblDownloadError.Visible = true;
                    lblDownloadError.Text = "No data exists for your selection.";
                    return;
                }

                lblDownloadError.Visible = false;
            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                lblDownloadError.Visible = true;
                lblDownloadError.Text = "Cannot download, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.";
                return;
            }

            if (result) return;

            //Either no data exists or an error occured.
            ////OLD MESSAGE - An error occured attempting to download. Please try again later, contact your provider, or use the contact us page for a large data download request.
            lblDownloadError.Visible = true;
            lblDownloadError.Text = "No data exists for your selection.";
        }

        private List<PointSelection> GetPoints()
        {
            var selectedPoints = new List<PointSelection>();

            if (rblDataset.SelectedValue == "1") // Selected
            {
                foreach (ListItem pt in points)
                {
                    if (pt.Selected) selectedPoints.Add(ParseListItemPointValue(pt, ddlBuildings.SelectedItem.Text, Convert.ToInt32(ddlBuildings.SelectedValue)));
                }
            }
            else // All
            {
                selectedPoints = DataMgr.PointDataMapper.GetAllPointSelectionsByCID(siteUser.CID, true, true);
#if false
                //selectedPoints = selectedPoints.Select(p => p.IsVisible);
                // Step 1: We need to get all buildings
                // Step 2: For each building, get all equipment classes
                // Step 3: For each equipment class, get all equipment
                // Step 4: For each equipment, get all points

                // Step 1
                foreach (ListItem building in ddlBuildings.Items)
                {
                    int buildingID = Convert.ToInt32(building.Value);
                    // Step 2
                    var equipmentClasses = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(buildingID);
                    foreach (EquipmentClass equipClass in equipmentClasses)
                    {
                        // Step 3
                        var equipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBIDAndEquipmentClassIDWithClassAndType(buildingID, equipClass.EquipmentClassID);
                        foreach (Equipment equip in equipment)
                        {
                            // Step 4
                            var points = DataMgr.PointDataMapper.GetAllVisiblePointsByEID(Convert.ToInt32(equip.EID), (p => p.PointType));
                            foreach (Point point in points)
                            {
                                selectedPoints.Add(point);
                            }
                        }
                    }
                }
#endif
            }
            return (selectedPoints);
        }

        private List<Int32> GetVPoints()
        {
            if (rblDataset.SelectedValue == "1") // Selected
            {
                var selectedVPIDs = new List<Int32>();

                foreach (ListItem vpt in vpoints)
                {
                    if (vpt.Selected) selectedVPIDs.Add(Int32.Parse(GetValueByIndex(vpt, 0)));
                }

                return selectedVPIDs;
            }

            // All
            return DataMgr.VAndVPreDataMapper.GetVPIDsForClient(siteUser.CID).ToList();
        }

        private bool HandleWhenDownloadDatasetTooLarge<T>(Type tReportType, IEnumerable<T> points, int iMaxSize, string fileName, DataConstants.AnalysisRange? range = null)
        {
            lblDownloadError.Visible = false;
            lblDownloadError.Text = "";
            double dWindow = DateTimeHelper.GetDiferenceBetweenDateTimesInDays(startDate, endDate);
            double divisor = 1;
            if (null != range)
            {
                if (range == DataConstants.AnalysisRange.Weekly)
                {
                    divisor = 7;
                }
                else if (range == DataConstants.AnalysisRange.Monthly)
                {
                    divisor = 30;
                }
            }

            double dActual = points.Count() * (dWindow / divisor);
            if (dActual > iMaxSize)
            {
                string sError = null;
                try
                {
                    KickoffBackendTask(tReportType, points, fileName);
                    sError = "The requested dataset is too large for an immediate download. However, your download has been scheduled for processing and will be emailed to you when completed. If the dataset in the download exceeds your data download size, you will be notified accordingly.";
                }
                catch (Exception ex)
                {
                    sError = ex.Message;
                }
                lblDownloadError.Visible = true;
                lblDownloadError.Text = sError;
            }
            return (lblDownloadError.Visible); // If visible, handled; this will cause calling method to return back to caller
        }

        private void KickoffBackendTask<T>(Type tReportType, IEnumerable<T> points, string fileName)
        {
            // The things we need to stuff in a queue message are:
            // 1. User's email address
            // 2. Start/End date
            // 3. Formatted or not (synchronized or not)
            // 4. points
            bool bVData = reportType == ReportCriteria.ReportType.Calculated;
            string sQueueName = bVData ? AzureConstants.VDataDownloadNotifications : AzureConstants.RawDataDownloadNotifications;

            // Get the queue
            AzureQueueDataContext queueContext = new AzureQueueDataContext(DataMgr.OrgBasedQueueStorageProvider, sQueueName);
            BaseDataDownloadQueueMessage msg = null;
            if (bVData)
            {
                VDataDownloadQueueMessage m = new VDataDownloadQueueMessage();
                m.VPIDs = points as List<Int32>;
                m.Range = AnalysisHelper.GetAnalysisRange(displayRange.ToString());
                msg = m;
                msg.DownloadType = (typeof(VDataXLSXFileGenerator) == tReportType ? BaseDataDownloadQueueMessage.eDownloadType.XLSX : BaseDataDownloadQueueMessage.eDownloadType.CSV);
            }
            else
            {
                RawDataDownloadQueueMessage m = new RawDataDownloadQueueMessage();
                m.Points = points as List<PointSelection>;
                msg = m;
                msg.DownloadType = (typeof(RawXLSXFileGenerator) == tReportType ? BaseDataDownloadQueueMessage.eDownloadType.XLSX : BaseDataDownloadQueueMessage.eDownloadType.CSV);
            }

            // add message to queue
            msg.CID = siteUser.CID;
            msg.IsSchneiderTheme = siteUser.IsSchneiderTheme;
            msg.IsKGSFullAdminOrHigher = siteUser.IsFullAdminOrHigherOrFullUser;
            msg.Email = siteUser.Email;
            msg.StartDate = startDate;
            msg.EndDate = endDate;
            msg.Formatted = chkSynchronized.Checked;
            msg.FileName = fileName;
            msg.CultureName = siteUser.CultureName;
            queueContext.AddMessage<BaseDataDownloadQueueMessage>(msg);
        }

        private void ClearAndSetListBox(ListBox lb, String text, String value, Object data)
        {
            lb.Items.Clear();
            lb.DataTextField = text;
            lb.DataValueField = value;
            lb.DataSource = data;
            lb.DataBind();

            AttachToolTips();
        }

        private List<Int32> GetSelectedIds(ListBox lb, Int32? index = null) { return ControlHelper.GetListBoxData<Int32>(lb, (_ => (index.HasValue) ? Convert.ToInt32(_.Value.Split(delimeter)[index.Value]) : Convert.ToInt32(_.Value)), criteria).ToList(); }

        private String GetValueByIndex(ListItem li, Int32 index) { return li.Value.Split(delimeter)[index]; }

        private void AttachToolTips()
        {
            if (lbCalculatedAnalyses.Items.Count > 0) ControlHelper.AttachToolTipToListBox(lbCalculatedAnalyses, delimeter, 4);
            if (lbCalculatedPoints.Items.Count > 0) ControlHelper.AttachToolTipToListBox(lbCalculatedPoints, delimeter, 1);
        }

        private String CreateValueFieldsForAnalysesListBox(Analyses_Equipment _)
        {
            return String.Join(DataConstants.DefaultDelimiter.ToString(), _.AID.ToString(), _.Equipment.EID.ToString(), _.Equipment.EquipmentName, _.Analyse.AnalysisName, String.Format("{0} ({1})", _.Analyse.AnalysisName, _.Equipment.EquipmentName));
        }

        private String CreateValueFieldsForPointTypesListBox(VPoint _)
        {
            return String.Join(DataConstants.DefaultDelimiter.ToString(), _.VPID.ToString(), String.Format("{0} ({1}, {2})", _.PointType.DisplayName, _.Equipment.EquipmentName, _.Analyse.AnalysisName));
        }

        private void AttachFileAndSetValidatorState(String fileType, Type vDataGeneratorType, Type rawDataGeneratorType)
        {
            var fileName = String.Empty;
            var extension = (fileType == "excel") ? "xlsx" : "csv";

            if (reportType == ReportType.Calculated) fileName = String.Format("$productCalculatedChart.{0}", extension);
            if (reportType == ReportType.Raw) fileName = String.Format("$productRawChart.{0}", extension);

            switch (reportType)
            {
                case ReportCriteria.ReportType.Calculated:
                    if (vDataGeneratorType == typeof(VDataXLSXFileGenerator)) AttachVDataFile<VDataXLSXFileGenerator>(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, fileName, true));
                    if (vDataGeneratorType == typeof(VDataCSVFileGenerator)) AttachVDataFile<VDataCSVFileGenerator>(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, fileName, true));
                    break;
                case ReportCriteria.ReportType.Raw:
                    if (rawDataGeneratorType == typeof(RawXLSXFileGenerator)) AttachRawDataFile<RawXLSXFileGenerator>(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, fileName, true), chkSynchronized.Checked);
                    if (rawDataGeneratorType == typeof(RawCSVFileGenerator)) AttachRawDataFile<RawCSVFileGenerator>(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, fileName, true), chkSynchronized.Checked);
                    break;
            }

            SetValidatorEnabledState();
        }

        #endregion

        public Boolean enabled
        {
            set
            {
                pnlReportCriteria.Enabled = value;
                chartTypeRequiredValidator.Enabled = value;
                buildingRequiredValidator.Enabled = value;
                equipmentClassRequiredValidator.Enabled = value;
                equipmentRequiredValidator.Enabled = value;
                analysisRequiredValidator.Enabled = value;
                //analysisEarlyCompareValidator.Enabled   = value;
                //analysisMaskedEditValidator.Enabled     = value;
                //startDateMaskedEditValidator.Enabled    = value;
                //endDateMaskedEditValidator.Enabled      = value;
                lbEquipment.Enabled = value;
                lbPoints.Enabled = value;
                lbCalculatedPoints.Enabled = value;
                //radChart.Visible = value;
                //rawChart.Visible = value;
                //dateStartEarlyCompareValidator.Enabled  = value;
                //dateEndEarlyCompareValidator.Enabled    = value;
                dateStartRequiredValidator.Enabled = value;
                dateEndRequiredValidator.Enabled = value;
                endDateCV.Enabled = value;
            }
            get { return pnlReportCriteria.Enabled; }
        }

        public ReportType reportType
        {
            set { ControlHelper.RadioButtonControl.CheckOption(rblReportType, value.ToString()); }
            get { return EnumHelper.Parse<ReportType>(rblReportType.SelectedValue); }
        }

        public ChartMode chartMode
        {
            get { return (ChartMode)rblChartMode.SelectedIndex; }
        }

        public String chartType
        {
            get { return ddlChartType.SelectedValue; }
        }

        public Boolean In3DMode
        {
            get { return chk3D.Checked; }
        }

        public ClientEntity clientEntity
        {
            get
            {
                if (_clientEntity == null)
                {
                    _clientEntity = new ClientEntity(siteUser.CID, siteUser.ClientName);
                }

                return _clientEntity;
            }
        }

        public Int32? bid
        {
            set
            {
                if (value == null) return;

                var val = Convert.ToString(value.Value);

                if (ddlBuildings.Items.FindByValue(val) == null) return;

                ControlHelper.DropDownListControl.SelectItem(ddlBuildings, val);
            }
            get
            {
                if (ddlBuildings.SelectedIndex == 0) return null;

                return Convert.ToInt32(ddlBuildings.SelectedValue);
            }
        }

        public String buildingName
        {
            get { return ddlBuildings.SelectedItem.Text; }
        }

        public Int32? equipmentClassID
        {
            set
            {
                if (value == null) return;

                var val = Convert.ToString(value.Value);

                if (ddlEquipmentClasses.Items.FindByValue(val) == null) return;

                ControlHelper.DropDownListControl.SelectItem(ddlEquipmentClasses, val);
            }
            get
            {
                if (ddlEquipmentClasses.SelectedIndex == 0) return null;

                return Convert.ToInt32(ddlEquipmentClasses.SelectedValue);
            }
        }

        //replace with EquipmentEntity
        public Int32? eid
        {
            set
            {
                if (value == null) return;

                var val = Convert.ToString(value.Value);

                if (ddlEquipment.Items.FindByValue(val) == null) return;

                ControlHelper.DropDownListControl.SelectItem(ddlEquipment, val);
            }
            get
            {
                if (ddlEquipment.SelectedIndex == 0) return null;

                return Convert.ToInt32(ddlEquipment.SelectedValue);
            }
        }

        public List<ListItem> equipment
        {
            get
            {
                if (EnumHelper.Parse<ReportType>(rblReportType.SelectedValue) == ReportType.Raw) return lbEquipment.Items.Cast<ListItem>().Where(criteria).ToList();
                if (EnumHelper.Parse<ReportType>(rblReportType.SelectedValue) == ReportType.Calculated) return lbCalculatedEquipment.Items.Cast<ListItem>().Where(criteria).ToList();

                List<ListItem> ls = new List<ListItem>();
                ls.Add(ddlEquipment.SelectedItem);
                return ls;
            }
        }

        public String equipmentAnalysesList
        {
            get
            {
                if (EnumHelper.Parse<ReportType>(rblReportType.SelectedValue) == ReportType.Calculated)
                {
                    var equipment = lbCalculatedEquipment.Items.Cast<ListItem>().Where(criteria).Select(_ => new String[] { _.Value, _.Text }).ToList();
                    var analyses = lbCalculatedAnalyses.Items.Cast<ListItem>().Where(criteria).Select(_ => new String[] { GetValueByIndex(_, 1), GetValueByIndex(_, 2), GetValueByIndex(_, 3) }).ToList();
                    var equipmentAnalyses = new StringBuilder();
                    var format = "{0} ({1})";

                    for (var e = 0; e < equipment.Count; e++)
                    {
                        var eid = equipment[e][0];
                        var equipmentName = equipment[e][1];
                        var tempAnalyses = analyses.Where(_ => _[0].Contains(eid) && _[1].Contains(equipmentName)).Select(_ => _[2]).ToList();

                        if (tempAnalyses.Count > 0)
                        {
                            var currentFormat = (e != equipment.Count - 1) ? format + ", " : format;

                            equipmentAnalyses.Append(String.Format(currentFormat, equipmentName, String.Join(", ", tempAnalyses)));
                        }
                    }

                    return equipmentAnalyses.ToString();
                }

                return null;
            }
        }

        public String analysisName
        {
            get { return ddlAnalysis.SelectedItem.Text; }
        }
        //

        public Int32? aid
        {
            set
            {
                if (value == null) return;

                var val = Convert.ToString(value.Value);

                if (ddlAnalysis.Items.FindByValue(val) == null) return;

                ControlHelper.DropDownListControl.SelectItem(ddlAnalysis, val);
            }
            get
            {
                if (ddlAnalysis.SelectedIndex == 0) return null;

                return Convert.ToInt32(ddlAnalysis.SelectedValue);
            }
        }

        public List<Int32> aids { get { return (lbCalculatedAnalyses.Items.Count != 0) ? GetSelectedIds(lbCalculatedAnalyses, 0) : null; } }

        public List<ListItem> points
        {
            get { return lbPoints.Items.Cast<ListItem>().Where(item => item.Selected).ToList(); }
        }

        public List<ListItem> vpoints
        {
            get
            {
                return lbCalculatedPoints.Items.Cast<ListItem>().Where(criteria).Select(_ => new ListItem() { Value = (_.Value.Contains(delimeter.ToString())) ? _.Value.Split(delimeter)[0] : _.Value, Text = _.Text, Selected = true }).ToList();
            }
        }

        public DateTime analysisDate
        {
            set
            {
                if (value == null || value == DateTime.MinValue) return;

                txtAnalysisDate.SelectedDate = value;
            }
            get { return (DateTime)txtAnalysisDate.SelectedDate; }
        }

        public DateTime startDate
        {
            set
            {
                if (value == null || value == DateTime.MinValue) return;

                txtStartDate.SelectedDate = value;
            }
            get
            {
                DateTime newDate = (DateTime)txtStartDate.SelectedDate;

                if (EnumHelper.Parse<ReportType>(rblReportType.SelectedValue) == ReportType.Calculated)
                {
                    switch (EnumHelper.Parse<DisplayRange>(rblRange.SelectedValue))
                    {
                        case DisplayRange.Weekly:
                            newDate = DateTimeHelper.GetLastSunday(newDate);
                            txtStartDate.SelectedDate = newDate;
                            break;
                        case DisplayRange.Monthly:
                            newDate = DateTimeHelper.GetFirstOfMonth(newDate);
                            txtStartDate.SelectedDate = newDate;
                            break;
                        default:
                            break;
                    }
                }

                return newDate;
            }
        }

        public DateTime endDate
        {
            set
            {
                if (value == null || value == DateTime.MinValue) return;

                txtEndDate.SelectedDate = value;
            }
            get
            {
                DateTime newDate = (DateTime)txtEndDate.SelectedDate;

                if (EnumHelper.Parse<ReportType>(rblReportType.SelectedValue) == ReportType.Calculated)
                {
                    switch (EnumHelper.Parse<DisplayRange>(rblRange.SelectedValue))
                    {
                        case DisplayRange.Weekly:
                            newDate = DateTimeHelper.GetNextSaturday(newDate);
                            txtEndDate.SelectedDate = newDate;
                            break;
                        case DisplayRange.Monthly:
                            newDate = DateTimeHelper.GetLastOfMonth(newDate);
                            txtEndDate.SelectedDate = newDate;
                            break;
                        default:
                            break;
                    }
                }

                return newDate;
            }
        }

        public DisplayRange displayRange
        {
            set { ControlHelper.RadioButtonControl.CheckOption(rblRange, value.ToString()); }
            get { return EnumHelper.Parse<DisplayRange>(rblRange.SelectedValue); }
        }
    }
}