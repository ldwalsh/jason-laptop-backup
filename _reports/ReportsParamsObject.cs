﻿using System;
using CW.Website.Reports;
using CW.Utility.Web;

namespace CW.Website._reports
{
    public sealed class ReportsParamsObject: QueryString.IParamsObject
    {
        #region QueryString.IParamsObject

            Boolean QueryString.IParamsObject.RequirePropertyAttribute
            {
                get {return false;}
            }

        #endregion

        //for all report types
        [
        QueryString.Property(Optional = true)
        ]
        public ReportCriteria.ReportType
                reportType;

        public  Int32
                cid;

        [
        QueryString.Property(Requires = "cid")
        ]
        public  Int32
                bid;

        [
        QueryString.Property(Optional = true, Requires = "bid")
        ]
        public  Int32?
                ecid;

        [
        QueryString.Property(Optional = true, Requires = "ecid")
        ]
        public  Int32?
                eid;


        //for raw and calculated
        [
        QueryString.Property(Optional = true, Requires = "eid")
        ]
        public DateTime
                sd;

        [
        QueryString.Property(Optional = true, Requires = "eid")
        ]
        public DateTime
                ed;
        

        
        //for analyzed and calculated
        [
        QueryString.Property(Optional = true, Requires = "eid")
        ]
                public Int32?
                        aid;

        [
        QueryString.Property(Optional = true, Requires = "aid")
        ]
                public ReportCriteria.DisplayRange
                        rng;

        //for analyzed only
        [
        QueryString.Property(Optional = true, Requires = "aid")
        ]
                public DateTime
                        ad;
        
    }
}