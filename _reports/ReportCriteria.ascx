﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ReportCriteria.ascx.cs" Inherits="CW.Website.Reports.ReportCriteria" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

<script type="text/javascript">

    var $this = new function () {
        this.buildingOffset = 0;

        this.findControl = function (id) //move to base class
        {
            return jQuery("div[id$='" + this.id + "'] [id$='" + id + "']")[0];
        };

        this.DisableValidators = function (ids, bDisable) {
            if (null != ids && typeof Page_Validators != 'undefined') {
                if (null != Sys.Extended.UI.ValidatorCalloutBehavior._currentCallout) {
                    Sys.Extended.UI.ValidatorCalloutBehavior._currentCallout.hide();
                }
                var arrayIDs = ids.split(',');

                var ctrlID = typeof $this.id == 'undefined' ? "" : $this.id.replace("_pnlReportCriteria", "");
                var ctrlIDLen = ctrlID.length;
                for (i = 0; i <= Page_Validators.length; i++) {
                    if (Page_Validators[i] != null) {
                        var sCtrltoValidate = Page_Validators[i].controltovalidate;
                        if (sCtrltoValidate.length > ctrlIDLen) {
                            if (sCtrltoValidate.substr(0, ctrlIDLen) == ctrlID) {
                                var sID = sCtrltoValidate.substr(ctrlIDLen + 1);
                                if (-1 != jQuery.inArray(sID, arrayIDs)) {
                                    if (Page_Validators[i].enabled != !bDisable) {
                                        Page_Validators[i].enabled = !bDisable;
                                        Page_Validators[i].style.display = !bDisable ? 'inline' : 'none';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };

        this.SetStateForValidators = function (bDisable) {
            if (null == bDisable) {
                var dataset = this.findControl("rblDataset");
                var rbSel = "#" + dataset.id + " input:checked";
                bDisable = "2" == $(rbSel).val();
            }
            this.DisableValidators("ddlBuildings,ddlEquipmentClasses,lbEquipment,lbPoints,ddlEquipment,ddlAnalysis,lbCalculatedPoints", bDisable);
        };
    };

    var ListBoxCustomValidator = function (sender, args) {
        var listBox = document.getElementById(sender.controltovalidate);

        //.disabled changed in framework 4.5, .classList doesnt work in ie9
        if (!$(listBox).hasClass('aspNetDisabled')) {

            var listBoxCnt = 0;

            for (var x = 0; x < listBox.options.length; x++) {
                if (listBox.options[x].selected) listBoxCnt++;
            }

            args.IsValid = (listBoxCnt > 0)
        }
        else {
            args.IsValid = true;
        }
    };

</script>
<asp:Panel ID="pnlReportCriteria" runat="server">
    <div id="divAllSelections">
        <div id="divReportSelection">
            <h3>Select Data Settings</h3>
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Data Type:</label>
                <asp:RadioButtonList ID="rblReportType" CssClass="radio" OnSelectedIndexChanged="rblReportType_OnSelectIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem Text="Raw" Value="Raw" />
                    <asp:ListItem Text="Analyzed" Value="Analyzed" />
                    <asp:ListItem Text="Calculated" Value="Calculated" />
                </asp:RadioButtonList>
            </div>
        </div>
        <div id="divRawChartOrCalculatedSelection" visible="false" runat="server">
            <hr />
            <h3>Select Chart Settings</h3>
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Chart Mode:</label>
                <asp:RadioButtonList ID="rblChartMode" CssClass="radio" OnSelectedIndexChanged="rblChartMode_OnSelectIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
                    <asp:ListItem Text="Simple" Selected="true" Value="1" />
                    <asp:ListItem Text="Advanced" Value="2" />
                </asp:RadioButtonList>
            </div>
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Chart Type:</label>
                <asp:DropDownList ID="ddlChartType" CssClass="dropdown" runat="server" />
            </div>
            <asp:RequiredFieldValidator ID="chartTypeRequiredValidator" runat="server" ErrorMessage="Chart Type is a required field." ControlToValidate="ddlChartType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Reporting" />
            <ajaxToolkit:ValidatorCalloutExtender ID="chartTypeRequiredValidatorExtender" runat="server" TargetControlID="chartTypeRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            <div id="div3D" class="divFormTwoColumn">
                <asp:Label ID="lbl3D" runat="server" Text="3D:" CssClass="labelTwoColumn" />
                <asp:CheckBox ID="chk3D" CssClass="checkbox" runat="server" />
            </div>
        </div>
        <hr />
        <div id="divAllDataPointSelection">
            <h3>Select Data Points</h3>
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Building:</label>
                <asp:DropDownList ID="ddlBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
            </div>
            <asp:RequiredFieldValidator ID="buildingRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBuildings" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Reporting" />
            <ajaxToolkit:ValidatorCalloutExtender ID="buildingRequiredValidatorExtender" runat="server" TargetControlID="buildingRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Equipment Class:</label>
                <asp:DropDownList ID="ddlEquipmentClasses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClasses_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
            </div>
            <asp:RequiredFieldValidator ID="equipmentClassRequiredValidator" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlEquipmentClasses" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Reporting" />
            <ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassRequiredValidatorExtender" runat="server" TargetControlID="equipmentClassRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
        </div>
        <div id="divRawSelectionOnly" visible="false" runat="server">
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Equipment:</label>
                <extensions:ListBoxExtension ID="lbEquipment" runat="server" AutoPostBack="true" SelectionMode="Multiple" CssClass="listboxNarrowest" OnSelectedIndexChanged="lbEquipment_OnSelectedIndexChanged" />
                <asp:CustomValidator ID="equipmentCustomValidator" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="lbEquipment" SetFocusOnError="true" ValidationGroup="Reporting" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None"></asp:CustomValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="equipmentCustomValidatorExtender" runat="server" TargetControlID="equipmentCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            </div>
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">Select Associated Equipment:</label>
                <extensions:ListBoxExtension ID="lbAssociatedEquipment" runat="server" AutoPostBack="true" SelectionMode="Multiple" CssClass="listboxNarrowest" OnSelectedIndexChanged="lbAssociatedEquipment_OnSelectedIndexChanged" />
            </div>
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Points:</label>
                <!--<div class="listboxNarrowestOuterDivHorizontalScroll">-->
                <extensions:ListBoxExtension ID="lbPoints" runat="server" SelectionMode="Multiple" CssClass="listboxNarrowest" />
                <!--</div>-->
                <asp:CustomValidator ID="pointsCustomValidator" runat="server" ErrorMessage="Points is a required field." ControlToValidate="lbPoints" SetFocusOnError="true" ValidationGroup="Reporting" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None"></asp:CustomValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="pointsCustomValidatorExtender" runat="server" TargetControlID="pointsCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            </div>
        </div>
        <div id="divAnalyzedSelectionOnly" runat="server" visible="false">
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Equipment:</label>
                <extensions:DropDownExtension ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
            </div>
            <asp:RequiredFieldValidator ID="equipmentRequiredValidator" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="ddlEquipment" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Reporting" />
            <ajaxToolkit:ValidatorCalloutExtender ID="equipmentRequiredValidatorExtender" runat="server" TargetControlID="equipmentRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Analysis:</label>
                <extensions:DropDownExtension ID="ddlAnalysis" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
            </div>
            <asp:RequiredFieldValidator ID="analysisRequiredValidator" runat="server" ErrorMessage="Analysis is a required field." ControlToValidate="ddlAnalysis" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Reporting" />
            <ajaxToolkit:ValidatorCalloutExtender ID="analysisRequiredValidatorExtender" runat="server" TargetControlID="analysisRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
        </div>
        <div id="divCalcuatedSelectionOnly" visible="false" runat="server">
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Equipment:</label>
                <extensions:ListBoxExtension ID="lbCalculatedEquipment" runat="server" AutoPostBack="true" SelectionMode="Multiple" CssClass="listboxNarrowest" OnSelectedIndexChanged="lbCalculatedEquipment_OnSelectedIndexChanged" />
                <asp:CustomValidator ID="calculatedEquipmentCV" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="lbCalculatedEquipment" SetFocusOnError="true" ValidationGroup="Reporting" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None"></asp:CustomValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="calculatedEquipmentVCE" runat="server" TargetControlID="calculatedEquipmentCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            </div>
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Analysis:</label>
                <extensions:ListBoxExtension ID="lbCalculatedAnalyses" runat="server" AutoPostBack="true" SelectionMode="Multiple" CssClass="listboxNarrowest" OnSelectedIndexChanged="lbCalculatedAnalyses_OnSelectedIndexChanged" />
                <asp:CustomValidator ID="calculatedAnalysisCV" runat="server" ErrorMessage="Analysis is a required field." ControlToValidate="lbCalculatedAnalyses" SetFocusOnError="true" ValidationGroup="Reporting" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None"></asp:CustomValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="calculatedAnalysisVCE" runat="server" TargetControlID="calculatedAnalysisCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            </div>
            <div class="divFormTwoColumn">
                <label class="labelTwoColumn">*Select Calculated Points:</label>
                <asp:ListBox ID="lbCalculatedPoints" runat="server" SelectionMode="Multiple" CssClass="listboxNarrowest" />
                <asp:CustomValidator ID="calculatedPointsCV" runat="server" ErrorMessage="Points is a required field." ControlToValidate="lbCalculatedPoints" SetFocusOnError="true" ValidationGroup="Reporting" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None"></asp:CustomValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="calculatedPointsVCE" runat="server" TargetControlID="calculatedPointsCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            </div>
        </div>
    </div>
    <hr />
    <div>
        <h3>Specify Date Range</h3>
    </div>
    <div id="divAnalysisRange" runat="server">
        <div class="divFormTwoColumn">
            <label class="labelTwoColumn">Display Interval:</label>
            <asp:RadioButtonList ID="rblRange" CssClass="radio" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Text="Half Day" Value="HalfDay" />
                <asp:ListItem Text="Daily" Value="Daily" Selected="true" />
                <asp:ListItem Text="Weekly" Value="Weekly" />
                <asp:ListItem Text="Monthly" Value="Monthly" />
            </asp:RadioButtonList>
        </div>
    </div>
    <div id="divAnalysisDate" runat="server">
        <div class="divFormTwoColumn">
            <label class="labelTwoColumn">*Analysis Date:</label>
            <telerik:RadDatePicker ID="txtAnalysisDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>
        </div>
    </div>
    <div id="divDateRange" visible="false" runat="server">
        <div class="divFormTwoColumn">
            <div class="quickDates">
                <a href="javascript:" onclick="SetDateRangeWithOffset('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 168, 'hours', <%= this.ClientID %>.buildingOffset);">1 week</a>
                <a href="javascript:" onclick="SetDateRangeWithOffset('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 120, 'hours', <%= this.ClientID %>.buildingOffset);">5 days</a>
                <a href="javascript:" onclick="SetDateRangeWithOffset('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 48, 'hours', <%= this.ClientID %>.buildingOffset);">2 days</a>
                <a href="javascript:" onclick="SetDateRangeWithOffset('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 24, 'hours', <%= this.ClientID %>.buildingOffset);">1 day</a>
                <a href="javascript:" onclick="SetDateRangeWithOffset('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 12, 'hours', <%= this.ClientID %>.buildingOffset);">12 hrs</a>
                <a href="javascript:" onclick="SetDateRangeWithOffset('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 6, 'hours', <%= this.ClientID %>.buildingOffset);">6 hrs</a>
                <a href="javascript:" onclick="SetDateRangeWithOffset('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 1, 'hours', <%= this.ClientID %>.buildingOffset);">1 hr</a>
            </div>
        </div>
        <div class="divFormTwoColumn">
            <label class="labelTwoColumn">*Start Date:</label>
            <telerik:RadDateTimePicker ID="txtStartDate" MinDate="01/01/2000" runat="server"></telerik:RadDateTimePicker>
            <asp:RequiredFieldValidator ID="dateStartRequiredValidator" runat="server"
                ErrorMessage="Start date is a required field."
                ControlToValidate="txtStartDate"
                SetFocusOnError="true"
                Display="None"
                ValidationGroup="Reporting">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="dateStartRequiredValidatorExtender" runat="server"
                TargetControlID="dateStartRequiredValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
            </ajaxToolkit:ValidatorCalloutExtender>
        </div>
        <div class="divFormTwoColumn">
            <label class="labelTwoColumn">*End Date:</label>
            <telerik:RadDateTimePicker ID="txtEndDate" MinDate="01/01/2000" runat="server"></telerik:RadDateTimePicker>
            <asp:RequiredFieldValidator ID="dateEndRequiredValidator" runat="server"
                ErrorMessage="End date is a required field."
                ControlToValidate="txtEndDate"
                SetFocusOnError="true"
                Display="None"
                ValidationGroup="Reporting">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="dateEndRequiredValidatorExtender" runat="server"
                TargetControlID="dateEndRequiredValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
            </ajaxToolkit:ValidatorCalloutExtender>
            <asp:CustomValidator ID="endDateCV" runat="server" ClientValidationFunction="RadDatePickerDatetimeComparer" ControlToValidate="txtEndDate" EnableClientScript="true" ErrorMessage="Invalid range." Display="None" ValidationGroup="Reporting" />
            <ajaxToolkit:ValidatorCalloutExtender ID="endDateCVVCE" runat="server" TargetControlID="endDateCV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="Right" Width="175" />
        </div>
    </div>
    <div id="divDataDownloads" class="divDataDownloads" visible="false" runat="server">
        <hr />
        <h3>Download Data</h3>

        <div class="divFormTwoColumn">
            <label class="labelTwoColumn">Dataset:</label>
            <asp:RadioButtonList ID="rblDataset" CssClass="radio" RepeatDirection="Horizontal" runat="server">
                <asp:ListItem Text="Selected" Selected="true" Value="1" onclick="$this.SetStateForValidators(false);" />
                <asp:ListItem Text="All" Value="2" onclick="$this.SetStateForValidators(true);" />
            </asp:RadioButtonList>
        </div>

        <div id="divSynchronized" class="divFormTwoColumn" runat="server">
            <label class="labelTwoColumn">Synchronized:</label>
            <asp:CheckBox ID="chkSynchronized" CssClass="checkbox" Checked="true" runat="server" />
        </div>

        <div id="divDownload" class="divFormTwoColumn" runat="server">
            <asp:ImageButton ID="imgExcelDownload" ValidationGroup="Reporting" ImageUrl="~/_assets/images/excel-icon.jpg" OnClientClick="$this.SetStateForValidators();" OnClick="downloadExcelButton_Click" ValidateRequestMode="Disabled" AlternateText="download" runat="server" />
            <asp:ImageButton ID="imgCSVDownload" ValidationGroup="Reporting" ImageUrl="~/_assets/images/csv-icon.jpg" OnClientClick="$this.SetStateForValidators();" OnClick="downloadCSVButton_Click" ValidateRequestMode="Disabled" AlternateText="download" runat="server" />
        </div>
        <br />
        <asp:Label ID="lblDownloadError" CssClass="errorMessage" runat="server" Text="" Visible="false"></asp:Label>
    </div>
</asp:Panel>

