﻿using System;
using System.IO;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CW.Utility;
using CW.Website.Reports;
using CW.Website._framework;
using Telerik.Web.UI;
using CW.Data;
using CW.Business;
using CW.Website._controls;
using AjaxControlToolkit;
using System.Web;
using CW.Utility.Web;

namespace CW.Website._reports
{
    public partial class ChartRegion: SiteUserControl
    {
        #region controls

            public HtmlGenericControl
                    reportInfo;

            public  HtmlGenericControl
                    regionTitle;
        
            public  Label
                    lblResults;
        
            public  HtmlGenericControl
                    analysisInfo;
        
            public  Label
                    lblClientName;
        
            public  HiddenField
                    hdnCID;
        
            public  Label
                    lblBuildingName;
        
            public  Label
                    lblEquipmentName;

            public Label
                    lblAssociatedEquipment;

            public HiddenField
                    hdnEID;

            public HiddenField
                    hdnAID;

            public HiddenField
                    hdnAnalysisRange;

            public HiddenField
                    hdnPIDs;

            public HiddenField
                    hdnVPIDs;

            public  Label
                    lblAnalysisName;
        
            public  Label
                    lblPoints;

            public Label
                    lblAssociatedEquipmentPoints;

            public Label
                    lblCalculatedPoints;      
        
            public  Label
                    lblDisplayInterval;
        
            public  Label
                    lblStartDate;
        
            public  Label
                    lblCost;
        
            public  Label
                    lblEnergy;
        
            public  Label 
                    lblComfort;
        
            public  Label
                    lblMaintenance;
        
            public  Literal
                    litNotes;
        
            public  CustomRadBinaryImage
                    binImageGraph;

            public Literal
                    litGraphNotAvailable;
        
            public  HtmlGenericControl
                    rawInfo;
        
            public  Label
                    lblRawClientName;
        
            public  HiddenField
                    hdnRawCID;

            public HiddenField
                    hdnRawEID;

            public  Label
                    lblRawBuildingName;
        
            public  Label
                    lblRawEquipmentName;
       
            public  Label
                    lblRawPoints;
        
            public  Label
                    lblRawStartDate;
        
            public  Label
                    lblRawEndDate;
        
            public  LinkButton
                    resetChart;
        
            public  Chart
                    rawChart;

            public  RadChart
                    radChart;

            public HtmlGenericControl
                    divPoints;

            public HtmlGenericControl
                    divCalculatedPoints;

            public CollapsiblePanelExtender
                    pointsCollapsiblePanelExtender;

            public CollapsiblePanelExtender
                        associatedEquipmentPointsCollapsiblePanelExtender;

        #endregion

        public ReportCriteria parentReportCriteria
        {
            set;
            get;
        }

        public void LoadReportInfoIntoPDFReportClasses(ref ITextSharpHelper.AnalysisPDFReport analysisPDFReport, ref ITextSharpHelper.RawPDFReport rawPDFReport, ref ITextSharpHelper.CalculatedPDFReport calculatedPDFReport)
        {
            if (analysisInfo.Visible)
            {
                analysisPDFReport = new ITextSharpHelper.AnalysisPDFReport();

                Equipment equipment = DataMgr.EquipmentDataMapper.GetEquipmentByID(Convert.ToInt32(hdnEID.Value));

                analysisPDFReport.AnalysisName          = lblAnalysisName.Text;
                analysisPDFReport.ClientName            = lblClientName.Text;
                analysisPDFReport.BuildingName          = lblBuildingName.Text;
                analysisPDFReport.EquipmentName         = lblEquipmentName.Text;
                analysisPDFReport.EquipmentLocation     = equipment.EquipmentLocation;
                analysisPDFReport.AssociatedEquipment   = lblAssociatedEquipment.Text;                
                analysisPDFReport.Points                = Convert.ToBoolean(pointsCollapsiblePanelExtender.ClientState) ? "" : lblPoints.Text;
                analysisPDFReport.AssociatedEquipmentPoints = Convert.ToBoolean(associatedEquipmentPointsCollapsiblePanelExtender.ClientState) ? "" : lblAssociatedEquipmentPoints.Text; 
                analysisPDFReport.DisplayInterval       = lblDisplayInterval.Text; //parentReportCriteria.displayRange.ToString();
                analysisPDFReport.StartDate             = lblStartDate.Text;
                //analysisPDFReport.EndDate             = lblEndDate.Text;
                analysisPDFReport.CostSavings           = lblCost.Text;
                analysisPDFReport.ComfortPriority       = lblComfort.Text;
                analysisPDFReport.EnergyPriority        = lblEnergy.Text;
                analysisPDFReport.MaintenancePriority   = lblMaintenance.Text;
                analysisPDFReport.Notes                 = litNotes.Text;

                analysisPDFReport.ImageByteArray        = DataMgr.DefaultCacheRepository.Get<Byte[]>(GetImageKey());
					//parentPage.sessionState.Get<Byte[]>(ID + "_imgStream");
            }

            if (rawInfo.Visible)
            {
                if (divPoints.Visible)
                {
                    rawPDFReport = new ITextSharpHelper.RawPDFReport();

                    //Equipment equipment = mDataManager.EquipmentDataMapper.GetEquipmentByID(Convert.ToInt32(hdnRawEID.Value));

                    rawPDFReport.ClientName = lblRawClientName.Text;
                    rawPDFReport.BuildingName = lblRawBuildingName.Text;
                    rawPDFReport.EquipmentName = lblRawEquipmentName.Text;
                    //rawPDFReport.EquipmentLocation = equipment.EquipmentLocation;
                    rawPDFReport.Points = lblRawPoints.Text;
                    rawPDFReport.StartDate = lblRawStartDate.Text;
                    rawPDFReport.EndDate = lblRawEndDate.Text;

                    rawPDFReport.ImageByteArray = DataMgr.DefaultCacheRepository.Get<Byte[]>(GetImageKey());
						//parentPage.sessionState.Get<Byte[]>(ID + "_imgStream");
                }
                else
                {
                    calculatedPDFReport = new ITextSharpHelper.CalculatedPDFReport();

                    //Equipment equipment = mDataManager.EquipmentDataMapper.GetEquipmentByID(Convert.ToInt32(hdnRawEID.Value));

                    calculatedPDFReport.ClientName = lblRawClientName.Text;
                    calculatedPDFReport.BuildingName = lblRawBuildingName.Text;
                    calculatedPDFReport.EquipmentName = lblRawEquipmentName.Text;
                    //calculatedPDFReport.EquipmentLocation = equipment.EquipmentLocation;
                    
                    //calculated points
                    calculatedPDFReport.CalculatedPoints = lblCalculatedPoints.Text;
                    
                    calculatedPDFReport.StartDate = lblRawStartDate.Text;
                    calculatedPDFReport.EndDate = lblRawEndDate.Text;

                    calculatedPDFReport.ImageByteArray = DataMgr.DefaultCacheRepository.Get<Byte[]>(GetImageKey());
						//parentPage.sessionState.Get<Byte[]>(ID + "_imgStream");
                }
            }
        }

        private string GetImageKey()
        {
            return String.Format("{0}-{1}-{2}", HttpContext.Current.Session.SessionID, ID, "_imgStream");
        }

        protected void radChart_OnZoom(object sender, EventArgs e)
        {
            //test           
        }
    }
}