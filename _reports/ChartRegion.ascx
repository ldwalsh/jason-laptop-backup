﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ChartRegion.ascx.cs" Inherits="CW.Website._reports.ChartRegion" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="cwcontrols" Namespace="CW.Website._controls" Assembly="CW.Website" %>

<div id="reportInfo" runat="server" visible="false">
<div>
    <h2 id="regionTitle" runat="server"></h2>
    <p>
        <asp:Label ID="lblResults" CssClass="narrowMessage" runat="server" Text="" Visible="false"></asp:Label>
    </p>
</div>
<div id="analysisInfo" visible="false" runat="server">
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Analysis Name:</label>
        <asp:Label CssClass="labelContent" ID="lblAnalysisName" runat="server" Text=""></asp:Label>
        <asp:HiddenField ID="hdnAID" runat="server" Visible="false" />
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Client Name:</label>
        <asp:Label CssClass="labelContent" ID="lblClientName" runat="server" Text=""></asp:Label>
        <asp:HiddenField ID="hdnCID" runat="server" Visible="false" />
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Building Name:</label>
        <asp:Label CssClass="labelContent" ID="lblBuildingName" runat="server" Text=""></asp:Label>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Equipment Name:</label>
        <asp:Label CssClass="labelContent" ID="lblEquipmentName" runat="server" Text=""></asp:Label>
        <asp:HiddenField ID="hdnEID" runat="server" Visible="false" />
    </div> 
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Associated Equipment:</label>
        <asp:HyperLink ID="lnkCollapsibleAssociatedEquipment" runat="server" CssClass="togglePadded"><asp:Label ID="lblCollapsibleAssociatedEquipment" runat="server"></asp:Label></asp:HyperLink>
        <ajaxToolkit:CollapsiblePanelExtender ID="associatedEquipmentCollapsiblePanelExtender" runat="Server"
                TargetControlID="pnlCollapsibleAssociatedEquipment"
                CollapsedSize="0"
                Collapsed="true" 
                ExpandControlID="lnkCollapsibleAssociatedEquipment"
                CollapseControlID="lnkCollapsibleAssociatedEquipment"
                AutoCollapse="false"
                AutoExpand="false"
                ScrollContents="false"
                ExpandDirection="Vertical"
                TextLabelID="lblCollapsibleAssociatedEquipment"
                CollapsedText="show associated equipment"
                ExpandedText="hide associated equipment" 
                /> 
                <asp:Panel ID="pnlCollapsibleAssociatedEquipment" runat="server"> 
                    <asp:Label CssClass="labelContent" ID="lblAssociatedEquipment" runat="server" Text=""></asp:Label>
                </asp:Panel>
    </div>      
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Points:</label>
        <asp:HyperLink ID="lnkCollapsiblePoints" runat="server" CssClass="togglePadded"><asp:Label ID="lblCollapsiblePoints" runat="server"></asp:Label></asp:HyperLink>        
        <ajaxToolkit:CollapsiblePanelExtender ID="pointsCollapsiblePanelExtender" runat="Server"
                TargetControlID="pnlCollapsiblePoints"
                CollapsedSize="0"
                Collapsed="true" 
                ExpandControlID="lnkCollapsiblePoints"
                CollapseControlID="lnkCollapsiblePoints"
                AutoCollapse="false"
                AutoExpand="false"
                ScrollContents="false"
                ExpandDirection="Vertical"
                TextLabelID="lblCollapsiblePoints"
                CollapsedText="show points"
                ExpandedText="hide points" 
                /> 
                <asp:Panel ID="pnlCollapsiblePoints" runat="server"> 
                    <asp:Label CssClass="labelContent" ID="lblPoints" runat="server" Text=""></asp:Label>
                </asp:Panel>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Associated Equip. Points:</label>
        <asp:HyperLink ID="lnkCollapsibleAssociatedEquipmentPoints" runat="server" CssClass="togglePadded"><asp:Label ID="lblCollapsibleAssociatedEquipmentPoints" runat="server"></asp:Label></asp:HyperLink>
        <ajaxToolkit:CollapsiblePanelExtender ID="associatedEquipmentPointsCollapsiblePanelExtender" runat="Server"
                TargetControlID="pnlCollapsibleAssociatedEquipmentPoints"
                CollapsedSize="0"
                Collapsed="true" 
                ExpandControlID="lnkCollapsibleAssociatedEquipmentPoints"
                CollapseControlID="lnkCollapsibleAssociatedEquipmentPoints"
                AutoCollapse="false"
                AutoExpand="false"
                ScrollContents="false"
                ExpandDirection="Vertical"
                TextLabelID="lblCollapsibleAssociatedEquipmentPoints"
                CollapsedText="show associated equipment points"
                ExpandedText="hide associated equipment points" 
                /> 
                <asp:Panel ID="pnlCollapsibleAssociatedEquipmentPoints" runat="server"> 
                    <asp:Label CssClass="labelContent" ID="lblAssociatedEquipmentPoints" runat="server" Text=""></asp:Label>
                </asp:Panel>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Display Interval:</label>
        <asp:Label CssClass="labelContent" ID="lblDisplayInterval" runat="server" Text=""></asp:Label>
        <asp:HiddenField ID="hdnAnalysisRange" runat="server" Visible="false" />
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Analysis Date:</label>
        <asp:Label CssClass="labelContent" ID="lblStartDate" runat="server" Text=""></asp:Label>
    </div>
    <%--<div class="divFormWideAndShort">
            <label class="labelNarrowBold">End Date:</label>
            <asp:Label CssClass="labelContent" ID="lblEndDate" runat="server" Text=""></asp:Label>
        </div>   --%>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Cost Savings:</label>
        <asp:Label CssClass="labelContent" ID="lblCost" runat="server" Text=""></asp:Label>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Energy Priority (0-10):</label>
        <asp:Label CssClass="labelContent" ID="lblEnergy" runat="server" Text=""></asp:Label>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Comfort Priority (0-10):</label>
        <asp:Label CssClass="labelContent" ID="lblComfort" runat="server" Text=""></asp:Label>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Maintenance Priority (0-10):</label>
        <asp:Label CssClass="labelContent" ID="lblMaintenance" runat="server" Text=""></asp:Label>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Notes:</label>
        <span class="richText">
            <asp:Literal ID="litNotes" runat="server" Text=""></asp:Literal>
        </span>
    </div>
    <div class="divGraph">
        <cwcontrols:CustomRadBinaryImage ID="binImageGraph" runat="server" />
        <asp:Literal ID="litGraphNotAvailable" runat="server" />
    </div>
</div>
<div id="rawInfo" visible="false" runat="server">
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Client Name:</label>
        <asp:Label CssClass="labelContent" ID="lblRawClientName" runat="server" Text=""></asp:Label>
        <asp:HiddenField ID="hdnRawCID" runat="server" Visible="false" />
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Building Name:</label>
        <asp:Label CssClass="labelContent" ID="lblRawBuildingName" runat="server" Text=""></asp:Label>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Equipment Name:</label>
        <asp:Label CssClass="labelContent" ID="lblRawEquipmentName" runat="server" Text=""></asp:Label>
        <asp:HiddenField ID="hdnRawEID" runat="server" Visible="false" />
    </div>
    <div id="divPoints" class="divFormWideAndShort" runat="server" visible="false">
        <label class="labelNarrowBold">Points:</label>
        <asp:Label CssClass="labelContentNarrow" ID="lblRawPoints" runat="server" Text=""></asp:Label>
        <asp:HiddenField ID="hdnPIDs" runat="server" Visible="false" />
    </div>
    <div id="divCalculatedPoints" class="divFormWideAndShort" runat="server" visible="false">
        <label class="labelNarrowBold">Calculated Points:</label>
        <asp:Label CssClass="labelContentNarrow" ID="lblCalculatedPoints" runat="server" Text=""></asp:Label>
        <asp:HiddenField ID="hdnVPIDs" runat="server" Visible="false" />
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">Start Date:</label>
        <asp:Label CssClass="labelContent" ID="lblRawStartDate" runat="server" Text=""></asp:Label>
    </div>
    <div class="divFormWideAndShort">
        <label class="labelNarrowBold">End Date:</label>
        <asp:Label CssClass="labelContent" ID="lblRawEndDate" runat="server" Text=""></asp:Label>
    </div>
    <div>
        <asp:LinkButton ID="resetChart" OnClientClick="ZoomOut(this); return false;" Text="Zoom Out" runat="server" Visible="false" />
    </div>
    <asp:Chart ID="rawChart" runat="server" ImageType="Png" EnableViewState="false" Height="620px" Width="1058px" Visible="false">
        <Titles>            
            <asp:Title Name="rawTitle" Font="Arial, 11pt, style=Bold" ForeColor="#333333"></asp:Title>
        </Titles>
        <Legends>            
            <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
        </Legends>        
        <Series></Series>
        <ChartAreas>
            <asp:ChartArea Name="rawChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                <AxisY Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false">
                    <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                    <MajorGrid LineColor="#CCCCCC" />
                    <MajorTickMark LineColor="#CCCCCC" />
                    <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                </AxisY>
                <AxisY2 Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="false">
                    <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                    <MajorGrid LineColor="#CCCCCC" />
                    <MajorTickMark LineColor="#CCCCCC" />
                    <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                </AxisY2>
                <AxisX Title="" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                    <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                    <MajorGrid LineColor="#CCCCCC" />
                    <MajorTickMark LineColor="#CCCCCC" />
                </AxisX>
            </asp:ChartArea>
        </ChartAreas>
    </asp:Chart>

    <telerik:RadChart ID="radChart" runat="server" EnableViewState="true" Height="620px" Width="1080px" AutoLayout="true" AutoTextWrap="true" Visible="false">
        <ClientSettings ScrollMode="Both" EnableZoom="true" ZoomRectangleColor="Orange" ZoomRectangleOpacity="0.4" />
        <ChartTitle TextBlock-Appearance-TextProperties-Font="Arial, 11pt, style=Bold" TextBlock-Appearance-TextProperties-Color="#333333"></ChartTitle>
        <Legend Appearance-ItemTextAppearance-TextProperties-Color="#333333"  Appearance-ItemTextAppearance-TextProperties-Font="Arial, 8pt" Appearance-Position-AlignedPosition="Center" Appearance-Overflow="Row" Visible="true">
            <Appearance Position-Auto="true"  ItemAppearance-Visible="true"></Appearance>
        </Legend>
        <PlotArea Appearance-FillStyle-FillType="Solid" Appearance-FillStyle-MainColor="White"> 
            <Appearance Border-Color="Black" Border-Width="1"></Appearance>
            <XAxis Appearance-TextAppearance-TextProperties-Color="#333333" Appearance-TextAppearance-TextProperties-Font="Arial, 7.75pt" AxisLabel-TextBlock-Text="Date and Time" Visible="true" AxisLabel-Visible="true">
                <Appearance MajorGridLines-Visible="true" MajorGridLines-Color="LightGray"></Appearance>
                <AxisLabel TextBlock-Appearance-TextProperties-Color="#333333" TextBlock-Appearance-TextProperties-Font="Arial, 9pt, style=Bold" Visible="true" TextBlock-Visible="true"></AxisLabel>
            </XAxis>
            <YAxis Appearance-TextAppearance-TextProperties-Color="#333333" Appearance-TextAppearance-TextProperties-Font="Arial, 7.75pt" AxisLabel-TextBlock-Text="Engineering Units" Visible="true" AxisLabel-Visible="true">
                <Appearance MajorGridLines-Visible="true" MajorGridLines-Color="LightGray"></Appearance>
                <AxisLabel TextBlock-Appearance-TextProperties-Color="#333333" TextBlock-Appearance-TextProperties-Font="Arial, 9pt, style=Bold" Visible="true" TextBlock-Visible="true"></AxisLabel>
            </YAxis>
        </PlotArea>
        <Series></Series> 
        <Appearance ImageQuality="AntiAlias" Border-Visible="false"></Appearance> 
    </telerik:RadChart>
</div>
<br />
<hr />
</div>
