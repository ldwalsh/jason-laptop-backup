﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;

using CW.Data;
using CW.Data.Helpers;
using CW.Utility;
using CW.Website._framework;
using CW.Data.Models.Diagnostics;
using CW.Data.Models.User;
using Telerik.Web.UI;
using CW.Common.Constants;
using System.IO;
//using Pechkin.Synchronized;
//using Pechkin;
//using Winnovative;
//using Aspose;
using EO.Pdf;
using Microsoft.WindowsAzure.ServiceRuntime;
using CW.Website._framework.httphandlers;
using CW.Reporting._fileservices;

namespace CW.Website
{
    public partial class BureauReport : SitePage
    {
        #region Properties

        private const string seTagline = "<h2>Make the most of your energy<sup>SM</sup></h2>";
        private const string kgsTagline = "<h2>Making buildings better</h2>";
        
        private enum ReportTheme { KGS, SE };
        
        private int cid, uid, oid, maintenanceFaultCountAccrossWholePeriod, previousMaintenanceFaultCountAccrossWholePeriod, comfortFaultCountAccrossWholePeriod, previousComfortFaultCountAccrossWholePeriod, lcid;
        private string introduction, recommended, energy, maintenance, comfort;        
        private double totalCost, previousTotalCost;
        private bool showLogo = false, isSETheme = false;
        private DateTime sd, ed, psd, ped;
        private List<int> bids;

        private Building building;
        private Client client;
        private Guid guid;

        #endregion

        #region Page Events

            protected void Page_Init(object sender, EventArgs e)
            {
                //if the scheduledBureau posted value exists
                if (!String.IsNullOrEmpty(Request.Form["$scheduledBureau"]) && Convert.ToBoolean(Request.Form["$scheduledBureau"]))
                {
                    //scheduled cid, uid, and oid get set from post
                }
                else
                {
                    //Check non anonymous. kgs super admin or higher or provider with super admin access to the client
                    if (siteUser.IsAnonymous || !(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient)))
                    {
                        Response.Redirect("/Home.aspx");
                    }


                    //dynamically add themed css and favicons based on se theme check
                    HtmlHead head = (HtmlHead)Page.Header;
                    HtmlLink link = new HtmlLink();
                    link.Attributes.Add("href", siteUser.IsSchneiderTheme ? Page.ResolveClientUrl("/_assets/styles/themes/ba.css") : Page.ResolveClientUrl("/_assets/styles/themes/cw.css"));
                    link.Attributes.Add("type", "text/css");
                    link.Attributes.Add("rel", "stylesheet");
                    head.Controls.Add(link);

                    link = new HtmlLink();
                    link.Attributes.Add("href", siteUser.IsSchneiderTheme ? Page.ResolveClientUrl("/_assets/styles/themes/images/se-favicon.ico") : Page.ResolveClientUrl("/_assets/styles/themes/images/cw-favicon.ico"));
                    link.Attributes.Add("type", "image/x-icon");
                    link.Attributes.Add("rel", "shortcut icon");
                    head.Controls.Add(link);

                    title.Text = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, title.Text).ToUpper();
                   
                    cid = siteUser.CID;
                    uid = siteUser.UID;
                    oid = siteUser.UserOID;
                }
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                    try
                    {
                        //GET FORM POST DATA----
                        GetFormPostData();

                        if (bids.Any())
                        {
                            //check if buildings are all of same culture
                            if (DataMgr.BuildingDataMapper.AreBuildingSettingsCulturesTheSame(bids))
                            {
                                if (bids.Count() > 1)
                                {
                                    //buildings
                                    IEnumerable<Building> buildings = DataMgr.BuildingDataMapper.GetBuildingsByBIDList(bids, true);

                                    //client
                                    client = DataMgr.ClientDataMapper.GetClient(cid);

                                    //header and new page building names
                                    headerBuilding.InnerText = newPageBuilding2.InnerText = newPageBuilding3.InnerText = "Mulitple Buildings";

                                    //building info
                                    clientName.InnerText = client.ClientName;
                                    buildingName.InnerText = "Mulitiple Buildings";
                                    buildingLocation.InnerText = "Varies";
                                    buildingType.InnerText = "Varies";
                                    buildingCount.InnerText = buildings.Count().ToString();
                                    buildingYear.InnerText = "Varies";
                                    buildingSqft.InnerText = buildings.Sum(b => b.Sqft).ToString();

                                    //set lcid
                                    lcid = buildings.First().BuildingSettings.First().LCID;
                                }
                                else
                                {
                                    //get building
                                    building = DataMgr.BuildingDataMapper.GetBuildingByBID(bids.First(), true, true, true, true, true, true);

                                    //header and new page building names
                                    headerBuilding.InnerText = newPageBuilding2.InnerText = newPageBuilding3.InnerText = building.BuildingName;

                                    //building info
                                    clientName.InnerText = building.Client.ClientName;
                                    buildingName.InnerText = building.BuildingName;
                                    buildingLocation.InnerText = building.Address;
                                    buildingType.InnerText = building.BuildingType.BuildingTypeName;
                                    buildingCount.InnerText = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(cid).Count().ToString();
                                    buildingYear.InnerText = building.YearBuilt.ToString();
                                    buildingSqft.InnerText = building.Sqft.ToString();

                                    //set lcid
                                    lcid = building.BuildingSettings.First().LCID;
                                }



                                //GET DIAGNOSTIC DATA-----
                                //diagnostics
                                IEnumerable<DiagnosticsResult> diagnosticsResults = DataMgr.DiagnosticsDataMapper.GetDiagnosticsResults(
                                    new DiagnosticsResultsInputs()
                                    {
                                        AnalysisRange = DataConstants.AnalysisRange.Daily,
                                        StartDate = sd, 
                                        EndDate = ed,
                                        CID = cid,
                                    },
                                    bids);

                                IEnumerable<DiagnosticsResult> previousDiagnosticsResults = DataMgr.DiagnosticsDataMapper.GetDiagnosticsResults(
                                                                        new DiagnosticsResultsInputs()
                                    {
                                        AnalysisRange = DataConstants.AnalysisRange.Daily, 
                                        StartDate = psd, 
                                        EndDate = ped,
                                        CID = cid
                                    }, bids);

                                totalCost = Math.Ceiling(diagnosticsResults.Sum(d => d.CostSavings));
                                previousTotalCost = Math.Ceiling(previousDiagnosticsResults.Sum(d => d.CostSavings));
                                maintenanceFaultCountAccrossWholePeriod = diagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                                previousMaintenanceFaultCountAccrossWholePeriod = previousDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                                comfortFaultCountAccrossWholePeriod = diagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
                                previousComfortFaultCountAccrossWholePeriod = previousDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();

                                    
                                //GET USER AND ORG INFO-----
                                //user
                                GetUserWithCountryAndState user = DataMgr.UserDataMapper.GetUserWithCountryAndState(uid);

                                //user organization
                                Organization userOrg = DataMgr.OrganizationDataMapper.GetOrganizationByID(oid);


                                //SET REPORT DATA-----

                                //set theme 
                                form1.Attributes["class"] = "bureau" + (isSETheme ? ReportTheme.SE : ReportTheme.KGS);

                                //header image
                                headerImage.Src = isSETheme ? "_assets/styles/themes/customReportImages/magnify-se.png" : "_assets/styles/themes/images/cw-gear-large.png";

                                //title
                                headerTitle.InnerText = TokenVariableHelper.FormatSpecialThemeBasedTokens(isSETheme, "$product");

                                //period
                                litHeaderPeriod.Text = String.Format("<div class='labelMedium'>{0} -<br />{1}</div>", sd.ToString("MMM. d, yyyy"), ed.ToString("MMM. d, yyyy"));

                                //newpage images, titles and taglines
                                imgNewPageBuilding2.Src = imgNewPageBuidling3.Src = isSETheme ? "_assets/styles/themes/customReportImages/magnify-small-se.png" : "_assets/styles/themes/images/cw-gears-large.png";
                                newPageTitle2.InnerText = newPageTitle3.InnerHtml = TokenVariableHelper.FormatSpecialThemeBasedTokens(isSETheme, "$product Report");
                                litTagline.Text = isSETheme ? seTagline : kgsTagline;

                                //bottom page images
                                if (showLogo)
                                {
                                    imgBottomPage.Src = imgBottomPage2.Src = isSETheme ? "_assets/styles/themes/images/se-header-secondary.png" : "_assets/styles/themes/images/kgs-header-no-shadow.png";
                                }
                                else
                                {
                                    imgBottomPage.Visible = imgBottomPage2.Visible = false;
                                }

                                //intro. use url decode because of form post.
                                litIntroduction.Text = Server.UrlDecode(introduction);

                                //TODO culture for currencty hex
                                //total avoidable energy
                                litAvoidableEnergyTotalValue.Text = String.Format("<span class='valueLargeColored'>{0}</span>", CultureHelper.FormatCurrencyAsString(lcid, totalCost));

                                //change in avoidable energy
                                litAvoidableEnergyChangeValue.Text = String.Format("<span class='valueLargeColored'>{0}</span>", (previousTotalCost - totalCost == 0 ? "N/A" : Math.Round(Math.Abs(((previousTotalCost - totalCost) / (NumericHelper.CleanseDivideByZero(previousTotalCost))) * 100), 2, MidpointRounding.AwayFromZero).ToString() + "%"));
                                divAvoidableEnergyChangeString.InnerHtml = (previousTotalCost > totalCost ? "Decrease" : (previousTotalCost == totalCost ? "Unchanged" : "Increase")) + " Since Last Report";

                                //prepared by
                                preparedBy.InnerText = String.Format("{0} - {1}", siteUser.FullName, userOrg.OrganizationName);
                                userAddress.InnerText = String.Format("{0}, {1}, {2} {3}", user.Address, user.City, user.StateName, user.Zip);
                                userPhone.InnerText = String.IsNullOrEmpty(user.Phone) ? user.MobilePhone : user.Phone;
                                userEmail.InnerText = user.Email;

                                //trend summary. round up to nearest cost int.
                                double reportNumberOfDays = DateTimeHelper.GetDiferenceBetweenDateTimesInDays(sd, ed) + 1;
                                double previousReportNumberOfDays = DateTimeHelper.GetDiferenceBetweenDateTimesInDays(psd, ped) + 1;


                                string averageEnergyPercent = Math.Round(((Math.Abs(Math.Ceiling(previousTotalCost / previousReportNumberOfDays) - Math.Ceiling(totalCost / reportNumberOfDays)) / Math.Ceiling(previousTotalCost / previousReportNumberOfDays)) * 100), 2, MidpointRounding.AwayFromZero).ToString() + "%";
                                litEnergyTrendSummary.Text = String.Format("<span class='labelLarge'>Avoidable energy costs average {0}/day. Costs {1}.</span>", CultureHelper.FormatCurrencyAsString(lcid, Math.Ceiling(totalCost / reportNumberOfDays)), (Math.Ceiling(previousTotalCost / previousReportNumberOfDays) > Math.Ceiling(totalCost / reportNumberOfDays) ? "decreasing by " + averageEnergyPercent : (Math.Ceiling(previousTotalCost / previousReportNumberOfDays) == Math.Ceiling(totalCost / reportNumberOfDays) ? "unchanged" : "increasing by " + averageEnergyPercent)));
                                imgEnergyArrow.Src = (Math.Ceiling(previousTotalCost / previousReportNumberOfDays) > Math.Ceiling(totalCost / reportNumberOfDays) ? "_assets/styles/themes/customReportImages/arrow-down" : (Math.Ceiling(previousTotalCost / previousReportNumberOfDays) == Math.Ceiling(totalCost / reportNumberOfDays) ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");

                                //TODO: based on avg priority per day or total faults per day?
                                string averageMaintenancePercent = Math.Round(((Math.Abs(Math.Ceiling(previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays) - Math.Ceiling(maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays)) / Math.Ceiling(previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays)) * 100), 2, MidpointRounding.AwayFromZero).ToString() + "%";
                                litMaintenanceTrendSummary.Text = String.Format("<span class='labelLarge'>Priority {0}; {1} total daily incidents.</span>", (Math.Ceiling(previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays) > Math.Ceiling(maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays) ? "decreasing by " + averageMaintenancePercent : (Math.Ceiling(previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays) == Math.Ceiling(maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays) ? "unchanged" : "increasing by " + averageMaintenancePercent)), Math.Ceiling(maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays));
                                imgMaintenanceArrow.Src = (Math.Ceiling(previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays) > Math.Ceiling(maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays) ? "_assets/styles/themes/customReportImages/arrow-down" : (Math.Ceiling(previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays) == Math.Ceiling(maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays) ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");

                                string averageComfortPercent = Math.Round(((Math.Abs(Math.Ceiling(previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays) - Math.Ceiling(comfortFaultCountAccrossWholePeriod / reportNumberOfDays)) / Math.Ceiling(previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays)) * 100), 2, MidpointRounding.AwayFromZero).ToString() + "%";
                                litComfortTrendSummary.Text = String.Format("<span class='labelLarge'>Priority {0}; {1} total daily incidents.</span>", (Math.Ceiling(previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays) > Math.Ceiling(comfortFaultCountAccrossWholePeriod / reportNumberOfDays) ? "decreasing by " + averageComfortPercent : (Math.Ceiling(previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays) == Math.Ceiling(comfortFaultCountAccrossWholePeriod / reportNumberOfDays) ? "unchanged" : "increasing by " + averageComfortPercent)), Math.Ceiling(comfortFaultCountAccrossWholePeriod / reportNumberOfDays));
                                imgComfortArrow.Src = (Math.Ceiling(previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays) > Math.Ceiling(comfortFaultCountAccrossWholePeriod / reportNumberOfDays) ? "_assets/styles/themes/customReportImages/arrow-down" : (Math.Ceiling(previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays) == Math.Ceiling(comfortFaultCountAccrossWholePeriod / reportNumberOfDays) ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");


                                //top issues                              
                                gridEnergy.DataSource = diagnosticsResults.Where(d => d.EnergyPriority != 0)
                                                                        .GroupBy(g => new { g.AID, g.EID })
                                                                        .Select(s => new DiagnosticsBureauReportResult
                                                                        {
                                                                            BuildingName = s.First().BuildingName,
                                                                            EquipmentName = s.First().EquipmentName,
                                                                            NotesSummary = s.OrderByDescending(c => c.CostSavings).First().NotesSummary,
                                                                            TotalCostSavings = s.Sum(c => c.CostSavings),
                                                                            Occurrences = s.Count(),
                                                                        })
                                                                        .OrderByDescending(c => c.TotalCostSavings).Take(5);
                                gridEnergy.DataBind();

                                gridMaintenance.DataSource = diagnosticsResults.Where(d => d.MaintenancePriority != 0)
                                                                        .GroupBy(g => new { g.AID, g.EID })
                                                                        .Select(s => new DiagnosticsBureauReportResult
                                                                        {
                                                                            BuildingName = s.First().BuildingName,
                                                                            EquipmentName = s.First().EquipmentName,
                                                                            NotesSummary = s.OrderByDescending(m => m.MaintenancePriority).First().NotesSummary,
                                                                            //MaintenancePriority = s.OrderByDescending(m => m.MaintenancePriority).First().MaintenancePriority,
                                                                            AverageMaintenancePriority = s.Average(m => m.MaintenancePriority),
                                                                            Occurrences = s.Count(),
                                                                            TotalPriority = s.Sum(m => m.MaintenancePriority),
                                                                        })
                                                                        .OrderByDescending(p => p.TotalPriority).Take(5);
                                gridMaintenance.DataBind();

                                gridComfort.DataSource = diagnosticsResults.Where(d => d.ComfortPriority != 0)
                                                                        .GroupBy(g => new { g.AID, g.EID })
                                                                        .Select(s => new DiagnosticsBureauReportResult
                                                                        {
                                                                            BuildingName = s.First().BuildingName,
                                                                            EquipmentName = s.First().EquipmentName,
                                                                            NotesSummary = s.OrderByDescending(c => c.ComfortPriority).First().NotesSummary,
                                                                            //ComfortPriority = s.OrderByDescending(c => c.ComfortPriority).First().ComfortPriority,
                                                                            AverageComfortPriority = s.Average(c => c.ComfortPriority),
                                                                            Occurrences = s.Count(),
                                                                            TotalPriority = s.Sum(c => c.ComfortPriority),
                                                                        })
                                                                        .OrderByDescending(p => p.TotalPriority).Take(5);
                                gridComfort.DataBind();

                                //recommend
                                litRecommended.Text = Server.UrlDecode(recommended);

                                //priorities
                                litEnergy.Text = Server.UrlDecode(energy);
                                litMaintenance.Text = Server.UrlDecode(maintenance);
                                litComfort.Text = Server.UrlDecode(comfort);

                                //trend charts-----

                                //energy chart
                                IEnumerable<DiagnosticsBureauReportResult> energyDiagnostics = diagnosticsResults.GroupBy(d => d.StartDate)
                                                                                                     .Select(s => new DiagnosticsBureauReportResult
                                                                                                            {
                                                                                                                StartDate = s.Key,
                                                                                                                TotalCostSavings = s.Sum(c => c.CostSavings)
                                                                                                            }).OrderBy(s => s.StartDate);

                                if (energyDiagnostics.Any())
                                {
                                    energyChart.Series.Clear();

                                    //set axis intervaltype
                                    energyChart.ChartAreas["energyChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    energyChart.ChartAreas["energyChartArea"].AxisX.LabelStyle.Format = "d";

                                    var series = new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 3,
                                        Name = "Line Series",
                                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                                        //LegendToolTip = ,
                                        //LegendText = ,
                                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.EnergyYellow)
                                    };

                                    //add data to series
                                    series.Points.DataBindXY(energyDiagnostics.ToArray(), "StartDate", energyDiagnostics.ToArray(), "TotalCostSavings");

                                    //interpolate
                                    energyChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero"; 
                                    
                                    energyChart.Series.Add(series);

                                    series = new Series
                                    {
                                        ChartType = SeriesChartType.Point,
                                        MarkerStyle = MarkerStyle.Circle,
                                        ShadowColor = Color.DimGray,
                                        ShadowOffset = 4,
                                        MarkerSize = 6,
                                        MarkerBorderWidth = 3,
                                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.EnergyYellow),
                                        MarkerColor = Color.White,
                                        //BorderWidth = 6,
                                        Name = "Point Series",
                                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                                        //LegendToolTip = ,
                                        //LegendText = ,
                                        //BorderColor = Color.Gold,
                                        //Color = Color.White,                            
                                    };

                                    series.Points.DataBindXY(energyDiagnostics.ToArray(), "StartDate", energyDiagnostics.ToArray(), "TotalCostSavings");

                                    //interpolate
                                    energyChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero"; 
                                    
                                    energyChart.Series.Add(series);
                                }


                                //maintenance chart
                                IEnumerable<DiagnosticsBureauReportResult> maintenanceDiagnostics = diagnosticsResults
                                                                                                     .GroupBy(d => d.StartDate)
                                                                                                     .Select(s => new DiagnosticsBureauReportResult
                                                                                                     {
                                                                                                         StartDate = s.Key,
                                                                                                         Occurrences = s.Where(m => m.MaintenancePriority != 0).Count(),
                                                                                                         AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                                                                                                     }).OrderBy(s => s.StartDate);

                                if (maintenanceDiagnostics.Any())
                                {
                                    maintenanceChart.Series.Clear();

                                    //set axis intervaltype
                                    maintenanceChart.ChartAreas["maintenanceChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    maintenanceChart.ChartAreas["maintenanceChartArea"].AxisX.LabelStyle.Format = "d";

                                    //area chart
                                    var series = new Series
                                    {
                                        ChartType = SeriesChartType.Area,
                                        Name = "Area Series",
                                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                                        //LegendToolTip = ,
                                        LegendText = "Average Maintenance Priority",
                                        Color = Color.DarkGray
                                    };

                                    series.YAxisType = AxisType.Secondary;
                                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "AverageMaintenancePriority");

                                    //interpolate
                                    maintenanceChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero"; 
                                    
                                    maintenanceChart.Series.Add(series);


                                    //line
                                    series = new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 3,
                                        Name = "Line Series",
                                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                                        //LegendToolTip = ,
                                        LegendText = "Total Maintenance Incidents",
                                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray)
                                    };

                                    //add data to series
                                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "Occurrences");

                                    //interpolate
                                    maintenanceChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero"; 
                                    
                                    maintenanceChart.Series.Add(series);


                                    //point
                                    series = new Series
                                    {
                                        ChartType = SeriesChartType.Point,
                                        MarkerStyle = MarkerStyle.Circle,
                                        ShadowColor = Color.DimGray,
                                        ShadowOffset = 4,
                                        MarkerSize = 6,
                                        MarkerBorderWidth = 3,
                                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray),
                                        MarkerColor = Color.White,
                                        //BorderWidth = 12,
                                        Name = "Point Series",
                                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                                        //LegendToolTip = ,
                                        //LegendText = ,
                                        Legend = "Hidden",
                                        //BorderColor = Color.DimGray,
                                        //Color = Color.White
                                    };
                                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "Occurrences");

                                    //interpolate
                                    maintenanceChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero"; 
                                    
                                    maintenanceChart.Series.Add(series);
                                }

                                //comfort chart
                                IEnumerable<DiagnosticsBureauReportResult> comfortDiagnostics = diagnosticsResults
                                                                                                     .GroupBy(d => d.StartDate)
                                                                                                     .Select(s => new DiagnosticsBureauReportResult
                                                                                                     {
                                                                                                         StartDate = s.Key,
                                                                                                         Occurrences = s.Where(c => c.ComfortPriority != 0).Count(),
                                                                                                         AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                                                                                                     }).OrderBy(s => s.StartDate);

                                if (comfortDiagnostics.Any())
                                {
                                    comfortChart.Series.Clear();

                                    //set axis intervaltype
                                    comfortChart.ChartAreas["comfortChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    comfortChart.ChartAreas["comfortChartArea"].AxisX.LabelStyle.Format = "d";

                                    //area chart
                                    var series = new Series
                                    {
                                        ChartType = SeriesChartType.Area,
                                        Name = "Area Series",
                                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                                        //LegendToolTip = ,
                                        LegendText = "Average Comfort Priority",
                                        Color = Color.LightSteelBlue,
                                    };

                                    series.YAxisType = AxisType.Secondary;
                                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "AverageComfortPriority");

                                    //interpolate
                                    comfortChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                                    comfortChart.Series.Add(series);


                                    //line
                                    series = new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 3,
                                        Name = "Line Series",
                                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                                        //LegendToolTip = ,
                                        //LegendText = ,
                                        LegendText = "Total Comfort Incidents",
                                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue)
                                    };

                                    //add data to series
                                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "Occurrences");

                                    //interpolate
                                    comfortChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                                    comfortChart.Series.Add(series);


                                    //point
                                    series = new Series
                                    {
                                        ChartType = SeriesChartType.Point,
                                        MarkerStyle = MarkerStyle.Circle,
                                        ShadowColor = Color.DimGray,
                                        ShadowOffset = 4,
                                        MarkerSize = 6,
                                        MarkerBorderWidth = 3,
                                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue),
                                        MarkerColor = Color.White,
                                        //BorderWidth = 12,
                                        Name = "Point Series",
                                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                                        //LegendToolTip = ,
                                        //LegendText = ,
                                        Legend = "Hidden"
                                        //BorderColor = Color.CornflowerBlue,
                                        //Color = Color.White
                                    };

                                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "Occurrences");

                                    //interpolate
                                    comfortChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                                    comfortChart.Series.Add(series);
                                }

                                pnlError.Visible = false;
                                pnlReport.Visible = true;

                            }
                            else
                            {
                                lblError.Text = "Selected buildings must be of the same culture.";
                                pnlError.Visible = true;
                                pnlReport.Visible = false;
                            }
                        }
                        else
                        {
                            lblError.Text = "No buildings were selected.";
                            pnlError.Visible = true;
                            pnlReport.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        lblError.Text = "Bureau report generation failed. Please contact an administrator.";
                        pnlError.Visible = true;
                        pnlReport.Visible = false;

                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Bureau report generation failed.", ex);
                    }
                }
            }

        #endregion

        #region Chart Methods

            protected void ChartMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
            {
                maintenanceChart.Legends[0].Position.Width = 85;
                maintenanceChart.Legends[0].Position.Height = 7;
                maintenanceChart.Legends[0].Position.X = 7;
                maintenanceChart.Legends[0].Position.Y = 84;
            }

            protected void ChartComfort_PrePaint(object sender, ChartPaintEventArgs e)
            {
                comfortChart.Legends[0].Position.Width = 85;
                comfortChart.Legends[0].Position.Height = 7;
                comfortChart.Legends[0].Position.X = 7;
                comfortChart.Legends[0].Position.Y = 84;
            } 

        #endregion

        #region Helper Methods

            private void GetFormPostData()
            {
                foreach (string key in Request.Form.AllKeys)
                {
                    //ignore cw header bar client ddl and module ddl

                    //start date
                    if (key.Contains("$txtStartDate"))
                    {
                        sd = Convert.ToDateTime(Request.Form[key]);
                    }
                    //end date
                    else if (key.Contains("$txtEndDate"))
                    {
                        ed = Convert.ToDateTime(Request.Form[key]);
                    }
                    //previous start date
                    else if (key.Contains("$txtPreviousStartDate"))
                    {
                        psd = Convert.ToDateTime(Request.Form[key]);
                    }
                    //previous end date
                    else if (key.Contains("$txtPreviousEndDate"))
                    {
                        ped = Convert.ToDateTime(Request.Form[key]);
                    }
                    //threshold
                    //else if (key.Contains("$ddlThreshold"))
                    //{
                    //    th = Convert.ToInt32(Request.Form[key]);
                    //}
                    //client ddl
                    //else if (key.Contains("$ddlClient"))
                    //{
                    //    cid = Convert.ToInt32(Request.Form[key]);
                    //}
                    //building lb
                    else if (key.Contains("$lbBuildings"))
                    {
                        bids = Request.Form[key].Split(',').Select(x => Int32.Parse(x)).ToList();
                    }
                    //theme ddl
                    else if (key.Contains("$ddlTheme"))
                    {
                        isSETheme = Convert.ToBoolean(Request.Form[key]);
                    }
                    //organization logo
                    else if (key.Contains("$chkShowLogo"))
                    {
                        //form post will post value as "on" only if it is selected, otherwise it wont be in the request.
                        showLogo = true;
                    }
                    //introduction
                    else if (key.Contains("$editorIntroduction"))
                    {
                        introduction = Request.Form[key];
                    }
                    //recommended
                    else if (key.Contains("$editorRecommendedActions"))
                    {
                        recommended = Request.Form[key];
                    }
                    //energy                                
                    else if (key.Contains("$editorEnergyTrend"))
                    {
                        energy = Request.Form[key];
                    }
                    //maintenance
                    else if (key.Contains("$editorMaintenanceTrend"))
                    {
                        maintenance = Request.Form[key];
                    }
                    //comfort
                    else if (key.Contains("$editorComfortTrend"))
                    {
                        comfort = Request.Form[key];
                    }
                    //cid (scheduled only)
                    else if (key.Contains("$scheduledCID"))
                    {
                        cid = Convert.ToInt32(Request.Form[key]);
                    }
                    //uid (scheduled only)
                    else if (key.Contains("$scheduledUID"))
                    {
                        uid = Convert.ToInt32(Request.Form[key]);
                    }
                    //oid (scheduled only)
                    else if (key.Contains("$scheduledOID"))
                    {
                        oid = Convert.ToInt32(Request.Form[key]);
                    }
                }
            }

            private void ToggleGridEdit(GridView gv, GridViewRowCollection rows)
            {
                foreach (GridViewRow row in rows)
                {
                    //if (row.RowState == DataControlRowState.Edit)
                    //{
                    //    ((Label)gridEnergy.Rows[row.RowIndex].Cells[3].Controls[2]).Text = ((TextBox)gridEnergy.Rows[row.RowIndex].Cells[3].Controls[4]).Text;
                    //}

                    //row.RowState = (row.RowState == DataControlRowState.Edit) ? DataControlRowState.Normal : DataControlRowState.Edit;

                    if ((gv.Rows[row.RowIndex].Cells[0].Controls[3]).Visible)
                    {
                        ((Label)gv.Rows[row.RowIndex].Cells[0].Controls[1]).Text = ((TextBox)gv.Rows[row.RowIndex].Cells[0].Controls[3]).Text;

                        (gv.Rows[row.RowIndex].Cells[0].Controls[3]).Visible = false;
                        (gv.Rows[row.RowIndex].Cells[0].Controls[1]).Visible = true;

                        ((Label)gv.Rows[row.RowIndex].Cells[1].Controls[1]).Text = ((TextBox)gv.Rows[row.RowIndex].Cells[1].Controls[3]).Text;

                        (gv.Rows[row.RowIndex].Cells[1].Controls[3]).Visible = false;
                        (gv.Rows[row.RowIndex].Cells[1].Controls[1]).Visible = true;

                        ((Label)gv.Rows[row.RowIndex].Cells[2].Controls[1]).Text = ((TextBox)gv.Rows[row.RowIndex].Cells[2].Controls[3]).Text;

                        (gv.Rows[row.RowIndex].Cells[2].Controls[3]).Visible = false;
                        (gv.Rows[row.RowIndex].Cells[2].Controls[1]).Visible = true;

                        ((Label)gv.Rows[row.RowIndex].Cells[3].Controls[1]).Text = ((TextBox)gv.Rows[row.RowIndex].Cells[3].Controls[3]).Text;

                        (gv.Rows[row.RowIndex].Cells[3].Controls[3]).Visible = false;
                        (gv.Rows[row.RowIndex].Cells[3].Controls[1]).Visible = true;

                        ((Label)gv.Rows[row.RowIndex].Cells[4].Controls[1]).Text = ((TextBox)gv.Rows[row.RowIndex].Cells[4].Controls[3]).Text;

                        (gv.Rows[row.RowIndex].Cells[4].Controls[3]).Visible = false;
                        (gv.Rows[row.RowIndex].Cells[4].Controls[1]).Visible = true;
                    }
                    else
                    {
                        (gv.Rows[row.RowIndex].Cells[0].Controls[3]).Visible = true;
                        (gv.Rows[row.RowIndex].Cells[0].Controls[1]).Visible = false;

                        (gv.Rows[row.RowIndex].Cells[1].Controls[3]).Visible = true;
                        (gv.Rows[row.RowIndex].Cells[1].Controls[1]).Visible = false;

                        (gv.Rows[row.RowIndex].Cells[2].Controls[3]).Visible = true;
                        (gv.Rows[row.RowIndex].Cells[2].Controls[1]).Visible = false;

                        (gv.Rows[row.RowIndex].Cells[3].Controls[3]).Visible = true;
                        (gv.Rows[row.RowIndex].Cells[3].Controls[1]).Visible = false;

                        (gv.Rows[row.RowIndex].Cells[4].Controls[3]).Visible = true;
                        (gv.Rows[row.RowIndex].Cells[4].Controls[1]).Visible = false;
                    }
                }
            }

            private void ToggleRadEdit(RadEditor re, Literal lit)
            {
                if (re.Visible)
                {
                    lit.Text = re.Content;
                }
                else
                {
                    re.Content = lit.Text;
                }

                lit.Visible = re.Visible;
                re.Visible = !re.Visible;                                              
            }

        #endregion

        #region Button Events

        protected void introductionButton_Click(object sender, EventArgs e)
        {
            ToggleRadEdit(editorIntroduction, litIntroduction);
        }
        protected void recommendedActionsButton_Click(object sender, EventArgs e)
        {
            ToggleRadEdit(editorRecommendedActions, litRecommended);
        }
        protected void energyTrendButton_Click(object sender, EventArgs e)
        {
            ToggleRadEdit(editorEnergyTrend, litEnergy);
        }
        protected void maintenanceTrendButton_Click(object sender, EventArgs e)
        {
            ToggleRadEdit(editorMaintenanceTrend, litMaintenance);
        }
        protected void comfortTrendButton_Click(object sender, EventArgs e)
        {
            ToggleRadEdit(editorComfortTrend, litComfort);
        }

        protected void energyGridEditButton_Click(object sender, EventArgs e)
        {
            ToggleGridEdit(gridEnergy, gridEnergy.Rows);  
        }
        protected void maintenanceGridEditButton_Click(object sender, EventArgs e)
        {
            ToggleGridEdit(gridMaintenance, gridMaintenance.Rows);
        }
        protected void comfortGridEditButton_Click(object sender, EventArgs e)
        {
            ToggleGridEdit(gridComfort, gridComfort.Rows);
        }

        protected void btnDownloadButton_Click(object sender, EventArgs e)
        {
            guid = Guid.NewGuid();
            string tempEnergyChart = Server.MapPath("\\tempCustomReports\\bureau_energy_" + guid + ".png");
            string tempMaintenanceChart = Server.MapPath("\\tempCustomReports\\bureau_maintenance_" + guid + ".png");
            string tempComfortChart = Server.MapPath("\\tempCustomReports\\bureau_comfort_" + guid + ".png");

            //Essential Objects-----------

            //Instanciate
            PdfDocument pdf = new PdfDocument();
            MemoryStream ms = new MemoryStream();
           
            //All Options
            HtmlToPdf.Options.InvisibleElementIds = "hdn_container;btnIntroduction;editorIntroduction;btnTopEnergy;btnTopMaintenance;btnTopComfort;btnRecommendedActions;editorRecommendedActions;" +
                                                    "btnEnergyTrend;editorEnergyTrend;btnMaintenanceTrend;editorMaintenanceTrend;btnComfortTrend;editorComfortTrend;" +
                                                    "divDownload;";
            HtmlToPdf.Options.OutputArea = new RectangleF(0.5f, 0.5f, 7.5f, 10f);
            HtmlToPdf.Options.NoScript = true; /*needed for multiple async downloads*/

            //Scheduled Post Options
            //HtmlToPdf.Options.AddPostData("$scheduledBureau", "true");
            //HtmlToPdf.Options.AddPostData("$scheduledCID", "1");
            //HtmlToPdf.Options.AddPostData("$scheduledUID", "1");
            //HtmlToPdf.Options.AddPostData("$scheduledOID", "1");
            //HtmlToPdf.Options.AddPostData("$txtStartDate", "01/01/2014");
            //HtmlToPdf.Options.AddPostData("$txtEndDate", "01/15/2014");
            //HtmlToPdf.Options.AddPostData("$txtPreviousStartDate", "01/01/2014");
            //HtmlToPdf.Options.AddPostData("$txtPreviousEndDate", "01/15/2014");
            //HtmlToPdf.Options.AddPostData("$lbBuildings", "1");
            //HtmlToPdf.Options.AddPostData("$ddlTheme", "true");
            //HtmlToPdf.Options.AddPostData("$chkShowLogo", "true");
            //HtmlToPdf.Options.AddPostData("$editorIntroduction", "");
            //HtmlToPdf.Options.AddPostData("$editorRecommendedActions", "");
            //HtmlToPdf.Options.AddPostData("$editorEnergyTrend", "");
            //HtmlToPdf.Options.AddPostData("$editorMaintenanceTrend", "");
            //HtmlToPdf.Options.AddPostData("$editorComfortTrend", "");


            //Get charts to write out locally
            energyChart.SaveImage(tempEnergyChart);
            maintenanceChart.SaveImage(tempMaintenanceChart);
            comfortChart.SaveImage(tempComfortChart);

            //User Button Convert
            HtmlToPdf.ConvertHtml(ReplacePaths(hdn_container.Value), pdf); 
            pdf.Save(ms);
            //byte[] pdfBytes = ms.GetBuffer();
            byte[] pdfBytes = ms.ToArray();


            //Scheduled Post Convert
            //HtmlToPdf.ConvertUrl("http://127.0.0.1:81/BureauReport4.aspx", ms);
            //byte[] pdfBytes = ms.ToArray();


            //Download
            Boolean result;

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    new CustomReportFileGenerator(pdfBytes),
                    LogMgr
                ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productBureauReport.pdf", true));
            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                return;
            }

            //clean up temp files
            File.Delete(tempEnergyChart);
            File.Delete(tempMaintenanceChart);
            File.Delete(tempComfortChart);           


            //TODO: move to scheduled reports later.
            //Clean up old chart files incase they didnt get deleted on post temp creation. 
            //DirectoryInfo di = new DirectoryInfo(Server.MapPath("\\temp\\"));
            //FileInfo[] files = di.GetFiles();
            //foreach (FileInfo fi in files)
            //{
            //    if (fi.CreationTimeUtc < DateTime.UtcNow.AddHours(-24))
            //        fi.Delete();
            //}


            if (result) return;
        }

        #endregion

        #region PDF Helper Methods


        //private void OnWarning(SimplePechkin converter, string warningtext)
        //{
        //    throw new NotImplementedException();
        //}

        //private void OnError(SimplePechkin converter, string errortext)
        //{
        //    throw new NotImplementedException();
        //}

        private string ReplacePaths(string html)
        {

            //set base url to current instance for this request.
            //baseTag.Attributes.Add("href", RoleEnvironment.CurrentRoleInstance.InstanceEndpoints.First().Value.ToString());
            string baseString = "id=\"baseTag\"";
            html = html.Replace(baseString, baseString + " " + "href=\"http://" + RoleEnvironment.CurrentRoleInstance.InstanceEndpoints.Values.First().IPEndpoint.ToString() + "/BureauReport.aspx\"");

            string handlerChartImg = "src=\"/ChartImg.axd?";
            string tempEnergyPath = "src=\"/tempCustomReports/bureau_energy_" + guid + ".png\"";
            string tempMaintenancePath = "src=\"/tempCustomReports/bureau_maintenance_" + guid + ".png\"";
            string tempComfortPath = "src=\"/tempCustomReports/bureau_comfort_" + guid + ".png\"";

            int start = 0, end = 0, length = 0;

            //energy
            start = html.IndexOf(handlerChartImg);
            end = html.IndexOf('"', start + 20);
            length = (end - start) + 1 ;            
            html = html.Replace(html.Substring(start, length), tempEnergyPath);
            
            //maintenance
            start = html.IndexOf(handlerChartImg);
            end = html.IndexOf('"', start + 20);
            length = (end - start) + 1;
            html = html.Replace(html.Substring(start, length), tempMaintenancePath);

            //comfort
            start = html.IndexOf(handlerChartImg);
            end = html.IndexOf('"', start + 20);
            length = (end - start) + 1;
            html = html.Replace(html.Substring(start, length), tempComfortPath);

            return html;
        }

        #endregion
    }
}
