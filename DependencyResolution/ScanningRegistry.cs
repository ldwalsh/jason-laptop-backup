﻿using CW.Business;
using CW.Business.Bootstrapping;
using CW.Business.Factories;
using CW.Caching.StackExchange;
using CW.Common.Config;
using CW.Common.Constants;
using CW.Data.AzureStorage.Config;
using CW.Data.AzureStorage.RetryPolicies;
using CW.Data.Models;
using CW.Logging;
using CW.Logging.Log4Net;
using CW.Serializers.ProtoBufNet;
using CW.Website.Logging;
using Newtonsoft.Json.Linq;
using StructureMap.Configuration.DSL;
using System.Configuration;

namespace CW.Website.DependencyResolution
{
    public class ScanningRegistry : Registry
    {
        private readonly string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CWConnectionString"].ConnectionString;
        public ScanningRegistry()
        {
            AzureConfigManager configMgr = new AzureConfigManager();
            var sessionLogger = new SessionLogger(new Log4NetLogger(configMgr.GetConfigurationSetting(DataConstants.ConfigKeyHasDebugLogging, false)), new[] { DataConstants.LogLevel.ERROR });
            JArray storageAccounts = JArray.Parse(System.Configuration.ConfigurationManager.AppSettings["storageaccounts"]);
            string cwEnvironment = System.Configuration.ConfigurationManager.AppSettings["CWEnvironment"];
            string storageKeyPosition = System.Configuration.ConfigurationManager.AppSettings["StorageKeyPosition"];

            var cacheStoreFactory = new StackExchangeRedisFactory(new CascadeSerializer(),
                (l, m, ex, ln) => { }, // update to support logging 
                DefaultRetryPolicyFactory.Create(3, 10), ConfigExporter.GetAppSettingsAsDictionary(ConfigurationManager.AppSettings), false);

            var csm = new CoreSettingsManager(DataConstants.LogSection.Website, ConnectionString, cwEnvironment, StorageAccounts.CreateInstance(storageAccounts, storageKeyPosition), cacheStoreFactory, null, null, sessionLogger);
            var smm = new StructureMapManager(this);

            smm.Assign(csm.Settings, true);

            // initial setup of logger
            new LogSetupWrapper(csm.Settings.DataManagerCommon).Setup();

            For<ILogManager>().Singleton().Use(sessionLogger);
            For<ISectionLogManager>().Singleton().Use(new SectionLogManager(sessionLogger, DataConstants.LogSection.Website));

            // StructureMap does NOT recommend using old ASP.NET lifecycles, modern web frameworks will be able to manage better 
            // http://structuremap.github.io/object-lifecycle/supported-lifecycles/ (see Legacy ASP.Net Lifecycles)
            For<DataManager>().AlwaysUnique().Use(ctx => ctx.GetInstance<IDataManagerFactory>().Create(ConnectionString));
            For<DataManagerCommonStorage>().Singleton().Use(csm.Settings.DataManagerCommon);
        }
    }
}