﻿using StructureMap;
using StructureMap.Configuration.DSL;

namespace CW.Website.DependencyResolution
{
    public static class IoC
    {
        #region properties

            #region static

                private static IContainer Container { get; set; }

            #endregion

        #endregion

        #region methods

			public static void Init(Registry registry)=> Container=new Container(registry);

            public static T Resolve<T>()
            {
                return Container.GetInstance<T>();
            }

        #endregion
    }
}