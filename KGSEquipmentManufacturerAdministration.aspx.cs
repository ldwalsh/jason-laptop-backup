﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.EquipmentManufacturer;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSEquipmentManufacturerAdministration: SitePage
    {
        #region Properties

            private EquipmentManufacturer mEquipmentManufacturer;
            const string addEquipmentManufacturerSuccess = " equipment manufacturer addition was successful.";
            const string addEquipmentManufacturerFailed = "Adding equipment manufacturer failed. Please contact an administrator.";
            const string addEquipmentManufacturerNameExists = "Cannot add equipment manufacturer because the name already exists.";
            const string updateSuccessful = "Equipment manufacturer update was successful.";
            const string updateFailed = "Equipment manufacturer update failed. Please contact an administrator.";
            const string updateEquipmentManufacturerNameExists = "Cannot update equipment manufacturer name because the name already exists.";
            const string deleteSuccessful = "Equipment manufacturer deletion was successful.";
            const string deleteFailed = "Equipment manufacturer deletion failed. Please contact an administrator.";
            const string associatedToEquipment = "Cannot delete equipment manufacturer becuase an equipment requires it.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "ManufacturerName";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindEquipmentManufacturers();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindCountries(ddlEditManufacturerCountry, ddlAddManufacturerCountry);

                    sessionState["Search"] = String.Empty;

                    if (!queryString.IsEmpty)
                    {
                        ProcessRequestWithQueryString();
                    }
                }                  
            }

        #endregion

        #region Set Fields and Data

            protected void SetEquipmentManufacturerIntoEditForm(CW.Data.EquipmentManufacturer equipmentManufacturer)
            {
                //ID
                hdnEditID.Value = Convert.ToString(equipmentManufacturer.ManufacturerID);
                
                //Equipment Manufacturer Name (set both the editable field and the hidden field in case the user changes this value)
                txtEditManufacturerName.Text = equipmentManufacturer.ManufacturerName;
                hdnEditManufacturerName.Value = equipmentManufacturer.ManufacturerName;

                //Phone
                txtEditManufacturerPhone.Text = equipmentManufacturer.ManufacturerPhone;
                //ReferenceID
                txtEditManufacturerEmail.Text = equipmentManufacturer.ManufacturerEmail;
                //Description
                txtEditDescription.Value = equipmentManufacturer.ManufacturerDescription;
                //Address
                txtEditManufacturerAddress.Text = equipmentManufacturer.ManufacturerAddress;
                //City
                txtEditManufacturerCity.Text = equipmentManufacturer.ManufacturerCity;
                //Country Alpha2Code
                try
                {
                    ddlEditManufacturerCountry.SelectedValue = equipmentManufacturer.ManufacturerCountryAlpha2Code;

                    ddlEditManufacturerCountry_OnSelectedIndexChanged(null, null);

                    //set zip validators accordingly
                    editManufacturerZipRegExValidator.Enabled = equipmentManufacturer.ManufacturerCountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                    txtEditManufacturerZip.Mask = equipmentManufacturer.ManufacturerCountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa"; 

                    //StateID
                    try
                    {
                        ddlEditManufacturerState.SelectedValue = Convert.ToString(equipmentManufacturer.ManufacturerStateID);
                    }
                    catch
                    {
                    }
                }
                catch
                {
                }
                //ZIP
                txtEditManufacturerZip.Text = equipmentManufacturer.ManufacturerZip;
            }

        #endregion

        #region Load and Bind Fields

            private void BindCountries(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<Country> countries = DataMgr.CountryDataMapper.GetAllCountries();

                ddl.DataTextField = "CountryName";
                ddl.DataValueField = "Alpha2Code";
                ddl.DataSource = countries;
                ddl.DataBind();

                ddl2.DataTextField = "CountryName";
                ddl2.DataValueField = "Alpha2Code";
                ddl2.DataSource = countries;
                ddl2.DataBind();
            }

            private void BindStates(DropDownList ddl, string alpha2Code)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code);
                ddl.DataBind();
            }

        #endregion

        #region Load Equipment Manufacturer

            protected void LoadAddFormIntoEquipmentManufacturer(EquipmentManufacturer equipmentManufacturer)
            {
                //Equipment Manufacturer Name
                equipmentManufacturer.ManufacturerName = txtAddManufacturerName.Text;
                //Phone
                equipmentManufacturer.ManufacturerPhone = String.IsNullOrEmpty(txtAddManufacturerPhone.Text) ? null : txtAddManufacturerPhone.Text; 
                //Email
                equipmentManufacturer.ManufacturerEmail = String.IsNullOrEmpty(txtAddManufacturerEmail.Text) ? null : txtAddManufacturerEmail.Text;
                //Description
                equipmentManufacturer.ManufacturerDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                //Address
                equipmentManufacturer.ManufacturerAddress = String.IsNullOrEmpty(txtAddManufacturerAddress.Text) ? null : txtAddManufacturerAddress.Text;
                //City
                equipmentManufacturer.ManufacturerCity = String.IsNullOrEmpty(txtAddManufacturerCity.Text) ? null : txtAddManufacturerCity.Text;
                //CountryAlpha2Code
                equipmentManufacturer.ManufacturerCountryAlpha2Code = ddlAddManufacturerCountry.SelectedValue != "-1" ? ddlAddManufacturerCountry.SelectedValue : null;
                //StateID
                equipmentManufacturer.ManufacturerStateID = ddlAddManufacturerState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddManufacturerState.SelectedValue) : null;
                //ZIP
                equipmentManufacturer.ManufacturerZip = String.IsNullOrEmpty(txtAddManufacturerZip.Text) ? null : txtAddManufacturerZip.Text;

                equipmentManufacturer.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoEquipmentManufacturer(EquipmentManufacturer equipmentManufacturer)
            {
                //ID
                equipmentManufacturer.ManufacturerID = Convert.ToInt32(hdnEditID.Value);
                //Equipment Manufacturer Name
                equipmentManufacturer.ManufacturerName = txtEditManufacturerName.Text;
                //Phone
                equipmentManufacturer.ManufacturerPhone = String.IsNullOrEmpty(txtEditManufacturerPhone.Text) ? null : txtEditManufacturerPhone.Text;
                //Email
                equipmentManufacturer.ManufacturerEmail = String.IsNullOrEmpty(txtEditManufacturerEmail.Text) ? null : txtEditManufacturerEmail.Text;
                //Description
                equipmentManufacturer.ManufacturerDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //Address
                equipmentManufacturer.ManufacturerAddress = String.IsNullOrEmpty(txtEditManufacturerAddress.Text) ? null : txtEditManufacturerAddress.Text;
                //City
                equipmentManufacturer.ManufacturerCity = String.IsNullOrEmpty(txtEditManufacturerCity.Text) ? null : txtEditManufacturerCity.Text;
                //CountryAlpha2Code
                equipmentManufacturer.ManufacturerCountryAlpha2Code = ddlEditManufacturerCountry.SelectedValue != "-1" ? ddlEditManufacturerCountry.SelectedValue : null;
                //StateID
                equipmentManufacturer.ManufacturerStateID = ddlEditManufacturerState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditManufacturerState.SelectedValue) : null;
                //ZIP
                equipmentManufacturer.ManufacturerZip = String.IsNullOrEmpty(txtEditManufacturerZip.Text) ? null : txtEditManufacturerZip.Text;
                }

        #endregion

        #region Dropdown Events

            protected void ddlAddManufacturerCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlAddManufacturerState, ddlAddManufacturerCountry.SelectedValue);

                addManufacturerZipRegExValidator.Enabled = ddlAddManufacturerCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtAddManufacturerZip.Mask = ddlAddManufacturerCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            }
            protected void ddlEditManufacturerCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlEditManufacturerState, ddlEditManufacturerCountry.SelectedValue);

                editManufacturerZipRegExValidator.Enabled = ddlEditManufacturerCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtEditManufacturerZip.Mask = ddlEditManufacturerCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add equipment manufacturer Button on click.
            /// </summary>
            protected void addEquipmentManufacturerButton_Click(object sender, EventArgs e)
            {
                mEquipmentManufacturer = new EquipmentManufacturer();

                //load the form into the equiment manufacturer
                LoadAddFormIntoEquipmentManufacturer(mEquipmentManufacturer);

                try
                {
                    //insert new equipment manufacturer, check if the manufacturer name has been entered already. If it has display the error and disallow insertion of the record
                    if (DataMgr.EquipmentManufacturerDataMapper.DoesEquipmentManufacturerNameExist(mEquipmentManufacturer.ManufacturerName))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, addEquipmentManufacturerNameExists, lnkSetFocusAdd);
                    }
                    else
                    {
                        DataMgr.EquipmentManufacturerDataMapper.InsertEquipmentManufacturer(mEquipmentManufacturer);
                        LabelHelper.SetLabelMessage(lblAddError, mEquipmentManufacturer.ManufacturerName + addEquipmentManufacturerSuccess, lnkSetFocusAdd);
                    }
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment manufacturer.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addEquipmentManufacturerFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment manufacturer.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addEquipmentManufacturerFailed, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update equipment manufacturer Button on click. Updates equipment manufacturer data.
            /// </summary>
            protected void updateEquipmentManufacturerButton_Click(object sender, EventArgs e)
            {
                EquipmentManufacturer mEquipmentManufacturer = new EquipmentManufacturer();

                //load the form into the Equipment Manufacturer
                LoadEditFormIntoEquipmentManufacturer(mEquipmentManufacturer);

                //try to update the Equipment Manufacturer           
                try
                {
                    //Only call the method check for existing manufacturers if the name value has changed (via the hidden value)
                    if (hdnEditManufacturerName.Value != mEquipmentManufacturer.ManufacturerName)
                    {
                        //update existing equipment manufacturer, check if the new manufacturer name has been entered. If it has display the error and disallow update of the record
                        if (DataMgr.EquipmentManufacturerDataMapper.DoesEquipmentManufacturerNameExist(mEquipmentManufacturer.ManufacturerName))
                        {
                            LabelHelper.SetLabelMessage(lblEditError, updateEquipmentManufacturerNameExists, lnkSetFocusAdd);
                        }
                        else
                            UpdateEquipmentManufacturer(mEquipmentManufacturer);
                    }
                    else
                        UpdateEquipmentManufacturer(mEquipmentManufacturer);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusAdd);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment manufacturer.", ex);
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindEquipmentManufacturers();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindEquipmentManufacturers();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind Equipment Manufacturer grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind equipment variables to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindEquipmentManufacturers();
                }
            }

        #endregion

        #region Grid Events

            protected void gridEquipmentManufacturers_OnDataBound(object sender, EventArgs e)
            {                             
                //CANNOT DELETE CERTAIN TYPES
                GridViewRowCollection rows = gridEquipmentManufacturers.Rows;

                foreach (GridViewRow row in rows)
                {
                    if ((row.RowState & DataControlRowState.Edit) == 0)
                    {
                        //hide delete link for undeletable equipment types
                        try
                        {
                            //get rows datakey which right now is just one single key. pointtype id.
                            if (BusinessConstants.EquipmentManufacturer.UnDeletableEquipmentManufacturersDictionary.ContainsValue(Convert.ToInt32(((GridView)sender).DataKeys[row.RowIndex].Value)))
                            {
                                //hide delete link
                                ((LinkButton)row.Cells[5].Controls[1]).Visible = false;
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }

            protected void gridEquipmentManufacturers_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditEquipmentManufacturer.Visible = false;
                dtvEquipmentManufacturer.Visible = true;
                
                int equipmentManufacturerID = Convert.ToInt32(gridEquipmentManufacturers.DataKeys[gridEquipmentManufacturers.SelectedIndex].Values["ManufacturerID"]);

                //set data source
                dtvEquipmentManufacturer.DataSource = DataMgr.EquipmentManufacturerDataMapper.GetFullEquipmentManufacturerByID(equipmentManufacturerID);
                //bind Equipment Manufacturer to details view
                dtvEquipmentManufacturer.DataBind();
            }

            protected void gridEquipmentManufacturers_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridEquipmentManufacturers_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvEquipmentManufacturer.Visible = false;
                pnlEditEquipmentManufacturer.Visible = true;

                int equipmentManufacturerID = Convert.ToInt32(gridEquipmentManufacturers.DataKeys[e.NewEditIndex].Values["ManufacturerID"]);

                EquipmentManufacturer mEquipmentManufacturer = DataMgr.EquipmentManufacturerDataMapper.GetEquipmentManufacturer(equipmentManufacturerID);

                //Set Equipment Manufacturer data
                SetEquipmentManufacturerIntoEditForm(mEquipmentManufacturer);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridEquipmentManufacturers_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows equipmentManufacturerid
                int equipmentManufacturerID = Convert.ToInt32(gridEquipmentManufacturers.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a Equipment Manufacturer if it as not been assocaited with
                    //an equipment

                    //check if a equipment is assocated with the Equipment Manufacturers
                    if (DataMgr.EquipmentManufacturerDataMapper.IsEquipmentAssociatedWithEquipmentManufacturer(equipmentManufacturerID))
                    {
                        lblErrors.Text = associatedToEquipment;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }

                    else
                    {
                        //delete equipment variable
                        DataMgr.EquipmentManufacturerDataMapper.DeleteEquipmentManufacturer(equipmentManufacturerID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting equipment manufacturer.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryEquipmentManufacturers());
                gridEquipmentManufacturers.PageIndex = gridEquipmentManufacturers.PageIndex;
                gridEquipmentManufacturers.DataSource = SortDataTable(dataTable as DataTable, true);
                gridEquipmentManufacturers.DataBind();

                //originally was using dataTable.ExtendedProperties.Count, that is used primarily for attributes of rows and columns, not rows directly, see article http://t8852.codeinpro.us/q/508148594f1eba38a44feb81
                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditEquipmentManufacturer.Visible = false;  
            }

            protected void gridEquipmentManufacturers_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEquipmentManufacturers(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridEquipmentManufacturers.DataSource = SortDataTable(dataTable, true);
                gridEquipmentManufacturers.PageIndex = e.NewPageIndex;
                gridEquipmentManufacturers.DataBind();
            }

            protected void gridEquipmentManufacturers_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridEquipmentManufacturers.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEquipmentManufacturers(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridEquipmentManufacturers.DataSource = SortDataTable(dataTable, false);
                gridEquipmentManufacturers.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetEquipmentManufacturerData> QueryEquipmentManufacturers()
            {
                try
                {
                    //get all equipment manufacturers 
                    return DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturersWithPartialData();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturers.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturers.", ex);
                    return null;
                }
            }

            private IEnumerable<GetEquipmentManufacturerData> QuerySearchedEquipmentManufacturers(string searchText)
            {
                try
                {
                    //get all equipment manufacturers 
                    return DataMgr.EquipmentManufacturerDataMapper.GetAllSearchedEquipmentManufacturersWithPartialData(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturers.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturers.", ex);
                    return null;
                }
            }

            private void BindEquipmentManufacturers()
            {
                //query equipment manufacturers
                IEnumerable<GetEquipmentManufacturerData> variables = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryEquipmentManufacturers() : QuerySearchedEquipmentManufacturers(txtSearch.Text);

                int count = variables.Count();
                
                gridEquipmentManufacturers.DataSource = variables;

                // bind grid
                gridEquipmentManufacturers.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedEquipmentManufacturers(string[] searchText)
            //{
            //    //query equipment manufacturers
            //    IEnumerable<GetEquipmentManufacturerData> variables = QuerySearchedEquipmentManufacturers(searchText);

            //    int count = variables.Count();

            //    gridEquipmentManufacturers.DataSource = variables;

            //    // bind grid
            //    gridEquipmentManufacturers.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} equipment manufacturers found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} equipment manufacturers found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No equipment manufacturers found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }         

            public void ProcessRequestWithQueryString()
            {
                if (!String.IsNullOrEmpty(Request.QueryString["mid"]))
                {
                    int equipmentManufacturerID;

                    if (!Int32.TryParse(Request.QueryString["mid"].ToString(), out equipmentManufacturerID)) return;

                    IEnumerable<GetEquipmentManufacturerData> equpmentManufacturer = DataMgr.EquipmentManufacturerDataMapper.GetFullEquipmentManufacturerByID(equipmentManufacturerID);

                    if (equpmentManufacturer.Any())
                    {
                        //set data source
                        dtvEquipmentManufacturer.DataSource = equpmentManufacturer;
                        //bind Equipment Manufacturer to details view
                        dtvEquipmentManufacturer.DataBind();
                    }
                }
            }

            private void UpdateEquipmentManufacturer(EquipmentManufacturer equipmentManufacturer)
            {
                DataMgr.EquipmentManufacturerDataMapper.UpdateEquipmentManufacturer(equipmentManufacturer);

                //re-update the hidden value, other if there is another edit, the subsequent changed value check will be incorrect.
                hdnEditManufacturerName.Value = equipmentManufacturer.ManufacturerName;

                lblEditError.Text = updateSuccessful;
                lblEditError.Visible = true;
                lnkSetFocusView.Focus();

                //Bind Equipment Manufacturers again
                BindEquipmentManufacturers();
            }

        #endregion
    }
}

