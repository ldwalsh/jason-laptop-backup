﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Telerik.Web.UI;

namespace Telerik.Web.Examples.Dock.MyPortal
{
    public partial class DefaultCS : System.Web.UI.Page
    {
        private bool _dockStateCleared = false;
        private List<DockState> CurrentDockStates
        {
            get
            {
                //Store the info about the added docks in the session. For real life
                // applications we recommend using database or other storage medium
                // for persisting this information.
                List<DockState> _currentDockStates = (List<DockState>)Session["CurrentDockStatesMyPortal"];
                if (Object.Equals(_currentDockStates, null))
                {
                    _currentDockStates = new List<DockState>();
                    Session["CurrentDockStatesMyPortal"] = _currentDockStates;
                }
                return _currentDockStates;
            }
            set
            {
                Session["CurrentDockStatesMyPortal"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DropDownZone.DataBind();
            }
        }

        public ArrayList GetZones()
        {
            ArrayList zones = new ArrayList();
            zones.Add(RadDockZone1);
            zones.Add(RadDockZone2);
            zones.Add(RadDockZone3);
            return zones;
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            //Recreate the docks in order to ensure their proper operation
            for (int i = 0; i < CurrentDockStates.Count; i++)
            {

                RadDock dock = CreateRadDockFromState(CurrentDockStates[i]);
                //We will just add the RadDock control to the RadDockLayout.
                // You could use any other control for that purpose, just ensure
                // that it is inside the RadDockLayout control.
                // The RadDockLayout control will automatically move the RadDock
                // controls to their corresponding zone in the LoadDockLayout
                // event (see below).
                RadDockLayout1.Controls.Add(dock);
                //We want to save the dock state every time a dock is moved.
                CreateSaveStateTrigger(dock);
                //Load the selected widget
                LoadWidget(dock);
                if (CurrentDockStates[i].Closed == true)
                {
                    dock.Visible = false;
                }
            }
        }

        protected void RadDockLayout1_LoadDockLayout(object sender, DockLayoutEventArgs e)
        {
            //Populate the event args with the state information. The RadDockLayout control
            // will automatically move the docks according that information.
            foreach (DockState state in CurrentDockStates)
            {
                e.Positions[state.UniqueName] = state.DockZoneID;
                e.Indices[state.UniqueName] = state.Index;
            }
        }

        protected void RadDockLayout1_SaveDockLayout(object sender, DockLayoutEventArgs e)
        {
            if (!_dockStateCleared)
            {
                //Save the dock state in the session. This will enable us
                // to recreate the dock in the next Page_Init.
                CurrentDockStates = RadDockLayout1.GetRegisteredDocksState();
            }
            else
            {
                //the clear state button was clicked, so we refresh the page and start over.
                Response.Redirect(Request.RawUrl, false);
            }
        }

        private RadDock CreateRadDockFromState(DockState state)
        {
            RadDock dock = new RadDock();
            dock.DockMode = DockMode.Docked;
            dock.ID = string.Format("RadDock{0}", state.UniqueName);
            dock.ApplyState(state);
            dock.Commands.Add(new DockCloseCommand());
            dock.Commands.Add(new DockExpandCollapseCommand());

            return dock;
        }

        private RadDock CreateRadDock()
        {
            RadDock dock = new RadDock();
            dock.DockMode = DockMode.Docked;
            dock.UniqueName = Guid.NewGuid().ToString().Replace("-", "a");
            dock.ID = string.Format("RadDock{0}", dock.UniqueName);
            dock.Title = "Dock";
            dock.Text = string.Format("Added at {0}", DateTime.Now);
            dock.Width = Unit.Pixel(300);

            dock.Commands.Add(new DockCloseCommand());
            dock.Commands.Add(new DockExpandCollapseCommand());

            return dock;
        }



        private void CreateSaveStateTrigger(RadDock dock)
        {
            //Ensure that the RadDock control will initiate postback
            // when its position changes on the client or any of the commands is clicked.
            //Using the trigger we will "ajaxify" that postback.
            dock.AutoPostBack = true;
            dock.CommandsAutoPostBack = true;

            AsyncPostBackTrigger saveStateTrigger = new AsyncPostBackTrigger();
            saveStateTrigger.ControlID = dock.ID;
            saveStateTrigger.EventName = "DockPositionChanged";
            UpdatePanel1.Triggers.Add(saveStateTrigger);

            saveStateTrigger = new AsyncPostBackTrigger();
            saveStateTrigger.ControlID = dock.ID;
            saveStateTrigger.EventName = "Command";
            UpdatePanel1.Triggers.Add(saveStateTrigger);
        }

        private void LoadWidget(RadDock dock)
        {
            if (string.IsNullOrEmpty(dock.Tag) || dock.Closed)
            {
                return;
            }
            Control widget = LoadControl(dock.Tag);
            dock.ContentContainer.Controls.Add(widget);
        }


        protected void ButtonAddDock_Click(object sender, EventArgs e)
        {
            RadDock dock = CreateRadDock();
            //find the target zone and add the new dock there
            RadDockZone dz = (RadDockZone)FindControl(DropDownZone.SelectedItem.Text);

            //adding the dock to the docklayout and then docking it to the zone to avoid ViewState issues on subsequent postback
            RadDockLayout1.Controls.Add(dock);
            dock.Dock(dz);

            CreateSaveStateTrigger(dock);

            //Load the selected widget in the RadDock control
            dock.Tag = DroptDownWidget.SelectedValue;
            LoadWidget(dock);
        }
        protected void ButtonPostBack_Click(object sender, EventArgs e)
        {
            //normal postback
        }
        protected void ButtonClear_Click(object sender, EventArgs e)
        {
            //clear docks state from the session
            CurrentDockStates.Clear();
            _dockStateCleared = true;
        }
    }
}