﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

using CW.Business;
using CW.Data;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Data.Models.UserRole;
using CW.Utility;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class SystemUserRoleAdministration : SitePage
    {
        #region Properties

            //private UserRole mUserRole;
            const string updateSuccessful = "User role update was successful.";
            const string updateFailed = "User role update failed. Please contact an administrator.";
            const string updateUserRoleNameExists = "Cannot update user role name because the name already exists.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "Role";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //hide labels
                lblErrors.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindUserRoles();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetUserRoleIntoEditForm(CW.Data.UserRole userRole)
            {
                //ID
                hdnEditID.Value = Convert.ToString(userRole.RoleID);
                lblEditRoleID.Text = Convert.ToString(userRole.RoleID);
                //User Role Name
                txtEditRoleName.Text = userRole.RoleName;
                //User Role Description
                txtEditDescription.Value = String.IsNullOrEmpty(userRole.RoleDescription) ? null : userRole.RoleDescription;
                //Visible
                chkEditVisible.Checked = userRole.IsVisible;
            }

        #endregion

        #region Load User Role

            protected void LoadEditFormIntoUserRole(UserRole userRole)
            {
                //RoleID
                userRole.RoleID = Convert.ToInt32(hdnEditID.Value);
                //Role Name
                userRole.RoleName = txtEditRoleName.Text;
                //USer Role Description
                userRole.RoleDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //Visible
                userRole.IsVisible = chkEditVisible.Checked;

                userRole.DateModified = DateTime.UtcNow;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Update user role Button on click. Updates user role data.
            /// </summary>
            protected void updateUserRoleButton_Click(object sender, EventArgs e)
            {
                UserRole mUserRole = new UserRole();

                //load the form into the user role
                LoadEditFormIntoUserRole(mUserRole);

                //try to update the user role              
                try
                {
                    DataMgr.UserRoleDataMapper.UpdateUserRole(mUserRole);

                    LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                    //Bind user roles again
                    BindUserRoles();
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating user role.", ex);
                }

            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindUserRoles();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindUserRoles();
            }

        #endregion

        #region Tab Events

        #endregion

        #region Grid Events

            protected void gridUserRoles_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridUserRoles_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditUserRole.Visible = false;
                dtvUserRole.Visible = true;
                
                int userRoleID = Convert.ToInt32(gridUserRoles.DataKeys[gridUserRoles.SelectedIndex].Values["RoleID"]);

                //set data source
                dtvUserRole.DataSource = DataMgr.UserRoleDataMapper.GetFullUserRoleByRoleID(userRoleID);
                //bind user role to details view
                dtvUserRole.DataBind();
            }

            protected void gridUserRoles_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridUserRoles_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvUserRole.Visible = false;
                pnlEditUserRole.Visible = true;

                int userRoleID = Convert.ToInt32(gridUserRoles.DataKeys[e.NewEditIndex].Values["RoleID"]);

                UserRole mUserRole = DataMgr.UserRoleDataMapper.GetRole(userRoleID);

                //Set User Role data
                SetUserRoleIntoEditForm(mUserRole);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridUserRoles_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedUserRoles(sessionState["Search"].Split(' ')));

                //maintain current sort direction and expresstion on paging
                gridUserRoles.DataSource = SortDataTable(dataTable, true);
                gridUserRoles.PageIndex = e.NewPageIndex;
                gridUserRoles.DataBind();
            }

            protected void gridUserRoles_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridUserRoles.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedUserRoles(sessionState["Search"].Split(' ')));

                GridViewSortExpression = e.SortExpression;

                gridUserRoles.DataSource = SortDataTable(dataTable, false);
                gridUserRoles.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetUserRoleData> QueryUserRoles()
            {
                try
                {
                    //get all user roles
                    return DataMgr.UserRoleDataMapper.GetAllFullUserRoles();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving user roles.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving user roles.", ex);
                    return null;
                }
            }

            private IEnumerable<GetUserRoleData> QuerySearchedUserRoles(string[] searchText)
            {
                try
                {
                    //get all user roles
                    return DataMgr.UserRoleDataMapper.GetAllFullSearchedUserRoles(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving user roles.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving user roles.", ex);
                    return null;
                }
            }

            private void BindUserRoles()
            {
                //query user roles
                IEnumerable<GetUserRoleData> variables = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryUserRoles() : QuerySearchedUserRoles(txtSearch.Text.Split(' '));

                int count = variables.Count();

                gridUserRoles.DataSource = variables;

                //bind grid
                gridUserRoles.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedUserRoles(string[] searchText)
            //{
            //    //query user roles
            //    IEnumerable<GetUserRoleData> variables = QuerySearchedUserRoles(searchText);

            //    int count = variables.Count();

            //    gridUserRoles.DataSource = variables;

            //    //bind grid
            //    gridUserRoles.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} user role found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} user roles found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No user roles found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

        #endregion
    }
}

