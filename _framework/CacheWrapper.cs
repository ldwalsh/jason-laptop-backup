﻿using System;
using System.Web;
using System.Web.Caching;

using CW.Data.Interfaces.State;

namespace CW.Website._framework
{
    public class CacheWrapper: ICacheStore
    {
        #region field

            private readonly Cache cache;

        #endregion

        #region constructor

            public CacheWrapper()
            {
                cache = HttpContext.Current.Cache;
            }

            public CacheWrapper(Cache cache)
            {
                this.cache = cache;
            }

        #endregion

        #region method

            private T DoGet<T>(String key)
            {
                var o = cache[key];

                if (o == null) return default(T);

                if (o is T) return (T)o;

                return default(T);
            }

        #endregion

        #region IStateHolder

            void IStateHolder.Set<T>(String key, T value)
            {
                var cacheStore = (ICacheStore)this;

                if (Equals(value, default(T)))
                {
                    cacheStore.Remove(key);

                    return;
                }

                cacheStore.Add(key, value);
            }

            T IStateHolder.Get<T>(String key)
            {
                return DoGet<T>(key);
            }

        #endregion

        #region ICacheStore

            void ICacheStore.Add<T>(String key, T value)
            {
                cache.Add(key, value, null, DateTime.MaxValue, TimeSpan.Zero, CacheItemPriority.Normal, null);
            }

            void ICacheStore.Add<T>(String key, T value, TimeSpan timeSpan)
            {
                cache.Add(key, value, null, DateTime.MaxValue, timeSpan, CacheItemPriority.Normal, null);
            }

            //void ICacheStore.Add<T>(String key, T value, CacheItemPriority priority)
            //{
            //    cache.Add(key, value, null, DateTime.MaxValue, TimeSpan.Zero, priority, null);
            //}

            void ICacheStore.Set<T>(String key, T value, TimeSpan timeSpan)
            {
                cache.Insert(key, value, null, DateTime.MaxValue, timeSpan, CacheItemPriority.Normal, null);
            }

            void ICacheStore.Upsert<T>(String key, T value)
            {
                var cacheStore = (ICacheStore)this;

                if (Equals(value, default(T)))
                {
                    cacheStore.Remove(key);

                    return;
                }

                cacheStore.Add(key, value);
            }

            void ICacheStore.Upsert<T>(String key, T value, TimeSpan timeout)
            {
                var cacheStore = (ICacheStore)this;

                if (Equals(value, default(T)))
                {
                    cacheStore.Remove(key);

                    return;
                }

                cacheStore.Add(key, value, timeout);
            }

            void ICacheStore.Remove(String key)
            {
                cache.Remove(key);
            }

        #endregion
    }
}