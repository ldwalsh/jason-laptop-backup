﻿using CW.Business;
using CW.Data;
using CW.Data.Interfaces;
using System;
using System.Collections.Generic;

namespace CW.Website._framework
{
    public interface ISiteUser: IBusinessCredentials
    {
        Int32 UID {get;}
        Int32 CID {get;}

		Int32 RoleID {get;}

		IEnumerable<Building> VisibleBuildings {get;}

        String Email {get;}

        Boolean IsKGSFullAdminOrHigher {get;}

        Boolean IsSuperAdminOrFullAdminOrFullUser {get;}

        Boolean IsSchneiderTheme {get;}

		IEnumerable<Building> GetUpdatedVisibleBuidings(int? cidOverride = null);

    }
}