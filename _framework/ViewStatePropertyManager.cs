﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;

namespace CW.Website._framework
{
    public class ViewStatePropertyManager
    {
        #region field

            private readonly
                    TemplateControl
                    control;

            private readonly
                    ViewState
                    viewState;

            private readonly
                    IList<PropertyInfo> 
                    props;

        #endregion

        public ViewStatePropertyManager(TemplateControl control, ViewState viewState)
        {
            this.control    = control;
            this.viewState  = viewState;

            //

            props = new List<PropertyInfo>();

            foreach (var p in control.GetType().BaseType.GetProperties(BindingFlags.Instance|BindingFlags.NonPublic))
            {
                if (!p.IsDefined(typeof(ViewStatePropertyAttribute), true)) continue;

                props.Add(p);
            }
        }

        public void SaveViewState()
        {
            foreach (var p in props)
            {
                viewState.Set(p.Name, p.GetValue(control, null));
            }
        }

        public void LoadViewState()
        {
            foreach (var p in props)
            {
                p.SetValue(control, viewState.Get(p.Name, p.PropertyType), null);
            }
        }
    }
}