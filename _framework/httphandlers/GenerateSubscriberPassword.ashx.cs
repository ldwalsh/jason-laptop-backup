﻿using System;
using System.Web;
using System.Web.SessionState;

namespace CW.Website._framework.httphandlers
{
    public class GenerateSubscriberPassword: IHttpHandler, IRequiresSessionState
	{
		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			//can do something useful here like log last contact time

            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            context.Response.Cache.SetNoStore();
            context.Response.Cache.SetNoServerCaching();
            string pwd = _controls.ApiSubscriberHelper.GeneratePassword();
            context.Response.ContentType = "text/plain";
            context.Response.Write(pwd);
		}

		Boolean IHttpHandler.IsReusable
		{
			get {return false;}
		}
	}
}