﻿using CW.Utility;
using System;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CW.Website._framework.httphandlers
{
    public class ScriptFileHandler: IHttpHandler
    {
        #region CONST

            private const String HANDLER_PREFIX = "HandledScriptFile_";

        #endregion
        
        #region STATIC

            public static String GetResourceIncludeSrc(Assembly assembly, String resourceNamespace, String resourceName)
            {
                return String.Concat(HANDLER_PREFIX, resourceName, "?a=", assembly.GetName().Name, "&ns=", resourceNamespace, "&n=", resourceName);
            }

        #endregion

        #region IHttpHandler Members

            void IHttpHandler.ProcessRequest(HttpContext context)
            {
                var res = context.Response;
				var qs  = context.Request.QueryString;

                res.ContentType = "text/javascript";
				
				res.Write(new ResourceHelper(AppDomain.CurrentDomain.GetAssemblies().First(_=>_.GetName().Name.StartsWith(qs["a"]))).GetEmbedded(qs["ns"], qs["n"]).ToString());
            }

            Boolean IHttpHandler.IsReusable
            {
                get {return true;}
            }

        #endregion
    }
}