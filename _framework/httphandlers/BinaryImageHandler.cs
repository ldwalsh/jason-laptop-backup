﻿using CW.Business;
using CW.Business.Blob;
using CW.Business.Blob.Images;
using CW.Common.Config;
using CW.Common.Constants;
using CW.Data.Collections;
using CW.Data.Helpers;
using CW.Data.Models;
using CW.Utility;
using CW.Utility.Web;
using CW.Website.DependencyResolution;

using Lokad.Cloud.Storage.Blobs;
using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.SessionState;

namespace CW.Website._framework.httphandlers
{
    public interface IBinaryImageProcessor
    {
        string ImageExt { get; set; }
        byte[] Image { get; set; }
        NameValueCollection QueryString { get; set; }
        BlobProps BlobProperties { get; set; } //TODO: remove Lokad BlobProps ref with in-house

        bool SetInputs();
        bool InitBlobProperties();
        void LoadDefaultImage();
        void LoadImage();
    }

    public abstract class BinaryImageProcessor : IBinaryImageProcessor
    {
		public static BlobProps ToLokadBlobProps(BlobProperties? properties)
		{
			if (properties == null) return null;

			var p = properties.Value;

			return new BlobProps(){BlobName=p.BlobName, ContainerName=p.ContainerName, ETag=p.ETag, LastModifiedUtc=p.ModifiedUtc,};
		}

        #region fields 
        
            private readonly DataManager mDataManager;

        #endregion

        #region Properties
            public string ImageExt { get; set; }
            public byte[] Image { get; set; }
            public NameValueCollection QueryString { get; set; }
            public BlobProps BlobProperties { get; set; } //TODO: remove Lokad BlobProps ref with in-house
            public DataManager DataMgr { get { return mDataManager; } }
            public StorageAccountCriteria SACriteria { get; set; }
        #endregion

        #region constructor
            public BinaryImageProcessor(NameValueCollection qs, string imageExt)
            {
                mDataManager = IoC.Resolve<DataManager>();

                QueryString = qs;
                ImageExt = imageExt;
            }
        #endregion

        #region Methods
            public BlobProps GetDefaultBlobProperties()
            {
                return BlobProperties = new BlobProps() { ETag = "/_assets/" };
            }

            public bool InitBlobProperties()
            {
                if(!SetInputs())
                    return false;

                var cm = IoC.Resolve<IConfigManager>();
                var env = EnvironmentType.FromValue<EnvironmentType>(cm.GetConfigurationSetting(DataConstants.ConfigKeyCWEnv, "", true));

                SACriteria.CWEnvironment = env;
                SACriteria.StorageAcctLevel = StorageAccountLevel.Primary;
                DataMgr.SetOrgBasedStorageAccounts(SACriteria);

                SetFields();

                if (BlobProperties.BlobName == null || BlobProperties.ContainerName == null || BlobProperties.LastModifiedUtc == null)
                    return false;

                return true;
            }
        #endregion

        #region Abstract
            public abstract bool SetInputs();
            public abstract void SetFields();
            public abstract void LoadImage();
            public abstract void LoadDefaultImage();
        #endregion
    }

    public class FigureBinaryImageProcessor : BinaryImageProcessor
    {
        #region fields
        public DataConstants.AnalysisRange mAnalysisRange;
        public int mCID;
		public int mEID;
        public int mAID;
        public DateTime mStartDate;
        #endregion

        #region constructor
        public FigureBinaryImageProcessor(NameValueCollection qs, string imageExt) : base(qs, imageExt){}
        #endregion

        #region abstract methods
        public override bool SetInputs()
        {
            if (String.IsNullOrWhiteSpace(ImageExt) || String.IsNullOrWhiteSpace(QueryString["AnalysisRange"]) || String.IsNullOrWhiteSpace(QueryString["CID"]) || String.IsNullOrWhiteSpace(QueryString["EID"]) || 
                String.IsNullOrWhiteSpace(QueryString["AID"]) || String.IsNullOrWhiteSpace(QueryString["StartDate"]))
            {
                return false;
            }

            mCID = Convert.ToInt32(QueryString["CID"]);
			mAID = Convert.ToInt32(QueryString["AID"]);
            mEID = Convert.ToInt32(QueryString["EID"]);

            SACriteria = new StorageAccountCriteria() { CID = mCID, EID = mEID };

            if (!AnalysisHelper.TryParseAnalysisRange(QueryString["AnalysisRange"], out mAnalysisRange))
            {
                return false;
            }

            if (!DateTime.TryParse(QueryString["StartDate"], out mStartDate))
            {
                return false;
            }

            return true;
        }

        public override void SetFields()
        {
            BlobProperties = ToLokadBlobProps(new FigureImage(mCID, DataMgr.OrgBasedBlobStorageProvider, ImageExt, new FigureImage.Args(mEID, mAID, mAnalysisRange, mStartDate)).GetProperties()) ?? GetDefaultBlobProperties();
        }

        public override void LoadImage()
        {
            var image = new FigureImage(mCID, DataMgr.OrgBasedBlobStorageProvider, ImageExt, new FigureImage.Args(mEID, mAID, mAnalysisRange, mStartDate));

			if (!image.Exists()) return;
			
			Image = image.Retrieve();
        }

        public override void LoadDefaultImage()
        {
        }
        #endregion
    }

    public class EquipmentBinaryImageProcessor : BinaryImageProcessor
    {
        #region fields
		public int mCID;
        public int mBID;
        public int mEID;
        #endregion

        #region constructor
        public EquipmentBinaryImageProcessor(NameValueCollection qs, string imageExt) : base(qs, imageExt) { }
        #endregion

        #region abstract methods
        public override bool SetInputs()
        {
            if (String.IsNullOrWhiteSpace(ImageExt) || String.IsNullOrWhiteSpace(QueryString["CID"]) || String.IsNullOrWhiteSpace(QueryString["EID"]))
            {
                return false;
            }

            if (!int.TryParse(QueryString["CID"], out mCID))
            {
                return false;
            }

            if (!int.TryParse(QueryString["EID"], out mEID))
            {
                return false;
            }

            SACriteria = new StorageAccountCriteria() { CID = mCID, EID = mEID };

            return true;
        }

        public override void SetFields()
        {
            BlobProperties = ToLokadBlobProps(new EquipmentProfileImage(mCID, DataMgr.OrgBasedBlobStorageProvider, ImageExt, new EquipmentProfileImage.Args(mEID)).GetProperties()) ?? GetDefaultBlobProperties();
        }

        public override void LoadImage()
        {
            var image = new EquipmentProfileImage(mCID, DataMgr.OrgBasedBlobStorageProvider, ImageExt, new EquipmentProfileImage.Args(mEID));

            if (image.Exists())
            {
                Image = image.Retrieve();
				
				return;				
            }

			LoadDefaultImage();
        }

        public override void LoadDefaultImage()
        {
            Image = HandlerHelper.ConvertFullImagePathToByteArray(HandlerHelper.GetDefaultEquipmentImage());
            ImageExt = HandlerHelper.GetDefaultEquipmentImage().Substring(HandlerHelper.GetDefaultEquipmentImage().Length - 4);
        }
        #endregion
    }

    public class OrganizationBinaryImageProcessor : BinaryImageProcessor
    {
        #region fields
        public int mOID;
        public HandlerHelper.OrganizationAssetType mOrganizationAssetType;
        #endregion

        #region constructor
        public OrganizationBinaryImageProcessor(NameValueCollection qs, string imageExt) : base(qs, imageExt) { }
        #endregion

        #region abstract fields
        public override bool SetInputs()
        {
            if (String.IsNullOrWhiteSpace(ImageExt) || String.IsNullOrWhiteSpace(QueryString["OID"]) || String.IsNullOrWhiteSpace(QueryString["Type"]))
            {
                return false;
            }

            if (!int.TryParse(QueryString["OID"], out mOID))
            {
                return false;
            }

            if (!Enum.TryParse(QueryString["Type"], true, out mOrganizationAssetType))
            {
                return false;
            }

            SACriteria = new StorageAccountCriteria() { OID = mOID };

            return true;
        }

        public override void SetFields()
        {
            BlobFile image;

            switch (mOrganizationAssetType)
            {
                case HandlerHelper.OrganizationAssetType.Icon:
                    {
                        image = new OrganizationIconImage(mOID, DataMgr.OrgBasedBlobStorageProvider, ImageExt);

                        break;
                    }
                case HandlerHelper.OrganizationAssetType.Logo:
                    {
                        image = new OrganizationLogoImage(mOID, DataMgr.OrgBasedBlobStorageProvider, ImageExt);

                        break;
                    }
                default: throw new InvalidOperationException();
            }

            var props = image.GetProperties();

            BlobProperties = ((props == null) ? GetDefaultBlobProperties() : ToLokadBlobProps(props));
        }

        public override void LoadImage()
        {
			BlobFile image;

            switch (mOrganizationAssetType)
			{
				case HandlerHelper.OrganizationAssetType.Icon:
				{
                    image = new OrganizationIconImage(mOID, DataMgr.OrgBasedBlobStorageProvider, ImageExt);

					break;
				}
				case HandlerHelper.OrganizationAssetType.Logo:
				{
                    image = new OrganizationLogoImage(mOID, DataMgr.OrgBasedBlobStorageProvider, ImageExt);

					break;
				}
				default: throw new InvalidOperationException();
			}

			if (image.Exists())
			{
				Image = image.Retrieve();

				return;
			}
			
			LoadDefaultImage();
        }

        public override void LoadDefaultImage()
        {
            SiteUser siteUser = SiteUser.Current;
            Image = HandlerHelper.ConvertFullImagePathToByteArray(HandlerHelper.GetDefaultOrganizationImage(mOrganizationAssetType, siteUser.IsSchneiderTheme));
            ImageExt = HandlerHelper.GetDefaultOrganizationImage(mOrganizationAssetType, siteUser.IsSchneiderTheme).Substring(HandlerHelper.GetDefaultOrganizationImage(mOrganizationAssetType, siteUser.IsSchneiderTheme).Length - 4);
        }
        #endregion
    }

    public class ClientBinaryImageProcessor : BinaryImageProcessor
    {
        #region fields
        public int mCID;
        #endregion

        #region constructor
        public ClientBinaryImageProcessor(NameValueCollection qs, string imageExt) : base(qs, imageExt) { }
        #endregion
        
        #region abstract fields
        public override bool SetInputs()
        {
            if (String.IsNullOrWhiteSpace(ImageExt) || String.IsNullOrWhiteSpace(QueryString["CID"]))
            {
                return false;
            }

            if (!int.TryParse(QueryString["CID"], out mCID))
            {
                return false;
            }

            SACriteria = new StorageAccountCriteria() { CID = mCID };

            return true;
        }

        public override void SetFields()
        {
            BlobProperties = ToLokadBlobProps(new ClientProfileImage(mCID, DataMgr.OrgBasedBlobStorageProvider, ImageExt).GetProperties()) ?? GetDefaultBlobProperties();
        }

        public override void LoadImage()
        {
            var image = new ClientProfileImage(mCID, DataMgr.OrgBasedBlobStorageProvider, ImageExt);

            if (image.Exists())
            {
				Image = image.Retrieve();

				return;
            }
			
			LoadDefaultImage();
        }

        public override void LoadDefaultImage()
        {
            Image = HandlerHelper.ConvertFullImagePathToByteArray(HandlerHelper.GetDefaultClientImage());
            ImageExt = HandlerHelper.GetDefaultClientImage().Substring(HandlerHelper.GetDefaultClientImage().Length - 4);
        }
        #endregion
    }

    public class BuildingBinaryImageProcessor : BinaryImageProcessor
    {
        #region fields
        public int mCID;
		public int mBID;
        #endregion 

        #region constructor
        public BuildingBinaryImageProcessor(NameValueCollection qs, string imageExt) : base(qs, imageExt) { }
        #endregion
        
        #region abstract fields
        public override bool SetInputs()
        {
            if (String.IsNullOrWhiteSpace(ImageExt) || String.IsNullOrWhiteSpace(QueryString["CID"]) || String.IsNullOrWhiteSpace(QueryString["BID"]))
            {
                return false;
            }

            if (!int.TryParse(QueryString["CID"], out mCID))
            {
                return false;
            }

            if (!int.TryParse(QueryString["BID"], out mBID))
            {
                return false;
            }

            SACriteria = new StorageAccountCriteria() { CID = mCID, BID = mBID };

            return true;
        }

        public override void SetFields()
        {
            BlobProperties = ToLokadBlobProps(new BuildingProfileImage(mCID, DataMgr.OrgBasedBlobStorageProvider, ImageExt, new BuildingProfileImage.Args(mBID)).GetProperties()) ?? GetDefaultBlobProperties();
        }

        public override void LoadImage()
        {
            var image = new BuildingProfileImage(mCID, DataMgr.OrgBasedBlobStorageProvider, ImageExt, new BuildingProfileImage.Args(mBID));

            if (image.Exists())
            {
                Image = image.Retrieve();

				return;
            }

			LoadDefaultImage();
        }
        public override void LoadDefaultImage()
        {
            Image = HandlerHelper.ConvertFullImagePathToByteArray(HandlerHelper.GetDefaultBuildingImage());
            ImageExt = HandlerHelper.GetDefaultBuildingImage().Substring(HandlerHelper.GetDefaultBuildingImage().Length - 4);
        }

        #endregion
    }

    public class BinaryImageHandler : IHttpHandler, IRequiresSessionState
    {      
        #region IHttpHandler Members

            public bool IsReusable
            {
                get { return false; }
            }

            public void ProcessRequest(HttpContext context)
            {
                IBinaryImageProcessor processor = null;
                NameValueCollection qs = context.Request.QueryString;

                string imageExt = qs["ImageExt"];
                string type = qs["processType"];

                switch (type)
                {
                    case "b":
                        processor = new BuildingBinaryImageProcessor(qs, imageExt);
                        break;
                    case "c":
                        processor = new ClientBinaryImageProcessor(qs, imageExt);
                        break;
                    case "e":
                        processor = new EquipmentBinaryImageProcessor(qs, imageExt);
                        break;
                    case "f":
                        processor = new FigureBinaryImageProcessor(qs, imageExt);
                        break;
                    case "o":
                        processor = new OrganizationBinaryImageProcessor(qs, imageExt);
                        break;
                    default:
                        return;
                }

                if (processor.InitBlobProperties())
                {
                    DateTime parsedIncomingDate = DateTime.MinValue;

                    if (!String.IsNullOrWhiteSpace(context.Request.Headers["If-None-Match"]) && context.Request.Headers["If-None-Match"] == processor.BlobProperties.ETag)
                    {
                        Write304Response(context, processor.BlobProperties.ETag, processor.BlobProperties.LastModifiedUtc.Value.UtcDateTime);
                    }
                    else if (DateTime.TryParse(context.Request.Headers["If-Modified-Since"], out parsedIncomingDate) && parsedIncomingDate == processor.BlobProperties.LastModifiedUtc)
                    {
                        Write304Response(context, processor.BlobProperties.ETag, processor.BlobProperties.LastModifiedUtc.Value.UtcDateTime);
                    }
                    else
                    {
                        processor.LoadImage();
                        Write200Response(context, processor.ImageExt, processor.Image, processor.BlobProperties.ETag, processor.BlobProperties.LastModifiedUtc.Value.UtcDateTime);
                    }
                }
                else
                {
                    processor.LoadDefaultImage();
                    Write200Response(context, processor.ImageExt, processor.Image, processor.BlobProperties.ETag, DateTime.UtcNow);
                }
            }
        #endregion

        #region Helper Methods

            public static void Write304Response(HttpContext context, string eTag, DateTime lastModifiedUTC)
            {
                context.Response.Clear();

                context.Response.StatusCode = 304;
                context.Response.StatusDescription = "Not Modified";
                //context.Response.Cache.SetLastModified(lastModifiedUTC);
                //context.Response.AddHeader("If-Modified-Since", lastModifiedUTC.ToUniversalTime().ToString("r")); 
                //context.Response.AddHeader("If-None-Match", eTag);
                //context.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
                //context.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
                //if (!String.IsNullOrWhiteSpace(eTag))
                //{
                //    context.Response.Cache.SetETag(eTag);
                //}

                // Explicitly set the Content-Length header so the client doesn't wait for
                //  content but keeps the connection open for other requests 
                context.Response.AddHeader("Content-Length", "0");

                context.Response.End();
            }

            public static void Write200Response(HttpContext context, string imageExt, byte[] image, string eTag, DateTime lastModifiedUTC)
            {
                context.Response.Clear();

                // NOTE: need to set ServerAndPrivate to set ETag: http://stackoverflow.com/questions/32824/why-does-httpcacheability-private-suppress-etags
                // NOTE: SetOmitVaryStar no longer needed, fixed in ASP.NET 4.0
                context.Response.Cache.SetCacheability(HttpCacheability.ServerAndPrivate);
                context.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
                if (!String.IsNullOrWhiteSpace(eTag))
                {
                    context.Response.Cache.SetETag(eTag);
                }

                //if (lastModifiedUTC != DateTime.MinValue)
                //{
                //    context.Response.Cache.SetLastModified(lastModifiedUTC);
                //}

                if (image != null)
                {
                    context.Response.AddHeader("Content-Length", image.Length.ToString());  //this was throwing errors when the image was null
                    context.Response.ContentType = "image/" + imageExt.Remove(0, 1);
                    context.Response.BinaryWrite(image);
                }

                context.Response.End();
            }

        #endregion
    }
}
