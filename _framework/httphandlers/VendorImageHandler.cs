﻿using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Specialized;
using System.Net;
using System.Web;
using System.Web.SessionState;

namespace CW.Website._framework.httphandlers
{
    public class ImageAsset
    {
        public byte[] Bytes { get; set; }
        public string Ext { get; set; }
    }

    public abstract class VendorImageProcessor
    {
        private ImageAsset mImage;
        private string mImageExt;

        public ImageAsset Image
        { 
            get {
                mImage = mImage ?? GetImage() ?? GetDefaultImage();
                return mImage;
            }
        }

        protected string AssetPath { get; set; }

        protected string GetExtFromPath(string path)
        {
            var imagePathParts = path.Split('.');

            return imagePathParts[imagePathParts.Length - 1];
        }

        public abstract ImageAsset GetImage();
        public abstract ImageAsset GetDefaultImage();
    }

    #region Uncomment if self hosting weather icons
    //public class WeatherImageProcessor : VendorImageProcessor
    //{
    //    private string mImageType;

    //    public WeatherImageProcessor(string imageType)
    //    {
    //        mImageType = imageType;
    //    }

    //    public override ImageAsset GetImage()
    //    {
    //        return new ImageAsset()
    //        {
    //            Bytes = null,
    //            Ext = ""
    //        };
    //    }

    //    public override ImageAsset GetDefaultImage()
    //    {
    //        var imagePath = HandlerHelper.GetDefaultEquipmentImage();

    //        return new ImageAsset()
    //        {
    //            Bytes = HandlerHelper.ConvertFullImagePathToByteArray(imagePath),
    //            Ext = GetExtFromPath(imagePath)
    //        };
    //    }
    //}
    #endregion

    public class WeatherUndergroundImageProcessor : VendorImageProcessor
    {
        private string mImageUrl;

        public WeatherUndergroundImageProcessor(string imageUrl)
        {
            mImageUrl = imageUrl;
        }

        public override ImageAsset GetImage()
        {
            var bytes = new WebClient().DownloadData(mImageUrl);

            if (bytes == null)
                return null;

            return new ImageAsset()
            {
                Bytes = bytes,
                Ext = GetExtFromPath(mImageUrl)
            };
        }

        public override ImageAsset GetDefaultImage()
        {
            // If default image is desired, wire up here
            var imagePath = HandlerHelper.GetDefaultWeatherImage();

            return new ImageAsset()
            {
                Bytes = HandlerHelper.ConvertFullImagePathToByteArray(imagePath),
                Ext = GetExtFromPath(imagePath)
            };
        }
    }
    

    public class VendorImageHandler : IHttpHandler, IRequiresSessionState
    {
        private VendorImageProcessor Processor { get; set; }

        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            NameValueCollection qs = context.Request.QueryString;

            string processorType = qs["t"];  // processor type
            string host          = qs["h"];  // host
            string scheme        = qs["s"];  // scheme
            string path          = qs["p"];  // path
            string query         = qs["q"];  // query

            switch (processorType)
            {
                case "w":
                    Processor = new WeatherUndergroundImageProcessor(new UriBuilder()
                    {
                        Path = path,
                        Host = host,
                        Scheme = scheme,
                        Query = query
                    }.ToString());
                    break;
                default:
                    throw new ArgumentException(String.Format("Processor Type not found: {0}", processorType));
            }

            Write200Response(context, Processor.Image.Ext, Processor.Image.Bytes, "", null);
        }
        #endregion

        #region Helper Methods

        public static void Write200Response(HttpContext context, string imageExt, byte[] image, string eTag, DateTime? lastModifiedUTC)
        {
            context.Response.Clear();

            if (image != null)
            {
                context.Response.AddHeader("Content-Length", image.Length.ToString());  //this was throwing errors when the image was null
                context.Response.ContentType = "image/" + imageExt.Remove(0, 1);
                context.Response.BinaryWrite(image);
            }

            context.Response.End();
        }

        #endregion
    }
}
