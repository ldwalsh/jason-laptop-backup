﻿using CW.Business;
using CW.Data;
using CW.Website._administration.client;
using CW.Website.DependencyResolution;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._framework.httphandlers
{
    public class GoalViewHandler : IHttpHandler
    {

        #region interface properties
        public bool IsReusable => false;      
        #endregion

        #region
        protected GoalService goalService;
        #endregion

        #region constructor
        public GoalViewHandler()
        {
            goalService = new GoalService();
        }
        #endregion

        #region interface method(s)
        public void ProcessRequest(HttpContext context)
        {             


            var fields = new string[]
                {
                    context.Request.QueryString["costTypeID"],
                    context.Request.QueryString["costValue"]?.Trim(),
                    context.Request.QueryString["energyTypeID"],
                    context.Request.QueryString["energyValue"]?.Trim(),
                    context.Request.QueryString["carbonTypeID"],
                    context.Request.QueryString["carbonValue"]?.Trim(),
                    context.Request.QueryString["carbonMetricID"]
                };

            string result = goalService.ParseAndSaveGoalsFromRequestFields(fields, 0);

            context.Response.Write(JsonConvert.SerializeObject(result));
        }
        #endregion
    }
}