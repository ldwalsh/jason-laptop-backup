﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace CW.Website._framework.httphandlers
{
	public class SessionKeepAliveHandler: IHttpHandler, IRequiresSessionState
	{
		void IHttpHandler.ProcessRequest(HttpContext context)
		{
			//can do something useful here like log last contact time

            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            context.Response.Cache.SetNoStore();
            context.Response.Cache.SetNoServerCaching();

            // Since we are using custom Session Provider for Azure, we now have to to "touch" Session so that it becomes "dirty" and 
            // nudges the Session manager to actually "feel" like the user "really" interacted with the server. If this is not done, 
            // the session manager thinks that the user did not do so. We likely would not have had to do this if we were using the 
            // native .net session provider.
            context.Session["SessionKeepAlive"] = DateTime.UtcNow.Ticks;  
		}

		Boolean IHttpHandler.IsReusable
		{
			get {return false;}
		}
	}
}