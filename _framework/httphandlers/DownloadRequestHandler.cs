﻿using CW.Business;
using CW.Data.Interfaces.State;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website.DependencyResolution;
using System;
using System.IO;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using CW.Utility.Web;
using CW.Caching;

namespace CW.Website._framework.httphandlers
{
	public class DownloadRequestHandler : IHttpHandler, IRequiresSessionState
    {
		#region CLASS

			public class HandledFileAttacher: IFileAttacher
			{
                public void AttachForDownload(String fileName, Byte[] fileContent)
				{
					var file = new File(fileName, fileContent);

					var key = String.Format("{0}:{1}:{2}", HttpContext.Current.Session.SessionID, file.Name, file.Content.Length);

					//if (downloadQueue.ContainsKey(key))
					//{
					//	downloadQueue[key] = file;
					//}
					//else
					//{
					//	downloadQueue.Add(key, file);
					//}

					Cache.Upsert(MakeCacheKey(key), file, TimeSpan.FromMinutes(5));

					var page = (Page)HttpContext.Current.Handler;

					ScriptManager.RegisterStartupScript
					(
						page,
						page.GetType(),
						key,
						String.Format("document.location.href=\"{0}\";", new Uri(String.Concat(LinkHelper.GetBaseUrl(page.Request), "file.download", "?k=", HttpUtility.UrlEncode(key)))),
						true
					);
				}
			}

		#endregion

		#region STRUCT

			[Serializable]
			public class File
			{
				#region constructor

					public File(String fileName, Byte[] content): this(fileName, content, String.Empty) { }

					private File(String fileName, Byte[] content, String contentType)
					{
                        Name = fileName;
                        ContentType = GetContentType(new FileInfo(fileName).Extension);
                        Content = content;
					}

				#endregion

				#region property

					public String Name
					{
						private set;
						get;
					}
                    
					public String ContentType
					{
						private set;
						get;
					}
                
					public Byte[] Content
					{
						private set;
						get;
					}

				#endregion

                #region methods

                    private String GetContentType(String fileExtension)
                    {
                        switch (fileExtension.ToLower())
                        {
                            case ".xls":
                                return "application/vnd.ms-excel";
                            case ".xlsx":
                                return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            case ".doc":
                                return "application/msword";
                            case ".docx":
                                return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                            case ".pdf":
                                return "application/pdf";
                            case ".txt":
                                return "text/plain";
                            case ".csv":
                                return "text/csv";
                        }

                        return String.Empty;
                    }

                #endregion
            }

		#endregion

		#region STATIC

			#region field

				private static String CacheKey = "DownloadRequestHandlerKey";

			#endregion

			#region constructor

				static DownloadRequestHandler()
				{
					Cache = IoC.Resolve<DataManagerCommonStorage>().DefaultCacheRepository;

					//var cache = DataManager.Instance.DefaultCacheRepository;

					//retry:

					//downloadQueue = cache.Get<Dictionary<String,File>>(QueueKey);

					//if (downloadQueue == null)
					//{
					//	cache.Add(QueueKey, new Dictionary<String,File>());

					//	goto retry;
					//}
				}

			#endregion

			#region method

				private static String MakeCacheKey(String fileKey)
				{
					return String.Concat(CacheKey, "_", fileKey);
				}

			#endregion

			#region property

				//public static Dictionary<String,File> downloadQueue
				//{
				//	private set;
				//	get;
				//}

				private static ICacheStore Cache
				{
					set;
					get;
				}

        #endregion

        #endregion

        #region IHttpHandler

            void IHttpHandler.ProcessRequest(HttpContext context) 
			{
				var req = context.Request;
				var res = context.Response;

				var key = MakeCacheKey(HttpUtility.UrlDecode(req.QueryString["k"]));

				//var file = downloadQueue[key];
				var file = Cache.Get<File>(key);

				//downloadQueue.Remove(key);
				Cache.Remove<File>(key);
                
				res.ContentType = file.ContentType;
				res.AddHeader("Content-Length", file.Content.Length.ToString());
				res.AddHeader("Content-Disposition", String.Format("attachment; filename=\"{0}\"", file.Name));
				res.BinaryWrite(file.Content);
				res.End();
			}

			Boolean IHttpHandler.IsReusable
			{
				get {return false;}
			}

		#endregion
	}
}