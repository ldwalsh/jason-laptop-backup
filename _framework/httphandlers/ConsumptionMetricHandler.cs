﻿using CW.Website._services.MetricHandler.Factories;
using CW.Website._services.MetricHandler.Interfaces;
using CW.Website._services.MetricHandler.Models;
using Newtonsoft.Json;
using System.Web;

namespace CW.Website._framework.httphandlers
{
    /// <summary>
    /// 
    /// </summary>
    public class ConsumptionMetricHandler : IHttpHandler
    {
        #region Interface Properties

        public bool IsReusable => false;

        #endregion

        #region Interface Method(s)

        /// <summary>
        /// Processes HTTP requests from the web application that
        /// ask for metric calculation results (e.g. from the
        /// SEU widget "drilldown" calls).
        /// </summary>
        /// <param name="context"></param>
        public void ProcessRequest(HttpContext context)
        {
            if (context == null) return;

            IMetricHandlerService svc = new MetricHandlerServiceFactory().GetMetricHandlerService(context);            

            MetricResponseModel metricResponse = svc.GetMetricResult();

            var jsonResponse = JsonConvert.SerializeObject(metricResponse);

            context.Response.Write(jsonResponse);    
        }

        #endregion

    }
}