﻿using CW.CMMSIntegration;
using CW.CMMSIntegration.WorkOrderSystems.Factories;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._framework.httphandlers
{
    public class TestConnectionHandler : IHttpHandler
    {
        #region field(s)

            IList<string> missingFields = new List<string>();

        #endregion

        #region interface properties

            public bool IsReusable => false;

        #endregion

        #region interface method(s)

            public void ProcessRequest(HttpContext context)
            {
                var fields = new string[]
                {
                    context.Request.QueryString["id"],
                    context.Request.QueryString["uri"],
                    context.Request.QueryString["username"].Trim(),
                    context.Request.QueryString["password"].Trim()
                };

                var settings = SetSettings(fields);

                var result = GetConnectionResults(settings);

                var jsonResult = JsonConvert.SerializeObject(result);

                context.Response.Write(jsonResult);
            }

        #endregion

        #region method(s)

            CMMSConfig SetSettings(string[] fields)
            {
                var id = fields[0];

                var uri = fields[1];

                var username = fields[2];

                var password = fields[3];

                if (id == "-1") missingFields.Add("Work Order System");

                if (string.IsNullOrWhiteSpace(uri)) missingFields.Add("Base Url");

                if (string.IsNullOrWhiteSpace(username)) missingFields.Add("Username");

                if (string.IsNullOrWhiteSpace(password)) missingFields.Add("Password");

                if (missingFields.Any()) return null;

                var settings = new CMMSConfig()
                {
                    VendorId = int.Parse(id),
                    Endpoint = new Uri(uri),
                    Identifier = username,
                    Passkey = password
                };

                return settings;
            }

            string GetConnectionResults(CMMSConfig settings)
            {
                if (settings == null)
                {
                    var message = "You must enter/select the following fields to test a connection:\n\n";

                    var fields = string.Join("\n", missingFields);

                    return $"{message}{fields}";
                }
            
                var _connectionSvc = new WorkOrderConnectionServiceFactory().GetWorkOrderConnectionService(settings);

                if (_connectionSvc == null) return "Your Work Order System is not configured for testing a connection";

                var result = _connectionSvc.SetConnection();

                if (result.Contains("Error")) return $"There was an error with your configuration:\n\n{result}\n\nPlease correct and try again.";

                return "Connection was successful!";
            }

        #endregion
    }
}