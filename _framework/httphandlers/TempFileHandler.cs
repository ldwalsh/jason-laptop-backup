﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.SessionState;
using CW.Utility;
using CW.Utility.Web;

namespace CW.Website._framework.httphandlers
{
    public sealed class TempFileHandler: IHttpHandler, IRequiresSessionState
    {
        #region STRUCT

			[Serializable]
            public sealed class TempFile //use a different construct so props can't be modified?
            {
                public  Byte[]
                        Content;

                public  String
                        Name;
            }

        #endregion

        #region CONST

            private const String SESSION_KEY = "TempFiles";

            public const String QueryStringKey = "k";
            public const String QueryStringPrefix = "p";

        #endregion
        
        #region STATIC

            #region method

                public static Guid CreateTempFile(SessionStateWrapper session, String fileName, Byte[] fileContent)
                {
                    if (!session.Keys.Contains(SESSION_KEY))
                    {
                        session.Set(SESSION_KEY, new Dictionary<Guid,TempFile>());
                    }

                    var dic = session.Get<IDictionary<Guid,TempFile>>(SESSION_KEY);

                    var key = Guid.NewGuid();

                    dic.Add(key, new TempFile{Content=fileContent, Name=fileName});

                    return key;
                }

                public static void DeleteTempFile(SessionStateWrapper session, Guid key)
                {
                    var dic = session.Get<IDictionary<Guid,TempFile>>(SESSION_KEY);

                    if (dic == null) goto err;

                    TempFile tempFile;

                    if (!dic.TryGetValue(key, out tempFile)) goto err;

                    dic.Remove(key);

                    return;
                    err:

                    throw new InvalidOperationException("The temp file does not exist");
                }

                public static TempFile GetFile(SessionStateWrapper session, Guid key)
                {
                    var dic = session.Get<IDictionary<Guid,TempFile>>(SESSION_KEY);

                    if (dic == null) return null;

                    return dic.ContainsKey(key) ? dic[key] : null;
                }

                public static String GetURL(SessionStateWrapper session, Guid key)
                {
                    return HttpHelper.ResolveUrl(String.Format("TempFileHandler.img?{0}={1}&{2}={3}", new Object[]{QueryStringKey, key, QueryStringPrefix, session.Prefix}));
                }
        
            #endregion

        #endregion

        #region IHttpHandler Members

            void IHttpHandler.ProcessRequest(HttpContext context)
            {
                var qs = new QueryString(context.Request.QueryString);

                var r = context.Response;

                r.BinaryWrite(new SessionStateWrapper(context.Session, qs[QueryStringPrefix]).Get<IDictionary<Guid,TempFile>>(SESSION_KEY)[qs.Get<Guid>(QueryStringKey).Value].Content);

                r.End();
            }

            Boolean IHttpHandler.IsReusable
            {
                get {return true;}
            }

        #endregion
    }
}
