﻿using CW.Business;
using CW.Website.DependencyResolution;

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.SessionState;

namespace CW.Website._framework.httphandlers
{
	public class CustomChartImageHandler: IHttpHandler, IRequiresSessionState
	{
	    #region IHttpHandler

            Boolean IHttpHandler.IsReusable
            {
                get {return false;}
            }

            void IHttpHandler.ProcessRequest(HttpContext context)
			{
				var response	= context.Response;
				var queryString = context.Request.QueryString;
				var chartID		= queryString.Get("ChartID");

				begin:
				
				if (chartID == null)
				{
					response.StatusCode = 500;

					return;
				}

				var image = (Image)context.Session[chartID];

				using (var stream = new MemoryStream())
				{
					try
					{
						image.Save(stream, ImageFormat.Png);
					}
					catch (NullReferenceException ex)
					{
						chartID = null;

						goto begin;
					}

					context.Session.Remove(chartID);

					IoC.Resolve<DataManagerCommonStorage>().HourlyCacheRepository.Set(chartID, image); 

					response.ContentType = "image/png";
					
					response.BinaryWrite(stream.ToArray());
				}
			}

		#endregion
	}
}
