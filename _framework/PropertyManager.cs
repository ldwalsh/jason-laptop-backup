﻿using System.Collections.Generic;
using System.Reflection;
using System.Web.UI;

namespace CW.Website._framework
{
    public class PropertyManager
    {
        #region field

            private readonly
                    TemplateControl
                    control;

            private readonly
                    ViewState
                    viewState;

            private readonly
                    SessionState
                    sessionState;

            private readonly
                    IList<PropertyInfo> 
                    vsProps;

            private readonly
                    IList<PropertyInfo> 
                    ssProps;

        #endregion

        #region constructor

            public PropertyManager(TemplateControl control, ViewState viewState, SessionState sessionState)
            {
                this.control      = control;
                this.viewState    = viewState;
                this.sessionState = sessionState;

                //

                vsProps = new List<PropertyInfo>();
                ssProps = new List<PropertyInfo>();

                foreach (var p in control.GetType().BaseType.GetProperties(BindingFlags.Instance|BindingFlags.NonPublic))
                {
                    if (p.IsDefined(typeof(ViewStatePropertyAttribute), true))
                    {
                        vsProps.Add(p);
                    }

                    if (p.IsDefined(typeof(SessionStatePropertyAttribute), true))
                    {
                        ssProps.Add(p);
                    }
                }
            }
        
        #endregion

        public void SaveViewState()
        {
            foreach (var p in vsProps)
            {
                viewState.Set(p.Name, p.GetValue(control, null));
            }
        }

        public void LoadViewState()
        {
            foreach (var p in vsProps)
            {
                p.SetValue(control, viewState.Get(p.Name, p.PropertyType), null);
            }
        }

        public void SaveSessionState()
        {
            //foreach (var p in ssProps)
            //{
            //    sessionState.Set(p.Name, p.GetValue(control, null));
            //}
        }

        public void LoadSessionState()
        {
            //foreach (var p in ssProps)
            //{
            //    p.SetValue(control, sessionState.Get(p.Name, p.PropertyType), null);
            //}
        }
    }
}