﻿using System;

namespace CW.Website._framework
{
    [
    AttributeUsage(AttributeTargets.Property)
    ]
    public class ViewStatePropertyAttribute: Attribute
    {
        public Object DefaultValue;
    }
}