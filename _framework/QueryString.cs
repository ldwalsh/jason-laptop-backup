﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CW.Website._framework
{
    public sealed class QueryString //this class should be broken up into 2+
    {
        #region INTERFACE

            public interface IParamsObject
            {
                Boolean RequirePropertyAttribute {get;}
            }

        #endregion

        #region CLASS

            [
            AttributeUsage(AttributeTargets.Field, AllowMultiple=false, Inherited=true)
            ]
            public class PropertyAttribute: Attribute
            {
                public  Boolean
                        Optional;

                public  Object
                        DefaultValue;

                public  String
                        Requires;
            }

            //public class GetArg
            //{
            //    public  readonly
            //            Type
            //            type;

            //    public  Boolean
            //            optional;

            //    public  Object
            //            defaultValue;

            //    public  String
            //            requires;

            //    [
            //    DebuggerStepThrough
            //    ]
            //    public GetArg(Type type)
            //    {
            //        this.type = type;
            //    }
            //}

            //public sealed class StringGetArg: GetArg
            //{
            //    [
            //    DebuggerStepThrough
            //    ]
            //    public StringGetArg():base(typeof(String))
            //    {
            //    }
            //}

            //public sealed class Int32GetArg: GetArg
            //{
            //    [
            //    DebuggerStepThrough
            //    ]
            //    public Int32GetArg():base(typeof(Int32))
            //    {
            //    }
            //}

            //public sealed class DateTimeGetArg: GetArg
            //{
            //    [
            //    DebuggerStepThrough
            //    ]
            //    public DateTimeGetArg():base(typeof(DateTime))
            //    {
            //    }
            //}

        #endregion

        #region STATIC

            #region method

                private static Dictionary<FieldInfo, PropertyAttribute> FindQueryStringFields(Type type)
                {
                    var found = new Dictionary<FieldInfo, PropertyAttribute>();

                    foreach (var field in type.GetFields())
                    {
                        if (!field.IsDefined(typeof(PropertyAttribute), true))
                        {
                            found.Add(field, null);
                        }
                        else
                        {
                            found.Add(field, (PropertyAttribute)field.GetCustomAttributes(typeof(PropertyAttribute), true)[0]);    
                        }
                    }

                    return found;
                }

                public static void AppendToQueryString(ref String queryString, String key, Object value)
                {
                    if (value == null) return;

                    if (queryString == String.Empty)
                    {
                        queryString += '?';
                    }

                    if (!new HashSet<Char>{'?', '&'}.Contains(queryString[queryString.Length-1]))
                    {
                        queryString += '&';
                    }

                    var type = value.GetType();
                    
                    type = Nullable.GetUnderlyingType(type) ?? type;

                    if (type.IsEnum)
                    {
                        value = (Int32)value;
                    }

                    queryString += String.Concat(key, "=", value.ToString());
                }

                public static String Create(IParamsObject paramsObject)
                {
                    return Create(null, paramsObject);
                }
                //
                public static String Create(String path, IParamsObject paramsObject)
                {
                    var fields = FindQueryStringFields(paramsObject.GetType());

                    if (!fields.Any()) return String.Empty;

                    var qs = String.Empty;
                    
                    foreach (var field in fields)
                    {
                        var val = field.Key.GetValue(paramsObject);

                        if (val == null) continue;

                        AppendToQueryString(ref qs, field.Key.Name, val);
                    }

                    return String.Concat((String.IsNullOrWhiteSpace(path) ? String.Empty : path.Trim('?')), qs.TrimEnd('&'));
                }

            #endregion

        #endregion

        #region field

            private readonly NameValueCollection collection;

        #endregion

        #region constructor

            public QueryString(NameValueCollection collection)
            {
                this.collection = collection;
            }

            public QueryString(String queryString)
            {
                if (queryString.Contains("?"))
                {
                    queryString = queryString.Split('?')[1];
                }

                collection = HttpUtility.ParseQueryString(queryString);
            }

        #endregion

        #region method

            //public Boolean Validate(KeyValuePair<String, Type>[] args)
            //{
            //    return args.All(arg => (Get(arg.Key, arg.Value) != null));
            //}
            ////
            //public Boolean Validate(Dictionary<String, Type> args)
            //{
            //    return args.All(arg => (Get(arg.Key, arg.Value) != null));
            //}
            ////
            //public Boolean Validate(Dictionary<String, GetArg> args)
            //{
            //    foreach (var arg in args)
            //    {
            //        if (Get(arg.Key, arg.Value.type) == null)
            //        {
            //            if (arg.Value.optional) continue;

            //            return false;
            //        }

            //        if (String.IsNullOrWhiteSpace(arg.Value.requires)) continue;

            //        if (Get(arg.Value.requires, args[arg.Value.requires].type) == null) return false;
            //    }

            //    return true;
            //}

            //public String Get(String key)
            //{
            //    return Get(key, typeof(String)) as String;
            //}
            //
            public Object Get(String key, Type type)
            {
                if (String.IsNullOrWhiteSpace(key)) return null;

                if (collection[key] == null) return null;

                type = Nullable.GetUnderlyingType(type) ?? type;

                if (type == typeof(String)) return collection[key];

                if (type.IsEnum)
                {
                    Int32 val;

                    if (Int32.TryParse(collection[key], out val)) return Enum.ToObject(type, val);

                    try
                    {
                        return Enum.Parse(type, collection[key], true);
                    }
                    catch
                    {
                        return null;
                    }
                }

                try
                {
                    return Convert.ChangeType(collection[key], type);
                }
                catch
                {
                    return null;
                }
            }

            public T? Get<T>(String key) where T: struct
            {
                var type = typeof(T);

                if ((type.BaseType == null) || !new HashSet<Type>{typeof(ValueType), typeof(Enum)}.Contains(type.BaseType)) throw new ArgumentException("Only ValueType objects are supported.");

                var val = Get(key, type);

                if (val == null) return null;

                return (T?)val;
            }

            public T ToObject<T>() where T: class, IParamsObject
            {
                if (IsEmpty) return null;

                var type = typeof(T);

                var fields = FindQueryStringFields(type);

                if (!fields.Any()) return null;

                var t = Activator.CreateInstance(type);

                foreach (var field in fields)
                {
                    var val = Get(field.Key.Name, field.Key.FieldType);

                    if (field.Value == null)
                    {
                        if (val == null) return null;
                    }
                    else
                    {
                        if (val == null)
                        {
                            if (field.Value.Optional)
                            {
                                if (field.Value.DefaultValue == null) continue;

                                val = field.Value.DefaultValue;

                                if (!val.GetType().IsAssignableFrom(field.Key.FieldType)) throw new InvalidOperationException(String.Format("The provided default value for field '{0}' of object '{1}' is not type compatible.", field.Key.Name, type.Name));
                            }
                            else return null;
                        }
                        else if (!String.IsNullOrWhiteSpace(field.Value.Requires))
                        {
                            var requires = field.Value.Requires;

                            var f = fields.Keys.FirstOrDefault(_=>(_.Name == requires));

                            if (f == null) throw new InvalidOperationException(String.Format("The required field '{0}' defined for '{1}' is not a valid field of the IParamsObject object.", requires, type.Name));
                            
                            if (this[requires] == null) return null;
                        }
                    }

                    field.Key.SetValue(t, val);
                }

                return (T)t;
            }

        #endregion

        #region indexer

            public String this[String key]
            {
               get {return Get(key, typeof(String)) as String;}
            }

        #endregion

        #region property

            public Boolean IsEmpty
            {
                get {return !collection.HasKeys();}
            }

        #endregion
    }
}