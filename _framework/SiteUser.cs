﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using System.Web.SessionState;
using System.Linq.Expressions;
using System.Threading.Tasks;

using CW.Business;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Interfaces;
using CW.Data.Models.Client;
using CW.Utility;
using CW.Website.DependencyResolution;
using CW.Data.Models;
using CW.Common.Config;
using CW.Data.Collections;
using System.Collections;
using CW.Utility.Web;

namespace CW.Website._framework
{
    [DataContract]
    public class SiteUser: ISiteUser, IBusinessCredentials
    {
		#region const

			private const string USER_SESSION_KEY = "_SiteUser";

		#endregion

        #region static

            #region methods

                public static void LogOn(User user, bool identityManagedExternalProvider, Int32 roleID, Int32 cid, Int32? providerID, String clientName, IEnumerable<GetClientsWithRoleFormat> clients, IEnumerable<GetClientsWithRoleFormat> providerClients, Single timeZoneOffset, DataManager dm)
                {
                    LogOn(HttpContext.Current.Session, user, identityManagedExternalProvider, roleID, cid, providerID, clientName, clients, providerClients, timeZoneOffset, dm);
                }
                //                    
                private static void LogOn(HttpSessionState session, User user, bool identityManagedExternalProvider, Int32 roleID, Int32 cid, Int32? providerID, String clientName, IEnumerable<GetClientsWithRoleFormat> clients, IEnumerable<GetClientsWithRoleFormat> providerClients, Single timeZoneOffset, DataManager dm)
                {
                    var su =
                    new SiteUser(clients, providerClients, dm)
                    {
                        CID = cid,
                        ClientName = clientName,

                        ProviderID = providerID,

                        UserOID = user.OID,
                        UID = user.UID,
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        RoleID = roleID,
                        IsOrgAdmin = user.IsOrganizationAdmin,

                        UserTimeZoneOffset = (timeZoneOffset - Convert.ToSingle(TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.UtcNow).TotalHours)),

                        CultureName = CultureHelper.GetCultureName(user.LCID),
                        IdentityManagedExternalProvider = identityManagedExternalProvider
                    };

                    su.SetProviderLists();

                    su.SwitchClient(cid, user.OID, providerID);
                }

                public void LogOff(HttpContext context)
                {
                    try
                    {
                        Global.Signout(context);
                    }
                    catch { };
                }

            #endregion

            #region property
                public static SiteUser Current
                {         
                    //[
                    //DebuggerStepThrough
                    //]           
                    get
                    {
                        if (HttpContext.Current?.Session == null)
                            return new SiteUser();

                        string sessionID = HttpContext.Current.Session.SessionID;
                        SiteUser su = (SiteUser)HttpContext.Current.Session[USER_SESSION_KEY];
                        if (null == su)
                        {
                            su = new SiteUser() { IsAnonymous = true, RoleID = Int32.MaxValue, IdentityManagedExternalProvider = false };
                            HttpContext.Current.Session[USER_SESSION_KEY] = su;
                            DataManager.Delete(sessionID);
                        }

                        DataManager dm = DataManager.Get(sessionID);
                        if (null == dm)
                        {
                            dm = IoC.Resolve<DataManager>();
                            if (!su.IsAnonymous)
                            {
                                dm.SetOrgBasedStorageAccounts(new StorageCreds(su.StorageAccountName, su.StorageAccountPrimaryKey, su.StorageAccountSecondaryKey));
                            }
                            DataManager.Set(sessionID, dm);
                        }
                        else if (dm.ClientOID != su.ClientOID ) // This may occur under a loadbalanced env.
                        {
                            if (!su.IsAnonymous)
                            {
                                dm.ClientOID = su.ClientOID;
                                dm.SetOrgBasedStorageAccounts(new StorageCreds(su.StorageAccountName, su.StorageAccountPrimaryKey, su.StorageAccountSecondaryKey));
                                DataManager.Set(sessionID, dm);
                            }
                        }

                        return (su);
                    }
                }
            #endregion

        #endregion
                
        #region constructor

            private SiteUser()
            {
                DetermineInstanceTheme();
            }
            private SiteUser(IEnumerable<GetClientsWithRoleFormat> clients, IEnumerable<GetClientsWithRoleFormat> providerClients, DataManager dm)
            {
                this.Clients = clients ?? Enumerable.Empty<GetClientsWithRoleFormat>();
                this.ProviderClients = providerClients ?? Enumerable.Empty<GetClientsWithRoleFormat>();

                this.ModulesProviderClients = new Dictionary<int, IEnumerable<Module>>();
                this.VisibleBuildingsProviderClients = new Dictionary<int, IEnumerable<Building>>();
                DetermineInstanceTheme();
            }
        
        #endregion

        #region method

            private void DetermineInstanceTheme()
            {
                try
                {
                    IsSchneiderTheme = HttpContext.Current.Request.Url.AbsoluteUri.ToLower().Contains(System.Configuration.ConfigurationManager.AppSettings["SchneiderElectricDomain"].ToLower());
                }
                catch
                {
                }
            }
            /// <summary>
            /// Called on client switch and does NOT represent the User's Organization but DOES represent the organization that the user is switching to
            /// </summary>
            /// <param name="organizationid"></param>
            /// <returns></returns>
            private async Task GetSSOInfoAsync(int organizationid)
            {
                AuthNSSOInfo results = await AuthnHelper.GetSSOInfoAsync(organizationid).ConfigureAwait(false);
                ClientIdentityManagedExternalProvider = results.SSOEnabled;
            }

            public void SwitchClient(Int32 cid, Int32 userOID, Int32? providerID)
            {
                var configMgr = IoC.Resolve<IConfigManager>();
                var env = EnvironmentType.FromValue<EnvironmentType>(configMgr.GetConfigurationSetting(DataConstants.ConfigKeyCWEnv, "", true));

				this.CID = cid;
                GetClientsWithRoleFormat client = Clients.Where(c => (c.CID == cid)).First();
                DataManager dm = DataManager.Get(HttpContext.Current.Session.SessionID);
                if (null == dm.AccountOrgBased || ClientOID != client.OID)
                {
                    // If the user's organization ID is not equal to that of the organization that the user has selected,
                    // determine if the latter is being managed externally
                    if (userOID != client.OID)
                    {
                        GetSSOInfoAsync(client.OID).Wait();
                    }
                    else // same
                    {
                        ClientIdentityManagedExternalProvider = IdentityManagedExternalProvider;
                    }
                    dm.ClientOID = client.OID;
                    StorageCreds creds = dm.SetOrgBasedStorageAccounts(new StorageAccountCriteria() { OID = dm.ClientOID, IsCommon = false, CWEnvironment = env, StorageAcctLevel = StorageAccountLevel.Primary });
                    StorageAccountName = creds.AccountName;
                    StorageAccountPrimaryKey = creds.AccountPrimaryKey;
                    StorageAccountSecondaryKey = creds.AccountSecondaryKey;
                    DataManager.Set(HttpContext.Current.Session.SessionID, dm);
                }

                this.ClientOID = client.OID;
                ClientName = client.ClientName;
                RoleID = client.RoleID;
                IsProxyClient = client.IsProxy;

                //FUTURE: they could switch to their own org client rather then one of the provider clients, which may be in the same client dropdown in the future.
                //currently just see if provider id is not null in session on client switch or from initial login. in the future we would pass in a provider if we are swtiching to a differnt one, to one, or not to one.
                IsLoggedInUnderProviderClient = this.ProviderID != null ? true : false;

                //by uid cid in usersclients azure table
                Modules = dm.ModuleDataMapper.GetAllActiveModulesAssociatedToCIDAndUID(cid, this.UID);

                var buildingAndBuildingGroupsTuple = GetUpdatedBuildingsAndBuildingGroups();

                VisibleBuildings = buildingAndBuildingGroupsTuple.Item1.ToList(); // ToList() is important to avoid deferred execution

                BuildingGroups = buildingAndBuildingGroupsTuple.Item2.ToList();

                TimeZoneInfo tzi = GetEarliestBuildingsTimeZone(VisibleBuildings);

                EarliestBuildingTimeZoneID = tzi == null ? "" : tzi.Id;

                //CUSTOM THEMING----
                    OrganizationTheme organizationTheme = null;

                    //get primary provider theming for cid if any
                    int? themedProvider = dm.ClientDataMapper.GetClientSettingsByClientID(cid).ThemedProvider;                    

                    //if primary provider theming exists
                    if (themedProvider != null)
                    {
                        //TODO: if we have provider theming, rathern then just org theming.
                        //check if provider theming is enabled
                        //ProviderTheme providerTheme = dm.ProviderDataMapper.GetProviderThemeByProviderID((int)themedProvider);
                        //if (providerTheme.EnableCustomTheme)
                        //{
                            //set provider theming
                        //}

                        organizationTheme = dm.OrganizationDataMapper.GetOrganizationThemeByOID(dm.OrganizationDataMapper.GetOrganizationByProviderID((int)themedProvider).OID);
                    }

                    //if previous theme not enabled
                    if(organizationTheme == null || !organizationTheme.EnableCustomTheme)
                    {                       
                        //get for clients org
                        organizationTheme = dm.OrganizationDataMapper.GetOrganizationThemeByOID(ClientOID);

                        //if previous them not enabled, and clientoid and useroid are different, get for user org
                        if ((organizationTheme == null || !organizationTheme.EnableCustomTheme) && (ClientOID != userOID))
                        {
                            organizationTheme = dm.OrganizationDataMapper.GetOrganizationThemeByOID(userOID);
                        }
                    }

                    SetOrClearOrgTheme(organizationTheme);
                    HttpContext.Current.Session[USER_SESSION_KEY] = this;
            }

            private void SetProviderLists()
            {
                int counter = 1;

                Double earliestTimezoneOffset = 0;

                DataManager dm = DataManager.Get(HttpContext.Current.Session.SessionID);
                foreach (GetClientsWithRoleFormat pc in ProviderClients)
                {
                    //***Security: Check if provider client with a maxrole, and override user to maxrole if thier role is higher***
                    int tempProviderClientMaxRoleID = dm.ProviderClientDataMapper.GetProvidersClientsListFlattenedByProviderIDAndCID((int)ProviderID, pc.CID).First().MaxRoleID;

                    if (tempProviderClientMaxRoleID > pc.RoleID)
                    {
                        //update the role in the client list. needed for client switch later.
                        ProviderClients.Where(m => m.CID == pc.CID).First().RoleID = tempProviderClientMaxRoleID;
                    }

                    if (!ModulesProviderClients.ContainsKey(pc.CID))
                    {
                        //by uid cid in usersclients azure table
                        this.ModulesProviderClients.Add(pc.CID, dm.ModuleDataMapper.GetAllActiveModulesAssociatedToCIDAndUID(pc.CID, this.UID));
                    }

                    if (!VisibleBuildingsProviderClients.ContainsKey(pc.CID))
                    {
                        this.VisibleBuildingsProviderClients.Add(pc.CID, GetUpdatedVisibleBuidings(pc.CID).ToList()); // ToList() is important to avoid deferred execution
                    }
                    
                    if (VisibleBuildingsProviderClients.ContainsKey(pc.CID))
                    {
                        TimeZoneInfo earliestBuildingTimezone = GetEarliestBuildingsTimeZone(VisibleBuildingsProviderClients[pc.CID]);

                        if (earliestBuildingTimezone != null && (counter == 1 || earliestBuildingTimezone.BaseUtcOffset.TotalHours > earliestTimezoneOffset))
                        {
                            earliestTimezoneOffset = earliestBuildingTimezone.BaseUtcOffset.TotalHours;
                            EarliestBuildingTimeZoneIDProviderClients = earliestBuildingTimezone.Id;
                            counter++;
                        }
                    }
                }
            }

			public IEnumerable<Building> GetUpdatedVisibleBuidings(int? cidOverride = null) //will be refactored to use passed in 'DataManager.Instance'. -Igor
			{
                DataManager dm = DataManager.Get(HttpContext.Current.Session.SessionID);
                //kgs super or higher all buildings
                if (this.IsKGSSuperAdminOrHigher) return dm.BuildingDataMapper.GetAllBuildingsByCID(cidOverride != null ? (int)cidOverride : CID, new Expression<Func<Building, Object>>[] { (b => b.State) });

                //if provider super admin allow invisible and inactive
                if (this.IsLoggedInUnderProviderClient && this.IsSuperAdmin)
                    return dm.BuildingDataMapper.GetAllBuildingsAssociatedToUIDForClient(UID, this.UserOID, cidOverride != null ? (int)cidOverride : CID, this.ClientOID, this.ProviderID, false, false, new Expression<Func<Building, Object>>[] { (b => b.State) });
                else
                    return dm.BuildingDataMapper.GetAllBuildingsAssociatedToUIDForClient(UID, this.UserOID, cidOverride != null ? (int)cidOverride : CID, this.ClientOID, this.ProviderID, true, true, new Expression<Func<Building, Object>>[] { (b => b.State) });
            }

            public Tuple<IEnumerable<Building>, IEnumerable<BuildingGroup>> GetUpdatedBuildingsAndBuildingGroups(int? cidOverride = null) //will be refactored to use passed in 'DataManager.Instance'. -Igor
            {
                DataManager dm = DataManager.Get(HttpContext.Current.Session.SessionID);
                Expression<Func<Building, Object>>[] loadWithExpressions = new Expression<Func<Building, Object>>[] { (b => b.State), (b => b.BuildingSettings) };
                //kgs super or higher all buildings and groups
                if (this.IsKGSSuperAdminOrHigher)
                    return new Tuple<IEnumerable<Building>, IEnumerable<BuildingGroup>>(dm.BuildingDataMapper.GetAllBuildingsByCID(cidOverride != null ? (int)cidOverride : CID, loadWithExpressions), dm.BuildingGroupDataMapper.GetAllBuildingGroupsByCID(cidOverride != null ? (int)cidOverride : CID));

                //if provider super admin allow invisible and inactive
                if (this.IsLoggedInUnderProviderClient && this.IsSuperAdmin)
                    return dm.BuildingDataMapper.GetAllBuildingsAndBuildingGroupsAssociatedToUIDForClient(UID, this.UserOID, cidOverride != null ? (int)cidOverride : CID, this.ClientOID, this.ProviderID, false, false, loadWithExpressions);
                else
                    return dm.BuildingDataMapper.GetAllBuildingsAndBuildingGroupsAssociatedToUIDForClient(UID, this.UserOID, cidOverride != null ? (int)cidOverride : CID, this.ClientOID, this.ProviderID, true, true, loadWithExpressions);

            }

            public TimeZoneInfo GetEarliestBuildingsTimeZone(IEnumerable<Building> buildings)
            {
                int counter = 1;

                TimeZoneInfo earliestTimeZone = null;
                
                //get all clients where they contain a building in the provided timezone
                foreach (Building building in buildings)
                {
                    if (counter == 1 || TimeZoneInfo.FindSystemTimeZoneById(building.TimeZone).BaseUtcOffset > earliestTimeZone.BaseUtcOffset)
                    {
                        earliestTimeZone = TimeZoneInfo.FindSystemTimeZoneById(building.TimeZone);
                    }
                }

                return earliestTimeZone;
            }

            private void SetOrClearOrgTheme(OrganizationTheme organizationTheme)
            {
                if (organizationTheme != null && organizationTheme.EnableCustomTheme)
                {
                    bool IsSchneiderTheme = SiteUser.Current.IsSchneiderTheme;
                    //set org theming
                    OrganizationHeaderBGColor = organizationTheme.HeaderBGColor;
                    OrganizationHeaderTextColor = organizationTheme.HeaderTextColor;
                    OrganizationIconUrl = String.IsNullOrEmpty(organizationTheme.IconExtension) ? "" : HandlerHelper.OrganizationImageUrl(organizationTheme.OID, organizationTheme.IconExtension, HandlerHelper.OrganizationAssetType.Icon, IsSchneiderTheme);
                    OrganizationLogoUrl = String.IsNullOrEmpty(organizationTheme.LogoExtension) ? "" : HandlerHelper.OrganizationImageUrl(organizationTheme.OID, organizationTheme.LogoExtension, HandlerHelper.OrganizationAssetType.Logo, IsSchneiderTheme);
                }
                else
                {
                    //clear or dont set theming
                    OrganizationHeaderBGColor = null;
                    OrganizationHeaderTextColor = null;
                    OrganizationIconUrl = null;
                    OrganizationLogoUrl = null;
                }
            }

        #endregion

        #region property

            [DataMember]
            public String StorageAccountName { get; private set; }

            [DataMember]
            public String StorageAccountPrimaryKey { get; private set; }

            [DataMember]
            public String StorageAccountSecondaryKey { get; private set; }

            [DataMember]
            public Boolean IsAnonymous
            {
                private set;
                get;
            }

            //
            [DataMember]
            public IEnumerable<GetClientsWithRoleFormat> Clients
            {
                private set;
                get;
            }
            [DataMember]
            public IEnumerable<GetClientsWithRoleFormat> ProviderClients
            {
                private set;
                get;
            }


            [DataMember]
            public IEnumerable<Module> Modules
            {
                private set;
                get;
            }
            [DataMember]
            public Dictionary<int, IEnumerable<Module>> ModulesProviderClients
            {
                private set;
                get;
            }

            [DataMember]
            public IEnumerable<Building> VisibleBuildings
            {
                private set;
                get;
            }
            [DataMember]
            public Dictionary<int, IEnumerable<Building>> VisibleBuildingsProviderClients
            {
                private set;
                get;
            }

            [DataMember]
            public IEnumerable<BuildingGroup> BuildingGroups
            {
                private set;
                get;
            }

            //

            [DataMember]
            public Int32 UserOID
            {
                private set;
                get;
            }

            [DataMember]
            public Int32 ClientOID
            {
                private set;
                get;
            }

            [DataMember]
            public Int32 CID
            {
                private set;
                get;
            }

            [DataMember]
            public String ClientName
            {
                private set;
                get;
            }

            [DataMember]
            public Int32 RoleID
            {
                private set;
                get;
            }

            //
            [DataMember]
            public Int32? ProviderID
            {
                private set;
                get;
            }

            //
            [DataMember]
            public Int32 UID
            {
                private set;
                get;
            }

            [DataMember]
            public String Email
            {
                private set;
                get;
            }

            [DataMember]
            public String FirstName
            {
                private set;
                get;
            }

            [DataMember]
            public String LastName
            {
                private set;
                get;
            }

            //

            public String FullName
            {
                get {return String.Concat(FirstName, " ", LastName);}
            }
            
            //

            //public Boolean AsProvider
            //{
            //    private set;
            //    get;
            //}

            //
            [DataMember]
            public Boolean IsOrgAdmin
            {
                private set;
                get;            
            }

            //
            [DataMember]
            public Single UserTimeZoneOffset
            {
                private set;
                get;
            }

            [DataMember]
            public String EarliestBuildingTimeZoneID
            {
                private set;
                get;
            }

            [DataMember]
            public String EarliestBuildingTimeZoneIDProviderClients
            {
                private set;
                get;
            }

            //
            [DataMember]
            public Boolean IsLoggedInUnderProviderClient
            {
                private set;
                get;  
            }        

            [DataMember]
            public Boolean IsProxyClient
            {
                private set;
                get;
            }


            public Boolean IsLoggedInUnderKGSBuildingsClient
            {
                get { return (CID == BusinessConstants.Client.KGSBuildingsCID); }
            }

            //

            public Boolean IsKGSSystemAdmin
            {
                get {return RoleHelper.IsKGSSystemAdmin(RoleID.ToString());}
            }

 
            public Boolean IsKGSSuperAdmin
            {
                get {return RoleHelper.IsKGSSuperAdmin(RoleID.ToString());}
            }

   
            public Boolean IsKGSFullAdmin
            {
                get {return RoleHelper.IsKGSFullAdmin(RoleID.ToString());}
            }

   
            public Boolean IsKGSSuperAdminOrHigher
            {
                get {return RoleHelper.IsKGSSuperAdminOrHigher(RoleID.ToString());}
            }


            public Boolean IsKGSFullAdminOrHigher
            {
                get {return RoleHelper.IsKGSFullAdminOrHigher(RoleID.ToString());}
            }

            //ROLE ID is never restricted, as its either set from the primary or secondary depending
            //public Boolean IsKGSRestrictedAdminOrHigher

            //

            public Boolean IsSuperAdmin
            {
                get {return RoleHelper.IsSuperAdmin(RoleID.ToString());}
            }


            public Boolean IsSuperAdminOrHigher
            {
                get {return RoleHelper.IsSuperAdminOrHigher(RoleID.ToString());}
            }


            public Boolean IsFullAdmin
            {
                get {return RoleHelper.IsFullAdmin(RoleID.ToString());}
            }


            public Boolean IsSuperAdminOrFullAdmin
            {
                get {return RoleHelper.IsSuperAdminOrFullAdmin(RoleID.ToString());}
            }

            public Boolean IsFullAdminOrHigher
            {
                get {return RoleHelper.IsFullAdminOrHigher(RoleID.ToString());}
            }


            //ROLE ID is never restricted, as its either set from the primary or secondary depending
            //public Boolean IsRestrictedAdminOrHigher

            //
            public Boolean IsFullUser
            {
                get {return RoleHelper.IsFullUser(RoleID.ToString());}
            }

            //
            public Boolean IsSuperAdminOrFullAdminOrFullUser
            {
                get {return RoleHelper.IsSuperAdminOrFullAdminOrFullUser(RoleID.ToString());}
            }

            public Boolean IsFullAdminOrHigherOrFullUser
            {
                get {return RoleHelper.IsFullAdminOrHigherOrFullUser(RoleID.ToString());}
            }

            //ROLE ID is never restricted, as its either set from the primary or secondary depending
            //public Boolean IsRestricted

            public Boolean IsPublicOrKioskUser
            {
                get { return RoleHelper.IsPublicOrKioskUser(RoleID.ToString()); }
            }
            public Boolean IsPublicUser
            {
                get { return RoleHelper.IsPublicUser(RoleID.ToString());}
            }
            public Boolean IsKioskUser
            {
                get { return RoleHelper.IsKioskUser(RoleID.ToString()); }
            }

            //
            [DataMember]
            public String OrganizationHeaderBGColor
            {
                private set;
                get;
            }
            [DataMember]
            public String OrganizationHeaderTextColor
            {
                private set;
                get;
            }
            [DataMember]
            public String OrganizationLogoUrl
            {
                private set;
                get;
            }
            [DataMember]
            public String OrganizationIconUrl
            {
                private set;
                get;
            }

            //
            [DataMember]
            public bool IsSchneiderTheme
            {
                private set;
                get;
            }

            //
            [DataMember]
            public String CultureName
            {
                private set;
                get;
            }

            /// <summary>
            /// This represents whether the User's Organization is under external IdP mgmt
            /// </summary>
            [DataMember]
            public bool IdentityManagedExternalProvider
            {
                set;
                get;
            }
        
            /// <summary>
            /// This represents whether the Organization is under external IdP mgmt
            /// </summary>
            [DataMember]
            public bool ClientIdentityManagedExternalProvider
            {
                set;
                get;
            }
        #endregion

        #region class

            //public class SiteUserClients
            //{
            //    private readonly
            //            SiteUser
            //            siteUser;

            //    private readonly
            //            IDictionary<Int32, ClientDataMapper.GetClientsDataViewFormat>
            //            cidToClient;

            //    public SiteUserClients(SiteUser siteUser, IEnumerable<ClientDataMapper.GetClientsDataViewFormat> clients)
            //    {
            //        this.siteUser = siteUser;

            //        cidToClient = new Dictionary<Int32, ClientDataMapper.GetClientsDataViewFormat>();
            //    }

            //    public ClientDataMapper.GetClientsDataViewFormat this[Int32 CID]
            //    {
            //        get
            //        {
            //            ClientDataMapper.GetClientsDataViewFormat client;

            //            cidToClient.TryGetValue(CID, out client);

            //            return client;
            //        }
            //    }

            //    protected Int32 Count
            //    {
            //        get
            //        {
            //            return cidToClient.Count;
            //        }
            //    }
            //}

            //public class SiteUserModules
            //{
            //    private readonly
            //            SiteUser
            //            siteUser;

            //    private readonly
            //            IDictionary<Int32, IList<ModuleHelper.Module>>
            //            clientToModuleDictionary;

            //    public SiteUserModules(SiteUser siteUser)
            //    {
            //        this.siteUser = siteUser;

            //        clientToModuleDictionary = new Dictionary<Int32, IList<ModuleHelper.Module>>();
            //    }

            //    public IList<ModuleHelper.Module> this[Int32 CID]
            //    {
            //        get
            //        {
            //            IList<ModuleHelper.Module> modules;

            //            clientToModuleDictionary.TryGetValue(CID, out modules);

            //            if (modules == null)
            //            {
            //                LoadModulesForClient(CID);
            //            }

            //            return clientToModuleDictionary[CID];
            //        }
            //    }

            //    private void LoadModulesForClient(Int32 CID)
            //    {
            //        IList<ModuleHelper.Module> modules;

            //        if (siteUser.clients[CID] == null)
            //        {
            //            //TODO: decide if a security exception should be thrown instead of a NULL

            //            modules = null;
            //        }
            //        else
            //        {
            //            modules = ModuleDataMapper.GetAllModulesAssociatedToClientID(CID).Select(m => (ModuleHelper.Module)m.ModuleID).ToList();
            //        }

            //        clientToModuleDictionary.Add(CID, modules);
            //    }
            //}

        #endregion
    }
}