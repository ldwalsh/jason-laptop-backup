﻿using System;

namespace CW.Website._framework
{
    public sealed class ViewStateEventArgs: EventArgs
    {
        public ViewState ViewState;
    }
}