﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._framework
{
    [
    AttributeUsage(AttributeTargets.Property)
    ]
    public class SessionStatePropertyAttribute: Attribute
    {
    }
}