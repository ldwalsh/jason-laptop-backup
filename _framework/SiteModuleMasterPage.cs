﻿using CW.Common.Constants;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._framework
{
    public class SiteModuleMasterPage: MasterPage
    {
        #region CLASS

			protected class HtmlElement : HtmlGenericControl //eventually move to CW.Utility.Web.UI.HtmlElement
            {
                public HtmlElement(String tagName, IEnumerable<KeyValuePair<String, String>> attributes)
                {
                    TagName = tagName;

                    foreach (var a in attributes)
                    {
                        Attributes.Add(a.Key, a.Value);
                    }
                }
            }

        #endregion

        #region field

            private readonly Type pageType;
            protected BusinessConstants.Module.Modules mModule;

        #endregion

        #region constructor

            public SiteModuleMasterPage()
            {
                pageType = GetType().BaseType;
            }

        #endregion

        #region method

            #region override

                protected override void OnInit(EventArgs e)
                {
                    var siteUser = SiteUser.Current;
                    HtmlHead head = (HtmlHead)Page.Header;
                    ClientScriptManager cs = Page.ClientScript;
                    Type cstype = this.GetType();

                    //Check if logged in
                    if (siteUser.IsAnonymous)
                    {
                        //in order to refer back to module after logging in.
                        //also for qr code reader redirect to profile pages after loggging in.
                        Response.Redirect("/Home.aspx?referrer=" + Request.Url.PathAndQuery);
                    }
                    else if (!siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(mModule)))
                    {
                        //dont redirect with referrer if they dont have access to the module but logged in as the home page will redirect right back to this module causeing and infinite loop.
                        Response.Redirect("/Home.aspx");
                    }
                    else if (!siteUser.VisibleBuildings.Any())
                    {
                        //cant load modules if they have not buildings yet
                        Response.Redirect("/Home.aspx");
                    }

                    new CSRFDefender(Page, ViewState);
                    Page.PreLoad += CSRFValidationSecondary_PreLoad;

                    //dynamically add themed css and favicons based on se theme check
                    HtmlLink link = new HtmlLink();
                    link.Attributes.Add("href", siteUser.IsSchneiderTheme ? Page.ResolveClientUrl("/_assets/styles/themes/ba.css") : Page.ResolveClientUrl("/_assets/styles/themes/cw.css"));
                    link.Attributes.Add("type", "text/css");
                    link.Attributes.Add("rel", "stylesheet");
                    head.Controls.Add(link);

                    link = new HtmlLink();
                    link.Attributes.Add("href", siteUser.IsSchneiderTheme ? Page.ResolveClientUrl("/_assets/styles/themes/images/se-favicon.ico") : Page.ResolveClientUrl("/_assets/styles/themes/images/cw-favicon.ico"));
                    link.Attributes.Add("type", "image/x-icon");
                    link.Attributes.Add("rel", "shortcut icon");
                    head.Controls.Add(link);

                    HtmlTitle title = (HtmlTitle)this.FindControl("title");
                    title.Text = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, title.Text).ToUpper();


                    if (!String.IsNullOrEmpty(siteUser.OrganizationHeaderBGColor) || !String.IsNullOrEmpty(siteUser.OrganizationHeaderTextColor))
                    {
                        //--Dynamically generated internal style sheet hack---------------------------------      
                        var dynamicallyGeneratedStyleSheetHack = (Literal)this.FindControl("dynamicallyGeneratedStyleSheetHack");

                        if (dynamicallyGeneratedStyleSheetHack != null)
                        {
                            dynamicallyGeneratedStyleSheetHack.Text = "<style> ";

                            //set organization custom theming if enabled
                            //set header bg color if exists
                            if (!String.IsNullOrEmpty(siteUser.OrganizationHeaderBGColor))
                            {
                                HtmlControl header = (HtmlControl)this.FindControl("header");
                                if (header != null) header.Style.Add("background", siteUser.OrganizationHeaderBGColor);

                                dynamicallyGeneratedStyleSheetHack.Text += ".divLeftNav, .divLeftNav a{background-color: " + siteUser.OrganizationHeaderBGColor + " !important;}";
                            }

                            if (!String.IsNullOrEmpty(siteUser.OrganizationHeaderTextColor))
                            {
                                dynamicallyGeneratedStyleSheetHack.Text += ".header .divHeaderBar .divLogin a, .header .divLogin,.header .divPrimaryNav,.header .divPrimaryNav a,.header .divSelectClient,.header .divSelectModule{color: " + siteUser.OrganizationHeaderTextColor + " !important;}";
                                dynamicallyGeneratedStyleSheetHack.Text += ".header .divHeaderBar ul#utility li{border-right: 1px solid " + siteUser.OrganizationHeaderTextColor + " !important;}";
                                dynamicallyGeneratedStyleSheetHack.Text += ".header .divHeaderBar ul#utility li.last{border-right: none !important;}";
                                dynamicallyGeneratedStyleSheetHack.Text += ".header .divSelectClient,.header .divSelectModule{border-left: 1px solid " + siteUser.OrganizationHeaderTextColor + " !important;}";
                                dynamicallyGeneratedStyleSheetHack.Text += ".divLeftNav a{color: " + siteUser.OrganizationHeaderTextColor + " !important;}";
                            }

                            dynamicallyGeneratedStyleSheetHack.Text += " </style>";
                        }
                        //----------------------------------------------------------------------------------
                    }

                    //set provider custom theming if enabled            

                    //dynmaically load users culture based kendo culture script.
                    HtmlGenericControl cultureInclude = new HtmlGenericControl("script");
                    cultureInclude.Attributes.Add("src", "/_assets/scripts/kendocultures/kendo.culture." + siteUser.CultureName + ".js");
                    cultureInclude.Attributes.Add("type", "text/javascript");
                    head.Controls.Add(cultureInclude);  

                    //register kendo startup script
                    string kendoInitializeScript = "<script type=\"text/javascript\"> kendo.culture(\"" + siteUser.CultureName + "\"); </script>";
                    cs.RegisterStartupScript(cstype, "Kendo Culture Intialize", kendoInitializeScript, false);
                
                    ControlHelper.InvokeTemplateControlEvent(this, "Page_Init", e);

                    base.OnInit(e);
                }

                protected override void Render(HtmlTextWriter writer)
                {
                    if (Page.Header == null) throw new Exception("The head tag must exist on the (master)page and have a runat='server' attribute.");

                    Page.Header.Controls.AddAt(0, new HtmlElement("base", new Dictionary<String,String>{{"href", LinkHelper.GetBaseUrl(Request, !UseFullBasePath)},}));

                    base.Render(writer);
                }

            #endregion

            protected void CSRFValidationSecondary_PreLoad(Object sender, EventArgs e)
            {
                if (this.IsPostBack)
                {
                    if ((String)ViewState["AntiXsrfSessionIdKey"] != HttpContext.Current.Session.SessionID) throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
                else
                {
                    ViewState["AntiXsrfSessionIdKey"] = HttpContext.Current.Session.SessionID;
                }
            }

        #endregion

        #region property

            protected virtual Boolean UseFullBasePath
            {
                get {return false;}
            }

        #endregion
    }
}