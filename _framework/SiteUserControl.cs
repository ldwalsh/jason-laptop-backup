﻿using CW.Business;
using CW.Common.Config;
using CW.Logging;
using CW.Utility;
using CW.Utility.Web;
using CW.Website.DependencyResolution;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace CW.Website._framework
{   
	[
	DebuggerStepThrough
	]
	public class SiteUserControl: UserControl, IEnhancedLifeCycle
	{
		#region INTERFACE

			//
			//These interfaces and associated logic will be removed.
			//They will no longer be needed after transition to JSAsserManager

			public interface IClientControl
			{
				//SitePage parentPage {get;}
                
				Control ContainerControl {get;}
			}

			public interface IClientControl2
			{
				String ScriptSource {get;}
			}

		#endregion

		#region IEnhancedLifeCycle

			#region event

				public event EventHandler FirstInit;
				public event EventHandler PostbackInit;
				public event EventHandler AsyncPostbackInit;

				public event EventHandler FirstLoad;
				public event EventHandler PostbackLoad;
				public event EventHandler AsyncPostbackLoad;

				public event EventHandler<ViewStateEventArgs> SavingViewState;
				public event EventHandler<ViewStateEventArgs> LoadedViewState;

			#endregion

		#endregion

		#region field

			#region readonly
            
				private readonly Type controlType;

			#endregion
            
			private ScriptManager _scriptManager;

			private ViewState           _viewState;
			private SessionStateWrapper _sessionState;

		#endregion

		#region event

			public event EventHandler Configure;

		#endregion

		#region constructor

			public SiteUserControl()
			{
				controlType = GetType().BaseType;

				ConfigMgr = IoC.Resolve<IConfigManager>();
				LogMgr = IoC.Resolve<ISectionLogManager>();
			}


		#endregion

		#region method

			#region override

				protected override void OnInit(EventArgs ea)
				{
					if (SupportAutoEvents) throw new Exception("AutoEventWireup must be disabled for ModuleUserControl: " + controlType.Name);

					//

					OnConfigure(ea);

					//

					Script = new ScriptInterop(this);
					
					if (EnablePropertyPersistanceManager)
					{
						Page.InitComplete += delegate{new PropertyPersistanceManager(this, viewState);}; //Need to make instantiation of PPM after LoadViewState?
					}

					//

					ControlHelper.InvokeTemplateControlEvent(this, "Page_Init", ea);

					base.OnInit(ea);

					if ((scriptManager != null) && scriptManager.IsInAsyncPostBack)
					{
						OnAsyncPostbackInit(ea);

						return;
					}
                    
					if (IsPostBack)
					{
						OnPostbackInit(ea);

						return;
					}
                    
					OnFirstInit(ea);
				}

				protected override void LoadViewState(Object savedState)
				{
					base.LoadViewState(savedState);

					ReflectionHelper.FireEvent(LoadedViewState, this, (IsPostBack ? new ViewStateEventArgs{ViewState=viewState} : null));
				}

				protected override void OnLoad(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_Load", e);

					base.OnLoad(e);

					if ((scriptManager != null) && scriptManager.IsInAsyncPostBack)
					{
						OnAsyncPostbackLoad(e);

						return;
					}

					if (IsPostBack)
					{
						OnPostbackLoad(e);

						return;
					}
                    
					OnFirstLoad(e);
				}

				protected override void OnPreRender(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_PreRender", e);
                    
					base.OnPreRender(e);
				}

				protected override Object SaveViewState()
				{
					ReflectionHelper.FireEvent(SavingViewState, this, new ViewStateEventArgs{ViewState=viewState});

					return base.SaveViewState();
				}

				protected override void Render(HtmlTextWriter writer)
				{
                    if (controlType.GetInterface(typeof(IClientControl).Name) != null)
					{
						var sb = new StringBuilder();
                    
						base.Render(new HtmlTextWriter(new StringWriter(sb)));

						var clientControl = (IClientControl)this;

						writer.Write
						(
							new Regex(@"var[\W]+\$this[\W]+=[\W]+new[\W]+function\(\)[\W]+{").Replace
							(
								sb.ToString(),
								String.Concat
								(
									("var " + ClientID + "=new function(){"),
									("var me=this;"),
									("this.ClientID=\"" + ClientID + "\";"),
									((clientControl.ContainerControl == null) ? String.Empty : String.Concat("this.id=\"", clientControl.ContainerControl.ClientID, "\";"))
									//("jQuery(document).ready(function(){alert(me);me.controls=jQuery(\"[id^='" + ClientID + "']\").get();alert(me.controls.length);});")
								)

							).Replace("$this", ClientID)
						);

						return;
					}

					base.Render(writer);
				}

			#endregion

			#region virtual

				protected virtual void OnConfigure(EventArgs ea)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_Configure", ea);

					ReflectionHelper.FireEvent(Configure, this, ea);
				}

				protected virtual void OnFirstInit(EventArgs ea)
				{
					var clientControl = this as IClientControl2;

					if (clientControl != null)
					{
						//var script =
						//new Regex(@"(var[\W]+|window.)\$this[\W]+=[\W]+new[\W]+function\(\)[\W]+{").Replace ////var[\W]+\$this[\W]+=[\W]+new[\W]+function\(\)[\W]+{
						//(
						//    clientControl.ClientScript,
						//    String.Concat
						//    (
						//        ("var " + ClientID + "=new function(){"),
						//        ("var me=this;"),
						//        ("this.ClientID=\"" + ClientID + "\";"),
						//        ("this.UniqueID=\"" + UniqueID + "\";"),
						//        ("var UpdatePanelID=" + ScriptInterop.ToJavascriptObject(FindParent<UpdatePanel>().ClientID) + ";")
						//    )
                                                        
						//).Replace("$this", ClientID);
                        
						//Page.ClientScript.RegisterClientScriptBlock(controlType, ("ControlScript_" + ClientID), script, true);

						if (!Page.ClientScript.IsClientScriptBlockRegistered(controlType, "ControlType"))
						{
							//
							//In Release, the scipt should be added as as Include

							Page.ClientScript.RegisterClientScriptBlock(controlType, "ControlType", clientControl.ScriptSource, true);
						}

						var ns = controlType.Namespace.Replace("CW.Website.", String.Empty) + "." + controlType.Name;

						Page.ClientScript.RegisterClientScriptBlock
						(
							controlType,
							String.Format("InstanceOf:{0}, {1}", new[]{controlType.Name, ClientID}),
							String.Format("window.{1}=new {0}();{1}.UniqueID='{1}';{1}.ClientID='{2}';", new Object[]{ns, UniqueID, ClientID,}), //"var x = new {0}();x.UniqueID='{1}';x.ClientID='{2}';window.{2}=x;x=null;",
							true
						);
					}

					//
                    
					ControlHelper.InvokeTemplateControlEvent(this, "Page_FirstInit",  ea);

					ReflectionHelper.FireEvent(FirstInit, this, ea);
				}

				protected virtual void OnPostbackInit(EventArgs ea)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_PostbackInit", ea);

					ReflectionHelper.FireEvent(PostbackInit, this, ea);
				}

				protected virtual void OnAsyncPostbackInit(EventArgs ea)
				{
					//if ((pageType.GetInterface(typeof(IClientControl).Name) != null) && (parentPage != null))
					//{
					//    parentPage.InvokeClientFunction("$this.Page_AsyncPostbackLoad".Replace("$this", ClientID));
					//}

					ControlHelper.InvokeTemplateControlEvent(this, "Page_AsyncPostbackInit", ea);

					ReflectionHelper.FireEvent(AsyncPostbackInit, this, ea);
				}

				protected virtual void OnFirstLoad(EventArgs ea)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_FirstLoad", ea);

					ReflectionHelper.FireEvent(FirstLoad, this, ea);
				}

				protected virtual void OnPostbackLoad(EventArgs ea)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_PostbackLoad", ea);

					ReflectionHelper.FireEvent(PostbackLoad, this, ea);
				}

				protected virtual void OnAsyncPostbackLoad(EventArgs ea)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_AsyncPostbackLoad", ea);

					ReflectionHelper.FireEvent(AsyncPostbackLoad, this, ea);
				}

			#endregion

			public IList<TControl> FindControls<TControl>(Boolean allowBase=false) where TControl: Control
			{
				return ControlHelper.FindControls<TControl>(this, allowBase);
			}

			public IList<Control> FindControls(IEnumerable<Type> types)
			{
				return ControlHelper.FindControls(this, types);
			}

			public TControl FindControl<TControl>(String id) where TControl: Control
			{
				return ControlHelper.FindControl<TControl>(this, id);
			}

            static public string KeyForTimeoutCookie()
            {
                return("MultiTabSupportExp");
            }

		#endregion

		#region property

			#region override

				protected override Boolean SupportAutoEvents
				{
					get {return false;}
				} 

			#endregion

			#region virtual

				protected virtual Boolean EnablePropertyPersistanceManager
				{
					get {return true;}
				}

			#endregion

			protected IConfigManager ConfigMgr { get; private set; }
			protected ISectionLogManager LogMgr { get; private set; }
            protected DataManager DataMgr { get { return DataManager.Get(System.Web.HttpContext.Current.Session.SessionID); } }

			public SitePage parentPage
			{
				get {return Page as SitePage;}              
			}

            public SiteMasterPage masterPage
            {
                get { return this.Parent as SiteMasterPage; }
            }

			protected ScriptManager scriptManager
			{
				get
				{
					_scriptManager = (_scriptManager ?? ScriptManager.GetCurrent(Page));

					return _scriptManager;
				}
			}

			public ScriptInterop Script
			{
				private set;
				get;
			}

            private SiteUser mSiteUser = null;
            protected bool SiteUserDirty
            {
                set;
                private get;
            }
			protected SiteUser siteUser
			{
				get
				{
                    if (SiteUserDirty)
                    {
                        SiteUserDirty = false;
                        mSiteUser = null;
                    }
                    mSiteUser = mSiteUser ?? SiteUser.Current;
                    return (mSiteUser);
                }
			}

			public SessionStateWrapper sessionState
			{
				get 
				{
					_sessionState = (_sessionState ?? new SessionStateWrapper(Session, Page.GetType().BaseType.Name));

					return _sessionState;
				}
			}

			protected ViewState viewState
			{
				get
				{
					_viewState = (_viewState ?? new ViewState(ViewState));

					return _viewState;
				}
			}

		#endregion
	}
}