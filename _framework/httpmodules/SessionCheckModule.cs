﻿using System;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace CW.Website._framework.httpmodules
{
    /// <summary>
    /// Multi tab session timeout solution. Uses a fake file called from the Login control.    
    /// </summary>
    public class SessionCheckModule : IHttpModule
    {
        public void Init(HttpApplication application)
        {
            application.BeginRequest += new EventHandler(Application_BeginRequest);
        }

        void Application_BeginRequest(object source, EventArgs e)
        {
            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;

            if (context.Request.CurrentExecutionFilePathExtension == ".sessioncheck" && context.Request.QueryString["id"] != null)
            {
                int milliseconds = 0;     

                string connectionString = ConfigurationManager.ConnectionStrings["CWSessionConnectionString"].ConnectionString;

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    string query = "SELECT TOP 1 Expires FROM dbo.Sessions WHERE SessionID LIKE '%" + context.Request.QueryString["id"] + "%'";

                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        try
                        {
                            con.Open();

                            //gets difference in milliseconds until expires
                            milliseconds = ((int)(Convert.ToDateTime(cmd.ExecuteScalar()) - DateTime.UtcNow).TotalMilliseconds);
                        }
                        catch
                        {
                        }
                    }
                }

                context.Response.Clear();
                context.Response.Write(milliseconds);
                context.Response.End();
                context.ApplicationInstance.CompleteRequest();         
            }
        }

        public void Dispose()
        {
        }
    }


}
