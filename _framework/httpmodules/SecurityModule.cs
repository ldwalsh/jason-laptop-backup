﻿using System;
using System.Web;
using System.Web.SessionState;

namespace CW.Website._framework.httpmodules
{
	public class SecurityModule: IHttpModule, IRequiresSessionState
	{
		#region field

			private HttpApplication application;

		#endregion

		#region method

			private void Application_PostAcquireRequestState(Object source, EventArgs e) 
			{
                if (!application.Request.Url.ToString().Contains(".asmx") && !application.Request.Url.ToString().Contains(".axd")) return;

				if (HttpContext.Current.Session == null) return; //this is necessary beacuse .axd requests can have null .Session

				if (!SiteUser.Current.IsAnonymous) return;

				var response = application.Context.Response;

				response.Clear();

				response.StatusCode = 403;

				response.End(); 
			}

		#endregion

		#region IHttpModule

			public void Init(HttpApplication application)
			{
				this.application = application;

				application.PostAcquireRequestState += Application_PostAcquireRequestState;
			}

			public void Dispose()
			{
			}

		#endregion 
	}
}
