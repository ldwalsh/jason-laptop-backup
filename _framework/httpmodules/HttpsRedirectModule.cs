﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace CW.Website._framework.httpmodules
{
    public class HttpsRedirectModule : IHttpModule
    {    
        #region IHttpModule Members

            public void Dispose()
            {
            }
 
            public void Init(HttpApplication context)
            {
                context.BeginRequest += new EventHandler(context_BeginRequest);
            }

            void context_BeginRequest(object source, EventArgs e)
            {
                bool IsHttpsEnabled = false;
                if (!Boolean.TryParse(ConfigurationManager.AppSettings["IsHttpsEnabled"], out IsHttpsEnabled) || !IsHttpsEnabled)
                    return;

                HttpApplication application = (HttpApplication)source;

                if (!application.Request.IsSecureConnection)
                {
                    string baseTargetUrl = ConfigurationManager.AppSettings["BaseSecureUri"];
                    string targetUrl = "";

                    if (string.IsNullOrWhiteSpace(baseTargetUrl))
                    {
                        targetUrl = String.Format("{0}{1}{2}{3}", Uri.UriSchemeHttps, Uri.SchemeDelimiter, application.Request.Url.Authority, application.Response.ApplyAppPathModifier(application.Request.RawUrl));
                    }
                    else 
                    {
                        // Build the appropriate URI based on the specified target URL.
                        var uri = new StringBuilder(baseTargetUrl);

                        // Use the full request path, but remove any sub-application path.
                        uri.Append(application.Request.RawUrl);
                        if (application.Request.ApplicationPath.Length > 1)
                        {
                            uri.Remove(baseTargetUrl.Length, application.Request.ApplicationPath.Length);
                        }

                        // Normalize the URI.
                        uri.Replace("//", "/", baseTargetUrl.Length - 1, uri.Length - baseTargetUrl.Length);

                        targetUrl = uri.ToString();
                    }

                    application.Response.RedirectPermanent(targetUrl);
                }
            }
           
        #endregion

    }
}