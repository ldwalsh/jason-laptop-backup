﻿using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace CW.Website._framework.httpmodules
{
    public class PathRewriteModule: IHttpModule
    {
        #region STATIC

            #region field

                #region readonly

                    private static readonly IDictionary<String,String> pathMaps;

                #endregion

                public static String KGSBuildingGroupAdministration;
                public static String KGSOrganizationAdministration;
                public static String KGSClientAdministration;
                public static String KGSProviderAdministration;
                public static String KGSProviderClientAdministration;
                public static String KGSUserGroupAdministration;

                public static String SystemBackportAdministration;
                public static String SystemBackportBlobAdministration;
                public static String SystemBackportDatabaseAdministration;
                public static String SystemContentAdministration;        
                public static String SystemFaultAdministration;
                public static String SystemGlobalFileAdministration;

                public static String ProviderBuildingGroupAdministration;
                public static String ProviderClientAdministration;
                public static String ProviderOrganizationAdministration;
                public static String ProviderUserGroupAdministration;

                public static String OrganizationAdministration;
                public static String ClientAdministration;
                public static String EquipmentAdministration;
                public static String UserGroupAdministration;
                
				public static String MatlabAssembly;

            #endregion

            #region constructor

                static PathRewriteModule()
                {
                    pathMaps = new Dictionary<String,String>();

                    var appPath = HttpContext.Current.Request.PhysicalApplicationPath.TrimEnd('\\');

                    foreach (var field in typeof(PathRewriteModule).GetFields(BindingFlags.Static|BindingFlags.Public))
                    {
                        var files = new DirectoryInfo(appPath).GetFiles((field.Name + ".aspx"), SearchOption.AllDirectories);

                        if (!files.Any()) continue;
                
                        field.SetValue(null, field.Name);

                        pathMaps.Add(field.Name, files[0].FullName.Replace(appPath, String.Empty).Replace("\\", "/"));
                    }
                }

            #endregion

        #endregion

        #region method

            private void app_BeginRequest(Object sender, EventArgs e)
            {
                var context = HttpContext.Current;
                var url     = LinkHelper.GetFullUrl(context.Request);

                var u = url.Substring(url.IndexOf(Uri.SchemeDelimiter) + 3).Split(new[]{'?'})[0].Split(new[]{'/'});

                if (u.Length != 2) return;
                if (u[1].Contains(".")) return;
                if (!pathMaps.ContainsKey(u[1])) return;

                context.RewritePath(pathMaps[u[1]] + context.Request.Url.Query);
            }

        #endregion

        #region IHttpModule

            void IHttpModule.Init(HttpApplication app)
            {
                app.BeginRequest += app_BeginRequest;
            }

            void IHttpModule.Dispose()
            {
            }

        #endregion
    }
}