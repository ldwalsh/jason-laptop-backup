﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using CW.Data.Interfaces.State;

namespace CW.Website._framework
{
    public class ApplicationStateWrapper: IStateHolder
    {
        #region const

            private const Char DELIMETER = '_';

        #endregion

        #region field

            private readonly String prefix;
            
            private readonly HttpApplicationState appState;

        #endregion

        #region constructor

            public ApplicationStateWrapper(HttpApplicationState appState)
            {
                this.appState = appState;
                
                prefix = String.Empty;
            }

            public ApplicationStateWrapper(HttpApplicationState appState, String prefix)
            {
                this.appState = appState;
                this.prefix   = (prefix + DELIMETER);
            }

        #endregion

        #region method

            private String PrependPrefix(String key)
            {
                return (prefix + key);
            }

            public void Remove(String key)
            {
                appState.Remove(PrependPrefix(key));
            }

            public Object Get(String key)
            {
                return Get<String>(key);
            }
            //
            public Object Get(String key, Type type)
            {
                if (String.IsNullOrWhiteSpace(key)) return null;

                key = PrependPrefix(key);

                var val = appState[key];
            
                if (val == null) return null;

                type = Nullable.GetUnderlyingType(type) ?? type;

                return (type.IsEnum ? Enum.ToObject(type, appState[key]) : val);
            }
            //
            public T Get<T>(String key)
            {
                var val = Get(key, typeof(T));

                if (val == null) return default(T);

                if (val is IConvertible)
                {
                    try
                    {
                        val = Convert.ChangeType(val, typeof(T));
                    }
                    catch
                    {
                        return default(T);
                    }
                }

                return (T)val; //try/catch here or a custom exception? type may not be castable
            }

            public void Set<T>(String key, T value)
            {
                appState.Add(PrependPrefix(key), value);
            }

        #endregion

        #region indexer

            public String this[String key]
            {
                set {Set(key, value);}
                get {return Get<String>(key);}
            }

        #endregion

        #region property

            public String Prefix
            {
                get {return prefix.TrimEnd(DELIMETER);}
            }

            public Int32 Count
            {
                get {return appState.Keys.Cast<String>().Count(s=>s.StartsWith(prefix));}
            }

            public ReadOnlyCollection<String> Keys
            {
                get
                {
                    return
                    new ReadOnlyCollection<String>
                    (
                        appState.Keys.Cast<String>().Where(s=>s.StartsWith(prefix)).Select(s=>s.Substring(prefix.Length)).ToList()
                    );
                }
            }

        #endregion
    }
}