﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using CW.Utility;

namespace CW.Website._framework
{
	public sealed class ViewState: ICollection
	{
		#region IEnumerator

			IEnumerator IEnumerable.GetEnumerator()
			{
				return stateBag.GetEnumerator();
			}

		#endregion

		#region ICollection

			#region method

				void ICollection.CopyTo(Array array, Int32 index)
				{
					stateBag.Values.CopyTo(array, index);
				}

			#endregion

			#region property

				Int32 ICollection.Count
				{
					get {return stateBag.Count;}
				}

				Object ICollection.SyncRoot
				{
					get {return this;}
				}

				Boolean ICollection.IsSynchronized
				{
					get {return false;}
				}

			#endregion

		#endregion
        
		#region field

			private readonly StateBag stateBag;

		#endregion

		#region constructor

			public ViewState(StateBag stateBag)
			{
				this.stateBag = stateBag;
			}

		#endregion

		#region method

			public Boolean HasKey(String key)
			{
				return (stateBag[key] != null);
			}

			public Object Get(String key, Type type)
			{
				if (String.IsNullOrWhiteSpace(key)) return null;
            
				if (stateBag[key] == null) return null;

				type = Nullable.GetUnderlyingType(type) ?? type;

				if (type.IsEnum) return Enum.ToObject(type, stateBag[key]);

				return stateBag[key];

				//try
				//{
				//	return Convert.ChangeType(stateBag[key], type);
				//}
				//catch
				//{
				//	return null;
				//}
			}
			//
			public TValue Get<TValue>(String key)
			{
				var val = Get(key, typeof(TValue));

				if (val == null) return default(TValue);

				return (TValue)val;
			}
			//
			public TValue Get<TType,TValue>(Expression<Func<TType,Object>> key)
			{
				return Get<TValue>(PropHelper.G(key));
			}

			public void Set(String key, Object value)
			{
				if (value is Enum)
				{
					value = (Int32)value;
				}

				stateBag.Add(key, value);
			}
			//
			public void Set<TType>(Expression<Func<TType,Object>> keyExpression, Object value)
			{
				Set(PropHelper.G(keyExpression), value);
			}
			//
			public void Set<TType, TValue>(Expression<Func<TType,Object>> keyExpression, TValue value)
			{
				Set<TType>(keyExpression, value);
			}

			public void Remove(String key)
			{
				stateBag.Remove(key);
			}

		#endregion

		#region indexer

			public String this[String key]
			{
				set {Set(key, value);}
				get {return Get<String>(key);}
			}

		#endregion

		#region property

			public Int32 Count
			{
				get {return stateBag.Count;}
			}

			public IEnumerable<String> Keys
			{
				get {return from String k in stateBag.Keys select k;}
			}

		#endregion
	}
}