﻿using System;

namespace CW.Website._framework
{
    public interface IEnhancedLifeCycle
    {
        event EventHandler FirstInit;
        event EventHandler PostbackInit;
        event EventHandler AsyncPostbackInit;

        //event EventHandler PreLoad;

        event EventHandler FirstLoad;
        event EventHandler PostbackLoad;
        event EventHandler AsyncPostbackLoad;

        event EventHandler<ViewStateEventArgs> LoadedViewState;
        event EventHandler<ViewStateEventArgs> SavingViewState;

        event EventHandler PreRender;
    }
}