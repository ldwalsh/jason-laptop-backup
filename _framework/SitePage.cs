﻿using CW.Business;
using CW.Common.Config;
using CW.Logging;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework.httphandlers;
using CW.Website.DependencyResolution;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._framework
{
	public class SitePage: Page, IEnhancedLifeCycle
	{
		#region INTERFACE

			public interface IClientPage
			{
				Dictionary<String,String> Properties {get;}

				Dictionary<String,String> AsyncProperties {get;}
			}

		#endregion

		#region IEnhancedLifeCycle

			#region event

				public event EventHandler FirstInit;
				public event EventHandler PostbackInit;
				public event EventHandler AsyncPostbackInit;

				public event EventHandler FirstLoad;
				public event EventHandler PostbackLoad;
				public event EventHandler AsyncPostbackLoad;

				public event EventHandler<ViewStateEventArgs> SavingViewState;
				public event EventHandler<ViewStateEventArgs> LoadedViewState;

			#endregion

		#endregion

		#region event

			public event EventHandler FirstPreInit;
			public event EventHandler PostbackPreInit;

		#endregion

		#region field

			#region readonly

				private readonly Type pageType;

			#endregion

		#endregion

		#region constructor

			public SitePage()
			{
				pageType = GetType().BaseType;

				ConfigMgr = IoC.Resolve<IConfigManager>();
				LogMgr = IoC.Resolve<ISectionLogManager>();
			}

		#endregion

		#region method

			#region override

				protected override void InitializeCulture()
				{
					if (siteUser.IsAnonymous)
					{
						//TODO: set user culture based on browser
					}
					else
					{
						UICulture = siteUser.CultureName;
						Culture = siteUser.CultureName;

						Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(siteUser.CultureName);
						Thread.CurrentThread.CurrentUICulture = new CultureInfo(siteUser.CultureName);
					}

					base.InitializeCulture();
				}

				protected override void OnPreInit(EventArgs e)
				{
					if (SupportAutoEvents) throw new Exception("AutoEventWireup must be disabled for ModuleUserControl: " + pageType.Name);

					//

					sessionState = new SessionStateWrapper(Session, pageType.Name.ToUpper());
					queryString  = new QueryString(Request.QueryString);
					viewState    = new ViewState(ViewState);

					InitComplete += delegate{new PropertyPersistanceManager(this, viewState);}; //Need to make instantiation of PPM after LoadViewState?
                    
					//

					ControlHelper.InvokeTemplateControlEvent(this, "Page_PreInit", e);

					base.OnPreInit(e);

					if (IsPostBack)
					{
						OnPostbackPreInit(e);

						return;
					}

					OnFirstPreInit(e);
				}

				protected override void OnInit(EventArgs e)
				{
					scriptManager = ScriptManager.GetCurrent(this);

					//

					ControlHelper.InvokeTemplateControlEvent(this, "Page_Init", e);

					base.OnInit(e);

					if ((scriptManager != null) && scriptManager.IsInAsyncPostBack)
					{
						OnAsyncPostbackInit(e);

						return;
					}
                    
					if (IsPostBack)
					{
						OnPostbackInit(e);

						return;
					}

					//
            
					OnFirstInit(e);
				}

				protected override void LoadViewState(Object savedState)
				{
					base.LoadViewState(savedState);

					ReflectionHelper.FireEvent(LoadedViewState, this, (IsPostBack ? new ViewStateEventArgs{ViewState=viewState} : null));
				}

				protected override void OnPreLoad(EventArgs e)
				{
					//TODO: fire SiteUserControl children OnPreLoad

					base.OnPreLoad(e);
				}

				protected override void OnLoad(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_Load", e);

					base.OnLoad(e);

					//

					Form.Action = Page.AppRelativeVirtualPath.Substring(1);

					//

					if ((scriptManager != null) && scriptManager.IsInAsyncPostBack)
					{
						OnAsyncPostbackLoad(e);

						return;
					}

					if (IsPostBack)
					{
						OnPostbackLoad(e);

						return;
					}

					OnFirstLoad(e);
				}

				protected override void OnPreRender(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_PreRender", e);

					if (IsIClientPage)
					{
						if (scriptManager.IsInAsyncPostBack)
						{
							var script = ((IClientPage)this).AsyncProperties.Aggregate("", (current, a) => current + String.Format("page[\"{0}\"]=\"{1}\";", new[]{a.Key, a.Value}));

							ScriptManager.RegisterStartupScript(this, this.GetType(), ScriptInterop.GetScriptKey(script), script, true);
						}
					}

					base.OnPreRender(e);
				}

				protected override Object SaveViewState()
				{
					ReflectionHelper.FireEvent(SavingViewState, this, new ViewStateEventArgs{ViewState=viewState});

					return base.SaveViewState();
				}

				protected override void Render(HtmlTextWriter writer)
				{
					if (IsIClientPage)
					{
						var sb = new StringBuilder();
                    
						base.Render(new HtmlTextWriter(new StringWriter(sb)));

						writer.Write
						(
							new Regex(@"var[\W]+\$this[\W]+=[\W]+new[\W]+function\(\)[\W]+{").Replace(sb.ToString(), String.Concat("var ", ID, "=new function(){this.id=\"", ID, "\";")).Replace("$this", ID)
						);

						return;
					}

					base.Render(writer);
				}

			#endregion

			#region virtual

				protected virtual void OnFirstPreInit(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_FirstPreInit",  e);

					ReflectionHelper.FireEvent(FirstPreInit, this, e);
				}

				protected virtual void OnPostbackPreInit(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_PostbackPreInit",  e);

					ReflectionHelper.FireEvent(PostbackPreInit, this, e);
				}

				protected virtual void OnFirstInit(EventArgs e)
				{
					Form.Controls.Add(new LiteralControl("<input type=\"hidden\" name=\"__EVENTDATA\" id=\"__EVENTDATA\" />"));

					//
                    
					if (IsIClientPage)
					{
						var script = ((IClientPage)this).Properties.Aggregate("window.page={};", (current, a) => current + String.Format("page.{0}={1};", new[]{a.Key, a.Value}));

						Page.ClientScript.RegisterClientScriptBlock(pageType, String.Format("InstanceOf:{0}, {1}", new[]{pageType.Name, ClientID}), script, true);
					}

					//

					ControlHelper.InvokeTemplateControlEvent(this, "Page_FirstInit",  e);

					ReflectionHelper.FireEvent(FirstInit, this, e);
				}

				protected virtual void OnPostbackInit(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_PostbackInit",  e);

					ReflectionHelper.FireEvent(PostbackInit, this, e);
				}

				protected virtual void OnAsyncPostbackInit(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_AsyncPostbackInit",  e);
                    
					ReflectionHelper.FireEvent(AsyncPostbackInit, this, e);
				}

				protected virtual void OnFirstLoad(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_FirstLoad",  e);

					ReflectionHelper.FireEvent(FirstLoad, this, e);
				}

				protected virtual void OnPostbackLoad(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_PostbackLoad",  e);
                    
					ReflectionHelper.FireEvent(PostbackLoad, this, e);
				}

				protected virtual void OnAsyncPostbackLoad(EventArgs e)
				{
					ControlHelper.InvokeTemplateControlEvent(this, "Page_AsyncPostbackLoad",  e);
                    
					ReflectionHelper.FireEvent(AsyncPostbackLoad, this, e);
				}

			#endregion

			public void IncludeAlarmStyleSheet()
			{
				Page.Header.Controls.Add(new LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + ResolveUrl("~/_assets/styles/alarms.css") + "\" />"));
			}

			public void RegisterResourceAsScriptInclude(String resourceNamespace, String resourceName)  //replace with EmbeddedResource Param
			{
				ClientScript.RegisterClientScriptInclude(GetType(), resourceName, ScriptFileHandler.GetResourceIncludeSrc(pageType.Assembly, resourceNamespace, resourceName));
			}

			public void RegisterFileAsScriptInclude(String relativePath)
			{
				ClientScript.RegisterClientScriptInclude(GetType(), relativePath, relativePath);
			}

			public void RegisterResourceAsScriptBlock(String resourcePath, String resourceName) //replace with EmbeddedResource Param
			{
				var script = new ResourceHelper(pageType.Assembly).GetEmbedded(resourcePath, resourceName).ToString();

				ScriptManager.RegisterClientScriptBlock(this, pageType, ScriptInterop.GetScriptKey(script), script, false);
			}
            
			public void RegisterStringAsScriptBlock(String script, Boolean addScriptTags=true)
			{
				//check to use ClientScript versus ScriptManager?
				ScriptManager.RegisterClientScriptBlock(this, pageType, ScriptInterop.GetScriptKey(script), script, addScriptTags);
			}

			public T GetEventData<T>()
			{
				var type = typeof(T);

				type = Nullable.GetUnderlyingType(type) ?? type;

				if (type.IsEnum) throw new NotImplementedException("");

				try
				{
					return (T)Convert.ChangeType(EventData, type);
				}
				catch
				{
					return default(T);
				}
			}

			public TControl FindControl<TControl>(String id) where TControl: Control
			{
				return ControlHelper.FindControl<TControl>(this, id);
			}

			public IEnumerable<TControl> FindControls<TControl>(Boolean allowBase=false) where TControl: Control
			{
				return ControlHelper.FindControls<TControl>(this, allowBase);
			}
            
			public Control GetPostbackControl()
			{
				var id = Request.Params.Get("__EVENTTARGET");

				return (String.IsNullOrWhiteSpace(id)) ? null : FindControl<Control>(id.Split('$').Last()); //revise FindControl... does not search children
			}
			            
		#endregion

		#region property

			private Boolean IsIClientPage { get { return (this as IClientPage != null); } }

			protected ViewState viewState { get; private set; }
			protected Label ErrorOuputLabel { get; set; }

            private SiteUser mSiteUser = null;
			public SiteUser siteUser //TODO: rename to SiteUser
			{
				get
				{
                    if (null == mSiteUser)
                    {
                        mSiteUser = SiteUser.Current;
                    }
					return(mSiteUser);
				}
			}

			public QueryString queryString { get; private set; }
			public SessionStateWrapper sessionState { get; private set; } //make accessible only through SiteUser object
			public ScriptManager scriptManager { get; private set; }
			public String EventTarget { get { return Request.Form["__EVENTTARGET"]; } }
			public String EventData { get { return Request.Form["__EVENTDATA"]; } }

			protected IConfigManager ConfigMgr { get; set; }
			protected ISectionLogManager LogMgr { get; set; }
            protected DataManager DataMgr { get { return DataManager.Get(System.Web.HttpContext.Current.Session.SessionID); } }

		#endregion

		[
		Obsolete
		]
		protected Control FindControlRecursive(String id, Control root = null)
		{
			if (root == null)
			{
				root = Page;
			}

			if (root.ID == id) return root;

			foreach (Control controlToSearch in root.Controls)
			{
				var controlToReturn = FindControlRecursive(id, controlToSearch);

				if (controlToReturn != null) return controlToReturn;
			}

			return null;
		}

		protected void DisplayErrorOutput(String output) //move out to new module specific Page sub-class
		{
			const String lblError = "lblError";

			var lbl = (ErrorOuputLabel ?? FindControlRecursive(lblError) as Label);

			if (lbl == null) return;

			DisplayErrorOutput(output, lbl);

			if (lbl.ID != lblError) return;
			if (String.IsNullOrWhiteSpace(output)) return;

			var lnk = FindControlRecursive("lnkSetFocusMessage") as HtmlAnchor;

			if (lnk == null) return;

			lnk.Focus();
		}
        
		protected void DisplayErrorOutput(String output, Label errorLabel) //move out to new module specific Page sub-class
		{
			if (String.IsNullOrWhiteSpace(output))
			{
				errorLabel.Text    = String.Empty;
				errorLabel.Visible = false;

				return;
			}

			errorLabel.Text    = output;
			errorLabel.Visible = true;
		}
	}
}