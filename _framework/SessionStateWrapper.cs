﻿using CW.Data.Interfaces.State;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.SessionState;

namespace CW.Website._framework
{
	public class SessionStateWrapper: IStateHolder
	{
		#region const

			private const Char DELIMETER = '_';

		#endregion

		#region field

			private readonly String prefix;
            
			private readonly HttpSessionState sessionState;

		#endregion

		#region constructor

			public SessionStateWrapper(HttpSessionState sessionState)
			{
				this.sessionState = sessionState;
                
				prefix = String.Empty;
			}

			public SessionStateWrapper(HttpSessionState sessionState, String prefix)
			{
				this.sessionState = sessionState;
				this.prefix       = (prefix + DELIMETER);
			}

		#endregion

		#region method

			private String PrependPrefix(String key)
			{
				return (prefix + key);
			}

			public void Remove(String key)
			{
				sessionState.Remove(PrependPrefix(key));
			}

			public Object Get(String key)
			{
				return Get<String>(key);
			}
			//
			public Object Get(String key, Type type)
			{
				if (String.IsNullOrWhiteSpace(key)) return null;

				key = PrependPrefix(key);

				var val = sessionState[key];
            
				if (val == null) return null;

				type = Nullable.GetUnderlyingType(type) ?? type;

				return (type.IsEnum ? Enum.ToObject(type, sessionState[key]) : val);
			}
			//
			public T Get<T>(String key)
			{
				var val = Get(key, typeof(T));

				if (val == null) return default(T);

				if (val is IConvertible)
				{
					try
					{
						val = Convert.ChangeType(val, typeof(T));
					}
					catch
					{
						return default(T);
					}
				}

				return (T)val; //try/catch here or a custom exception? type may not be castable
			}

			public void Set<T>(String key, T value)
			{
				sessionState.Add(PrependPrefix(key), value);
			}

		#endregion

		#region indexer

			public String this[String key]
			{
				set {Set(key, value);}
				get {return Get<String>(key);}
			}

		#endregion

		#region property

			public String Prefix
			{
				get {return prefix.TrimEnd(DELIMETER);}
			}

			public Int32 Timeout
			{
				get {return sessionState.Timeout;}
			}

			public Int32 Count
			{
				get {return sessionState.Keys.Cast<String>().Count(s=>s.StartsWith(prefix));}
			}

			public ReadOnlyCollection<String> Keys
			{
				get {return new ReadOnlyCollection<String>(sessionState.Keys.Cast<String>().Where(s=>s.StartsWith(prefix)).Select(s=>s.Substring(prefix.Length)).ToList());}
			}

		#endregion
	}
}