﻿using CW.Utility;
using CW.Utility.Extension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CW.Website._framework
{
	public class PropertyPersistanceManager
	{
		#region CONST

			private const String PersistanceKey = "PropertyPersistanceManager.ViewStatePropertyIndexes";

		#endregion

		#region STATIC

			private static IEnumerable<PropertyInfo> GetAllProperties(IEnhancedLifeCycle control)
			{
				return ReflectionHelper.GetAllProperties(control.GetType().BaseType).ToList();
			}

		#endregion
        
		#region field

			#region readonly

				private readonly IEnhancedLifeCycle	control;

				private readonly ViewState viewState;

			#endregion

			private IList<PropertyInfo> vsProps;

		#endregion

		#region constructor

			public PropertyPersistanceManager(IEnhancedLifeCycle control, ViewState viewState)
			{
				//check to make sure only one PropertyPersistanceManager is registered per page

				this.control   = control;
				this.viewState = viewState;

				//

				LoadPropertyLists();

				if (vsProps == null)
				{	
					vsProps = new List<PropertyInfo>();

					SavePropertyList();
				}
                
				//

				if (vsProps.Count == 0) return;

				control.LoadedViewState += LoadedViewState;
				control.SavingViewState += SavingViewState;
			}
        
		#endregion

		#region method
            
			public void SavePropertyList()
			{
				var indexes = new List<Int32>();

				var i = 0;

				foreach (var p in GetAllProperties(control))
				{
					if (!p.IsDefined(typeof(ViewStatePropertyAttribute), true)) continue;

					indexes.Add(i++);

					vsProps.Add(p);
				}

				viewState.Set(PersistanceKey, indexes.AsEnumerable());
			}

			public void LoadPropertyLists()
			{
				var indexes = viewState.Get<IEnumerable<Int32>>(PersistanceKey);

				if (indexes == null) return;

				vsProps = new List<PropertyInfo>();

				if (!indexes.Any()) return;

				var allProps = GetAllProperties(control).ToList();

				indexes.ForEach(_=>vsProps.Add(allProps[_]));
			}

			public void LoadedViewState(Object sender, EventArgs e)
			{
				if (e == null) return;

				foreach (var p in vsProps)
				{
					var value = viewState.Get(p.Name, p.PropertyType);

					var attribute = (ViewStatePropertyAttribute)p.GetCustomAttributes(typeof(ViewStatePropertyAttribute), true)[0];

					if ((value == null) && (attribute.DefaultValue != null))
					{
						value = attribute.DefaultValue;
					}

					p.SetValue(control, value, null);
				}
			}

			public void SavingViewState(Object sender, EventArgs e)
			{
				vsProps.ToList().ForEach(_=>viewState.Set(_.Name, _.GetValue(control, null)));
			}

		#endregion
	}
}