﻿using CW.Business;
using CW.Business.Query;

namespace CW.Website._framework
{
    public class SiteModulePage : SitePage
    {
        #region method

            protected override void OnPreInit(System.EventArgs e)
            {
                QueryManager = new QueryManager(DataMgr.ConnectionString, siteUser);

                base.OnPreInit(e);
            }

        #endregion

        #region property

            public QueryManager QueryManager
            {
                private set;
                get;
            }

        #endregion
    }
}