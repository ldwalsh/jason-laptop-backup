﻿using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Security;
using System.Web.UI;

namespace CW.Website._framework
{
    public class CSRFDefender
    {
        #region CONST

            private const String AntiXsrfTokenKey = "__AntiXsrfToken";
            private const String AntiXsrfUserNameKey = "__AntiXsrfUserName";

            private const Char SeperatorChar = '~';

            private static Int32[] NoiseStrBounds = new[]{10,100};

        #endregion

        #region STATIC

            private static Char GetRandomCharFromRange(IEnumerable<Int32> range, Random random=null)
            {
                return GetRandomCharFromRanges(new[]{range}, random);
            }

            private static Char GetRandomCharFromRanges(IEnumerable<IEnumerable<Int32>> ranges, Random random=null)
            {
                random = (random ?? new Random());

                return (Char)ranges.Select(_=>random.Next(_.Min(), _.Max()+1)).ToArray()[random.Next(0, ranges.Count())];
            }

            public static String EncodeKey(String key)
            {
                if (String.IsNullOrWhiteSpace(key)) throw new ArgumentException(String.Empty, nameof(key));

                var random = new Random((Int32)DateTime.Now.Ticks);

                var ranges = new[]{Enumerable.Range((int)'A', ((int)'Z'-(int)'A')), Enumerable.Range((int)'a', ((int)'z'-(int)'a'))};

                //return Utility.RijndaelEncryptor.Encrypt(key, paraphrase) //don't think we need to further encrypt

                return
                key.Insert
                (
                    random.Next(0, key.Length),
                    String.Join(String.Empty, Enumerable.Range(NoiseStrBounds.Min(), random.Next(NoiseStrBounds.Min(), NoiseStrBounds.Max()+1)).Select(_=> GetRandomCharFromRanges(ranges, random))).Encapsulate('~')
                );
            }

            public static String DecodeKey(String encodedKey)
            {
                var f = encodedKey.IndexOf(SeperatorChar);
            
                return encodedKey.Remove(f, encodedKey.LastIndexOf(SeperatorChar)-f+1);
            }

        #endregion

        #region field

            private readonly Page page;
            private readonly StateBag viewState;

        #endregion

        #region constructor

            public CSRFDefender(Page page, StateBag viewState)
            {
                this.page      = page;
                this.viewState = viewState;

                page.Init    += (sender, e) => ConfigureCSRFVars();
                page.PreLoad += (sender, e) => CSRFValidationPreLoad();
            }

        #endregion

        #region method

            private void ConfigureCSRFVars()
            {
                var requestCookie = page.Request.Cookies[AntiXsrfTokenKey];

                Guid requestCookieGuidValue;

                if ((requestCookie == null) || Guid.TryParse(requestCookie.Value, out requestCookieGuidValue)) //csrf cookie not yet been set
                {
                    var key = Guid.NewGuid().ToString("N");

                    page.ViewStateUserKey = key;
            
                    page.Response.Cookies.Set(new HttpCookie(AntiXsrfTokenKey){HttpOnly=true, Value=EncodeKey(page.ViewStateUserKey), Secure=(FormsAuthentication.RequireSSL && page.Request.IsSecureConnection)});

                    return;
                }
            
                page.ViewStateUserKey = DecodeKey(requestCookie.Value);
            }

            protected void CSRFValidationPreLoad()
            {
                if (!page.IsPostBack) //initial page visit only
                {
                    viewState[AntiXsrfTokenKey]    = EncodeKey(page.ViewStateUserKey);
                    viewState[AntiXsrfUserNameKey] = (page.User.Identity.Name ?? String.Empty);

                    return;
                }

                if ((DecodeKey((String)viewState[AntiXsrfTokenKey]) == page.ViewStateUserKey) && ((String)viewState[AntiXsrfUserNameKey] == (page.User.Identity.Name ?? String.Empty))) return;
            
                throw new SecurityException("Validation of Anti-XSRF token failed.");
            }

        #endregion
    }
}