﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CW.Website._framework
{
	public class ScriptInterop
	{
		#region ENUM

			[
			Flags
			]
			public enum InteropFlags
			{
				Default = 0,
				ParentWindow = 1,
			}

		#endregion

		#region STATIC

			#region field

				private static HashSet<Type> numerics;

			#endregion

			#region constructor

				static ScriptInterop()
				{
					numerics = new HashSet<Type>{typeof(Decimal), typeof(Single), typeof(Double), typeof(UInt16), typeof(UInt32), typeof(UInt64), typeof(Byte), typeof(SByte), typeof(Int16), typeof(Int32), typeof(Int64)};
				}

			#endregion

			#region method

				public static String GetScriptKey(String script)
				{
					return script.GetHashCode().ToString();
				}

				public static String ToJavascriptObject(Object obj)
				{
					if (obj == null) return "null";

					if (numerics.Contains(obj.GetType())) return obj.ToString();

					if (obj is Boolean) return obj.ToString().ToLower();

					if (obj is String) return new Func<String,String>(_=>((_[0] == '@') ? _.Substring(1) : String.Concat('"', _, '"')))(obj.ToString());

					if (obj is IEnumerable) return ('[' + String.Join(",", ((IEnumerable)obj).Cast<Object>().Select(_=>ToJavascriptObject(_))) + ']');

					return ToJavascriptObject(obj.ToString());
				}

				public static void SetProperty(Control control, String propertyName, Object value)
				{
					SetProperty(control, "window", propertyName, value);
				}

				public static void SetProperty(Control control, String objectName, String propertyName, Object value)
				{
					var script = String.Concat(objectName, '.', propertyName, '=', ToJavascriptObject(value), ';');

					ScriptManager.RegisterStartupScript(control.Page, control.GetType(), GetScriptKey(script), script, true);
				}

				private static StringBuilder BuildArgsString(IEnumerable<Object> args)
				{
					var sb = new StringBuilder();

					if ((args == null) || (!args.Any())) return sb;
                    
					foreach (var a in args)
					{
						sb.Append(ToJavascriptObject(a) + ',');
					}

					sb.Remove((sb.Length-1), 1);

					return sb;
				}

				//public static Boolean IsScriptRegistered(ScriptManager scriptManager, Type type, String key)
				//{
				//    return scriptManager.GetRegisteredClientScriptBlocks().Any(s=> (s.Type == type) && (s.Key == key));
				//}

			#endregion

		#endregion

		#region field

			private readonly Control control;

			private readonly Type type;
         
		#endregion

		#region constructor

			public ScriptInterop(Control control)
			{
				this.control = control;

				type = control.GetType().BaseType;
			}

		#endregion

		#region method

			public void SetProperty(String propertyName, Object value)
			{
				var script = String.Concat("window.", control.ID, '.', propertyName, '=', ToJavascriptObject(value), ';');

				ScriptManager.RegisterStartupScript(control, type, GetScriptKey(script), script, true);
			}

			public void InvokeFunction(String functionName, IEnumerable<Object> args)
			{
				InvokeFunction(functionName, InteropFlags.Default, args);
			}

			public void InvokeFunction(String functionName, InteropFlags flags, IEnumerable<Object> args)
			{
				var script =
				String.Format
				(
					"{0}window.{1}.{2}({3});",
					new Object[]
					{
						((flags == InteropFlags.ParentWindow) ? "window.parent." : String.Empty),
						control.ClientID,
						functionName,
						BuildArgsString(args)
					}
				);

				ScriptManager.RegisterStartupScript(control.Page, type, GetScriptKey(script), script, true);
			}

		#endregion
	}
}