﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.ScheduledBureauReport;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Content = CW.Data.Content;

namespace CW.Website
{
    public partial class BureauReportsv2 : SitePage
    {
        #region Properties

        private IEnumerable<Content> mContent;
        private ScheduledBureauReport mScheduledBureauReport;
        const string addBureauReportSuccess = " bureau report scheduling was successful.";
        const string addBureauReportFailed = "Scheduling bureau report failed. Please contact an administrator.";
        const string updateBureauReportSuccessful = "Bureau report update was successful.";
        const string updateBureauReportFailed = "Bureau report update failed. Please contact an administrator.";
        const string deleteSuccessful = "Bureau report deletion was successful.";
        const string deleteFailed = "Bureau report deletion failed. Please contact an administrator.";

        //gets and sets the gridview viewstate sort direction for the dataview
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? string.Empty; }
            set { ViewState["SortDirection"] = value; }
        }

        //gets and sets the gridview viewstate sort expression for the dataview
        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        #endregion

        #region Page Events

            private void Page_Init()
            {
                //logged in security check in master page.
                //secondary security checks specific to this page.   
              
                //module access
                if (!(siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Diagnostics)))                
                    Response.Redirect("/Home.aspx");
                
                GetAndSetReportContent(siteUser.CultureName);
            }

            private void Page_FirstLoad()
            {
                //datamanger in sitepage

                BindThemes(ddlTheme);
                BindThemes(ddlEditTheme);

                BindBuildings(lbBuildings, siteUser.CID);
                BindBuildings(lbEditBuildings, siteUser.CID);
                BindCultures(ddlCulture);
                BindCultures(ddlEditCulture);
                BindLanguageCultures(ddlLanguage);
                BindLanguageCultures(ddlEditLanguage);

                //get and bind all user groups for that client, and provider.
                IEnumerable<UserGroup> fullUserGroupSet = Enumerable.Empty<UserGroup>();
                int clientOID = DataMgr.OrganizationDataMapper.GetOIDByCID(siteUser.CID);
                
                fullUserGroupSet = DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(clientOID, null);
                
                if(clientOID != siteUser.UserOID)
                    fullUserGroupSet = fullUserGroupSet.Union(DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(siteUser.UserOID, null));

                BindUserGroups(lbUserGroupsTop, fullUserGroupSet);
                

                SetEnabledCheckboxes();

                SetHiddenFields();
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetBureauReportIntoEditForm(GetScheduledBureauReportData bureauReport)
            {
                //ID
                hdnEditID.Value = Convert.ToString(bureauReport.SBRID);
                //Report Name
                txtEditReportName.Text = bureauReport.ReportName;
                //Report Description
                txtEditDescription.Value = bureauReport.ReportDescription;
                //GUID
                hdnEditGUID.Value = Convert.ToString(bureauReport.GUID);
                //Prepared By
                lblEditPreparedByFullName.Text = bureauReport.PreparedByFullName;

                chkEditRunMonthly.Checked = bureauReport.RunMonthly;
                chkEditRunQuarterly.Checked = bureauReport.RunQuarterly;
                chkEditComparePrevious.Checked = bureauReport.CompareToPreviousPeriod;

                //BIDS
                foreach (int bid in bureauReport.BIDS ?? new int[0])
                {
                    var temp = lbEditBuildings.Items.FindByValue(bid.ToString());
                    if (temp != null)
                    {
                        temp.Selected = true;
                    }
                }                

                //Title
                txtEditTitle.Text = bureauReport.Title;                
                //Theme
                ddlEditTheme.SelectedValue = bureauReport.Theme.ToString();
                //Language
                ddlEditLanguage.SelectedValue = bureauReport.LanguageCultureName;
                //PageCulture
                ddlEditCulture.SelectedValue = bureauReport.PageLCID.ToString();

                chkEditShowClientLogos.Checked = bureauReport.ShowClientsLogos;
                chkEditIncludeCoverPage.Checked = bureauReport.IncludeCoverPage;
                chkEditIncludeContentsPage.Checked = bureauReport.IncludeTableOfContents;
                chkEditIncludeBuildingSummaryReport.Checked = bureauReport.IncludeBuildingSummaryReport;
                chkEditIncludeEquipmentClassSummaryReport.Checked = bureauReport.IncludeEquipmentClassSummaryReport;
                chkEditIncludeBuildingTrendsSummaryReport.Checked = bureauReport.IncludeBuildingTrendsSummaryReport;
                chkEditIncludeBuildingTopIssuesSummaryReport.Checked = bureauReport.IncludeDiagnosticIssuesSummaryReport;
                chkEditIncludeVentilationEquipmentReport.Checked = bureauReport.IncludeVentilationReport;
                ddlEditTopVentilation.SelectedValue = bureauReport.TopVentilation.ToString();
                chkEditIncludeHeatingEquipmentReport.Checked = bureauReport.IncludeHeatingReport;
                ddlEditTopHeating.SelectedValue = bureauReport.TopHeating.ToString();
                chkEditIncludeCoolingEquipmentReport.Checked = bureauReport.IncludeCoolingReport;
                ddlEditTopCooling.SelectedValue = bureauReport.TopCooling.ToString();
                chkEditIncludePlantEquipmentReport.Checked = bureauReport.IncludePlantReport;
                ddlEditTopPlant.SelectedValue = bureauReport.TopPlant.ToString();
                chkEditIncludeZoneEquipmentReport.Checked = bureauReport.IncludeZoneReport;
                ddlEditTopZone.SelectedValue = bureauReport.TopZone.ToString();
                chkEditIncludeSpecialtyEquipmentReport.Checked = bureauReport.IncludeSpecialtyReport;
                ddlEditTopSpecialty.SelectedValue = bureauReport.TopSpecialty.ToString();
                chkEditIncludePerformanceReport.Checked = bureauReport.IncludePerformanceReport;
                chkEditIncludeAlarmReport.Checked = bureauReport.IncludeAlarmReport;
                chkEditIncludeProjectsReport.Checked = bureauReport.IncludeProjectsReport;
                ddlEditTopProjects.SelectedValue = bureauReport.TopProjects.ToString();
                chkEditIncludeTasksReport.Checked = bureauReport.IncludeTasksReport;
                ddlEditTopTasks.SelectedValue = bureauReport.TopTasks.ToString();
                chkEditIncludeTopDiagnosticsDetails.Checked = bureauReport.IncludeTopWeeklyDiagnosticIssues;
                chkEditIncludeTopDiagnosticsFigures.Checked = bureauReport.IncludeTopWeeklyDiagnosticFigures;


                //UserGroups
                int clientOID = DataMgr.OrganizationDataMapper.GetOIDByCID(bureauReport.CID);
                IEnumerable<UserGroup> fullUserGroupSet = Enumerable.Empty<UserGroup>();
                List<int> assignedUserGroupIDs = new List<int>();
                assignedUserGroupIDs = DataMgr.UserGroupDataMapper.GetUserGroupIDsByGUID(bureauReport.GUID);

                //get all user groups for that client, and user org
                fullUserGroupSet = DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(clientOID, null);

                if(clientOID != bureauReport.PreparedByOID)
                    fullUserGroupSet = fullUserGroupSet.Union(DataMgr.UserGroupDataMapper.GetAllUserGroupsByOID(bureauReport.PreparedByOID, null));

                BindUserGroups(lbEditUserGroupsTop, fullUserGroupSet.Where(ug => !assignedUserGroupIDs.Contains(ug.UserGroupID)));
                BindUserGroups(lbEditUserGroupsBottom, fullUserGroupSet.Where(ug => assignedUserGroupIDs.Contains(ug.UserGroupID)));
            }

        #endregion

        #region Load and Bind Fields

            private void BindBuildings(ListBox lb, int clientID)
            {
                //clear just in case
                lb.Items.Clear();

                lb.DataSource = siteUser.VisibleBuildings;
                lb.DataTextField = "BuildingName";
                lb.DataValueField = "BID";
                lb.DataBind();
            }

            private void BindThemes(DropDownList ddl)
            {
                ddl.Items.Clear();

                Dictionary<int, string> themes = Common.Constants.BusinessConstants.Theme.ReportThemes;
                
                if(!siteUser.IsKGSFullAdminOrHigher && !siteUser.IsSchneiderTheme)
                    themes = themes.Where(t => t.Key != 1).ToDictionary(t => t.Key, t => t.Value);
                else if (!siteUser.IsKGSFullAdminOrHigher && siteUser.IsSchneiderTheme)
                    themes = themes.Where(t => t.Key != 0).ToDictionary(t => t.Key, t => t.Value);

                ddl.DataValueField = "Key";
                ddl.DataTextField = "Value";
                ddl.DataSource = themes;
				ddl.DataBind();
            }

            internal void BindLanguageCultures(DropDownList ddl)
            {
                var cultureItems = CultureHelper.AllowedContentCultures(CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.DisplayName).ToArray());

                ddl.DataTextField = "DisplayName";
                ddl.DataValueField = "Name";
                ddl.DataSource = cultureItems.Select(c => new { DisplayName = c.DisplayName, Name = c.Name });

                if(cultureItems.Contains(CultureHelper.GetCultureInfo(siteUser.CultureName)))
                {                   
                    ddl.SelectedValue = siteUser.CultureName;
                }
                else
                {
                    ddl.SelectedValue = Common.Constants.BusinessConstants.Culture.DefaultCultureName;
                }

                ddl.DataBind();
            }

            private void BindCultures(DropDownList ddl)
            {
                ddl.DataTextField = "DisplayName";
                ddl.DataValueField = "LCID";
                ddl.DataSource = CultureHelper.FilterUserCultures(CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.DisplayName).ToArray());
                ddl.SelectedValue = CultureHelper.GetCultureInfo(siteUser.CultureName).LCID.ToString();
                ddl.DataBind();
            }

            private void BindUserGroups(ListBox lb, IEnumerable<UserGroup> dataSource)
            {
                lb.Items.Clear();
                lb.DataTextField = "UserGroupName";
                lb.DataValueField = "UserGroupID";
                lb.DataSource = dataSource;
                lb.DataBind();
            }

        #endregion

        #region Load Bureau Report

            protected void LoadAddFormIntoBureauReport(ScheduledBureauReport bureauReport)
            {
                //Report Name
                bureauReport.ReportName = txtReportName.Text;
                //Report Description
                bureauReport.ReportDescription = String.IsNullOrEmpty(txtDescription.Value) ? null : txtDescription.Value;
                //Client
                bureauReport.CID = siteUser.CID;
                //Organization
                bureauReport.PreparedByOID = siteUser.UserOID;
                //GUID
                bureauReport.GUID = Guid.NewGuid();

                bureauReport.RunMonthly = chkRunMonthly.Checked;
                bureauReport.RunQuarterly = chkRunQuarterly.Checked;
                bureauReport.CompareToPreviousPeriod = chkComparePrevious.Checked;

                //BIDS
                List<int> bids = new List<int>();
                foreach (ListItem li in lbBuildings.Items)
                {
                    if (li.Selected)
                        bids.Add(Convert.ToInt32(li.Value));
                }
                bureauReport.BIDS = StringHelper.ParseIntArrayToCSVString(bids.ToArray());

                bureauReport.Title = txtUserTitle.Text;
                bureauReport.Theme = Convert.ToInt32(ddlTheme.SelectedValue);
                bureauReport.LanguageCultureName = ddlLanguage.SelectedValue;
                bureauReport.PageLCID = Convert.ToInt32(ddlCulture.SelectedValue);
                bureauReport.PreparedByUID = siteUser.UID;

                bureauReport.ShowClientLogos = chkShowClientLogos.Checked;
                bureauReport.IncludeCoverPage = chkIncludeCoverPage.Checked;
                bureauReport.IncludeTableOfContents = chkIncludeContentsPage.Checked;
                bureauReport.IncludeBuildingSummaryReport = chkIncludeBuildingSummaryReport.Checked;
                bureauReport.IncludeEquipmentClassSummaryReport = chkIncludeEquipmentClassSummaryReport.Checked;
                bureauReport.IncludeBuildingTrendsSummaryReport = chkIncludeBuildingTrendsSummaryReport.Checked;
                bureauReport.IncludeDiagnosticIssuesSummaryReport = chkIncludeBuildingTopIssuesSummaryReport.Checked;
                bureauReport.IncludeVentilationReport = chkIncludeVentilationEquipmentReport.Checked;
                bureauReport.TopVentilation = Convert.ToInt32(ddlTopVentilation.SelectedValue);
                bureauReport.IncludeHeatingReport = chkIncludeHeatingEquipmentReport.Checked;
                bureauReport.TopHeating = Convert.ToInt32(ddlTopHeating.SelectedValue);
                bureauReport.IncludeCoolingReport = chkIncludeCoolingEquipmentReport.Checked;
                bureauReport.TopCooling = Convert.ToInt32(ddlTopCooling.SelectedValue);
                bureauReport.IncludePlantReport = chkIncludePlantEquipmentReport.Checked;
                bureauReport.TopPlant = Convert.ToInt32(ddlTopPlant.SelectedValue);
                bureauReport.IncludeZoneReport = chkIncludeZoneEquipmentReport.Checked;
                bureauReport.TopZone = Convert.ToInt32(ddlTopZone.SelectedValue);
                bureauReport.IncludeSpecialtyReport = chkIncludeSpecialtyEquipmentReport.Checked;
                bureauReport.TopSpecialty = Convert.ToInt32(ddlTopSpecialty.SelectedValue);
                bureauReport.IncludePerformanceReport = chkIncludePerformanceReport.Checked;
                bureauReport.IncludeAlarmReport = chkIncludeAlarmReport.Checked;
                bureauReport.IncludeProjectsReport = chkIncludeProjectReport.Checked;
                bureauReport.TopProjects = Convert.ToInt32(ddlTopProjects.SelectedValue);
                bureauReport.IncludeTasksReport = chkIncludeTaskReport.Checked;
                bureauReport.TopTasks = Convert.ToInt32(ddlTopTasks.SelectedValue);

                bureauReport.IncludeTopWeeklyDiagnosticIssues = chkIncludeTopDiagnosticsDetails.Checked;
                bureauReport.IncludeTopWeeklyDiagnosticFigures = chkIncludeTopDiagnosticsFigures.Checked;

                bureauReport.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoBureauReport(ScheduledBureauReport bureauReport)
            {
                //ID
                bureauReport.SBRID = Convert.ToInt32(hdnEditID.Value);

                //Report Name
                bureauReport.ReportName = txtEditReportName.Text;
                //Report Description
                bureauReport.ReportDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;

                bureauReport.RunMonthly = chkEditRunMonthly.Checked;
                bureauReport.RunQuarterly = chkEditRunQuarterly.Checked;
                bureauReport.CompareToPreviousPeriod = chkEditComparePrevious.Checked;

                //BIDS
                List<int> bids = new List<int>();
                foreach (ListItem li in lbEditBuildings.Items)
                {
                    if (li.Selected)
                        bids.Add(Convert.ToInt32(li.Value));
                }
                bureauReport.BIDS = StringHelper.ParseIntArrayToCSVString(bids.ToArray());

                bureauReport.Title = txtEditTitle.Text;
                bureauReport.Theme = Convert.ToInt32(ddlEditTheme.SelectedValue);
                bureauReport.LanguageCultureName = ddlEditLanguage.SelectedValue;
                bureauReport.PageLCID = Convert.ToInt32(ddlEditCulture.SelectedValue);
                bureauReport.PreparedByUID = siteUser.UID;
                bureauReport.GUID = Guid.Parse(hdnEditGUID.Value);

                bureauReport.ShowClientLogos = chkEditShowClientLogos.Checked;
                bureauReport.IncludeCoverPage = chkEditIncludeCoverPage.Checked;
                bureauReport.IncludeTableOfContents = chkEditIncludeContentsPage.Checked;
                bureauReport.IncludeBuildingSummaryReport = chkEditIncludeBuildingSummaryReport.Checked;
                bureauReport.IncludeEquipmentClassSummaryReport = chkEditIncludeEquipmentClassSummaryReport.Checked;
                bureauReport.IncludeBuildingTrendsSummaryReport = chkEditIncludeBuildingTrendsSummaryReport.Checked;
                bureauReport.IncludeDiagnosticIssuesSummaryReport = chkEditIncludeBuildingTopIssuesSummaryReport.Checked;
                bureauReport.IncludeVentilationReport = chkEditIncludeVentilationEquipmentReport.Checked;
                bureauReport.TopVentilation = Convert.ToInt32(ddlEditTopVentilation.SelectedValue);
                bureauReport.IncludeHeatingReport = chkEditIncludeHeatingEquipmentReport.Checked;
                bureauReport.TopHeating = Convert.ToInt32(ddlEditTopHeating.SelectedValue);
                bureauReport.IncludeCoolingReport = chkEditIncludeCoolingEquipmentReport.Checked;
                bureauReport.TopCooling = Convert.ToInt32(ddlEditTopCooling.SelectedValue);
                bureauReport.IncludePlantReport = chkEditIncludePlantEquipmentReport.Checked;
                bureauReport.TopPlant = Convert.ToInt32(ddlEditTopPlant.SelectedValue);
                bureauReport.IncludeZoneReport = chkEditIncludeZoneEquipmentReport.Checked;
                bureauReport.TopZone = Convert.ToInt32(ddlEditTopZone.SelectedValue);
                bureauReport.IncludeSpecialtyReport = chkEditIncludeSpecialtyEquipmentReport.Checked;
                bureauReport.TopSpecialty = Convert.ToInt32(ddlEditTopSpecialty.SelectedValue);
                bureauReport.IncludePerformanceReport = chkEditIncludePerformanceReport.Checked;
                bureauReport.IncludeAlarmReport = chkEditIncludeAlarmReport.Checked;
                bureauReport.IncludeProjectsReport = chkEditIncludeProjectsReport.Checked;
                bureauReport.TopProjects = Convert.ToInt32(ddlEditTopProjects.SelectedValue);
                bureauReport.IncludeTasksReport = chkEditIncludeTasksReport.Checked;
                bureauReport.TopTasks = Convert.ToInt32(ddlEditTopTasks.SelectedValue);

                bureauReport.IncludeTopWeeklyDiagnosticIssues = chkEditIncludeTopDiagnosticsDetails.Checked;
                bureauReport.IncludeTopWeeklyDiagnosticFigures = chkEditIncludeTopDiagnosticsFigures.Checked;

                bureauReport.DateModified = DateTime.UtcNow;
            }

            #endregion

        #region Dropdown and Checkbox Events

            protected void ddlLanguage_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                GetAndSetReportContent(ddlLanguage.SelectedValue);
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Add bureau reports button on click.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void addBureauReportButton_Click(object sender, EventArgs e)
            {
                //check that report does not already exist for client by name??

                //if (!mDataManager.BuildingDataMapper.DoesScheduledBureauReportExistForClient(Convert.ToInt32(ddlAddClient.SelectedValue), null, txtAddBuildingName.Text))
                //{

                    mScheduledBureauReport = new ScheduledBureauReport();

                    //load the form into the scheduled bureau report
                    LoadAddFormIntoBureauReport(mScheduledBureauReport);

                    try
                    {
                        //insert new bureau report
                        DataMgr.ScheduledBureauReportsDataMapper.InsertScheduledBureauReport(mScheduledBureauReport);

                        LabelHelper.SetLabelMessage(lblAddError, mScheduledBureauReport.ReportName + addBureauReportSuccess, lnkSetFocusAdd);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error scheduling bureau report.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addBureauReportFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error scheduling bureau report.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addBureauReportFailed, lnkSetFocusAdd);
                    }

                    try
                    {
                        List<UserGroupCollectionLookup_UserGroup> ugs = new List<UserGroupCollectionLookup_UserGroup>();

                        foreach (ListItem item in lbUserGroupsBottom.Items)
                        {
                            UserGroupCollectionLookup_UserGroup ug = new UserGroupCollectionLookup_UserGroup { UserGroupID = Convert.ToInt32(item.Value), GUID = mScheduledBureauReport.GUID };
                            ugs.Add(ug);
                        }

                        DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups(ugs);

                        LabelHelper.SetLabelMessage(lblAddError, mScheduledBureauReport.ReportName + addBureauReportSuccess, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding scheduled bureau report usergroups.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addBureauReportFailed, lnkSetFocusAdd);                        
                    }
                //}
                //else
                //{
                //    //report exists
                //    LabelHelper.SetLabelMessage(lblAddError, bureauReportExists, lnkSetFocusAdd);
                //}
            }

            /// <summary>
            /// Update bureau reports button on click.
            /// </summary>
            protected void updateBureauReportButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the bulding
                var mScheduledBureauReport = new ScheduledBureauReport();
                LoadEditFormIntoBureauReport(mScheduledBureauReport);

                //try to update the bureau report              
                try
                {
                    //update bureau report 
                    DataMgr.ScheduledBureauReportsDataMapper.UpdateScheduledBureauReport(mScheduledBureauReport);

                    LabelHelper.SetLabelMessage(lblEditError, updateBureauReportSuccessful, lnkSetFocusEdit);

                    //Bind bureau reports again
                    BindBureauReports();
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateBureauReportFailed, lnkSetFocusEdit);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating scheduled bureau report.", ex);
                }

                try
                {
                    //delete
                    DataMgr.UserGroupDataMapper.DeleteUserGroupCollectionAssociationsByGuids(new Guid[] { mScheduledBureauReport.GUID });

                    List<UserGroupCollectionLookup_UserGroup> ugs = new List<UserGroupCollectionLookup_UserGroup>();

                    foreach (ListItem item in lbEditUserGroupsBottom.Items)
                    {
                        UserGroupCollectionLookup_UserGroup ug = new UserGroupCollectionLookup_UserGroup { UserGroupID = Convert.ToInt32(item.Value), GUID = mScheduledBureauReport.GUID };
                        ugs.Add(ug);
                    }

                    //reinsert all
                    DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups(ugs);

                    LabelHelper.SetLabelMessage(lblEditError, updateBureauReportSuccessful, lnkSetFocusEdit);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateBureauReportFailed, lnkSetFocusEdit);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating scheduled bureau report user groups.", ex);
                }
            }


            /// <summary>
            /// on button up click, remove user groups from bottom listbox
            /// </summary>
            protected void btnUserGroupsUpButton_Click(object sender, EventArgs e)
            {
                while (lbUserGroupsBottom.SelectedIndex != -1)
                {
                    lbUserGroupsTop.Items.Add(lbUserGroupsBottom.SelectedItem);
                    lbUserGroupsBottom.Items.Remove(lbUserGroupsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add user groups to bottom listbox
            /// </summary>
            protected void btnUserGroupsDownButton_Click(object sender, EventArgs e)
            {
                while (lbUserGroupsTop.SelectedIndex != -1)
                {
                    lbUserGroupsBottom.Items.Add(lbUserGroupsTop.SelectedItem);
                    lbUserGroupsTop.Items.Remove(lbUserGroupsTop.SelectedItem);
                }
            }

            /// <summary>
            /// on button up click, remove user groups from bottom listbox
            /// </summary>
            protected void btnEditUserGroupsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditUserGroupsBottom.SelectedIndex != -1)
                {
                    lbEditUserGroupsTop.Items.Add(lbEditUserGroupsBottom.SelectedItem);
                    lbEditUserGroupsBottom.Items.Remove(lbEditUserGroupsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add user groups to bottom listbox
            /// </summary>
            protected void btnEditUserGroupsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditUserGroupsTop.SelectedIndex != -1)
                {
                    lbEditUserGroupsBottom.Items.Add(lbEditUserGroupsTop.SelectedItem);
                    lbEditUserGroupsTop.Items.Remove(lbEditUserGroupsTop.SelectedItem);
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindBureauReports();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindBureauReports();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind bureau grid after tab changed back from add bureau report tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 1)
                {
                    BindBureauReports();
                }
            }

        #endregion

        #region Grid Events

            protected void gridBureauReports_OnDataBound(object sender, EventArgs e)
            {                                      
            }

            protected void gridBureauReports_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditBureauReport.Visible = false;
                dtvBureauReport.Visible = true;

                int sbrid = Convert.ToInt32(gridBureauReports.DataKeys[gridBureauReports.SelectedIndex].Values["SBRID"]);

                GetScheduledBureauReportData sbr = DataMgr.ScheduledBureauReportsDataMapper.GetScheduledBureauReportByID(sbrid);

                //set data source
                dtvBureauReport.DataSource = new List<GetScheduledBureauReportData>() { sbr }; ;
                //bind bureau report to details view
                dtvBureauReport.DataBind();
            }

            protected void gridBureauReports_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridBureauReports_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvBureauReport.Visible = false;
                pnlEditBureauReport.Visible = true;

                int sbrid = Convert.ToInt32(gridBureauReports.DataKeys[e.NewEditIndex].Values["SBRID"]);

                GetScheduledBureauReportData sbr = DataMgr.ScheduledBureauReportsDataMapper.GetScheduledBureauReportByID(sbrid);

                //Set bureaureport data
                SetBureauReportIntoEditForm(sbr);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridBureauReports_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows sbrid
                int sbrid = Convert.ToInt32(gridBureauReports.DataKeys[e.RowIndex].Value);

                try
                {
                    // deletes bureau report.
                    DataMgr.ScheduledBureauReportsDataMapper.DeleteScheduledBureauReport(sbrid);

                    lblErrors.Text = deleteSuccessful;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    pnlEditBureauReport.Visible = false;
                    dtvBureauReport.Visible = false;
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting scheduled bureau report.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryBureauReports());
                gridBureauReports.PageIndex = gridBureauReports.PageIndex;
                gridBureauReports.DataSource = SortDataTable(dataTable as DataTable, true);
                gridBureauReports.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);
            }

            protected void gridBureauReports_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBureauReports(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridBureauReports.DataSource = SortDataTable(dataTable, true);
                gridBureauReports.PageIndex = e.NewPageIndex;
                gridBureauReports.DataBind();
            }

            protected void gridBureauReports_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridBureauReports.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBureauReports(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridBureauReports.DataSource = SortDataTable(dataTable, false);
                gridBureauReports.DataBind();
            }

            #endregion

        #region Helper Methods

            private IEnumerable<GetScheduledBureauReportData> QueryBureauReports()
            {
                try
                {
                    //get all bureau reports 
                    return siteUser.IsKGSFullAdminOrHigher ? DataMgr.ScheduledBureauReportsDataMapper.GetAllScheduledBureauReportsByCID(siteUser.CID) : DataMgr.ScheduledBureauReportsDataMapper.GetAllScheduledBureauReportsByOIDForCID(siteUser.UserOID, siteUser.CID);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving scheduled bureau reports in bureau reports v2 administration.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving scheduled bureau reports in bureau reports v2 administration.", ex);
                    return null;
                }
            }

            private IEnumerable<GetScheduledBureauReportData> QuerySearchedBureauReports(string searchText)
            {
                try
                {
                    //get all searched bureau reports 
                    return siteUser.IsKGSFullAdminOrHigher ? DataMgr.ScheduledBureauReportsDataMapper.GetAllSearchedScheduledBureauReportsByCID(searchText, siteUser.CID) : DataMgr.ScheduledBureauReportsDataMapper.GetAllSearchedScheduledBureauReportsByOIDForCID(searchText, siteUser.UserOID, siteUser.CID);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving searched scheduled bureau reports in bureau reports v2 administration.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving searched scheduled bureau reports in bureau reports v2 administration.", ex);
                    return null;
                }
            }

            private void BindBureauReports()
            {
                IEnumerable<GetScheduledBureauReportData> bureauReports;

                //query bureau reports
                if (!String.IsNullOrWhiteSpace(txtSearch.Text))
                {
                    bureauReports = QuerySearchedBureauReports(txtSearch.Text);
                }
                else
                {
                    bureauReports = QueryBureauReports();
                }                

                int count = bureauReports.Count();

                gridBureauReports.DataSource = bureauReports;

                //bind grid
                gridBureauReports.DataBind();

                //set count label
                SetGridCountLabel(count);

                //set search visibility. dont remove when last result from searched results in the grid is deleted.
                pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && count == 0) ? false : true;
            }
      
            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} bureau reports found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} bureau reports found.", count);
                }
                else
                {
                    lblResults.Text = "No bureau reports found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            private void SetEnabledCheckboxes()
            {
                //chkIncludePerformanceReport.Enabled = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.ModuleConstants.Module.PerformanceDashboard));
                chkIncludeAlarmReport.Enabled = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Alarms));
                chkIncludeProjectReport.Enabled = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Projects));
                chkIncludeTaskReport.Enabled = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Tasks));
            }

            private void GetAndSetReportContent(string cultureName)
            {
                //get content            
                mContent = DataMgr.ContentDataMapper.GetSafeContentSections(new int[] { 2 }, cultureName);

                editorAutomatedSummary.Content = new ContentHelper(LogMgr).GetSafeContentItem(mContent, "AutomatedSummaryDefaultValue", this.GetType().Name);
            }

            private void SetHiddenFields()
            {
                hdnUID.Value = siteUser.UID.ToString();
                hdnCID.Value = siteUser.CID.ToString();
                hdnOID.Value = siteUser.UserOID.ToString();
                hdnUserCulture.Value = siteUser.CultureName;
            }


         #endregion
    }
}
