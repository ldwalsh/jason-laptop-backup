﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemReports.aspx.cs" Inherits="CW.Website.SystemReports" %>
<%@ Register tagPrefix="CW" tagName="UserAuditSystemDownloadArea" src="~/_controls/admin/Users/UserAuditSystemDownloadArea.ascx" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                 	                  	        	
        	<h1>System Reports</h1>                        
            <div class="richText">The system reports page provides a current snapshot of active total relational information within clockworks.</div>
            <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>                                       
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
            </div> 
            <div class="administrationControls">
                    <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                    <Tabs>
                        <telerik:RadTab Text="System Statistics"></telerik:RadTab>
                        <telerik:RadTab Text="Users Audit"></telerik:RadTab>
                        <telerik:RadTab Text="Buildings Audit"></telerik:RadTab>
                        <telerik:RadTab Text="Points Audit"></telerik:RadTab>
                        <telerik:RadTab Text="Storage Statistics"></telerik:RadTab>
                    </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                        <telerik:RadPageView ID="RadPageView1" runat="server">
                            <h2>Current Total Active System Statistics</h2>
                            <div class="divForm">
                                <label class="label">Active Organizations:</label>                        
                                <asp:Label ID="lblOrganizations" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Active Providers:</label>                        
                                <asp:Label ID="lblProviders" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Active Clients:</label>                        
                                <asp:Label ID="lblClients" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Active Buildings:</label>                        
                                <asp:Label ID="lblBuildings" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Active Data Sources:</label>                        
                                <asp:Label ID="lblDataSources" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>     
                            <div class="divForm">
                                <label class="label">Active Equipment:</label>                        
                                <asp:Label ID="lblEquipment" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Active Equipment Point Associations:</label>                        
                                <asp:Label ID="lblEquipmentPoints" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div> 
                            <div class="divForm">
                                <label class="label">Active Points:</label>                        
                                <asp:Label ID="lblPoints" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div> 
                            <div class="divForm">
                                <label class="label">Active Alarm Points:</label>                        
                                <asp:Label ID="lblAlarmPoints" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>   
                            <div class="divForm">
                                <label class="label">Active Scheduled Analyses:</label>                        
                                <asp:Label ID="lblScheduledAnalyses" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Active Users:</label>                        
                                <asp:Label ID="lblUsers" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div> 
                            <div class="divForm">
                                <label class="label">Active Retrieval Methods:</label>                        
                                <asp:Label ID="lblRetrievalMethods" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Active Data Transfer Services:</label>                        
                                <asp:Label ID="lblServices" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Total Countries:</label>                        
                                <asp:Label ID="lblTotalCountries" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div> 
                            <div class="divForm">
                                <label class="label">Total Analysis Builder Views:</label>                        
                                <asp:Label ID="lblTotalViews" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                            <div class="divForm">
                                <label class="label">Total Data Points Per Hour:</label>                        
                                <asp:Label ID="lblTotalDataPoints" CssClass="labelContent" runat="server"></asp:Label>                                                    
                            </div>
                        </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server"> 
                                <CW:TabHeader runat="server" ID="tabHeaderAudit" Title="Users Audit">
                                  <RightAreaTemplate>
                                    <CW:UserAuditSystemDownloadArea runat="server" ID="UserAuditSystemDownloadArea" />
                                  </RightAreaTemplate>
                                </CW:TabHeader>

                                <p>
                                    <a id="lnkSetFocusView" href="#" runat="server"></a>
                                    <asp:Label ID="lblUsersAuditResults" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblUsersAuditErrors" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>
                                </p>
                                <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridUsersAudit"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="AuditID,UID"  
                                             GridLines="None"                                      
                                             PageSize="100" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridUsersAudit_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridUsersAudit_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridUsersAudit_Sorting"   
                                             OnDataBound="gridUsersAudit_OnDataBound"
                                             AutoGenerateColumns="false"                                               
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>                                                                                                 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Email" HeaderText="Email">                                                                                              
                                                    <ItemTemplate><label title="<%# Eval("Email") %>"><%# StringHelper.TrimText(Eval("Email"),25) %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>                                                  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FirstName" HeaderText="First Name">                                                      
                                                    <ItemTemplate><label title="<%# Eval("FirstName") %>"><%# StringHelper.TrimText(Eval("FirstName"),10) %></label></ItemTemplate>                                                                      
                                                </asp:TemplateField>    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="LastName" HeaderText="Last Name">  
                                                    <ItemTemplate><label title="<%# Eval("LastName") %>"><%# StringHelper.TrimText(Eval("LastName"),15) %></label></ItemTemplate>                                    
                                                </asp:TemplateField>                                                 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="OrganizationName" HeaderText="Organization Name">  
                                                    <ItemTemplate><label title="<%# Eval("OrganizationName") %>"><%# StringHelper.TrimText(Eval("OrganizationName"),20) %></label></ItemTemplate>                                    
                                                </asp:TemplateField>                                                   
                                                <asp:BoundField SortExpression="LoginSuccessTotalCount" HeaderText="Login Count" DataField="LoginSuccessTotalCount" DataFormatString="{0:D}" />
                                                <asp:BoundField SortExpression="LoginFailedAttemptCount" HeaderText="Failed Count" DataField="LoginFailedAttemptCount" DataFormatString="{0:D}" />
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="IsActive" HeaderText="Active">  
                                                    <ItemTemplate><%# Convert.ToBoolean(Eval("IsActive")) %></ItemTemplate>                                    
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="IsLocked" HeaderText="Locked">  
                                                    <ItemTemplate><%# Convert.ToBoolean(Eval("IsLocked")) %></ItemTemplate>                                    
                                                </asp:TemplateField> 
                                                <asp:BoundField SortExpression="DateCreated" HeaderText="User Created" DataField="DateCreated" DataFormatString="{0:g}" />
                                                <asp:BoundField SortExpression="DateModified" HeaderText="Last Login Attempt" DataField="DateModified" DataFormatString="{0:g}" />                                                                                                                                                                                                                  
                                             </Columns>        
                                         </asp:GridView>
                                    </div>
                         </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView3" runat="server"> 
                                <h2>Buildings Audit</h2> 
                                <p>
                                    <a id="lnkBuildingsSetFocusView" href="#" runat="server"></a>
                                    <asp:Label ID="lblBuildingsAuditResults" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblBuildingsAuditErrors" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>
                                </p>
                                <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridBuildingsAudit"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="BID"  
                                             GridLines="None"                                      
                                             PageSize="100" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridBuildingsAudit_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridBuildingsAudit_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridBuildingsAudit_Sorting"   
                                             OnDataBound="gridBuildingsAudit_OnDataBound"
                                             AutoGenerateColumns="false"                                               
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client Name">                                                      
                                                    <ItemTemplate><label title="<%# Eval("ClientName") %>"><%# StringHelper.TrimText(Eval("ClientName"),25) %></label></ItemTemplate>                                                                      
                                                </asp:TemplateField>                             
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building Name">                                                                                              
                                                    <ItemTemplate><label title="<%# Eval("BuildingName") %>"><%# StringHelper.TrimText(Eval("BuildingName"),30) %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>        
                                                 <asp:TemplateField ItemStyle-Wrap="true" SortExpression="CountryName" HeaderText="CountryName">  
                                                    <ItemTemplate><label title="<%# Eval("CountryName") %>"><%# StringHelper.TrimText(Eval("CountryName"), 15) %></label></ItemTemplate>                                    
                                                </asp:TemplateField>                                          
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Address" HeaderText="Address">  
                                                    <ItemTemplate><label title="<%# Eval("Address") %>"><%# StringHelper.TrimText(Eval("Address"), 30) %></label></ItemTemplate>                                    
                                                </asp:TemplateField>                                                 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="City" HeaderText="City">  
                                                    <ItemTemplate><label title="<%# Eval("City") %>"><%# StringHelper.TrimText(Eval("City"), 15) %></label></ItemTemplate>                                    
                                                </asp:TemplateField>                                                                                                  
                                                <asp:BoundField SortExpression="Sqft" HeaderText="Sqft" DataField="Sqft" DataFormatString="{0:D}" />
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="IsActive" HeaderText="Active">  
                                                    <ItemTemplate><%# Convert.ToBoolean(Eval("IsActive")) %></ItemTemplate>                                    
                                                </asp:TemplateField>   
                                                <asp:BoundField SortExpression="DateCreated" HeaderText="Date Created" DataField="DateCreated" DataFormatString="{0:g}" />                                                                                                                                                                                                                  
                                             </Columns>        
                                         </asp:GridView>
                                    </div>
                         </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView4" runat="server"> 
                                <h2>Points Audit</h2> 
                                <div class="richText">The points audit displays counts of unique points and equipment for active clients, buildings, equipment, points, and datasources.</div>
                                <p>
                                    <a id="lnkPointsSetFocusView" href="#" runat="server"></a>
                                    <asp:Label ID="lblPointsAuditResults" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblPointsAuditErrors" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>
                                </p>
                                <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridPointsAudit"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="CID"  
                                             GridLines="None"                                      
                                             PageSize="1000" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridPointsAudit_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridPointsAudit_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridPointsAudit_Sorting"   
                                             OnDataBound="gridPointsAudit_OnDataBound"
                                             AutoGenerateColumns="false"                                               
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client Name">                                                      
                                                    <ItemTemplate><label title="<%# Eval("ClientName") %>"><%# StringHelper.TrimText(Eval("ClientName"),35) %></label></ItemTemplate>                                                                      
                                                </asp:TemplateField>       
                                                 <asp:TemplateField ItemStyle-Wrap="true" SortExpression="CountryName" HeaderText="Country Name">                                                      
                                                    <ItemTemplate><label title="<%# Eval("CountryName") %>"><%# StringHelper.TrimText(Eval("CountryName"),25) %></label></ItemTemplate>                                                                      
                                                </asp:TemplateField>  
                                                 <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ReportThemeString" HeaderText="Report Theme">                                                      
                                                    <ItemTemplate><label title="<%# Eval("ReportThemeString") %>"><%# StringHelper.TrimText(Eval("ReportThemeString"),20) %></label></ItemTemplate>                                                                      
                                                </asp:TemplateField>       
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentCount" HeaderText="Equipment Count">                                                                                              
                                                    <ItemTemplate><label title="<%# Eval("EquipmentCount") %>"><%# Eval("EquipmentCount") %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>        
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="MajorPointCount" HeaderText="Major Point Count">                                                                                              
                                                    <ItemTemplate><label title="<%# Eval("MajorPointCount") %>"><%# Eval("MajorPointCount") %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>        
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="MinorPointCount" HeaderText="Minor Point Count">                                                                                              
                                                    <ItemTemplate><label title="<%# Eval("MinorPointCount") %>"><%# Eval("MinorPointCount") %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>  
						                        <asp:TemplateField ItemStyle-Wrap="true" SortExpression="AlarmPointCount" HeaderText="Alarm Point Count">                                                                                              
                                                    <ItemTemplate><label title="<%# Eval("AlarmPointCount") %>"><%# Eval("AlarmPointCount") %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>                                                                                                                                                                                               
                                             </Columns>        
                                         </asp:GridView>
                                    </div>
                         </telerik:RadPageView>                        
                        <telerik:RadPageView ID="RadPageView5" runat="server"> 
                            <h2>Storage Statistics</h2> 
                            <asp:Repeater ID="rptQueueCounts" runat="server">
                                <HeaderTemplate>                                                   
                                    <h3>Queue Counts</h3>
                                </HeaderTemplate>
                                <ItemTemplate>    
                                    <div class="divForm">
                                        <label class="label"><%# Eval("DisplayName") %>:</label>
                                        <span class="labelContent"><%# Eval("Count") %></span>  
                                    </div>                                                                                                                                                                                                                                        
                                </ItemTemplate>
                                <FooterTemplate>
                                </FooterTemplate>
                            </asp:Repeater>
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                </div>
           
</asp:Content>                