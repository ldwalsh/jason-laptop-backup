﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.DataSourceVendorContact;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSDataSourceVendorContactAdministration: SitePage
    {
        #region Properties

        private DataSourceVendorContact mDataSourceVendorContact;

        const string addDataSourceVendorContactSuccess = " data source vendor contact addition was successful.";
        const string addDataSourceVendorContactFailed = "Adding data source vendor contact failed. Please contact an administrator.";
        const string updateSuccessful = "Data source vendor contact update was successful.";
        const string updateFailed = "Data source vendor contact update failed. Please contact an administrator.";
        const string deleteSuccessful = "Data source vendor contact deletion was successful.";
        const string deleteFailed = "Data source vendor contact deletion failed. Please contact an administrator.";
        const string initialSortDirection = "ASC";
        const string initialSortExpression = "VendorName";

        //gets and sets the gridview viewstate sort direction for the dataview
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? string.Empty; }
            set { ViewState["SortDirection"] = value; }
        }

        //gets and sets the gridview viewstate sort expression for the dataview
        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindDataSourceVendorContacts();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindVendors(ddlEditVendor, ddlAddVendor);
                    BindCountries(ddlEditCountry, ddlAddCountry);

                    sessionState["Search"] = String.Empty;

                    //if page not postback and cid (contactID) querystring value exists try to set selected contact
                    if (!String.IsNullOrEmpty(Request.QueryString["cid"]))
                    {
                        try
                        {
                            //set data source vendor contact
                            dtvDataSourceVendorContact.DataSource = DataMgr.DataSourceVendorContactDataMapper.GetFullDataSourceVendorContactByID(Convert.ToInt32(Request.QueryString["cid"].ToString()));
                            //bind data source vendor to details view
                            dtvDataSourceVendorContact.DataBind();
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error getting querystring vendor contact.", ex);
                        }
                    }
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetDataSourceVendorContactIntoEditForm(DataSourceVendorContact dataSourceVendorContact)
            {
                //ID
                hdnEditID.Value = Convert.ToString(dataSourceVendorContact.ContactID);
                //DataSourceVendorContact Name
                txtEditName.Text = dataSourceVendorContact.Name;
                //Phone
                txtEditPhone.Text = dataSourceVendorContact.Phone;
                //Email
                txtEditEmail.Text = dataSourceVendorContact.Email;
                //Notes
                txtEditNotes.Value = dataSourceVendorContact.Notes;
                //Address
                txtEditAddress.Text = dataSourceVendorContact.Address;
                //City
                txtEditCity.Text = dataSourceVendorContact.City;
                //Country Alpha2Code
                try
                {
                    ddlEditCountry.SelectedValue = dataSourceVendorContact.CountryAlpha2Code;

                    ddlEditCountry_OnSelectedIndexChanged(null, null);

                    //set zip validators accordingly
                    editDataSourceVendorContactZipRegExValidator.Enabled = dataSourceVendorContact.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                    txtEditZip.Mask = dataSourceVendorContact.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa"; 

                    //StateID
                    try
                    {
                        ddlEditState.SelectedValue = Convert.ToString(dataSourceVendorContact.StateID);
                    }
                    catch
                    {
                    }
                }
                catch
                {
                }
                //ZIP
                txtEditZip.Text = dataSourceVendorContact.Zip;
                //Vendor
                ddlEditVendor.SelectedValue = Convert.ToString(dataSourceVendorContact.VendorID);
            }

        #endregion

        #region Load and Bind Fields

            private void BindVendors(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<DataSourceVendor> vendors = DataMgr.DataSourceVendorDataMapper.GetAllVendors();

                ddl.DataTextField = "VendorName";
                ddl.DataValueField = "VendorID";
                ddl.DataSource = vendors;
                ddl.DataBind();

                ddl2.DataTextField = "VendorName";
                ddl2.DataValueField = "VendorID";
                ddl2.DataSource = vendors;
                ddl2.DataBind();
            }

            private void BindCountries(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<Country> countries = DataMgr.CountryDataMapper.GetAllCountries();

                ddl.DataTextField = "CountryName";
                ddl.DataValueField = "Alpha2Code";
                ddl.DataSource = countries;
                ddl.DataBind();

                ddl2.DataTextField = "CountryName";
                ddl2.DataValueField = "Alpha2Code";
                ddl2.DataSource = countries;
                ddl2.DataBind();
            }

            private void BindStates(DropDownList ddl, string alpha2Code)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code);
                ddl.DataBind();
            }

        #endregion

        #region Load Data Source Vendor Contact

            protected void LoadAddFormIntoDataSourceVendorContact(DataSourceVendorContact dataSourceVendorContact)
            {
                //DataSourceVendorContact Name
                dataSourceVendorContact.Name = txtAddName.Text;
                //Phone
                dataSourceVendorContact.Phone = String.IsNullOrEmpty(txtAddPhone.Text) ? null : txtAddPhone.Text; 
                //Email
                dataSourceVendorContact.Email = String.IsNullOrEmpty(txtAddEmail.Text) ? null : txtAddEmail.Text; 
                //Notes
                dataSourceVendorContact.Notes = String.IsNullOrEmpty(txtAddNotes.Value) ? null : txtAddNotes.Value; 
                //Address
                dataSourceVendorContact.Address = String.IsNullOrEmpty(txtAddAddress.Text) ? null : txtAddAddress.Text;
                //City
                dataSourceVendorContact.City = String.IsNullOrEmpty(txtAddCity.Text) ? null : txtAddCity.Text;
                //CountryAlpha2Code
                dataSourceVendorContact.CountryAlpha2Code = ddlAddCountry.SelectedValue != "-1" ? ddlAddCountry.SelectedValue : null;
                //StateID
                dataSourceVendorContact.StateID = ddlAddState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddState.SelectedValue) : null;
                //ZIP
                dataSourceVendorContact.Zip = String.IsNullOrEmpty(txtAddZip.Text) ? null : txtAddZip.Text; 
                //Vendor Product
                dataSourceVendorContact.VendorID = Convert.ToInt32(ddlAddVendor.SelectedValue);

                dataSourceVendorContact.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoDataSourceVendorContact(DataSourceVendorContact dataSourceVendorContact)
            {
                //ID
                dataSourceVendorContact.ContactID = Convert.ToInt32(hdnEditID.Value);
                //DataSourceVendor Name
                dataSourceVendorContact.Name = txtEditName.Text;
                //Phone
                dataSourceVendorContact.Phone = String.IsNullOrEmpty(txtEditPhone.Text) ? null : txtEditPhone.Text;
                //Email
                dataSourceVendorContact.Email = String.IsNullOrEmpty(txtEditEmail.Text) ? null : txtEditEmail.Text; 
                //Notes
                dataSourceVendorContact.Notes = String.IsNullOrEmpty(txtEditNotes.Value) ? null : txtEditNotes.Value; 
                //Address
                dataSourceVendorContact.Address = String.IsNullOrEmpty(txtEditAddress.Text) ? null : txtEditAddress.Text;
                //City
                dataSourceVendorContact.City = String.IsNullOrEmpty(txtEditCity.Text) ? null : txtEditCity.Text;
                //CountryAlpha2Code
                dataSourceVendorContact.CountryAlpha2Code = ddlEditCountry.SelectedValue != "-1" ? ddlEditCountry.SelectedValue : null;
                //StateID
                dataSourceVendorContact.StateID = ddlEditState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditState.SelectedValue) : null;
                //ZIP
                dataSourceVendorContact.Zip = String.IsNullOrEmpty(txtEditZip.Text) ? null : txtEditZip.Text; 
                //VendorProductID
                dataSourceVendorContact.VendorID = Convert.ToInt32(ddlEditVendor.SelectedValue);
            }

        #endregion

        #region Dropdown Events

            protected void ddlAddCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlAddState, ddlAddCountry.SelectedValue);

                addDataSourceVendorContactZipRegExValidator.Enabled = ddlAddCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtAddZip.Mask = ddlAddCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            }
            protected void ddlEditCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlEditState, ddlEditCountry.SelectedValue);

                editDataSourceVendorContactZipRegExValidator.Enabled = ddlEditCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtEditZip.Mask = ddlEditCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Add data source vendor contact Button on click.
            /// </summary>
            protected void addDataSourceVendorContactButton_Click(object sender, EventArgs e)
            {
                mDataSourceVendorContact = new DataSourceVendorContact();

                //load the form into the datasource vendor contact
                LoadAddFormIntoDataSourceVendorContact(mDataSourceVendorContact);

                try
                {
                    //insert new data source vendor contact 
                    DataMgr.DataSourceVendorContactDataMapper.InsertContact(mDataSourceVendorContact);
                    LabelHelper.SetLabelMessage(lblAddError, mDataSourceVendorContact.Name + addDataSourceVendorContactSuccess, lnkSetFocusAdd);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding data source vendor contact.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addDataSourceVendorContactFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding data source vendor contact.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addDataSourceVendorContactFailed, lnkSetFocusAdd);
                }
            }


            /// <summary>
            /// Update Data source vendor contact Button on click.
            /// </summary>
            protected void updateDataSourceVendorContactButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the data source vendor
                DataSourceVendorContact mDataSourceVendorContact = new DataSourceVendorContact();
                LoadEditFormIntoDataSourceVendorContact(mDataSourceVendorContact);

                //try to update the data source vendor contact           
                try
                {
                    DataMgr.DataSourceVendorContactDataMapper.UpdateContact(mDataSourceVendorContact);

                    LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                    BindDataSourceVendorContacts();

                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating data source vendor contact.", ex);
                }
            }


            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindDataSourceVendorContacts();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindDataSourceVendorContacts();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind buildings grid after tab changed back from add buidling tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindDataSourceVendorContacts();
                }
            }

        #endregion

        #region Grid Events

            protected void gridDataSourceVendorContacts_OnDataBound(object sender, EventArgs e)
            {
                //check if kgs admin or higher, hide edit and delete column if not
                //bool kgsAdmin = RoleHelper.IsKGSAdminOrHigher(Session["UserRoleID"].ToString());
                //gridDataSourceVendors.Columns[1].Visible = kgsAdmin;
                //gridDataSourceVendors.Columns[4].Visible = kgsAdmin;
            }

            protected void gridDataSourceVendorContacts_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditDataSourceVendorContact.Visible = false;
                dtvDataSourceVendorContact.Visible = true;
                
                int dataSourceVendorContactID = Convert.ToInt32(gridDataSourceVendorContacts.DataKeys[gridDataSourceVendorContacts.SelectedIndex].Values["ContactID"]);

                //set data source vendor contact
                dtvDataSourceVendorContact.DataSource = DataMgr.DataSourceVendorContactDataMapper.GetFullDataSourceVendorContactByID(dataSourceVendorContactID);
                //bind data source vendor to details view
                dtvDataSourceVendorContact.DataBind();
            }

            protected void gridDataSourceVendorContacts_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridDataSourceVendorContacts_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvDataSourceVendorContact.Visible = false;
                pnlEditDataSourceVendorContact.Visible = true;

                int dataSourceVendorContactID = Convert.ToInt32(gridDataSourceVendorContacts.DataKeys[e.NewEditIndex].Values["ContactID"]);

                DataSourceVendorContact mDataSourceVendorContact = DataMgr.DataSourceVendorContactDataMapper.GetContact(dataSourceVendorContactID);

                //Set data source vendor contact data
                SetDataSourceVendorContactIntoEditForm(mDataSourceVendorContact);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridDataSourceVendorContacts_Deleting(object sender, GridViewDeleteEventArgs e)
             {
                 //get deleting rows datasource vendor contact id
                 int dataSourceVendorContactID = Convert.ToInt32(gridDataSourceVendorContacts.DataKeys[e.RowIndex].Value);

                 try
                 {
                     //remove all datasource vendor contacts assigned to a datasource  
                     DataMgr.DataSourceDataMapper.DeleteDataSourceVendorContactAssociations(dataSourceVendorContactID);

                     DataMgr.DataSourceVendorContactDataMapper.DeleteDataSourceVendorContact(dataSourceVendorContactID);

                     lblErrors.Text = deleteSuccessful;
                     lblErrors.Visible = true;
                     lnkSetFocusView.Focus();

                     pnlEditDataSourceVendorContact.Visible = false;
                     dtvDataSourceVendorContact.Visible = false;

                 }
                 catch (Exception ex)
                 {
                     lblErrors.Text = deleteFailed;
                     lblErrors.Visible = true;
                     lnkSetFocusView.Focus();

                     LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting data source vendor.", ex);
                 }
                 
                 //Bind data again keeping current page and sort mode
                 DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryDataSourceVendorContacts());
                 gridDataSourceVendorContacts.PageIndex = gridDataSourceVendorContacts.PageIndex;
                 gridDataSourceVendorContacts.DataSource = SortDataTable(dataTable as DataTable, true);
                 gridDataSourceVendorContacts.DataBind();

                 SetGridCountLabel(dataTable.Rows.Count);

                 //hide edit form if shown
                 pnlEditDataSourceVendorContact.Visible = false;  
             }

            protected void gridDataSourceVendorContacts_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedDataSourceVendorContacts(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridDataSourceVendorContacts.DataSource = SortDataTable(dataTable, true);
                gridDataSourceVendorContacts.PageIndex = e.NewPageIndex;
                gridDataSourceVendorContacts.DataBind();
            }

            protected void gridDataSourceVendorContacts_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridDataSourceVendorContacts.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedDataSourceVendorContacts(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridDataSourceVendorContacts.DataSource = SortDataTable(dataTable, false);
                gridDataSourceVendorContacts.DataBind();
            }

        #endregion

        #region Helper Methods

            private IEnumerable<GetDataSourceVendorContactsData> QueryDataSourceVendorContacts()
            {
                try
                {
                    //get all data sources vendors contacts
                    return DataMgr.DataSourceVendorContactDataMapper.GetAllDataSourceVendorContactsWithPartialData();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor contacts.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor contacts.", ex);
                    return null;
                }
            }

            private IEnumerable<GetDataSourceVendorContactsData> QuerySearchedDataSourceVendorContacts(string searchText)
            {
                try
                {
                    //get all data sources vendors contacts
                    return DataMgr.DataSourceVendorContactDataMapper.GetAllSearchedDataSourceVendorContactsWithPartialData(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor contacts.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendor contacts.", ex);
                    return null;
                }
            }

            private void BindDataSourceVendorContacts()
            {
                //query data source vendor contacts
                IEnumerable<GetDataSourceVendorContactsData> dataSourceVendorContacts = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryDataSourceVendorContacts() : QuerySearchedDataSourceVendorContacts(txtSearch.Text);

                int count = dataSourceVendorContacts.Count();

                gridDataSourceVendorContacts.DataSource = dataSourceVendorContacts;

                // bind grid
                gridDataSourceVendorContacts.DataBind();

                SetGridCountLabel(count);

                //set search visibility. dont remove when last result from searched results in the grid is deleted.
                pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && count == 0) ? false : true;
            }

            //private void BindSearchedDataSourceVendorContacts(string[] searchText)
            //{
            //    //query data source vendor contacts
            //    IEnumerable<GetDataSourceVendorContactsData> dataSourceVendorContacts = QuerySearchedDataSourceVendorContacts(searchText);

            //    int count = dataSourceVendorContacts.Count();

            //    gridDataSourceVendorContacts.DataSource = dataSourceVendorContacts;

            //    // bind grid
            //    gridDataSourceVendorContacts.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} contact found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} contacts found.", count);
                }
                else
                {
                    lblResults.Text = "No contacts found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }         

        #endregion
    }
}

