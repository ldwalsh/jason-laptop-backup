﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemBMSInfoAdministration.aspx.cs" Inherits="CW.Website.SystemBMSInfoAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/BMSInfo/ViewBMSInfo.ascx" tagname="ViewBMSInfo" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/BMSInfo/AddBMSInfo.ascx" tagname="AddBMSInfo" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">                	                  	
    
      <h1>System BMS Information Administration</h1>

      <div class="richText">The system administration area is used to view, add, and edit BMS information used for raw data extraction.</div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>                                       
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div> 
       
      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" CausesValidation="false" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View BMS Info" />
            <telerik:RadTab Text="Add BMS Info" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
            <telerik:RadPageView ID="radPageView1" runat="server">
                <CW:ViewBMSInfo ID="ViewBMSInfo" runat="server" />                                                                        
            </telerik:RadPageView>
            <telerik:RadPageView ID="radPageView2" runat="server"> 
                <CW:AddBMSInfo ID="AddBMSInfo" runat="server" />
            </telerik:RadPageView>                                       
        </telerik:RadMultiPage>                     
      </div>
       
</asp:Content>