﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Help.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="Help.aspx.cs" Inherits="CW.Website.Help" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                	                  	        	
    <h1>Help</h1>                          
    <div class="richText"> 
        <asp:Literal ID="litHelpBody" runat="server"></asp:Literal>            
    </div>                                  
    
</asp:Content>                