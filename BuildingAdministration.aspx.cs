﻿using CW.Utility;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Buildings;
using System;

namespace CW.Website
{
	public partial class BuildingAdministration : AdminSitePageTemp
    {
        #region events

            private void Page_FirstLoad()
            {
                radTabStrip.Tabs[2].Visible = radTabStrip.Tabs[3].Visible = siteUser.IsKGSFullAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient && siteUser.IsProxyClient) || (siteUser.IsSuperAdmin && !siteUser.IsLoggedInUnderProviderClient);

                var globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings(); //set admin global settings. Make sure to decode from ascii to html 

                litAdminBuildingsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.AdminBuildingsBody);
		    }

            private void Page_Init()
            {
                BuildingVariables.AdminMode = BuildingVariables.AdminModeEnum.NONKGS;
            }

        #endregion

        public enum TabMessages { EditBuilding }

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewBuildings).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.NONKGS; } }

            #endregion

        #endregion
	}
}