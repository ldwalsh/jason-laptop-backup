﻿using System.Text;
using Newtonsoft.Json.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;

namespace CW.Website._confluence
{
    /// <summary>
    /// Provides retrieval and storage of JSON data obtained from Confluence
    /// via Confluence's REST API.
    /// </summary>
    public class ApiDocumentationService
    {
        private static ConfluenceConfig config;

        //An HTTP Client for getting data from Confluence via it's REST API
        private static HttpClient client = new HttpClient();

        private static IDictionary<string, string> resourceTables = new Dictionary<string, string>();

        /// <summary>
        /// Service constructor
        /// </summary>
        public ApiDocumentationService()
        {
            //set up config
            config = new ConfluenceConfig();

            //authorize
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(config.Credentials)));
        }


        #region Get content and pages from Confluence, process to JSON, HTML, XML, or IEnumerable

        public List<ConfluencePageDropdownFormat> GetConfluencePages()
        {
            var url = ProcessUri(config.UriRoot, config.ExternalRESTAPIParentsChildrenPath, config.ExpandMetadata);

            var data = client.GetAsync(url).Result;

            StatusCodeCheck(data);

            var jsonContent = data.Content.ReadAsStringAsync().Result;

            var pageList = JsonToList(jsonContent);

            //return pageList.results.Where(r => r.metadata.labels.results.Count == 1).Select(r => new ConfluencePageDropdownFormat { id = r.id, title = r.title, self = r._links.self, label = r.metadata.labels.results.First().name }).ToList();

            return pageList;
        }


        /// <summary>
        /// Given a config instanec with the proper parameters, get JSON data from Confluence
        /// Get content from Confluence via HTTP client and convert extracted JSON into HTML / XML
        /// </summary>
        /// <param name="pageUri"></param>
        /// <returns></returns>
        public string GetDocumentation(string pageUri)
        {
            var url = ProcessUri(pageUri, "", config.ExpandBody);

            var data = client.GetAsync(url).Result;

            StatusCodeCheck(data);

            var jsonContent = data.Content.ReadAsStringAsync().Result;

            var rawHtml = JsonToHtml(jsonContent);

            var html = PostProcessHTML(rawHtml);

            //var xml = HtmlToXml(html);

            return html;
        }

        #endregion

        private Uri ProcessUri(string uri, string path, string queryString)
        {
            var builder = new UriBuilder(uri);
            if(!String.IsNullOrEmpty(path)) builder.Path = path;
            builder.Query = queryString;
            return builder.Uri;
        }

        /// <summary>
        /// Parse incoming content from Confluence to JSON
        /// </summary>
        /// <param name="jsonContent">A string representing the unparsed JSON</param>
        /// <returns>A string of HTML extracted from the JSON</returns>
        private string JsonToHtml(string jsonContent)
        {
            JObject data = JObject.Parse(jsonContent);

            var html = data.SelectToken(config.BodyEditorValueToken).Value<string>();

            return html;
        }

        private List<ConfluencePageDropdownFormat> JsonToList(string jsonContent)
        {
            JObject data = JObject.Parse(jsonContent);
            
            var results = data.SelectToken("results").Children();

            var pages = results.Select(r => new ConfluencePageDropdownFormat
                {
                    id = r.SelectToken("id").ToString(),
                    title = r.SelectToken("title").ToString(),
                    self = r.SelectToken("_links").SelectToken("self").ToString(),
                    label = r.SelectToken("metadata").SelectToken("labels").SelectToken("results").Children().Select(l => l.SelectToken("name").ToString()).FirstOrDefault(),            
              });

            return pages.ToList();
        }

        /// <summary>
        /// Post-processing to clean the HTML of various unwanted things
        /// and other preparation for further processing
        /// </summary>
        /// <param name="html">A string of raw HTML from Confluence</param>
        /// <returns>A string of processed HTML</returns>
        private string PostProcessHTML(string html)
        {
            // this regex removes img tags(we don't use or need them and they come as invalid xhtml) 
            // inline style and class attributes to strip confluence styling 
            html = Regex.Replace(html, @"((<img.*?/?>\s*?)(<))|(\bstyle=\"".*?\"")|(\bclass=\"".*?\"")",
                m => {
                    if (m.Groups.Count > 1)
                    {
                        for (var i = 0; i < m.Groups.Count; i++)
                        {
                            if (m.Groups[i].Value == "<")
                                return m.Groups[i].Value;
                        }

                        return string.Empty;
                    }
                    else
                        return string.Empty;

                }, RegexOptions.Compiled);

            html = Regex.Replace(html, @"<table.*?>", @"<table border=""1"">", RegexOptions.Compiled);

            return html;
        }

        /// <summary>
        /// Convert the string of HTML to XML
        /// </summary>
        /// <param name="html">A string of HTML</param>
        /// <returns>An <see cref="XDocument"/> conversion of the HTML</returns>
        private XDocument HtmlToXml(string html)
        {
            var htmlToXml = new StringBuilder(html);

            htmlToXml.Insert(0, "<rootNode>");

            htmlToXml.Append("</rootNode>");

            htmlToXml.Insert(0, @"<?xml version=""1.0"" encoding=""UTF-8""?>");

            var str = htmlToXml.ToString();

            return XDocument.Parse(str, LoadOptions.PreserveWhitespace);
        }

        private void StatusCodeCheck(HttpResponseMessage msg)
        {
            if (!msg.IsSuccessStatusCode)
            {
                string fullMsg = String.Format(" {0} - {1} - {2} ", "Unsuccessful response from Confluence API.", (int)msg.StatusCode, msg.ReasonPhrase);

                throw new Exception(fullMsg);
            }
        }
    }

    /// <summary>
    /// Config object for use by this service class to perform the
    /// necessary retrieval and parsing of Confluence data.
    /// </summary>
    internal class ConfluenceConfig
    {
        private static Configuration Config;

        public ConfluenceConfig()
        {
            InitializeConfig();
        }

        /// <summary>
        /// The root domain URL for Confluence, i.e, http://foobar.confluence.com
        /// </summary>
        public string UriRoot { get; private set; }

        /// <summary>
        /// Basic Auth user credentials in the form "user:password"
        /// </summary>
        public string Credentials { get; private set; }

        /// <summary>
        /// The Confluence REST API path to the parent, i.e, /wiki/rest/api/content/49840147/
        /// </summary>
        public string ExternalRESTAPIParentPath { get; private set; }

        /// <summary>
        /// The Confluence REST API path to the parents children, i.e, /wiki/rest/api/content/49840147/child/page
        /// </summary>
        public string ExternalRESTAPIParentsChildrenPath { get; private set; }

        /// <summary>
        /// The expand body querystring parameter.
        /// </summary>
        public string ExpandBody { get; private set; }
        
        /// <summary>
        /// The expand metadata querystring parameter.
        /// </summary>
        public string ExpandMetadata { get; private set; }

        /// <summary>
        /// The JSON token key for the particular content to extract from what Confluence returned.
        /// </summary>
        public string BodyEditorValueToken { get; private set; }

        private void InitializeConfig()
        {
            UriRoot = "https://kgsbuildings.atlassian.net";

            Credentials = ConfigurationManager.AppSettings["ConfluenceCredentials"];

            //paths
            ExternalRESTAPIParentPath = "/wiki/rest/api/content/49840147";
            ExternalRESTAPIParentsChildrenPath = "/wiki/rest/api/content/49840147/child/page";

            //query string parameters
            ExpandBody = "expand=body.editor.value";
            ExpandMetadata = "expand=metadata.labels";
      
            //tokens
            BodyEditorValueToken = "body.editor.value";
        }

        private string GetSettingValue(string key)
        {
            return Config.AppSettings.Settings[key].Value ?? string.Empty;
        }
    }

   
    public class ConfluencePageDropdownFormat
    {
        public string id { get; set; }
        public string title { get; set; }
        public string self { get; set; }
        public string label { get; set; }
    }
}
