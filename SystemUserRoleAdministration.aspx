﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemUserRoleAdministration.aspx.cs" Inherits="CW.Website.SystemUserRoleAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>System User Roles Administration</h1>                        
                <div class="richText">The system user roles administration area is used to view and edit user roles.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                                    <h2>View User Roles</h2> 
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p>       
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>     
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridUserRoles"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="RoleID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridUserRoles_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridUserRoles_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridUserRoles_Sorting"  
                                             OnSelectedIndexChanged="gridUserRoles_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridUserRoles_Editing"                                                                                                                                                                                   
                                             OnDataBound="gridUserRoles_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-Width="50" SortExpression="RoleID" HeaderText="Role ID">  
                                                    <ItemTemplate><%# Eval("RoleID")%></ItemTemplate> 
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Role" HeaderText="Role Name">  
                                                    <ItemTemplate><%# Eval("Role")%></ItemTemplate>
                                                </asp:TemplateField>      
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Description">  
                                                    <ItemTemplate><%# Eval("RoleDescription")%></ItemTemplate>
                                                </asp:TemplateField>     
                                                <asp:TemplateField SortExpression="IsVisible" HeaderText="Visible">  
                                                    <ItemTemplate><%# Eval("IsVisible")%></ItemTemplate>                                                  
                                                </asp:TemplateField>                                                                                                                                                                   
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT ROLE DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvUserRole" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>User Role Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Role ID: </strong>" + Eval("RoleID") + "</li>"%> 
                                                            <%# "<li><strong>Role Name: </strong>" + Eval("Role") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("RoleDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("RoleDescription") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("IsVisible"))) ? "" : "<li><strong>Visible: </strong>" + Eval("IsVisible") + "</li>"%>
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT EQUIPMENT PANEL -->                              
                                    <asp:Panel ID="pnlEditUserRole" runat="server" Visible="false" DefaultButton="btnUpdateUserRole">                                                                                             
                                        <div>
                                            <h2>Edit User Role</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">   
                                                        <label class="label">Role ID:</label>    
                                                        <asp:Label ID="lblEditRoleID" CssClass="labelContent" runat="server"></asp:Label>                                                                                                                                                                       
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Role Name:</label>
                                                <asp:TextBox ID="txtEditRoleName" MaxLength="50" runat="server"></asp:TextBox>
                                                <asp:HiddenField ID="hdnRoleName" runat="server" Visible="false" />                                         
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>  
                                            <div class="divForm">
                                                    <label class="label">*Visible:</label>
                                                    <asp:CheckBox ID="chkEditVisible" CssClass="checkbox" runat="server" />                                                        
                                                    <p>(Warning: Setting a role as not visible will hide the role on user administrative pages.</p>                                                        
                                            </div>                                                                                              
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateUserRole" runat="server" Text="Update Role"  OnClick="updateUserRoleButton_Click" ValidationGroup="EditUserRole"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editRoleNameRequiredValidator" runat="server"
                                        ErrorMessage="Role Name is a required field."
                                        ControlToValidate="txtEditRoleName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditUserRole">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editRoleNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editRoleNameRequiredValidatorExtender"
                                        TargetControlID="editRoleNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                                    </asp:Panel>

                   </div>                                                                 
        
</asp:Content>


                    
                  
