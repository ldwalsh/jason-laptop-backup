﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" AutoEventWireup="false" CodeBehind="KGSUploadDataSources.aspx.cs" Inherits="CW.Website.KGSUploadDataSources" %>
<%@ Register src="~/_controls/upload/UploadHeader.ascx" tagname="UploadHeader" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/UploadFormat.ascx" tagname="UploadFormat" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupsDataSourceUpload.ascx" tagname="IDLookupsDataSourceUpload" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/Upload.ascx" tagname="Upload" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
  
  <CW:UploadHeader ID="UploadHeader" runat="server" />
  <CW:UploadFormat ID="UploadFormat" runat="server" />
  <CW:IDLookupsDataSourceUpload ID="IDLookupsDataSourceUpload" runat="server" />
  <CW:Upload ID="Upload" runat="server" />
    
</asp:Content>                