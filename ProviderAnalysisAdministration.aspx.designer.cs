﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace CW.Website {
    
    
    public partial class ProviderAnalysisAdministration {
        
        /// <summary>
        /// updateProgressTop control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdateProgress updateProgressTop;
        
        /// <summary>
        /// RadPageView1 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView1;
        
        /// <summary>
        /// ViewAnalyses control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Analyses.nonkgs.ViewAnalyses ViewAnalyses;
        
        /// <summary>
        /// RadPageView2 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView2;
        
        /// <summary>
        /// BuildingVariables control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Analyses.nonkgs.BuildingVariables BuildingVariables;
        
        /// <summary>
        /// RadPageView3 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView3;
        
        /// <summary>
        /// EquipmentVariables control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Analyses.nonkgs.EquipmentVariables EquipmentVariables;
        
        /// <summary>
        /// RadPageView4 control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Telerik.Web.UI.RadPageView RadPageView4;
        
        /// <summary>
        /// InputPointTypes control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.admin.Analyses.nonkgs.InputPointTypes InputPointTypes;
    }
}
