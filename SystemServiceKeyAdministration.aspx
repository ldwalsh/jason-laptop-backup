﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemServiceKeyAdministration.aspx.cs" Inherits="CW.Website.SystemServiceKeyAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
    
    <script type="text/javascript">
        //<![CDATA[
        function validationEditUploadFailed(sender, eventArgs) {
            $(".ErrorUploadEditHolder").append("<label class='errorMessage'>Validation failed for '" + eventArgs.get_fileName() + "'.</label>").fadeIn("slow");
        }

        function validationAddUploadFailed(sender, eventArgs) {
            $(".ErrorUploadAddHolder").append("<label class='errorMessage'>Validation failed for '" + eventArgs.get_fileName() + "'.</label>").fadeIn("slow");
        }
        //]]>
   </script> 
                  	                  	
            	<h1>System Service Keys Administration</h1>                        
                <div class="richText">
                    The system service keys administration page is designed to view, add, edit, and generate new service keys, or permit access to the webservices via key assignment.
                </div>
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Service Keys"></telerik:RadTab>
                            <telerik:RadTab Text="Add Service Key"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Service Keys</h2>
                                    <p>
                                        <a id="lnkSetFocusView" href="#" runat="server"></a>
                                        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                                    </p>                                           
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>   
                                                                                                                
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridServiceKeys"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="SKID"  
                                             GridLines="None"                                      
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridServiceKeys_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridServiceKeys_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridServiceKeys_Sorting"   
                                             OnSelectedIndexChanged="gridServiceKeys_OnSelectedIndexChanged"                                                                                                             
                                             OnRowDeleting="gridServiceKeys_Deleting" 
                                             OnRowEditing="gridServiceKeys_Editing"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" Text="Edit" CommandName="Edit"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ServiceKeyName" HeaderText="Service Key Name">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("ServiceKeyName"),30) %></ItemTemplate> 
                                                </asp:TemplateField>                                                 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client Name">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("ClientName"),30) %></ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField SortExpression="PortalAccess" HeaderText="Portal Access">  
                                                    <ItemTemplate><%# Eval("PortalAccess")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="ServiceAccess" HeaderText="Service Access">  
                                                    <ItemTemplate><%# Eval("ServiceAccess")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                 <asp:TemplateField SortExpression="DataTransferServiceAccess" HeaderText="Data Transfer Service Access">  
                                                    <ItemTemplate><%# Eval("DataTransferServiceAccess")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this key permanently?\n\nAlternatively you can deactivate a key temporarily instead.');"
                                                               CommandName="Delete" Text="Delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                                                          
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT KEY DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvServiceKey" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Service Key Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Name: </strong>" + Eval("ServiceKeyName") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ServiceKeyDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("ServiceKeyDescription") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ClientName"))) ? "" : "<li><strong>Client: </strong>" + Eval("ClientName") + "</li>"%>
                                                            <%# "<li><strong>Portal Access: </strong>" + Eval("PortalAccess") + "</li>"%>                    
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PortalKey"))) ? "" : "<li><strong>Portal Key: </strong>" + Eval("PortalKey") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PortalUserName"))) ? "" : "<li><strong>Portal User Name: </strong>" + Eval("PortalUserName") + "</li>"%>
                                                            <%# "<li><strong>Service Access: </strong>" + Eval("ServiceAccess") + "</li>"%> 
                                                            <%# "<li><strong>Data Transfer Service Access: </strong>" + Eval("DataTransferServiceAccess") + "</li>"%>     
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PublicKeyAsXML"))) ? "" : "<li><strong>Public Key: </strong>" + Eval("PublicKeyAsXML") + "</li>"%>            
                                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>                                                	                                                                	                                                                
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>                                     
                                                                     
                                    <!--EDIT SERVICE KEY PANEL -->                              
                                    <asp:Panel ID="pnlEditServiceKeys" runat="server" Visible="false" DefaultButton="btnUpdateServiceKey">                                                                                             
                                              <div>
                                                    <h2>Edit Service Key</h2>
                                              </div>  
                                              <div>                                                    
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <asp:HiddenField ID="hdnEditKey" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*Service Key Name:</label>
                                                        <asp:TextBox ID="txtEditServiceKeyName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                        <asp:RequiredFieldValidator ID="editServiceKeyNameRequiredValidator" runat="server"
                                                            ErrorMessage="Service Key Name is a required field."
                                                            ControlToValidate="txtEditServiceKeyName"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditServiceKey">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editServiceKeyNameRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editServiceKeyNameRequiredValidatorExtender"
                                                            TargetControlID="editServiceKeyNameRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender> 
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">Service Key Description:</label>
                                                         <textarea name="txtEditServiceKeyDescription" id="txtEditServiceKeyDescription" cols="40" rows="5" onkeyup="limitChars(this, 500, 'divEditServiceKeyDescriptionCharInfo')" runat="server"></textarea>
                                                         <div id="divEditServiceKeyDescriptionCharInfo"></div>                                                         
                                                    </div>                                                                                                       
                                                    <div class="divForm">   
                                                        <label class="label">*Client:</label>    
                                                        <asp:DropDownList ID="ddlEditClient" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                                                                
                                                            <asp:ListItem Value="-1">Select one...</asp:ListItem>  
                                                        </asp:DropDownList> 
                                                        <asp:RequiredFieldValidator ID="editClientRequiredValidator" runat="server"
                                                            ErrorMessage="Client is a required field." 
                                                            ControlToValidate="ddlEditClient"  
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            InitialValue="-1"
                                                            ValidationGroup="EditServiceKey">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editClientRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editClientRequiredValidatorExtender" 
                                                            TargetControlID="editClientRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>    
                                                    </div>
                                                                                                                                                                                                                                                                                                         
                                                    <div class="divForm">
                                                        <label class="label">*Active:</label>
                                                        <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                        
                                                    </div>  

                                                    <hr />
                                                    <h3>Portal</h3>
                                                    <div class="divForm">
                                                        <label class="label">*Portal Access:</label>
                                                        <asp:CheckBox ID="chkEditPortalAccess" CssClass="checkbox" OnCheckedChanged="editPortal_OnCheckChanged" AutoPostBack="true" runat="server" />                                                        
                                                    </div> 
                                                    <div id="divEditPortal" runat="server" visible="false">        
                                                    <div class="divForm">
                                                        <label class="label">*User Name:</label>
                                                        <asp:TextBox ID="txtEditUserName" CssClass="textbox" MaxLength="20" runat="server"></asp:TextBox>                                                                                                        
                                                        <asp:RequiredFieldValidator ID="editUserNameRequiredValidator" runat="server"
                                                            ErrorMessage="User Name is a required field."
                                                            ControlToValidate="txtEditUserName"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditServiceKey">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editUserNameRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editUserNameRequiredValidatorExtender"
                                                            TargetControlID="editUserNameRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div>                                                       
                                                    <div class="divForm">                                                          
                                                        <label class="label">New Password:</label>                                                                                          
                                                        <asp:TextBox ID="txtEditNewPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>                                            
                                                        <p>Enter a new password if one doesn't exist or if you wish to reset it.</p>
                                                        <asp:RegularExpressionValidator ID="editNewPasswordRegExValidator" runat="server"
                                                            ErrorMessage="Password must be 6-12 characters."
                                                            ValidationExpression=".{6,12}"
                                                            ControlToValidate="txtEditNewPassword"
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditServiceKey">
                                                            </asp:RegularExpressionValidator>                         
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editNewPaswordRegExValidatorExtender" runat="server"
                                                            BehaviorID="editNewPasswordRegExValidatorExtender" 
                                                            TargetControlID="editNewPasswordRegExValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                        <asp:RequiredFieldValidator ID="editNewPasswordRequiredValidator" runat="server"
                                                            ErrorMessage="New Password is a required field."
                                                            ControlToValidate="txtEditNewPassword"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditServiceKey">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editNewPasswordRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editNewPasswordRequiredValidatorExtender"
                                                            TargetControlID="editNewPasswordRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div> 
                                                    <div class="divForm">                                                          
                                                        <label class="label">Key:</label>   
                                                        <asp:LinkButton CssClass="link" ValidationGroup="RegeneratePortalKey" runat="server" Text="Regenerate Key" ID="btnRegenerate" onclick="regeneratePortalKeyButton_Click" />
                                                    </div>
                                                    </div>

                                                    <hr />
                                                    <h3>Web Services</h3>  
                                                    <div class="divForm">
                                                        <label class="label">*Service Access:</label>
                                                        <asp:CheckBox ID="chkEditServiceAccess" CssClass="checkbox" runat="server" />                                                        
                                                    </div>  

                                                   <hr />
                                                   <h3>Data Transfer Services</h3>   
                                                   <div class="divForm">
                                                        <label class="label">*Data Transfer Service Access:</label>
                                                        <asp:CheckBox ID="chkEditDataTransferServiceAccess" CssClass="checkbox" OnCheckedChanged="editDataTransfer_OnCheckChanged" AutoPostBack="true" runat="server" />
                                                   </div>
                                                   <div id="divEditDataTransfer" runat="server">         
                                                   <div class="divForm">                                                        
                                                        <label class="label">*Public Key File (.cer):</label>
                                                        <telerik:RadAsyncUpload Enabled="false" ID="rauEditPublicKey" runat="server"
                                                            AllowedFileExtensions="cer"
                                                            MaxFileInputsCount="1"
                                                            OnClientValidationFailed="validationEditUploadFailed">
                                                        </telerik:RadAsyncUpload>
                                                       <div class="ErrorUploadEditHolder"></div>
                                                    </div>                                                                                                                 
                                                    </div>     
                                                                                                                                                           
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateServiceKey" runat="server" Text="Update Key"  OnClick="updateServiceKeyButton_Click" ValidationGroup="EditServiceKey"></asp:LinkButton>                                                    
                                                </div>
                                                
                                                <!--Ajax Validators are next to each form element, because they are not always visiable/in use-->
                                    </asp:Panel>                                                                                                         
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddServiceKey" runat="server" DefaultButton="btnAddServiceKey">
                                    <h2>Add Service Key</h2>
                                    <div>        
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                            
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Service Key Name:</label>
                                            <asp:TextBox ID="txtAddServiceKeyName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                            <asp:RequiredFieldValidator ID="addServiceKeyNameRequiredValidator" runat="server"
                                                ErrorMessage="Service Key is a required field."
                                                ControlToValidate="txtAddServiceKeyName"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddServiceKey">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addServiceKeyNameRequiredValidatorExtender" runat="server"
                                                BehaviorID="addServiceKeyNameRequiredValidatorExtender"
                                                TargetControlID="addServiceKeyNameRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>
                                        <div class="divForm">
                                                <label class="label">Service Key Description:</label>
                                                <textarea name="txtAddServiceKeyDescription" id="txtAddServiceKeyDescription" cols="40" rows="5" onkeyup="limitChars(this, 500, 'divAddServiceKeyDescriptionCharInfo')" runat="server"></textarea>
                                                <div id="divAddServiceKeyDescriptionCharInfo"></div>                                                         
                                        </div> 
                                                                               
                                        <div class="divForm">   
                                            <label class="label">*Client:</label>    
                                            <asp:DropDownList ID="ddlAddClient" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                                                                
                                                <asp:ListItem Value="-1" >Select one...</asp:ListItem>  
                                            </asp:DropDownList> 
                                            <asp:RequiredFieldValidator ID="addClientRequiredValidator" runat="server"
                                                ErrorMessage="Client is a required field." 
                                                ControlToValidate="ddlAddClient"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="AddServiceKey">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addClientRequiredValidatorExtender" runat="server"
                                                BehaviorID="addClientRequiredValidatorExtender" 
                                                TargetControlID="addClientRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>                                                                     
                                            
                                        <hr />
                                        <h3>Portal</h3>
                                        <div class="divForm">
                                            <label class="label">*Portal Access:</label>
                                            <asp:CheckBox ID="chkAddPortalAccess" CssClass="checkbox" OnCheckedChanged="addPortal_OnCheckChanged" AutoPostBack="true" runat="server" />                                                        
                                        </div> 
                                        <div id="divAddPortal" runat="server" visible="false">                                                                        
                                        <div class="divForm">
                                            <label class="label">*User Name:</label>
                                            <asp:TextBox ID="txtAddUserName" CssClass="textbox" MaxLength="20" runat="server"></asp:TextBox>                                                                                                        
                                            <asp:RequiredFieldValidator ID="addUserNameRequiredValidator" runat="server"
                                                ErrorMessage="User Name is a required field."
                                                ControlToValidate="txtAddUserName"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddServiceKey"
                                                Enabled="false">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addUserNameRequiredValidatorExtender" runat="server"
                                                BehaviorID="addUserNameRequiredValidatorExtender"
                                                TargetControlID="addUserNameRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175"
                                                >
                                                </ajaxToolkit:ValidatorCalloutExtender>  
                                        </div> 
                                        <div class="divForm">  
                                            <label class="label">*Password:</label>                                                                                          
                                            <asp:TextBox ID="txtAddInitialPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="addInitialPasswordRequiredValidator" runat="server"
                                                ErrorMessage="Password is a required field." 
                                                ControlToValidate="txtAddInitialPassword"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddServiceKey"
                                                Enabled="false">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addInitialPasswordRequiredValidatorExtender" runat="server"
                                                BehaviorID="addInitialPasswordRequiredValidatorExtender" 
                                                TargetControlID="addInitialPasswordRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175"
                                                >
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addInitialPasswordRegExValidator" runat="server"
                                                ErrorMessage="Password must be 6-12 characters."
                                                ValidationExpression=".{6,12}"
                                                ControlToValidate="txtAddInitialPassword"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddServiceKey"
                                                Enabled="false">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addInitialPaswordRegExValidatorExtender" runat="server"
                                                BehaviorID="addInitialPasswordRegExValidatorExtender" 
                                                TargetControlID="addInitialPasswordRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175"
                                                >
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Confirm Password:</label>                                                                                          
                                            <asp:TextBox ID="txtAddConfirmPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="addConfirmPasswordRequiredValidator" runat="server"
                                                ErrorMessage="Confirm Password is a required field." 
                                                ControlToValidate="txtAddConfirmPassword"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddServiceKey"
                                                Enabled="false">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmPasswordRequiredValidatorExtender" runat="server"
                                                BehaviorID="addConfirmPasswordRequiredValidatorExtender" 
                                                TargetControlID="addConfirmPasswordRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175"
                                                >
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addConfirmPasswordRegExValidator" runat="server"
                                                ErrorMessage="Confirm Password must be 6-12 characters."
                                                ValidationExpression=".{6,12}"
                                                ControlToValidate="txtAddConfirmPassword"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddServiceKey"
                                                Enabled="false">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmPasswordRegExValidatorExtender" runat="server"
                                                BehaviorID="addConfirmPasswordRegExValidatorExtender" 
                                                TargetControlID="addConfirmPasswordRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175"
                                                >
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:CompareValidator ID="addConfirmPasswordCompareValidator" runat="server"
                                                ErrorMessage="The passwords do not match. Passwords are case sensitive." 
                                                ControlToValidate="txtAddConfirmPassword"
                                                ControlToCompare="txtAddInitialPassword" 
                                                Operator="Equal"
                                                Display="None"
                                                ValidationGroup="AddServiceKey"
                                                Enabled="false">
                                                </asp:CompareValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmPasswordCompareValidatorExtender" runat="server"
                                                BehaviorID="addConfirmPasswordCompareValidatorExtender" 
                                                TargetControlID="addConfirmPasswordCompareValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175"
                                                >
                                                </ajaxToolkit:ValidatorCalloutExtender>   
                                        </div>                                        
                                        </div>

                                        <hr />
                                        <h3>Web Services</h3>       
                                        <div class="divForm">
                                            <label class="label">*Service Access:</label>
                                            <asp:CheckBox ID="chkAddServiceAccess" CssClass="checkbox" runat="server" />                                                        
                                        </div>
                                        
                                        <hr />
                                        <h3>Data Transfer Services</h3>       
                                        <div class="divForm">
                                            <label class="label">*Data Transfer Service Access:</label>
                                            <asp:CheckBox ID="chkAddDataTransferServiceAccess" CssClass="checkbox" OnCheckedChanged="addDataTransfer_OnCheckChanged" AutoPostBack="true" runat="server" />
                                        </div>
    
                                        <div id="divAddDataTransfer" runat="server">                                                                                  
                                        <div class="divForm">
                                            <label class="label">*Public Key File (.cer):</label>
                                            <telerik:RadAsyncUpload Enabled="false" ID="rauAddPublicKey" runat="server"
                                                AllowedFileExtensions="cer"
                                                MaxFileInputsCount="1"
                                                OnClientValidationFailed="validationAddUploadFailed">
                                            </telerik:RadAsyncUpload>
                                            <div class="ErrorUploadAddHolder"></div>
                                        </div>      
                                        </div>
                                                                                                                                                                                               
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddServiceKey" runat="server" Text="Add Key"  OnClick="addServiceKeyButton_Click" ValidationGroup="AddServiceKey"></asp:LinkButton>                                                                                   
                                    </div>
                                    
                                    <!--Ajax Validators are next to each form element, because they are not always visiable/in use-->    
                                </asp:Panel>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
                            </telerik:RadPageView>                                                                                                                      
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>


                    
                  
