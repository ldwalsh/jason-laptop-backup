﻿using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.IO;

using CW.Website._framework;
using CW.Data.Models.Documentation;
using System.Collections.Generic;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;

namespace CW.Website
{
    //***GroupingRepeater extension viewstate, commandarguments, and asyncpostbacks not working***

    public partial class Documentation : SitePage
    {
        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    //temp, until we upload to blob
                    DirectoryInfo di = new DirectoryInfo(Server.MapPath(ConfigurationManager.AppSettings["DocumentationAssetPath"].ToString() + TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "\\$productshort\\")));
                    FileInfo[] files = di.GetFiles();

                    List<GetDocumentationData> documentation = ConvertFileInfoToModalClass(files);

                    //bind equipment variabels to repeater
                    rptDocumentation.DataSource = documentation;
                    rptDocumentation.DataBind();

                    //rptDocumentation.DataSource = documentation;
                    //rptDocumentation.Comparer = new SectionComparer();
                    //rptDocumentation.DataBind();
                }
                else
                {
                    //rebind from extensions viewstate
                    //rptDocumentation.DataReBindFromViewState();
                }
            }

        #endregion

        #region Button Click Events

            protected void downloadButton_Click(object sender, RepeaterCommandEventArgs e)
            {
                byte[] bytes = File.ReadAllBytes(e.CommandArgument.ToString());

                //byte[] bytes = File.ReadAllBytes(((LinkButton)rptDocumentation.Items[e.Item.ItemIndex].FindControl("btnDownload")).CommandArgument);
                string fileName = ((HtmlGenericControl)((RepeaterItem)rptDocumentation.Items[e.Item.ItemIndex]).Controls[1]).InnerText;
                
                try
                {
                    new FilePublishService(LogMgr).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), fileName, bytes);
                }
                catch
                {
                }
            }

        #endregion

            private List<GetDocumentationData> ConvertFileInfoToModalClass(FileInfo[] files)
            {
                List<GetDocumentationData> docs = new List<GetDocumentationData>();               

                foreach(FileInfo file in files)
                {
                    GetDocumentationData doc = new GetDocumentationData()
                    {
                         DocumentationSection = "Manuals",
                         FileName = file.Name,
                         FilePath = file.FullName,
                    };

                    docs.Add(doc);
                }

                return docs;
            }

            //private class SectionComparer : System.Collections.IComparer
            //{
            //    public int Compare(object x, object y)
            //    {
            //        if (x == null || y == null)
            //            return -1;

            //        string section1 = (x as GetDocumentationData).DocumentationSection;
            //        string section2 = (y as GetDocumentationData).DocumentationSection;

            //        return section1.CompareTo(section2);
            //    }
            //}
    }
}
