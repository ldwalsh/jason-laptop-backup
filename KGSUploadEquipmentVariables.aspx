﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" AutoEventWireup="false" CodeBehind="KGSUploadEquipmentVariables.aspx.cs" Inherits="CW.Website.KGSUploadEquipmentVariables" %>
<%@ Register src="~/_controls/upload/UploadHeader.ascx" tagname="UploadHeader" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/UploadFormat.ascx" tagname="UploadFormat" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupsEquipmentVariableUpload.ascx" tagname="IDLookupsEquipmentVariableUpload" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/Upload.ascx" tagname="Upload" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <CW:UploadHeader ID="UploadHeader" runat="server" />
  <CW:UploadFormat ID="UploadFormat" runat="server" />
  <CW:IDLookupsEquipmentVariableUpload ID="IDLookupsEquipmentVariableUpload" runat="server" />
  <CW:Upload ID="Upload" runat="server" />

</asp:Content>   