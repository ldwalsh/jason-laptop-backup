﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="PointAdministration.aspx.cs" Inherits="CW.Website.PointAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/_controls/DropDowns.ascx" tagname="DropDowns" tagprefix="CW" %>
<%@ Register tagPrefix="CW" tagName="PointDownloadArea" src="~/_controls/admin/Points/PointDownloadArea.ascx" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>Point Administration</h1>                        
                <div class="richText">
                    <asp:Literal ID="litAdminPointsBody" runat="server"></asp:Literal> 
                </div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                    <CW:TabHeader runat="server" ID="tabHeader" Title="View Points">
                      <RightAreaTemplate>
                        <CW:PointDownloadArea runat="server" ID="PointDownloadArea" Visible="false" />
                      </RightAreaTemplate>
                    </CW:TabHeader>

                    <p>
                        <a id="lnkSetFocusView" href="#" runat="server"></a>
                        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                        <asp:Label ID="lblErrors" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>                                            
                    </p> 
                    <CW:DropDowns runat="server"
                                  ID="dropDowns" 
                                  validationEnabled="true"    
                                  StartViewMode="Building"                              
                                  ViewMode="Equipment" 
                                  StartOptionalViewMode="EquipmentClass"
                                  OnBuildingChanged="dropDowns_OnBuildingChanged" 
                                  OnEquipmentClassChanged="dropDowns_OnEquipmentClassChanged"
                                  OnEquipmentChanged ="dropDowns_OnEquipmentChanged">
                    </CW:DropDowns>
                    <br />                                         
                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                    </asp:Panel> 
                                                                                                
                    <div id="gridTbl">                                        
                           <asp:GridView 
                             ID="gridPoints"   
                             EnableViewState="true"           
                             runat="server"  
                             DataKeyNames="EPID,PID"  
                             GridLines="None"                                      
                             PageSize="20" PagerSettings-PageButtonCount="20"
                             OnRowCreated="gridPoints_OnRowCreated" 
                             AllowPaging="true"  OnPageIndexChanging="gridPoints_PageIndexChanging"
                             AllowSorting="true"  OnSorting="gridPoints_Sorting"   
                             OnSelectedIndexChanged="gridPoints_OnSelectedIndexChanged"                                                                                                                                         
                             OnDataBound="gridPoints_OnDataBound"
                             OnRowEditing="gridPoints_Editing"
                             AutoGenerateColumns="false"                                              
                             HeaderStyle-CssClass="tblTitle" 
                             RowStyle-CssClass="tblCol1"
                             AlternatingRowStyle-CssClass="tblCol2"   
                             RowStyle-Wrap="true"                                                                                   
                             > 
                             <Columns>
                                <asp:CommandField ShowSelectButton="true" />       
                                <asp:TemplateField>
                                    <ItemTemplate>                                                
                                            <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>                                                                                                                                       
                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointName" HeaderText="Point Name">    
                                    <ItemTemplate><label title="<%# Eval("PointName") %>"><%# StringHelper.TrimText(Eval("PointName"),20) %></label></ItemTemplate>
                                </asp:TemplateField>                                                  
                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentName" HeaderText="Equipment Name">  
                                    <ItemTemplate><label title="<%# Eval("EquipmentName") %>"><%# StringHelper.TrimText(Eval("EquipmentName"),20) %></label></ItemTemplate>                                  
                                </asp:TemplateField>  
                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataSourceName" HeaderText="Data Source Name">                                 
                                    <ItemTemplate><label title="<%# Eval("DataSourceName") %>"><%# StringHelper.TrimText(Eval("DataSourceName"),20) %></label></ItemTemplate>                                  
                                </asp:TemplateField>  
                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointTypeName" HeaderText="Point Type Name">  
                                    <ItemTemplate><label title="<%# Eval("PointTypeName") %>"><%# StringHelper.TrimText(Eval("PointTypeName"),15) %></label></ItemTemplate>                                  
                                </asp:TemplateField>                                               
                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingSettingBasedEngUnits" HeaderText="Eng Units">                           
                                    <ItemTemplate><label title="<%# Eval("BuildingSettingBasedEngUnits") %>"><%# StringHelper.TrimText(Eval("BuildingSettingBasedEngUnits"),10) %></label></ItemTemplate>                                  
                                </asp:TemplateField>  
                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="ReferenceID">  
                                    <ItemTemplate><label title="<%# Eval("ReferenceID") %>"><%# StringHelper.TrimText(Eval("ReferenceID"),10) %></label></ItemTemplate>                                                                                
                                </asp:TemplateField>   
                                <asp:TemplateField SortExpression="Active" HeaderText="Active">  
                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                 </asp:TemplateField>  
                                 <asp:TemplateField SortExpression="IsVisible" HeaderText="Visible">  
                                    <ItemTemplate><%# Eval("IsVisible")%></ItemTemplate>                                                  
                                </asp:TemplateField>                                                                                                                                                                                                                   
                             </Columns>        
                         </asp:GridView> 
                                                          
                    </div>                                    
                    <br /><br /> 
                    <div>                                                            
                    <!--SELECT POINT DETAILS VIEW -->
                    <asp:DetailsView ID="dtvPoint" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                        <Fields>
                            <asp:TemplateField  
                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                            ItemStyle-BorderWidth="0" 
                            ItemStyle-BorderColor="White" 
                            ItemStyle-BorderStyle="none"
                            ItemStyle-Width="100%"
                            HeaderStyle-Width="1" 
                            HeaderStyle-BorderWidth="0" 
                            HeaderStyle-BorderColor="White" 
                            HeaderStyle-BorderStyle="none"
                            >
                                <ItemTemplate>                                    
                                    <div>
                                        <h2>Point Details</h2>                                                                                                                           
                                        <ul class="detailsList">
                                            <%# "<li><strong>Point Name: </strong>" + Eval("PointName") + "</li>"%> 	                                                                            
                                            <%# "<li><strong>Point Class: </strong>" + Eval("PointClassName") + "</li>"%>
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PointClassDescription"))) ? "" : "<li><strong>Point Class Description: </strong>" + Eval("PointClassDescription") + "</li>"%>                                                            
                                            <%# "<li><strong>Point Type: </strong>" + Eval("PointTypeName") + "</li>"%>
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PointTypeDescription"))) ? "" : "<li><strong>Point Type Description: </strong>" + Eval("PointTypeDescription") + "</li>"%>
                                            <%# "<li><strong>Data Source Name: </strong>" + Eval("DataSourceName") + "</li>"%>  
                                            <%# "<li><strong>Equipment Name: </strong>" + Eval("EquipmentName") + "</li>"%> 
                                            <%# "<li><strong>Building Name: </strong>" + Eval("BuildingName") + "</li>"%> 
                                            <%# "<li><strong>Time Zone: </strong>" + Eval("TimeZone") + "</li>"%> 	                                                                            
                                            <%# "<li><strong>ReferenceID: </strong>" + Eval("ReferenceID") + "</li>"%> 
                                            <%# "<li><strong>Engineering Units: </strong>" + Eval("BuildingSettingBasedEngUnits") + "</li>"%> 
                                            <%# "<li><strong>Sampling Interval: </strong>" + Eval("SamplingInterval") + " minutes</li>"%> 
                                            <%# "<li><strong>Subscription Based: </strong>" + Eval("IsSubscriptionBased") + "</li>"%>
                                            <%# "<li><strong>Rel. Percent Error: </strong>" + Eval("RelPctError") + "</li>"%> 
                                            <%# "<li><strong>Power (Storage): </strong>" + Eval("Power") + "</li>"%> 
                                            <%# "<li><strong>Multiply (Storage): </strong>" + Eval("Multiply") + "</li>"%>   
                                            <%# "<li><strong>Addition (Storage): </strong>" + Eval("Addition") + "</li>"%>
                                            <%# "<li><strong>Power (Analyses): </strong>" + Eval("Power") + "</li>"%> 
                                            <%# "<li><strong>Multiply (Analyses): </strong>" + Eval("Multiply") + "</li>"%>   
                                            <%# "<li><strong>Addition (Analyses): </strong>" + Eval("Addition") + "</li>"%>
                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>
                                            <%# "<li><strong>Visible: </strong>" + Eval("IsVisible") + "</li>"%>
                                         </ul>
                                    </div>
                                </ItemTemplate>                                             
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>   
                    
                   </div>                                     
                   
                   <!--EDIT POINT PANEL -->                              
                    <asp:Panel ID="pnlEditPoint" runat="server" Visible="false" DefaultButton="btnUpdatePoint">                                                                                             
                            <div>
                                <h2>Edit Point</h2>
                                <p>Any alterations to a point will be applied to every piece of equipment the point is associated with.</p>
                                <p>Changes associated with incomming data will take effect at the <strong>top of the hour</strong> when the lookup table is refreshed.</p>
                            </div>  
                            <div>      
                                <a id="lnkSetFocusEdit" runat="server"></a>                                              
                                <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />                                                
                                <div class="divForm">
                                    <label class="label">*Point Name:</label>
                                    <asp:TextBox ID="txtEditPointName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                </div>
 
                                <asp:HiddenField ID="hdnEditDataSourceID" runat="server" Visible="false" />   

                                <div class="divForm">
                                    <label class="label">*ReferenceID:</label>
                                    <asp:TextBox ID="txtEditReferenceID" CssClass="textbox" MaxLength="250" runat="server"></asp:TextBox>                                                                                                        
                                    <asp:HiddenField ID="hdnEditReferenceID" runat="server" Visible="false" />
                                    <p>(Warning: Changing the reference id may result in a mis-match for incoming associated data.)</p>
                                </div>                                                                                                                                                   
                                <asp:LinkButton CssClass="lnkButton" ID="btnUpdatePoint" runat="server" Text="Update Point"  OnClick="updatePointButton_Click" ValidationGroup="EditPoint"></asp:LinkButton>    
                            </div>
                                            
                            <!--Ajax Validators-->      
                            <asp:RequiredFieldValidator ID="editPointNameRequiredValidator" runat="server"
                                ErrorMessage="Point Name is a required field."
                                ControlToValidate="txtEditPointName"          
                                SetFocusOnError="true"
                                Display="None"
                                ValidationGroup="EditPoint">
                                </asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="editPointNameRequiredValidatorExtender" runat="server"
                                BehaviorID="editPointNameRequiredValidatorExtender"
                                TargetControlID="editPointNameRequiredValidator"
                                HighlightCssClass="validatorCalloutHighlight"
                                Width="175">
                                </ajaxToolkit:ValidatorCalloutExtender>                                                                                              
                            <asp:RequiredFieldValidator ID="editReferenceIDRequiredValidator" runat="server"
                                ErrorMessage="ReferenceID is a required field."
                                ControlToValidate="txtEditReferenceID"          
                                SetFocusOnError="true"
                                Display="None"
                                ValidationGroup="EditPoint">
                                </asp:RequiredFieldValidator>
                            <ajaxToolkit:ValidatorCalloutExtender ID="editReferenceIDRequiredValidatorExtender" runat="server"
                                BehaviorID="editReferenceIDRequiredValidatorExtender"
                                TargetControlID="editReferenceIDRequiredValidator"
                                HighlightCssClass="validatorCalloutHighlight"
                                Width="175">
                                </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
                    </asp:Panel>
                   
                                                                                                      
                   </div>                                                                 
               
</asp:Content>


                    
                  
