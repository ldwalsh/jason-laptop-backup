﻿using System;
using System.Collections.Generic;
using System.Linq;
using CW.Business.Config;
using CW.Common.Config;
using CW.Common.Helpers;
using CW.Common.Instrumentation;
using CW.Data;
using CW.Data.AzureStorage.Config;
using CW.Data.AzureStorage.Helpers;
using CW.Data.AzureStorage.Instrumentation;
using CW.Utility;
using CW.Business;
using CW.Common.Constants;
using StructureMap;
using CW.Data.Collections;
using System.Security.Cryptography.X509Certificates;
using CW.Data.Models;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage;
using CW.Business.Factories;

namespace CW.Website
{
    public class IoC
    {
        private static IContainer Container { get; set; }

        static IoC()
        {
            Initialize();
        }

        public IoC()
        {
        }

        public T Resolve<T>()
        {
            return Container.GetInstance<T>();
        }

        public static void Initialize()
        {
            Container = new Container(x =>
            {
                #region custom app specific

                var cm = new AzureConfigManager();

                var env = EnvironmentType.FromValue<EnvironmentType>(cm.GetConfigurationSetting(DataConstants.ConfigKeyCWEnv, "", true));

                var logger = new Log4NetLogger(cm, true);
                var sectionLogger = new SectionLogManager(logger, Common.Constants.DataConstants.LogSection.Website);
                var dateTimeHelper = new DateTimeHelper(sectionLogger);
                var bingMapsHelper = new BingMapsHelper(sectionLogger);

                var localStoragePath = cm.GetLocalStoragePath(DataConstants.ConfigKeyLogsLocalStorage);
                DecryptedStorageAccount dAcct = new DataManagerSQL().StorageAccountDataMapper.GetDecryptedStorageAccount(new StorageAccountCriteria() { IsCommon = true, CWEnvironment = env, StorageAcctLevel = StorageAccountLevel.Primary }, cm, localStoragePath);
                var cert = CW.Utility.CertificateHelper.LoadCertificate(StoreName.My, StoreLocation.LocalMachine, cm.GetConfigurationSetting(DataConstants.CertMgmtFootprint, "", true));
                var storageCreds = new StorageCredentials(dAcct.Creds.AccountName, dAcct.Creds.GetActiveAccountKey(cm));
                var azureStorageAccount = new CloudStorageAccount(storageCreds, true);
                var dmCommon = new DataManagerCommonStorage(azureStorageAccount, cm, sectionLogger, env);

                x.For<IConfigManager>().Singleton().Use(cm);
                x.For<ILogManager>().Singleton().Use(logger);
                x.For<ISectionLogManager>().Singleton().Use(sectionLogger);
                x.For<DateTimeHelper>().Singleton().Use(dateTimeHelper);
                x.For<BingMapsHelper>().Singleton().Use(bingMapsHelper);

                x.For<IDataManagerFactory>().Singleton().Use<DataManagerFactory>()
                    .Ctor<CloudStorageAccount>("account").Is(azureStorageAccount)
                    .Ctor<IConfigManager>("cm").Is(cm)
                    .Ctor<ISectionLogManager>("logger").Is(sectionLogger)
                    .Ctor<string>("env").Is(cm.GetConfigurationSetting(DataConstants.ConfigKeyCWEnv, "", true));

                // StructureMap does NOT recommend using old ASP.NET lifecycles, modern web frameworks will be able to manage better 
                // http://structuremap.github.io/object-lifecycle/supported-lifecycles/ (see Legacy ASP.Net Lifecycles)
                x.For<DataManager>().AlwaysUnique().Use(ctx => ctx.GetInstance<IDataManagerFactory>().Create());
                x.For<DataManagerCommonStorage>().Singleton().Use(dmCommon);

                //x.Policies.FillAllPropertiesOfType<DataManager>();

                #endregion
            });

            Container.AssertConfigurationIsValid();
        }
    }
}