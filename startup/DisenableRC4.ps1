﻿# If any settings are changed, this will change to $True and the server will reboot
$reboot = $False

$regParentPath = @("SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers")
$ciphers = "RC4 40/128,RC4 56/128,RC4 64/128,RC4 128/128"

Function Set-CryptoSetting {
  param (
    $keyindex,
    $value,
    $valuedata,
    $valuetype,
    $restart
  )

  $regParentPathWithPrefix = "HKLM:\" + $regParentPath[$keyindex];

  Write-Host $regParentPathWithPrefix

  # Check for existence of registry key, and create if it does not exist
  If (!(Test-Path -Path $regParentPathWithPrefix)) {
    Write-Host "before"
    New-Item $regParentPathWithPrefix | Out-Null
    Write-Host "after"
  }

  foreach ($cipher in $ciphers.Split("{,}")) {
	
    $cipherFullPath = "${regParentPathWithPrefix}\${cipher}";

    If (!(Test-Path -Path $cipherFullPath)) { 
        # create sub-key if does not exist
        $key = (Get-Item HKLM:\).OpenSubKey($regParentPath[$keyindex], $true)
        $key.CreateSubKey($cipher)
        $key.Close()
    }

    # get data of registry value, or null if it does not exist
    $val = (Get-ItemProperty -Path $cipherFullPath -Name $value -ErrorAction SilentlyContinue).$value

    If ($val -eq $null) {
	    # value does not exist - create and set to desired value
		New-ItemProperty -Path $cipherFullPath -Name $value -Value $valuedata -PropertyType $valuetype | Out-Null
		$restart = $True
	} 
    Else {
	    # value does exist - if not equal to desired value, change it
    	If ($val -ne $valuedata) {
		    Set-ItemProperty -Path $cipherFullPath -Name $value -Value $valuedata
	    	$restart = $True
    	}
    }
  }

  return $restart
}



# Ensure RC4 is disenabled for server
$reboot = Set-CryptoSetting 0 Enabled 0 DWord $reboot

# If any settings were changed, reboot
If ($reboot) {
  Write-Host "Rebooting now..."
  shutdown.exe /r /t 5 /c "Crypto settings changed" /f /d p:2:4
}