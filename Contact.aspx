﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Help.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="Contact.aspx.cs" Inherits="CW.Website.Contact" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                	                  	        	
        	<h1>Contact Us</h1>                                           
                <div class="richText">       
                    Please contact KGS Buildings for general questions involving clockworks, suggestions, or in the result that an error has occured in your system and your administrator cannot help you any further.                                                
                </div>      
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
                </div>  
                <div class="administrationControls">    
                    <asp:Panel ID="pnlContact" runat="server" DefaultButton="btnContact">
                    <p>
                        <a id="lnkSetFocusView" href="#" runat="server"></a>
                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                    </p>  
                    <div class="divForm">
                         <label class="label">*Email Subject:</label>
                         <asp:DropDownList CssClass="dropdown" ID="ddlSubject" AppendDataBoundItems="true" runat="server" AutoPostBack="true">  
                            <asp:ListItem Value="-1">Select one...</asp:ListItem>   
                            <asp:ListItem Value="General Inquiry">General Inquiry</asp:ListItem>
                            <asp:ListItem Value="Increase Download Limit Request">Increase Download Limit Request</asp:ListItem>
                            <asp:ListItem Value="Large Data Download Request">Large Data Download Request</asp:ListItem>
                            <asp:ListItem Value="New Building, Equipment, or Point Setup">New Building, Datasource, Equipment, or Point Setup</asp:ListItem>  
                            <asp:ListItem Value="Other">Other</asp:ListItem> 
                            <asp:ListItem Value="Report Bug/Issue">Report Bug/Issue</asp:ListItem> 
                            <asp:ListItem Value="Module Access Request">Module Access Request</asp:ListItem> 
                            <asp:ListItem Value="Provider Setup/Provider Client Access">Provider Setup/Provider Client Access</asp:ListItem> 
                            <asp:ListItem Value="Single Sign On Setup Request">Single Sign On Setup Request</asp:ListItem> 
                            <asp:ListItem Value="Suggestion/Recommendation">Suggestion/Recommendation</asp:ListItem> 
                            <asp:ListItem Value="Technical Support">Technical Support</asp:ListItem>                             
                         </asp:DropDownList>   
                         <asp:RequiredFieldValidator ID="subjectRequiredValidator" runat="server"
                            ErrorMessage="Subject is a required field." 
                            ControlToValidate="ddlSubject"  
                            SetFocusOnError="true" 
                            Display="None" 
                            InitialValue="-1"
                            ValidationGroup="Contact">
                            </asp:RequiredFieldValidator>
                         <ajaxToolkit:ValidatorCalloutExtender ID="subjectRequiredValidatorExtender" runat="server"
                            BehaviorID="subjectRequiredValidatorExtender" 
                            TargetControlID="subjectRequiredValidator"
                            HighlightCssClass="subjectCalloutHighlight"
                            Width="175">
                            </ajaxToolkit:ValidatorCalloutExtender>                                              
                    </div> 
                    <div class="divForm">
                            <label class="label">*Email Description:</label>
                            <textarea name="txtDescription" id="txtDescription" cols="40" onkeyup="limitChars(this, 1000, 'divDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                            <div id="divDescriptionCharInfo"></div>
                    </div>
                    <asp:LinkButton CssClass="lnkButton" ID="btnContact" runat="server" Text="Submit"  OnClick="submitButton_Click" ValidationGroup="Contact"></asp:LinkButton>    

                    </asp:Panel>
                </div>                  
    
</asp:Content>                