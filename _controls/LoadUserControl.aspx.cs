﻿using CW.Common.Constants;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Web.Services;

namespace CW.Website._controls
{
    public partial class LoadUserControl : SitePage
    {
        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                //Check if logged in
                if (!siteUser.IsAnonymous)
                {
                    if (!String.IsNullOrWhiteSpace(Request.QueryString["uc"]) || Request.QueryString["uc"] != null)
                    {
                        try
                        {
                            var isProviderView = (!String.IsNullOrWhiteSpace(Request.QueryString["providerView"]) || Request.QueryString["providerView"] != null) ? true : false;
                            var isByClient = (!String.IsNullOrWhiteSpace(Request.QueryString["isByClient"]) || Request.QueryString["isByClient"] != null) ? true : false;
                            
                            switch (Request.QueryString["uc"])
                            {
                                case "BuildingMap":
                                    BuildingMap.LoadModule(isProviderView);
                                    break;
                                case "LastMonthsDiagnosticSummary":
                                    LastMonthsDiagnosticSummary.LoadModule(isProviderView, isByClient);
                                    break;
                                case "LastMonthsDiagnosticResults":
                                    LastMonthsDiagnosticResults.LoadModule(isProviderView);
                                    break;
                                case "Weather":
                                    Weather.LoadModule();
                                    break;
                                case "Past30DaysAvoidableCosts":
                                    Past30DaysAvoidableCosts.LoadModule(isProviderView);
                                    break;
                                case "YesterdaysDiagnosticSummary":
                                    YesterdaysDiagnosticSummary.LoadModule(isProviderView, isByClient);
                                    break;
                                case "YesterdaysDiagnosticResults":
                                    YesterdaysDiagnosticResults.LoadModule(isProviderView);
                                    break;
                                case "Past30DaysPortfolioPriorities":
                                    Past30DaysPortfolioPriorities.LoadModule(isProviderView);
                                    break;
                                case "RecentTasks":
                                    RecentTasks.LoadModule();
                                    break;
                                case "TopProjects":
                                    TopProjects.LoadModule();
                                    break;
                                case "ConsumptionComparison":
                                    ucConsumptionComparison.LoadModule();
                                    break;								
                                case "QuickLinks":
                                    QuickLinks.LoadModule();
                                    break;
                                case "BuildingDataPointsPieChart":
                                    BuildingDataPointsPieChart.LoadModule(isProviderView);
                                    break;
                                case "FaultsSparkLine":
                                    FaultsSparkLine.LoadModule(isProviderView);
                                    break;	
                            }
                        }
                        catch (Exception ex)
                        {
                            divLoadControlError.Attributes.Add("class", "dockMessageLabel");
                            divLoadControlError.InnerText = BusinessConstants.Message.GenericError;

                            ExceptionHelper.WriteExceptionMessage(ex);
                        }
                    }
                }
                else
                    Response.Redirect("/");
            }

        #endregion

        #region methods

            [WebMethod]
            public static string CheckLoggedIn()
            {
                return (SiteUser.Current.IsAnonymous) ? "1" : "0";
            }

        #endregion
    }
}