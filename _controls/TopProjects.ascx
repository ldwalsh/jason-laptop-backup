﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopProjects.ascx.cs" Inherits="CW.Website._controls.TopProjects" %>
<%@ Import Namespace="CW.Utility" %>

<div id="gridTbl">
                                            
  <asp:GridView ID="gridTopProjects" EnableViewState="true" runat="server" DataKeyNames="ProjectID" GridLines="None" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true" EmptyDataRowStyle-CssClass="dockEmptyGridLabel" 
      EmptyDataText="No projects exist."> 
    <Columns>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Project">
        <ItemTemplate><%# StringHelper.TrimText(Eval("ProjectName"),30) %></ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Equipment Analyses">
        <ItemTemplate><%# StringHelper.TrimText(String.Join(", ",((string[])Eval("AnalysisEquipmentNameArray"))),100) %></ItemTemplate>
      </asp:TemplateField>       
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Projected Savings">
        <ItemTemplate><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval("ProjectedSavings"))) %></ItemTemplate>
      </asp:TemplateField>         
      <asp:TemplateField ItemStyle-Wrap="true">
        <ItemTemplate><a href="Projects.aspx?cid=<%# Eval("CID") %>&bid=0&sfd=<%# Eval("TargetStartDate") %>" class='dockLink'>view</a></ItemTemplate>
      </asp:TemplateField>                                                                                                                                                                         
    </Columns>        
  </asp:GridView> 

</div>