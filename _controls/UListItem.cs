﻿using CW.Utility;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    public class UListItem: WebControl, INamingContainer
    {
        public String Label {get;set;}
        public String Value {get;set;}
        
        public String Format = String.Empty;

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);

            base.OnInit(e);
        }

        protected override Object SaveControlState()
        {
            return new Pair(base.SaveControlState(), Value);
        }

        protected override void LoadControlState(Object state)
        {
            if (state == null) return;

            var p = state as Pair;

            if (p == null)
            {
                base.LoadControlState(state);

                return;
            }

            base.LoadControlState(p.First);
                
            Value = (String)p.Second;
        }

        public override void RenderControl(HtmlTextWriter writer) 
        {
            if (String.IsNullOrEmpty(Value)) return;
                        
            switch (Format.ToLower())
            {
                case "email":
                {
                    Value = StringHelper.CreateToEmail(Value);
                    
                    break;
                }

                case "whatever":
                {
                    break;
                }
            }

            writer.Write(String.Concat("<li><strong>", Label, ":", "</strong>", Value, "</li>"));
        }    
    }
}