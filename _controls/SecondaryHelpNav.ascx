﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecondaryHelpNav.ascx.cs" Inherits="CW.Website._controls.SecondaryHelpNav" %>

<!--
 <div class="helpNav">
        <a class="hoverBtn" href="~/Help.aspx" id="lnkHelp" runat="server">Help</a>
        <a class="hoverBtn" href="~/FAQ.aspx" id="lnkFAQ" runat="server">FAQ's</a>
        <a class="hoverBtn" href="~/Privacy.aspx" id="lnkPrivacy" runat="server">Privacy Policy</a>
        <a class="hoverBtn" href="~/SystemInfo.aspx" id="lnkSystemInfo" runat="server">System Info</a>
        <a class="hoverBtn" href="~/Contact.aspx" id="lnkContact" runat="server">Contact</a>
 </div>
 -->
 
 <div class="helpNav">
    <ul class="dropdown">
        <li><a href="Help.aspx" class="hoverBtn">Help</a></li>        	
    	<li><a href="Documentation.aspx" class="hoverBtn">Documentation</a></li>
        <li><a href="FAQ.aspx" class="hoverBtn">FAQ's</a></li>
    	<li><a href="ReleaseNotes.aspx" class="hoverBtn">Release Notes</a></li>
    	<li><a href="SystemInfo.aspx" class="hoverBtn">System Info</a></li>        	
    	<li><a href="Privacy.aspx" class="hoverBtn">Privacy Policy</a></li>
        <li><a href="TermsAndConditions.aspx" class="hoverBtn">Terms</a></li>
        <li><a href="Contact.aspx" class="hoverBtn">Contact</a></li>        	
    </ul>
</div>    