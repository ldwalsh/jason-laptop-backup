﻿using CW.Common.Constants;
using CW.Data;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Dock;

namespace CW.Website._controls
{
    public partial class ProviderWidgets : SiteUserControl
    {
        #region properties

            public Boolean IsProviderView { get; set; }

        #endregion

        #region page events

            protected void Page_PreRender(Object sender, EventArgs e)
            {
                if (!siteUser.IsAnonymous && IsProviderView)
                {
                    SetDockZoneAndDockAttributes();
                    SetWidgetVisibility();
                }
            }

        #endregion

        #region methods

            private void SetDockZoneAndDockAttributes()
            {
                //TODO, possible create a utility or helper since these are common with docks and zones
                //dynamically create all common events and settings for each radDockZone and radDock
                foreach (var radDockZone in this.FindControls<RadDockZone>())
                {
                    radDockZone.BorderStyle = BorderStyle.None;
                    radDockZone.FitDocks = false;
                    radDockZone.Orientation = Orientation.Horizontal;
                }

                foreach (var radDock in this.FindControls<RadDock>())
                {
                    radDock.DefaultCommands = DefaultCommands.ExpandCollapse;
                    radDock.EnableAnimation = true;
                    radDock.EnableRoundedCorners = true;
                    radDock.DockMode = DockMode.Docked;
                    radDock.Resizable = false;

                    if (radDock.ID != "radDockBuildingMap")
                    {
                        radDock.ForbiddenZones = new string[] { "radDockZone1" };
                        radDock.CssClass = "radDockMedium"; 
                        //radDock.Width = Unit.Pixel(438);
                    }
                    else
                    {
                        radDock.EnableDrag = false;
                        radDock.CssClass = "radDockLarge";
                        //radDock.Width = Unit.Pixel(888);
                    }
                }
            }

            private void SetWidgetVisibility()
            {
                var hasDiagnosticAndCommissioningAccess = false;
                var hasBuildings = siteUser.VisibleBuildingsProviderClients.Where(v => v.Value.Any()).Any();
                var hasProviderClients = siteUser.ProviderClients.Any();

                if (hasProviderClients)
                {
                    var moduleIds = new List<Int32>() 
                    { 
                        (Int32)BusinessConstants.Module.Modules.Diagnostics, 
                        (Int32)BusinessConstants.Module.Modules.CommissioningDashboard 
                    };

                    var cids = siteUser.ProviderClients.Select(pc => pc.CID);

                    foreach (var cid in cids)
                    {
                        foreach (var moduleId in moduleIds)
                            hasDiagnosticAndCommissioningAccess = (siteUser.ModulesProviderClients.Where(m => m.Key == cid).First().Value.Select(i => i.ModuleID).Contains(moduleId));

                        if (hasDiagnosticAndCommissioningAccess)
                            break;
                    }

                    radDockBuildingMap.Visible = hasBuildings;
                    radDockLastMonthsDiagnosticSummaryByClient.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                    radDockLastMonthsDiagnosticSummary.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                    radDockLastMonthsDiagnosticResults.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                    radDockPast30DaysAvoidableCosts.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;

                    radDockYesterdaysDiagnosticSummaryByClient.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                    radDockYesterdaysDiagnosticSummary.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                    radDockYesterdaysDiagnosticResults.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                    radDockPast30DaysPortfolioPriorities.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                }
            }

        #endregion
    }
}