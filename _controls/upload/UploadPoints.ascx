﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadPoints.ascx.cs" Inherits="CW.Website._controls.upload.UploadPoints" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

 <script type="text/javascript">
        function CopyGridView(panelName) {
            var div = document.getElementById(panelName);
            var controlRange;

            div.contentEditable = 'true';

            if (document.body.createControlRange) {
                controlRange = document.body.createControlRange();
                controlRange.addElement(div);
                controlRange.execCommand('Copy');
            }

            div.contentEditable = 'false';
        }

        var ListBoxCustomValidator = function (sender, args) {
            var listBox = document.getElementById(sender.controltovalidate);

            //.disabled changed in framework 4.5, .classList doesnt work in ie9
            if (!$(listBox).hasClass('aspNetDisabled')) {

                var listBoxCnt = 0;

                for (var x = 0; x < listBox.options.length; x++) {
                    if (listBox.options[x].selected) listBoxCnt++;
                }

                args.IsValid = (listBoxCnt > 0)
            }
            else {
                args.IsValid = true;
            }
        };
    </script>
                       	                  	        	
      <h1>Upload Points</h1>
      <div class="richText">
        The upload points page is designed to upload points to the system from a csv file. All points must be assigned to an initial equipment during this upload process.
        <br /><br />
        Please do not upload points with a v or vpre point enabled point type.     
        <br /><br />
        <b>WARNING:</b> Before uploading, if the excel or csv file was created in an encoding type other than <b>"UTF-8"</b> and contains special characters please perform the following steps to avoid question marks <b>"?"</b> in place of those special characters. In other odd cases, you may need to perform a find on that character in excel and replace with a manually typed or pasted character from outside of excel.
        <ol>
          <li>Open the csv in notepad. Copy the contents. Close the file.</li>
          <li>Open notepad from scratch, and paste in the contents.</li>
          <li>If there are any fields with commas (,), be sure to wrap them in double quotes (").  Unless using excel.</li>
          <li>Select File, and Save As.</li>
          <li>Before clicking the Save button, change the Encoding type to <b>"UTF-8"</b> from the dropdown next to the Save button.</li>
          <li>Save as .txt file.</li>
          <li>Rename extension as .csv and it should now be in an acceptable format for upload.</li>
        </ol>
      </div>
      <hr />

      <h2>ID Lookup:</h2>
      <asp:HyperLink ID="lnkIDLookup" runat="server" CssClass="toggle"><asp:Label ID="lblIDLookup" CssClass="toggle" runat="server" /></asp:HyperLink>
      <ajaxToolkit:CollapsiblePanelExtender ID="idLookupCollapsiblePanelExtender" runat="Server" TargetControlID="pnlIDLookup" CollapsedSize="0" Collapsed="true" ExpandControlID="lnkIDLookup" CollapseControlID="lnkIDLookup" AutoCollapse="false" AutoExpand="false" ExpandDirection="Vertical" CollapsedText="show id lookup" ExpandedText="hide id lookup" TextLabelID="lblIDLookup" />

      <asp:Panel ID="pnlIDLookup" runat="server">

        <div class="divForm">   
          <label class="label">Client:</label>    
          <label class="labelContent"><%= siteUser.ClientName %></label>
        </div>

        <div class="divForm">   
          <label class="label">Building:</label>    
          <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlLookup_OnSelectedIndexChanged" />
          <asp:Label runat="server" ID="lblBuildingID" Visible="true" Text="ID:" />
          <br />

          <asp:HyperLink ID="lnkBuildingList" runat="server" CssClass="toggle"><asp:Label ID="lblBuildingList" CssClass="toggle" runat="server" /></asp:HyperLink>                    
          <ajaxToolkit:CollapsiblePanelExtender ID="buildingListCPE" runat="Server" TargetControlID="pnlBuildingList" CollapsedSize="0" Collapsed="true" ExpandControlID="lnkBuildingList" CollapseControlID="lnkBuildingList" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lblBuildingList" CollapsedText="show selected grid view" ExpandedText="hide selected grid view" />
          &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_pnlBuildingList')">copy grid (ie only)</a>
        </div>

        <asp:Panel ID="pnlBuildingList" CssClass="richText" runat="server">
          <div id="gridTbl">   
            <asp:GridView ID="gridBuildingList" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
              <Columns>
                <asp:BoundField DataField="DSID" HeaderText="DataSource ID" />
                <asp:BoundField DataField="EID" HeaderText="Equipment ID" />
                <asp:BoundField DataField="ReferenceID" HeaderText="Point Reference ID" />
                <asp:BoundField DataField="PointName" HeaderText="Point Name" />
                <asp:BoundField DataField="PointTypeID" HeaderText="Point Type ID" />
                <asp:BoundField DataField="SamplingInterval" HeaderText="Sampling Interval" />
                <asp:BoundField DataField="RelPctError" HeaderText="RelPctError" />
                <asp:BoundField DataField="Power" HeaderText="Power (Storage)" />
                <asp:BoundField DataField="Multiply" HeaderText="Multiply (Storage)" />
                <asp:BoundField DataField="Addition" HeaderText="Addition (Storage)" />
                <asp:BoundField DataField="PowerMatLab" HeaderText="Power (Analyses)" />
                <asp:BoundField DataField="MultiplyMatLab" HeaderText="Multiply (Analyses)" />
                <asp:BoundField DataField="AdditionMatLab" HeaderText="Addition (Analyses)" />
                <asp:BoundField DataField="TimeZone" HeaderText="Time Zone" />
                <asp:BoundField DataField="PID" HeaderText="Point ID" />
              </Columns>        
            </asp:GridView>
          </div>
        </asp:Panel>

        <div class="divForm">   
          <label class="label">Data Source:</label>    
          <asp:DropDownList ID="ddlDataSource" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlLookup_OnSelectedIndexChanged" />
          <asp:Label runat="server" ID="lblDataSourceID" Visible="true" Text="ID:" />
          <br />

          <asp:HyperLink ID="lnk1" runat="server" CssClass="toggle"><asp:Label ID="lbl1" CssClass="toggle" runat="server" /></asp:HyperLink>                    
          <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender1" runat="Server" TargetControlID="pnl1" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk1" CollapseControlID="lnk1" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl1" CollapsedText="show selected grid view" ExpandedText="hide selected grid view" />
      &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_pnl1')">copy grid (ie only)</a>
        </div> 

        <asp:Panel ID="pnl1" CssClass="richText" runat="server">
          <div id="gridTbl">   
            <asp:GridView ID="gridDataSources" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
              <Columns>
                <asp:BoundField DataField="DataSourceName" HeaderText="DataSourceName" />
                <asp:BoundField DataField="DSID" HeaderText="DSID" />
                <asp:BoundField DataField="ReferenceID" HeaderText="ReferenceID" />
              </Columns>        
            </asp:GridView>
          </div>
        </asp:Panel>
              
        <div class="divForm">   
          <label class="label">Equipment:</label>    
          <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlLookup_OnSelectedIndexChanged" />
          <asp:Label runat="server" ID="lblEquipmentID" Visible="true" Text="ID:" />
          <br />

          <asp:HyperLink ID="lnk2" runat="server" CssClass="toggle"><asp:Label ID="lbl2" CssClass="toggle" runat="server" /></asp:HyperLink>                    
          <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender2" runat="Server" TargetControlID="pnl2" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk2" CollapseControlID="lnk2" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl2" CollapsedText="show selected grid view" ExpandedText="hide selected grid view" />
          &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_pnl2')">copy grid (ie only)</a>
        </div>

        <asp:Panel ID="pnl2" CssClass="richText" runat="server">
          <div id="gridTbl">   
            <asp:GridView ID="gridEquipment" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
              <Columns>
                <asp:BoundField DataField="BuildingName" HeaderText="BuildingName" />
                <asp:BoundField DataField="EquipmentName" HeaderText="EquipmentName" />
                <asp:BoundField DataField="EID" HeaderText="EID" />
                <asp:BoundField DataField="EquipmentTypeID" HeaderText="EquipmentTypeID" />
              </Columns>        
            </asp:GridView>
          </div>
        </asp:Panel>
                            
        <div class="divForm">   
          <label class="label">Point Type (Point Enabled Only):</label>
          <asp:DropDownList ID="ddlPointType" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlLookup_OnSelectedIndexChanged" />
          <asp:Label runat="server" ID="lblPointTypeID" Visible="true" Text="ID:" />
          <br />

          <asp:HyperLink ID="lnk3" runat="server" CssClass="toggle"><asp:Label ID="lbl3" CssClass="toggle" runat="server" /></asp:HyperLink>                    
          <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender3" runat="Server" TargetControlID="pnl3" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk3" CollapseControlID="lnk3" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl3" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
          &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_pnl3')">copy grid (ie only)</a>
        </div>

        <asp:Panel ID="pnl3" CssClass="richText" runat="server">
          <div id="gridTbl">  
            <asp:GridView ID="gridPointTypes" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
              <Columns>
                <asp:BoundField DataField="PointTypeName" HeaderText="PointTypeName" />
                <asp:BoundField DataField="PointTypeID" HeaderText="PointTypeID" />
                <asp:BoundField DataField="PointClassID" HeaderText="PointClassID" />
                <asp:BoundField DataField="PointClassName" HeaderText="PointClassName" />
                <asp:BoundField DataField="IPUnits" HeaderText="IPUnits" />
                <asp:BoundField DataField="SIUnits" HeaderText="SIUnits" />
              </Columns>
            </asp:GridView>
          </div>
        </asp:Panel>

        <div class="divForm">   
          <label class="label">V and VPre Point Type (V and VPre Point Enabled Only):</label>    
          <asp:DropDownList ID="ddlVAndVPrePointType" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlLookup_OnSelectedIndexChanged" />
          <asp:Label runat="server" ID="lblVAndVPrePointTypeID" Visible="true" Text="ID:" />
          <br />

          <asp:HyperLink ID="lnk4" runat="server" CssClass="toggle"><asp:Label ID="lbl4" CssClass="toggle" runat="server" /></asp:HyperLink>                    
          <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender4" runat="Server" TargetControlID="pnl4" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk4" CollapseControlID="lnk4" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl4" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
          &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_pnl4')">copy grid (ie only)</a>
        </div>
    
        <asp:Panel ID="pnl4" CssClass="richText" runat="server">
          <div id="gridTbl">  
            <asp:GridView ID="gridVAndVPrePointTypes" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
              <Columns>
                <asp:BoundField DataField="PointTypeName" HeaderText="PointTypeName" />
                <asp:BoundField DataField="PointTypeID" HeaderText="PointTypeID" />          
                <asp:BoundField DataField="PointClassID" HeaderText="PointClassID" />                                        
              </Columns>        
            </asp:GridView>
          </div>
        </asp:Panel>
                         
        <div class="divForm">  
          <label class="label">Time Zones:</label>    
          <asp:DropDownList ID="ddlTimeZone" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlLookup_OnSelectedIndexChanged" />
          <asp:Label runat="server" ID="lblTimeZoneID" Visible="true" Text="Zone:" />
        </div>

      </asp:Panel>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>                                       
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div>

      <div class="administrationControls">

        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="Upload Points" />
            <telerik:RadTab Text="Upload Points With Point Typing" />
          </Tabs>
        </telerik:RadTabStrip>

        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">

            <asp:UpdatePanel ID="updatePanelNestedUpload" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">  
              <ContentTemplate>   
                <div>
                  <h2>Upload Points</h2>
                  <div class="richText">
                    <p>Please make sure every row of the csv is in the following format below. Please click the download template button for template file with headers. Check override checkbox in upload form below if override download template is desired.</p>
                    <asp:HyperLink ID="lnkUploadPointsFieldsFormat" runat="server" CssClass="toggle"><asp:Label ID="lblUploadPointsFieldsFormat" CssClass="toggle" runat="server" /></asp:HyperLink>
                    <ajaxToolkit:CollapsiblePanelExtender ID="uploadFieldsCollapsiblePanelExtender" runat="Server" TargetControlID="pnlPointsFields" CollapsedSize="0" Collapsed="true" ExpandControlID="lnkUploadPointsFieldsFormat" CollapseControlID="lnkUploadPointsFieldsFormat" AutoCollapse="false" AutoExpand="false" ExpandDirection="Vertical" CollapsedText="show upload points fields format" ExpandedText="hide upload points fields format" TextLabelID="lblUploadPointsFieldsFormat" />

                    <asp:Panel ID="pnlPointsFields" CssClass="richText" runat="server">              
                      <asp:Literal ID="litUploadPointsFieldList" runat="server" />
                    </asp:Panel>
                    <br />

                    <asp:HyperLink ID="lnkUploadPointsOverrideFieldsFormat" runat="server" CssClass="toggle"><asp:Label ID="lblUploadPointsOverrideFieldsFormat" CssClass="toggle" runat="server" /></asp:HyperLink>
                    <ajaxToolkit:CollapsiblePanelExtender ID="uploadOverrideFieldsCollapsiblePanelExtender" runat="Server" TargetControlID="pnlPointsOverrideFields" CollapsedSize="0" Collapsed="true" ExpandControlID="lnkUploadPointsOverrideFieldsFormat" CollapseControlID="lnkUploadPointsOverrideFieldsFormat" AutoCollapse="false" AutoExpand="false" ExpandDirection="Vertical" CollapsedText="show upload points override fields format" ExpandedText="hide upload points override fields format" TextLabelID="lnkUploadPointsOverrideFieldsFormat" />

                    <asp:Panel ID="pnlPointsOverrideFields" CssClass="richText" runat="server">              
                      <asp:Literal ID="litUploadPointsOverrideFieldList" runat="server" />
                    </asp:Panel>
                    <br />
                    
                    <div class="downloadDiv">
                        <span>Download Template:</span> <asp:ImageButton ID="imgDownloadFileTemplate" runat="server" ImageUrl="~/_assets/images/csv-icon.jpg" OnClick="DownloadFileTemplateForPoints_Click" AlternateText="download" CausesValidation="false" />
                    </div>
                    <div class="downloadDiv">
                        <span>Download Override Template:</span> <asp:ImageButton ID="imgOverrideDownloadFileTemplate" runat="server" ImageUrl="~/_assets/images/csv-icon.jpg" OnClick="DownloadOverrideFileTemplateForPoints_Click" AlternateText="download" CausesValidation="false" />
                    </div>

                  </div>
                  <hr />

                  <h2>Upload:</h2>
                  <p><asp:Label ID="lblUploadPointsResult" CssClass="successMessage" runat="server" /></p>
                  <div>
                    <div class="divForm">
                      <label class="label">Culture:</label>
                      <asp:DropDownList CssClass="dropdown" ID="ddlCulturePointsUpload" runat="server" /> 
                    </div>
                    <div class="divForm">
                      <label class="label">Override Values:</label>
                      <asp:CheckBox ID="chkOverrideValues" CssClass="checkbox" runat="server" />
                      <span>(Excluding: EID)</span>
                    </div>
                    <div class="divForm">
                      <label class="label">*CSV File:</label>
                      <asp:FileUpLoad ID="csvPointsFileUpload" runat="server" />  
                    </div>  

                    <asp:LinkButton CssClass="lnkButton" ID="btnUploadPoints" runat="server" Text="Upload Points" OnClick="uploadPointsButton_Click" ValidationGroup="UploadPoints" /> 
                  </div>

                  <asp:RequiredFieldValidator ID="csvPointsFileUploadRequiredValidator" runat="server" ErrorMessage="No file selected." ControlToValidate="csvPointsFileUpload" SetFocusOnError="true" Display="None" ValidationGroup="UploadPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="csvPointsFileUploadRequiredValidatorExtender" runat="server" BehaviorID="csvPointsFileUploadRequiredValidatorExtender" TargetControlID="csvPointsFileUploadRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                    
                  <asp:RegularExpressionValidator ID="csvPointsFileUploadRegExValidator" runat="server" ErrorMessage="Upload csv only." ValidationExpression=".*(\.csv|.CSV)$" ControlToValidate="csvPointsFileUpload" SetFocusOnError="true" Display="None" ValidationGroup="UploadPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="csvPointsFileUploadRegExValidatorExtender" runat="server" BehaviorID="csvPointsFileUploadRegExValidatorExtender" TargetControlID="csvPointsFileUploadRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                        
                  <asp:HyperLink ID="lnkUploadPointsMessages" runat="server" CssClass="toggle"><asp:Label ID="lblUploadPointsMessages" CssClass="toggle" runat="server" /></asp:HyperLink>
                  <ajaxToolkit:CollapsiblePanelExtender ID="pointsMessagesCollapsiblePanelExtender" runat="Server" TargetControlID="pnlUploadPointsMessages" CollapsedSize="0" ExpandedSize="200" ExpandControlID="lnkUploadPointsMessages" CollapseControlID="lnkUploadPointsMessages" AutoCollapse="false" AutoExpand="false" ScrollContents="true" ExpandDirection="Vertical" CollapsedText="show messages" ExpandedText="hide messages" TextLabelID="lblUploadPointsMessages" />

                  <asp:Panel ID="pnlUploadPointsMessages" CssClass="richText" runat="server">
                    <p> 
                      <asp:Label ID="lblUploadPointsErrors" CssClass="errorMessage" runat="server" /><br />
                      <asp:Label ID="lblUploadPointsOverrides" CssClass="overrideMessage" runat="server" />
                    </p>
                  </asp:Panel> 
                      
                </div>    
              </ContentTemplate>

              <Triggers>
                <asp:PostBackTrigger ControlID="btnUploadPoints"/>                                                    
              </Triggers> 
            </asp:UpdatePanel>  
                 
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">   

            <asp:UpdatePanel ID="updatePanelNested2Upload" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">  
              <ContentTemplate>

                <div>
                  <h2>Upload Points With Point Typing</h2>
                    <div class="richText">
                    <p>Please make sure every row of the csv is in the following format below. Please click the download template button for template file with headers. The point name group will be removed from the dropdown after a confirmed add attempt.</p>
                    <asp:HyperLink ID="lnkUploadPointTypingFieldsFormat" runat="server" CssClass="toggle"><asp:Label ID="lblUploadPointTypingFieldsFormat" CssClass="toggle" runat="server" /></asp:HyperLink>
                    <ajaxToolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender5" runat="Server" TargetControlID="pnlPointTypingFields" CollapsedSize="0" Collapsed="true" ExpandControlID="lnkUploadPointTypingFieldsFormat" CollapseControlID="lnkUploadPointTypingFieldsFormat" AutoCollapse="false" AutoExpand="false" ExpandDirection="Vertical" CollapsedText="show upload point typing fields format" ExpandedText="hide upload point typing fields format" TextLabelID="lblUploadPointTypingFieldsFormat" />

                    <asp:Panel ID="pnlPointTypingFields" CssClass="richText" runat="server">              
                      <asp:Literal ID="litUploadPointTypingFieldList" runat="server" />
                    </asp:Panel>
                    <br />

                    <span>Download Template:</span> <asp:ImageButton ID="imgDownloadFileTemplatePointTyping" runat="server" ImageUrl="~/_assets/images/csv-icon.jpg" OnClick="DownloadFileTemplateForPointTyping_Click" AlternateText="download" CausesValidation="false" />
                  </div>

                  <asp:Panel ID="pnlPointTyping" runat="server">

                    <hr />
                    <h2>Pre-Upload:</h2>
                    <p><asp:Label ID="lblUploadPointTypingResult" CssClass="successMessage" runat="server" /></p>
                  
                    <div>
                      <div class="divForm">
                        <label class="label">Culture:</label>
                        <asp:DropDownList CssClass="dropdown" ID="ddlCulturePointTypingUpload" runat="server" /> 
                      </div>
                      <div class="divForm">
                        <label class="label">*CSV File:</label>
                        <asp:FileUpLoad ID="csvPointTypingFileUpload" runat="server" />  
                      </div>  

                      <asp:LinkButton CssClass="lnkButton" ID="btnUploadPointTypingPoints" runat="server" Text="Pre-Upload Points" OnClick="preUploadPointTypingPointsButton_Click" ValidationGroup="UploadPointTypingPoints" />    
                    </div>

                    <asp:RequiredFieldValidator ID="csvPointTypingFileUploadRequiredValidator" runat="server" ErrorMessage="No file selected." ControlToValidate="csvPointTypingFileUpload" SetFocusOnError="true" Display="None" ValidationGroup="UploadPointTypingPoints" />
                    <ajaxToolkit:ValidatorCalloutExtender ID="csvPointTypingFileUploadRequiredValidatorExtender" runat="server" BehaviorID="csvPointTypingFileUploadRequiredValidatorExtender" TargetControlID="csvPointTypingFileUploadRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                    
                    <asp:RegularExpressionValidator ID="csvPointTypingFileUploadRegExValidator" runat="server" ErrorMessage="Upload csv only." ValidationExpression=".*(\.csv|.CSV)$" ControlToValidate="csvPointTypingFileUpload" SetFocusOnError="true" Display="None" ValidationGroup="UploadPointTypingPoints" />
                    <ajaxToolkit:ValidatorCalloutExtender ID="csvPointTypingFileUploadRegExValidatorExtender" runat="server" BehaviorID="csvPointTypingFileUploadRegExValidatorExtender" TargetControlID="csvPointTypingFileUploadRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                    <asp:HyperLink ID="lnkUploadPointTypingMessages" runat="server" CssClass="toggle"><asp:Label ID="lblUploadPointTypingMessages" CssClass="toggle" runat="server" /></asp:HyperLink>
                    <ajaxToolkit:CollapsiblePanelExtender ID="pointTypingMessagesCollapsiblePanelExtender" runat="Server" TargetControlID="pnlUploadPointTypingMessages" CollapsedSize="0" ExpandedSize="200" ExpandControlID="lnkUploadPointTypingMessages" CollapseControlID="lnkUploadPointTypingMessages" AutoCollapse="false" AutoExpand="false" ScrollContents="true" ExpandDirection="Vertical" CollapsedText="show messages" ExpandedText="hide messages" TextLabelID="lblUploadPointTypingMessages" />

                    <asp:Panel ID="pnlUploadPointTypingMessages" CssClass="richText" runat="server">
                      <p> 
                        <asp:Label ID="lblUploadPointTypingErrors" CssClass="errorMessage" runat="server" /><br />
                        <asp:Label ID="lblUploadPointTypingOverrides" CssClass="overrideMessage" runat="server" />
                      </p>
                    </asp:Panel>

                  </asp:Panel>  
                </div>    
                

                <asp:Panel ID="pnlPointTypingConfirm" runat="server" Visible="false">
                  <hr />

                  <h2>Point Typing:</h2>
                   <p><asp:Label ID="lblConfirmPointTypingResult" CssClass="successMessage" runat="server" /></p>

                  <div class="divForm">   
                    <label class="label">*Equipment Type:</label>      
                    <asp:DropDownList ID="ddlPointTypingEquipmentType" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server" OnSelectedIndexChanged="ddlPointTypingEquipmentType_OnSelectedIndexChanged" />
                  </div>

                  <div class="divForm">
                    <label class="label">*Point Name Group:</label>
                    <extensions:ListBoxExtension ID="lbPointTypingPointNameGroup" runat="server" AutoPostBack="true" SelectionMode="Multiple" CssClass="listboxNarrowest" OnSelectedIndexChanged="lbPointTypingPointNameGroup_OnSelectedIndexChanged" />
                  </div>
                  <br />
                  
                  <div id="gridTbl">   
                    <asp:GridView ID="gridPointTypingPoints" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2">
                      <Columns>
                        <asp:BoundField DataField="PointName" HeaderText="Point Name" />
                        <asp:BoundField DataField="EquipmentName" HeaderText="Equipment Name" />
                        <asp:BoundField DataField="EID" HeaderText="EID" />
                        <asp:BoundField DataField="DSID" HeaderText="DSID" /> 
                      	<asp:BoundField DataField="ReferenceID" HeaderText="ReferenceID" />
						<asp:BoundField DataField="SamplingInterval" HeaderText="Sampling Interval" /> 
                      	<asp:BoundField DataField="RelPctError" HeaderText="RelPctError" /> 
                      	<asp:BoundField DataField="TimeZone" HeaderText="TimeZone" />
                        <asp:BoundField DataField="DateCreated" HeaderText="DateCreated" />  
                      </Columns>        
                    </asp:GridView>
                  </div>
                  <br />

                  <div class="divForm">   
                    <label class="label">*Point Class:</label>    
                    <asp:DropDownList ID="ddlPointTypingPointClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPointTypingPointClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                      <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                    </asp:DropDownList> 
                    <p>(Note: Only point classes that contain a point type with point enabled are shown.)</p>
                  </div>

                  <div class="divForm">   
                    <label class="label">*Point Type:</label>    
                    <asp:DropDownList ID="ddlPointTypingPointType" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                                                                
                      <asp:ListItem Value="-1">Select class first...</asp:ListItem>                             
                    </asp:DropDownList> 
                    <p>(Note: More than one of the same point type cannot be associated to a peice of equipment unless the point is an unassigned type.)</p>
                    <p>(Note: More than one of the same point type cannot be associated to a peice of equipment unless the equipment is a group class.)</p>
                  </div>

                  <div class="divForm">
                    <br />
                    <p>Point equation: multiplier*x^power+addition</p>
                    <label class="label">*Power (Storage) (Default=1):</label>
                    <asp:TextBox CssClass="textbox" ID="txtPointTypingPower" Text="1" runat="server" MaxLength="10" />
                  </div>

                  <div class="divForm">
                    <label class="label">*Multiply (Storage) (Default=1):</label>
                    <asp:TextBox CssClass="textbox" ID="txtPointTypingMultiply" Text="1" runat="server" MaxLength="10" />
                  </div>                                                     
                  
                  <div class="divForm">
                    <label class="label">*Addition (Storage) (Default=0):</label>
                    <asp:TextBox CssClass="textbox" ID="txtPointTypingAddition" Text="0" runat="server" MaxLength="10" />
                  </div>

                  <div class="divForm">
                    <label class="label">*Power (Analyses) (Default=1):</label>
                    <asp:TextBox CssClass="textbox" ID="txtPointTypingPowerMatlab" Text="1" runat="server" MaxLength="10" />
                  </div>

                  <div class="divForm">
                    <label class="label">*Multiply (Analyses) (Default=1):</label>
                    <asp:TextBox CssClass="textbox" ID="txtPointTypingMultiplyMatlab" Text="1" runat="server" MaxLength="10" />
                  </div>

                  <div class="divForm">
                    <label class="label">*Addition (Analyses) (Default=0):</label>
                    <asp:TextBox CssClass="textbox" ID="txtPointTypingAdditionMatlab" Text="0" runat="server" MaxLength="10" />
                  </div>

                  <asp:RequiredFieldValidator ID="addEquipmentTypeRequiredValidator" runat="server" ErrorMessage="Equipment Type is a required field." ControlToValidate="ddlPointTypingEquipmentType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentTypeRequiredValidatorExtender" runat="server" BehaviorID="addEquipmentTypeRequiredValidatorExtender" TargetControlID="addEquipmentTypeRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                  <asp:CustomValidator ID="addPointNameGroupCustomValidator" runat="server" ErrorMessage="Point Name Group is a required field." ControlToValidate="lbPointTypingPointNameGroup" SetFocusOnError="true" ValidationGroup="ConfirmPointTypingPoints" ValidateEmptyText="true" ClientValidationFunction="ListBoxCustomValidator" Display="None" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addPointNameGroupCustomValidatorExtender" runat="server" BehaviorID="addPointNameGroupCustomValidatorExtender" TargetControlID="addPointNameGroupCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                  <asp:RequiredFieldValidator ID="addPointClassRequiredValidator" runat="server" ErrorMessage="Point Class is a required field." ControlToValidate="ddlPointTypingPointClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addPointClassRequiredValidatorExtender" runat="server" BehaviorID="addPointClassRequiredValidatorExtender" TargetControlID="addPointClassRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                  <asp:RequiredFieldValidator ID="addPointTypeRequiredValidator" runat="server" ErrorMessage="Point Type is a required field." ControlToValidate="ddlPointTypingPointType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addPointTypeRequiredValidatorExtender" runat="server" BehaviorID="addPointTypeRequiredValidatorExtender" TargetControlID="addPointTypeRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                  <ajaxToolkit:FilteredTextBoxExtender ID="addPowerFilteredTextBoxExtender" runat="server" TargetControlID="txtPointTypingPower" FilterType="Custom, Numbers" ValidChars="-." />
                  <asp:RequiredFieldValidator ID="addPowerRequiredValidator" runat="server" ErrorMessage="Power is a required field." ControlToValidate="txtPointTypingPower" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addPowerRequiredValidatorExtender" runat="server" BehaviorID="addPowerRequiredValidatorExtender" TargetControlID="addPowerRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                  <asp:RegularExpressionValidator ID="addPowerRegExValidator" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\.?\d*$" ControlToValidate="txtPointTypingPower" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addPowerRegExValidatorExtender" runat="server" BehaviorID="addPowerRegExValidatorExtender" TargetControlID="addPowerRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />                     
                  
                  <ajaxToolkit:FilteredTextBoxExtender ID="addMultiplyFilteredTextBoxExtender" runat="server" TargetControlID="txtPointTypingMultiply" FilterType="Custom, Numbers" ValidChars="-." />
                  <asp:RequiredFieldValidator ID="addMultiplyRequiredValidator" runat="server" ErrorMessage="Multiply is a required field." ControlToValidate="txtPointTypingMultiply" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addMultiplyRequiredValidatorExtender" runat="server" BehaviorID="addMultiplyRequiredValidatorExtender" TargetControlID="addMultiplyRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                  <asp:RegularExpressionValidator ID="addMultiplyRegExValidator" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\.?\d*$" ControlToValidate="txtPointTypingMultiply" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addMultiplyRegExValidatorExtender" runat="server" BehaviorID="addMultiplyRegExValidatorExtender" TargetControlID="addMultiplyRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                      
                  <ajaxToolkit:FilteredTextBoxExtender ID="addAdditionFilteredTextBoxExtender" runat="server" TargetControlID="txtPointTypingAddition" FilterType="Custom, Numbers" ValidChars="-." />
                  <asp:RequiredFieldValidator ID="addAdditionRequiredValidator" runat="server" ErrorMessage="Addition is a required field." ControlToValidate="txtPointTypingAddition" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addAdditionRequiredValidatorExtender" runat="server" BehaviorID="addAdditionRequiredValidatorExtender" TargetControlID="addAdditionRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                  <asp:RegularExpressionValidator ID="addAdditionRegExValidator" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\.?\d*$" ControlToValidate="txtPointTypingAddition" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addAdditionRegExValidatorExtender" runat="server" BehaviorID="addAdditionRegExValidatorExtender" TargetControlID="addAdditionRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                         
                  <ajaxToolkit:FilteredTextBoxExtender ID="addPowerMatlabFilteredTextBoxExtender" runat="server" TargetControlID="txtPointTypingPowerMatlab" FilterType="Custom, Numbers" ValidChars="-." />
                  <asp:RequiredFieldValidator ID="addPowerMatlabRequiredValidator" runat="server" ErrorMessage="Power is a required field." ControlToValidate="txtPointTypingPowerMatlab" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addPowerMatlabRequiredValidatorExtender" runat="server" BehaviorID="addPowerMatlabRequiredValidatorExtender" TargetControlID="addPowerMatlabRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                  <asp:RegularExpressionValidator ID="addPowerMatlabRegExValidator" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\.?\d*$" ControlToValidate="txtPointTypingPowerMatlab" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addPowerMatlabRegExValidatorExtender" runat="server" BehaviorID="addPowerMatlabRegExValidatorExtender" TargetControlID="addPowerMatlabRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                  
                  <ajaxToolkit:FilteredTextBoxExtender ID="addMultiplyMatlabFilteredTextBoxExtender" runat="server" TargetControlID="txtPointTypingMultiplyMatlab" FilterType="Custom, Numbers" ValidChars="-." />
                  <asp:RequiredFieldValidator ID="addMultiplyMatlabRequiredValidator" runat="server" ErrorMessage="Multiply is a required field." ControlToValidate="txtPointTypingMultiplyMatlab" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addMultiplyMatlabRequiredValidatorExtender" runat="server" BehaviorID="addMultiplyMatlabRequiredValidatorExtender" TargetControlID="addMultiplyMatlabRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                  <asp:RegularExpressionValidator ID="addMultiplyMatlabRegExValidator" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\.?\d*$" ControlToValidate="txtPointTypingMultiplyMatlab" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addMultiplyMatlabRegExValidatorExtender" runat="server" BehaviorID="addMultiplyMatlabRegExValidatorExtender" TargetControlID="addMultiplyMatlabRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                  <ajaxToolkit:FilteredTextBoxExtender ID="addAdditionMatlabFilteredTextBoxExtender" runat="server" TargetControlID="txtPointTypingAdditionMatlab" FilterType="Custom, Numbers" ValidChars="-." />
                  <asp:RequiredFieldValidator ID="addAdditionMatlabRequiredValidator" runat="server" ErrorMessage="Addition is a required field." ControlToValidate="txtPointTypingAdditionMatlab" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addAdditionMatlabRequiredValidatorExtender" runat="server" BehaviorID="addAdditionMatlabRequiredValidatorExtender" TargetControlID="addAdditionMatlabRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                  <asp:RegularExpressionValidator ID="addAdditionMatlabRegExValidator" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\.?\d*$" ControlToValidate="txtPointTypingAdditionMatlab" SetFocusOnError="true" Display="None" ValidationGroup="ConfirmPointTypingPoints" />
                  <ajaxToolkit:ValidatorCalloutExtender ID="addAdditionMatlabRegExValidatorExtender" runat="server" BehaviorID="addAdditionMatlabRegExValidatorExtender" TargetControlID="addAdditionMatlabRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                  <asp:LinkButton CssClass="lnkButton" ID="btnPointTypingConfirm" runat="server" Text="Confirm/Add Points" OnClick="confirmPointTypingPointsButton_Click" ValidationGroup="ConfirmPointTypingPoints" />

                  <asp:HyperLink ID="lnkConfirmPointTypingMessages" runat="server" CssClass="toggle"><asp:Label ID="lblConfirmPointTypingMessages" CssClass="toggle" runat="server" /></asp:HyperLink>
                  <ajaxToolkit:CollapsiblePanelExtender ID="confirmPointTypingMessagesCollapsiblePanelExtender" runat="Server" TargetControlID="pnlConfirmPointTypingMessages" CollapsedSize="0" ExpandedSize="200" ExpandControlID="lnkConfirmPointTypingMessages" CollapseControlID="lnkConfirmPointTypingMessages" AutoCollapse="false" AutoExpand="false" ScrollContents="true" ExpandDirection="Vertical" CollapsedText="show messages" ExpandedText="hide messages" TextLabelID="lblConfirmPointTypingMessages" />

                  <asp:Panel ID="pnlConfirmPointTypingMessages" CssClass="richText" runat="server">
                    <p> 
                      <asp:Label ID="lblConfirmPointTypingErrors" CssClass="errorMessage" runat="server" /><br />
                      <asp:Label ID="lblConfirmPointTypingOverrides" CssClass="overrideMessage" runat="server" />
                    </p>
                  </asp:Panel>
                </asp:Panel>
              </ContentTemplate>

              <Triggers>
                <asp:PostBackTrigger ControlID="btnUploadPointTypingPoints"/>
                <asp:PostBackTrigger ControlID="btnPointTypingConfirm"/>
              </Triggers>
            </asp:UpdatePanel>               

          </telerik:RadPageView>

        </telerik:RadMultiPage>

      </div> <!--end of administrationControls div-->