﻿using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using System;
using System.Reflection;
using System.Text;
using CW.Utility.Web;

namespace CW.Website._controls.upload
{
    public partial class UploadFormat : SiteUserControl
    {
        #region properties

            public Type ParserType { get; set; }
            public Boolean DisplayOverrideFields { get; set; }
            public String UploadFileName { get; set; }
            public String UploadOverrideFileName { get; set; }

        #endregion

        #region events

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                lblFormatTitle.Text = "Format" + ":";
                lblFormatMessage.Text = "Please make sure every row of the csv is in the following format below. Please click the download template button for template file with headers.";

                pnlOverrideFieldsLinkAndFields.Visible = DisplayOverrideFields;
                pnlOverrideFileTemplate.Visible = DisplayOverrideFields;

                UploadHelper.SetDefaultCollapsiblePanelSettings(this, uploadFieldsCPE.ID);

                lblDownloadTemplate.Text = "Download Template" + ":";
                litUploadFieldList.Text = FileParserBase.FormatFieldNamesForOutput("<br />", "FieldList", GetFieldDefs("FieldDefs"));

                if (DisplayOverrideFields)
                {
                    UploadHelper.SetDefaultCollapsiblePanelSettings(this, uploadOverrideFieldsCPE.ID);

                    lblDownloadOverrideTemplate.Text = "Download Override Template" + ":";
                    litUploadOverrideFieldList.Text = FileParserBase.FormatFieldNamesForOutput("<br />", "FieldList", GetFieldDefs("FieldDefsOverride"));
                }
            }

            #region button events

                protected void DownloadFileTemplateClick(Object sender, EventArgs e)
                {
                    CreateDownloadFile(GetFieldDefs("FieldDefs"), UploadFileName);
                }

                protected void DownloadOverrideFileTemplateClick(Object sender, EventArgs e)
                {
                    CreateDownloadFile(GetFieldDefs("FieldDefsOverride"), UploadOverrideFileName);
                }

            #endregion

        #endregion

        #region methods

            private FileParserBase.FieldDefBase[] GetFieldDefs(String fieldName)
            {
                return (FileParserBase.FieldDefBase[])ParserType.GetField(fieldName, BindingFlags.Static | BindingFlags.Public).GetValue(null);
            }

            private void CreateDownloadFile(FileParserBase.FieldDefBase[] fieldDefs, String fileName)
            {
                var fieldNamesForFile = Encoding.UTF8.GetBytes(FileParserBase.FormatFieldNamesForOutput(",", "TemplateDownload", fieldDefs));

                var fileHandler = new DownloadRequestHandler.HandledFileAttacher();
                ((IFileAttacher)fileHandler).AttachForDownload((fileName != String.Empty) ? fileName : "UploadFileTemplate.csv", fieldNamesForFile);
            }

        #endregion
    }
}