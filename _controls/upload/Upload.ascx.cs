﻿using CW.Utility;
using CW.Utility.Web;
using CW.Website._controls.admin;
using CW.Website._framework;
using System;
using System.Collections.Generic;

namespace CW.Website._controls.upload
{
    public partial class Upload : SiteUserControl
    {
        #region fields

            private Object parser;
            private Boolean displayOverrideOption = true;

        #endregion

        #region properties

            public Type ParserType { get; set; }
            public String UploaderName { get; set; }
            public Boolean DisplayOverrideOption { get { return displayOverrideOption; } set { displayOverrideOption = value; } }
            public Boolean DisplayCultureDDL { get; set; }
            public String[] ExcludedFields { get; set; }
            public IClearCache UploadClearCache { get; set; }
            
        #endregion

        #region events

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                lblUpload.Text = "Upload" + ":";
                lblCulture.Text = "Culture" + ":";
                lblOverrideValues.Text = "Override Values" + ":";
                lblCSVFile.Text = "CSV File" + ":";

                UploadHelper.SetDefaultCollapsiblePanelSettings(this, messagesCPE.ID);

                if (DisplayCultureDDL)
                {
                    FileParserBase.BindCultures(ddlCulture);
                    pnlCulture.Visible = true;
                }

                pnlOverrideOption.Visible = DisplayOverrideOption;

                if (DisplayOverrideOption) lblExludedFields.Text = "(Excluding: " + String.Join(", and ", ExcludedFields) +")";
                
                btnUpload.Text = UploaderName;

                HideLabels();
            }

            #region button events

                protected void ButtonClick(Object sender, EventArgs e)
                {
                    CreateParser();
                    ExecuteParser();
                }

            #endregion

        #endregion

        #region methods

            private void CreateParser()
            {
                parser = Activator.CreateInstance(ParserType, new Object[] { csvFileUpload.FileContent, DataMgr, chkOverrideValues.Checked, LogMgr, new FileParserBase.UserInfo(siteUser.CID, siteUser.ClientName) });

               if (DisplayCultureDDL) ((FileParserBase)parser).TempSetUserCulture(Convert.ToInt32(ddlCulture.SelectedValue));
            }

            private void ExecuteParser()
            {
                var executor = new FileParserExecuter((FileParserBase)parser, UploaderName, chkOverrideValues.Checked, LogMgr);

                executor.ExecuteParseAndSave();

                DisplayMessages(executor.Messages, executor.BIDs);
            }

            private void HideLabels()
            {
                lblErrors.Visible = false;
                lblResult.Visible = false;
                lblOverrides.Visible = false;

                lblMessages.Visible = false;
                messagesCPE.Collapsed = true;
                messagesCPE.ClientState = "true";
            }

            private void DisplayMessages(FileParserMessages messages, IEnumerable<Int32> bids = null)
            {
                lblErrors.Text = lblOverrides.Text = lblResult.Text = String.Empty;

                lblMessages.Visible = true;
                messagesCPE.Collapsed = false;
                messagesCPE.ClientState = "false";

                if (messages.HasMessage(FileParserMessages.MessageType.Exception))
                {
                    lblErrors.Visible = true;
                    lblErrors.Text = messages.GetMessage(FileParserMessages.MessageType.Exception, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Error))
                {
                    lblErrors.Visible = true;
                    lblErrors.Text = messages.GetMessage(FileParserMessages.MessageType.Error, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Update))
                {
                    lblOverrides.Visible = true;
                    lblOverrides.Text = messages.GetMessage(FileParserMessages.MessageType.Update, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Insert))
                {
                    lblResult.Visible = true;
                    lblResult.Text = messages.GetMessage(FileParserMessages.MessageType.Insert, FileParserMessages.MessageFormat.Html);
                }

                //code for clearing cache
                if (messages.HasMessage(FileParserMessages.MessageType.Update) || messages.HasMessage(FileParserMessages.MessageType.Insert)) UploadClearCache.ClearCache(siteUser.CID, bids, siteUser.UID);
            }

        #endregion
    }
}