﻿using CW.Website._controls.admin;
using CW.Website._framework;
using System;

namespace CW.Website._controls.upload
{
    public abstract class UploadBase : SitePage
    {
        #region fields

            protected UploadHeader UploadHeader;
            protected UploadFormat UploadFormat;
            protected Upload Upload;

        #endregion

        #region properties

            #region abstract

                protected abstract String[] ExcludeFields { get; }

                protected abstract IClearCache ClearCache { get; }

            #endregion

        #endregion

        #region methods

            protected void WireUpUploadSettings(String uploaderName, 
                                                String uploaderDisplayName, 
                                                Type parserType, String uploadFileName, 
                                                String uploadOverrideFileName, 
                                                Boolean displayOverrideFields, 
                                                Boolean displayCultureDDL,
                                                Boolean displayOverrideOption = true)
            {
                UploadHeader.PageTitle = uploaderName;
                UploadHeader.UploaderName = uploaderName.ToLower(); //TODO, update to use db for localization

                UploadFormat.ParserType = parserType;
                UploadFormat.UploadFileName = uploadFileName;
                
                if (!String.IsNullOrWhiteSpace(uploadOverrideFileName)) UploadFormat.UploadOverrideFileName = uploadOverrideFileName;

                Upload.ParserType = parserType;
                UploadFormat.DisplayOverrideFields = displayOverrideFields;
                Upload.DisplayOverrideOption = displayOverrideOption;
                Upload.DisplayCultureDDL = displayCultureDDL;
                Upload.UploaderName = (!String.IsNullOrWhiteSpace(uploaderDisplayName)) ? uploaderDisplayName : uploaderName;
                Upload.UploadClearCache = ClearCache;
                
                if (ExcludeFields != null) Upload.ExcludedFields = ExcludeFields;
            }

        #endregion
    }
}