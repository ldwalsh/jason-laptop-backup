﻿using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Equipment;
using CW.Website._parsers;
using System;
using System.Collections.Generic;

namespace CW.Website._controls.upload
{
    public abstract class UploadEquipmentToEquipmentBase : UploadBase
    {
        #region properties

            #region overrides

                protected override String[] ExcludeFields { get { return null; } }

                protected override IClearCache ClearCache { get { return new ClearCacheEquipmentToEquipment(); } }

            #endregion

        #endregion

        #region events

            private void Page_Load()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");

                WireUpUploadSettings("Upload Equipment To Equipment", "Upload Equipment", typeof(EquipmentEquipmentParser), "UploadEquipmentToEquipmentTemplate.csv", String.Empty, false, false, false);
            }

        #endregion
    }

    public class ClearCacheEquipmentToEquipment : IClearCache
    {
        public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid) //uid not used here, just passed in as null
        {
            if (bids != null)
            {
                foreach (var bid in bids)
                    GridCacher.Clear(AdminUserControlGrid.CacheStore, EquipmentStats.CreateCacheKey(cid, bid));
            }
        }
    }
}