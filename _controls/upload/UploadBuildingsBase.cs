﻿using CW.Data;
using CW.Utility;
using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Buildings;
using CW.Website._parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.upload
{
    public abstract class UploadBuildingsBase : UploadBase
    {
        #region properties

            #region overrides

                protected override String[] ExcludeFields { get { return new String[] { BuildingsParser.FieldDefs.Where(_ => _.FieldName == PropHelper.G<Building>(b => b.BuildingName)).Select(f => f.DisplayName).First() }; } }

                protected override IClearCache ClearCache { get { return new ClearCacheBuildings(); } }

            #endregion

        #endregion

        #region events

            private void Page_Load()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");

                WireUpUploadSettings("Upload Buildings", String.Empty, typeof(BuildingsParser), "UploadBuildingTemplate.csv", String.Empty, false, true);
            }

        #endregion
    }

    public class ClearCacheBuildings : IClearCache
    {
        public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid) //bids list not used here, just passed in as null
        {
            var cacheStore = AdminUserControlGrid.CacheStore;

            //both KGS and non-KGS buildings admin pages are using the same cache, so just clearing one would do the trick.  Explicitly set here so programmer knows it affects both admin pages.
            GridCacher.Clear(cacheStore, ViewBuildings.CreateCacheKey(cid, uid));
            GridCacher.Clear(cacheStore, CW.Website._controls.admin.Buildings.nonkgs.ViewBuildings.CreateCacheKey(cid, uid));
            GridCacher.Clear(cacheStore, BuildingStats.CreateCacheKey(cid, uid));
        }
    }
}