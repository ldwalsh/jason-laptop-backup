﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UploadHeader.ascx.cs" Inherits="CW.Website._controls.upload.UploadHeader" %>

<h1><asp:Label ID="lblPageTitle" runat="server" /></h1>

<div class="richText">
  <asp:Label ID="lblPageMessage" runat="server" />
</div>

<div class="updateProgressDiv">
  <asp:UpdateProgress ID="updateProgressTop" runat="server">
    <ProgressTemplate>
     <img src="_assets/images/Pik4F3300.gif" alt="loading" />
     <span><asp:Label ID="lblLoadingText" runat="server" /></span>
    </ProgressTemplate>
  </asp:UpdateProgress>
</div>

<hr />