﻿using CW.Website._framework;
using System;
using System.Text;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload
{
    public partial class UploadHeader : SiteUserControl
    {
        #region properties

            public String PageTitle { get; set; }
            public String UploaderName { get; set; }
            public String CustomHeaderText { get; set; }

        #endregion

        #region events

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                lblPageTitle.Text = PageTitle;

                var sbPageText = new StringBuilder();

                sbPageText.AppendLine((!String.IsNullOrWhiteSpace(CustomHeaderText)) ? CustomHeaderText : String.Format("<p>The {0} page is designed to {1} to the system from a csv file.</p><br />", UploaderName, UploaderName));
                sbPageText.AppendLine("<p><b>WARNING:</b> Before uploading, if the excel or csv file was created in an encoding type other than <b>\"UTF-8\"</b> ");
                sbPageText.AppendLine("and contains special characters please perform the following steps to avoid question marks <b>\"?\"</b> in place of those special characters. ");
                sbPageText.AppendLine("In other odd cases, you may need to perform a find on that character in excel and replace with a manually typed or pasted character from outside of excel.</p>");
                sbPageText.AppendLine("<ol>");
                sbPageText.AppendLine("<li>Open the csv in notepad. Copy the contents. Close the file.</li>");
                sbPageText.AppendLine("<li>Open notepad from scratch, and paste in the contents.</li>");
                sbPageText.AppendLine("<li>If there are any fields with commas (,), be sure to wrap them in double quotes (\").  Unless using excel.</li>");
                sbPageText.AppendLine("<li>Select File, and Save As.</li>");
                sbPageText.AppendLine("<li>Before clicking the Save button, change the Encoding type to <b>\"UTF-8\"</b> from the dropdown next to the Save button.</li>");
                sbPageText.AppendLine("<li>Save as .txt file.</li>");
                sbPageText.AppendLine("<li>Rename extension as .csv and it should now be in an acceptable format for upload.</li>");
                sbPageText.AppendLine("</ol>");

                lblPageMessage.Text = sbPageText.ToString();

                var lblLoadingText = (Label)updateProgressTop.FindControl("lblLoadingText");

                if (lblLoadingText != null) lblLoadingText.Text = "Loading...";
            }

        #endregion
    }
}