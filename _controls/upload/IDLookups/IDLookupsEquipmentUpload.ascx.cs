﻿using CW.Utility.Web;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupsEquipmentUpload : IDLookupsControl
    {
        #region events

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, gridBuildingsCPE.ID);
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, gridEquipmentCPE.ID);
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, gridEquipmentTypesCPE.ID);
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, gridManufacturersCPE.ID);
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, gridManufacturerModelsCPE.ID);

                BindBuildings(ddlBuilding);
                BindEquipment(ddlEquipment);
                BindEquipmentTypes(ddlEquipmentType);
                BindManufacturers(ddlManufacturer);
            }

            #region ddl events

                protected void ddlBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetLabel(lblBuildingID, ddlBuilding);
                }

                protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetLabel(lblEquipmentID, ddlEquipment);
                }

                protected void ddlEquipmentType_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetLabel(lblEquipmentTypeID, ddlEquipmentType);
                }

                protected void ddlManufacturer_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindManufacturerModels(ddlManufacturerModel, Convert.ToInt32(ddlManufacturer.SelectedValue));
                    BindContacts(ddlContact, Convert.ToInt32(ddlManufacturer.SelectedValue));
                }

                protected void ddlManufacturerModel_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetLabel(lblManufacturerModelID, ddlManufacturerModel);
                }

                protected void ddlContact_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetLabel(lblContactID, ddlContact);
                }

            #endregion

        #endregion

        #region methods

            private void SetLabel(Label lbl, DropDownList ddl)
            {
                lbl.Text = "ID: " + ddl.SelectedValue;
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    var buildings = siteUser.VisibleBuildings;

                    BindDDL(ddl, "BuildingName", "BID", buildings, true);
                    BindGrid(gridBuildings, buildings);
                }

                private void BindEquipment(DropDownList ddl)
                {
                    var equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByCIDWithUploadDropdownFormat(siteUser.CID);

                    BindDDL(ddl, "EquipmentConcatinated", "EID", equipment, true);
                    BindGrid(gridEquipment, equipment.OrderBy(e => e.BuildingName).ThenBy(e => e.EquipmentName));
                }


                private void BindEquipmentTypes(DropDownList ddl)
                {
                    var equipmentTypes = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesWithUploadDropdownFormat();

                    BindDDL(ddl, "EquipmentTypeConcatinated", "EquipmentTypeID", equipmentTypes, true);
                    BindGrid(gridEquipmentTypes, equipmentTypes);
                }

                private void BindManufacturers(DropDownList ddl)
                {
                    var manufacturers = DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturers(true);

                    BindDDL(ddl, "ManufacturerName", "ManufacturerID", manufacturers, true);
                    BindGrid(gridManufacturers, manufacturers);
                    BindGrid(gridManufacturerModels, DataMgr.EquipmentManufacturerModelDataMapper.GetAllFullManufacturerModels());
                }

                private void BindManufacturerModels(DropDownList ddl, Int32 manufacturerID)
                {
                    var manufacturerModels = DataMgr.EquipmentManufacturerModelDataMapper.GetAllManufacturerModelsByManufacturerIDWithUploadDropdownFormat(manufacturerID);

                    BindDDL(ddl, "ManufacturerModelConcatinated", "ManufacturerModelID", manufacturerModels, true);
                }

                private void BindContacts(DropDownList ddl, Int32 manufacturerID)
                {
                    var contacts = DataMgr.EquipmentManufacturerContactDataMapper.GetAllManufacturerContactsByManufacturerIDWithUploadDropdownFormat(manufacturerID);

                    BindDDL(ddl, "ContactConcatinated", "ContactID", contacts, true);
                }

            #endregion

        #endregion
    }
}