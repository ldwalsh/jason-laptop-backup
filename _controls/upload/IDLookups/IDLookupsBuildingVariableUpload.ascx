﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupsBuildingVariableUpload.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupsBuildingVariableUpload" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupHeader.ascx" tagname="IDLookupHeader" tagprefix="CW" %>

<CW:IDLookupHeader ID="IDLookupHeader" runat="server" />

<asp:Panel ID="pnlIDLookup" runat="server">

  <div class="divForm">
    <label class="label"><asp:Literal ID="litBuildingVariables" runat="server" /></label>
    <asp:HyperLink ID="lnkGridLookup" runat="server" CssClass="toggle"><asp:Label ID="lblGridLookup" CssClass="toggle" runat="server" /></asp:HyperLink>
    <ajaxToolkit:CollapsiblePanelExtender ID="pnlGridCPE" runat="server" TargetControlID="pnlGrid" ExpandControlID="lnkGridLookup" CollapseControlID="lnkGridLookup" TextLabelID="lblGridLookup" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsBuildingVariableUpload_pnlGrid')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnlGrid" CssClass="richText" runat="server">
    <div id="gridTbl">
      <asp:GridView ID="gridBuildingVariables" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false"> 
        <HeaderStyle CssClass="tblTitle" />
        <RowStyle CssClass="tblCol1" />
        <AlternatingRowStyle CssClass="tblCol2" />
        <Columns>
          <asp:BoundField DataField="BuildingVariableDisplayName" HeaderText="BuildingVariableDisplayName" />
          <asp:BoundField DataField="BVID" HeaderText="BVID" />
          <asp:BoundField DataField="IPEngUnits" HeaderText="IPEngUnits" />
          <asp:BoundField DataField="SIEngUnits" HeaderText="SIEngUnits" />
        </Columns>
      </asp:GridView>
    </div>
  </asp:Panel>
  <br />

</asp:Panel>