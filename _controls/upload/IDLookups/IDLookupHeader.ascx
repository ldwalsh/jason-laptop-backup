﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupHeader.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupHeader" %>

<script type="text/javascript" src="_assets/scripts/upload.js"></script>

<h2>ID Lookup:</h2>

<asp:HyperLink ID="lnkIDLookup" runat="server" CssClass="toggle"><asp:Label ID="lblIDLookup" CssClass="toggle" runat="server" /></asp:HyperLink>
<ajaxToolkit:CollapsiblePanelExtender ID="idLookupsCPE" runat="Server" TargetControlID="pnlIDLookup" ExpandControlID="lnkIDLookup" CollapseControlID="lnkIDLookup" CollapsedText="show id lookup" ExpandedText="hide id lookup" TextLabelID="lblIDLookup" />