﻿using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupHeader : SiteUserControl
    {
        #region events

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, idLookupsCPE.ID);
            }

        #endregion
    }
}