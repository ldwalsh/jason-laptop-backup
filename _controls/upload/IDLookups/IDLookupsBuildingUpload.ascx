﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupsBuildingUpload.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupsBuildingUpload" %>

<%@ Register src="~/_controls/upload/IDLookups/IDLookupHeader.ascx" tagname="IDLookupHeader" tagprefix="CW" %>

<CW:IDLookupHeader ID="IDLookupHeader" runat="server" />

<asp:Panel ID="pnlIDLookup" runat="server">

  <div class="divForm">
    <label class="label">CountryAlph2Code:</label>  
    <asp:DropDownList ID="ddlCountryAlpha2Code" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlCountryAlpha2Code_OnSelectedIndexChanged" />
    <asp:Label runat="server" ID="lblCountryAlpha2Code" Visible="true" Text="Code:" />
  </div>

  <div class="divForm">
    <label class="label">State:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlState" AppendDataBoundItems="true" AutoPostBack="true" runat="server"  OnSelectedIndexChanged="ddlState_OnSelectedIndexChanged">  
      <asp:ListItem Value="-1">Select country first...</asp:ListItem>                             
    </asp:DropDownList>              
    <asp:Label runat="server" ID="lblStateID" Visible="true" Text="ID:" />
  </div>

  <div class="divForm">
    <label class="label">Time Zones:</label>                  
    <asp:DropDownList ID="ddlTimeZone" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlTimeZone_OnSelectedIndexChanged" />
    <asp:Label runat="server" ID="lblTimeZone" Visible="true" Text="Zone:" />            
  </div>

  <div class="divForm">
    <label class="label">Building Type:</label>    
    <asp:DropDownList ID="ddlBuildingType" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBuildingType_OnSelectedIndexChanged" />
    <asp:Label runat="server" ID="lblBuildingTypeID" Visible="true" Text="ID:" />
    <br />

    <asp:HyperLink ID="lnk2" runat="server" CssClass="toggle"><asp:Label ID="lbl2" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender2" runat="Server" TargetControlID="pnl2" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk2" CollapseControlID="lnk2" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl2" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsBuildingUpload_pnl2')">copy grid (ie only)</a>        
  </div>

  <asp:Panel ID="pnl2" CssClass="richText" runat="server">              
    <div id="gridTbl">
      <asp:GridView ID="gridBuildingTypes" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="BuildingTypeName" HeaderText="BuildingTypeName" />
          <asp:BoundField DataField="BuildingTypeID" HeaderText="BuildingTypeID" />                                                 
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>

  <div class="divForm">
    <label class="label">Builiding Ownership:</label>                                                  
    <asp:HyperLink ID="lnk3" runat="server" CssClass="toggle"><asp:Label ID="lbl3" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender3" runat="Server" TargetControlID="pnl3" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk3" CollapseControlID="lnk3" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl3" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsBuildingUpload_pnl3')">copy grid (ie only)</a>            
  </div>

  <asp:Panel ID="pnl3" CssClass="richText" runat="server">
    <div id="gridTbl">   
      <asp:GridView ID="gridBuildingOwnerships" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="BuildingOwnershipItems" HeaderText="Building Ownership" />                                             
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>
                            
  <div class="divForm">           
    <label class="label">Building Shape:</label>                
    <asp:HyperLink ID="lnk4" runat="server" CssClass="toggle"><asp:Label ID="lbl4" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender4" runat="Server" TargetControlID="pnl4" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk4" CollapseControlID="lnk4" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl4" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsBuildingUpload_pnl4')">copy grid (ie only)</a>            
  </div>

  <asp:Panel ID="pnl4" CssClass="richText" runat="server">  
    <div id="gridTbl">   
      <asp:GridView ID="gridBuildingShapes" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="BuildingShapeItems" HeaderText="Building Shape" />                                             
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>

  <div class="divForm">               
    <label class="label">Main Cooling System:</label>            
    <asp:HyperLink ID="lnk5" runat="server" CssClass="toggle"><asp:Label ID="lbl5" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender5" runat="Server" TargetControlID="pnl5" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk5" CollapseControlID="lnk5" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl5" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsBuildingUpload_pnl5')">copy grid (ie only)</a>        
  </div>

  <asp:Panel ID="pnl5" CssClass="richText" runat="server">              
    <div id="gridTbl">   
      <asp:GridView ID="gridMainCoolingSystems" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="MainCoolingSystemItems" HeaderText="Main Cooling System" />                                             
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>  

  <div class="divForm">
    <label class="label">Main Heating System:</label>            
    <asp:HyperLink ID="lnk6" runat="server" CssClass="toggle"><asp:Label ID="lbl6" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender6" runat="Server" TargetControlID="pnl6" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk6" CollapseControlID="lnk6" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl6" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsBuildingUpload_pnl6')">copy grid (ie only)</a>            
  </div>

  <asp:Panel ID="pnl6" CssClass="richText" runat="server">
    <div id="gridTbl">   
      <asp:GridView ID="gridMainHeatingSystems" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="MainHeatingSystemItems" HeaderText="Main Heating System" />                                             
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>  

  <div class="divForm">               
    <label class="label">Roof Construction:</label>            
    <asp:HyperLink ID="lnk7" runat="server" CssClass="toggle"><asp:Label ID="lbl7" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender7" runat="Server" TargetControlID="pnl7" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk7" CollapseControlID="lnk7" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl7" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsBuildingUpload_pnl7')">copy grid (ie only)</a>        
  </div>

  <asp:Panel ID="pnl7" CssClass="richText" runat="server">
    <div id="gridTbl">
      <asp:GridView ID="gridRoofConstruction" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="RoofConstructionItems" HeaderText="Roof Construction" />                                             
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>  

</asp:Panel>