﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupsBMSInfoUpload.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupBMSInfoUpload" %>

<%@ Register src="~/_controls/upload/IDLookups/IDLookupHeader.ascx" tagname="IDLookupHeader" tagprefix="CW" %>

<CW:IDLookupHeader ID="IDLookupHeader" runat="server" />

<asp:Panel ID="pnlIDLookup" runat="server">

  <div class="divForm">
    <label class="label">BMS Info Name:</label>  
    <asp:DropDownList ID="ddlBmsInfoName2ID" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBmsInfoName2ID_OnSelectedIndexChanged" />
    <asp:Label runat="server" ID="lblBmsInfoName2ID" Visible="true" Text="BMSInfoID:" />
  </div>

</asp:Panel>