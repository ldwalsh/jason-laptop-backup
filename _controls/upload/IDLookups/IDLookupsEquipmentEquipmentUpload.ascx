﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupsEquipmentEquipmentUpload.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupsEquipmentEquipmentUpload" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupHeader.ascx" tagname="IDLookupHeader" tagprefix="CW" %>

<CW:IDLookupHeader ID="IDLookupHeader" runat="server" />

<asp:Panel ID="pnlIDLookup" runat="server">

 <div class="divForm">
    <label class="label"><asp:Literal ID="litBuilding" runat="server" /></label>
    <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" />
    <asp:Label runat="server" ID="lblBID" Visible="true" Text="ID:" />
    <br />

    <asp:HyperLink ID="lnkToggle" runat="server" CssClass="toggle"><asp:Label ID="lblToggle" CssClass="toggle" runat="server" /></asp:HyperLink>
    <ajaxToolkit:CollapsiblePanelExtender ID="gridBuildingEquipmentCPE" runat="Server" TargetControlID="pnlBuildingEquipment" ExpandControlID="lnkToggle" CollapseControlID="lnkToggle" TextLabelID="lblToggle" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsEquipmentEquipmentUpload_pnlBuildingEquipment')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnlBuildingEquipment" CssClass="richText" runat="server">
    <div id="gridTbl">
      <asp:GridView ID="gridBuildingEquipment" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="Building.BuildingName" HeaderText="BuildingName" />
          <asp:BoundField DataField="EquipmentName" HeaderText="EquipmentName" />
          <asp:BoundField DataField="EID" HeaderText="EID" />
          <asp:BoundField DataField="EquipmentTypeID" HeaderText="EquipmentTypeID" />
        </Columns>
      </asp:GridView>
    </div>
  </asp:Panel>

</asp:Panel>