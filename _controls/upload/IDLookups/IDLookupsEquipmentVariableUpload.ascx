﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupsEquipmentVariableUpload.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupsEquipmentVariableUpload" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupHeader.ascx" tagname="IDLookupHeader" tagprefix="CW" %>

<CW:IDLookupHeader ID="IDLookupHeader" runat="server" />

<asp:Panel ID="pnlIDLookup" runat="server">

  <div class="divForm">
    <label class="label"><asp:Literal ID="litEquipmentVariables" runat="server" /></label>
    <asp:HyperLink ID="lnkGridLookup" runat="server" CssClass="toggle"><asp:Label ID="lblGridLookup" CssClass="toggle" runat="server" /></asp:HyperLink>
    <ajaxToolkit:CollapsiblePanelExtender ID="pnlGridCPE" runat="Server" TargetControlID="pnlGrid" ExpandControlID="lnkGridLookup" CollapseControlID="lnkGridLookup" TextLabelID="lblGridLookup" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsEquipmentVariableUpload_pnlGrid')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnlGrid" CssClass="richText" runat="server">
    <div id="gridTbl">
      <asp:GridView ID="gridEquipmentVariables" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false">
        <HeaderStyle CssClass="tblTitle" />
        <RowStyle CssClass="tblCol1" />
        <AlternatingRowStyle CssClass="tblCol2" />
        <Columns>
          <asp:BoundField DataField="EquipmentVariableDisplayName" HeaderText="EquipmentVariableDisplayName" />
          <asp:BoundField DataField="EVID" HeaderText="EVID" />
          <asp:BoundField DataField="IPEngUnitID" HeaderText="IPEngUnitID" />
          <asp:BoundField DataField="SIEngUnitID" HeaderText="SIEngUnitID" />
        </Columns>
      </asp:GridView>
    </div>
  </asp:Panel>
  <br />

</asp:Panel>