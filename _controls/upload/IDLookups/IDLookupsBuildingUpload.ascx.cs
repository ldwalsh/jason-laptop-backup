﻿using CW.Common.Constants;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupsBuildingUpload : IDLookupsControl
    {
        #region events

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                BindBuildingTypes(ddlBuildingType);
                BindTimeZones(ddlTimeZone);
                BindCountries(ddlCountryAlpha2Code);
                BindBuildingOwnerships();
                BindBuildingShapes();
                BindMainCoolingSystems();
                BindMainHeatingSystems();
                BindRoofConstruction();
            }

            #region ddl events

                protected void ddlBuildingType_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    lblBuildingTypeID.Text = "ID: " + ddlBuildingType.SelectedValue.ToString();
                }

                protected void ddlTimeZone_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    lblTimeZone.Text = "Zone: " + ddlTimeZone.SelectedValue.ToString();
                }

                protected void ddlCountryAlpha2Code_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindStates(ddlState, ddlCountryAlpha2Code.SelectedValue);

                    lblCountryAlpha2Code.Text = "Code: " + ddlCountryAlpha2Code.SelectedValue.ToString();
                }

                protected void ddlState_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    lblStateID.Text = "ID: " + ddlState.SelectedValue.ToString();
                }

            #endregion

        #endregion

        #region methods

            #region binders

                private void BindBuildingTypes(DropDownList ddl)
                {
                    var buildingTypes = DataMgr.BuildingTypeDataMapper.GetAllBuildingTypes();

                    BindDDL(ddl, "BuildingTypeName", "BuildingTypeID", buildingTypes, true);
                    BindGrid(gridBuildingTypes, buildingTypes);
                }

                private void BindTimeZones(DropDownList ddl)
                {
                    BindDDL(ddl, "DisplayName", "Id", TimeZoneInfo.GetSystemTimeZones(), true);
                }

                private void BindCountries(DropDownList ddl)
                {
                    BindDDL(ddl, "CountryName", "Alpha2Code", DataMgr.CountryDataMapper.GetAllCountries(), true);
                }

                private void BindStates(DropDownList ddl, String alpha2Code)
                {
                    BindDDL(ddl, "StateName", "StateID", DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code), true);
                }

                private void BindBuildingOwnerships()
                {
                    var buildingOwnershipItems = (from BuildingOwnershipItem in BusinessConstants.Building.BuildingOwnershipList select new { BuildingOwnershipItems = BuildingOwnershipItem }).ToList();

                    BindGrid(gridBuildingOwnerships, buildingOwnershipItems);
                }

                private void BindBuildingShapes()
                {
                    var buildingShapeItems = (from BuildingShapeItem in BusinessConstants.Building.BuildingShapesList select new { BuildingShapeItems = BuildingShapeItem }).ToList();

                    BindGrid(gridBuildingShapes, buildingShapeItems);
                }

                private void BindMainCoolingSystems()
                {
                    var mainCoolingSystemItems = (from MainCoolingSystemItem in BusinessConstants.Building.MainCoolingSystemList select new { MainCoolingSystemItems = MainCoolingSystemItem }).ToList();

                    BindGrid(gridMainCoolingSystems, mainCoolingSystemItems);
                }

                private void BindMainHeatingSystems()
                {
                    var mainHeatingSystemItems = (from MainHeatingSystemItem in BusinessConstants.Building.MainHeatingSystemList select new { MainHeatingSystemItems = MainHeatingSystemItem }).ToList();

                    BindGrid(gridMainHeatingSystems, mainHeatingSystemItems);
                }

                private void BindRoofConstruction()
                {
                    var roofConstructionItems = (from RoofConstructionItem in BusinessConstants.Building.RoofConstructionList select new { RoofConstructionItems = RoofConstructionItem }).ToList();

                    BindGrid(gridRoofConstruction, roofConstructionItems);
                }

            #endregion

        #endregion
    }
}