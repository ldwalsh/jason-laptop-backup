﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupsEquipmentUpload.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupsEquipmentUpload" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupHeader.ascx" tagname="IDLookupHeader" tagprefix="CW" %>

<CW:IDLookupHeader ID="IDLookupHeader" runat="server" />

<asp:Panel ID="pnlIDLookup" runat="server">

  <div class="divForm">
    <label class="label">Building:</label>
    <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" />
    <asp:Label runat="server" ID="lblBuildingID" Visible="true" Text="ID:"></asp:Label>
    <br />

    <asp:HyperLink ID="lnk1" runat="server" CssClass="toggle"><asp:Label ID="lbl1" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="gridBuildingsCPE" runat="Server" TargetControlID="pnl1" ExpandControlID="lnk1" CollapseControlID="lnk1" TextLabelID="lbl1" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsEquipmentUpload_pnl1')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnl1" CssClass="richText" runat="server">
    <div id="gridTbl">                     
      <asp:GridView ID="gridBuildings" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="BuildingName" HeaderText="BuildingName" />
          <asp:BoundField DataField="BID" HeaderText="BID" />
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>

  <div class="divForm">   
    <label class="label">Equipment:</label>    
    <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" />
    <asp:Label runat="server" ID="lblEquipmentID" Visible="true" Text="ID:" />
    <br />

    <asp:HyperLink ID="hlEquipment" runat="server" CssClass="toggle"><asp:Label ID="lblEquipment" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="gridEquipmentCPE" runat="Server" TargetControlID="pnlEquipment" CollapsedSize="0" Collapsed="true" ExpandControlID="hlEquipment" CollapseControlID="hlEquipment" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lblEquipment" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_pnl2')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnlEquipment" CssClass="richText" runat="server">
    <div id="gridTbl">   
      <asp:GridView ID="gridEquipment" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="BuildingName" HeaderText="BuildingName" />
          <asp:BoundField DataField="EquipmentName" HeaderText="EquipmentName" />
          <asp:BoundField DataField="EID" HeaderText="EID" />
          <asp:BoundField DataField="EquipmentTypeID" HeaderText="EquipmentTypeID" />
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>
                            
  <div class="divForm">
    <label class="label">Equipment Type:</label>                    
    <asp:DropDownList ID="ddlEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlEquipmentType_OnSelectedIndexChanged" />  
    <asp:Label runat="server" ID="lblEquipmentTypeID" Visible="true" Text="ID:" />
    <br />

    <asp:HyperLink ID="lnk2" runat="server" CssClass="toggle"><asp:Label ID="lbl2" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="gridEquipmentTypesCPE" runat="Server" TargetControlID="pnl2" ExpandControlID="lnk2" CollapseControlID="lnk2" TextLabelID="lbl2" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsEquipmentUpload_pnl2')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnl2" CssClass="richText" runat="server">
    <div id="gridTbl">
      <asp:GridView ID="gridEquipmentTypes" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="EquipmentTypeName" HeaderText="EquipmentTypeName" />
          <asp:BoundField DataField="EquipmentTypeID" HeaderText="EquipmentTypeID" />
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>

  <div class="divForm">
    <label class="label">Manufacturer:</label>                  
    <asp:DropDownList ID="ddlManufacturer" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlManufacturer_OnSelectedIndexChanged" />
    <br />

    <asp:HyperLink ID="lnk3" runat="server" CssClass="toggle"><asp:Label ID="lbl3" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="gridManufacturersCPE" runat="Server" TargetControlID="pnl3" ExpandControlID="lnk3" CollapseControlID="lnk3" TextLabelID="lbl3" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsEquipmentUpload_pnl3')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnl3" CssClass="richText" runat="server">
    <div id="gridTbl">                    
      <asp:GridView ID="gridManufacturers" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="ManufacturerName" HeaderText="ManufacturerName" />
          <asp:BoundField DataField="ManufacturerID" HeaderText="ManufacturerID" />
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>

  <div class="divForm">
    <label class="label">Manufacturer Model:</label>                   
    <asp:DropDownList ID="ddlManufacturerModel" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true"  runat="server" OnSelectedIndexChanged="ddlManufacturerModel_OnSelectedIndexChanged">                                     
      <asp:ListItem Value="-1" >Select manufacturer first...</asp:ListItem> 
    </asp:DropDownList>
    <asp:Label runat="server" ID="lblManufacturerModelID" Visible="true" Text="ID:"></asp:Label>
    <br />

    <asp:HyperLink ID="lnk4" runat="server" CssClass="toggle"><asp:Label ID="lbl4" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="gridManufacturerModelsCPE" runat="Server" TargetControlID="pnl4" ExpandControlID="lnk4" CollapseControlID="lnk4" TextLabelID="lbl4" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsEquipmentUpload_pnl4')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnl4" CssClass="richText" runat="server">
    <div id="gridTbl">                    
      <asp:GridView ID="gridManufacturerModels" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="ManufacturerName" HeaderText="ManufacturerName" />
          <asp:BoundField DataField="ManufacturerModelName" HeaderText="ManufacturerModelName" />
          <asp:BoundField DataField="ManufacturerModelID" HeaderText="ManufacturerModelID" />
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>

  <div class="divForm">
    <label class="label">Contact:</label>                 
    <asp:DropDownList ID="ddlContact" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlContact_OnSelectedIndexChanged">
      <asp:ListItem Value="-1" >Select manufacturer first...</asp:ListItem> 
    </asp:DropDownList>
    <asp:Label runat="server" ID="lblContactID" Visible="true" Text="ID:" />
  </div>

</asp:Panel>