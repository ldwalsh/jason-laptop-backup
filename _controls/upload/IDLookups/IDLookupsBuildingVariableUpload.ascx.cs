﻿using CW.Utility;
using CW.Utility.Web;
using System;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupsBuildingVariableUpload : IDLookupsControl
    {
        #region events

            private void Page_FirstLoad()
            {
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, pnlGridCPE.ID);
                litBuildingVariables.Text = "Building Variables" + ":";
                BindGrid(gridBuildingVariables, DataMgr.BuildingVariableDataMapper.GetAllFullBuildingVariables());
            }

        #endregion
    }
}