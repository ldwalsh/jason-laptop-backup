﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupBMSInfoUpload : IDLookupsControl
    {
        protected void Page_FirstLoad(object sender, EventArgs e)
        {
            BindBMSInfoNames(ddlBmsInfoName2ID);
        }

        protected void ddlBmsInfoName2ID_OnSelectedIndexChanged(Object sender, EventArgs e)
        {          

            lblBmsInfoName2ID.Text = "BMSInfoID: " + ddlBmsInfoName2ID.SelectedValue.ToString();            
        }

        private void BindBMSInfoNames(DropDownList ddl)
        {
            BindDDL(ddl, "BMSInfoName", "BMSInfoID", DataMgr.BMSInfoDataMapper.GetBMSInfoByClientID(null, siteUser.CID), true);            
        }
    }
}