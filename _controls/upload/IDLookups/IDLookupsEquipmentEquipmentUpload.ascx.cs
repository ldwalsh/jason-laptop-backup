﻿using CW.Utility.Web;
using System;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupsEquipmentEquipmentUpload : IDLookupsControl
    {
        #region events

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, gridBuildingEquipmentCPE.ID);

                litBuilding.Text = "Building" + ":";

                BindBuildings(ddlBuilding);
            }

            #region ddl events

                protected void ddlBuilding_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    lblBID.Text = "ID: " + ddlBuilding.SelectedValue.ToString();

                    BindBuildingEquipmentGrid(Convert.ToInt32(ddlBuilding.SelectedValue));
                }

            #endregion

        #endregion

        #region methods

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID), true);
                }

                private void BindBuildingEquipmentGrid(Int32 bid)
                {
                    BindGrid(gridBuildingEquipment, DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid, (b => b.Building)));
                }

            #endregion

        #endregion
    }
}