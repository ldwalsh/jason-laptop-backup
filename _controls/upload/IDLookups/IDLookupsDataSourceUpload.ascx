﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupsDataSourceUpload.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupsDataSourceUpload" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupHeader.ascx" tagname="IDLookupHeader" tagprefix="CW" %>

<CW:IDLookupHeader ID="IDLookupHeader" runat="server" />

<asp:Panel ID="pnlIDLookup" runat="server">

  <div class="divForm">
    <label class="label">Vendor:</label>
    <asp:DropDownList ID="ddlVendor" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlVendor_OnSelectedIndexChanged" />
  </div>

  <div class="divForm">
    <label class="label">Vendor Product:</label>
    <asp:DropDownList ID="ddlVendorProduct" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlVendorProduct_OnSelectedIndexChanged">
      <asp:ListItem Value="-1">Select vendor first...</asp:ListItem>  
    </asp:DropDownList>
    <asp:Label runat="server" ID="lblVendorProductID" Text="ID:" />
  </div>

  <div class="divForm">
    <label class="label">Vendor Contact:</label>
    <asp:DropDownList ID="ddlVendorContact" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlVendorContact_OnSelectedIndexChanged">
      <asp:ListItem Value="-1">Select vendor product first...</asp:ListItem>  
    </asp:DropDownList>
    <asp:Label runat="server" ID="lblVendorContactID" Text="ID:" />
  </div>

  <div class="divForm">
    <label class="label">Retrieval Method:</label>
    <asp:DropDownList ID="ddlRetrievalMethod" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlRetrievalMethod_OnSelectedIndexChanged">
      <asp:ListItem Value="-1">Select vendor product first...</asp:ListItem>
    </asp:DropDownList>
    <asp:Label runat="server" ID="lblRetrievalMethodID" Text="ID:" />
  </div>

  <div class="divForm">
    <label class="label">Data Transfer Service:</label>
    <asp:DropDownList ID="ddlDataTransferService" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlDataTransferService_OnSelectedIndexChanged" />
    <asp:Label runat="server" ID="lblDataTransferServiceId" Text="ID:" />
  </div>

  <div class="divForm">
    <label class="label">BMS Info:</label>
    <asp:DropDownList ID="ddlBMSInfo" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBMSInfo_OnSelectedIndexChanged" />
    <asp:Label runat="server" ID="lblBMSInfoId" Text="ID:" />
      <br />

    <asp:HyperLink ID="lnk1" runat="server" CssClass="toggle"><asp:Label ID="lbl1" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="collapsiblePanelExtender1" runat="Server" TargetControlID="pnl1" CollapsedSize="0" Collapsed="true" ExpandControlID="lnk1" CollapseControlID="lnk1" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lbl1" CollapsedText="show full grid view" ExpandedText="hide full grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsBuildingUpload_pnl1')">copy grid (ie only)</a>        
  </div>

  <asp:Panel ID="pnl1" CssClass="richText" runat="server">              
    <div id="gridTbl">
      <asp:GridView ID="gridBMSInfo" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="BMSInfoName" HeaderText="BMSInfoName" />
          <asp:BoundField DataField="BMSInfoID" HeaderText="BMSInfoID" />                                                 
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>



</asp:Panel>