﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IDLookupsEquipmentPointUpload.ascx.cs" Inherits="CW.Website._controls.upload.IDLookups.IDLookupsEquipmentPointUpload" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupHeader.ascx" tagname="IDLookupHeader" tagprefix="CW" %>

<CW:IDLookupHeader ID="IDLookupHeader" runat="server" />

<asp:Panel ID="pnlIDLookup" runat="server">
  <div class="divForm">
    <label class="label">Building:</label>                  
    <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" />
    <br />

    <asp:HyperLink ID="lnk1" runat="server" CssClass="toggle"><asp:Label ID="lbl1" CssClass="toggle" runat="server" /></asp:HyperLink>                    
    <ajaxToolkit:CollapsiblePanelExtender ID="gridBuildingDataCPE" runat="Server" TargetControlID="pnlGridBuildingData" ExpandControlID="lnk1" CollapseControlID="lnk1" TextLabelID="lbl1" CollapsedText="show selected grid view" ExpandedText="hide selected grid view" />
    &nbsp;|&nbsp;&nbsp;<a class="link" onclick="CopyGridView('ctl00_plcCopy_IDLookupsEquipmentPointUpload_pnlGridBuildingData')">copy grid (ie only)</a>
  </div>

  <asp:Panel ID="pnlGridBuildingData" CssClass="richText" runat="server">
    <div id="gridTbl">   
      <asp:GridView ID="gridBuildingData" EnableViewState="true" runat="server" GridLines="None" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2"> 
        <Columns>
          <asp:BoundField DataField="BuildingName" HeaderText="BuildingName" />
          <asp:BoundField DataField="EquipmentName" HeaderText="EquipmentName" />
          <asp:BoundField DataField="EID" HeaderText="EID" />
          <asp:BoundField DataField="DSID" HeaderText="DSID" />
          <asp:BoundField DataField="PointName" HeaderText="PointName" />
          <asp:BoundField DataField="PID" HeaderText="PID" />
          <asp:BoundField DataField="PointTypeID" HeaderText="PointTypeID" />
          <asp:BoundField DataField="ReferenceID" HeaderText="PointReferenceID" />
        </Columns>        
      </asp:GridView>
    </div>
  </asp:Panel>

  <div class="divForm">
    <label class="label">Equipment:</label>
    <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged">
      <asp:ListItem Value="-1">Select building first...</asp:ListItem>
    </asp:DropDownList>                 
    <asp:Label runat="server" ID="lblEquipmentID" Visible="true" Text="ID:" />
  </div>

  <div class="divForm">
    <label class="label">Point:</label>
    <asp:DropDownList ID="ddlPoint" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlPoint_OnSelectedIndexChanged">
      <asp:ListItem Value="-1" >Select equipment first...</asp:ListItem> 
    </asp:DropDownList>
    <asp:Label runat="server" ID="lblPointID" Visible="true" Text="ID:" />
  </div>

</asp:Panel>