﻿using CW.Utility;
using CW.Utility.Web;
using System;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupsEquipmentPointUpload : IDLookupsControl
    {
        #region events

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, gridBuildingDataCPE.ID);

                BindBuildings(ddlBuilding);
            }

            #region ddl events

                protected void ddlBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindBuildingGrid(ddlBuilding, Convert.ToInt32(ddlBuilding.SelectedValue));
                    BindEquipment(ddlEquipment, Convert.ToInt32(ddlBuilding.SelectedValue));
                }

                protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindPoints(ddlPoint, Convert.ToInt32(ddlEquipment.SelectedValue));

                    lblEquipmentID.Text = "ID: " + ddlEquipment.SelectedValue.ToString();
                }

                protected void ddlPoint_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    lblPointID.Text = "ID: " + ddlPoint.SelectedValue.ToString();
                }

            #endregion

        #endregion

        #region methods

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID), true);
                }

                private void BindBuildingGrid(DropDownList ddl, Int32 bid)
                {
                    BindGrid(gridBuildingData, DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointsWithPartialDataByBID(bid));
                }

                private void BindEquipment(DropDownList ddl, Int32 bid)
                {
                    BindDDL(ddl, "EquipmentConcatinated", "EID", DataMgr.EquipmentDataMapper.GetAllEquipmentByBIDWithUploadDropdownFormat(bid), true);
                }

                private void BindPoints(DropDownList ddl, Int32 eid)
                {
                    BindDDL(ddl, "PointConcatinated", "PID", DataMgr.PointDataMapper.GetAllPointsByEIDWithUploadDropdownFormat(eid), true);
                }

            #endregion

        #endregion
    }
}