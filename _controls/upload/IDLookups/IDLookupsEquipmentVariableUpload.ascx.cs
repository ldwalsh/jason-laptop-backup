﻿using CW.Utility;
using CW.Utility.Web;
using System;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupsEquipmentVariableUpload : IDLookupsControl
    {
        #region events

            private void Page_FirstLoad()
            {
                UploadHelper.SetDefaultCollapsiblePanelSettings(this, pnlGridCPE.ID);
                litEquipmentVariables.Text = "Equipment Variables" + ":";
                BindGrid(gridEquipmentVariables, DataMgr.EquipmentVariableDataMapper.GetAllFullEquipmentVariables());
            }

        #endregion
    }
}