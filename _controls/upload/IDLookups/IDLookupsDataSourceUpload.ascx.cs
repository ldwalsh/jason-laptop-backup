﻿using System;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload.IDLookups
{
    public partial class IDLookupsDataSourceUpload : IDLookupsControl
    {
        #region properties

            int Cid => siteUser.CID;
        
        #endregion

        #region events

            protected void Page_FirstLoad(object sender, EventArgs e)
            {
                BindVendors(ddlVendor);
                BindDataTransferServices(ddlDataTransferService);
                BindBMSInfo(ddlBMSInfo);
            }

            #region ddl events

                protected void ddlVendor_OnSelectedIndexChanged(object sender, EventArgs e) => BindVendorProducts(ddlVendorProduct, int.Parse(ddlVendor.SelectedValue));
                
                protected void ddlVendorProduct_OnSelectedIndexChanged(object sender, EventArgs e)
                {
                    var vpid = ddlVendorProduct.SelectedValue;

                    SetLabel(lblVendorProductID, vpid.ToString());

                    BindVendorContacts(ddlVendorContact, Convert.ToInt32(ddlVendor.SelectedValue));

                    BindRetrievalMethods(ddlRetrievalMethod, int.Parse(vpid));
                }

                protected void ddlVendorContact_OnSelectedIndexChanged(object sender, EventArgs e) => SetLabel(lblVendorContactID, ddlVendorContact.SelectedValue.ToString());

                protected void ddlRetrievalMethod_OnSelectedIndexChanged(object sender, EventArgs e) => SetLabel(lblRetrievalMethodID, ddlRetrievalMethod.SelectedValue.ToString());

                protected void ddlDataTransferService_OnSelectedIndexChanged(object sender, EventArgs e) => SetLabel(lblDataTransferServiceId, ddlDataTransferService.SelectedValue.ToString());

                protected void ddlBMSInfo_OnSelectedIndexChanged(object sender, EventArgs e) => SetLabel(lblBMSInfoId, ddlBMSInfo.SelectedValue.ToString());

            #endregion

        #endregion

        #region methods

            #region binders

                void BindVendors(DropDownList ddl) => BindDDL(ddl, "VendorName", "VendorID", DataMgr.DataSourceVendorDataMapper.GetAllVendors(), true);
                
                void BindVendorProducts(DropDownList ddl, int vendorID) 
                => BindDDL
                   (
                        ddl, 
                        "VendorProductConcatinated", 
                        "VendorProductID", 
                        DataMgr.DataSourceVendorProductDataMapper.GetAllVendorProductsByVendorIDWithUploadDropdownFormat(vendorID), 
                        true
                   );
                
                void BindRetrievalMethods(DropDownList ddl, int vendorProductID)
                => BindDDL
                   (
                        ddl, 
                        "VendorProductRetrievalMethodConcatinated", 
                        "VendorProductRetrievalMethodID", 
                        DataMgr.DataSourceVendorProductDataMapper.GetAllDataSourceVendorProductsRetrievalMethodsByProductIDAsUploadDropdownFormat(vendorProductID), 
                        true
                   );
                
                void BindVendorContacts(DropDownList ddl, int vendorID)
                {
                    BindDDL(ddl, "ContactConcatinated", "ContactID", DataMgr.DataSourceVendorContactDataMapper.GetAllVendorContactsByVendorIDWithUploadDropdownFormat(vendorID), true);
                }

                void BindDataTransferServices(DropDownList ddl) => BindDDL(ddl, "DataTransferServiceName", "DTSID", DataMgr.DataTransferServiceDataMapper.GetAllActiveServicesByCID(Cid), true);

                void BindBMSInfo(DropDownList ddl)
                {
                    var bmsInfos = DataMgr.BMSInfoDataMapper.GetBMSInfoByClientID(null, Cid);

                    BindDDL(ddl, "BMSInfoName", "BMSInfoID", bmsInfos, true);
                    BindGrid(gridBMSInfo, bmsInfos);
                }
        
            #endregion

        #endregion
    }
}