﻿using CW.Data;
using CW.Utility;
using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Equipment;
using CW.Website._parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.upload
{
    public abstract class UploadEquipmentVariablesBase : UploadBase
    {
        #region properties

            #region overrides

                protected override String[] ExcludeFields
                {
                    get
                    {
                        return new String[] 
                        { 
                            EquipmentVariableParser.FieldDefs.Where(_ => _.FieldName == PropHelper.G<Equipment>(eq => eq.EID)).Select(f => f.DisplayName).First(),
                            EquipmentVariableParser.FieldDefs.Where(_ => _.FieldName == PropHelper.G<EquipmentVariable>(ev => ev.EquipmentVariableDisplayName)).Select(f => f.DisplayName).First()
                        };
                    }
                }

                protected override IClearCache ClearCache { get { return new ClearCacheEquipmentVariables(); } }

            #endregion

        #endregion

        #region events

            private void Page_Load()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");

                WireUpUploadSettings("Upload Equipment Variables", "Upload Variables", typeof(EquipmentVariableParser), "UploadEquipmentVariableTemplate.csv", String.Empty, false, false);
            }

        #endregion
    }

    public class ClearCacheEquipmentVariables : IClearCache
    {
        public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid) //uid not used here, just passed in as null
        {
            if (bids != null)
            {
                foreach (var bid in bids)
                    GridCacher.Clear(AdminUserControlGrid.CacheStore, EquipmentStats.CreateCacheKey(cid, bid));
            }
        }
    }
}