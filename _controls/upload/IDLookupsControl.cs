﻿using CW.Website._framework;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload
{
    public abstract class IDLookupsControl : SiteUserControl
    {
        #region methods

            protected void BindDDL(DropDownList ddl, string dataTextField, string dataValueField, object data, bool clearItems = false)
            {
                if (clearItems)
                {
                    ddl.Items.Clear();

                    ddl.Items.Add(new ListItem() { Text = "Select one...", Value = "-1", Selected = true });
                }

                ddl.DataTextField = dataTextField;

                ddl.DataValueField = dataValueField;

                ddl.DataSource = data;

                ddl.DataBind();
            }

            protected void BindGrid(GridView grid, object data)
            {
                grid.DataSource = data;

                grid.DataBind();
            }

            protected void SetLabel(Label lbl, string val) => lbl.Text = $"ID: {((val != "-1") ? val : string.Empty)}";

        #endregion
    }
}