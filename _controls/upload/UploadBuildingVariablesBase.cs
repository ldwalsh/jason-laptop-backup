﻿using CW.Data;
using CW.Utility;
using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Buildings;
using CW.Website._parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.upload
{
    public abstract class UploadBuildingVariablesBase : UploadBase
    {
        #region properties

            #region overrides

                protected override String[] ExcludeFields
                {
                    get
                    {
                        return new String[] 
                        { 
                            BuildingVariableParser.FieldDefs.Where(_ => _.FieldName == PropHelper.G<Building>(b => b.BID)).Select(f => f.DisplayName).First(),
                            BuildingVariableParser.FieldDefs.Where(_ => _.FieldName == PropHelper.G<BuildingVariable>(bv => bv.BuildingVariableDisplayName)).Select(f => f.DisplayName).First()
                        };
                    }
                }

                protected override IClearCache ClearCache { get { return new ClearCacheBuildingVariables(); } }

            #endregion

        #endregion

        #region events

            private void Page_Load()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");
                
                WireUpUploadSettings("Upload Building Variables", "Upload Variables", typeof(BuildingVariableParser), "UploadBuildingVariableTemplate.csv", String.Empty, false, false);                
            }

        #endregion
    }

    public class ClearCacheBuildingVariables : IClearCache
    {
        public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid) //bids list not used here, just passed in as null
        {
            GridCacher.Clear(AdminUserControlGrid.CacheStore, BuildingStats.CreateCacheKey(cid, Convert.ToInt32(uid)));
        }
    }
}