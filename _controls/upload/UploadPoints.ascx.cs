﻿using AjaxControlToolkit;
using CW.Data.Helpers;
using CW.Data.Models.Point;
using CW.Utility;
using CW.Utility.Web;
using CW.Reporting._fileservices;
using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Points;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using CW.Website._parsers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.upload
{
    public partial class UploadPoints : SiteUserControl
    {
        #region fields

            private ListItem listItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };
            private FileParserBase.UserInfo userInfo;

        #endregion

        #region properties

            private List<GetPointsDataUploadSerializedFormat> PointTypingPoints
            {
                get
                {
                    if (ViewState["PointTypingPoints"] == null)
                        ViewState["PointTypingPoints"] = new List<GetPointsDataUploadSerializedFormat>();

                    return (List<GetPointsDataUploadSerializedFormat>)ViewState["PointTypingPoints"];
                }
                set { this.ViewState["PointTypingPoints"] = value; }
            }
            public IClearCache UploadClearCache { get; set; }

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");
 
                UploadClearCache = new ClearCachePoints();

                HideLabelsAndExtenders(lblUploadPointsErrors, lblUploadPointsOverrides, lblUploadPointsResult, lblUploadPointsMessages, pointsMessagesCollapsiblePanelExtender);
                HideLabelsAndExtenders(lblUploadPointTypingErrors, lblUploadPointTypingOverrides, lblUploadPointTypingResult, lblUploadPointTypingMessages, pointTypingMessagesCollapsiblePanelExtender);
                HideLabelsAndExtenders(lblConfirmPointTypingErrors, lblConfirmPointTypingOverrides, lblConfirmPointTypingResult, lblConfirmPointTypingMessages, confirmPointTypingMessagesCollapsiblePanelExtender);

                if (!Page.IsPostBack)
                {
                    BindBuildings(ddlBuilding);
                    BindDataSources(ddlDataSource);
                    BindEquipment(ddlEquipment);
                    BindPointTypes(ddlPointType);
                    BindVAndVPrePointTypes(ddlVAndVPrePointType);
                    BindTimeZones(ddlTimeZone);
                    BindUploadPointsFieldsList();
                    BindUploadPointsOverrideFieldsList();
                    BindUploadPointTypingFieldsList();

                    FileParserBase.BindCultures(ddlCulturePointsUpload);
                    FileParserBase.BindCultures(ddlCulturePointTypingUpload);
                }
                //allow for postbacks in upload buttons             
                //Page.Form.Attributes.Add("enctype", "multipart/form-data");
                userInfo = new FileParserBase.UserInfo(siteUser.CID, siteUser.ClientName);
            }

            protected void Page_FirstLoad(Object sender, EventArgs e)
            {
                ViewState.Clear();

                BindPointClasses(ddlPointTypingPointClass);
            }

            #region ddl events

                protected void ddlLookup_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    var ddl = (DropDownList)sender;

                    DisplaySelectedValueItem(ddl);
                    UpdateSelectedGridView(ddl);
                }

                protected void ddlPointTypingEquipmentType_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindPointNameGroups(lbPointTypingPointNameGroup, Convert.ToInt32(ddlPointTypingEquipmentType.SelectedValue));
                }

                protected void ddlPointTypingPointClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindPointTypes(ddlPointTypingPointType, Convert.ToInt32(ddlPointTypingPointClass.SelectedValue));
                }

            #endregion

            #region button events

                protected void uploadPointsButton_Click(Object sender, EventArgs e)
                {
                    var parser = new PointsParser(csvPointsFileUpload.FileContent, DataMgr, chkOverrideValues.Checked, LogMgr, userInfo);
                    var executor = new FileParserExecuter(parser, "Upload Points Parser", chkOverrideValues.Checked, LogMgr);

                    parser.TempSetUserCulture(Convert.ToInt32(ddlCulturePointsUpload.SelectedValue));

                    executor.ExecuteParseAndSave();

                    DisplayMessages(executor.Messages, lblUploadPointsMessages, pointsMessagesCollapsiblePanelExtender, lblUploadPointsErrors, lblUploadPointsOverrides, lblUploadPointsResult, executor.BIDs);
                }

                protected void preUploadPointTypingPointsButton_Click(Object sender, EventArgs e)
                {
                    using (var streamForViewState = new MemoryStream())
                    {
                        //copy the stream to another object and set the position back to 0, otherwise the parser nor the setting viewState creation will work, due to the stream being closed.
                        csvPointTypingFileUpload.FileContent.CopyTo(streamForViewState);
                        csvPointTypingFileUpload.FileContent.Position = 0;

                        var parser = new PointTypingParser(csvPointTypingFileUpload.FileContent, DataMgr, false, LogMgr, userInfo);
                        var executor = new FileParserExecuter(parser, "Upload Point Typing Parser", false, LogMgr);

                        parser.TempSetUserCulture(Convert.ToInt32(ddlCulturePointTypingUpload.SelectedValue));

                        executor.ExecuteParse();

                        if (executor.Messages.HasMessage(FileParserMessages.MessageType.Error) || executor.Messages.HasMessage(FileParserMessages.MessageType.Exception))
                        {
                            DisplayMessages(executor.Messages, lblUploadPointTypingMessages, pointTypingMessagesCollapsiblePanelExtender, lblUploadPointTypingErrors, lblUploadPointTypingOverrides, lblUploadPointTypingResult);
                            return;
                        }

                        SetViewStateAndPanelDisplay(streamForViewState, parser);
                        executor.SetMessages();

                        if (executor.Messages.HasMessage(FileParserMessages.MessageType.Error) || executor.Messages.HasMessage(FileParserMessages.MessageType.Exception))
                        {
                            DisplayMessages(executor.Messages, lblUploadPointTypingMessages, pointTypingMessagesCollapsiblePanelExtender, lblUploadPointTypingErrors, lblUploadPointTypingOverrides, lblUploadPointTypingResult);
                            return;
                        }
                    }
                }

                protected void confirmPointTypingPointsButton_Click(Object sender, EventArgs e)
                {
                    var points = PointTypingPoints.Where(p => p.EquipmentTypeID == Convert.ToInt32(ddlPointTypingEquipmentType.SelectedValue) && lbPointTypingPointNameGroup.Items.Cast<ListItem>()
                                                  .Where(item => item.Selected).Select(item => item.Value).ToArray().Contains(p.PointName)).ToList();

                    using (var stream = new MemoryStream())
                    {
                        using (var csvWriter = new StreamWriter(stream, Encoding.UTF8))
                        {
                            SetMemoryStreamForParser(csvWriter, points);

                            csvWriter.Flush();

                            stream.Position = 0;

                            var parser = new PointsParser(stream, DataMgr, false, LogMgr, userInfo);
                            var executor = new FileParserExecuter(parser, "Submit Point Typing Parser", false, LogMgr);

                            executor.ExecuteParseAndSave();

                            //remove updated points from view state and reset form, if there are no errors/exceptions
                            if (!executor.Messages.HasMessage(FileParserMessages.MessageType.Exception) || !executor.Messages.HasMessage(FileParserMessages.MessageType.Error)) RemoveUpdatedPointsAndResetForm(points);

                            DisplayMessages(executor.Messages, lblConfirmPointTypingMessages, confirmPointTypingMessagesCollapsiblePanelExtender, lblConfirmPointTypingErrors, lblConfirmPointTypingOverrides, lblConfirmPointTypingResult, executor.BIDs);
                        }
                    }
                }

                protected void DownloadFileTemplateForPoints_Click(Object sender, EventArgs e)
                {
                    CreateDownloadFile(PointsParser.FieldDefs, "UploadPointsTemplate.csv");
                }

                protected void DownloadOverrideFileTemplateForPoints_Click(Object sender, EventArgs e)
                {
                    CreateDownloadFile(PointsParser.FieldDefsOverride, "UploadOverridePointsTemplate.csv");
                }

                protected void DownloadFileTemplateForPointTyping_Click(Object sender, EventArgs e)
                {
                    CreateDownloadFile(PointTypingParser.PointTypeFieldDefs, "UploadPointTypingTemplate.csv");
                }

            #endregion

            #region listbox events

                protected void lbPointTypingPointNameGroup_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    gridPointTypingPoints.DataSource = PointTypingPoints.Where(p => p.EquipmentTypeID == Convert.ToInt32(ddlPointTypingEquipmentType.SelectedValue) && lbPointTypingPointNameGroup.Items.Cast<ListItem>()
                                                                        .Where(item => item.Selected).Select(item => item.Value).ToArray().Contains(p.PointName)).ToList();
                    gridPointTypingPoints.DataBind();
                }

            #endregion

        #endregion

        #region methods

            private void ClearAndAddDDLItem(DropDownList ddl)
            {
                ddl.Items.Clear();
                ddl.Items.Add(listItem);
            }

            private void DisplaySelectedValueItem(DropDownList ddl)
            {
                var lbl = (Label)ddl.Parent.FindControl("lbl" + ddl.ID.Substring(3) + "ID");

                if (lbl != null)
                {
                    lbl.Text = (!ddl.ID.Contains("Zone")) ? "ID: " : "Zone: ";
                    lbl.Text += ddl.SelectedValue;
                }
            }

            private void UpdateSelectedGridView(DropDownList ddl)
            {
                //Right now only supports the building ddl, others may be introduced later.
                var value = ddl.SelectedValue;
                var id = (!NumericHelper.IsInt(value)) ? -1 : Convert.ToInt32(value);

                if (id != -1)
                {
                    switch (ddl.ID)
                    {
                        case "ddlBuilding":
                            var buildingsPoints = DataMgr.BuildingDataMapper.GetAllBuildingsPointsByCIDAndBID(siteUser.CID, id);
                            SetGridData(gridBuildingList, buildingsPoints);
                            break;
                    }
                }
            }

            private void SetGridData(GridView grid, IEnumerable<Object> data)
            {
                grid.DataSource = data;
                grid.DataBind();
            }

            private void HideLabelsAndExtenders(Label errorMessage, Label overrideMessage, Label resultMessage, Label extenderMessage, CollapsiblePanelExtender panelExtender)
            {
                var placeHolder = (ContentPlaceHolder)this.Parent;
                var upload = placeHolder.Controls[1];

                var errorMsg = (Label)upload.FindControl(errorMessage.ID);
                var resultMsg = (Label)upload.FindControl(resultMessage.ID);
                var overrideMsg = (Label)upload.FindControl(overrideMessage.ID);
                var extenderMsg = (Label)upload.FindControl(extenderMessage.ID);
                var panelExt = (CollapsiblePanelExtender)upload.FindControl(panelExtender.ID);

                errorMsg.Visible = false;
                overrideMsg.Visible = false;
                resultMsg.Visible = false;
                extenderMsg.Visible = false;
                panelExt.Collapsed = true;
                panelExt.ClientState = "true";
            }

            private void DisplayMessages(FileParserMessages messages, Label extenderMessage, CollapsiblePanelExtender panelExtender, Label errorMessage, Label overrideMessage, Label resultMessage, IEnumerable<Int32> bids = null)
            {
                var placeHolder = (ContentPlaceHolder)this.Parent;
                var upload = placeHolder.Controls[1];

                if (!messages.HasMessage(FileParserMessages.MessageType.Insert))
                {
                    var extenderMsg = (Label)upload.FindControl(extenderMessage.ID);
                    extenderMsg.Visible = true;

                    var panelExt = (CollapsiblePanelExtender)upload.FindControl(panelExtender.ID);
                    panelExt.Collapsed = false;
                    panelExt.ClientState = "false";
                }

                var errorMsg = (Label)upload.FindControl(errorMessage.ID);
                var overrideMsg = (Label)upload.FindControl(overrideMessage.ID);
                var resultMsg = (Label)upload.FindControl(resultMessage.ID);

                if (messages.HasMessage(FileParserMessages.MessageType.Exception))
                {
                    errorMsg.Visible = true;
                    errorMsg.Text = messages.GetMessage(FileParserMessages.MessageType.Exception, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Error))
                {
                    errorMsg.Visible = true;
                    errorMsg.Text = messages.GetMessage(FileParserMessages.MessageType.Error, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Update))
                {
                    overrideMsg.Visible = true;
                    overrideMsg.Text = messages.GetMessage(FileParserMessages.MessageType.Update, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Insert))
                {
                    resultMsg.Visible = true;
                    resultMsg.Text = messages.GetMessage(FileParserMessages.MessageType.Insert, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Update) || messages.HasMessage(FileParserMessages.MessageType.Insert)) UploadClearCache.ClearCache(siteUser.CID, bids, siteUser.UID);
            }

            private void SetViewStateAndPanelDisplay(MemoryStream tempStream, PointTypingParser parser)
            {
                var fieldDefs = PointTypingParser.FieldDefs;
                var equipments = DataMgr.EquipmentDataMapper.GetAllEquipmentByCID(siteUser.CID);
                var equipmentTypes = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypes();

                var fullEquipmentList = from e in equipments join et in equipmentTypes on e.EquipmentTypeID equals et.EquipmentTypeID select new { e.EID, et.EquipmentTypeID, et.EquipmentTypeName };

                tempStream.Position = 0;
                var rowIndex = 0;

                //save the memory stream information to viewstate
                using (var reader = new CsvReader(tempStream, fieldDefs[0].DisplayName))
                {
                    foreach (String[] dataRow in reader.RowEnumerator)
                    {
                        rowIndex++;

                        var point = new GetPointsDataUploadSerializedFormat();

                        point.DSID = Convert.ToInt32(dataRow[0]);
                        point.EID = Convert.ToInt32(dataRow[1]);
                        point.ReferenceID = StringHelper.TrimLeadingTrailingWhiteSpace(dataRow[2]);
                        point.PointName = StringHelper.TrimLeadingTrailingWhiteSpace(dataRow[3]);
                        point.SamplingInterval = Convert.ToInt32(dataRow[4]);
                        point.RelPctError = Convert.ToDouble(dataRow[5]);
                        point.TimeZone = dataRow[6];

                        DateTime tempDate;
                        point.DateCreated = DateTime.TryParse(dataRow[7], out tempDate) ? tempDate : DateTime.MinValue;

                        var currentEqupmentType = fullEquipmentList.Where(e => e.EID == point.EID).FirstOrDefault();
                        if (currentEqupmentType == null)
                        {
                            var msg = String.Format("Error: Equipment Type not found for Point Name: {0} EID: {1} for Client: {2}", point.PointName, point.EID, siteUser.ClientName);
                            parser.Exceptions.Add(new FileParserBase.InvalidTemplateException(rowIndex, msg));
                            continue;
                        }

                        point.EquipmentTypeID = currentEqupmentType.EquipmentTypeID;
                        point.EquipmentTypeName = currentEqupmentType.EquipmentTypeName;

                        PointTypingPoints.Add(point);
                    }
                }

                //clear/bind appropriate controls and set panel displays accordingly
                lbPointTypingPointNameGroup.Items.Clear();

                gridPointTypingPoints.DataSource = null;
                gridPointTypingPoints.DataBind();

                BindEquipmentTypes(ddlPointTypingEquipmentType, PointTypingPoints);

                pnlPointTyping.Visible = !PointTypingPoints.Any() ? true : false;
                pnlPointTypingConfirm.Visible = PointTypingPoints.Any() ? true : false;
            }

            private void SetMemoryStreamForParser(StreamWriter csvWriter, List<GetPointsDataUploadSerializedFormat> points)
            {
                foreach (var p in points)
                {
                    var point = new GetPointsDataUploadSerializedFormat();

                    point.PointName = p.PointName;
                    point.DSID = p.DSID;
                    point.ReferenceID = p.ReferenceID;
                    point.RelPctError = p.RelPctError;
                    point.SamplingInterval = p.SamplingInterval;
                    point.TimeZone = p.TimeZone;
                    point.PointTypeID = Convert.ToInt32(ddlPointTypingPointType.SelectedValue);
                    point.EID = p.EID;
                    point.Power = Convert.ToDouble(txtPointTypingPower.Text);
                    point.Multiply = Convert.ToDouble(txtPointTypingMultiply.Text);
                    point.Addition = Convert.ToDouble(txtPointTypingAddition.Text);
                    point.PowerMatlab = Convert.ToDouble(txtPointTypingPowerMatlab.Text);
                    point.MultiplyMatlab = Convert.ToDouble(txtPointTypingMultiplyMatlab.Text);
                    point.AdditionMatlab = Convert.ToDouble(txtPointTypingAdditionMatlab.Text);
                    point.DateCreated = p.DateCreated;

                    var valueList = new List<string>();

                    var properties = typeof(GetPointsDataUploadSerializedFormat).GetProperties();

                    foreach (var fieldDef in PointsParser.FieldDefs)
                    {
                        var prop = properties.Where(f => fieldDef.FieldName == f.Name).FirstOrDefault();

                        if (prop != null)
                        {
                            var propValue = prop.GetValue(point, null);

                            if (prop.Name == "DateCreated" && ((DateTime)prop.GetValue(point, null)) == DateTime.MinValue)
                                valueList.Add(String.Empty);
                            else
                                valueList.Add(propValue.ToString());
                        }
                    }

                    csvWriter.WriteLine(String.Join(",", valueList));
                }
            }

            private void RemoveUpdatedPointsAndResetForm(List<GetPointsDataUploadSerializedFormat> points)
            {
                foreach (GetPointsDataUploadSerializedFormat item in points)
                    PointTypingPoints.Remove(item);

                //clear grid
                gridPointTypingPoints.DataSource = null;
                gridPointTypingPoints.DataBind();

                //rebind types
                BindEquipmentTypes(ddlPointTypingEquipmentType, PointTypingPoints);

                //reselect ddl
                ddlPointTypingEquipmentType.SelectedIndex = -1;
                ddlPointTypingEquipmentType_OnSelectedIndexChanged(null, null);

                ddlPointTypingPointClass.SelectedIndex = -1;
                ddlPointTypingPointClass_OnSelectedIndexChanged(null, null);

                ddlPointTypingPointType.SelectedIndex = -1;

                txtPointTypingPower.Text = "1";
                txtPointTypingMultiply.Text = "1";
                txtPointTypingMultiply.Text = "1";
                txtPointTypingAddition.Text = "0";
                txtPointTypingPowerMatlab.Text = "1";
                txtPointTypingMultiplyMatlab.Text = "1";
                txtPointTypingAdditionMatlab.Text = "0";

                if (!PointTypingPoints.Any()) PointTypingPoints = null;
            }

            private void CreateDownloadFile(FileParserBase.FieldDefBase[] fieldDefs, String fileName)
            {
                var fieldNamesForFile = Encoding.UTF8.GetBytes(FileParserBase.FormatFieldNamesForOutput(",", "TemplateDownload", fieldDefs));

                var fileHandler = new DownloadRequestHandler.HandledFileAttacher();
                ((IFileAttacher)fileHandler).AttachForDownload((fileName != String.Empty) ? fileName : "UploadFileTemplate.csv", fieldNamesForFile);
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    var buildings = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID);

                    ClearAndAddDDLItem(ddl);
                    BindData(ddl, "BuildingName", "BID", buildings);
                }

                private void BindDataSources(DropDownList ddl)
                {
                    var dataSources = DataMgr.DataSourceDataMapper.GetAllDataSourcesByCIDWithUploadDropdownFormat(siteUser.CID);

                    ClearAndAddDDLItem(ddl);
                    BindData(ddl, "DataSourceConcatinated", "DSID", dataSources);
                    SetGridData(gridDataSources, dataSources);
                }

                private void BindEquipment(DropDownList ddl)
                {
                    var equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByCIDWithUploadDropdownFormat(siteUser.CID);

                    ClearAndAddDDLItem(ddl);
                    BindData(ddl, "EquipmentConcatinated", "EID", equipment);
                    SetGridData(gridEquipment, equipment.OrderBy(e => e.BuildingName).ThenBy(e => e.EquipmentName));
                }

                private void BindEquipmentTypes(DropDownList ddl, List<GetPointsDataUploadSerializedFormat> points)
                {
                    var newEquipmentTypes = points.GroupBy(p => p.EquipmentTypeID).Select(p => p.First()).OrderBy(n => n.EquipmentTypeName).ToList();

                    ClearAndAddDDLItem(ddl);
                    BindData(ddl, "EquipmentTypeName", "EquipmentTypeID", newEquipmentTypes);
                }

                private void BindPointTypes(DropDownList ddl)
                {
                    //only point enabled point types
                    var pointTypes = DataMgr.PointTypeDataMapper.GetAllPointTypesPointEnabledWithUploadDropdownFormat();

                    ClearAndAddDDLItem(ddl);
                    BindData(ddl, "PointTypeConcatinated", "PointTypeID", pointTypes);
                    SetGridData(gridPointTypes, pointTypes);
                }

                private void BindVAndVPrePointTypes(DropDownList ddl)
                {
                    //only point enabled point types
                    var pointTypes = DataMgr.PointTypeDataMapper.GetAllPointTypesVOrVPrePointEnabledWithUploadDropdownFormat();

                    ClearAndAddDDLItem(ddl);
                    BindData(ddl, "PointTypeConcatinated", "PointTypeID", pointTypes);
                    SetGridData(gridVAndVPrePointTypes, pointTypes);
                }

                private void BindPointNameGroups(ListBox lb, Int32 equipmentTypeID)
                {
                    lb.Items.Clear();

                    lb.DataSource = PointTypingPoints.Where(p => p.EquipmentTypeID == equipmentTypeID).GroupBy(n => n.PointName).OrderBy(k => k.Key).Select(p => p.Key).ToList();
                    lb.DataBind();
                }

                private void BindTimeZones(DropDownList ddl)
                {
                    ClearAndAddDDLItem(ddl);
                    BindData(ddl, "DisplayName", "Id", TimeZoneInfo.GetSystemTimeZones());
                }

                private void BindPointClasses(DropDownList ddl, Boolean includeUnassigned = true)
                {
                    var pointClasses = DataMgr.PointClassDataMapper.GetAllPointClassesByTypeEnabled(true, false, false, includeUnassigned);

                    BindData(ddl, "PointClassName", "PointClassID", pointClasses);
                }

                private void BindPointTypes(DropDownList ddl, Int32 pointClassID)
                {
                    var pointTypes = DataMgr.PointTypeDataMapper.GetAllPointTypesByPointClassIDAndPointEnabled(pointClassID);

                    ClearAndAddDDLItem(ddl);
                    BindData(ddl, "PointTypeName", "PointTypeID", pointTypes);
                }

                private void BindData(DropDownList ddl, String textField, String valueField, IEnumerable<Object> data)
                {
                    ddl.DataTextField = textField;
                    ddl.DataValueField = valueField;
                    ddl.DataSource = data;
                    ddl.DataBind();
                }

                private void BindUploadPointsFieldsList()
                {
                    litUploadPointsFieldList.Text = FileParserBase.FormatFieldNamesForOutput("<br />", "FieldList", PointsParser.FieldDefs);
                }

                private void BindUploadPointsOverrideFieldsList()
                {
                    litUploadPointsOverrideFieldList.Text = FileParserBase.FormatFieldNamesForOutput("<br />", "FieldList", PointsParser.FieldDefsOverride);
                }

                private void BindUploadPointTypingFieldsList()
                {
                    litUploadPointTypingFieldList.Text = FileParserBase.FormatFieldNamesForOutput("<br />", "FieldList", PointTypingParser.PointTypeFieldDefs);
                }

            #endregion

        #endregion
    }

    public class ClearCachePoints : IClearCache
    {
        public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid) //uid not used here, just passed in as null
        {
            if (bids != null)
            {
                foreach (var bid in bids)
                    GridCacher.Clear(ViewPoints.CacheStore, ViewPoints.CreateCacheKey(cid, bid));
            }
        }
    }
}