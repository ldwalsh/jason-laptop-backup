﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UploadFormat.ascx.cs" Inherits="CW.Website._controls.upload.UploadFormat" %>

<h2><asp:Label ID="lblFormatTitle" runat="server" /></h2>
<p><asp:Label ID="lblFormatMessage" runat="server" /></p>
<asp:HyperLink ID="lnkUploadFieldsFormat" runat="server" CssClass="toggle"><asp:Label ID="lblUploadFieldsFormat" CssClass="toggle" runat="server" /></asp:HyperLink>
<ajaxToolkit:CollapsiblePanelExtender ID="uploadFieldsCPE" runat="Server" TargetControlID="pnlFields" ExpandControlID="lnkUploadFieldsFormat" CollapseControlID="lnkUploadFieldsFormat" CollapsedText="show upload fields format" ExpandedText="hide upload fields format" TextLabelID="lblUploadFieldsFormat" />

<asp:Panel ID="pnlFields" CssClass="richText" runat="server">
  <asp:Literal ID="litUploadFieldList" runat="server" />
</asp:Panel>
<br />

<asp:Panel ID="pnlOverrideFieldsLinkAndFields" runat="server" Visible="false">
  <asp:HyperLink ID="lnkUploadOverrideFieldsFormat" runat="server" CssClass="toggle"><asp:Label ID="lblUploadOverrideFieldsFormat" CssClass="toggle" runat="server" /></asp:HyperLink>
  <ajaxToolkit:CollapsiblePanelExtender ID="uploadOverrideFieldsCPE" runat="Server" TargetControlID="pnlOverrideFields" ExpandControlID="lnkUploadOverrideFieldsFormat" CollapseControlID="lnkUploadOverrideFieldsFormat" CollapsedText="show upload override fields format" ExpandedText="hide upload override fields format" TextLabelID="lblUploadOverrideFieldsFormat" />
  
  <asp:Panel ID="pnlOverrideFields" CssClass="richText" runat="server">
    <asp:Literal ID="litUploadOverrideFieldList" runat="server" />
  </asp:Panel>
  <br />
</asp:Panel>

<div class="downloadDiv">
  <span><asp:Label ID="lblDownloadTemplate" runat="server" /></span> <asp:ImageButton ID="imgDownloadFileTemplate" runat="server" ImageUrl="~/_assets/images/csv-icon.jpg" OnClick="DownloadFileTemplateClick" AlternateText="download" CausesValidation="false" />
</div>

<asp:Panel ID="pnlOverrideFileTemplate" runat="server" Visible="false">
  <div class="downloadDiv">
    <span><asp:Label ID="lblDownloadOverrideTemplate" runat="server" /></span> <asp:ImageButton ID="imgOverrideDownloadFileTemplate" runat="server" ImageUrl="~/_assets/images/csv-icon.jpg" OnClick="DownloadOverrideFileTemplateClick" AlternateText="download" CausesValidation="false" />
  </div>
</asp:Panel>

<hr />