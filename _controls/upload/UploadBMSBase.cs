﻿using CW.Data;
using CW.Utility;
using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.BMSInfo;
using CW.Website._parsers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._controls.upload
{
    public class UploadBMSInfoBase : UploadBase
    {
        #region properties

        #region overrides
        protected override String[] ExcludeFields { get { return new String[] { ""}; } }

        protected override IClearCache ClearCache { get { return new ClearCacheBMSInfo(); } }

        #endregion

        #endregion

        #region events

        private void Page_Load()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");

            WireUpUploadSettings("Upload BMSInfo", String.Empty, typeof(BMSInfoParser), "UploadBMSInfoTemplate.csv", String.Empty, false, true);
        }

        #endregion

        public class ClearCacheBMSInfo : IClearCache
        {
            public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid) //bids list and uid not used here, just passed in as null
            {
                var cacheStore = AdminUserControlGrid.CacheStore;

                GridCacher.Clear(cacheStore, ViewBMSInfo.CreateCacheKey(cid));
            }
        }
    }
}