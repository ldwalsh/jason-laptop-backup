﻿using CW.Data;
using CW.Utility;
using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Buildings;
using CW.Website._controls.admin.Equipment;
using CW.Website._controls.admin.Points;
using CW.Website._parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.upload
{
    public abstract class UploadEquipmentPointsBase : UploadBase
    {
        #region properties

            #region overrides

                protected override String[] ExcludeFields { get { return new String[] { EquipmentPointsParser.FieldDefs.Where(_ => _.FieldName == PropHelper.G<Equipment_Point>(ep => ep.PID)).Select(f => f.DisplayName).First() }; } }

                protected override IClearCache ClearCache { get { return new ClearCacheEquipmentPoints(); } }

            #endregion

        #endregion

        #region events

            private void Page_Load()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");

                WireUpUploadSettings("Upload Equipment Points", "Upload Equip. Points", typeof(EquipmentPointsParser), "UploadEquipmentPointTemplate.csv", "UploadOverrideEquipmentPointTemplate.csv", true, false);
            }

        #endregion
    }

    public class ClearCacheEquipmentPoints : IClearCache
    {
        public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid) 
        {
            var cacheStore = AdminUserControlGrid.CacheStore;

            if (bids != null)
            {
                foreach (var bid in bids)
                {
                    GridCacher.Clear(cacheStore, ViewPoints.CreateCacheKey(cid, bid));
                    GridCacher.Clear(cacheStore, EquipmentStats.CreateCacheKey(cid, bid));
                }
            }

            GridCacher.Clear(cacheStore, BuildingStats.CreateCacheKey(cid, uid));
        }
    }
}