﻿using CW.Data;
using CW.Utility;
using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Buildings;
using CW.Website._controls.admin.Equipment;
using CW.Website._parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.upload
{
    public abstract class UploadEquipmentBase : UploadBase
    {
        #region properties

            #region overrides

                protected override String[] ExcludeFields
                {
                    get
                    {
                        return new String[] 
                        { 
                            EquipmentParser.FieldDefs.Where(_ => _.FieldName == PropHelper.G<Equipment>(eq => eq.BID)).Select(f => f.DisplayName).First(),
                            EquipmentParser.FieldDefsOverride.Where(_ => _.FieldName == PropHelper.G<Equipment>(eq => eq.EquipmentName)).Select(f => f.DisplayName).First()
                        };
                    }
                }

                protected override IClearCache ClearCache { get { return new ClearCacheEquipment(); } }

            #endregion

        #endregion

        #region events

            private void Page_Load()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");

                WireUpUploadSettings("Upload Equipment", String.Empty, typeof(EquipmentParser), "UploadEquipmentTemplate.csv", "UploadOverrideEquipmentTemplate.csv", true, false);
            }

        #endregion
    }

    public class ClearCacheEquipment : IClearCache
    {
        public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid)
        {
            var cacheStore = AdminUserControlGrid.CacheStore;

            if (bids != null)
            {
                foreach (var bid in bids)
                {
                    GridCacher.Clear(cacheStore, ViewEquipment.CreateCacheKey(cid, bid));
                    GridCacher.Clear(cacheStore, EquipmentStats.CreateCacheKey(cid, bid));
                }
            }

            GridCacher.Clear(cacheStore, BuildingStats.CreateCacheKey(cid, uid));
        }
    }
}