﻿using CW.Data;
using CW.Utility;
using CW.Website._administration;
using CW.Website._controls.admin;
using CW.Website._controls.admin.DataSources;
using CW.Website._parsers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.upload
{
    public abstract class UploadDataSourcesBase : UploadBase
    {
        #region properties

            #region overrides

                protected override String[] ExcludeFields { get { return new String[] { DataSourceParser.FieldDefs.Where(_ => _.FieldName == PropHelper.G<DataSource>(ds => ds.DataSourceName)).Select(f => f.DisplayName).First(), }; } }

                protected override IClearCache ClearCache { get { return new ClearCacheDataSources(); } }

            #endregion

        #endregion

        #region events

            private void Page_Load()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                if (!(siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsSuperAdmin && siteUser.IsLoggedInUnderProviderClient))) Response.Redirect("/Home.aspx");

                WireUpUploadSettings("Upload Data Sources", String.Empty, typeof(DataSourceParser), "UploadDataSourceTemplate.csv", String.Empty, false, false);
            }

        #endregion
    }

    public class ClearCacheDataSources : IClearCache
    {
        public void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid) //bids list and uid not used here, just passed in as null
        {
            var cacheStore = AdminUserControlGrid.CacheStore;

            GridCacher.Clear(cacheStore, ViewDataSources.CreateCacheKey(cid));
            GridCacher.Clear(cacheStore, DataSourceStats.CreateCacheKey(cid));
        }
    }
}