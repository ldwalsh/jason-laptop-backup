﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Upload.ascx.cs" Inherits="CW.Website._controls.upload.Upload" %>

<asp:UpdatePanel ID="updatePanelNestedUpload" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">
  <ContentTemplate>
    <hr />

    <h2><asp:Label ID="lblUpload" runat="server" /></h2>
    <p><asp:Label ID="lblResult" CssClass="successMessage" runat="server" /></p>

    <div>
      <asp:Panel ID="pnlCulture" runat="server" Visible="false">
        <div class="divForm">
          <label class="label"><asp:Label ID="lblCulture" runat="server" /></label>
          <asp:DropDownList CssClass="dropdown" ID="ddlCulture" runat="server" /> 
        </div>
     </asp:Panel>

      <asp:Panel ID="pnlOverrideOption" runat="server">
        <div class="divForm">
          <label class="label"><asp:Label ID="lblOverrideValues" runat="server" /></label>
          <asp:CheckBox ID="chkOverrideValues" CssClass="checkbox" runat="server" />
          <span><asp:Label ID="lblExludedFields" runat="server" /></span>
        </div>
      </asp:Panel>

      <div class="divForm">
        <label class="label"><asp:Label ID="lblCSVFile" runat="server" /></label>
        <asp:FileUpLoad ID="csvFileUpload" runat="server" />
      </div>  

      <asp:LinkButton ID="btnUpload" runat="server" OnClick="ButtonClick" CssClass="lnkButton" ValidationGroup="Upload" />
    </div>

    <asp:RequiredFieldValidator ID="csvFileUploadRequiredValidator" runat="server" ErrorMessage="No file selected." ControlToValidate="csvFileUpload" SetFocusOnError="true" Display="None" ValidationGroup="Upload" />
    <ajaxToolkit:ValidatorCalloutExtender ID="csvFileUploadRequiredValidatorExtender" runat="server" BehaviorID="csvFileUploadRequiredValidatorExtender" TargetControlID="csvFileUploadRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />                    
          
    <asp:RegularExpressionValidator ID="csvFileUploadRegExValidator" runat="server" ErrorMessage="Upload csv only." ValidationExpression=".*(\.csv|.CSV)$" ControlToValidate="csvFileUpload" SetFocusOnError="true" Display="None" ValidationGroup="Upload" />
    <ajaxToolkit:ValidatorCalloutExtender ID="csvFileUploadRegExValidatorExtender" runat="server" BehaviorID="csvFileUploadRegExValidatorExtender" TargetControlID="csvFileUploadRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <asp:HyperLink ID="lnkMessages" runat="server" CssClass="toggle"><asp:Label ID="lblMessages" CssClass="toggle" runat="server" /></asp:HyperLink>
    <ajaxToolkit:CollapsiblePanelExtender ID="messagesCPE" runat="Server" TargetControlID="pnlMessages" ExpandedSize="200" ExpandControlID="lnkMessages" CollapseControlID="lnkMessages" ScrollContents="true" CollapsedText="show messages" ExpandedText="hide Messages" TextLabelID="lblMessages" />

    <asp:Panel ID="pnlMessages" CssClass="richText" runat="server">           
      <p> 
        <asp:Label ID="lblErrors" CssClass="errorMessage" runat="server" /><br />
        <asp:Label ID="lblOverrides" CssClass="overrideMessage" runat="server" />
      </p>
    </asp:Panel>

  </ContentTemplate>

  <Triggers>
    <asp:PostBackTrigger ControlID="btnUpload"/>
  </Triggers>
</asp:UpdatePanel>