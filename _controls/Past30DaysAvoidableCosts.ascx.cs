﻿using CW.Common.Constants;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;

namespace CW.Website._controls
{
    public partial class Past30DaysAvoidableCosts : ModularSiteUserControl
    {
        #region fields

            private DateTime startDate, endDate;
     
        #endregion

        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.Past30Days; } }

        #endregion

        #region methods

            #region overrides

                protected override void Initialize()
                {
                    DateTimeHelper.GenerateDefaultDatesYesterday(out startDate, out endDate, EarliestBuildingTimeZoneID);
                }

                protected override void BindControl()
                {
                    RetrieveAndBindDiagnostics();
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    return GetConcatenatedResults(DateTimeHelper.GetXDaysAgo(endDate, 30), endDate, DataConstants.AnalysisRange.Daily, false);
                }

                protected override void BindDiagnostics(IEnumerable<DiagnosticsResult> results)
                {
                    var hasResults = results.Any();

                    if (hasResults)
                        BindAvoidableCostChart(results);

                    portfolioAvoidableCostsChart.Visible = (hasResults) ? true : false;
                    lblPortfolioAvoidableCosts.Visible = !hasResults;
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion

            public void BindAvoidableCostChart(IEnumerable<DiagnosticsResult> results)
            {
                //for each lcid
                var lcids = results.GroupBy(l => l.LCID).Select(s => s.First().LCID).ToArray();
                var counter = 1;

                portfolioAvoidableCostsChart.Series.Clear();

                foreach (var lcid in lcids)
                {
                    var cultureinfo = CultureHelper.GetCultureInfo(lcid);
                    var currencyISOSymbol = CultureHelper.GetCurrencyISOSymbol(lcid);
                    var currentResults = results.Where(l => l.LCID == lcid).GroupBy(d => d.StartDate).Select(s => new DiagnosticsResult
                    {
                        StartDate = s.Key,
                        CostSavings = s.Sum(c => c.CostSavings),
                        CostSavingsCultureFormated = CultureHelper.FormatCurrencyAsString(s.First().LCID, s.Sum(c => c.CostSavings), false),
                    }).OrderBy(s => s.StartDate);
                    
                    //set axis intervaltype
                    portfolioAvoidableCostsChart.ChartAreas["portfolioAvoidableCostsChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;
                    portfolioAvoidableCostsChart.ChartAreas["portfolioAvoidableCostsChartArea"].AxisX.LabelStyle.Interval = 2;
                    portfolioAvoidableCostsChart.ChartAreas["portfolioAvoidableCostsChartArea"].AxisX.LabelStyle.Format = "d"; //set axis format:  g = short date and time, d = short date, M/d/yy
                    //set major tick interval to match
                    portfolioAvoidableCostsChart.ChartAreas["portfolioAvoidableCostsChartArea"].AxisX.MajorTickMark.Interval = 2;

                var series = new Series
                    {
                        ChartType = SeriesChartType.FastLine,
                        BorderWidth = 3,
                        Name = "Line Series " + lcid, 
                        LegendText = currencyISOSymbol + " (" + cultureinfo.NumberFormat.CurrencySymbol + ")",
                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColorsHex[counter - 1]),
                    };

                    //add new rawData to series
                    series.Points.DataBindXY(currentResults.ToArray(), "StartDate", currentResults.ToArray(), "CostSavings");

                    //interpolate
                    portfolioAvoidableCostsChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    portfolioAvoidableCostsChart.Series.Add(series);

                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        ShadowColor = Color.DimGray,
                        ShadowOffset = 4,
                        MarkerSize = 6,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColorsHex[counter - 1]),
                        MarkerColor = Color.White,
                        Name = "Point Series " + lcid,
                        ToolTip = "x: #VALX{d}, y: #VALY",
                        Legend = "Hidden"
                    };

                    series.Points.DataBindXY(currentResults.ToArray(), "StartDate", currentResults.ToArray(), "CostSavings");

                    //interpolate
                    portfolioAvoidableCostsChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    portfolioAvoidableCostsChart.Series.Add(series);

                    counter++;
                }

                //if more than one series
                if (lcids.Count() > 1)
                    portfolioAvoidableCostsChart.Legends[0].Enabled = true; //set legend 
                else
                {
                    portfolioAvoidableCostsChart.Legends[0].Enabled = false; //disable legend
                    portfolioAvoidableCostsChart.ChartAreas["portfolioAvoidableCostsChartArea"].AxisY.Title += " (" + CultureHelper.GetCurrencySymbol(lcids[0]) + ")"; //set yaxis title
                }
            }

            protected void portfolioAvoidableCostsChart_PrePaint(Object sender, ChartPaintEventArgs e)
            {
                portfolioAvoidableCostsChart.Legends[0].Position.Width = 90;
                portfolioAvoidableCostsChart.Legends[0].Position.X = 5;
                portfolioAvoidableCostsChart.Legends[0].Position.Y = 83;
            }

        #endregion
    }
}