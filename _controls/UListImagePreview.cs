﻿using CW.Business.Blob;
using CW.Business.Blob.Images;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    public class UListImagePreview: SiteUserControl
    {
        public Int32  ClientId  {get;set;}
        public String Extension {get;set;}

        protected override void OnInit(EventArgs e)
        {
            Page.RegisterRequiresControlState(this);
            
            base.OnInit(e);
        }

        protected override object SaveControlState()
        {
            return new Pair(base.SaveControlState(), new Object[]{ClientId, Extension});
        }

        protected override void LoadControlState(Object state)
        {
            if (state == null) return;

            var p = state as Pair;

            if (p == null)
            {
                base.LoadControlState(state);

                return;
            }

            base.LoadControlState(p.First);
                
            var s = (Object[])p.Second;

            ClientId    = Convert.ToInt32(s[0]);
            Extension   = Convert.ToString(s[1]);
        }

        public override void RenderControl(System.Web.UI.HtmlTextWriter writer)
        {
            if (String.IsNullOrEmpty(Extension)) return;

			if (!new ClientProfileImage(ClientId, DataMgr.OrgBasedBlobStorageProvider, Extension).Exists()) return;

            new HtmlGenericControl("li"){Controls={new HtmlGenericControl("strong"){InnerHtml="Client Image: "}, new Image(){ ImageUrl=HandlerHelper.ClientImageUrl(ClientId, Extension, true), CssClass="imgDetailsListPreview"}}}.RenderControl(writer);
        }
    }
}