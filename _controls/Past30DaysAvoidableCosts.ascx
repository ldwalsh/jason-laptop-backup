﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Past30DaysAvoidableCosts.ascx.cs" Inherits="CW.Website._controls.Past30DaysAvoidableCosts" %>

<script type="text/javascript">
    function onChartPostRenderResize(sender, args) {
        try {
            document.getElementById('Past30DaysAvoidableCosts_portfolioAvoidableCostsChart').style.width = '100%';
        } catch (e) {
        }
    }
</script> 

<asp:Literal ID="litError" runat="server" />

<asp:Chart ID="portfolioAvoidableCostsChart" OnPrePaint="portfolioAvoidableCostsChart_PrePaint" CssClass="widgetAspChart" runat="server" ImageType="Png" EnableViewState="true" Width="504px" Visible="false">

  <chartAreas>
    <asp:ChartArea Name="portfolioAvoidableCostsChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                         
      <AxisY Title="Daily Avoidable Cost" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold"  LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
        <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
        <MajorGrid LineColor="#CCCCCC" />
        <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
      </AxisY>

      <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
        <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 7.5pt, style=Bold" />
        <MajorGrid LineColor="#CCCCCC" /> 
        <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
      </AxisX>                                                             
    </asp:ChartArea>
  </chartAreas>                                                   
      
  <Legends>                                                            
    <asp:Legend Font="Arial, 8.25pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666" IsTextAutoFit="true" Enabled="false" />
    <asp:Legend Name="Hidden" Enabled="false" />
  </Legends>
</asp:Chart>
    
<asp:Label ID="lblPortfolioAvoidableCosts" runat="server" CssClass="dockMessageLabel" Text="No avoidable costs." Visible="false" />

<script type="text/javascript">
    onChartPostRenderResize();
</script>	