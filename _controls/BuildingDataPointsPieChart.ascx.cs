﻿using System;
using System.Linq;
using System.Collections.Generic;

using CW.Common.Constants;
using CW.Data.Models.Building;
using CW.Data.Models.Diagnostics;


namespace CW.Website._controls
{
    public partial class BuildingDataPointsPieChart : ModularSiteUserControl
    {
        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.LastMonth; } }

            protected string BuildingPointPieChartString { get; set; }
        
        #endregion

        #region methods

            #region override methods

                protected override void BindControl()
                {
                    if (HasBuildings)
                        SetBuildingPointPieChartString();
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    return null;
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion

            public void SetBuildingPointPieChartString()
            {
                var buildingPoints = DataMgr.BuildingDataMapper.GetAllBuildingsByBIDsInBuildingPointPieFormat(siteUser.VisibleBuildings.Select(b => b.BID).ToList());

                BuildingPointPieChartString = "[[|Building Name|, |Points|],";

                foreach (var building in buildingPoints)
                    BuildingPointPieChartString += String.Format("[|{0}|, {1}],", building.BuildingName, building.PointCount);

                BuildingPointPieChartString += "]";
                BuildingPointPieChartString = BuildingPointPieChartString.Replace(",]", "]");
            }

        #endregion
    }
}