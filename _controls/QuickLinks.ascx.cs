﻿using CW.Data;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    public partial class QuickLinks : SiteUserControl
    {
        #region Properties

            const string noQuickLinks = "<div class='dockMessageLabel'>No quick links have been set for your account.</div>";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void LoadModule()
        {
            if (!siteUser.IsAnonymous)
                BindQuickLinks(DataMgr.QuickLinksDataMapper.GetAllQuickLinksAssignedToUser(siteUser.UID, siteUser.CID));
        }

        #region WIDGET

            /// <summary>
            /// Binds quick links
            /// </summary>
            public void BindQuickLinks(IEnumerable<QuickLink> linkList)
            {
                //TODO: bind based on preset admin quick links in db.
                StringBuilder quickLinks = new StringBuilder();

                if (linkList.Count() != 0)
                {
                    quickLinks.AppendLine("<ul>");

                    //for each v pre point                 
                    using (IEnumerator<QuickLink> list = linkList.GetEnumerator())
                    {
                        while (list.MoveNext())
                        {
                            //current equipment variable
                            QuickLink item = (QuickLink)list.Current;

                            quickLinks.AppendLine("<li><a href=\"" + item.QuickLink1 + "\">" + item.QuickLinkName + "</a>" + (String.IsNullOrEmpty(item.QuickLinkDescription) ? "" : "<p>" + item.QuickLinkDescription + "</p>") + "</li>");
                        }
                    }

                    quickLinks.AppendLine("</ul>");
                    litQuickLinks.Text = quickLinks.ToString();
                }
                else
                    litQuickLinks.Text = noQuickLinks;
            }

        #endregion
    }
}