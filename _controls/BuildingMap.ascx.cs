﻿using CW.Business;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CW.Website._controls
{
    public partial class BuildingMap : ModularSiteUserControl
    {
        #region fields

            private DateTime startDate, endDate;

            private IEnumerable<Building> filteredClientBuildingsWithSettings = Enumerable.Empty<Building>();

        #endregion

        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.Yesterday; } }

            protected String UserDataforBuildingMap { get; set; }

            protected String GeocodeCredentialsKey { get; set; }

        #endregion

        #region methods

            #region overrides

                protected override void Initialize()
                {
                    DateTimeHelper.GenerateDefaultDatesYesterday(out startDate, out endDate, EarliestBuildingTimeZoneID);

                    var allClientBuildings = DataMgr.BuildingDataMapper.GetAllBuildings((b => b.BuildingSettings));
                    var filteredClientBuildings = Enumerable.Empty<Building>();

                    if (IsProviderView)
                    {
                        var cids = siteUser.ProviderClients.Select(pc => pc.CID);
                        var visibleBuildings = siteUser.VisibleBuildingsProviderClients;

                        foreach (var cid in cids)
                        {
                            filteredClientBuildings = allClientBuildings.Where(cb => cb.CID == cid);
                            filteredClientBuildingsWithSettings = filteredClientBuildingsWithSettings.Concat(
                                filteredClientBuildings.Intersect(visibleBuildings[cid], new BuildingDataMapper.BuildingComparer()));
                        }
                    }
                    else
                    {
                        filteredClientBuildings = allClientBuildings.Where(cb => cb.CID == siteUser.CID);
                        filteredClientBuildingsWithSettings = filteredClientBuildings.Intersect(siteUser.VisibleBuildings, new BuildingDataMapper.BuildingComparer());
                    }

                    filteredClientBuildingsWithSettings = filteredClientBuildingsWithSettings.ToList();
                }

                protected override void BindControl()
                {
                    if (!siteUser.IsAnonymous && !Page.IsPostBack)
                        RetrieveAndBindDiagnostics();
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    return GetConcatenatedResults(startDate, endDate, DataConstants.AnalysisRange.Daily);
                }

                protected override void BindDiagnostics(IEnumerable<DiagnosticsResult> results)
                {
                    SetMapUserControlProps(results);
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion

            private void SetMapUserControlProps(IEnumerable<DiagnosticsResult> results)
            {
                var clients = DataMgr.ClientDataMapper.GetAllActiveClients();

                GeocodeCredentialsKey = Convert.ToBase64String(Encoding.UTF8.GetBytes(DataConstants.GeocodeCredentialsKey));

                var list = new List<string>();

                foreach (var b in Buildings)
                {
                    list.Add(
                     String.Format("\"{0}\":[\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",{6},\"{7}\",{8},{9},{10},{11},\"{12}\",{13},{14}]",
                            b.BID.ToString(),
                            (b.Address != null) ? StringHelper.ReplaceSpecialCharactersForQueryString(b.Address) : String.Empty,
                            (b.City != null) ? StringHelper.ReplaceSpecialCharactersForQueryString(b.City) : String.Empty,
                            (b.StateID != null) ? StringHelper.ReplaceSpecialCharactersForQueryString(b.State.StateName) : null,
                            b.CountryAlpha2Code,
                            StringHelper.ReplaceSpecialCharactersForQueryString(b.BuildingName),
                            CreateFaults(results, b.BID),
                            CreateCostsString(results, b.BID),
                            (b.Latitude != null) ? CultureHelper.FormatNumberToInvariantString((decimal)b.Latitude) : "null",
                            (b.Longitude != null) ? CultureHelper.FormatNumberToInvariantString((decimal)b.Longitude) : "null",
                            CostThresholdValue(results, b.BID),
                            (IsProviderView) ? 1 : 0,
                            (IsProviderView) ? StringHelper.ReplaceSpecialCharactersForQueryString(clients.Where(c => c.CID == b.CID).Select(c => c.ClientName).FirstOrDefault()) : null,
                            (ModuleAccessList.Where(ma => ma.CID == b.CID).Select(ma => ma.HasDiagnosticAndCommissioningAccess).FirstOrDefault()) ? 1 : 0,
                            (ModuleAccessList.Where(ma => ma.CID == b.CID).Select(ma => ma.HasBuildingProfileAccess).FirstOrDefault()) ? 1 : 0)
                            );
                }

                UserDataforBuildingMap = "{" + String.Join(",", list.ToArray()) + "}";
            }

            private String CreateCostsString(IEnumerable<DiagnosticsResult> diagnostics, Int32 bid)
            {
                if (diagnostics == null)
                    return String.Empty;

                var building = filteredClientBuildingsWithSettings.Where(b => b.BID == bid).First(); //get building settings

                if (building != null)
                    return CultureHelper.FormatCurrencyAsString(building.BuildingSettings.First().LCID, diagnostics.Where(bd => bd.BID == bid).Select(bd => bd.CostSavings).Sum());

                return String.Empty;
            }

            private Int32 CreateFaults(IEnumerable<DiagnosticsResult> diagnostics, Int32 bid)
            {
                if (diagnostics == null) 
                    return 0;

                return diagnostics.Where(bd => bd.BID == bid).Count();
            }

            private Int32 CostThresholdValue(IEnumerable<DiagnosticsResult> diagnostics, Int32 bid)
            {
                if (diagnostics == null) 
                    return 0;

                var buildingVariable = DataMgr.BuildingVariableDataMapper.GetBuildingVariableByBIDAndBVID(bid, BusinessConstants.BuildingVariable.AnnualBuildingCostThresholdBVID);
                var costSavings = diagnostics.Where(bd => bd.BID == bid).Select(bd => bd.CostSavings).Sum();
                var costThreshold = (buildingVariable != null) ? Convert.ToDouble(buildingVariable.Value) / 365 : 0;

                if (costThreshold == 0)                    
                    return 0;
                if (costSavings == 0)
                    return 1;
                if (costSavings > 0 && costSavings < (costThreshold / 2))
                    return 2;
                if (costSavings < costThreshold && costSavings >= (costThreshold / 2))
                    return 3;
                if (costSavings >= costThreshold)
                    return 4;

                return 0;
            }

        #endregion
    }
}