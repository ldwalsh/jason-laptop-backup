﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="Login.ascx.cs" Inherits="CW.Website._controls.Login" EnableViewState="false"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

	<script type="text/javascript">
	    var myId = "<%=this.ClientID%>";
	    (function ($) {
	        $.fn.invisible = function () {
	            return this.each(function () {
	                $(this).css({ "visibility": "hidden", "display": "none" });
	            });
	        };
	        $.fn.visible = function () {
	            return this.each(function () {
	                $(this).css({ "visibility": "visible", "display": "block" });
	            });
	        };
	    }(jQuery));

	    function MakeClientID(unqualifiedId) {
	        return (myId + "_" + unqualifiedId);
	    }

        // Always returns without trailing forward slash
	    function getBaseUrl() {
	        return (window.location.protocol + "//" + window.location.host);
	    }

	    function resetUrl() {
	        var url = getBaseUrl() + "<%=mHomeUrl%>";
	        window.history.pushState("cw", document.title, url);
	        setLoginState("-1");
	        var $panel = $("#" + MakeClientID("pnlLogin"));
	        var onkeypressval = ($panel).attr("onkeypress");

	        var id = $("#" + MakeClientID("lastLoginPanelDefaultButtonID")).val();
	        onkeypressval = onkeypressval.replace(MakeClientID(id), MakeClientID("btnContinueOnEmail"));
	        ($panel).attr("onkeypress", onkeypressval);
        }

		function ShowLoginModal() {
		    try {
		        $find('mpeLoginBehavior').show();
		        $find('mpeForgotBehavior').hide();
		    }
		    catch (e) {
		    }
		    HideValidators();
		    resetUrl();

		    setLoginState("0");
		    $("#" + MakeClientID("lblLoginIntro")).text('Please enter your email address.');
		    $("#" + MakeClientID("emailAddressModal")).visible();
		    $this.DisableValidators("txtLoginEmail", false);
		    $("#" + MakeClientID("passwordModal")).invisible();
		    $("#" + MakeClientID("pnlLogin")).visible();
		    $("#" + MakeClientID("mpeLogin")).visible();
		    $("#" + MakeClientID("clientModal")).invisible();
		    $("#" + MakeClientID("radCaptcha")).invisible();
		    $("#" + MakeClientID("btnContinueOnEmail")).prop('disabled', false);
		    var ddlOrganizationClient = document.getElementById(MakeClientID("ddlOrganizationClient"));
		    if (null != ddlOrganizationClient) {
		        ddlOrganizationClient.selectedIndex = 0;
		    }

		    var ddlProviderClient = document.getElementById(MakeClientID("ddlProviderClient"));
		    if (null != ddlProviderClient) {
		        ddlProviderClient.selectedIndex = 0;
		    }

		    var ddlProvider = document.getElementById(MakeClientID("ddlProvider"));
		    if (null != ddlProvider) {
		        ddlProvider.selectedIndex = 0;
		    }
		    var rm = $("#" + MakeClientID("rememberedName")).val();
		    var obj = $("#" + MakeClientID("txtLoginEmail"));
		    obj.val(null == rm ? "" : rm);
		    obj.focus();
		}

		function showOrganizationalDiv() {
		    $("#" + MakeClientID("divOrganizationSelection")).visible();
		    $("#" + MakeClientID("divProviderSelection")).invisible();
        }
		function showProviderDiv() {
		    $("#" + MakeClientID("divProviderSelection")).visible();
		    $("#" + MakeClientID("divOrganizationSelection")).invisible();
		}
		function HideModals() {
			$find('mpeLoginBehavior').hide();
			$find('mpeForgotBehavior').hide();
			HideValidators();
		}

		function ShowForgotModal() {
		    //$("#" + MakeClientID("btnActiveID")).val("");
		    setLoginState("-2");
			$find('mpeForgotBehavior').show();
			document.getElementById('<%=pnlForgot.ClientID%>').style.display = "block";
			document.getElementById('<%=pnlForgot.ClientID%>').style.visibility = "visible";
			$find('mpeLoginBehavior').hide();
			document.getElementById('<%=txtForgotEmail.ClientID%>').focus();
		}

		function HideRenewSessionModal() {
		}

		function HideValidators() {
			try {
				var myValidators = Page_Validators;

				for (i = 0; i < myValidators.length; i++) {
					var myValidator = myValidators[i];

					//hide validators
					document.getElementById(myValidator.id).style.display = "none";

					//hide extenders
					var myValidatorID = myValidator.id + "Extender";

					myValidatorID = myValidatorID.substring(myValidatorID.indexOf('_') + 1, myValidatorID.length);

					try {
						$find(myValidatorID).hide();
					}
					catch (e) {
					}

					//hide extenders after session expire postback
					try {
						$find("loginEmailRequiredValidatorExtender").hide();
						document.getElementById("loginPasswordRequiredValidatorExtender_popupTable").style.visibility = "hidden";
						document.getElementById("forgotEmailRequiredValidatorExtender_popupTable").style.visibility = "hidden";                        
					}
					catch (e) {
					}
				}

				try {
					document.getElementById("ctl00_Login_lblLoginError").style.display = "none";
				}
				catch (e) {
				}
				try {
					document.getElementById("ctl00_Login_lblClientError").style.display = "none";
				}
				catch (e) {
				}
				try {
					document.getElementById("ctl00_Login_lblForgotError").style.display = "none";
				}
				catch (e) {
				}
			}
			catch (e) {
			}
		}

		function setLoginState(state) {
		    document.getElementById(MakeClientID("hdnLoginState")).value = state;
		}

		function getLoginState() {
		    return (parseInt(document.getElementById(MakeClientID("hdnLoginState")).value));

		}
	    function OnLogin() {
	        SetHiddenOrganizationClientSelected();
	        SetHiddenProviderClientSelected();
	    }

		function SetTimeZoneOffset() {
		    var name = "TimeZoneOffset";
			
			if (document.getElementById(name) != null) return;
			
			var form = document.forms[0];
			var hidden = document.createElement("input");

			hidden.id    = name;
			hidden.name  = name;
			hidden.type  = "hidden";
			hidden.value = (new Date().getTimezoneOffset() / -60);

			form.appendChild(hidden);
		}


		function SetHiddenOrganizationClientSelected() {
			var ddl = document.getElementById("ctl00_Login_ddlOrganizationClient");
			if (ddl != null && ddl.options[ddl.selectedIndex].value != -1) {
			    document.getElementById("ctl00_Login_hdnOrganizationClientSelected").value = ddl.options[ddl.selectedIndex].value;
			}
			else if (ddl != null ) {
			    document.getElementById("ctl00_Login_hdnOrganizationClientSelected").value = "";
			}
		}

		function SetHiddenProviderSelected() {
		    var ddl = document.getElementById("ctl00_Login_ddlProvider");
		    if (ddl != null && ddl.options[ddl.selectedIndex].value != -1) {
		        document.getElementById("ctl00_Login_hdnProviderSelected").value = ddl.options[ddl.selectedIndex].value;
		    }
		    else if (ddl != null) {
		        document.getElementById("ctl00_Login_hdnProviderSelected").value = "";
		    }
		}

		function SetHiddenProviderClientSelected() {
		    var ddl = document.getElementById("ctl00_Login_ddlProviderClient");		    
		    if (ddl != null && ddl.options[ddl.selectedIndex].value != -1) {
		        document.getElementById("ctl00_Login_hdnProviderClientSelected").value = ddl.options[ddl.selectedIndex].value;
		    }
		    else if (ddl != null) {
		        document.getElementById("ctl00_Login_hdnProviderClientSelected").value = "";
		    }
		}
		
		var $this = new function()
		{
		    var millisecondsBeforeSessionTimeout = 120000; // 2 minutes before server timeout event occurs
			var renewedTimer;
			var expiresTimer;
			var initialTimeout = 0;

			this.showedRenewModalCallbackArray = [];

			this.showOrgOrProviderClient = function (provider) {
			    var id = MakeClientID("rblClientType") + "_" + (provider ? "1" : "0");
			    $("#" + id).click();
			}
			this.DisableValidators = function (ids, bDisable) {
			    if (null != ids && typeof Page_Validators != 'undefined') {
			        var arrayIDs = ids.split(',');
			        var clientid = "<%=ClientID%>";
			        var ctrlID = clientid + "_";
			        var ctrlIDLen = ctrlID.length;
			        for (i = 0; i <= Page_Validators.length; i++) {
			            if (Page_Validators[i] != null) {
			                var sCtrltoValidate = Page_Validators[i].controltovalidate;
			                if (null != sCtrltoValidate && sCtrltoValidate.length > ctrlIDLen) {
			                    if (sCtrltoValidate.substr(0, ctrlIDLen) == ctrlID) {
			                        var sID = sCtrltoValidate.substr(ctrlIDLen);
			                        if (-1 != jQuery.inArray(sID, arrayIDs)) {
			                            if (Page_Validators[i].enabled != !bDisable) {
			                                Page_Validators[i].enabled = !bDisable;
			                                Page_Validators[i].style.display = !bDisable ? 'inline' : 'none';
			                                document.getElementById(Page_Validators[i].id).style.display = !bDisable ? 'inline' : 'none';
                                        }
			                        }
			                    }
			                }
			            }
			        }
			    }
			};
		    this.onAuthenticated = function (partialUrl) {
		        // partialUrl will always have leading forward slash
		        $(location).attr('href', getBaseUrl() + partialUrl);
		    }

		    this.startSession = function (timeout)
			{
				if (renewedTimer != null)
				{
					clearTimeout(renewedTimer);
				}
				
		        initialTimeout = timeout - millisecondsBeforeSessionTimeout;
				renewedTimer = setTimeout($this._showRenewSessionModal, initialTimeout);
			};

			this.renewSession = function()
			{
			    clearTimeout(expiresTimer);
			    expiresTimer = null;
			    $this.KeepSessionAlive();
			};
			
			this.getCookie = function(name) {
			    var regEx = new RegExp(name + "=([^;]+)");
			    var ckValue = regEx.exec(document.cookie);
			    return (null != ckValue ? unescape(ckValue[1]) : null);
			}

		    this.getCurrentUTC = function () {
		        var now = new Date();
		        var nowUTC = (new Date(now.getTime() + (now.getTimezoneOffset() * 60000)));
		        return (nowUTC);
		    }
		    this.getTimeoutInMiliseconds = function (preempt) {
		        // This returns # of milliseconds to wait (if prempt, less 2 minutes) before the session on the server times out.
                // futureUTC is when the session on the server will actually timeout
			    var timeout = 0;
			    var f = $this.getCookie('<%=mKeyForTimeoutCookie%>');
			    if (null != f && f.length > 0) {
			        var futureUTC = new Date(f);
			        var nowUTC = $this.getCurrentUTC();
                    // Calculate how many milliseconds left from now (nowUTC) until the session on the server will timeout (futureUTC)
			        timeout = futureUTC - nowUTC; // milliseconds from now until until the session on the server will timeout
			        if (preempt) {
			            // Now we need to determine wait time 2 minutes before the session on the server will timeout (futureUTC)
			            if (timeout > millisecondsBeforeSessionTimeout) {
			                timeout = timeout - millisecondsBeforeSessionTimeout; // milliseconds
			            }
			        }
			    }
			    return(Math.max(timeout, 0)); // milliseconds
			}
		    this.KeepSessionAlive = function () {
		        var newTimer = $this.getTimeoutInMiliseconds(false);
		        if (0 == newTimer) {
		            // Too late; already timed out
		            document.getElementById('<%=btnLogout.ClientID%>').click();
		        }
		        else {
		            var request = new XMLHttpRequest();
		            request.onreadystatechange = function () {
		                if (request.readyState == 4 && request.status == 200) {
		                    newTimer = $this.getTimeoutInMiliseconds(false);
		                    if (0 == newTimer) {
		                        document.getElementById('<%=btnLogout.ClientID%>').click();
		                    }
		                    else {
		                        $find('mpeRenewSessionBehavior').hide();
		                        renewedTimer = setTimeout($this._showRenewSessionModal, initialTimeout);
		                    }
		                }
		            }

		            try {
		                var url = "/_framework/httpHandlers/SessionKeepAliveHandler.ashx";
		                request.open('GET', url, true);
		                request.send();
		            }
		            catch (err) {
		                document.getElementById('<%=btnLogout.ClientID%>').click();
		            }
		        }
		    }

			this._showRenewSessionModal = function()
			{
			    var newTimer = $this.getTimeoutInMiliseconds(false);
			    if (0 == newTimer) {
			        document.getElementById('<%=btnLogout.ClientID%>').click();
			    }
			    else {
			        var dtFutureUTC = new Date($this.getCookie('<%=mKeyForTimeoutCookie%>'));
			        var dtCurUTC = $this.getCurrentUTC();
			        var dtExpectedTimeoutUTC = dtCurUTC;
			        dtExpectedTimeoutUTC.setMilliseconds(dtExpectedTimeoutUTC.getMilliseconds() + millisecondsBeforeSessionTimeout);
			        if (dtExpectedTimeoutUTC < dtFutureUTC) {
			            // renewed in another tab ?
			            newTimer = dtFutureUTC.getTime() - dtExpectedTimeoutUTC.getTime()
			            if (renewedTimer != null) {
			                clearTimeout(renewedTimer);
			            }
			            renewedTimer = setTimeout($this._showRenewSessionModal, newTimer);
			        }
			        else {
			            $find('mpeRenewSessionBehavior').show();

			            var pnlRenewSesstion_style = document.getElementById('<%=pnlRenewSession.ClientID%>').style;

			            pnlRenewSesstion_style.display = "block";
			            pnlRenewSesstion_style.visibility = "visible";

			            newTimer = $this.getTimeoutInMiliseconds(true);
			            expiresTimer = setTimeout($this._sessionExpired, newTimer);
			        }
			    }

				for (var i=0; i<$this.showedRenewModalCallbackArray.length; i++)
				{
					var f = $this.showedRenewModalCallbackArray[i];

					if (!(f instanceof Function)) continue;

					f();
				}
			}
			
			this._sessionExpired = function()
			{
			    // Am I really expired? Check cookie
			    var newTimer = $this.getTimeoutInMiliseconds(false);
			    if (0 == newTimer) {
			        document.getElementById('<%=btnLogout.ClientID%>').click();
			    }
			    else {
			        $find('mpeRenewSessionBehavior').hide();
			        // Another tab issued a "renew"; therefore simply hide the renew modal
			        if (null != expiresTimer) {
			            clearTimeout(expiresTimer);
			            expiresTimer = null;
			        }
			    }
			}
		};


        function ValidateRequiredSelection(source, args) {

		    var ddlOrganizationClient = document.getElementById("ctl00_Login_ddlOrganizationClient");
		    var ddlProviderClient = document.getElementById("ctl00_Login_ddlProviderClient");

		    if ((ddlOrganizationClient != null && ddlOrganizationClient.options[ddlOrganizationClient.selectedIndex].value != -1) || (ddlProviderClient != null && ddlProviderClient.options[ddlProviderClient.selectedIndex].value != -1)) {
		        args.IsValid = true;
		        return true;
		    }
		    args.IsValid = false;
		    return false;
		}

		function ValidateSingleSelection(source, args) {

		    var ddlOrganizationClient = document.getElementById("ctl00_Login_ddlOrganizationClient");
		    var ddlProviderClient = document.getElementById("ctl00_Login_ddlProviderClient");

		    if ((ddlOrganizationClient != null && ddlOrganizationClient.options[ddlOrganizationClient.selectedIndex].value != -1) && (ddlProviderClient != null && ddlProviderClient.options[ddlProviderClient.selectedIndex].value != -1)) {
		        args.IsValid = false;
		        return false;
		    }
		    args.IsValid = true;
		    return true;
		}
		
	</script>
	
<asp:MultiView ID="mv" runat="server" EnableViewState="true">
	<asp:View ID="loggedOut" runat="server">       
			 <div class="divLogin">
				 <p runat="server">Click here to                  
					 <a href="javascript:void(0)" onclick="ShowLoginModal();" id="lnkLoginShow">Login</a>
				 </p> 
			 </div>

			 <asp:HiddenField ID="hdnReferrer" runat="server" />    
			 <asp:HiddenField ID="hdnLoginState" runat="server" Value="-1" />    

			 <ajaxToolkit:ModalPopupExtender ID="mpeLogin" runat="server"
				TargetControlID="lnkLoginHidden"
				PopupControlID="pnlLogin"  
				BackgroundCssClass="modalBackground"  
				CancelControlID="lnkCancelHidden"
				Y="42"         
				BehaviorID="mpeLoginBehavior"       
				/>
					
						<asp:Panel ID="pnlLogin" CssClass="modalPanel" runat="server" DefaultButton="btnProxy" >   
							<asp:HiddenField ID="lastLoginPanelDefaultButtonID" runat="server" Value="btnProxy"/>
                           
							<a href="javascript:void(0)"  onclick="resetUrl(); return HideModals();" class="modalLink" id="lnkLoginModalCancel">Cancel</a>
							<asp:Button ID="btnProxy" Visible="false" runat="server" />
							<input type="button" class="modalButtonSmall" id="btnLoginModalCancel" onclick="return HideModals();" title="X" />                           
							<asp:HiddenField ID="hdnCaptchaIsVisible" runat="server" />                                                                                                               
							<asp:LinkButton ID="lnkLoginHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>                          
							<asp:LinkButton ID="lnkCancelHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>     
														
							<div id="lblLoginTitle" class="modalTitle">Login</div>                            
							<div id="lblLoginIntro" class="modalContent" runat="server"></div>
							<asp:Label ID="lblLoginError" CssClass="modalError" runat="server"></asp:Label>
							
							<div id="emailAddressModal" runat="server">
                                <div class="divFormModal" >
								    <label id="lblLoginEmail" class="modalLabel">*Email Address:</label>
								    <asp:TextBox ID="txtLoginEmail" CssClass="modalTextbox" MaxLength="100" Columns="50" runat="server"></asp:TextBox>

								    <asp:RequiredFieldValidator ID="loginEmailRequiredValidator" runat="server"
									    ErrorMessage="Email is a required field." 
									    ControlToValidate="txtLoginEmail"  
									    SetFocusOnError="true" 
									    Display="None" 
									    ValidationGroup="Login1">
									    </asp:RequiredFieldValidator>
								    <ajaxToolkit:ValidatorCalloutExtender ID="loginEmailRequiredValidatorExtender" runat="server"
									    BehaviorID="loginEmailRequiredValidatorExtender" 
									    TargetControlID="loginEmailRequiredValidator"
									    HighlightCssClass="validatorCalloutHighlight"
									    Width="175">
									    </ajaxToolkit:ValidatorCalloutExtender>
								    <asp:RegularExpressionValidator ID="loginEmailRegExValidator" runat="server"
									    ErrorMessage="Invalid Email Format."
									    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
									    ControlToValidate="txtLoginEmail"
									    SetFocusOnError="true" 
									    Display="None" 
									    ValidationGroup="Login1">
									    </asp:RegularExpressionValidator>                         
								    <ajaxToolkit:ValidatorCalloutExtender ID="loginEmailRegExValidatorExtender" runat="server"
									    BehaviorID="loginEmailRegExValidatorExtender" 
									    TargetControlID="loginEmailRegExValidator"
									    HighlightCssClass="validatorCalloutHighlight"
									    Width="175">
									    </ajaxToolkit:ValidatorCalloutExtender>
                                </div>
                                <div class="divFormModal">
                                    <asp:CheckBox ID="chkRememberMe" CssClass="modalChekbox" Checked="true" runat="server" />
                                    <span class="modalRememberMe modalContent">- Remember Me</span>
								    <asp:HiddenField ID="rememberedName" Value="" runat="server" />     			
                                </div>
                                <div>
								    <asp:Button ID="btnContinueOnEmail" CssClass="modalButton" Text="Continue" ValidationGroup="Login1" runat="server" OnClick="ContinueOnEmail_Click" OnClientClick="SetTimeZoneOffset();" />
                                </div>
							</div>

    						<telerik:RadCaptcha ID="radCaptcha" runat="server" ValidationGroup="Login2" CaptchaTextBoxLabelCssClass="modalLabel" CaptchaTextBoxLabel="*Enter Code:" ></telerik:RadCaptcha>
							<div id="passwordModal" runat="server">
								<div class="divFormModal">
                                    <label id="lblLoginPassword" class="modalLabel">*Password:</label>
								    <asp:TextBox ID="txtLoginPassword" CssClass="modalTextbox" MaxLength="20" Columns="25" runat="server" TextMode="Password"></asp:TextBox>
								    <asp:RequiredFieldValidator ID="loginPasswordRequiredValidator" runat="server"
									    ErrorMessage="Password is a required field." 
									    ControlToValidate="txtLoginPassword"  
									    SetFocusOnError="true" 
									    Display="None" 
									    ValidationGroup="Login2">
									    </asp:RequiredFieldValidator>
								    <ajaxToolkit:ValidatorCalloutExtender ID="loginPasswordRequiredValidatorExtender" runat="server"
									    BehaviorID="loginPasswordRequiredValidatorExtender" 
									    TargetControlID="loginPasswordRequiredValidator"
									    HighlightCssClass="validatorCalloutHighlight"
									    Width="175">
									    </ajaxToolkit:ValidatorCalloutExtender>								
							    </div>
                                <div class="divFormModal"  id="forgotPasswordModal">
								    <a href="javascript:void(0)" onclick="return ShowForgotModal();" class="modalLink">Forgot Password?</a>
							    </div>
    							<asp:Button ID="btnContinueOnPwd" CssClass="modalButton" Text="Continue" ValidationGroup="Login2" runat="server" OnClick="ContinueOnPwd_Click" />
							</div>

                            <div id="clientModal" runat="server">
                                <asp:Label ID="lblClientError" CssClass="modalError" runat="server"></asp:Label>
                                <asp:CustomValidator ID="SelectionRequiredCustomValidator" 
                                     runat="server"
                                     ClientValidationFunction="ValidateRequiredSelection"
                                     ErrorMessage="A client selection is required." ForeColor="Red"
                                     Display="Dynamic"
                                     ValidationGroup="Login3"></asp:CustomValidator>	
                                <asp:CustomValidator ID="SingleSelectionCustomValidator" 
                                     runat="server"
                                     ClientValidationFunction="ValidateSingleSelection"
                                     ErrorMessage="Please select only one client." ForeColor="Red"
                                     Display="Dynamic"
                                     ValidationGroup="Login3">
                                </asp:CustomValidator>

                                <br /><hr />
                                <div class="divFormModal" id="clientType" runat="server">
                                    <label class="modalLabel labelTwoColumn">Client Type:</label>
                                    <asp:RadioButtonList ID="rblClientType" CssClass="modalRadio" AutoPostBack="false" RepeatDirection="Horizontal" runat="server">
                                        <asp:ListItem Text="Organizational" Value="0" onclick="showOrganizationalDiv();" />
                                        <asp:ListItem Text="Provider" Value="1" onclick="showProviderDiv();"/>
                                    </asp:RadioButtonList>
                                </div>                                
                                <div class="divFormModal" id="divOrganizationSelection" runat="server">                                
								    <label id="lblClientLabel" class="modalLabel" runat="server">Select Client:</label>    
								    <asp:DropDownList ID="ddlOrganizationClient" CssClass="modalDropdown" AppendDataBoundItems="true" runat="server">
									    <asp:ListItem Text="Select one..." Value="-1"></asp:ListItem>
								    </asp:DropDownList>
								    <asp:HiddenField ID="hdnOrganizationClientSelected" Value="" runat="server" />     			                                
							    </div>

                                <div id="divProviderSelection" runat="server">                                    
                                    <div class="divFormModal">
                                        <label id="lblProvider" class="modalLabel">Select Provider:</label>    
								        <asp:DropDownList ID="ddlProvider" CssClass="modalDropdown" AppendDataBoundItems="true" runat="server" AutoPostBack="true" onchange="SetHiddenProviderSelected();"  OnSelectedIndexChanged="ddlProvider_OnSelectedIndexChanged">
									        <asp:ListItem Text="Select one..." Value="-1"></asp:ListItem>
								        </asp:DropDownList>
                                        <asp:HiddenField ID="hdnProviderSelected" Value="" runat="server" /> 
                                    </div>
                                    <div class="divFormModal">
								        <label id="lblProviderClient" class="modalLabel">Select Client:</label>    
								        <asp:DropDownList ID="ddlProviderClient" CssClass="modalDropdown" AppendDataBoundItems="true" runat="server">
                                            <asp:ListItem Text="Select provider first..." Value="-1"></asp:ListItem>
								        </asp:DropDownList>
								        <asp:HiddenField ID="hdnProviderClientSelected" Value="" runat="server" />                                         
							        </div>
                                </div>
							   
                                <br /><hr /><br />
								<asp:Button ID="btnLogin" CssClass="modalButton" Text="Login" ValidationGroup="Login3" runat="server" OnClick="loginButton_Click" OnClientClick="OnLogin();" />                                
                            </div>
						</asp:Panel>

			    <ajaxToolkit:ModalPopupExtender ID="mpeForgot" runat="server"
				TargetControlID="lnkForgotHidden"
				PopupControlID="pnlForgot" 
				BackgroundCssClass="modalBackground"  
				CancelControlID="lnkForgotCancelHidden"  
				Y="42"        
				BehaviorID="mpeForgotBehavior"                  
				/>
					
						<asp:Panel ID="pnlForgot" ClientIDMode="Inherit" CssClass="modalPanel" runat="server" DefaultButton="btnForgot">
							<a href="javascript:void(0)" onclick="return HideModals();" class="modalLink" id="lnkForgotModalCancel">Cancel</a>
							<input type="button" class="modalButtonSmall" id="btnForgotModalCancel" onclick="return HideModals();" title="X" />
							<asp:LinkButton ID="lnkForgotHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>      
							<asp:LinkButton ID="lnkForgotCancelHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton> 
							
							<div id="lblForgotTitle" class="modalTitle">Forgot Password</div>
							<label id="lblForgotIntro" class="modalContent">Enter your email to have a new password generated and emailed.</label>  
							<asp:Label ID="lblForgotError" CssClass="modalError" runat="server"></asp:Label>
							   							   
							<div class="divFormModal">
								<label id="lblForgotEmail" class="modalLabel">*Email Address:</label>
								<asp:TextBox ID="txtForgotEmail" CssClass="modalTextbox" MaxLength="100" Columns="50" runat="server"></asp:TextBox>

								<asp:RequiredFieldValidator ID="forgotEmailRequiredValidator" runat="server"
									ErrorMessage="Email is a required field." 
									ControlToValidate="txtForgotEmail"  
									SetFocusOnError="true" 
									Display="None" 
									ValidationGroup="Forgot">
									</asp:RequiredFieldValidator>
								<ajaxToolkit:ValidatorCalloutExtender ID="forgotEmailRequiredValidatorExtender" runat="server"
									BehaviorID="forgotEmailRequiredValidatorExtender" 
									TargetControlID="forgotEmailRequiredValidator"
									HighlightCssClass="validatorCalloutHighlight"
									Width="175">
									</ajaxToolkit:ValidatorCalloutExtender>
								<asp:RegularExpressionValidator ID="forgotEmailRegExValidator" runat="server"
									ErrorMessage="Invalid Email Format."
									ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
									ControlToValidate="txtForgotEmail"
									SetFocusOnError="true" 
									Display="None" 
									ValidationGroup="Forgot">
									</asp:RegularExpressionValidator>                         
								<ajaxToolkit:ValidatorCalloutExtender ID="forgotEmailRegExValidatorExtender" runat="server"
									BehaviorID="forgotEmailRegExValidatorExtender" 
									TargetControlID="forgotEmailRegExValidator"
									HighlightCssClass="validatorCalloutHighlight"
									Width="175">
									</ajaxToolkit:ValidatorCalloutExtender>
							</div>
							<div class="divFormModal">
								<asp:Button ID="btnForgot" CssClass="modalButton" Text="Submit" ValidationGroup="Forgot" runat="server" OnClick="forgotButton_Click" />
							</div>
																		 
						</asp:Panel>
	</asp:View>
	<asp:View ID="loggedIn" runat="server">
		<div class="divLogin">
            <a href="/Home.aspx"><img alt="" src="" id="imgSmallLogo" runat="server" /></a>
            <span runat="server"><%=mWelcome%>'s <asp:HyperLink ID="lnkMyAccount" NavigateUrl="~/MyAccount.aspx" runat="server">Account</asp:HyperLink> &nbsp;|&nbsp; <asp:LinkButton ID="lnkLogout" OnClick="logoutButton_Click" CausesValidation="false" runat="server" Text="Logout"></asp:LinkButton></span>
		</div>

			<ajaxToolkit:ModalPopupExtender ID="mpeRenewSession" runat="server"
				TargetControlID="lnkRenewSessionHidden"
				PopupControlID="pnlRenewSession"  
				BackgroundCssClass="modalBackground"  
				Y="42"         
				BehaviorID="mpeRenewSessionBehavior"
				/>
					
				<asp:Panel ID="pnlRenewSession" CssClass="modalPanel" runat="server" DefaultButton="btnRenewSession">                                                
					<asp:LinkButton ID="lnkRenewSessionHidden" CausesValidation="false" Text="" runat="server"></asp:LinkButton>                                                                                                      					
                    <div id="lblRenewSessionTitle" class="modalTitle">Renew Session</div>                            
					<label id="lblRenewSessionIntro" class="modalContent">Your session is about to expire. Do you wish to renew it?</label>
					<br />   
					<br />
					<div id="buttons">
						<asp:Button ID="btnLogout" CssClass="modalButton" OnClick="logoutButton_Click" CausesValidation="false" runat="server" Text="Logout" />
						<asp:Button ID="btnRenewSession" CssClass="modalButton" Text="Renew Session" CausesValidation="false" runat="server" OnClientClick="$this.renewSession(); return(false)" />                        
					</div>
				</asp:Panel>

	</asp:View>
</asp:MultiView>


