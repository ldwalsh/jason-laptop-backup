﻿using CW.Data;
using CW.Utility;
using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
	[ParseChildren(true)]
	public partial class DropDowns: SiteUserControl
	{
		#region CLASS

			public class ItemSelectedEventArgs: EventArgs
			{
				public Int32 SelectedIndex;
				public String SelectedValue;
			}

		#endregion

		#region STRUCT

			public struct BindingInfoStruct
			{
				public String Value;
				public String Text;
			}

		#endregion
        
		#region ENUM

			public enum ViewModeEnum
			{
				Client,
				Building,
				EquipmentClass,
				Equipment,
				Analysis
			}

            public enum ElementEnum
            {
                Div,
                Label,
                DropDown
            }

		#endregion

		#region STATIC

			#region field

				private static readonly IEnumerable<String> valuesToNull;

				private static readonly Dictionary<ViewModeEnum,BindingInfoStruct> bindingInfos;

			#endregion
            
			#region constructor

				static DropDowns()
				{
					valuesToNull = new[]{"-1", "all", "0", ""};

					bindingInfos =
					new Dictionary<ViewModeEnum,BindingInfoStruct>
					{
						{ViewModeEnum.Analysis,       new BindingInfoStruct{Text=PropHelper.G<Analyse>(p=>p.AnalysisName), Value=PropHelper.G<Analyse>(p=>p.AID)}},
						{ViewModeEnum.Equipment,      new BindingInfoStruct{Text=PropHelper.G<Equipment>(p=>p.EquipmentName), Value=PropHelper.G<Equipment>(p=>p.EID)}},
						{ViewModeEnum.EquipmentClass, new BindingInfoStruct{Text=PropHelper.G<EquipmentClass>(p=>p.EquipmentClassName), Value=PropHelper.G<EquipmentClass>(p=>p.EquipmentClassID)}},
						{ViewModeEnum.Building,       new BindingInfoStruct{Text=PropHelper.G<Building>(p=>p.BuildingName), Value=PropHelper.G<Building>(p=>p.BID)}},
						{ViewModeEnum.Client,         new BindingInfoStruct{Text=PropHelper.G<Client>(p=>p.ClientName), Value=PropHelper.G<Client>(p=>p.CID)}},
					};
				}

			#endregion

			#region method

				private static void Bind(DropDownList ddl, IEnumerable data, IEnumerable<ListItem> otherItems)
				{
					var mode = EnumHelper.Parse<ViewModeEnum>(ddl.ID.Substring(3));

					//DropDownSequencer.ClearAndCreateDefaultItem(ddl);

					ddl.DataValueField = bindingInfos[mode].Value;
					ddl.DataTextField  = bindingInfos[mode].Text;

					ddl.DataSource = data;

					ddl.DataBind();

					//

					if (otherItems == null) return;

					foreach (var item in otherItems)
					{
						ddl.Items.Insert(1, item);
					}
				}

			#endregion

		#endregion

		#region event

			public event Action<DropDowns,ItemSelectedEventArgs> ClientChanged;
			public event Action<DropDowns,ItemSelectedEventArgs> BuildingChanged;
			public event Action<DropDowns,ItemSelectedEventArgs> EquipmentClassChanged;
			public event Action<DropDowns,ItemSelectedEventArgs> EquipmentChanged;

			//public event EventHandler Selected;
			//public event EventHandler Deselected;
        
		#endregion

		#region field

			private String                                      dropDownClass;            
			private DropDownSequencer                           dropDownSequencer;
			private IEnumerable<DropDownList>                   ddls;
			private ViewModeEnum                                startViewMode;
			private ViewModeEnum                                viewMode;  
            private ViewModeEnum?                               startOptionalViewMode;  
			public  String                                      validationGroup;
            
			private Boolean changedBID;
			private Boolean changedEquipmentClassID;
			private Boolean changedEID;

		#endregion

		#region method

			#region override

				protected override void OnPreRender(EventArgs e)
				{
					foreach (var ddl in ddls)
					{
						ddl.Parent.Visible = visibleDropDowns.Contains(ddl);
					}

					ddls.ForEach(_=>_.CssClass = DropDownClass);

					//

                    var divs = new[] { divClient, divBuilding, divEquipmentClass, divEquipment, divAnalysis };
                    var labels = new[]{lblClient, lblBuilding, lblEquipmentClass, lblEquipment, lblAnalysis };
					var validators = new[]{clientRequiredValidator, buildingRequiredValidator, equipmentClassRequiredValidator, equipmentRequiredValidator, analysisRequiredValidator };
					var validatorExtendors = new[]{clientRequiredValidatorExtender, buildingRequiredValidatorExtender, equipmentClassRequiredValidatorExtender, equipmentRequiredValidatorExtender, analysisRequiredValidatorExtender };

                    var start = (Int32)StartViewMode;

                    validators = validators.Skip(start).ToArray();
                    validatorExtendors = validatorExtendors.Skip(start).ToArray();
                    labels = labels.Skip(start).ToArray();
                    ddls = ddls.Skip(start).ToArray();

                    var optionalMode = startOptionalViewMode != null ? (Int32)StartOptionalViewMode - start : validators.Length;

                    for (var i = 0; i < validators.Length; i++)
                    {
                        if (ddls.ElementAt(i).Visible && validationEnabled && (i < optionalMode))
                        {
                            validators[i].Enabled = validatorExtendors[i].Enabled = true;
                            validators[i].ValidationGroup = ValidationGroup;     
                            
                            if(!labels[i].InnerText.StartsWith("*"))
                                labels[i].InnerText = labels[i].InnerText.Insert(0, "*");
                        }
                        else
                        {
                            validators[i].Enabled = validatorExtendors[i].Enabled = false;

                            if(labels[i].InnerText.StartsWith("*"))
                                labels[i].InnerText = labels[i].InnerText.Remove(0, 1);
                        }
                    }

                    if (DropDownsStylesDict != null && DropDownsStylesDict.Keys.Any())
                    {
                        if (DropDownsStylesDict.ContainsKey(ElementEnum.Div))
                        {
                            divs.ForEach(_ => _.Attributes.Remove("class"));
                            divs.ForEach(_ => _.Attributes.Add("class", DropDownsStylesDict[ElementEnum.Div]));
                        }

                        if (DropDownsStylesDict.ContainsKey(ElementEnum.Label))
                        {
                            labels.ForEach(_ => _.Attributes.Remove("class"));
                            labels.ForEach(_ => _.Attributes.Add("class", DropDownsStylesDict[ElementEnum.Label]));
                        }

                        if (DropDownsStylesDict.ContainsKey(ElementEnum.DropDown)) ddls.ForEach(_ => _.CssClass = DropDownsStylesDict[ElementEnum.DropDown]);
                    }

					base.OnPreRender(e);
				}

			#endregion

			#region method:page

				protected void Page_Init()
				{
					
				}

				protected void Page_Load(Object sender, EventArgs e)
				{
					ddls = new DropDownList[]{ddlClient, ddlBuilding, ddlEquipmentClass, ddlEquipment, ddlAnalysis};
					
					dropDownSequencer = new DropDownSequencer(visibleDropDowns);
                   
					//dropDownSequencer.Selected   += dropDownSequencer_Selected;
					//dropDownSequencer.Deselected += dropDownSequencer_Deselected;

					var id = Request.Params.Get("__EVENTTARGET");var pc  = (String.IsNullOrWhiteSpace(id) ? null : FindControl(id.Split('$').Last())); //revise

					if (changedBID && (pc != ddlBuilding))
					{
						BID = BID;
					}

					if (ClientContentTemplate == null) return;

					var container = new Control();

					ClientContentTemplate.InstantiateIn(container);
                
					ClientPlaceHolder.Controls.Add(container);
				}

				protected void Page_FirstLoad()
				{
					var ddl = visibleDropDowns.Reverse().FirstOrDefault(_=>_.SelectedValue == "-1");

					if (ddl == null) return;
					
					dropDownSequencer.ResetSubDropDownsOf(ddl);
				}

			#endregion

            //public void Reset()
            //{
            //    dropDownSequencer.ResetSubDropDownsOf(visibleDropDowns.First());

            //    visibleDropDowns.First().SelectedIndex = -1;
            //}

            //public void ResetOf(DropDownList ddl)
            //{
            //    dropDownSequencer.ResetSubDropDownsOf(ddl);
            //}

			private IEnumerable<DropDownList> visibleDropDowns
			{
				get {return ddls.Skip((Int32)StartViewMode).Take((ViewMode - StartViewMode) +1);}
			}

            //private void dropDownSequencer_Selected(object sender, EventArgs e)
            //{
            //    if (IsSelected) return;

            //    IsSelected = true;

            //    if (Selected == null) return;

            //    Selected(this, EventArgs.Empty);
            //}

            //private void dropDownSequencer_Deselected(object sender, EventArgs e)
            //{
            //    if (!IsSelected || Deselected == null) return;

            //    Deselected(this, EventArgs.Empty);

            //    IsSelected = false;
            //}

			private ItemSelectedEventArgs CreateEventArgs(DropDownList ddl=null)
			{
				return
				(
					(ddl == null) ?
					new ItemSelectedEventArgs{SelectedIndex=0, SelectedValue="-1",} :
					new ItemSelectedEventArgs{SelectedIndex=ddl.SelectedIndex, SelectedValue=ddl.SelectedValue,}
				);
			}

			protected void ddlClient_OnSelectedIndexChanged(Object sender, EventArgs e)
			{
				var ddl = sender as DropDownList;

				dropDownSequencer.ResetSubDropDownsOf(ddlClient);

				ReflectionHelper.FireEvent(ClientChanged, this, CreateEventArgs(ddl));

				if (ddlClient.SelectedValue != "-1") return;

				ReflectionHelper.FireEvent(BuildingChanged, this, CreateEventArgs());
			}

			protected void ddlBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
			{
				var ddl = sender as DropDownList;

				dropDownSequencer.ResetSubDropDownsOf(ddlBuilding);

				ReflectionHelper.FireEvent(BuildingChanged, this, CreateEventArgs(ddl));

				if (changedEquipmentClassID)
				{
					EquipmentClassID = EquipmentClassID;

					return;
				}

				if (ddlBuilding.SelectedValue != "-1") return;

				ReflectionHelper.FireEvent(EquipmentClassChanged, this, CreateEventArgs());
			}

			protected void ddlEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
			{
				var ddl = sender as DropDownList;

				dropDownSequencer.ResetSubDropDownsOf(ddlEquipmentClass);

				ReflectionHelper.FireEvent(EquipmentClassChanged, this, CreateEventArgs(ddl));

                if (changedEID)
                {
                    EID = EID;

                    return;
                }

				if (ddlEquipmentClass.SelectedValue != "-1") return;

				ReflectionHelper.FireEvent(EquipmentChanged, this, CreateEventArgs());
			}

			protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
			{
				var ddl = sender as DropDownList;

				dropDownSequencer.ResetSubDropDownsOf(ddlEquipment);
            
				ReflectionHelper.FireEvent(EquipmentChanged, this, CreateEventArgs(ddl));
			}

			public DropDownList GetDDL(ViewModeEnum viewMode)
			{
				switch (viewMode)
				{
					case ViewModeEnum.Building:       return ddlBuilding;
					case ViewModeEnum.EquipmentClass: return ddlEquipmentClass;
					case ViewModeEnum.Equipment:      return ddlEquipment;
                    case ViewModeEnum.Analysis:       return ddlAnalysis;
                    
					default: throw new NotImplementedException();
				}
			}

            //public HtmlGenericControl GetDivContainer(ViewModeEnum viewMode)
            //{
            //    switch (viewMode)
            //    {                     
            //        case ViewModeEnum.Building:       return divBuilding;
            //        case ViewModeEnum.EquipmentClass: return divEquipmentClass;
            //        case ViewModeEnum.Equipment:      return divEquipment;

            //        default: throw new NotImplementedException();
            //    }
            //}

			private Int32? GetDDLValue(DropDownList ddl)
			{
				return (valuesToNull.Contains(ddl.SelectedValue) ? null : (Int32?)Convert.ToInt32(ddl.SelectedValue));
			}

			private Boolean checkSequencer(ref Boolean c)
			{
				return (c = (dropDownSequencer == null));
			}

			#region method:binders
            
			public void BindClientList(IEnumerable<Object> clients, IEnumerable<ListItem> otherItems=null)
			{
				Bind(ddlClient, clients, otherItems);
			}

            public void BindBuildingList(IEnumerable<Building> buildings, bool includeDefault = true, bool includeViewAll = false, bool includeOptionalDefault = false, IEnumerable<ListItem> otherItems = null)
			{
                DropDownSequencer.ClearAndCreateDefaultItems(ddlBuilding, includeDefault, includeViewAll, includeOptionalDefault);

				Bind(ddlBuilding, buildings, otherItems);
			}

            public void BindEquipmentClassList(IEnumerable<EquipmentClass> equipmentClasses, bool includeDefault = true, bool includeViewAll = false, bool includeOptionalDefault = false, IEnumerable<ListItem> otherItems = null)
			{
                DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipmentClass, includeDefault, includeViewAll, includeOptionalDefault);

				Bind(ddlEquipmentClass, equipmentClasses, otherItems);
			}

            public void BindEquipmentList(IEnumerable<Equipment> equipment, bool includeDefault = true, bool includeViewAll = false, bool includeOptionalDefault = false, IEnumerable<ListItem> otherItems = null)
			{
                DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipment, includeDefault, includeViewAll, includeOptionalDefault);

				Bind(ddlEquipment, equipment, otherItems);
			}

            public void BindAnalysisList(IEnumerable<Analyse> analysis, bool includeDefault = true, bool includeViewAll = false, IEnumerable<ListItem> otherItems = null)
			{
                DropDownSequencer.ClearAndCreateDefaultItems(ddlAnalysis, includeDefault, includeViewAll, false);

				Bind(ddlAnalysis, analysis, otherItems);
			}

			#endregion

		#endregion

		#region property

			[
			TemplateContainer(typeof(TemplateControl)),
			PersistenceMode(PersistenceMode.InnerProperty),
			TemplateInstance(TemplateInstance.Single)
			]
			public ITemplate ClientContentTemplate
			{
				get;
				set;
			}

			[
			TemplateContainer(typeof(TemplateControl)),
			PersistenceMode(PersistenceMode.InnerProperty),
			TemplateInstance(TemplateInstance.Single)
			]
			public ITemplate BuildingContentTemplate
			{
				get;
				set;
			}

            //[
            //ViewStateProperty
            //]
            //private Boolean IsSelected
            //{
            //    get;
            //    set;
            //}

			public String DropDownClass
			{
				set
				{
					dropDownClass = (String.IsNullOrWhiteSpace(dropDownClass) ? "dropdown" : value);
				}
				get
				{
					if (dropDownClass == null)
					{
						DropDownClass = null;
					}

					return dropDownClass;
				}
			}

			public ViewModeEnum StartViewMode
			{
				get {return startViewMode;}
				set
				{
					startViewMode = value;

					if (viewMode < startViewMode)
					{
						ViewMode = startViewMode;
					}
				}
			}

            public ViewModeEnum? StartOptionalViewMode
            {
                get { return startOptionalViewMode; }
                set
                {
                    if (value < StartViewMode) throw new InvalidOperationException("OptionalMode can not supercede StartViewMode.");

                    startOptionalViewMode = value;
                }
            }

			public ViewModeEnum ViewMode
			{
				get {return viewMode;}
				set
				{
					if (value < StartViewMode) throw new InvalidOperationException("ViewMode can not supercede StartViewMode.");

					viewMode = value;
				}
			}

			public Boolean validationEnabled
			{
				get;
				set;
			}

			public String ValidationGroup
			{
				get {return validationGroup;}
				set
				{
					validationGroup = (String.IsNullOrWhiteSpace(value) ? String.Empty : value);
				}
			}

			//

			public Int32? CID
			{
				get {return GetDDLValue(ddlClient);}
			}

			public Int32? BID
			{
				get {return GetDDLValue(ddlBuilding);}
				set
				{
					ddlBuilding.SelectedValue = value.Value.ToString();

					if (checkSequencer(ref changedBID)) return;

					ddlBuilding_OnSelectedIndexChanged(ddlBuilding, null);
				}
			}

			public Int32? EquipmentClassID
			{
				get {return GetDDLValue(ddlEquipmentClass);}
				set
				{
                    if (StartViewMode <= ViewModeEnum.EquipmentClass && ViewMode >= ViewModeEnum.EquipmentClass)
                    {
                        ddlEquipmentClass.SelectedValue = value.Value.ToString();

                        if (checkSequencer(ref changedEquipmentClassID)) return;

                        ddlEquipmentClass_OnSelectedIndexChanged(ddlEquipmentClass, null);
                    }                    
				}
			}

			public Int32? EID
			{
				get {return GetDDLValue(ddlEquipment);}
				set
				{
                    if (StartViewMode <= ViewModeEnum.Equipment && ViewMode >= ViewModeEnum.Equipment)
                    {
                        ddlEquipment.SelectedValue = value.Value.ToString();

                        if (checkSequencer(ref changedEID)) return;

                        ddlEquipment_OnSelectedIndexChanged(ddlEquipment, null);
                    }                    
				}
			}

			public Int32? AID
			{
				get {return GetDDLValue(ddlAnalysis);}
			}

            public Dictionary<DropDowns.ElementEnum, String> DropDownsStylesDict { get; set; }

		#endregion
	}
}