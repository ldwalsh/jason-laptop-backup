﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CW.Website._controls
{
    public partial class YesterdaysDiagnosticSummary : ModularSiteUserControl
    {
        #region fields

            private DateTime mStartDate, mEndDate;
    
        #endregion

        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.Yesterday; } }
           
        #endregion

        #region methods

            #region overrides

                protected override void Initialize()
                {
                    DateTimeHelper.GenerateDefaultDatesYesterday(out mStartDate, out mEndDate, EarliestBuildingTimeZoneID);
                }

                protected override void BindControl()
                {
                    RetrieveAndBindDiagnostics();

                    if (IsProviderView)
                    {
                        if (IsByClient)
                            gridYesterdaysTopPortfolioDiagnosticSummaries.Columns[GetColumnIndexByHeader(gridYesterdaysTopPortfolioDiagnosticSummaries, "Building")].Visible = false;

                        gridYesterdaysTopPortfolioDiagnosticSummaries.Columns[GetColumnIndexByHeader(gridYesterdaysTopPortfolioDiagnosticSummaries, "Client")].Visible = true;
                        gridYesterdaysTopPortfolioDiagnosticSummaries.Columns[GetColumnIndexByHeader(gridYesterdaysTopPortfolioDiagnosticSummaries, "lastColumn")].Visible = false;
                    }
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    var results = GetConcatenatedResults(mStartDate, mEndDate, DataConstants.AnalysisRange.Daily);

                    if (!results.Any())
                        gridYesterdaysTopPortfolioDiagnosticSummaries.EmptyDataText = BusinessConstants.Message.DiagnosticsNoPriorites;

                    return results;
                }

                protected override void BindDiagnostics(IEnumerable<DiagnosticsResult> results)
                {
                    BindYesterdaysTopPortfolioDiagnosticSummaries(results);
                    BindYesterdaysSummary(results, results.Count(), GetCostSavings(results));

                    if (!results.Any())
                        litYesterdaysSummary.Visible = false;
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion

            public void BindYesterdaysTopPortfolioDiagnosticSummaries(IEnumerable<DiagnosticsResult> results)
            {
                gridYesterdaysTopPortfolioDiagnosticSummaries.DataSource = (results.Any()) ? GetDiagnosticsSummaries(results, GetBuildingsWithSettings(results), mStartDate).OrderByDescending(ds => ds.CostSavings).Take(10) : null;
                gridYesterdaysTopPortfolioDiagnosticSummaries.DataBind();
            }

            private void BindYesterdaysSummary(IEnumerable<DiagnosticsResult> results, Int32 totalFaults, IEnumerable<CostSavings> totalCostSavings)
            {
                if (results.Any())
                {
                    if (!IsProviderView || IsByClient)
                    {
                        var summary = new StringBuilder();

                        summary.AppendLine("<ul>");
                        summary.AppendLine("<li><span>Total Faults = " + totalFaults + "</span></li>");

                        foreach (var cs in totalCostSavings)
                            summary.AppendLine("<li><span>Total Avoidable Costs (" + CultureHelper.GetCurrencyISOSymbol(cs.LCID) + ") = " + CultureHelper.FormatCurrencyAsString(cs.LCID, cs.Cost) + "</span></li>");

                        summary.AppendLine("</ul>");

                        litYesterdaysSummary.Text = summary.ToString();
                    }
                }
            }

        #endregion
    }
}