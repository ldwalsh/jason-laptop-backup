﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuildingAlarmSummary.ascx.cs" Inherits="CW.Website._controls.alarm.BuildingAlarmSummary" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.Raw" %>

<div id="gridTbl">

  <asp:GridView ID="gridBuildingAlarmSummary"           
                runat="server"  
                GridLines="None"                                      
                PageSize="10"
                AllowPaging="true" 
                AllowSorting="true" 
                OnRowCreated="gridBuildingAlarmSummary_RowCreated"
                OnPageIndexChanging="gridBuildingAlarmSummary_PageIndexChanging"
                OnSorting="gridBuildingAlarmSummary_Sorting" 
                OnDataBound="gridBuildingAlarmSummary_DataBound"
                AutoGenerateColumns="false">
    <Columns>
      <asp:TemplateField SortExpression="BuildingName" HeaderText="Building">
        <ItemTemplate><label title="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(_ => _.BuildingName)) %>"><%# StringHelper.TrimText(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.BuildingName)), 25) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="ActiveAlarmCount" HeaderText="#"> 
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.ActiveAlarmCount)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="ActiveAlarmPct" HeaderText="%">
        <ItemTemplate><label><%# Math.Round(Convert.ToDouble(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.ActiveAlarmPct))), 2) + "%" %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="NumOfOccurrences" HeaderText="# Of Occurrences">
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.NumOfOccurrences)) %></label></ItemTemplate>
      </asp:TemplateField>

      <%--
      <asp:TemplateField SortExpression="AvgTimeInAlarm" HeaderText="Average Time In Alarm Per Day (Min.)">
        <ItemTemplate><label><%# Math.Round(Convert.ToDouble(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.AvgTimeInAlarm))), 1) %></label></ItemTemplate>
      </asp:TemplateField>
      --%>

      <asp:TemplateField SortExpression="TotalAvoidableCost" HeaderText="Related Cost">
        <ItemTemplate><label><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.LCID))), Convert.ToString(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.TotalAvoidableCost)))) %></label></ItemTemplate>
      </asp:TemplateField>
    </Columns>

    <HeaderStyle CssClass="tblTitle" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <EmptyDataRowStyle CssClass="dockEmptyGridLabel" />
    <PagerSettings PageButtonCount="10" />
  </asp:GridView>

</div>