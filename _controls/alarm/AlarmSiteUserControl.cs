﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Linq.Expressions;

using CW.Data;
using CW.Data.Models.Raw;
using CW.Website._framework;
using CW.Common.Constants;
using CW.Business.Results;
using CW.Data.Extensions;
using CW.Data.Collections;
using CW.Data.Helpers;

namespace CW.Website._controls.alarm
{
    public static class AlarmSiteUserControlExtensions
    {
        public static IEnumerable<GetBuildingRawDataWithCalc> Filter(this IEnumerable<GetBuildingRawDataWithCalc> data, IEnumerable<int> bids)
        {
            return data.Where(d => bids.Contains(d.BID));
        }
    }

    public abstract class AlarmSiteUserControl : SiteUserControl
    {
        #region Fields

            private IEnumerable<GetBuildingRawDataWithCalc> results;
            
        #endregion

        #region Properties

            protected IEnumerable<GetBuildingRawDataWithCalc> SelectedAlarmData { get; set; }
            protected IEnumerable<GetBuildingRawDataWithCalc> CurrentAlarmData { get; set; }
            protected Label ErrorOuputLabel { get; private set; }
            protected string QueryStringDataDisplay { get; set; }

            //gets and sets the gridview viewstate query string data for the dataview
            protected NameValueCollection QueryStringData
            {
                get { return ViewState["QueryStringData"] as NameValueCollection ?? null; }
                set { ViewState["QueryStringData"] = value; }
            }

            //gets and sets the gridview viewstate sort direction for the dataview
            protected string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            protected string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

            protected IEnumerable<Building> FilteredBuildings
            {
                get
                {
                    return DataMgr.BuildingDataMapper.FilterBuildingsByPointClassID(siteUser.VisibleBuildings.Select(b => b.BID).ToList(), 
                                                                                         BusinessConstants.PointClass.AlarmPointClassID);
                }
            }

            protected string NoAlarmDataMessage {
                get { return "No alarm data found."; }
            }

            protected string NoAlarmDataOrActiveAlarmsMessage
            {
                get { return "No data or active alarm occurrences found."; }
            }

            protected long StartDateMilliseconds
            {
                get
                {
                    long sd;
                    bool isDate = long.TryParse(Request.QueryString["sd"], out sd);

                    if (isDate) ViewState["sd"] = sd;

                    return (long)ViewState["sd"];
                }
            }

            protected long EndDateMilliseconds
            {
                get
                {
                    long ed;
                    bool isDate = long.TryParse(Request.QueryString["ed"], out ed);

                    if (isDate) ViewState["ed"] = ed;

                    return (long)ViewState["ed"];
                }
            }

            protected int Days
            {
                get { return SetNumOfDaysBetweenDates(); }
            }

        #endregion

        #region Load/Set methods

            private IEnumerable<GetBuildingRawDataWithCalc> LoadRawData(AlarmRawDataInputs inputs, bool isLocalTime)
            {
                var drs = new DateRangeSetter();

                if (isLocalTime)
                {
                    drs.SetRangeFromLocalDate(Duration.Daily, inputs.StartDateLocal, inputs.EndDateLocal, null);
                }
                else
                {
                    var tzi = DataMgr.BuildingDataMapper.GetLastestBuildingTimeZoneByCID(inputs.CID);
                    drs.SetRangeFromUTCDate(Duration.Daily, inputs.StartDateUTC, inputs.EndDateUTC, tzi.Id);
                }

                IEnumerable<DateRange> dateRanges = drs.Ranges;

                //var func = mDataManager.AlarmDataMapper.GetAlarmCalcFunc(inputs.AlarmWidget);
                
                results = Enumerable.Empty<GetBuildingRawDataWithCalc>();
                foreach (var dr in dateRanges)
                {
                    inputs.StartDateUTC = dr.StartDateUTC;
                    inputs.EndDateUTC = dr.EndDateUTC;
                    inputs.StartDateLocal = dr.StartDateLocal;
                    inputs.EndDateLocal = dr.EndDateLocal;

                    var grm = new AlarmResultsManager(DataMgr, null, inputs);
                    grm.HasGetFromStorage = false;
                    results = results.Concat(grm.Get().ToList());
                }

                return results;
            }

            protected void LoadSelectedAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum alarmWidget, string bid = "")
            {
                var parsedStartDate = DateSetter.ConvertJSMilliseconds(StartDateMilliseconds);
                var parsedEndDate = DateSetter.ConvertJSMilliseconds(EndDateMilliseconds);

                var inputs = new AlarmRawDataInputs()
                {
                    AlarmWidget = alarmWidget,
                    CID = siteUser.CID,
                    StartDateLocal = parsedStartDate.Date,
                    EndDateLocal = parsedEndDate.Date,
                    Duration = Duration.Daily
                };

                SelectedAlarmData = GetAlarmData(inputs, true, bid);
            }

            protected void LoadCurrentAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum alarmWidget, string bid = "")
            {
                var inputs = new AlarmRawDataInputs()
                {
                    AlarmWidget = alarmWidget,
                    CID = siteUser.CID,
                    StartDateUTC = DateTime.UtcNow,
                    EndDateUTC = DateTime.UtcNow,
                    Duration = Duration.Daily
                };

                CurrentAlarmData = GetAlarmData(inputs, false, bid);
            }

            protected IEnumerable<GetBuildingRawDataWithCalc> GetAlarmData(AlarmRawDataInputs inputs, bool isLocalTime, string bid = "")
            {
                var bids = FilteredBuildings.Select(b => b.BID);

                var results = Enumerable.Empty<GetBuildingRawDataWithCalc>();

                try
                {
                    results = LoadRawData(inputs, isLocalTime).Filter(bids);
                }
                catch(Exception ex)
                {
                    if (ex.IsFatal())
                        throw;

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error getting calculated alarm data", ex);
                }

                if (bid != "" && bid != "-1")
                    results = results.Filter(new int[] { Convert.ToInt32(bid) });

                return results;
            }

        #endregion

        #region Bind Methods

            protected void BindBuildingDropdownList(DropDownList ddlBuildings, bool displayViewAll = true)
            {
                if (FilteredBuildings.Count() > 1)
                {
                    ddlBuildings.Items.Add
                        (
                            new ListItem
                            {
                                Text = "View All",
                                Value = "-1",
                            }
                        );

                    ddlBuildings.DataTextField = "BuildingName";
                    ddlBuildings.DataValueField = "BID";
                    ddlBuildings.DataSource = FilteredBuildings.OrderBy(b => b.BuildingName);

                    ddlBuildings.DataBind();
                }
            }

        #endregion

        #region helper methods

            private int SetNumOfDaysBetweenDates()
            {
                return results.ToList().Select(r => r.DateLoggedLocal.Date).Distinct().Count();
            }

            protected static TOutput GetValidAlarmCalcRow<TInput, TOutput>(IEnumerable<TInput> source, Expression<Func<TInput, bool>> criteria, Expression<Func<TInput, TOutput>> selector)
            {
                var value = GetValidRow<TInput, TOutput>(source, criteria, selector);

                if (value == null) return default(TOutput);

                return value;
            }

            protected static TOutput GetValidRow<TInput, TOutput>(IEnumerable<TInput> source, Expression<Func<TInput, bool>> criteria, Expression<Func<TInput, TOutput>> selector)
            {
                IQueryable<TInput> currentAlarmData = source.AsQueryable();

                return currentAlarmData.Where(criteria).Select(selector).FirstOrDefault();
            }

            [
            Obsolete
            ]
            protected Control FindControlRecursive(String id, Control root = null)
            {
                if (root == null)
                    root = Page;

                if (root.ID == id) return root;

                foreach (Control controlToSearch in root.Controls)
                {
                    var controlToReturn = FindControlRecursive(id, controlToSearch);

                    if (controlToReturn != null) return controlToReturn;
                }

                return null;
            }

            protected void DisplayErrorOutput(String output) //move out to new module specific Page sub-class
            {
                const String lblError = "lblError";

                var lbl = (ErrorOuputLabel ?? FindControlRecursive(lblError) as Label);

                if (lbl == null) return;

                DisplayErrorOutput(output, lbl);

                if (lbl.ID != lblError) return;
                if (String.IsNullOrWhiteSpace(output)) return;

                var lnk = FindControlRecursive("lnkSetFocusMessage") as HtmlAnchor;

                if (lnk == null) return;

                lnk.Focus();
            }

            protected void DisplayErrorOutput(String output, Label errorLabel) //move out to new module specific Page sub-class
            {
                if (String.IsNullOrWhiteSpace(output))
                {
                    errorLabel.Text = String.Empty;
                    errorLabel.Visible = false;

                    return;
                }

                errorLabel.Text = output;
                errorLabel.Visible = true;
            }

            protected void SetGridSorting(string sortDirection, string sortExpression)
            {
                ViewState["SortDirection"] = String.Empty;
                ViewState["SortExpression"] = String.Empty;

                GridViewSortDirection = sortDirection;
                GridViewSortExpression = sortExpression;
            }

        #endregion

        #region Grid Helper Methods

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            protected string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }

                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection); //maintains the currect viewstate sorting for paging
                        else
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection()); //reverses the sort direction on sort
                    }

                    return dataView;
                }
                else
                    return new DataView();
            }

        #endregion
    }
}