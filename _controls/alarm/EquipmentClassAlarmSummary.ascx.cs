﻿using CW.Common.Constants;
using CW.Data.Models.Raw;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.alarm
{
    public partial class EquipmentClassAlarmSummary : AlarmSiteUserControl
    {
         #region fields

            private string initialSortDirection = "ASC";
            private string initialSortExpression = "EquipmentClassName";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback, initially load and bind the Equipment Class Alarm Data and ddl buildings data
                if (!Page.IsPostBack)
                {
                    SetGridSorting(initialSortDirection, initialSortExpression);

                    LoadEquipmentClassAlarmSummaryData();

                    BindBuildingDropdownList(ddlBuildings);
                    BindEquipmentClassAlarmSummaryData();
                }
            }

        #endregion

        #region Load Methods

            private void LoadEquipmentClassAlarmSummaryData(string bid = "-1")
            {
                ddlBuildings.SelectedValue = bid;

                LoadCurrentAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.EquipmentClassAlarmSummary, bid); 
                LoadSelectedAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.EquipmentClassAlarmSummary, bid);
            }

        #endregion

        #region Bind Methods

            private void BindEquipmentClassAlarmSummaryData()
            {
                gridEquipmentClassAlarmSummary.EmptyDataText = NoAlarmDataMessage;

                gridEquipmentClassAlarmSummary.DataSource = GetBuildingEquipmentClassAlarmSummaryData().ToList();
                gridEquipmentClassAlarmSummary.DataBind();
            }

        #endregion

        #region Grid Events

            protected void gridEquipmentClassAlarmSummary_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridEquipmentClassAlarmSummary_Sorting(object sender, GridViewSortEventArgs e)
            {
                LoadEquipmentClassAlarmSummaryData(ddlBuildings.SelectedValue);

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(GetBuildingEquipmentClassAlarmSummaryData().ToList());

                GridViewSortExpression = e.SortExpression;

                gridEquipmentClassAlarmSummary.DataSource = SortDataTable(dataTable, false);
                gridEquipmentClassAlarmSummary.DataBind();
            }

            protected void gridEquipmentClassAlarmSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                LoadEquipmentClassAlarmSummaryData(ddlBuildings.SelectedValue);

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(GetBuildingEquipmentClassAlarmSummaryData().ToList());

                gridEquipmentClassAlarmSummary.DataSource = SortDataTable(dataTable, true);
                gridEquipmentClassAlarmSummary.PageIndex = e.NewPageIndex;
                gridEquipmentClassAlarmSummary.DataBind();
            }

            protected void ggridEquipmentClassAlarmSummary_DataBound(object sender, EventArgs e)
            {
                if (gridEquipmentClassAlarmSummary.HeaderRow != null)
                {
                    GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);

                    TableHeaderCell cell = new TableHeaderCell();
                    row.Controls.Add(cell);

                    cell = new TableHeaderCell();
                    cell.Text = "<u>Current Active Alarms</u>";
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    cell.ColumnSpan = 2;
                    row.Controls.Add(cell);

                    cell = new TableHeaderCell();
                    cell.ColumnSpan = 1;
                    cell.Text = "<u>Active Alarms Over Period</u>";
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    row.Controls.Add(cell);

                    cell = new TableHeaderCell();
                    cell.ColumnSpan = 1;
                    cell.Text = "<u>Diagnostics</u>";
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    row.Controls.Add(cell);

                    gridEquipmentClassAlarmSummary.HeaderRow.Parent.Controls.AddAt(0, row);
                }
            }

        #endregion

        #region DropDownList Event Methods

            protected void ddlBuildings_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                LoadEquipmentClassAlarmSummaryData(ddlBuildings.SelectedValue);

                gridEquipmentClassAlarmSummary.DataSource = GetBuildingEquipmentClassAlarmSummaryData().ToList();
                gridEquipmentClassAlarmSummary.PageIndex = 0;
                gridEquipmentClassAlarmSummary.DataBind();
            }

        #endregion

        #region Get Data Methods

            private IEnumerable<GetBuildingRawDataWithCalc> GetBuildingEquipmentClassAlarmSummaryData()
            {
                var currentAlarmData = CurrentAlarmData.GroupBy(x => new { x.EquipmentClassID, x.LCID }).ToDictionary(x => x.Key.EquipmentClassID + "-" + x.Key.LCID, 
                                                                                                                      x => Tuple.Create<int, double>((x.FirstOrDefault().ActiveAlarmCount), (x.FirstOrDefault().ActiveAlarmPct)));

                //grouped by class and lcid after we pull back cached caclculated data for the users accessible selected building or buildings.
                var selectedAlarmData = (from d in SelectedAlarmData
                                         group d by new { d.EquipmentClassID, d.LCID } into gd
                                         select new GetBuildingRawDataWithCalc
                                         {
                                             LCID = gd.Key.LCID, 
                                             EquipmentClassID = gd.Key.EquipmentClassID,
                                             EquipmentClassName = gd.FirstOrDefault().EquipmentClassName,
                                             NumOfOccurrences = gd.Sum(c => c.NumOfOccurrences),

                                             //TODO: This is still wrong, we cannot weigh each buildings equipmentclass the same amount. 
                                             //TODO: This is also wrong because we cannot weigh each day the same amount, which is also averaged here.
                                             //Only way to do this is to cache the point count and averages so we can filter out users accessable buildings and group by culture first... then caclulate here.
                                             AvgTimeInAlarm = gd.Average(c => c.AvgTimeInAlarm),

                                             //FaultCount = gd.Sum(c => c.FaultCount), 
                                             TotalAvoidableCost = gd.Sum(c => c.TotalAvoidableCost),
                                         });

                return (from sad in selectedAlarmData
                        select new GetBuildingRawDataWithCalc
                        {
                            //selected
                            LCID = sad.LCID,
                            EquipmentClassID = sad.EquipmentClassID,
                            EquipmentClassName = sad.EquipmentClassName,
                            NumOfOccurrences = sad.NumOfOccurrences,
                            AvgTimeInAlarm = sad.AvgTimeInAlarm / Days,
                            //FaultCount = sad.FaultCount,
                            TotalAvoidableCost = sad.TotalAvoidableCost,

                            //current
                            
                            ActiveAlarmCount = (currentAlarmData.ContainsKey(sad.EquipmentClassID + "-" + sad.LCID)) ? currentAlarmData[sad.EquipmentClassID + "-" + sad.LCID].Item1 : 0,
                            ActiveAlarmPct = (currentAlarmData.ContainsKey(sad.EquipmentClassID + "-" + sad.LCID)) ? currentAlarmData[sad.EquipmentClassID + "-" + sad.LCID].Item2 : 0,
                        }).OrderBy(n => n.EquipmentClassName);
            }

        #endregion
    }
}