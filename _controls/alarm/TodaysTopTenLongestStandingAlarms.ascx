﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TodaysTopTenLongestStandingAlarms.ascx.cs" Inherits="CW.Website._controls.alarm.TodaysTopTenLongestStandingAlarms" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.Raw" %>

<div id="divBuilding" class="divDockFormWrapperShortest">
  <div class="divDockFormRight">
    <label class="label">Building:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlBuildings" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" data-buildingMap="attached" lookup-id="ddlBuildings" />
  </div>
</div> 

<div id="gridTbl">

  <asp:GridView ID="gridTodaysTopTenLongestStandingAlarms"           
                runat="server"  
                GridLines="None"
                PageSize="10"
                AllowSorting="true" 
                OnSorting="gridTodaysTopTenLongestStandingAlarms_Sorting"
                AutoGenerateColumns="false">
    <Columns>
      <asp:TemplateField SortExpression="PointName" HeaderText="Alarm">
        <ItemTemplate><label title="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(_ => _.PointName)) %>"><%# Eval(StringHelper.TrimText(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.PointName), 25)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="EquipmentName" HeaderText="Equipment">
        <ItemTemplate><label title="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(_ => _.EquipmentName)) %>"><%# Eval(StringHelper.TrimText(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.EquipmentName), 25)) %></label></ItemTemplate>
      </asp:TemplateField>
        
      <asp:TemplateField SortExpression="BuildingName" HeaderText="Building">
        <ItemTemplate><label title="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(_ => _.BuildingName)) %>"><%# StringHelper.TrimText(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.BuildingName)), 25) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="TimeInAlarm" HeaderText="Today's Total Time in Alarm (Min.)">
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.TimeInAlarm)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="LongestTimeInAlarm" HeaderText="Longest Time In Alarm Per Day (Min.)">
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.LongestTimeInAlarm)) %></label></ItemTemplate>
      </asp:TemplateField>
    </Columns>

    <HeaderStyle CssClass="tblTitle" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <EmptyDataRowStyle CssClass="dockEmptyGridLabel" />
  </asp:GridView>

</div>