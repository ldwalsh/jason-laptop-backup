﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Raw;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CW.Business;
using CW.Utility.Web;

namespace CW.Website._controls.alarm
{
    public partial class TopTenMostFrequentAlarms : AlarmSiteUserControl
    {
        #region fields

            private string initialSortDirection = "DESC";
            private string initialSortExpression = "NumOfOccurrences";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback, initially load and bind the Top Ten Most Frequent Alarms and ddl buildings data
                if (!Page.IsPostBack)
                {
                    SetGridSorting(initialSortDirection, initialSortExpression);

                    LoadTopTenMostFrequentAlarmsData();

                    BindBuildingDropdownList(ddlBuildings);
                    BindTopTenMostFrequentAlarmsData();
                }
            }

        #endregion

        #region Load Methods

            private void LoadTopTenMostFrequentAlarmsData(string bid = "-1")
            {
                ddlBuildings.SelectedValue = bid;

                LoadSelectedAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.TopTenMostFrequentAlarms, bid);
            }

        #endregion

        #region Bind Methods

            private void BindTopTenMostFrequentAlarmsData()
            {
                gridTopTenMostFrequentAlarms.EmptyDataText = NoAlarmDataOrActiveAlarmsMessage;

                gridTopTenMostFrequentAlarms.DataSource = GetTopTenMostFrequentAlarmsData().ToList();
                gridTopTenMostFrequentAlarms.DataBind();
            }

        #endregion

        #region Grid Events

            protected void gridTopTenMostFrequentAlarms_Sorting(object sender, GridViewSortEventArgs e)
            {
                LoadTopTenMostFrequentAlarmsData(ddlBuildings.SelectedValue);

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(GetTopTenMostFrequentAlarmsData().ToList());

                GridViewSortExpression = e.SortExpression;

                gridTopTenMostFrequentAlarms.DataSource = SortDataTable(dataTable, false);
                gridTopTenMostFrequentAlarms.DataBind();
            }

        #endregion

        #region DropDownList Event Methods

            protected void ddlBuildings_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                LoadTopTenMostFrequentAlarmsData(ddlBuildings.SelectedValue);

                gridTopTenMostFrequentAlarms.DataSource = GetTopTenMostFrequentAlarmsData().ToList();
                gridTopTenMostFrequentAlarms.DataBind();
            }

        #endregion

        #region Get Data Methods

            private IEnumerable<GetBuildingRawDataWithCalc> GetTopTenMostFrequentAlarmsData()
            {
                return (from ad in SelectedAlarmData
                        group ad by ad.PID into gd
                        select new GetBuildingRawDataWithCalc
                        {
                            PID = gd.Key,
                            PointName = gd.FirstOrDefault().PointName,
                            EID = gd.FirstOrDefault().EID,
                            EquipmentName = gd.FirstOrDefault().EquipmentName,
                            BID = gd.FirstOrDefault().BID,
                            BuildingName = gd.FirstOrDefault().BuildingName,
                            NumOfOccurrences = gd.Sum(x => x.NumOfOccurrences),
                        }).Where(bad => bad.NumOfOccurrences > 0).OrderByDescending(ad => ad.NumOfOccurrences).Take(10);
            }

        #endregion
    }
}