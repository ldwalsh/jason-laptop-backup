﻿using CW.Common.Constants;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.alarm
{
    public partial class LoadUserControl : SitePage
    {
        #region Properties

            //gets and sets the UserControlPath viewstate for loading user control path
            protected string UserControlPath
            {
                get { return ViewState["UserControlPath"] as string ?? string.Empty; }
                set { ViewState["UserControlPath"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                IncludeAlarmStyleSheet();

                if (!String.IsNullOrWhiteSpace(Request.QueryString["uc"]) || Request.QueryString["uc"] != null)
                    UserControlPath = Request.QueryString["uc"];

                try
                {
                    Control ctrl = Page.LoadControl(UserControlPath);

                    if (ctrl != null)
                        plcUserControl.Controls.Add(ctrl);
                }
                catch (Exception ex)
                {
                    divLoadControlError.Attributes.Add("class", "dockMessageLabel");
                    divLoadControlError.InnerText = BusinessConstants.Message.GenericError;

                    ExceptionHelper.WriteExceptionMessage(ex);
                }
            }

        #endregion
    }
}