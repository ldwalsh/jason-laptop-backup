﻿using CW.Data;
using CW.Data.Models.Raw;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CW.Common.Constants;

namespace CW.Website._controls.alarm
{
    public partial class BuildingMap : AlarmSiteUserControl
    {
        #region Properties

            protected string GeocodeCredentialsKey { get; set; }
            protected string UserDataforBuildingMap { get; set; }
            
        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                if (!Page.IsPostBack)
                {
                    LoadBuildingMapAlarmData();
                    GeocodeCredentialsKey = Convert.ToBase64String(Encoding.UTF8.GetBytes(DataConstants.GeocodeCredentialsKey));
                    BindBuildingAlarmSummaryForBingMap();
                }
            }

        #endregion

        #region Load Methods

            private void LoadBuildingMapAlarmData()
            {
                LoadSelectedAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.BuildingMap);
            }

        #endregion

        #region Bind Methods

            private void BindBuildingAlarmSummaryForBingMap()
            {
                lblNoBuildingMaphData.Text = NoAlarmDataMessage;

                UserDataforBuildingMap = "{" + String.Join(",", GetBuildingMapData().ToList().Select(b => string.Format("\"{0}\":[\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",{6},{7},{8},{9},{10},{11}]",
                                                                                                     b.BID.ToString(),
                                                                                                     StringHelper.ReplaceSpecialCharactersForQueryString(b.BuildingName),
                                                                                                     (b.Address != null) ? StringHelper.ReplaceSpecialCharactersForQueryString(b.Address) : string.Empty,
                                                                                                     (b.City != null) ? StringHelper.ReplaceSpecialCharactersForQueryString(b.City) : string.Empty,
                                                                                                     (b.StateID != null) ? StringHelper.ReplaceSpecialCharactersForQueryString(b.StateName) : null,
                                                                                                     b.CountryAlpha2Code,
                                                                                                     b.TotalEverActiveAlarmCount,
                                                                                                     CultureHelper.FormatNumberToInvariantString((decimal)Math.Round(b.TotalEverActiveAlarmPct, 2)),
                                                                                                     (b.Latitude != null) ? CultureHelper.FormatNumberToInvariantString((decimal)b.Latitude) : "null",
                                                                                                     (b.Longitude != null) ? CultureHelper.FormatNumberToInvariantString((decimal)b.Longitude) : "null",
                                                                                                     b.CriticalAlarmThreshold,
                                                                                                     b.NonCriticalAlarmThreshold))

                                                          ) + "}";
            }

        #endregion

        #region Get Data Methods

            public IEnumerable<GetBuildingRawDataWithCalc> GetBuildingMapData()
            {
                return SelectedAlarmData;
            }

        #endregion
    }
}