﻿using CW.Business;
using CW.Common.Constants;
using CW.Data.Models.Raw;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.alarm
{
    public partial class TopTenTotalTimeInAlarm : AlarmSiteUserControl
    {
        #region fields

            private string initialSortDirection = "DESC";
            private string initialSortExpression = "TotalTimeInAlarmOverPeriod";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback, initially load and bind the Top Ten Longest Alarms and ddl buildings data
                if (!Page.IsPostBack)
                {
                    SetGridSorting(initialSortDirection, initialSortExpression);

                    LoadTopTenTotalTimeInAlarmData();

                    BindBuildingDropdownList(ddlBuildings);
                    BindTopTenLongestAlarmsData();
                }
            }

        #endregion

        #region Load Methods

            private void LoadTopTenTotalTimeInAlarmData(string bid = "-1")
            {
                ddlBuildings.SelectedValue = bid;

                LoadSelectedAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.TopTenTotalTimeInAlarm, bid);
                LoadCurrentAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.TopTenTotalTimeInAlarm, bid);
            }

        #endregion

        #region Bind Methods

            private void BindTopTenLongestAlarmsData()
            {
                gridTopTenTotalTimeInAlarm.EmptyDataText = NoAlarmDataOrActiveAlarmsMessage;

                gridTopTenTotalTimeInAlarm.DataSource = GetTopTenTotalTimeInAlarmData().ToList();
                gridTopTenTotalTimeInAlarm.DataBind();
            }

        #endregion

        #region Grid Events

            protected void gridTopTenTotalTimeInAlarm_Sorting(object sender, GridViewSortEventArgs e)
            {
                LoadTopTenTotalTimeInAlarmData(ddlBuildings.SelectedValue);

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(GetTopTenTotalTimeInAlarmData().ToList());

                GridViewSortExpression = e.SortExpression;

                gridTopTenTotalTimeInAlarm.DataSource = SortDataTable(dataTable, false);
                gridTopTenTotalTimeInAlarm.DataBind();
            }

        #endregion

        #region DropDownList Event Methods

            protected void ddlBuildings_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                LoadTopTenTotalTimeInAlarmData(ddlBuildings.SelectedValue);

                gridTopTenTotalTimeInAlarm.DataSource = GetTopTenTotalTimeInAlarmData().ToList();
                gridTopTenTotalTimeInAlarm.DataBind();
            }

        #endregion

        #region Get Data Methods

            private IEnumerable<GetBuildingRawDataWithCalc> GetTopTenTotalTimeInAlarmData()
            {
                var currentAlarmData = CurrentAlarmData.GroupBy(x => new { x.PID, x.BID }).ToDictionary(x => x.Key.PID + "-" + x.Key.BID, x => (x.FirstOrDefault().TimeInAlarm));
                var selectedAlarmData = SelectedAlarmData.GroupBy(x => new { x.PID, x.BID }).ToDictionary(x => x.Key.PID + "-" + x.Key.BID, x => x.Sum(y => y.TimeInAlarm));

                return (from sad in SelectedAlarmData
                        group sad by sad.PID into gd
                        select new GetBuildingRawDataWithCalc
                        {
                            PID = gd.Key,
                            PointName = gd.FirstOrDefault().PointName,
                            EID = gd.FirstOrDefault().EID,
                            EquipmentName = gd.FirstOrDefault().EquipmentName,
                            BID = gd.FirstOrDefault().BID,
                            BuildingName = gd.FirstOrDefault().BuildingName,
                            TimeInAlarm = (currentAlarmData.ContainsKey(gd.Key + "-" + gd.FirstOrDefault().BID)) ? currentAlarmData[gd.Key + "-" + gd.FirstOrDefault().BID] : 0,
                            SelectedRangeTimeInAlarm = (selectedAlarmData.ContainsKey(gd.Key + "-" + gd.FirstOrDefault().BID)) ? selectedAlarmData[gd.Key + "-" + gd.FirstOrDefault().BID] : 0
                        }).OrderByDescending(sad => sad.SelectedRangeTimeInAlarm).Where(sad => sad.SelectedRangeTimeInAlarm > 0).Take(10);
            }

        #endregion
    }
}