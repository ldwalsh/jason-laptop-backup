﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AlarmIncidenceGraph.ascx.cs" Inherits="CW.Website._controls.alarm.AlarmIncidenceGraph" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>

<script type="text/javascript">
    function formatIncidenceGraphTooltip(id)
    {
        var d = kendo.parseDate(id);  
        d.setHours(d.getHours() + 1);

        document.getElementById(id).innerHTML = kendo.toString(d, "g");
    }
</script>

<div id="divBuilding" class="divDockFormWrapperShortest">
  <div class="divDockFormRight">
    <label class="label">Building:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlBuildings" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" data-buildingMap="attached" lookup-id="ddlBuildings" />
  </div>
</div> 

<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" ResizeWithParentPane="true">
<telerik:RadPane ID="RadPane1" Width="100%" runat="server" Scrolling="None">
<telerik:RadHtmlChart ID="RadChartAlarmIncidenceGraphHtml" runat="server" Height="366px">
  <PlotArea>

    <Series />
    
    <XAxis Type="Date">
      <TitleAppearance Text="Time Occurred">
        <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" />
      </TitleAppearance> 
         
      <LabelsAppearance RotationAngle="-45">
        <TextStyle FontSize="7.5pt" Bold="true" FontFamily="Arial" Color="#666666" />
      </LabelsAppearance>
    </XAxis>

    <YAxis>           
      <TitleAppearance Text="# Active Alarms">
        <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" /> 
      </TitleAppearance>  

      <LabelsAppearance RotationAngle="0">
        <TextStyle FontSize="7.5pt" Bold="true" FontFamily="Arial" Color="#666666" />
      </LabelsAppearance>
    </YAxis>

  </PlotArea>

  <Legend>
    <Appearance BackgroundColor="White" Position="Bottom">
      <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" />
    </Appearance>
  </Legend>

  <ChartTitle>
    <Appearance Visible="false" />
  </ChartTitle>
</telerik:RadHtmlChart>
</telerik:RadPane>
</telerik:RadSplitter>

<asp:Label ID="lblNoAlarmIncidenceGraphData" CssClass="dockMessageLabel" Visible="false" runat="server" />