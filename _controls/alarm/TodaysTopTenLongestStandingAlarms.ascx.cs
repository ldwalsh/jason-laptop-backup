﻿using CW.Common.Constants;
using CW.Data.Models.Raw;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.alarm
{
    public partial class TodaysTopTenLongestStandingAlarms : AlarmSiteUserControl
    {
        #region fields

            private string initialSortDirection = "DESC";
            private string initialSortExpression = "LongestTimeInAlarm";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback, initially load and bind the Top Ten Longest Alarms and ddl buildings data
                if (!Page.IsPostBack)
                {
                    SetGridSorting(initialSortDirection, initialSortExpression);

                    LoadTopTenLongestAlarmsData();

                    BindBuildingDropdownList(ddlBuildings);
                    BindTopTenLongestAlarmsData();
                }
            }

        #endregion

        #region Load Methods

            private void LoadTopTenLongestAlarmsData(string bid = "-1")
            {
                ddlBuildings.SelectedValue = bid;

                LoadCurrentAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.TodaysTopTenLongestStandingAlarms, bid);
            }

        #endregion

        #region Bind Methods

            private void BindTopTenLongestAlarmsData()
            {
                gridTodaysTopTenLongestStandingAlarms.EmptyDataText = NoAlarmDataOrActiveAlarmsMessage;

                gridTodaysTopTenLongestStandingAlarms.DataSource = GetTodaysTopTenLongestStandingAlarmsData().ToList();
                gridTodaysTopTenLongestStandingAlarms.DataBind();
            }

        #endregion

        #region Grid Events

            protected void gridTodaysTopTenLongestStandingAlarms_Sorting(object sender, GridViewSortEventArgs e)
            {
                LoadTopTenLongestAlarmsData(ddlBuildings.SelectedValue);

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(GetTodaysTopTenLongestStandingAlarmsData().ToList());

                GridViewSortExpression = e.SortExpression;

                gridTodaysTopTenLongestStandingAlarms.DataSource = SortDataTable(dataTable, false);
                gridTodaysTopTenLongestStandingAlarms.DataBind();
            }

        #endregion

        #region DropDownList Event Methods

            protected void ddlBuildings_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                LoadTopTenLongestAlarmsData(ddlBuildings.SelectedValue);

                gridTodaysTopTenLongestStandingAlarms.DataSource = GetTodaysTopTenLongestStandingAlarmsData().ToList();
                gridTodaysTopTenLongestStandingAlarms.DataBind();
            }

        #endregion

        #region Get Data Methods

            private IEnumerable<GetBuildingRawDataWithCalc> GetTodaysTopTenLongestStandingAlarmsData()
            {
                return CurrentAlarmData.OrderByDescending(cad => cad.LongestTimeInAlarm).Where(cad => cad.LongestTimeInAlarm > 0).Take(10);
            }

        #endregion
    }
}