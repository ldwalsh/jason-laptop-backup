﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EquipmentClassAlarmSummary.ascx.cs" Inherits="CW.Website._controls.alarm.EquipmentClassAlarmSummary" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.Raw" %>

<div id="divBuilding" class="divDockFormWrapperShortest">
  <div class="divDockFormRight">
    <label class="label">Building:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlBuildings" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" data-buildingMap="attached" lookup-id="ddlBuildings" />
  </div>
</div> 

<div id="gridTbl">

  <asp:GridView ID="gridEquipmentClassAlarmSummary"           
                runat="server"  
                GridLines="None"                                      
                PageSize="10"
                AllowPaging="true" 
                AllowSorting="true" 
                OnRowCreated="gridEquipmentClassAlarmSummary_RowCreated"
                OnPageIndexChanging="gridEquipmentClassAlarmSummary_PageIndexChanging"
                OnSorting="gridEquipmentClassAlarmSummary_Sorting" 
                OnDataBound="ggridEquipmentClassAlarmSummary_DataBound"
                AutoGenerateColumns="false">
    <Columns>
      <asp:TemplateField SortExpression="EquipmentClassName" HeaderText="Equipment Class">
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.EquipmentClassName)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="ActiveAlarmCount" HeaderText="#"> 
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.ActiveAlarmCount)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="ActiveAlarmPct" HeaderText="%">
        <ItemTemplate><label><%# Math.Round(Convert.ToDouble(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.ActiveAlarmPct))), 2) + "%" %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="NumOfOccurrences" HeaderText="# Of Occurrences">
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.NumOfOccurrences)) %></label></ItemTemplate>
      </asp:TemplateField>
      <%--
      <asp:TemplateField SortExpression="AvgTimeInAlarm" HeaderText="Average Time In Alarm Per Day (Min.)">
        <ItemTemplate><label><%# Math.Round(Convert.ToDouble(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.AvgTimeInAlarm))), 1) %></label></ItemTemplate>
      </asp:TemplateField>
      --%>
      <asp:TemplateField SortExpression="TotalAvoidableCost" HeaderText="Related Cost">
        <ItemTemplate><label><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.LCID))), Convert.ToString(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.TotalAvoidableCost)))) %></label></ItemTemplate>
      </asp:TemplateField>
    </Columns>

    <HeaderStyle CssClass="tblTitle" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <EmptyDataRowStyle CssClass="dockEmptyGridLabel" />
  </asp:GridView>

</div>