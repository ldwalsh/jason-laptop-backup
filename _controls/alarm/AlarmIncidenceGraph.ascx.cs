﻿using CW.Common.Constants;
using CW.Data.Models.Raw;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.HtmlChart;

namespace CW.Website._controls.alarm
{
    public partial class AlarmIncidenceGraph : AlarmSiteUserControl
    {
        #region Fields

            private IEnumerable<GetBuildingRawDataWithCalc> alarmIncidenceGraphDataToBind;
            private DateTimeBaseUnit dateTimeBaseUnit;

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback, initially load and bind the Alarm Incidence Graph Data and ddl buildings data
                if (!Page.IsPostBack)
                {
                    LoadAlarmIncidenceGraphData();

                    BindBuildingDropdownList(ddlBuildings);
                    BindAlarmIncidenceGraphData();

                    SetGraphAppearanceAndData();
                }
            }

        #endregion

        #region Load Methods

            private void LoadAlarmIncidenceGraphData(string bid = "-1")
            {
                ddlBuildings.SelectedValue = bid;

                LoadSelectedAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.AlarmIncidenceGraph, bid);
            }

        #endregion

        #region Bind Methods

            private void BindAlarmIncidenceGraphData()
            {
                lblNoAlarmIncidenceGraphData.Text = NoAlarmDataOrActiveAlarmsMessage;
                alarmIncidenceGraphDataToBind = GetAlarmIncidenceGraphData().ToList();
            }

        #endregion

        #region Get Data Methods

            private IEnumerable<GetBuildingRawDataWithCalc> GetAlarmIncidenceGraphData()
            {
                var selectedAlarmData = Enumerable.Empty<GetBuildingRawDataWithCalc>();

                //It is necessary to set the first and last items to the average and max alarm counts, and ignore the rest.  This way the lines will plot correctly on the chart for the date/time range.
                if (SelectedAlarmData.Any())
                {
                    if (Days <= 2)
                        dateTimeBaseUnit = DateTimeBaseUnit.Hours;
                    else if (Days > 2 && Days <= 31)
                        dateTimeBaseUnit = DateTimeBaseUnit.Days;
                    else if (Days > 31 && Days <= 93)
                        dateTimeBaseUnit = DateTimeBaseUnit.Weeks;
                    else
                        dateTimeBaseUnit = DateTimeBaseUnit.Months;

                    var orderedAlarmData = SelectedAlarmData.OrderBy(d => d.DateLoggedLocal);
                    var groupedAlarmData = Enumerable.Empty<IGrouping<Object, GetBuildingRawDataWithCalc>>();

                    switch (dateTimeBaseUnit)
                    {
                        case DateTimeBaseUnit.Hours:
                            groupedAlarmData = orderedAlarmData.GroupBy(d => new { d.DateLoggedLocal.Year, d.DateLoggedLocal.Month, d.DateLoggedLocal.Day, d.DateLoggedLocal.Hour });
                            break;
                        case DateTimeBaseUnit.Days:
                            groupedAlarmData = orderedAlarmData.GroupBy(d => new { d.DateLoggedLocal.Year, d.DateLoggedLocal.Month, d.DateLoggedLocal.Day });
                            break;
                        case DateTimeBaseUnit.Weeks:
                            groupedAlarmData = orderedAlarmData.GroupBy(d => new { d.DateLoggedLocal.Year, d.DateLoggedLocal.Month, 
                                                                                   Week = DateTimeFormatInfo.CurrentInfo.Calendar.GetWeekOfYear(d.DateLoggedLocal, CalendarWeekRule.FirstDay, DayOfWeek.Sunday) });
                            break;
                        case DateTimeBaseUnit.Months:
                            groupedAlarmData = orderedAlarmData.GroupBy(d => new { d.DateLoggedLocal.Year, d.DateLoggedLocal.Month });
                            break;
                    }

                    //TEMP: remove null coalesce, was just for new field in cache set
                    selectedAlarmData = groupedAlarmData.Select(d => new GetBuildingRawDataWithCalc { DateLoggedLocal = d.FirstOrDefault().DateLoggedLocal, TotalEverActiveAlarmCount = d.SelectMany(p => p.TotalEverActiveAlarmPIDsList ?? Enumerable.Empty<int>()).Distinct().Count() }).ToList();

                    var avgAlarmCount = CalculateAverageAlarmValue(selectedAlarmData);
                    var maxAlarmCount = CalculateMaxAlarmValue(selectedAlarmData);

                    selectedAlarmData.First().AvgAlarmCount = avgAlarmCount;
                    selectedAlarmData.Last().AvgAlarmCount = avgAlarmCount;

                    selectedAlarmData.First().MaxAlarmCount = maxAlarmCount;
                    selectedAlarmData.Last().MaxAlarmCount = maxAlarmCount;
                }

                return selectedAlarmData;
            }

        #endregion

        #region DropDownList Event Methods

            protected void ddlBuildings_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                LoadAlarmIncidenceGraphData(ddlBuildings.SelectedValue);

                BindAlarmIncidenceGraphData();
                SetGraphAppearanceAndData();
            }

        #endregion

        #region Helper Methods

            private double CalculateAverageAlarmValue(IEnumerable<GetBuildingRawDataWithCalc> alarmData)
            {
                return Math.Round(alarmData.Average(x => x.TotalEverActiveAlarmCount), 1);
            }

            private int CalculateMaxAlarmValue(IEnumerable<GetBuildingRawDataWithCalc> alarmData)
            {
                return alarmData.Max(x => x.TotalEverActiveAlarmCount);
            }

            private void SetGraphAppearanceAndData()
            {
                if (alarmIncidenceGraphDataToBind.Count() > 0)
                {
                    RadChartAlarmIncidenceGraphHtml.PlotArea.Series.Clear();
                    RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.Items.Clear();

                    RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.LabelsAppearance.Step = 1;

                    CreateScatterLineSeries("Average Alarm Rate", "AvgAlarmCount", "DateLoggedLocal", ColorHelper.SEColors.ComfortBlue, "{1:N1} alarms", true);
                    CreateScatterLineSeries("High Water Mark", "MaxAlarmCount", "DateLoggedLocal", ColorHelper.SEColors.SpruceGreen, "{1:N0} alarms", true);
                    CreateScatterLineSeries("Daily Alarm Rate", "TotalEverActiveAlarmCount", "DateLoggedLocal", ColorHelper.SEColors.FuchsiaRed, String.Empty, false, true);

                    RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.BaseUnit = dateTimeBaseUnit;
                    
                    switch (dateTimeBaseUnit)
                    {
                        case DateTimeBaseUnit.Hours:
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.LabelsAppearance.DataFormatString = "t";
                            break;
                        case DateTimeBaseUnit.Days:
                        case DateTimeBaseUnit.Weeks:
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.LabelsAppearance.DataFormatString = "d";
                            break;
                        case DateTimeBaseUnit.Months:
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.LabelsAppearance.DataFormatString = "MMMM";
                            break;
                    }
                    
                    RadChartAlarmIncidenceGraphHtml.DataSource = alarmIncidenceGraphDataToBind;
                    RadChartAlarmIncidenceGraphHtml.DataBind();
                }

                RadChartAlarmIncidenceGraphHtml.Visible = (alarmIncidenceGraphDataToBind.Count() > 0) ? true : false;
                lblNoAlarmIncidenceGraphData.Visible = (alarmIncidenceGraphDataToBind.Count() > 0) ? false : true;
            }

            private void CreateScatterLineSeries(String name, String dataFieldY, String dataFieldX, String backgroundColor, String dataFormatString, Boolean setSize, Boolean setStep = false)
            {
                var scatterLineSeries = new ScatterLineSeries() { Name = name };

                scatterLineSeries.DataFieldY = dataFieldY;
                scatterLineSeries.DataFieldX = dataFieldX;
                scatterLineSeries.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(backgroundColor);
                scatterLineSeries.LabelsAppearance.Visible = false;
                scatterLineSeries.LineAppearance.Width = Unit.Pixel(2);
                scatterLineSeries.TooltipsAppearance.Color = Color.White;

                if (!String.IsNullOrWhiteSpace(dataFormatString)) scatterLineSeries.TooltipsAppearance.DataFormatString = dataFormatString;
                if (setSize) scatterLineSeries.MarkersAppearance.Size = 0;

                if (setStep)
                {
                    var startDateTime = alarmIncidenceGraphDataToBind.Select(gd => gd.DateLoggedLocal).First();
                    var endDateTime = alarmIncidenceGraphDataToBind.Select(gd => gd.DateLoggedLocal).Last();
                    var startDate = alarmIncidenceGraphDataToBind.Select(gd => gd.DateLoggedLocal.Date).First();
                    var endDate = alarmIncidenceGraphDataToBind.Select(gd => gd.DateLoggedLocal.Date).Last();

                    switch (dateTimeBaseUnit)
                    {
                        case DateTimeBaseUnit.Hours:
                            scatterLineSeries.TooltipsAppearance.DataFormatString = "{1:N0} alarms from {0:g} to <span id=\"{0:g}\"></span><script type=\"text/javascript\"> formatIncidenceGraphTooltip(\"{0:g}\"); </script>";                      
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.Step = 2;
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.MinDateValue = startDateTime;
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.MaxDateValue = endDateTime;
                            break;
                        case DateTimeBaseUnit.Days:
                        case DateTimeBaseUnit.Weeks:
                            scatterLineSeries.TooltipsAppearance.DataFormatString = "{1:N0} alarms on {0:d} ";
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.Step = 1;
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.MinDateValue = startDate;
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.MaxDateValue = endDate;
                            break;
                        case DateTimeBaseUnit.Months:
                            scatterLineSeries.TooltipsAppearance.DataFormatString = "{1:N0} alarms for {0:MMMM} ";
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.Step = 1;
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.MinDateValue = startDate;
                            RadChartAlarmIncidenceGraphHtml.PlotArea.XAxis.MaxDateValue = endDate;
                            break;
                    }
                }

                RadChartAlarmIncidenceGraphHtml.PlotArea.Series.Add(scatterLineSeries);
            }

        #endregion
    }
}