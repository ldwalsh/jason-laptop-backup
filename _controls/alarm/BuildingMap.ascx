﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuildingMap.ascx.cs" Inherits="CW.Website._controls.alarm.BuildingMap" %>

<script type="text/javascript" src="../../_assets/scripts/alarmBuildingMap.js"></script>

<div id="buildingMap" style="display: none;">

  <input type="hidden" id="geocodeCredentialsKey" value='<%= GeocodeCredentialsKey %>' />
  <input type="hidden" id="userDataforBuildingMap" value='<%= UserDataforBuildingMap %>' />
  <input type="hidden" id="userCulture" value='<%= siteUser.CultureName %>' />

  <div id="mapDiv" class="buildingMap"></div>

</div>

<div id="noDataMessage" style="display: none;">
  <asp:Label ID="lblNoBuildingMaphData" CssClass="dockMessageLabel" runat="server" />
</div>