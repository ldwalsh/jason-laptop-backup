﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Raw;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CW.Utility.Web;

namespace CW.Website._controls.alarm
{
    public partial class SearchAlarms : AlarmSiteUserControl
    {
        #region fields

            private string initialSortDirection = "DESC";
            private string initialSortExpression = "NumOfOccurrences";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                //if the page is not a postback, initially bind the ddls and Search Alarm Data
                if (!Page.IsPostBack)
                {
                    SetGridSorting(initialSortDirection, initialSortExpression);
                    LoadSearchAlarmData();
                    BindDropdownLists();
                    BindSearchAlarmsData();
                }
            }

            #region grid events

                protected void gridSearchAlarms_OnRowDataBound(Object sender, GridViewRowEventArgs e)
                {
                    var rows = gridSearchAlarms.Rows;

                    foreach (GridViewRow row in rows)
                    {
                        var spanCurrentState = (HtmlGenericControl)row.FindControl("currentState");
                        var currentStateVal = ((HiddenField)row.FindControl("hdnCurrentStateVal")).Value;
                        var isValNullOrEmpty = String.IsNullOrWhiteSpace(currentStateVal);

                        spanCurrentState.InnerText = (isValNullOrEmpty) ? "N/A" : (currentStateVal == "1") ? "On" : "Off";
                        spanCurrentState.Attributes.Add("class", (isValNullOrEmpty) ? "statusBlue" : (currentStateVal == "1") ? "statusRed" : "statusGreen");
                    }
                }

                protected void gridSearchAlarms_OnRowCreated(Object sender, GridViewRowEventArgs e)
                {
                    if (e.Row.RowType == DataControlRowType.Pager)
                    {
                        var tblPager = (Table)e.Row.Cells[0].Controls[0];
                        var theRow = tblPager.Rows[0];
                        var ctrlPrevious = new LinkButton() { CommandArgument = "Prev", CommandName = "Page", Text = "« Previous Page", CssClass = "pageLink" };
                        var ctrlNext = new LinkButton() { CommandArgument = "Next", CommandName = "Page", Text = "Next Page »", CssClass = "pageLink" };

                        tblPager.CssClass = "pageTable";

                        var cellPreviousPage = new TableCell();

                        cellPreviousPage.Controls.Add(ctrlPrevious);
                        tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);

                        var cellNextPage = new TableCell();

                        cellNextPage.Controls.Add(ctrlNext);
                        tblPager.Rows[0].Cells.Add(cellNextPage);
                    }
                }

                protected void gridSearchAlarms_Sorting(Object sender, GridViewSortEventArgs e)
                {
                    GridViewSortExpression = e.SortExpression;

                    gridSearchAlarms.DataSource = SortDataTable(EnumerableHelper.ConvertIEnumerableToDataTable(FilterData().ToList()), false);
                    gridSearchAlarms.DataBind();
                }

                protected void gridSearchAlarms_PageIndexChanging(Object sender, GridViewPageEventArgs e)
                {
                    gridSearchAlarms.DataSource = SortDataTable(EnumerableHelper.ConvertIEnumerableToDataTable(FilterData().ToList()), true);
                    gridSearchAlarms.PageIndex = e.NewPageIndex;
                    gridSearchAlarms.DataBind();
                }

            #endregion

            #region button events

                protected void btnDownloadExcelFile_Click(Object sender, EventArgs e)
                {
                    Boolean result = false;

                    //Wish this didn't have to be here, but we need to force paging to be disallowed, and rebind the grid to get all records for the download.  Web Service based calls can fix this issue.
                    gridSearchAlarms.AllowPaging = false;

                    BindFilteredData();

                    try
                    {
                        result = new FilePublishService
                        (
                            DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                            DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                            siteUser.IsKGSFullAdminOrHigher,
                            siteUser.IsSchneiderTheme,
                            siteUser.Email,
                            new AlarmsXLSFileGenerator(gridSearchAlarms, false, new int[] { }, true, siteUser.IsSchneiderTheme, "SearchAlarms"),
                            LogMgr
                        ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productSearchAlarms.xls", true));

                        gridSearchAlarms.DataSource = SortDataTable(EnumerableHelper.ConvertIEnumerableToDataTable(FilterData().ToList()), true);
                        gridSearchAlarms.DataBind();
                    }
                    catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
                    {
                        DisplayErrorOutput("Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");
                        return;
                    }

                    gridSearchAlarms.AllowPaging = true;
                    gridSearchAlarms.DataBind();

                    if (!result)
                    {
                        DisplayErrorOutput("An error occured attempting to download. Please try again later.");
                        return;
                    }
                }

            #endregion

            #region DropDownList Events

                protected void ddlBuildings_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    var bid = GetBID();

                    BindEquipmentClasses(bid);
                    BindEquipment(bid, Convert.ToInt32(ddlEquipmentClass.SelectedValue));
                    BindAlarmType(bid);
                    BindFilteredData();
                }

                protected void ddlEquipmentClass_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipment(GetBID(), Convert.ToInt32(ddlEquipmentClass.SelectedValue));
                    BindFilteredData();
                }

                protected void ddlEquipment_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindFilteredData();
                }

                protected void ddlAlarmType_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindFilteredData();
                }

            #endregion

        #endregion

        #region methods

            private Int32 GetBID()
            {
                return (ddlBuildings.Items.Count > 0) ? Convert.ToInt32(ddlBuildings.SelectedValue) : siteUser.VisibleBuildings.First().BID;
            }

            private void LoadSearchAlarmData(String bid = "-1")
            {
                LoadSelectedAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.SearchAlarms, bid);
                LoadCurrentAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.SearchAlarms, bid);
            }

            private IEnumerable<GetBuildingRawDataWithCalc> GetSearchAlarmData()
            {
                var currentAlarmData = CurrentAlarmData.GroupBy(x => x.PID).ToDictionary(x => x.Key, x => Tuple.Create<string, double>((x.FirstOrDefault().RawValue), (x.FirstOrDefault().TimeInAlarm)));

                return (from ad in SelectedAlarmData
                        group ad by ad.PID into gd
                        select new GetBuildingRawDataWithCalc
                        {
                            //selected
                            AvgTimeInAlarm = gd.FirstOrDefault().AvgTimeInAlarm,
                            BID = gd.FirstOrDefault().BID,
                            EID = gd.FirstOrDefault().EID,
                            EquipmentClassID = gd.FirstOrDefault().EquipmentClassID,
                            EquipmentName = gd.FirstOrDefault().EquipmentName,
                            FaultCount = gd.FirstOrDefault().FaultCount,
                            LCID = gd.FirstOrDefault().LCID,
                            SelectedRangeTimeInAlarm = gd.Sum(x => x.TimeInAlarm),
                            NumOfOccurrences = gd.Sum(x => x.NumOfOccurrences),
                            PID = gd.Key,
                            PointName = gd.FirstOrDefault().PointName,
                            PointTypeID = gd.FirstOrDefault().PointTypeID,
                            PointTypeName = gd.FirstOrDefault().PointTypeName,
                            TotalAvoidableCost = gd.FirstOrDefault().TotalAvoidableCost,

                            //current
                            RawValue = (currentAlarmData.ContainsKey(gd.Key)) ? currentAlarmData[gd.Key].Item1 : "0",
                            TimeInAlarm = (currentAlarmData.ContainsKey(gd.Key)) ? currentAlarmData[gd.Key].Item2 : 0
                        }).OrderByDescending(ad => ad.NumOfOccurrences);
            }

            private IEnumerable<GetBuildingRawDataWithCalc> FilterData()
            {
                var bid = GetBID();
                var ecid = ddlEquipmentClass.SelectedValue;

                try
                {
                    LoadSearchAlarmData(bid.ToString());

                    var searchAlaramData = GetSearchAlarmData();

                    if (ddlEquipmentClass.SelectedValue != "-1") searchAlaramData = searchAlaramData.Where(sad => sad.EquipmentClassID == Convert.ToInt32(ecid));
                    if (ddlEquipment.SelectedValue != "-1") searchAlaramData = searchAlaramData.Where(sad => sad.EID == Convert.ToInt32(ddlEquipment.SelectedValue));
                    if (ddlAlarmType.SelectedValue != "-1") searchAlaramData = searchAlaramData.Where(sad => sad.PointTypeID == Convert.ToInt32(ddlAlarmType.SelectedValue));
                    if (searchAlaramData.Count() == 0) gridSearchAlarms.EmptyDataText = NoAlarmDataMessage;

                    return searchAlaramData;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error occurred for CID {0}, BID {1} and Equipment Class ID {2} on FilterData() for SearchAlarms.ascx.cs.", siteUser.CID, bid, ecid), ex);

                    return Enumerable.Empty<GetBuildingRawDataWithCalc>();
                }
            }

            #region binders

                private void BindDropdownLists()
                {
                    BindBuildingDropdownList(ddlBuildings, false);
                    BindEquipmentClasses();
                    BindEquipment();
                    BindAlarmType();
                }

                private void BindEquipmentClasses(Int32 bid = -1)
                {
                    var buildingID = GetBID();
                    var equipmentClasses = (buildingID != -1) ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(buildingID) : DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();
                    
                    BindDDL(ddlEquipmentClass, "EquipmentClassName", "EquipmentClassID", equipmentClasses, Page.IsPostBack);
                }

                private void BindEquipment(Int32 bid = -1, Int32 equipmentClassID = -1)
                {
                    var equipment = Enumerable.Empty<Equipment>();

                    if (bid != -1 && equipmentClassID != -1)
                        equipment = DataMgr.EquipmentDataMapper.GetAllActiveEquipmentByBuildingIDAndEquipmentClassID(bid, equipmentClassID);
                    else if (equipmentClassID != -1)
                        equipment = DataMgr.EquipmentDataMapper.GetAllActiveEquipmentByCIDAndEquipmentClassID(siteUser.CID, equipmentClassID);
                    else
                        equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByCID(siteUser.CID);

                    BindDDL(ddlEquipment, "EquipmentName", "EID", equipment, Page.IsPostBack);
                }

                private void BindAlarmType(Int32 bid = -1)
                {
                    BindDDL(ddlAlarmType, "PointTypeName", "PointTypeID", DataMgr.PointTypeDataMapper.GetAllPointTypesByPointClassID(BusinessConstants.PointClass.AlarmPointClassID));

                    if (bid == -1) ddlAlarmType.SelectedIndex = 0;
                }

                private void BindSearchAlarmsData()
                {
                    LoadSearchAlarmData();

                    gridSearchAlarms.EmptyDataText = NoAlarmDataMessage;
                    gridSearchAlarms.DataSource = GetSearchAlarmData().ToList();
                    gridSearchAlarms.DataBind();

                    filters.Style.Add("display", (gridSearchAlarms.Rows.Count > 0) ? "block" : "none");
                }

                private void BindSearchAlarmGrid()
                {

                }

                private void BindFilteredData()
                {
                    gridSearchAlarms.DataSource = FilterData().ToList();
                    gridSearchAlarms.DataBind();
                }

                private void BindDDL(DropDownList ddl, String textField, String valueField, Object data, Boolean resetOnPostBack = false)
                {
                    if (resetOnPostBack) ddl.Items.Clear();

                    ddl.Items.Add(new ListItem { Text = "View All", Value = "-1", });

                    ddl.DataTextField = textField;
                    ddl.DataValueField = valueField;
                    ddl.DataSource = data;
                    ddl.DataBind();
                }

            #endregion

        #endregion
    }
}