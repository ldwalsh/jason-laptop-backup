﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchAlarms.ascx.cs" Inherits="CW.Website._controls.alarm.SearchAlarms" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.Raw" %>

<div id="filters" class="divDockFormWrapperShortest" runat="server">

  <div id="divBuilding" class="divDockFormFirst">
    <label class="label">Building:</label>
    <asp:DropDownList ID="ddlBuildings" CssClass="dropdown" runat="server" AppendDataBoundItems="true" lookup-id="ddlBuildings" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlBuildings_SelectedIndexChanged" />
  </div>
  
  <div class="divDockForm">
    <label class="label">Equipment Class:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlEquipmentClass" runat="server" AppendDataBoundItems="true" set-css-to="divDockFormFirst" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlEquipmentClass_SelectedIndexChanged" />
  </div>

  <div class="divDockForm">
    <label class="label">Equipment:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlEquipment" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlEquipment_SelectedIndexChanged" />
  </div>

  <div class="divDockForm">
    <label class="label">Alarm Type:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlAlarmType" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlAlarmType_SelectedIndexChanged" />
  </div>

  <asp:ImageButton ID="imgExcelCurrentPageDownload" CssClass="imgDockDownload" ImageUrl="../../_assets/images/excel-icon.jpg" OnClick="btnDownloadExcelFile_Click" AlternateText="download" runat="server" CausesValidation="false" />

</div>

<hr />

<div id="gridTbl">

  <asp:GridView ID="gridSearchAlarms" runat="server" GridLines="None" PageSize="25" AllowPaging="true" AllowSorting="true" OnRowCreated="gridSearchAlarms_OnRowCreated" OnPageIndexChanging="gridSearchAlarms_PageIndexChanging" OnSorting="gridSearchAlarms_Sorting" OnRowDataBound="gridSearchAlarms_OnRowDataBound" AutoGenerateColumns="false">
    <Columns>
      <asp:TemplateField SortExpression="PointName" HeaderText="Alarm">
        <ItemTemplate><asp:Label ID="lblPointName" runat="server" Text="<%# Eval(StringHelper.TrimText(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.PointName), 25)) %>" ToolTip="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(_ => _.PointName)) %>" /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="PointTypeName" HeaderText="Alarm Type">
        <ItemTemplate><asp:Label ID="lblPointTypeName" runat="server" Text="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.PointTypeName)) %>" /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="EquipmentName" HeaderText="Equipment">
        <ItemTemplate><asp:Label ID="lblEquipmentName" runat="server" Text="<%# Eval(StringHelper.TrimText(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.EquipmentName), 25)) %>" ToolTip="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(_ => _.EquipmentName)) %>" /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="RawValue" HeaderText="Current State">
        <ItemTemplate><span id="currentState" runat="server"></span><asp:HiddenField ID="hdnCurrentStateVal" runat="server" Value="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.RawValue)).ToString() %>" /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="TimeInAlarm" HeaderText="Today's Total Time in Alarm (Min.)">
        <ItemTemplate><asp:Label ID="lblTimeInAlarm" runat="server" Text="<%# Math.Round(Convert.ToDouble(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.TimeInAlarm))), 0) %>" /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="NumOfOccurrences" HeaderText="# Of Occurrences">
        <ItemTemplate><asp:Label ID="lblNumOfOccurrences" runat="server" Text="<%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.NumOfOccurrences)) %>" /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="SelectedRangeTimeInAlarm" HeaderText="Total time in Alarm Over Period (Min.)">
        <ItemTemplate><asp:Label ID="lblSelectedRangeTimeInAlarm" runat="server" Text="<%# Math.Round(Convert.ToDouble(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.SelectedRangeTimeInAlarm))), 0) %>" /></ItemTemplate>
      </asp:TemplateField>
    </Columns>

    <HeaderStyle CssClass="tblTitle" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <EmptyDataRowStyle CssClass="dockEmptyGridLabel" />
    <PagerSettings PageButtonCount="20" />    
  </asp:GridView>

</div>