﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Raw;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.alarm
{
    public partial class BuildingAlarmSummary : AlarmSiteUserControl
    {
        #region fields

            private string initialSortDirection = "ASC";
            private string initialSortExpression = "BuildingName";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback, initially load and bind the Building Alarm Summary Data
                if (!Page.IsPostBack)
                {
                    SetGridSorting(initialSortDirection, initialSortExpression);

                    LoadBuildingAlarmSummaryData();

                    BindBuildingAlarmSummary();
                }
            }

        #endregion

        #region Load Methods

            private void LoadBuildingAlarmSummaryData()
            {
                LoadSelectedAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.BuildingAlarmSummary);
                LoadCurrentAlarmData(BusinessConstants.Alarm.AlarmWidgetsEnum.BuildingAlarmSummary);
            }

        #endregion

        #region Bind Methods

            private void BindBuildingAlarmSummary()
            {
                gridBuildingAlarmSummary.EmptyDataText = NoAlarmDataMessage;

                gridBuildingAlarmSummary.DataSource = GetBuildingAlarmSummaryData().ToList();
                gridBuildingAlarmSummary.DataBind();
            }

        #endregion

        #region Grid Events

            protected void gridBuildingAlarmSummary_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridBuildingAlarmSummary_Sorting(object sender, GridViewSortEventArgs e)
            {
                LoadBuildingAlarmSummaryData();

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(GetBuildingAlarmSummaryData().ToList());

                GridViewSortExpression = e.SortExpression;

                gridBuildingAlarmSummary.DataSource = SortDataTable(dataTable, false);
                gridBuildingAlarmSummary.DataBind();
            }

            protected void gridBuildingAlarmSummary_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                LoadBuildingAlarmSummaryData();

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(GetBuildingAlarmSummaryData().ToList());

                gridBuildingAlarmSummary.DataSource = SortDataTable(dataTable, true);
                gridBuildingAlarmSummary.PageIndex = e.NewPageIndex;
                gridBuildingAlarmSummary.DataBind();
            }

            protected void gridBuildingAlarmSummary_DataBound(object sender, EventArgs e)
            {
                if (gridBuildingAlarmSummary.HeaderRow != null)
                {
                    GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);

                    TableHeaderCell cell = new TableHeaderCell();
                    row.Controls.Add(cell);

                    cell = new TableHeaderCell();
                    cell.Text = "<u>Current Active Alarms</u>";
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    cell.ColumnSpan = 2;
                    row.Controls.Add(cell);

                    cell = new TableHeaderCell();
                    cell.ColumnSpan = 1;
                    cell.Text = "<u>Active Alarms Over Period</u>";
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    row.Controls.Add(cell);

                    cell = new TableHeaderCell();
                    cell.ColumnSpan = 1;
                    cell.Text = "<u>Diagnostics</u>";
                    cell.HorizontalAlign = HorizontalAlign.Center;
                    row.Controls.Add(cell);

                    gridBuildingAlarmSummary.HeaderRow.Parent.Controls.AddAt(0, row);
                }
            }

        #endregion

        #region Get Data Methods

            public IEnumerable<GetBuildingRawDataWithCalc> GetBuildingAlarmSummaryData()
            {
                var currentAlarmData = CurrentAlarmData.GroupBy(x => x.BID).ToDictionary(x => x.Key, x => Tuple.Create<int, double>((x.FirstOrDefault().ActiveAlarmCount), (x.FirstOrDefault().ActiveAlarmPct)));

                //needed to group the building data across multiple days
                var selectedAlarmData = (from d in SelectedAlarmData
                                         group d by d.BID into gd
                                         select new GetBuildingRawDataWithCalc
                                            {
                                                BID = gd.Key,
                                                LCID = gd.FirstOrDefault().LCID,
                                                BuildingName = gd.FirstOrDefault().BuildingName,
                                                NumOfOccurrences = gd.FirstOrDefault().NumOfOccurrences,

                                                //TODO: this is still wrong. we cannot just weigh each day the same amount. it has to be an average across the whole time period.
                                                //TODO: This is also wrong because we cannot weigh each day the same amount, which is also averaged here.
                                                //Only way to do this is to cache the point count and averages so we can filter out users accessable buildings and group by culture first... then caclulate here.
                                                AvgTimeInAlarm = gd.FirstOrDefault().AvgTimeInAlarm,

                                                //FaultCount = gd..FirstOrDefault().FaultCount,
                                                TotalAvoidableCost = gd.FirstOrDefault().TotalAvoidableCost,
                                            });

                return (from sad in selectedAlarmData
                        select new GetBuildingRawDataWithCalc
                         {
                             //selected
                             BID = sad.BID,
                             LCID = sad.LCID,
                             BuildingName = sad.BuildingName,
                             NumOfOccurrences = sad.NumOfOccurrences,                            
                             AvgTimeInAlarm = sad.AvgTimeInAlarm / Days,

                             //FaultCount = sad.FaultCount,
                             TotalAvoidableCost = sad.TotalAvoidableCost,

                             //current
                             ActiveAlarmCount = (currentAlarmData.ContainsKey(sad.BID)) ? currentAlarmData[sad.BID].Item1 : 0,
                             ActiveAlarmPct = (currentAlarmData.ContainsKey(sad.BID)) ? currentAlarmData[sad.BID].Item2 : 0,
                         }).OrderBy(n => n.BuildingName);
            }

        #endregion
    }
}