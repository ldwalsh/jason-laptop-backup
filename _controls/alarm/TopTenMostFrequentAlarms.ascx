﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopTenMostFrequentAlarms.ascx.cs" Inherits="CW.Website._controls.alarm.TopTenMostFrequentAlarms" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.Raw" %>

<div id="divBuilding" class="divDockFormWrapperShortest">
  <div class="divDockFormRight">
    <label class="label">Building:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlBuildings" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" data-buildingMap="attached" lookup-id="ddlBuildings" />
  </div>
</div> 

<div id="gridTbl">

  <asp:GridView ID="gridTopTenMostFrequentAlarms"
                runat="server"  
                GridLines="None"
                PageSize="10"
                AllowSorting="true" 
                OnSorting="gridTopTenMostFrequentAlarms_Sorting"
                AutoGenerateColumns="false">
    <Columns>
      <asp:TemplateField SortExpression="PointName" HeaderText="Alarm">
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.PointName)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="EquipmentName" HeaderText="Equipment">
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.EquipmentName)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="BuildingName" HeaderText="Building">
        <ItemTemplate><label><%# StringHelper.TrimText(Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.BuildingName)),30) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="NumOfOccurrences" HeaderText="# Of Occurrences">
        <ItemTemplate><label><%# Eval(PropHelper.G<GetBuildingRawDataWithCalc>(b => b.NumOfOccurrences)) %></label></ItemTemplate>
      </asp:TemplateField>
    </Columns>

    <HeaderStyle CssClass="tblTitle" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <EmptyDataRowStyle CssClass="dockEmptyGridLabel" />
  </asp:GridView>

</div>