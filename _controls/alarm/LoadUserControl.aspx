﻿<%@ Page Title="" Language="C#" MasterPageFile="~/_masters/Widget.Master" AutoEventWireup="false" CodeBehind="LoadUserControl.aspx.cs" Inherits="CW.Website._controls.alarm.LoadUserControl" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
  <div id="divLoadControlError" runat="server" />
  <asp:PlaceHolder runat="server" ID="plcUserControl" />
</asp:Content>