﻿using CW.Common.Constants;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Linq;
using System.Collections.Generic;

namespace CW.Website._controls
{
    public partial class YesterdaysDiagnosticResults : ModularSiteUserControl
    {
        #region fields

            private DateTime startDate, endDate;

        #endregion

        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.Yesterday; } }

        #endregion

        #region methods

            #region overrides

                protected override void Initialize()
                {
                    DateTimeHelper.GenerateDefaultDatesYesterday(out startDate, out endDate, EarliestBuildingTimeZoneID);
                }

                protected override void BindControl()
                {
                    RetrieveAndBindDiagnostics();

                    if (IsProviderView)
                    {
                        gridYesterdaysTopPortfolioDiagnosticsResults.Columns[GetColumnIndexByHeader(gridYesterdaysTopPortfolioDiagnosticsResults, "Client")].Visible = true;
                        gridYesterdaysTopPortfolioDiagnosticsResults.Columns[GetColumnIndexByHeader(gridYesterdaysTopPortfolioDiagnosticsResults, "lastColumn")].Visible = false;
                    }
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    var results = GetConcatenatedResults(startDate, endDate, DataConstants.AnalysisRange.Daily);

                    if (!results.Any()) 
                        gridYesterdaysTopPortfolioDiagnosticsResults.EmptyDataText = BusinessConstants.Message.DiagnosticsNoPriorites;

                    return results;
                }

                protected override void BindDiagnostics(IEnumerable<DiagnosticsResult> results)
                {
                    BindTopPortfolioDiagnosticsResults(results);
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion

            private void BindTopPortfolioDiagnosticsResults(IEnumerable<DiagnosticsResult> results)
            {
                gridYesterdaysTopPortfolioDiagnosticsResults.DataSource = (results.Any()) ? results.OrderByDescending(ds => ds.CostSavings).Take(10) : null;
                gridYesterdaysTopPortfolioDiagnosticsResults.DataBind();
            }

        #endregion
    }
}