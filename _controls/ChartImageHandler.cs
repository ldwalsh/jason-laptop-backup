﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using CW.Business;
using CW.Website.DependencyResolution;

namespace CW.Website._controls
{
    public class ChartImageHandler : IChartStorageHandler
    {
        private readonly DataManagerCommonStorage mDataManager;

        public ChartImageHandler()
        {
            mDataManager = IoC.Resolve<DataManagerCommonStorage>();
        }

        #region IChartStorageHandler Members

        public void Delete(string key)
        {
            mDataManager.DefaultCacheRepository.Remove<byte[]>(key);
        }

        public bool Exists(string key)
        {
            return mDataManager.DefaultCacheRepository.Get<byte[]>(key) == null ? false : true;
        }

        public byte[] Load(string key)
        {
            return mDataManager.DefaultCacheRepository.Get<byte[]>(key);
        }

        public void Save(string key, byte[] data)
        {
            mDataManager.DefaultCacheRepository.Set(key, data);
        }

        #endregion
    }
}