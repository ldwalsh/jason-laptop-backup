﻿using System;
using CW.Utility;
using CW.Website._framework;
using System.Linq;
using CW.Common.Constants;

namespace CW.Website._controls
{
    public partial class LeftNavExapander: SiteUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!siteUser.IsAnonymous)
            {
                rptModules.DataSource = siteUser.Modules.OrderBy(m => m.SortOrder);
                rptModules.DataBind();
            }
        }
    }
}
