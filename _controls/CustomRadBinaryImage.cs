﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using CW.Business;
using CW.Data.AzureStorage;
using CW.Data.AzureStorage.DataContexts.Blob;
using CW.Data.AzureStorage.Models.Blob;
using Lokad.Cloud.Storage;
using Telerik.Web.UI;
using CW.Data.Interfaces.State;
using CW.Website.DependencyResolution;
using CW.Caching;

namespace CW.Website._controls
{
    public class CustomRadBinaryImage : RadBinaryImage
    {
        public string DefaultImageVirtualPath { get; set; }
        public string ImageFileName { get; set; }

        IRadImagePersister mPersister;

        private readonly ICacheStore mCacheStore;

        public CustomRadBinaryImage()
        {
            this.HttpHandlerUrl = ConfigurationManager.AppSettings["CustomRadBinaryImageHandler"];

            mCacheStore = IoC.Resolve<DataManager>().DefaultCacheRepository;
        }

        protected override IRadImagePersister ImagePersister
        {
            get
            {
                if (mPersister == null)
                {
                    mPersister = new CacheRadImageHttpCachePersister(mCacheStore);
                }
                return mPersister;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (base.DataValue == null)
            {
                base.DataValue = GetDefaultImage(DefaultImageVirtualPath);
            }
        }

        protected override void SetImageFileNameToPersister(IRadImagePersister persister)
        {
            if (!this.Visible)
            {
                return;
            }

            var p = (CacheRadImageHttpCachePersister)persister;

            p.ImageFileName = ImageFileName;
            p.DefaultImageVirtualPath = DefaultImageVirtualPath;
            p.ImageData = this.DataValue;

        }

        public static byte[] GetDefaultImage(string virtualPath)
        {
            if(String.IsNullOrWhiteSpace(virtualPath))
            {
                return null;
            }

            return System.IO.File.ReadAllBytes(HttpContext.Current.Server.MapPath(virtualPath));
        }
    }

    public class SessionRadImageHttpCachePersister : RadImageHttpCachePersister
    {
        public byte[] ImageData { get; set; }
        public string DefaultImageVirtualPath { get; set; }

        public override BinaryImageDataContainer LoadImage()
        {
            if (CurrentContext != null && !string.IsNullOrEmpty(CurrentContext.Request[UrlKey]))
            {
                BinaryImageDataContainer binaryContainer = new BinaryImageDataContainer();

                // if data in session is a byte[], set byte[], if null, set default image
                binaryContainer.Data = HttpContext.Current.Session[CurrentContext.Request[UrlKey]] as byte[] ?? CustomRadBinaryImage.GetDefaultImage(DefaultImageVirtualPath);

                return binaryContainer;
            }

            return null;
        }

        public override void SaveImage(byte[] image)
        {
            HttpContext.Current.Session.Add(HttpContext.Current.Server.UrlEncode(ImageKey), ImageData);
        }
    }

    public class CustomRadBinaryImageHandler : Telerik.Web.UI.RadBinaryImageHandler, System.Web.SessionState.IRequiresSessionState
    {
        Telerik.Web.UI.IRadImagePersister _persister;
        protected override Telerik.Web.UI.IRadImagePersister ImagePersister
        {
            get
            {
                if (_persister == null)
                    _persister = new CacheRadImageHttpCachePersister(IoC.Resolve<DataManager>().DefaultCacheRepository);

                return _persister;
            }
        }
    }

    public class CacheRadImageHttpCachePersister : RadImageHttpCachePersister
    {
        public byte[] ImageData { get; set; }
        public string DefaultImageVirtualPath { get; set; }

        private readonly ICacheStore mCacheStore;
        public CacheRadImageHttpCachePersister(ICacheStore cs)
        {
            mCacheStore = cs;
        }

        public override BinaryImageDataContainer LoadImage()
        {
            if (CurrentContext != null && !string.IsNullOrEmpty(CurrentContext.Request[UrlKey]))
            {
                BinaryImageDataContainer binaryContainer = new BinaryImageDataContainer();

                // if data in cache is a byte[], set byte[], if null, set default image
                binaryContainer.Data = mCacheStore.Get<byte[]>(CurrentContext.Request[UrlKey]) as byte[];
                
                if(binaryContainer.Data  == null)
                    binaryContainer.Data  = CustomRadBinaryImage.GetDefaultImage(DefaultImageVirtualPath);
                //binaryContainer.Data = mCacheStore.Get<byte[]>(CurrentContext.Request[UrlKey]) as byte[] ?? CustomRadBinaryImage.GetDefaultImage(DefaultImageVirtualPath);

                return binaryContainer;
            }

            return null;
        }

        public override void SaveImage(byte[] image)
        {
            mCacheStore.Set(HttpContext.Current.Server.UrlEncode(ImageKey), image);
        }
    }
}