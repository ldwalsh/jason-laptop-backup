﻿using CW.Common.Constants;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls
{
    public partial class LastMonthsDiagnosticResults : ModularSiteUserControl
    {
        #region fields

            private DateTime startDateMonth, endDateMonth;

        #endregion

        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.LastMonth; } }

        #endregion

        #region methods

            #region overrides

                protected override void Initialize()
                {
                    DateTimeHelper.GenerateDefaultDatesPastMonth(out startDateMonth, out endDateMonth, EarliestBuildingTimeZoneID);
                }

                protected override void BindControl()
                {
                    RetrieveAndBindDiagnostics();

                    if (IsProviderView)
                    {
                        gridLastMonthsTopPortfolioDiagnosticsResults.Columns[GetColumnIndexByHeader(gridLastMonthsTopPortfolioDiagnosticsResults, "Client")].Visible = true;
                        gridLastMonthsTopPortfolioDiagnosticsResults.Columns[GetColumnIndexByHeader(gridLastMonthsTopPortfolioDiagnosticsResults, "lastColumn")].Visible = false;
                    }
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    var results = GetConcatenatedResults(startDateMonth, endDateMonth, DataConstants.AnalysisRange.Monthly);

                    if (!results.Any()) 
                        gridLastMonthsTopPortfolioDiagnosticsResults.EmptyDataText = BusinessConstants.Message.DiagnosticsNoPriorites;

                    return results;
                }

                protected override void BindDiagnostics(IEnumerable<DiagnosticsResult> results)
                {
                    BindTopPortfolioDiagnosticsResults(results);
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion

            private void BindTopPortfolioDiagnosticsResults(IEnumerable<DiagnosticsResult> results)
            {
                gridLastMonthsTopPortfolioDiagnosticsResults.DataSource = (results.Any()) ? results.OrderByDescending(ds => ds.CostSavings).Take(10).ToList() : null;
                gridLastMonthsTopPortfolioDiagnosticsResults.DataBind();
            }

        #endregion
    }
}