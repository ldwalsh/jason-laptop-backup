﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CW.Website._framework;
using CW.Utility;
using CW.Business;
using CW.Utility.Web;

namespace CW.Website._controls
{

    public partial class PrimaryNavDropdowns : SiteUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (siteUser.IsAnonymous)
            {
                divSelectClient.Visible = false;
                divSelectModule.Visible = false;
            }
            else
            {
                if (!Page.IsPostBack)
                {
                    SiteUser user = siteUser;

                    if (user.Clients.Count() > 1)
                    {
                        BindClients(user);
                    }
                    else
                    {
                        divSelectClient.Visible = false;
                    }

                    //if we want to show the modules dropdown again.
                    //if (user.Modules.Any()) BindModules(user);                                    
                    divSelectModule.Visible = false;
                    
                }
            }
        }

        /// <summary>
        /// ddlSelectClient on selected index changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlSelectClient_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            SiteUser user = siteUser;

            siteUser.SwitchClient(Convert.ToInt32(ddlSelectClient.SelectedValue), siteUser.UserOID, siteUser.ProviderID);           
           
            //perform postback on current page url
            Response.Redirect(LinkHelper.GetFullUrl(Request));
        }


        /// <summary>
        /// Binds a html dropdown list of modules
        /// </summary>
        private void BindModules(SiteUser user)
        {
            divSelectModule.Visible = true;

            ListItem lItem = new ListItem();
            lItem.Text = "Goto module...";
            lItem.Value = "-1";
            lItem.Selected = true;

            ddlSelectModule.Items.Clear();
            ddlSelectModule.DataTextField = "ModuleName";
            ddlSelectModule.DataValueField = "ModuleLink";
            ddlSelectModule.DataSource = user.Modules;
            ddlSelectModule.DataBind();

            //insert after bind because html select doesnt have append.
            ddlSelectModule.Items.Insert(0, lItem);
        }


        /// <summary>
        /// Binds a dropdown list of clients
        /// </summary>
        private void BindClients(SiteUser user)
        {
            divSelectClient.Visible = true;

            ddlSelectClient.Items.Clear();
            ddlSelectClient.DataTextField = "ClientName";
            ddlSelectClient.DataValueField = "CID";
            ddlSelectClient.DataSource = user.Clients;
            ddlSelectClient.DataBind();
            ddlSelectClient.SelectedValue = user.CID.ToString();

            BindModules(user);
        }
    }
}
