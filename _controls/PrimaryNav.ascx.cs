﻿using System;
using CW.Utility;
using CW.Website._framework;
using System.Linq;

namespace CW.Website._controls
{
    public partial class PrimaryNav: SiteUserControl
    {
        protected void Page_Load(Object sender, EventArgs e)
        {
            //Check if logged in and set role based nav visibility
            
            if (siteUser.IsAnonymous)
            {
                lnkAdmin.Visible            = false;
                lnkProviderAdmin.Visible    = false;    
                lnkKGSAdmin.Visible         = false;
                lnkSystemAdmin.Visible      = false;
                lnkHelp.Visible             = false;

                lnkHome.Attributes.Add("class", "last");

                return;
            }

            //also show for system admins
            lnkAdmin.Visible            = siteUser.IsFullAdminOrHigher;
            lnkProviderAdmin.Visible    = siteUser.IsKGSFullAdminOrHigher || (siteUser.IsFullAdminOrHigher && siteUser.IsLoggedInUnderProviderClient);            
            lnkKGSAdmin.Visible         = siteUser.IsKGSFullAdminOrHigher;
            lnkSystemAdmin.Visible      = siteUser.IsKGSSuperAdminOrHigher;

            if (siteUser.IsPublicOrKioskUser)
            {
                lnkHelp.Visible = false;
                lnkHome.Attributes.Add("class", "last");
            }
            else
            {
                lnkHelp.Attributes.Add("class", "last");
            }
        }
    }
}
