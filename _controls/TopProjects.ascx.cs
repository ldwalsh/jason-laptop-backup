﻿using CW.Data.Models.Diagnostics;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using CW.Common.Helpers;
using CW.Data;
using System.Data.SqlClient;
using CW.Common.Constants;
using CW.Data.Models.Projects;
using CW.Logging;

namespace CW.Website._controls
{
    public partial class TopProjects : SiteUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void LoadModule()
        {
            if(siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Projects))
                BindTopProjects();
        }

        #region bind methods

            public void BindTopProjects()
            {
                //projects top 10
                IEnumerable<GetGroupedProjectAssociationsData> projects = Enumerable.Empty<GetGroupedProjectAssociationsData>();

                try
                {
                    projects = DataMgr.ProjectDataMapper.GetTopGroupedProjectAssociations(new ProjectsInputs() { CID = siteUser.CID, BIDArray = siteUser.VisibleBuildings.Select(b => b.BID).ToArray() }, 10);
                }
                catch (SqlException sqlex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error getting projects for homepage", sqlex);
                }

                gridTopProjects.DataSource = projects;
                gridTopProjects.DataBind();
            }

        #endregion
    }
}