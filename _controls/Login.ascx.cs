﻿using CW.Business;
using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Data.Models.Client;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using CW.Website._masters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._controls
{
    public partial class Login: SiteUserControl, SiteUserControl.IClientControl
    {
        #region IClientControl

            Control IClientControl.ContainerControl
            {
                get {return mv;}
            }
        
        #endregion

        #region Properties

            protected User User
            {
                get
                {
                    return (Session["TempUser"] as User);
                }
                set
                {
                    if (null != value)
                    {
                        Session["TempUser"] = value;
                    }
                    else
                    {
                        Session.Remove("TempUser");
                    }
                }
            }
            public string mHomeUrl = "/Home.aspx";
            public string sessionID;
            public string mKeyForTimeoutCookie;
            protected IEnumerable<GetClientsWithRoleFormat> mClients;
            protected string clientSelectedValue;

            //private bool isAccountPage;
            //private HttpCookie mCookie;
            protected string mWelcome;

            //const string loginPageText = "Please enter your email and password to login.";
            //const string clientPageText = "Please select the client you wish to login under.";
            //const string forgotPasswordPageText = "Enter your email to have a new password generated and emailed.";

            const string loginAccountDeactivated = "Your account is currently deactivated. Please contact an administrator for further assistance.";
            protected string loginAccountLocked = "Your account is currently locked. Please contact support for further assistance. " + ConfigurationManager.AppSettings["Mail.DefaultSupportAddress"];
            const string loginClientDeactivated = "Your client account is currently deactivated. Please contact an administrator for further assistance.";
            const string loginInvalidPassowrd = "Invalid password.";
            const string loginInvalidUser = "Invalid user.";

            const string noClients = "Sorry, no clients have been associated with your account yet.";

            const string forgotPasswordAccountDeactivated = "Your account has been temporarily deactivated. Please constact an administrator for further assistance.";
            const string forgotPasswordSuccess = "A new password has been sent to the email on your account.";
            const string forgotPasswordFailed = "Forgot password failed. Please contact an administrator.";
            const string forgotPasswordInvalidUser = "Invalid user.";
            const string conflictingAuthn = "System has detected a previously authenticated user that is different than the one you are attempting to login as. Please signout from your Identity Provider and then try logging in again.";

            const string loginInvalidCaptcha = "Invalid code. Please try again.";

            const string headerMsgForInterstitialPage = "Please wait... ";
            const string paragraphMsgForInterstitialPage = " while we knock on your identity provider's door and ask to come in.";

            public const string cookieName = "clockworksCookie";

        #endregion

            private int LoginState
            {
                get
                {
                    return (Convert.ToInt16(hdnLoginState.Value));
                }
                set
                {
                    hdnLoginState.Value = Convert.ToString(value);
                }
            }
        #region Page Events

            protected void Page_Load()
            {
                //if session doesnt exist load login controls to modal panel and show login link.
                //check sessions
                if (siteUser.IsAnonymous)
                {
                    mv.ActiveViewIndex = 0;

                    HideModals();
                    //check if refferer url exists and set hdn field
                    if (!String.IsNullOrWhiteSpace(Request.QueryString[DataConstants.ReferrerQSKey]))
                    {
                        //carry over entire querystring not just referrer
                        hdnReferrer.Value = Request.QueryString[DataConstants.ReferrerQSKey];

                        //add the rest of the querystirng values if any, except the refferrer
                        if(Request.QueryString.AllKeys.Count() > 1){
                            foreach (String key in Request.QueryString.AllKeys)
                            {
                                if (key == DataConstants.ReferrerQSKey) continue;
                                hdnReferrer.Value += "&" + key + "=" + Request.QueryString[key];                                    
                            }
                        }
                    }

                    LogMgr.Log(Common.Constants.DataConstants.LogLevel.DEBUG, "LoginState =  " + LoginState);
                    if (!Page.IsPostBack)
                    {
                        string aliasOID = Request.QueryString["OID"];
                        User u = User;
                        if (null == u && siteUser.IdentityManagedExternalProvider && string.IsNullOrEmpty(aliasOID))
                        {
                            // This condition would only be met if the discovery (email) was bypassed.
                            // We need to get the emailAddress from the claims collection and use its value.
                            string emailAddress = null;
                            int orgId = 0;
                            string reasonForFailure = "Discovery Bypass: ";
                            // If such a claim is found, check if user exists in DB
                            if (!AuthnHelper.GetEmailAddressFromClaims(ref emailAddress, ref orgId))
                            {
                                reasonForFailure += "(Email: " + (emailAddress ?? "None") + ", OrgId: " + orgId + ")";
                            }
                            else
                            {
                                u = DataMgr.UserDataMapper.GetUser(emailAddress);
                                if (null == u)
                                {
                                    reasonForFailure += "Did not find user with Email: " + emailAddress + " in organization id: " + orgId;
                                }
                                else if (u.OID != orgId)
                                {
                                    reasonForFailure += "User with Email: " + emailAddress + " has mismatch in organization id (provided: " + orgId + " assigned: " + u.OID;
                                    u = null;
                                }
                                else
                                {
                                    User = u;
                                }

                                // Note: If u is null, the call HandleNullUser(u) below would take the user to the home page.
                                //       As per story [WEB-2624], we do not show any message and silently take the user to Home.
                            }
                            if (null == u)
                            {
                                siteUser.IdentityManagedExternalProvider = false; // Set to false to prevent infinite loop
                                LogMgr.Log(DataConstants.LogLevel.ERROR, reasonForFailure);
                                HandleNullUser(u, false);
                            }
                        }

                        if (null != u)
                        {
                            mHomeUrl = AuthnHelper.Cleanse(Request.Url.PathAndQuery);
                        }
                        HandleDiscoveryBypass(aliasOID);
                        if (!string.IsNullOrEmpty(SignData))
                        {
                            Global.Signin(SignData);
                            SignData = null;
                        }
                        else if (siteUser.IdentityManagedExternalProvider)
                        {
                            HandleNullUser(u);
                            bool authnFromIdP = false;
                            if (AuthnHelper.CompareLoginStateInfo(Request, u.Email, ref authnFromIdP) && authnFromIdP) // We came from an successful authn from Idp?
                            {
                                // Yes
                                LoginState = 2; // Set to client selection
                            }
                            else if(authnFromIdP) // Conflict
                            {
                                LoginState = 0; // Set back to soliciting of email address
                                lblLoginError.Visible = true;
                                lblLoginError.Text = conflictingAuthn;
                                txtLoginEmail.Text = u.Email;
                            }
                        }
                        //set rememberme email cookie if exists
                        HttpCookie cookie = Request.Cookies[cookieName];

                        if (cookie != null)
                        {
                            FormsAuthenticationTicket ticket = null;

                            try
                            {
                                ticket = FormsAuthentication.Decrypt(cookie.Value);

                                if (!ticket.Expired)
                                    rememberedName.Value = ticket.UserData;
                            }
                            catch (HttpUnhandledException huex)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error decrypting remember me cookie value", huex);
                            }

                        }       
                    }
                    ShowDivBasedOnLoginState();
                }
                //dont load login controls and show logout link
                else
                {
                    sessionID = Context.Session.SessionID;
                    mKeyForTimeoutCookie = KeyForTimeoutCookie();

                    mv.ActiveViewIndex = 1;

                    mWelcome = StringHelper.TrimText((String.IsNullOrWhiteSpace(siteUser.FirstName) ? siteUser.Email : siteUser.FirstName), 13);

                    //hide myaccoutn for public or kiosk user
                    if (siteUser.IsPublicOrKioskUser)
                        lnkMyAccount.Visible = false;

                    HideModals();
                    
					if (this.Visible)
					{
						Script.InvokeFunction
						(
							"startSession", new Object[]{(Session.Timeout * 60000)}
						);
					}

                    //organization override
                    if (!String.IsNullOrEmpty(siteUser.OrganizationIconUrl))
                    {               
                        imgSmallLogo.Alt = "Org";
                        imgSmallLogo.Src = siteUser.OrganizationIconUrl;
                        imgSmallLogo.Width = 32;
                        imgSmallLogo.Height = 32;                   
                    }
                    else
                    {
                        //SE theme check
                        imgSmallLogo.Alt = siteUser.IsSchneiderTheme ? "SE" : "CW";
                        imgSmallLogo.Src = ConfigurationManager.AppSettings["ThemeImageAssetPath"] + (siteUser.IsSchneiderTheme ? "se-small.png" : "cw-gears.png");
                    }
                   
                }
            }

        #endregion

        #region Dropdown Events

            protected void ddlProvider_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int selectedProviderID = -1;
                if (int.TryParse(hdnProviderSelected.Value, out selectedProviderID))
                {
                    if (selectedProviderID != -1)
                    {
                        ListItem lItem = new ListItem();
                        lItem.Text = "Select one...";
                        lItem.Value = "-1";
                        lItem.Selected = true;

                        //bind provider clients
                        ddlProviderClient.Items.Clear();
                        ddlProviderClient.Items.Add(lItem);
                        ddlProviderClient.DataTextField = "ClientName";
                        ddlProviderClient.DataValueField = "CID";
                        ddlProviderClient.DataSource = DataMgr.ClientDataMapper.FilterOutProviderClientsByProviderID(Session["TempProviderClients"] as IEnumerable<GetClientsWithRoleFormat>, selectedProviderID).OrderBy(c => c.ClientName);
                        ddlProviderClient.DataBind();
                    }

                    //rebind providers                
                    BindProviders(((Session["TempProviders"] as IList<KeyValuePair<int, string>>)));
                    ddlProvider.SelectedValue = selectedProviderID.ToString();
                }
            }

        #endregion

        #region Button Click Events

            bool redirectingToIdP = false;
            protected async void ContinueOnEmail_Click(object sender, EventArgs e)
            {
                User = null;
                Session.Remove("TempOrganizationClients");
                Session.Remove("TempProviders");
                Session.Remove("TempProviderClients");
                string email = txtLoginEmail.Text;

                lblLoginError.Text = "";
                //Get entered user
                User user = DataMgr.UserDataMapper.GetUser(email);

                if (user == null)
                {
                    lblLoginError.Text = loginInvalidUser;
                    LogMgr.Log(DataConstants.LogLevel.INFO, String.Format("Login invalid email: UserEmail={0}.", email));
                }
                else if (!user.IsActive)
                {
                    lblLoginError.Text = loginAccountDeactivated;
                    LogMgr.Log(DataConstants.LogLevel.INFO, String.Format("Login deactived account attempt: UserId={0}, UserEmail={1}.", user.UID, email));
                }
                else if (false == await GetSSOInfoAsync(email, user.OID))
                {
                    //Error
                }
                else if (user.IsLocked)
                {
                    lblLoginError.Text = loginAccountLocked;
                    LogMgr.Log(DataConstants.LogLevel.INFO, String.Format("Login locked account attempt: UserId={0}, UserEmail={1}.", user.UID, email));
                }

                if (!string.IsNullOrEmpty(SignData))
                {
                    Global.Signin(SignData);
                    SignData = null;
                }
                mv.ActiveViewIndex = 0;
                if (string.IsNullOrEmpty(lblLoginError.Text))
                {
                    if (!redirectingToIdP)
                    {
                        LoginState = 1; // switch to next state
                        txtLoginPassword.Focus();
                    }
                    User = user;
                    lblLoginError.Visible = false;
                }
                else
                {
                    lblLoginError.Visible = true;
                }
                ShowDivBasedOnLoginState();
            }

        /// <summary>
        /// Only valid for NonSSO (button will only appear if this is the case)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ContinueOnPwd_Click(object sender, EventArgs e)
        {
            LogMgr.Log(Common.Constants.DataConstants.LogLevel.DEBUG, "In ContinueOnPwd_Click()");
            lblLoginError.Text = "";
            User user = User;

            if (user == null)
            {
                string clients = mClients == null ? "" : String.Join(",", mClients.Select(c => c.ClientName));
                LogMgr.Log(Common.Constants.DataConstants.LogLevel.ERROR, $"User is null, clientSelectedValue: {clientSelectedValue} sessionID: {sessionID} clientList: {clients}");
            }
            else if (user.IsLocked)
            {
                mv.ActiveViewIndex = 0;
                lblLoginError.Text = loginAccountLocked;
                lblLoginError.Visible = true;
                pnlLogin.Style.Value = "display:block;";
                mpeLogin.Show();
            }

            SetCaptcha();

            ProcessLogin(user);

            ShowDivBasedOnLoginState();
        }

        private void ProcessLogin(User user)
        {
            if (user == null)
                return;

            if (!string.IsNullOrEmpty(lblLoginError.Text))
                return;

            if (Encryptor.verifyMd5Hash(txtLoginPassword.Text.Trim(), user.Password))
            {
                LogMgr.Log(Common.Constants.DataConstants.LogLevel.DEBUG, "In Pwd match succeeded!");
                // We good - user email/pwd matched

                // try to clear failed login attempts after success, and increment login success count
                try
                {
                    //show captcha
                    captchaVisible = false;

                    //Get user audit
                    UsersAudit mUserAudit = DataMgr.UserAuditDataMapper.GetUserAuditByUID(user.UID);

                    mUserAudit.LoginFailedAttemptCount = 0;
                    mUserAudit.LoginSuccessTotalCount = mUserAudit.LoginSuccessTotalCount + 1;
                    DataMgr.UserAuditDataMapper.UpdateUserAudit(mUserAudit);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("User audit update failed for user: UserEmail={0}.", user.Email), ex);
                }

                LoginState = 2; // Transition to next state

                return;
            }

            LogMgr.Log(Common.Constants.DataConstants.LogLevel.DEBUG, "In Pwd match failed!");
            //try to increment failed login attempt, show captcha, or lock account.
            try
            {
                //Get user audit
                UsersAudit mUserAudit = DataMgr.UserAuditDataMapper.GetUserAuditByUID(user.UID);

                mUserAudit.LoginFailedAttemptCount = mUserAudit.LoginFailedAttemptCount + 1;

                //show captcha
                captchaVisible = (mUserAudit.LoginFailedAttemptCount > 4);

                if (mUserAudit.LoginFailedAttemptCount > 9)
                {
                    //lock account
                    user.IsLocked = true;
                    DataMgr.UserDataMapper.UpdateUserLock(user);

                    LogMgr.Log(DataConstants.LogLevel.INFO, String.Format("Account locked for user: UserEmail={0}.", user.Email));
                }
                else
                {
                    LogMgr.Log(DataConstants.LogLevel.INFO, String.Format("Login password incorrect for user: UserEmail={0}.", user.Email));
                }

                mv.ActiveViewIndex = 0;
                lblLoginError.Text = user.IsLocked ? loginAccountLocked : loginInvalidPassowrd;
                lblLoginError.Visible = true;
                pnlLogin.Style.Value = "display:block;";
                mpeLogin.Show();

                DataMgr.UserAuditDataMapper.UpdateUserAudit(mUserAudit);
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("User audit update failed for user: UserEmail={0}.", user.Email), ex);
            }
        }

        private void SetCaptcha()
        {
            //**Captcha**
            //we use a hdn variable to check for capchas visibility because captcha cannot be set visible/not visible on button click in order for validation to work.
            //It must be visible from page_init, so we just show and hide via css but capcha remains enabled.                
            if (captchaVisible)
            {
                radCaptcha.Validate();
                if (!radCaptcha.IsValid)
                {
                    //show captcha again
                    captchaVisible = true;
                    mv.ActiveViewIndex = 0;
                    lblLoginError.Text = loginInvalidCaptcha;
                    lblLoginError.Visible = true;
                    pnlLogin.Style.Value = "display:block;";
                    mpeLogin.Show();
                }
            }
        }

        protected void loginButton_Click(object sender, EventArgs e)
            {
                lblLoginError.Text = "";
                DoLogin(User);
            }

            protected void logoutButton_Click(object sender, EventArgs e)
            {
                mv.ActiveViewIndex = 0;

                //hide panel so it doesnt cause validation
                pnlLogin.Style.Value = "display:none;";   
                mpeLogin.Hide();

                //hide forgot panel also
                pnlForgot.Style.Value = "display:none;";  
                mpeForgot.Hide();

                siteUser.LogOff(Context);
                Response.Redirect("~/Home.aspx");
            }     

            protected void forgotButton_Click(object sender, EventArgs e)
            {
                string email = txtForgotEmail.Text;

                //Get entered user
                User user = DataMgr.UserDataMapper.GetUser(email);
                
                //if email doesnt exist show error message
                if (user != null)
                {
                    if (user.IsActive)
                    {
                        string oldHashedPassword = user.Password;

                        //hash a new random password
                        string randomPassword = Membership.GeneratePassword(12, 2);
                        string newHashedPassword = Encryptor.getMd5Hash(randomPassword);

                        //IoC.LogHelper.LogCWData("Forgot password user's email correct: $email", mUser, null);

                        //try to email the user
                        try
                        {
                            Email.SendForgotPasswordEmail(user, randomPassword, ((Main)this.parentPage.Master).isSEDomain);

                            //IoC.LogHelper.LogCWData("Forgot password email was sent for user:$email", mUser, null); 

                            //try to update the user
                            try
                            {
                                //set new hashed password to user
                                user.Password = newHashedPassword;

                                //update user
                                DataMgr.UserDataMapper.UpdateUserPassword(user);

                                lblForgotError.Text = forgotPasswordSuccess;
                                lblForgotError.Visible = true;

                                //IoC.LogHelper.LogCWData("Forgot password update was successful for user: $email", mUser, null);
                            }
                            catch (Exception ex)
                            {
                                lblForgotError.Text = forgotPasswordFailed;
                                lblForgotError.Visible = true;

                                LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Forgot password update failed for user: UserEmail={0}.", email), ex);
                            }
                        }
                        catch (Exception ex)
                        {
                            lblForgotError.Text = forgotPasswordFailed;
                            lblForgotError.Visible = true;

                            LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Forgot password email was not sent for user: UserEmail={0}.", email), ex);
                        }
                    }
                    else
                    {
                        lblForgotError.Text = forgotPasswordAccountDeactivated;
                        lblForgotError.Visible = true;

                        LogMgr.Log(DataConstants.LogLevel.INFO, String.Format("Forgot password deactived account attempt: UserEmail={0}.", email));
                    }
                }
                else
                {
                    lblForgotError.Text = forgotPasswordInvalidUser;
                    lblForgotError.Visible = true;

                    LogMgr.Log(DataConstants.LogLevel.INFO, String.Format("Forgot password invalid email: UserEmail={0}.", email));
                }

                mv.ActiveViewIndex = 0;
                pnlForgot.Style.Value = "display:block;";  
                mpeForgot.Show();
            }

        #endregion

        #region Helper Methods

            private void HideModals()
            {
                //LOGIN----------

                lblLoginError.Visible = false;

                //to avoid panel showing on page load.
                pnlLogin.Style.Value = "display:none;";
                pnlLogin.Style.Value = "visibility:hidden;";

                //hide panel so it doesnt cause validation
                mpeLogin.Hide();

                //FORGOT PASS---------

                lblForgotError.Visible = false;

                //to avoid panel showing on page load.
                pnlForgot.Style.Value = "display:none;";
                pnlForgot.Style.Value = "visibility:hidden;";

                //hide panel so it doesnt cause validation
                mpeForgot.Hide();


                //RENEW SESSION------

                //to avoid panel showing on page load.
                pnlRenewSession.Style.Value = "display:none;";
                pnlRenewSession.Style.Value = "visibility:hidden;";

                //hide panel so it doesnt cause validation
                mpeRenewSession.Hide();
            }

            //protected override void OnPreRender(EventArgs e)
            //{
            //    //base.OnInit(e);
            //    //Validate only on postback 
            //    try
            //    {
            //        if (IsPostBack && radCaptcha.Visible)
            //        {
            //            radCaptcha.Validate();
            //            //initRadCaptchaValid = r.IsValid;
            //        }
            //    }
            //    catch
            //    {
            //    }
            //} 

        #endregion
            private Button LoginPanelLastDefaultButton
            {
                set
                {
                    lastLoginPanelDefaultButtonID.Value = value.ID;
                    pnlLogin.DefaultButton = value.ID;
                }
            }
            private void ShowDivBasedOnLoginState()
            {
                if (0 > LoginState)
                {
                    pnlLogin.Style.Value = "display:none;";
                    mpeLogin.Hide();
                    LoginPanelLastDefaultButton = btnProxy;
                }
                else
                {
                    pnlLogin.Style.Value = "display:block;";
                    mpeLogin.Show();
                }
                switch (LoginState)
                {
                    case 0: // Email
                    {
                        LoginPanelLastDefaultButton = btnContinueOnEmail;
                        ShowEmailArea(true);
                        ShowPasswordArea(false);
                        ShowClientArea(false);
                        break;
                    }
                    case 1: // Password
                    {
                        LoginPanelLastDefaultButton = btnContinueOnPwd;
                        ShowEmailArea(false);
                        ShowPasswordArea(true);
                        ShowClientArea(false);
                        break;
                    }
                    case 2: // Client
                    {
                        LoginPanelLastDefaultButton = btnLogin;
                        ShowEmailArea(false);
                        ShowPasswordArea(false);
                        ShowClientArea(true);
                        break;
                    }
                }
            }
            private void ShowEmailArea(bool show)
            {
                emailAddressModal.Style.Value = (show ? "display:block;" : "display:none;");
                lblLoginIntro.InnerText = "Please enter your email address.";
                loginEmailRequiredValidator.Enabled = show;
                loginEmailRequiredValidatorExtender.Enabled = show;
                loginEmailRegExValidator.Enabled = show;
                loginEmailRegExValidatorExtender.Enabled = show;
                btnContinueOnEmail.Enabled = show;
            }
            private void ShowPasswordArea(bool show)
            {
                if (show)
                {
                    HandleNullUser(User);
                    lblLoginIntro.InnerText = "Please enter your password.";
                }
                passwordModal.Style.Value = (show ? "display:block;" : "display:none;");
                loginPasswordRequiredValidator.Enabled = show;
                loginPasswordRequiredValidatorExtender.Enabled = show;
                btnContinueOnPwd.Enabled = show;
                if (!show) captchaVisible = false;
            }

            private void ShowClientArea(bool show)
            {
                clientModal.Style.Value = (show ? "display:block;" : "display:none;");
                btnLogin.Enabled = show;
                if (show)
                {
                    User user = User;
                    HandleNullUser(user);
                    lblLoginIntro.InnerText = "Please select the client you wish to login under.";
                    //Get Clients------
                    //get all distinct user clients from azure by uid. Ignore isActive at the moment. Distinct because restricted by building will present more than one of the same client
                    IEnumerable<UsersClients> usersClients = DataMgr.UserClientDataMapper.GetDistinctUsersClientsByUID(user.UID);

                    int primaryRoleID = usersClients.First().PrimaryRoleID;
                    bool isRestricted = RoleHelper.IsRestricted(primaryRoleID.ToString());
                    bool isKGSSuperAdminOrHigher = RoleHelper.IsKGSSuperAdminOrHigher(primaryRoleID.ToString());

                    //if one client and primary roleid is not restricted then get all clients, else populate with restricted clients
                    //active clients only.
                    mClients = DataMgr.ClientDataMapper.GetAllActiveClientsAssociatedToUIDWithRoleFlattened(user.UID, primaryRoleID, isKGSSuperAdminOrHigher, isRestricted, usersClients);

                    if (mClients.Count() != 0)
                    {
                        if (isKGSSuperAdminOrHigher)
                        {
                            mv.ActiveViewIndex = 0;
                            lblLoginError.Visible = false;

                            //bind organization client dropdown
                            ddlOrganizationClient.DataTextField = "ClientName";
                            ddlOrganizationClient.DataValueField = "CID";
                            ddlOrganizationClient.DataSource = mClients.OrderBy(c => c.ClientName);
                            ddlOrganizationClient.DataBind();

                            Session["TempOrganizationClients"] = mClients.ToList();

                            //hide div
                            divProviderSelection.Visible = false;
                        }
                        else
                        {
                            //filter providers client out, only organization clients
                            IEnumerable<GetClientsWithRoleFormat> organizationUsersClients = mClients.Where(c => c.OID == user.OID);
                            //IEnumerable<GetClientsWithRoleFormat> organizationUsersClients = DataMgr.ClientDataMapper.FilterOutProviderClientsByOID(mClients, mUser.OID);

                            //filter organization client out, only provider clients
                            IEnumerable<GetClientsWithRoleFormat> providerUsersClients = mClients.Where(c => c.OID != user.OID).Distinct(new CW.Business.ClientDataMapper.ClientFilterComparer()).ToList();
                            //IEnumerable<GetClientsWithRoleFormat> providerUsersClients = DataMgr.ClientDataMapper.FilterOutOrganizationClientsByOID(mClients, mUser.OID);                

                            int organizationUsersClientsCount = organizationUsersClients.Count();
                            int providerUsersClientsCount = providerUsersClients.Count();

                            //if only one org or prov client exists, skip client modal and set site user
                            if ((organizationUsersClientsCount == 1 && providerUsersClientsCount == 0) || (organizationUsersClientsCount == 0 && providerUsersClientsCount == 1))
                            {
                                //get and set providerid if providerUserClient = 1
                                int? providerID = null;
                                if (providerUsersClientsCount == 1)
                                {
                                    providerID = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByProviderIDsFlattenedByCID(DataMgr.ProviderDataMapper.GetAllProvidersIDsByOrg(user.OID)).Where(pc => pc.CID == providerUsersClients.First().CID).First().ProviderID;
                                }

                                //role id is set to primary or secondary accordingly already
                                int roleID = mClients.First().RoleID;

                                //***Security: Check if provider client with a maxrole, and override user to maxrole if thier role is higher. Protects against super admins that are not restricted just getting super admin access.***
                                if (providerID != null)
                                {
                                    int tempProviderClientMaxRoleID = DataMgr.ProviderClientDataMapper.GetProvidersClientsListFlattenedByProviderIDAndCID((int)providerID, mClients.First().CID).First().MaxRoleID;

                                    if (tempProviderClientMaxRoleID > roleID)
                                        roleID = tempProviderClientMaxRoleID;
                                }

                                //hide panel so it doesnt cause validation
                                pnlLogin.Style.Value = "display:none;";
                                mpeLogin.Hide();

                                SiteUser.LogOn(user, siteUser.IdentityManagedExternalProvider, roleID, mClients.First().CID, providerID, mClients.First().ClientName, mClients, providerUsersClients, Convert.ToSingle(Request.Form["TimeZoneOffset"]), DataMgr); //the SiteUser object will handle all Session variables related to user account... above lines will be obsolete

                                CompleteLogin(user);
                            }
                            else
                            {
                                //bind clients and show client modal
                                //show client modal
                                mv.ActiveViewIndex = 0;
                                lblLoginError.Visible = false;
                                pnlLogin.Style.Value = "display:block;";
                                mpeLogin.Show();

                                if (organizationUsersClientsCount != 0)
                                {
                                    //bind organization client dropdown
                                    ddlOrganizationClient.DataTextField = "ClientName";
                                    ddlOrganizationClient.DataValueField = "CID";
                                    ddlOrganizationClient.DataSource = organizationUsersClients.OrderBy(c => c.ClientName);
                                    ddlOrganizationClient.DataBind();

                                    Session["TempOrganizationClients"] = organizationUsersClients.ToList();
                                }
                                else
                                {
                                    //hide div
                                    divOrganizationSelection.Visible = false;
                                }

                                if (providerUsersClientsCount != 0)
                                {
                                    //IList<Provider> providers = DataMgr.ProviderDataMapper.GetAllProvidersByOrg(mUser.OID);
                                    IList<KeyValuePair<int, string>> providers = DataMgr.ProviderDataMapper.GetAllProvidersIDNameByOrg(user.OID);
                                    BindProviders(providers);

                                    Session["TempProviders"] = providers.ToList();
                                    Session["TempProviderClients"] = providerUsersClients.ToList();
                                }
                                else
                                {
                                    //hide div
                                    divProviderSelection.Visible = false;
                                }
                            }
                        }

                        clientType.Visible = divProviderSelection.Visible && divOrganizationSelection.Visible ? true : false;
                        if (clientType.Visible)
                        {
                            Script.InvokeFunction
                            (
                                "showOrgOrProviderClient", new Object[] { true }
                            );
                        }
                    }
                }
            }

            private string SignData { get; set; }
            private async Task<bool> GetSSOInfoAsync(Guid guid)
            {
                AuthNSSOInfo results = await AuthnHelper.GetSSOInfoAsync(null, guid).ConfigureAwait(false);
                return (_GetSSOInfoAsync(results));
            }
            private async Task<bool> GetSSOInfoAsync(string email, int oid)
            {
                AuthNSSOInfo results = await AuthnHelper.GetSSOInfoAsync(oid).ConfigureAwait(false);
                if (null != results)
                {
                    results.OID = oid;
                }
                return (_GetSSOInfoAsync(results, email));
            }

            private bool _GetSSOInfoAsync(AuthNSSOInfo results, string emailAddress = null)
            {
                SignData = null;
                bool ok = null != results;
                if( ok )
                {
                    if (results.SSOEnabled)
                    {
                        siteUser.IdentityManagedExternalProvider = true;
                        redirectingToIdP = true;
                        // Trigger redirect to IdP via our proxy
                        // Create anonymous json object
                        string referrer = null;
                        if (!String.IsNullOrWhiteSpace(hdnReferrer.Value))
                        {
                            referrer = hdnReferrer.Value;
                        }
                        try
                        {
                            SignData = CreateJsonForLogin(siteUser, referrer, false, emailAddress, Convert.ToString(results.OID));
                        }
                        catch (Exception ex)
                        {
                            lblLoginError.Text = ex.Message;
                        }
                    }
                }

                return (ok);
            }

            private void DoLogin(User user)
            {
                //protect against double clicks
                if (siteUser.IsAnonymous)
                {
                    //determine login path. org vs provider
                    bool isOrganizationLoginPath = String.IsNullOrEmpty(hdnOrganizationClientSelected.Value) || hdnOrganizationClientSelected.Value == "-1" ? false : true;

                    //set based on which login path org vs provider

                    //set providerid if provider path
                    int? providerID = isOrganizationLoginPath ? null : (int?)Convert.ToInt32(hdnProviderSelected.Value);

                    //clients and provider clients for dropdown
                    IEnumerable<GetClientsWithRoleFormat> deserializedTempOrganizationClients = Session["TempOrganizationClients"] as IEnumerable<GetClientsWithRoleFormat>;
                    IEnumerable<GetClientsWithRoleFormat> deserializedTempProviderClients = Session["TempProviderClients"] as IEnumerable<GetClientsWithRoleFormat>;

                    //filter clients for only logged in provider so we can have a home page provider view.
                    if (deserializedTempProviderClients != null && providerID != null)
                    {
                        deserializedTempProviderClients = DataMgr.ClientDataMapper.FilterOutProviderClientsByProviderID(deserializedTempProviderClients, (int)providerID);
                    }
                    else
                    {
                        //clear so logged in users for an org client doesnt have any provider stuff in session.
                        deserializedTempProviderClients = Enumerable.Empty<GetClientsWithRoleFormat>();
                    }

                    //TODO: maybe do a real provider client lookup and switch later.
                    ////old with unions, cant go from org client to provider client. 
                    //mClients = (isOrganizationLoginPath ? (deserializedTempProviderClients != null ? deserializedTempOrganizationClients.Union(deserializedTempProviderClients) : deserializedTempOrganizationClients) : (deserializedTempOrganizationClients != null ? deserializedTempProviderClients.Union(deserializedTempOrganizationClients) : deserializedTempProviderClients)).OrderBy(c => c.ClientName);                
                    mClients = (isOrganizationLoginPath ? deserializedTempOrganizationClients : deserializedTempProviderClients).OrderBy(c => c.ClientName);

                    //set based on which login path
                    int selectedCID = isOrganizationLoginPath ? Convert.ToInt32(hdnOrganizationClientSelected.Value) : Convert.ToInt32(hdnProviderClientSelected.Value);

                    //set roleid 
                    int roleID = mClients.Where(m => m.CID == selectedCID).First().RoleID;

                    //***Security: Check if provider client with a maxrole, and override user to maxrole if thier role is higher***
                    if (providerID != null)
                    {
                        int tempProviderClientMaxRoleID = DataMgr.ProviderClientDataMapper.GetProvidersClientsListFlattenedByProviderIDAndCID((int)providerID, selectedCID).First().MaxRoleID;

                        if (tempProviderClientMaxRoleID > roleID)
                        {
                            roleID = tempProviderClientMaxRoleID;

                            //update the role in the client list also. needed for client switch later.
                            mClients.Where(m => m.CID == selectedCID).First().RoleID = roleID;
                        }
                    }

                    //the SiteUser object will handle all Session variables related to user account
                    //ddlClientClient.SelectedValue dissapears on button click, must set value client side to a hidden field and then get value. 
                    SiteUser.LogOn(user, siteUser.IdentityManagedExternalProvider, roleID, selectedCID, providerID, mClients.Where(m => m.CID == selectedCID).First().ClientName, mClients, deserializedTempProviderClients, Convert.ToSingle(Request.Form["TimeZoneOffset"]), DataMgr);

                }
                CompleteLogin(user);
            }

            private void CompleteLogin(User user)
            {
                SiteUserDirty = true; // Force reget of siteuser from Session
                if (null == user && null != siteUser)
                {
                    user = DataMgr.UserDataMapper.GetUser(siteUser.Email);
                }
                HandleNullUser(user);
                string referrer = null;
                if(!String.IsNullOrWhiteSpace(hdnReferrer.Value))
                {
                    referrer = hdnReferrer.Value;
                }
                captchaVisible = false;
                SetSelectedClientOnPostback();
                CompleteLogin(user, siteUser, DataMgr, Script, Context, referrer, chkRememberMe.Checked);
            }

            static public void CompleteLogin(User user, SiteUser siteuser, DataManager dm, ScriptInterop scriptIop, HttpContext ctx, string referrer, bool rememberMe)
            {
                bool isHttpsEnabled;
                bool.TryParse(ConfigurationManager.AppSettings["IsHttpsEnabled"], out isHttpsEnabled);

                if (null == user && null != siteuser)
                {
                    user = dm.UserDataMapper.GetUser(siteuser.Email);
                }
                //remove Temp sessions
                ctx.Session.Remove("TempUser");
                ctx.Session.Remove("TempOrganizationClients");
                ctx.Session.Remove("TempProviders");
                ctx.Session.Remove("TempProviderClients");

                //set session timeout by config or override by org.
                var timeoutOverride = dm.OrganizationDataMapper.GetOrganizationByID(user.OID).SessionTimeoutOverride;
                if (null != timeoutOverride)
                {
                    ctx.Session.Timeout = timeoutOverride.Value;
                }
                string ckName = KeyForTimeoutCookie();
                HttpCookie ck = ctx.Request.Cookies[ckName];
                if (null != ck)
                {
                    ctx.Response.Cookies.Remove(ckName);
                }
                ck = new HttpCookie(ckName);
                DateTime dtFuture = DateTime.UtcNow.AddMinutes(ctx.Session.Timeout);
                ck.HttpOnly = false;
                ck.Secure = isHttpsEnabled;
                ck.Value = dtFuture.ToString();
                ck.Expires = dtFuture;
                ctx.Response.Cookies.Add(ck);

                //check if refferer url was provided
                string partialUrl = "";
                if (!string.IsNullOrEmpty(referrer) && !referrer.ToUpper().Contains("HOME.ASPX"))
                {
                    partialUrl = AuthnHelper.Cleanse(referrer);
                }
                else
                {
                    partialUrl = "Home.aspx";
                }

                //store rememberme cookie for email

                HttpCookie cookie = ctx.Request.Cookies[cookieName] ?? new HttpCookie(cookieName);
                cookie.HttpOnly = true;
                cookie.Secure = isHttpsEnabled;

                if (rememberMe)
                {
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                       "clockworksUser",
                       DateTime.UtcNow,
                       DateTime.UtcNow.AddYears(1),
                       true,
                        user.Email); 
                    
                    cookie.Value = FormsAuthentication.Encrypt(ticket);
                    cookie.Expires = DateTime.UtcNow.AddYears(1);

                    if(ctx.Request.Cookies[cookieName] == null)
                        ctx.Response.Cookies.Add(cookie);
                    else
                        ctx.Response.SetCookie(cookie);
                }
                else
                {
                    if (ctx.Request.Cookies[cookieName] != null)
                    {
                        cookie.Expires = DateTime.UtcNow.AddDays(-1);
                        ctx.Response.SetCookie(cookie);
                    }
                }

                if (siteuser.IdentityManagedExternalProvider)
                {
                    Uri baseUri = new Uri(GetSiteAddress(siteuser));
                    Uri fullUrl = new Uri(baseUri, partialUrl);
                    if (null != scriptIop)
                    {
                        scriptIop.InvokeFunction
                        (
                            "onAuthenticated", new Object[] { fullUrl.PathAndQuery }
                        );
                    }
                }
                else
                {
                    // Create anonymous json object
                    Global.Signin(CreateJsonForLogin(siteuser, referrer, true, user.Email));
                }
            }

            /// <summary>
            /// silent is true for non SSO
            /// </summary>
            /// <param name="siteuser"></param>
            /// <param name="referrer"></param>
            /// <param name="silent"></param>
            /// <param name="emailAddress"></param>
            /// <param name="orgId"></param>
            /// <param name="baseUri"></param>
            /// <returns></returns>
            static private string CreateJsonForLogin(SiteUser siteuser, string referrer, bool silent, string emailAddress, string orgId = null, Uri baseUri = null)
            {
                string msg = headerMsgForInterstitialPage;
                string msg2 = paragraphMsgForInterstitialPage;
                if (null == baseUri) 
                {
                    baseUri = new Uri(GetSiteAddress(siteuser));
                }
                bool isSchnieder = siteuser.IsSchneiderTheme;
                //string imgLoc = "/_assets/styles/themes/images/";
                string imgLoc = ConfigurationManager.AppSettings["ThemeImageAssetPath"].Replace("\\", "/").Replace("~", "").Trim();
                string imgFile = !isSchnieder ? "buildings-bg.png" : "buildings-bg-se.png";
                string hdrImgFile = !isSchnieder ? "cw-header.png" : "se-header.png";
                object json = null;
                if (silent) // Non SSO (silent login)
                {
                    // OrgId will always be null
                    json = new { RequestType = "silent", Message = msg, Message2 = msg2, Referrer = referrer ?? "", ImgLoc = imgLoc, ImgFile = imgFile, EmailAddress = emailAddress, HdrImgFile = hdrImgFile, Timestamp = DateTime.UtcNow.Ticks };
                }
                else // SSO (redirect)
                {
                    // OrgId will ALWAYS be present
                    json = new { RequestType = "idp", Message = msg, Message2 = msg2, Referrer = referrer ?? "", ImgLoc = imgLoc, ImgFile = imgFile, EmailAddress = emailAddress, HdrImgFile = hdrImgFile, OrgID = orgId, Timestamp = DateTime.UtcNow.Ticks, DiscoveryBypassed = null == emailAddress };
                }
                return (JsonConvert.SerializeObject(json));
            }

            /// <summary>
            /// bind provider dropdown
            /// </summary>
            /// <param name="providers"></param>
            private void BindProviders(IList<KeyValuePair<int, string>> providers)
            {
                if (null != providers && providers.Count() > 0)
                {
                    while (ddlProvider.Items.Count > 1) // 1st item is the "Select one..." which we keep
                    {
                        ddlProvider.Items.RemoveAt(1);
                    }
                    ddlProvider.DataTextField = "Value";
                    ddlProvider.DataValueField = "Key";
                    ddlProvider.DataSource = providers.OrderBy(p => p.Value);
                    ddlProvider.DataBind();
                }
                else
                {
                    ddlProvider.Items.Clear();
                }
            }

            private void HandleNullUser(User user, bool log = true)
            {
                if (null == user)
                {
                    LogMgr.Log(Common.Constants.DataConstants.LogLevel.DEBUG, "User is NULL in HandleNullUser(); redirecting to " + mHomeUrl);
                    Response.Redirect("~" + mHomeUrl);
                }
            }
            private bool captchaVisible
            {
                get
                {
                    bool isVisible = false;
                    if (!bool.TryParse(hdnCaptchaIsVisible.Value, out isVisible))
                    {
                        isVisible = false;
                    }
                    return (isVisible);
                }
                set
                {
                    radCaptcha.Style.Value = "display:" + (value ? "block" : "none") + ";";
                    radCaptcha.Style.Add("visibility", (value ? "visible" : "hidden"));
                    hdnCaptchaIsVisible.Value = value.ToString().ToLower();
                }
            }
            private void SetSelectedClientOnPostback()
            {
                if (2 == LoginState)
                {
                    DropDownList ddl = null;
                    if (!clientType.Visible)
                    {
                        ddl = ddlOrganizationClient;
                    }
                    else
                    {
                        switch (rblClientType.SelectedValue)
                        {
                            case "0": //Organizational
                                {
                                    ddl = ddlOrganizationClient;
                                    break;
                                }
                            case "1": //Provider
                                {
                                    ddl = ddlProviderClient;
                                    break;
                                }
                        }
                    }
                    if (null != ddl)
                    {
                        string val = Request.Form[ddl.ClientID.Replace('_', '$')];
                        ddl.SelectedValue = val;
                    }
                }
            }

            static public string GetSiteAddress(SiteUser su)
            {
                string key = su.IsSchneiderTheme ? "SchneiderElectricSiteAddress" : "SiteAddress";
                return (ConfigurationManager.AppSettings[key]);
            }
            private void HandleDiscoveryBypass(string aliasOID)
            {
                if (!string.IsNullOrEmpty(aliasOID))
                {
                    User = null;
                    siteUser.IdentityManagedExternalProvider = false;
                    Guid guid = Guid.Empty;
                    if (Guid.TryParse(aliasOID, out guid) && guid != Guid.Empty)
                    {
                        bool ok = GetSSOInfoAsync(guid).Result;
                    }
                    else
                    {
                        Response.Redirect("~" + mHomeUrl);
                    }
                }
            }
    }
}