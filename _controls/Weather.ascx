﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Weather.ascx.cs" Inherits="CW.Website._controls.Weather" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>

<div id="weather" class="weather">    
  <div class="divDockFormWrapperShortest">                                      
      <div class="divDockFormRight">
        <label class="labelRight">Building:</label>
        <asp:DropDownList CssClass="dropdownRight" ID="ddlWeatherBuilding" runat="server" ClientIDMode="Static" />
      </div>
  </div>  
       
  <asp:Panel ID="pnlNoWeather" runat="server" Visible="false">
    <label id="lblNoWeather" class="dockMessageLabel" runat="server">Cannot find weather information for zipcode, city, state or country on account.</label>  
  </asp:Panel>

  <asp:Panel ID="pnlHasWeather" runat="server" Visible="false">
    <label id="lblWeatherLocation" class="lblWeatherLocation" runat="server" />                                   
    
    <div id="currentWeather">       
      <img id="currentWeatherImage" src="Current" alt="" class="currentWeatherImage" runat="server" />        
      <div id="currentTemp">
        <label id="lblTemp" runat="server" />
      </div>                                                                                  
      <div id="currentCond">
        <div id="divForecast" runat="server"><label id="lblForecast" runat="server">Current: </label> <span ID="spnForecastVal" runat="server" /></div>
        <div id="divHumidity" runat="server"><label id="lblHumidity" runat="server">Humidity: </label><span ID="spnHumidityVal" runat="server" /></div>
        <div id="divWind" runat="server"><label id="lblWind" runat="server">Wind: </label><span ID="spnWindVal" runat="server" /></div>
      </div>
    </div> 

    <div id="futureWeather">              
      <div id="todayDiv">             
        <div id="todayTop">
          <label id="lblToday" runat="server" /><br />
          <img id="todayImage" src="Today" alt="" runat="server" />
        </div>
        <div id="Div3">                                                
          <label id="lblTodayTempHi" runat="server" />&nbsp;|&nbsp;
          <label id="lblTodayTempLo" runat="server" /><br />
          <label id="lblTodayForcast" runat="server" />
        </div>  
      </div> 
                             
      <div id="tomorrowDiv">
        <div id="tomorrowTop">
          <label id="lblTomorrow" runat="server" /><br />
          <img id="tomorrowImage" src="Tomorrow" alt="" runat="server" />
        </div>
        <div id="tomorrowCond">                                                
          <label id="lblTomorrowTempHi" runat="server" />&nbsp;|&nbsp;
          <label id="lblTomorrowTempLo" runat="server" /><br />
          <label id="lblTomorrowForcast" runat="server" />
        </div>              
      </div>

      <div id="twoDaysDiv">
        <div id="twoDaysTop">
          <label id="lblTwoDays" runat="server" /><br />
          <img id="twoDaysImage" src="Two Days" alt="" runat="server" />
        </div>
        <div id="twoDaysCond">
          <label id="lblTwoDaysTempHi" runat="server" />&nbsp;|&nbsp;
          <label id="lblTwoDaysTempLo" runat="server" /><br />
          <label id="lblTwoDaysForcast" runat="server" />
        </div>           
      </div>

      <div id="threeDaysDiv">
        <div id="threeDaysTop">
          <label id="lblThreeDays" runat="server" /><br />
          <img id="threeDaysImage" src="Three Days" alt="" runat="server" />
        </div>
        <div id="threeDaysCond">
          <label id="lblThreeDaysTempHi" runat="server" />&nbsp;|&nbsp;
          <label id="lblThreeDaysTempLo" runat="server" /><br />
          <label id="lblThreeDaysForcast" runat="server" />
        </div>        
      </div>
                               
    </div> <!--end of futureWeather div-->
      
  </asp:Panel> <!--end of pnlHasWeather div-->

</div> <!--end of weather div-->