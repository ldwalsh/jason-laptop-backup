﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SecondaryAdminNav.ascx.cs" Inherits="CW.Website._controls.SecondaryAdminNav" %>
<%@ Import Namespace="CW.Website._framework.httpmodules" %>

<div class="adminNav">

  <ul class="dropdown">
    <li><a href="/Administration.aspx" class="hoverBtn">Admin Home</a></li>
    <li><a href="/DataSourceAdministration.aspx" class="hoverBtn">Data Sources</a></li>
    <li><a class="hoverBtnNoCursor">Buildings</a>
      <ul class="sub_menu">
        <li runat="server" data-boolean-check="1"><a href="/BuildingGroupAdministration.aspx">Building Groups</a></li>
        <li><a href="/BuildingAdministration.aspx">Buildings</a></li>
      </ul>
    </li>
    <li><a href="/EquipmentAdministration" class="hoverBtn">Equipment</a></li>
    <li><a href="/PointAdministration.aspx" class="hoverBtn">Points</a></li>
    <li><a class="hoverBtnNoCursor">Analyses</a>
      <ul class="sub_menu">
        <li><a href="/AnalysisAdministration.aspx">Analyses</a></li>
        <li><a href="/ScheduledAnalysesAdministration.aspx">Scheduled Analyses</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Users</a>
      <ul class="sub_menu">
        <li><a href="/UserAdministration.aspx">Users</a></li>
        <li runat="server" data-boolean-check="2"><a href="/<%= PathRewriteModule.UserGroupAdministration %>">User Groups</a></li>
      </ul>
    </li>
    <li runat="server" data-boolean-check="2"><a href="/<%= PathRewriteModule.ClientAdministration %>" class="hoverBtn">Client</a></li>
    <li runat="server" data-boolean-check="3"><a href="/<%= PathRewriteModule.OrganizationAdministration %>" class="hoverBtn">Organization</a></li>
    <li runat="server" data-boolean-check="0"><a class="hoverBtnNoCursor">Services</a>
      <ul class="sub_menu">
        <li runat="server" data-boolean-check="0"><a href="/BureauReportsv2.aspx">Bureau Reports V2</a></li>
      </ul>
    </li>
  </ul>

</div>         