﻿using CW.Common.Constants;
using CW.Logging;
using CW.Data;
using CW.Data.Extensions;
using CW.Data.Models;
using CW.Data.Models.Diagnostics;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    public abstract class ModularSiteUserControl : SiteUserControl
    {
        #region fields

            protected enum DiagnosticsResultTypes { LastMonth = 0, Past30Days = 1, Yesterday = 2 }

            private List<ModuleAccess> moduleAccessList = new List<ModuleAccess>();
                
        #endregion

        #region properties

            protected IEnumerable<Building> Buildings { get; set; }
            protected IDictionary<String, IEnumerable<DiagnosticsResult>> DiagnosticResultSets { get; set; }
            protected Boolean HasDiagnosticAndCommissioningAccess { get; set; }
            protected Boolean HasProfileAccess { get; set; }
            protected Boolean HasBuildings { get; set; }
            protected Boolean IsProviderView { get; set; }
            protected Boolean IsByClient { get; set; }
            protected String EarliestBuildingTimeZoneID { get; set; }
            protected List<ModuleAccess> ModuleAccessList { get; set; }
            protected IEnumerable<Int32> ClientIDs { get; set; }

        #endregion

        #region constructor
        
            public ModularSiteUserControl() { }

        #endregion

        #region abstract/virtual methods
        
            protected abstract DiagnosticsResultTypes DiagnosticResultType { get; }
            protected abstract IEnumerable<DiagnosticsResult> QueryDiagnostics();
            protected abstract void SetError();

            protected virtual void BindDiagnostics(IEnumerable<DiagnosticsResult> results) { }
            protected virtual void Initialize() { }
            protected virtual void BindControl() { }
        
        #endregion

        #region methods

            public void LoadModule(Boolean isProviderView = false, Boolean isByClient = false)
            {
                if (!siteUser.IsAnonymous)
                {
                    IsProviderView = isProviderView;
                    IsByClient = isByClient;

                    HasBuildings = (IsProviderView) ? siteUser.VisibleBuildingsProviderClients.Any() : siteUser.VisibleBuildings.Any();
                    EarliestBuildingTimeZoneID = (IsProviderView) ? siteUser.EarliestBuildingTimeZoneIDProviderClients : siteUser.EarliestBuildingTimeZoneID;
                    ClientIDs = (IsProviderView) ? siteUser.ProviderClients.Select(c => c.CID) : new List<Int32>() { siteUser.CID };

                    if (HasBuildings)
                    {
                        if (IsProviderView)
                            SetProviderModulesAccessAndBuildings();
                        else
                            SetClientModulesAccessAndBuildings();

                        ModuleAccessList = moduleAccessList;

                        DiagnosticResultSets = new Dictionary<String, IEnumerable<DiagnosticsResult>>();
                    }

                    BindControl();
                }
            }

            protected void RetrieveAndBindDiagnostics()
            {
                var key = Enum.GetName(typeof(DiagnosticsResultTypes), DiagnosticResultType);

                try
                {
                    Initialize();

                    if (!HasDiagnosticsResults(key))
                        SetDiagnosticsResults(key, QueryDiagnostics());

                    BindDiagnostics(GetDiagnosticsResults(key));
                }
                catch (Exception ex)
                {
                    if (ex.IsFatal())
                        throw;

                    SetError();
                    LogMgr.Log(DataConstants.LogLevel.ERROR, ex.Message, ex); 
                }
            }

        #endregion

        #region helper methods

            private Boolean HasDiagnosticsResults(String key)
            {
                return DiagnosticResultSets.ContainsKey(key);
            }

            private void SetDiagnosticsResults(String key, IEnumerable<DiagnosticsResult> results)
            {
                DiagnosticResultSets[key] = results;
            }

            protected IEnumerable<DiagnosticsResult> GetDiagnosticsResults(String key)
            {
                return DiagnosticResultSets.ContainsKey(key) ? DiagnosticResultSets[key] : null;
            }

            protected IEnumerable<DiagnosticsResult> DiagnosticsRetrieval(DiagnosticsResultsInputs inputs)
            {
                return DataMgr.DiagnosticsDataMapper.GetDiagnosticsResults(inputs, Buildings.Where(b => b.CID == inputs.CID).Select(b => b.BID));
            }

            protected DiagnosticSummary GetDiagnosticsSummary(Building b, String clientName, Int32 lcid, Int32 faultCount, Double costSavings, DateTime startDate, DataConstants.AnalysisRange range)
            {
                return new DiagnosticSummary()
                {
                    CID = b.CID,
                    BID = b.BID,
                    ClientName = clientName,
                    BuildingName = b.BuildingName,
                    CostSavings = costSavings,
                    FaultCount = faultCount,
                    StartDate = startDate,
                    AnalysisRange = range,
                    LCID = lcid
                };
            }

            protected IEnumerable<DiagnosticSummary> GetDiagnosticsSummaries(IEnumerable<DiagnosticsResult> results, IEnumerable<Building> buildingsWithSettings, DateTime startDate)
            {
                if (results == null || !results.Any()) 
                    return Enumerable.Empty<DiagnosticSummary>();

                var diagnosticsResults = buildingsWithSettings
                                         .Select(b => GetDiagnosticsSummary(b,
                                                                            DataMgr.DiagnosticsDataMapper.FilterNonFaultResults(results).Where(dr => dr.BID == b.BID).Select(dr => dr.ClientName).FirstOrDefault(),
                                                                            b.BuildingSettings.First().LCID,
                                                                            DataMgr.DiagnosticsDataMapper.FilterNonFaultResults(results).Where(bd => bd.BID == b.BID).Count(),
                                                                            results.Where(bd => bd.BID == b.BID).Select(d => d.CostSavings).Sum(),
                                                                            startDate,
                                                                            results.First().AnalysisRange));

                return (IsProviderView && IsByClient) ? diagnosticsResults.GroupBy(dr => new { dr.CID, dr.LCID })
                                                                          .Select(dr => new DiagnosticSummary()
                                                                          {
                                                                              CID = dr.Key.CID,
                                                                              BID = dr.FirstOrDefault().BID,
                                                                              ClientName = dr.FirstOrDefault().ClientName,
                                                                              BuildingName = dr.FirstOrDefault().BuildingName,
                                                                              CostSavings = diagnosticsResults.Where(r => r.CID == dr.Key.CID && r.LCID == dr.Key.LCID).Select(r => r.CostSavings).Sum(),
                                                                              FaultCount = diagnosticsResults.Where(r => r.CID == dr.Key.CID && r.LCID == dr.Key.LCID).Select(r => r.FaultCount).Sum(),
                                                                              StartDate = dr.FirstOrDefault().StartDate,
                                                                              AnalysisRange = dr.FirstOrDefault().AnalysisRange,
                                                                              LCID = dr.Key.LCID
                                                                          }) : diagnosticsResults;
            }

            protected IEnumerable<DiagnosticsResult> GetConcatenatedResults(DateTime startDate, DateTime endDate, DataConstants.AnalysisRange analysisRange, Boolean hasFilter = true)
            {
                var results = Enumerable.Empty<DiagnosticsResult>();
                var inputs = new DiagnosticsResultsInputs();

                inputs.StartDate = startDate;
                inputs.EndDate = endDate;
                inputs.AnalysisRange = analysisRange;

                foreach (var cid in ClientIDs)
                {
                    inputs.CID = cid;

                    if (moduleAccessList.Where(ma => ma.CID == cid).Select(ma => ma.HasDiagnosticAndCommissioningAccess).FirstOrDefault() && HasBuildings)
                        results = (hasFilter) ? results.Concat(DataMgr.DiagnosticsDataMapper.FilterNonFaultResults(DiagnosticsRetrieval(inputs))) : results.Concat(DiagnosticsRetrieval(inputs));
                }

                return results.ToList();
            }

            protected IEnumerable<CostSavings> GetCostSavings(IEnumerable<DiagnosticsResult> results)
            {
                if (results == null || !results.Any())
                    return Enumerable.Empty<CostSavings>();

                return results.GroupBy(b => b.LCID).Select(g =>
                    new CostSavings()
                    {
                        Cost = g.Sum(c => c.CostSavings),
                        LCID = g.Key
                    }
                );
            }

            protected IEnumerable<Building> GetBuildingsWithSettings(IEnumerable<DiagnosticsResult> results)
            {
                var buildingsWithSettings = Enumerable.Empty<Building>();

                try
                {
                    buildingsWithSettings = DataMgr.BuildingDataMapper.GetBuildingsByBIDList(results.GroupBy(r => r.BID).Select(b => b.Key), true);
                }
                catch (SqlException sqlex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error calling GetBuildingsByBIDList", sqlex);
                }

                return buildingsWithSettings;
            }

            private void SetProviderModulesAccessAndBuildings()
            {
                var moduleIds = new List<Int32>() 
                            { 
                                (Int32)BusinessConstants.Module.Modules.Diagnostics, 
                                (Int32)BusinessConstants.Module.Modules.CommissioningDashboard 
                            };

                foreach (var cid in ClientIDs)
                {
                    foreach (var moduleId in moduleIds)
                    {
                        var moduleProviderClients = siteUser.ModulesProviderClients;
                        var moduleAccessItem = new ModuleAccess()
                        {
                            CID = cid,
                            HasDiagnosticAndCommissioningAccess = (moduleProviderClients.Where(m => m.Key == cid).First().Value.Select(i => i.ModuleID).Contains(moduleId)),
                            HasBuildingProfileAccess = moduleProviderClients.Any(m => m.Key == cid && m.Value.Select(i => i.ModuleID).Contains((Int32)BusinessConstants.Module.Modules.BuildingProfiles))
                        };

                        moduleAccessList.Add(moduleAccessItem);
                    }
                }

                Buildings = new List<Building>();

                foreach (var cid in ClientIDs)
                {
                    var visibleBuildingsList = siteUser.VisibleBuildingsProviderClients.Where(vb => vb.Key == cid).Select(vb => vb.Value);

                    foreach (var buildings in visibleBuildingsList)
                        Buildings = Buildings.Concat(buildings).OrderBy(b => b.BID);
                }
            }

            private void SetClientModulesAccessAndBuildings()
            {
                var moduleAccessItem = new ModuleAccess()
                {
                    CID = ClientIDs.First(),
                    HasDiagnosticAndCommissioningAccess = siteUser.Modules.Where(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Diagnostics ||
                                                                                      s.ModuleID == (Int32)BusinessConstants.Module.Modules.CommissioningDashboard).Count() == 2 ? true : false,
                    HasBuildingProfileAccess = siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.BuildingProfiles)
                };

                moduleAccessList.Add(moduleAccessItem);

                Buildings = siteUser.VisibleBuildings.OrderBy(b => b.BID);
            }

            //TODO, possibly make a utility or helper for grid related items like this.
            protected Int32 GetColumnIndexByHeader(GridView gridView, String headerText)
            {
                if (headerText.ToLower().Trim() == "lastcolumn")
                    return gridView.Columns.Count - 1;

                for (var index = 0; index < gridView.Columns.Count; index++)
                {
                    if (gridView.Columns[index].HeaderText.ToLower().Trim() == headerText.ToLower().Trim())
                        return index;
                }

                return -1;
            }

        #endregion
    }
}