﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecondaryProfileNav.ascx.cs" Inherits="CW.Website._controls.SecondaryProfileNav" %>

 
 <div class="profileNav">
    <ul class="dropdown">
        <li><a href="Profiles.aspx" class="hoverBtn">Profiles</a></li>        	
    	<li><a href="BuildingProfile.aspx" class="hoverBtn">Buildings</a></li>
    	<li><a href="EquipmentProfile.aspx" class="hoverBtn">Equipment</a></li>    	
    </ul>
</div>    