﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecentTasks.ascx.cs" Inherits="CW.Website._controls.RecentTasks" %>
<%@ Import Namespace="CW.Data.Models" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Literal ID="litError" runat="server" />
<asp:Literal ID="litTaskMessages" runat="server" />

<div id="gridTbl">
                                            
  <asp:GridView ID="gridRecentTasks" runat="server"> 
    <Columns>
      <asp:TemplateField>
        <ItemTemplate><label title="<%# Eval(PropHelper.G<GetTasks>(_ => _.Summary)) %>"><%# TrimCellContents(Eval(PropHelper.G<GetTasks>(_ => _.Summary)).ToString(), 30) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate><label title="<%# Eval(PropHelper.G<GetTasks>(_ => _.EquipmentName)) %>"><%# TrimCellContents(Eval(PropHelper.G<GetTasks>(_ => _.EquipmentName)).ToString(), 22) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate><label title="<%# Eval(PropHelper.G<GetTasks>(_ => _.Email)) %>"><%# TrimCellContents(Eval(PropHelper.G<GetTasks>(_ => _.Email)).ToString(), 22) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate><asp:HyperLink ID="lnkTaskRecord" runat="server" /></ItemTemplate>
      </asp:TemplateField>                                                                                                                                                                         
    </Columns>        
  </asp:GridView> 

</div>