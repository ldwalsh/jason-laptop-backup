﻿<%@ Page Language="C#" AutoEventWireup="false" CodeBehind="LoadUserControl.aspx.cs" Inherits="CW.Website._controls.LoadUserControl" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/BuildingMap.ascx" tagname="BuildingMap" tagprefix="CW" %>
<%@ Register src="~/_controls/LastMonthsDiagnosticSummary.ascx" tagname="LastMonthsDiagnosticSummary" tagprefix="CW" %>
<%@ Register src="~/_controls/LastMonthsDiagnosticResults.ascx" tagname="LastMonthsDiagnosticResults" tagprefix="CW" %>
<%@ Register src="~/_controls/Weather.ascx" tagname="Weather" tagprefix="CW" %>
<%@ Register src="~/_controls/Past30DaysAvoidableCosts.ascx" tagname="Past30DaysAvoidableCosts" tagprefix="CW" %>
<%@ Register src="~/_controls/YesterdaysDiagnosticSummary.ascx" tagname="YesterdaysDiagnosticSummary" tagprefix="CW" %>
<%@ Register src="~/_controls/YesterdaysDiagnosticResults.ascx" tagname="YesterdaysDiagnosticResults" tagprefix="CW" %>
<%@ Register src="~/_controls/Past30DaysPortfolioPriorities.ascx" tagname="Past30DaysPortfolioPriorities" tagprefix="CW" %>
<%@ Register src="~/_controls/QuickLinks.ascx" tagname="QuickLinks" tagprefix="CW" %>
<%@ Register src="~/_controls/BuildingDataPointsPieChart.ascx" tagname="BuildingDataPointsPieChart" tagprefix="CW" %>
<%@ Register src="~/_controls/ConsumptionComparison.ascx" tagname="ConsumptionComparison" tagprefix="CW" %>
<%@ Register src="~/_controls/RecentTasks.ascx" tagname="RecentTasks" tagprefix="CW" %>
<%@ Register src="~/_controls/TopProjects.ascx" tagname="TopProjects" tagprefix="CW" %>
<%@ Register src="~/_controls/FaultsSparkLine.ascx" tagname="FaultsSparkLine" tagprefix="CW" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

  <head id="Head1" runat="server">
    <title></title>
    <style type="text/css">
        .dockMessageLabel {
            font-size: 11px;
            padding: 18px 0;
            text-align: center;
            display: block;
        }
    </style>
  </head>

  <body>

    <form id="form1" runat="server">
      <div>

        <telerik:RadScriptManager ID="ScriptManager" EnableScriptCombine="true" EnableCdn="true" EnableScriptGlobalization="true" AsyncPostBackTimeout="600" runat="server" />

        <div id="divLoadControlError" runat="server" />

        <div id="contentToLoadBuildingMap">
          <CW:BuildingMap ID="BuildingMap" runat="server" />
        </div>

        <div id="contentToLoadLastMonthsDiagnosticSummary">
          <CW:LastMonthsDiagnosticSummary ID="LastMonthsDiagnosticSummary" runat="server" />
        </div>

        <div id="contentToLoadLastMonthsDiagnosticResults">
          <CW:LastMonthsDiagnosticResults ID="LastMonthsDiagnosticResults" runat="server" />
        </div>

        <div id="contentToLoadWeather">
          <CW:Weather ID="Weather" runat="server" />
        </div>
        <!---->
        <div id="contentToLoadPast30DaysAvoidableCosts">
          <CW:Past30DaysAvoidableCosts ID="Past30DaysAvoidableCosts" runat="server" />
        </div>

        <div id="contentToLoadYesterdaysDiagnosticSummary">
          <CW:YesterdaysDiagnosticSummary ID="YesterdaysDiagnosticSummary" runat="server" />
        </div>

        <div id="contentToLoadYesterdaysDiagnosticResults">
          <CW:YesterdaysDiagnosticResults ID="YesterdaysDiagnosticResults" runat="server" />
        </div>

        <div id="contentToLoadPast30DaysPortfolioPriorities">
          <CW:Past30DaysPortfolioPriorities ID="Past30DaysPortfolioPriorities" runat="server" />
        </div>

        <div id="contentToLoadQuickLinks">
          <CW:QuickLinks ID="QuickLinks" runat="server" />
        </div>

        <div id="contentToLoadBuildingDataPointsPieChart">
          <CW:BuildingDataPointsPieChart ID="BuildingDataPointsPieChart" runat="server" />
        </div>

		<div id="contentToLoadConsumptionComparison">
          <CW:ConsumptionComparison ID="ucConsumptionComparison" runat="server" />
		</div>

        <div id="contentToLoadRecentTasks">
		  <CW:RecentTasks ID="RecentTasks" runat="server" />
		</div>

        <div id="contentToLoadTopProjects">
		  <CW:TopProjects ID="TopProjects" runat="server" />
		</div>
          
    	<div id="contentToLoadFaults">
          <CW:FaultsSparkLine ID="FaultsSparkLine" runat="server" />
        </div>

      </div>
    </form>

  </body>

</html>