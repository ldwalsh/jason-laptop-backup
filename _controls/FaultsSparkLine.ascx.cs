﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

using CW.Common.Constants;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using CW.Reporting.Highcharts;
using CW.Website.Dashboard;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using System.Globalization;


namespace CW.Website._controls
{
    public partial class FaultsSparkLine : ModularSiteUserControl
    {
        #region fields

            private DateTime startDate, endDate;
            private CultureInfo cultureInfo;
            private DataConstants.AnalysisRange dataRange;
     
        #endregion

        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.LastMonth; } }

        #endregion

        #region methods

            #region overrides

                protected override void Initialize()
                {
                    cultureInfo = CultureHelper.GetCultureInfo(siteUser.CultureName);
                }

                protected override void BindControl()
                {
                    RetrieveAndBindDiagnostics();
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    //Building Monthly Faults

                    //monthly
                    dataRange = DataConstants.AnalysisRange.Monthly;
                    DateTimeHelper.GenerateDefaultDatesPastMonth(out startDate, out endDate, EarliestBuildingTimeZoneID);
                    var results = GetConcatenatedResults(DateTimeHelper.GetXMonthsAgo(startDate, 11), endDate, dataRange, false);
                    
                    if(!results.Any())
                    {
                        //weekly
                        dataRange = DataConstants.AnalysisRange.Weekly;
                        DateTimeHelper.GenerateDefaultDatesPastWeek(out startDate, out endDate, EarliestBuildingTimeZoneID);
                        results = GetConcatenatedResults(DateTimeHelper.GetXWeeksAgo(startDate, 9), endDate, dataRange, false);

                        if(!results.Any())
                        {
                            //daily
                            dataRange = DataConstants.AnalysisRange.Daily;
                            DateTimeHelper.GenerateDefaultDatesYesterday(out startDate, out endDate, EarliestBuildingTimeZoneID);
                            results = GetConcatenatedResults(DateTimeHelper.GetXDaysAgo(startDate, 9), endDate, dataRange, false);
                        }
                    }

                    return results; 
                }

                protected override void BindDiagnostics(IEnumerable<DiagnosticsResult> results)
                {
                    var hasResults = results.Any();

                    if (hasResults)
                    {
                        BuildTableHeader();
                        BindCharts(results);
                    }
                    else
                    {                        
                        TableRow tr = new TableRow(){ CssClass = "dockEmptyGridLabel" };
                        tr.Cells.Add(new TableCell() { Text = BusinessConstants.Message.DiagnosticsNoPriorites });
                        tblSparks.Rows.Add(tr);                   
                    }

                    litChart.Visible = (hasResults) ? true : false;                    
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion


            public void BindCharts(IEnumerable<DiagnosticsResult> results)
            {
                string current = "prev. ";
                string sum = "sum: ";
                CommissioningDashboardDataStore.PlotRangeEnum commissioning;
              
                switch(dataRange)
                {
                    case DataConstants.AnalysisRange.Monthly:
                        current += "mo.: ";
                        commissioning = CommissioningDashboardDataStore.PlotRangeEnum.Past12Months;
                        break;
                    case DataConstants.AnalysisRange.Weekly:
                        current += "wk.: ";
                        commissioning = CommissioningDashboardDataStore.PlotRangeEnum.Past10Weeks;
                        break;
                    default:
                        current += "day: ";
                        commissioning = CommissioningDashboardDataStore.PlotRangeEnum.Past30Days;
                        break;
                }

                //order by building cost sums
                var bids = results.GroupBy(g => g.BID).Select(g => new { BID = g.Key, CostSum = g.Sum(c => c.CostSavings) }).OrderByDescending(s => s.CostSum).Select(b => b.BID);

                foreach (int bid in bids)
                {
                    TableRow tr = new TableRow();

                    string buildingName = results.Where(b => b.BID == bid).First().BuildingName;
                    string buildingNameTrimmed = StringHelper.RemoveSpecialCharacters(buildingName);
                    int lcid = results.Where(b => b.BID == bid).First().LCID;
                    bool hasData;

                    var buildingFaultsByRange = results.Where(b => b.BID == bid).GroupBy(b => b.StartDate)
                        .Select(g => new { StartDate = g.Key, Count = g.Count(),
                                           TotalEnergyPriority = g.Where(e => e.EnergyPriority != 0).Count(),
                                           EnergyPriority = g.Where(e => e.EnergyPriority != 0).Any() ? g.Where(e => e.EnergyPriority != 0).Average(e => e.EnergyPriority) : 0,
                                           TotalMaintenancePriority = g.Where(m => m.MaintenancePriority != 0).Count(),
                                           MaintenancePriority = g.Where(m => m.MaintenancePriority != 0).Any() ? g.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                                           TotalComfortPriority = g.Where(c => c.ComfortPriority != 0).Count(),
                                           ComfortPriority = g.Where(c => c.ComfortPriority != 0).Any() ? g.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                                           CostSavings = g.Sum(c => c.CostSavings),
                        }).OrderBy(sd => sd.StartDate).ToList();
                    
                    hasData = buildingFaultsByRange.Any();

                    tr.Cells.Add(new TableCell { Text = buildingName });


                    //Point sets
                    List<DotNet.Highcharts.Options.Point> pointsTotalFaults = new List<DotNet.Highcharts.Options.Point>();
                    List<DotNet.Highcharts.Options.Point> pointsAvoidableCosts = new List<DotNet.Highcharts.Options.Point>();
                    List<DotNet.Highcharts.Options.Point> pointsEnergyTotalIncidents = new List<DotNet.Highcharts.Options.Point>(); 
                    List<DotNet.Highcharts.Options.Point> pointsMaintenanceTotalIncidents = new List<DotNet.Highcharts.Options.Point>();                    
                    List<DotNet.Highcharts.Options.Point> pointsComfortTotalIncidents = new List<DotNet.Highcharts.Options.Point>();

   
                    foreach (var p in buildingFaultsByRange)
                    {
                        var item = new DotNet.Highcharts.Options.Point { };

                        //highcharts will either convert to utc, or convert based on the server timezone. need to set both highcharts to use utc rather then the server timezone, and then pretend our datetimes are already in utc.
                        var mockUTCDateTimeTotalMilliseconds = DotNet.Highcharts.Helpers.Tools.GetTotalMilliseconds(DateTime.SpecifyKind(p.StartDate, DateTimeKind.Utc));
                        
                        //Has max for 1000pts unless overriden or use object array instead
                        //total
                        item = new DotNet.Highcharts.Options.Point { X = mockUTCDateTimeTotalMilliseconds, Y = p.Count };
                        pointsTotalFaults.Add(item);
                        //cost
                        item = new DotNet.Highcharts.Options.Point { X = mockUTCDateTimeTotalMilliseconds, Y = p.CostSavings };
                        pointsAvoidableCosts.Add(item);
                        //energy
                        item = new DotNet.Highcharts.Options.Point { X = mockUTCDateTimeTotalMilliseconds, Y = p.TotalEnergyPriority };
                        pointsEnergyTotalIncidents.Add(item);
                        //maintenance
                        item = new DotNet.Highcharts.Options.Point { X = mockUTCDateTimeTotalMilliseconds, Y = p.TotalMaintenancePriority };
                        pointsMaintenanceTotalIncidents.Add(item);
                        //comfort
                        item = new DotNet.Highcharts.Options.Point { X = mockUTCDateTimeTotalMilliseconds, Y = p.TotalComfortPriority };
                        pointsComfortTotalIncidents.Add(item);
                    }

                    //Total Faults-------
                    Highcharts sparkChart = new SparkChart("spark" + buildingNameTrimmed + "TotalFaults", cultureInfo, 44, 128, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.DarkGray)).sparkChart;
                    var series = new Series() { Name = "" };
                    
                    //object[,] pointArray = new object[buildingFaultsByRange.Count(), 2];
                    //int i = 0;
                    //foreach (var p in buildingFaultsByRange)
                    //{
                    //    pointArray[i, 0] = DotNet.Highstock.Helpers.Tools.GetTotalMilliseconds(p.StartDate);
                    //    pointArray[i, 1] = p.Count;
                    //    i++;
                    //}
                    //Has max for 1000pts unless overriden or use object array instead
                    //DotNet.Highcharts.Helpers.Data d = new DotNet.Highcharts.Helpers.Data(pointArray);

                    //Has max for 1000pts unless overriden or use object array instead
                    series.Data = new DotNet.Highcharts.Helpers.Data(pointsTotalFaults.ToArray());
                    sparkChart.SetSeries(series);

                    tr.Cells.Add(new TableCell { CssClass = "sparkColumn", Text = (hasData ? sparkChart.ChartContainerHtmlString().ToString() : "") });
                    tr.Cells.Add(new TableCell { Text = current + (hasData ? buildingFaultsByRange.Last().Count.ToString() : "") });

                    if(hasData) litChart.Text += sparkChart.ChartScriptHtmlString();   


                    //Avoidable Cost -------
                    sparkChart = new SparkChart("spark" + buildingNameTrimmed + "AvoidableCost", cultureInfo, 44, 128, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.SpruceGreen)).sparkChart;
                    series = new Series() { Name = "" };
                    series.Data = new DotNet.Highcharts.Helpers.Data(pointsAvoidableCosts.ToArray());
                    sparkChart.SetSeries(series);

                    tr.Cells.Add(new TableCell { CssClass = "sparkColumn", Text = (hasData ? sparkChart.ChartContainerHtmlString().ToString() : "") });
                    tr.Cells.Add(new TableCell { Text = current + (hasData ? CultureHelper.FormatCurrencyAsString(lcid, buildingFaultsByRange.Last().CostSavings) : "") + "<br />" + sum + (hasData ? CultureHelper.FormatCurrencyAsString(lcid, buildingFaultsByRange.Sum(c => c.CostSavings)) : "") });

                    if(hasData) litChart.Text += sparkChart.ChartScriptHtmlString();   


                    //Energy Total Incidents -------
                    sparkChart = new SparkChart("spark" + buildingNameTrimmed + "TotalEnergyIncidents", cultureInfo, 44, 128, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.SunflowerYellow)).sparkChart;
                    series = new Series() { Name = "" };
                    series.Data = new DotNet.Highcharts.Helpers.Data(pointsEnergyTotalIncidents.ToArray());
                    sparkChart.SetSeries(series);

                    tr.Cells.Add(new TableCell { CssClass = "sparkColumn", Text = (hasData ? sparkChart.ChartContainerHtmlString().ToString() : "") });
                    tr.Cells.Add(new TableCell { Text = current + (hasData ? buildingFaultsByRange.Last().TotalEnergyPriority.ToString() : "") });

                    if(hasData) litChart.Text += sparkChart.ChartScriptHtmlString();   


                    //Maintenance Total Incidents -------
                    sparkChart = new SparkChart("spark" + buildingNameTrimmed + "TotalMaintenanceIncidents", cultureInfo, 44, 128, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue)).sparkChart;
                    series = new Series() { Name = "" };
                    series.Data = new DotNet.Highcharts.Helpers.Data(pointsMaintenanceTotalIncidents.ToArray());
                    sparkChart.SetSeries(series);

                    tr.Cells.Add(new TableCell { CssClass = "sparkColumn", Text = (hasData ? sparkChart.ChartContainerHtmlString().ToString() : "") });
                    tr.Cells.Add(new TableCell { Text = current + (hasData ? buildingFaultsByRange.Last().TotalMaintenancePriority.ToString() : "") });

                    if(hasData) litChart.Text += sparkChart.ChartScriptHtmlString();   


                    //Comfort Total Incidents -------
                    sparkChart = new SparkChart("spark" + buildingNameTrimmed + "TotalComfortIncidents", cultureInfo, 44, 128, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed)).sparkChart;
                    series = new Series() { Name = "" };
                    series.Data = new DotNet.Highcharts.Helpers.Data(pointsComfortTotalIncidents.ToArray());
                    sparkChart.SetSeries(series);

                    tr.Cells.Add(new TableCell { CssClass = "sparkColumn", Text = (hasData ? sparkChart.ChartContainerHtmlString().ToString() : "") });
                    tr.Cells.Add(new TableCell { Text = current + (hasData ? buildingFaultsByRange.Last().TotalComfortPriority.ToString() : "") });

                    if(hasData) litChart.Text += sparkChart.ChartScriptHtmlString();   


                    //Add View link----
                    HtmlAnchor a = new HtmlAnchor{ HRef = "/CommissioningDashboard.aspx?bid=" + bid + "&int=" +  dataRange + "&rng=" +  commissioning.ToString(), InnerText = "view", Target = "_blank" };
                    a.Attributes.Add("class", "dockLink");
                    TableCell t = new TableCell();
                    t.Controls.Add(a);
                    tr.Cells.Add(t);

                    //Add Row----
                    tblSparks.Rows.Add(tr);
                }
            }

            private void BuildTableHeader()
            {
                TableHeaderRow thr = new TableHeaderRow();
                
                thr.Cells.Add(new TableHeaderCell { Text = "Building", ColumnSpan = 1 });
                thr.Cells.Add(new TableHeaderCell { Text = "Total Faults", ColumnSpan = 2 });
                thr.Cells.Add(new TableHeaderCell { Text = "Avoidable Cost", ColumnSpan = 2 });
                thr.Cells.Add(new TableHeaderCell { Text = "Energy Faults", ColumnSpan = 2 });
                thr.Cells.Add(new TableHeaderCell { Text = "Maintenance Faults", ColumnSpan = 2 });
                thr.Cells.Add(new TableHeaderCell { Text = "Comfort Faults", ColumnSpan = 2 });

                tblSparks.Rows.Add(thr);
            }

        #endregion
    }
}