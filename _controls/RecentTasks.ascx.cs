﻿using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    public partial class RecentTasks : SiteUserControl
    {
        #region fields

            int openStatusId = Convert.ToInt32(BusinessConstants.Status.Statuses.Open);
            int inProcessStatusId = Convert.ToInt32(BusinessConstants.Status.Statuses.InProcess);

        #endregion

        #region events

            #region grid events

                protected void gridRecentTasks_RowDataBound(object sender, GridViewRowEventArgs e)
                {
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        var hlTask = (HyperLink)e.Row.FindControl($"lnk{typeof(TaskRecord).Name}");
                        var task = (GetTasks)e.Row.DataItem;

                        hlTask.NavigateUrl = LinkHelper.BuildTaskQuickLinkForSingleTask(task);
                        hlTask.Target = "_blank";
                        hlTask.Text = "view";
                        hlTask.CssClass = "dockLink";
                        hlTask.ToolTip = "View Task";
                    }
                }

            #endregion

        #endregion

        #region methods

            public void LoadModule() { if (siteUser.Modules.Any(s => s.ModuleID == (int)BusinessConstants.Module.Modules.Tasks)) BindRecentOpenTasks(); }

            void BindRecentOpenTasks()
            {
                try
                {
                    SetGridAttributes();
                    SetColumnAttributes();

                    var query = new TaskRecordQuery(DataMgr.ConnectionString, siteUser).LoadWith(_ => _.User1).LoadWith<Analyses_Equipment>(_ => _.Analyses_Equipment, _ => _.Equipment).FinalizeLoadWith.GetAll(siteUser.CID);
                    var bids = siteUser.VisibleBuildings.Select(_ => _.BID).ToArray();

                    if (!siteUser.IsKGSFullAdminOrHigher)
                    {
                        if (!bids.Any())
                            query.ByBuilding(0);
                        else
                            query.ByBuildings(bids);
                    }
                    else
                        query.ByBuildings(bids);

                    var results = query.ToEnumerable();

                    SetTaskMessages(results);

                    gridRecentTasks.DataSource = GetTransformTaskData(results.Where(_ => _.StatusID == openStatusId || _.StatusID == inProcessStatusId).OrderByDescending(t => t.DateCreated).Take(10));
                    gridRecentTasks.DataBind();
                }
                catch (Exception ex) 
                {
                    litError.Text = "An error has occurred.  Please contact Administrator";
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error getting recent open tasks for homepage", ex); 
                }
            }

            IEnumerable<GetTasks> GetTransformTaskData(IEnumerable<TaskRecord> tasks)
            {
                var getTasks = new List<GetTasks>();

                foreach (var task in tasks)
                {
                    var getTask = new GetTasks();

                    getTask.TaskID = task.TaskID;
                    getTask.Bid = task.Analyses_Equipment.Equipment.BID;
                    getTask.Eid = task.Analyses_Equipment.EID;
                    getTask.Aid = task.Analyses_Equipment.AID;
                    getTask.Cid = task.CID;
                    getTask.AnalysisStartDate = task.AnalysisStartDate;
                    getTask.Summary = task.Summary;
                    getTask.EquipmentName = task.Analyses_Equipment.Equipment.EquipmentName;
                    getTask.Email = (task.User1 != null) ? task.User1.Email : "No Assignee";
                    
                    getTasks.Add(getTask);
                }

                return getTasks.AsEnumerable();
            }

            void SetTaskMessages(IEnumerable<TaskRecord> taskRecords)
            {
                if (taskRecords.Any())
                {
                    var messages = new StringBuilder();
                    var endDateCreated = DateTime.UtcNow;
                    var startDateCreated = endDateCreated.AddDays(-30);
                    var completedStatusId = Convert.ToInt32(BusinessConstants.Status.Statuses.Completed);

                    messages.AppendLine("<ul>");
                    messages.AppendLine($"<li><span>Total Number of Open Tasks: {taskRecords.Count(_ => _.StatusID == openStatusId)}</span></li>");
                    messages.AppendLine($"<li><span>Task Completed in the last 30 days: {taskRecords.Count(_ => _.StatusID == completedStatusId && _.DateCompleted >= startDateCreated && _.DateCompleted <= endDateCreated)}</span></li>");
                    messages.AppendLine("</ul>");

                    litTaskMessages.Text = messages.ToString();
                }
            }

            void SetGridAttributes()
            {
                gridRecentTasks.DataKeyNames = new string[] { PropHelper.G<TaskRecord>(_ => _.TaskID) };
                gridRecentTasks.GridLines = GridLines.None;
                gridRecentTasks.AllowPaging = gridRecentTasks.AllowSorting = gridRecentTasks.AutoGenerateColumns = false;
                gridRecentTasks.RowDataBound += gridRecentTasks_RowDataBound;
                gridRecentTasks.EmptyDataText = "No recent open tasks exist.";
                gridRecentTasks.HeaderStyle.CssClass = "tblTitle";
                gridRecentTasks.RowStyle.CssClass = "tblCol1";
                gridRecentTasks.RowStyle.Wrap = true;
                gridRecentTasks.AlternatingRowStyle.CssClass = "tblCol2";
                gridRecentTasks.EmptyDataRowStyle.CssClass = "dockEmptyGridLabel";
            }

            void SetColumnAttributes()
            {
                var dictColumns = new Dictionary<int, string>();

                dictColumns.Add(0, PropHelper.G<TaskRecord>(_ => _.Summary));
                dictColumns.Add(1, typeof(Equipment).Name);
                dictColumns.Add(2, "Assignee");
                dictColumns.Add(3, string.Empty);

                for (var i = 0; i < gridRecentTasks.Columns.Count; i++)
                {
                    gridRecentTasks.Columns[i].HeaderText = dictColumns[i];
                    gridRecentTasks.Columns[i].ItemStyle.Wrap = true;
                }
            }

            protected string TrimCellContents(string value, int maxLength) { return StringHelper.TrimText(value, maxLength); }

        #endregion    
    }
}