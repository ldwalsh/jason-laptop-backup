﻿using CW.Common.Constants;
using CW.Website._framework;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Factories;
using CW.Website._services.PerformanceDashboard.Interfaces;
using CW.Website._services.PerformanceDashboard.Models;
using CW.Website._services.SearchCriteria.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;

namespace CW.Website._controls.performance
{
    public partial class SEUDropdownPanel : SiteUserControl
    {
        private SearchCriteriaUIModel mFilterData => mSearchCriteria.GetSearchCriteriaModel();
        private SearchCriteria mSearchCriteria => ((SignificantEnergyUsers)Parent).SearchCriteria;
        private WidgetCriteriaUIModel _uiModel;
        private IByAreaDDLService _byAreaDDLSvc;        

        protected void Page_FirstLoad(object sender, EventArgs e)
        {
            configureDropdowns();            
        }

        protected void Page_Load(object sender, EventArgs e)
        {            
            RefreshDisplayTypeDDL();
        }

        private void configureDropdowns()
        {
            RefreshDisplayTypeDDL();

            _uiModel = GetWidgetCriteriaModel();

            //wire up analysis types dropdown
            var analysisTypesDDLSvc = new AnalysisTypesDDLServiceFactory(_uiModel).GetAnalysisTypesDDLService();

            analysisTypesDDLSvc.WireUpAnalysisTypes();

            //wire up by area dropdown
            _byAreaDDLSvc = new ByAreaDDLServiceFactory(_uiModel).GetByAreaDDLService();

            _byAreaDDLSvc.WireUpByArea();
        }

        private void RefreshDisplayTypeDDL()
        {
            var uiModel = GetWidgetCriteriaModel();
            
            var displayTypesDDLSvc = new DisplayTypesDDLServiceFactory(uiModel).GetDisplayTypeDDLService();            
            
            IDictionary<string, string> displayTypes = getDisplayTypeItems(false, false);

            displayTypesDDLSvc.WireUpDisplayTypes(displayTypes);

            SetClientSEUVariables(displayTypes);

        }

        /// <summary>
        /// Sets the client side ASP literal with a script containing dictionary lookups
        /// of display types for different analysis types.
        /// </summary>
        /// <param name="model">UI Model</param>
        /// <param name="displayTypes">Display types</param>
        private void SetClientSEUVariables(IDictionary<string, string> displayTypes)
        {
            if (displayTypes == null) return;

            StringBuilder divText = new StringBuilder();

            divText.Append("<script type = 'text/javascript'>");

            string baselineIDs = JsonConvert.SerializeObject(BusinessConstants.Dropdowns.DisplayType.BaselineTypes);

            string goalIDs = JsonConvert.SerializeObject(BusinessConstants.Dropdowns.DisplayType.GoalTypes);

            string regularDisplayTypes = JsonConvert.SerializeObject(getDisplayTypeItems(false, false));

            string baselineDisplayTypes = JsonConvert.SerializeObject(getDisplayTypeItems(true, false));

            string goalDisplayTypes = JsonConvert.SerializeObject(getDisplayTypeItems(true, true));

            string byAreaAnalysisTypes = JsonConvert.SerializeObject(BusinessConstants.Dropdowns.ByArea.ShouldDisplayByArea);

            divText.Append("\nvar baselineDisplayIDs = " + baselineIDs);

            divText.Append("\nvar goalDisplayIDs = " + goalIDs);

            divText.Append("\nvar regularDisplayTypes = " + regularDisplayTypes);

            divText.Append("\nvar baselineDisplayTypes = " + baselineDisplayTypes);

            divText.Append("\nvar goalDisplayTypes = " + goalDisplayTypes);

            divText.Append("\nvar byAreaAnalysisTypes = " + byAreaAnalysisTypes);

            divText.Append("\nKGS.RefreshSEUDisplayDDL();");

            divText.Append("</script>");

            this.seuDisplayTypesDiv.InnerHtml = divText.ToString();

            this.seuDisplayTypesDiv.Attributes.Add("style", "display:none");
        }

        /// <summary>
        /// Convenience method to get analysis types from preset data
        /// </summary>
        /// <returns>Preset analysis types dictionary</returns>
        IDictionary<string, string> getDisplayTypeItems(bool isBaseline, bool isGoal)
        {
            var items = new Dictionary<string, string>();                        

            items.Add("Bar Chart", BusinessConstants.Dropdowns.DisplayType.BarChart);

            if (isBaseline)
            {
                items.Add("BaseLine Comparison", BusinessConstants.Dropdowns.DisplayType.BaseLineComparison);
            }

            if (isGoal)
            {
                items.Add("Goal Comparison", BusinessConstants.Dropdowns.DisplayType.GoalComparison);
            }

            items.Add("Table", BusinessConstants.Dropdowns.DisplayType.Table);

            return items;
        }

        //Yes this is being called twice for the two different ddls that need this model, but that's okay for now
        SEUChartCriteriaUIModel GetWidgetCriteriaModel()
        {
            SearchCriteriaUIModel searchModel = mSearchCriteria == null ? null : mSearchCriteria.GetSearchCriteriaModel();

            var model = new SEUChartCriteriaUIModel(searchModel);

            model.DisplayModeDDL = ddlDisplayMode;

            model.AnalysisTypesDDL = ddlAnalysisType;

            model.AreaDDL = ddlByArea;                  

            return model;
        }

        public DropdownPanelDataModel GetDropdownFields()
        {
            DropdownPanelDataModel dropdownFields = new DropdownPanelDataModel();

            //only set fields for dropdowns that are visible, so queries using this 
            //model don't filter on hidden dropdown fields            
            dropdownFields.AnalysisType = ddlAnalysisType.SelectedValue;
            dropdownFields.DisplayType = ddlDisplayMode.SelectedValue;

            if (ddlByArea.Visible)
            {
                dropdownFields.IsByArea = ddlByArea.SelectedValue == BusinessConstants.Dropdowns.ByArea.Area;
            }

            return dropdownFields;
        }

        public ImageButton GetRefreshButton()
        {
            return btnSEURefresh;
        }
    }
}