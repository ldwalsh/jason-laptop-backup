﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._controls.performance
{
    public class DropdownPanelDataModel
    {
        public string TimePeriod { get; set; }
        public string AnalysisType { get; set; }
        public string EquipmentClass { get; set; }
        public bool IsByArea { get; set; }
        public string ConstumptionType { get; set; }
        public string BuildingSubmeter { get; set; }
        public string Plant { get; set; }
        public string Equipment { get; set; }
        public bool IsUtility { get; set; }
        public bool IsAvoidable { get; set; }
        public bool IsEfficiency { get; set; }
        public string DisplayType { get; set; }        
    }
}