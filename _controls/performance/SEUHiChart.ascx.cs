﻿using CW.Common.Constants;
using CW.Website._framework;
using CW.Website._services.Chart;
using CW.Website._services.Chart.Factories;
using CW.Website._services.Chart.Models;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.MetricHandler.Interfaces;
using CW.Website._services.MetricHandler.Factories;
using CW.Website._services.PerformanceDashboard.Models;
using DotNet.Highcharts.Enums;
using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace CW.Website._controls.performance
{
    public partial class SEUHiChart : SiteUserControl
    {
        private DropdownPanelDataModel _ddlData => ((SignificantEnergyUsers)Parent).GetDropdownData();

        private SearchCriteria _searchCriteria => ((SignificantEnergyUsers)Parent).SearchCriteria;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void RefreshChart()
        {
            SEUChartCriteriaUIModel uiModel = GetSEUChartUICriteriaModel();

            var units = getUnitsLabel(uiModel);

            var highChartUIModel = new HighChartUIModel()
            {
                Name = this.SEUChartContainer.ID,

                UnitsLabel = units,

                Container = this.SEUChartContainer,

                Script = this.SEUChartScript,

                Type = ChartTypes.Bar,

                CultureInfo = new CultureInfo(siteUser.CultureName),

                CurrencyLCID = int.Parse(uiModel.CurrenciesDDL.SelectedValue),

                DrilldownEvents = "function (e) { KGS.SEUDrilldownClick(e, this); }",

                DrillupEvents = "function(e) { KGS.SEUDrilluupClick(e, this); }"
            };

            var chartFactory = new ChartServiceFactory<SEUChartService>(DataMgr);

            var chartSvc = chartFactory.GetChartService(uiModel, highChartUIModel);

            if (chartSvc != null)
            {
                chartSvc.CreateChart();

                //set the query paramters on the client side so drilldown js can access them 
                setClientSideQueryParams(chartSvc.DataConfigModel);
            }                                     
        }


        /// <summary>
        /// Serializes the query parameters used to create the chart data, "saves"
        /// it as a script in an ASP literal on the page (which is evaluated when
        /// page loads), which pushes the params onto a client-side stack. We need
        /// to do this to save the "breadcrumbs" of queries for when the user
        /// navigates up/down the SEU chart drilldown.
        /// </summary>        
        private void setClientSideQueryParams(MetricCalcServiceConfigModel metricSvcConfig)
        {
            IMetricConfigSerializer metricSerializer = new MetricConfigSerializerFactory().GetSerializer(); 

            string queryParams = metricSerializer.Serialize(metricSvcConfig);

            if (queryParams == null) return;                                        

            StringBuilder divText = new StringBuilder();

            divText.Append("<script type = 'text/javascript'>");

            //push the json-serialized query params onto the client side stack
            divText.Append($"KGS.SEUQueryParamStack.push( { queryParams } )");

            divText.Append("</script>");

            this.SEUClientSideChartQueryParams.InnerHtml = divText.ToString();
        }

        private SEUChartCriteriaUIModel GetSEUChartUICriteriaModel()
        {
            var searchCriteriaModel = _searchCriteria.GetSearchCriteriaModel();

            var model = new SEUChartCriteriaUIModel(searchCriteriaModel);

            model.ddlData = _ddlData;

            return model;
        }

        private string getUnitsLabel(SEUChartCriteriaUIModel model)
        {
            if (model == null) return null;

            switch (model.ddlData.AnalysisType)
            {
                case BusinessConstants.Dropdowns.AnalysisType.AvoidableCosts:
                    return "$";
                default:
                    return "";
            }
        }

    }
}