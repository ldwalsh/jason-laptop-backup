﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EnergyDropDownPanel.ascx.cs" Inherits="CW.Website._controls.EnergyDropDownPanel" %>

<!--common-->
<div class="divDockFormFirstCentered">

  <asp:DropDownList ID="ddlDisplayMode" runat="server" CssClass="dropdown ddlEnergyConsumptionTableToggle" AppendDataBoundItems="true" onchange="KGS.ToggleTable('.ddlEnergyConsumptionTableToggle');" />

  <asp:DropDownList ID="ddlTimePeriod" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDisplayMode_OnSelectedIndexChanged" />

  <asp:DropDownList ID="ddlAnalysisType" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDisplayMode_OnSelectedIndexChanged" AutoPostBack="true" />

  <!--avoidable cost-specific-->
  <span id="spanAvoidable" runat="server">
    <asp:DropDownList ID="ddlEquipmentClass" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDisplayMode_OnSelectedIndexChanged" AutoPostBack="true" />    
  </span>

  <!--utility-specific-->
  <span id="spanUtility" runat="server">

    <asp:DropDownList ID="ddlByArea" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDisplayMode_OnSelectedIndexChanged" />

    <asp:DropDownList ID="ddlConsumptionType" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDisplayMode_OnSelectedIndexChanged" AutoPostBack="true" />

    <asp:DropDownList ID="ddlBuildingSubmeter" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDisplayMode_OnSelectedIndexChanged" AutoPostBack="true" />

  </span>

  <!--efficiency-specific-->
  <span id="spanEfficiency" runat="server">
    <asp:DropDownList ID="ddlPlant" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDisplayMode_OnSelectedIndexChanged" AutoPostBack="true" />    
  </span>

  <asp:DropDownList ID="ddlEquipment" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlDisplayMode_OnSelectedIndexChanged" AutoPostBack="true" />

</div>

<div class="divDockForm">
  <asp:ImageButton ID="btnEnergyReferesh" runat="server" ImageUrl="~/_assets/styles/images/refresh.jpg" CausesValidation="false" />
</div>

<!--table download button-->
<div class="divDockForm tableDownloadDiv tableDownloadDivEnergy">
  <asp:ImageButton runat="server" OnClientClick="KGS.DownloadTable('.energyConsumptionTable'); return false;" CssClass="imgDownload" ImageUrl="/_assets/images/excel-icon.jpg" AlternateText="download" AutoPostBack="false"/>
</div>