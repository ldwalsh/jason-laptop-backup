﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SignificantEnergyUsers.ascx.cs" Inherits="CW.Website._controls.performance.SignificantEnergyUsers" Debug="false"%>
<%@ Register src="~/_controls/performance/SEUDropDownPanel.ascx" tagname="SEUDropDownPanel" tagprefix="CW" %>
<%@ Register src="~/_controls/performance/SEUHiChart.ascx" tagname="SEUHiChart" tagprefix="CW" %>
 
<div class="divDockFormWrapperShortest">
  <CW:SEUDropDownPanel ID="SEUDropDownPanel" runat="server" />
</div>

<hr />

<div class="SEUHighChart">
  <CW:SEUHiChart ID="SEUHiChart" runat="server" />
</div>