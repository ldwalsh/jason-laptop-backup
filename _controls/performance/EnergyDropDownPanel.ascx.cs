﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Website._controls.performance;
using CW.Website._framework;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Factories;
using CW.Website._services.SearchCriteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    //TODO: Luke, this could definitely be made into a service to make your life easier :)
    public partial class EnergyDropDownPanel : SiteUserControl
    {
        #region fields
        private SearchCriteriaUIModel mFilterData => mSearchCriteria.GetSearchCriteriaModel();
        private SearchCriteria mSearchCriteria => ((EnergyConsumption)Parent).SearchCriteria;

        //private bool multipleBuildings = false;
        #endregion

        #region properties
        
        #endregion

        protected void Page_FirstLoad(object sender, EventArgs e)
        {            
            configureStandardDropdowns();
            populateDropdowns(ddlAnalysisType);
            SetDefaultTimePeriod();
            RefreshDropdownVisibility();            
        }

        protected void Page_Load(object sender, EventArgs e)
        {            
            RefreshDropdownVisibility();
        }

        #region events
        protected void ddlDisplayMode_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            populateDropdowns(sender);
            RefreshDropdownVisibility();
        }
        #endregion

        public ImageButton GetRefreshButton()
        {
            return btnEnergyReferesh;
        }

        #region methods

        public DropdownPanelDataModel GetDropdownFields()
        {
            DropdownPanelDataModel dropdownFields = new DropdownPanelDataModel();

            //only set fields for dropdowns that are visible, so queries using this 
            //model don't filter on hidden dropdown fields
            dropdownFields.TimePeriod = ddlTimePeriod.SelectedValue;
            dropdownFields.AnalysisType = ddlAnalysisType.SelectedValue;

            if (spanAvoidable.Visible)
            {
                dropdownFields.EquipmentClass = ddlEquipmentClass.SelectedValue;
            }
            if (spanUtility.Visible)
            {
                dropdownFields.IsByArea = ddlByArea.SelectedValue == BusinessConstants.Dropdowns.ByArea.Area;
                dropdownFields.ConstumptionType = ddlConsumptionType.SelectedValue;
                dropdownFields.BuildingSubmeter = ddlBuildingSubmeter.SelectedValue;
            }
            if (spanEfficiency.Visible)
            {
                dropdownFields.Plant = ddlPlant.SelectedValue;
            }
            if (ddlEquipment.Visible)
            {
                dropdownFields.Equipment = ddlEquipment.SelectedValue;
            }
            dropdownFields.IsUtility = spanUtility.Visible;
            dropdownFields.IsAvoidable = spanAvoidable.Visible;
            dropdownFields.IsEfficiency = spanEfficiency.Visible;

            return dropdownFields;
        }

        /// <summary>
        /// Toggles dropdown menu visibility based on which analysis type is selected
        /// by the user.
        /// </summary>
        /// <param name="energyUpdateData">Model containing data about what's selected on the page</param>
        public void RefreshDropdownVisibility()
        {
            var bids = mFilterData.BuildingsLBE.Items.OfType<ListItem>().Where(i => i.Value != "0").Select(i => int.Parse(i.Value)).ToArray();

            //show/hide spans based on analysis type
            spanAvoidable.Visible = ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.AvoidableCosts;
            spanUtility.Visible = ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Trees ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Cars ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Cost ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Energy ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Carbon;
            spanEfficiency.Visible = ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.CoolingEfficiency ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.HeatingEfficiency ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.ProcessWaterEfficiency ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.VentilationEfficiency;

            //consumption type dropdown
            ddlConsumptionType.Visible = spanUtility.Visible;

            //submeter specific dropdown
            ddlBuildingSubmeter.Visible = ddlConsumptionType.SelectedValue != BusinessConstants.Dropdowns.DefaultAllIndex && !ddlConsumptionType.SelectedValue.IsNullOrEmpty() &&
                bids.Length == 1;

            //plant system dropdown
            ddlPlant.Visible = ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.CoolingEfficiency ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.HeatingEfficiency ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.ProcessWaterEfficiency;

            //equipment
            ddlEquipment.Visible =                 
                (
                    spanEfficiency.Visible && 
                    (
                        (!ddlPlant.SelectedValue.IsNullOrEmpty() && ddlPlant.SelectedValue != BusinessConstants.Dropdowns.DefaultAllIndex) || 
                        ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.VentilationEfficiency
                    )
                ) ||
                (spanAvoidable.Visible && ddlEquipmentClass.SelectedValue != BusinessConstants.Dropdowns.DefaultAllIndex);
        }

        public void SetDefaultTimePeriod()
        {
            var months = int.Parse(mFilterData.RangesDDL.SelectedValue);

            if (months >= 12)
            {
                ddlTimePeriod.SelectedValue = BusinessConstants.Dropdowns.TimePeriod.Monthly;

                return;
            }

            if (months >= 3)
            {
                ddlTimePeriod.SelectedValue = BusinessConstants.Dropdowns.TimePeriod.Weekly;

                return;
            }
            
            ddlTimePeriod.SelectedValue = BusinessConstants.Dropdowns.TimePeriod.Daily;            
        }

        /// <summary>
        /// Wires up standard dropdowns, i.e. dropdowns whose data are always known
        /// when control is created beforehand and do not require data queries.
        /// </summary>
        private void configureStandardDropdowns()
        {
            configureDDLDisplayMode();
            configureDDLTimePeriod();
            configureDDLAnalysisType();
            configureDDLByArea();            
        }

        private void configureDDLDisplayMode()
        {
            ddlDisplayMode.Items.Add(new ListItem("Line Chart", BusinessConstants.Dropdowns.DisplayType.LineChart));
            ddlDisplayMode.Items.Add(new ListItem("Table", BusinessConstants.Dropdowns.DisplayType.Table));
        }

        private void configureDDLTimePeriod()
        {
            ddlTimePeriod.Items.Add(new ListItem("Daily", BusinessConstants.Dropdowns.TimePeriod.Daily));
            ddlTimePeriod.Items.Add(new ListItem("Weekly", BusinessConstants.Dropdowns.TimePeriod.Weekly));
            ddlTimePeriod.Items.Add(new ListItem("Monthly", BusinessConstants.Dropdowns.TimePeriod.Monthly));
        }

        private void configureDDLAnalysisType()
        {
            var model = GetWidgetCriteriaModel();

            var analysisTypesSvc = new AnalysisTypesDDLServiceFactory(model).GetAnalysisTypesDDLService();

            analysisTypesSvc.WireUpAnalysisTypes();
        }

        private void configureDDLByArea()
        {
            ddlByArea.Items.Add(new ListItem("-----", BusinessConstants.Dropdowns.ByArea.None));
            ddlByArea.Items.Add(new ListItem("By Area", BusinessConstants.Dropdowns.ByArea.Area));
        }        

        /// <summary>
        /// Populates dropdowns that were previously "hidden", after user
        /// makes selection on a different dropdown.
        /// </summary>
        /// <param name="sender"></param>
        private void populateDropdowns(object sender)
        {
            DropDownList ddl = (DropDownList)sender;

            var bids = mFilterData.BuildingsLBE.Items.OfType<ListItem>().Where(i => i.Value != "0").Select(i => int.Parse(i.Value)).ToArray();

            //analysis type: avoidable cost
            if (ddl == ddlAnalysisType && ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.AvoidableCosts)
            {
                populateEquipmentClassDropdown();
            }
            //analysis type: carbon/energy
            else if (ddl == ddlAnalysisType && (ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Carbon ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Cars ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Cost ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Energy ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Trees))
            {
                populateConsumptionTypeDropdown();
            }
            //analysis type: efficiency (not ventilation)
            else if (ddl == ddlAnalysisType && (ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.CoolingEfficiency ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.HeatingEfficiency ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.ProcessWaterEfficiency))
            {
                populatePlantDropdown();
            }
            //analysis type: ventilation efficiency
            else if (ddl == ddlAnalysisType && ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.VentilationEfficiency)
            {
                populateEquipmentDropdown(ddl);
            }
            //consumption type
            else if (ddl == ddlConsumptionType && bids.Length == 1 && ddlConsumptionType.SelectedValue != BusinessConstants.Dropdowns.DefaultAllIndex)
            {
                populateSubMeterDropdown();
            }
            //plant
            else if (ddl == ddlPlant)
            {
                populateEquipmentDropdown(ddl);
            }
            //equipment class
            else if (ddl == ddlEquipmentClass && ddlEquipmentClass.SelectedValue != BusinessConstants.Dropdowns.DefaultAllIndex)
            {
                populateEquipmentDropdown(ddl);
            }
        }

        /// <summary>
        /// Populates the equipment class dropdown.
        /// </summary>
        private void populateEquipmentClassDropdown()
        {
            ddlEquipmentClass.Items.Clear();

            //set default value
            ddlEquipmentClass.Items.Add(new ListItem("All Equipment Classes", BusinessConstants.Dropdowns.DefaultAllIndex));

            var bids = mFilterData.BuildingsLBE.Items.OfType<ListItem>().Where(i => i.Value != "0").Select(i => int.Parse(i.Value)).ToArray();

            //bind equipment classes available for set of selected buildings
            IEnumerable<EquipmentClass> eqClasses = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingIDs(bids);
            ddlEquipmentClass.DataTextField = PropHelper.G<EquipmentClass>(_ => _.EquipmentClassName);
            ddlEquipmentClass.DataValueField = PropHelper.G<EquipmentClass>(_ => _.EquipmentClassID);
            ddlEquipmentClass.DataSource = eqClasses;

            ddlEquipmentClass.DataBind();
        }

        private void populateConsumptionTypeDropdown()
        {
            ddlConsumptionType.Items.Clear();

            var model = GetWidgetCriteriaModel();

            var utilitiesSvc = new UtiltiesDDLServiceFactory(DataMgr, model).GetUtilitiesDDLService();

            utilitiesSvc.WireUpUtilities();
        }

        private void populateSubMeterDropdown()
        {
            //TODO: add filter on consumption type
            var bids = mFilterData.BuildingsLBE.Items.OfType<ListItem>().Where(i => i.Value != "0").Select(i => int.Parse(i.Value)).ToArray();

            if (bids == null) return;
            
            ddlBuildingSubmeter.Items.Clear();

            //set default value
            ddlBuildingSubmeter.Items.Add(new ListItem("All Submeters", BusinessConstants.Dropdowns.DefaultAllIndex));

            var submeters = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentClassID
            (
                bids.FirstOrDefault(), 
                BusinessConstants.EquipmentClass.UtilityClassID
            );

            ddlBuildingSubmeter.DataTextField = PropHelper.G<Equipment>(_ => _.EquipmentName);
            ddlBuildingSubmeter.DataValueField = PropHelper.G<Equipment>(_ => _.EID);
            ddlBuildingSubmeter.DataSource = submeters;

            ddlBuildingSubmeter.DataBind();
        }

        private void populateEquipmentDropdown(DropDownList ddl)
        {
            ddlEquipment.Items.Clear();
            IEnumerable<Equipment> equipment = null;

            var bids = mFilterData.BuildingsLBE.Items.OfType<ListItem>().Where(i => i.Value != "0").Select(i => int.Parse(i.Value)).ToArray();

            if (ddl == ddlEquipmentClass)
            {
                //set default value
                ddlEquipment.Items.Add(new ListItem("All " + ddlEquipmentClass.SelectedItem + "s", BusinessConstants.Dropdowns.DefaultAllIndex));
                
                //get list of equipment
                equipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByEquipmentClassAndBuildingIDS(bids, int.Parse(ddlEquipmentClass.SelectedValue));
            }
            else if(ddl == ddlPlant)
            {
                //set default value
                ddlEquipment.Items.Add(new ListItem("All", BusinessConstants.Dropdowns.DefaultAllIndex));

                equipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBIDSAndEquipmentTypeID(bids, int.Parse(ddlPlant.SelectedValue));
            }
            else if (ddl == ddlAnalysisType)
            {
                //set default value
                ddlEquipment.Items.Add(new ListItem("All Air Handlers", BusinessConstants.Dropdowns.DefaultAllIndex));

                equipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBIDSAndEquipmentTypeID(bids, int.Parse(BusinessConstants.Dropdowns.AnalysisType.VentilationEfficiency));
            }

            ddlEquipment.DataTextField = PropHelper.G<Equipment>(_ => _.EquipmentName);
            ddlEquipment.DataValueField = PropHelper.G<Equipment>(_ => _.EID);
            ddlEquipment.DataSource = equipment;

            ddlEquipment.DataBind();
        }

        private void populatePlantDropdown()
        {
            string[] validPlantIDs = new string[] {BusinessConstants.Dropdowns.AnalysisType.CoolingEfficiency,
                BusinessConstants.Dropdowns.AnalysisType.HeatingEfficiency , BusinessConstants.Dropdowns.AnalysisType.ProcessWaterEfficiency };

            if (!validPlantIDs.Contains(ddlAnalysisType.SelectedValue))
            {
                return;
            }            

            string defaultName = getPlantDefaultName();
            int equipmentClass;

            ddlPlant.Items.Clear();
            ddlPlant.Items.Add(new ListItem(defaultName, BusinessConstants.Dropdowns.DefaultAllIndex));

            var bids = mFilterData.BuildingsLBE.Items.OfType<ListItem>().Where(i => i.Value != "0").Select(i => int.Parse(i.Value)).ToArray();

            if (int.TryParse(ddlAnalysisType.SelectedValue, out equipmentClass))
            {
                IEnumerable<EquipmentType> plants = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByBuildingIDsAndEquipmentTypeID(bids, equipmentClass);

                ddlPlant.DataTextField = PropHelper.G<EquipmentType>(_ => _.EquipmentTypeName);
                ddlPlant.DataValueField = PropHelper.G<EquipmentType>(_ => _.EquipmentTypeID);
                ddlPlant.DataSource = plants;

                ddlPlant.DataBind();
            }
        }

        private string getPlantDefaultName()
        {
            string analysisType = ddlAnalysisType.SelectedItem.Text;
            if (analysisType.IsNullOrEmpty() || analysisType.Length < 11) 
            {
                return "All Plants";
            }

            return "All " + analysisType.Substring(0, analysisType.Length - 10) + " Plants";
        }

            //Yes this is being called twice for the two different ddls that need this model, but that's okay for now
            WidgetCriteriaUIModel GetWidgetCriteriaModel()
            {
                SearchCriteriaUIModel searchModel = mSearchCriteria.GetSearchCriteriaModel();

                var model = new WidgetCriteriaUIModel(searchModel);                         

                model.AnalysisTypesDDL = ddlAnalysisType;

                model.UtilitiesDDL = ddlConsumptionType;
            
                return model;
            }

        #endregion
    }
}