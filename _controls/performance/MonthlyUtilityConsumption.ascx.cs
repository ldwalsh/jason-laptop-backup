﻿using CW.Common.Constants;
using CW.Website._framework;
using CW.Website._services.Chart;
using CW.Website._services.Chart.Factories;
using CW.Website._services.Chart.Models;
using CW.Website._services.UtilityConsumption.Factories;
using CW.Website._services.UtilityConsumption.Models;
using DotNet.Highcharts.Enums;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;

namespace CW.Website._controls.performance
{
    /// <summary>
    /// Code behind class for the Monthly Utility Consumption control
    /// </summary>
    public partial class MonthlyUtilityConsumption : SiteUserControl
    {
        #region field(s)

            UtilityConsumptionCriteriaUIModel uiModel;

        #endregion

        #region properties

            public SearchCriteria SearchCriteria { get; set; }

        #endregion

        #region event(s)

            void Page_FirstLoad()
            {
                uiModel = GeUtilityConsumptionCriteriaUIModel();

                var utilityConsumptionUISvc = new UtilityConsumptionUIServiceFactory(DataMgr, uiModel);

                utilityConsumptionUISvc.GetUtilitiesConsumptionUIService().WireUpUI();

                WireUpChart();

                SetHiddenFieldValues(); //this may be able to be moved into the service layer above
            }

            void Page_Load()
            {
                if (Page.IsPostBack)
                {
                    var id = ddlAnalysisTypes.SelectedValue;

                    var item = hdnIdsToHideFilter.Value.Split(',').SingleOrDefault(v => v == id);

                    var type = (item == null) ? "block" : "none";

                    divSubFilters.Style.Add("display", type);

                    var eventTarget = ((SitePage)Page).EventTarget;

                    if (eventTarget == null) return;

                    if (eventTarget.Contains("btnGenerate")) WireUpChart();
                }
            }

            #region button event(s)

                protected void btnGenerate_Click(object sender, ImageClickEventArgs e) => WireUpChart();
                
            #endregion

        #endregion

        #region method(s)

            UtilityConsumptionCriteriaUIModel GeUtilityConsumptionCriteriaUIModel()
            {
                var searchCriteriaModel = SearchCriteria.GetSearchCriteriaModel();

                var model = new UtilityConsumptionCriteriaUIModel(searchCriteriaModel);

                model.AnalysisTypesDDL = ddlAnalysisTypes;

                model.AreaCHK = chkArea;

                model.WeatherCHK = chkWeather;

                model.UtilitiesDDL = ddlUtilities;

                model.TotalConsumption = spanTotalConsumption;

                model.PeakMonthConsumption = spanPeakMonthConsumption;

                model.GreatestBuildingConsumption = spanGreatestBuildingConsumption;

                return model;
            }

            void WireUpChart()
            {
                if (uiModel == null) uiModel = GeUtilityConsumptionCriteriaUIModel();

                var highChartUIModel = new HighChartUIModel
                {
                    Name = monthlyConsumptionChartContainer.ID,

                    Container = monthlyConsumptionChartContainer,

                    Script = monthlyConsumptionChartScript,

                    Type = ChartTypes.Column,

                    CultureInfo = new CultureInfo(siteUser.CultureName),

                    CurrencyLCID = int.Parse(uiModel.CurrenciesDDL.SelectedValue)
                };

                var chartFactory = new ChartServiceFactory<UtilityConsumptionChartService>(DataMgr);
            
                var chartSvc = chartFactory.GetChartService(uiModel, highChartUIModel);

                if (chartSvc != null)
                {
                    chartSvc.CreateChart();

                    consumptionChartDiv.Attributes.Add("style", "display:block");
                }
            }

            void SetHiddenFieldValues()
            {
                var value = new StringBuilder();

                value.Append($"{BusinessConstants.Dropdowns.AnalysisType.AvoidableCosts},");

                value.Append($"{BusinessConstants.Dropdowns.AnalysisType.CoolingEfficiency},");

                value.Append($"{BusinessConstants.Dropdowns.AnalysisType.HeatingEfficiency},");

                value.Append($"{BusinessConstants.Dropdowns.AnalysisType.ProcessWaterEfficiency},");

                value.Append($"{BusinessConstants.Dropdowns.AnalysisType.VentilationEfficiency}");

                hdnIdsToHideFilter.Value = value.ToString();
            }

            //TODO: figure out how to get this working in the service layer
            //void BindConsumptionStatistics(IEnumerable<UtilityColumnBarDataModel> data)
            //{
            //    if (data == null) return;

            //    spanTotalConsumption.InnerText = SetMessage(data.Sum(v => v.Value));

            //    var maxMonth = data.GroupBy(d => d.FactorOfTime).Select(group =>
            //    new UtilityColumnBarDataModel
            //    {
            //        FactorOfTime = group.First().FactorOfTime,
            //        Value = group.Sum(v => v.Value)
            //    }).OrderByDescending(v => v.Value).First();

            //    spanPeakMonthConsumption.InnerText = SetMessage(maxMonth.Value, maxMonth.FactorOfTime);

            //    var maxBuilding = data.GroupBy(d => d.Name).Select(group =>
            //    new UtilityColumnBarDataModel
            //    {
            //        Name = group.First().Name,
            //        Value = group.Sum(v => v.Value)
            //    })
            //    .OrderByDescending(v => v.Value).First();

            //    spanGreatestBuildingConsumption.InnerText = SetMessage(maxBuilding.Value, maxBuilding.Name);
            //}

            //string SetMessage(decimal val, string val2 = null)
            //{
            //    var formattedVal = CultureHelper.FormatNumber(LCID, val, IsByArea);

            //    return (val2 != null) ? $"{formattedVal} ({val2})" : formattedVal;
            //}

        #endregion
    }
}