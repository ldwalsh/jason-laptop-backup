﻿using CW.Website._framework;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    public partial class EnergyConsumptionHiChart : SiteUserControl
    {
        #region properties

            public Literal GetChartContainer() => energyConsumptionChartContainer;
        
            public Literal GetChartScript() => energyConsumptionChartScript;

        #endregion

        #region event(s)

            //preset the no results text on first load of page (maintained in page viewstate)
            void Page_FirstLoad() => lblNoResults.Text = "No data available";
            
        #endregion

        #region method(s)

            public void SetChartVisibility(bool chartHasData)
            {
                var hiChartDisplay = chartHasData ? "display:block" : "display:none";

                energyConsumptionChartDiv.Attributes.Add("style", hiChartDisplay);

                lblNoResults.Visible = !chartHasData;
            }

        #endregion
    }
}