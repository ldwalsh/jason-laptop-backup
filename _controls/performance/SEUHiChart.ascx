﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SEUHiChart.ascx.cs" Inherits="CW.Website._controls.performance.SEUHiChart" %>

<div id="SEUChartDiv" runat="server" class="SEUChartDiv">    
  <asp:Literal ID="SEUChartContainer" runat="server" Mode="PassThrough" />
</div>

<div id="SEUTableDiv" runat="server" class="SEUTableDiv scrollableDiv tableDiv richText" style="display:none;">
  <table class="SEUTable tablesorter" border="1">        
    <tbody class="tbody"></tbody>
  </table> 
</div>

<div id="SEUChartScriptDiv">
  <asp:Literal ID="SEUChartScript" runat="server" Mode="PassThrough" ClientIDMode="Static" />
</div>

<asp:Literal ID="SEUTableScript" runat="server" Mode="PassThrough" />

<asp:Label ID="lblNoResults" runat="server" CssClass="dockMessageNoResultsLabel" />

<div id="SEUClientSideChartQueryParams" runat="server" class="SEUClientSideChartQueryParams" style="display:none;"></div>