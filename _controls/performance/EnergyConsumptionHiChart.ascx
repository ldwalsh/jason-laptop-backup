﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EnergyConsumptionHiChart.ascx.cs" Inherits="CW.Website._controls.EnergyConsumptionHiChart" Debug="false"%>

<div id="energyConsumptionChartDiv" runat="server" class="energyConsumptionChartDiv">
  <asp:Literal ID="energyConsumptionChartContainer" runat="server" Mode="PassThrough" />
</div>

<div id="energyConsumptionTableDiv" runat="server" class="energyConsumptionTableDiv scrollableDiv tableDiv richText" style="display:none;">

  <table class="energyConsumptionTable tablesorter" border="1">        
    <tbody class="tbody"></tbody>
  </table> 

</div>

<div id="energyConsumptionChartScriptDiv">
  <asp:Literal ID="energyConsumptionChartScript" runat="server" Mode="PassThrough" ClientIDMode="Static" />
</div>

<asp:Literal ID="energyConsumptionTableScript" runat="server" Mode="PassThrough" />

<asp:Label ID="lblNoResults" runat="server" CssClass="dockMessageNoResultsLabel" />