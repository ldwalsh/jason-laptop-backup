﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EnergyConsumption.ascx.cs" Inherits="CW.Website._controls.EnergyConsumption"%>
<%@ Register src="~/_controls/performance/EnergyDropDownPanel.ascx" tagname="EnergyDropDownPanel" tagprefix="CW" %>
<%@ Register src="~/_controls/performance/EnergyConsumptionHiChart.ascx" tagname="EnergyConsumptionHiChart" tagprefix="CW" %>

<div class="divDockFormWrapperShortest">
    <CW:EnergyDropDownPanel ID="EnergyDropDownPanel" runat="server" />                  
</div>

<hr />

<div class="energyConsumptionHighChart">
  <CW:EnergyConsumptionHiChart ID="EnergyConsumptionHiChart" runat="server" />
</div>