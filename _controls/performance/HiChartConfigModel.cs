﻿using CW.Website._services.SearchCriteria.Models;

namespace CW.Website._controls.performance
{
    public class EnergyChartConfigModel : SearchCriteriaUIModel
    {
        public DropdownPanelDataModel ddlPanelData { get; set; }
    }
}