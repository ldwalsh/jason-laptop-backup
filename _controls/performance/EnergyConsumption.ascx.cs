﻿using CW.Website._controls.performance;
using CW.Website._framework;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    public partial class EnergyConsumption : SiteUserControl
    {
        #region field(s)

          EnergyConsumptionViewManager _viewManager;

        #endregion

        #region properties

            ImageButton _btnRefresh => EnergyDropDownPanel.GetRefreshButton();

            public EnergyConsumptionViewManager ViewManager =>_viewManager = _viewManager ?? new EnergyConsumptionViewManager
            (
                EnergyDropDownPanel, 
                EnergyConsumptionHiChart, 
                DataMgr, 
                siteUser, 
                SearchCriteria
            );

            public SearchCriteria SearchCriteria { get; set; }

        #endregion

        #region event(s)

            void Page_Load()
            {
                _btnRefresh.Click += refreshChart;

                //we have to manually call the dropdown refresh click otherwise the javascript to 
                //render the hichart won't be fired - temp fix/inefficient b/c re-creating chart on each ddl change
                var eventTarget = ((SitePage)Page).EventTarget;

                if (eventTarget == null) return;

                if (eventTarget.Contains("btnGenerate") || eventTarget.Contains("btnEnergyReferesh"))
                {
                    if (eventTarget.Contains("btnGenerate")) ViewManager.SetTimePeriodDDL();
                    
                    refreshChart(null, null);
                }
            }

            void Page_PreRender()
            {
                if (!Page.IsPostBack) refreshChart(null, null);
            }

        #endregion

        #region method(s)

            void refreshChart(object sender, EventArgs e) => ViewManager.RefreshChart();

        #endregion
    }
}