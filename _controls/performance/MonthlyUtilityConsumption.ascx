﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MonthlyUtilityConsumption.ascx.cs" Inherits="CW.Website._controls.performance.MonthlyUtilityConsumption" Debug="false"%>

<script type="text/javascript" src="../../_assets/scripts/utilityConsumption.js"></script>

<div class="dockBodyWide">

  <asp:HiddenField ID="hdnIdsToHideFilter" runat="server" ClientIDMode="Static" />

  <div class="divDockFormFirstCentered">
    <asp:DropDownList ID="ddlAnalysisTypes" runat="server" AppendDataBoundItems="true" onchange="KGS.ToggleSubFilters();" CssClass="dropdown ddlAnalysisTypes" />                                                                               
  </div>

  <div id="divSubFilters" runat="server" class="divSubFilters">
  
    <div class="divDockFormCheckBox">
      <asp:CheckBox ID="chkArea" runat="server" CssClass="checkbox" ClientIDMode="Static" /><label class="labelCheckboxShort"> By Area</label>
      <asp:CheckBox ID="chkWeather" runat="server" CssClass="checkbox" ClientIDMode="Static" /><label class="labelCheckboxShort"> By Weather</label>
    </div>

    <div class="divDockFormCentered">
      <asp:DropDownList ID="ddlUtilities" runat="server" AppendDataBoundItems="true" CssClass="dropdown ddlUtilities" />
    </div>

  </div>

  <div class="divDockForm">  
    <asp:ImageButton ID="btnGenerate" runat="server" OnClick="btnGenerate_Click" ImageUrl="~/_assets/styles/images/refresh.jpg" CausesValidation="false" />
  </div>

</div>

<hr />

<div id="noResults" runat="server" class="divDockFormNoResultMessage">
  <asp:Label ID="lblNoResults" CssClass="dockMessageNoResultsLabel" runat="server" />
</div>
        
<div class="dockBodyWide">
                          
  <div id="consumptionChartDiv" runat="server">
    
    <div id="monthlyConsumptionChart" class="hiChart" runat="server">
      <asp:Literal id="monthlyConsumptionChartContainer" runat="server" Mode="PassThrough" />
    </div>

    <div>
      <span>Total Overall Consumption: </span>
      <span id="spanTotalConsumption" runat="server"></span>
    </div>

    <div>
      <span>Total Peak Month Consumption: </span>
      <span id="spanPeakMonthConsumption" runat="server"></span>
    </div>

    <div>
      <span>Total Greatest Building Consumption: </span>
      <span id="spanGreatestBuildingConsumption" runat="server"></span>
    </div>

  </div> <!--end of divConsumptionChart-->

  <div id="monthlyConsumptionChartScriptDiv">
    <asp:Literal ID="monthlyConsumptionChartScript" runat="server" Mode="PassThrough" />
  </div>

</div> <!--end of div for dock-->