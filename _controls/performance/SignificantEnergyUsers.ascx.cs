﻿using CW.Website._framework;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.performance
{
    public partial class SignificantEnergyUsers : SiteUserControl
    {
        public SearchCriteria SearchCriteria { get; set; }

        private ImageButton _btnRefresh => SEUDropDownPanel.GetRefreshButton();

        protected void Page_Load(object sender, EventArgs e)
        {
            _btnRefresh.Click += refreshChart;

            //we have to manually call the dropdown refresh click otherwise the javascript to 
            //render the hichart won't be fired - temp fix/inefficient b/c re-creating chart on each ddl change
            var eventTarget = ((SitePage)Page).EventTarget;

            if (eventTarget == null) return;

            if (eventTarget.Contains("btnGenerate") || eventTarget.Contains("btnSEURefresh"))
            {
                refreshChart(null, null);
            }
        }

        void Page_PreRender()
        {
            if (!Page.IsPostBack) refreshChart(null, null);
        }

        void refreshChart(object sender, EventArgs e) => SEUHiChart.RefreshChart();

        public DropdownPanelDataModel GetDropdownData()
        {
            return this.SEUDropDownPanel.GetDropdownFields();
        }
    }
}