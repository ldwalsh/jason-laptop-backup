﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SEUDropdownPanel.ascx.cs" Inherits="CW.Website._controls.performance.SEUDropdownPanel" %>

<div class="divDockFormFirstCentered">

<asp:DropDownList ID="ddlDisplayMode" runat="server" CssClass="dropdown ddlSEUToggle" AppendDataBoundItems="true" onchange="KGS.ToggleTable('.ddlSEUToggle'); return false;" />

<asp:DropDownList ID="ddlAnalysisType" runat="server" CssClass="dropdown ddlSEUAnalysisType" AppendDataBoundItems="true" AutoPostBack="false" onchange="KGS.RefreshSEUDisplayDDL(); return false;" />

<asp:DropDownList ID="ddlByArea" runat="server" CssClass="dropdown seuByArea" AppendDataBoundItems="true" AutoPostBack="false" />

</div>

<div class="divDockForm">
  <asp:ImageButton ID="btnSEURefresh" runat="server" ImageUrl="~/_assets/styles/images/refresh.jpg" CausesValidation="false" />
</div>

<!--table download button-->
<div class="divDockForm tableDownloadDiv tableDownloadDivSEU">
  <asp:ImageButton runat="server" 
                   OnClientClick="KGS.DownloadTable('.energyConsumptionTable'); return false;" 
                   CssClass="imgDownload"   
                   ImageUrl="/_assets/images/excel-icon.jpg" 
                   AlternateText="download" 
                   AutoPostBack="false" />
</div>

<div ID="seuDisplayTypesDiv" class="seuDisplayTypesDiv" runat="server"></div> 