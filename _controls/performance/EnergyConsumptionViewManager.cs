﻿using CW.Business;
using CW.Common.Constants;
using CW.Website._framework;
using CW.Website._services.Chart;
using CW.Website._services.Chart.Factories;
using CW.Website._services.Chart.Models;
using CW.Website._services.PerformanceDashboard.Models;
using DotNet.Highcharts.Enums;
using System.Globalization;

namespace CW.Website._controls.performance
{
    //Luke: This might be able to be combined with the Energy Consumption control directly
    public sealed class EnergyConsumptionViewManager
    {
        #region field(s)

            EnergyDropDownPanel _ddlPanel;

            EnergyConsumptionHiChart mEnergyChart;

            DataManager mDataMgr;

            SiteUser mSiteUser;
        
        #endregion

        #region properties

            SearchCriteria mSearchCriteria { get; set; }

        #endregion

        #region constructor(s)

            public EnergyConsumptionViewManager
            (   
                EnergyDropDownPanel ddlPanel, 
                EnergyConsumptionHiChart energyChart, 
                DataManager dataMgr, 
                SiteUser siteUser, 
                SearchCriteria searchCriteria
            )
            {
                _ddlPanel = ddlPanel;

                mEnergyChart = energyChart;

                mDataMgr = dataMgr;

                mSiteUser = siteUser;

                mSearchCriteria = searchCriteria;
            }

        #endregion

        #region method(s)

            public void SetTimePeriodDDL() => _ddlPanel.SetDefaultTimePeriod();
        
            /// <summary>
            /// Creates a new chart, updates the chart javascript on the page, and then
            /// re-registers the new script with page.
            /// </summary>
            public void RefreshChart()
            {
                var uiModel = GetEnergyChartUICriteriaModel();

                var units = getUnitsLabel(uiModel);

                var highChartUIModel = new HighChartUIModel()
                {
                    Name = mEnergyChart.GetChartContainer().ID,

                    UnitsLabel = units,
                    
                    Container = mEnergyChart.GetChartContainer(),

                    Script =  mEnergyChart.GetChartScript(),

                    Type = ChartTypes.Line,

                    CultureInfo = new CultureInfo(mSiteUser.CultureName),

                    CurrencyLCID = int.Parse(uiModel.CurrenciesDDL.SelectedValue)
                };

                var chartFactory = new ChartServiceFactory<EnergyConsupmtionChartService>(mDataMgr);

                var chartSvc = chartFactory.GetChartService(uiModel, highChartUIModel);

                if (chartSvc != null)
                {
                    chartSvc.CreateChart();

                    mEnergyChart.SetChartVisibility(true);
                }
            }
        
            EnergyChartUICriteriaModel GetEnergyChartUICriteriaModel()
            {
                var searchCriteriaModel = mSearchCriteria.GetSearchCriteriaModel();

                var model = new EnergyChartUICriteriaModel(searchCriteriaModel);

                model.EnergyDDLData = _ddlPanel.GetDropdownFields();

                return model;
            }

            //test labels - need to update logic
            private string getUnitsLabel(EnergyChartUICriteriaModel model)
            {
                if (model == null) return null;                

                switch(model.EnergyDDLData.AnalysisType)
                {
                    case BusinessConstants.Dropdowns.AnalysisType.AvoidableCosts:
                        return "$";
                    default:
                        return "";
                }                
            }

        #endregion
    }
}