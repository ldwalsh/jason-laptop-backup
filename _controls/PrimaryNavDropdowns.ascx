﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PrimaryNavDropdowns.ascx.cs" Inherits="CW.Website._controls.PrimaryNavDropdowns" %>


    <div id="divSelectClient" class="divSelectClient" runat="server">
        <label>Client:</label>
        <asp:DropDownList ID="ddlSelectClient" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlSelectClient_OnSelectedIndexChanged" AutoPostBack="true" runat="server">            
        </asp:DropDownList>
    </div>   

    <div id="divSelectModule" class="divSelectModule" runat="server">
        <label>Module:</label>
        <select id="ddlSelectModule" runat="server" name="menu" onChange="window.location.href='/' + this.options[this.selectedIndex].value;" value="GO">
        </select>
    </div>   
