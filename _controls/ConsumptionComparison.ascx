﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConsumptionComparison.ascx.cs" Inherits="CW.Website._controls.ConsumptionComparison" %>

<div class="divDockFormWrapperShortest">                                      
   <div class="divDockFormRight">
        <label class="labelRight">Utility:</label>
        <select id="cmbUtility" runat="server" class="dropdownRight" onchange="ConsumptionWidget.cmbUtility_change(event)"></select>
    </div>
    <div class="divDockFormRight">
        <label class="labelRight">Building:</label>
        <select id="cmbBuilding" runat="server" class="dropdownRight" onchange="ConsumptionWidget.cmbBuilding_change(event)"></select>
    </div>
</div>

<div style="text-align:center;">
	<div style="display:inline-block;">
		<div style="display:table-cell">
			<img id="imgEarth" src="/_assets/images/earthicons/earth_grey.png" runat="server" />
		</div>
		<div style="display:table-cell; vertical-align:middle; text-align:center; padding-left:5px;">
			<div id="divPercent" style="font-size:2.5em;" runat="server"></div>
			<div>
				<img id="imgArrow" src="_assets/images/arrowicons/neutral_grey.png" runat="server" />
			</div>
		</div>
	</div>
</div>
