﻿using CW.Website._framework;
using CW.Website._services.SearchCriteria.Factories;
using CW.Website._services.SearchCriteria.Models;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    /// <summary>
    /// Code behind class for the Search Criteria control
    /// </summary>
    public partial class SearchCriteria : SiteUserControl
    {
        #region event(s)

            /// <summary>
            /// Event which will fire on the initial load of the page, it will create a search criteria model, and will
            /// create a search criteria service to wire up the UI based on the model information. It will also wire up
            /// link button controls
            /// </summary>
            void Page_FirstLoad()
            {
                WireUpUI();

                WireUpLinkButtons();
            }

            /// <summary>
            /// Event which will fire on each post back (and initial load) of the web forms control. This is only used here
            /// for ensuring that the collaspible panel is wired up on each post back.  Otherwise information would be lost.
            /// A new collaspible panel mode is created and passed to the collaspible panel service to wire up the panel.
            /// </summary>
            void Page_PreRender()
            {
                var model = new CollapsiblePanelModel();

                model.SearchCriteriaCPE = cpeSearchCriteria;

                model.SelectionLNK = lnkSelection;

                model.SelectionLBL = lblSelection;

                model.SelectionPNL = pnlSelection;

                var cpSvc = new CollapsiblePanelServiceFactory(model).GetCollapsiblePanelService();

                cpSvc.WireUpCollapsiblePanel();
            }

            #region drop down event(s)

                /// <summary>
                /// Drop down event handler which wires up the building types service and executes the building types event
                /// </summary>
                /// <param name="sender">Not used</param>
                /// <param name="e">Not used</param>
                protected void ddlBuildingTypes_OnSelectedIndexChanged(object sender, EventArgs e)
                {
                    var model = GetSearchCriteriaModel();

                    var buildingTypesSvc = new BuildingTypesServiceFactory(DataMgr, siteUser, model).GetBuildingTypesService();

                    buildingTypesSvc.ExecuteBuildingTypesEvent();
                }

                /// <summary>
                /// Drop down event handler which wires up the units service and executes the units event
                /// </summary>
                /// <param name="sender">Not used</param>
                /// <param name="e">Not used</param>
                protected void ddlUnits_SelectedIndexChanged(object sender, EventArgs e)
                {
                    var model = GetSearchCriteriaModel();

                    var unitsSvc = new UnitsServiceFactory(DataMgr, siteUser, model).GetUnitsService();

                    unitsSvc.ExecuteUnitsEvent();
                }

                /// <summary>
                /// Drop down event handler which wires up the building groups service and executes the building groups event
                /// </summary>
                /// <param name="sender">Not used</param>
                /// <param name="e">Not used</param>
                protected void ddlCurrencies_SelectedIndexChanged(object sender, EventArgs e)
                {
                    var model = GetSearchCriteriaModel();

                    var currenciesSvc = new CurrenciesServiceFactory(DataMgr, siteUser, model).GetCurrenciesService();

                    currenciesSvc.ExecuteCurrenciesEvent();
                }

            #endregion

            #region list box event(s)

                /// <summary>
                /// List box event handler which wires up the building groups service and executes the building groups event
                /// </summary>
                /// <param name="sender">Not used</param>
                /// <param name="e">Not used</param>
                protected void lbeBuildingGroups_SelectedIndexChanged(object sender, EventArgs e)
                {
                    var model = GetSearchCriteriaModel();

                    var buildingGroupsSvc = new BuildingGroupsServiceFactory(DataMgr, siteUser, model).GetBuildingGroupsService();

                    buildingGroupsSvc.ExecuteBuildingGroupsEvent();
                }
                
            #endregion

            #region link button event(s)

                /// <summary>
                /// List button event handler which wires up the link button service and executes the link button event
                /// </summary>
                /// <param name="sender">Not used</param>
                /// <param name="e">Not used</param>
                protected void lnkAll_Click(object sender, EventArgs e)
                {
                    var lbModel = GetLinkButtonModel();

                    var scModel = GetSearchCriteriaModel();

                    var lbSvc = new LinkButtonServiceFactory(lbModel, DataMgr, siteUser, scModel).GetLinkButtonService();

                    lbSvc.ExecuteLinkButtonEvent(); 
                }

            #endregion

        #endregion

        #region method(s)

            /// <summary>
            /// Convenience method for wiring up the UI by executing the search criteria service
            /// </summary>
            void WireUpUI()
            {
                var model = GetSearchCriteriaModel();

                var searchCriteriaSvc = new SearchCriteriaServiceFactory(DataMgr, siteUser, model).GetSearchCriteriaService();

                searchCriteriaSvc.WireUpUI();
            }

            /// <summary>
            /// Convenience method for wiring up link buttons by executing the link button service
            /// </summary>
            void WireUpLinkButtons()
            {
                var model = GetLinkButtonModel();

                var lbSvc = new LinkButtonServiceFactory(model).GetLinkButtonService();

                lbSvc.WireUpLinkButtons(); 
            }

            /// <summary>
            /// Convenience method to set the Search Criteria model for the various Search Criteria services
            /// </summary>
            /// <returns>Search Criteria model with populated information</returns>
            public SearchCriteriaUIModel GetSearchCriteriaModel()
            {
                var model = new SearchCriteriaUIModel();

                model.BuildingTypesDDL = ddlBuildingTypes;

                model.BuildingTypesDDL = ddlBuildingTypes;

                model.BuildingGroupsLBE = lbeBuildingGroups;

                model.BuildingsLBE = lbeBuildings;

                model.RangesDDL = ddlRanges;

                model.UnitsDDL = ddlUnits;

                model.CurrenciesDDL = ddlCurrencies;

                return model;
            }
            
            /// <summary>
            /// Convenience method to set the Link button model for the link button service
            /// </summary>
            /// <returns>Link Button model with populated information</returns>
            LinkButtonModel GetLinkButtonModel()
            {
                var model = new LinkButtonModel();

                model.Text = "All";

                model.CausesValidation = false;

                model.CssClass = "dashboardViewAllLink";

                model.LinkButtons = new List<LinkButton>();

                model.LinkButtons.Add(lnkAll1);

                model.LinkButtons.Add(lnkAll2);

                return model;
            }

        #endregion        
    }
}