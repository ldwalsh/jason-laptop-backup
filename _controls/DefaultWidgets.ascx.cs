﻿using CW.Common.Constants;
using CW.Website._framework;
using System;
using System.Linq;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Dock;

namespace CW.Website._controls
{
    public partial class DefaultWidgets : SiteUserControl
    {
        #region properties

            public Boolean IsClientView { get; set; }

        #endregion

        #region events

            protected void Page_PreRender(Object sender, EventArgs e)
            {
                if (!siteUser.IsAnonymous && IsClientView)
                {
                    SetDockZoneAndDockAttributes();
                    SetWidgetVisibility();
                }
            }

        #endregion

        #region methods

            private void SetDockZoneAndDockAttributes()
            {
                //TODO, possible create a utility or helper since these are common with docks and zones
                //dynamically create all common events and settings for each radDockZone and radDock
                var radDocks = new RadDock[] { radDockBuildingMap, radDockLastMonthsDiagnosticSummary, radDockLastMonthsDiagnosticResults, radDockRecentTasks, radDockTopProjects, radDockUtilityConsumptionComparison, radDockBuildingDataPointsPieChart, radDockPast30DaysAvoidableCosts, radDockYesterdaysDiagnosticSummary, radDockYesterdaysDiagnosticResults, radDockPast30DaysPortfolioPriorities, radDockQuickLinks, radDockFaultsSparkLine, radDockNews };
                var radDockZones = new RadDockZone[] { radDockZone1, radDockZone2, radDockZone3, radDockZone4, radDockZone5 };

                foreach (var radDockZone in radDockZones)
                {
                    radDockZone.BorderStyle = BorderStyle.None;
                    radDockZone.FitDocks = false;
                    radDockZone.Orientation = Orientation.Horizontal;
                }

                foreach (var radDock in radDocks)
                {
                    radDock.DefaultCommands = DefaultCommands.ExpandCollapse;
                    radDock.EnableAnimation = radDock.EnableRoundedCorners = true;
                    radDock.DockMode = DockMode.Docked;
                    radDock.Resizable = false;

                    if (radDock.ID != radDockNews.ID && radDock.ID != radDockBuildingMap.ID && radDock.ID != radDockFaultsSparkLine.ID)
                    {
                        radDock.ForbiddenZones = new string[] { radDockZone1.ID, radDockZone4.ID, radDockZone5.ID };
                        radDock.CssClass = "radDockMedium";
                        //radDock.Width = Unit.Pixel(438);
                    }
                    else
                    {
                        radDock.EnableDrag = false;
                        radDock.CssClass = "radDockLarge";
                        //radDock.Width = Unit.Percentage(100);
                    }
                }
            }

            private void SetWidgetVisibility()
            {
                var hasDiagnosticAndCommissioningAccess = siteUser.Modules.Where(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Diagnostics || s.ModuleID == (Int32)BusinessConstants.Module.Modules.CommissioningDashboard).Count() == 2 ? true : false;
                var hasBuildings = siteUser.VisibleBuildings.Any();
                
                radDockBuildingMap.Visible = hasBuildings;
                radDockFaultsSparkLine.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                radDockLastMonthsDiagnosticSummary.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                radDockLastMonthsDiagnosticResults.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                //radDockWeather.Visible = hasBuildings;

                radDockPast30DaysAvoidableCosts.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                radDockYesterdaysDiagnosticSummary.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                radDockYesterdaysDiagnosticResults.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;
                radDockPast30DaysPortfolioPriorities.Visible = hasDiagnosticAndCommissioningAccess && hasBuildings;

                radDockBuildingDataPointsPieChart.Visible = hasBuildings;
                radDockUtilityConsumptionComparison.Visible = siteUser.Modules.Where(_ => _.ModuleID.Equals((Int32)BusinessConstants.Module.Modules.Kiosk)).Any();
                radDockNews.Visible = !siteUser.IsPublicOrKioskUser;
                radDockRecentTasks.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Tasks));
                radDockTopProjects.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Projects));
            }

        #endregion
    }
}