﻿using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Data;

namespace CW.Website._controls
{
    public partial class News : SiteUserControl
    {
        #region Properties

        const string noNews = "<div class='dockMessageLabel'>No internal organization news has been posted.</div>";

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!siteUser.IsAnonymous)
            {
                //get client by user
                var client = DataMgr.ClientDataMapper.GetClient(siteUser.CID);

                //set client settings
                ClientSetting clientSettings = DataMgr.ClientDataMapper.GetClientSettingsByClientID(client.CID);

                litNewsBody.Text = !String.IsNullOrEmpty(clientSettings.NewsBody) ? clientSettings.NewsBody : noNews;
            }
        }
    }
}