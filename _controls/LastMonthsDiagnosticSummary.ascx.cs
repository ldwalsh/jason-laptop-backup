﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CW.Website._controls
{
    public partial class LastMonthsDiagnosticSummary : ModularSiteUserControl
    {
        #region fields

            private DateTime mStartDate, endDate, startDateMonth, endDateMonth;

        #endregion

        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.LastMonth; } }

            private IEnumerable<DiagnosticSummary> LastMonthsDiagnosticsSummaries { get; set; }
            private IEnumerable<CostSavings> LastMonthsCostSavings { get; set; }

        #endregion

        #region methods

            #region overrides

                protected override void Initialize()
                {
                    DateTimeHelper.GenerateDefaultDatesYesterday(out mStartDate, out endDate, EarliestBuildingTimeZoneID);
                    DateTimeHelper.GenerateDefaultDatesPastMonth(out startDateMonth, out endDateMonth, EarliestBuildingTimeZoneID);
                }

                protected override void BindControl()
                {
                    RetrieveAndBindDiagnostics();

                    if (IsProviderView)
                    {
                        if (IsByClient)
                            gridLastMonthsTopPortfolioDiagnosticSummaries.Columns[GetColumnIndexByHeader(gridLastMonthsTopPortfolioDiagnosticSummaries, "Building")].Visible = false;

                        gridLastMonthsTopPortfolioDiagnosticSummaries.Columns[GetColumnIndexByHeader(gridLastMonthsTopPortfolioDiagnosticSummaries, "Client")].Visible = true;
                        gridLastMonthsTopPortfolioDiagnosticSummaries.Columns[GetColumnIndexByHeader(gridLastMonthsTopPortfolioDiagnosticSummaries, "lastColumn")].Visible = false;
                    }
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    var results = GetConcatenatedResults(startDateMonth, endDateMonth, DataConstants.AnalysisRange.Monthly);

                    if (!results.Any())
                        gridLastMonthsTopPortfolioDiagnosticSummaries.EmptyDataText = BusinessConstants.Message.DiagnosticsNoPriorites;

                    return results;
                }

                protected override void BindDiagnostics(IEnumerable<DiagnosticsResult> results)
                {
                    BindLastMonthsTopPortfolioDiagnosticSummaries(results);    
                    BindLastMonthsSummary(results, results.Count(), GetCostSavings(results));

                    if (!results.Any())
                        litLastMonthsSummary.Visible = false;
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion

            public void BindLastMonthsTopPortfolioDiagnosticSummaries(IEnumerable<DiagnosticsResult> results)
            {
                gridLastMonthsTopPortfolioDiagnosticSummaries.DataSource = (results.Any()) ? GetDiagnosticsSummaries(results, GetBuildingsWithSettings(results), startDateMonth).OrderByDescending(ds => ds.CostSavings).Take(10) : null;
                gridLastMonthsTopPortfolioDiagnosticSummaries.DataBind();
            }

            private void BindLastMonthsSummary(IEnumerable<DiagnosticsResult> results, Int32 totalFaults, IEnumerable<CostSavings> totalCostSavings)
            {
                if (results.Any())
                {
                    if (!IsProviderView || IsByClient)
                    {
                        var summary = new StringBuilder();

                        summary.AppendLine("<ul>");
                        summary.AppendLine("<li><span>Total Faults = " + totalFaults + "</span></li>");

                        foreach (var cs in totalCostSavings)
                            summary.AppendLine("<li><span>Total Avoidable Costs (" + CultureHelper.GetCurrencyISOSymbol(cs.LCID) + ") = " + CultureHelper.FormatCurrencyAsString(cs.LCID, cs.Cost) + "</span></li>");

                        summary.AppendLine("</ul>");

                        litLastMonthsSummary.Text = summary.ToString();
                    }
                }
            }

        #endregion
    }
}