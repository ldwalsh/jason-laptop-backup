﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProviderWidgets.ascx.cs" Inherits="CW.Website._controls.ProviderWidgets" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
              
<input type="button" id="btnUILoad" value="LoadProviderWidgets" style="display: none;" />

<div class="homeColumns dockWrapper">

  <div>
    
    <telerik:RadDockLayout ID="radDockLayout1" runat="server">
      <telerik:RadDockZone ID="radDockZone1" runat="server">
                        
        <telerik:RadDock ID="radDockBuildingMap" CssClass="radDockLarge" Title="Building Map" runat="server">
          <ContentTemplate>
            <div id="updateProgressBuildingMap" class="updateProgressDivForWidgets"></div>
            <div id="buildingMap"></div>
		  </ContentTemplate>
        </telerik:RadDock>
          
      </telerik:RadDockZone>
    </telerik:RadDockLayout>    

  </div>

  <div class="homeLeftColumn">

    <telerik:RadDockLayout ID="radDockLayout2" runat="server">
      <telerik:RadDockZone ID="radDockZone2" runat="server">

        <telerik:RadDock ID="radDockLastMonthsDiagnosticSummaryByClient"  CssClass="radDockMedium" Title="Last Month's Top Portfolio Diagnostic Summaries" runat="server">
          <ContentTemplate>
            <div id="updateProgressLastMonthsDiagnosticSummaryByClient" class="updateProgressDivForWidgets"></div>
            <div id="lastMonthsDiagnosticSummaryByClient"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockLastMonthsDiagnosticSummary"  CssClass="radDockMedium" Title="Last Month's Top Portfolio Building Diagnostic Summaries" runat="server">
          <ContentTemplate>
            <div id="updateProgressLastMonthsDiagnosticSummary" class="updateProgressDivForWidgets"></div>
            <div id="lastMonthsDiagnosticSummary"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockLastMonthsDiagnosticResults"  CssClass="radDockMedium" Title="Last Month's Top Portfolio Diagnostic Results" runat="server">
          <ContentTemplate>
            <div id="updateProgressLastMonthsDiagnosticResults" class="updateProgressDivForWidgets"></div>
            <div id="lastMonthsDiagnosticResults"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockPast30DaysAvoidableCosts"  CssClass="radDockMedium" Title="Past 30 Days Portfolio Avoidable Costs" runat="server">
          <ContentTemplate>
            <div id="updateProgressPast30DaysAvoidableCosts" class="updateProgressDivForWidgets"></div>
            <div id="past30DaysAvoidableCosts"></div>
          </ContentTemplate>
        </telerik:RadDock>

      </telerik:RadDockZone>
    </telerik:RadDockLayout>

  </div>
  <div class="homeRightColumn">

    <telerik:RadDockLayout ID="radDockLayout3" runat="server">
      <telerik:RadDockZone ID="radDockZone3" runat="server">

        <telerik:RadDock ID="radDockYesterdaysDiagnosticSummaryByClient"  CssClass="radDockMedium" Title="Yesterday's Top Portfolio Diagnostic Summaries" runat="server">
          <ContentTemplate>
            <div id="updateProgressYesterdaysDiagnosticSummaryByClient" class="updateProgressDivForWidgets"></div>
            <div id="yesterdaysDiagnosticSummaryByClient"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockYesterdaysDiagnosticSummary"  CssClass="radDockMedium" Title="Yesterday's Top Portfolio Building Diagnostic Summaries" runat="server">
          <ContentTemplate>
            <div id="updateProgressYesterdaysDiagnosticSummary" class="updateProgressDivForWidgets"></div>
            <div id="yesterdaysDiagnosticSummary"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockYesterdaysDiagnosticResults"  CssClass="radDockMedium" Title="Yesterday's Top Portfolio Diagnostic Results" runat="server">
          <ContentTemplate>
            <div id="updateProgressYesterdaysDiagnosticResults" class="updateProgressDivForWidgets"></div>
            <div id="yesterdaysDiagnosticResults"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockPast30DaysPortfolioPriorities"  CssClass="radDockMedium" Title="Past 30 Days Portfolio Priorities" runat="server">
          <ContentTemplate>
            <div id="updateProgressPast30DaysPortfolioPriorities" class="updateProgressDivForWidgets"></div>
            <div id="past30DaysPortfolioPriorities"></div>
          </ContentTemplate>
        </telerik:RadDock>

      </telerik:RadDockZone>
    </telerik:RadDockLayout>

  </div>

</div> <!--end of homeColumns div-->