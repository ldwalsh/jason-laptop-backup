﻿using CW.Data;
using CW.Common.Helpers;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Common.Constants;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website._controls
{
    public partial class Weather : SiteUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public void LoadModule()
        {
            if (!siteUser.IsAnonymous && siteUser.VisibleBuildings.Any())
                BindBuildings(siteUser.VisibleBuildings);
        }

        #region Load and Bind Fields

            /// <summary>
            /// Binds a dropdown list by client id
            /// </summary>
            /// <param name="ddl"></param>
            public void BindBuildings(IEnumerable<Building> buildings)
            {
                ddlWeatherBuilding.DataTextField = "BuildingName";
                ddlWeatherBuilding.DataValueField = "BID";
                ddlWeatherBuilding.DataSource = buildings;
                ddlWeatherBuilding.DataBind();

                int bid = 0;
                if (!String.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                   bid = Convert.ToInt32(Request.QueryString["bid"]);

                if (buildings.Where(b => b.BID == bid).Any())
                    ddlWeatherBuilding.SelectedValue = bid.ToString();
                else
                    ddlWeatherBuilding.SelectedIndex = 0;

                BindWUnderGroundWeatherWidget(Convert.ToInt32(ddlWeatherBuilding.SelectedValue));
            }

        #endregion

        #region WIDGET

            /// <summary>
            /// Binds wunderground weather 
            /// </summary>
            private void BindWUnderGroundWeatherWidget(int bid)
            {
                //building weather
                Building b = DataMgr.BuildingDataMapper.GetBuildingByBID(bid, false, true, true, false, false, true);
                WeatherHelper bfg = new WeatherHelper(b, b.BuildingSettings.First().UnitSystem, "https://api.wunderground.com/api/c7feea76301cdad7/");
                string bindError = bfg.BindDataSets();

                //Try to get weather from wunderground
                try
                {
                    if (String.IsNullOrEmpty(bindError))
                    {
                        //Current Weather Image
                        string currentConditionsText = bfg.GetConditions(WeatherHelper.WeatherRange.Current);
                        currentWeatherImage.Src = GetHandlerUrl(bfg.GetWeatherImage(WeatherHelper.WeatherRange.Current));
                        currentWeatherImage.Alt = currentConditionsText;

                        //Current Temperature
                        lblTemp.InnerText = bfg.GetTemp(WeatherHelper.TempType.Current);

                        //Current Conditions
                        spnForecastVal.InnerText = currentConditionsText;
                        divForecast.Visible = (currentConditionsText != string.Empty) ? true : false;

                        spnHumidityVal.InnerText = bfg.GetCurrentHumidity();
                        divHumidity.Visible = (spnHumidityVal.InnerText != string.Empty) ? true : false;

                        spnWindVal.InnerText = bfg.GetCurrentWind();
                        divWind.Visible = (spnWindVal.InnerText != string.Empty) ? true : false;

                        //Today    
                        string todayForcastConditionsText = bfg.GetConditions(WeatherHelper.WeatherRange.Today);
                        todayImage.Src = GetHandlerUrl(bfg.GetWeatherImage(WeatherHelper.WeatherRange.Today));
                        todayImage.Alt = todayForcastConditionsText;

                        lblToday.InnerText = bfg.GetDate(WeatherHelper.WeatherRange.Today);
                        lblTodayTempHi.InnerText = bfg.GetTemp(WeatherHelper.TempType.TodayHigh);
                        lblTodayTempLo.InnerText = bfg.GetTemp(WeatherHelper.TempType.TodayLow);
                        lblTodayForcast.InnerText = todayForcastConditionsText;

                        //Tomorrow     
                        string tomorrowForcastConditionsText = bfg.GetConditions(WeatherHelper.WeatherRange.Tomorrow);
                        tomorrowImage.Src = GetHandlerUrl(bfg.GetWeatherImage(WeatherHelper.WeatherRange.Tomorrow));
                        tomorrowImage.Alt = tomorrowForcastConditionsText;

                        lblTomorrow.InnerText = bfg.GetDate(WeatherHelper.WeatherRange.Tomorrow);
                        lblTomorrowTempHi.InnerText = bfg.GetTemp(WeatherHelper.TempType.TomorrowHigh);
                        lblTomorrowTempLo.InnerText = bfg.GetTemp(WeatherHelper.TempType.TomorrowLow);
                        lblTomorrowForcast.InnerText = tomorrowForcastConditionsText;

                        //Two Days
                        string twoDaysForcastConditionsText = bfg.GetConditions(WeatherHelper.WeatherRange.TwoDays);
                        twoDaysImage.Src = GetHandlerUrl(bfg.GetWeatherImage(WeatherHelper.WeatherRange.TwoDays));
                        twoDaysImage.Alt = twoDaysForcastConditionsText;

                        lblTwoDays.InnerText = bfg.GetDate(WeatherHelper.WeatherRange.TwoDays);
                        lblTwoDaysTempHi.InnerText = bfg.GetTemp(WeatherHelper.TempType.TwoDayHigh);
                        lblTwoDaysTempLo.InnerText = bfg.GetTemp(WeatherHelper.TempType.TwoDayLow);
                        lblTwoDaysForcast.InnerText = twoDaysForcastConditionsText;

                        //Three Days
                        string threeDaysForcastConditionsText = bfg.GetConditions(WeatherHelper.WeatherRange.ThreeDays);
                        threeDaysImage.Src = GetHandlerUrl(bfg.GetWeatherImage(WeatherHelper.WeatherRange.ThreeDays));
                        threeDaysImage.Alt = threeDaysForcastConditionsText;

                        lblThreeDays.InnerText = bfg.GetDate(WeatherHelper.WeatherRange.ThreeDays);
                        lblThreeDaysTempHi.InnerText = bfg.GetTemp(WeatherHelper.TempType.ThreeDayHigh);
                        lblThreeDaysTempLo.InnerText = bfg.GetTemp(WeatherHelper.TempType.ThreeDayLow);
                        lblThreeDaysForcast.InnerText = threeDaysForcastConditionsText;

                        pnlHasWeather.Visible = true;
                        pnlNoWeather.Visible = false;

                        //Location
                        lblWeatherLocation.InnerText = bfg.GetLocation();
                    }
                    else
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("{0} for bid:{1}", bindError, bid), null);
                        pnlHasWeather.Visible = false;
                        pnlNoWeather.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Home Page: Weather Widget", ex);
                    pnlHasWeather.Visible = false;
                    pnlNoWeather.Visible = true;
                }
            }

            private string GetHandlerUrl(string imageUrl)
            {
                return HandlerHelper.TransformSecureWeatherImageUrl("VendorImageHandler.axd", Request.Url.Host, imageUrl); 
            }

        #endregion
    }
}