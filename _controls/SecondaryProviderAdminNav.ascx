﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SecondaryProviderAdminNav.ascx.cs" Inherits="CW.Website._controls.SecondaryProviderAdminNav" %>
<%@ Import Namespace="CW.Website._framework.httpmodules" %>

<div class="adminNav">

  <ul class="dropdown">
    <li><a href="/ProviderAdministration.aspx" class="hoverBtn">Admin Home</a></li>
    <li><a href="/ProviderDataSourceAdministration.aspx" class="hoverBtn">Data Sources</a></li>
    <li><a class="hoverBtnNoCursor">Buildings</a>
      <ul class="sub_menu">
        <li runat="server" data-boolean-check="4"><a href="/<%= PathRewriteModule.ProviderBuildingGroupAdministration %>">Building Groups</a></li>
        <li><a href="/ProviderBuildingAdministration.aspx">Buildings</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Equipment</a>
      <ul class="sub_menu">
        <li><a href="/ProviderEquipmentAdministration.aspx">Equipment</a></li>
        <li><a href="/ProviderEquipmentTypeAdministration.aspx">Equipment Types</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Points</a>
      <ul class="sub_menu">
        <li><a href="/ProviderPointAdministration.aspx">Points</a></li>
        <li><a href="/ProviderPointTypeAdministration.aspx">Point Types</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Analyses</a>
      <ul class="sub_menu">
        <li><a href="/ProviderAnalysisAdministration.aspx">Analyses</a></li>
        <li><a href="/ProviderScheduledAnalysesAdministration.aspx">Scheduled Analyses</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Users</a>
      <ul class="sub_menu">
        <li><a href="/UserAdministration.aspx">Users</a></li> <!--TODO Provider-->
        <li runat="server" data-boolean-check="5"><a href="/<%= PathRewriteModule.ProviderUserGroupAdministration %>">User Groups</a></li>
      </ul>
    </li>
    <li runat="server" data-boolean-check="5"><a href="/<%= PathRewriteModule.ProviderClientAdministration %>" class="hoverBtn">Client</a></li>
    <li runat="server" data-boolean-check="6"><a href="/<%= PathRewriteModule.ProviderOrganizationAdministration %>" class="hoverBtn">Organization</a></li>    
    <li><a class="hoverBtnNoCursor">Services</a>
      <ul class="sub_menu">                   
        <li><a href="/CustomerValueReports.aspx">Customer Value Reports</a></li>
      </ul>
    </li>
    <li runat="server" data-boolean-check="4"><a class="hoverBtnNoCursor">System</a>
      <ul class="sub_menu">
        <li><a href="/ProviderSystemTest.aspx">System Test</a></li>
        <li><a href="/ProviderUploadBuildings.aspx">Upload Buildings</a></li>
        <li><a href="/ProviderUploadBuildingVariables.aspx">Upload Building Variables</a></li>
        <li><a href="/ProviderUploadDataSources.aspx">Upload Data Sources</a></li>
        <li><a href="/ProviderUploadEquipment.aspx">Upload Equipment</a></li>
        <li><a href="/ProviderUploadEquipmentPoints.aspx">Upload Equipment Points</a></li>
        <li><a href="/ProviderUploadEquipmentToEquipment.aspx">Upload Equipment To Equipment</a></li>
        <li><a href="/ProviderUploadEquipmentVariables.aspx">Upload Equipment Variables</a></li>
        <li><a href="/ProviderUploadPoints.aspx">Upload Points</a></li>
      </ul>
    </li>
  </ul>

</div>