﻿using CW.Business;
using CW.Business.Results;
using CW.Website._framework;
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using CW.Common.Constants;
using CW.Utility;
using CW.Data.Models.Utility;
using CW.Utility.Web;

namespace CW.Website._controls
{
	public partial class ConsumptionComparison: SiteUserControl
	{
		#region static

			#region field

				private static String[] arrowColorLookup = new[]{"neutral_grey", "dark_green_down", "light_green_down", "yellow_up", "red_up",};
				private static String[] earthColorLookup = new[]{"grey", "darkgreen", "lightgreen", "yellow", "red",};

			#endregion

			private static String[] utilities = new[]{"Total", "Chilled Water", "Electric", "Gas", "Hot Water", "Steam", "Water",};

			#region method

				private static T GetPropertyValue<T>(Object instance, String propertyName)
				{
					return (T)instance.GetType().GetProperty(propertyName, (BindingFlags.Instance | BindingFlags.Public)).GetValue(instance);
				}

			#endregion

		#endregion

		public void LoadModule()
		{
            if (siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Kiosk))
            { 
                var dataStr = Page.Request.QueryString["data"];

                var data = ((dataStr == null) ? null : new JavaScriptSerializer().Deserialize<dynamic>(dataStr));

                var visibleBuildings = siteUser.VisibleBuildings.ToList();

                visibleBuildings.ForEach(_ => cmbBuilding.Items.Add(new ListItem(_.BuildingName, _.BID.ToString())));

                utilities.ToList().ForEach(_ => cmbUtility.Items.Add(_));

                if (data != null)
                {
                    cmbBuilding.Items.FindByValue(data["bid"].ToString()).Selected = true;
                    cmbUtility.Items.FindByText(data["utility"].ToString()).Selected = true;
                }

                var inputs = new KioskBuildingInputs() { CID = siteUser.CID, ColorPointTypeIDs = BusinessConstants.PointType.KioskColorPointTypeIDs, EnergyUsePointTypeIDs = BusinessConstants.PointType.KioskEnergyUsePointTypeIDs, EnergyPointClassIDs = BusinessConstants.PointClass.KioskTopLevelPointClassIDs, PowerPointClassIDs = EnumHelper.ToArray<BusinessConstants.PointClass.PowerPointClassEnum>(), Creds = SiteUser.Current};

                var kioskBuildings =
                    new KioskBuildingResultsManager(DataMgr, inputs, (cid, bid, imageExt) => { return HandlerHelper.BuildingImageUrl(cid, bid, imageExt); }).Get().Where(_ => _.BID == Int32.Parse(cmbBuilding.Value)).First();

                var utility = cmbUtility.Value;
                var performance = GetPropertyValue<Int32>(kioskBuildings, (utility + "PerfColor"));

                imgEarth.Src = imgEarth.Src.Replace(Path.GetFileNameWithoutExtension(imgEarth.Src), String.Concat("earth_", earthColorLookup[performance]));
                imgArrow.Src = imgArrow.Src.Replace(Path.GetFileNameWithoutExtension(imgArrow.Src), arrowColorLookup[performance]);

                var percent = GetPropertyValue<Double>(kioskBuildings, (utility + "PerfPct"));

                divPercent.InnerHtml = ((percent == 0) ? "N/A" : (((percent > 0) ? (Func<Double, Double>)Math.Ceiling : Math.Floor)(percent * 1000) / 10).ToString() + "%");
            }
		}
	}
}