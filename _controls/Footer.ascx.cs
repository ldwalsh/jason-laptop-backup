﻿using System;
using System.Configuration;

using CW.Website._framework;
using CW.Utility;

namespace CW.Website._controls
{
    public partial class Footer : SiteUserControl
    {       
        protected void Page_Load(object sender, EventArgs e)
        {
            string footerContent = "$product Powered by KGS Buildings.<br />Copyright © " + DateTime.UtcNow.Year + " KGS Buildings LLC. All rights reserved.";

            //SE theme check
            if (siteUser.IsAnonymous)
            {                
                litFooterContent.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(Request.Url.AbsoluteUri.Contains(ConfigurationManager.AppSettings["SchneiderElectricDomain"]), footerContent);
                divFooterLeft.Visible = divFooterRight.Visible = false;
            }
            else
            {
                litFooterContent.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, footerContent);

                //Org logo
                if (!String.IsNullOrEmpty(siteUser.OrganizationLogoUrl))
                {
                    imgOrganizationLogo.Alt = "Org";
                    imgOrganizationLogo.Src = siteUser.OrganizationLogoUrl;

                    divFooterLeft.Attributes["class"] = "divFooterMultiLogoLeft";
                    divFooterRight.Attributes["class"] = "divFooterMultiLogoRight";
                }
                else
                {
                    divFooterRight.Visible = false;
                }
            }
        }
    }
}
