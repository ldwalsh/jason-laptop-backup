﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNavResponsive.ascx.cs" Inherits="CW.Website._controls.LeftNavResponsive" %>

<div class="divLeftNav divLeftNavResponsive">
    <div>
        <a title="Home Page" href="/Home.aspx">
            <div class="moduleIcon homeModuleIconWhite">
                <h1>Home</h1>       
            </div>                    
        </a>
    </div>
    <hr />
    <asp:Repeater ID="rptModules" runat="server">
        <HeaderTemplate>                                                               
        </HeaderTemplate>
        <ItemTemplate>   
            <div>
                <a title='<%# Eval("ModuleDescription") %>' href='<%# "/" + Eval("ModuleLink") %>'>
                    <div class='<%# "moduleIcon" + " " + Eval("ModuleName").ToString().Replace(" ", "").ToLower() + "ModuleIconWhite" %>'>
                        <h1><%# Eval("ModuleName") %></h1>       
                    </div>                    
                </a>
            </div>
            <hr />                                                                                                                                                                                                                                                           
        </ItemTemplate>
        <FooterTemplate>
        </FooterTemplate>
    </asp:Repeater>
</div>
