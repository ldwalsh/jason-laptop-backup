﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PrimaryNav.ascx.cs" Inherits="CW.Website._controls.PrimaryNav" %>
           <div class="divPrimaryNav">
               <ul id="utility">
                        <li id="lnkHome" runat="server"><a href="/Home.aspx">Home</a></li>                    
                        <li id="lnkAdmin" runat="server"><a href="/Administration.aspx">Admin</a></li>
                        <li id="lnkProviderAdmin" runat="server"><a href="/ProviderAdministration.aspx">Provider</a></li>
                        <li id="lnkKGSAdmin" runat="server"><a href="/KGSAdministration.aspx">KGS</a></li>
                        <li id="lnkSystemAdmin" runat="server"><a href="/SystemAdministration.aspx">System</a></li>                    
                        <!--<li id="lnkDashboard" runat="server"><a href="/Dashboard.aspx">Dashboard</a></li>
                        <li id="lnkDiagnostics" runat="server"><a href="/Diagnostics.aspx">Diagnostics</a></li>
                        <li id="lnkProfiles" runat="server"><a href="/Profiles.aspx">Profiles</a></li>-->
                        <!--<li id="lnkEI" runat="server"><a href="#">Efficiency Investments</a></li>-->                   
                        <!--<li id="lnkReports" runat="server"><a href="/Reports.aspx">Reports</a></li>
                        <li id="lnkDocuments" runat="server"><a href="/Documents.aspx">Documents</a></li>-->
                        <li id="lnkHelp" runat="server"><a href="/Help.aspx">Help</a></li>
               </ul>
           </div>