﻿namespace CW.Website._controls.admin
{
    public enum AdminModeEnum
    {
        KGS,
        PROVIDER,
        NONKGS,
    };
}