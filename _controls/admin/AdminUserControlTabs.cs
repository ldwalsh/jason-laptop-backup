﻿using CW.Utility;
using CW.Utility.Web;
using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Telerik.Web.UI;

namespace CW.Website._controls.admin
{
    public class AdminUserControlTabs
    {
        #region fields

            private RadTabStrip mRadTabStrip;
            private RadMultiPage mRadMultiPage;
            private String defaultControl;

        #endregion

        #region constructor

            public AdminUserControlTabs(RadTabStrip radTabStrip, RadMultiPage radMultiPage, String control = null)
            {
                mRadTabStrip = radTabStrip;
                mRadMultiPage = radMultiPage;

                if (control != null) defaultControl = control;
            }

        #endregion

        #region methods

            public RadPageView GetRadPageView(String control)
            {
                if (mRadTabStrip != null && mRadMultiPage != null)
                {
                    if (control != null)
                    {
                        foreach (RadPageView pageView in mRadMultiPage.PageViews)
                        {
                            var radPageView = pageView.Controls.Cast<Control>().Where(c => c.GetType().BaseType.Name == control).Select(c => (RadPageView)c.Parent).FirstOrDefault();

                            if (radPageView != null) return radPageView;
                        }
                    }
                }

                return null;
            }

            public void SetActiveTab()
            {
                var url = HttpContext.Current.Request.Url.PathAndQuery;
                var index = url.IndexOf('?');
                var control = defaultControl;

                if (index > 0)
                {
                    var qs = new QueryString(url.Substring(index + 1));
                    var qsControl = qs.Get("control", typeof(String));

                    if (qsControl != null) control = qsControl.ToString();
                }

                if (mRadTabStrip != null && mRadMultiPage != null)
                {
                    if (control != null)
                    {
                        var radPageView = GetRadPageView(control);

                        if (radPageView != null)
                        {
                            mRadTabStrip.SelectedIndex = radPageView.Index;
                            mRadMultiPage.SelectedIndex = radPageView.Index;
                        }
                    }
                }
            }

            public Boolean IsCurrentTabActive(String currentControl)
            {
                if (mRadTabStrip != null)
                {
                    var radPageView = GetRadPageView(currentControl);

                    if (radPageView != null) return (mRadTabStrip.SelectedIndex == radPageView.Index);
                }

                return false;
            }

        #endregion
    }
}