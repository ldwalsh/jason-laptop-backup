﻿using CW.Utility;
using CW.Utility.Web;
using FileUploaderControl;
using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._controls.admin
{
    public class AdminUserControlForm
    {
        #region fields

            Control control;

        #endregion

        #region constructor

            public AdminUserControlForm(Control userControl)
            {
                control = userControl;
            }

        #endregion

        #region methods

            public void ClearTextBoxes()
            {
                var textBoxes = ControlHelper.FindControls<TextBox>(control);

                if (textBoxes.Count > 0)
                {
                    foreach (var textBox in textBoxes) 
                        textBox.Text = String.Empty;
                }
            }

            public void ClearRadMaskedTextBox()
            {
                var radMaskedTextBoxes = ControlHelper.FindControls<RadMaskedTextBox>(control);

                if (radMaskedTextBoxes.Count > 0)
                {
                    foreach (var radMaskedTextBox in radMaskedTextBoxes) 
                        radMaskedTextBox.Text = String.Empty;
                }
            }

            public void ClearCheckBoxes()
            {
                var checkBoxes = ControlHelper.FindControls<CheckBox>(control);

                if (checkBoxes.Count > 0)
                {
                    foreach (var checkBox in checkBoxes) 
                        checkBox.Checked = false;
                }
            }

            public void ClearTextAreas()
            {
                var textAreas = ControlHelper.FindControls<HtmlTextArea>(control);

                if (textAreas.Count > 0)
                {
                    foreach (var textArea in textAreas) 
                        textArea.InnerText = String.Empty;
                }
            }

            public void ClearRadEditors()
            {
                var radEditors = ControlHelper.FindControls<RadEditor>(control);

                if (radEditors.Count > 0)
                {
                    foreach (var radEditor in radEditors) 
                        radEditor.Content = String.Empty;
                }
            }

            public void ClearImageFields()
            {
                var imageFields = ControlHelper.FindControls<FileUploader>(control);

                if (imageFields.Count > 0)
                {
                    foreach (var imageField in imageFields) 
                        imageField.Clear();
                }
            }

            public void ResetDropDownLists()
            {
                var dropDownLists = ControlHelper.FindControls<DropDownList>(control);

                if (dropDownLists.Count > 0)
                {
                    foreach (var dropDownList in dropDownLists)
                    {
                        if (dropDownList.Visible) dropDownList.SelectedIndex = 0;
                    }
                }
            }

        #endregion
    }
}