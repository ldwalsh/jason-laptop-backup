﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace CW.Website._controls.admin.ScheduledAnalyses
{
    public partial class ScheduleAnalysisForAnEquipment : AdminUserControlBase
    {
        #region constants

            const String scheduledAnalysisExists = "Scheduled Analysis already exists.";
            const String addScheduledAnalysisSuccess = "Scheduled analysis addition was successful.";
            const String addScheduledAnalysisFailed = "Adding scheduled analysis failed. Please contact an administrator.";

        #endregion

        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get { return new Enum[] { KGSScheduledAnalysesAdministration.TabMessages.DeleteScheduledAnalyses }; }
                }

            #endregion

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAddError.Visible = false;

                if (!Page.IsPostBack)
                {
                    var buildings = siteUser.VisibleBuildings;

                    dropDowns.BindBuildingList(buildings);

                    InitializeControl();
                }
            }

            #region ddl events

                protected void ddlCW_BuildingChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    if (ea.SelectedValue == "-1") return;

                    sender.BindEquipmentClassList(DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(dropDowns.BID.Value));
                }

                protected void ddlCW_EquipmentClassChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    if (ea.SelectedValue == "-1") return;

                    sender.BindEquipmentList(DataMgr.EquipmentDataMapper.GetAllActiveEquipmentByBuildingIDAndEquipmentClassID(dropDowns.BID.Value, dropDowns.EquipmentClassID.Value));
                }

                protected void ddlCW_EquipmentChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    if (ea.SelectedValue == "-1") return;

                    sender.BindAnalysisList(DataMgr.AnalysisDataMapper.GetAllActiveNotUtilityAndNotScheduledAnalysesByEID(dropDowns.EID.Value));
                }
            
            #endregion

            #region button events

                protected void addScheduledAnalysisButton_Click(Object sender, EventArgs e)
                {
                    if (DataMgr.ScheduledAnalysisDataMapper.DoesScheduledAnalysisExist(dropDowns.EID.Value, dropDowns.AID.Value))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, scheduledAnalysisExists, lnkSetFocus);
                        return;
                    }
                    else
                    {
                        var mScheduledAnalysis = new ScheduledAnalyse();

                        LoadAddFormIntoScheduledAnalysis(mScheduledAnalysis);

                        try
                        {
                            DataMgr.ScheduledAnalysisDataMapper.InsertScheduledAnalysis(mScheduledAnalysis);

                            LabelHelper.SetLabelMessage(lblResults, addScheduledAnalysisSuccess, lnkSetFocus);
                            SetChangeStateForTabs();
                        }
                        catch (SqlException sqlEx)
                        {
                            LabelHelper.SetLabelMessage(lblAddError, addScheduledAnalysisFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding scheduled analysis.", sqlEx);

                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblAddError, addScheduledAnalysisFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding scheduled analysis.", ex);
                        }
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    var adminUserControlForm = new AdminUserControlForm(this);

                    var ddls = new[] 
                    { 
                        dropDowns.GetDDL(DropDowns.ViewModeEnum.Building),
                        dropDowns.GetDDL(DropDowns.ViewModeEnum.EquipmentClass),
                        dropDowns.GetDDL(DropDowns.ViewModeEnum.Equipment),
                        dropDowns.GetDDL(DropDowns.ViewModeEnum.Analysis)
                    };

                    new DropDownSequencer(ddls).ResetSubDropDownsOf(dropDowns.GetDDL(DropDowns.ViewModeEnum.Building));

                    adminUserControlForm.ResetDropDownLists();
                    adminUserControlForm.ClearTextBoxes();

                    chkAddRunHalfDay.Checked = false;
                    chkAddRunDaily.Checked = true;
                    chkAddRunWeekly.Checked = true;
                    chkAddRunMonthly.Checked = true;
                }

            #endregion

            private void LoadAddFormIntoScheduledAnalysis(ScheduledAnalyse scheduledAnalysis)
            {
                scheduledAnalysis.BID = dropDowns.BID.Value;
                scheduledAnalysis.EID = dropDowns.EID.Value;
                scheduledAnalysis.AID = dropDowns.AID.Value;
                scheduledAnalysis.RunStartDate = (DateTime)txtAddRunStartDate.SelectedDate;
                scheduledAnalysis.RunHalfDay = chkAddRunHalfDay.Checked;
                scheduledAnalysis.RunDaily = chkAddRunDaily.Checked;
                scheduledAnalysis.RunWeekly = chkAddRunWeekly.Checked;
                scheduledAnalysis.RunMonthly = chkAddRunMonthly.Checked;
                scheduledAnalysis.DateModified = DateTime.UtcNow;
            }

        #endregion
    }
}