﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViewScheduledAnalyses.ascx.cs" Inherits="CW.Website._controls.admin.ScheduledAnalyses.ViewScheduledAnalyses" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" TagName="DropDowns" src="~/_controls/DropDowns.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Data.Models.ScheduledAnalysis" %>
<%@ Import Namespace="CW.Utility" %>

<h2>View Scheduled Analyses</h2> 

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>

<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div> 

<CW:DropDowns runat="server" ID="gridDropDowns" validationEnabled="true" ValidationGroup="Search" StartViewMode="Building" ViewMode="EquipmentClass" OnBuildingChanged="GridDropDowns_BuildingChanged" OnEquipmentClassChanged="GridDropDowns_EquipmentClassChanged" OnEquipment="GridDropDowns_EquipmentChanged" />
<br />

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" /> 
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>            
          <asp:LinkButton ID="lnkEdit" Runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="AnalysisName" HeaderText="Analysis Name">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.AnalysisName)) %>"><%# GetCellContent(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.AnalysisName), 30) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentName" HeaderText="Equipment Name">                                                    
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.EquipmentName)) %>"><%# GetCellContent(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.EquipmentName), 30) %></label></ItemTemplate>     
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building Name">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.BuildingName)) %>"><%# GetCellContent(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.BuildingName), 30) %></label></ItemTemplate>     
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="RunStartDate" HeaderText="Run Start Date">  
        <ItemTemplate><%# GetCellContentAsDateTime(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.RunStartDate)).ToShortDateString() %></ItemTemplate>                                    
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="RunPostponeDate" HeaderText="Postpone Date">  
        <ItemTemplate><%# String.IsNullOrEmpty(GetCellContent(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.RunPostponeDate))) ? null : GetCellContentAsDateTime(Container, PropHelper.G<GetScheduledAnalysisData>(_ => _.RunPostponeDate)).ToShortDateString()  %></ItemTemplate>                                    
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>
          <asp:LinkButton ID="lnkDelete" Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this scheduled analysis permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<div>
  <!--SELECT SCHEDULED ANALYSIS DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptScheduledAnalysisDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Scheduled Analysis Details</h2>
          <ul id="scheduledAnalysisDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>                              
</div> 
                                   
<!--EDIT ANALYSIS PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateScheduledAnalysis">
  <div>
    <h2>Edit Scheduled Analysis</h2>
  </div>

  <div>
    <a id="lnkSetFocusEdit" runat="server"></a>
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*Analysis Name:</label>
      <asp:Label ID="lblEditAnalysisName" CssClass="labelContent" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Equipment Name:</label>
      <asp:Label ID="lblEditEquipmentName" CssClass="labelContent" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Building Name:</label>
      <asp:Label ID="lblEditBuildingName" CssClass="labelContent" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Run Start Date:</label>
      <asp:Label ID="lblEditRunStartDate" CssClass="labelContent" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Run Postpone Date:</label>
      <telerik:RadDatePicker ID="txtEditRunPostponeDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>              
    </div>
    <div class="divForm">
      <label class="label">*Run Half Day:</label>
      <asp:CheckBox ID="chkEditRunHalfDay" CssClass="checkbox" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Run Daily:</label>
      <asp:CheckBox ID="chkEditRunDaily" CssClass="checkbox" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Run Weekly:</label>
      <asp:CheckBox ID="chkEditRunWeekly" CssClass="checkbox" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Run Monthly:</label>
      <asp:CheckBox ID="chkEditRunMonthly" ChCssClass="checkbox" runat="server" />
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateScheduledAnalysis" runat="server" Text="Update" OnClick="updateScheduledAnalysisButton_Click" ValidationGroup="EditScheduledAnalysis" />

  </div>

</asp:Panel>