﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BulkDeleteScheduledAnalyses.ascx.cs" Inherits="CW.Website._controls.admin.ScheduledAnalyses.BulkDeleteScheduledAnalyses" %>

<asp:Panel ID="pnlBulkDeleteScheduledAnalyses" runat="server" DefaultButton="btnBulkDeleteScheduledAnalyses">

  <h2>Bulk Delete Scheduled Analyses</h2>

  <p>Please select a client and building in order select and bulk delete schedulded analyses. Try to limit your selection in order not to timeout.</p> 

  <div>              
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblBulkDeleteError" CssClass="errorMessage" runat="server" />

    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>
    <div class="divForm">   
      <label class="label">*Select Building:</label>    
      <asp:DropDownList ID="ddlBulkDeleteBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBulkDeleteBuildings_SelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <asp:RequiredFieldValidator ID="bulkDeleteBuildingRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBulkDeleteBuildings" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="BulkDelete" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkDeleteBuildingRequiredValidatorExtender" runat="server" BehaviorID="bulkDeleteBuildingRequiredValidatorExtender" TargetControlID="bulkDeleteBuildingRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <div class="divForm">   
      <label class="label">*Select Analysis:</label>    
      <asp:DropDownList ID="ddlBulkDeleteAnalysis" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBulkDeleteAnalysis_SelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>  
    <asp:RequiredFieldValidator ID="bulkDeleteAnalysisRequiredValidator" runat="server" ErrorMessage="Analysis is a required field." ControlToValidate="ddlBulkDeleteAnalysis" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="BulkDelete" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkDeleteAnalysisRequiredValidatorExtender" runat="server" BehaviorID="bulkDeleteAnalysisRequiredValidatorExtender" TargetControlID="bulkDeleteAnalysisRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                            
    <div class="divForm" runat="server">                                       
      <label class="label">*Scheduled Analyses:</label> 
      <div class="divListSearchExtender">
        <ajaxToolkit:ListSearchExtender id="lseBulkDeleteScheduledAnalyses" runat="server" TargetControlID="lbBulkDeleteScheduledAnalyses" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true"/>   
      </div>

      <asp:ListBox ID="lbBulkDeleteScheduledAnalyses" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnBulkDeleteScheduledAnalyses" runat="server" Text="Delete Sch. Analyses"  OnClick="bulkDeleteScheduledAnalysesButton_Click" ValidationGroup="BulkDelete" />

  </div>

</asp:Panel>