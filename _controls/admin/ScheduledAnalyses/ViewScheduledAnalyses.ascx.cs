﻿using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.ScheduledAnalysis;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.ScheduledAnalyses
{
    public partial class ViewScheduledAnalyses : AdminUserControlGrid
    {
        #region constants

            const String deleteSuccessful = "Scheduled analysis deletion was successful.";
            const String deleteFailed = "Scheduled analysis deletion failed. Please contact an administrator.";
            const String updateSuccessful = "Scheduled analysis update was successful.";
            const String updateFailed = "Scheduled analysis update failed. Please contact an administrator.";

        #endregion

        #region fields

            private enum ScheduledAnalysisDetails { Default };
            static string scheduledAnalysisName = "scheduled analysis";

        #endregion

        #region properties

            #region overrides

                protected override Boolean ContainsDropDownListControl { get { return true; } }

                protected override DropDowns DropDownListControl { get { return gridDropDowns; } }

                protected override String CurrentControlName { get { return typeof(ViewScheduledAnalyses).Name; } }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    if (gridDropDowns.BID == null) return Enumerable.Empty<GetScheduledAnalysisData>();

                    var scheduledAnalyses = new ScheduledAnalyseQuery(DataMgr.ConnectionString, null).LoadWith(_ => _.Analyse, _ => _.Equipment, _ => _.Building).FinalizeLoadWith.GetAllForClient(siteUser.CID).ByBuilding(gridDropDowns.BID);
                    var hasSearchCriteria = !String.IsNullOrWhiteSpace(String.Join("", searchCriteria)); // TODO: NP: could control this higher up in the base 

                    return scheduledAnalyses.Queryable.Select(sa => new GetScheduledAnalysisData
                    {
                        EID = sa.EID,
                        EquipmentClassID = sa.Equipment.EquipmentType.EquipmentClassID,
                        SAID = sa.SAID,
                        AnalysisName = sa.Analyse.AnalysisName,
                        EquipmentName = sa.Equipment.EquipmentName,
                        BuildingName = sa.Building.BuildingName,
                        RunStartDate = sa.RunStartDate,
                        RunPostponeDate = sa.RunPostponeDate
                    }).ToList();
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetScheduledAnalysisData>(_ => _.EID, _ => _.EquipmentClassID, _ => _.SAID, _ => _.AnalysisName, _ => _.EquipmentName, _ => _.BuildingName, _ => _.RunStartDate, _ => _.RunPostponeDate); }
                }

                protected override String NameField { get { return PropHelper.G<GetScheduledAnalysisData>(_ => _.AnalysisName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetScheduledAnalysisData>(_ => _.SAID); } }

                protected override String Name { get { return scheduledAnalysisName; } }

                protected override Type Entity { get { return typeof(GetScheduledAnalysisData); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID, gridDropDowns.BID }); } }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(int cid, int bid) { return new GridCacher.CacheKeyInfo(scheduledAnalysisName + "." + typeof(GetScheduledAnalysisData).Name, string.Join("|", new string[] { cid.ToString(), bid.ToString() })); }

        #endregion

        #region events

        private void Page_FirstLoad()
            {
                if (AdminModeEnum == admin.AdminModeEnum.NONKGS) grid.Columns[grid.Columns.Count - 1].Visible = false;
            }

            #region grid events

                #region overrides

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var said = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());

                        SetScheduledAnalysisIntoEditForm(DataMgr.ScheduledAnalysisDataMapper.GetScheduledAnalysisByID(said));

                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                    protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
                    {
                        var said = Convert.ToInt32(grid.DataKeys[e.RowIndex].Value);

                        try
                        {
                            DataMgr.ScheduledAnalysisDataMapper.DeleteSchduledAnalysis(said);

                            ChangeCurrentTab(deleteSuccessful);
                            SetChangeStateForTabs(KGSScheduledAnalysesAdministration.TabMessages.DeleteScheduledAnalyses);
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblDeleteError, deleteFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting scheduled analyses.", ex);
                        }
                    }

                #endregion

            #endregion

            #region button events

                protected void updateScheduledAnalysisButton_Click(Object sender, EventArgs e)
                {
                    var mScheduledAnalysis = new ScheduledAnalyse();

                    LoadEditFormIntoScheduledAnalysis(mScheduledAnalysis);
         
                    try
                    {
                        DataMgr.ScheduledAnalysisDataMapper.UpdatePartialScheduledAnalysis(mScheduledAnalysis);
                        OperationComplete(OperationType.Update, updateSuccessful);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating partial scheduled analysis.", ex);
                    }
                }

            #endregion

            #region ddl events

                protected void GridDropDowns_BuildingChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    ResetUI();

                    if (gridDropDowns.BID == null) return;

                    if (gridDropDowns.ViewMode == DropDowns.ViewModeEnum.Building)
                    {
                        BindDataAndDisplayGrid();
                        return;
                    }

                    sender.BindEquipmentClassList((ea.SelectedValue == "0") ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses() : DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(gridDropDowns.BID.Value), true, true);
                }

                protected void GridDropDowns_EquipmentClassChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    ResetUI();

                    if (ea.SelectedValue == "-1") return;
                    if (gridDropDowns.EquipmentClassID != null) sender.BindEquipmentList(DataMgr.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(gridDropDowns.EquipmentClassID.Value, gridDropDowns.BID.Value));

                    BindDataAndDisplayGrid();
                }

                protected void GridDropDowns_EquipmentChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    ResetUI();

                    if (ea.SelectedValue == "-1") return;

                    BindDataAndDisplayGrid();
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var scheduledAnalysis = DataMgr.ScheduledAnalysisDataMapper.GetFullScheduledAnalysisByID(CurrentID).First();

                    SetScheduledAnalysisIntoViewState(scheduledAnalysis);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(ScheduledAnalysisDetails.Default), rptScheduledAnalysisDetails);
                }

            #endregion

            private void BindDataAndDisplayGrid()
            {
                BindData();
                grid.Visible = true;
            }

            private void SetScheduledAnalysisIntoViewState(GetScheduledAnalysisData scheduledAnalysis)
            {
                SetEquipmentPointDetails(scheduledAnalysis);
            }

            private void SetEquipmentPointDetails(GetScheduledAnalysisData scheduledAnalysis)
            { 
                SetKeyValuePairItem("Analysis Name", scheduledAnalysis.AnalysisName);
                SetKeyValuePairItem("Equipment Name", scheduledAnalysis.EquipmentName);
                SetKeyValuePairItem("Building Name", scheduledAnalysis.EquipmentName);
                SetKeyValuePairItem("Run Start Date", DateTime.Parse(scheduledAnalysis.RunStartDate.ToString()).ToShortDateString());

                if (scheduledAnalysis.RunPostponeDate != null) SetKeyValuePairItem("Run Postpone Date", DateTime.Parse(scheduledAnalysis.RunPostponeDate.ToString()).ToShortDateString());

                SetKeyValuePairItem("Run Half Day", scheduledAnalysis.RunHalfDay.ToString());
                SetKeyValuePairItem("Run Daily", scheduledAnalysis.RunDaily.ToString());
                SetKeyValuePairItem("Run Weekly", scheduledAnalysis.RunWeekly.ToString());
                SetKeyValuePairItem("Run Monthly", scheduledAnalysis.RunMonthly.ToString());

                SetDetailsViewDataToViewState(Convert.ToInt32(ScheduledAnalysisDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            private void SetScheduledAnalysisIntoEditForm(GetScheduledAnalysisData scheduledAnalysis)
            {
                hdnEditID.Value = Convert.ToString(scheduledAnalysis.SAID);
                lblEditBuildingName.Text = scheduledAnalysis.BuildingName;
                lblEditEquipmentName.Text = scheduledAnalysis.EquipmentName;
                lblEditAnalysisName.Text = scheduledAnalysis.AnalysisName;
                lblEditRunStartDate.Text = scheduledAnalysis.RunStartDate.ToShortDateString();
                txtEditRunPostponeDate.SelectedDate = scheduledAnalysis.RunPostponeDate;
                chkEditRunHalfDay.Checked = scheduledAnalysis.RunHalfDay;
                chkEditRunDaily.Checked = scheduledAnalysis.RunDaily;
                chkEditRunWeekly.Checked = scheduledAnalysis.RunWeekly;
                chkEditRunMonthly.Checked = scheduledAnalysis.RunMonthly;
            }

            private void LoadEditFormIntoScheduledAnalysis(ScheduledAnalyse scheduledAnalysis)
            {
                scheduledAnalysis.SAID = Convert.ToInt32(hdnEditID.Value);
                scheduledAnalysis.RunPostponeDate = txtEditRunPostponeDate.SelectedDate;
                scheduledAnalysis.RunHalfDay = chkEditRunHalfDay.Checked;
                scheduledAnalysis.RunDaily = chkEditRunDaily.Checked;
                scheduledAnalysis.RunWeekly = chkEditRunWeekly.Checked;
                scheduledAnalysis.RunMonthly = chkEditRunMonthly.Checked;
            }

        #endregion
    }
}