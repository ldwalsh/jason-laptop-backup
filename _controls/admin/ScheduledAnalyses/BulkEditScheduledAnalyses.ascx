﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BulkEditScheduledAnalyses.ascx.cs" Inherits="CW.Website._controls.admin.ScheduledAnalyses.BulkEditScheduledAnalyses" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Panel ID="pnlBulkEditScheduledAnalyses" runat="server" DefaultButton="btnBulkEditScheduledAnalyses">

  <h2>Bulk Edit Scheduled Analyses</h2>

  <p>Please select a client, building, and analysis in order select and bulk edit equipment schedulded analysis fields. This page only allows the bulk edit of the certain scheduled analysis fields. Try to limit your selection in order not to timeout.</p> 

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblBulkEditError" CssClass="errorMessage" runat="server" /> 

    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>
    <div class="divForm">   
      <label class="label">*Select Building:</label>    
      <asp:DropDownList ID="ddlBulkEditBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBulkEditBuildings_SelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <asp:RequiredFieldValidator ID="bulkEditBuildingRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBulkEditBuildings" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="BulkEdit" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditBuildingRequiredValidatorExtender" runat="server" BehaviorID="bulkEditBuildingRequiredValidatorExtender" TargetControlID="bulkEditBuildingRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <div class="divForm">   
      <label class="label">*Select Analysis:</label>
      <asp:DropDownList ID="ddlBulkEditAnalysis" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBulkEditAnalysis_SelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div> 
    <asp:RequiredFieldValidator ID="bulkEditAnalysisRequiredValidator" runat="server" ErrorMessage="Analysis is a required field." ControlToValidate="ddlBulkEditAnalysis" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="BulkEdit" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditAnalysisRequiredValidatorExtender" runat="server" BehaviorID="bulkEditAnalysisRequiredValidatorExtender" TargetControlID="bulkEditAnalysisRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
    
    <div class="divForm" runat="server">
      <label class="label">Available Equipment Scheduled Analyses:</label>

      <div class="divListSearchExtender">
        <ajaxToolkit:ListSearchExtender id="lseBulkEditEquipmentScheduledAnalysesTop" runat="server" TargetControlID="lbBulkEditEquipmentScheduledAnalysesTop" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true" />
      </div>
      <asp:ListBox ID="lbBulkEditEquipmentScheduledAnalysesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

      <div class="divArrows">
        <asp:ImageButton CssClass="up-arrow"  ID="ImageButton1"  OnClick="btnBulkEditEquipmentScheduledAnalysesUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
        <asp:ImageButton CssClass="down-arrow" ID="ImageButton2"  OnClick="btnBulkEditEquipmentScheduledAnalysesDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
      </div>

      <label class="label">*Selected Equipment Scheduled Analyses:</label> 
      <asp:ListBox ID="lbBulkEditEquipmentScheduledAnalysesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>
    <hr />

    <h2>Optional Bulk Edit Fields</h2>

    <p>Please check, and select or fill in all fields you wish to bulk edit. Leave any fields unchecked, blank, and unselected if you do not wish to bulk edit them.</p>

    <div class="divForm">
      <label class="label">Run Postpone Date:</label>
      <telerik:RadDatePicker ID="txtBulkEditRunPostponeDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>       
      <asp:CheckBox ID="chkBulkEditVerifyPostponeDate" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                 
    </div>
    <div class="divForm">
      <label class="label">New Run Half Day:</label>
      <asp:CheckBox ID="chkBulkEditHalfDay" CssClass="checkbox" runat="server" />|<asp:CheckBox ID="chkBulkEditVerifyHalfDay" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                          
    </div>
    <div class="divForm">
      <label class="label">New Run Daily:</label>
      <asp:CheckBox ID="chkBulkEditDaily" CssClass="checkbox" runat="server" />|<asp:CheckBox ID="chkBulkEditVerifyDaily" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                          
    </div>
    <div class="divForm">
      <label class="label">New Run Weekly:</label>
      <asp:CheckBox ID="chkBulkEditWeekly" CssClass="checkbox" runat="server" />|<asp:CheckBox ID="chkBulkEditVerifyWeekly" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                          
    </div>
    <div class="divForm">
      <label class="label">New Run Monthly:</label>
      <asp:CheckBox ID="chkBulkEditMonthly" CssClass="checkbox" runat="server" />|<asp:CheckBox ID="chkBulkEditVerifyMonthly" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>             
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnBulkEditScheduledAnalyses" runat="server" Text="Update Sch. Analyses"  OnClick="bulkEditScheduledAnalysesButton_Click" ValidationGroup="BulkEdit" />

  </div>

</asp:Panel>