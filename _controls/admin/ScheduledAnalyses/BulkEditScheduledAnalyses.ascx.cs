﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.ScheduledAnalyses
{
    public partial class BulkEditScheduledAnalyses : AdminUserControlBase
    {
        #region constants

            const String noBulkEditAnalysesSelected = "No bulk edit scheduled analyses were selected.";
            const String noBulkEditFieldsSelected = "No bulk edit fields were selected.";
            const String bulkEditFailed = "Bulk edit scheduled analyses failed. Please contact an administrator.";

        #endregion

        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get { return new Enum[] { KGSScheduledAnalysesAdministration.TabMessages.DeleteScheduledAnalyses }; }
                }

            #endregion

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblBulkEditError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindBuildingsDropdownList(ddlBulkEditBuildings);
                    BindBulkEditAnalysisDropdownList(ddlBulkEditAnalysis, -1);
                }
            }

            #region ddl events

                protected void ddlBulkEditBuildings_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    ClearBulkEditEquipmentScheduledAnalysesListBoxes();
                    BindBulkEditAnalysisDropdownList(ddlBulkEditAnalysis, Convert.ToInt32(ddlBulkEditBuildings.SelectedValue));
                }

                protected void ddlBulkEditAnalysis_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    ClearBulkEditEquipmentScheduledAnalysesListBoxes();

                    if (ddlBulkEditAnalysis.SelectedValue != "-1") BindBulkEditEquipmentScheduledAnalyses();
                }

            #endregion

            #region button events

                protected void btnBulkEditEquipmentScheduledAnalysesUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbBulkEditEquipmentScheduledAnalysesBottom.SelectedIndex != -1)
                    {
                        lbBulkEditEquipmentScheduledAnalysesTop.Items.Add(lbBulkEditEquipmentScheduledAnalysesBottom.SelectedItem);
                        lbBulkEditEquipmentScheduledAnalysesBottom.Items.Remove(lbBulkEditEquipmentScheduledAnalysesBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBulkEditEquipmentScheduledAnalysesTop);
                }

                protected void btnBulkEditEquipmentScheduledAnalysesDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbBulkEditEquipmentScheduledAnalysesTop.SelectedIndex != -1)
                    {
                        lbBulkEditEquipmentScheduledAnalysesBottom.Items.Add(lbBulkEditEquipmentScheduledAnalysesTop.SelectedItem);
                        lbBulkEditEquipmentScheduledAnalysesTop.Items.Remove(lbBulkEditEquipmentScheduledAnalysesTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBulkEditEquipmentScheduledAnalysesBottom);
                }

                protected void bulkEditScheduledAnalysesButton_Click(Object sender, EventArgs e)
                {
                    try
                    {
                        if (lbBulkEditEquipmentScheduledAnalysesBottom.Items.Count == 0)
                        {
                            LabelHelper.SetLabelMessage(lblBulkEditError, noBulkEditAnalysesSelected, lnkSetFocus);
                            return;
                        }
                        else
                        {
                            var checkBoxList = FindControls<CheckBox>(true);

                            if (!checkBoxList.Where(cbl => cbl.Checked).Any())
                            {
                                LabelHelper.SetLabelMessage(lblBulkEditError, noBulkEditFieldsSelected, lnkSetFocus);
                                return;
                            }
                            else
                            {
                                Int32 said;

                                lblResults.Text = String.Empty;

                                foreach (ListItem item in lbBulkEditEquipmentScheduledAnalysesBottom.Items)
                                {
                                    if (!Int32.TryParse(item.Value, out said)) { continue; }

                                    var mScheduledAnalysis = SetScheduledAnalysisFields(said);

                                    DataMgr.ScheduledAnalysisDataMapper.UpdatePartialScheduledAnalysis(mScheduledAnalysis, chkBulkEditVerifyPostponeDate.Checked, chkBulkEditVerifyHalfDay.Checked, chkBulkEditVerifyDaily.Checked,
                                                                                                                                chkBulkEditVerifyWeekly.Checked, chkBulkEditVerifyMonthly.Checked);

                                    lblResults.Text += item.Text + " has been successfully updated.<br />";
                                }

                                BindBulkEditEquipmentScheduledAnalyses();

                                SetChangeStateForTabs();

                                if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                            }
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblBulkEditError, bulkEditFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error during bulk edit scheduled analyses in kgs scheduled analyses administration.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblBulkEditError, bulkEditFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error during bulk edit scheduled analyses in kgs scheduled analyses administration.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    ClearBulkEditEquipmentScheduledAnalysesListBoxes();

                    var adminUserControlForm = new AdminUserControlForm(this);

                    adminUserControlForm.ClearTextBoxes();
                    adminUserControlForm.ClearCheckBoxes();
                    adminUserControlForm.ResetDropDownLists();

                    ddlBulkEditBuildings_SelectedIndexChanged(null, null);
                }

            #endregion

            private void ClearBulkEditEquipmentScheduledAnalysesListBoxes()
            {
                lbBulkEditEquipmentScheduledAnalysesTop.Items.Clear();
                lbBulkEditEquipmentScheduledAnalysesBottom.Items.Clear();
            }

            private ScheduledAnalyse SetScheduledAnalysisFields(Int32 said)
            {
                var scheduledAnalyse = new ScheduledAnalyse();

                scheduledAnalyse.SAID = said;
                scheduledAnalyse.RunPostponeDate = txtBulkEditRunPostponeDate.SelectedDate;
                scheduledAnalyse.RunHalfDay = chkBulkEditHalfDay.Checked;
                scheduledAnalyse.RunDaily = chkBulkEditDaily.Checked;
                scheduledAnalyse.RunWeekly = chkBulkEditWeekly.Checked;
                scheduledAnalyse.RunMonthly = chkBulkEditMonthly.Checked;

                return scheduledAnalyse;
            }

            #region binders

                private void BindBuildingsDropdownList(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindBulkEditAnalysisDropdownList(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "AnalysisName", "AID", (bid != -1) ? DataMgr.AnalysisDataMapper.GetAllAnalysesByBID(bid) : null, true, lItem);
                }

                private void BindBulkEditEquipmentScheduledAnalyses()
                {
                    var data = DataMgr.ScheduledAnalysisDataMapper.GetAllScheduledAnalysesWithPartialDataByBIDAndAID(Convert.ToInt32(ddlBulkEditBuildings.SelectedValue), Convert.ToInt32(ddlBulkEditAnalysis.SelectedValue));

                    lbBulkEditEquipmentScheduledAnalysesTop.DataSource = data;
                    lbBulkEditEquipmentScheduledAnalysesTop.DataTextField = "EquipmentName";
                    lbBulkEditEquipmentScheduledAnalysesTop.DataValueField = "SAID";
                    lbBulkEditEquipmentScheduledAnalysesTop.DataBind();

                    lbBulkEditEquipmentScheduledAnalysesBottom.Items.Clear();
                }

            #endregion

        #endregion
    }
}