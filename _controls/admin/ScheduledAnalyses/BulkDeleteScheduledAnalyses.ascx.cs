﻿using CW.Common.Constants;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.ScheduledAnalyses
{
    public partial class BulkDeleteScheduledAnalyses : AdminUserControlBase
    {
        #region constants

            const String bulkDeleteFailed = "Bulk delete scheduled analyses failed. Please contact an administrator.";

        #endregion

        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get { return new Enum[] { KGSScheduledAnalysesAdministration.TabMessages.DeleteScheduledAnalyses }; }
                }

            #endregion

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblBulkDeleteError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindBuildingsDropdownList(ddlBulkDeleteBuildings);
                    BindBulkDeleteAnalysisDropdownList(ddlBulkDeleteAnalysis, -1);
                }
            }

            #region ddl events

                protected void ddlBulkDeleteBuildings_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindBulkDeleteAnalysisDropdownList(ddlBulkDeleteAnalysis, Convert.ToInt32(ddlBulkDeleteBuildings.SelectedValue));
                    ddlBulkDeleteAnalysis_SelectedIndexChanged(null, null);
                }

                protected void ddlBulkDeleteAnalysis_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    var bid = Convert.ToInt32(ddlBulkDeleteBuildings.SelectedValue);
                    var aid = Convert.ToInt32(ddlBulkDeleteAnalysis.SelectedValue);

                    if (bid != -1)
                        BindBulkDeleteScheduledAnalysesListBox(lbBulkDeleteScheduledAnalyses, bid, aid);
                    else
                        lbBulkDeleteScheduledAnalyses.Items.Clear();
                }

            #endregion

            #region button events

                protected void bulkDeleteScheduledAnalysesButton_Click(Object sender, EventArgs e)
                {
                    var bid = Convert.ToInt32(ddlBulkDeleteBuildings.SelectedValue);
                    var aid = Convert.ToInt32(ddlBulkDeleteAnalysis.SelectedValue);

                    try
                    {
                        foreach (ListItem item in lbBulkDeleteScheduledAnalyses.Items)
                        {
                            Int32 said;

                            if (item.Selected && Int32.TryParse(item.Value, out said))
                            {
                                DataMgr.ScheduledAnalysisDataMapper.DeleteSchduledAnalysis(said);
                                lblResults.Text += item.Text + " has been successfully deleted.<br />";
                            }
                        }

                        BindBulkDeleteScheduledAnalysesListBox(lbBulkDeleteScheduledAnalyses, bid, aid);

                        if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblBulkDeleteError, bulkDeleteFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error during bulk delete scheduled analyses in kgs scheduled analyses administration.", sqlEx);
                        
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblBulkDeleteError, bulkDeleteFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error during bulk delete scheduled analyses in kgs scheduled analyses administration.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();
                    ddlBulkDeleteBuildings_SelectedIndexChanged(null, null);
                    lbBulkDeleteScheduledAnalyses.Items.Clear();
                }

            #endregion

            #region binders

                private void BindBuildingsDropdownList(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindBulkDeleteAnalysisDropdownList(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "AnalysisName", "AID", (bid != -1) ? DataMgr.AnalysisDataMapper.GetAllAnalysesByBID(bid) : null, true, lItem);
                }

                private void BindBulkDeleteScheduledAnalysesListBox(ListBox lb, Int32 bid, Int32 aid)
                {
                    lb.Items.Clear();

                    var data = (aid == -1) ? DataMgr.ScheduledAnalysisDataMapper.GetAllScheduledAnalysesWithListboxFormatByBID(bid) : DataMgr.ScheduledAnalysisDataMapper.GetAllScheduledAnalysesWithListboxFormatByBIDAndAID(bid, aid);

                    ControlHelper.BindListBox(lb, "ScheduledAnalysisConcatinated", "SAID", data);
                }

            #endregion

        #endregion
    }
}