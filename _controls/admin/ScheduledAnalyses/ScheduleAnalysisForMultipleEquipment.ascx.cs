﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.ScheduledAnalyses
{
    public partial class ScheduleAnalysisForMultipleEquipment : AdminUserControlBase
    {
        #region constants

            const String mulitScheduledAnalysisExists = "One or more Scheduled Analyses already exist for equipment ";
            const String mulitNotAssociatedWithEquipment = "One or more Scheduled Analyses cannot be scheduled because the analysis is not associated with eqiupment ";
            const String addScheduledAnalysisSuccess = "Scheduled analysis addition was successful for equipment ";
            const String addScheduledAnalysisFailed = "Adding scheduled analysis failed. Please contact an administrator.";

        #endregion

        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get { return new Enum[] { KGSScheduledAnalysesAdministration.TabMessages.DeleteScheduledAnalyses }; }
                }

            #endregion

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblMultiAddError.Visible = false;
                
                if (!Page.IsPostBack)
                {
                    BindBuildingsDropdownList(ddlMultiAddBuildings);
                    BindMultiAddAnalysisDropdownList(ddlMultiAddAnalysis, -1);

                    InitializeControl();
                }
            }

            #region ddl events

                protected void ddlMultiAddBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    lbMultiAddEquipment.Items.Clear();
                    ddlMultiAddAnalysis.SelectedIndex = 0;

                    if (ddlMultiAddBuildings.SelectedValue != "-1") BindMultiAddAnalysisDropdownList(ddlMultiAddAnalysis, Convert.ToInt32(ddlMultiAddBuildings.SelectedValue));
                }

                protected void ddlMultiAddAnalysis_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    lbMultiAddEquipment.Items.Clear();

                    if (ddlMultiAddAnalysis.SelectedValue != "-1" && ddlMultiAddBuildings.SelectedValue != "-1") BindMultiAddEquipmentListBox(Convert.ToInt32(ddlMultiAddBuildings.SelectedValue), Convert.ToInt32(ddlMultiAddAnalysis.SelectedValue));
                }

            #endregion

            #region button events

                protected void multiAddScheduledAnalysisButton_Click(Object sender, EventArgs e)
                {
                    var bid = Convert.ToInt32(ddlMultiAddBuildings.SelectedValue);
                    var aid = Convert.ToInt32(ddlMultiAddAnalysis.SelectedValue);

                    try
                    {
                        lblMultiAddError.Text = String.Empty;
                        lblResults.Text = String.Empty;

                        foreach (ListItem equipmentItem in lbMultiAddEquipment.Items)
                        {
                            var eid = 0;

                            if (equipmentItem.Selected && Int32.TryParse(equipmentItem.Value, out eid))
                            {
                                var equipmentName = equipmentItem.Text;

                                if (DataMgr.ScheduledAnalysisDataMapper.DoesScheduledAnalysisExist(eid, Convert.ToInt32(ddlMultiAddAnalysis.SelectedValue)))
                                    lblMultiAddError.Text += mulitScheduledAnalysisExists + equipmentName + "<br />";
                                else
                                {
                                    //check if the analysis is associated with the equipment before we can schedule it.
                                    //since we are doing a bulk assign, all analyses show regardless if they are associated with the equipment.
                                    if (!DataMgr.AnalysisEquipmentDataMapper.IsAnalysisAssignedToEquipment(eid, Convert.ToInt32(ddlMultiAddAnalysis.SelectedValue)))
                                        lblMultiAddError.Text += mulitNotAssociatedWithEquipment + equipmentName + "<br />";
                                    else
                                    {
                                        var mScheduledAnalysis = new ScheduledAnalyse();

                                        LoadMultiAddFormIntoScheduledAnalysis(mScheduledAnalysis, eid);

                                        DataMgr.ScheduledAnalysisDataMapper.InsertScheduledAnalysis(mScheduledAnalysis);
                                        lblResults.Text += addScheduledAnalysisSuccess + equipmentName + "<br />";
                                    }
                                }
                            }
                        }

                        BindMultiAddEquipmentListBox(bid, aid);

                        if (lblMultiAddError.Text != String.Empty) LabelHelper.SetLabelMessage(lblMultiAddError, lblMultiAddError.Text, lnkSetFocus);
                        if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblMultiAddError, addScheduledAnalysisFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding scheduled analysis.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblMultiAddError, addScheduledAnalysisFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding scheduled analysis.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();

                    ddlMultiAddAnalysis_OnSelectedIndexChanged(null, null);

                    txtMultiAddRunStartDate.SelectedDate = DateTime.UtcNow;

                    chkMultiAddRunHalfDay.Checked = false;
                    chkMultiAddRunDaily.Checked = true;
                    chkMultiAddRunWeekly.Checked = true;
                    chkMultiAddRunMonthly.Checked = true;
                }

            #endregion

            protected void LoadMultiAddFormIntoScheduledAnalysis(ScheduledAnalyse scheduledAnalysis, Int32 eid)
            {
                scheduledAnalysis.BID = Convert.ToInt32(ddlMultiAddBuildings.SelectedValue);
                scheduledAnalysis.EID = Convert.ToInt32(eid);
                scheduledAnalysis.AID = Convert.ToInt32(ddlMultiAddAnalysis.SelectedValue);
                scheduledAnalysis.RunStartDate = (DateTime)txtMultiAddRunStartDate.SelectedDate;
                scheduledAnalysis.RunHalfDay = chkMultiAddRunHalfDay.Checked;
                scheduledAnalysis.RunDaily = chkMultiAddRunDaily.Checked;
                scheduledAnalysis.RunWeekly = chkMultiAddRunWeekly.Checked;
                scheduledAnalysis.RunMonthly = chkMultiAddRunMonthly.Checked;
                scheduledAnalysis.DateModified = DateTime.UtcNow;
            }

            #region binders

                private void BindBuildingsDropdownList(DropDownList ddl)
                {
                    var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    ddl.DataTextField = "BuildingName";
                    ddl.DataValueField = "BID";
                    ddl.DataSource = siteUser.VisibleBuildings;
                    ddl.DataBind();
                }

                private void BindMultiAddAnalysisDropdownList(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    if (bid != -1)
                    {
                        ddl.DataTextField = "AnalysisName";
                        ddl.DataValueField = "AID";
                        ddl.DataSource = DataMgr.AnalysisDataMapper.GetAllActiveAnalysesByBID(bid);
                        ddl.DataBind();
                    }
                }

                private void BindMultiAddEquipmentListBox(Int32 bid, Int32 aid)
                {
                    lbMultiAddEquipment.Items.Clear();

                    lbMultiAddEquipment.DataTextField = "EquipmentName";
                    lbMultiAddEquipment.DataValueField = "EID";
                    lbMultiAddEquipment.DataSource = DataMgr.EquipmentDataMapper.GetAllActiveEquipmentNotScheduledByAnalysis(bid, aid);
                    lbMultiAddEquipment.DataBind();
                }

            #endregion

        #endregion
    }
}