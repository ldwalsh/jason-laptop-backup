﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ScheduleAnalysisForAnEquipment.ascx.cs" Inherits="CW.Website._controls.admin.ScheduledAnalyses.ScheduleAnalysisForAnEquipment" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/DropDowns.ascx" tagname="DropDowns" tagprefix="CW" %>

<asp:Panel ID="pnlAddScheduledAnalysis" runat="server" DefaultButton="btnAddScheduledAnalysis">

  <h2>Schedule Analysis For An Equipment</h2>

  <p>Please select a building then a peice of equipment in order to select and schedule an analysis. Inactive or already scheduled analyses will not appear in the below dropdowns. Utility billing analyses will not apper in the below dropdowns.</p>

  <div>                
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />
    
    <div class="divForm">
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>

    <CW:DropDowns ID="dropDowns" validationEnabled="true" StartViewMode="Building" ViewMode="Analysis" ValidationGroup="AddScheduledAnalysis" runat="server" OnBuildingChanged="ddlCW_BuildingChanged" OnEquipmentClassChanged="ddlCW_EquipmentClassChanged" OnEquipmentChanged="ddlCW_EquipmentChanged" />

    <div class="divForm">
      <label class="label">*Run Start Date:</label>
      <telerik:RadDatePicker ID="txtAddRunStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>      
      <br />
      <asp:RequiredFieldValidator ID="addRunStartDateRequiredValidator" runat="server" CssClass="errorMessage" ErrorMessage="Date is a required field." ControlToValidate="txtAddRunStartDate" SetFocusOnError="true" Display="None" ValidationGroup="AddScheduledAnalysis" />      
    </div>
    <div class="divForm">
      <label class="label">*Run Half Day:</label>
      <asp:CheckBox ID="chkAddRunHalfDay" CssClass="checkbox" runat="server" />                                                                                                    
    </div>
    <div class="divForm">
      <label class="label">*Run Daily:</label>
      <asp:CheckBox ID="chkAddRunDaily" Checked="true" CssClass="checkbox" runat="server" />                                                                                                    
    </div>
    <div class="divForm">
      <label class="label">*Run Weekly:</label>
      <asp:CheckBox ID="chkAddRunWeekly" Checked="true" CssClass="checkbox" runat="server" />                                                                                                    
    </div>
    <div class="divForm">
      <label class="label">*Run Monthly:</label>
      <asp:CheckBox ID="chkAddRunMonthly" Checked="true" CssClass="checkbox" runat="server" />                                                                                                    
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnAddScheduledAnalysis" runat="server" Text="Add Analysis"  OnClick="addScheduledAnalysisButton_Click" ValidationGroup="AddScheduledAnalysis" />

  </div>
  <br />

</asp:Panel>