﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ScheduleAnalysisForMultipleEquipment.ascx.cs" Inherits="CW.Website._controls.admin.ScheduledAnalyses.ScheduleAnalysisForMultipleEquipment" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Panel ID="pnlMultiAddScheduledAnalysis" runat="server" DefaultButton="btnMultiAddScheduledAnalysis">

  <h2>Schedule Analysis For Multiple Equipment</h2>

  <p>Please select an analysis then equipment in order to schedule an analysis for multiple equipment. Inactive or already scheduled analyses will not appear in the below dropdowns.</p>
  
  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblMultiAddError" CssClass="errorMessage" runat="server" />                                                                   

    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div> 
    <div class="divForm">   
      <label class="label">*Select Building:</label>    
      <asp:DropDownList ID="ddlMultiAddBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlMultiAddBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />    
    </div>
    <div class="divForm">   
      <label class="label">*Select Analysis:</label>    
      <asp:DropDownList ID="ddlMultiAddAnalysis" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlMultiAddAnalysis_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />    
    </div>
    <div class="divForm">
      <label class="label">*Select Equipment:</label>    
      <asp:ListBox ID="lbMultiAddEquipment" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>                                                                                                                                                                                                                                     
    <div class="divForm">
      <label class="label">*Run Start Date:</label>
      <telerik:RadDatePicker ID="txtMultiAddRunStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>      
      <br /> 
      <asp:RequiredFieldValidator ID="multiAddRunStartDateRequiredValidator" runat="server" CssClass="errorMessage" ErrorMessage="Date is a required field." ControlToValidate="txtMultiAddRunStartDate" SetFocusOnError="true" Display="None" ValidationGroup="MultiAddScheduledAnalysis" />      
    </div>
    <div class="divForm">
      <label class="label">*Run Half Day:</label>
      <asp:CheckBox ID="chkMultiAddRunHalfDay" CssClass="checkbox" runat="server" />                                                                                                    
    </div>
    <div class="divForm">
      <label class="label">*Run Daily:</label>
      <asp:CheckBox ID="chkMultiAddRunDaily" Checked="true" CssClass="checkbox" runat="server" />                                                                                                    
    </div>
    <div class="divForm">
      <label class="label">*Run Weekly:</label>
      <asp:CheckBox ID="chkMultiAddRunWeekly" Checked="true" CssClass="checkbox" runat="server" />                                                                                                    
    </div>
    <div class="divForm">
      <label class="label">*Run Monthly:</label>
      <asp:CheckBox ID="chkMultiAddRunMonthly" Checked="true" CssClass="checkbox" runat="server" />                                                                                                    
    </div>
         
    <asp:LinkButton CssClass="lnkButton" ID="btnMultiAddScheduledAnalysis" runat="server" Text="Add Analysis"  OnClick="multiAddScheduledAnalysisButton_Click" ValidationGroup="MultiAddScheduledAnalysis" />

  </div>

  <!--Ajax Validators--> 
  <asp:RequiredFieldValidator ID="buildingMultiAddRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlMultiAddBuildings" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="MultiAddScheduledAnalysis" />
  <ajaxToolkit:ValidatorCalloutExtender ID="buildingMultiAddRequiredValidatorExtender" runat="server" BehaviorID="buildingMultiAddRequiredValidatorExtender" TargetControlID="buildingMultiAddRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                                                                                     
  <%--<asp:RequiredFieldValidator ID="analysisMultiAddRequiredValidator" runat="server" ErrorMessage="Analysis is a required field." ControlToValidate="ddlMultiAddAnalysis" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="MultiAddScheduledAnalysis" />
  <ajaxToolkit:ValidatorCalloutExtender ID="analysisMultiAddRequiredValidatorExtender" runat="server" BehaviorID="analysisMultiAddRequiredValidatorExtender" TargetControlID="analysisMultiAddRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />--%>

  <asp:RequiredFieldValidator ID="equipmentMultiAddRequiredValidator" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="lbMultiAddEquipment" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="MultiAddScheduledAnalysis" />
  <ajaxToolkit:ValidatorCalloutExtender ID="equipmentMultiAddRequiredValidatorExtender" runat="server" BehaviorID="equipmentMultiAddRequiredValidatorExtender" TargetControlID="equipmentMultiAddRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <br />

</asp:Panel>                                            