﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using CW.Website._framework;
using Telerik.Web.UI;
using CW.Utility.Web;

namespace CW.Website._controls.admin
{
    public class AdminDropDowns : SiteUserControl
    {
        #region fields

            private String bid, eid;
            private Boolean containsQSvalues;
            private RadTabStrip mRadTabStrip;
            private RadMultiPage mRadMultiPage;
            private RadioButtonList mRblMode;
            private AdminUserControlTabs mAdminUserControlTabs;
            private Label mLblResults;

        #endregion

        #region constructor

            public AdminDropDowns(RadTabStrip radTabStrip, RadMultiPage radMultiPage, Label lblResults, RadioButtonList rblMode = null)
            {
                if (radTabStrip != null) mRadTabStrip = radTabStrip;
                if (radMultiPage != null) mRadMultiPage = radMultiPage;
                if (lblResults != null) mLblResults = lblResults;
                if (rblMode != null) mRblMode = rblMode;

                var url = HttpContext.Current.Request.Url.PathAndQuery;
                var index = url.IndexOf('?');

                if (index > 0)
                {
                    var qs = new QueryString(url.Substring(index + 1));
                    var qsBID = qs.Get("bid", typeof(String));
                    var qsEID = qs.Get("eid", typeof(String));

                    bid = (qsBID != null) ? qsBID.ToString() : null;
                    eid = (qsEID != null) ? qsEID.ToString() : null;

                    containsQSvalues = (bid != null || eid != null);
                }

                mAdminUserControlTabs = new AdminUserControlTabs(mRadTabStrip, mRadMultiPage);
            }

        #endregion

        #region methods

            public Boolean HideTabMessage(DropDowns.ViewModeEnum viewMode, String currentControl)
            {
                if (containsQSvalues)
                {
                    if (mAdminUserControlTabs.IsCurrentTabActive(currentControl))
                    {
                        switch (viewMode)
                        {
                            case DropDowns.ViewModeEnum.Building:
                                return (bid == null);
                            case DropDowns.ViewModeEnum.EquipmentClass:
                            case DropDowns.ViewModeEnum.Equipment:
                                return (eid == null);
                            default:
                                return false;
                        }
                    }
                }

                return true;
            }

            public void PopulateAndSetDropDownLists(DropDowns dropDowns, String currentControl)
            {
                var buildings = siteUser.VisibleBuildings;

                dropDowns.BindBuildingList(buildings);

                if (containsQSvalues)
                {
                    if (bid != null)
                    {
                        if (mAdminUserControlTabs.IsCurrentTabActive(currentControl))
                        {
                            SetViewMode(dropDowns, DropDowns.ViewModeEnum.Building);
                            dropDowns.BID = Convert.ToInt32(bid);

                            if (eid != null)
                            {
                                var ecid = DataMgr.EquipmentClassDataMapper.GetEquipmentClassIDByEID(Convert.ToInt32(eid));

                                SetViewMode(dropDowns, DropDowns.ViewModeEnum.EquipmentClass);
                                dropDowns.BindEquipmentClassList(DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(dropDowns.BID.Value));
                                dropDowns.EquipmentClassID = ecid;

                                SetViewMode(dropDowns, DropDowns.ViewModeEnum.Equipment);
                                dropDowns.BindEquipmentList(DataMgr.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(ecid, dropDowns.BID.Value));
                                dropDowns.EID = Convert.ToInt32(eid);
                            }
                        }
                    }
                }
            }

            public void SetBuildingDropDownListAndTabIndex(DropDownList ddlBuilding, String currentControl)
            {
                //TODO: possibly check if the physical item exists, ie create a list item of bid/building name and check via contains
                if (bid != null)
                {
                    if (mAdminUserControlTabs.IsCurrentTabActive(currentControl)) ddlBuilding.SelectedValue = bid.ToString();
                }
            }

            public void SetEquipmentDropDownList(DropDownList ddlEquipment, String currentControl)
            {
                //TODO: possibly check if the physical item exists, ie create a list item of eid/equipment name and check via contains
                if (eid != null)
                {
                    if (mAdminUserControlTabs.IsCurrentTabActive(currentControl)) ddlEquipment.SelectedValue = eid.ToString();
                }
            }

            private void SetViewMode(DropDowns dropDowns, DropDowns.ViewModeEnum viewMode)
            {
                if (mRblMode != null) mRblMode.SelectedValue = Enum.GetName(typeof(DropDowns.ViewModeEnum), viewMode);

                dropDowns.ViewMode = viewMode;
            }

        #endregion
    }
}