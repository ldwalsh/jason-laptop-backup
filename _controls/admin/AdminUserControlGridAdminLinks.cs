﻿using CW.Common.Constants;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin
{
    public abstract class AdminUserControlGridAdminLinks : AdminUserControlGrid
    {
        #region fields

            private Lazy<Dictionary<AdminPage, KeyValuePair<String, String>>> mAdminPages;
            protected enum LinkTypes { DropDownLink, GridLink };
            private Lazy<Dictionary<AdminPage, List<KeyValuePair<String, Object>>>> mDictionaryItems;

        #endregion

        #region enums

            public enum AdminPage
            {
                AnalysesToEquipment,
                AssociatedPoints,
                Buildings,
                BuildingVariables,
                Diagnostics,
                Equipment,
                EquipmentToEquipment,
                EquipmentVariables,
                Points,
                ScheduledAnalyses
            }

        #endregion

        #region constructor

            public AdminUserControlGridAdminLinks()
            {
                mDictionaryItems = new Lazy<Dictionary<AdminPage, List<KeyValuePair<String, Object>>>>(() => GetLinksAndItemsToAdd());
                mAdminPages = new Lazy<Dictionary<AdminPage, KeyValuePair<String, String>>>(() => GetAdminPages());
            }

        #endregion

        #region properties

            #region abstract properties

                protected abstract LinkTypes LinkType { get; }

            #endregion

            private Dictionary<AdminPage, List<KeyValuePair<String, Object>>> LinksAndItemsToAdd { get { return mDictionaryItems.Value; } }

            private Dictionary<AdminPage, KeyValuePair<String, String>> AdminPages { get { return mAdminPages.Value; } }

        #endregion

        #region events

            #region grid events

                #region virtual grid events

                    protected override void OnGridDataBound(Object sender, GridViewRowEventArgs e)
                    {
                        if (LinksAndItemsToAdd != null)
                        {
                            if (e.Row.RowType == DataControlRowType.DataRow)
                            {
                                if ((e.Row.RowState == DataControlRowState.Normal) || (e.Row.RowState == DataControlRowState.Alternate))
                                {
                                    if (LinkType == LinkTypes.DropDownLink)
                                        CreateAdminLinksForDDLItems(e.Row, "ddlAction");
                                    else
                                        CreateAdminLinksForGridItems(e.Row);
                                }
                            }
                        }
                    }

                #endregion

            #endregion

        #endregion

        #region methods

            #region abstract methods

                protected abstract Dictionary<AdminPage, List<KeyValuePair<String, Object>>> GetLinksAndItemsToAdd();

            #endregion

            private Dictionary<AdminPage, KeyValuePair<String, String>> GetAdminPages()
            {
                var adminPages = new Dictionary<AdminPage, KeyValuePair<String, String>>();
                var diagnosticsPage = BusinessConstants.AdminPageConstants.Diagnostics;
                var buildingsAdministrationPage = (AdminModeEnum == admin.AdminModeEnum.KGS) ? BusinessConstants.AdminPageConstants.KGSBuildingAdministration : BusinessConstants.AdminPageConstants.ProviderBuildingAdministration;
                var equipmentAdministrationPage = (AdminModeEnum == admin.AdminModeEnum.KGS) ? BusinessConstants.AdminPageConstants.KGSEquipmentAdministration : BusinessConstants.AdminPageConstants.ProviderEquipmentAdministration;
                var pointAdministrationPage = (AdminModeEnum == admin.AdminModeEnum.KGS) ? BusinessConstants.AdminPageConstants.KGSPointAdministration : BusinessConstants.AdminPageConstants.ProviderPointAdministration;
                var scheduledAnalysesAdministrationPage = (AdminModeEnum == admin.AdminModeEnum.KGS) ? BusinessConstants.AdminPageConstants.KGSScheduledAnalysesAdministration : BusinessConstants.AdminPageConstants.ProviderScheduledAnalysesAdministration;

                adminPages.Add(AdminPage.AnalysesToEquipment, new KeyValuePair<String, String>("Analyses To Equipment", equipmentAdministrationPage));
                adminPages.Add(AdminPage.Buildings, new KeyValuePair<String, String>("Buildings", buildingsAdministrationPage));
                adminPages.Add(AdminPage.BuildingVariables, new KeyValuePair<String, String>("Building Variables", buildingsAdministrationPage));

                if (siteUser.Modules.Where(m => m.ModuleID == (Int32)BusinessConstants.Module.Modules.Diagnostics).Any()) adminPages.Add(AdminPage.Diagnostics, new KeyValuePair<String, String>("Diagnostics", diagnosticsPage));

                adminPages.Add(AdminPage.Equipment, new KeyValuePair<String, String>("Equipment", equipmentAdministrationPage));
                adminPages.Add(AdminPage.EquipmentToEquipment, new KeyValuePair<String, String>("Equipment To Equipment", equipmentAdministrationPage));
                adminPages.Add(AdminPage.EquipmentVariables, new KeyValuePair<String, String>("Equipment Variables", equipmentAdministrationPage));
                adminPages.Add(AdminPage.Points, new KeyValuePair<String, String>("Points", pointAdministrationPage));
                adminPages.Add(AdminPage.AssociatedPoints, new KeyValuePair<String, String>("Associated Points", pointAdministrationPage));
                adminPages.Add(AdminPage.ScheduledAnalyses, new KeyValuePair<String, String>("Scheduled Analyses", scheduledAnalysesAdministrationPage));
                
                return adminPages;
            }
            
            private void CreateAdminLinksForDDLItems(GridViewRow row, String ddlID)
            {
                var ddl = row.FindControl(ddlID);

                if (ddl != null)
                {
                    var defaultItem = new ListItem() { Text = String.Empty, Value = "-1", Selected = true };
                    var select = (HtmlSelect)ddl;

                    select.Items.Add(defaultItem);
                    select.Attributes.Add("onchange", "OpenUrlInNewWindow(this)");

                    foreach (var key in LinksAndItemsToAdd.Keys)
                    {
                        if (AdminPages.ContainsKey(key))
                        {
                            var listItem = new ListItem();
                            var keyValuePairItems = GetKeyValuePairItems(key, row);

                            listItem.Text = AdminPages[key].Key;
                            listItem.Value = BuildAdminQuickLink(AdminPages[key].Value, keyValuePairItems);

                            select.Items.Add(listItem);
                        }
                    }
                }
            }

            private void CreateAdminLinksForGridItems(GridViewRow row)
            {
                foreach (var key in LinksAndItemsToAdd.Keys)
                {
                    var hyperLinkControl = (HyperLink)row.FindControl("hl" + Enum.GetName(typeof(AdminPage), key));

                    if (!AdminPages.ContainsKey(key))
                    {
                        foreach (TableCell cell in row.Cells)
                        {
                            if (cell.Controls.Contains(hyperLinkControl))
                            {
                                var cellIndex = row.Cells.GetCellIndex(cell);

                                grid.Columns[cellIndex].Visible = false;
                            }
                        }

                        continue;
                    }

                    var keyValuePairItems = GetKeyValuePairItems(key, row);

                    hyperLinkControl.NavigateUrl = BuildAdminQuickLink(AdminPages[key].Value, keyValuePairItems);
                    hyperLinkControl.Target = "_blank";
                }
            }

            private List<KeyValuePair<String, Object>> GetKeyValuePairItems(AdminPage key, GridViewRow row)
            {
                var keyValuePairItems = new List<KeyValuePair<String, Object>>();
                var items = LinksAndItemsToAdd[key];

                foreach (var item in items)
                {
                    if (item.Value == null) //if the value is null, that means we need to retrieve the value from the grid item
                    {
                        if (((GridCacher.GridSet.Row)row.DataItem).Cells.ContainsKey(item.Key)) keyValuePairItems.Add(new KeyValuePair<String, Object>(item.Key, ((GridCacher.GridSet.Row)row.DataItem).Cells[item.Key]));
                    }
                    else
                        keyValuePairItems.Add(new KeyValuePair<String, Object>(item.Key, item.Value));
                }

                return keyValuePairItems;
            }

            private String BuildAdminQuickLink(String adminPage, List<KeyValuePair<String, Object>> keyValuePairItems)
            {
                var page = adminPage;

                if (page != null)
                {
                    page = "/" + page + "?";

                    foreach (var keyValuePairItem in keyValuePairItems)
                        QueryString.AppendToQueryString(ref page, keyValuePairItem.Key, keyValuePairItem.Value);
                }

                return page;
            }

        #endregion
    }
}