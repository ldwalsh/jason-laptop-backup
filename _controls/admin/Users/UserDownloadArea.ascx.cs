﻿using CW.Data;
using CW.Data.Models.User;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._downloads;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Users
{
    public partial class UserDownloadArea : DownloadAreaBase<GetUsersData, UserDownloadGenerator>
    {
        #region fields

            private String fnOid = PropHelper.G<Organization>(_ => _.OID);

        #endregion

        #region properties

            #region overrides

                protected override Func<Dictionary<String, Object>, IEnumerable<GetUsersData>> GetData { get { return GetAllUserDataByOid; } }

                protected override Dictionary<String, Object> Parameters { get { return GetSelectedOrDefaultValues(); } }

            #endregion

        #endregion

        #region methods

            private Dictionary<String, Object> GetSelectedOrDefaultValues()
            {
                var items = new Dictionary<String, Object>();
                var organization = (DropDownList == null) ? DataMgr.OrganizationDataMapper.GetOrganizationByCID(siteUser.CID) : null;

                items.Add(fnOid, (organization != null) ? organization.OID.ToString() : DropDownList.SelectedValue);
                items.Add(PropHelper.G<Organization>(_ => _.OrganizationName), (organization != null) ? organization.OrganizationName : DropDownList.SelectedItem.Text);

                return items;
            }

            public IEnumerable<GetUsersData> GetAllUserDataByOid(Dictionary<String, Object> parameters)
            {
                var oid = Convert.ToInt32(parameters[fnOid]);

                return DataMgr.UserDataMapper.GetAllPartialUsersByOIDWithPrimaryRole(oid, IsKGSUserAdminPageValue, IsKGSUserAdminPageValue);
            }

        #endregion
    }
}