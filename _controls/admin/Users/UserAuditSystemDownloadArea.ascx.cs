﻿using CW.Data;
using CW.Data.Models.User;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._downloads;
using System;
using System.Collections.Generic;

namespace CW.Website._controls.admin.Users
{
    public partial class UserAuditSystemDownloadArea : DownloadAreaBase<GetUsersAuditData, UserAuditSystemDownloadGenerator>
    {
        #region properties

            #region overrides

                protected override Func<Dictionary<String, Object>, IEnumerable<GetUsersAuditData>> GetData { get { return GetAllUserAuditData; } }

                protected override Dictionary<String, Object> Parameters { get { return GetDefaultValues(); } }

            #endregion

        #endregion

        #region methods

            private Dictionary<String, Object> GetDefaultValues()
            {
                var items = new Dictionary<String, Object>();
                var organization = DataMgr.OrganizationDataMapper.GetOrganizationByCID(siteUser.CID);

                items.Add(PropHelper.G<Organization>(_ => _.OrganizationName), organization.OrganizationName);

                return items;
            }

            public IEnumerable<GetUsersAuditData> GetAllUserAuditData(Dictionary<String, Object> parameters) { return DataMgr.UserAuditDataMapper.GetAllUsersAudits(); } //parameters are not used here, but method signature must match. Refine?

        #endregion
    }
}