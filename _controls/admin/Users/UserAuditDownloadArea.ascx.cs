﻿using CW.Data;
using CW.Data.Models.User;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._downloads;
using System;
using System.Collections.Generic;

namespace CW.Website._controls.admin.Users
{
    public partial class UserAuditDownloadArea : DownloadAreaBase<GetUsersAuditData, UserAuditDownloadGenerator>
    {
        #region fields

            private String fnOid = PropHelper.G<Organization>(_ => _.OID);

        #endregion

        #region properties

            #region overrides

                protected override Func<Dictionary<String, Object>, IEnumerable<GetUsersAuditData>> GetData { get { return GetAllUserAuditDataByOid; } }

                protected override Dictionary<String, Object> Parameters { get { return GetDefaultValues(); } }

            #endregion

        #endregion

        #region methods

            private Dictionary<String, Object> GetDefaultValues()
            {
                var items = new Dictionary<String, Object>();
                var organization = DataMgr.OrganizationDataMapper.GetOrganizationByCID(siteUser.CID);

                items.Add(fnOid, organization.OID);
                items.Add(PropHelper.G<Organization>(_ => _.OrganizationName), organization.OrganizationName);

                return items;
            }

            public IEnumerable<GetUsersAuditData> GetAllUserAuditDataByOid(Dictionary<String, Object> parameters)
            {
                var oid = Convert.ToInt32(parameters[fnOid]);

                return DataMgr.UserAuditDataMapper.GetAllUsersAuditsByOID(oid, IsKGSUserAdminPageValue);
            }

        #endregion
    }
}