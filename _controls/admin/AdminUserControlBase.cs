﻿using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._controls.admin
{
    public abstract class AdminUserControlBase : SiteUserControl, TabStateMonitor.IReactive
    {
        #region fields

            protected Label lblResults;
            protected HtmlAnchor lnkSetFocus;
            
        #endregion

        #region properties

            public Boolean AddTabForTabReload { get { return true; } }

            protected String SuccessMessage { get; set; }

            protected RadTabStrip RadTabStrip { get { return (RadTabStrip)Parent.FindControl("radTabStrip"); } }

            protected RadMultiPage RadMultiPage { get { return (RadMultiPage)Parent.FindControl("radMultiPage"); } }

            protected AdminModeEnum AdminModeEnum { get { return ((AdminSitePageTemp)Page).AdminModeEnum; } }

            #region virtual properties

                public virtual IEnumerable<Enum> ReactToChangeStateList { get { return Enumerable.Empty<Enum>(); } }

            #endregion

        #endregion

        #region methods

            #region virtual methods

                protected virtual void InitializeControl() { } //doesn't always need to be implemented in sub-class, add tabs for example will never reset

            #endregion

            protected void SetChangeStateForTabs(Enum message = null)
            {
                ((AdminSitePageTemp)Page).TabStateMonitor.ChangeState(message);
            }

            protected void ChangeCurrentTab(String successMessage)
            {
                SuccessMessage = successMessage;
                ResetUserControl();
            }

            protected void OperationComplete(Enum tabEnum)
            {
                SetChangeStateForTabs(tabEnum);
            }

            public void ResetUserControl()
            {
                InitializeControl();

                if (!String.IsNullOrWhiteSpace(SuccessMessage)) LabelHelper.SetLabelMessage(lblResults, SuccessMessage, lnkSetFocus);
            }

            protected void BindDDL(DropDownList ddl, String textField, String valueField, Object data, Boolean clearItems, ListItem listItem = null)
            {
                var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

                if (clearItems)
                {
                    ddl.Items.Clear();
                    ddl.Items.Add((listItem == null) ? lItem : listItem);
                }

                if (data != null)
                {
                    ddl.DataTextField = textField;
                    ddl.DataValueField = valueField;
                    ddl.DataSource = data;
                    ddl.DataBind();
                }
            }

        #endregion
    }
}