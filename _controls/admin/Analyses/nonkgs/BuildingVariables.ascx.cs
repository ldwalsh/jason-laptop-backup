﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Analyses.nonkgs
{
    public partial class BuildingVariables : AdminUserControlBase
    {
        #region constants

            const String buildingVarsEmpty = "No building variables have been assigned.";

        #endregion

        #region properties

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblVariablesError.Visible = false;

                if (!Page.IsPostBack) BindAnalyses(ddlAnalyses);
            }

            #region ddl events

                protected void ddlAnalyses_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    var aid = Convert.ToInt32(ddlAnalyses.SelectedValue);

                    if (aid != -1) BindBuildingVariables(aid);
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();
                    ddlAnalyses_OnSelectedIndexChanged(null, null);
                }

            #endregion

            #region binders
                
                private void BindAnalyses(DropDownList ddl)
                {
                    BindDDL(ddl, "AnalysisName", "AID", DataMgr.AnalysisDataMapper.GetAllAvailableActiveAnalysesByCID(siteUser.CID), true);
                }

                private void BindBuildingVariables(Int32 aid)
                {
                    //get all for display purposes
                    var mBuildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesAssociatedToAIDWithData(aid);

                    if (mBuildingVariables.Any())
                    {
                        rptBuildingVars.DataSource = mBuildingVariables;
                        rptBuildingVars.DataBind();
                        rptBuildingVars.Visible = true;
                        lblBuildingVarsEmpty.Visible = false;
                    }
                    else
                    {
                        rptBuildingVars.Visible = false;
                        lblBuildingVarsEmpty.Text = buildingVarsEmpty;
                        lblBuildingVarsEmpty.Visible = true;
                    }

                    buildingVariables.Visible = true;
                }

            #endregion

        #endregion
    }
}