﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Analyses.nonkgs
{
    public partial class InputPointTypes : AdminUserControlBase
    {
        #region constants

        const String inputPointTypesEmpty = "No input point types have been assigned.";

        #endregion

        #region properties

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblInputPointTypesError.Visible = false;

                if (!Page.IsPostBack) BindAnalyses(ddlAnalyses);
            }

            #region ddl events

                protected void ddlAnalyses_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    var aid = Convert.ToInt32(ddlAnalyses.SelectedValue);

                    if (aid != -1) BindInputPointTypes(aid);
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();
                    ddlAnalyses_OnSelectedIndexChanged(null, null);
                }

            #endregion

            #region binders
                
                private void BindAnalyses(DropDownList ddl)
                {
                    BindDDL(ddl, "AnalysisName", "AID", DataMgr.AnalysisDataMapper.GetAllAvailableActiveAnalysesByCID(siteUser.CID), true);
                }

                private void BindInputPointTypes(Int32 aid)
                {
                    //get all for display purposes
                    var mInputPointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllInputPointTypesAssociatedToAIDWithData(aid);

                    if (mInputPointTypes.Any())
                    {
                        rptInputPointTypes.DataSource = mInputPointTypes;
                        rptInputPointTypes.DataBind();
                        rptInputPointTypes.Visible = true;
                        lblInputPointTypesEmpty.Visible = false;
                    }
                    else
                    {
                        rptInputPointTypes.Visible = false;
                        lblInputPointTypesEmpty.Text = inputPointTypesEmpty;
                        lblInputPointTypesEmpty.Visible = true;
                    }

                    inputPointTypes.Visible = true;
                }

            #endregion

        #endregion
    }
}