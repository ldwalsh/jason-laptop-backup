﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InputPointTypes.ascx.cs" Inherits="CW.Website._controls.admin.Analyses.nonkgs.InputPointTypes" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.AnalysisPointType" %>

<h2>Input Point Types</h2>

<p>Please select an analysis in order to view all assigned input point types.</p>

<div>
  <a id="lnkSetFocus" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblInputPointTypesError" CssClass="errorMessage" runat="server" />
</div>
<div class="divForm">
  <label class="label">*Select Analyses:</label>
  <asp:DropDownList ID="ddlAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<div id="inputPointTypes" visible="false" runat="server">
  <hr />

  <h2>Input Point Types List</h2>

  <div>
    <asp:Label ID="lblInputPointTypesEmpty" Visible="false" runat="server" />
  </div>

  <asp:Repeater ID="rptInputPointTypes" Visible="false" runat="server">
    <HeaderTemplate>
      <ul class="detailsListProperties">
    </HeaderTemplate>

    <ItemTemplate>
      <%# "<li><strong>" + Eval(PropHelper.G<GetAnalysisInputPointTypeData>(_ => _.PointTypeDisplayName)) + "</strong></li>"%>
    </ItemTemplate>

    <FooterTemplate>
      </ul>
    </FooterTemplate>
  </asp:Repeater>
</div>