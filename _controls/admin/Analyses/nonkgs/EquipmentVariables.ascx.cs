﻿using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Analyses.nonkgs
{
    public partial class EquipmentVariables : AdminUserControlBase
    {
        #region constants

            const String equipmentVarsEmpty = "No equipment variables have been assigned.";

        #endregion

        #region properties

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblVariablesError.Visible = false;

                if (!Page.IsPostBack) BindAnalyses(ddlAnalyses);
            }

            #region ddl events

                protected void ddlAnalyses_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    var aid = Convert.ToInt32(ddlAnalyses.SelectedValue);

                    if (aid != -1) BindEquipmentVariables(aid);
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();
                    ddlAnalyses_OnSelectedIndexChanged(null, null);
                }

            #endregion

            #region binders
                
                private void BindAnalyses(DropDownList ddl)
                {
                    BindDDL(ddl, "AnalysisName", "AID", DataMgr.AnalysisDataMapper.GetAllAvailableActiveAnalysesByCID(siteUser.CID), true);
                }

                private void BindEquipmentVariables(Int32 aid)
                {
                    //get all for display purposes
                    var mEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesAssociatedToAIDWithData(aid);

                    if (mEquipmentVariables.Any())
                    {
                        rptEquipmentVars.DataSource = mEquipmentVariables;
                        rptEquipmentVars.DataBind();
                        rptEquipmentVars.Visible = true;
                        lblEquipmentVarsEmpty.Visible = false;
                    }
                    else
                    {
                        rptEquipmentVars.Visible = false;
                        lblEquipmentVarsEmpty.Text = equipmentVarsEmpty;
                        lblEquipmentVarsEmpty.Visible = true;
                    }

                    equipmentVariables.Visible = true;
                }

            #endregion

        #endregion
    }
}