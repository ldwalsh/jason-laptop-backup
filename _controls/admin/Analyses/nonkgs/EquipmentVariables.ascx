﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EquipmentVariables.ascx.cs" Inherits="CW.Website._controls.admin.Analyses.nonkgs.EquipmentVariables" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.EquipmentVariable" %>

<h2>Equipment Variables</h2>

<p>Please select an analysis in order to view all assigned equipment variables.</p>

<div>
  <a id="lnkSetFocus" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblVariablesError" CssClass="errorMessage" runat="server" />
</div>
<div class="divForm">
  <label class="label">*Select Analyses:</label>
  <asp:DropDownList ID="ddlAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<div id="equipmentVariables" visible="false" runat="server">
  <hr />

  <h2>Variables List</h2>

  <div>
    <asp:Label ID="lblEquipmentVarsEmpty" Visible="false" runat="server" />
  </div>

  <asp:Repeater ID="rptEquipmentVars" Visible="false" runat="server">
    <HeaderTemplate>
      <ul class="detailsListProperties">
    </HeaderTemplate>

    <ItemTemplate>
      <%# "<li><strong>" + Eval(PropHelper.G<GetAnalysisEquipmentVariableData>(_ => _.EquipmentVariableDisplayName)) + "</strong></li>"%>
    </ItemTemplate>

    <FooterTemplate>
      </ul>
    </FooterTemplate>
  </asp:Repeater>
</div>