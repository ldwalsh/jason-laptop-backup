﻿using CW.Data;
using CW.Data.Models.Analysis;
using CW.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.admin.Analyses.nonkgs
{
    public partial class ViewAnalyses : AdminUserControlGrid
    {
        #region fields

            private enum AnalysisDetails { Default };

        #endregion

        #region properties

            #region overrides

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    return DataMgr.AnalysisDataMapper.GetAllAvailableVisibleSearchedAnalysesByClientID(searchCriteria.FirstOrDefault(), siteUser.CID);
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<Analyse>(_ => _.AnalysisName, _ => _.AnalysisTeaser); }
                }

                protected override String NameField { get { return PropHelper.G<Analyse>(_ => _.AnalysisName); } }

                protected override String IdColumnName { get { return PropHelper.G<Analyse>(_ => _.AID); } }

                protected override String Name { get { return typeof(Analyse).Name; } }

                protected override Type Entity { get { return typeof(Analyse); } }

                protected override List<String> CacheName { get { return base.CacheName.Concat(new List<String>() { "." + Enum.GetName(typeof(AdminModeEnum), AdminModeEnum) }).ToList(); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID }); } }

                protected override IEnumerable<String> SearchGridColumnNames { get { return new[] { NameField, PropHelper.G<Analyse>(_ => _.AnalysisTeaser) }; } }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var analysis = DataMgr.AnalysisDataMapper.GetAnalysisByAID(CurrentID).First();

                    SetAnalysisDataIntoViewState(analysis);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(AnalysisDetails.Default), rptAnalysisDetails);
                }

            #endregion

            private void SetAnalysisDataIntoViewState(GetAnalysisData analysis)
            {
                SetAnalysisDetails(analysis);
            }

            private void SetAnalysisDetails(GetAnalysisData analysis)
            {
                SetKeyValuePairItem("Analysis Name", analysis.AnalysisName);
                SetKeyValuePairItem("Analysis Teaser", analysis.AnalysisTeaser, "<div class='divContentPreWrap'>{0}</div>");
                SetKeyValuePairItem("Analysis Description", analysis.AnalysisDescription, "<div class='divContentPreWrap'>{0}</div>");

                SetDetailsViewDataToViewState(Convert.ToInt32(AnalysisDetails.Default), true); //True means to clear viewstate since this is a new record
            }

        #endregion
    }
}