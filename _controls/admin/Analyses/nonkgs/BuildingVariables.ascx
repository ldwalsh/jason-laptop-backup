﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuildingVariables.ascx.cs" Inherits="CW.Website._controls.admin.Analyses.nonkgs.BuildingVariables" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.BuildingVariable" %>

<h2>Building Variables</h2>

<p>Please select an analysis in order to view all assigned building variables.</p>

<div>
  <a id="lnkSetFocus" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblVariablesError" CssClass="errorMessage" runat="server" />
</div>
<div class="divForm">
  <label class="label">*Select Analyses:</label>
  <asp:DropDownList ID="ddlAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<div id="buildingVariables" visible="false" runat="server">
  <hr />

  <h2>Variables List</h2>

  <div>
    <asp:Label ID="lblBuildingVarsEmpty" Visible="false" runat="server" />
  </div>

  <asp:Repeater ID="rptBuildingVars" Visible="false" runat="server">
    <HeaderTemplate>
      <ul class="detailsListProperties">
    </HeaderTemplate>

    <ItemTemplate>
      <%# "<li><strong>" + Eval(PropHelper.G<GetAnalysisBuildingVariableData>(_ => _.BuildingVariableDisplayName)) + "</strong></li>"%>
    </ItemTemplate>

    <FooterTemplate>
      </ul>
    </FooterTemplate>
  </asp:Repeater>
</div>