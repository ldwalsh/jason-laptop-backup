﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Linq;
using CW.Utility.Web;

namespace CW.Website._controls.admin.EquipmentTypes
{
    public partial class AddEquipmentType : AdminUserControlBase
    {
        #region constants

            const String equipmentTypeExists = "An equipment type with that name already exists.";
            const String addEquipmentTypeSuccess = "{0} equipment type addition was successful.";
            const String addEquipmentTypeFailed = "Adding equipment type failed. Please contact an administrator.";

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindEquipmentClasses(ddlAddEquipmentClass);
            }

            private void Page_Load()
            {
                lblResults.Visible = false;
                lblAddError.Visible = false;
            }

            #region button events

                protected void addEquipmentTypeButton_Click(Object sender, EventArgs e)
                {
                    if (DataMgr.EquipmentTypeDataMapper.DoesEquipmentTypeExist(txtAddEquipmentTypeName.Text))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, equipmentTypeExists, lnkSetFocus);
                        return;
                    }
                    else
                    {
                        var mEquipmentType = new EquipmentType();

                        LoadAddFormIntoEquipmentType(mEquipmentType);

                        try
                        {
                            mEquipmentType.EquipmentTypeID = DataMgr.EquipmentTypeDataMapper.InsertEquipmentTypeAndReturnID(mEquipmentType);
                            
                            //insert all equipmenttype_pointtype relationships for unassigned pointtypes.
                            DataMgr.EquipmentTypeDataMapper.InsertEquipmentTypePointTypes((DataMgr.PointTypeDataMapper.GetAllUnassignedPointTypes()).Select(pt => new EquipmentTypes_PointType { EquipmentTypeID = mEquipmentType.EquipmentTypeID, PointTypeID = pt.PointTypeID } ));

                            LabelHelper.SetLabelMessage(lblResults, String.Format(addEquipmentTypeSuccess, mEquipmentType.EquipmentTypeName), lnkSetFocus);
                            OperationComplete(KGSEquipmentTypeAdministration.TabMessages.AddEquipmentType);
                        }
                        catch (SqlException sqlEx)
                        {
                            LabelHelper.SetLabelMessage(lblAddError, addEquipmentTypeFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment type", sqlEx);
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblAddError, addEquipmentTypeFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment type", ex);
                        }
                    }
                    
                }

            #endregion

        #endregion

        #region methods

            protected void LoadAddFormIntoEquipmentType(EquipmentType equipmentType)
            {
                equipmentType.EquipmentTypeName = txtAddEquipmentTypeName.Text;
                equipmentType.EquipmentClassID = Convert.ToInt32(ddlAddEquipmentClass.SelectedValue);
                equipmentType.EquipmentTypeDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                equipmentType.DateModified = DateTime.UtcNow;
            }

            #region binders

                private void BindEquipmentClasses(DropDownList ddl)
                {
                    BindDDL(ddl, "EquipmentClassName", "EquipmentClassID", DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses(), true);
                }

            #endregion

        #endregion
    }
}