﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AddEquipmentType.ascx.cs" Inherits="CW.Website._controls.admin.EquipmentTypes.AddEquipmentType" %>

<asp:Panel ID="pnlAddEquipmentType" runat="server" DefaultButton="btnAddEquipmentType">

  <h2>Add Equipment Type</h2>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />

    <div class="divForm">
      <label class="label">*Equipment Type Name:</label>
      <asp:TextBox ID="txtAddEquipmentTypeName" CssClass="textbox" Columns="50" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Equipment Class:</label>
      <asp:DropDownList ID="ddlAddEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
      <p>(Warning: You cannot change the equipment class later because different equipment variables are associated to different classes.)</p>
      <p>(Warning: You cannot change the equipment class later if you select any class that is a group.)</p>
    </div>
    <div class="divForm">
      <label class="label">Description:</label>
      <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server" />
      <div id="divAddDescriptionCharInfo"></div>
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnAddEquipmentType" runat="server" Text="Add Type"  OnClick="addEquipmentTypeButton_Click" ValidationGroup="AddEquipmentType" />

  </div>

  <!--Ajax Validators-->
  <asp:RequiredFieldValidator ID="equipmentTypeNameRequiredValidator" runat="server" ErrorMessage="Equipment Type Name is a required field." ControlToValidate="txtAddEquipmentTypeName"  SetFocusOnError="true" Display="None" ValidationGroup="AddEquipmentType" />
  <ajaxToolkit:ValidatorCalloutExtender ID="equipmentTypeNameRequiredValidatorExtender" runat="server" BehaviorID="equipmentTypeNameRequiredValidatorExtender" TargetControlID="equipmentTypeNameRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addEquipmentClassRequiredValidator" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlAddEquipmentClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddEquipmentType" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentClassRequiredValidatorExtender" runat="server" BehaviorID="addEquipmentClassRequiredValidatorExtender" TargetControlID="addEquipmentClassRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>