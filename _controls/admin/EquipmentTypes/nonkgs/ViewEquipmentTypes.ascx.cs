﻿using CW.Data;
using CW.Data.Models.EquipmentType;
using CW.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.admin.EquipmentTypes.nonkgs
{
    public partial class ViewEquipmentTypes : AdminUserControlGrid
    {
        #region fields

            private enum EquipmentTypeDetails { Default };

        #endregion

        #region properties

            #region overrides

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    return DataMgr.EquipmentTypeDataMapper.GetAllFullSearchedEquipmentTypes(searchCriteria.FirstOrDefault());
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetEquipmentTypeData>(_ => _.EquipmentTypeID, _ => _.EquipmentTypeName, _ => _.EquipmentClassName, _ => _.EquipmentClassID, _ => _.EquipmentTypeDescription); }
                }

                protected override String NameField { get { return PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeID); } }

                protected override String Name { get { return typeof(EquipmentType).Name; } }

                protected override Type Entity { get { return typeof(GetEquipmentTypeData); } }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var equipmentType = DataMgr.EquipmentTypeDataMapper.GetFullEquipmentTypeByEquipmentTypeID(CurrentID).First();

                    SetEquipmentTypeIntoViewState(equipmentType);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(EquipmentTypeDetails.Default), rptEquipmentTypeDetails);
                }

            #endregion

                private void SetEquipmentTypeIntoViewState(GetEquipmentTypeData equipmentType)
                {
                    SetEquipmentTypeDetails(equipmentType);
                }

                private void SetEquipmentTypeDetails(GetEquipmentTypeData equipmentType)
                {
                    SetKeyValuePairItem("Equipment Type Name", equipmentType.EquipmentTypeName);
                    SetKeyValuePairItem("Equipment Class Name", equipmentType.EquipmentClassName);
                    SetKeyValuePairItem("Equipment Type Description", equipmentType.EquipmentTypeDescription);

                    SetDetailsViewDataToViewState(Convert.ToInt32(EquipmentTypeDetails.Default), true); //True means to clear viewstate since this is a new record
                }

        #endregion
    }
}