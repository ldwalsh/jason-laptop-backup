﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViewEquipmentTypes.ascx.cs" Inherits="CW.Website._controls.admin.EquipmentTypes.nonkgs.ViewEquipmentTypes" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Data.Models.EquipmentType" %>
<%@ Import Namespace="CW.Utility" %>

<h2>View Equipment Types</h2>

<p>
  <a id="lnkSetFocusView" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
</p>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentTypeName" HeaderText="Equipment Type">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeName), 30) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Equipment Class" SortExpression="EquipmentClassName">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentClassName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentClassName), 30) %></label>
          <asp:HiddenField ID="hdnEquipmentClassID" runat="server" Visible="true" Value='<%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentClassID)) %>' />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Equipment Type Description">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeDescription)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeDescription), 100) %></label></ItemTemplate>
      </asp:TemplateField>

    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<div>
  <!--SELECT EQUIPMENT TYPE DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptEquipmentTypeDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Equipment Type Details</h2>
          <ul id="equipmentTypeDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>