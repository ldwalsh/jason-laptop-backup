﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.EquipmentTypes
{
    public partial class AnalysesToAnEquipmentType : AdminUserControlBase
    {
        #region fields

            private String noItemsSelected = "No items were selected for reassignment.";
            private String unassignAnalysesUpdateSuccessful = "Unassignment of analyses {0} to equipment type {1} was successful.";
            private String assignAnalysesUpdateSuccessful = "Assignment of analyses {0} to equipment type {1} was successful.";
            private String reassignAnalysesUpdateFailed = "Analyses reassignment failed. Please contact an administrator.";
            private String unassignmentError = "Cannot unassign analysis {0}.  Analysis is assigned to equipment of equipment type {1}.";    
            private String reassignExceptionMessage = "Error reassigning analyses to equipment type.";

            private String equipmentTypeAnalysesUpdateSuccessful = "Equipment type analyses update was successful.";
            private String equipmentTypeAnalysesUpdateFailed = "Equipment type analyses update failed. Please contact an administrator.";
            private String updateExceptionMessage = "Error updating equipment type analyses.";
            private String partiallySuccessfulFmt = "{0} out of {1} Equipment type analyses were updated successfully. Please see the failure(s) below.";

            private IEnumerable<Analyse> analyses;

        #endregion

        #region properites

            public override IEnumerable<Enum> ReactToChangeStateList
            {
                get
                {
                    return new Enum[]
                    {
                        KGSEquipmentTypeAdministration.TabMessages.AddEquipmentType,
                        KGSEquipmentTypeAdministration.TabMessages.EditEquipmentType,
                        KGSEquipmentTypeAdministration.TabMessages.DeleteEquipmentType
                    };
                }
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindEquipmentTypes(ddlEquipmentType);
            }

            private void Page_Load()
            {
                lblResults.Visible = false;
                lblAnalysesError.Visible = false;

                analyses = (analyses == null) ? DataMgr.AnalysisDataMapper.GetAllAnalyses() : analyses;
            }

            #region ddl events

                protected void DDLEquipmentTypeSelectedIndexChanged(Object sender, EventArgs e)
                {
                    equipmentTypeAnalyses.Visible = false;

                    var etid = Convert.ToInt32(ddlEquipmentType.SelectedValue);

                    if (etid != -1)
                    {
                        var assignedAnalysesList = DataMgr.AnalysisEquipmentTypeDataMapper.GetAssignedAnalysesByEquipmentTypeID(etid, (a_et => a_et.EquipmentType), (a_et => a_et.Analyse)).OrderBy(a => a.Analyse.AnalysisName);

                        BindEquipmentTypeAnalyses(assignedAnalysesList);
                        BindRepeater(assignedAnalysesList);
                    }                   
                }

            #endregion

            #region button events

                protected void AnalysesUpClick(Object sender, EventArgs e)
                {
                    while (lbAnalysesBottom.SelectedIndex != -1)
                    {
                        lbAnalysesTop.Items.Add(lbAnalysesBottom.SelectedItem);
                        lbAnalysesBottom.Items.Remove(lbAnalysesBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbAnalysesTop);
                }

                protected void AnalysesDownClick(Object sender, EventArgs e)
                {
                    while (lbAnalysesTop.SelectedIndex != -1)
                    {
                        lbAnalysesBottom.Items.Add(lbAnalysesTop.SelectedItem);
                        lbAnalysesTop.Items.Remove(lbAnalysesTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbAnalysesBottom);
                }

                protected void ReassignEquipmentAnalysesClick(Object sender, EventArgs e)
                {
                    var etid = Convert.ToInt32(ddlEquipmentType.SelectedValue);
                    var equipmentTypeName = ddlEquipmentType.SelectedItem.Text;
                    var selector = new Func<ListItem, Analyses_EquipmentType>(li => new Analyses_EquipmentType { EquipmentTypeID = etid, AID = Convert.ToInt32(li.Value.Split(DataConstants.DefaultDelimiter)[0]) });

                    try
                    {
                        lblAnalysesError.Text = lblResults.Text = String.Empty;

                        UnassignAnalysesForEquipmentType(etid, selector, analyses, equipmentTypeName);
                        AssignAnalysesForEquipmentType(etid, selector, analyses, equipmentTypeName);

                        if (lblAnalysesError.Text == String.Empty && lblResults.Text == String.Empty)
                        {
                            LabelHelper.SetLabelMessage(lblAnalysesError, noItemsSelected, lnkSetFocus);
                            return;
                        }

                        var assignedAnalysesList = DataMgr.AnalysisEquipmentTypeDataMapper.GetAssignedAnalysesByEquipmentTypeID(etid, (a_et => a_et.EquipmentType), (a_et => a_et.Analyse)).OrderBy(a => a.Analyse.AnalysisName);
                        
                        BindEquipmentTypeAnalyses(assignedAnalysesList);
                        BindRepeater(assignedAnalysesList);

                        if (lblAnalysesError.Text != String.Empty) LabelHelper.SetLabelMessage(lblAnalysesError, lblAnalysesError.Text, lnkSetFocus);
                        if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblAnalysesError, reassignAnalysesUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, reassignExceptionMessage, sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblAnalysesError, reassignAnalysesUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, reassignExceptionMessage, ex);
                    }
                }

                protected void UpdateEquipmentTypeAnalyses_ButtonClick(Object sender, EventArgs e)
                {
                    try
                    {
                        int numToProcess = 0;
                        int numSucessful = 0;
                        foreach (RepeaterItem rptItem in rptEquipmentTypeAnalyses.Items)
                        {
                            if (rptItem.ItemType == ListItemType.Item || rptItem.ItemType == ListItemType.AlternatingItem)
                            {
                                ++numToProcess;
                                HtmlGenericControl valFailure = rptItem.FindControl("valFailure") as HtmlGenericControl;
                                var id = Convert.ToInt32(((HiddenField)rptItem.FindControl("hdnID")).Value);

                                bool d = ((CheckBox)rptItem.FindControl("chkEditDaily")).Checked;
                                bool w = ((CheckBox)rptItem.FindControl("chkEditWeekly")).Checked;
                                bool m = ((CheckBox)rptItem.FindControl("chkEditMonthly")).Checked;
                                bool hd = ((CheckBox)rptItem.FindControl("chkEditHalfDay")).Checked;

                                try
                                {
                                    DataMgr.AnalysisEquipmentTypeDataMapper.UpdatePartialAnalysisEquipmentType(id, d, w, m, hd);
                                    ++numSucessful;
                                    valFailure.InnerHtml = "";
                                    valFailure.Style[HtmlTextWriterStyle.Display] = "none";
                                }
                                catch (CW.Data.Exceptions.ValidationException ex)
                                {
                                    valFailure.Style[HtmlTextWriterStyle.Display] = "block";
                                    valFailure.InnerHtml = ex.Message;
                                }
                            }
                        }

                        if (numToProcess > 0)
                        {
                            if (numToProcess == numSucessful)
                            {
                                LabelHelper.SetLabelMessage(lblResults, equipmentTypeAnalysesUpdateSuccessful, lnkSetFocus);
                            }
                            else
                            {
                                LabelHelper.SetLabelMessage(lblResults, string.Format(partiallySuccessfulFmt, numSucessful, numToProcess), lnkSetFocus);
                            }
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, updateExceptionMessage, sqlEx);
                        LabelHelper.SetLabelMessage(lblAnalysesError, equipmentTypeAnalysesUpdateFailed, lnkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, updateExceptionMessage, ex);
                        LabelHelper.SetLabelMessage(lblAnalysesError, equipmentTypeAnalysesUpdateFailed, lnkSetFocus);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    BindEquipmentTypes(ddlEquipmentType);

                    new AdminUserControlForm(this).ResetDropDownLists();

                    DDLEquipmentTypeSelectedIndexChanged(null, null); //hardset any fields to their initial values
                }

            #endregion

            private void ClearListBoxes()
            {
                lbAnalysesTop.Items.Clear();
                lbAnalysesBottom.Items.Clear();
            }

            private void UnassignAnalysesForEquipmentType(Int32 etid, Func<ListItem, Analyses_EquipmentType> selector, IEnumerable<Analyse> analyses, String equipmentTypeName)
            {
                var unassignedAnalyses = lbAnalysesTop.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, true)).Select(selector).ToList();

                foreach (var item in unassignedAnalyses)
                {
                    var aid = Convert.ToInt32(item.AID);
                    var analysisName = analyses.Where(a => a.AID == aid).Select(a => a.AnalysisName).First();

                    if (DataMgr.AnalysisEquipmentTypeDataMapper.IsAnalysisAssignedToEquipmentType(etid, aid))
                    {
                        if (!DataMgr.EquipmentDataMapper.IsAnalysisAssignedToEquipmentWithType(etid, aid))
                        {
                            DataMgr.AnalysisEquipmentTypeDataMapper.DeleteAnalysisEquipmentTypeByETIDAndAID(etid, aid);
                            lblResults.Text += String.Format(unassignAnalysesUpdateSuccessful, analysisName, equipmentTypeName) + "<br />";
                        }
                        else
                            lblAnalysesError.Text += String.Format(unassignmentError, analysisName, equipmentTypeName) + "<br />";
                    }
                }
            }

            private void AssignAnalysesForEquipmentType(Int32 etid, Func<ListItem, Analyses_EquipmentType> selector, IEnumerable<Analyse> analyses, String equipmentTypeName)
            {
                var assignedAnalyses = lbAnalysesBottom.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, false)).Select(selector).ToList();

                foreach (var item in assignedAnalyses)
                {
                    var analysisEquipmentType = new Analyses_EquipmentType();
                    var aid = Convert.ToInt32(item.AID);
                    var analysisName = analyses.Where(a => a.AID == aid).Select(a => a.AnalysisName).First();

                    analysisEquipmentType.AID = aid;
                    analysisEquipmentType.EquipmentTypeID = etid;
                    analysisEquipmentType.AutoAssignAndScheduleDaily = analysisEquipmentType.AutoAssignAndScheduleWeekly = analysisEquipmentType.AutoAssignAndScheduleMonthly = true;
                    analysisEquipmentType.AutoAssignAndScheduleHalfDay = false;

                    if (!DataMgr.AnalysisEquipmentTypeDataMapper.IsAnalysisAssignedToEquipmentType(etid, aid))
                    {
                        DataMgr.AnalysisEquipmentTypeDataMapper.InsertAnalysisEquipmentType(analysisEquipmentType);
                        lblResults.Text += String.Format(assignAnalysesUpdateSuccessful, analysisName, equipmentTypeName) + "<br />";
                    }
                }
            }

            #region binders

                private void BindEquipmentTypes(DropDownList ddl)
                {
                    BindDDL(ddl, "EquipmentTypeName", "EquipmentTypeID", DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypes(), true);
                }

                private void BindEquipmentTypeAnalyses(IEnumerable<Analyses_EquipmentType> assignedAnalysesList)
                {
                    ClearListBoxes();
                    
                    var availableAnalysesList = analyses.Where(a => !assignedAnalysesList.Select(assigned => assigned.AID).Contains(a.AID)).OrderBy(a => a.AnalysisName);

                    if (availableAnalysesList.Any()) ControlHelper.SetListBoxData(lbAnalysesTop, availableAnalysesList, "AnalysisName", "AID", false);
                    if (assignedAnalysesList.Any()) ControlHelper.SetListBoxData(lbAnalysesBottom, assignedAnalysesList.Select(a => a.Analyse), "AnalysisName", "AID", true);

                    equipmentTypeAnalyses.Visible = (availableAnalysesList.Any() || assignedAnalysesList.Any());
                }

                private void BindRepeater(IEnumerable<Analyses_EquipmentType> assignedAnalysesList)
                {
                    pnlUpdateEquipmentTypeAnalyses.Visible = (assignedAnalysesList != null && assignedAnalysesList.Any());

                    if (pnlUpdateEquipmentTypeAnalyses.Visible)
                    {
                        rptEquipmentTypeAnalyses.DataSource = assignedAnalysesList;
                        rptEquipmentTypeAnalyses.DataBind();
                    }
                }

            #endregion

        #endregion
    }
}