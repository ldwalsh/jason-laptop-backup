﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AnalysesToAnEquipmentType.ascx.cs" Inherits="CW.Website._controls.admin.EquipmentTypes.AnalysesToAnEquipmentType" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Panel ID="pnlReassignEquipmentTypeAnalyses" runat="server" DefaultButton="btnReassignEquipmentTypeAnalyses">

  <h2>Analyses To An Equipment Type</h2>

  <p>Please select equipment type to select in order to select and assign analyses.</p>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAnalysesError" CssClass="errorMessage" runat="server" />
  </div>

  <div class="divForm">
    <label class="label">*Select Equipment Type:</label>    
    <asp:DropDownList ID="ddlEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DDLEquipmentTypeSelectedIndexChanged" />
  </div>

  <div id="equipmentTypeAnalyses" visible="false" runat="server">
    <hr />

    <div class="divForm">
      <label class="label">Available:</label>    
      <asp:ListBox ID="lbAnalysesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

      <div class="divArrows">
        <asp:ImageButton CssClass="up-arrow" ID="btnAnalysesUp" ImageUrl="~/_assets/images/up-arrow.png" runat="server" OnClick="AnalysesUpClick" CausesValidation="false" />
        <asp:ImageButton CssClass="down-arrow" ID="btnAnalysesDown" ImageUrl="~/_assets/images/down-arrow.png" runat="server" OnClick="AnalysesDownClick" CausesValidation="false" />
      </div>

      <label class="label">Assigned:</label>
      <asp:ListBox ID="lbAnalysesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnReassignEquipmentTypeAnalyses" runat="server" Text="Reassign" OnClick="ReassignEquipmentAnalysesClick" ValidationGroup="ReassignEquipmentTypeAnalyses" />

  </div>
</asp:Panel>


<!--EDIT ANALYSES TO AN EQUIPMENT TYPE PANEL -->                              
<asp:Panel ID="pnlUpdateEquipmentTypeAnalyses" runat="server" Visible="false" DefaultButton="btnUpdateEquipmentTypeAnalyses">        
  <hr />

  <p>(Note: If one or more "auto assign and schedule" options are selected, the analysis will be auto assigned to eqiupment of that type when added/uploaded.)</p>
  <p>(Note: Default "auto assign and schedule" options are pre selected as Daily, Weekly, and Monthly. This will auto schedule analyses to run for equipment of that type when added/uploaded.)</p>

  <asp:Repeater ID="rptEquipmentTypeAnalyses" runat="server">
    <ItemTemplate> 
      <div class="divForm">                                                                                                                  
        <label class="label">*<%# Eval("EquipmentType.EquipmentTypeName").ToString() + " (" + Eval("Analyse.AnalysisName").ToString() + ")" %>:</label>   
        <asp:Checkbox ID="chkEditDaily" runat="server" Checked='<%# Convert.ToBoolean(Eval(PropHelper.G<Analyses_EquipmentType>(_ => _.AutoAssignAndScheduleDaily)))%>' />&nbsp; - Auto assign and schedule (Daily)<br />
        <asp:Checkbox ID="chkEditWeekly" runat="server" Checked='<%# Convert.ToBoolean(Eval(PropHelper.G<Analyses_EquipmentType>(_ => _.AutoAssignAndScheduleWeekly)))%>' />&nbsp; - Auto assign and schedule (Weekly)<br /> 
        <asp:Checkbox ID="chkEditMonthly" runat="server" Checked='<%# Convert.ToBoolean(Eval(PropHelper.G<Analyses_EquipmentType>(_ => _.AutoAssignAndScheduleMonthly)))%>' />&nbsp; - Auto assign and schedule (Monthly)<br /> 
        <asp:Checkbox ID="chkEditHalfDay" runat="server" Checked='<%# Convert.ToBoolean(Eval(PropHelper.G<Analyses_EquipmentType>(_ => _.AutoAssignAndScheduleHalfDay)))%>' />&nbsp; - Auto assign and schedule (HalfDay)<br />
        <div id='valFailure' runat="server" class="errorMessage" style="display:none"></div>
        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval(PropHelper.G<Analyses_EquipmentType>(_ => _.ID)) %>' />
      </div>
    </ItemTemplate>
  </asp:Repeater>
                                        
  <asp:LinkButton ID="btnUpdateEquipmentTypeAnalyses" runat="server" Text="Update" CssClass="lnkButton" OnClick="UpdateEquipmentTypeAnalyses_ButtonClick" ValidationGroup="UpdateEquipmentTypeAnalyses" />

</asp:Panel>