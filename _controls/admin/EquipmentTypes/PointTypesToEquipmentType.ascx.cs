﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.EquipmentTypes
{
    public partial class PointTypesToEquipmentType : AdminUserControlBase
    {
        #region constants

        const String unassignPointTypeUpdateSuccessful = "Unassignment of point type {0} to equipment type {1} was successful.";
        const String unassignPointTypeRelationshipsStillExist = "Cannot unassign point type id {0} from equipment type id {1} because points with that type still exist on equipment with that type.";
        const String assignPointTypeUpdateSuccessful = "Assignment point type {0} to equipment type {1} was successful.";
        const String reassignUpdateFailed = "Point types reassignment failed. Please contact an administrator.";
        const String noItemsSelected = "No items were selected for reassignment.";
        const Char delimeter = Common.Constants.DataConstants.DefaultDelimiter;

        #endregion

        #region properites

        public override IEnumerable<Enum> ReactToChangeStateList
        {
            get
            {
                return new Enum[]
                {
                        KGSEquipmentTypeAdministration.TabMessages.AddEquipmentType,
                        KGSEquipmentTypeAdministration.TabMessages.EditEquipmentType,
                        KGSEquipmentTypeAdministration.TabMessages.DeleteEquipmentType
                };
            }
        }

        #endregion

        #region events

        private void Page_FirstLoad()
        {
            BindEquipmentClasses(ddlEquipmentClass);
            BindEquipmentTypes(ddlEquipmentType, -1);
        }

        private void Page_Load()
        {
            assignmentMessaging.CssClass = "";
            lblResults.Visible = lblPointTypesError.Visible = false;
            ControlHelper.ToggleVisibility((AdminModeEnum == admin.AdminModeEnum.KGS), new Control[] { EquipmentTypeDownloadArea });
        }

        #region button events

        protected void btnPointTypesUpButton_Click(Object sender, EventArgs e)
        {
            while (lbPointTypesBottom.SelectedIndex != -1)
            {
                lbPointTypesTop.Items.Add(lbPointTypesBottom.SelectedItem);
                lbPointTypesBottom.Items.Remove(lbPointTypesBottom.SelectedItem);
            }

            ControlHelper.SortListBox(lbPointTypesTop);
        }

        protected void btnPointTypesDownButton_Click(Object sender, EventArgs e)
        {
            while (lbPointTypesTop.SelectedIndex != -1)
            {
                lbPointTypesBottom.Items.Add(lbPointTypesTop.SelectedItem);
                lbPointTypesTop.Items.Remove(lbPointTypesTop.SelectedItem);
            }

            ControlHelper.SortListBox(lbPointTypesBottom);
        }

        protected void updatePointTypesButton_Click(Object sender, EventArgs e)
        {
            var etid = Convert.ToInt32(ddlEquipmentType.SelectedValue);
            var equipmentTypeName = ddlEquipmentType.SelectedItem.Text;
            var pointTypes = DataMgr.PointTypeDataMapper.GetAllPointTypes();
            var selector = new Func<ListItem, EquipmentTypes_PointType>(li => new EquipmentTypes_PointType { EquipmentTypeID = etid, PointTypeID = Convert.ToInt32(li.Value.Split(delimeter)[0]) });

            lblPointTypesError.Text = String.Empty;
            lblResults.Text = String.Empty;

            try
            {
                var unassignedPointTypes = lbPointTypesTop.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, true)).Select(selector).ToList();

                foreach (EquipmentTypes_PointType unassignedPointType in unassignedPointTypes)
                {
                    //Checks if a point type is associated with any points that are associated to equipment with that equipment type in order to see if we can delete the association.
                    var exists = DataMgr.EquipmentTypeDataMapper.IsPointTypeAssociatedWithAnyPointsAssociatedToEquipmentWithEquipmentType(unassignedPointType.EquipmentTypeID, unassignedPointType.PointTypeID);

                    if (exists)
                    {
                        LabelHelper.SetLabelMessage(lblPointTypesError, String.Format(unassignPointTypeRelationshipsStillExist, unassignedPointType.PointTypeID, unassignedPointType.EquipmentTypeID), lnkSetFocus);
                        return;
                    }
                }

                if (unassignedPointTypes.Any()) DataMgr.EquipmentTypeDataMapper.DeleteEquipmentTypePointTypes(unassignedPointTypes);
                SetMessages(unassignedPointTypes, equipmentTypeName, pointTypes, unassignPointTypeUpdateSuccessful);

                //

                var assignedPointTypes = lbPointTypesBottom.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, false)).Select(selector).ToList();

                if (assignedPointTypes.Any()) DataMgr.EquipmentTypeDataMapper.InsertEquipmentTypePointTypes(assignedPointTypes);
                SetMessages(assignedPointTypes, equipmentTypeName, pointTypes, assignPointTypeUpdateSuccessful);

                if (lblPointTypesError.Text == String.Empty && lblResults.Text == String.Empty)
                {
                    LabelHelper.SetLabelMessage(lblPointTypesError, noItemsSelected, lnkSetFocus);
                    return;
                }

                BindPointTypesListBoxes(etid);

                if (lblPointTypesError.Text != String.Empty) LabelHelper.SetLabelMessage(lblPointTypesError, lblPointTypesError.Text, lnkSetFocus);
                if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
            }
            catch (SqlException sqlEx)
            {
                LabelHelper.SetLabelMessage(lblPointTypesError, reassignUpdateFailed, lnkSetFocus);
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning point types to equipment type.", sqlEx);
            }
            catch (Exception ex)
            {
                LabelHelper.SetLabelMessage(lblPointTypesError, reassignUpdateFailed, lnkSetFocus);
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning point types to equipment type.", ex);
            }
        }

        #endregion

        #region ddl events

        protected void ddlEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            pointTypes.Visible = false;
            BindEquipmentTypes(ddlEquipmentType, Convert.ToInt32(ddlEquipmentClass.SelectedValue));
        }

        protected void ddlEquipmentType_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            pointTypes.Visible = false;

            var etid = Convert.ToInt32(ddlEquipmentType.SelectedValue);

            if (etid != -1) BindPointTypesListBoxes(etid);
        }

        #endregion

        #endregion

        #region methods

        #region overrides

        protected override void InitializeControl()
        {
            new AdminUserControlForm(this).ResetDropDownLists();

            //hardset any fields to their initial values
            ddlEquipmentClass_OnSelectedIndexChanged(null, null);
        }

        #endregion

        private void SetMessages(List<EquipmentTypes_PointType> processedItems, String equipmentTypeName, IEnumerable<PointType> items, String message)
        {
            int lineCounter = 0;

            foreach (var item in processedItems)
            {
                var updatedPointTypeName = items.Single(pt => pt.PointTypeID == item.PointTypeID).PointTypeName;

                lblResults.Text += String.Format(message, updatedPointTypeName, equipmentTypeName) + "<br />";

                lineCounter++;
            }

            if (lineCounter > 15) assignmentMessaging.CssClass = "contentScrollBox";
        }

        #region binders

        private void BindEquipmentClasses(DropDownList ddl)
        {
            var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

            ddl.Items.Clear();
            ddl.Items.Add(lItem);

            ddl.DataTextField = "EquipmentClassName";
            ddl.DataValueField = "EquipmentClassID";
            ddl.DataSource = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();
            ddl.DataBind();
        }

        private void BindEquipmentTypes(DropDownList ddl, Int32 ecid)
        {
            var lItem = new ListItem() { Text = (ecid != -1) ? "Select one..." : "Select equipment class first...", Value = "-1", Selected = true };

            ddl.Items.Clear();
            ddl.Items.Add(lItem);

            if (ecid != -1)
            {
                ddl.DataTextField = "EquipmentTypeName";
                ddl.DataValueField = "EquipmentTypeID";
                ddl.DataSource = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByEquipmentClassID(ecid);
                ddl.DataBind();
            }
        }

        private void BindPointTypesListBoxes(Int32 etid)
        {
            lbPointTypesTop.Items.Clear();
            lbPointTypesBottom.Items.Clear();

            var notAssociated = DataMgr.EquipmentTypeDataMapper.GetAllPointTypesNotAssociatedByETID(etid);
            var associated = DataMgr.EquipmentTypeDataMapper.GetAllPointTypesAssociatedByETID(etid);
            var pointTypeName = PropHelper.G<PointType>(pt => pt.PointTypeName);
            var pointTypeID = PropHelper.G<PointType>(pt => pt.PointTypeID);

            if (notAssociated.Any()) ControlHelper.SetListBoxData(lbPointTypesTop, notAssociated, pointTypeName, pointTypeID, false);
            if (associated.Any()) ControlHelper.SetListBoxData(lbPointTypesBottom, associated, pointTypeName, pointTypeID, true);

            pointTypes.Visible = (notAssociated.Any() || associated.Any());
        }

        #endregion

        #endregion
    }
}