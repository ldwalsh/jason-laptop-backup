﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.EquipmentTypes
{
    public partial class EquipmentTypesToEquipmentType : AdminUserControlBase
    {
        #region constants

            const String unassignEquipmentTypeUpdateSuccessful = "Unassignment of Equipment type {0} to equipment type {1} was successful.";
            const String assignEquipmentTypeUpdateSuccessful = "Assignment Equipment type {0} to equipment type {1} was successful.";
            const String equipmentExists = "Unassignment of equipment type {0} failed because equipment of that type are still associated to equipment with the selected primary type.";
            const String reassignUpdateFailed = "Equipment types reassignment failed. Please contact an administrator.";
            const String noItemsSelected = "No items were selected for reassignment.";
            const Char delimeter = Common.Constants.DataConstants.DefaultDelimiter;

        #endregion

        #region properites

            public override IEnumerable<Enum> ReactToChangeStateList
            {
                get
                {
                    return new Enum[]
                    {
                        KGSEquipmentTypeAdministration.TabMessages.AddEquipmentType,
                        KGSEquipmentTypeAdministration.TabMessages.EditEquipmentType,
                        KGSEquipmentTypeAdministration.TabMessages.DeleteEquipmentType
                    };
                }
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindEquipmentClasses(ddlEquipmentClass);
                BindEquipmentTypes(ddlEquipmentType, -1);
            }

            private void Page_Load()
            {
                lblResults.Visible = false;
                lblEquipmentTypesError.Visible = false;
            }

            #region button events

                protected void btnEquipmentTypesUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbEquipmentTypesBottom.SelectedIndex != -1)
                    {
                        lbEquipmentTypesTop.Items.Add(lbEquipmentTypesBottom.SelectedItem);
                        lbEquipmentTypesBottom.Items.Remove(lbEquipmentTypesBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbEquipmentTypesTop);
                }

                protected void btnEquipmentTypesDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbEquipmentTypesTop.SelectedIndex != -1)
                    {
                        lbEquipmentTypesBottom.Items.Add(lbEquipmentTypesTop.SelectedItem);
                        lbEquipmentTypesTop.Items.Remove(lbEquipmentTypesTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbEquipmentTypesBottom);
                }

                protected void updateEquipmentTypesButton_Click(Object sender, EventArgs e)
                {
                    var petid = Convert.ToInt32(ddlEquipmentType.SelectedValue);
                    var equipmentTypeName = ddlEquipmentType.SelectedItem.Text;
                    var equipmentTypes = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypes();
                    var selector = new Func<ListItem, EquipmentTypes_EquipmentType>(li => new EquipmentTypes_EquipmentType { PrimaryEquipmentTypeID = petid, AssignedEquipmentTypeID = Convert.ToInt32(li.Value.Split(delimeter)[0]) });

                    lblEquipmentTypesError.Text = lblResults.Text = String.Empty;
                    
                    try
                    {
                        UnassignItems(selector, petid, equipmentTypeName, equipmentTypes);
                        AssignItems(selector, equipmentTypeName, equipmentTypes);

                        if (lblEquipmentTypesError.Text == String.Empty && lblResults.Text == String.Empty)
                        {
                            LabelHelper.SetLabelMessage(lblEquipmentTypesError, noItemsSelected, lnkSetFocus);
                            return;
                        }

                        BindEquipmentTypesListBoxes(petid);

                        if (lblEquipmentTypesError.Text != String.Empty) LabelHelper.SetLabelMessage(lblEquipmentTypesError, lblEquipmentTypesError.Text, lnkSetFocus);
                        if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblEquipmentTypesError, reassignUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment types to equipment type.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEquipmentTypesError, reassignUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment types to equipment type.", ex);
                    }
                }

            #endregion

            #region ddl events

                protected void ddlEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    equipmentTypes.Visible = false;
                    BindEquipmentTypes(ddlEquipmentType, Convert.ToInt32(ddlEquipmentClass.SelectedValue));
                }

                protected void ddlEquipmentType_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    equipmentTypes.Visible = false;

                    var etid = Convert.ToInt32(ddlEquipmentType.SelectedValue);

                    if (etid != -1) BindEquipmentTypesListBoxes(etid);
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();

                    ddlEquipmentClass_OnSelectedIndexChanged(null, null); //hardset any fields to their initial values
                }

            #endregion

            private void UnassignItems(Func<ListItem, EquipmentTypes_EquipmentType> selector, Int32 petid, String equipmentTypeName, IEnumerable<EquipmentType> equipmentTypes)
            {
                var unassignedEquipmentTypes = lbEquipmentTypesTop.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, true)).Select(selector).ToList();
                var newUnassignedEquipmentTypes = new List<EquipmentTypes_EquipmentType>();

                if (unassignedEquipmentTypes.Any())
                {
                    foreach (EquipmentTypes_EquipmentType et_et in unassignedEquipmentTypes)
                    {
                        if (!DataMgr.EquipmentDataMapper.IsEquipmentOfEquipmentTypeAssociatedWithEquipmentOfEquipmentType(petid, et_et.AssignedEquipmentTypeID))
                            newUnassignedEquipmentTypes.Add(et_et);
                        else
                            SetMessages(unassignedEquipmentTypes, equipmentTypeName, equipmentTypes, equipmentExists, lblEquipmentTypesError);
                    }
                }

                if (newUnassignedEquipmentTypes.Any())
                {
                    DataMgr.EquipmentTypeDataMapper.DeleteEquipmentTypeEquipmentTypes(newUnassignedEquipmentTypes);
                    SetMessages(unassignedEquipmentTypes, equipmentTypeName, equipmentTypes, unassignEquipmentTypeUpdateSuccessful, lblResults);
                }
            }

            private void AssignItems(Func<ListItem, EquipmentTypes_EquipmentType> selector, String equipmentTypeName, IEnumerable<EquipmentType> equipmentTypes)
            {
                var assignedEquipmentTypes = lbEquipmentTypesBottom.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, false)).Select(selector).ToList();

                if (assignedEquipmentTypes.Any())
                {
                    DataMgr.EquipmentTypeDataMapper.InsertEquipmentTypeEquipmentTypes(assignedEquipmentTypes);
                    SetMessages(assignedEquipmentTypes, equipmentTypeName, equipmentTypes, assignEquipmentTypeUpdateSuccessful, lblResults);
                }
            }

            private void SetMessages(List<EquipmentTypes_EquipmentType> processedItems, String equipmentTypeName, IEnumerable<EquipmentType> items, String message, Label lbl)
            {
                foreach (var item in processedItems)
                {
                    var updatedEquipmentTypeName = items.Where(et => et.EquipmentTypeID == item.AssignedEquipmentTypeID).Single().EquipmentTypeName;

                    LabelHelper.SetLabelMessage(lbl, String.Format(message, updatedEquipmentTypeName, equipmentTypeName) + "<br />", lnkSetFocus, true);
                }
            }

            #region binders

                private void BindEquipmentClasses(DropDownList ddl)
                {
                    BindDDL(ddl, "EquipmentClassName", "EquipmentClassID", DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses(), true);
                }

                private void BindEquipmentTypes(DropDownList ddl, Int32 ecid)
                {
                    var lItem = new ListItem() { Text = (ecid != -1) ? "Select one..." : "Select equipment class first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "EquipmentTypeName", "EquipmentTypeID", (ecid != -1) ? DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByEquipmentClassID(ecid) : null, true, lItem);
                }

                private void BindEquipmentTypesListBoxes(Int32 petid)
                {
                    lbEquipmentTypesTop.Items.Clear();
                    lbEquipmentTypesBottom.Items.Clear();

                    var notAssociated = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesNotAssociatedByPETID(petid);
                    var associated = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesAssociatedByPETID(petid);
                    var equipmentTypeName = PropHelper.G<EquipmentType>(et => et.EquipmentTypeName);
                    var equipmentTypeID = PropHelper.G<EquipmentType>(et => et.EquipmentTypeID);

                    if (notAssociated.Any()) ControlHelper.SetListBoxData(lbEquipmentTypesTop, notAssociated, equipmentTypeName, equipmentTypeID, false);
                    if (associated.Any()) ControlHelper.SetListBoxData(lbEquipmentTypesBottom, associated, equipmentTypeName, equipmentTypeID, true);

                    equipmentTypes.Visible = (notAssociated.Any() || associated.Any());
                }

            #endregion

        #endregion
    }
}