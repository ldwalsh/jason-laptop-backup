﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViewEquipmentTypes.ascx.cs" Inherits="CW.Website._controls.admin.EquipmentTypes.ViewEquipmentTypes" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Data.Models.EquipmentType" %>
<%@ Import Namespace="CW.Utility" %>

<h2>View Equipment Types</h2> 

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" /> 
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>
          <asp:LinkButton ID="lnkEdit" Runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentTypeName" HeaderText="Equipment Type">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeName), 30) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Equipment Class" SortExpression="EquipmentClassName">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentClassName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentClassName), 30) %></label>
          <asp:HiddenField ID="hdnEquipmentClassID" runat="server" Visible="true" Value='<%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentClassID)) %>' />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Equipment Type Description">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeDescription)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeDescription), 100) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>
          <asp:LinkButton ID="lnkDelete" Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this equipment type permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<div>
  <!--SELECT EQUIPMENT TYPE DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptEquipmentTypeDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Equipment Type Details</h2>
          <ul id="equipmentTypeDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>

</div>

<!--EDIT EQUIPMENT PANEL -->
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateEquipmentType">
  <div>
    <h2>Edit Equipment Type</h2>
  </div>  

  <div>
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*Equipment Type Name:</label>
      <asp:TextBox ID="txtEditEquipmentTypeName" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Equipment Class Name:</label>
      <asp:Label ID="lblEditEquipmentClass" CssClass="labelContent" runat="server" />
      <p>(Note: An equipment types class cannot be changed as different equipment variables are associated to a class.)</p>
      <p>(Note: You cannot change the equipment class if a group was originally selected.)</p> 	 
    </div>
    <div class="divForm">
      <label class="label">Description:</label>
      <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server" />
      <div id="divEditDescriptionCharInfo"></div>
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentType" runat="server" Text="Edit Type"  OnClick="updateEquipmentTypeButton_Click" ValidationGroup="EditEquipmentType" />
  </div>

  <!--Ajax Validators-->
  <asp:RequiredFieldValidator ID="editEquipmentTypeNameRFV" runat="server" ErrorMessage="Equipment Type Name is a required field." ControlToValidate="txtEditEquipmentTypeName" SetFocusOnError="true" Display="None" ValidationGroup="EditEquipmentType" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editEquipmentTypeNameVCE" runat="server" BehaviorID="editEquipmentTypeNameVCE" TargetControlID="editEquipmentTypeNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>