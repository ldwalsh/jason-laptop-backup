﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentTypesToEquipmentType.ascx.cs" Inherits="CW.Website._controls.admin.EquipmentTypes.EquipmentTypesToEquipmentType" %>
<h2>Equipment Types To An Equipment Type</h2>

<p>Please select an equipment class and equipment type in order to select and assign equipment types.</p>

<div>
  <a id="lnkSetFocus" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblEquipmentTypesError" CssClass="errorMessage" runat="server" />
</div>
<div class="divForm">   
  <label class="label">*Select Equipment Class:</label>
  <asp:DropDownList ID="ddlEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>
<div class="divForm">
  <label class="label">*Equipment Type:</label>
  <asp:DropDownList ID="ddlEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentType_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<div id="equipmentTypes" visible="false" runat="server">

  <hr />

  <div class="divForm">
    <label class="label">Available:</label>
    <asp:ListBox ID="lbEquipmentTypesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    <div class="divArrows">
      <asp:ImageButton CssClass="up-arrow"  ID="btnUp"  OnClick="btnEquipmentTypesUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
      <asp:ImageButton CssClass="down-arrow" ID="btnDown"  OnClick="btnEquipmentTypesDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
    </div>

    <label class="label">Assigned:</label>
    <asp:ListBox ID="lbEquipmentTypesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
  </div>

  <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentTypes" runat="server" Text="Reassign" OnClick="updateEquipmentTypesButton_Click" ValidationGroup="EquipmentTypesToEquipmentType" />

</div>

<!--Ajax Validators-->
<asp:RequiredFieldValidator ID="equipmentTypesEquipmentClassRFV" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlEquipmentClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EquipmentTypesToEquipmentType" />
<ajaxToolkit:ValidatorCalloutExtender ID="equipmentTypesEquipmentClassVCE" runat="server" BehaviorID="equipmentTypesEquipmentClassVCE" TargetControlID="equipmentTypesEquipmentClassRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="equipmentTypesEquipmentTypeRFV" runat="server" ErrorMessage="Equipment Type is a required field." ControlToValidate="ddlEquipmentType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EquipmentTypesToEquipmentType" />
<ajaxToolkit:ValidatorCalloutExtender ID="equipmentTypesEquipmentTypeVCE" runat="server" BehaviorID="equipmentTypesEquipmentTypeVCE" TargetControlID="equipmentTypesEquipmentTypeRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />