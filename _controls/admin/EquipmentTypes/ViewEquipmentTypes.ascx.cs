﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.EquipmentType;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.EquipmentTypes
{
    public partial class ViewEquipmentTypes : AdminUserControlGrid
    {
        #region constants

            const String equipmentExists = "Cannot delete equipment type because one or more equipments are associated. Please disassociate from equipments first.";
            const String pointTypeAssocaitionExists = "Cannot delete equipment type because one or more point types are associated. Please disassociate from equipment type first.";
            const String analysisAssociationExists = "Cannot delete equipment type because one or more analyses are associated. Please disassociate from analysis first.";
            const String equipmentTypeAssociationExists = "Cannot delete equipment type because one or more equipment types are associated. Please disassociate equipment type from others and other from it first.";
            const String deleteSuccessful = "Equipment type deletion was successful.";
            const String deleteFailed = "Equipment type deletion failed. Please contact an administrator.";
            const String updateSuccessful = "Equipment type update was successful.";
            const String updateFailed = "Equipment type update failed. Please contact an administrator.";
            const String equipmentTypeExists = "An equipment type with that name already exists.";

        #endregion

        #region fields

            private enum EquipmentTypeDetails { Default };

        #endregion

        #region properties

            #region overrides

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    return DataMgr.EquipmentTypeDataMapper.GetAllFullSearchedEquipmentTypes(searchCriteria.FirstOrDefault());
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetEquipmentTypeData>(_ => _.EquipmentTypeID, _ => _.EquipmentTypeName, _ => _.EquipmentClassName, _ => _.EquipmentClassID, _ => _.EquipmentTypeDescription); }
                }

                protected override String NameField { get { return PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetEquipmentTypeData>(_ => _.EquipmentTypeID); } }

                protected override String Name { get { return typeof(EquipmentType).Name; } }

                protected override Type Entity { get { return typeof(GetEquipmentTypeData); } }

                public override IEnumerable<Enum> ReactToChangeStateList { get { return new Enum[] { KGSEquipmentTypeAdministration.TabMessages.AddEquipmentType }; } }

            #endregion

        #endregion

        #region events

            #region grid events

                #region overrides

                    protected override void OnGridDataBound(Object sender, GridViewRowEventArgs e)
                    {
                        //CANNOT DELETE CERTAIN TYPES
                        var rows = grid.Rows;

                        foreach (GridViewRow row in rows)
                        {
                            if ((row.RowState & DataControlRowState.Edit) == 0)
                            {
                                try
                                {
                                    //get rows datakey which right now is just one single key. pointtype id.
                                    var equipmentTypeID = Convert.ToInt32(grid.DataKeys[row.RowIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());

                                    if (BusinessConstants.EquipmentType.UnDeletableEquipmentTypesDictionary.ContainsValue(equipmentTypeID)) ((LinkButton)row.Cells[5].Controls[1]).Visible = false;
                                }
                                catch { }
                            }
                        }
                    }

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var equipmentTypeID = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());
                        var mEquipmentType = DataMgr.EquipmentTypeDataMapper.GetEquipmentTypeWithEquipmentClass(equipmentTypeID);

                        SetEquipmentTypeIntoEditForm(mEquipmentType);

                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                    protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
                    {
                        var equipmentTypeID = Convert.ToInt32(grid.DataKeys[e.RowIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());

                        try
                        {
                            //Only allow deletion of a equipment type if no equipment has been associated.                    
                            if (DataMgr.EquipmentDataMapper.IsEquipmentAssociatedWithEquipmentType(equipmentTypeID))
                            {
                                LabelHelper.SetLabelMessage(lblDeleteError, equipmentExists, lnkSetFocus);
                                return;
                            }

                            //Check equipmenttypes to pointtypes exist
                            if (DataMgr.EquipmentTypeDataMapper.IsEquipmentTypeAssociatedWithPointTypes(equipmentTypeID))
                            {
                                LabelHelper.SetLabelMessage(lblDeleteError, pointTypeAssocaitionExists, lnkSetFocus);
                                return;
                            }

                            //Check equipmenttypes to analyses exist
                            if (DataMgr.AnalysisEquipmentTypeDataMapper.IsEquipmentTypeAssociatedWithAnalyses(equipmentTypeID))
                            {
                                LabelHelper.SetLabelMessage(lblDeleteError, analysisAssociationExists, lnkSetFocus);
                                return;
                            }

                            //Check equipmenttypes to equipmenttypes exist
                            if (DataMgr.EquipmentTypeDataMapper.IsEquipmentTypeAssociatedWithEquipmentTypes(equipmentTypeID))
                            {
                                LabelHelper.SetLabelMessage(lblDeleteError, equipmentTypeAssociationExists, lnkSetFocus);
                                return;
                            }

                            //no need to delete equipmenttype_pointtype associations, because it should have already been unassociated.

                            DataMgr.EquipmentTypeDataMapper.DeleteEquipmentType(equipmentTypeID);
                            ChangeCurrentTab(deleteSuccessful);
                            SetChangeStateForTabs(KGSEquipmentTypeAdministration.TabMessages.DeleteEquipmentType);
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblDeleteError, deleteFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting equipment type.", ex);
                        }
                    }

                #endregion

            #endregion

            #region button events

                protected void updateEquipmentTypeButton_Click(Object sender, EventArgs e)
                {
                    var mEquipmentType = new EquipmentType();

                    LoadEditFormIntoEquipmentType(mEquipmentType);

                    if (DataMgr.EquipmentTypeDataMapper.DoesEquipmentTypeExist(mEquipmentType.EquipmentTypeID, mEquipmentType.EquipmentTypeName))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, equipmentTypeExists, lnkSetFocus);
                        return;
                    }
                    else
                    {          
                        try
                        {
                            DataMgr.EquipmentTypeDataMapper.UpdateEquipmentType(mEquipmentType);
                            OperationComplete(OperationType.Update, updateSuccessful, KGSEquipmentTypeAdministration.TabMessages.EditEquipmentType);
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment type.", ex);
                        }
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var equipmentType = DataMgr.EquipmentTypeDataMapper.GetFullEquipmentTypeByEquipmentTypeID(CurrentID).First();

                    SetEquipmentTypeIntoViewState(equipmentType);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(EquipmentTypeDetails.Default), rptEquipmentTypeDetails);
                }

            #endregion

            private void SetEquipmentTypeIntoViewState(GetEquipmentTypeData equipmentType)
            {
                SetEquipmentTypeDetails(equipmentType);
            }

            private void SetEquipmentTypeDetails(GetEquipmentTypeData equipmentType)
            {
                SetKeyValuePairItem("Equipment Type Name", equipmentType.EquipmentTypeName);
                SetKeyValuePairItem("Equipment Class Name", equipmentType.EquipmentClassName);
                SetKeyValuePairItem("Equipment Type Description", equipmentType.EquipmentTypeDescription);

                SetDetailsViewDataToViewState(Convert.ToInt32(EquipmentTypeDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            private void SetEquipmentTypeIntoEditForm(EquipmentType equipmentType)
            {
                hdnEditID.Value = Convert.ToString(equipmentType.EquipmentTypeID);
                txtEditEquipmentTypeName.Text = equipmentType.EquipmentTypeName;

                //NEVER editable because different equip variabels are associated to different equip classes
                lblEditEquipmentClass.Text = Convert.ToString(equipmentType.EquipmentClass.EquipmentClassName);
                txtEditDescription.Value = String.IsNullOrEmpty(equipmentType.EquipmentTypeDescription) ? null : equipmentType.EquipmentTypeDescription;
            }

            private void LoadEditFormIntoEquipmentType(EquipmentType equipmentType)
            {
                equipmentType.EquipmentTypeID = Convert.ToInt32(hdnEditID.Value);
                equipmentType.EquipmentTypeName = txtEditEquipmentTypeName.Text;
                equipmentType.EquipmentTypeDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //Equipment Class not editable
                equipmentType.DateModified = DateTime.UtcNow;
            }

        #endregion
    }
}