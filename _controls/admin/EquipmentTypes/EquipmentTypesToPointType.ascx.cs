﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.EquipmentTypes
{
    public partial class EquipmentTypesToPointType : AdminUserControlBase
    {
        #region constants

        const String unassignEquipmentTypeUpdateSuccessful = "Unassignment of equipment type {0} from point type {1} was successful.";
        const String unassignPointTypeRelationshipsStillExist = "Cannot unassign equipment type {0}, id {1} from point type {2}, id {3} because points with that type still exist on equipment with that type.";
        const String assignEquipmentTypeUpdateSuccessful = "Assignment of equipment type {0} from point type {1} was successful.";
        const String reassignUpdateFailed = "Equipment types reassignment failed. Please contact an administrator.";
        const String noItemsSelected = "No items were selected for reassignment.";
        const Char delimiter = Common.Constants.DataConstants.DefaultDelimiter;

        #endregion

        #region events

        private void Page_FirstLoad()
        {
            BindPointTypes(ddlPointTypes);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            assignmentMessaging.CssClass = "";
            lblResults.Visible = lblPointTypesError.Visible = false;
            ControlHelper.ToggleVisibility((AdminModeEnum == admin.AdminModeEnum.KGS), new Control[] { EquipmentTypeDownloadArea });
        }

        protected void ddlPointTypes_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            var ptid = Convert.ToInt32(ddlPointTypes.SelectedValue);

            if (ptid != -1) BindEquipmentTypesListBoxes(ptid);
        }

        protected void btnEquipmentTypesUpButton_Click(Object sender, EventArgs e)
        {
            while (lbEquipmentTypesBottom.SelectedIndex != -1)
            {
                lbEquipmentTypesTop.Items.Add(lbEquipmentTypesBottom.SelectedItem);
                lbEquipmentTypesBottom.Items.Remove(lbEquipmentTypesBottom.SelectedItem);
            }

            ControlHelper.SortListBox(lbEquipmentTypesTop);
        }

        protected void btnEquipmentTypesDownButton_Click(Object sender, EventArgs e)
        {
            while (lbEquipmentTypesTop.SelectedIndex != -1)
            {
                lbEquipmentTypesBottom.Items.Add(lbEquipmentTypesTop.SelectedItem);
                lbEquipmentTypesTop.Items.Remove(lbEquipmentTypesTop.SelectedItem);
            }

            ControlHelper.SortListBox(lbEquipmentTypesBottom);
        }

        protected void updateEquipmentTypesButton_Click(Object sender, EventArgs e)
        {
            var ptid = Convert.ToInt32(ddlPointTypes.SelectedValue);
            var pointTypeName = ddlPointTypes.SelectedItem.Text;

            var equipmentTypes = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypes();

            var selector = new Func<ListItem, EquipmentTypes_PointType>(li => 
                new EquipmentTypes_PointType { EquipmentTypeID = Convert.ToInt32(li.Value.Split(delimiter)[0]), PointTypeID = ptid});

            lblPointTypesError.Text = String.Empty;
            lblResults.Text = String.Empty;

            try
            {
                var unassignedEquipmentTypes = lbEquipmentTypesTop.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, true)).Select(selector).ToList();

                foreach (EquipmentTypes_PointType unassignedEquipmentType in unassignedEquipmentTypes)
                {
                    //Checks if a point type is associated with any points that are associated to equipment with that equipment type in order to see if we can delete the association.
                    var exists = DataMgr.EquipmentTypeDataMapper.IsPointTypeAssociatedWithAnyPointsAssociatedToEquipmentWithEquipmentType(unassignedEquipmentType.EquipmentTypeID, unassignedEquipmentType.PointTypeID);

                    if (exists)
                    {
                        var equipmentType = equipmentTypes.Where(et => et.EquipmentTypeID == unassignedEquipmentType.EquipmentTypeID).SingleOrDefault();

                        LabelHelper.SetLabelMessage(lblPointTypesError, 
                            String.Format(unassignPointTypeRelationshipsStillExist,
                                            equipmentType.EquipmentTypeName,
                                            unassignedEquipmentType.EquipmentTypeID,
                                            pointTypeName,
                                            unassignedEquipmentType.PointTypeID), lnkSetFocus);
                        return;
                    }
                }

                if (unassignedEquipmentTypes.Any())
                {
                    DataMgr.EquipmentTypeDataMapper.DeleteEquipmentTypePointTypes(unassignedEquipmentTypes);
                }

                SetMessages(unassignedEquipmentTypes, pointTypeName, equipmentTypes, unassignEquipmentTypeUpdateSuccessful);

                //---------------------------------------------------------------------------------------------------------

                var assignedEquipmentTypes = lbEquipmentTypesBottom.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, false)).Select(selector).ToList();

                if (assignedEquipmentTypes.Any())
                {
                    DataMgr.EquipmentTypeDataMapper.InsertEquipmentTypePointTypes(assignedEquipmentTypes);
                }

                SetMessages(assignedEquipmentTypes, pointTypeName, equipmentTypes, assignEquipmentTypeUpdateSuccessful);

                if (lblPointTypesError.Text == String.Empty && lblResults.Text == String.Empty)
                {
                    LabelHelper.SetLabelMessage(lblPointTypesError, noItemsSelected, lnkSetFocus);
                    return;
                }

                BindEquipmentTypesListBoxes(ptid);

                if (lblPointTypesError.Text != String.Empty) LabelHelper.SetLabelMessage(lblPointTypesError, lblPointTypesError.Text, lnkSetFocus);
                if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
            }
            catch (SqlException sqlEx)
            {
                LabelHelper.SetLabelMessage(lblPointTypesError, reassignUpdateFailed, lnkSetFocus);
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment types to point type.", sqlEx);
            }
            catch (Exception ex)
            {
                LabelHelper.SetLabelMessage(lblPointTypesError, reassignUpdateFailed, lnkSetFocus);
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment types to point type.", ex);
            }
        }

        #endregion

        #region methods

        private void SetMessages(List<EquipmentTypes_PointType> processedItems, String pointTypeName, IEnumerable<EquipmentType> items, String message)
        {
            int lineCounter = 0;

            foreach (var item in processedItems)
            {
                var updatedEquipmentTypeName = items.Single(et => et.EquipmentTypeID == item.EquipmentTypeID).EquipmentTypeName;

                lblResults.Text += String.Format(message, updatedEquipmentTypeName, pointTypeName) + "<br />";

                lineCounter++;
            }

            if (lineCounter > 15) assignmentMessaging.CssClass = "contentScrollBox";
        }

        protected override void InitializeControl()
        {
            new AdminUserControlForm(this).ResetDropDownLists();
            pnlEquipmentType.Visible = false;
            BindEquipmentTypesListBoxes(Convert.ToInt32(ddlPointTypes.SelectedValue));
        }

        #endregion

        #region binders

        private void BindPointTypes(DropDownList ddl)
        {
            var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

            ddl.Items.Clear();
            ddl.Items.Add(lItem);

            ddl.DataTextField = "PointTypeName";
            ddl.DataValueField = "PointTypeID";
            ddl.DataSource = DataMgr.PointTypeDataMapper.GetAllPointTypes();
            ddl.DataBind();
        }

        private void BindEquipmentTypesListBoxes(Int32 ptid)
        {
            lbEquipmentTypesTop.Items.Clear();
            lbEquipmentTypesBottom.Items.Clear();

            var notAssociated = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesNotAssociatedWithPTID(ptid);
            var associated = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesAssociatedWithPTID(ptid);
            var EquipmentTypeName = PropHelper.G<EquipmentType>(pt => pt.EquipmentTypeName);
            var EquipmentTypeID = PropHelper.G<EquipmentType>(pt => pt.EquipmentTypeID);

            if (notAssociated.Any()) ControlHelper.SetListBoxData(lbEquipmentTypesTop, notAssociated, EquipmentTypeName, EquipmentTypeID, false);
            if (associated.Any()) ControlHelper.SetListBoxData(lbEquipmentTypesBottom, associated, EquipmentTypeName, EquipmentTypeID, true);

            pnlEquipmentType.Visible = (notAssociated.Any() || associated.Any());
        }

            #endregion

        
    }
}