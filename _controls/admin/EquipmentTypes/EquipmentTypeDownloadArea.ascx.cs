﻿using CW.Business.Query;
using CW.Data;
using CW.Data.Models;
using CW.Reporting._fileservices;
using CW.Website._downloads;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.admin.EquipmentTypes
{
    public partial class EquipmentTypeDownloadArea : DownloadAreaBase<EquipmentTypePointType, EquipmentTypePointTypeDownloadGenerator>
    {
        #region properties

            #region overrides

                protected override Func<Dictionary<String, Object>, IEnumerable<EquipmentTypePointType>> GetData { get { return GetAllEquipmentTypePointTypes; } }

                protected override Dictionary<String, Object> Parameters { get { return null; } }

            #endregion

        #endregion

        #region methods

            public IEnumerable<EquipmentTypePointType> GetAllEquipmentTypePointTypes(Dictionary<String, Object> parameters)
            {
                var query = new EquipmentType_PointTypeQuery(DataMgr.ConnectionString, siteUser).LoadWith(_ => _.EquipmentType).LoadWith(_ => _.PointType).FinalizeLoadWith.GetAll();

                return GetEquipmentTypePointTypesTranformData(query.ToEnumerable());
            }

            private IEnumerable<EquipmentTypePointType> GetEquipmentTypePointTypesTranformData(IEnumerable<EquipmentTypes_PointType> equipmentTypePointTypes)
            {
                var equipmentTypePointTypeList = new List<EquipmentTypePointType>();

                foreach (var et_pt in equipmentTypePointTypes)
                {
                    var equipmentTypePointType = new EquipmentTypePointType();

                    equipmentTypePointType.EquipmentTypeId = et_pt.EquipmentTypeID;
                    equipmentTypePointType.EquipmentTypeName = et_pt.EquipmentType.EquipmentTypeName;
                    equipmentTypePointType.PointTypeId = et_pt.PointTypeID;
                    equipmentTypePointType.PointTypeName = et_pt.PointType.PointTypeName;
                    equipmentTypePointType.DisplayName = et_pt.PointType.DisplayName;

                    equipmentTypePointTypeList.Add(equipmentTypePointType);
                }

                return equipmentTypePointTypeList.AsEnumerable();
            }

        #endregion
    }
}