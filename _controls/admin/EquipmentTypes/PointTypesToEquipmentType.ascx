﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PointTypesToEquipmentType.ascx.cs" Inherits="CW.Website._controls.admin.EquipmentTypes.PointTypesToEquipmentType" %>
<%@ Register TagPrefix="CW" TagName="EquipmentTypeDownloadArea" Src="~/_controls/admin/EquipmentTypes/EquipmentTypeDownloadArea.ascx" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Point Types To An Equipment Type">
    <SubTitleTemplate>
        <p>Please select an equipment class and equipment type in order to select and assign point types.</p>
    </SubTitleTemplate>
    <RightAreaTemplate>
        <CW:EquipmentTypeDownloadArea runat="server" ID="EquipmentTypeDownloadArea" Visible="false" />
    </RightAreaTemplate>
</CW:TabHeader>

<asp:Panel ID="assignmentMessaging" runat="server">
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblPointTypesError" CssClass="errorMessage" runat="server" />
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
</asp:Panel>

<div class="divForm">
    <label class="label">*Select Equipment Class:</label>
    <asp:DropDownList ID="ddlEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>
<div class="divForm">
    <label class="label">*Equipment Type:</label>
    <asp:DropDownList ID="ddlEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentType_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<div id="pointTypes" visible="false" runat="server">

    <hr />

    <div class="divForm">
        <label class="label">Available:</label>
        <asp:ListBox ID="lbPointTypesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

        <div class="divArrows">
            <asp:ImageButton CssClass="up-arrow" ID="btnUp" OnClick="btnPointTypesUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
            <asp:ImageButton CssClass="down-arrow" ID="btnDown" OnClick="btnPointTypesDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
        </div>

        <label class="label">Assigned:</label>
        <asp:ListBox ID="lbPointTypesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdatePointTypes" runat="server" Text="Reassign" OnClick="updatePointTypesButton_Click" ValidationGroup="PointTypesToEquipmentType" />

</div>

<!--Ajax Validators-->
<asp:RequiredFieldValidator ID="pointTypesEquipmentClassRFV" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlEquipmentClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="PointTypesToEquipmentType" />
<ajaxToolkit:ValidatorCalloutExtender ID="pointTypesEquipmentClassVCE" runat="server" BehaviorID="pointTypesEquipmentClassVCE" TargetControlID="pointTypesEquipmentClassRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="pointTypesEquipmentTypeRFV" runat="server" ErrorMessage="Equipment Type is a required field." ControlToValidate="ddlEquipmentType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="PointTypesToEquipmentType" />
<ajaxToolkit:ValidatorCalloutExtender ID="pointTypesEquipmentTypeVCE" runat="server" BehaviorID="pointTypesEquipmentTypeVCE" TargetControlID="pointTypesEquipmentTypeRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
