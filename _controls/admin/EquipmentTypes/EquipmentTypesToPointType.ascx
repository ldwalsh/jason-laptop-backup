﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentTypesToPointType.ascx.cs" Inherits="CW.Website._controls.admin.EquipmentTypes.EquipmentTypesToPointType" %>
<%@ Register TagPrefix="CW" TagName="EquipmentTypeDownloadArea" Src="~/_controls/admin/EquipmentTypes/EquipmentTypeDownloadArea.ascx" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Equipment Types To A Point Type">
    <SubTitleTemplate>
        <p>Please select a point type in order to select and assign equipment types.</p>
    </SubTitleTemplate>
   <RightAreaTemplate>
        <CW:EquipmentTypeDownloadArea runat="server" ID="EquipmentTypeDownloadArea" Visible="false" />
    </RightAreaTemplate>
</CW:TabHeader>

<asp:Panel ID="assignmentMessaging" runat="server">
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblPointTypesError" CssClass="errorMessage" runat="server" />
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
</asp:Panel>

<div class="divForm">
    <label class="label">Point Type:</label>
    <asp:DropDownList ID="ddlPointTypes" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlPointTypes_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<asp:Panel ID="pnlEquipmentType" Visible="false" runat="server">
    <div class="divForm">
        <label class="label">Available:</label>
        <asp:ListBox ID="lbEquipmentTypesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

        <div class="divArrows">
            <asp:ImageButton CssClass="up-arrow" ID="btnUp" OnClick="btnEquipmentTypesUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
            <asp:ImageButton CssClass="down-arrow" ID="btnDown" OnClick="btnEquipmentTypesDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
        </div>

        <label class="label">Assigned:</label>
        <asp:ListBox ID="lbEquipmentTypesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentTypes" runat="server" Text="Reassign" OnClick="updateEquipmentTypesButton_Click" ValidationGroup="EquipmentTypesToPointType" />
</asp:Panel>
