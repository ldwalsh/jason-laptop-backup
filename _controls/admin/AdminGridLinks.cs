﻿using CW.Common.Constants;
using CW.Utility;
using CW.Website._controls.admin.Buildings;
using CW.Website._controls.admin.Equipment;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.admin
{
    public sealed class AdminGridLinks : SiteUserControl
    {
        #region fields

            private List<KeyValuePair<String, Object>> defaultItems;
            private readonly String controlText = "Control";

        #endregion

        #region properties

            public Dictionary<AdminUserControlGridAdminLinks.AdminPage, List<KeyValuePair<String, Object>>> Dict { get; set; }

        #endregion

        #region constructor

            public AdminGridLinks(List<String> defaultFields, List<AdminUserControlGridAdminLinks.AdminPage> adminPages)
            {
                SetDefaultItems(defaultFields);
                SetAdminPagesDict(adminPages);
            }

        #endregion

        #region methods

            private void SetDefaultItems(List<String> defaultFields)
            {
                if (defaultFields.Any())
                {
                    defaultItems = new List<KeyValuePair<String, Object>>();

                    foreach (var defaultField in defaultFields)
                        defaultItems.Add(new KeyValuePair<String, Object>(defaultField, null));
                }
            }

            private void SetAdminPagesDict(List<AdminUserControlGridAdminLinks.AdminPage> adminPages)
            {
                if (adminPages.Any())
                {
                    Dict = new Dictionary<AdminUserControlGridAdminLinks.AdminPage, List<KeyValuePair<String, Object>>>();

                    foreach (var adminPage in adminPages)
                    {
                        var items = new List<KeyValuePair<String, Object>>();

                        items.AddRange(defaultItems);

                        switch (adminPage)
                        {
                            case AdminUserControlGridAdminLinks.AdminPage.AnalysesToEquipment:
                                AddItems(adminPage, items, typeof(AnalysesToAnEquipment));
                                break;
                            case AdminUserControlGridAdminLinks.AdminPage.BuildingVariables:
                                AddItems(adminPage, items, typeof(BuildingVariables));
                                break;
                            case AdminUserControlGridAdminLinks.AdminPage.EquipmentToEquipment:
                                AddItems(adminPage, items, typeof(EquipmentToEquipment));
                                break;
                            case AdminUserControlGridAdminLinks.AdminPage.EquipmentVariables:
                                AddItems(adminPage, items, typeof(EquipmentVariables));
                                break;
                            case AdminUserControlGridAdminLinks.AdminPage.Diagnostics:
                                AddDiagnosticsItems(adminPage, items);
                                break;
                        }

                        Dict.Add(adminPage, items);
                    }
                }
            }

            private void AddItems(AdminUserControlGridAdminLinks.AdminPage adminPage, List<KeyValuePair<String, Object>> items, Type type)
            {
                items.Add(new KeyValuePair<String, Object>(controlText, type.Name));
            }

            private void AddDiagnosticsItems(AdminUserControlGridAdminLinks.AdminPage adminPage, List<KeyValuePair<String, Object>> items)
            {
                DateTime mStartDate, mEndDate;
                DateTimeHelper.GenerateDefaultDatesYesterday(out mStartDate, out mEndDate, siteUser.EarliestBuildingTimeZoneID);

                items.Add(new KeyValuePair<String, Object>("cid", siteUser.CID));
                items.Add(new KeyValuePair<String, Object>("rng", Enum.GetName(typeof(DataConstants.AnalysisRange), DataConstants.AnalysisRange.Daily)));
                items.Add(new KeyValuePair<String, Object>("sd", mStartDate.AddDays(-1).ToShortDateString()));
                items.Add(new KeyValuePair<String, Object>("ed", mEndDate.ToShortDateString()));
            }

        #endregion
    }
}