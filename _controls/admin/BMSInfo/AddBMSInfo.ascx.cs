﻿using CW.Common.Constants;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.BMSInfo
{
    public partial class AddBMSInfo : AdminUserControlBase
    {
        #region constants

        const String addBMSInfoSuccess = "BMS Info addition was successful.";
        const String addBMSInfoFailed = "Adding BMS Info failed. Please contact an administrator.";
        const String addBMSInfoError = "Error adding BMS Info.";
        const String bmsInfoExists = "A BMS Info with that name already exists.";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAddError.Visible = false;

                if (!Page.IsPostBack)
                {
                }
            }

            #region ddl events


            #endregion

            #region button events

                protected void addBMSInfoButton_Click(Object sender, EventArgs e)
                {
                    if (DataMgr.BMSInfoDataMapper.DoesBMSInfoNameExistForClient(siteUser.CID, txtAddBMSInfoName.Text))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, bmsInfoExists, lnkSetFocus);
                        return;
                    }

                    var bmsInfo = new CW.Data.BMSInfo();
                    LoadAddFormIntoBMSInfo(bmsInfo);

                    try
                    {
                        int bmsInfoID = DataMgr.BMSInfoDataMapper.InsertBMSInfo(bmsInfo, ConfigMgr.GetConfigurationSetting(DataConstants.EncryptionParaphrase, "", true));

                        LabelHelper.SetLabelMessage(lblResults, bmsInfo.BMSInfoName + addBMSInfoSuccess, lnkSetFocus);
                        OperationComplete(SystemBMSInfoAdministration.TabMessages.AddBMSInfo);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, addBMSInfoError, ex);
                        LabelHelper.SetLabelMessage(lblAddError, addBMSInfoFailed, lnkSetFocus);
                    }
                }


            #endregion

        #endregion

        #region methods

            private void LoadAddFormIntoBMSInfo(CW.Data.BMSInfo bmsInfo)
            {
                bmsInfo.BMSInfoName = txtAddBMSInfoName.Text;
                bmsInfo.CID = siteUser.CID;
                bmsInfo.Endpoint = txtAddBMSInfoEndpoint.Text;
                bmsInfo.Username = txtAddBMSInfoUsername.Text;

                if (!String.IsNullOrWhiteSpace(txtAddBMSInfoPassword.Text))
                    bmsInfo.Password = txtAddBMSInfoPassword.Text;

                bmsInfo.ExtraInfo = txtAddBMSInfoExtraInfo.Text;
            }

        #endregion
    }
}