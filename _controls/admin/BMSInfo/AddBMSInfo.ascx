﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddBMSInfo.ascx.cs" Inherits="CW.Website._controls.admin.BMSInfo.AddBMSInfo" %>

<asp:Panel ID="pnlAddBMSInfo" runat="server" DefaultButton="btnAddBMSInfo">

  <h2>Add BMS Information</h2>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />
    <div class="divForm">
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>
    <div class="divForm">
      <label class="label">*BMS Info Name:</label>
      <asp:TextBox ID="txtAddBMSInfoName" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Endpoint:</label>
      <asp:TextBox ID="txtAddBMSInfoEndpoint" CssClass="textbox" MaxLength="512" runat="server" />
    </div>  
    <div class="divForm">
      <label class="label">Username:</label>
      <asp:TextBox ID="txtAddBMSInfoUsername" CssClass="textbox" MaxLength="100" runat="server" />
    </div>  
    <div class="divForm">
      <label class="label">Password:</label>
      <asp:TextBox ID="txtAddBMSInfoPassword" CssClass="textbox" MaxLength="100" runat="server" />
    </div>  
    <div class="divForm">
      <label class="label">Extra Info:</label>
      <asp:TextBox ID="txtAddBMSInfoExtraInfo" CssClass="textbox" MaxLength="512" runat="server" />
    </div> 
  </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnAddBMSInfo" runat="server" Text="Add BMS Info"  OnClick="addBMSInfoButton_Click" ValidationGroup="AddBMSInfo" />

  <asp:RequiredFieldValidator ID="addBMSInfoNameRFV" runat="server" ErrorMessage="BMS Info Name is a required field." ControlToValidate="txtAddBMSInfoName" SetFocusOnError="true" Display="None" ValidationGroup="AddBMSInfo" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addBMSInfoNameVCE" runat="server" BehaviorID="addBMSInfoNameVCE" TargetControlID="addBMSInfoNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addBMSInfoEndpointRFV" runat="server" ErrorMessage="BMS Info Endpoint is a required field." ControlToValidate="txtAddBMSInfoEndpoint" SetFocusOnError="true" Display="None" ValidationGroup="AddBMSInfo" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addBMSInfoEndpointVCE" runat="server" BehaviorID="addBMSInfoEndpointVCE" TargetControlID="addBMSInfoEndpointRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            
</asp:Panel>    