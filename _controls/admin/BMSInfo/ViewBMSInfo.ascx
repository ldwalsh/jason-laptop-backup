﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewBMSInfo.ascx.cs" Inherits="CW.Website._controls.admin.BMSInfo.ViewBMSInfo" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Common.Constants" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<h2>View BMS Information</h2>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>  
                                
<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div> 
                                   
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>       
                                                                                                                
<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkEdit" Runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building Name">
        <ItemTemplate>
            <label title="<%# GetCellContent(Container, PropHelper.G<BMSInfo>(_ => _.BMSInfoName)) %>"><%# GetCellContent(Container, PropHelper.G<BMSInfo>(_ => _.BMSInfoName), 100) %></label>
            <asp:HiddenField ID="hdnCID" runat="server" Visible="true" Value='<%# GetCellContent(Container, PropHelper.G<BMSInfo>(_=>_.CID)) %>' />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Endpoint" HeaderText="Endpoint">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<BMSInfo>(_ => _.Endpoint)) %>"><%# GetCellContent(Container, PropHelper.G<BMSInfo>(_ => _.Endpoint), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Username" HeaderText="Username">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<BMSInfo>(_ => _.Username)) %>"><%# GetCellContent(Container, PropHelper.G<BMSInfo>(_ => _.Username), 23) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ExtraInfo" HeaderText="Extra Info">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<BMSInfo>(_ => _.ExtraInfo)) %>"><%# GetCellContent(Container, PropHelper.G<BMSInfo>(_ => _.ExtraInfo), 23) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkDelete" Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this BMS Information permanently?');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>        
  </asp:GridView>                                                                      
</div>
<br />
<br />

<div>
  <!--SELECT DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptBMSInfoDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>BMS Info Details</h2>
          <ul id="bmsInfoDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
          <br />
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>

<!--EDIT BUILDING PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateBMSInfo">                                                                                             
  <div>
    <h2>Edit BMS Information</h2>
  </div>

  <div>
    <a id="lnkSetFocusEdit" runat="server"></a>
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
    <asp:HiddenField ID="hdnEditCID" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*BMS Info Name:</label>
      <asp:TextBox ID="txtEditBMSInfoName" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Endpoint:</label>
      <asp:TextBox ID="txtEditEndpoint" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Username:</label>
      <asp:TextBox ID="txtEditUsername" CssClass="textbox" MaxLength="100" runat="server" />
    </div>                                                                                                                                                                                           
     <div class="divForm">
      <label class="label">Password:</label>
      <asp:TextBox ID="txtEditPassword" CssClass="textbox" MaxLength="100" runat="server" />
      <p>(Note: Password will be updated if filled out.)</p>
    </div>     
     <div class="divForm">
      <label class="label">Extra Information:</label>
      <asp:TextBox ID="txtEditExtraInfo" TextMode="multiline" Columns="40" Rows="5" CssClass="textbox" MaxLength="200" runat="server" />
    </div>     
                                                                                                                                                     
    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBMSInfo" runat="server" Text="Update BMS Info"  OnClick="updateBMSInfoButton_Click" ValidationGroup="EditBMSInfo" />

  </div>
                                                
  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="editBMSInfoNameRFV" runat="server" ErrorMessage="BMS Info Name is a required field." ControlToValidate="txtEditBMSInfoName" SetFocusOnError="true" Display="None" ValidationGroup="EditBMSInfo" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editBMSInfoNameVCE" runat="server" BehaviorID="editBMSInfoNameVCE" TargetControlID="editBMSInfoNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editBMSInfoEndpointRFV" runat="server" ErrorMessage="BMS Info Endport is a required field." ControlToValidate="txtEditEndpoint" SetFocusOnError="true" Display="None" ValidationGroup="EditBMSInfo" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editBMSInfoEndpointVCE" runat="server" BehaviorID="editBMSInfoEndpointVCE" TargetControlID="editBMSInfoEndpointRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                                                                                                                                                                                                                                                                            
</asp:Panel>