﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.BMSInfo
{
    public partial class ViewBMSInfo : AdminUserControlGrid
    {
        #region constants

            const String deleteSuccessful = "BMS Info deletion was successful.";
            const String deleteFailed = "BMS Info deletion failed. Please contact an administrator.";
            const String updateSuccessful = "BMS Info update was successful.";
            const String updateFailed = "BMS Info update failed. Please contact an administrator.";
            const String updateBMSInfoNameExists = "A BMS Info with that name already exists.";

        #endregion

        #region fields

            private enum BMSInfoDetails { Default };

        #endregion

        #region properties

            #region AdminUserControlGrid overrides

                protected override IEnumerable<string> GridColumnNames
                {
                    get { return PropHelper.G<CW.Data.BMSInfo>(_ => _.CID, _ => _.BMSInfoName, _ => _.Endpoint, _ => _.Username, _ => _.ExtraInfo); }
                }

                protected override string NameField { get { return PropHelper.G<CW.Data.BMSInfo>(_ => _.BMSInfoName); } }

                protected override string IdColumnName { get { return PropHelper.G<CW.Data.BMSInfo>(_ => _.BMSInfoID); } }

                protected override string Name { get { return "BMS Info"; } }

                protected override Type Entity { get { return typeof(CW.Data.BMSInfo); } }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<string> searchCriteria)
                {
                    return DataMgr.BMSInfoDataMapper.GetBMSInfoByClientID(searchCriteria.FirstOrDefault(), siteUser.CID);
                }

                protected override String CacheHashKey { get { return FormatHashKey(new object[] { siteUser.CID }); } }

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[] 
                            {
                                SystemBMSInfoAdministration.TabMessages.AddBMSInfo,
                                SystemBMSInfoAdministration.TabMessages.EditBMSInfo,
                                SystemBMSInfoAdministration.TabMessages.DeleteBMSInfo
                            };
                    }
                }

            #endregion

                public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid)
                {
                    return new GridCacher.CacheKeyInfo(typeof(CW.Data.BMSInfo).Name, String.Join("|", new String[] { cid.ToString() }));
                }

        #endregion

        #region events

            private void Page_FirstLoad()
            {

            }

            private void Page_Load()
            {

            }

            #region grid events

                #region overrides

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var bmsInfoID = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());
                        var bmsInfo = DataMgr.BMSInfoDataMapper.GetBMSInfo(bmsInfoID, ConfigMgr.GetConfigurationSetting(DataConstants.EncryptionParaphrase, "", true));

                        SetBMSInfoIntoEditForm(bmsInfo);
                        
                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                    protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
                    {
                        var bmsInfoID = Convert.ToInt32(grid.DataKeys[e.RowIndex].Value);

                        try
                        {
                            DataMgr.BMSInfoDataMapper.DeleteBMSInfo(bmsInfoID);
                            ChangeCurrentTab(deleteSuccessful);
                            SetChangeStateForTabs(SystemBMSInfoAdministration.TabMessages.DeleteBMSInfo);
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblDeleteError, deleteFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting equipment.", ex);
                        }
                    }

                #endregion

            #endregion

            #region button events

                protected void updateBMSInfoButton_Click(Object sender, EventArgs e)
                {
                    var bmsInfo = new CW.Data.BMSInfo();

                    LoadEditFormIntoBMSInfo(bmsInfo);

                    if (DataMgr.BMSInfoDataMapper.DoesBMSInfoNameExistForClient(siteUser.CID, txtEditBMSInfoName.Text, bmsInfo.BMSInfoID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateBMSInfoNameExists, lnkSetFocus);
                        return;
                    }

                    try
                    {
                        DataMgr.BMSInfoDataMapper.UpdateBMSInfo(bmsInfo, ConfigMgr.GetConfigurationSetting(DataConstants.EncryptionParaphrase, "", true));
                        OperationComplete(OperationType.Update, updateSuccessful, SystemBMSInfoAdministration.TabMessages.EditBMSInfo);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating bms info.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void TabStateMonitor_OnChangeState(TabBase tab, IEnumerable<Enum> messages)
                {
                    base.TabStateMonitor_OnChangeState(tab, messages);
                }

                protected override void SetDataForDetailsViewFromDB()
                {
                    var bmsInfo = DataMgr.BMSInfoDataMapper.GetBMSInfo(CurrentID, ConfigMgr.GetConfigurationSetting(DataConstants.EncryptionParaphrase, "", true));
                    SetBMSInfoIntoViewState(bmsInfo);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(BMSInfoDetails.Default), rptBMSInfoDetails);
                }

            #endregion

                private void SetBMSInfoIntoViewState(CW.Data.BMSInfo bmsInfo)
                {
                    SetKeyValuePairItem("BMS Info Name", bmsInfo.BMSInfoName);
                    SetKeyValuePairItem("Endpoint", bmsInfo.Endpoint);
                    SetKeyValuePairItem("Username", bmsInfo.Username);
                    SetKeyValuePairItem("Extra Info", bmsInfo.ExtraInfo);

                    SetDetailsViewDataToViewState(Convert.ToInt32(BMSInfoDetails.Default), true); //True means to clear viewstate since this is a new record
                }


            private void SetBMSInfoIntoEditForm(CW.Data.BMSInfo bmsInfo)
            {
                hdnEditID.Value = Convert.ToString(bmsInfo.BMSInfoID);
                hdnEditCID.Value = Convert.ToString(bmsInfo.CID);
                txtEditBMSInfoName.Text = bmsInfo.BMSInfoName;
                txtEditPassword.Text = bmsInfo.Password;
                txtEditEndpoint.Text = bmsInfo.Endpoint;
                txtEditExtraInfo.Text = bmsInfo.ExtraInfo;
                txtEditUsername.Text = bmsInfo.Username;
            }

            private void LoadEditFormIntoBMSInfo(CW.Data.BMSInfo bmsInfo)
            {
                bmsInfo.BMSInfoID = Convert.ToInt32(hdnEditID.Value);
                bmsInfo.CID = Convert.ToInt32(hdnEditCID.Value);
                bmsInfo.BMSInfoName = txtEditBMSInfoName.Text;
                bmsInfo.Endpoint = txtEditEndpoint.Text;
                bmsInfo.Username = txtEditUsername.Text;

                if (!String.IsNullOrWhiteSpace(txtEditPassword.Text))
                    bmsInfo.Password = txtEditPassword.Text;

                bmsInfo.ExtraInfo = txtEditExtraInfo.Text;
            }

            #region binders


            #endregion

        #endregion
    }
}