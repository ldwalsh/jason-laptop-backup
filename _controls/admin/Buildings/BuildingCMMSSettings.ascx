﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingCMMSSettings.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.BuildingCMMSSettings" %>
<%@ Register src="~/_administration/_shared/CMMS/CMMSSettingsView.ascx" tagPrefix="CW" tagName="CMMSSettingsView" %>

<script type="text/javascript" src="../../../_assets/scripts/testConnection.js"></script>

<h2>Work Order Settings</h2>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblMessage" CssClass="successMessage" runat="server" />
</p> 

<div class="divForm">
  <label class="label">*Select Building:</label>
  <asp:DropDownList ID="ddlBuilding" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" CssClass="dropdown" />
</div>

<asp:Panel ID="pnlCMMSSettingsView" runat="server" Visible="false">

  <div class="divForm">
    <label class="label">Enable:</label>
    <asp:CheckBox ID="chkEnable" runat="server" CssClass="checkbox" AutoPostBack="true" OnCheckedChanged="chkEnable_CheckedChanged" />
  </div>

  <div class="divForm">
    <label class="label">Inherit:</label>
    <asp:CheckBox ID="chkInherit" runat="server" CssClass="checkbox" AutoPostBack="true" OnCheckedChanged="chkInherit_CheckedChanged" Enabled="false" />
  </div>

  <CW:CMMSSettingsView ID="CMMSSettingsView" runat="server" />

  <asp:LinkButton ID="btnUpdate" runat="server" Text="Update" CssClass="lnkButton" OnClick="UpdateButton_Click" ValidationGroup="WorkOrder" />
  <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CssClass="lnkButton" OnClick="DeleteButton_Click" OnClientClick="return confirm('Are you sure you wish to delete this CMMS Configuration?')" CausesValidation="false" />

</asp:Panel>