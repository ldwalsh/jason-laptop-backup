﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingStats.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.BuildingStats" %>
<%@ Import Namespace="CW.Data.Models.Building" %>
<%@ Import Namespace="CW.Utility" %>

<h2>Building Stats</h2>

<p>Inital loading of this tab may take some time.  Subsequent operations (sorting, paging and searching) will process quickly by utilizing cached data.</p>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
</p>

<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div> 

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building Name">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<BuildingAdministrationStatistics>(_ => _.BuildingName)) %>"><%# GetCellContent(Container, PropHelper.G<BuildingAdministrationStatistics>(_ => _.BuildingName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="BuildingVariablesCount" HeaderText="# of Building Variables">
        <ItemTemplate><asp:HyperLink ID="hlBuildingVariables" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<BuildingAdministrationStatistics>(_ => _.BuildingVariablesCount)) %>' /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="EquipmentCount" HeaderText="# of Equipment">
        <ItemTemplate><asp:HyperLink ID="hlEquipment" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<BuildingAdministrationStatistics>(_ => _.EquipmentCount)) %>' /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="PointCount" HeaderText="# of Points">
        <ItemTemplate><asp:HyperLink ID="hlPoints" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<BuildingAdministrationStatistics>(_ => _.PointCount)) %>' /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="AssociatedPointCount" HeaderText="# of Point Associations">
        <ItemTemplate><asp:HyperLink ID="hlAssociatedPoints" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<BuildingAdministrationStatistics>(_ => _.AssociatedPointCount)) %>' /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="AnalysesYesterdayCount" HeaderText="# of Analyses Yesterday (Daily)">
        <ItemTemplate><asp:HyperLink ID="hlDiagnostics" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<BuildingAdministrationStatistics>(_ => _.AnalysesYesterdayCount)) %>' /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="ScheduledAnalysesCount" HeaderText="# of Sch Analyses (All)">
        <ItemTemplate><asp:HyperLink ID="hlScheduledAnalyses" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<BuildingAdministrationStatistics>(_ => _.ScheduledAnalysesCount)) %>' /></ItemTemplate>
      </asp:TemplateField>                                                                                                                                         
    </Columns>
  </asp:GridView>
</div> 