﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingVariablesList.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.BuildingVariablesList" %>
<%@ Import Namespace="CW.Data.Models.BuildingVariable" %>
<%@ Import Namespace="CW.Utility" %>

<hr />

<h2>Variables List</h2>

<div>
  <asp:Label ID="lblBuildingVarsEmpty" Visible="false" runat="server" />
</div>

<asp:Repeater ID="rptBuildingVars" Visible="false" runat="server">
  <HeaderTemplate>
    <ul class="detailsListProperties">
  </HeaderTemplate>
  <ItemTemplate>
    <li>
      <strong><%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.BuildingVariableDisplayName)) %></strong>

      <dl>
        <%# (!String.IsNullOrWhiteSpace(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.Value)).ToString())) ? "<dt>Value:</dt> <dd>" + Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.Value)) + "</dd>" : String.Empty %>
        <%# (!String.IsNullOrWhiteSpace(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IPDefaultValue)).ToString())) ? "<dt>IP Default Value:</dt> <dd>" + Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IPDefaultValue)) + "</dd>" : String.Empty %>
        <%# (!String.IsNullOrWhiteSpace(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.SIDefaultValue)).ToString())) ? "<dt>SI Default Value:</dt> <dd>" + Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.SIDefaultValue)) + "</dd>" : String.Empty %>
        <%# (!String.IsNullOrWhiteSpace(Convert.ToString(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.BuildingVariableDescription))))) ? "<dt>Description:</dt> <dd>" + Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.BuildingVariableDescription)) + "</dd>" : String.Empty %>
      </dl>

    </li>
  </ItemTemplate>
  <FooterTemplate>
    </ul>
  </FooterTemplate>
</asp:Repeater>