﻿using CW.Business.Blob.Images;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Building;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using CW.Website.DependencyResolution;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Buildings
{
    public partial class ViewBuildings : AdminUserControlGridAdminLinks
    {
        #region constants

            const String buildingExists = "A building with that name already exists.";
            const String equipmentExists = "Cannot delete building because equipment is associated. Please delete equipment first.";
            const String filesExist = "Cannot delete building because files in the documents are associated. Please delete files first.";            
            const String deleteSuccessful = "Building deletion was successful.";
            const String deleteFailed = "Building deletion failed. Please contact an administrator.";
            const String updateSuccessful = "Building update was successful.";
            const String updateFailed = "Building update failed. Please contact an administrator.";
            const String noBuildingImage = "no-building-image.png";
            
        #endregion

        #region fields

            protected String buildingImageUrl = String.Empty;
            private enum BuildingDetails { Default, SAS, Internal, Account };
            private List<KeyValuePair<String, Object>> buildingVariablesItems = new List<KeyValuePair<String, Object>>();
            private List<KeyValuePair<String, Object>> diagnosticsItems = new List<KeyValuePair<String, Object>>();

        #endregion

        #region properties

            #region overrides

                protected override LinkTypes LinkType { get { return LinkTypes.DropDownLink; } }
        
                protected override Dictionary<AdminPage, List<KeyValuePair<String, Object>>> GetLinksAndItemsToAdd()
                {
                    var defaultFields = new List<String>() { PropHelper.G<Building>(_ => _.BID) };
                    var adminPages = new List<AdminPage>() { AdminPage.BuildingVariables, AdminPage.Diagnostics, AdminPage.Equipment, AdminPage.Points, AdminPage.ScheduledAnalyses };

                    return new AdminGridLinks(defaultFields, adminPages).Dict;
                }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchText)
                {
                    var buildingExpression = new Expression<Func<Building, Object>>[] { (_ => _.BuildingType), (_ => _.Client), (_ => _.Country), (_ => _.State) };
                    var buildingTypeExpression = new Expression<Func<BuildingType, Object>>[] { (_ => _.BuildingClass) };

                    if (siteUser.IsKGSSuperAdminOrHigher || (siteUser.IsLoggedInUnderProviderClient && siteUser.IsSuperAdmin))
                        return DataMgr.BuildingDataMapper.GetAllSearchedBuildings(searchText.FirstOrDefault(), siteUser.CID, siteUser.UID, siteUser.IsKGSSuperAdminOrHigher, chkAllClients.Checked, false, false, buildingExpression, buildingTypeExpression);
                    else
                        return DataMgr.BuildingDataMapper.GetAllSearchedBuildings(searchText.FirstOrDefault(), siteUser.CID, siteUser.UID, siteUser.IsKGSSuperAdminOrHigher, false, true, true, buildingExpression, buildingTypeExpression);
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetBuildingsData>(_ => _.BID, _ => _.BuildingName, _ => _.ClientName, _ => _.CID, _ => _.Address, _ => _.City, _ => _.Sqft, _ => _.CountryName, _ => _.BuildingTypeName, _ => _.IsActive, _ => _.IsVisible); }
                }

                protected override String NameField { get { return PropHelper.G<GetBuildingsData>(_ => _.BuildingName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetBuildingsData>(_ => _.BID); } }

                protected override String Name { get { return typeof(Building).Name; } }

                protected override Type Entity { get { return typeof(GetBuildingsData); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID, siteUser.UID }); } }

                public override IEnumerable<Enum> ReactToChangeStateList { get { return new Enum[] { KGSBuildingAdministration.TabMessages.AddBuilding }; } }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid, Int32 uid) {
                return new GridCacher.CacheKeyInfo(typeof(Building).Name + "." + typeof(GetBuildingsData).Name, cid.ToString() +  "|" + uid.ToString());
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                divSuperSearch.Visible = siteUser.IsKGSSuperAdminOrHigher;
            }

            #region grid events

                #region overrides

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var bid = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());
                        var mBuilding = DataMgr.BuildingDataMapper.GetBuildingByBID(bid, true, false, false, false, true, false);

                        BindCountries(ddlEditCountry);
                        BindTimeZones(ddlEditTimeZone);
                        BindBuildingClasses(ddlEditBuildingClass);
                        BindBuildingTypes(ddlEditBuildingType, mBuilding.BuildingType.BuildingClassID);
                        BindWeatherFiles(ddlEditGlobalFile);

                        SetBuildingIntoEditForm(mBuilding);

                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                    protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
                    {
                        var bid = Convert.ToInt32(grid.DataKeys[e.RowIndex].Value);

                        try
                        {
                            var hasEquipment = DataMgr.EquipmentDataMapper.IsEquipmentAssignedToBuilding(bid);

                            if (hasEquipment)
                            {
                                LabelHelper.SetLabelMessage(lblDeleteError, equipmentExists, lnkSetFocus);
                                return;
                            }
                            else
                            {
                                if (DataMgr.FileDataMapper.DoFilesExistForBID(bid))
                                {
                                    LabelHelper.SetLabelMessage(lblDeleteError, filesExist, lnkSetFocus);
                                    return;
                                }
                                else
                                {
                                    DataMgr.GoalDataMapper.RemoveBuildingGoalsByBID(bid);
                                    DataMgr.BuildingDataMapper.DeleteBuilding(bid);
                                    OperationComplete(OperationType.Delete, deleteSuccessful, KGSBuildingAdministration.TabMessages.DeleteBuilding);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblDeleteError, deleteFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting building.", ex);
                        }
                    }

                #endregion

            #endregion

            #region button events

                protected void updateBuildingButton_Click(Object sender, EventArgs e)
                {
                    var mBuilding = new Building();

                    LoadEditFormIntoBuilding(mBuilding);

                    try
                    {
                        //TODO: make building name, building classid, and imgextension hdn variables so we can remove this query
                        var originalBuilding = DataMgr.BuildingDataMapper.GetBuildingByBID(mBuilding.BID, false, false, false, false, true, false);

                        if (originalBuilding.BuildingName != mBuilding.BuildingName && DataMgr.BuildingDataMapper.DoesBuildingExistForClient(mBuilding.CID, mBuilding.BID, mBuilding.BuildingName))
                        {
                            LabelHelper.SetLabelMessage(lblEditError, buildingExists, lnkSetFocusEdit);
                            return;
                        }

                        if (fileUploaderEdit.HasChanged)
                        {
                            var image = new BuildingProfileImage(siteUser.CID, DataMgr.OrgBasedBlobStorageProvider, null, new BuildingProfileImage.Args(originalBuilding.BID));

                            if (!String.IsNullOrEmpty(originalBuilding.ImageExtension))
                            {
                                image.SetExtension(originalBuilding.ImageExtension).Delete();
                                mBuilding.ImageExtension = String.Empty;
                            }

                            if (fileUploaderEdit.File != null)
                            {
                                image.SetExtension(fileUploaderEdit.File.Extension).Insert(fileUploaderEdit.File.Content);
                                mBuilding.ImageExtension = fileUploaderEdit.File.Extension;
                            }
                        }

                        var addressStr = CreateAddressString(mBuilding, ddlEditState);

                        if ((String.IsNullOrWhiteSpace(txtEditLatitude.Text) || String.IsNullOrWhiteSpace(txtEditLongitude.Text)) || (addressStr != CreateAddressString(originalBuilding, ddlEditState)))
                        {
                            var coordinates = IoC.Resolve<BingMapsHelper>().GetGeocodeData(addressStr);

                            if (coordinates != null)
                            {
                                txtEditLatitude.Text = coordinates[0].ToString();
                                txtEditLongitude.Text = coordinates[1].ToString();

                                mBuilding.Latitude = coordinates[0];
                                mBuilding.Longitude = coordinates[1];
                            }
                        }

                        DataMgr.BuildingDataMapper.UpdateBuilding(mBuilding);
                        OperationComplete(OperationType.Update, updateSuccessful, KGSBuildingAdministration.TabMessages.EditBuilding);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building.", ex);
                    }
                }

            #endregion

            #region ddl events

                protected void ddlEditCountry_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindStates(ddlEditState, ddlEditCountry.SelectedValue);
                    ToggleStateAndZipValidators(ddlEditCountry.SelectedValue);
                }

                protected void ddlEditBuildingClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindBuildingTypes(ddlEditBuildingType, Convert.ToInt32(ddlEditBuildingClass.SelectedValue));
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void TabStateMonitor_OnChangeState(TabBase tab, IEnumerable<Enum> messages)
                {
                    siteUser.SwitchClient(siteUser.CID, siteUser.UserOID, (siteUser.ProviderID.HasValue) ? siteUser.ProviderID : null);
                    base.TabStateMonitor_OnChangeState(tab, messages);
                }

                protected override void SetDataForDetailsViewFromDB()
                {
                    var building = DataMgr.BuildingDataMapper.GetFullBuildingByBID(CurrentID).First();

                    if (String.IsNullOrEmpty(building.ImageExtension))
                        buildingImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage;
                    else
                    {
                        buildingImageUrl =
                        (
                            new BuildingProfileImage(siteUser.CID,
                                                     DataMgr.OrgBasedBlobStorageProvider,
                                                     building.ImageExtension,
                                                     new BuildingProfileImage.Args(CurrentID))
                                                     .Exists() ? HandlerHelper.BuildingImageUrl(siteUser.CID, CurrentID, building.ImageExtension) : ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage
                        );
                    }

                    SetBuildingDataIntoViewState(building, buildingImageUrl);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(BuildingDetails.Default), rptBuildingDetails);
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(BuildingDetails.SAS), rptSASBuildingDetails);
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(BuildingDetails.Internal), rptInternalBuildingDetails);
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(BuildingDetails.Account), rptBuildingAccountDetails);
                }

            #endregion

            private void SetBuildingDataIntoViewState(GetBuildingsData building, String buildingImageUrl)
            {
                SetBuildingDetails(building, buildingImageUrl);
                SetSASBuildingDetails(building);
                SetInternalBuildingDetails(building);
                SetBuildingAccountDetails(building);
            }

            private void SetBuildingDetails(GetBuildingsData building, String buildingImageUrl)
            {
                SetKeyValuePairItem("Building Name", building.BuildingName);
                SetKeyValuePairItem("Client", building.ClientName);
                SetKeyValuePairItem("Address", building.Address);
                SetKeyValuePairItem("City", building.City);
                SetKeyValuePairItem("State", building.StateName);
                SetKeyValuePairItem("Zip", building.Zip);
                SetKeyValuePairItem("Country", building.CountryName);

                if (building.Latitude != null) SetKeyValuePairItem("Latitude", CultureHelper.FormatNumberToInvariantString((decimal)building.Latitude));
                if (building.Longitude != null) SetKeyValuePairItem("Longitude", CultureHelper.FormatNumberToInvariantString((decimal)building.Longitude));

                SetKeyValuePairItem("Time Zone", building.TimeZone);
                SetKeyValuePairItem("Square Footage", building.Sqft.ToString());
                SetKeyValuePairItem("Floors", building.Floors.ToString());
                SetKeyValuePairItem("Year Built", building.YearBuilt.ToString());
                SetKeyValuePairItem("Building Class", building.BuildingClassName);
                SetKeyValuePairItem("Building Class Description", building.BuildingClassDescription);
                SetKeyValuePairItem("Building Type", building.BuildingTypeName);
                SetKeyValuePairItem("Building Type Description", building.BuildingTypeDescription);
                SetKeyValuePairItem("Building Ownership", building.BuildingOwnership);
                SetKeyValuePairItem("Building Shape", building.BuildingShape);
                SetKeyValuePairItem("Main Cooling System", building.MainCoolingSystem);
                SetKeyValuePairItem("Main Heating System", building.MainHeatingSystem);
                SetKeyValuePairItem("Roof Construction", building.RoofConstruction);
                SetKeyValuePairItem("Envelope Glass Percentage", building.EnvelopeGlassPercentage.ToString());
                SetKeyValuePairItem("Operating Hours/Week (Max 168)", building.OperatingHours.ToString());
                SetKeyValuePairItem("Computers", building.Computers.ToString());
                SetKeyValuePairItem("Occupants", building.Occupants.ToString());
                SetKeyValuePairItem("Location", building.Location);
                SetKeyValuePairItem("Description", building.Description);
                SetKeyValuePairItem("Building Image", buildingImageUrl, "<img id='imgPreview' src='{0}' alt='" + building.BuildingName + "' class='imgDetailsListPreview' />");
                SetKeyValuePairItem("Primary Contact Name", building.PrimaryContactName);
                SetKeyValuePairItem("Primary Contact Email", building.PrimaryContactEmail);
                SetKeyValuePairItem("Primary Contact Phone", building.PrimaryContactPhone);
                SetKeyValuePairItem("Website Link", building.WebsiteLink, "<a href='{0}' target='_blank'>{0}</a>");
                SetKeyValuePairItem("Kiosk Content", building.KioskContent);
                SetKeyValuePairItem("Weather File Name", building.WeatherFileName);
                SetKeyValuePairItem("IsActive", building.IsActive.ToString());
                SetKeyValuePairItem("IsVisible", building.IsVisible.ToString());

                SetDetailsViewDataToViewState(Convert.ToInt32(BuildingDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            private void SetSASBuildingDetails(GetBuildingsData building)
            {
                if (!String.IsNullOrWhiteSpace(building.SASReferenceID) || !String.IsNullOrWhiteSpace(building.SASLink))
                {
                    SetKeyValuePairItem("SAS (Space Accounting Software) ReferenceID", building.SASReferenceID);
                    if (building.SASLink != null) SetKeyValuePairItem("SAS (Space Accounting Software) Link", "<a href='" + StringHelper.ExternalLinkCorrector(building.SASLink) + "' target='_blank'>" + building.SASLink + "</a>");

                    SetDetailsViewDataToViewState(Convert.ToInt32(BuildingDetails.SAS));
                }
            }

            private void SetInternalBuildingDetails(GetBuildingsData building)
            {
                if (!String.IsNullOrWhiteSpace(building.SetupContactName) || !String.IsNullOrWhiteSpace(building.EnergyBureauContactName) || !String.IsNullOrWhiteSpace(building.InternalNotes))
                {
                    SetKeyValuePairItem("Setup Contact Name", building.SetupContactName);
                    SetKeyValuePairItem("Energy Bureau Contact Name", building.EnergyBureauContactName);
                    SetKeyValuePairItem("Internal Notes", building.InternalNotes);

                    SetDetailsViewDataToViewState(Convert.ToInt32(BuildingDetails.Internal));
                }
            }

            private void SetBuildingAccountDetails(GetBuildingsData building)
            {
                SetKeyValuePairItem("Unit System", DataConstants.UnitSystem[Convert.ToInt32(building.UnitSystem)]);
                SetKeyValuePairItem("Culture", CultureHelper.GetCultureInfo(Convert.ToInt32(building.LCID)).DisplayName);
                SetKeyValuePairItem("Support Email Override", building.SupportEmailOverride);
                SetKeyValuePairItem("Energy Bureau Service", building.EnergyBureauService.ToString());

                if (building.EnergyBureauService)
                {
                    SetKeyValuePairItem("Energy Bureau Service Frequency", building.EnergyBureauServiceFrequency);
                    SetKeyValuePairItem("Energy Bureau Service Hours", building.EnergyBureauServiceHours.ToString());
                }

                SetKeyValuePairItem("Subscription Plan (Months)", building.SubscriptionPlan.ToString());

                if (building.SubscriptionStartDate.HasValue) SetKeyValuePairItem("Subscription Start Date", DateTime.Parse(building.SubscriptionStartDate.ToString()).ToShortDateString());
                if (building.SubscriptionRenewalDate.HasValue) SetKeyValuePairItem("Subscription Date Renewed", DateTime.Parse(building.SubscriptionRenewalDate.ToString()).ToShortDateString());

                SetKeyValuePairItem("External Archive Plan (Months)", building.ArchivePlan.ToString());

                if (building.ArchiveStartDate.HasValue) SetKeyValuePairItem("External Archive Start Date", DateTime.Parse(building.ArchiveStartDate.ToString()).ToShortDateString());
                if (building.ArchiveRenewalDate.HasValue) SetKeyValuePairItem("External Archive Date Renewed", DateTime.Parse(building.ArchiveRenewalDate.ToString()).ToShortDateString());

                SetKeyValuePairItem("Max Point Limit", building.MaxPointLimit.ToString());

                SetKeyValuePairItem("Automated Priority Daily Emails", building.AutomatedPriorityDailyEmails.ToString());
                SetKeyValuePairItem("Automated Priority Weekly Emails", building.AutomatedPriorityWeeklyEmails.ToString());
                SetKeyValuePairItem("Automated Priority Monthly Emails", building.AutomatedPriorityMonthlyEmails.ToString());
                SetKeyValuePairItem("Last Modified Date", building.SettingsDateModified.ToString());

                SetDetailsViewDataToViewState(Convert.ToInt32(BuildingDetails.Account));
            }
    
            private void SetBuildingIntoEditForm(Building building)
            {
                hdnEditID.Value = Convert.ToString(building.BID);
                txtEditBuildingName.Text = building.BuildingName;
                lblClientName.Text = building.Client.ClientName;
                txtEditAddress.Text = building.Address;
                txtEditCity.Text = building.City;

                ddlEditCountry.SelectedValue = building.CountryAlpha2Code;
                ddlEditCountry_OnSelectedIndexChanged(null, null);

                if (building.StateID != null) ddlEditState.SelectedValue = Convert.ToString(building.StateID);

                txtEditZip.Text = building.Zip;
                txtEditLatitude.Text = Convert.ToString(building.Latitude, CultureInfo.InvariantCulture);
                txtEditLongitude.Text = Convert.ToString(building.Longitude, CultureInfo.InvariantCulture);
                ddlEditTimeZone.SelectedValue = building.TimeZone;
                txtEditPhone.Text = building.Phone;
                txtEditSqft.Text = Convert.ToString(building.Sqft);
                txtEditFloors.Text = Convert.ToString(building.Floors);
                txtEditYearBuilt.Text = Convert.ToString(building.YearBuilt);
                ddlEditBuildingClass.SelectedValue = building.BuildingType.BuildingClassID.ToString();
                ddlEditBuildingType.SelectedValue = building.BuildingTypeID.ToString();
                ddlEditBuildingOwnership.SelectedValue = building.BuildingOwnership;
                ddlEditBuildingShape.SelectedValue = building.BuildingShape;
                ddlEditMainCoolingSystem.SelectedValue = building.MainCoolingSystem;
                ddlEditMainHeatingSystem.SelectedValue = building.MainHeatingSystem;
                ddlEditRoofConstruction.SelectedValue = building.RoofConstruction;
                radNumericEditEnvelope.Text = building.EnvelopeGlassPercentage.ToString();
                radNumericEditOperating.Text = building.OperatingHours.ToString();
                txtEditOccupants.Text = Convert.ToString(building.Occupants);
                radNumericEditComputers.Text = building.Computers.ToString();
                txtEditLocation.Value = building.Location;
                txtEditDescription.Value = building.Description;

                //Image Preview
                fileUploaderEdit.Clear();

                if (!String.IsNullOrEmpty(building.ImageExtension))
                {
                    var image = new BuildingProfileImage(siteUser.CID, DataMgr.OrgBasedBlobStorageProvider, building.ImageExtension, new BuildingProfileImage.Args(building.BID));

                    if (image.Exists()) fileUploaderEdit.SetFile(image.Retrieve(), image.FileName);
                }

                txtEditPrimaryContactName.Text = building.PrimaryContactName;
                txtEditPrimaryContactEmail.Text = building.PrimaryContactEmail;
                txtEditPrimaryContactPhone.Text = building.PrimaryContactPhone;
                txtEditWebsiteLink.Text = building.WebsiteLink;
                txtEditKioskContent.Value = building.KioskContent;
                ddlEditGlobalFile.SelectedValue = building.WeatherGFID != null ? building.WeatherGFID.ToString() : "-1";
                chkEditActive.Checked = building.IsActive;
                chkEditVisible.Checked = building.IsVisible;

                //SAS Information
                txtEditSASReferenceID.Text = building.SASReferenceID;
                txtEditSASLink.Text = building.SASLink;

                //Internal information
                txtEditInternalNotes.Value = building.InternalNotes;
                txtEditSetupContactName.Text = building.SetupContactName;
                txtEditEnergyBureauContactName.Text = building.EnergyBureauContactName;
            }

            private void LoadEditFormIntoBuilding(Building building)
            {
                building.BID = Convert.ToInt32(hdnEditID.Value);
                building.BuildingName = txtEditBuildingName.Text;
                building.Address = txtEditAddress.Text;
                building.City = txtEditCity.Text;
                building.CountryAlpha2Code = ddlEditCountry.SelectedValue;
                building.StateID = ddlEditState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditState.SelectedValue) : null;
                building.Zip = String.IsNullOrEmpty(txtEditZip.Text) ? null : txtEditZip.Text;
                building.Latitude = String.IsNullOrEmpty(txtEditLatitude.Text) ? null : (decimal?)Convert.ToDecimal(txtEditLatitude.Text, CultureInfo.InvariantCulture);
                building.Longitude = String.IsNullOrEmpty(txtEditLongitude.Text) ? null : (decimal?)Convert.ToDecimal(txtEditLongitude.Text, CultureInfo.InvariantCulture);
                building.TimeZone = ddlEditTimeZone.SelectedValue;
                building.Phone = String.IsNullOrEmpty(txtEditPhone.Text) ? null : txtEditPhone.Text;
                building.Sqft = Convert.ToInt32(txtEditSqft.Text);
                building.Floors = String.IsNullOrEmpty(txtEditFloors.Text) ? null : (int?)Convert.ToInt32(txtEditFloors.Text);
                building.YearBuilt = String.IsNullOrEmpty(txtEditYearBuilt.Text) ? null : (int?)Convert.ToInt32(txtEditYearBuilt.Text);
                //buildingClassID is assigned via BuildingTypeID
                building.BuildingTypeID = Convert.ToInt32(ddlEditBuildingType.SelectedValue);
                building.BuildingOwnership = ddlEditBuildingOwnership.SelectedValue != "-1" ? ddlEditBuildingOwnership.SelectedValue : null;
                building.BuildingShape = ddlEditBuildingShape.SelectedValue != "-1" ? ddlEditBuildingShape.SelectedValue : null;
                building.MainCoolingSystem = ddlEditMainCoolingSystem.SelectedValue != "-1" ? ddlEditMainCoolingSystem.SelectedValue : null;
                building.MainHeatingSystem = ddlEditMainHeatingSystem.SelectedValue != "-1" ? ddlEditMainHeatingSystem.SelectedValue : null;
                building.RoofConstruction = ddlEditRoofConstruction.SelectedValue != "-1" ? ddlEditRoofConstruction.SelectedValue : null;
                building.EnvelopeGlassPercentage = !String.IsNullOrEmpty(radNumericEditEnvelope.Text) ? Convert.ToInt32(radNumericEditEnvelope.Text) : 0;
                building.OperatingHours = !String.IsNullOrEmpty(radNumericEditOperating.Text) ? Convert.ToInt32(radNumericEditOperating.Text) : 0;
                building.Occupants = String.IsNullOrEmpty(txtEditOccupants.Text) ? null : (int?)Convert.ToInt32(txtEditOccupants.Text);
                building.Computers = !String.IsNullOrEmpty(radNumericEditComputers.Text) ? Convert.ToInt32(radNumericEditComputers.Text) : 0;
                building.Location = String.IsNullOrEmpty(txtEditLocation.Value) ? null : txtEditLocation.Value;
                building.Description = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                building.ImageExtension = (fileUploaderEdit.File == null) ? String.Empty : fileUploaderEdit.File.Extension;
                building.PrimaryContactName = String.IsNullOrEmpty(txtEditPrimaryContactName.Text) ? null : txtEditPrimaryContactName.Text;
                building.PrimaryContactEmail = String.IsNullOrEmpty(txtEditPrimaryContactEmail.Text) ? null : txtEditPrimaryContactEmail.Text;
                building.PrimaryContactPhone = String.IsNullOrEmpty(txtEditPrimaryContactPhone.Text) ? null : txtEditPrimaryContactPhone.Text;
                building.WebsiteLink = String.IsNullOrEmpty(txtEditWebsiteLink.Text) ? null : txtEditWebsiteLink.Text;
                building.KioskContent = String.IsNullOrEmpty(txtEditKioskContent.Value) ? null : txtEditKioskContent.Value;
                building.WeatherGFID = (ddlEditGlobalFile.SelectedValue != "-1") ? Convert.ToInt32(ddlEditGlobalFile.SelectedValue) : (int?)null;
                building.IsActive = chkEditActive.Checked;
                building.IsVisible = chkEditVisible.Checked;

                //SAS Information
                building.SASReferenceID = String.IsNullOrEmpty(txtEditSASReferenceID.Text) ? null : txtEditSASReferenceID.Text;
                building.SASLink = String.IsNullOrEmpty(txtEditSASLink.Text) ? null : txtEditSASLink.Text;

                //Internal Information
                building.InternalNotes = String.IsNullOrEmpty(txtEditInternalNotes.Value) ? null : txtEditInternalNotes.Value;
                building.SetupContactName = String.IsNullOrEmpty(txtEditSetupContactName.Text) ? null : txtEditSetupContactName.Text;
                building.EnergyBureauContactName = String.IsNullOrEmpty(txtEditEnergyBureauContactName.Text) ? null : txtEditEnergyBureauContactName.Text;
            }

            private void ToggleStateAndZipValidators(String value)
            {
                editZipRequiredRFV.Enabled = editZipREV.Enabled = (value == BusinessConstants.Countries.UnitedStatesAlpha2Code);
                txtEditZip.Mask = (value == BusinessConstants.Countries.UnitedStatesAlpha2Code) ? "#####" : "aaaaaaaaaa";
                editStateRFV.Enabled = (ddlEditState.Items.Count > 2);
            }

            private String CreateAddressString(Building building, DropDownList ddl)
            {
                return String.Join(",", new[] { building.Address, building.City, ((building.StateID == null) ? String.Empty : (ddl.Items.FindByValue(building.StateID.Value.ToString()) == null ? String.Empty : ddl.Items.FindByValue(building.StateID.Value.ToString()).Text)), building.Zip, building.CountryAlpha2Code });
            }

            #region binders

                private void BindCountries(DropDownList ddl)
                {
                    BindDDL(ddl, "CountryName", "Alpha2Code", DataMgr.CountryDataMapper.GetAllCountries(), true);
                }

                private void BindStates(DropDownList ddl, String alpha2Code)
                {
                    var lItem = new ListItem() { Text = (alpha2Code != "-1") ? "Select one..." : "Select country first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "StateName", "StateID", (alpha2Code != "-1") ? DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code) : null, true, lItem);
                }

                private void BindTimeZones(DropDownList ddl)
                {
                    BindDDL(ddl, "DisplayName", "Id", TimeZoneInfo.GetSystemTimeZones(), true);
                }

                private void BindBuildingClasses(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingClassName", "BuildingClassID", DataMgr.BuildingClassDataMapper.GetAllBuildingClasses(), true);
                }

                private void BindBuildingTypes(DropDownList ddl, Int32 buildingClassID)
                {
                    var lItem = new ListItem() { Text = (buildingClassID != -1) ? "Select one..." : "Select class first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "BuildingTypeName", "BuildingTypeID", (buildingClassID != -1) ? DataMgr.BuildingTypeDataMapper.GetAllBuildingTypesByBuildingClassID(buildingClassID) : null, true, lItem);
                }

                private void BindWeatherFiles(DropDownList ddl)
                {
                    var lItem = new ListItem() { Text = "Optionally select one...", Value = "-1", Selected = true };
                    var dataSource = DataMgr.GlobalFileDataMapper.GetAllGlobalFileItems<GlobalFilesLookup>((gfl => gfl), (gfl => gfl.GlobalFileTypeID == BusinessConstants.GlobalFileType.WeatherFileTypeID), (gfl => gfl.DisplayName));

                    BindDDL(ddl, "DisplayName", "GFID", dataSource, true, lItem);
                }

            #endregion

        #endregion
    }
}