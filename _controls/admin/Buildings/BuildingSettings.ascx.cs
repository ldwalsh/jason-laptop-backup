﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Buildings
{
    public partial class BuildingSettings : AdminUserControlBase
    {
        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[] 
                        {
                            KGSBuildingAdministration.TabMessages.AddBuilding,
                            KGSBuildingAdministration.TabMessages.EditBuilding,
                            KGSBuildingAdministration.TabMessages.DeleteBuilding
                        };
                    }
                }

            #endregion

        #endregion

        #region constants

            const String updateSettingsSuccessful = "Building account settings update was successful.";
            const String updateSettingsFailed = "Building account settings update failed. Please contact an administrator.";
            const String updateMaxPointLimit = "Max point limit cannot be lower then current existing point count.";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblSettingsError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindBuildings(ddlSettingsBuildings);
                    BindCultures(ddlSettingsCulture);
                }
            }

            #region ddl events

                protected void ddlSettingsBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    lblSettingsError.Visible = false;

                    var buildingID = Convert.ToInt32(ddlSettingsBuildings.SelectedValue);

                    if (buildingID != -1)
                    {
                        var mBuildingSettings = DataMgr.BuildingDataMapper.GetBuildingSettingsByBID(buildingID);

                        SetBuildingSettingsIntoEditForm(mBuildingSettings);
                    }

                    settings.Visible = (buildingID != -1);
                }

            #endregion

            #region checkbox events

                protected void chkSettingsEnergyBureauService_OnCheckedChanged(Object sender, EventArgs e)
                {
                    divEnergyBureau.Visible = chkSettingsEnergyBureauService.Checked;
                }

            #endregion

            #region button events
            
                protected void updateSettingsButton_Click(Object sender, EventArgs e)
                {
                    var mBuildingSettings = new BuildingSetting();
                    var isSuccessful = false;
                    lblSettingsError.Text = lblResults.Text = String.Empty;

                    LoadEditFormIntoBuildingSettings(mBuildingSettings);

                    if (mBuildingSettings.MaxPointLimit < DataMgr.PointDataMapper.GetAllPointsCountByBID(mBuildingSettings.BID))
                    {
                        LabelHelper.SetLabelMessage(lblSettingsError, updateMaxPointLimit, lnkSetFocus);
                        isSuccessful = false;
                    }
                    else
                    {
                        try
                        {
                            DataMgr.BuildingDataMapper.UpdateBuildingSettings(mBuildingSettings);

                            try
                            {
                                DataMgr.UserGroupDataMapper.DeleteUserGroupCollectionAssociationsByGuids(new Guid[] { reportCriteriaDailyPriority.GUID, reportCriteriaWeeklyPriority.GUID, reportCriteriaMonthlyPriority.GUID });

                                if (reportCriteriaDailyPriority.IsActive) DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups((from g in reportCriteriaDailyPriority.UserGroups select new UserGroupCollectionLookup_UserGroup { GUID = reportCriteriaDailyPriority.GUID, UserGroupID = g }).ToList());
                                if (reportCriteriaWeeklyPriority.IsActive) DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups((from g in reportCriteriaWeeklyPriority.UserGroups select new UserGroupCollectionLookup_UserGroup { GUID = reportCriteriaWeeklyPriority.GUID, UserGroupID = g }).ToList());
                                if (reportCriteriaMonthlyPriority.IsActive) DataMgr.UserGroupDataMapper.InsertUserGroupCollectionUserGroups((from g in reportCriteriaMonthlyPriority.UserGroups select new UserGroupCollectionLookup_UserGroup { GUID = reportCriteriaMonthlyPriority.GUID, UserGroupID = g }).ToList());

                                isSuccessful = true;
                            }
                            catch (Exception ex)
                            {
                                LabelHelper.SetLabelMessage(lblSettingsError, updateSettingsFailed, lnkSetFocus);
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Unable to update building account usergroup related settings.", ex);
                                isSuccessful = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblSettingsError, updateSettingsFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Unable to update building account settings.", ex);
                            isSuccessful = false;
                        }
                    }

                    if (isSuccessful) LabelHelper.SetLabelMessage(lblResults, updateSettingsSuccessful, lnkSetFocus);
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    BindBuildings(ddlSettingsBuildings, true);

                    new AdminUserControlForm(this).ResetDropDownLists();

                    settings.Visible = false; //hardset any fields to their initial values
                }

            #endregion

            private void SetBuildingSettingsIntoEditForm(BuildingSetting buildingSettings)
            {
                ddlSettingsUnitSystem.SelectedValue = Convert.ToString(buildingSettings.UnitSystem);
                ddlSettingsCulture.SelectedValue = Convert.ToString(buildingSettings.LCID);
                txtSettingsSupportEmailOverride.Text = buildingSettings.SupportEmailOverride;

                txtSettingsRawDataDelay.Text = Convert.ToString(buildingSettings.RawDataDelay);
                divEnergyBureau.Visible = chkSettingsEnergyBureauService.Checked = buildingSettings.EnergyBureauService;
                ddlSettingsEnergyBureauServiceFrequency.SelectedValue = buildingSettings.EnergyBureauServiceFrequency;
                txtSettingsEnergyBureauServiceHours.Text = Convert.ToString(buildingSettings.EnergyBureauServiceHours);
                
                settingsSubscriptionSliderExtender.Value = buildingSettings.SubscriptionPlan.ToString(); 
                sliderSettingsSubscription.Text = buildingSettings.SubscriptionPlan.ToString();
                
                txtSettingsSubscriptionStartDate.SelectedDate = buildingSettings.SubscriptionStartDate;
                txtSettingsSubscriptionRenewalDate.SelectedDate = buildingSettings.SubscriptionRenewalDate;

                settingsArchiveSliderExtender.Value = buildingSettings.ArchivePlan.ToString(); 
                sliderSettingsArchive.Text = buildingSettings.ArchivePlan.ToString();
                
                txtSettingsArchiveStartDate.SelectedDate = buildingSettings.ArchiveStartDate;
                txtSettingsArchiveRenewalDate.SelectedDate = buildingSettings.ArchiveRenewalDate;

                txtSettingsMaxPointLimit.Text = Convert.ToString(buildingSettings.MaxPointLimit);

                reportCriteriaMonthlyPriority.ClearForm();
                reportCriteriaMonthlyPriority.UserGroupAccessLevel = ReportAssignmentCriteria.UserGroupAccessLevels.KGS;
                reportCriteriaMonthlyPriority.ReportType = ReportAssignmentCriteria.ReportTypes.Building;
                reportCriteriaMonthlyPriority.CID = siteUser.CID;
                reportCriteriaMonthlyPriority.BID = buildingSettings.BID;
                reportCriteriaMonthlyPriority.IsActive = buildingSettings.AutomatedPriorityMonthlyEmails;
                reportCriteriaMonthlyPriority.UserGroups = DataMgr.UserGroupDataMapper.GetUserGroupIDsByGUID(buildingSettings.AutomatedPriorityMonthlyEmailsUserGroupsGUID);
                reportCriteriaMonthlyPriority.GUID = buildingSettings.AutomatedPriorityMonthlyEmailsUserGroupsGUID;
                reportCriteriaMonthlyPriority.CurrentUID = siteUser.UID;
                reportCriteriaMonthlyPriority.AnalysisRange = DataConstants.AnalysisRange.Monthly;
                reportCriteriaMonthlyPriority.BindForm();

                reportCriteriaWeeklyPriority.ClearForm();
                reportCriteriaWeeklyPriority.UserGroupAccessLevel = ReportAssignmentCriteria.UserGroupAccessLevels.KGS;
                reportCriteriaWeeklyPriority.ReportType = ReportAssignmentCriteria.ReportTypes.Building;
                reportCriteriaWeeklyPriority.CID = siteUser.CID;
                reportCriteriaWeeklyPriority.BID = buildingSettings.BID;
                reportCriteriaWeeklyPriority.IsActive = buildingSettings.AutomatedPriorityWeeklyEmails;
                reportCriteriaWeeklyPriority.UserGroups = DataMgr.UserGroupDataMapper.GetUserGroupIDsByGUID(buildingSettings.AutomatedPriorityWeeklyEmailsUserGroupsGUID);
                reportCriteriaWeeklyPriority.GUID = buildingSettings.AutomatedPriorityWeeklyEmailsUserGroupsGUID;
                reportCriteriaWeeklyPriority.CurrentUID = siteUser.UID;
                reportCriteriaWeeklyPriority.AnalysisRange = DataConstants.AnalysisRange.Weekly;
                reportCriteriaWeeklyPriority.BindForm();

                reportCriteriaDailyPriority.ClearForm();
                reportCriteriaDailyPriority.UserGroupAccessLevel = ReportAssignmentCriteria.UserGroupAccessLevels.KGS;
                reportCriteriaDailyPriority.ReportType = ReportAssignmentCriteria.ReportTypes.Building;
                reportCriteriaDailyPriority.CID = siteUser.CID;
                reportCriteriaDailyPriority.BID = buildingSettings.BID;
                reportCriteriaDailyPriority.IsActive = buildingSettings.AutomatedPriorityDailyEmails;
                reportCriteriaDailyPriority.UserGroups = DataMgr.UserGroupDataMapper.GetUserGroupIDsByGUID(buildingSettings.AutomatedPriorityDailyEmailsUserGroupsGUID);
                reportCriteriaDailyPriority.GUID = buildingSettings.AutomatedPriorityDailyEmailsUserGroupsGUID;
                reportCriteriaDailyPriority.CurrentUID = siteUser.UID;
                reportCriteriaDailyPriority.AnalysisRange = DataConstants.AnalysisRange.Daily;
                reportCriteriaDailyPriority.BindForm();
            }

            private void LoadEditFormIntoBuildingSettings(BuildingSetting buildingSettings)
            {
                buildingSettings.BID = Convert.ToInt32(ddlSettingsBuildings.SelectedValue);
                buildingSettings.UnitSystem = Convert.ToBoolean(ddlSettingsUnitSystem.SelectedValue);
                buildingSettings.LCID = Convert.ToInt32(ddlSettingsCulture.SelectedValue);
                buildingSettings.SupportEmailOverride = txtSettingsSupportEmailOverride.Text;
                buildingSettings.RawDataDelay = Convert.ToInt16(txtSettingsRawDataDelay.Text);
                buildingSettings.EnergyBureauService = chkSettingsEnergyBureauService.Checked;
                buildingSettings.EnergyBureauServiceFrequency = chkSettingsEnergyBureauService.Checked ? ddlSettingsEnergyBureauServiceFrequency.SelectedValue : null;
                buildingSettings.EnergyBureauServiceHours = chkSettingsEnergyBureauService.Checked && !String.IsNullOrEmpty(txtSettingsEnergyBureauServiceHours.Text) ? (short?)Convert.ToInt16(txtSettingsEnergyBureauServiceHours.Text) : null;
                buildingSettings.SubscriptionPlan = Convert.ToInt32(sliderSettingsSubscription.Text);
                buildingSettings.SubscriptionStartDate = txtSettingsSubscriptionStartDate.SelectedDate;
                buildingSettings.SubscriptionRenewalDate = txtSettingsSubscriptionRenewalDate.SelectedDate;
                buildingSettings.ArchivePlan = Convert.ToInt32(sliderSettingsArchive.Text);
                buildingSettings.ArchiveStartDate = txtSettingsArchiveStartDate.SelectedDate;
                buildingSettings.ArchiveRenewalDate = txtSettingsArchiveRenewalDate.SelectedDate;
                buildingSettings.MaxPointLimit = Convert.ToInt32(txtSettingsMaxPointLimit.Text);
                buildingSettings.AutomatedPriorityMonthlyEmails = reportCriteriaMonthlyPriority.IsActive;
                buildingSettings.AutomatedPriorityWeeklyEmails = reportCriteriaWeeklyPriority.IsActive;
                buildingSettings.AutomatedPriorityDailyEmails = reportCriteriaDailyPriority.IsActive;
                buildingSettings.AutomatedPriorityMonthlyEmailsUserGroupsGUID = reportCriteriaMonthlyPriority.GUID;
                buildingSettings.AutomatedPriorityWeeklyEmailsUserGroupsGUID = reportCriteriaWeeklyPriority.GUID;
                buildingSettings.AutomatedPriorityDailyEmailsUserGroupsGUID = reportCriteriaDailyPriority.GUID;
            }

            #region binders

                private void BindBuildings(DropDownList ddl, Boolean isDDLRefresh = false)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindCultures(DropDownList ddl)
                {
                    BindDDL(ddl, "DisplayName", "LCID", CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.DisplayName), true);
                }

            #endregion

        #endregion
    }
}