﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingVariables.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.BuildingVariables" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="CW" TagName="BuildingVariablesList" src="~/_controls/admin/Buildings/BuildingVariablesList.ascx" %>
<%@ Register TagPrefix="CW" TagName="BuildingVariablesListBoxes" src="~/_controls/admin/Buildings/BuildingVariablesListBoxes.ascx" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data.Models.BuildingVariable" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" />

<div>
  <a id="lnkSetFocus" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblVariablesError" CssClass="errorMessage" runat="server" />
</div>
<div id="clientName" class="divForm" runat="server" visible="false">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div>   
<div class="divForm">
  <label class="label">*Select Building:</label>    
  <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<%-- NON-KGS --%>
<CW:BuildingVariablesList ID="BuildingVariablesList" runat="server" Visible="false" />

<%-- KGS --%>
<CW:BuildingVariablesListBoxes ID="BuildingVariablesListBoxes" runat="server" Visible="false" />

<!--EDIT BUILDING VARIABLES PANEL -->
<asp:Panel ID="pnlUpdateBuildingVariables" runat="server" Visible="false" DefaultButton="btnUpdateBuildingVariables">        
  <hr />

  <h2><asp:Label ID="lblUpdateBuildingVariables" runat="server" /></h2>

  <p>Note: Default values are established on a building variable level.</p>

  <asp:Repeater ID="rptVars" runat="server">
    <ItemTemplate> 
      <div class="divForm">                                                                                                                  
        <label class="label">*<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.BuildingVariableDisplayName))%>:</label>
        <asp:TextBox ID="txtEditValue" runat="server" 
            CssClass='<%# Convert.ToBoolean(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? "" : "textbox" %>' 
            Rows='<%# Convert.ToBoolean(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? 5 : 1 %>' 
            Columns='<%# Convert.ToBoolean(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? 40 : 1 %>' 
            TextMode='<%# Convert.ToBoolean(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? TextBoxMode.MultiLine : TextBoxMode.SingleLine %>' 
            MaxLength='<%# Convert.ToBoolean(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? 2500 : 25 %>' 
            Text='<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.Value)) %>'
            onkeyup='<%# "limitChars(this, 2500, &#39;divCharInfo" + Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.ID)) + "&#39;)"%>' />
        &nbsp;<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.Unit))  %>
        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.ID)) %>' />
        <asp:HiddenField ID="hdnIsBuildingSettingBasedEngUnitStringOrArray" runat="server" Value='<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray)) %>' />
        <asp:HiddenField ID="hdnIsBuildingSettingBasedEngUnitsBoolean" runat="server" Value='<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitsBoolean)) %>' />
        <asp:HiddenField ID="hdnMinValue" runat="server" Value='<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.MinValue)) %>' />
        <asp:HiddenField ID="hdnMaxValue" runat="server" Value='<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.MaxValue)) %>' />
        <div id='valFailure' runat="server" class="errorMessage" style="display:none"></div>

        <!--TODO: Validator for Bools-->
        <asp:RequiredFieldValidator ID="editValueRFV" runat="server" CssClass="errorMessage" ErrorMessage="Required" ControlToValidate="txtEditValue" SetFocusOnError="true" ValidationGroup="UpdateBuildingVariables" Display="Dynamic" />

        <asp:Panel Visible='<%# !Convert.ToBoolean(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) %>' runat="server">
          (<%# Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.Info)) %>)
        </asp:Panel>
        <asp:Panel Visible='<%# Convert.ToBoolean(Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) %>' runat="server">
          (String Format = text, Array Format (max 2500 characters) = {0;1.1;2;3})
          <div id='<%# "divCharInfo" + Eval(PropHelper.G<GetBuildingBuildingVariableData>(_ => _.ID))%>'></div>
        </asp:Panel>
      </div>
    </ItemTemplate>
  </asp:Repeater>
                                        
  <asp:LinkButton ID="btnUpdateBuildingVariables" runat="server" Text="Update" CssClass="lnkButton" OnClick="btnUpdateBuildingVariables_ButtonClick" ValidationGroup="UpdateBuildingVariables" />

</asp:Panel>