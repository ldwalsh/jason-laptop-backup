﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingVariablesToMultipleBuildings.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.BuildingVariablesToMultipleBuildings" %>

<asp:Panel ID="pnlAssignBuildingVariableToMultipleBuilding" runat="server" DefaultButton="btnAssignBuildingVariableToMultipleBuildings">

  <h2>Building Variable To Multiple Buildings</h2>

  <p>Please select a building variable, building, and set a value in order mass assign a building variable.</p>
  
  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblVariablesError" CssClass="errorMessage" runat="server" />
  </div>                                    
  <div class="divForm">
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>  
  </div>   
  <div class="divForm">
    <label class="label">Building Unit System:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlSettingsUnitSystem" OnSelectedIndexChanged="ddlSettingsUnitSystem_OnSelectedIndexChanged" AutoPostBack="true" runat="server">  
        <asp:ListItem Value="0" Selected="True">IP</asp:ListItem>
        <asp:ListItem Value="1">SI</asp:ListItem>
      </asp:DropDownList>
  </div>   
  <div class="divForm">   
    <label class="label">*Select Building Variable:</label>    
    <asp:DropDownList ID="ddlBuildingVariables" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildingVariables_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    <asp:RequiredFieldValidator ID="buildingVariablesRFV" runat="server" ErrorMessage="Building Variables is a required field." ControlToValidate="ddlBuildingVariables" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Variables" />
    <ajaxToolkit:ValidatorCalloutExtender ID="buildingVariablesVCE" runat="server" BehaviorID="buildingVariablesVCE" TargetControlID="buildingVariablesRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>                                  
  <div class="divForm">                                                                                                                  
    <label class="label">*Value</label>   
    <asp:TextBox CssClass="textbox" ID="txtVariablesValue" runat="server" MaxLength="25" />
    <span id="spanEngUnits" runat="server"></span>                                         
    <div id="info" runat="server"></div>                                         
    <p>(Note: Value set here will be set for the building variable on ALL buildings.)</p> 

    <!--filter is disabled if engineering unit is "STRING" or "ARRAY"-->
    <asp:RequiredFieldValidator ID="variablesValueRFV" runat="server" ErrorMessage="Value is a required field." ControlToValidate="txtVariablesValue" SetFocusOnError="true" Display="None" ValidationGroup="Variables" />
    <ajaxToolkit:ValidatorCalloutExtender ID="variablesValueVCE" runat="server" BehaviorID="variablesValueVCE" TargetControlID="variablesValueRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
  <div class="divForm">                                                        
    <asp:CheckBox ID="chkVariablesOverride" CssClass="checkbox" runat="server" /> 
    <span>Override value if variable already exists for building.</span>
  </div>  
  <div class="divForm">
    <label class="label">Available Buildings:</label> 
    <div class="divListSearchExtender">
      <ajaxToolkit:ListSearchExtender id="lseBuildingsTop" runat="server" TargetControlID="lbBuildingsTop" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true" />   
    </div>

    <asp:ListBox ID="lbBuildingsTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    <div class="divArrows">
      <asp:ImageButton CssClass="up-arrow"  ID="ImageButton1"  OnClick="btnBuildingsUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
      <asp:ImageButton CssClass="down-arrow" ID="ImageButton2"  OnClick="btnBuildingsDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
    </div>

    <label class="label">*Selected Buildings:</label> 
    <asp:ListBox ID="lbBuildingsBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />                                                        

  </div>

  <asp:LinkButton CssClass="lnkButton" ID="btnAssignBuildingVariableToMultipleBuildings" runat="server" Text="Assign" OnClick="assignBuildingVariableToMultipleBuildings_Click" ValidationGroup="Variables" />

</asp:Panel>