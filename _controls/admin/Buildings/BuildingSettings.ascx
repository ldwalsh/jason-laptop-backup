﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingSettings.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.BuildingSettings" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" TagName="ReportCriteria" Src="~/_administration/ReportAssignmentCriteria.ascx" %>

<asp:Panel ID="pnlSettings" runat="server" DefaultButton="btnUpdateSettings">
                                                                    
  <h2>Building Settings</h2>

  <p>Please select a building in order to configure settings.</p> 

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblSettingsError" CssClass="errorMessage" runat="server" />
  </div>
  <div class="divForm" runat="server">   
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>
  </div>                                                         
  <div class="divForm">
    <label class="label">*Select Building:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlSettingsBuildings" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlSettingsBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
  </div>
  
  <div id="settings" visible="false" runat="server">                                            
    <hr />

    <h2>General Settings</h2>

    <div class="divForm">
      <label class="label">*Unit System:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlSettingsUnitSystem" runat="server">  
        <asp:ListItem Value="False">IP</asp:ListItem>
        <asp:ListItem Value="True">SI</asp:ListItem>
      </asp:DropDownList>
       <p>(Warning: Changing the engineering unit system for the building will likely invalidate currently set values for this building.  Please make sure to convert building and equipment variables to the expected engineering input units and utilize the analysis point multipliers for conversions to the raw data points.)</p>
       <p>(Note: Changes will take effect on next login.)</p>
    </div>
    <div class="divForm">
      <label class="label">*Culture:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlSettingsCulture" AppendDataBoundItems="true" runat="server" />  
    </div>
    <div class="divForm">
        <label class="label">Support Email Override:</label>
        <asp:TextBox ID="txtSettingsSupportEmailOverride" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>  
        <p>(Warning: Inputting an email here will override where both system level and client level support emails related to this building will be delivered.)</p>                                          
        <asp:RegularExpressionValidator ID="settingsSupportEmailOverrideRegExValidator" runat="server"
            ErrorMessage="Invalid Email Format."
            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            ControlToValidate="txtSettingsSupportEmailOverride"
            SetFocusOnError="true" 
            Display="None" 
            ValidationGroup="UpdateSettings">
            </asp:RegularExpressionValidator>                         
        <ajaxToolkit:ValidatorCalloutExtender ID="settingsSupportEmailOverrideRegExValidatorExtender" runat="server"
            BehaviorID="settingsSupportEmailOverrideRegExValidatorExtender" 
            TargetControlID="settingsSupportEmailOverrideRegExValidator"
            HighlightCssClass="validatorCalloutHighlight"
            Width="175">
            </ajaxToolkit:ValidatorCalloutExtender>
    </div>
    <hr />

    <h2>Raw Data Settings</h2>

    <div class="divForm">
      <label class="label">*Raw Data Delay (Hours):</label>
      <telerik:RadMaskedTextBox ID="txtSettingsRawDataDelay" CssClass="textbox" runat="server" Mask="####" PromptChar="" ValidationGroup="UpdateSettings" />
      <asp:RequiredFieldValidator ID="editSettingsRawDataDelayRFV" runat="server" ErrorMessage="Raw Data Delay is a required field." ControlToValidate="txtSettingsRawDataDelay" SetFocusOnError="true" Display="None" ValidationGroup="UpdateSettings" />
      <ajaxToolkit:ValidatorCalloutExtender ID="editSettingsRawDataDelayVCE" runat="server" BehaviorID="editSettingsRawDataDelayVCE" TargetControlID="editSettingsRawDataDelayRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
      <p>(Note: Delays anything scheduled to run for this building. EX: Diagnostics, Cache, Notifications, and etc.)</p>
    </div>

    <hr />

    <h2>Energy Bureau</h2>

    <div class="divForm">
      <label class="label">*Energy Bureau Service:</label>
      <asp:CheckBox ID="chkSettingsEnergyBureauService" CssClass="checkbox" OnCheckedChanged="chkSettingsEnergyBureauService_OnCheckedChanged" AutoPostBack="true" runat="server" />
    </div>
    <div id="divEnergyBureau" visible="false" runat="server">

      <div class="divForm">
        <label class="label">Energy Bureau Service Frequency:</label>
        <asp:DropDownList CssClass="dropdown" ID="ddlSettingsEnergyBureauServiceFrequency" runat="server">                                                      
          <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
          <asp:ListItem Value="Quarterly">Quarterly</asp:ListItem>
        </asp:DropDownList>
      </div>
      <div class="divForm">
        <label class="label">Energy Bureau Service Hours:</label>                                                    
        <telerik:RadMaskedTextBox ID="txtSettingsEnergyBureauServiceHours" CssClass="textbox" runat="server" Mask="##" PromptChar="" ValidationGroup="UpdateSettings" />
      </div>

    </div>
    <hr />

    <h2>Subscription And Archive Plans</h2>

    <p>***WARNING: These fields could be a catastrophic disaster if messed up or ever incorrect.***</p>

    <div class="divForm">   
      <label class="label">*Subscription Plan (Months):</label>           
        <input id="settingsSubscriptionSliderExtender" runat="server" type="range" style="width:200px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_BuildingSettings_sliderSettingsSubscription')"  onchange="sliderOnChange(this, 'ctl00_plcCopy_BuildingSettings_sliderSettingsSubscription')" min="0" max="60" step="1" />
        <asp:TextBox ID="sliderSettingsSubscription" MaxLength="2" CssClass="sliderDisplay" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_BuildingSettings_settingsSubscriptionSliderExtender")'></asp:TextBox>
      <p>(Note: After this many months from the subscription start date, or renewal date, analyzed data will be removed from the system permanently.)</p>
      <p>(Note: This is the subscription term for the current subscription, not from the start of the first subscription if renewed.)</p>
    </div> 
    <div class="divForm">
      <label class="label">*Subscription Start Date:</label>
      <telerik:RadDatePicker ID="txtSettingsSubscriptionStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>          
      <p>(Note: This is start date of the first subscription. Do not change if renewed.)</p>      
      <asp:RequiredFieldValidator ID="subscriptionStartRequiredValidator" runat="server" CssClass="errorMessage" ErrorMessage="Date is a required field." ControlToValidate="txtSettingsSubscriptionStartDate" SetFocusOnError="true" Display="None" ValidationGroup="UpdateSettings"></asp:RequiredFieldValidator>
      <ajaxToolkit:ValidatorCalloutExtender ID="subscriptionStartRequiredValidatorExtender" runat="server" 
                                                    BehaviorID="subscriptionStartRequiredValidatorExtender" 
                                                    TargetControlID="subscriptionStartRequiredValidator" 
                                                    HighlightCssClass="validatorCalloutHighlight" 
                                                    PopupPosition="Right"
                                                    Width="175" />  
    </div>
    <div class="divForm">
      <label class="label">Subscription Date Renewed:</label>
      <telerik:RadDatePicker ID="txtSettingsSubscriptionRenewalDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>       
      <p>(Note: This is the start date of a renewed subscription.)</p>      
      <asp:CompareValidator ID="subscriptionRenewalCompareValidator" runat="server" CssClass="errorMessage" ErrorMessage="Invalid range." ControlToValidate="txtSettingsSubscriptionRenewalDate" ControlToCompare="txtSettingsSubscriptionStartDate" Type="Date" Operator="GreaterThanEqual" ValidationGroup="UpdateSettings" Display="Dynamic" SetFocusOnError="true" />
    </div>
    <div class="divForm">   
      <label class="label">External Archive Plan (Months):</label>                                                                                                                   
      <input id="settingsArchiveSliderExtender" runat="server" type="range" style="width:200px;" oninput="sliderOnChange(this, 'ctl00_plcCopy_BuildingSettings_sliderSettingsArchive')" onchange="sliderOnChange(this, 'ctl00_plcCopy_BuildingSettings_sliderSettingsArchive')" min="0" max="60" step="1" />
      <asp:TextBox ID="sliderSettingsArchive" MaxLength="2" CssClass="sliderDisplay" runat="server" onblur='sliderOnChange(this, "ctl00_plcCopy_BuildingSettings_settingsArchiveSliderExtender")'></asp:TextBox>
      <p>(Note: After this many months from the archive start date, or renewal date, archived raw data will be removed from storage permanently.)</p>
      <p>(Note: This is the archive term for the current archive plan, not from the start of the first archive plan if renewed.)</p>
    </div> 
    <div class="divForm">
      <label class="label">External Archive Start Date:</label>
      <telerik:RadDatePicker ID="txtSettingsArchiveStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>        
      <p>(Note: This is start date of the first archive plan. Do not change if renewed.)</p>      
    </div>
    <div class="divForm">
      <label class="label">External Archive Date Renewed:</label>
      <telerik:RadDatePicker ID="txtSettingsArchiveRenewalDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>       
      <p>(Note: This is the start date of a renewed archive plan.)</p>            
      <asp:CompareValidator ID="archiveRenewedCompareValidator" runat="server" CssClass="errorMessage" ErrorMessage="Invalid range" ControlToValidate="txtSettingsArchiveRenewalDate" ControlToCompare="txtSettingsArchiveStartDate" Type="Date" Operator="GreaterThanEqual" ValidationGroup="UpdateSettings" Display="Dynamic" SetFocusOnError="true" />
    </div>
    <div class="divForm">
      <label class="label">*Max Point Limit:</label>
      <telerik:RadMaskedTextBox ID="txtSettingsMaxPointLimit" CssClass="textbox" runat="server" Mask="######" PromptChar="" ValidationGroup="UpdateSettings" />
      <asp:RequiredFieldValidator ID="editSettingsMaxPointLimitRFV" runat="server" ErrorMessage="Max Point Limit is a required field." ControlToValidate="txtSettingsMaxPointLimit" SetFocusOnError="true" Display="None" ValidationGroup="UpdateSettings" />
      <ajaxToolkit:ValidatorCalloutExtender ID="editSettingsMaxPointLimitVCE" runat="server" BehaviorID="editSettingsMaxPointLimitVCE" TargetControlID="editSettingsMaxPointLimitRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
    </div>
    <hr />

    <h2>Scheduled Reports</h2>

    <CW:ReportCriteria runat="server" ID="reportCriteriaDailyPriority" Title="Priority Daily Emails" />
    <CW:ReportCriteria runat="server" ID="reportCriteriaWeeklyPriority" Title="Priority Weekly Emails" />
    <CW:ReportCriteria runat="server" ID="reportCriteriaMonthlyPriority" Title="Priority Monthly Emails" />

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateSettings" runat="server" Text="Update" OnClick="updateSettingsButton_Click" ValidationGroup="UpdateSettings" />

  </div>

</asp:Panel>

