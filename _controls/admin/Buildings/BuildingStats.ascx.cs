﻿using CW.Data;
using CW.Data.Models.Building;
using CW.Utility;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.admin.Buildings
{
    public partial class BuildingStats : AdminUserControlGridAdminLinks
    {
        #region fields

            private static String name = "building statistic";
            private List<KeyValuePair<String, Object>> buildingItems = new List<KeyValuePair<String, Object>>();
            private List<KeyValuePair<String, Object>> diagnosticsItems = new List<KeyValuePair<String, Object>>();
            private DateTime mStartDate, mEndDate;

        #endregion

        #region properties

            #region overrides

                protected override LinkTypes LinkType  { get { return LinkTypes.GridLink; } }

                protected override Dictionary<AdminPage, List<KeyValuePair<String, Object>>> GetLinksAndItemsToAdd()
                {
                    if (!siteUser.VisibleBuildings.Any()) return null;

                    var defaultFields = new List<String>() { PropHelper.G<Building>(_ => _.BID) };
                    var adminPages = new List<AdminPage>() { AdminPage.AssociatedPoints, AdminPage.BuildingVariables, AdminPage.Diagnostics, AdminPage.Equipment, AdminPage.Points, AdminPage.ScheduledAnalyses };

                    return new AdminGridLinks(defaultFields, adminPages).Dict;
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<BuildingAdministrationStatistics>(_ => _.BID, _ => _.BuildingName, _ => _.EquipmentCount, _ => _.PointCount, _ => _.AssociatedPointCount, _ => _.BuildingVariablesCount, _ => _.ScheduledAnalysesCount, _ => _.AnalysesYesterdayCount); }
                }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    if (!siteUser.VisibleBuildings.Any()) return Enumerable.Empty<BuildingAdministrationStatistics>();

                    DateTimeHelper.GenerateDefaultDatesYesterday(out mStartDate, out mEndDate, siteUser.EarliestBuildingTimeZoneID);

                    return  DataMgr.BuildingDataMapper.GetBuildingAdministrationStatisticsByCID(siteUser.CID, mStartDate, searchCriteria);
                }

                protected override String NameField { get { return PropHelper.G<BuildingAdministrationStatistics>(_ => _.BuildingName); } }

                protected override String IdColumnName { get { return PropHelper.G<BuildingAdministrationStatistics>(_ => _.BID); } }

                protected override String Name { get { return name; } }

                protected override Type Entity { get { return typeof(BuildingAdministrationStatistics); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[]{ siteUser.CID, siteUser.UID }); } }

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[] 
                        {
                            KGSBuildingAdministration.TabMessages.Init,
                            KGSBuildingAdministration.TabMessages.AddBuilding,
                            KGSBuildingAdministration.TabMessages.EditBuilding,
                            KGSBuildingAdministration.TabMessages.EditBuildingVariables,
                            KGSBuildingAdministration.TabMessages.DeleteBuilding
                        };
                    }
                }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid, Int32 uid) {
                return new GridCacher.CacheKeyInfo(name + "." + typeof(BuildingAdministrationStatistics).Name, cid.ToString() + "|" + uid.ToString());
            }

        #endregion

        #region events

            private void Page_Load()
            {               
                IsDeferredLoading = (!Page.IsPostBack);
            }

        #endregion
    }
}