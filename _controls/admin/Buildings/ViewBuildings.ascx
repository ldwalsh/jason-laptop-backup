﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViewBuildings.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.ViewBuildings" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="FileUploaderControl" Assembly="FileUploaderControl" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Common.Constants" %>
<%@ Import Namespace="CW.Data.Models.Building" %>
<%@ Import Namespace="CW.Utility" %>

<script type="text/javascript" src="../../../_assets/scripts/admin.js"></script>

<h2>View Buildings</h2>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>  
                                
<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div> 
                                   
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <div id="divSuperSearch" runat="server">
    <label class="labelLeft">Search across all clients:</label>
    <asp:CheckBox ID="chkAllClients" runat="server" CssClass="checkbox" /> 
  </div>

  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>       
                                                                                                                
<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkEdit" Runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building Name">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.BuildingName)) %>"><%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.BuildingName), 30) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.ClientName)) %>"><%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.ClientName), 20) %></label>
          <asp:HiddenField ID="hdnCID" runat="server" Visible="true" Value='<%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_=>_.CID)) %>' />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Address" HeaderText="Address">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.Address)) %>"><%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.Address), 23) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="CountryName" HeaderText="Country">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.CountryName)) %>"><%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.CountryName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingTypeName" HeaderText="Building Type">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.BuildingTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.BuildingTypeName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="IsActive" ItemStyle-Wrap="true" HeaderText="Active">
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.IsActive)) %></ItemTemplate>                
      </asp:TemplateField>

      <asp:TemplateField SortExpression="IsVisible" ItemStyle-Wrap="true" HeaderText="Visible">  
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.IsVisible)) %></ItemTemplate>                                                  
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>
          <asp:HyperLink ID="hlBuildingConnect" NavigateUrl='<%# "/Connect.aspx?bid=" + GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.BID)) %>' runat="server" Target="_blank" Text="Building Connect" />
          <br />
          <asp:HyperLink ID="hlEquipmentConnect" NavigateUrl='<%# "/Connect.aspx?bid=" + GetCellContent(Container, PropHelper.G<GetBuildingsData>(_ => _.BID)) + "&all=true"%>' runat="server" Target="_blank" Text="Equipment Connect" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkDelete" Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this building permanently?\n\nAlternatively you can deactivate a building temporarily instead.');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField HeaderText="Actions">
        <ItemTemplate>
          <select id="ddlAction" runat="server" class="dropdownArrowOnly" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>        
  </asp:GridView>                                                                      
</div>
<br />
<br />

<div>
  <!--SELECT BUILDING DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptBuildingDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Building Details</h2>
          <ul id="buildingDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
          <br />
        </FooterTemplate>
      </asp:Repeater>

      <asp:Repeater ID="rptSASBuildingDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>SAS Building Details</h2>
          <ul id="SASBuildingDetails" class="detailsListWider">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
          <br />
        </FooterTemplate>
      </asp:Repeater>

      <asp:Repeater ID="rptInternalBuildingDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Internal Building Details</h2>
          <ul id="internalBuildingDetails" class="detailsListWider">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
          <br />
        </FooterTemplate>
      </asp:Repeater>

      <asp:Repeater ID="rptBuildingAccountDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Building Account Details</h2>
          <ul id="buildingAccountDetails" class="detailsListWide">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>

<!--EDIT BUILDING PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateBuilding">                                                                                             
  <div>
    <h2>Edit Building</h2>
  </div>

  <div>
    <a id="lnkSetFocusEdit" runat="server"></a>
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*Building Name:</label>
      <asp:TextBox ID="txtEditBuildingName" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Client:</label>
      <asp:Label ID="lblClientName" CssClass="labelContent" runat="server"></asp:Label>
    </div>
    <div class="divForm">
      <label class="label">*Address:</label>
      <asp:TextBox ID="txtEditAddress" CssClass="textbox" MaxLength="200" runat="server" />
    </div>                                                                                                                                                                                           
    <div class="divForm">
      <label class="label">*City:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditCity" runat="server" MaxLength="100" />
    </div>
    <div class="divForm">
      <label class="label">*Country:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlEditCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">State (Required based on Country):</label>
      <asp:DropDownList ID="ddlEditState" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
        <asp:ListItem Value="-1">Select country first...</asp:ListItem>
      </asp:DropDownList>                                                                                                                                                              
    </div> 
    <div class="divForm">
      <label class="label">Zip Code (Required based on Country):</label>
      <telerik:RadMaskedTextBox ID="txtEditZip" CssClass="textbox" runat="server" PromptChar="" ValidationGroup="EditBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Latitude:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditLatitude" runat="server" MaxLength="15" />
    </div>
    <div class="divForm">
      <label class="label">Longitude:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditLongitude" runat="server" MaxLength="15" />
    </div>
    <div class="divForm">
      <label class="label">*Time Zone:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlEditTimeZone" AppendDataBoundItems="true" runat="server" />
      <p>(Note: This value is used to accuratly view data using this timezone and not by the hosted locations timezones or by different point time zones. If points are in different timezones, they will be converted and displayed using the buildings time zone.)</p>
      <p>(Note: This time zone is not the timezone for which a points data is recorded as.)</p>
      <p>(Note: Daylight savings will MAY show double data during the "fall back" hour period, and a gap in data during the "spring forward" hour period.)</p>
    </div>                                               
    <div class="divForm">
      <label class="label">Phone:</label>
      <telerik:RadMaskedTextBox ID="txtEditPhone" CssClass="textbox" runat="server" Mask="(###)###-####" ValidationGroup="EditBuilding" />
    </div>
    <div class="divForm">
      <label class="label">*Square Footage:</label>                                                        
      <telerik:RadMaskedTextBox ID="txtEditSqft" CssClass="textbox" runat="server" Mask="##########" PromptChar="" ValidationGroup="EditBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Floors:</label>                                            
      <telerik:RadMaskedTextBox ID="txtEditFloors" CssClass="textbox" runat="server" Mask="####" ValidationGroup="EditBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Year Built:</label>
      <telerik:RadMaskedTextBox ID="txtEditYearBuilt" CssClass="textbox" runat="server" Mask="####" ValidationGroup="EditBuilding" />
    </div> 
    <div class="divForm">   
      <label class="label">*Building Class:</label>    
      <asp:DropDownList ID="ddlEditBuildingClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditBuildingClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>                                                                                                         
    <div class="divForm">   
      <label class="label">*Building Type:</label>    
      <asp:DropDownList ID="ddlEditBuildingType" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Building Ownership:</label>    
      <asp:DropDownList ID="ddlEditBuildingOwnership" CssClass="dropdown" runat="server">   
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>                                                             
        <asp:ListItem Value="Federal Government" Text="Federal Government" />
        <asp:ListItem Value="Individual Owner" Text="Individual Owner" />
        <asp:ListItem Value="Local Government" Text="Local Government" />
        <asp:ListItem Value="Non-Government Owner" Text="Non-Government Owner" />
        <asp:ListItem Value="Non-Profit Organization" Text="Non-Profit Organization" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Privately-Owned School" Text="Privately-Owned School" />
        <asp:ListItem Value="Property Management Company" Text="Property Management Company" />
        <asp:ListItem Value="Religious Organization" Text="Religious Organization" />
        <asp:ListItem Value="State Government" Text="State Government" />
      </asp:DropDownList>
    </div> 
    <div class="divForm">   
      <label class="label">Building Shape:</label>    
      <asp:DropDownList ID="ddlEditBuildingShape" CssClass="dropdown" runat="server">
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>                                                               
        <asp:ListItem Value="Cross-Shaped" Text="Cross-Shaped" />
        <asp:ListItem Value="E-Shaped" Text="E-Shaped" />
        <asp:ListItem Value="H-Shaped" Text="H-Shaped" />
        <asp:ListItem Value="L-Shaped" Text="L-Shaped" />
        <asp:ListItem Value="Narrow Rectangle" Text="Narrow Rectangle" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Rectangle with Courtyard" Text="Rectangle with Courtyard" />
        <asp:ListItem Value="Square" Text="Square" />
        <asp:ListItem Value="T-Shaped" Text="T-Shaped" />
        <asp:ListItem Value="U-Shaped" Text="U-Shaped" />
        <asp:ListItem Value="Wide Rectangle" Text="Wide Rectangle" />
      </asp:DropDownList>
    </div>        
    <div class="divForm">   
      <label class="label">Main Cooling System:</label>                                            
      <asp:DropDownList ID="ddlEditMainCoolingSystem" CssClass="dropdown" runat="server">
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>  
        <asp:ListItem Value="Central AC or Roof Top Unit" Text="Central AC or Roof Top Unit" />
        <asp:ListItem Value="Chillers" Text="Chillers" />
        <asp:ListItem Value="District Services" Text="District Services" />
        <asp:ListItem Value="Evaporative Coolers" Text="Evaporative Coolers" />
        <asp:ListItem Value="Heat Pumps" Text="Heat Pumps" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Packaged Terminal Units" Text="Packaged Terminal Units" />
        <asp:ListItem Value="Window AC" Text="Window AC" />
      </asp:DropDownList>
    </div> 
    <div class="divForm">   
      <label class="label">Main Heating System:</label>    
      <asp:DropDownList ID="ddlEditMainHeatingSystem" CssClass="dropdown" runat="server">
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>  
        <asp:ListItem Value="District Services" Text="District Services" />
        <asp:ListItem Value="Electric Units" Text="Electric Units" />
        <asp:ListItem Value="Furnace" Text="Furnace" />
        <asp:ListItem Value="Heat Pump" Text="Heat Pump" />
        <asp:ListItem Value="Hydronic Boiler" Text="Hydronic Boiler" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Packaged Terminal" Text="Packaged Terminal" />
        <asp:ListItem Value="Roof Top Unit" Text="Roof Top Unit" />
        <asp:ListItem Value="Space Heaters" Text="Space Heaters" />
        <asp:ListItem Value="Steam Boiler" Text="Steam Boiler" />
      </asp:DropDownList>
    </div> 
    <div class="divForm">   
      <label class="label">Roof Construction:</label>    
      <asp:DropDownList ID="ddlEditRoofConstruction" CssClass="dropdown" runat="server">
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>  
        <asp:ListItem Value="Asphalt" Text="Asphalt" />
        <asp:ListItem Value="Built-Up" Text="Built-Up" />
        <asp:ListItem Value="Concrete" Text="Concrete" />
        <asp:ListItem Value="Metal Surfacing" Text="Metal Surfacing" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Slate or Tile Shingles" Text="Slate of Tile Shingles" />
        <asp:ListItem Value="Synthetic Covering" Text="Synthetic Covering" />
        <asp:ListItem Value="Wood Shingles" Text="Wood Shingles" />
      </asp:DropDownList>
    </div>             
    <div class="divForm">   
      <label class="label">Envelope Glass Percentage (Default = 0):</label>    
      <telerik:RadNumericTextBox runat="server" ID="radNumericEditEnvelope" CssClass="textbox" Value="0"  MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>                                                                                                          
    </div>
    <div class="divForm">   
      <label class="label">Operating Hours Per Week (Default = 0):</label>      
      <telerik:RadNumericTextBox runat="server" ID="radNumericEditOperating" CssClass="textbox" Value="0"  MinValue="0" MaxValue="168" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
    </div>
    <div class="divForm">   
      <label class="label">Computers (Default = 0):</label>                                     
      <telerik:RadNumericTextBox runat="server" ID="radNumericEditComputers" CssClass="textbox" Value="0"  MinValue="0" MaxValue="10000" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>                                                                              
    </div>
    <div class="divForm">
      <label class="label">Occupants:</label>                                            
      <telerik:RadMaskedTextBox ID="txtEditOccupants" CssClass="textbox" runat="server" Mask="#####" ValidationGroup="EditBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Location:</label>
      <textarea name="txtEditLocation" id="txtEditLocation" cols="40" rows="5" onkeyup="limitChars(this, 250, 'divEditLocationCharInfo')" runat="server" />
      <div id="divEditLocationCharInfo"></div>
    </div> 
    <div class="divForm">
      <label class="label">Description:</label>
      <textarea name="txtEditDescription" id="txtEditDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" runat="server" />
      <div id="divEditDescriptionCharInfo"></div>                                                         
    </div>
                                                                                         
    <!--NESTED UPDATE PANEL NEEDED FOR FILE UPLOADER TRIGGERS-->    
    <asp:UpdatePanel ID="updatePanelNestedUpdate" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional"> 
      <ContentTemplate>  
        <div class="divForm">
          <label class="label">Image (800x600):</label>
          <CW:FileUploader ID="fileUploaderEdit" runat="server" />
        </div>
      </ContentTemplate>
    </asp:UpdatePanel>
                                                      
    <div class="divForm">
      <label class="label">Primary Contact Name:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditPrimaryContactName" runat="server" MaxLength="100" />
    </div>
    <div class="divForm">
      <label class="label">Primary Contact Email:</label>
      <asp:TextBox ID="txtEditPrimaryContactEmail" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Primary Contact Phone:</label>
      <telerik:RadMaskedTextBox ID="txtEditPrimaryContactPhone" CssClass="textbox" runat="server" Mask="(###)###-####" ValidationGroup="EditBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Website Link:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditWebsiteLink" runat="server" MaxLength="100" />
    </div>
    <div class="divForm">
      <label class="label">Kiosk Content:</label>
      <textarea name="txtEditKioskContent" id="txtEditKioskContent" cols="40" rows="5" onkeyup="limitChars(this, 500, 'divEditKioskContentCharInfo')" runat="server" />
      <div id="divEditKioskContentCharInfo"></div>
    </div>
    <div class="divForm">   
      <label class="label">Weather File:</label>    
      <asp:DropDownList ID="ddlEditGlobalFile" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div> 
    <div class="divForm">
      <label class="label">*Active:</label>
      <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                        
      <p>(Warning: Setting a building as inactive will not allow scheduled analyses to be put in the queue, and no incoming data will be recorded.)</p>
    </div>
    <div class="divForm">
      <label class="label">*Visible:</label>
      <asp:CheckBox ID="chkEditVisible" CssClass="checkbox" runat="server" />                                                        
      <p>(Warning: Setting a building as not visible will hide the building on non administrative pages, except for KGS or Provider Super Administrators or higher.)</p>
      <p>(Note: For any restricted users already logged in this will take effect upon next login.)</p>
    </div>
                                                                                                                                                                       
    <hr />

    <h2>Edit SAS Building Information</h2>

    <div class="divForm">
      <label class="label">SAS (Space Accounting Software) ReferenceID:</label>
      <asp:TextBox ID="txtEditSASReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">SAS (Space Accounting Software) Link:</label>
      <asp:TextBox ID="txtEditSASLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>

    <hr />

    <h2>Edit Internal Building Information</h2>

    <div class="divForm">
      <label class="label">Internal Notes:</label>
      <textarea name="txtEditInternalNotes" id="txtEditInternalNotes" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditInternalNotesCharInfo')" runat="server" />
      <div id="divEditInternalNotesCharInfo"></div>                                                         
    </div>
    <div class="divForm">
      <label class="label">Setup Contact Name:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditSetupContactName" runat="server" MaxLength="100" />
    </div>
    <div class="divForm">
      <label class="label">Energy Bureau Contact Name:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditEnergyBureauContactName" runat="server" MaxLength="100" />
    </div> 
                                                                                                                                                     
    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBuilding" runat="server" Text="Update Building"  OnClick="updateBuildingButton_Click" ValidationGroup="EditBuilding" />

  </div>
                                                
  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="editBuildingNameRFV" runat="server" ErrorMessage="Building Name is a required field." ControlToValidate="txtEditBuildingName" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingNameVCE" runat="server" BehaviorID="editBuildingNameVCE" TargetControlID="editBuildingNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editAddressRFV" runat="server" ErrorMessage="Address is a required field." ControlToValidate="txtEditAddress" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editAddressVCE" runat="server" BehaviorID="editAddressVCE" TargetControlID="editAddressRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editCityRFV" runat="server" ErrorMessage="City is a required field." ControlToValidate="txtEditCity" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editCityVCE" runat="server" BehaviorID="editCityVCE" TargetControlID="editCityRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editCountryRFV" runat="server" ErrorMessage="Country is a required field." ControlToValidate="ddlEditCountry" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editCountryVCE" runat="server" BehaviorID="editCountryVCE" TargetControlID="editCountryRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editStateRFV" runat="server" ErrorMessage="State is a required field." ControlToValidate="ddlEditState" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editStateVCE" runat="server" BehaviorID="editStateVCE" TargetControlID="editStateRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                            
  <asp:RequiredFieldValidator ID="editZipRequiredRFV" runat="server" ErrorMessage="Zip Code is a required field." ControlToValidate="txtEditZip" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" Enabled="false" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editZipRequiredVCE" runat="server" BehaviorID="editZipRequiredVCE" TargetControlID="editZipRequiredRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:RegularExpressionValidator ID="editZipREV" runat="server" ErrorMessage="Invalid Zip Format." ValidationExpression="\d{5}" ControlToValidate="txtEditZip" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" Enabled="false" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editZipInvalidVCE" runat="server" BehaviorID="editZipInvalidVCE" TargetControlID="editZipREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
     
  <ajaxToolkit:FilteredTextBoxExtender ID="editLatitudeFTE" runat="server" TargetControlID="txtEditLatitude" FilterType="Custom, Numbers" ValidChars="-." />
  <asp:RegularExpressionValidator ID="editLatitudeREV" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\.?\d*$" ControlToValidate="txtEditLatitude" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editLatitudeVCE" runat="server" BehaviorID="editLatitudeVCE" TargetControlID="editLatitudeREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="editLongitudeFTE" runat="server" TargetControlID="txtEditLongitude" FilterType="Custom, Numbers" ValidChars="-." />
  <asp:RegularExpressionValidator ID="editLongitudeREV" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\.?\d*$" ControlToValidate="txtEditLongitude" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editLongitudeVCE" runat="server" BehaviorID="editLongitudeVCE" TargetControlID="editLongitudeREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editTimeZoneRFV" runat="server" ErrorMessage="Time Zone is a required field." ControlToValidate="ddlEditTimeZone" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editTimeZoneVCE" runat="server" BehaviorID="editTimeZoneVCE" TargetControlID="editTimeZoneRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                     
  <asp:RegularExpressionValidator ID="editPhoneREV" runat="server" ErrorMessage="Invalid Phone Format." ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$" ControlToValidate="txtEditPhone" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPhoneVCE" runat="server" BehaviorID="editPhoneVCE" TargetControlID="editPhoneREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                        
  <asp:RequiredFieldValidator ID="editSqftRFV" runat="server" ErrorMessage="Sqft is a required field." ControlToValidate="txtEditSqft" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editSqftVCE" runat="server" BehaviorID="editSqftVCE" TargetControlID="editSqftRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editSqftCV" runat="server" ErrorMessage="Must be greater than 0" ControlToValidate="txtEditSqft" ValueToCompare="0" Type="Integer" Operator="GreaterThan" Display="None" SetFocusOnError="true" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editSqftCompareVCE" runat="server" BehaviorID="editSqftCompareVCE" TargetControlID="editSqftCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="editYearBuiltREV" runat="server" ErrorMessage="Invalid Year Built Format." ValidationExpression="\d{4}" ControlToValidate="txtEditYearBuilt" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editYearBuiltVCE" runat="server" BehaviorID="editYearBuiltVCE" TargetControlID="editYearBuiltREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                                                                                                
  <asp:RequiredFieldValidator ID="editBuildingTypeRFV" runat="server" ErrorMessage="Building Type is a required field." ControlToValidate="ddlEditBuildingType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingTypeVCE" runat="server" BehaviorID="editBuildingTypeVCE" TargetControlID="editBuildingTypeRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                                                                                                                                                                                                                                                              
  <asp:RegularExpressionValidator ID="editPrimaryContactEmailREV" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtEditPrimaryContactEmail" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPrimaryContactEmailVCE" runat="server" BehaviorID="editPrimaryContactEmailVCE" TargetControlID="editPrimaryContactEmailREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="editPrimaryContactPhoneREV" runat="server" ErrorMessage="Invalid Phone Format." ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$" ControlToValidate="txtEditPrimaryContactPhone" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPrimaryContactPhoneVCE" runat="server" BehaviorID="editPrimaryContactPhoneVCE" TargetControlID="editPrimaryContactPhoneREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="editWebsiteLinkREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtEditWebsiteLink" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editWebsiteLinkVCE" runat="server" BehaviorID="editWebsiteLinkVCE" TargetControlID="editWebsiteLinkREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="editSASLinkREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtEditSASLink" SetFocusOnError="true" Display="None" ValidationGroup="EditBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editSASLinkVCE" runat="server" BehaviorID="editSASLinkVCE" TargetControlID="editSASLinkREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />   
                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
</asp:Panel>