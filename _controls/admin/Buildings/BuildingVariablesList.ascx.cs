﻿using CW.Data.Models.BuildingVariable;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Buildings
{
    public partial class BuildingVariablesList : SiteUserControl
    {
        #region constants

            const String variablesUpdateSuccessful = "Building variables update was successful.";
            const String variablesUpdateFailed = "Building variables update failed. Please contact an administrator.";
            const String buildingVarsEmpty = "No building variables have been assigned.";

        #endregion

        #region properties

            public Int32 BID { get; set; }

        #endregion

        #region events

            private void Page_Load()
            {
                rptBuildingVars.Visible = lblBuildingVarsEmpty.Visible = false;
            }

        #endregion

        #region methods

            public void BindBuildingVariables(IEnumerable<GetBuildingBuildingVariableData> buildingVariables = null)
            {
                if (buildingVariables == null) buildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesWithDataByBID(BID).Where(bv => bv.UserEditable);

                if (buildingVariables.Any())
                {
                    rptBuildingVars.Visible = true;
                    rptBuildingVars.DataSource = buildingVariables;
                    rptBuildingVars.DataBind();
                    return;
                }

                lblBuildingVarsEmpty.Visible = true;
                lblBuildingVarsEmpty.Text = buildingVarsEmpty;
            }

        #endregion
    }
}