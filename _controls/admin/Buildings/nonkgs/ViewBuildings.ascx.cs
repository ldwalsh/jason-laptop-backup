﻿using CW.Business.Blob.Images;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Building;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using CW.Website.DependencyResolution;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Buildings.nonkgs
{
    public partial class ViewBuildings : AdminUserControlGrid
    {
        #region constants

            const String noBuildingImage = "no-building-image.png";
            const String updateSuccessful = "Building update was successful.";
            const String updateFailed = "Building update failed. Please contact an administrator.";            
            const String stateRequired = "State is a required field.";

        #endregion

        #region fields

            protected String buildingImageUrl = String.Empty;
            private enum BuildingDetails { Default, SAS, Account };

        #endregion

        #region properties

            #region overrides

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    var expression = new Expression<Func<Building, Object>>[] { (_ => _.BuildingType), (_ => _.Client), (_ => _.Country), (_ => _.State) };

                    return DataMgr.BuildingDataMapper.GetAllSearchedBuildings(searchCriteria.FirstOrDefault(), siteUser.CID, siteUser.UID, siteUser.IsKGSSuperAdminOrHigher, false, true, true, expression);
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetBuildingsData>(_ => _.BID, _ => _.BuildingName, _ => _.ClientName, _ => _.CID, _ => _.Address, _ => _.City, _ => _.Sqft, _ => _.CountryName, _ => _.BuildingTypeName, _ => _.IsActive, _ => _.IsVisible); }
                }

                protected override String NameField { get { return PropHelper.G<Building>(_ => _.BuildingName); } }

                protected override String IdColumnName { get { return PropHelper.G<Building>(_ => _.BID); } }

                protected override String Name { get { return typeof(Building).Name; } }

                protected override Type Entity { get { return typeof(GetBuildingsData); } }
                
                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID, siteUser.UID }); } }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid, Int32 uid)
            {
                return new GridCacher.CacheKeyInfo(typeof(Building).Name + "." + typeof(GetBuildingsData).Name, cid.ToString() + "|" + uid.ToString());
            }

        #endregion

        #region events

            #region grid events

                #region override

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var bid = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString()); 
                        var mBuilding = DataMgr.BuildingDataMapper.GetBuildingByBID(bid, true, false, false, true, true, false);

                        BindCountries(ddlEditCountry);
                        BindBuildingClasses(ddlEditBuildingClass);
                        BindBuildingTypes(ddlEditBuildingType, mBuilding.BuildingType.BuildingClassID);

                        SetBuildingIntoEditForm(mBuilding);

                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                #endregion

            #endregion

            #region button events

                protected void updateBuildingButton_Click(Object sender, EventArgs e)
                {
                    var mBuilding = new Building();

                    LoadEditFormIntoBuilding(mBuilding);

                    try
                    {
                        //Strange bug here on IE where even though state is a required field and the value is null, the UI is not catching it.  Oddly enough it does for the KGS Building Admin page
                        if (mBuilding.StateID == null && editStateRFV.Enabled)
                        {
                            LabelHelper.SetLabelMessage(lblEditError, stateRequired, lnkSetFocusEdit);
                            return;
                        }

                        var originalBuilding = DataMgr.BuildingDataMapper.GetBuildingByBID(mBuilding.BID, false, false, false, false, true, false);

                        if (fileUploader.HasChanged)
                        {
                            var image = new BuildingProfileImage(siteUser.CID, DataMgr.OrgBasedBlobStorageProvider, null, new BuildingProfileImage.Args(mBuilding.BID));

                            if (!String.IsNullOrEmpty(originalBuilding.ImageExtension))
                            {
                                image.SetExtension(originalBuilding.ImageExtension).Delete();
                                mBuilding.ImageExtension = String.Empty;
                            }

                            if (fileUploader.File != null)
                            {
                                image.SetExtension(fileUploader.File.Extension).Insert(fileUploader.File.Content);
                                mBuilding.ImageExtension = fileUploader.File.Extension;
                            }
                        }

                        var addressStr = CreateAddressString(mBuilding, ddlEditState);

                        if ((String.IsNullOrWhiteSpace(txtEditLatitude.Text) || String.IsNullOrWhiteSpace(txtEditLongitude.Text)) || (addressStr != CreateAddressString(originalBuilding, ddlEditState)))
                        {
                            var coordinates = IoC.Resolve<BingMapsHelper>().GetGeocodeData(addressStr);

                            if (coordinates != null)
                            {
                                txtEditLatitude.Text = coordinates[0].ToString();
                                txtEditLongitude.Text = coordinates[1].ToString();

                                mBuilding.Latitude = coordinates[0];
                                mBuilding.Longitude = coordinates[1];
                            }
                        }

                        DataMgr.BuildingDataMapper.UpdatePartialUserBuilding(mBuilding);
                        OperationComplete(OperationType.Update, updateSuccessful, BuildingAdministration.TabMessages.EditBuilding);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating partial user building.", ex);
                    }
                }

            #endregion

            #region ddl events

                protected void ddlEditCountry_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindStates(ddlEditState, ddlEditCountry.SelectedValue);
                    ToggleStateAndZipValidators(ddlEditCountry.SelectedValue);
                }

                protected void ddlEditBuildingClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindBuildingTypes(ddlEditBuildingType, Convert.ToInt32(ddlEditBuildingClass.SelectedValue));
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var building = DataMgr.BuildingDataMapper.GetFullBuildingByBID(CurrentID).First();

                    if (String.IsNullOrEmpty(building.ImageExtension))
                        buildingImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage;
                    else
                    {
                        buildingImageUrl =
                        (
                            new BuildingProfileImage(siteUser.CID,
                                                     DataMgr.OrgBasedBlobStorageProvider,
                                                     building.ImageExtension,
                                                     new BuildingProfileImage.Args(CurrentID))
                                                     .Exists() ? HandlerHelper.BuildingImageUrl(siteUser.CID, CurrentID, building.ImageExtension) : ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage
                        );
                    }

                    SetBuildingDataIntoViewState(building, buildingImageUrl);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(BuildingDetails.Default), rptBuildingDetails);
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(BuildingDetails.SAS), rptSASBuildingDetails);
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(BuildingDetails.Account), rptBuildingAccountDetails);
                }

            #endregion

            private void SetBuildingDataIntoViewState(GetBuildingsData building, String buildingImageUrl)
            {
                SetBuildingDetails(building, buildingImageUrl);
                SetSASBuildingDetails(building);
                SetBuildingAccountDetails(building);
            }

            private void SetBuildingDetails(GetBuildingsData building, String buildingImageUrl)
            {
                SetKeyValuePairItem("Building Name", building.BuildingName);
                SetKeyValuePairItem("Address", building.Address);
                SetKeyValuePairItem("City", building.City);
                SetKeyValuePairItem("State", building.StateName);
                SetKeyValuePairItem("Zip", building.Zip);
                SetKeyValuePairItem("Country", building.CountryName);

                if (building.Latitude != null) SetKeyValuePairItem("Latitude", CultureHelper.FormatNumberToInvariantString((decimal)building.Latitude));
                if (building.Longitude != null) SetKeyValuePairItem("Longitude", CultureHelper.FormatNumberToInvariantString((decimal)building.Longitude));

                SetKeyValuePairItem("Time Zone", building.TimeZone);
                SetKeyValuePairItem("Phone", building.Phone);
                SetKeyValuePairItem("Square Footage", building.Sqft.ToString());
                SetKeyValuePairItem("Floors", building.Floors.ToString());
                SetKeyValuePairItem("Year Built", building.YearBuilt.ToString());
                SetKeyValuePairItem("Building Class", building.BuildingClassName);
                SetKeyValuePairItem("Building Class Description", building.BuildingClassDescription);
                SetKeyValuePairItem("Building Type", building.BuildingTypeName);
                SetKeyValuePairItem("Building Type Description", building.BuildingTypeDescription);
                SetKeyValuePairItem("Building Ownership", building.BuildingOwnership);
                SetKeyValuePairItem("Building Shape", building.BuildingShape);
                SetKeyValuePairItem("Main Cooling System", building.MainCoolingSystem);
                SetKeyValuePairItem("Main Heating System", building.MainHeatingSystem);
                SetKeyValuePairItem("Roof Construction", building.RoofConstruction);
                SetKeyValuePairItem("Envelope Glass Percentage", building.EnvelopeGlassPercentage.ToString());
                SetKeyValuePairItem("Operating Hours/Week (Max 168)", building.OperatingHours.ToString());
                SetKeyValuePairItem("Computers", building.Computers.ToString());
                SetKeyValuePairItem("Occupants", building.Occupants.ToString());
                SetKeyValuePairItem("Location", building.Location);
                SetKeyValuePairItem("Description", building.Description);
                SetKeyValuePairItem("Building Image", buildingImageUrl, "<img id='imgPreview' src='{0}' alt='" + building.BuildingName + "' class='imgDetailsListPreview' />");
                SetKeyValuePairItem("Primary Contact Name", building.PrimaryContactName);
                SetKeyValuePairItem("Primary Contact Email", building.PrimaryContactEmail);
                SetKeyValuePairItem("Primary Contact Phone", building.PrimaryContactPhone);
                SetKeyValuePairItem("Website Link", building.WebsiteLink, "<a href='{0}' target='_blank'>{0}</a>");
                SetKeyValuePairItem("Kiosk Content", building.KioskContent);
                SetKeyValuePairItem("Weather File Name", building.WeatherFileName);
                SetKeyValuePairItem("IsActive", building.IsActive.ToString());
                SetKeyValuePairItem("IsVisible", building.IsVisible.ToString());

                SetDetailsViewDataToViewState(Convert.ToInt32(BuildingDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            private void SetSASBuildingDetails(GetBuildingsData building)
            {
                if (!String.IsNullOrWhiteSpace(building.SASReferenceID) || !String.IsNullOrWhiteSpace(building.SASLink))
                {
                    SetKeyValuePairItem("SAS (Space Accounting Software) ReferenceID", building.SASReferenceID);
                    if(building.SASLink != null) SetKeyValuePairItem("SAS (Space Accounting Software) Link", "<a href='" + StringHelper.ExternalLinkCorrector(building.SASLink) + "' target='_blank'>" + building.SASLink + "</a>");

                    SetDetailsViewDataToViewState(Convert.ToInt32(BuildingDetails.SAS));
                }
            }

            private void SetBuildingAccountDetails(GetBuildingsData building)
            {
                SetKeyValuePairItem("Unit System", DataConstants.UnitSystem[Convert.ToInt32(building.UnitSystem)]);
                SetKeyValuePairItem("Culture", CultureHelper.GetCultureInfo(Convert.ToInt32(building.LCID)).DisplayName);
                SetKeyValuePairItem("Energy Bureau Service", building.EnergyBureauService.ToString());

                if (building.EnergyBureauService)
                {
                    SetKeyValuePairItem("Energy Bureau Service Frequency", building.EnergyBureauServiceFrequency);
                    SetKeyValuePairItem("Energy Bureau Service Hours", building.EnergyBureauServiceHours.ToString());
                }

                SetKeyValuePairItem("Subscription Plan (Months)", building.SubscriptionPlan.ToString());

                if (building.SubscriptionStartDate.HasValue) SetKeyValuePairItem("Subscription Start Date", DateTime.Parse(building.SubscriptionStartDate.ToString()).ToShortDateString());
                if (building.SubscriptionRenewalDate.HasValue) SetKeyValuePairItem("Subscription Date Renewed", DateTime.Parse(building.SubscriptionRenewalDate.ToString()).ToShortDateString());

                SetKeyValuePairItem("External Archive Plan (Months)", building.ArchivePlan.ToString());

                if (building.ArchiveStartDate.HasValue) SetKeyValuePairItem("External Archive Start Date", DateTime.Parse(building.ArchiveStartDate.ToString()).ToShortDateString());
                if (building.ArchiveRenewalDate.HasValue) SetKeyValuePairItem("External Archive Date Renewed", DateTime.Parse(building.ArchiveRenewalDate.ToString()).ToShortDateString());

                SetKeyValuePairItem("Automated Priority Weekly Emails", building.AutomatedPriorityWeeklyEmails.ToString());
                SetKeyValuePairItem("Automated Priority Monthly Emails", building.AutomatedPriorityMonthlyEmails.ToString());

                SetDetailsViewDataToViewState(Convert.ToInt32(BuildingDetails.Account));
            }

            private void SetBuildingIntoEditForm(Building building)
            {
                hdnEditID.Value = Convert.ToString(building.BID);
                lblEditBuildingName.Text = building.BuildingName;
                txtEditAddress.Text = building.Address;
                txtEditCity.Text = building.City;

                ddlEditCountry.SelectedValue = building.CountryAlpha2Code;
                ddlEditCountry_OnSelectedIndexChanged(null, null);

                if (building.StateID != null) ddlEditState.SelectedValue = Convert.ToString(building.StateID); 
                    
                txtEditZip.Text = building.Zip;
                txtEditLatitude.Text = Convert.ToString(building.Latitude, CultureInfo.InvariantCulture);
                txtEditLongitude.Text = Convert.ToString(building.Longitude, CultureInfo.InvariantCulture);
                lblEditTimeZone.Text = building.TimeZone;
                txtEditPhone.Text = building.Phone;
                txtEditSqft.Text = Convert.ToString(building.Sqft);
                txtEditFloors.Text = Convert.ToString(building.Floors);
                txtEditYearBuilt.Text = Convert.ToString(building.YearBuilt);
                ddlEditBuildingClass.SelectedValue = building.BuildingType.BuildingClassID.ToString();
                ddlEditBuildingType.SelectedValue = building.BuildingTypeID.ToString();
                ddlEditBuildingOwnership.SelectedValue = building.BuildingOwnership;
                ddlEditBuildingShape.SelectedValue = building.BuildingShape;
                ddlEditMainCoolingSystem.SelectedValue = building.MainCoolingSystem;
                ddlEditMainHeatingSystem.SelectedValue = building.MainHeatingSystem;
                ddlEditRoofConstruction.SelectedValue = building.RoofConstruction;
                radNumericEditEnvelope.Text = building.EnvelopeGlassPercentage.ToString();
                radNumericEditOperating.Text = building.OperatingHours.ToString();
                txtEditOccupants.Text = Convert.ToString(building.Occupants);
                radNumericEditComputers.Text = building.Computers.ToString();
                txtEditLocation.Value = building.Location;
                txtEditDescription.Value = building.Description;

                //Image Preview
                fileUploader.Clear();

                if (!String.IsNullOrEmpty(building.ImageExtension))
                {
                    var image = new BuildingProfileImage(building, DataMgr.OrgBasedBlobStorageProvider);

                    if (image.Exists()) fileUploader.SetFile(image.Retrieve(), image.FileName);
                }

                txtEditPrimaryContactName.Text = building.PrimaryContactName;
                txtEditPrimaryContactEmail.Text = building.PrimaryContactEmail;
                txtEditPrimaryContactPhone.Text = building.PrimaryContactPhone;
                txtEditWebsiteLink.Text = building.WebsiteLink;
                txtEditKioskContent.Value = building.KioskContent;
                lblEditActive.Text = building.IsActive.ToString();
                lblEditVisible.Text = building.IsVisible.ToString();

                //SAS Information
                txtEditSASReferenceID.Text = building.SASReferenceID;
                txtEditSASLink.Text = building.SASLink;
            }

            protected void LoadEditFormIntoBuilding(Building building)
            {
                building.BID = Convert.ToInt32(hdnEditID.Value);

                //Building Name not user editable

                building.Address = txtEditAddress.Text;
                building.City = txtEditCity.Text;
                building.CountryAlpha2Code = ddlEditCountry.SelectedValue;
                building.StateID = ddlEditState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditState.SelectedValue) : null;
                building.Zip = String.IsNullOrEmpty(txtEditZip.Text) ? null : txtEditZip.Text;
                building.Latitude = String.IsNullOrEmpty(txtEditLatitude.Text) ? null : (decimal?)Convert.ToDecimal(txtEditLatitude.Text, CultureInfo.InvariantCulture);
                building.Longitude = String.IsNullOrEmpty(txtEditLongitude.Text) ? null : (decimal?)Convert.ToDecimal(txtEditLongitude.Text, CultureInfo.InvariantCulture);

                //Building Timezone not user editable

                building.Phone = String.IsNullOrEmpty(txtEditPhone.Text) ? null : txtEditPhone.Text;
                building.Sqft = Convert.ToInt32(txtEditSqft.Text);
                building.Floors = String.IsNullOrEmpty(txtEditFloors.Text) ? null : (int?)Convert.ToInt32(txtEditFloors.Text);
                building.YearBuilt = String.IsNullOrEmpty(txtEditYearBuilt.Text) ? null : (int?)Convert.ToInt32(txtEditYearBuilt.Text);

                //buildingClassID is assigned via BuildingTypeID
                building.BuildingTypeID = Convert.ToInt32(ddlEditBuildingType.SelectedValue);

                building.BuildingOwnership = ddlEditBuildingOwnership.SelectedValue != "-1" ? ddlEditBuildingOwnership.SelectedValue : null;
                building.BuildingShape = ddlEditBuildingShape.SelectedValue != "-1" ? ddlEditBuildingShape.SelectedValue : null;
                building.MainCoolingSystem = ddlEditMainCoolingSystem.SelectedValue != "-1" ? ddlEditMainCoolingSystem.SelectedValue : null;
                building.MainHeatingSystem = ddlEditMainHeatingSystem.SelectedValue != "-1" ? ddlEditMainHeatingSystem.SelectedValue : null;
                building.RoofConstruction = ddlEditRoofConstruction.SelectedValue != "-1" ? ddlEditRoofConstruction.SelectedValue : null;
                building.EnvelopeGlassPercentage = !String.IsNullOrEmpty(radNumericEditEnvelope.Text) ? Convert.ToInt32(radNumericEditEnvelope.Text) : 0;
                building.OperatingHours = !String.IsNullOrEmpty(radNumericEditOperating.Text) ? Convert.ToInt32(radNumericEditOperating.Text) : 0;
                building.Occupants = String.IsNullOrEmpty(txtEditOccupants.Text) ? null : (int?)Convert.ToInt32(txtEditOccupants.Text);
                building.Computers = !String.IsNullOrEmpty(radNumericEditComputers.Text) ? Convert.ToInt32(radNumericEditComputers.Text) : 0;
                building.Location = String.IsNullOrEmpty(txtEditLocation.Value) ? null : txtEditLocation.Value;
                building.Description = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                building.ImageExtension = (fileUploader.File == null) ? String.Empty : fileUploader.File.Extension;
                building.PrimaryContactName = String.IsNullOrEmpty(txtEditPrimaryContactName.Text) ? null : txtEditPrimaryContactName.Text;
                building.PrimaryContactEmail = String.IsNullOrEmpty(txtEditPrimaryContactEmail.Text) ? null : txtEditPrimaryContactEmail.Text;
                building.PrimaryContactPhone = String.IsNullOrEmpty(txtEditPrimaryContactPhone.Text) ? null : txtEditPrimaryContactPhone.Text;
                building.WebsiteLink = String.IsNullOrEmpty(txtEditWebsiteLink.Text) ? null : txtEditWebsiteLink.Text;
                building.KioskContent = String.IsNullOrEmpty(txtEditKioskContent.Value) ? null : txtEditKioskContent.Value;

                //Active not changeable here
                //Visible not changeable here

                //Additional Information
                building.SASReferenceID = String.IsNullOrEmpty(txtEditSASReferenceID.Text) ? null : txtEditSASReferenceID.Text;
                building.SASLink = String.IsNullOrEmpty(txtEditSASLink.Text) ? null : txtEditSASLink.Text;
            }

            private void ToggleStateAndZipValidators(String value)
            {
                editZipRFV.Enabled = editZipREV.Enabled = (value == BusinessConstants.Countries.UnitedStatesAlpha2Code);
                txtEditZip.Mask = (value == BusinessConstants.Countries.UnitedStatesAlpha2Code) ? "#####" : "aaaaaaaaaa";
                editStateRFV.Enabled = (ddlEditState.Items.Count > 2);
            }

            private String CreateAddressString(Building building, DropDownList ddl)
            {
                return String.Join(",", new[] { building.Address,
						                        building.City,
						                        ((building.StateID == null) ? String.Empty : (ddl.Items.FindByValue(building.StateID.Value.ToString()) == null ? String.Empty : ddl.Items.FindByValue(building.StateID.Value.ToString()).Text)),
						                        building.Zip,
						                        building.CountryAlpha2Code }
                                  );
            }

            #region binders

                private void BindCountries(DropDownList ddl)
                {
                    BindDDL(ddl, "CountryName", "Alpha2Code", DataMgr.CountryDataMapper.GetAllCountries(), true);
                }

                private void BindStates(DropDownList ddl, String alpha2Code)
                {
                    var lItem = new ListItem() { Text = (alpha2Code != "-1") ? "Select one..." : "Select country first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "StateName", "StateID", (alpha2Code != "-1") ? DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code) : null, true, lItem);
                }

                private void BindBuildingClasses(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingClassName", "BuildingClassID", DataMgr.BuildingClassDataMapper.GetAllBuildingClasses(), true);
                }

                private void BindBuildingTypes(DropDownList ddl, Int32 buildingClassID)
                {
                    var lItem = new ListItem() { Text = (buildingClassID != -1) ? "Select one..." : "Select class first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "BuildingTypeName", "BuildingTypeID", (buildingClassID != -1) ? DataMgr.BuildingTypeDataMapper.GetAllBuildingTypesByBuildingClassID(buildingClassID) : null, true, lItem);
                }
            
            #endregion

        #endregion
    }
}