﻿using CW.CMMSIntegration;
using CW.Data;
using CW.Utility;
using CW.Website._administration._shared.CMMS;
using System;

namespace CW.Website._controls.admin.Buildings
{
    public partial class BuildingCMMSSettings : AdminUserControlBase
    {
        #region fields

            SettingsViewManager _viewManager;

        #endregion

        #region properties

            CMMSSettingsView View => CMMSSettingsView;
            SettingsViewManager ViewManager => _viewManager = _viewManager ?? new SettingsViewManager(DataMgr.ConnectionString, LogMgr, View);
            int BuildingId => int.Parse(ddlBuilding.SelectedValue);
            public bool IsReadOnly { private get; set; }
            bool HasBuildingSettings => (ViewManager.SettingsType == ConfigTypeEnum.Building);

        #endregion

        #region events

            void Page_FirstLoad()
            {
                BindDDL(ddlBuilding, PropHelper.G<Building>(_ => _.BuildingName), PropHelper.G<Building>(_ => _.BID), siteUser.VisibleBuildings, true);

                ViewManager.ConfigureView();
            }

            //need to reset message on async post backs, due to view state saving value
            void Page_AsyncPostbackLoad() { lblMessage.Text = string.Empty; }

            #region ddl events

                protected void ddlBuilding_SelectedIndexChanged(object sender, EventArgs e)
                {
                    var index = ddlBuilding.SelectedIndex;

                    pnlCMMSSettingsView.Visible = (index > 0);

                    //ensure that settings type viewstate value is reset to None, as a new building has been selected
                    //and the new selected building needs to be checked for if it has settings or not
                    ViewManager.SettingsType = ConfigTypeEnum.None;

                    if (index == 0) return;

                    ViewManager.SetCMMSConfiguration(ConfigTypeEnum.Building, BuildingId);

                    chkEnable.Checked = chkInherit.Enabled = ViewManager.IsEnabled;

                    chkInherit.Checked = ViewManager.IsInherit;

                    if (DisableIfReadOnly()) return; //the form is read only, no need to enable submittal and/or deletion below

                    //form is submittable only if it's not being inherited from client, and it has existing building settings to update
                    btnUpdate.Visible = (HasBuildingSettings && !chkInherit.Checked); 
                
                    btnDelete.Visible = HasBuildingSettings; //settings are deletable only if they are existing building settings (not inheriting)
                }

            #endregion
    
            #region button events

                protected void UpdateButton_Click(object sender, EventArgs e)
                {
                    ViewManager.SaveConfig(BuildingId, chkEnable.Checked, chkInherit.Checked);
                    
                    switch (ViewManager.SettingsType) //alter message based on a new building settings record or updated one
                    {
                        case ConfigTypeEnum.Client:
                        case ConfigTypeEnum.None:
                            lblMessage.Text = ViewManager.Message.Replace("updated", "created");
                            break;
                        default:
                            lblMessage.Text = ViewManager.Message;
                            break;
                    }

                    lnkSetFocus.Focus();

                    if (!chkInherit.Checked) //only set the following items if the user explicitly created building settings
                    {
                        ViewManager.SettingsType = ConfigTypeEnum.Building; //explicitly set settings type viewState prop to building, as user created or updated a building config

                        btnUpdate.Visible = btnDelete.Visible = true; //explicitly enable submittal and deletion of building config settings, now that they exist
                    }
                }

                protected void DeleteButton_Click(object sender, EventArgs e) //TODO: Determine a way to centralize this later in a clean way
                {
                    ViewManager.DeleteBuildingConfig(BuildingId);

                    lblMessage.Text = ViewManager.Message;

                    ViewManager.SettingsType = ConfigTypeEnum.None; //explicitly set settings type viewState prop to none, as user deleted a building config

                    //explicitly disable submittal and deletion of config settings, now that they were deleted, and uncheck the enabled checkbox, and disable the chkInherit checkbox
                    chkEnable.Checked = chkInherit.Enabled = btnUpdate.Visible = btnDelete.Visible = false;

                    ViewManager.ClearCMMSViewFields(); //clear out the form fields
                }

            #endregion

            #region checkbox events

                protected void chkEnable_CheckedChanged(object sender, EventArgs e)
                {
                    var chkEnabledChecked = chkEnable.Checked;

                    ViewManager.SetEditability(chkEnabledChecked);

                    chkInherit.Checked = false;

                    chkInherit.Enabled = chkEnabledChecked;

                    //we only want to enable form submittal if the building either has setting, or it's the initial record (new building settings)
                    //where the form has been enabled via the check box being checked, and the inherit option is disabled
                    btnUpdate.Visible = (HasBuildingSettings || chkEnabledChecked);

                    //for deletion, it only matters that building settings exist to be deleted, check box value is irrelevant
                    btnDelete.Visible = HasBuildingSettings;
                }

                protected void chkInherit_CheckedChanged(object sender, EventArgs e)
                {
                    var originalType = ViewManager.SettingsType; //grab original settings type value before checking below based on inheritance for a client (i.e. IsInherit)
                    var isInherit = (chkInherit.Checked);
                    var type = (isInherit) ? ConfigTypeEnum.Client : ConfigTypeEnum.Building;
                    var id = (isInherit) ? siteUser.CID : int.Parse(ddlBuilding.SelectedValue);

                    ViewManager.SetCMMSConfiguration(type, id);

                    ViewManager.SetEditability(!isInherit);
            
                    if (isInherit)
                    {
                        //no client settings were found, explict message for user information is set here
                        if (ViewManager.SettingsType == ConfigTypeEnum.None) lblMessage.Text = "No client settings, there is nothing to inherit";

                        btnUpdate.Visible = (originalType == ConfigTypeEnum.Building); //explicit check here, as submittal should only be enabled if there are building settings to delete and override

                        btnDelete.Visible = false; //explicitly disable form deletion, delete is handled by the update button above ^^^

                        return;
                    }

                    btnUpdate.Visible = true; //the are not inheriting, allow form submittal

                    btnDelete.Visible = HasBuildingSettings; //settings are deletable only if they are existing building settings (not inheriting)
                }

            #endregion

        #endregion

        #region methods

            bool DisableIfReadOnly()
            {
                if (IsReadOnly) chkEnable.Enabled = chkInherit.Enabled = View.IsEditable = btnUpdate.Visible = btnDelete.Visible = false;

                return IsReadOnly;
            }

        #endregion
    }
}