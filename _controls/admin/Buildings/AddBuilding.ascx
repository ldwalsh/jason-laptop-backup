﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AddBuilding.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.AddBuilding" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="FileUploaderControl" Assembly="FileUploaderControl" %>

<asp:Panel ID="pnlAddBuilding" runat="server" DefaultButton="btnAddBuilding">

  <h2>Add Building</h2>

  <div>
         
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />

    <div class="divForm">
      <label class="label">*Building Name:</label>
      <asp:TextBox ID="txtAddBuildingName" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>                                                    
    <div class="divForm">
      <label class="label">*Address:</label>
      <asp:TextBox ID="txtAddAddress" CssClass="textbox" MaxLength="200" runat="server" />
    </div>                                                                                                                                                                                           
    <div class="divForm">
      <label class="label">*City:</label>
      <asp:TextBox CssClass="textbox" ID="txtAddCity" runat="server" MaxLength="100" />
    </div>
    <div class="divForm">
      <label class="label">*Country:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlAddCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>  
    <div class="divForm">
      <label class="label">State (Required based on Country):</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlAddState" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Zip Code (Required based on Country):</label>
      <telerik:RadMaskedTextBox ID="txtAddZip" CssClass="textbox" runat="server" Mask="aaaaaaaaaa" PromptChar="" ValidationGroup="AddBuilding" />
    </div>  
    <div class="divForm">
      <label class="label">*Time Zone:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlAddTimeZone" AppendDataBoundItems="true" runat="server" />
      <p>(Note: This value is used to accuratly view data using this timezone and not by the hosted locations timezones or by different point time zones. If points are in different timezones, they will be converted and displayed using the buildings time zone. Ex: reports.)</p>
      <p>(Note: This time zone is not the timezone for which a points data is recorded as.)</p>
      <p>(Note: Daylight savings will MAY show double data during the "fall back" hour period, and a gap in data during the "spring forward" hour period.)</p>
    </div>                                                   
    <div class="divForm">
      <label class="label">Phone:</label>
      <telerik:RadMaskedTextBox ID="txtAddPhone" CssClass="textbox" runat="server" Mask="(###)###-####" ValidationGroup="AddBuilding" />
    </div>
    <div class="divForm">
      <label class="label">*Square Footage:</label>
      <telerik:RadMaskedTextBox ID="txtAddSqft" CssClass="textbox" runat="server" Mask="##########" PromptChar="" ValidationGroup="AddBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Floors:</label>
      <telerik:RadMaskedTextBox ID="txtAddFloors" CssClass="textbox" runat="server" Mask="####" PromptChar="" ValidationGroup="AddBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Year Built:</label>
      <telerik:RadMaskedTextBox ID="txtAddYearBuilt" CssClass="textbox" runat="server" Mask="####" ValidationGroup="AddBuilding" />
    </div>
    <div class="divForm">   
      <label class="label">*Building Class:</label>    
      <asp:DropDownList ID="ddlAddBuildingClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddBuildingClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Building Type:</label>    
      <asp:DropDownList ID="ddlAddBuildingType" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Building Ownership:</label>    
      <asp:DropDownList ID="ddlAddBuildingOwnership" CssClass="dropdown" runat="server">                                                                
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem>   
        <asp:ListItem Value="Federal Government" Text="Federal Government" />
        <asp:ListItem Value="Individual Owner" Text="Individual Owner" />
        <asp:ListItem Value="Local Government" Text="Local Government" />
        <asp:ListItem Value="Non-Government Owner" Text="Non-Government Owner" />
        <asp:ListItem Value="Non-Profit Organization" Text="Non-Profit Organization" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Privately-Owned School" Text="Privately-Owned School" />
        <asp:ListItem Value="Property Management Company" Text="Property Management Company" />
        <asp:ListItem Value="Religious Organization" Text="Religious Organization" />
        <asp:ListItem Value="State Government" Text="State Government" />
      </asp:DropDownList>
    </div> 
    <div class="divForm">   
      <label class="label">Building Shape:</label>    
      <asp:DropDownList ID="ddlAddBuildingShape" CssClass="dropdown" runat="server">                                                                
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem> 
        <asp:ListItem Value="Cross-Shaped" Text="Cross-Shaped" />
        <asp:ListItem Value="E-Shaped" Text="E-Shaped" />
        <asp:ListItem Value="H-Shaped" Text="H-Shaped" />
        <asp:ListItem Value="L-Shaped" Text="L-Shaped" />
        <asp:ListItem Value="Narrow Rectangle" Text="Narrow Rectangle" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Rectangle with Courtyard" Text="Rectangle with Courtyard" />
        <asp:ListItem Value="Square" Text="Square" />
        <asp:ListItem Value="T-Shaped" Text="T-Shaped" />
        <asp:ListItem Value="U-Shaped" Text="U-Shaped" />
        <asp:ListItem Value="Wide Rectangle" Text="Wide Rectangle" />
      </asp:DropDownList>
    </div>        
    <div class="divForm">   
      <label class="label">Main Cooling System:</label>                                            
      <asp:DropDownList ID="ddlAddMainCoolingSystem" CssClass="dropdown" runat="server">
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem> 
        <asp:ListItem Value="Central AC or Roof Top Unit" Text="Central AC or Roof Top Unit" />
        <asp:ListItem Value="Chillers" Text="Chillers" />
        <asp:ListItem Value="District Services" Text="District Services" />
        <asp:ListItem Value="Evaporative Coolers" Text="Evaporative Coolers" />
        <asp:ListItem Value="Heat Pumps" Text="Heat Pumps" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Packaged Terminal Units" Text="Packaged Terminal Units" />
        <asp:ListItem Value="Window AC" Text="Window AC" />
      </asp:DropDownList>
    </div>
    <div class="divForm">   
      <label class="label">Main Heating System:</label>    
      <asp:DropDownList ID="ddlAddMainHeatingSystem" CssClass="dropdown" runat="server">
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem> 
        <asp:ListItem Value="District Services" Text="District Services" />
        <asp:ListItem Value="Electric Units" Text="Electric Units" />
        <asp:ListItem Value="Furnace" Text="Furnace" />
        <asp:ListItem Value="Heat Pump" Text="Heat Pump" />
        <asp:ListItem Value="Hydronic Boiler" Text="Hydronic Boiler" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Packaged Terminal" Text="Packaged Terminal" />
        <asp:ListItem Value="Roof Top Unit" Text="Roof Top Unit" />
        <asp:ListItem Value="Space Heaters" Text="Space Heaters" />
        <asp:ListItem Value="Steam Boiler" Text="Steam Boiler" />
      </asp:DropDownList>
    </div>
    <div class="divForm">   
      <label class="label">Roof Construction:</label>    
      <asp:DropDownList ID="ddlAddRoofConstruction" CssClass="dropdown" runat="server">
        <asp:ListItem Value="-1">Optionally select one...</asp:ListItem> 
        <asp:ListItem Value="Asphalt" Text="Asphalt" />
        <asp:ListItem Value="Built-Up" Text="Built-Up" />
        <asp:ListItem Value="Concrete" Text="Concrete" />
        <asp:ListItem Value="Metal Surfacing" Text="Metal Surfacing" />
        <asp:ListItem Value="Other" Text="Other" />
        <asp:ListItem Value="Slate or Tile Shingles" Text="Slate of Tile Shingles" />
        <asp:ListItem Value="Synthetic Covering" Text="Synthetic Covering" />
        <asp:ListItem Value="Wood Shingles" Text="Wood Shingles" />
      </asp:DropDownList>
    </div>             
    <div class="divForm">   
      <label class="label">Envelope Glass Percentage (Default = 0):</label>    
      <telerik:RadNumericTextBox runat="server" ID="radNumericAddEnvelope" CssClass="textbox" Value="0"  MinValue="0" MaxValue="100" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>                                                                                                          
    </div>
    <div class="divForm">   
      <label class="label">Operating Hours Per Week (Default = 0):</label>      
      <telerik:RadNumericTextBox runat="server" ID="radNumericAddOperating" CssClass="textbox" Value="0"  MinValue="0" MaxValue="168" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>
    </div>
    <div class="divForm">   
      <label class="label">Computers (Default = 0):</label>                                     
      <telerik:RadNumericTextBox runat="server" ID="radNumericAddComputers" CssClass="textbox" Value="0"  MinValue="0" MaxValue="10000" ShowSpinButtons="true" NumberFormat-DecimalDigits="0"></telerik:RadNumericTextBox>                                                                              
    </div>
    <div class="divForm">
      <label class="label">Occupants:</label>
      <telerik:RadMaskedTextBox ID="txtAddOccupants" CssClass="textbox" runat="server" Mask="#####" PromptChar="" ValidationGroup="AddBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Location:</label>
      <textarea name="txtAddLocation" id="txtAddLocation" cols="40" rows="5" onkeyup="limitChars(this, 250, 'divAddLocationCharInfo')" runat="server" />
      <div id="divAddLocationCharInfo"></div>                                                         
    </div> 
    <div class="divForm">
      <label class="label">Description:</label>
      <textarea name="txtAddDescription" id="txtAddDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" runat="server" />
      <div id="divAddDescriptionCharInfo"></div>                                                         
    </div>

    <!--NESTED UPDATE PANEL NEEDED FOR FILE UPLOADER TRIGGERS-->    
    <asp:UpdatePanel ID="updatePanelNestedAdd" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">  
      <ContentTemplate>                                              
        <div class="divForm">
          <label class="label">Image (800x600):</label>
          <CW:FileUploader runat="server" ID="fileUploaderAdd" />
        </div>
      </ContentTemplate>
    </asp:UpdatePanel> 

    <div class="divForm">
      <label class="label">Primary Contact Name:</label>
      <asp:TextBox CssClass="textbox" ID="txtAddPrimaryContactName" runat="server" MaxLength="100" />
    </div>                                                             
    <div class="divForm">
      <label class="label">Primary Contact Email:</label>
      <asp:TextBox ID="txtAddPrimaryContactEmail" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Primary Contact Phone:</label>
      <telerik:RadMaskedTextBox ID="txtAddPrimaryContactPhone" CssClass="textbox" runat="server" Mask="(###)###-####" ValidationGroup="AddBuilding" />
    </div>
    <div class="divForm">
      <label class="label">Website Link:</label>
      <asp:TextBox ID="txtAddWebsiteLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Kiosk Content:</label>
      <textarea name="txtAddKioskContent" id="txtAddKioskContent" cols="40" rows="5" onkeyup="limitChars(this, 500, 'divAddKioskContentCharInfo')" runat="server" />
      <div id="divAddKioskContentCharInfo"></div>
    </div>
    <div class="divForm">   
      <label class="label">Weather File:</label>    
      <asp:DropDownList ID="ddlAddGlobalFile" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>  
                                                      
    <hr />
                                                    
    <h2>Add SAS Building Information</h2>
                                                    
    <div class="divForm">
      <label class="label">SAS (Space Accounting Software) ReferenceID:</label>
      <asp:TextBox ID="txtAddSASReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">SAS (Space Accounting Software) Link:</label>
      <asp:TextBox ID="txtAddSASLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div> 

    <hr />

    <h2>Add Internal Building Information</h2>
                                                    
    <div class="divForm">
      <label class="label">Internal Notes:</label>
      <textarea name="txtAddInternalNotes" id="txtAddInternalNotes" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divAddInternalNotesCharInfo')" runat="server" />
      <div id="divAddInternalNotesCharInfo"></div>                                                         
    </div>
    <div class="divForm">
      <label class="label">Setup Contact Name:</label>
      <asp:TextBox CssClass="textbox" ID="txtAddSetupContactName" runat="server" MaxLength="100" />
    </div>   
    <div class="divForm">
      <label class="label">Energy Bureau Contact Name:</label>
      <asp:TextBox CssClass="textbox" ID="txtAddEnergyBureauContactName" runat="server" MaxLength="100" />
    </div> 
                                                                                                                                                                                                      
    <asp:LinkButton CssClass="lnkButton" ID="btnAddBuilding" runat="server" Text="Add Building"  OnClick="addBuildingButton_Click" ValidationGroup="AddBuilding" />
         
  </div>
                                                
  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="addBuildingNameRFV" runat="server" ErrorMessage="Building Name is a required field." ControlToValidate="txtAddBuildingName" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addBuildingNameVCE" runat="server" BehaviorID="addBuildingNameVCE" TargetControlID="addBuildingNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addAddressRFV" runat="server" ErrorMessage="Address is a required field." ControlToValidate="txtAddAddress" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addAddressVCE" runat="server" BehaviorID="addAddressVCE" TargetControlID="addAddressRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addCityRFV" runat="server" ErrorMessage="City is a required field." ControlToValidate="txtAddCity" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addCityVCE" runat="server" BehaviorID="addCityVCE" TargetControlID="addCityRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addCountryRFV" runat="server" ErrorMessage="Country is a required field." ControlToValidate="ddlAddCountry" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addCountryVCE" runat="server" BehaviorID="addCountryVCE" TargetControlID="addCountryRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addStateRFV" runat="server" ErrorMessage="State is a required field." ControlToValidate="ddlAddState" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addStateVCE" runat="server" BehaviorID="addStateVCE" TargetControlID="addStateRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addZipRFV" runat="server" ErrorMessage="Zip Code is a required field." ControlToValidate="txtAddZip" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" Enabled="false" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addZipRequiredVCE" runat="server" BehaviorID="addZipRequiredVCE" TargetControlID="addZipRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addZipREV" runat="server" ErrorMessage="Invalid Zip Format." ValidationExpression="\d{5}" ControlToValidate="txtAddZip" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" Enabled="false" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addZipInvalidVCE" runat="server" BehaviorID="addZipInvalidVCE" TargetControlID="addZipREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                             
  <asp:RequiredFieldValidator ID="addTimeZoneRFV" runat="server" ErrorMessage="Time Zone is a required field." ControlToValidate="ddlAddTimeZone" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addTimeZoneVCE" runat="server" BehaviorID="addTimeZoneVCE" TargetControlID="addTimeZoneRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addPhoneREV" runat="server" ErrorMessage="Invalid Phone Format." ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$" ControlToValidate="txtAddPhone" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPhoneVCE" runat="server" BehaviorID="addPhoneVCE" TargetControlID="addPhoneREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addSqftRFV" runat="server" ErrorMessage="Sqft is a required field." ControlToValidate="txtAddSqft" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addSqftVCE" runat="server" BehaviorID="addSqftVCE" TargetControlID="addSqftRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:CompareValidator ID="addSqftCV" runat="server" ErrorMessage="Must be greater than 0" ControlToValidate="txtAddSqft" ValueToCompare="0" Type="Integer" Operator="GreaterThan" Display="None" SetFocusOnError="true" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addSqftCompareVCE" runat="server" BehaviorID="addSqftCompareVCE" TargetControlID="addSqftCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                     
  <asp:RegularExpressionValidator ID="addYearBuiltREV" runat="server" ErrorMessage="Invalid Year Built Format." ValidationExpression="\d{4}" ControlToValidate="txtAddYearBuilt" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addYearBuilVCE" runat="server" BehaviorID="addYearBuilVCE" TargetControlID="addYearBuiltREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                          
  <asp:RequiredFieldValidator ID="addBuildingClassRFV" runat="server" ErrorMessage="Building Class is a required field." ControlToValidate="ddlAddBuildingClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addBuildingClassVCE" runat="server" BehaviorID="addBuildingClassVCE" TargetControlID="addBuildingClassRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addBuildingTypeRFV" runat="server" ErrorMessage="Building Type is a required field." ControlToValidate="ddlAddBuildingType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addBuildingTypeVCE" runat="server" BehaviorID="addBuildingTypeVCE" TargetControlID="addBuildingTypeRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addPrimaryContactEmailREV" runat="server" ErrorMessage="Invalid Email Format." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="txtAddPrimaryContactEmail" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPrimaryContactEmailVCE" runat="server" BehaviorID="addPrimaryContactEmailVCE" TargetControlID="addPrimaryContactEmailREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addPrimaryContactPhoneREV" runat="server" ErrorMessage="Invalid Phone Format." ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$" ControlToValidate="txtAddPrimaryContactPhone" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPrimaryContactPhoneVCE" runat="server" BehaviorID="addPrimaryContactPhoneVCE" TargetControlID="addPrimaryContactPhoneREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addWebsiteLinkREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtAddWebsiteLink" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addWebsiteLinkVCE" runat="server" BehaviorID="addWebsiteLinkVCE" TargetControlID="addWebsiteLinkREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addSASLinkREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtAddSASLink" SetFocusOnError="true" Display="None" ValidationGroup="AddBuilding" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addSASLinkVCE" runat="server" BehaviorID="addSASLinkVCE" TargetControlID="addSASLinkREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>   