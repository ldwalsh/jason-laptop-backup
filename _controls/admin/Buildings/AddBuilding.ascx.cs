﻿using CW.Business.Blob.Images;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website.DependencyResolution;
using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Buildings
{
    public partial class AddBuilding : AdminUserControlBase
    {
        #region constants

            const String addBuildingSuccess = " building addition was successful.";
            const String addBuildingFailed = "Adding building failed. Please contact an administrator.";
            const String addBuildingError = "Error adding building.";
            const String buildingExists = "A building with that name already exists.";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAddError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindCountries(ddlAddCountry);
                    BindStates(ddlAddState, ddlAddCountry.SelectedValue);
                    BindTimeZones(ddlAddTimeZone);
                    BindBuildingClasses(ddlAddBuildingClass);
                    BindBuildingTypes(ddlAddBuildingType, -1);
                    BindWeatherFiles(ddlAddGlobalFile);
                }
            }

            #region ddl events

                protected void ddlAddCountry_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindStates(ddlAddState, ddlAddCountry.SelectedValue);
                    ToggleStateAndZipValidators(ddlAddCountry.SelectedValue);
                }

                protected void ddlAddBuildingClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindBuildingTypes(ddlAddBuildingType, Convert.ToInt32(ddlAddBuildingClass.SelectedValue));
                }

            #endregion

            #region button events

                protected void addBuildingButton_Click(Object sender, EventArgs e)
                {
                    if (DataMgr.BuildingDataMapper.DoesBuildingExistForClient(siteUser.CID, null, txtAddBuildingName.Text))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, buildingExists, lnkSetFocus);
                        return;
                    }
                    else
                    {
                        var mBuilding = new Building();
                        var mBuildingSettings = new BuildingSetting();
                        
                        LoadAddFormIntoBuilding(mBuilding);

                        var mCountry = DataMgr.CountryDataMapper.GetCountryByAlpha2Code(mBuilding.CountryAlpha2Code);

                        //other default values are in the db as default values or bindings.
                        mBuildingSettings.LCID = mCountry.DefaultLCID;
                        mBuildingSettings.UnitSystem = mCountry.DefaultUnitSystem;
                        mBuildingSettings.AutomatedPriorityDailyEmailsUserGroupsGUID = Guid.NewGuid();
                        mBuildingSettings.AutomatedPriorityMonthlyEmailsUserGroupsGUID = Guid.NewGuid();
                        mBuildingSettings.AutomatedPriorityWeeklyEmailsUserGroupsGUID = Guid.NewGuid();
                        mBuildingSettings.SubscriptionStartDate = DateTime.UtcNow;
                        mBuildingSettings.SubscriptionPlan = BusinessConstants.BuildingSettings.DefaultSubscriptionPlan;
                        mBuildingSettings.MaxPointLimit = 0;

                        try
                        {
                            mBuildingSettings.BID = DataMgr.BuildingDataMapper.InsertBuildingAndReturnBID(mBuilding);
                            DataMgr.BuildingDataMapper.InsertBuildingSettings(mBuildingSettings);

							if (fileUploaderAdd.File != null) new BuildingProfileImage(siteUser.CID, DataMgr.OrgBasedBlobStorageProvider, fileUploaderAdd.File.Extension, new BuildingProfileImage.Args(mBuilding.BID)).Insert(fileUploaderAdd.File.Content);

                            var coordinates = IoC.Resolve<BingMapsHelper>().GetGeocodeData(CreateAddressString(mBuilding, ddlAddState));

                            if (coordinates != null)
                            {
                                mBuilding.Latitude = coordinates[0];
                                mBuilding.Longitude = coordinates[1];
                            }

                            LabelHelper.SetLabelMessage(lblResults, mBuilding.BuildingName + addBuildingSuccess, lnkSetFocus);
                            OperationComplete(KGSBuildingAdministration.TabMessages.AddBuilding);
                        }
                        catch (SqlException sqlEx)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, addBuildingError, sqlEx);
                            LabelHelper.SetLabelMessage(lblAddError, addBuildingFailed, lnkSetFocus);
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, addBuildingError, ex);
                            LabelHelper.SetLabelMessage(lblAddError, addBuildingFailed, lnkSetFocus);
                        }
                    }   
                }

            #endregion

        #endregion

        #region methods

            private void LoadAddFormIntoBuilding(Building building)
            {
                building.BuildingName = txtAddBuildingName.Text;
                building.CID = siteUser.CID;
                building.Address = txtAddAddress.Text;
                building.City = txtAddCity.Text;
                building.CountryAlpha2Code = ddlAddCountry.SelectedValue;
                building.StateID = ddlAddState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddState.SelectedValue) : null;
                building.Zip = String.IsNullOrEmpty(txtAddZip.Text) ? null : txtAddZip.Text;

                //TODO: Latitude/Longitude service lookup

                building.TimeZone = ddlAddTimeZone.SelectedValue;
                building.Phone = String.IsNullOrEmpty(txtAddPhone.Text) ? null : txtAddPhone.Text;
                building.Sqft = Convert.ToInt32(txtAddSqft.Text);
                building.Floors = String.IsNullOrEmpty(txtAddFloors.Text) ? null : (int?)Convert.ToInt32(txtAddFloors.Text);
                building.YearBuilt = String.IsNullOrEmpty(txtAddYearBuilt.Text) ? null : (int?)Convert.ToInt32(txtAddYearBuilt.Text);
                building.BuildingTypeID = Convert.ToInt32(ddlAddBuildingType.SelectedValue);
                building.BuildingOwnership = ddlAddBuildingOwnership.SelectedValue != "-1" ? ddlAddBuildingOwnership.SelectedValue : null;
                building.BuildingShape = ddlAddBuildingShape.SelectedValue != "-1" ? ddlAddBuildingShape.SelectedValue : null;
                building.MainCoolingSystem = ddlAddMainCoolingSystem.SelectedValue != "-1" ? ddlAddMainCoolingSystem.SelectedValue : null;
                building.MainHeatingSystem = ddlAddMainHeatingSystem.SelectedValue != "-1" ? ddlAddMainHeatingSystem.SelectedValue : null;
                building.RoofConstruction = ddlAddRoofConstruction.SelectedValue != "-1" ? ddlAddRoofConstruction.SelectedValue : null;
                building.EnvelopeGlassPercentage = !String.IsNullOrEmpty(radNumericAddEnvelope.Text) ? Convert.ToInt32(radNumericAddEnvelope.Text) : 0;
                building.OperatingHours = !String.IsNullOrEmpty(radNumericAddOperating.Text) ? Convert.ToInt32(radNumericAddOperating.Text) : 0;
                building.Occupants = String.IsNullOrEmpty(txtAddOccupants.Text) ? null : (int?)Convert.ToInt32(txtAddOccupants.Text);
                building.Computers = !String.IsNullOrEmpty(radNumericAddComputers.Text) ? Convert.ToInt32(radNumericAddComputers.Text) : 0;
                building.Location = String.IsNullOrEmpty(txtAddLocation.Value) ? null : txtAddLocation.Value;
                building.Description = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                building.ImageExtension = (fileUploaderAdd.File == null) ? String.Empty : fileUploaderAdd.File.Extension;
                building.PrimaryContactName = String.IsNullOrEmpty(txtAddPrimaryContactName.Text) ? null : txtAddPrimaryContactName.Text;
                building.PrimaryContactEmail = String.IsNullOrEmpty(txtAddPrimaryContactEmail.Text) ? null : txtAddPrimaryContactEmail.Text;
                building.PrimaryContactPhone = String.IsNullOrEmpty(txtAddPrimaryContactPhone.Text) ? null : txtAddPrimaryContactPhone.Text;
                building.WebsiteLink = String.IsNullOrEmpty(txtAddWebsiteLink.Text) ? null : txtAddWebsiteLink.Text;
                building.KioskContent = String.IsNullOrEmpty(txtAddKioskContent.Value) ? null : txtAddKioskContent.Value;
                building.WeatherGFID = (ddlAddGlobalFile.SelectedValue != "-1") ? Convert.ToInt32(ddlAddGlobalFile.SelectedValue) : (int?)null;
                building.IsActive = true;
                building.IsVisible = true;
                building.DateModified = DateTime.UtcNow;

                //SAS Information
                building.SASReferenceID = String.IsNullOrEmpty(txtAddSASReferenceID.Text) ? null : txtAddSASReferenceID.Text;
                building.SASLink = String.IsNullOrEmpty(txtAddSASLink.Text) ? null : txtAddSASLink.Text;

                //Internal Information
                building.InternalNotes = String.IsNullOrEmpty(txtAddInternalNotes.Value) ? null : txtAddInternalNotes.Value;
                building.SetupContactName = String.IsNullOrEmpty(txtAddSetupContactName.Text) ? null : txtAddSetupContactName.Text;
                building.EnergyBureauContactName = String.IsNullOrEmpty(txtAddEnergyBureauContactName.Text) ? null : txtAddEnergyBureauContactName.Text;
            }

            private void ToggleStateAndZipValidators(String value)
            {
                addZipRFV.Enabled = addZipREV.Enabled = (value == BusinessConstants.Countries.UnitedStatesAlpha2Code);
                txtAddZip.Mask = (value == BusinessConstants.Countries.UnitedStatesAlpha2Code) ? "#####" : "aaaaaaaaaa";
                addStateRFV.Enabled = (ddlAddState.Items.Count > 2);
            }

            private String CreateAddressString(Building building, DropDownList ddl)
            {
                return String.Join(",", new[] { building.Address,
						                        building.City,
						                        ((building.StateID == null) ? String.Empty : ddl.Items.FindByValue(building.StateID.Value.ToString()).Text),
						                        building.Zip,
						                        building.CountryAlpha2Code }
                                  );
            }

            #region binders

                private void BindCountries(DropDownList ddl)
                {
                    BindDDL(ddl, "CountryName", "Alpha2Code", DataMgr.CountryDataMapper.GetAllCountries(), true);
                }

                private void BindStates(DropDownList ddl, String alpha2Code)
                {
                    var lItem = new ListItem() { Text = (alpha2Code != "-1") ? "Select one..." : "Select country first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "StateName", "StateID", (alpha2Code != "-1") ? DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code) : null, true, lItem);
                }

                private void BindTimeZones(DropDownList ddl)
                {
                    BindDDL(ddl, "DisplayName", "Id", TimeZoneInfo.GetSystemTimeZones(), true);
                }

                private void BindBuildingClasses(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingClassName", "BuildingClassID", DataMgr.BuildingClassDataMapper.GetAllBuildingClasses(), true);
                }

                private void BindBuildingTypes(DropDownList ddl, Int32 buildingClassID)
                {
                    var lItem = new ListItem() { Text = (buildingClassID != -1) ? "Select one..." : "Select class first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "BuildingTypeName", "BuildingTypeID", (buildingClassID != -1) ? DataMgr.BuildingTypeDataMapper.GetAllBuildingTypesByBuildingClassID(buildingClassID) : null, true, lItem);
                }

                private void BindWeatherFiles(DropDownList ddl)
                {
                    var lItem = new ListItem() { Text = "Optionally select one...", Value = "-1", Selected = true };
                    var dataSource = DataMgr.GlobalFileDataMapper.GetAllGlobalFileItems<GlobalFilesLookup>((gfl => gfl), (gfl => gfl.GlobalFileTypeID == BusinessConstants.GlobalFileType.WeatherFileTypeID), (gfl => gfl.DisplayName));

                    BindDDL(ddl, "DisplayName", "GFID", dataSource, true, lItem);
                }

            #endregion

        #endregion
    }
}