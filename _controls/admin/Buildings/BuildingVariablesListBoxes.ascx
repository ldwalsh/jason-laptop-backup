﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BuildingVariablesListBoxes.ascx.cs" Inherits="CW.Website._controls.admin.Buildings.BuildingVariablesListBoxes" %>

<hr />

<p>Building variables are available based on the assigned variables to the building class.</p>

<div class="divForm">
  <label class="label">Available:</label>
  <asp:ListBox ID="lbBuildingVariablesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

  <div class="divArrows">
    <asp:ImageButton ID="btnUp" CausesValidation="false" OnClick="btnUp_ButtonClick" ImageUrl="~/_assets/images/up-arrow.png" runat="server" CssClass="up-arrow" />
    <asp:ImageButton ID="btnDown" CausesValidation="false" OnClick="btnDown_ButtonClick" ImageUrl="~/_assets/images/down-arrow.png" runat="server" CssClass="down-arrow" />
  </div>

<label class="label">Assigned:</label>
  <asp:ListBox ID="lbBuildingVariablesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
</div>

<asp:LinkButton ID="btnReassignBuildingVariables" runat="server" Text="Reassign" OnClick="btnReassignBuildingVariables_ButtonClick" CssClass="lnkButton" ValidationGroup="reassignBuildingVariables" />