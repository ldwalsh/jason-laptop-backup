﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Linq;
using System.Text;
using CW.Data.Helpers;
using CW.Utility.Web;

namespace CW.Website._controls.admin.Buildings
{
    public partial class BuildingVariablesToMultipleBuildings : AdminUserControlBase
    {
        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[] 
                        {
                            KGSBuildingAdministration.TabMessages.AddBuilding,
                            KGSBuildingAdministration.TabMessages.EditBuilding,
                            KGSBuildingAdministration.TabMessages.DeleteBuilding
                        };
                    }
                }

            #endregion

        #endregion

        #region constants

            const String assignBuildingVariableNoBuildingsSelected = "No building was selected.";
            const String buildingVariableCannotBeAssigned = "{0} cannot be assigned to {1}.";
            const String buildingVariableHasBeenUpdated = "The value for {0} and building {1} has been updated to {2}.";
            const String buildingVariableHasBeenAssigned = "{0} has been assigned to {1}.";
            const String buildingVariableAlreadyAssigned = "{0} is already assigned to {1}.";
            const String assignBuildingVariableFailed = "Building variable assignment to multiple buildings failed. Please contact an administrator.";
            const String thereAreNoBuildingsWithUnitSystem = "There are no buildings available that have been configured with the {0} Unit System.";
            const String buildingVariableCannotBeAssignedValidationFailure = "The Building variable {0} could not be assigned to following buildings for the reason {1}:";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblVariablesError.Visible = false;

                if (!Page.IsPostBack) BindBuildingVariables(ddlBuildingVariables);
            }

            #region ddl events

                protected void ddlSettingsUnitSystem_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetupBuildings();
                }

                protected void ddlBuildingVariables_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetupBuildings();
                }

                private void SetupBuildings()
                {
                    ClearBuildingBoxes();
                    IEnumerable<Building> buildings = siteUser.VisibleBuildings.Where(bldg => bldg.BuildingSettings.Single().UnitSystem == ("1" == ddlSettingsUnitSystem.SelectedValue));
                    if (0 == buildings.Count())
                    {
                        LabelHelper.SetLabelMessage(lblVariablesError, string.Format(thereAreNoBuildingsWithUnitSystem, ddlSettingsUnitSystem.SelectedItem.Text), lnkSetFocus);
                        txtVariablesValue.Text = spanEngUnits.InnerText = info.InnerHtml = String.Empty;
                        // Disable Value input box
                        txtVariablesValue.Enabled = false;
                        // Disable Building Variables dropdown
                        ddlBuildingVariables.SelectedIndex = -1;
                        ddlBuildingVariables.Enabled = false;
                        // Disable Assign button
                        btnAssignBuildingVariableToMultipleBuildings.Enabled = false;
                    }
                    else
                    {
                        txtVariablesValue.Enabled = true; // Re-enable
                        lblVariablesError.Text = "";
                        lblVariablesError.Visible = false;
                        ddlBuildingVariables.Enabled = true; // Re-enable
                        btnAssignBuildingVariableToMultipleBuildings.Enabled = true; // Re-enable
                        var mBuildingVariable = DataMgr.BuildingVariableDataMapper.GetFullBuildingVariableByBVID(Convert.ToInt32(ddlBuildingVariables.SelectedValue));

                        if (mBuildingVariable != null && mBuildingVariable.BVID != 0)
                        {
                            bool IsBuildingUnitSystemSI = "1" == ddlSettingsUnitSystem.SelectedValue;
                            int engUnitID = !IsBuildingUnitSystemSI ? mBuildingVariable.IPEngUnitID : mBuildingVariable.SIEngUnitID;
                            BindBuildingListBox(lbBuildingsTop, buildings);
                            if (0 == lbBuildingsTop.Items.Count)
                            {
                                txtVariablesValue.Text = spanEngUnits.InnerText = info.InnerHtml = String.Empty;
                            }
                            else
                            {
                                spanEngUnits.InnerText = !IsBuildingUnitSystemSI ? mBuildingVariable.IPEngUnits : mBuildingVariable.SIEngUnits;
                                txtVariablesValue.Text = IsBuildingUnitSystemSI ? mBuildingVariable.SIDefaultValue : mBuildingVariable.IPDefaultValue;  // Already in culture format
                                info.InnerHtml = BaseVariable.Info(IsBuildingUnitSystemSI, engUnitID, txtVariablesValue.Text);
                            }
                            ControlHelper.SetTextBoxSingleMultiple(engUnitID, txtVariablesValue);
                        }
                        else
                        {
                            txtVariablesValue.Text = spanEngUnits.InnerText = info.InnerHtml = String.Empty;
                        }
                    }
                }

            #endregion

            #region button events

                protected void btnBuildingsUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbBuildingsBottom.SelectedIndex != -1)
                    {
                        lbBuildingsTop.Items.Add(lbBuildingsBottom.SelectedItem);
                        lbBuildingsBottom.Items.Remove(lbBuildingsBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBuildingsTop);
                }

                protected void btnBuildingsDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbBuildingsTop.SelectedIndex != -1)
                    {
                        lbBuildingsBottom.Items.Add(lbBuildingsTop.SelectedItem);
                        lbBuildingsTop.Items.Remove(lbBuildingsTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBuildingsBottom);
                }

                protected void assignBuildingVariableToMultipleBuildings_Click(Object sender, EventArgs e)
                {
                    try
                    {
                        if (lbBuildingsBottom.Items.Count == 0)
                        {
                            LabelHelper.SetLabelMessage(lblVariablesError, assignBuildingVariableNoBuildingsSelected, lnkSetFocus);
                            return;
                        }
                        else
                        {
                            var mBvid = Convert.ToInt32(ddlBuildingVariables.SelectedValue);
                            var mBuildingVariableName = ddlBuildingVariables.SelectedItem.Text;
                            

                            lblVariablesError.Text = lblResults.Text = String.Empty;

                            string validationFailures = "";
                            foreach (ListItem item in lbBuildingsBottom.Items)
                            {
                                var mBuildingVariable = new Buildings_BuildingVariable();
                                var mBid = Convert.ToInt32(item.Value);
                                var mBuildingName = item.Text;

                                mBuildingVariable.BID = mBid;
                                mBuildingVariable.BVID = mBvid;
                                mBuildingVariable.Value = txtVariablesValue.Text;
                                mBuildingVariable.DateModified = DateTime.UtcNow;
                                mBuildingVariable.EngUnitID = null;

                                try
                                {
                                    if (!DataMgr.BuildingVariableDataMapper.IsBuildingVariableAssociatedToBuilding(mBvid))
                                    {
                                        lblVariablesError.Text += String.Format(buildingVariableCannotBeAssigned, mBuildingVariableName, mBuildingName) + "<br />";
                                    }
                                    else if (chkVariablesOverride.Checked)
                                    {
                                        var buildingVariableToUpdate = DataMgr.BuildingVariableDataMapper.GetBuildingVariableByBIDAndBVID(mBid, mBvid);

                                        if (buildingVariableToUpdate == null)
                                        {
                                            DataMgr.BuildingVariableDataMapper.InsertBuildingBuildingVariable(mBuildingVariable);
                                            lblResults.Text += String.Format(buildingVariableHasBeenAssigned, mBuildingVariableName, mBuildingName) + "<br />";
                                        }
                                        else
                                        {
                                            var value = txtVariablesValue.Text;

                                            DataMgr.BuildingVariableDataMapper.UpdatePartialBuildingVariable(buildingVariableToUpdate.ID, value);
                                            lblResults.Text += String.Format(buildingVariableHasBeenUpdated, mBuildingVariableName, mBuildingName, value) + "<br />";
                                        }
                                    }
                                    else if (DataMgr.BuildingVariableDataMapper.DoesBuildingVariableExistForBuilding(mBid, mBvid))
                                    {
                                        lblVariablesError.Text += String.Format(buildingVariableAlreadyAssigned, mBuildingVariableName, mBuildingName) + "<br />";
                                    }
                                    else
                                    {
                                        DataMgr.BuildingVariableDataMapper.InsertBuildingBuildingVariable(mBuildingVariable);
                                        lblResults.Text += String.Format(buildingVariableHasBeenAssigned, mBuildingVariableName, mBuildingName) + "<br />";
                                    }
                                }
                                catch (CW.Data.Exceptions.ValidationException ex)
                                {
                                    if( 0 == validationFailures.Length)
                                    {
                                        validationFailures = string.Format(buildingVariableCannotBeAssignedValidationFailure, mBuildingVariableName, ex.Message);
                                    }
                                    validationFailures += "<li>" + mBuildingName + "</li>";
                                }
                            }

                            lbBuildingsBottom.SelectedIndex = -1;
                            if( validationFailures.Length > 0)
                            {
                                validationFailures = "<ul>" + validationFailures + "</ul>";
                                if(lblVariablesError.Text != String.Empty)
                                {
                                    validationFailures = "<p>" + validationFailures + "</p>" + lblVariablesError.Text;
                                }
                                LabelHelper.SetLabelMessage(lblVariablesError, validationFailures, lnkSetFocus);
                            }
                            else if (lblVariablesError.Text != String.Empty) LabelHelper.SetLabelMessage(lblVariablesError, lblVariablesError.Text, lnkSetFocus);
                            if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblVariablesError, assignBuildingVariableFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning building variable to multiple buildings.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblVariablesError, assignBuildingVariableFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning building variable to multiple buildings.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    var adminUserControlForm = new AdminUserControlForm(this);

                    adminUserControlForm.ClearTextBoxes();
                    adminUserControlForm.ClearCheckBoxes();
                    adminUserControlForm.ResetDropDownLists();

                    //hardset any fields to their initial values
                    spanEngUnits.InnerText = String.Empty;
                    ddlBuildingVariables_OnSelectedIndexChanged(null, null);
                }

            #endregion

            private void ClearBuildingBoxes()
            {
                lbBuildingsTop.Items.Clear();
                lbBuildingsBottom.Items.Clear();
            }

            #region binders

                private void BindBuildingVariables(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingVariableDisplayName", "BVID", DataMgr.BuildingVariableDataMapper.GetAllBuildingVariables(), true);
                }

                private void BindBuildingListBox(ListBox lb, IEnumerable<Building> buildings)
                {
                    lb.Items.Clear();
                    ControlHelper.BindListBox(lb, "BuildingName", "BID", buildings);
                }

            #endregion

        #endregion
    }
}