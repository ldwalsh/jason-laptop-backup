﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.BuildingVariable;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Buildings
{
    public partial class BuildingVariablesListBoxes : SiteUserControl
    {
        #region constants

            const String analysisRequiresBuildingVariable = "Building variable {0} could not be unassigned from equipment {0} because an associated analysis requires it.";
            const String unassignBuildingVariableUpdateSuccessful = "Unassignment of building variable {0} to building {1} was successful.";
            const String assignBuildingVariableUpdateSuccessful = "Assignment of building variable {0} to building {1} was successful.";
            const String reassignUpdateFailed = "Building variables reassignment failed. Please contact an administrator.";
            const String noItemsSelected = "No items were selected for reassignment.";
            const Char delimeter = Common.Constants.DataConstants.DefaultDelimiter;

        #endregion

        #region fields

            private enum Operation { Assign, Unassign };

        #endregion

        #region properties

            public Int32 BID { get; set; }
            public String BuildingName { get; set; }
            public Boolean HasListBoxItemChanged { get; set; }
            public IEnumerable<GetBuildingBuildingVariableData> BuildingVariablesForRepeater { get; set; }
            public Label LabelResults { get; set; }
            public Label LabelVariablesError { get; set; }
            public HtmlAnchor LinkSetFocus { get; set; }

        #endregion

        #region events

            #region button events

                protected void btnUp_ButtonClick(Object sender, EventArgs e)
                {
                    while (lbBuildingVariablesBottom.SelectedIndex != -1)
                    {
                        lbBuildingVariablesTop.Items.Add(lbBuildingVariablesBottom.SelectedItem);
                        lbBuildingVariablesBottom.Items.Remove(lbBuildingVariablesBottom.SelectedItem);
                    }

                    GetUpdatedBuildingVariables();

                    ControlHelper.SortListBox(lbBuildingVariablesTop);
                }

                protected void btnDown_ButtonClick(Object sender, EventArgs e)
                {
                    while (lbBuildingVariablesTop.SelectedIndex != -1)
                    {
                        lbBuildingVariablesBottom.Items.Add(lbBuildingVariablesTop.SelectedItem);
                        lbBuildingVariablesTop.Items.Remove(lbBuildingVariablesTop.SelectedItem);
                    }

                    GetUpdatedBuildingVariables();

                    ControlHelper.SortListBox(lbBuildingVariablesBottom);
                }

                protected void btnReassignBuildingVariables_ButtonClick(Object sender, EventArgs e)
                {
                    var bcid = DataMgr.BuildingClassDataMapper.GetBuildingClassIDByBID(BID);
                    var buildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariables();
                    var selector = new Func<ListItem, Buildings_BuildingVariable>(li => new Buildings_BuildingVariable { BID = BID, BVID = Convert.ToInt32(li.Value.Split(delimeter)[0]) });

                    try
                    {
                        LabelResults.Text = LabelVariablesError.Text = String.Empty;

                        UnassignBuildingVariables(selector, buildingVariables);
                        AssignBuildingVariables(selector, buildingVariables);

                        if (LabelResults.Text == String.Empty && LabelVariablesError.Text == String.Empty)
                        {
                            LabelHelper.SetLabelMessage(LabelVariablesError, noItemsSelected, LinkSetFocus);
                            return;
                        }

                        GetUpdatedBuildingVariables();

                        if (LabelVariablesError.Text != String.Empty) LabelHelper.SetLabelMessage(LabelVariablesError, LabelVariablesError.Text, LinkSetFocus);
                        if (LabelResults.Text != String.Empty)
                        {
                            BuildingStats.CreateCacheKey(siteUser.CID, siteUser.UID); //clear cache if there was a successful addition or removal of building variable(s).
                            ((AdminSitePageTemp)Page).TabStateMonitor.ChangeState(KGSBuildingAdministration.TabMessages.EditBuildingVariables);
                            LabelHelper.SetLabelMessage(LabelResults, LabelResults.Text, LinkSetFocus);
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning building vars to building.", sqlEx);
                        LabelHelper.SetLabelMessage(LabelVariablesError, reassignUpdateFailed, LinkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning building vars to building.", ex);
                        LabelHelper.SetLabelMessage(LabelVariablesError, reassignUpdateFailed, LinkSetFocus);
                    }
            }

            #endregion

        #endregion

        #region methods

            public void BindBuildingVariablesToListBoxes()
            {
                lbBuildingVariablesTop.Items.Clear();
                lbBuildingVariablesBottom.Items.Clear();

                var equipmntClassID = DataMgr.BuildingClassDataMapper.GetBuildingClassIDByBID(BID);
                var mBuildingVariablesNotAssociated = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesNotAssociatedToBID(BID, equipmntClassID);
                var mBuildingVariablesAssociated = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesAssociatedToBID(BID, equipmntClassID);
                var buildingVariableName = PropHelper.G<BuildingVariable>(bv => bv.BuildingVariableName);
                var bvid = PropHelper.G<BuildingVariable>(bv => bv.BVID);

                if (mBuildingVariablesNotAssociated.Any()) ControlHelper.SetListBoxData(lbBuildingVariablesTop, mBuildingVariablesNotAssociated, buildingVariableName, bvid, false);
                if (mBuildingVariablesAssociated.Any()) ControlHelper.SetListBoxData(lbBuildingVariablesBottom, mBuildingVariablesAssociated, buildingVariableName, bvid, true);
            }

            private void GetUpdatedBuildingVariables()
            {
                var items = lbBuildingVariablesBottom.Items;

                if (items.Count > 0)
                {
                    var selector = new Func<ListItem, GetBuildingBuildingVariableData>(li => new GetBuildingBuildingVariableData { BVID = Convert.ToInt32(li.Value.Split(delimeter)[0]) });
                    var lbBuildingVariables = items.Cast<ListItem>().Select(selector).ToList();

                    GetUpdatedBuildingVariablesForRepeater(lbBuildingVariables);
                }

                HasListBoxItemChanged = true; //set this to true, even if there is nothing in the bottom list. this is due to if you remove the last item, you want to ensure the corresponding repeater item is removed.
            }

            private void GetUpdatedBuildingVariablesForRepeater(IEnumerable<GetBuildingBuildingVariableData> buildingVariables)
            {
                var updatedBuildingVariables = new List<GetBuildingBuildingVariableData>();

                foreach (var buildingVariable in buildingVariables)
                {
                    var updateBuildingVariable = DataMgr.BuildingVariableDataMapper.GetBuildingVariableWithDataByBIDAndBVID(BID, buildingVariable.BVID);

                    if (updateBuildingVariable != null) updatedBuildingVariables.Add(updateBuildingVariable);
                }

                if (updatedBuildingVariables.Any()) BuildingVariablesForRepeater = updatedBuildingVariables.OrderBy(ubv => ubv.BuildingVariableDisplayName).ToList();
            }

            private void UnassignBuildingVariables(Func<ListItem, Buildings_BuildingVariable> selector, IEnumerable<BuildingVariable> buildingVariables)
            {
                GetItemsAndAssignOrUnassign(lbBuildingVariablesTop, selector, buildingVariables, true, Operation.Unassign);
            }

            private void AssignBuildingVariables(Func<ListItem, Buildings_BuildingVariable> selector, IEnumerable<BuildingVariable> buildingVariables)
            {
                GetItemsAndAssignOrUnassign(lbBuildingVariablesBottom, selector, buildingVariables, false, Operation.Assign, true);
            }

            private void GetItemsAndAssignOrUnassign(ListBox lb, Func<ListItem, Buildings_BuildingVariable> selector, IEnumerable<BuildingVariable> buildingVariables, Boolean isUnassigned, Operation operation, Boolean unitSystem = false)
            {
                if (lb.Items.Count > 0)
                {
                    if (unitSystem) unitSystem = DataMgr.BuildingDataMapper.GetBuildingSettingsByBID(BID).UnitSystem;

                    var items = lb.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, isUnassigned)).Select(selector).ToList();

                    if (items.Any())
                    {
                        foreach (var item in items)
                        {
                            var bvid = Convert.ToInt32(item.BVID);
                            var lItem = lb.Items.Cast<ListItem>().Single(li => Convert.ToInt32(li.Value.Split(delimeter)[0]) == bvid);
                            var buildingVariableDisplayName = buildingVariables.Single(bv => bv.BVID == bvid).BuildingVariableDisplayName;

                            lItem.Value = lItem.Value.Contains("True") ? lItem.Value.Replace("True", "False") : lItem.Value.Replace("False", "True");

                            if (operation == Operation.Unassign) { UnassignBuildingVariable(bvid, buildingVariableDisplayName); continue; }
                            if (operation == Operation.Assign) { AssignBuildingVariable(bvid, buildingVariableDisplayName, unitSystem); continue; }
                        }
                    }
                }
            }

            private void UnassignBuildingVariable(Int32 bvid, String buildingVariableDisplayName)
            {
                DataMgr.BuildingVariableDataMapper.DeleteBuildingVariableByBIDAndBVID(BID, bvid);
                LabelHelper.SetLabelMessage(LabelResults, String.Format(unassignBuildingVariableUpdateSuccessful, buildingVariableDisplayName, BuildingName) + "<br />", LinkSetFocus, true);
            }

            private void AssignBuildingVariable(Int32 bvid, String buildingVariableDisplayName, Boolean unitSystem)
            {
                if (!DataMgr.BuildingVariableDataMapper.DoesBuildingVariableExistForBuilding(BID, bvid))
                {
                    var buildingExpressions = unitSystem ? new Expression<Func<BuildingVariable, Object>>[] { _ => _.EngUnit1 } : new Expression<Func<BuildingVariable, Object>>[] { _ => _.EngUnit };
                    var bv = DataMgr.BuildingVariableDataMapper.GetBuildingVariable(bvid, buildingExpressions);
                    var buildingBuildingVariable = new Buildings_BuildingVariable()
                    {
                        BID = BID,
                        BVID = bvid,
                        Value = BaseVariable.GenerateCultureFormattedValue(unitSystem ? bv.SIDefaultValue : bv.IPDefaultValue, (unitSystem ? bv.EngUnit1 : bv.EngUnit).EngUnitID),
                        DateModified = DateTime.UtcNow
                    };

                    DataMgr.BuildingVariableDataMapper.InsertBuildingBuildingVariable(buildingBuildingVariable);
                    LabelHelper.SetLabelMessage(LabelResults, String.Format(assignBuildingVariableUpdateSuccessful, buildingVariableDisplayName, BuildingName) + "<br />", LinkSetFocus, true);
                }
            }

        #endregion
    }
}