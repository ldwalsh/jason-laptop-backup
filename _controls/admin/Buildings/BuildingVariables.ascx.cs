﻿using CW.Common.Constants;
using CW.Data.Models.BuildingVariable;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Buildings
{
    public partial class BuildingVariables : AdminUserControlBase
    {
        //TODO: Update this control and the EquipmentVariables control to remove and centralize duplicated fields, properties and methods.

        #region constants

            const String partiallySuccessfulFmt = "{0} out of {1} Building variables were updated successfully. Please see the failure(s) below.";
            const String variablesUpdateSuccessful = "Building variables update was successful.";
            const String variablesUpdateFailed = "Building variables update failed. Please contact an administrator.";

        #endregion

        #region fields

            public enum AdminModeEnum { NONKGS, KGS, PROVIDER };

        #endregion

        #region properites

            public static AdminModeEnum AdminMode { get; set; }

            public override IEnumerable<Enum> ReactToChangeStateList
            {
                get
                {
                    return new Enum[]
                    {
                        KGSBuildingAdministration.TabMessages.AddBuilding,
                        KGSBuildingAdministration.TabMessages.EditBuilding,
                        KGSBuildingAdministration.TabMessages.DeleteBuilding
                    };
                }
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                tabHeader.Title = IsKGSOrProvider() ? "Building Variables To A Building" : "Buildings Variables";
                clientName.Visible = IsKGSOrProvider();
                BindBuildings(ddlBuilding);

                var adminDropDowns = new AdminDropDowns(RadTabStrip, RadMultiPage, lblResults);
                var currentControl = typeof(BuildingVariables).Name;

                adminDropDowns.SetBuildingDropDownListAndTabIndex(ddlBuilding, currentControl);
                ddlBuilding_SelectedIndexChanged(null, null);

                lblUpdateBuildingVariables.Text = IsKGSOrProvider() ? "Associated Variables" : "Editable Variables";
            }

            private void Page_Load()
            {
                lblResults.Visible = lblVariablesError.Visible = false;

                tabHeader.SubTitle = IsKGSOrProvider() ? "Please select a building in order to select and assign building variables." : "Please select a building in order to view and edit building variables.";

                //set the properties on each post back, otherwise values are lost when referenced in each control
                BuildingVariablesList.BID = (!String.IsNullOrWhiteSpace(ddlBuilding.SelectedValue)) ? Convert.ToInt32(ddlBuilding.SelectedValue) : -1;
                BuildingVariablesListBoxes.BID = (!String.IsNullOrWhiteSpace(ddlBuilding.SelectedValue)) ? Convert.ToInt32(ddlBuilding.SelectedValue) : -1;
                BuildingVariablesListBoxes.BuildingName = (ddlBuilding.SelectedItem != null) ? ddlBuilding.SelectedItem.Text : String.Empty;
                BuildingVariablesListBoxes.LabelResults = lblResults;
                BuildingVariablesListBoxes.LabelVariablesError = lblVariablesError;
                BuildingVariablesListBoxes.LinkSetFocus = lnkSetFocus;
            }

            private void Page_PreRender()
            {
                if (Page.IsPostBack)
                {
                    if (IsKGSOrProvider())
                    {
                        if (BuildingVariablesListBoxes.HasListBoxItemChanged) BindRepeater(BuildingVariablesListBoxes.BuildingVariablesForRepeater);
                    }
                }
            }

            #region ddl events

                protected void ddlBuilding_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    HideBuildingVariableContainers();

                    var bid = Convert.ToInt32(ddlBuilding.SelectedValue);

                    if (bid != -1)
                    {
                        if (ContainsQueryString()) BuildingVariablesListBoxes.BID = Convert.ToInt32(ddlBuilding.SelectedValue);

                        var buildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesWithDataByBID(bid);

                        BuildingVariablesListBoxes.Visible = IsKGSOrProvider();
                        BuildingVariablesList.Visible = !IsKGSOrProvider();

                        if (IsKGSOrProvider())
                            BuildingVariablesListBoxes.BindBuildingVariablesToListBoxes();
                        
                        else
                            BuildingVariablesList.BindBuildingVariables(buildingVariables);

                        if (buildingVariables.Any())
                        {
                          if(!IsKGSOrProvider()) 
                              buildingVariables = buildingVariables.Where(bv => bv.UserEditable);
                            
                          BindRepeater(buildingVariables);
                        } 
                    }
                }

            #endregion

            #region button events

                protected void btnUpdateBuildingVariables_ButtonClick(Object sender, EventArgs e)
                {
                    try
                    {
                        int numToProcess = 0;
                        int numSucessful = 0;
                        foreach (RepeaterItem rptItem in rptVars.Items)
                        {
                            if (rptItem.ItemType == ListItemType.Item || rptItem.ItemType == ListItemType.AlternatingItem)
                            {
                                ++numToProcess;
                                HtmlGenericControl valFailure = rptItem.FindControl("valFailure") as HtmlGenericControl;
                                var id = Convert.ToInt32(((HiddenField)rptItem.FindControl("hdnID")).Value);
                                var value = ((TextBox)rptItem.FindControl("txtEditValue")).Text;
                                try
                                {
                                    DataMgr.BuildingVariableDataMapper.UpdatePartialBuildingVariable(id, value);
                                    ++numSucessful;
                                    valFailure.InnerHtml = "";
                                    valFailure.Style[HtmlTextWriterStyle.Display] = "none";
                                }
                                catch (CW.Data.Exceptions.ValidationException ex)
                                {
                                    valFailure.Style[HtmlTextWriterStyle.Display] = "block";
                                    valFailure.InnerHtml = ex.Message;
                                }
                            }
                        }

                        if (!IsKGSOrProvider()) BuildingVariablesList.BindBuildingVariables(); //no parameter means get the updated data from db for nonkgs "variables list" repeater via this update.
                        if (numToProcess > 0)
                        {
                            if (numToProcess == numSucessful)
                            {
                                LabelHelper.SetLabelMessage(lblResults, variablesUpdateSuccessful, lnkSetFocus);
                            }
                            else
                            {
                                LabelHelper.SetLabelMessage(lblVariablesError, string.Format(partiallySuccessfulFmt, numSucessful, numToProcess), lnkSetFocus);
                            }
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building building variables values.", sqlEx);
                        LabelHelper.SetLabelMessage(lblVariablesError, variablesUpdateFailed, lnkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building building variables values.", ex);
                        LabelHelper.SetLabelMessage(lblVariablesError, variablesUpdateFailed, lnkSetFocus);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();

                    //hardset any fields to their initial values
                    ddlBuilding_SelectedIndexChanged(null, null);
                }

            #endregion

            private void HideBuildingVariableContainers()
            {
                BuildingVariablesList.Visible = BuildingVariablesListBoxes.Visible = pnlUpdateBuildingVariables.Visible = false;
            }

            private Boolean IsKGSOrProvider()
            {
                if (ContainsQueryString()) return true; //this only means that the page loaded with qs links from another admin page, so default to true (KGS or provider)

                return (AdminMode == AdminModeEnum.KGS || AdminMode == AdminModeEnum.PROVIDER);
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindRepeater(IEnumerable<Object> buildingVariables)
                {
                    pnlUpdateBuildingVariables.Visible = (buildingVariables != null && buildingVariables.Any());

                    if (pnlUpdateBuildingVariables.Visible)
                    {
                        rptVars.DataSource = buildingVariables;
                        rptVars.DataBind();
                    }
                }

                private Boolean ContainsQueryString()
                {
                    var url = HttpContext.Current.Request.Url.PathAndQuery;
                    var index = url.IndexOf('?');

                    return (index > 0);
                }

            #endregion

        #endregion
    }
}