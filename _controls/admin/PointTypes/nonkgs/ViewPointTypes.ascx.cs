﻿using CW.Data;
using CW.Data.Models.PointType;
using CW.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.admin.PointTypes.nonkgs
{
    public partial class ViewPointTypes : AdminUserControlGrid
    {
        #region fields

            private enum PointTypeDetails { Default };

        #endregion

        #region properties

            #region overrides

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    return DataMgr.PointTypeDataMapper.GetAllFullSearchedPointTypes(searchCriteria.FirstOrDefault());
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetPointTypeData>(_ => _.PointTypeID, _ => _.PointTypeName, _ => _.PointClassName, _ => _.PointClassID, _ => _.PointTypeDescription, _ => _.IPEngUnits, _ => _.SIEngUnits); }
                }

                protected override String NameField { get { return PropHelper.G<GetPointTypeData>(_ => _.PointTypeName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetPointTypeData>(_ => _.PointTypeID); } }

                protected override String Name { get { return typeof(PointType).Name; } }

                protected override Type Entity { get { return typeof(GetPointTypeData); } }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var pointType = DataMgr.PointTypeDataMapper.GetFullPointTypeByID(CurrentID).First();

                    SetPointTypeIntoViewState(pointType);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(PointTypeDetails.Default), rptPointTypeDetails);
                }

            #endregion

                private void SetPointTypeIntoViewState(GetPointTypeData pointType)
                {
                    SetPointTypeDetails(pointType);
                }

                private void SetPointTypeDetails(GetPointTypeData pointType)
                {
                    SetKeyValuePairItem("Point Type Name", pointType.PointTypeName);
                    SetKeyValuePairItem("Point Type Description", pointType.PointTypeDescription);
                    SetKeyValuePairItem("Point Class Name", pointType.PointClassName);
                    SetKeyValuePairItem("IP Eng. Units", pointType.IPEngUnits);
                    SetKeyValuePairItem("SI Eng. Units", pointType.SIEngUnits);

                    SetDetailsViewDataToViewState(Convert.ToInt32(PointTypeDetails.Default), true); //True means to clear viewstate since this is a new record
                }

        #endregion
    }
}