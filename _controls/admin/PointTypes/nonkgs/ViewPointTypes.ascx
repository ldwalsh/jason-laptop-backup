﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViewPointTypes.ascx.cs" Inherits="CW.Website._controls.admin.PointTypes.nonkgs.ViewPointTypes" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Data.Models.PointType" %>
<%@ Import Namespace="CW.Utility" %>

<h2>View Point Types</h2>

<p>
  <a id="lnkSetFocusView" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
</p>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointTypeName" HeaderText="Point Type">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.PointTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.PointTypeName), 30) %></ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="IP Eng. Units" SortExpression="IPEngUnits"> 
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.IPEngUnits)) %></ItemTemplate>
      </asp:TemplateField>  
      <asp:TemplateField HeaderText="SI Eng. Units" SortExpression="SIEngUnits"> 
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.SIEngUnits)) %></ItemTemplate>
      </asp:TemplateField>    
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Point Class" SortExpression="PointClassName">  
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.PointClassName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.PointClassName), 30) %></label>
          <asp:HiddenField ID="hdnPointClassID" runat="server" Visible="true" Value='<%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.PointClassID)) %>' />
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Point Type Description">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.PointTypeDescription)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointTypeData>(_ => _.PointTypeDescription), 100) %></label></ItemTemplate>
      </asp:TemplateField>

    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<div>
  <!--SELECT POINT TYPE DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptPointTypeDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Point Type Details</h2>
          <ul id="pointTypeDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>