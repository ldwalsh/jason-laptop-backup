﻿using CW.Business;
using CW.Caching;
using CW.Data;
using CW.Data.Interfaces.State;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using CW.Website._framework;
using CW.Website.DependencyResolution;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin
{
    public abstract class AdminUserControlGrid : AdminUserControlBase
    {
        #region fields
    
            private Lazy<List<KeyValuePair<String, String>>> keyValuePairItems;
            protected Label lblDeleteError;
            protected Label lblEditError;
            protected RadioButtonList rblViewPointsByMode;
            protected TextBox txtSearch;
            protected Panel pnlSearch;
            protected CheckBox chkAllClients;
            protected Panel pnlEdit;
            protected GridView grid;
            protected Panel pnlDetailsView;
            private AdminDropDowns adminDropDowns;
            private GridCacher _gridCacher;
            protected enum OperationType { Delete, Update };
            
        #endregion

        #region properties

            #region abstract properties

                protected abstract IEnumerable<String> GridColumnNames { get; }

                protected abstract String NameField { get; }

                protected abstract String IdColumnName { get; }

                protected abstract String Name { get; }

                protected abstract Type Entity { get; }

            #endregion

            #region virtual properties

                protected virtual Int32 PageSize { get { return 20; } }

                protected virtual List<String> CacheName { get { return new List<String> { Name, ".", Entity.Name }; } }

                protected virtual String CacheHashKey { get { return null; } }

                protected virtual IEnumerable<String> SearchGridColumnNames { get { return new[] { NameField }; } }

                protected virtual Boolean AlwaysForceGridCacherRefresh { get {return false; } }

                protected virtual Boolean ContainsDropDownListControl { get { return false; } }

                protected virtual DropDowns DropDownListControl { get; set; }

                protected virtual String CurrentControlName { get; set; }

                protected virtual Boolean HideTabMessage { get; set; } //TODO:  Change this to say HideInitialTabMessage

            #endregion

            [ViewStateProperty]
            protected Int32 CurrentID { get; set; }

            [ViewStateProperty]
            protected Int32 SelectedID { get; set; }

            protected bool UseKeyNotId { get; set; }

            [ViewStateProperty]
            protected string CurrentKey { get; set; }

            [ViewStateProperty]
            protected string SelectedKey { get; set; }

            [ViewStateProperty]
            protected Dictionary<Int32, List<KeyValuePair<String, String>>> DetailsViewData { get; set; }

            [ViewStateProperty]
            protected String GridViewSortExpression { get; set; }

            [ViewStateProperty]
            protected SortDirection GridViewSortDirection { get; set; }

            [ViewStateProperty]
            protected string SearchCriteria { get; set; } //should be made private when PropertyPersistanceManager can support it

            protected AdminModeEnum AdminModeEnum { get { return ((AdminSitePageTemp)Page).AdminModeEnum; } }

            protected Boolean IsDeferredLoading { get; set; }

            private Boolean IsEdit { get; set; }

            private Boolean ResetPaging { get; set; }

            public static ICacheStore CacheStore { get { return IoC.Resolve<DataManagerCommonStorage>().DefaultCacheRepository; } }

            protected GridCacher gridCacher
            {
                get
                {
                    _gridCacher =
                    (
                        _gridCacher ??
                        new GridCacher
                        (
                            Entity,
                            CacheStore,
							CreateCacheKeyInfo(),                            
                            GetEntitiesForGrid,
                            new GridCacher.ColumnInfo(IdColumnName, GridColumnNames, SearchGridColumnNames)
                        ) { AlwaysForceRefresh = AlwaysForceGridCacherRefresh }
                    );

                    return _gridCacher;
                }
            }

        #endregion

        #region constructor

            public AdminUserControlGrid()
            {
                keyValuePairItems = new Lazy<List<KeyValuePair<String, String>>>(() => new List<KeyValuePair<String, String>>()); //create instance as necessary via Lazy class
            }

        #endregion

        #region events

            private void Page_Init()
            {
                grid.RowCreated += grid_RowCreated;
                grid.RowDataBound += grid_DataBound;
                grid.PageIndexChanging += grid_PageIndexChanging;
                grid.Sorting += grid_Sorting;
                grid.SelectedIndexChanged += grid_SelectedIndexChanged;
                grid.RowEditing += grid_Editing;
                grid.RowDeleting += grid_Deleting;   
            }

            private void Page_FirstLoad()
            {
                grid.AllowPaging = true;
                grid.AutoGenerateColumns = false;
                grid.AllowSorting = true;
                grid.DataKeyNames = new[] { PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID) };
                grid.GridLines = GridLines.None;
                grid.PageSize = PageSize;
                grid.PagerSettings.PageButtonCount = 20;

                GridViewSortExpression = NameField;
                GridViewSortDirection = SortDirection.Ascending;

                if (ContainsDropDownListControl && DropDownListControl != null)
                {
                    adminDropDowns = new AdminDropDowns(RadTabStrip, RadMultiPage, lblResults, rblViewPointsByMode);
                    adminDropDowns.PopulateAndSetDropDownLists(DropDownListControl, CurrentControlName);
                    HideTabMessage = adminDropDowns.HideTabMessage(DropDownListControl.ViewMode, CurrentControlName);
                }

                if (HideTabMessage) return;
                if (!IsDeferredLoading) BindData();
            }

            private void Page_Load()
            {
                lblResults.Visible = false;

                if (lblDeleteError != null) lblDeleteError.Visible = false;
                if (lblEditError != null) lblEditError.Visible = false;

                ((AdminSitePageTemp)Page).TabStateMonitor.OnChangeState += TabStateMonitor_OnChangeState;
            }

            private void Page_PreRender()
            {
                //since we are dynamically setting the detailsview data now, make sure to re-retrieve the data based on the grid ID (pulls from db initially, then viewstate)
                if (pnlDetailsView == null) return;
                if (Page.IsPostBack && pnlDetailsView.Visible && (CurrentID != 0 || (UseKeyNotId && !string.IsNullOrWhiteSpace(CurrentKey)))) GetDetailsViewData();
            }

            #region grid events

                private void grid_RowCreated(Object sender, GridViewRowEventArgs e)
                {
                    if (e.Row.RowType == DataControlRowType.Pager)
                    {
                        var tblPager = (Table)e.Row.Cells[0].Controls[0];

                        tblPager.CssClass = "pageTable";

                        var row = tblPager.Rows[0];
                        var cellPreviousPage = new TableCell();

                        cellPreviousPage.Controls.Add(new LinkButton { CommandArgument = "Prev", CommandName = "Page", Text = "« Previous Page", CssClass = "pageLink" });

                        row.Cells.AddAt(0, cellPreviousPage);

                        var cellNextPage = new TableCell();

                        cellNextPage.Controls.Add(new LinkButton { CommandArgument = "Next", CommandName = "Page", Text = "Next Page »", CssClass = "pageLink" });

                        row.Cells.Add(cellNextPage);
                    }
                }

                private void grid_Editing(Object sender, GridViewEditEventArgs e)
                {
                    string value = grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString();
                    if (UseKeyNotId)
                        CurrentKey = value;
                    else
                        CurrentID = Convert.ToInt32(value);
                    OnGridEditing(sender, e);
                }


                private void grid_Deleting(Object sender, GridViewDeleteEventArgs e)
                {
                    OnGridDeleting(sender, e);
                }

                private void grid_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    if (pnlEdit != null) pnlEdit.Visible = false;

                    if (pnlDetailsView != null)
                    {
                        pnlDetailsView.Visible = true;
                        var idOrKey = grid.DataKeys[grid.SelectedIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString();

                        if (UseKeyNotId)
                            CurrentKey = idOrKey;
                        else
                            CurrentID = int.Parse(idOrKey);
                    }

                    OnGridSelectedIndexChanged(sender, e);
                }

                private void grid_DataBound(Object sender, GridViewRowEventArgs e)
                {
                    OnGridDataBound(sender, e);
                }

                private void grid_PageIndexChanging(Object sender, GridViewPageEventArgs e)
                {
                    if (e.NewPageIndex < 0) return;

                    grid.PageIndex = e.NewPageIndex;

                    BindData();
                }

                private void grid_Sorting(Object sender, GridViewSortEventArgs e)
                {
                    grid.EditIndex = -1;

                    if (GridViewSortExpression == e.SortExpression)
                        GridViewSortDirection = ((GridViewSortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending);
                    else
                        GridViewSortExpression = e.SortExpression;

                    BindData();
                }

                #region virtual grid events

                    protected virtual void OnGridDataBound(Object sender, GridViewRowEventArgs e) { }
                    protected virtual void OnGridSelectedIndexChanged(Object sender, EventArgs e) { }
                    protected virtual void OnGridEditing(Object sender, GridViewEditEventArgs e){ }
                    protected virtual void OnGridDeleting(Object sender, GridViewDeleteEventArgs e){ }

                #endregion

            #endregion

            #region button events

                protected void viewAll_Click(Object sender, EventArgs e)
                {
                    SetSearchSettings(true); //true means reset search i.e. "viewAll"
                }

                protected void searchButton_Click(Object sender, EventArgs e)
                {
                    SetSearchSettings();
                }

            #endregion

            #region repeater events

                protected void repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
                {
                    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                    {   
                        var key = ((KeyValuePair<String, String>)e.Item.DataItem).Key;
                        var value = ((KeyValuePair<String, String>)e.Item.DataItem).Value;
                        var uListItem = (UListItem)e.Item.FindControl("uListItem");

                        uListItem.Label = key;
                        uListItem.Value = value;
                    }
                }

            #endregion

            #region tab events

                #region virtual

                    protected virtual void TabStateMonitor_OnChangeState(TabBase tab, IEnumerable<Enum> messages)
                    {
                        gridCacher.Clear(); //revise to only, Clear when state change requires it (by comparing passed in enum value)
                    }

                #endregion

            #endregion

        #endregion

        #region methods

            #region virtual methods

                protected virtual GridCacher.CacheKeyInfo CreateCacheKeyInfo()
                {
                    return new GridCacher.CacheKeyInfo(String.Concat(CacheName), () => { return CacheHashKey; }, () => { return null; });
                }

                protected virtual void SetDataForDetailsViewFromDB() { }

                protected virtual void SetFieldsIntoDetailsView() { }

                protected virtual void BindData(Boolean forceRefresh = false) //made this method virtual to be overridden with special case grids if necessary.
                {
                    var gs = (SearchCriteria == null) ? gridCacher.Get(forceRefresh) : (forceRefresh) ? gridCacher.Get(SearchCriteria, forceRefresh) : gridCacher.Get(SearchCriteria);
                    var rows = gs.Rows;

                    if (SearchCriteria == null) pnlSearch.Visible = rows.Any();

                    if (rows != null)
                    {
                        if (ResetPaging) grid.PageIndex = 0;

                        var sort = GridViewSortExpression;
                         
                        DateTime date;                                                                                                    
                        rows = (gs.ColumnTypes[sort] == "DateTime") ? rows.OrderBy(_ => String.IsNullOrEmpty(_.Cells[sort])).ThenBy(_ => DateTime.TryParse(_.Cells[sort], out date) ? (DateTime?)Convert.ToDateTime(date, CultureInfo.InvariantCulture) : null)
                                                             : (gs.ColumnTypes[sort] == "Int32") ? rows.OrderBy(_ => Int32.Parse(_.Cells[sort])) : rows.OrderBy(_ => _.Cells[sort]);

                        if (GridViewSortDirection == SortDirection.Descending) rows = rows.Reverse();

                        //filtering
                        rows = FilterData(rows);

                        grid.DataSource = rows.ToList();
                        grid.DataBind();

                        SetGridCountLabel(rows.Count());
                    }
                }

            #endregion

            #region abstract methods
                protected abstract IEnumerable GetEntitiesForGrid(IEnumerable<string> searchCriteria);

            #endregion

            protected void SetColumnAttributes(int columnIndex, string sortExpression, string headerText)
            {
                var column = grid.Columns[columnIndex];

                column.SortExpression = sortExpression;
                column.HeaderText = headerText;
            }

            private void GetDetailsViewData()
            {
                if (CurrentID != SelectedID || (UseKeyNotId && CurrentKey != SelectedKey)) SetDataForDetailsViewFromDB();
                
                SetFieldsIntoDetailsView();
            }

            protected void SetKeyValuePairItem(String key, String value, String content = "")
            {
                if (String.IsNullOrWhiteSpace(key)) throw new ArgumentException("Key cannot be empty");

                if (!String.IsNullOrWhiteSpace(value))                    
                    keyValuePairItems.Value.Add(new KeyValuePair<String, String>(key, String.IsNullOrWhiteSpace(content) ? value : String.Format(content, value)));
            }

            protected void SetKeyValuePairItemsToRepeater(Int32 index, Repeater repeater)
            {
                if (DetailsViewData.ContainsKey(index))
                {
                    var fields = DetailsViewData[index];

                    if (fields.Count() > 0)
                    {
                        repeater.DataSource = fields.Select(f => new KeyValuePair<String, String>(f.Key, f.Value)).ToList();
                        repeater.DataBind();
                    }
                }
                else
                {
                    repeater.DataSource = null;
                    repeater.DataBind();
                }
            }

            protected void SetDetailsViewDataToViewState(Int32 index, Boolean clearViewState = false)
            {
                if (clearViewState) DetailsViewData = null;

                DetailsViewData = DetailsViewData ?? new Dictionary<Int32, List<KeyValuePair<String, String>>>();

                if (keyValuePairItems.IsValueCreated) DetailsViewData.Add(index, new List<KeyValuePair<String, String>>(keyValuePairItems.Value));

                keyValuePairItems.Value.Clear();
            }

            private void SetGridCountLabel(Int32 count)
            {
                if (String.IsNullOrWhiteSpace(SuccessMessage))
                {
                    var formattedName = LanguageHelper.GetFormattedName(Name, (count == 1));

                    LabelHelper.SetLabelMessage(lblResults, (count == 0) ? String.Format("No {0} found.", formattedName) : String.Format("{0} {1} found.", count, formattedName), null);
                }
            }

            private IEnumerable<GridCacher.GridSet.Row> FilterData(IEnumerable<GridCacher.GridSet.Row> rows)
            {
                if (ContainsDropDownListControl && DropDownListControl != null)
                {
                    if (DropDownListControl.EID != null) return rows.Where(gd => gd.Cells[PropHelper.G<CW.Data.Equipment>(_ => _.EID)] == DropDownListControl.GetDDL(DropDowns.ViewModeEnum.Equipment).SelectedValue);
                    if (DropDownListControl.EquipmentClassID != null) return rows.Where(gd => gd.Cells[PropHelper.G<EquipmentClass>(_ => _.EquipmentClassID)] == DropDownListControl.GetDDL(DropDowns.ViewModeEnum.EquipmentClass).SelectedValue);
                }

                return rows;
            }

            protected void ResetUI()
            {
                lblResults.Text = txtSearch.Text = String.Empty;
                SearchCriteria = null;
                grid.Visible = pnlSearch.Visible = false;
                HideEditAndDetailsPanel();
            }

            private void SetSearchSettings(Boolean isViewAll = false)
            {
                var forceRefresh = HasSearchAllClientsCheckBox();

                HideEditAndDetailsPanel();

                if (isViewAll)
                {
                    if (HasSearchAllClientsCheckBox()) chkAllClients.Checked = false;
                    
                    txtSearch.Text = String.Empty;
                }

                ResetPaging = true;

                RebindGrid(forceRefresh, (!String.IsNullOrWhiteSpace(txtSearch.Text)) ? txtSearch.Text : string.Empty);
            }

            private void HideEditAndDetailsPanel()
            {
                if (pnlEdit != null) pnlEdit.Visible = IsEdit;
                if (pnlDetailsView != null) pnlDetailsView.Visible = false;                
            }

            protected string FormatHashKey(IEnumerable<Object> fieldValues)
            {
                var keys = new List<String>();

                foreach (var val in fieldValues)
                {
                    if (val == null) continue;

                    var stringVal = val.ToString();

                    if (val is Int32)
                    {
                        if (stringVal == "0") continue;

                        keys.Add(stringVal);
                    }

                    if (Nullable.GetUnderlyingType(val.GetType()) != null) keys.Add(val.ToString());
                }

                return String.Join("|", keys);
            }

            protected String GetCellContent(IDataItemContainer container, String propName, Int32 maxLength = 0)
            {
                var v = ((GridCacher.GridSet.Row)container.DataItem).Cells[propName];

                return ((maxLength > 0) ? StringHelper.TrimText(v, maxLength) : v);
            }

            protected DateTime GetCellContentAsDateTime(IDataItemContainer container, String propName)
            {
                return DateTime.Parse(((GridCacher.GridSet.Row)container.DataItem).Cells[propName], CultureInfo.InvariantCulture);
            }        
        
        	private void RebindGrid(Boolean forceRefresh = false, string criteria = null)
            {
                SearchCriteria = criteria;
                BindData(forceRefresh);
            }

            protected void OperationComplete(OperationType operationType, String message, Enum tabEnum = null)
            {
                IsEdit = (operationType == OperationType.Update);
                ChangeCurrentTab(message);
                SetChangeStateForTabs(tabEnum);

                if(operationType == OperationType.Update) SetDataForDetailsViewFromDB();
            }

            private Boolean HasSearchAllClientsCheckBox()
            {
                return (chkAllClients != null);
            }

            //override the base method here, since all grids reset in exactly the same fashion
            protected override void InitializeControl()
            {
                HideEditAndDetailsPanel();

                GridViewSortDirection = SortDirection.Ascending;
                GridViewSortExpression = NameField;

                SearchCriteria = null;

                ResetPaging = true;

                BindData(true);
            }

        #endregion
    }
}