﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PointRangeCheck.ascx.cs" Inherits="CW.Website._controls.admin.System.PointRangeCheck" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
<%@ Register tagPrefix="CW" tagName="PointRangeDownloadArea" src="~/_controls/admin/System/PointRangeDownloadArea.ascx" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Common.Constants" %>
<%@ Import Namespace="CW.Data.Helpers" %>
<%@ Import Namespace="CW.Data.Models.Point" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Panel ID="pnlPointRangeCheck" runat="server" DefaultButton="btnPointRangeCheck">  

  <CW:TabHeader runat="server" ID="tabHeader" Title="Point Range Check">
    <SubTitleTemplate>
      <p>Checks if data received on specified day for all points for a client are within a min and max engineering unit range. This may take several minutes to run.</p>
    </SubTitleTemplate>
    <RightAreaTemplate>
      <CW:PointRangeDownloadArea runat="server" ID="PointRangeDownloadArea" Visible="false" />
    </RightAreaTemplate>
  </CW:TabHeader>
  
  <p>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
  </p> 

  <div class="divForm">
    <label class="label">*Date:</label>
    <telerik:RadDatePicker ID="txtPointRangeDate" runat="server" MinDate="01/01/2005" />
    <asp:RequiredFieldValidator ID="pointRangeDateRFV" runat="server" CssClass="errorMessage" ErrorMessage="Date is a required field." ControlToValidate="txtPointRangeDate" SetFocusOnError="true" Display="Dynamic" ValidationGroup="PointRangeCheck" />    
  </div>  
  <div class="divForm">
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>
  </div>
  <div class="divForm">   
    <label class="label">*Select Building:</label>
    <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" AutoPostBack="true" />
    <asp:RequiredFieldValidator ID="ddlBuildingRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="PointRangeCheck" />
    <ajaxToolkit:ValidatorCalloutExtender ID="ddlBuildingVCE" runat="server" TargetControlID="ddlBuildingRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
  <div class="divForm">
    <label class="label">Select Equipment Class:</label>
    <asp:DropDownList ID="ddlEquipmentClass" CssClass="dropdown" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" />
  </div>
  <div class="divForm">
    <label class="label">Select Equipment Type:</label>
    <asp:DropDownList ID="ddlEquipmentType" CssClass="dropdown" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentType_OnSelectedIndexChanged" AutoPostBack="true" />
  </div>
  <div class="divForm">
    <label class="label">Select Equipment:</label>
    <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" />
  </div>
  <br/>

  <asp:LinkButton ID="btnPointRangeCheck" runat="server" CssClass="lnkButton" Text="Run" OnClick="btnPointRangeCheck_Click" ValidationGroup="PointRangeCheck" />
 
  <hr />

</asp:Panel>  

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
</asp:Panel>     
                                                                                                                
<div id="gridTbl">                                        
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>                                                                                                   
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointName" HeaderText="Point Name">  
        <ItemTemplate>
          <label title="<%# "Initial Equipment_Point Association: " + GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.EquipmentName)) + " - " + GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.PointName)) %>">
            <%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.PointName), 25) %>
          </label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointTypeName" HeaderText="Point Type">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.PointTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.PointTypeName), 15) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataSourceName" HeaderText="Data Source Name">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataSourceName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataSourceName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ReferenceID" HeaderText="Reference ID"> 
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.ReferenceID)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.ReferenceID), 25) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EngUnits" HeaderText="Eng Units">  
        <ItemTemplate>
          <%# "<span title='Min = " + GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.Min)) + " , Max = " + GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.Max)) + "'>"
                 + GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.EngUnits)) + 
              "</span>" %>
        </ItemTemplate>                                                  
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="A">
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.Addition)) %></ItemTemplate>                                                  
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="M">
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.Multiply)) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="P">
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.Power)) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Min">
        <ItemTemplate><%# NumericHelper.RoundPrecisionCurrentThread(GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.CalculatedMin)), 3)%></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Max">
        <ItemTemplate><%# NumericHelper.RoundPrecisionCurrentThread(GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.CalculatedMax)), 3)%></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataStatus" HeaderText="Rng Status">
        <ItemTemplate>
          <%# "<span class='" + (GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataStatus)) == "Good" ? "statusGreen" 
                              : (GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataStatus)) == "Bad" ? "statusRed" : "statusBlue")) + "'>" 
                              + GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataStatus)) + "</span>" %>
        </ItemTemplate>
      </asp:TemplateField>                                                                                                                                                                        
    </Columns>       
  </asp:GridView>                                                                           
</div> 