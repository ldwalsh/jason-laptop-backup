﻿using CW.Utility.Web;
using System;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.System
{
    public partial class IndividualDataSourceCheck : AdminUserControlBase
    {
        #region constants

            const Int32 MaxQueryHrs = 72;
            const String dataCheckErrorFormat = "There has been no data received between {0} and {1} UTC.";
            const String dataCheckSuccess = "Success";
            const String dataCheckException = "An exception was thrown: ";
            const String maxQueryLengthExceeded = "The max query length ({0} hrs) has been exceeded";

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindDataSources(ddlDataSource);
            }

            private void Page_Load()
            {
                lblError.Visible = lblIndividualResultSuccess.Visible = false;
            }

            #region button events

                protected void btnIndividual_ButtonClick(Object sender, EventArgs e)
                {
                    DateTime startDate, endDate;

                    startDate = Convert.ToDateTime(txtStartDateDataSource.SelectedDate);
                    endDate = Convert.ToDateTime(txtEndDateDataSource.SelectedDate);

                    if ((endDate - startDate).TotalHours > MaxQueryHrs)
                    {
                        LabelHelper.SetLabelMessage(lblError, String.Format(maxQueryLengthExceeded, MaxQueryHrs), lnkSetFocus);
                        return;
                    }

                    try
                    {
                        var points = DataMgr.PointDataMapper.GetAllActivePointsByDSID(Convert.ToInt32(ddlDataSource.SelectedValue));
                        var recentData = DataMgr.RawDataMapper.GetRawDataForPointsFull(points, startDate, endDate, false);
                        var hasRecentData = recentData.Any();

                        divIndividual.Visible = hasRecentData;

                        if (hasRecentData)
                        {
                            var counter = 1;

                            foreach (var data in recentData)
                            {
                                txtIndividual.Text += String.Format("{0}) PID={1}, RawValue={2}, DateLogged={3} \n", counter, data.PID.ToString(), data.RawValue, data.DateLoggedLocal.ToString());
                                counter++;
                            }

                            LabelHelper.SetLabelMessage(lblResults, dataCheckSuccess, lnkSetFocus);
                            return;
                        }
                        else
                        {
                            LabelHelper.SetLabelMessage(lblError, String.Format(dataCheckErrorFormat, startDate.ToString(), endDate.ToString()), lnkSetFocus);
                            return;
                        }
                    }
                    catch (Exception ex) { LabelHelper.SetLabelMessage(lblError, dataCheckException + ex, lnkSetFocus); }
                }

            #endregion

        #endregion

        #region methods

            #region binders

                private void BindDataSources(DropDownList ddl)
                {
                    BindDDL(ddl, "DataSourceName", "DSID", DataMgr.DataSourceDataMapper.GetAllDataSourcesByCID(siteUser.CID), true);
                }

            #endregion

        #endregion
    }
}