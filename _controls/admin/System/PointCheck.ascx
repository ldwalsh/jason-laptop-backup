﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PointCheck.ascx.cs" Inherits="CW.Website._controls.admin.System.PointCheck" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Common.Constants" %>
<%@ Import Namespace="CW.Data.Models.Point" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Point  Check">
  <SubTitleTemplate>
    <p>Checks if data has been received for all points in a building for the current day.</p>
  </SubTitleTemplate>
</CW:TabHeader>

<p>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
</p>

<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div>
<div class="divForm">
  <label class="label">*Select Building:</label>
  <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" AutoPostBack="true" />
</div> 
<br/>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointName" HeaderText="Point Name">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.PointName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.PointName), 30) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataSourceName" HeaderText="Data Source Name">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataSourceName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataSourceName), 35) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ReferenceID" HeaderText="ReferenceID"> 
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.ReferenceID)) %>"><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.ReferenceID), 35) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Active">  
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.IsActive)) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Health Status" SortExpression="DataStatus">
        <ItemTemplate>
          <%# "<span class='" + (GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataStatus)).ToString() == "Good" ? "statusGreen" 
                              : (GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataStatus)).ToString() == "Bad" ? "statusRed" : "statusBlue")) + "'>" 
                              + GetCellContent(Container, PropHelper.G<GetPointsDataStatusFormat>(_ => _.DataStatus)).ToString() + "</span>" %>
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>