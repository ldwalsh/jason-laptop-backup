﻿using CW.Data.Models.DataSource;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.System
{
    public partial class DataSourceCheck : AdminUserControlGrid
    {
        #region constants

            const Int32 MaxQueryHrs = 72;

        #endregion

        #region fields

            private String maxQueryLengthExceeded = String.Format("The max query length ({0} hrs) has been exceeded", MaxQueryHrs);

        #endregion

        #region properties

            #region overrides

                protected override Int32 PageSize { get { return 10000; } }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchText)
                {
                    DateTime startDate, endDate;

                    var emptySet = Enumerable.Empty<GetDataSourcesDataStatusFormat>();

                    startDate = Convert.ToDateTime(txtStartDate.SelectedDate);
                    endDate = Convert.ToDateTime(txtEndDate.SelectedDate);

                    if (startDate == DateTime.MinValue && endDate == DateTime.MinValue) return emptySet;

                    if ((endDate - startDate).TotalHours > MaxQueryHrs)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, maxQueryLengthExceeded, lnkSetFocus);
                        return emptySet;
                    }

                    return DataMgr.DataSourceDataMapper.QueryDataSourcesDataStatusByBID(Convert.ToInt32(ddlBuilding.SelectedValue), startDate, endDate, chkIncludeInactiveDataSources.Checked, searchText.FirstOrDefault());
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetDataSourcesDataStatusFormat>(_ => _.DSID, _ => _.DataSourceName, _ => _.IPList, _ => _.ReferenceID, _ => _.IsIntegrated, _ => _.IsActive, _ => _.DataStatus); }
                }

                protected override String NameField { get { return PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.DataSourceName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.DSID); } }

                protected override String Name { get { return "data source"; } }

                protected override Type Entity { get { return typeof(GetDataSourcesDataStatusFormat); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID, txtStartDate.SelectedDate, txtEndDate.SelectedDate, ddlBuilding.SelectedValue }); } }

                protected override Boolean HideTabMessage { get { return true; } }

            #endregion

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindBuildings(ddlBuilding);
            }

            #region button events

                protected void btnHealthCheckByBuilding_ButtonClick(Object sender, EventArgs e)
                {
                    txtSearch.Text = String.Empty;
                    BindData(true);
                }

            #endregion

        #endregion

        #region methods

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID), true);
                }

            #endregion

        #endregion
    }
}