﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="IndividualDataSourceCheck.ascx.cs" Inherits="CW.Website._controls.admin.System.IndividualDataSourceCheck" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>

<asp:Panel ID="pnlIndividual" runat="server" DefaultButton="btnIndividual">

  <h2>Individual Data Source Check</h2>

  <p>Checks if data has been received between selected date range for a selected client's data source. This may take several minutes to run.</p>

  <p>
    <a id="lnkSetFocus" href="#" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
     <asp:Label ID="lblError" CssClass="errorMessage" runat="server" />
  </p>

  <div class="divForm">
    <div class="quickDates">Prior:
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDateDataSource.ClientID %>', '<%= txtEndDateDataSource.ClientID %>', 48, 'hours', true);">2 days</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDateDataSource.ClientID %>', '<%= txtEndDateDataSource.ClientID %>', 24, 'hours', true);">1 day</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDateDataSource.ClientID %>', '<%= txtEndDateDataSource.ClientID %>', 12, 'hours', true);">12 hrs</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDateDataSource.ClientID %>', '<%= txtEndDateDataSource.ClientID %>', 6, 'hours', true);">6 hrs</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDateDataSource.ClientID %>', '<%= txtEndDateDataSource.ClientID %>', 1, 'hours', true);">1 hr</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDateDataSource.ClientID %>', '<%= txtEndDateDataSource.ClientID %>', 10, 'minutes', true);">10 mins</a>
    </div>
  </div>
  <div class="divForm">
    <label class="label">Start Date (UTC):</label>
    <telerik:RadDateTimePicker ID="txtStartDateDataSource" runat="server"></telerik:RadDateTimePicker> 
    <asp:RequiredFieldValidator ID="startDateStartDateRFV" runat="server" ErrorMessage="Date is a required field." ControlToValidate="txtStartDateDataSource" SetFocusOnError="true" Display="None" ValidationGroup="Individual" />
    <ajaxToolkit:ValidatorCalloutExtender ID="startDateDataSourceVCE" runat="server" BehaviorID="startDateDataSourceVCE" TargetControlID="startDateStartDateRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
  <div class="divForm">
    <label class="label">End Date (UTC):</label>
    <telerik:RadDateTimePicker ID="txtEndDateDataSource" runat="server"></telerik:RadDateTimePicker> 
    <asp:RequiredFieldValidator ID="endDateDataSourceRFV" runat="server" ErrorMessage="End date is a required field." ControlToValidate="txtEndDateDataSource" SetFocusOnError="true" Display="None" ValidationGroup="Individual" />
    <ajaxToolkit:ValidatorCalloutExtender ID="endDateDataSourceVCE" runat="server" BehaviorID="endDateDataSourceVCE" TargetControlID="endDateDataSourceRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />    
    <asp:CustomValidator ID="endDateDataSourceCV" runat="server" ClientValidationFunction="RadDatePickerDatetimeComparer" ControlToValidate="txtEndDateDataSource" EnableClientScript="true" ErrorMessage="End Date cannot be before Start Date" Display="None" ValidationGroup="Individual" /> 
    <ajaxToolkit:ValidatorCalloutExtender ID="endDateDataSourceCVVCE" runat="server" BehaviorID="endDateDataSourceCVVCE" TargetControlID="endDateDataSourceCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
  <div class="divForm">
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>
  </div>
  <div class="divForm">
    <label class="label">*Data Source:</label>
    <asp:DropDownList CssClass="dropdown" ID="ddlDataSource" AppendDataBoundItems="true" runat="server" />
    <asp:RequiredFieldValidator ID="dataSourceRFV" runat="server" ErrorMessage="Data Source is a required field." ControlToValidate="ddlDataSource" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Individual" />
    <ajaxToolkit:ValidatorCalloutExtender ID="dataSourceVCE" runat="server" BehaviorID="dataSourceVCE" TargetControlID="dataSourceRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>

  <asp:LinkButton ID="btnIndividual" runat="server" Text="Run" CssClass="lnkButton" OnClick="btnIndividual_ButtonClick" ValidationGroup="Individual" />

  <hr />

  <div class="divForm">
    <div id="divIndividual" runat="server" visible="false">
      <asp:TextBox ID="txtIndividual" runat="server" TextMode="MultiLine" ReadOnly="true" Height="300" Width="480" />
      <p>
        <asp:Label ID="lblIndividualResultError" runat="server" CssClass="errorMessage" Visible="false" />
        <asp:Label ID="lblIndividualResultSuccess" runat="server" CssClass="successMessage" Visible="false" />
      </p>   
    </div>              
  </div>

</asp:Panel>