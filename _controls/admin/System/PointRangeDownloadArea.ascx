﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PointRangeDownloadArea.ascx.cs" Inherits="CW.Website._controls.admin.System.PointRangeDownloadArea" %>

<div class="divDownloadBox2">
  <asp:Label ID="lblError" CssClass="errorMessage" Visible="false" runat="server" />

  <div class="divDownload">
    <asp:ImageButton runat="server" CausesValidation="false" ID="imgDownload" CssClass="imgDownload" ImageUrl="/_assets/images/excel-icon.jpg" AlternateText="download" />
    <asp:LinkButton runat="server" CausesValidation="false" ID="lnkDownload" CssClass="lnkDownload" Text="Download Point Range" />
  </div>
</div>