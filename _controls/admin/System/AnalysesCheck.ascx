﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AnalysesCheck.ascx.cs" Inherits="CW.Website._controls.admin.System.AnalysesCheck" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Data.AzureStorage.Models" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="System.Collections.Generic" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Analyses Check">
  <SubTitleTemplate>
    <p>Checks analyses status and performance</p>
  </SubTitleTemplate>
</CW:TabHeader>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblError" CssClass="errorMessage" runat="server" />
</p>

<div class="divForm">   
  <label class="label">*Select Building:</label>
  <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" />
  <asp:RequiredFieldValidator ID="buildingAC_RFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AnalysesCheck" />
  <ajaxToolkit:ValidatorCalloutExtender ID="buildingAC_VCE" runat="server" BehaviorID="buildingAC_VCE" TargetControlID="buildingAC_RFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</div>

<div class="divForm">   
  <label class="label">*Select Analysis Range:</label>
  <asp:DropDownList ID="ddlAnalysisRange" runat="server" CssClass="dropdown" AppendDataBoundItems="true"  />
  <asp:RequiredFieldValidator ID="analysisRangeRFV" runat="server" ErrorMessage="Analysis Range is a required field." ControlToValidate="ddlAnalysisRange" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AnalysesCheck" />
  <ajaxToolkit:ValidatorCalloutExtender ID="analysisRangeVCE" runat="server" BehaviorID="analysisRangeVCE" TargetControlID="analysisRangeRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</div>

<div class="divForm">   
  <label class="label">Analysis:</label>
  <asp:DropDownList ID="ddlAnalysis" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />
</div>

<div class="divForm">   
  <label class="label">Equipment:</label>
  <asp:DropDownList ID="ddlEquipment" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />
</div>

<div class="divForm">   
  <label class="label">Status:</label>
  <asp:DropDownList ID="ddlStatus" runat="server" CssClass="dropdown" AppendDataBoundItems="true"  />
</div>
<br/>

<asp:LinkButton ID="btnAnalysesCheck" runat="server" CssClass="lnkButton" Text="Check Analyses" OnClick="AnalysesCheck_Click" ValidationGroup="AnalysesCheck" />

<div id="gridTbl">
  <asp:GridView ID="grid" runat="server">
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField ItemStyle-Wrap="true">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.BuildingName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.BuildingName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.AnalysisName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.AnalysisName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.EquipmentName)) %>"><%# GetCellContent(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.EquipmentName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true">  
        <ItemTemplate><label><%# GetCellContentAsDateTime(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.LastExectionStartDate)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true">  
        <ItemTemplate><label><%# GetCellContentAsDateTime(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.LastExectionEndDate)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true">  
        <ItemTemplate><label><%# GetCellContent(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.LastStatusCode)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true">  
        <ItemTemplate><label><%# GetCellContent(Container, PropHelper.G<GetPerfMatlabRecordData>(_ => _.AnalysisRange)) %></label></ItemTemplate>
      </asp:TemplateField>
    </Columns>
  </asp:GridView>
</div>
<br />
<br />

<asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
  <div>

    <asp:Repeater ID="rptAnalysesCheckDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
      <HeaderTemplate>
        <h2>Analyses Check Details</h2>
        <ul id="analysesCheckDetails" class="detailsList">
      </HeaderTemplate>

      <ItemTemplate>
        <CW:UListItem ID="uListItem" runat="server" />
      </ItemTemplate>

      <FooterTemplate>
        </ul>
      </FooterTemplate>
    </asp:Repeater>

  </div>
</asp:Panel>

<!--this has to be here since the grid base class is looking for this panel control, can be fixed, but is outside the scope of this task-->
<asp:Panel ID="pnlSearch" runat="server" CssClass="divSearch" visible="false"></asp:Panel>