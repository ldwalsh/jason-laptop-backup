﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="InterpreterCheck.ascx.cs" Inherits="CW.Website._controls.admin.System.InterpreterCheck" %>

<h2>Interpreter Check</h2>

<p>Checks if the interpreter is up and running by recieving a response.</p>
 
<p>
  <asp:Label ID="lblInterpreterCheckError" CssClass="errorMessage" runat="server" Visible="false" />
  <asp:Label ID="lblInterpreterCheckSuccess" CssClass="successMessage" runat="server" Visible="false" />
</p>
  
<div class="divForm">
  <asp:LinkButton ID="btnInterpreterCheck" runat="server" Text="Run" CssClass="lnkButton" OnClick="btnInterpreterCheck_ButtonClick" ValidationGroup="InterpreterCheck" />
</div>

<div class="divForm">
  <div id="divInterpreterCheck" runat="server" visible="false">
    <asp:TextBox ID="interpreterCheck" Height="300" Width="480" runat="server" TextMode="MultiLine" ReadOnly="true" />
  </div>
</div>