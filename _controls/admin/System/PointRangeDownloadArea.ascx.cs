﻿using CW.Data;
using CW.Data.Models.Point;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._downloads;
using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Web.UI;

namespace CW.Website._controls.admin.System
{
    public partial class PointRangeDownloadArea : DownloadAreaBase<GetPointsDataStatusFormat, PointRangeDownloadGenerator>
    {
        #region fields

            private String fnDate = PropHelper.G<Point>(_ => _.DateModified);
            private String fnBid = PropHelper.G<Building>(_ => _.BID);
            private String fnEquipmentClassId = PropHelper.G<EquipmentClass>(_ => _.EquipmentClassID);
            private String fnEquipmentTypeId = PropHelper.G<EquipmentType>(_ => _.EquipmentTypeID);
            private String fnEid = PropHelper.G<CW.Data.Equipment>(_ => _.EID);
            private Dictionary<String, IEnumerable<String>> fieldDefs;

        #endregion

        #region enums

            protected enum DDLNames { ddlBuilding, ddlEquipmentClass, ddlEquipmentType, ddlEquipment }

        #endregion

        #region properties

            #region overrides

                protected override Func<Dictionary<String, Object>, IEnumerable<GetPointsDataStatusFormat>> GetData { get { return GetPointRangeDataByParameters; } }

                protected override Dictionary<String, Object> Parameters
                {
                    get
                    {
                        var items = new Dictionary<String, Object>();

                        items.Add(fnDate, RadDatePicker.SelectedDate);

                        CreateFieldDefs();

                        foreach (var ddl in DropDownLists)
                        {
                            var ddlID = ddl.ID;

                            if (fieldDefs.ContainsKey(ddlID))
                            {
                                var fieldDef = fieldDefs[ddlID];

                                items.Add(fieldDef.First(), ddl.SelectedValue);

                                if (fieldDef.Count() > 1) items.Add(fieldDef.Last(), ddl.SelectedItem.Text); //can refine later for multiple fields per item
                            }
                        }

                        return items;
                    }
                }

            #endregion

            public RadDatePicker RadDatePicker { get; set; }

        #endregion

        #region methods

            public void CreateFieldDefs()
            {
                fieldDefs = new Dictionary<String, IEnumerable<String>>();

                fieldDefs.Add(Enum.GetName(typeof(DDLNames), DDLNames.ddlBuilding), new List<String>() { fnBid, PropHelper.G<Building>(_ => _.BuildingName) });
                fieldDefs.Add(Enum.GetName(typeof(DDLNames), DDLNames.ddlEquipmentClass), new List<String>() { fnEquipmentClassId });
                fieldDefs.Add(Enum.GetName(typeof(DDLNames), DDLNames.ddlEquipmentType), new List<String>() { fnEquipmentTypeId });
                fieldDefs.Add(Enum.GetName(typeof(DDLNames), DDLNames.ddlEquipment), new List<String>() { fnEid });
            }

            public IEnumerable<GetPointsDataStatusFormat> GetPointRangeDataByParameters(Dictionary<String, Object> parameters)
            {
                var selectedDate = Convert.ToDateTime(parameters[fnDate]);
                var bid = Convert.ToInt32(parameters[fnBid]);
                var ecid = Convert.ToInt32(parameters[fnEquipmentClassId]);
                var etid = Convert.ToInt32(parameters[fnEquipmentTypeId]);
                var eid = Convert.ToInt32(parameters[fnEid]);

                ecid = (ecid != -1) ? ecid : 0;
                etid = (etid != -1) ? etid : 0;
                eid = (eid != -1) ? eid : 0;

                return DataMgr.PointDataMapper.SearchPointsWithPartialDataByBIDAsRangeStatusFormat(null, selectedDate, bid, ecid, etid, eid);
            }

        #endregion
    }
}