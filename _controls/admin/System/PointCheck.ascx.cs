﻿using CW.Data;
using CW.Data.Models.Point;
using CW.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.System
{
    public partial class PointCheck : AdminUserControlGrid
    {
        #region properties

            #region overrides

                protected override Int32 PageSize { get { return 10000; } }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    var hasSearchCriteria = !String.IsNullOrWhiteSpace(String.Join("", searchCriteria)); // TODO: NP: could control this higher up in the base 
                    var data = Enumerable.Empty<GetPointsDataStatusFormat>();
                    var bid = (!String.IsNullOrWhiteSpace(ddlBuilding.SelectedValue)) ? Convert.ToInt32(ddlBuilding.SelectedValue) : -1;

                    if (bid != -1) 
                        data = (hasSearchCriteria) ? DataMgr.PointDataMapper.SearchPointsWithPartialDataByBIDAsHealthStatusFormat(searchCriteria.FirstOrDefault(), bid) 
                                                   : DataMgr.PointDataMapper.GetAllPointsWithPartialDataByBIDAsHealthStatusFormat(bid);

                    return data;
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetPointsDataStatusFormat>(_ => _.PID, _ => _.PointName, _ => _.DataSourceName, _ => _.ReferenceID, _ => _.IsActive, _ => _.DataStatus); }
                }

                protected override String NameField { get { return PropHelper.G<GetPointsDataStatusFormat>(_ => _.PointName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetPointsDataStatusFormat>(_ => _.PID); } }

                protected override String Name { get { return typeof(Point).Name; } }

                protected override Type Entity { get { return typeof(GetPointsDataStatusFormat); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID, ddlBuilding.SelectedValue }); } }

                protected override Boolean HideTabMessage { get { return true; } }

            #endregion

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindBuildings(ddlBuilding);
            }

            #region ddl events

                protected void ddlBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    if (ddlBuilding.SelectedValue != "-1") BindData(true); //reset and re-cache data on each index change for building
                }

            #endregion

        #endregion

        #region methods

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID), true);
                }

            #endregion

        #endregion
    }
}