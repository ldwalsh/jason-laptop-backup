﻿using CW.Business;
using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.System
{
    public partial class AnalysesCheck : AdminUserControlGrid
    {
        #region const

            const string initialValue = "-1";

        #endregion

        #region fields

            int cid;
            string className = typeof(AnalysesCheck).Name;
            ListItem viewAll = new ListItem() { Text = "View All", Value = initialValue };
            string buildingNameField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.BuildingName);
            string analysisNameField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.AnalysisName);
            string equipmentNameField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.EquipmentName);
            string lastExectionStartDateField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.LastExectionStartDate);
            string lastExectionEndDateField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.LastExectionEndDate);
            string lastStatusCodeField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.LastStatusCode);
            string analysisRangeField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.AnalysisRange);

        #endregion

        #region enums

            enum Statuses { ViewAll, Success, Error, NoData, Incomplete, Exists, ConfigIssue, MatlabError}
            enum AnalysesCheckDetails { Default };

        #endregion

        #region structs

            struct LI
            {
                public string Text { get; set; }
                public string Value { get; set; }
            }

        #endregion
        
        #region properties

            #region overrides

                protected override bool HideTabMessage { get { return true; } }
        
                protected override IEnumerable GetEntitiesForGrid(IEnumerable<string> searchCriteria)
                {
                    var bid = int.Parse(ddlBuilding.SelectedValue);
                    var analysisRange = ddlAnalysisRange.SelectedValue;
                    var aid = int.Parse(ddlAnalysis.SelectedValue);
                    var eid = int.Parse(ddlEquipment.SelectedValue);
                    var selectedStatus = (Statuses)Enum.Parse(typeof(Statuses), ddlStatus.SelectedValue);
                    var dataMgr = new PerfRecordDataMapper(DataMgr, LogMgr);

                    var analyses = (aid != -1) ? dataMgr.GetByPartitionAndRowKey(bid, analysisRange, aid) : dataMgr.GetByPartitionKey(bid, analysisRange);
            
                    if (eid != -1) analyses = analyses.Where(_ => _.EID == eid);

                    switch (selectedStatus)
                    {
                        case Statuses.Success:
                        case Statuses.Error:
                        case Statuses.NoData:
                        case Statuses.Exists:
                        case Statuses.ConfigIssue:
                        case Statuses.MatlabError:
                            analyses = analyses.Where(_ => _.LastStatusCode == selectedStatus.ToString() && _.LastExectionStartDate <= _.LastExectionEndDate);
                            break;
                        case Statuses.Incomplete:
                            analyses = analyses.Where(_ => _.LastExectionStartDate > _.LastExectionEndDate);
                            break;
                    }

                    return GetTransformData(analyses);
                }

                protected override IEnumerable<string> GridColumnNames
                {
                    get
                    {
                        return PropHelper.F<GetPerfMatlabRecordData>(_ => _.Key,
                                                                     _ => _.BID,
                                                                     _ => _.BuildingName,
                                                                     _ => _.AID,
                                                                     _ => _.AnalysisName,
                                                                     _ => _.EID,
                                                                     _ => _.EquipmentName,
                                                                     _ => _.LastExectionStartDate,
                                                                     _ => _.LastExectionEndDate,
                                                                     _ => _.LastStatusCode,
                                                                     _ => _.AnalysisRange,
                                                                     _ => _.Min,
                                                                     _ => _.Max,
                                                                     _ => _.Average,
                                                                     _ => _.Executions,
                                                                     _ => _.CollectionStartDate);
                    }
                }

                protected override string NameField { get { return analysisNameField; } }

                protected override string IdColumnName { get { return PropHelper.G<GetPerfMatlabRecordData>(_ => _.Key); } }

                protected override string Name { get { return FormatValue(className); } }

                protected override Type Entity { get { return typeof(GetPerfMatlabRecordData); } }

                protected override string CacheHashKey { get { return FormatHashKey(new object[] { siteUser.CID, ddlBuilding.SelectedValue }); } }

            #endregion

        #endregion

        #region events

            void Page_FirstLoad()
            {
                BindBuildings(ddlBuilding);
                BindAnalysisRanges(ddlAnalysisRange);
                BindAnalyses(ddlAnalysis);

                ddlEquipment.Items.Add(viewAll);

                BindStatuses(ddlStatus);

                SetColumnAttributes();
            }

            void Page_Init()
            {
                UseKeyNotId = true;
            }

            void Page_Load() {
                cid = siteUser.CID;

                if (Page.IsPostBack)
                {
                   if(grid.HeaderRow != null) CreateHeaderInfo(grid.HeaderRow.Cells);
                }
            }

            #region button events

                protected void AnalysesCheck_Click(object sender, EventArgs e)
                {
                    pnlDetailsView.Visible = false;
                    DetailsViewData = null;
                    BindData(true);
                }

            #endregion

            #region ddl events

                protected void ddlBuilding_SelectedIndexChanged(object sender, EventArgs e) { BindEquipment(ddlEquipment); }

        #endregion

            #region grid events

                #region overrides

                    protected override void OnGridDataBound(Object sender, GridViewRowEventArgs e)
                    {
                        if (e.Row.RowType == DataControlRowType.Header)
                        {
                            CreateHeaderInfo(e.Row.Cells);                            
                        }

                        base.OnGridDataBound(sender, e);
                    }

                #endregion

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var analysesCheck = gridCacher.Get().Rows.Single(_ => _.ID == CurrentKey).Cells;

                    SetAnalysesCheckIntoViewState(analysesCheck);

                    SelectedKey = CurrentKey; //make sure to update the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView() { SetKeyValuePairItemsToRepeater(Convert.ToInt32(AnalysesCheckDetails.Default), rptAnalysesCheckDetails); }              

            #endregion

            #region binders

                void BindBuildings(DropDownList ddl) { BindDDL(ddl, buildingNameField, PropHelper.G<PerfMatlabRecord>(_ => _.BID), DataMgr.BuildingDataMapper.GetAllBuildingsByCID(cid), true); }

                void BindAnalysisRanges(DropDownList ddl)
                {
                    var analysisRanges = new[] 
                    {
                        Enum.GetName(typeof(DataConstants.AnalysisRange), DataConstants.AnalysisRange.Daily),
                        Enum.GetName(typeof(DataConstants.AnalysisRange), DataConstants.AnalysisRange.Weekly),
                        Enum.GetName(typeof(DataConstants.AnalysisRange), DataConstants.AnalysisRange.Monthly),
                        Enum.GetName(typeof(DataConstants.AnalysisRange), DataConstants.AnalysisRange.HalfDay),
                    };

                    BindListItems(ddl, analysisRanges);
                }

                void BindAnalyses(DropDownList ddl) { BindDDL(ddl, analysisNameField, PropHelper.G<PerfMatlabRecord>(_ => _.AID), DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByCID(cid), true, viewAll); }
        
                void BindEquipment(DropDownList ddl) { BindDDL(ddl, equipmentNameField, PropHelper.G<PerfMatlabRecord>(_ => _.EID), DataMgr.EquipmentDataMapper.GetEquipmentByBID(int.Parse(ddlBuilding.SelectedValue)), true, viewAll); }

                void BindStatuses(DropDownList ddl) { BindListItems(ddl, Enum.GetNames(typeof(Statuses)), false); }

                void BindListItems(DropDownList ddl, string[] items, bool clearItems = true)
                {
                    var listItems = new List<LI>();

                    foreach (var item in items)
                        listItems.Add(new LI() { Text = FormatValue(item), Value = item });

                    BindDDL(ddl, PropHelper.G<LI>(_ => _.Text), PropHelper.G<LI>(_ => _.Value), listItems, clearItems);
                }

            #endregion

            IEnumerable<GetPerfMatlabRecordData> GetTransformData(IEnumerable<PerfMatlabRecord> perfMatlabRecords) //perhaps this can be moved to a transform layer?
            {
                var getPerfMatlabRecordDataItems = new List<GetPerfMatlabRecordData>();
                using (var db = new CWDataContext(DataMgr.ConnectionString))
                {

                    var results = (from pmlr in perfMatlabRecords
                                   join b in db.Buildings on pmlr.BID equals b.BID
                                   join a in db.Analyses on pmlr.AID equals a.AID
                                   join e in db.Equipments on pmlr.EID equals e.EID
                                   select new { BID = b.BID, AID = a.AID, EID = e.EID, BuildingName = b.BuildingName, AnalysisName = a.AnalysisName, EquipmentName = e.EquipmentName }).ToList();

                    foreach (var perfMatlabRecord in perfMatlabRecords)
                    {
                        var getPerfMatlabRecordDataItem = new GetPerfMatlabRecordData();
                        var analysisRange = perfMatlabRecord.AnalysisRange;
                        var bid = perfMatlabRecord.BID;
                        var aid = perfMatlabRecord.AID;
                        var eid = perfMatlabRecord.EID;

                        getPerfMatlabRecordDataItem.Key = string.Format("{0}_{1}_{2}_{3}", bid, analysisRange, aid, eid);
                        getPerfMatlabRecordDataItem.BID = bid;
                        getPerfMatlabRecordDataItem.AID = aid;
                        getPerfMatlabRecordDataItem.EID = eid;
                        getPerfMatlabRecordDataItem.BuildingName = results.Where(_ => _.BID == bid).First().BuildingName;
                        getPerfMatlabRecordDataItem.AnalysisName = results.Where(_ => _.AID == aid).First().AnalysisName;
                        getPerfMatlabRecordDataItem.EquipmentName = results.Where(_ => _.EID == eid).First().EquipmentName;
                        getPerfMatlabRecordDataItem.LastExectionStartDate = perfMatlabRecord.LastExectionStartDate;
                        getPerfMatlabRecordDataItem.LastExectionEndDate = perfMatlabRecord.LastExectionEndDate;
                        getPerfMatlabRecordDataItem.LastStatusCode = perfMatlabRecord.LastStatusCode;
                        getPerfMatlabRecordDataItem.AnalysisRange = analysisRange;
                        getPerfMatlabRecordDataItem.Min = perfMatlabRecord.Min;
                        getPerfMatlabRecordDataItem.Max = perfMatlabRecord.Max;
                        getPerfMatlabRecordDataItem.Average = perfMatlabRecord.Average;
                        getPerfMatlabRecordDataItem.Executions = perfMatlabRecord.Executions;
                        getPerfMatlabRecordDataItem.CollectionStartDate = perfMatlabRecord.CollectionStartDate;

                        getPerfMatlabRecordDataItems.Add(getPerfMatlabRecordDataItem);
                    }

                    return getPerfMatlabRecordDataItems.AsEnumerable();
                }
            }

            void SetColumnAttributes()
            {
                var columnsToDisplay = new[] { buildingNameField, analysisNameField, equipmentNameField, lastExectionStartDateField, lastExectionEndDateField, lastStatusCodeField, analysisRangeField };

                for (var i = 0; i < columnsToDisplay.Length; i++)
                {
                    var column = columnsToDisplay[i];

                    SetColumnAttributes(i + 1, column, FormatValue(column));
                }
            }

            string FormatValue(string value) { return Regex.Replace(value, "(\\B[A-Z])", " $1"); }

            void SetAnalysesCheckIntoViewState(IDictionary<string, string> analysesCheck)
            {
                var minField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.Min);
                var maxField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.Max);
                var averageField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.Average);
                var executionsField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.Executions);
                var collectionStartDateField = PropHelper.G<GetPerfMatlabRecordData>(_ => _.CollectionStartDate);

                SetKeyValuePairItem(FormatValue(buildingNameField), analysesCheck[buildingNameField]);
                SetKeyValuePairItem(FormatValue(analysisNameField), analysesCheck[analysisNameField]);
                SetKeyValuePairItem(FormatValue(equipmentNameField), analysesCheck[equipmentNameField]);
                SetKeyValuePairItem(FormatValue(lastExectionStartDateField) + " (UTC)", analysesCheck[lastExectionStartDateField]);
                SetKeyValuePairItem(FormatValue(lastExectionEndDateField) + " (UTC)", analysesCheck[lastExectionEndDateField]);
                SetKeyValuePairItem(FormatValue(lastStatusCodeField), analysesCheck[lastStatusCodeField], "{0}<p>(Note: Status Code of last Analysis for this record's Analysis / Equipment pair.)</p>");
                SetKeyValuePairItem(FormatValue(analysisRangeField), analysesCheck[analysisRangeField]);
                SetKeyValuePairItem(FormatValue(minField) + " (Milliseconds)", analysesCheck[minField], "{0}<p>(Note: The fastest analysis runtime for this record's Analysis / Equipment pair since Collection Start Date.)</p>");
                SetKeyValuePairItem(FormatValue(maxField) + " (Milliseconds)", analysesCheck[maxField], "{0}<p>(Note: The slowest analysis runtime for this record's Analysis / Equipment pair since Collection Start Date.)</p>");
                SetKeyValuePairItem(FormatValue(averageField) + " (Milliseconds)", analysesCheck[averageField], "{0}<p>(Note: The average analysis runtime for this record's Analysis / Equipment pair since Collection Start Date.)</p>");
                SetKeyValuePairItem(FormatValue(executionsField), analysesCheck[executionsField], "{0}<p>(Note: The number of executions since Collection Start Date.)</p>");
                SetKeyValuePairItem(FormatValue(collectionStartDateField) + " (UTC)", analysesCheck[collectionStartDateField], "{0}<p>(Note: Date that the metrics collection started.)</p>");

                SetDetailsViewDataToViewState(Convert.ToInt32(AnalysesCheckDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            void CreateHeaderInfo(TableCellCollection cells)
            {
                foreach (TableCell cell in cells)
                {
                    bool requiredHeaderInfo = false;

                    var div = new HtmlGenericControl("DIV");
                    div.Attributes.Add("class", "gridHeaderInfo");

                    foreach (Control ctl in cell.Controls)
                    {
                        if (ctl.GetType().ToString().Contains("DataControlLinkButton"))
                        {
                            switch (((LinkButton)ctl).Text)
                            {
                                case "Last Status Code":
                                    div.Attributes.Add("title", "Status Code of last Analysis for this record's Analysis / Equipment pair.");
                                    requiredHeaderInfo = true;
                                    break;
                            }
                        }

                        if (requiredHeaderInfo) break;
                    }

                    if (requiredHeaderInfo) cell.Controls.Add(div);
                }
        }

        #endregion
    }
}