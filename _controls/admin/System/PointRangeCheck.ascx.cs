﻿using CW.Data;
using CW.Data.Models.Point;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.System
{
    public partial class PointRangeCheck : AdminUserControlGrid
    {
        #region properties

            #region overrides

                protected override Int32 PageSize { get { return 10000; } }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    return DataMgr.PointDataMapper.SearchPointsWithPartialDataByBIDAsRangeStatusFormat(searchCriteria.FirstOrDefault(),
                                                                                                       Convert.ToDateTime(txtPointRangeDate.SelectedDate),
                                                                                                       (ddlBuilding.SelectedValue != "-1") ? Convert.ToInt32(ddlBuilding.SelectedValue) : 0,
                                                                                                       (ddlEquipmentClass.SelectedValue != "-1") ? Convert.ToInt32(ddlEquipmentClass.SelectedValue) : 0,
                                                                                                       (ddlEquipmentType.SelectedValue != "-1") ? Convert.ToInt32(ddlEquipmentType.SelectedValue) : 0,
                                                                                                       (ddlEquipment.SelectedValue != "-1") ? Convert.ToInt32(ddlEquipment.SelectedValue) : 0);
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get 
                    { 
                        return PropHelper.F<GetPointsDataStatusFormat>(_ => _.PID, 
                                                                       _ => _.PointName,
                                                                       _ => _.EquipmentName,
                                                                       _ => _.PointTypeName,
                                                                       _ => _.DataSourceName, 
                                                                       _ => _.ReferenceID,
                                                                       _ => _.Min,
                                                                       _ => _.Max,
                                                                       _ => _.EngUnits,
                                                                       _ => _.Addition,
                                                                       _ => _.Multiply,
                                                                       _ => _.Power,
                                                                       _ => _.DataStatus,
                                                                       _ => _.CalculatedMin,
                                                                       _ => _.CalculatedMax); 
                    }
                }

                protected override String NameField { get { return PropHelper.G<GetPointsDataStatusFormat>(_ => _.PointName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetPointsDataStatusFormat>(_ => _.PID); } }

                protected override String Name { get { return typeof(Point).Name; } }

                protected override Type Entity { get { return typeof(GetPointsDataStatusFormat); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID, txtPointRangeDate.SelectedDate, ddlBuilding.SelectedValue }); } }

                protected override Boolean HideTabMessage { get { return true; } }

            #endregion

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindBuildings(ddlBuilding);
                BindEquipmentClasses(ddlEquipmentClass, -1);
                BindEquipmentTypes(ddlEquipmentType, -1, -1);
                BindEquipment(ddlEquipment, -1, -1, -1);
            }

            private void Page_Load()
            {
                PointRangeDownloadArea.DropDownLists = new List<DropDownList>() { ddlBuilding, ddlEquipmentClass, ddlEquipmentType, ddlEquipment };
                PointRangeDownloadArea.RadDatePicker = txtPointRangeDate;
            }

            #region ddl events

                protected void ddlBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    var isBIDSelected = (ddlBuilding.SelectedValue != "-1");

                    ControlHelper.ToggleVisibility(isBIDSelected, new Control[] { PointRangeDownloadArea });
                    PointRangeDownloadArea.FindControl("lblError").Visible = false;

                    BindEquipmentClasses(ddlEquipmentClass, Convert.ToInt32(ddlBuilding.SelectedValue));
                    BindEquipmentDDL();
                    HideGrid();
                }

                protected void ddlEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipmentTypes(ddlEquipmentType, Convert.ToInt32(ddlBuilding.SelectedValue), Convert.ToInt32(ddlEquipmentClass.SelectedValue));
                    BindEquipmentDDL();
                    HideGrid();
                }

                protected void ddlEquipmentType_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipmentDDL();
                    HideGrid();
                }

                protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    HideGrid();
                }

            #endregion

            #region button events

                protected void btnPointRangeCheck_Click(Object sender, EventArgs e)
                {
                    txtSearch.Text = String.Empty;
                    BindData(true);
                }           

            #endregion

        #endregion

        #region methods

            private void BindEquipmentDDL()
            {
                BindEquipment(ddlEquipment, Convert.ToInt32(ddlBuilding.SelectedValue), Convert.ToInt32(ddlEquipmentClass.SelectedValue), Convert.ToInt32(ddlEquipmentType.SelectedValue));
            }

            private void HideGrid()
            {
                pnlSearch.Visible = false;
                grid.DataSource = null;
                grid.DataBind();
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID), true);
                }

                private void BindEquipmentClasses(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = "View all...", Value = "-1", Selected = true };

                    BindDDL(ddl, "EquipmentClassName", "EquipmentClassID", (bid != -1) ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid) : null, true, lItem);
                }

                private void BindEquipmentTypes(DropDownList ddl, Int32 bid, Int32 ecid)
                {
                    var lItem = new ListItem() { Text = "View all...", Value = "-1", Selected = true };
                    var equipmentTypes = Enumerable.Empty<EquipmentType>();

                    if (bid != -1 && ecid != -1) 
                        equipmentTypes = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByBuildingIDAndEquipmentClassID(bid, ecid);
                    else if (bid != -1) 
                        equipmentTypes = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByBuildingID(bid);

                    BindDDL(ddl, "EquipmentTypeName", "EquipmentTypeID", equipmentTypes.Any() ? equipmentTypes : null, true, lItem);
                }

                private void BindEquipment(DropDownList ddl, Int32 bid, Int32 ecid, Int32 etid)
                {
                    var lItem = new ListItem() { Text = "View all...", Value = "-1", Selected = true };
                    var equipment = Enumerable.Empty<CW.Data.Equipment>();

                    if (bid != -1 && etid != -1)
                        equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByBIDAndEquipmentTypeID(bid, etid);
                    else if (bid != -1 && ecid != -1 && etid == -1)
                        equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(ecid, bid);
                    else if (bid != -1)
                        equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid);

                    BindDDL(ddl, "EquipmentName", "EID", equipment.Any() ? equipment : null, true, lItem);
                }

            #endregion

        #endregion
    }
}