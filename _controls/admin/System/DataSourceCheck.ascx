﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="DataSourceCheck.ascx.cs" Inherits="CW.Website._controls.admin.System.DataSourceCheck" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Data.Models.DataSource" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Panel ID="pnlHealthCheckByBuilding" runat="server" DefaultButton="btnHealthCheckByBuilding">

  <h2>Data Sources Check</h2>

  <p>Checks if data has been received for all datasources for a specified client and date range. This may take several minutes to run.</p>

  <p>
    <a id="lnkSetFocus" href="#" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
  </p>
   
  <div class="divForm">
    <div class="quickDates">Prior:
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 48, 'hours', true);">2 days</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 24, 'hours', true);">1 day</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 12, 'hours', true);">12 hrs</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 6, 'hours', true);">6 hrs</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 1, 'hours', true);">1 hr</a>
      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 10, 'minutes', true);">10 mins</a>
    </div>
  </div>
  <div class="divForm">
    <label class="label">Start Date (UTC):</label>    
    <telerik:RadDateTimePicker ID="txtStartDate" runat="server"></telerik:RadDateTimePicker>  
    <asp:RequiredFieldValidator ID="startDateRFV" runat="server" ErrorMessage="Date is a required field." ControlToValidate="txtStartDate" SetFocusOnError="true" Display="None" ValidationGroup="DataSourceCheck" />
    <ajaxToolkit:ValidatorCalloutExtender ID="startDateVCE" runat="server" BehaviorID="startDateVCE" TargetControlID="startDateRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
  <div class="divForm">
    <label class="label">End Date (UTC):</label>
    <telerik:RadDateTimePicker ID="txtEndDate" runat="server"></telerik:RadDateTimePicker>  
    <asp:RequiredFieldValidator ID="endDateRFV" runat="server" ErrorMessage="End date is a required field." ControlToValidate="txtEndDate" SetFocusOnError="true" Display="None" ValidationGroup="DataSourceCheck" />
    <ajaxToolkit:ValidatorCalloutExtender ID="endDateVCE" runat="server" BehaviorID="endDateVCE" TargetControlID="endDateRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
    <asp:CustomValidator ID="endDateCV" runat="server" ClientValidationFunction="RadDatePickerDatetimeComparer" ControlToValidate="txtEndDate" EnableClientScript="true" ErrorMessage="Invalid range." Display="None" ValidationGroup="DataSourceCheck" />    
    <ajaxToolkit:ValidatorCalloutExtender ID="endDateCVVCE" runat="server" BehaviorID="endDateCVVCE" TargetControlID="endDateCV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="Right" Width="175" />
  </div>
  <div class="divForm">
    <label class="label">Include Inactive Data Sources</label>
    <asp:CheckBox ID="chkIncludeInactiveDataSources" runat="server" CssClass="checkbox" />
  </div> 
  <div class="divForm">
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>
  </div> 
  <div class="divForm">   
    <label class="label">*Select Building:</label>
    <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="dropdown" AppendDataBoundItems="true" />
    <asp:RequiredFieldValidator ID="buildingRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="DataSourceCheck" />
    <ajaxToolkit:ValidatorCalloutExtender ID="buildingVCE" runat="server" BehaviorID="buildingVCE" TargetControlID="buildingRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
  <br/>

  <asp:LinkButton ID="btnHealthCheckByBuilding" runat="server" Text="Run" CssClass="lnkButton" OnClick="btnHealthCheckByBuilding_ButtonClick" ValidationGroup="DataSourceCheck" />

  <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" visible="false">
    <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
  </asp:Panel>

  <div id="gridTbl">
    <asp:GridView ID="grid" EnableViewState="true" runat="server">
      <PagerSettings PageButtonCount="20" />
      <HeaderStyle CssClass="tblTitle" />
      <AlternatingRowStyle CssClass="tblCol2" />
      <RowStyle CssClass="tblCol1" Wrap="true" />
      <Columns>
        <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataSourceName" HeaderText="Data Source Name">  
          <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.DataSourceName)) %>"><%# GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.DataSourceName), 35) %></label></ItemTemplate>
        </asp:TemplateField>   

        <asp:TemplateField ItemStyle-Wrap="true" SortExpression="IPList"  HeaderText="IP">  
          <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.IPList)) %>"><%# GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.IPList), 30) %></label></ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ReferenceID" HeaderText="ReferenceID"> 
          <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.ReferenceID)) %>"><%# GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.ReferenceID), 35) %></label></ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Integrated">  
          <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.IsIntegrated)) %></ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Active">  
          <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.IsActive)) %></ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Health Status" SortExpression="DataStatus">
          <ItemTemplate>
            <%# "<span class='" + (GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.DataStatus)).ToString() == "Good" ? "statusGreen" 
                                : (GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.DataStatus)).ToString() == "Bad" ? "statusRed" : "statusBlue")) + "'>" 
                                +  GetCellContent(Container, PropHelper.G<GetDataSourcesDataStatusFormat>(_ => _.DataStatus)).ToString() + "</span>" %>
          </ItemTemplate>
        </asp:TemplateField>
      </Columns>
    </asp:GridView>
  </div>

</asp:Panel>