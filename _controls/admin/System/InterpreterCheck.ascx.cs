﻿using CW.Utility;
using CW.Utility.Web;
using System;
using System.Configuration;
using System.IO;
using System.Net;

namespace CW.Website._controls.admin.System
{
    public partial class InterpreterCheck : AdminUserControlBase
    {
        #region constants

            const String interpreterCheckSuccess = "Success";
            const String interpreterCheckException = "An exception was thrown: {0}";
            const String interpreterLocalhost = "Cannot perform this test locally beacause the interpreter has a unknown local host casini port.";

        #endregion

        #region events

            private void Page_Load()
            {
                lblInterpreterCheckError.Visible = lblInterpreterCheckSuccess.Visible = divInterpreterCheck.Visible = false;
            }

            #region button events

                protected void btnInterpreterCheck_ButtonClick(Object sender, EventArgs e)
                {
                    if ((!LinkHelper.GetBaseUrl(Request).Contains("localhost") && !LinkHelper.GetBaseUrl(Request).Contains("127.0.0.1") && !LinkHelper.GetBaseUrl(Request).Contains("127.0.0.2")))
                    {
                        StreamReader streamReader = null;
                        WebResponse response = null;

                        try
                        {
                            var uri = ConfigurationManager.AppSettings["InterpreterSiteAddress"] + "Interpreter.aspx?systemtest=true";
                            var request = (HttpWebRequest)WebRequest.Create(uri);

                            response = (WebResponse)request.GetResponse();
                            streamReader = new StreamReader(response.GetResponseStream());
                            interpreterCheck.Text = streamReader.ReadToEnd();

                            streamReader.Close();
                            response.Close();

                            LabelHelper.SetLabelMessage(lblInterpreterCheckSuccess, interpreterCheckSuccess);
                            divInterpreterCheck.Visible = true;
                            return;
                        }
                        catch (Exception ex)
                        {
                            if (streamReader != null) streamReader.Close();
                            if (response != null) response.Close();

                            LabelHelper.SetLabelMessage(lblInterpreterCheckError, String.Format(interpreterCheckException, ex));
                            return;
                        }
                    }
                    else
                        LabelHelper.SetLabelMessage(lblInterpreterCheckError, interpreterLocalhost);
                }

            #endregion

        #endregion
    }
}