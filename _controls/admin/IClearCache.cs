﻿using System;
using System.Collections.Generic;

namespace CW.Website._controls.admin
{
    public interface IClearCache
    {
        void ClearCache(Int32 cid, IEnumerable<Int32> bids, Int32 uid);
    }
}