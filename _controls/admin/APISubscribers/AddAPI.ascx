﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddAPI.ascx.cs" Inherits="CW.Website._controls.admin.APISubscribers.AddAPI" %>
<asp:Panel ID="pnlAddAPI" runat="server" DefaultButton="btnAddAPI">
    <script type="text/javascript">
        function onAPIContainerChanged(ddl) {
            var publicEndpoint = document.getElementById('<%=lblPublicEndpoint.ClientID %>');
            var baseEndpoint = document.getElementById('<%=addAPIEndpoint.ClientID %>').value;
            var suffixes = document.getElementById('<%=suffixes.ClientID %>');
            
            var apiEndPoint = baseEndpoint;
            if ("" != ddl.options[ddl.selectedIndex].value) {
                var array = suffixes.value.split('|');
                var index = ddl.selectedIndex - 1;
                if (array.length > 0 && index < array.length) {
                    var suffix = array[index];
                    if (suffix.length > 0) {
                        apiEndPoint += suffix + "/";
                    }
                    var text = "";
                    var ddlAddAPIs = document.getElementById('<%=ddlAddAPIs.ClientID %>');
                    if ("" != ddlAddAPIs.options[ddlAddAPIs.selectedIndex].value) {
                        text = ddlAddAPIs.options[ddlAddAPIs.selectedIndex].text;
                        if (text.length > 0) {
                            text = text.replace(/^\W+/, '');
                            text = text.replace(/^\s+/, '');
                            text = text.replace(/[^0-9a-zA-Z{}/]/g, '');
                        }
                    }
                    apiEndPoint += text;
                }
            }
            publicEndpoint.innerText = apiEndPoint;
        }
        function onAPIChanged(ddl) {
            var publicEndpoint = document.getElementById('<%=lblPublicEndpoint.ClientID %>');
            var baseEndpoint = document.getElementById('<%=addAPIEndpoint.ClientID %>').value;
            var suffixes = document.getElementById('<%=suffixes.ClientID %>');
            
            var apiEndPoint = baseEndpoint;
            var verb = "", options = "";
            if ("" != ddl.options[ddl.selectedIndex].value) {
                var array = ddl.options[ddl.selectedIndex].value.split('_');
                if (array.length > 0) {
                    verb = array[0];
                    options = array[2] == "true" ? document.getElementById('<%=txtSupportsOptions.ClientID %>').value : "";
                }
                array = suffixes.value.split('|');
                var ddlAddAPIContainers = document.getElementById('<%=ddlAddAPIContainers.ClientID %>');
                var index = ddlAddAPIContainers.selectedIndex - 1;
                if (array.length > 0 && index >= 0 && index < array.length) {
                    var suffix = array[index];
                    if (suffix.length > 0) {
                        apiEndPoint += suffix + "/";
                    }
                    var text = ddl.options[ddl.selectedIndex].text;
                    if (text.length > 0) {
                        text = text.replace(/^\W+/, '');
                        text = text.replace(/^\s+/, '');
                        text = text.replace(/[^0-9a-zA-Z{}/]/g, '');
                    }
                    apiEndPoint += text;
                }
            }
            publicEndpoint.innerText = apiEndPoint;
            document.getElementById('<%=lblAPIVerb.ClientID %>').innerText = verb + options;
        }
    </script>
  <h2>Add API</h2>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />
    <div id="divAddAPIContainer" class="divForm" runat="server">   
        <label class="label">*Select API Container:</label>    
        <asp:DropDownList ID="ddlAddAPIContainers" CssClass="dropdown"  AppendDataBoundItems="true" AutoPostBack="false" runat="server">    
            <asp:ListItem Value="">Select one...</asp:ListItem>                          
        </asp:DropDownList> 
        <asp:HiddenField ID="suffixes" Value="" runat="server" />
    </div> 

    <div class="divForm">
      <label class="label">*Name:</label>
      <asp:TextBox ID="txtAddAPIName" CssClass="textbox" MaxLength="20" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">*Description:</label>
      <asp:TextBox ID="txtAddAPIDescription" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>     
    <div class="divForm">
      <label class="label">*API:</label>
      <asp:DropDownList ID="ddlAddAPIs" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
          <asp:ListItem Value="">Select one...</asp:ListItem>
      </asp:DropDownList>
      &nbsp;<label class="labelContent" id="lblAPIVerb" runat="server"></label>
      <asp:HiddenField ID="txtSupportsOptions" runat="server" Value="" />
    </div>     
    <div class="divForm">
      <label class="label">External Endpoint:</label>
      <asp:Label ID="lblPublicEndpoint" CssClass="labelContent" runat="server"/>
      <asp:HiddenField ID="addAPIEndpoint" runat="server" Value="" />
    </div>
    <div id="APIThrottlingAndQuotasArea" runat="server">
    <br />
    <hr />
    <asp:CheckBox ID="enableThrottling" runat="server" Checked="false" Text="Throttling"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtRateLimit" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txtRateLimit" Width="100" Step="1" />
      <label class="labelContent" id="trlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtRateLimit" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" TargetControlID="txtRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="trplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtRenewalPeriod" FilterType="Numbers" />
    </div>
    <hr />
    <asp:CheckBox ID="enableQuota" runat="server" Checked="false" Text="Quota"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtNumberCalls" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="txtNumberCalls" Width="100" Step="1" />
      <label class="labelContent" id="qrlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtNumberCalls" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtQuotaRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender4" runat="server" TargetControlID="txtQuotaRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="qrplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtQuotaRenewalPeriod" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Bandwidth:</label>
      <asp:TextBox ID="txtKiloBytes" CssClass="textbox" MaxLength="10" Width="100" runat="server"/>&nbsp;KB
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtKiloBytes" FilterType="Numbers" />
    </div>
    </div>
    <asp:LinkButton CssClass="lnkButton" ID="btnAddAPI" runat="server" Text="Add API"  OnClick="addAPIButton_Click" ValidationGroup="AddAPI" />
  </div>

  <asp:RequiredFieldValidator ID="apiContainerRequiredValidator" runat="server" ErrorMessage="API Container is a required field." ControlToValidate="ddlAddAPIContainers" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="AddAPI" />
  <ajaxToolkit:ValidatorCalloutExtender ID="apiContainerRequiredValidatorExtender" runat="server" TargetControlID="apiContainerRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="txtAddAPINameRFV" runat="server" ErrorMessage="API Name is a required field." ControlToValidate="txtAddAPIName" SetFocusOnError="true" Display="None" ValidationGroup="AddAPI" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtAddAPINameVCE" runat="server" BehaviorID="txtAddAPINameVCE" TargetControlID="txtAddAPINameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="txtAddAPIDescriptionRFV" runat="server" ErrorMessage="API Description is a required field." ControlToValidate="txtAddAPIDescription" SetFocusOnError="true" Display="None" ValidationGroup="AddAPI" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtAddAPIDescriptionVCE" runat="server" BehaviorID="txtAddAPIDescriptionVCE" TargetControlID="txtAddAPIDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="apiRequiredValidator" runat="server" ErrorMessage="API is a required field." ControlToValidate="ddlAddAPIs" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="AddAPI" />
  <ajaxToolkit:ValidatorCalloutExtender ID="apiRequiredValidatorExtender" runat="server" TargetControlID="apiRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
</asp:Panel>    
