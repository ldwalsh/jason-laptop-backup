﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewAPISubscribers.ascx.cs" Inherits="CW.Website._controls.admin.APISubscribers.ViewAPISubscribers" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Data.Models.APISubscriber" %>
<%@ Import Namespace="CW.Utility" %>

<h2>View API Subscribers</h2>
    <script type="text/javascript">
        function generatePwd() {
            try {
                var txtPwdObj = document.getElementById('<%=txtEditPassword.ClientID %>');
                var info = document.getElementById('<%=info.ClientID %>');
                info.innerText = "";
                var url = "/_framework/httpHandlers/GenerateSubscriberPassword.ashx";
                $.get(url, function (data, textStatus) {
                    txtPwdObj.value = data;
                    info.innerText = "<%=notePwd %>";
                });
		    }
		    catch (err) {
		        alert(err);
		    }
        }
        function clearPwd() {
            var txtPwdObj = document.getElementById('<%=txtEditPassword.ClientID %>');
            txtPwdObj.value = "";
            document.getElementById('<%=info.ClientID %>').innerText = "";
        }
        function getProductIdFromCheckboxId(cbId) {
            return (cbId.substr(cbId.lastIndexOf('_') + 1));
        }

        function setSelectedAll(checked) {
            var s = "";
            if (checked) {
                var arr = [];
                $('input:checkbox:not("#cbAll")').each(function () {
                    var $this = $(this);
                    $this.prop('checked', checked);
                    var cbId = $this.prop("id");
                    arr.push(getProductIdFromCheckboxId(cbId));
                });
                s = arr.join();
                if (s.length > 0) {
                    if (',' == s[0]) {
                        s = s.substr(1); // remove leading comma
                    }
                }
            }
            else {
                $('input:checkbox:not("#cbAll")').prop('checked', false);
            }

            var obj = document.getElementById('<%=productCBIDs.ClientID %>');
            obj.value = s;
        }

        function setSelected(cbId, checked) {
            cbId = getProductIdFromCheckboxId(cbId);
            var obj = document.getElementById('<%=productCBIDs.ClientID %>');
            var arr = obj.value.split(",");
            if (checked) {
                arr.splice(arr.length, 0, cbId);
            }
            else {
                var index = arr.indexOf(cbId);
                if (index >= 0) {
                    arr.splice(index, 1);
                }
            }
            var s = "";
            if (arr.length > 0) {
                s = arr.join();
            }
            if (s.length > 0) {
                if (',' == s[0]) {
                    s = s.substr(1); // remove leading comma
                }
            }
                
            obj.value = s;
        }
    </script>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>  
                                                                 
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>       
                                                                                                                
<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="false" />
      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/select.png" ID="btnSelect" runat="server"  CausesValidation="false" ToolTip="View this Subscriber" CommandName="Select"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/edit.png" ID="btnEdit" runat="server"  CausesValidation="false" ToolTip="Edit this Subscriber" CommandName="Edit"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="OrganizationName" HeaderText="Organization Name">
        <ItemTemplate>
            <label title="<%# GetCellContent(Container, PropHelper.G<GetAPISubscriberData>(_ => _.OrganizationName)) %>"><%# GetCellContent(Container, PropHelper.G<GetAPISubscriberData>(_ => _.OrganizationName), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Email" HeaderText="Email">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<GetAPISubscriberData>(_ => _.Email)) %>"><%# GetCellContent(Container, PropHelper.G<GetAPISubscriberData>(_ => _.Email), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="SubscriberID" HeaderText="SubscriberID" Visible="false">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetAPISubscriberData>(_ => _.SubscriberID)) %>"><%# GetCellContent(Container, PropHelper.G<GetAPISubscriberData>(_ => _.SubscriberID), 50) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/delete.png" ID="btnDelete" runat="server" CausesValidation="false" ToolTip="Delete this Subscriber" CommandName="Delete" OnClientClick="return confirm('Are you sure you wish to delete this API Subscriber permanently?\r\nPlease note that any subscriptions this user may have will also be deleted.');"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>        
  </asp:GridView>                                                                      
</div>
<br />
<br />

<div>
  <!--SELECT DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptAPISubscriberDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>API Subscriber Details</h2>
          <ul id="apiSubscriberDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
          <br />
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>

<!--EDIT PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateAPISubscriber">                                                                                             
  <div>
    <h2>Edit API Subscriber</h2>
  </div>

  <div>
    <asp:HiddenField ID="hdnSubscriberId" runat="server" Visible="false" />
    <a id="lnkSetFocusEdit" runat="server"></a>
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnEditAPISubscriberID" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">Organization:</label>
      <label id="lblEditAPISubscriberOrganization" class="labelContent" runat="server" ></label>
    </div>
    <div class="divForm">
      <label class="label">User:</label>
      <label id="lblEditAPISubscriberUser" class="labelContent" runat="server" ></label>
    </div>     
                                                                                                                                                     
    <div class="divForm">
      <label class="label">Password:</label>
      <table class="labelContent">
          <tr>
              <td>
                <asp:TextBox ID="txtEditPassword" CssClass="textbox" runat="server" ReadOnly="true" />
              </td>
              <td style="padding-left:5px;"><img src="/_assets/styles/images/refresh.jpg" class="imgBtn" onclick="generatePwd();" alt="Generate a new Password" /></td>
              <td style="padding-left:5px;"><img src="/_assets/images/delete.png" class="imgBtn" onclick="clearPwd();" alt="Clear Password" /></td>
          </tr>
      </table>
    </div>
    <div class="divForm">
      <label class="label"></label>
      <label class="labelContent" id="info" runat="server"></label>
    </div>
    <div class="divForm">
        <label class="label">Select Product(s) to subscribe to:</label>
        <asp:ListView runat="server" ID="subscribedProducts" ClientIDMode="Predictable" ClientIDRowSuffix="ID">
            <LayoutTemplate>
            <table id="table1" class="richText labelContent" runat="server" style="border:thin solid gray!important; padding:0px!important">
                <tr>
                    <th class="richText" style="padding-left:2px!important">
                        <input type="checkbox" id="cbAll" onclick="setSelectedAll(this.checked);"/>
                    </th>
                    <th class="richText">Name</th>
                    <th class="richText">Primary Subscriber Key</th>
                    <th class="richText">Secondary Subscriber Key</th>
                </tr>
                <tr runat="server" id="itemPlaceholder" ></tr>
            </table>
            </LayoutTemplate>
            <ItemTemplate>
            <tr>
                <td class="richText">
                    <asp:CheckBox runat="server" onclick="setSelected(this.id, this.checked);" Checked=<%#Eval("Subscribed") %>/>
                </td>
                <td class="richText" style="text-align:left"><asp:Label runat="server" ID="lblId"><%#Eval("Name") %></asp:Label></td>
                <td class="richText" style="text-align:left"><asp:Label runat="server" ID="lblName"><%#Eval("PrimaryKey") %></asp:Label></td>
                <td class="richText" style="text-align:left"><asp:Label runat="server" ID="lblType"><%#Eval("SecondaryKey") %></asp:Label></td>
            </tr>
            </ItemTemplate>
        </asp:ListView>
        <asp:HiddenField ID="productCBIDs" Value="" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Notes:</label>
      <asp:TextBox ID="txtNotesEdit" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateAPISubscriber" runat="server" Text="Update API Subscriber"  OnClick="updateAPISubscriberButton_Click" ValidationGroup="EditAPISubscriber" />

  </div>
                                                
  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="txtNotesEditRFV" runat="server" ErrorMessage="Notes is a required field." ControlToValidate="txtNotesEdit" SetFocusOnError="true" Display="None" ValidationGroup="EditAPISubscriber" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtNotesEditVCE" runat="server" BehaviorID="txtNotesEditVCE" TargetControlID="txtNotesEditRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>