﻿using CW.Common.Constants;
using CW.Data.Helpers;
using CW.Utility;
using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._administration;
using CW.Website._controls.admin.APISubscribers.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.APISubscribers
{
    public partial class ViewProducts : AdminUserControlGrid
    {
        #region Constructor
        public ViewProducts() : base()
        {
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }
        #endregion Constructor

        #region fields
        private string DeploymentId { get; set; }
        private string callFmt1 = "", callFmt2 = "";
        private enum ProductDetails { Default };

        #endregion
        #region Constants
        const String deleteSuccessful = "Product deletion was successful.";
        const String updateSuccessful = "Product update was successful";
        const String cloningSuccessful = "Product cloning was successful";
        public const String productNotPubliclyAccessible = "(NOTE: This Product will not be publicly accessible until it is published)";
        #endregion Constants

        #region AdminUserControlGrid overrides

        protected override IEnumerable<string> GridColumnNames
        {
            get { return PropHelper.G<APIProductMetaData>(_ => _.Name, _ => _.SubscriptionRequiredDisplay, _ => _.ApprovalRequiredDisplay, _ => _.PublishedDisplay, _ => _.SubscriberCount, _ => _.HasAPIsDisplay, _ => _.HasThrottlingDisplay, _ => _.HasQuotaDisplay); }
        }

        protected override string NameField { get { return PropHelper.G<APIProductMetaData>(_ => _.Name); } }

        protected override string IdColumnName { get { return PropHelper.G<APIProductMetaData>(_ => _.ID); } }

        protected override string Name { get { return "Product"; } }

        protected override Type Entity { get { return typeof(APIProductMetaData); } }

        protected override IEnumerable GetEntitiesForGrid(IEnumerable<string> searchCriteria)
        {
            return (GetAllProducts().ToList());
        }

        public override IEnumerable<Enum> ReactToChangeStateList
        {
            get
            {
                return new Enum[]
                    {
                        SystemAPISubscriberAdministration.TabMessages.AddProduct,
                        SystemAPISubscriberAdministration.TabMessages.EditProduct,
                        SystemAPISubscriberAdministration.TabMessages.DeleteProduct,
                        SystemAPISubscriberAdministration.TabMessages.AddAPIContainer,
                        SystemAPISubscriberAdministration.TabMessages.EditAPIContainer,
                        SystemAPISubscriberAdministration.TabMessages.DeleteAPIContainer,
                        SystemAPISubscriberAdministration.TabMessages.AddAPISubscriber,
                        SystemAPISubscriberAdministration.TabMessages.EditAPISubscriber,
                        SystemAPISubscriberAdministration.TabMessages.DeleteAPISubscriber,
                    };
            }
        }

        protected override void InitializeControl()
        {
            RebindProducts(true);
        }
        private void RebindProducts(bool forceRefresh)
        {
            gridCacher.Clear();
            if (forceRefresh)
            {
                ResetUI(); // Force refresh
            }
            BindData(true);
            grid.Visible = true;
        }
        protected override void SetDataForDetailsViewFromDB()
        {
            SetProductIntoViewState();
            SelectedKey = CurrentKey; //make sure to updated the ids to match, until there is another selected index changed later.
        }
        protected override void SetFieldsIntoDetailsView()
        {
            SetKeyValuePairItemsToRepeater(Convert.ToInt32(ProductDetails.Default), rptProductDetails);
        }
        #endregion AdminUserControlGrid overrides

        public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid)
        {
            return new GridCacher.CacheKeyInfo(typeof(APIProductMetaData).Name, "");
        }

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            NumericUpDownExtender0.Minimum = APIHelper.SubscriptionLimitRange[0];
            NumericUpDownExtender0.Maximum = APIHelper.SubscriptionLimitRange[1];
            subinfo.InnerText = "(" + NumericUpDownExtender0.Minimum + " - " + NumericUpDownExtender0.Maximum + ")";
            txtEditSubscriptionLimit.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender0.Minimum + "," + NumericUpDownExtender0.Maximum + ");";

            NumericUpDownExtender1.Minimum = APIHelper.ThrottlingRateLimitRange[0];
            NumericUpDownExtender1.Maximum = APIHelper.ThrottlingRateLimitRange[1];
            trlinfo.InnerText = "(" + NumericUpDownExtender1.Minimum + " - " + NumericUpDownExtender1.Maximum + ")";
            txtEditRateLimit.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender1.Minimum + "," + NumericUpDownExtender1.Maximum + ");";

            NumericUpDownExtender2.Minimum = APIHelper.ThrottlingRenewalPeriodLimitRange[0];
            NumericUpDownExtender2.Maximum = APIHelper.ThrottlingRenewalPeriodLimitRange[1];
            trplinfo.InnerText = "(" + NumericUpDownExtender2.Minimum + " - " + NumericUpDownExtender2.Maximum + ") minutes";
            txtEditRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender2.Minimum + "," + NumericUpDownExtender2.Maximum + ");";

            NumericUpDownExtender3.Minimum = APIHelper.QuotaRateLimitRange[0];
            NumericUpDownExtender3.Maximum = APIHelper.QuotaRateLimitRange[1];
            qrlinfo.InnerText = "(" + NumericUpDownExtender3.Minimum + " - " + NumericUpDownExtender3.Maximum + ")";
            txtEditNumberCalls.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender3.Minimum + "," + NumericUpDownExtender3.Maximum + ");";

            NumericUpDownExtender4.Minimum = APIHelper.QuotaRenewalPeriodLimitRange[0];
            NumericUpDownExtender4.Maximum = APIHelper.QuotaRenewalPeriodLimitRange[1];
            qrplinfo.InnerText = "(" + NumericUpDownExtender4.Minimum + " - " + NumericUpDownExtender4.Maximum + ") minutes";
            txtEditQuotaRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender4.Minimum + "," + NumericUpDownExtender4.Maximum + ");";

            lblResults.Visible = false;
            lblEditError.Visible = false;
            ckEditPublished.Attributes["onclick"] = "onPublishedChangeEdit(this.checked);";
            callFmt1 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtEditRateLimit.ClientID + "','" + txtEditRenewalPeriod.ClientID + "','" + NumericUpDownExtender1.ClientID + "','" + NumericUpDownExtender2.ClientID + "', null);";
            enableThrottling.Attributes["onclick"] = string.Format(callFmt1, "this.checked");
            callFmt2 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtEditNumberCalls.ClientID + "','" + txtEditQuotaRenewalPeriod.ClientID + "','" + NumericUpDownExtender3.ClientID + "','" + NumericUpDownExtender4.ClientID + "','" + txtEditKiloBytes.ClientID + "');";
            enableQuota.Attributes["onclick"] = string.Format(callFmt2, "this.checked");
        }
        void Page_Init()
        {
            UseKeyNotId = true;
        }
        private void Page_FirstLoad()
        {
            gridCacher.Clear();
        }
        #region Grid Events
        protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
        {
            pnlDetailsView.Visible = false;
            pnlEdit.Visible = true;
            lblEditError.Text = "";
            lblEditResults.Text = "";
            hdnProductId.Value = grid.DataKeys[e.NewEditIndex].Value as string;
            lbEditProductApis.Items.Clear();
            APIProductMetaData product = GetAllProducts().FirstOrDefault(p => p.ID == hdnProductId.Value);
            txtEditName.Text = product.Name;
            txtEditDescription.Text = product.Description;
            txtEditTerms.Text = product.Terms;
            ckEditSubscriptionRequired.Checked = product.SubscriptionRequired;
            ckEditApprovalRequired.Checked = product.ApprovalRequired;
            ckEditPublished.Checked = product.Published;
            lblPublishInfoEdit.InnerText = (product.Published ? "" : productNotPubliclyAccessible);

            txtEditSubscriptionLimit.Text = APIHelper.ApplyRangeConstraint(product.SubscriptionLimit, APIHelper.SubscriptionLimitRange).ToString();
            txtEditRateLimit.Text = APIHelper.ApplyRangeConstraint(product.RateLimit, APIHelper.ThrottlingRateLimitRange).ToString();
            txtEditRenewalPeriod.Text = APIHelper.ApplyRangeConstraint(product.RenewalPeriod, APIHelper.ThrottlingRenewalPeriodLimitRange).ToString();
            txtEditNumberCalls.Text = APIHelper.ApplyRangeConstraint(product.QuotaRateLimit, APIHelper.QuotaRateLimitRange).ToString();
            txtEditQuotaRenewalPeriod.Text = APIHelper.ApplyRangeConstraint(product.QuotaRenewalPeriod, APIHelper.QuotaRenewalPeriodLimitRange).ToString();
            txtEditKiloBytes.Text = product.QuotaBandwidth.ToString();

            IList<string> apiContainerIDs = APIHelper.GetProductApiContainerIDs(DeploymentId, product.ID);
            GetAllApiContainers().ForEach(container =>
            {
                ListItem lvi = new ListItem(container.Name, container.ID);
                if (apiContainerIDs.Contains(container.ID))
                {
                    lvi.Selected = true;
                }
                lbEditProductApis.Items.Add(lvi);
            });

            enableThrottling.Checked = product.RateLimit > 0;
            enableQuota.Checked = product.QuotaRateLimit > 0;
            APIHelper.RegisterSettingThrottleAndQuotaJS(this, enableThrottling.Checked, enableQuota.Checked, callFmt1, callFmt2);
        }

        private IEnumerable<APIContainerMetaData> GetAllApiContainers()
        {
            return ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_CONTAINERS_ID) as IEnumerable<APIContainerMetaData>);
        }
        protected void OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandSource is ImageButton)
            {
                string command = e.CommandArgument.ToString().ToUpper();
                if ("CLONEPRODUCT" == command)
                {
                    GridViewRow row = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
                    if (0 <= row.DataItemIndex)
                    {
                        int index = row.DataItemIndex >= grid.PageSize ? row.DataItemIndex % grid.PageSize : row.DataItemIndex;

                        //get the datakey uid
                        string productId = grid.DataKeys[index].Value as string;
                        CloneProduct(productId);
                    }
                }
            }
        }
        private void CloneProduct(string productId)
        {
            Tuple<string,string> status = APIHelper.CloneProduct(DeploymentId, productId);
            if (!string.IsNullOrEmpty(status.Item1)) // Error
            {
                LabelHelper.SetLabelMessage(lblDeleteError, status.Item1, lnkSetFocus);
            }
            else
            {
                string info = status.Item2;
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.EditProduct);
                ChangeCurrentTab(cloningSuccessful + (!string.IsNullOrEmpty(info) ? ": " + info : ""));
                SetChangeStateForTabs(SystemAPISubscriberAdministration.TabMessages.EditProduct);
            }
        }
        protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            string productId = grid.DataKeys[e.RowIndex].Value as string;
            string error = APIHelper.DeleteProduct(DeploymentId, productId);
            if (!string.IsNullOrEmpty(error))
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, error);
            }
            else
            {
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.DeleteProduct);
                ChangeCurrentTab(deleteSuccessful);
                SetChangeStateForTabs(SystemAPISubscriberAdministration.TabMessages.DeleteProduct);
            }
        }

        static private string[] infoBtnsWithConfirmation = { "btnDelete", "return(confirmProductDeletion({0},{1}));", "btnClone", "return(confirmProductClone({0},{1}));" };
        protected override void OnGridDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int rowIndex = e.Row.RowIndex + 1; // Skip header
                int cellIndex = 4;
                for (int i = 0, iNum = infoBtnsWithConfirmation.Count(); i < iNum; i += 2)
                {
                    ImageButton imgBtn = e.Row.FindControl(infoBtnsWithConfirmation[i]) as ImageButton;
                    if (null != imgBtn)
                    {
                        imgBtn.OnClientClick = string.Format(infoBtnsWithConfirmation[i + 1], rowIndex, cellIndex);
                    }
                }
            }
            base.OnGridDataBound(sender, e);
        }
        #endregion Grid Events
        #endregion events

        private IEnumerable<APIProductMetaData> GetAllProducts()
        {
            return ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_PRODUCTS_ID) as IEnumerable<APIProductMetaData>);
        }

#region button events

        protected void updateProductButton_Click(Object sender, EventArgs e)
        {
            int[] values = { 0, 0, 0, 0, 0 };
            string info = "";
            string error = APIHelper.ValidateThrottlingAndQuotaConstraints(enableThrottling.Checked, enableQuota.Checked, txtEditRateLimit.Text, txtEditRenewalPeriod.Text, txtEditNumberCalls.Text, txtEditQuotaRenewalPeriod.Text, txtEditKiloBytes.Text, values);
            if (string.IsNullOrEmpty(error))
            {
                int subscriptionsLimit = 0;

                if (!int.TryParse(txtEditSubscriptionLimit.Text, out subscriptionsLimit) || subscriptionsLimit < 0)
                {
                    error = "Subscription Limit is invalid";
                }
                else
                {
                    var selectedProductApiIds = lbEditProductApis.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value);
                    if (0 == selectedProductApiIds.Count())
                    {
                        error = editProductAPIsRequiredValidator.ErrorMessage;
                    }
                    else
                    {
                        Tuple<string, string> status = APIHelper.UpdateProduct(DeploymentId, hdnProductId.Value, txtEditName.Text, txtEditDescription.Text, txtEditTerms.Text,
                                                ckEditSubscriptionRequired.Checked, ckEditApprovalRequired.Checked, ckEditPublished.Checked,
                                                subscriptionsLimit, values[0], values[1], values[2], values[3], values[4], selectedProductApiIds.ToArray());
                        if (!string.IsNullOrEmpty(status.Item1))
                        {
                            error = status.Item1;
                        }
                        else
                        {
                            info = status.Item2;
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(error))
            {
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.EditProduct);
                LabelHelper.SetLabelMessage(lblEditResults, updateSuccessful + (!string.IsNullOrEmpty(info) ? ": " + info : ""), lnkSetFocus);
                OperationComplete(SystemAPISubscriberAdministration.TabMessages.EditProduct);
                RebindProducts(false);
            }
            else
            {
                lblEditResults.Text = "";   
                LabelHelper.SetLabelMessage(lblEditError, error, lnkSetFocus);
            }
        }

#endregion button events
        private void SetProductIntoViewState()
        {
            APIProductMetaData product = GetAllProducts().FirstOrDefault(p => p.ID == CurrentKey);
            if (null != product)
            {
                SetKeyValuePairItem("Name", product.Name);
                SetKeyValuePairItem("Description", product.Description);
                SetKeyValuePairItem("Terms", product.Terms);
                string content = null;
                IList<string> apiContainerIDs = APIHelper.GetProductApiContainerIDs(DeploymentId, CurrentKey);
                GetAllApiContainers().ForEach(container =>
                {
                    if (apiContainerIDs.Contains(container.ID))
                    {
                        if (null == content)
                        {
                            content = container.Name;
                        }
                        else
                        {
                            content += "\n" + container.Name;
                        }
                    }
                });

                if (null == content)
                {
                    SetKeyValuePairItem("Associated API Container(s)", "None");
                }
                else
                {
                    SetKeyValuePairItem("Associated API Container(s)", content, "<div class='divContentPreWrap'>{0}</div>");
                }
                SetKeyValuePairItem("Subscription Required", product.SubscriptionRequired.ToString());
                SetKeyValuePairItem("Approval Required", product.ApprovalRequired.ToString());
                SetKeyValuePairItem("Published", product.Published.ToString());
                SetKeyValuePairItem("Subscription Limit", product.SubscriptionLimit.ToString());
                SetKeyValuePairItem("Throttling Rate Limit", product.RateLimit.ToString());
                SetKeyValuePairItem("Throttling Renewal Period", product.RenewalPeriod.ToString());
                SetKeyValuePairItem("Quota Rate Limit", product.QuotaRateLimit.ToString());
                SetKeyValuePairItem("Quota Renewal Period", product.QuotaRateLimit.ToString());
                SetKeyValuePairItem("Quota Bandwidth", NumericHelper.FormatInvariantToSpecificCulture(Convert.ToDouble(product.QuotaBandwidth)) + " KB");
            }
            SetDetailsViewDataToViewState(Convert.ToInt32(ProductDetails.Default), true); //True means to clear viewstate since this is a new record
        }
    }
}