﻿using CW.Utility;
using CW.Utility.Extension;
using CW.Website._administration;
using CW.Website._controls.admin.APISubscribers.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Linq;
using CW.Utility.Web;
using CW.Data.Helpers;

namespace CW.Website._controls.admin.APISubscribers
{
    public partial class ViewAPIContainers : AdminUserControlGrid
    {
        #region Constructor
        public ViewAPIContainers() : base()
        {
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }
        #endregion Constructor

        #region fields
        private string DeploymentId { get; set; }
        private enum APIContainerDetails { Default };
        string callFmt1 = "", callFmt2 = "";

        #endregion
        #region Constants
        const String deleteSuccessful = "API Container deletion was successful.";
        const String updateSuccessful = "API Container update was successful.";
        #endregion Constants
        #region AdminUserControlGrid overrides

        protected override IEnumerable<string> GridColumnNames
        {
            get { return PropHelper.G<APIContainerMetaData>(_ => _.Name, _ => _.Suffix, _ => _.HasThrottling, _ => _.HasQuota, _ => _.NumberOfOperations, _ => _.EndPoint); }
        }

        protected override string NameField { get { return PropHelper.G<APIContainerMetaData>(_ => _.Name); } }

        protected override string IdColumnName { get { return PropHelper.G<APIContainerMetaData>(_ => _.ID); } }

        protected override string Name { get { return "APIContainer"; } }

        protected override Type Entity { get { return typeof(APIContainerMetaData); } }

        protected override IEnumerable GetEntitiesForGrid(IEnumerable<string> searchCriteria)
        {
            return (GetAllContainers());
        }

        public override IEnumerable<Enum> ReactToChangeStateList
        {
            get
            {
                return new Enum[]
                    {
                                SystemAPISubscriberAdministration.TabMessages.AddAPIContainer,
                                SystemAPISubscriberAdministration.TabMessages.EditAPIContainer,
                                SystemAPISubscriberAdministration.TabMessages.DeleteAPIContainer
                    };
            }
        }

        protected override void SetDataForDetailsViewFromDB()
        {
            SetProductIntoViewState();
            SelectedKey = CurrentKey; //make sure to updated the ids to match, until there is another selected index changed later.
        }
        protected override void SetFieldsIntoDetailsView()
        {
            SetKeyValuePairItemsToRepeater(Convert.ToInt32(APIContainerDetails.Default), rptAPIDetails);
        }
        #endregion AdminUserControlGrid overrides

        public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid)
        {
            return new GridCacher.CacheKeyInfo(typeof(APIContainerMetaData).Name, "");
        }

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblResults.Visible = false;
            lblEditError.Visible = false;
            txtEditSuffix.Attributes["onKeyUp"] = "suffixChangedEdit(this.value);";
            NumericUpDownExtender1.Minimum = APIHelper.ThrottlingRateLimitRange[0];
            NumericUpDownExtender1.Maximum = APIHelper.ThrottlingRateLimitRange[1];
            trlinfo.InnerText = "(" + NumericUpDownExtender1.Minimum + " - " + NumericUpDownExtender1.Maximum + ")";
            txtRateLimit.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender1.Minimum + "," + NumericUpDownExtender1.Maximum + ");";

            NumericUpDownExtender2.Minimum = APIHelper.ThrottlingRenewalPeriodLimitRange[0];
            NumericUpDownExtender2.Maximum = APIHelper.ThrottlingRenewalPeriodLimitRange[1];
            trplinfo.InnerText = "(" + NumericUpDownExtender2.Minimum + " - " + NumericUpDownExtender2.Maximum + ") minutes";
            txtRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender2.Minimum + "," + NumericUpDownExtender2.Maximum + ");";

            NumericUpDownExtender3.Minimum = APIHelper.QuotaRateLimitRange[0];
            NumericUpDownExtender3.Maximum = APIHelper.QuotaRateLimitRange[1];
            qrlinfo.InnerText = "(" + NumericUpDownExtender3.Minimum + " - " + NumericUpDownExtender3.Maximum + ")";
            txtNumberCalls.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender3.Minimum + "," + NumericUpDownExtender3.Maximum + ");";

            NumericUpDownExtender4.Minimum = APIHelper.QuotaRenewalPeriodLimitRange[0];
            NumericUpDownExtender4.Maximum = APIHelper.QuotaRenewalPeriodLimitRange[1];
            qrplinfo.InnerText = "(" + NumericUpDownExtender4.Minimum + " - " + NumericUpDownExtender4.Maximum + ") minutes";
            txtQuotaRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender4.Minimum + "," + NumericUpDownExtender4.Maximum + ");";

            callFmt1 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtRateLimit.ClientID + "','" + txtRenewalPeriod.ClientID + "','" + NumericUpDownExtender1.ClientID + "','" + NumericUpDownExtender2.ClientID + "', null);";
            enableThrottling.Attributes["onclick"] = string.Format(callFmt1, "this.checked");
            callFmt2 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtNumberCalls.ClientID + "','" + txtQuotaRenewalPeriod.ClientID + "','" + NumericUpDownExtender3.ClientID + "','" + NumericUpDownExtender4.ClientID + "','" + txtKiloBytes.ClientID + "');";
            enableQuota.Attributes["onclick"] = string.Format(callFmt2, "this.checked");
        }
        void Page_Init()
        {
            UseKeyNotId = true;
        }
        private void Page_FirstLoad()
        {
            gridCacher.Clear();
        }
        #region Grid Events
        protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
        {
            pnlDetailsView.Visible = false;
            pnlEdit.Visible = true;
            hdnAPIContainerId.Value = grid.DataKeys[e.NewEditIndex].Value as string;
            IEnumerable<APIContainerMetaData> allContainers = GetAllContainers();
            if (null != allContainers)
            {
                APIContainerMetaData acmd = allContainers.FirstOrDefault(container => container.ID == hdnAPIContainerId.Value);
                if (null != acmd)
                {
                    txtEditName.Text = acmd.Name;
                    txtEditDescription.Text = acmd.Description;
                    txtEditSuffix.Text = acmd.Suffix;
                    endpoint.Value = APIHelper.ServiceRootEndPoint;
                    lblEditPublicEndpoint.Text = acmd.EndPoint;
                    txtRateLimit.Text = APIHelper.ApplyRangeConstraint(acmd.ThrottlingRateLimit, APIHelper.ThrottlingRateLimitRange).ToString();
                    txtRenewalPeriod.Text = APIHelper.ApplyRangeConstraint(acmd.ThrottlingRenewalPeriod, APIHelper.ThrottlingRenewalPeriodLimitRange).ToString();
                    txtNumberCalls.Text = APIHelper.ApplyRangeConstraint(acmd.QuotaRateLimit, APIHelper.QuotaRateLimitRange).ToString();
                    txtQuotaRenewalPeriod.Text = APIHelper.ApplyRangeConstraint(acmd.QuotaRenewalPeriod, APIHelper.QuotaRenewalPeriodLimitRange).ToString();
                    txtKiloBytes.Text = NumericHelper.FormatInvariantToSpecificCulture(Convert.ToDouble(acmd.QuotaBandwidth));
                    enableThrottling.Checked = acmd.ThrottlingRateLimit > 0;
                    enableQuota.Checked = acmd.QuotaRateLimit > 0;
                    APIHelper.RegisterSettingThrottleAndQuotaJS(this, enableThrottling.Checked, enableQuota.Checked, callFmt1, callFmt2);
                }
            }

            string content = null;
            IEnumerable<string> operations = APIHelper.GetAPIOperationsNames(DeploymentId, hdnAPIContainerId.Value);
            if (null != operations)
            {
                operations.ForEach(operationName =>
                {
                    if (null == content)
                    {
                        content = operationName;
                    }
                    else
                    {
                        content += "\n" + operationName;
                    }
                });
            }
            divAssociatedAPIs.InnerText = content??"None";
        }

        protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            string apiId = grid.DataKeys[e.RowIndex].Value as string;
            string error = APIHelper.DeleteAPIContainer(DeploymentId, apiId);
            if (!string.IsNullOrEmpty(error))
            {
                LabelHelper.SetLabelMessage(lblDeleteError, error, lnkSetFocus);
            }
            else
            {
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.DeleteAPIContainer);
                ChangeCurrentTab(deleteSuccessful);
                SetChangeStateForTabs(SystemAPISubscriberAdministration.TabMessages.DeleteAPIContainer);
            }
        }
        #endregion Grid Events
        #endregion events

        private IEnumerable<APIContainerMetaData> GetAllContainers()
        {
            return ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_CONTAINERS_ID) as IEnumerable<APIContainerMetaData>);
        }
        #region button events

        protected void updateAPIContainerButton_Click(Object sender, EventArgs e)
        {
            int[] values = { 0, 0, 0, 0, 0 };
            string error = APIHelper.ValidateThrottlingAndQuotaConstraints(enableThrottling.Checked, enableQuota.Checked, txtRateLimit.Text, txtRenewalPeriod.Text, txtNumberCalls.Text, txtQuotaRenewalPeriod.Text, txtKiloBytes.Text, values);
            if (string.IsNullOrEmpty(error))
            {
                Tuple<bool, string> results = APIHelper.UpdateAPIContainer(DeploymentId, txtEditName.Text, txtEditDescription.Text, txtEditSuffix.Text, values[0], values[1], values[2], values[3], values[4], new string[] { "HTTPS" }, hdnAPIContainerId.Value);
                if (!results.Item1)
                {
                    error = results.Item2;
                }
            }
            if (string.IsNullOrEmpty(error))
            {
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.EditAPIContainer);
                ChangeCurrentTab(updateSuccessful);
                SetChangeStateForTabs(SystemAPISubscriberAdministration.TabMessages.EditAPIContainer);
            }
            else
            {
                LabelHelper.SetLabelMessage(lblEditError, error, lnkSetFocus);
            }
        }

        #endregion button events
        private void SetProductIntoViewState()
        {
            IEnumerable<APIContainerMetaData> allContainers = GetAllContainers();
            if (null != allContainers)
            {
                APIContainerMetaData acmd = allContainers.FirstOrDefault(container => container.ID == CurrentKey);
                if (null != acmd)
                {
                    SetKeyValuePairItem("Name", acmd.Name);
                    SetKeyValuePairItem("Description", acmd.Description);
                    SetKeyValuePairItem("External Endpoint", acmd.EndPoint);
                    string content = null;
                    // Now get operations in api container
                    IEnumerable<string> operations = APIHelper.GetAPIOperationsNames(DeploymentId, CurrentKey);
                    if (null != operations)
                    {
                        operations.ForEach(operationName =>
                        {
                            if (null == content)
                            {
                                content = operationName;
                            }
                            else
                            {
                                content += "\n" + operationName;
                            }
                        });
                    }
                    if (null == content)
                    {
                        SetKeyValuePairItem("Operations", "None");
                    }
                    else
                    {
                        SetKeyValuePairItem("Operations", content, "<div class='divContentPreWrap'>{0}</div>");
                    }
                    SetKeyValuePairItem("Throttling Rate Limit.Text", acmd.ThrottlingRateLimit.ToString());
                    SetKeyValuePairItem("Throttling Renewal Period", acmd.ThrottlingRenewalPeriod.ToString() + " minutes");
                    SetKeyValuePairItem("Quota Rate Limit", acmd.QuotaRateLimit.ToString());
                    SetKeyValuePairItem("Quota Renewal Period", acmd.QuotaRenewalPeriod.ToString() + " minutes");
                    SetKeyValuePairItem("Quota Bandwidth", NumericHelper.FormatInvariantToSpecificCulture(Convert.ToDouble(acmd.QuotaBandwidth)) + " KB");
                }
            }
            SetDetailsViewDataToViewState(Convert.ToInt32(APIContainerDetails.Default), true); //True means to clear viewstate since this is a new record
        }
    }
}