﻿using CW.Common.Constants;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Configuration;

namespace CW.Website._controls.admin.APISubscribers
{
    public partial class AddAPIContainer : AdminUserControlBase
    {
        #region Constants
        const String addSuccess = " API Container addition was successful.";
        #endregion Constants

        public AddAPIContainer() : base()
        {
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }
        private string DeploymentId { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            txtAddSuffix.Attributes["onKeyUp"] = "suffixChangedAdd(this.value);";
            NumericUpDownExtender1.Minimum = APIHelper.ThrottlingRateLimitRange[0];
            NumericUpDownExtender1.Maximum = APIHelper.ThrottlingRateLimitRange[1];
            trlinfo.InnerText = "(" + NumericUpDownExtender1.Minimum + " - " + NumericUpDownExtender1.Maximum + ")";
            txtRateLimit.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender1.Minimum + "," + NumericUpDownExtender1.Maximum + ");";

            NumericUpDownExtender2.Minimum = APIHelper.ThrottlingRenewalPeriodLimitRange[0];
            NumericUpDownExtender2.Maximum = APIHelper.ThrottlingRenewalPeriodLimitRange[1];
            trplinfo.InnerText = "(" + NumericUpDownExtender2.Minimum + " - " + NumericUpDownExtender2.Maximum + ") minutes";
            txtRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender2.Minimum + "," + NumericUpDownExtender2.Maximum + ");";

            NumericUpDownExtender3.Minimum = APIHelper.QuotaRateLimitRange[0];
            NumericUpDownExtender3.Maximum = APIHelper.QuotaRateLimitRange[1];
            qrlinfo.InnerText = "(" + NumericUpDownExtender3.Minimum + " - " + NumericUpDownExtender3.Maximum + ")";
            txtNumberCalls.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender3.Minimum + "," + NumericUpDownExtender3.Maximum + ");";

            NumericUpDownExtender4.Minimum = APIHelper.QuotaRenewalPeriodLimitRange[0];
            NumericUpDownExtender4.Maximum = APIHelper.QuotaRenewalPeriodLimitRange[1];
            qrplinfo.InnerText = "(" + NumericUpDownExtender4.Minimum + " - " + NumericUpDownExtender4.Maximum + ") minutes";
            txtQuotaRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender4.Minimum + "," + NumericUpDownExtender4.Maximum + ");";

            string callFmt1 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtRateLimit.ClientID + "','" + txtRenewalPeriod.ClientID + "','" + NumericUpDownExtender1.ClientID + "','" + NumericUpDownExtender2.ClientID + "', null);";
            enableThrottling.Attributes["onclick"] = string.Format(callFmt1, "this.checked");
            string callFmt2 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtNumberCalls.ClientID + "','" + txtQuotaRenewalPeriod.ClientID + "','" + NumericUpDownExtender3.ClientID + "','" + NumericUpDownExtender4.ClientID + "','" + txtKiloBytes.ClientID + "');";
            enableQuota.Attributes["onclick"] = string.Format(callFmt2, "this.checked");
            if (!IsPostBack)
            {
                txtRateLimit.Text = NumericUpDownExtender1.Minimum.ToString();
                txtRenewalPeriod.Text = NumericUpDownExtender2.Minimum.ToString();
                txtNumberCalls.Text = NumericUpDownExtender3.Minimum.ToString();
                txtQuotaRenewalPeriod.Text = NumericUpDownExtender4.Minimum.ToString();
                lblAddPublicEndpoint.Text = endpoint.Value = APIHelper.GetExternalEndpoint(DeploymentId);
            }
            APIHelper.RegisterSettingThrottleAndQuotaJS(this, enableThrottling.Checked, enableQuota.Checked, callFmt1, callFmt2);
        }
        protected void addAPIContainerButton_Click(Object sender, EventArgs e)
        {
            lblAddError.Text = "";
            lblResults.Text = "";
            int[] values = { 0, 0, 0, 0, 0 };
            string error = APIHelper.ValidateThrottlingAndQuotaConstraints(enableThrottling.Checked, enableQuota.Checked, txtRateLimit.Text, txtRenewalPeriod.Text, txtNumberCalls.Text, txtQuotaRenewalPeriod.Text, txtKiloBytes.Text, values);
            if (string.IsNullOrEmpty(error))
            {
                Tuple<bool, string> results = APIHelper.UpdateAPIContainer(DeploymentId, txtAddName.Text, txtAddDescription.Text, txtAddSuffix.Text, values[0], values[1], values[2], values[3], values[4], new string[] { "HTTPS" });
                if (!results.Item1)
                {
                    error = results.Item2;
                }
            }

            if (string.IsNullOrEmpty(error))
            {
                LabelHelper.SetLabelMessage(lblResults, addSuccess, lnkSetFocus);
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.AddAPIContainer);
                OperationComplete(SystemAPISubscriberAdministration.TabMessages.AddAPIContainer);
            }
            else
            {
                LabelHelper.SetLabelMessage(lblAddError, error, lnkSetFocus);
            }
        }
    }
}