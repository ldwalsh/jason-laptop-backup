﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddAPISubscriber.ascx.cs" Inherits="CW.Website._controls.admin.APISubscribers.AddAPISubscriber" %>

<asp:Panel ID="pnlAddAPISubscriber" runat="server" DefaultButton="btnAddAPISubscriber">
    <script type="text/javascript">
        function generateNewPwd() {
            try {
                var txtPwdObj = document.getElementById('<%=txtPassword.ClientID %>');
                var info = document.getElementById('<%=info.ClientID %>');
                info.innerText = "";
                var url = "/_framework/httpHandlers/GenerateSubscriberPassword.ashx";
                $.get(url, function (data, textStatus) {
                    txtPwdObj.value = data;
                    info.innerText = "<%=CW.Website._controls.admin.APISubscribers.ViewAPISubscribers.notePwd %>";
                });
		    }
		    catch (err) {
		        alert(err);
		    }
        }
    </script>
  <h2>Add API Subscriber</h2>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />
    <div class="divForm">
      <label class="label">*Organization:</label>
      <asp:DropDownList ID="ddlAddAPISubscriberOrganization" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddAPISubscriberOrganization_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
          <asp:ListItem Text="Select one..." Value="" Selected="True"></asp:ListItem>
      </asp:DropDownList>
    </div>
    <div class="divForm">
      <label class="label">*User:</label>
      <asp:DropDownList ID="ddlAddAPISubscriberUser" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddAPISubscriberUser_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
      </asp:DropDownList>
    </div>     
    <div class="divForm">
      <label class="label">*Password:</label>
      <table class="labelContent">
          <tr>
              <td>
                <asp:TextBox ID="txtPassword" CssClass="textbox" runat="server" ReadOnly="true" />
              </td>
              <td style="padding-left:5px;"><img id="newPwd" style="display:none" runat="server" src="/_assets/styles/images/refresh.jpg" class="imgBtn" onclick="generateNewPwd();" alt="Generate a new Password" /></td>
          </tr>
      </table>
    </div>
    <div class="divForm">
      <label class="label"></label>
      <label class="labelContent" id="info" runat="server"></label>
    </div>
    <div class="divForm" id="productsArea" runat="server">
      <label class="label">*Select Product(s) to subscribe to:</label>
      <asp:ListBox ID="lbProducts" AppendDataBoundItems="false" CssClass="listbox" runat="server" Width="230" SelectionMode="Multiple" />
    </div>
    <div class="divForm">
      <label class="label">*Notes:</label>
      <asp:TextBox ID="txtNotes" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>
  </div>

  <asp:LinkButton CssClass="lnkButton" ID="btnAddAPISubscriber" runat="server" Text="Add API Subscriber"  OnClick="addAPISubscriberButton_Click" ValidationGroup="AddAPISubscriber" />
    
  <asp:RequiredFieldValidator ID="addAPISubscriberOrganizationRFV" runat="server" ErrorMessage="Organization is a required field." ControlToValidate="ddlAddAPISubscriberOrganization" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="AddAPISubscriber" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addAPISubscriberOrganizationVCE" runat="server" BehaviorID="addAPISubscriberOrganizationVCE" TargetControlID="addAPISubscriberOrganizationRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addAPISubscriberUserRFV" runat="server" ErrorMessage="User is a required field." ControlToValidate="ddlAddAPISubscriberUser" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="AddAPISubscriber" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addAPISubscriberUserVCE" runat="server" BehaviorID="addAPISubscriberUserVCE" TargetControlID="addAPISubscriberUserRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="txtPasswordRFV" runat="server" ErrorMessage="Password is a required field." ControlToValidate="txtPassword" SetFocusOnError="true" Display="None" ValidationGroup="AddAPISubscriber" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtPasswordVCE" runat="server" BehaviorID="txtPasswordVCE" TargetControlID="txtPasswordRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="productsRequiredValidator" runat="server" ErrorMessage="At least one Product is required." ControlToValidate="lbProducts" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="AddAPISubscriber" />
  <ajaxToolkit:ValidatorCalloutExtender ID="productsRequiredValidatorExtender" runat="server" TargetControlID="productsRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="txtNotesRFV" runat="server" ErrorMessage="Notes is a required field." ControlToValidate="txtNotes" SetFocusOnError="true" Display="None" ValidationGroup="AddAPISubscriber" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtNotesVCE" runat="server" BehaviorID="txtNotesVCE" TargetControlID="txtNotesRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>    