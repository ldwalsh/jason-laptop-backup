﻿using CW.Utility;
using CW.Utility.Web;
using CW.Website._controls.admin.APISubscribers.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.APISubscribers
{
    public partial class AddProduct : AdminUserControlBase
    {
        #region Constructor
        public AddProduct() : base()
        {
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }
        #endregion Constructor
        #region fields
        private string DeploymentId { get; set; }

        #endregion fields
        #region Constants
        const String addProductSuccess = " Product addition was successful.";
        public const String productNotPubliclyAccessible = "(NOTE: This Product will not be publicly accessible until it is published)";
        #endregion Constants
        #region events

        protected void Page_Load(Object sender, EventArgs e)
        {
            lblResults.Visible = false;
            lblAddError.Visible = false;
            addProductNumericUpDownExtender0.Minimum = APIHelper.SubscriptionLimitRange[0];
            addProductNumericUpDownExtender0.Maximum = APIHelper.SubscriptionLimitRange[1];
            subinfo.InnerText = "(" + addProductNumericUpDownExtender0.Minimum + " - " + addProductNumericUpDownExtender0.Maximum + ")";
            txtSubscriptionLimit.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + addProductNumericUpDownExtender0.Minimum + "," + addProductNumericUpDownExtender0.Maximum + ");";

            addProductNumericUpDownExtender1.Minimum = APIHelper.ThrottlingRateLimitRange[0];
            addProductNumericUpDownExtender1.Maximum = APIHelper.ThrottlingRateLimitRange[1];
            trlinfo.InnerText = "(" + addProductNumericUpDownExtender1.Minimum + " - " + addProductNumericUpDownExtender1.Maximum + ")";
            txtRateLimit.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + addProductNumericUpDownExtender1.Minimum + "," + addProductNumericUpDownExtender1.Maximum + ");";

            addProductNumericUpDownExtender2.Minimum = APIHelper.ThrottlingRenewalPeriodLimitRange[0];
            addProductNumericUpDownExtender2.Maximum = APIHelper.ThrottlingRenewalPeriodLimitRange[1];
            trplinfo.InnerText = "(" + addProductNumericUpDownExtender2.Minimum + " - " + addProductNumericUpDownExtender2.Maximum + ") minutes";
            txtRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + addProductNumericUpDownExtender2.Minimum + "," + addProductNumericUpDownExtender2.Maximum + ");";

            addProductNumericUpDownExtender3.Minimum = APIHelper.QuotaRateLimitRange[0];
            addProductNumericUpDownExtender3.Maximum = APIHelper.QuotaRateLimitRange[1];
            qrlinfo.InnerText = "(" + addProductNumericUpDownExtender3.Minimum + " - " + addProductNumericUpDownExtender3.Maximum + ")";
            txtNumberCalls.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + addProductNumericUpDownExtender3.Minimum + "," + addProductNumericUpDownExtender3.Maximum + ");";

            addProductNumericUpDownExtender4.Minimum = APIHelper.QuotaRenewalPeriodLimitRange[0];
            addProductNumericUpDownExtender4.Maximum = APIHelper.QuotaRenewalPeriodLimitRange[1];
            qrplinfo.InnerText = "(" + addProductNumericUpDownExtender4.Minimum + " - " + addProductNumericUpDownExtender4.Maximum + ") minutes";
            txtQuotaRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + addProductNumericUpDownExtender4.Minimum + "," + addProductNumericUpDownExtender4.Maximum + ");";

            ckPublished.Attributes["onclick"] = "onPublishedChangeAdd(this.checked);";

            string callFmt1 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtRateLimit.ClientID + "','" + txtRenewalPeriod.ClientID + "','" + addProductNumericUpDownExtender1.ClientID + "','" + addProductNumericUpDownExtender2.ClientID + "', null);";
            enableThrottling.Attributes["onclick"] = string.Format(callFmt1, "this.checked");
            string callFmt2 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtNumberCalls.ClientID + "','" + txtQuotaRenewalPeriod.ClientID + "','" + addProductNumericUpDownExtender3.ClientID + "','" + addProductNumericUpDownExtender4.ClientID + "','" + txtKiloBytes.ClientID + "');";
            enableQuota.Attributes["onclick"] = string.Format(callFmt2, "this.checked");

            if (!IsPostBack)
            {
                FillApis();
                txtKiloBytes.Text = "0";
                publishInfoAdd.InnerText = productNotPubliclyAccessible;
                txtSubscriptionLimit.Text = addProductNumericUpDownExtender0.Minimum.ToString();
                txtRateLimit.Text = APIHelper.ThrottlingRateLimitRange[0].ToString();
                txtRenewalPeriod.Text = APIHelper.ThrottlingRenewalPeriodLimitRange[0].ToString();
                txtNumberCalls.Text = APIHelper.QuotaRateLimitRange[0].ToString();
                txtQuotaRenewalPeriod.Text = APIHelper.QuotaRenewalPeriodLimitRange[0].ToString();
            }

            APIHelper.RegisterSettingThrottleAndQuotaJS(this, enableThrottling.Checked, enableQuota.Checked, callFmt1, callFmt2);
        }
        #endregion

        protected override void InitializeControl()
        {
            FillApis();
        }
        public override IEnumerable<Enum> ReactToChangeStateList
        {
            get
            {
                return new Enum[]
                    {
                                SystemAPISubscriberAdministration.TabMessages.AddAPIContainer,
                                SystemAPISubscriberAdministration.TabMessages.EditAPIContainer,
                                SystemAPISubscriberAdministration.TabMessages.DeleteAPIContainer
                    };
            }
        }
        private void FillApis()
        {
            // Any selected? If so, reselect after populating (this would happen via ReactToChangeStateList
            var selectedProductApiIds = lbAddProductAPIs.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToList();
            lbAddProductAPIs.Items.Clear();
            IEnumerable<APIContainerMetaData> apiContainers = GetAllApiContainers();
            if (null != apiContainers)
            {
                apiContainers.ToList().ForEach(api =>
                {
                    ListItem li = new ListItem(api.Name, api.ID);
                    if (null != selectedProductApiIds.FirstOrDefault(id => id == api.ID))
                    {
                        li.Selected = true;
                    }
                    lbAddProductAPIs.Items.Add(li);
                });
            }
        }
        private IEnumerable<APIContainerMetaData> GetAllApiContainers()
        {
            return ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_CONTAINERS_ID) as IEnumerable<APIContainerMetaData>);
        }

        #region button events
        protected void addProductButton_Click(Object sender, EventArgs e)
        {
            int[] values = { 0, 0, 0, 0, 0 };
            string error = APIHelper.ValidateThrottlingAndQuotaConstraints(enableThrottling.Checked, enableQuota.Checked, txtRateLimit.Text, txtRenewalPeriod.Text, txtNumberCalls.Text, txtQuotaRenewalPeriod.Text, txtKiloBytes.Text, values);
            if (string.IsNullOrEmpty(error))
            {
                int subscriptionsLimit = 0;

                if (!int.TryParse(txtSubscriptionLimit.Text, out subscriptionsLimit) || subscriptionsLimit < 0)
                {
                    error = "Subscription Limit is invalid";
                }
                else
                {
                    IEnumerable<APIContainerMetaData> apiContainers = GetAllApiContainers();
                    if (null != apiContainers)
                    {
                        var selectedProductApiIds = lbAddProductAPIs.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value);
                        var apis = apiContainers.Where(p => null != selectedProductApiIds.FirstOrDefault(id => id == p.ID)).Select(p => p.ID).ToArray();
                        if (0 == apis.Count())
                        {
                            error = addProductAPIsRequiredValidator.ErrorMessage;
                        }
                        else
                        {
                            error = APIHelper.CreateProduct(DeploymentId, txtName.Text, txtDescription.Text, txtTerms.Text,
                                                        ckSubscriptionRequired.Checked, ckApprovalRequired.Checked, ckPublished.Checked,
                                                        subscriptionsLimit, values[0], values[1], values[2], values[3], values[4], apis);
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(error))
            {
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.AddProduct);
                LabelHelper.SetLabelMessage(lblResults, addProductSuccess, lnkSetFocus);
                OperationComplete(SystemAPISubscriberAdministration.TabMessages.AddProduct);
            }
            else
            {
                LabelHelper.SetLabelMessage(lblAddError, error, lnkSetFocus);
            }
        }
        #endregion
    }
}