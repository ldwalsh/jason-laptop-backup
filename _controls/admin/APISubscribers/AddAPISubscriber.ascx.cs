﻿using CW.Common.Constants;
using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._controls.admin.APISubscribers.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.APISubscribers
{
    public partial class AddAPISubscriber : AdminUserControlBase
    {
        #region Constructor
        public AddAPISubscriber() : base()
        {
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }
        #endregion Constructor

        #region fields

        private string DeploymentId { get; set; }
        #endregion fields
        #region constants

        const String addAPISubscriberSuccess = " API Subscriber addition was successful.";
        const String addAPISubscriberFailed = "Adding API Subscriber failed. Please contact an administrator.";
        const String addAPISubscriberError = "Error adding API Subscriber.";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAddError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindOrganizations();
                    BindUsers();
                }
            }

            #region ddl events

                protected void ddlAddAPISubscriberOrganization_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindUsers();
                    if ("" == ddlAddAPISubscriberOrganization.SelectedValue)
                    {
                        lbProducts.Items.Clear();
                    }
                    else
                    {
                        GetAllProducts();
                    }
                }
        
                protected void ddlAddAPISubscriberUser_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    string pwd = "" != ddlAddAPISubscriberUser.SelectedValue ? ApiSubscriberHelper.GeneratePassword() : "";
                    txtPassword.Text = pwd;
                    info.InnerText = !string.IsNullOrEmpty(pwd) ? ViewAPISubscribers.notePwd : "";
                    newPwd.Style["display"] = !string.IsNullOrEmpty(pwd) ? "block" : "none";
        }
        #endregion

        #region button events

                protected void addAPISubscriberButton_Click(Object sender, EventArgs e)
                {
                    lblAddError.Text = "";
                    lblResults.Text = "";
                    var apiSubscriber = new CW.Data.APISubscriber();
                    LoadAddFormIntoAPISubscriber(apiSubscriber);

                    IDictionary<string, string> selectedProducts = lbProducts.Items.Cast<ListItem>().Where(product => product.Selected).ToDictionary(item => item.Value, item => item.Text);
                    Data.User user = DataMgr.UserDataMapper.GetUser(apiSubscriber.UID);
                    string error = SystemAPISubscriberAdministration.SaveUserAsync(apiSubscriber, user.FirstName, user.LastName, user.Email, txtPassword.Text, txtNotes.Text, selectedProducts).Result;

                    if(! string.IsNullOrEmpty(apiSubscriber.SubscriberID)) // The user would have been created in Azure even though some product subscriptions may have failed (e.g. product is not published)
                    {
                        try
                        {
                            int apiSubscriberID = DataMgr.APISubscriberDataMapper.InsertAPISubscriber(apiSubscriber, ConfigMgr.GetConfigurationSetting(DataConstants.EncryptionParaphrase, "", true));

                            (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.AddAPISubscriber);
                            if (string.IsNullOrEmpty(error))
                            {
                                LabelHelper.SetLabelMessage(lblResults, apiSubscriber.SubscriberID + addAPISubscriberSuccess, lnkSetFocus);
                            }
                            OperationComplete(SystemAPISubscriberAdministration.TabMessages.AddAPISubscriber);

                            //rebind users with updated set excluding newly added user
                            BindUsers();
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, addAPISubscriberError, ex);
                            error = addAPISubscriberFailed;
                        }
                    }

                    if (!string.IsNullOrEmpty(error))
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, error);
                        LabelHelper.SetLabelMessage(lblAddError, error, lnkSetFocus);
                    }
                }


            #endregion

        #endregion

        #region methods

            private void LoadAddFormIntoAPISubscriber(CW.Data.APISubscriber apiSubscriber)
            {
                apiSubscriber.UID = Convert.ToInt32(ddlAddAPISubscriberUser.SelectedValue);
            }

            private void GetAllProducts()
            {
                if (0 == lbProducts.Items.Count)
                {
                    IEnumerable<APIProductMetaData> allProducts = ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_PRODUCTS_ID) as IEnumerable<APIProductMetaData>);
                    allProducts.ForEach(product =>
                    {
                        string productId = product.ID;
                        lbProducts.Items.Add(new ListItem(product.Name, product.ID));
                    });
                }
            }

        #region binders

            private void BindOrganizations()
            {
                ddlAddAPISubscriberOrganization.DataTextField = "OrganizationName";
                ddlAddAPISubscriberOrganization.DataValueField = "OID";
                ddlAddAPISubscriberOrganization.DataSource = DataMgr.OrganizationDataMapper.GetAllOrganizations();
                ddlAddAPISubscriberOrganization.DataBind();
            }

            private void BindUsers()
            {
                ddlAddAPISubscriberUser.Items.Clear();
                int oid = 0;
                bool orgSelected = int.TryParse(ddlAddAPISubscriberOrganization.SelectedValue, out oid) && oid > 0;
                var lItem = new ListItem() { Text = (!orgSelected ? "Select organization first..." : "Select one..."), Value = "", Selected = true };
                ddlAddAPISubscriberUser.Items.Add(lItem);
                if (orgSelected)
                {
                    ddlAddAPISubscriberUser.DataTextField = "Email";
                    ddlAddAPISubscriberUser.DataValueField = "UID";
                    ddlAddAPISubscriberUser.DataSource = DataMgr.UserDataMapper.GetAllUsersNotAPISubscribedByOID(Convert.ToInt32(ddlAddAPISubscriberOrganization.SelectedValue));
                    ddlAddAPISubscriberUser.DataBind();
                }

                // Clear out pwd and hide info msg
                txtPassword.Text = "";
                info.InnerText = "";
            }

        #endregion

        #endregion
    }
}