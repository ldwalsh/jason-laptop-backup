﻿using CW.Data.Models.APISubscriber;
using CW.RESTAdapter.MAPIM;
using CW.Website._controls.admin.APISubscribers.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI;

namespace CW.Website._controls.admin.APISubscribers
{
    static public class APIHelper
    {
        public static string ServiceRootEndPoint { get; set; }
        public const string API_PRODUCTS_ID = "allproducts";
        public const string API_CONTAINERS_ID = "allcontainers";
        public const string API_SUBSCRIBERS = "allsubscribers";

        static private Regex mRegEx = new Regex("^[a-zA-Z0-9 _]+$", RegexOptions.Compiled);
        private const string INVALID_CHARACTERS = "Only alphanumeric, _ and space are permitted";

        #region Helpers
        private const string SUFFIX_DELIMITER = "|";
        static public string SetSuffixCollection(IOrderedEnumerable<APIContainerMetaData> apiContainerCollection)
        {
            return(string.Join(SUFFIX_DELIMITER, apiContainerCollection.Select(container => container.Suffix)));
        }

        static public string Suffix(string srcSuffixes, int selectedAPIContainerIndex)
        {
            string suffix = "";
            string[] items = srcSuffixes.Split(SUFFIX_DELIMITER[0]);
            if (null != items && selectedAPIContainerIndex < items.Count())
            {
                suffix = items[selectedAPIContainerIndex];
            }
            return (suffix);
        }

        static public string MakeAPIEndpoint(string publicBaseEndPoint, string suffix, string urlTemplate)
        {
            string publicEndPoint = publicBaseEndPoint;
            if (!publicEndPoint.EndsWith("/"))
            {
                publicEndPoint += "/";
            }
            if (suffix.Length > 0 && suffix.StartsWith("/"))
            {
                suffix = suffix.Substring(1);
            }
            if (suffix.Length > 0 && !suffix.EndsWith("/"))
            {
                suffix += "/";
            }
            if (suffix.Length > 0)
            {
                publicEndPoint += suffix;
            }
            if (urlTemplate.Length > 0 && urlTemplate.StartsWith("/"))
            {
                urlTemplate = urlTemplate.Substring(1);
            }
            return (publicEndPoint + urlTemplate);
        }
        #endregion Helpers

        #region Product
        static public IEnumerable<APIProductMetaData> GetAllProducts(string deploymentId, IEnumerable<GetAPISubscriberData> actualSubscribers)
        {
            return (GetAllProductsAsync(deploymentId, actualSubscribers).Result);
        }
        static public async Task<IEnumerable<APIProductMetaData>> GetAllProductsAsync(string deploymentId, IEnumerable<GetAPISubscriberData> actualSubscribers)
        {
            IEnumerable<APIProductMetaData> ds = null;
            RESTProduct restProduct = new RESTProduct(deploymentId);
            JArray products = await restProduct.GetAllProductsAsync().ConfigureAwait(false);
            if (null != products)
            {
                ds = products.OrderBy(product => product["name"].Value<string>()).Select(product =>
                new APIProductMetaData()
                {
                    ID = product["id"].Value<string>(),
                    Name = product["name"].Value<string>(),
                    Description = product["description"].Value<string>(),
                    Terms = product["terms"].Value<string>(),
                    SubscriptionRequired = product["subscriptionRequired"].Value<bool>(),
                    ApprovalRequired = product["approvalRequired"].Value<bool>(),
                    Published = product["published"].Value<bool>(),
                    SubscriptionLimit = product["subscriptionsLimit"].Value<int>(),
                    RateLimit = product["rateLimit"].Value<int>(),
                    RenewalPeriod = product["renewalPeriod"].Value<int>(),
                    QuotaRateLimit = product["rateLimitQuota"].Value<int>(),
                    QuotaRenewalPeriod = product["renewalPeriodQuota"].Value<int>(),
                    QuotaBandwidth = product["bandwidthQuota"].Value<int>(),
                    HasAPIs = product["hasoperations"].Value<bool>(),
                    HasThrottling = product["rateLimit"].Value<int>() > 0,
                    HasQuota = product["rateLimitQuota"].Value<int>() > 0,
                    SubscriberCount = GetProductSubscriberCount(product["subscribers"].Value<JArray>(), actualSubscribers)
                });
            }
            return (ds ?? Enumerable.Empty<APIProductMetaData>());
        }

        static private int GetProductSubscriberCount(JArray productSubscribers, IEnumerable<GetAPISubscriberData> actualSubscribers)
        {
            // actualSubscribers will never be null; can be empty though
            var activeSubscribers = productSubscribers.Where(ps => null != actualSubscribers.FirstOrDefault(sub => sub.SubscriberID == ps["id"].Value<string>()));
            return (activeSubscribers.Count());
        }
        static public string DeleteProduct(string deploymentId, string productId)
        {
            return (DeleteProductAsync(deploymentId, productId).Result);
        }
        static public async Task<string> DeleteProductAsync(string deploymentId, string productId)
        {
            string error = null;
            RESTProduct restProduct = new RESTProduct(deploymentId);
            bool ok = await restProduct.DeleteProductAsync(productId).ConfigureAwait(false);
            if (!ok)
            {
                error = restProduct.Error;
            }

            return (error);
        }

        static public IList<string> GetProductApiContainerIDs(string deploymentId, string productId)
        {
            return (GetProductApiContainerIDsAsync(deploymentId, productId).Result);
        }
        static public async Task<IList<string>> GetProductApiContainerIDsAsync(string deploymentId, string productId)
        {
            IList<string> list = null;
            JArray array = await GetProductApiContainersAsync(deploymentId, productId).ConfigureAwait(false);
            if (null != array)
            {
                list = array.Select(item => item.Value<string>("id")).ToList();
            }
            return (list ?? new List<string>());
        }
        static public JArray GetProductApiContainers(string deploymentId, string productId)
        {
            return(GetProductApiContainersAsync(deploymentId, productId).Result);
        }

        static public async Task<JArray> GetProductApiContainersAsync(string deploymentId, string productId)
        {
            RESTProduct restProduct = new RESTProduct(deploymentId);
            JArray productApis = await restProduct.GetProductApisAsync(productId).ConfigureAwait(false);
            return (productApis);
        }

        static public string CreateProduct(string deploymentId, string name, string description, string terms, bool subscriptionRequired, bool approvalRequired, bool published, int subscriptionsLimit,
                                                   int rateLimit, int renewalPeriod, int rateLimitQuota, int renewalPeriodQuota, int bandwidthQuota, string[] apiIds = null)
        {
            return (CreateProductAsync(deploymentId, name, description, terms, subscriptionRequired, approvalRequired, published, subscriptionsLimit,
                                                   rateLimit, renewalPeriod, rateLimitQuota, renewalPeriodQuota, bandwidthQuota, apiIds).Result);
        }
        static public async Task<string> CreateProductAsync(string deploymentId, string name, string description, string terms, bool subscriptionRequired, bool approvalRequired, bool published, int subscriptionsLimit,
                                                   int rateLimit, int renewalPeriod, int rateLimitQuota, int renewalPeriodQuota, int bandwidthQuota, string[] apiIds = null)
        {
            string error = "";
            if (!mRegEx.IsMatch(name))
            {
                error = INVALID_CHARACTERS;
            }
            else
            {
                RESTProduct rest = new RESTProduct(deploymentId);
                bool ok = await rest.CreateProductAsync(name, description, terms, subscriptionRequired, approvalRequired, published, subscriptionsLimit,
                                                       rateLimit, renewalPeriod, rateLimitQuota, renewalPeriodQuota, bandwidthQuota, apiIds).ConfigureAwait(false);
                if (!ok)
                {
                    error = rest.Error;
                }
            }
            return (error);
        }
        static public Tuple<string, string> UpdateProduct(string deploymentId, string productId, string name, string description, string terms, bool subscriptionRequired, bool approvalRequired, bool published, int subscriptionsLimit,
                                                   int rateLimit, int renewalPeriod, int rateLimitQuota, int renewalPeriodQuota, int bandwidthQuota, string[] apiIds = null)
        {
            return(UpdateProductAsync(deploymentId, productId, name, description, terms, subscriptionRequired, approvalRequired, published, subscriptionsLimit,
                                                   rateLimit, renewalPeriod, rateLimitQuota, renewalPeriodQuota, bandwidthQuota, apiIds).Result);
        }
        static public async Task<Tuple<string, string>> UpdateProductAsync(string deploymentId, string productId, string name, string description, string terms, bool subscriptionRequired, bool approvalRequired, bool published, int subscriptionsLimit,
                                                   int rateLimit, int renewalPeriod, int rateLimitQuota, int renewalPeriodQuota, int bandwidthQuota, string[] apiIds = null)
        {
            string error = "", info = "";
            if (!mRegEx.IsMatch(name))
            {
                error = INVALID_CHARACTERS;
            }
            else
            {
                RESTProduct rest = new RESTProduct(deploymentId);
                bool ok = await rest.UpdateProductAsync(productId, name, description, terms, subscriptionRequired, approvalRequired, published, subscriptionsLimit,
                                                       rateLimit, renewalPeriod, rateLimitQuota, renewalPeriodQuota, bandwidthQuota, apiIds).ConfigureAwait(false);
                if (!ok)
                {
                    error = rest.Error;
                }
                else
                {
                    info = rest.Information;
                }
            }
            return (new Tuple<string, string>(error, info));
        }

        /// <summary>
        /// Returns a tuple; 1st item is error, 2nd is info. Only one will ever exist if error/warning. If none, both will be empty or null
        /// </summary>
        /// <param name="deploymentId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        static public Tuple<string,string> CloneProduct(string deploymentId, string productId)
        {
            return (CloneProductAsync(deploymentId, productId).Result);
        }
        static public async Task<Tuple<string, string>> CloneProductAsync(string deploymentId, string productId)
        {
            string error = "", info = "";

            RESTProduct rest = new RESTProduct(deploymentId);
            bool ok = await rest.CloneProductAsync(productId).ConfigureAwait(false);
            if (!ok)
            {
                error = rest.Error;
            }
            else
            {
                info = rest.Information;
            }
            return (new Tuple<string, string>(error, info));
        }
        #endregion Product
        #region API Container
        static public IEnumerable<APIContainerMetaData> GetAllApiContainers(string deploymentId, bool includePolicies)
        {
            IEnumerable<APIContainerMetaData> ds = null;
            JArray results = GetAllApiContainersAsync(deploymentId, includePolicies).Result;
            if (null != results)
            {
                ds = results.Select(p => FromJToken(p));
            }
            return (ds?? Enumerable.Empty<APIContainerMetaData>());
        }

        static public async Task<JArray> GetAllApiContainersAsync(string deploymentId, bool includePolicies)
        {
            JArray allApis = null;
            RESTAPIs rest = new RESTAPIs(deploymentId);
            JObject json = await rest.GetAllApiContainersAsync(includePolicies).ConfigureAwait(false);
            if (null != json)
            {
                allApis = json["value"] as JArray;
            }
            return( allApis);
        }
        static public APIContainerMetaData GetApiContainer(string deploymentId, string apiId)
        {
            return (GetApiContainerAsync(deploymentId, apiId).Result);
        }
        static public async Task<APIContainerMetaData> GetApiContainerAsync(string deploymentId, string apiId)
        {
            APIContainerMetaData acmd = null;
            RESTAPIs rest = new RESTAPIs(deploymentId);
            JObject json = await rest.GetApiContainerAsync(apiId).ConfigureAwait(false);
            if (null != json)
            {
                acmd = FromJToken(json);
            }

            return (acmd);
        }

        static public APIContainerMetaData FromJToken(JToken container)
        {
            APIContainerMetaData acmd = new APIContainerMetaData
            {
                ID = container["id"].Value<string>(),
                Name = container["name"].Value<string>(),
                Description = container["description"].Value<string>(),
                Suffix = container["path"].Value<string>(),
                EndPoint = ServiceRootEndPoint + container["path"].Value<string>(),
                NumberOfOperations = container["numoperations"].Value<int>(),
                ThrottlingRateLimit = container["rateLimit"].Value<decimal>(),
                ThrottlingRenewalPeriod = container["renewalPeriod"].Value<decimal>(),
                QuotaRateLimit = container["rateLimitQuota"].Value<decimal>(),
                QuotaRenewalPeriod = container["renewalPeriodQuota"].Value<decimal>(),
                QuotaBandwidth = container["bandwidthQuota"].Value<decimal>()
            };
            acmd.HasThrottling = acmd.ThrottlingRateLimit > 0;
            acmd.HasQuota = acmd.QuotaRateLimit > 0;
            return (acmd);
        }

        static public Tuple<bool, string> UpdateAPIContainer(string deploymentId, string name, string description, string suffix, int rateLimit, int renewalPeriod, int rateLimitQuota, int renewalPeriodQuota, int bandwidthQuota, string[] httpProtocols, string apiContainerId = null)
        {
            return (UpdateAPIContainerAsync(deploymentId, name, description, suffix, rateLimit, renewalPeriod, rateLimitQuota, renewalPeriodQuota, bandwidthQuota, httpProtocols, apiContainerId).Result);
        }
        static public async Task<Tuple<bool, string>> UpdateAPIContainerAsync(string deploymentId, string name, string description, string suffix, int rateLimit, int renewalPeriod, int rateLimitQuota, int renewalPeriodQuota, int bandwidthQuota, string[] httpProtocols, string apiContainerId = null)
        {
            Tuple <bool, string> result = null;
            string backendServiceUrl = (new RESTBackendServiceUrls(deploymentId)).GetBackendServiceUrl(deploymentId);
            if (string.IsNullOrEmpty(backendServiceUrl))
            {
                result = new Tuple<bool, string>(false, "Backend service for deployment id " + deploymentId + " is not available.");
            }
            else
            {
                RESTAPIs rest = new RESTAPIs(deploymentId);
                bool ok = await rest.UpdateAPIContainerAsync(name, description, suffix, rateLimit, renewalPeriod, rateLimitQuota, renewalPeriodQuota, bandwidthQuota, httpProtocols, backendServiceUrl, apiContainerId).ConfigureAwait(false);
                if (ok)
                {
                    result = new Tuple<bool, string>(true, rest.NewContainerID ?? apiContainerId);
                }
                else
                {
                    result = new Tuple<bool, string>(false, rest.Error);
                }
            }
            return (result);
        }

        #endregion API Container

        #region Exposed APIs
        static public IEnumerable<KeyValuePair<string, string>> GetAllExposedAPIs(string deploymentId)
        {
            return (GetAllExposedAPIsAsync(deploymentId).Result);
        }

        static public async Task<IEnumerable<KeyValuePair<string, string>>> GetAllExposedAPIsAsync(string deploymentId)
        {
            RESTAPIs rest = new RESTAPIs(deploymentId);

            var apis = await rest.GetAllExposedAPIsAsync(MakeValueForExposedApi).ConfigureAwait(false);

            return (apis);
        }

        static private string MakeValueForExposedApi(string httpMethod, string url, bool hasOptions)
        {
            return ($"{httpMethod}_{url.GetHashCode()}_{hasOptions.ToString().ToLower()}");
        }

        /// <summary>
        /// Retunrns null on failure
        /// </summary>
        /// <param name="concatenatedValue"></param>
        /// <returns></returns>
        static public string GetHTTPMethod(string concatenatedValue)
        {
            // HTTP method preceeds the 1st _
            int index = concatenatedValue.IndexOf('_');
            // 3 is the smallest http method (GET) i.e. the 1st _ cannot be at an index less than 3 (GET_)
            string httpMethod = 3 <= index ? concatenatedValue.Remove(index).Trim() : null;
            return (!string.IsNullOrEmpty(httpMethod) ? httpMethod : null);
        }

        static public string GetExternalEndpoint(string deploymentId)
        {
            return (GetExternalEndpointAsync(deploymentId).Result);
        }
        static public async Task<string> GetExternalEndpointAsync(string deploymentId)
        {
            RESTAPIs rest = new RESTAPIs(deploymentId);
            string endPoint = await rest.GetExternalEndpointAsync().ConfigureAwait(false);
            return (endPoint);
        }
        #endregion Exposed APIs

        #region API (Operations)
        static public string SaveAPIOperation(string deploymentId, string apiContainerId, string name, string description, string httpMethod, string urlTemplate, int rateLimit, int renewalPeriod, int rateLimitQuota, int renewalPeriodQuota, int bandwidthQuota, string apiOperationId = null)
        {
            return (SaveAPIOperationAsync(deploymentId, apiContainerId, name, description, httpMethod, urlTemplate, rateLimit, renewalPeriod, rateLimitQuota, renewalPeriodQuota, bandwidthQuota, apiOperationId).Result);
        }
        static public async Task<string> SaveAPIOperationAsync(string deploymentId, string apiContainerId, string name, string description, string httpMethod, string urlTemplate, int rateLimit, int renewalPeriod, int rateLimitQuota, int renewalPeriodQuota, int bandwidthQuota, string apiOperationId = null)
        {
            RESTAPIs rest = new RESTAPIs(deploymentId);
            bool ok = await rest.UpdateAPIOperationAsync(apiContainerId, name, description, httpMethod, urlTemplate, rateLimit, renewalPeriod, rateLimitQuota, renewalPeriodQuota, bandwidthQuota, apiOperationId).ConfigureAwait(false);
            return (ok ? null : rest.Error);
        }
        static public IEnumerable<APIMetaData> GetAPIOperations(string deploymentId, string apiId, string suffixes, int selectedAPIContainerIndex)
        {
            return (GetAPIOperationsAsync(deploymentId, apiId, suffixes, selectedAPIContainerIndex).Result);
        }
        static public async Task<IEnumerable<APIMetaData>> GetAPIOperationsAsync(string deploymentId, string apiId, string suffixes, int selectedAPIContainerIndex)
        {
            IEnumerable<APIMetaData> ds = null;
            RESTAPIs rest = new RESTAPIs(deploymentId);
            JArray operations = await rest.GetAPIOperationsAsync(apiId).ConfigureAwait(false);
            if (null != operations)
            {
                ds = operations.OrderBy(operation => operation.Value<string>("name")).Select(operation => FromJToken(operation, suffixes, selectedAPIContainerIndex));
            }
            return ((ds ?? Enumerable.Empty<APIMetaData>()).ToList());
        }
        static public IEnumerable<string> GetAPIOperationsNames(string deploymentId, string apiContainerId)
        {
            return (GetAPIOperationsNamesAsync(deploymentId, apiContainerId).Result);
        }
        static public async Task<IEnumerable<string>> GetAPIOperationsNamesAsync(string deploymentId, string apiContainerId)
        {
            IEnumerable<string> list = null;
            RESTAPIs rest = new RESTAPIs(deploymentId);
            JArray operations = await rest.GetAPIOperationsAsync(apiContainerId).ConfigureAwait(false);
            if (null != operations)
            {
                list = operations.OrderBy(operation => operation.Value<string>("name")).Select(operation => operation.Value<string>("name"));
            }
            return ((list ?? Enumerable.Empty<string>()).ToList());
        }

        static public APIMetaData GetAPIOperation(string deploymentId, string apiContainerId, string apiId, string suffixes, int selectedAPIContainerIndex)
        {
            return (GetAPIOperationAsync(deploymentId, apiContainerId, apiId, suffixes, selectedAPIContainerIndex).Result);
        }
        static public async Task<APIMetaData> GetAPIOperationAsync(string deploymentId, string apiContainerId, string apiId, string suffixes, int selectedAPIContainerIndex)
        {
            APIMetaData amd = null;
            RESTAPIs rest = new RESTAPIs(deploymentId);
            JObject operation = await rest.GetAPIOperationAsync(apiContainerId, apiId).ConfigureAwait(false);
            if (null != operation)
            {
                amd = FromJToken(operation, suffixes, selectedAPIContainerIndex);
            }
            return (amd);
        }
        static public APIMetaData FromJToken(JToken operation, string suffixes, int selectedAPIContainerIndex)
        {
            string suffix = APIHelper.Suffix(suffixes, selectedAPIContainerIndex);
            APIMetaData amd = new APIMetaData
            {
                ID = operation["id"].Value<string>(),
                Name = operation["name"].Value<string>(),
                Description = operation["description"].Value<string>(),
                Method = operation["method"].Value<string>(),
                ExposedApi = operation["urlTemplate"].Value<string>(),
                SupportsOptions = null != operation["hasoptions"] ? operation["hasoptions"].Value<bool>() : false
            };
            amd.EndPoint = MakeAPIEndpoint(ServiceRootEndPoint, suffix, amd.ExposedApi);
            amd.ThrottlingRateLimit = operation["rateLimit"].Value<decimal>();
            amd.ThrottlingRenewalPeriod = operation["renewalPeriod"].Value<decimal>();
            amd.QuotaRateLimit = operation["rateLimitQuota"].Value<decimal>();
            amd.QuotaRenewalPeriod = operation["renewalPeriodQuota"].Value<decimal>();
            amd.QuotaBandwidth = operation["bandwidthQuota"].Value<decimal>();
            amd.HasThrottling = amd.ThrottlingRateLimit > 0;
            amd.HasQuota = amd.QuotaRateLimit > 0;

            return (amd);
        }
        static public string DeleteAPIOperation(string deploymentId, string apiId, string operationId)
        {
            return (DeleteAPIOperationAsync(deploymentId, apiId, operationId).Result);
        }
        static public async Task<string> DeleteAPIOperationAsync(string deploymentId, string apiId, string operationId)
        {
            string error = null;
            RESTAPIs rest = new RESTAPIs(deploymentId);
            bool ok = await rest.DeleteAPIOperationAsync(apiId, operationId).ConfigureAwait(false);
            if (!ok)
            {
                error = rest.Error;
            }

            return (error);
        }
        #endregion API (Operations)
        #region Subscribers
        public delegate IEnumerable<Tuple<int, string, string>> GetOrgsForSubscribers(string[] subscriberIDs);
        static public IEnumerable<GetAPISubscriberData> GetAllSubscribers(string deploymentId, GetOrgsForSubscribers dlg)
        {
            return (GetAllSubscribersAsync(deploymentId, dlg).Result);
        }
        static public async Task<IEnumerable<GetAPISubscriberData>> GetAllSubscribersAsync(string deploymentId, GetOrgsForSubscribers dlg)
        {
            IEnumerable<GetAPISubscriberData> results = null;
            RESTAPISubscriber restSubscriber = new RESTAPISubscriber(deploymentId);
            JObject json = await restSubscriber.GetAllSubscribersAsync().ConfigureAwait(false);
            JArray users = json["value"] as JArray;
            IEnumerable<Tuple<int, string, string>> subscriberOrgs = dlg(users.Select(token => token.Value<string>("id")).ToArray());
            if (null != subscriberOrgs)
            {
                // Remove any that do not have an org; this should really never happen
                IEnumerable<JToken> validSubscribers = users.Where(token => null != subscriberOrgs.FirstOrDefault(sid => sid.Item2 == token.Value<string>("id")));

                if (null != validSubscribers && validSubscribers.Count() > 0)
                {
                    List<GetAPISubscriberData> list = new List<GetAPISubscriberData>();
                    validSubscribers.ToList().ForEach(token =>
                    {
                        string subscriberId = token.Value<string>("id");
                        Tuple<int, string, string> item = subscriberOrgs.First(sid => sid.Item2 == subscriberId);
                        list.Add(new GetAPISubscriberData()
                        {
                            ID = item.Item1,
                            OrganizationName = item.Item3,
                            SubscriberID = subscriberId,
                            DateCreated = token.Value<DateTime>("registrationDate"),
                            Email = token.Value<string>("email")
                        });
                    });

                    results = list;
                }
            }
            return (results ?? Enumerable.Empty<GetAPISubscriberData>());
        }
        static public IEnumerable<APISubscriberSubscriptionMetaData> GetSubscriberSubscriptions(string deploymentId, string subscriberId)
        {
            return (GetSubscriberSubscriptionsAsync(deploymentId, subscriberId).Result);
        }
        static public async Task<IEnumerable<APISubscriberSubscriptionMetaData>> GetSubscriberSubscriptionsAsync(string deploymentId, string subscriberId)
        {
            RESTAPISubscriber restUser = new RESTAPISubscriber(deploymentId);
            JArray collection = await restUser.GetSubscriberSubscriptionsAsync(subscriberId).ConfigureAwait(false);
            return(null != collection ? collection.Select(p => new APISubscriberSubscriptionMetaData { ProductID = p.Value<string>("productId"), PrimaryKey = p.Value<string>("primaryKey"), SecondaryKey = p.Value<string>("secondaryKey") }) : Enumerable.Empty<APISubscriberSubscriptionMetaData>());
        }
        static public string DeleteSubscriber(string subscriberId, string deploymentId)
        {
            return(DeleteSubscriberAsync(subscriberId, deploymentId).Result);
        }
        static public async Task<string> DeleteSubscriberAsync(string subscriberId, string deploymentId)
        {
            string error = null;
            RESTAPISubscriber restUser = new RESTAPISubscriber(deploymentId);
            bool ok = await restUser.DeleteSubscriberAsync(subscriberId).ConfigureAwait(false);
            if (!ok)
            {
                error = restUser.Error;
            }
            return (error);
        }
        #endregion Subscribers

        static public string DeleteAPIContainer(string deploymentId, string apiId)
        {
            return (DeleteAPIContainerAsync(deploymentId, apiId).Result);
        }
        static public async Task<string> DeleteAPIContainerAsync(string deploymentId, string apiId)
        {
            string error = null;
            RESTAPIs rest = new RESTAPIs(deploymentId);
            bool ok = await rest.DeleteAPIContainerAsync(apiId).ConfigureAwait(false);
            if (!ok)
            {
                error = rest.Error;
            }

            return (error);
        }

        static public int[] SubscriptionLimitRange = { 0, 500};
        static public int[] ThrottlingRateLimitRange = { 1, 20 };
        static public int[] ThrottlingRenewalPeriodLimitRange = { 1, 5 }; // Minutes
        static public int[] QuotaRateLimitRange = { 1, 20 };
        static public int[] QuotaRenewalPeriodLimitRange = { 60, 120 }; // Minutes
        static public string ValidateThrottlingAndQuotaConstraints(bool throttlingEnabled, bool quotaEnabled, string txtRateLimit, string txtRenewalPeriod, string txtNumberCalls, string txtQuotaRenewalPeriod, string txtKiloBytes, int[] values)
        {
            values.ToList().ForEach(val => val = 0);
            string error = null;
            if (throttlingEnabled)
            {
                if (!int.TryParse(txtRateLimit, out values[0]) || !(ThrottlingRateLimitRange[0] <= values[0] && ThrottlingRateLimitRange[1] >= values[0]))
                {
                    error = "Throttling Rate Limit must be greater than or equal to " + ThrottlingRateLimitRange[0] + " and less than or equal to " + ThrottlingRateLimitRange[1];
                }
                else if (!int.TryParse(txtRenewalPeriod, out values[1]) || !(ThrottlingRenewalPeriodLimitRange[0] <= values[1] && ThrottlingRenewalPeriodLimitRange[1] >= values[1]))
                {
                    error = "Throttling Renewal Period must be greater than or equal to " + ThrottlingRenewalPeriodLimitRange[0] + " and less than or equal to " + ThrottlingRenewalPeriodLimitRange[1] + " minutes";
                }
                else
                {
                    string[] quotaLabels = { "Rate Limit", "Renewal Period" };
                    // For Throttling, if any are specified then all MUST be specified
                    string[] valuesNotSpecified = { "", "" };
                    int numValuesSpecified = 0;
                    for (int index = 0; index < 2; index++)
                    {
                        if (values[index] > 0)
                        {
                            ++numValuesSpecified;
                        }
                        else
                        {
                            valuesNotSpecified[index] = quotaLabels[index];
                        }
                    }
                    if (numValuesSpecified > 0 && numValuesSpecified != 2)
                    {
                        error = "If any Throttling is enabled, then all values must be specified. The following were not:<ul>";
                        valuesNotSpecified.Where(val => val.Length > 0).ToList().ForEach(val => error += "<li>" + val + "</li>");
                        error += "</ul>";
                    }
                }
            }

            if (string.IsNullOrEmpty(error) && quotaEnabled)
            {
                if (!int.TryParse(txtNumberCalls, out values[2]) || !(QuotaRateLimitRange[0] <= values[2] && QuotaRateLimitRange[1] >= values[2]))
                {
                    error = "Quota Rate Limit must be greater than or equal to " + QuotaRateLimitRange[0] + " and less than or equal to " + QuotaRateLimitRange[1];
                }
                else if (!int.TryParse(txtQuotaRenewalPeriod, out values[3]) || !(QuotaRenewalPeriodLimitRange[0] <= values[3] && QuotaRenewalPeriodLimitRange[1] >= values[3]))
                {
                    error = "Quota Renewal Period must be greater than or equal to " + QuotaRenewalPeriodLimitRange[0] + " and less than or equal to " + QuotaRenewalPeriodLimitRange[1] + " minutes";
                }
                else if (!int.TryParse(txtKiloBytes, out values[4]) || values[4] < 0)
                {
                    error = "Bandwidth must be greater than or equal to 0 KB";
                }
                else
                {
                    string[] quotaLabels = { "Rate Limit", "Renewal Period", "Bandwidth" };
                    // For Quotas, if any are specified then all MUST be specified
                    string[] valuesNotSpecified = { "", "", "" };
                    int numValuesSpecified = 0;
                    for (int index = 2; index < 5; index++)
                    {
                        if (values[index] > 0)
                        {
                            ++numValuesSpecified;
                        }
                        else
                        {
                            valuesNotSpecified[index - 2] = quotaLabels[index - 2];
                        }
                    }
                    if (numValuesSpecified > 0 && numValuesSpecified != 3)
                    {
                        error = "If any Quota is enabled, then all values must be specified. The following were not:<ul>";
                        valuesNotSpecified.Where(val => val.Length > 0).ToList().ForEach(val => error += "<li>" + val + "</li>");
                        error += "</ul>";
                    }
                }
            }

            if (!string.IsNullOrEmpty(error))
            {
                values.ToList().ForEach(val => val = 0);
            }
            return (error);
        }

        static private string GetBackendServiceUrl(string deploymentId, ref string error)
        {
            RESTBackendServiceUrls rest = new RESTBackendServiceUrls(deploymentId);
            string url = rest.GetBackendServiceUrl(deploymentId);
            error = rest.Error;
            return (url);
        }

        static public void RegisterSettingThrottleAndQuotaJS(UserControl ctrl, bool throttlingEnabled, bool quotaEnabled, string fmt1, string fmt2 )
        {
            string functionName = ctrl.ID.Replace(" ", "").Replace(".", "").Trim();
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("function " + functionName + "(){");  
            sb.AppendLine(string.Format(fmt1, throttlingEnabled.ToString().ToLower()));
            sb.AppendLine(string.Format(fmt2, quotaEnabled.ToString().ToLower()));
            sb.AppendLine("}"); // Terminate function

            ScriptManager.RegisterClientScriptBlock(ctrl, typeof(Page), functionName, sb.ToString(), true);
            ScriptManager.RegisterStartupScript(ctrl, typeof(Page), "run" + functionName, "Sys.Application.add_load(" + functionName + ");", true);
        }

        static public int ApplyRangeConstraint(decimal value, int[] range)
        {
            int val = (int)value;
            if (val < range[0])
            {
                val = range[0];
            }
            else if (val > range[1])
            {
                val = range[1];
            }
            return (val);
        }
    }
}
