﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.APISubscriber;
using CW.Utility;
using CW.Utility.Web;
using CW.Utility.Extension;
using CW.Website._administration;
using CW.Website._controls.admin.APISubscribers.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.APISubscribers
{
    public partial class ViewAPISubscribers : AdminUserControlGrid
    {
        #region Constructor
        public ViewAPISubscribers() : base()
        {
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }
        #endregion Constructor

        #region constants

            const String deleteSuccessful = "API Subscriber deletion was successful.";
            const String deleteFailed = "API Subscriber deletion failed. Please contact an administrator.";
            const String updateSuccessful = "API Subscriber update was successful.";
            const String updateFailed = "API Subscriber update failed. Please contact an administrator.";
            const String updateAPISubscriberSubscriberIDExists = "A API Subscriber with that subscriber id already exists.";
            public const String notePwd = "(NOTE: This is the only time you will see the password for this user. If necessary, please store securely.)";

        #endregion

        #region fields

            private string DeploymentId { get; set; }
            private enum APISubscriberDetails { Default };

        #endregion

        #region properties

        #region AdminUserControlGrid overrides

                protected override IEnumerable<string> GridColumnNames
                {
                    get { return PropHelper.G<GetAPISubscriberData>(_ => _.OrganizationName, _ => _.Email, _ => _.SubscriberID, _ => _.DateCreated); }
                }

                protected override string NameField { get { return PropHelper.G<GetAPISubscriberData>(_ => _.SubscriberID); } }

                protected override string IdColumnName { get { return PropHelper.G<GetAPISubscriberData>(_ => _.ID); } }

                protected override string Name { get { return "API Subscriber"; } }

                protected override Type Entity { get { return typeof(GetAPISubscriberData); } }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<string> searchCriteria)
                {
                    return ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_SUBSCRIBERS) as IEnumerable<GetAPISubscriberData>);
                }

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[] 
                            {
                                SystemAPISubscriberAdministration.TabMessages.AddProduct,
                                SystemAPISubscriberAdministration.TabMessages.EditProduct,
                                SystemAPISubscriberAdministration.TabMessages.DeleteProduct,
                                SystemAPISubscriberAdministration.TabMessages.AddAPISubscriber,
                                SystemAPISubscriberAdministration.TabMessages.EditAPISubscriber,
                                SystemAPISubscriberAdministration.TabMessages.DeleteAPISubscriber
                            };
                    }
                }

#endregion

                public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid)
                {
                    return new GridCacher.CacheKeyInfo(typeof(CW.Data.APISubscriber).Name, "");
                }

#endregion

#region events

            private void Page_FirstLoad()
            {
                //to account for when a user is deleted from another page
                gridCacher.Clear();
            }

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblEditError.Visible = false;
            }

#region ddl events

#endregion

#region grid events

#region overrides

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var apiSubscriberID = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());
                        var apiSubscriber = DataMgr.APISubscriberDataMapper.GetAPISubscriberByID(apiSubscriberID, new Expression<Func<APISubscriber, Object>>[] { _ => _.User }, new Expression<Func<User, Object>>[] { _ => _.Organization });

                        SetAPISubscriberIntoEditForm(apiSubscriber);
                        txtNotesEdit.Text = "";
                        IEnumerable<APIProductMetaData> allProducts = GetAllProducts();
                        if (null != allProducts)
                        {
                            string ids = "";
                            List<APISubscriptionMetaData> list = allProducts.Select(p => new APISubscriptionMetaData { ID = p.ID, Name = p.Name, PrimaryKey = "", SecondaryKey = "" }).ToList();
                            IEnumerable<APISubscriberSubscriptionMetaData> collection = APIHelper.GetSubscriberSubscriptions(DeploymentId, apiSubscriber.SubscriberID);
                            if (null != collection)
                            {
                                list.ForEach(p =>
                                {
                                    var subs = collection.FirstOrDefault(sub => sub.ProductID == p.ID);
                                    if (null != subs)
                                    {
                                        p.PrimaryKey = subs.PrimaryKey;
                                        p.SecondaryKey = subs.SecondaryKey;
                                        p.Subscribed = true;
                                        if (ids.Length > 0)
                                        {
                                            ids += ",";
                                        }
                                        ids += subs.ProductID;
                                    }
                                });
                            }
                            productCBIDs.Value = ids;
                            subscribedProducts.DataSource = list;
                            subscribedProducts.DataBind();
                        }
                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                    protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
                    {
                        string error = "";
                        var apiSubscriberID = Convert.ToInt32(grid.DataKeys[e.RowIndex].Value);
                        var subscriberId = DataMgr.APISubscriberDataMapper.GetSubscriberIdFromID(apiSubscriberID);
                        if (!string.IsNullOrEmpty(subscriberId))
                        {
                            error = APIHelper.DeleteSubscriber(subscriberId, DeploymentId);
                            if (string.IsNullOrEmpty(error))
                            { 
                                try
                                {
                                    DataMgr.APISubscriberDataMapper.DeleteAPISubscriber(apiSubscriberID);
                                    (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.DeleteAPISubscriber);
                                    ChangeCurrentTab(deleteSuccessful);
                                    lblDeleteError.Text = "";
                                    SetChangeStateForTabs(SystemAPISubscriberAdministration.TabMessages.DeleteAPISubscriber);
                                }
                                catch (Exception ex)
                                {
                                    error = ex.Message;
                                }

                                if( ! string.IsNullOrEmpty(error))
                                { 
                                    LabelHelper.SetLabelMessage(lblDeleteError, error, lnkSetFocus);
                                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting subscriber: " + error);
                                }
                            }
                        }
                    }
#endregion

#endregion

#region button events

                protected void updateAPISubscriberButton_Click(Object sender, EventArgs e)
                {
                    IEnumerable<APIProductMetaData> allProducts = GetAllProducts();
                    if (null != allProducts)
                    {
                        var selectedProductIds = productCBIDs.Value.Split(',').Where(id => !string.IsNullOrEmpty(id)).Select(id => id);
                        var selProds = allProducts.Where(p => null != selectedProductIds.FirstOrDefault(id => id == p.ID)).ToDictionary(item => item.ID, item => item.Name);
                        var apiSubscriber = new CW.Data.APISubscriber();

                        LoadEditFormIntoAPISubscriber(apiSubscriber);

                        string error = null;
                        try
                        {
                            Data.User user = DataMgr.UserDataMapper.GetUserFromSubscriberId(apiSubscriber.SubscriberID);
                            error = SystemAPISubscriberAdministration.SaveUserAsync(apiSubscriber, user.FirstName, user.LastName, user.Email, txtEditPassword.Text, txtNotesEdit.Text, selProds).Result;
                            if (string.IsNullOrEmpty(error))
                            {
                                DataMgr.APISubscriberDataMapper.UpdateAPISubscriber(apiSubscriber);
                            }
                        }
                        catch (Exception ex)
                        {
                            error = ex.Message;
                        }

                        if (string.IsNullOrEmpty(error))
                        {
                            (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.EditAPISubscriber);
                            LabelHelper.SetLabelMessage(lblResults, updateSuccessful, lnkSetFocus);
                            OperationComplete(OperationType.Update, updateSuccessful, SystemAPISubscriberAdministration.TabMessages.EditAPISubscriber);
                        }
                        else
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, updateFailed + ":" + error);
                            LabelHelper.SetLabelMessage(lblEditError, updateFailed + " " + error, lnkSetFocus);
                        }
                    }
                }

#endregion

#endregion

#region methods

#region overrides

                protected override void TabStateMonitor_OnChangeState(TabBase tab, IEnumerable<Enum> messages)
                {
                    base.TabStateMonitor_OnChangeState(tab, messages);
                }

                protected override void SetDataForDetailsViewFromDB()
                {
                    var apiSubscriber = DataMgr.APISubscriberDataMapper.GetAPISubscriberByID(CurrentID, new Expression<Func<APISubscriber, Object>>[] { _ => _.User }, new Expression<Func<User, Object>>[] { _ => _.Organization });
                    SetAPISubscriberIntoViewState(apiSubscriber);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(APISubscriberDetails.Default), rptAPISubscriberDetails);
                }

#endregion

            private void SetAPISubscriberIntoViewState(CW.Data.APISubscriber apiSubscriber)
            {
                SetKeyValuePairItem("Organization", apiSubscriber.User.Organization.OrganizationName);
                SetKeyValuePairItem("Email", apiSubscriber.User.Email);
                string content = null;
                IEnumerable<APIProductMetaData> allProducts = GetAllProducts();
                if( null != allProducts)
                {
                    IEnumerable<APISubscriberSubscriptionMetaData> collection = APIHelper.GetSubscriberSubscriptions(DeploymentId, apiSubscriber.SubscriberID);
                    if (null != collection)
                    {
                        collection.ForEach(subs =>
                        {
                            APIProductMetaData product = allProducts.FirstOrDefault(p => p.ID == subs.ProductID);
                            if (null != product)
                            {
                                if (null == content)
                                {
                                    content = product.Name;
                                }
                                else
                                {
                                    content += "\n" + product.Name;
                                }
                            }
                        });
                    }
                }

                if (null == content)
                {
                    SetKeyValuePairItem("Products Subscribed To", "None");
                }
                else
                {
                    SetKeyValuePairItem("Products Subscribed To", content, "<div class='divContentPreWrap'>{0}</div>");
                }
                //SetKeyValuePairItem("Date Created", apiSubscriber.DateCreated);

                SetDetailsViewDataToViewState(Convert.ToInt32(APISubscriberDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            private void SetAPISubscriberIntoEditForm(CW.Data.APISubscriber apiSubscriber)
            {
                hdnEditAPISubscriberID.Value = Convert.ToString(apiSubscriber.ID);
                hdnSubscriberId.Value = apiSubscriber.SubscriberID;
                lblEditAPISubscriberOrganization.InnerText = apiSubscriber.User.Organization.OrganizationName;                                                                       
                lblEditAPISubscriberUser.InnerText = apiSubscriber.User.Email;
            }

            private void LoadEditFormIntoAPISubscriber(CW.Data.APISubscriber apiSubscriber)
            {
                apiSubscriber.ID = Convert.ToInt32(hdnEditAPISubscriberID.Value);
                apiSubscriber.SubscriberID = hdnSubscriberId.Value;                
            }

            private IEnumerable<APIProductMetaData> GetAllProducts()
            {
                return ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_PRODUCTS_ID) as IEnumerable<APIProductMetaData>);
            }
#region binders

                    #endregion

                    #endregion
            }
        }