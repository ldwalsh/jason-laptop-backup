﻿using System;

namespace CW.Website._controls.admin.APISubscribers.Models
{
    [Serializable]
    public class APISubscriberSubscriptionMetaData
    {
        public string ProductID { get; set; }
        public string PrimaryKey { get; set; }
        public string SecondaryKey { get; set; }
    }
}
