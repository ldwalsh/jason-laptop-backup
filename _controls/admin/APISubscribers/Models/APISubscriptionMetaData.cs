﻿using System;

namespace CW.Website._controls.admin.APISubscribers.Models
{
    [Serializable]
    public class APISubscriptionMetaData
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string PrimaryKey { get; set; }
        public string SecondaryKey { get; set; }
        public bool Subscribed { get; set; }

    }
}
