﻿using System;

namespace CW.Website._controls.admin.APISubscribers.Models
{
    [Serializable]
    public class APIContainerMetaData
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Suffix { get; set; }
        public int NumberOfOperations { get; set; }
        public string EndPoint { get; set; }
        public decimal ThrottlingRateLimit { get; set; }
        public decimal ThrottlingRenewalPeriod { get; set; }
        public decimal QuotaRateLimit { get; set; }
        public decimal QuotaRenewalPeriod { get; set; }
        public decimal QuotaBandwidth { get; set; }
        public bool HasThrottling { get; set; }
        public bool HasQuota { get; set; }
    }
}
