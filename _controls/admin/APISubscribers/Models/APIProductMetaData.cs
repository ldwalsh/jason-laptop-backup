﻿using System;

namespace CW.Website._controls.admin.APISubscribers.Models
{
    [Serializable]
    public class APIProductMetaData
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Terms { get; set; }
        public bool SubscriptionRequired { get; set; }
        public bool ApprovalRequired { get; set; }
        public bool Published{ get; set; }
        public int SubscriptionLimit{ get; set; }
        public int RateLimit { get; set; }
        public int RenewalPeriod{ get; set; }
        public int QuotaRateLimit { get; set; }
        public int QuotaRenewalPeriod { get; set; }
        public int QuotaBandwidth { get; set; }
        public bool HasAPIs { get; set; }
        public bool HasThrottling { get; set; }
        public bool HasQuota { get; set; }
        public int SubscriberCount { get; set; }
        public string HasQuotaDisplay { get { return (HasQuota ? "Yes" : "No"); } }
        public string HasThrottlingDisplay { get { return (HasThrottling ? "Yes" : "No"); } }
        public string SubscriptionRequiredDisplay { get { return (SubscriptionRequired ? "Yes" : "No"); } }
        public string ApprovalRequiredDisplay { get { return (ApprovalRequired ? "Yes" : "No"); } }
        public string PublishedDisplay { get { return (Published ? "Yes" : "No"); } }
        public string HasAPIsDisplay { get { return (HasAPIs ? "Yes" : "No"); } }
    }
}
