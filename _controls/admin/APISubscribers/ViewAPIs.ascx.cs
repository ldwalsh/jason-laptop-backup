﻿using CW.Data.Helpers;
using CW.Utility;
using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._administration;
using CW.Website._controls.admin.APISubscribers.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.APISubscribers
{
    public partial class ViewAPIs : AdminUserControlGrid
    {
        #region Constructor
        public ViewAPIs() : base()
        {
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }
        #endregion Constructor
        #region fields
        private string DeploymentId { get; set; }
        string callFmt1 = "", callFmt2 = "";
        private enum APIDetails { Default };

        #endregion
        #region Constants
        const String deleteSuccessful = "API deletion was successful.";
        const String updateSuccessful = "API update was successful.";
        #endregion Constants
        #region AdminUserControlGrid overrides

        protected override IEnumerable<string> GridColumnNames
        {
            get { return PropHelper.G<APIMetaData>(_ => _.Name, _ => _.Method, _ => _.HasThrottling, _ => _.HasQuota); }
        }

        protected override string NameField { get { return PropHelper.G<APIMetaData>(_ => _.Name); } }

        protected override string IdColumnName { get { return PropHelper.G<APIMetaData>(_ => _.ID); } }

        protected override string Name { get { return "API"; } }

        protected override Type Entity { get { return typeof(APIMetaData); } }

        protected override IEnumerable GetEntitiesForGrid(IEnumerable<string> searchCriteria)
        {
            return (CreateDatasetForGrid(ddlViewAPIContainers.SelectedValue));
        }

        public override IEnumerable<Enum> ReactToChangeStateList
        {
            get
            {
                return new Enum[]
                    {
                                SystemAPISubscriberAdministration.TabMessages.AddAPI,
                                SystemAPISubscriberAdministration.TabMessages.EditAPI,
                                SystemAPISubscriberAdministration.TabMessages.DeleteAPI,
                                SystemAPISubscriberAdministration.TabMessages.AddAPIContainer,
                                SystemAPISubscriberAdministration.TabMessages.EditAPIContainer,
                                SystemAPISubscriberAdministration.TabMessages.DeleteAPIContainer
                    };
            }
        }

        protected override void InitializeControl()
        {
            RebindAPIs(true);
        }

        private void RebindAPIs( bool forceRefresh)
        {
            gridCacher.Clear();
            string selectedAPIContainer = (!IsPostBack ? null : ddlViewAPIContainers.SelectedValue);
            if (forceRefresh)
            {
                string selectedAPI = (!IsPostBack ? null : ddlAPIs.SelectedValue);
                new AdminUserControlForm(this).ResetDropDownLists();
                if (null != selectedAPI)
                {
                    ddlAPIs.SelectedValue = selectedAPI;
                }
            }
            BindAPIContainers(selectedAPIContainer);
            if (forceRefresh)
            {
                ResetUI(); // Force refresh
            }
            BindData(true);
            grid.Visible = true;
        }
        protected override void SetDataForDetailsViewFromDB()
        {
            SetProductIntoViewState();
            SelectedKey = CurrentKey; //make sure to updated the ids to match, until there is another selected index changed later.
        }
        protected override void SetFieldsIntoDetailsView()
        {
            SetKeyValuePairItemsToRepeater(Convert.ToInt32(APIDetails.Default), rptAPIDetails);
        }
        #endregion AdminUserControlGrid overrides

        public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid)
        {
            return new GridCacher.CacheKeyInfo(typeof(APIMetaData).Name, "");
        }

        #region events
        protected void Page_Load(object sender, EventArgs e)
        {
            txtSupportsOptions.Value = APIMetaData.SUPPORTSOPTIONS;
            APIThrottlingAndQuotasArea.Style["display"] = (AddAPI.ShowAPIThrottlingAndQuotasArea ? "default" : "none");
            NumericUpDownExtender1.Minimum = APIHelper.ThrottlingRateLimitRange[0];
            NumericUpDownExtender1.Maximum = APIHelper.ThrottlingRateLimitRange[1];
            trlinfo.InnerText = "(" + NumericUpDownExtender1.Minimum + " - " + NumericUpDownExtender1.Maximum + ")";
            txtRateLimit.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender1.Minimum + "," + NumericUpDownExtender1.Maximum + ");";

            NumericUpDownExtender2.Minimum = APIHelper.ThrottlingRenewalPeriodLimitRange[0];
            NumericUpDownExtender2.Maximum = APIHelper.ThrottlingRenewalPeriodLimitRange[1];
            trplinfo.InnerText = "(" + NumericUpDownExtender2.Minimum + " - " + NumericUpDownExtender2.Maximum + ") minutes";
            txtRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender2.Minimum + "," + NumericUpDownExtender2.Maximum + ");";

            NumericUpDownExtender3.Minimum = APIHelper.QuotaRateLimitRange[0];
            NumericUpDownExtender3.Maximum = APIHelper.QuotaRateLimitRange[1];
            qrlinfo.InnerText = "(" + NumericUpDownExtender3.Minimum + " - " + NumericUpDownExtender3.Maximum + ")";
            txtNumberCalls.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender3.Minimum + "," + NumericUpDownExtender3.Maximum + ");";

            NumericUpDownExtender4.Minimum = APIHelper.QuotaRenewalPeriodLimitRange[0];
            NumericUpDownExtender4.Maximum = APIHelper.QuotaRenewalPeriodLimitRange[1];
            qrplinfo.InnerText = "(" + NumericUpDownExtender4.Minimum + " - " + NumericUpDownExtender4.Maximum + ") minutes";
            txtQuotaRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender4.Minimum + "," + NumericUpDownExtender4.Maximum + ");";

            callFmt1 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtRateLimit.ClientID + "','" + txtRenewalPeriod.ClientID + "','" + NumericUpDownExtender1.ClientID + "','" + NumericUpDownExtender2.ClientID + "', null);";
            enableThrottling.Attributes["onclick"] = string.Format(callFmt1, "this.checked");
            callFmt2 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtNumberCalls.ClientID + "','" + txtQuotaRenewalPeriod.ClientID + "','" + NumericUpDownExtender3.ClientID + "','" + NumericUpDownExtender4.ClientID + "','" + txtKiloBytes.ClientID + "');";
            enableQuota.Attributes["onclick"] = string.Format(callFmt2, "this.checked");

            ddlAPIs.Attributes["onchange"] = "onEditAPIChanged(this);";

            lblResults.Visible = false;
            lblEditError.Visible = false;
            if (!Page.IsPostBack)
            {
                //bind all organizations since only kgs super admin or kgs system admins have access to this page
                BindAPIContainers();
                grid.Visible = false;
            }
        }
        private void Page_FirstLoad()
        {
            gridCacher.Clear();
        }
        void Page_Init()
        {
            UseKeyNotId = true;
        }

        protected void BindAPIContainers(string selectedValue = null)
        {
            ddlViewAPIContainers.Items.Cast<ListItem>().Where(item => "" != item.Value).ToList().ForEach(item => ddlViewAPIContainers.Items.Remove(item));
            IEnumerable<APIContainerMetaData> apiContainers = GetAllApiContainers();
            if (null != apiContainers)
            {
                var apiContainerCollection = apiContainers.OrderBy(container => container.Name);
                suffixes.Value = APIHelper.SetSuffixCollection(apiContainerCollection);
                IEnumerable ds = apiContainerCollection.Select(container => new { Name = container.Name, ID = container.ID });
                ddlViewAPIContainers.DataTextField = "Name";
                ddlViewAPIContainers.DataValueField = "ID";
                ddlViewAPIContainers.DataSource = ds;
                ddlViewAPIContainers.DataBind();
                if (!string.IsNullOrEmpty(selectedValue))
                {
                    ddlViewAPIContainers.SelectedValue = selectedValue;
                }
            }
        }

        private IEnumerable<APIContainerMetaData> GetAllApiContainers()
        {
            return ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_CONTAINERS_ID) as IEnumerable<APIContainerMetaData>);
        }
        protected void ddlViewAPIContainers_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            var isSelected = !string.IsNullOrEmpty(ddlViewAPIContainers.SelectedValue);

            pnlSearch.Visible = isSelected;
            grid.Visible = isSelected;
            pnlDetailsView.Visible = false;
            pnlEdit.Visible = false;
            lblResults.Visible = isSelected;
            lblDeleteError.Visible = isSelected;
            if (!isSelected)
            {
                gridCacher.Clear();
            }
            // Force refresh
            ResetUI();
            BindData(true);
            grid.Visible = true;
        }

        #region Grid Events
        protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
        {
            pnlDetailsView.Visible = false;
            pnlEdit.Visible = true;
            string exposedApiInOperation = null;
            hdnAPIId.Value = grid.DataKeys[e.NewEditIndex].Value as string;
            APIMetaData amd = APIHelper.GetAPIOperation(DeploymentId, ddlViewAPIContainers.SelectedValue, hdnAPIId.Value, suffixes.Value, ddlViewAPIContainers.SelectedIndex - 1); // -1 b/c 1st is "Select..." which we need to skip
            if (null != amd)
            {
                txtEditAPIName.Text = amd.Name;
                txtEditAPIDescription.Text = amd.Description;
                exposedApiInOperation = amd.ExposedApi;
                lblEditAPIPublicEndpoint.Text = amd.EndPoint;
                string baseEPWithSuffix = amd.EndPoint;
                int index = baseEPWithSuffix.LastIndexOf(amd.ExposedApi);
                if (index > 0)
                {
                    baseEPWithSuffix = baseEPWithSuffix.Substring(0, index);
                }
                if (!baseEPWithSuffix.EndsWith("/"))
                {
                    baseEPWithSuffix += "/";
                }
                endpoint.Value = baseEPWithSuffix;
                txtRateLimit.Text = APIHelper.ApplyRangeConstraint(amd.ThrottlingRateLimit, APIHelper.ThrottlingRateLimitRange).ToString();
                txtRenewalPeriod.Text = APIHelper.ApplyRangeConstraint(amd.ThrottlingRenewalPeriod, APIHelper.ThrottlingRenewalPeriodLimitRange).ToString();
                txtNumberCalls.Text = APIHelper.ApplyRangeConstraint(amd.QuotaRateLimit, APIHelper.QuotaRateLimitRange).ToString();
                txtQuotaRenewalPeriod.Text = APIHelper.ApplyRangeConstraint(amd.QuotaRenewalPeriod, APIHelper.QuotaRenewalPeriodLimitRange).ToString();
                txtKiloBytes.Text = NumericHelper.FormatInvariantToSpecificCulture(Convert.ToDouble(amd.QuotaBandwidth));
                enableThrottling.Checked = amd.ThrottlingRateLimit > 0;
                enableQuota.Checked = amd.QuotaRateLimit > 0;
                APIHelper.RegisterSettingThrottleAndQuotaJS(this, enableThrottling.Checked, enableQuota.Checked, callFmt1, callFmt2);
            }
            if (1 == ddlAPIs.Items.Count)
            {
                var results = APIHelper.GetAllExposedAPIs(DeploymentId);
                results.OrderBy(api => api.Key).ForEach(api =>
                {
                    ddlAPIs.Items.Add(new ListItem() { Text = api.Key, Value = api.Value });
                });
            }

            if( ! string.IsNullOrEmpty(exposedApiInOperation))
            {
                // Since the same api endpoint might support more than 1 verbs, we need to find the api by name whose value starts with the verb (amd.Method)
                ListItem item = ddlAPIs.Items.Cast<ListItem>().FirstOrDefault(li => li.Text == exposedApiInOperation && li.Value.StartsWith(amd.Method));
                if (null != item )
                {
                    ddlAPIs.SelectedIndex = -1;
                    item.Selected = true;
                    string hasOptions = amd.SupportsOptions ? APIMetaData.SUPPORTSOPTIONS : "";
                    lblEditAPIVerb.InnerText = $"{amd.Method} {hasOptions}";
                }
                else
                {
                    ddlAPIs.SelectedIndex = 0;
                    lblEditAPIVerb.InnerText = "";
                }
            }
        }

        protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
        {
            string operationId = grid.DataKeys[e.RowIndex].Value as string;
            string error = APIHelper.DeleteAPIOperation(DeploymentId, ddlViewAPIContainers.SelectedValue, operationId);
            if (!string.IsNullOrEmpty(error))
            {
                LabelHelper.SetLabelMessage(lblDeleteError, error, lnkSetFocus);
            }
            else
            {
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.DeleteProduct);
                ChangeCurrentTab(deleteSuccessful);
                SetChangeStateForTabs(SystemAPISubscriberAdministration.TabMessages.DeleteProduct);
                RebindAPIs(false);
            }
        }
        protected override void OnGridDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton imgBtnDelete = e.Row.FindControl("btnDelete") as ImageButton;
                if (null != imgBtnDelete)
                {
                    // Skip header
                    int rowIndex = e.Row.RowIndex + 1;
                    int cellIndex = 3;
                    imgBtnDelete.OnClientClick = "return(confirmAPIDeletion(" + rowIndex + "," + cellIndex + "));";
                }
            }
            base.OnGridDataBound(sender, e);
        }
        #endregion Grid Events
        #endregion events

        private IEnumerable<APIMetaData> CreateDatasetForGrid(string apiContainerId)
        {
            IEnumerable<APIMetaData> ds = null;
            if (!string.IsNullOrEmpty(apiContainerId))
            {
                // Now get operations in api container
                ds = APIHelper.GetAPIOperations(DeploymentId, apiContainerId, suffixes.Value, ddlViewAPIContainers.SelectedIndex - 1);
            }
            return ((ds?? Enumerable.Empty<APIMetaData>()).ToList());
        }
        #region button events

        protected void updateAPIButton_Click(Object sender, EventArgs e)
        {
            int[] values = { 0, 0, 0, 0, 0 };
            if (!AddAPI.ShowAPIThrottlingAndQuotasArea)
            {
                enableThrottling.Checked = false;
                enableQuota.Checked = false;
            }
            string error = APIHelper.ValidateThrottlingAndQuotaConstraints(enableThrottling.Checked, enableQuota.Checked, txtRateLimit.Text, txtRenewalPeriod.Text, txtNumberCalls.Text, txtQuotaRenewalPeriod.Text, txtKiloBytes.Text, values);
            if (string.IsNullOrEmpty(error))
            {
                ListItem li = ddlAPIs.SelectedItem;
                string httpMethod = APIHelper.GetHTTPMethod(li.Value);
                if (null != httpMethod)
                {
                    error = APIHelper.SaveAPIOperation(DeploymentId, ddlViewAPIContainers.SelectedValue, txtEditAPIName.Text, txtEditAPIDescription.Text, httpMethod, li.Text, values[0], values[1], values[2], values[3], values[4], hdnAPIId.Value);
                }
                else
                {
                    error = "Invalid value specified for API";
                }
            }

            if (string.IsNullOrEmpty(error))
            {
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.EditAPI);
                LabelHelper.SetLabelMessage(lblResults, updateSuccessful, lnkSetFocus);
                OperationComplete(OperationType.Update, updateSuccessful, SystemAPISubscriberAdministration.TabMessages.EditAPI);
            }
            else
            {
                LabelHelper.SetLabelMessage(lblEditError, error, lnkSetFocus);
            }
        }

        #endregion button events
        private void SetProductIntoViewState()
        {
            APIMetaData amd = APIHelper.GetAPIOperation(DeploymentId, ddlViewAPIContainers.SelectedValue, CurrentKey, suffixes.Value, ddlViewAPIContainers.SelectedIndex - 1); // -1 b/c 1st is "Select..." which we need to skip
            if (null != amd)
            {
                string hasOptions = amd.SupportsOptions ? APIMetaData.SUPPORTSOPTIONS : "";
                SetKeyValuePairItem("Name", amd.Name);
                SetKeyValuePairItem("Description", amd.Description);
                SetKeyValuePairItem("External Endpoint", $"{amd.EndPoint} {amd.Method}{hasOptions}");

                if (AddAPI.ShowAPIThrottlingAndQuotasArea)
                {
                    SetKeyValuePairItem("Throttling Rate Limit.Text", amd.ThrottlingRateLimit.ToString());
                    SetKeyValuePairItem("Throttling Renewal Period", amd.ThrottlingRenewalPeriod.ToString() + " minutes");
                    SetKeyValuePairItem("Quota Rate Limit", amd.QuotaRateLimit.ToString());
                    SetKeyValuePairItem("Quota Renewal Period", amd.QuotaRenewalPeriod.ToString() + " minutes");
                    SetKeyValuePairItem("Quota Bandwidth", NumericHelper.FormatInvariantToSpecificCulture(Convert.ToDouble(amd.QuotaBandwidth)) + " KB");
                }
            }
            SetDetailsViewDataToViewState(Convert.ToInt32(APIDetails.Default), true); //True means to clear viewstate since this is a new record
        }
    }
}