﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewAPIContainers.ascx.cs" Inherits="CW.Website._controls.admin.APISubscribers.ViewAPIContainers" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Website._controls.admin.APISubscribers" %>
<%@ Import Namespace="CW.Website._controls.admin.APISubscribers.Models" %>

<script type="text/javascript">
    function suffixChangedEdit(text) {
        var publicEndpoint = document.getElementById('<%=lblEditPublicEndpoint.ClientID %>');
        var baseEndpoint = document.getElementById('<%=endpoint.ClientID %>').value;
        if (text.length > 0) {
            text = text.replace(/^\W+/, '');
            text = text.replace(/^\s+/, '');
            text = text.replace(/[^0-9a-zA-Z]/g, '');
        }
        publicEndpoint.innerText = baseEndpoint + text;
    }
</script>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>  

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>       
                                                                                                                
<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="false" />
      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/select.png" ID="btnSelect" runat="server"  CausesValidation="false" ToolTip="View this product" CommandName="Select"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/edit.png" ID="btnEdit" runat="server"  CausesValidation="false" ToolTip="Edit this API Container" CommandName="Edit"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Name" HeaderText="API Name">
        <ItemTemplate>
            <label title="<%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.Name)) %>"><%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.Name), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Suffix" HeaderText="Suffix">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.Suffix)) %>"><%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.Suffix), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="HasThrottling" HeaderText="Has Throttling">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.HasThrottling)) %>"><%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.HasThrottling), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="HasQuota" HeaderText="Has Quota">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.HasQuota)) %>"><%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.HasQuota), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="NumberOfOperations" HeaderText="Number Of Operations">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.NumberOfOperations)) %>"><%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.NumberOfOperations), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EndPoint" HeaderText="External EndPoint">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.EndPoint)) %>"><%# GetCellContent(Container, PropHelper.G<APIContainerMetaData>(_ => _.EndPoint), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/delete.png" ID="btnDelete" runat="server" CausesValidation="false" ToolTip="Delete this API Container" CommandName="Delete" OnClientClick="return confirm('Are you sure you wish to delete this API permanently?\r\nPlease note that any subscribers to any Product that has this API will no longer be able to access the associated APIs.');"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>        
  </asp:GridView>                                                                      
</div>
<br />
<br />

<div>
  <!--SELECT DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptAPIDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Product Details</h2>
          <ul id="apiDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
          <br />
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>

<!--EDIT PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateAPIContainer">                                                                                             
  <div>
    <h2>Edit API Container</h2>
  </div>

  <div>
    <a id="lnkSetFocusEdit" runat="server"></a>
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnAPIContainerId" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*Name:</label>
      <asp:TextBox ID="txtEditName" CssClass="textbox" MaxLength="20" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">*Description:</label>
      <asp:TextBox ID="txtEditDescription" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>     
    <div class="divForm">
      <label class="label">Suffix:</label>
      <asp:TextBox ID="txtEditSuffix" CssClass="textbox" MaxLength="20" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">External Endpoint:</label>
      <asp:Label ID="lblEditPublicEndpoint" CssClass="labelContent" runat="server"/>
      <asp:HiddenField ID="endpoint" runat="server" Value="" />
    </div>
    <div class="divForm" style="height:auto!important">
      <label class="label">API(s) currently associated with this Container:</label>
      <div id="divAssociatedAPIs" class="divContentPreWrap labelContent" runat="server" style="display:inline-table; width:230px"/>
    </div>

    <br />
    <hr />
    <asp:CheckBox ID="enableThrottling" runat="server" Checked="false" Text="Throttling"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtRateLimit" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txtRateLimit" Width="100" Step="1" />
      <label class="labelContent" id="trlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtRateLimit" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" TargetControlID="txtRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="trplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtRenewalPeriod" FilterType="Numbers" />
    </div>
    <hr />
    <asp:CheckBox ID="enableQuota" runat="server" Checked="false" Text="Quota"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtNumberCalls" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="txtNumberCalls" Width="100" Step="1" />
      <label class="labelContent" id="qrlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtNumberCalls" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtQuotaRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender4" runat="server" TargetControlID="txtQuotaRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="qrplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtQuotaRenewalPeriod" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Bandwidth:</label>
      <asp:TextBox ID="txtKiloBytes" CssClass="textbox" MaxLength="10" Width="100" runat="server"/>&nbsp;KB
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtKiloBytes" FilterType="Numbers" />
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateAPIContainer" runat="server" Text="Update API Container"  OnClick="updateAPIContainerButton_Click" ValidationGroup="EditAPIContainer" />

    <!--Ajax Validators-->      
    <asp:RequiredFieldValidator ID="txtEditNameRFV" runat="server" ErrorMessage="Product Name is a required field." ControlToValidate="txtEditName" SetFocusOnError="true" Display="None" ValidationGroup="EditAPIContainer" />
    <ajaxToolkit:ValidatorCalloutExtender ID="txtEditNameVCE" runat="server" BehaviorID="txtEditNameVCE" TargetControlID="txtEditNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <asp:RequiredFieldValidator ID="txtEditDescriptionRFV" runat="server" ErrorMessage="Product Description is a required field." ControlToValidate="txtEditDescription" SetFocusOnError="true" Display="None" ValidationGroup="EditAPIContainer" />
    <ajaxToolkit:ValidatorCalloutExtender ID="txtEditDescriptionVCE" runat="server" BehaviorID="txtEditDescriptionVCE" TargetControlID="txtEditDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  </div>
</asp:Panel>