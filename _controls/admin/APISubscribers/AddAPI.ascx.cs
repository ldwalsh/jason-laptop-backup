﻿using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._controls.admin.APISubscribers.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.APISubscribers
{
    public partial class AddAPI : AdminUserControlBase
    {
        static public bool ShowAPIThrottlingAndQuotasArea = false;
        #region Constants
        const String addProductSuccess = " API addition was successful.";
        #endregion Constants
        public AddAPI() : base()
        {
            DeploymentId = ConfigurationManager.AppSettings["DeploymentID"];
        }

        #region overrides

        public override IEnumerable<Enum> ReactToChangeStateList
        {
            get
            {
                return new Enum[]
                {
                    SystemAPISubscriberAdministration.TabMessages.AddAPIContainer,
                    SystemAPISubscriberAdministration.TabMessages.EditAPIContainer,
                    SystemAPISubscriberAdministration.TabMessages.DeleteAPIContainer
                };
            }
        }
        protected override void InitializeControl()
        {
            PopulateAPIContainersDdl();

            new AdminUserControlForm(this).ResetDropDownLists();
        }

        #endregion
        private string DeploymentId { get; set; }
        protected void PopulateAPIContainersDdl()
        {
            ddlAddAPIContainers.Items.Cast<ListItem>().Where(item => "" != item.Value).ToList().ForEach(item => ddlAddAPIContainers.Items.Remove(item));
            IEnumerable<APIContainerMetaData> apiContainers = GetAllApiContainers();
            if (null != apiContainers)
            {
                var apiContainerCollection = apiContainers.OrderBy(container => container.Name);
                suffixes.Value = APIHelper.SetSuffixCollection(apiContainerCollection);
                //apiContainerCollection.ForEach(_ =>
                //{
                //    ddlAddAPIContainers.Items.Add(new ListItem() { Text = _.Name, Value = _.ID });
                //});

                //For some reason, the ddl.DataBind() is causing JS errors
                IEnumerable ds = apiContainerCollection.Select(container => new { Name = container.Name, ID = container.ID });
                ddlAddAPIContainers.DataTextField = "Name";
                ddlAddAPIContainers.DataValueField = "ID";
                ddlAddAPIContainers.DataSource = ds;
                ddlAddAPIContainers.DataBind();
            }
        }
        private IEnumerable<APIContainerMetaData> GetAllApiContainers()
        {
            return ((Page as AdminSitePageTemp).GetCachedObject(APIHelper.API_CONTAINERS_ID) as IEnumerable<APIContainerMetaData>);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            APIThrottlingAndQuotasArea.Style["display"] = (ShowAPIThrottlingAndQuotasArea ? "default" : "none");
            ddlAddAPIContainers.Attributes["onchange"] = "onAPIContainerChanged(this);";
            ddlAddAPIs.Attributes["onchange"] = "onAPIChanged(this);";
            NumericUpDownExtender1.Minimum = APIHelper.ThrottlingRateLimitRange[0];
            NumericUpDownExtender1.Maximum = APIHelper.ThrottlingRateLimitRange[1];
            trlinfo.InnerText = "(" + NumericUpDownExtender1.Minimum + " - " + NumericUpDownExtender1.Maximum + ")";
            txtRateLimit.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender1.Minimum + "," + NumericUpDownExtender1.Maximum + ");";

            NumericUpDownExtender2.Minimum = APIHelper.ThrottlingRenewalPeriodLimitRange[0];
            NumericUpDownExtender2.Maximum = APIHelper.ThrottlingRenewalPeriodLimitRange[1];
            trplinfo.InnerText = "(" + NumericUpDownExtender2.Minimum + " - " + NumericUpDownExtender2.Maximum + ") minutes";
            txtRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender2.Minimum + "," + NumericUpDownExtender2.Maximum + ");";

            NumericUpDownExtender3.Minimum = APIHelper.QuotaRateLimitRange[0];
            NumericUpDownExtender3.Maximum = APIHelper.QuotaRateLimitRange[1];
            qrlinfo.InnerText = "(" + NumericUpDownExtender3.Minimum + " - " + NumericUpDownExtender3.Maximum + ")";
            txtNumberCalls.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender3.Minimum + "," + NumericUpDownExtender3.Maximum + ");";

            NumericUpDownExtender4.Minimum = APIHelper.QuotaRenewalPeriodLimitRange[0];
            NumericUpDownExtender4.Maximum = APIHelper.QuotaRenewalPeriodLimitRange[1];
            qrplinfo.InnerText = "(" + NumericUpDownExtender4.Minimum + " - " + NumericUpDownExtender4.Maximum + ") minutes";
            txtQuotaRenewalPeriod.Attributes["onblur"] = "checkThrottlingQuotaInput(this, " + NumericUpDownExtender4.Minimum + "," + NumericUpDownExtender4.Maximum + ");";

            string callFmt1 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtRateLimit.ClientID + "','" + txtRenewalPeriod.ClientID + "','" + NumericUpDownExtender1.ClientID + "','" + NumericUpDownExtender2.ClientID + "', null);";
            enableThrottling.Attributes["onclick"] = string.Format(callFmt1, "this.checked");
            string callFmt2 = "setEnabledBasedOnThrottleQuotaEnableState({0}, '" + txtNumberCalls.ClientID + "','" + txtQuotaRenewalPeriod.ClientID + "','" + NumericUpDownExtender3.ClientID + "','" + NumericUpDownExtender4.ClientID + "','" + txtKiloBytes.ClientID + "');";
            enableQuota.Attributes["onclick"] = string.Format(callFmt2, "this.checked");
            txtSupportsOptions.Value = APIMetaData.SUPPORTSOPTIONS;
            if (!IsPostBack)
            {
                txtRateLimit.Text = NumericUpDownExtender1.Minimum.ToString();
                txtRenewalPeriod.Text = NumericUpDownExtender2.Minimum.ToString();
                txtNumberCalls.Text = NumericUpDownExtender3.Minimum.ToString();
                txtQuotaRenewalPeriod.Text = NumericUpDownExtender4.Minimum.ToString();
                var results = APIHelper.GetAllExposedAPIs(DeploymentId);
                if (null != results)
                {
                    txtKiloBytes.Text = "0";
                    PopulateAPIContainersDdl();
                    lblPublicEndpoint.Text = addAPIEndpoint.Value = APIHelper.ServiceRootEndPoint;
                    results.OrderBy(api => api.Key).ForEach(api =>
                    {
                        ddlAddAPIs.Items.Add(new ListItem() { Text = api.Key, Value = api.Value });
                    });
                    lblAPIVerb.InnerText = "";
                }
            }
            APIHelper.RegisterSettingThrottleAndQuotaJS(this, enableThrottling.Checked, enableQuota.Checked, callFmt1, callFmt2);
        }

        protected void addAPIButton_Click(Object sender, EventArgs e)
        {
            lblAddError.Text = "";
            lblResults.Text = "";
            int[] values = { 0, 0, 0, 0, 0 };
            if (!ShowAPIThrottlingAndQuotasArea)
            {
                enableThrottling.Checked = false;
                enableQuota.Checked = false;
            }

            string error = APIHelper.ValidateThrottlingAndQuotaConstraints(enableThrottling.Checked, enableQuota.Checked, txtRateLimit.Text, txtRenewalPeriod.Text, txtNumberCalls.Text, txtQuotaRenewalPeriod.Text, txtKiloBytes.Text, values);
            if( string.IsNullOrEmpty(error))
            {
                ListItem li = ddlAddAPIs.SelectedItem;
                string httpMethod = APIHelper.GetHTTPMethod(li.Value);
                if (null != httpMethod)
                {
                    error = APIHelper.SaveAPIOperation(DeploymentId, ddlAddAPIContainers.SelectedValue, txtAddAPIName.Text, txtAddAPIDescription.Text, httpMethod, li.Text, values[0], values[1], values[2], values[3], values[4]);
                }
                else
                {
                    error = "Invalid value specified for API";
                }
            }

            if (string.IsNullOrEmpty(error))
            {
                (Page as AdminSitePageTemp).NotifyPageOnChangeEvent(SystemAPISubscriberAdministration.TabMessages.AddAPI);
                LabelHelper.SetLabelMessage(lblResults, addProductSuccess, lnkSetFocus);
                OperationComplete(SystemAPISubscriberAdministration.TabMessages.AddAPI);
            }
            else
            {
                LabelHelper.SetLabelMessage(lblAddError, error, lnkSetFocus);
            }
        }
    }
}