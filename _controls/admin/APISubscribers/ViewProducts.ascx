﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewProducts.ascx.cs" Inherits="CW.Website._controls.admin.APISubscribers.ViewProducts" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Website._controls.admin.APISubscribers" %>
<%@ Import Namespace="CW.Website._controls.admin.APISubscribers.Models" %>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>  

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>       

<script type="text/javascript">
    function getProductName(rowIndex, cellIndex) {
        var grid = document.getElementById('<%= grid.ClientID %>');
        var productName = grid.rows[rowIndex].cells[cellIndex].innerText;
        return (productName);
    }
    function confirmProductDeletion(rowIndex, cellIndex) {
        var message = "Are you sure you wish to delete the Product:\n'" + getProductName(rowIndex, cellIndex) + "' permanently?\r\nPlease note that any subscribers to this Product will no longer be able to access the associated APIs.";
        return confirm(message)
    }

    function confirmProductClone(rowIndex, cellIndex) {
        var message = "Are you sure you wish to clone the Product:\n'" + getProductName(rowIndex, cellIndex) + "' ?\r\nThis can take some time.";
        return confirm(message)
    }
</script>
<div id="gridTbl">
  <asp:GridView ID="grid" 
      EnableViewState="true" 
      OnRowCommand="OnRowCommand"                                                                              
      runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="false" />
      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/select.png" ID="btnSelect" runat="server"  CausesValidation="false" ToolTip="View this product" CommandName="Select"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/edit.png" ID="btnEdit" runat="server"  CausesValidation="false" ToolTip="Edit this product" CommandName="Edit"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/clone.png" ID="btnClone" runat="server"  CausesValidation="false" ToolTip="Clone this product" CommandArgument="CloneProduct" CommandName="CloneProduct"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Name" HeaderText="Product Name">
        <ItemTemplate>
            <label title="<%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.Name)) %>"><%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.Name), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="SubscriptionRequiredDisplay" HeaderText="Subscription Required">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.SubscriptionRequiredDisplay)) %>"><%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.SubscriptionRequiredDisplay)) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ApprovalRequiredDisplay" HeaderText="Approval Required">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.ApprovalRequiredDisplay)) %>"><%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.ApprovalRequiredDisplay)) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PublishedDisplay" HeaderText="Published">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.PublishedDisplay)) %>"><%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.PublishedDisplay)) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="SubscriberCount" HeaderText="Subscribers">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.SubscriberCount)) %>"><%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.SubscriberCount)) %></label>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="HasAPIsDisplay" HeaderText="APIs">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.HasAPIsDisplay)) %>"><%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.HasAPIsDisplay)) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="HasThrottlingDisplay" HeaderText="Throttling">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.HasThrottlingDisplay)) %>"><%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.HasThrottlingDisplay)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="HasQuotaDisplay" HeaderText="Quota">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.HasQuotaDisplay)) %>"><%# GetCellContent(Container, PropHelper.G<APIProductMetaData>(_ => _.HasQuotaDisplay)) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/delete.png" ID="btnDelete" runat="server" CausesValidation="false" ToolTip="Delete this product" CommandName="Delete"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>        
  </asp:GridView>                                                                      
</div>
<br />
<br />

<div>
  <!--SELECT DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptProductDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Product Details</h2>
          <ul id="apiProductDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
          <br />
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>

<!--EDIT PANEL -->                              
    <script type="text/javascript">
        function onPublishedChangeEdit(published) {
            document.getElementById('<%=lblPublishInfoEdit.ClientID %>').innerText = (published ? "" : "<%=productNotPubliclyAccessible %>");
        }
    </script>
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateProduct">                                                                                             
  <div>
    <h2>Edit Product</h2>
  </div>

  <div>
    <a id="lnkSetFocusEdit" runat="server"></a>
    <asp:Label ID="lblEditResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnProductId" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*Name:</label>
      <asp:TextBox ID="txtEditName" CssClass="textbox" MaxLength="20" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">*Description:</label>
      <asp:TextBox ID="txtEditDescription" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>     
    <div class="divForm">
      <label class="label">*Terms:</label>
      <asp:TextBox ID="txtEditTerms" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Associate API Container(s) to Product:</label>
      <asp:ListBox ID="lbEditProductApis" AppendDataBoundItems="false" CssClass="listbox" runat="server" Width="230" SelectionMode="Multiple" />
    </div>
    <div class="divForm">
      <label class="label">Subscription Required:</label>
      <asp:CheckBox ID="ckEditSubscriptionRequired" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">Approval Required:</label>
      <asp:CheckBox ID="ckEditApprovalRequired" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">Published:</label>
      <asp:CheckBox ID="ckEditPublished" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label"></label>
      <label class="labelContent" id="lblPublishInfoEdit" runat="server"></label>
    </div>
    <div class="divForm">
      <label class="label">Subscription Limit:</label>
      <asp:TextBox ID="txtEditSubscriptionLimit" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender0" runat="server" TargetControlID="txtEditSubscriptionLimit" Width="100" Step="1"/>
      <label class="labelContent" id="subinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtEditSubscriptionLimit" FilterType="Numbers" />
    </div>
    <hr />
    <asp:CheckBox ID="enableThrottling" runat="server" Checked="false" Text="Throttling"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtEditRateLimit" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txtEditRateLimit" Width="100" Step="1" />
      <label class="labelContent" id="trlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtEditRateLimit" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtEditRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" TargetControlID="txtEditRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="trplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtEditRenewalPeriod" FilterType="Numbers" />
    </div>
    <hr />
    <asp:CheckBox ID="enableQuota" runat="server" Checked="false" Text="Quota"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtEditNumberCalls" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="txtEditNumberCalls" Width="100" Step="1" />
      <label class="labelContent" id="qrlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtEditNumberCalls" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtEditQuotaRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender4" runat="server" TargetControlID="txtEditQuotaRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="qrplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtEditQuotaRenewalPeriod" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Bandwidth:</label>
      <asp:TextBox ID="txtEditKiloBytes" CssClass="textbox" MaxLength="10" Width="100" runat="server"/>&nbsp;KB
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtEditKiloBytes" FilterType="Numbers" />
    </div>
    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateProduct" runat="server" Text="Update Product"  OnClick="updateProductButton_Click" ValidationGroup="EditProduct" />

    <!--Ajax Validators-->      
    <asp:RequiredFieldValidator ID="txtNameEditRFV" runat="server" ErrorMessage="Product Name is a required field." ControlToValidate="txtEditName" SetFocusOnError="true" Display="None" ValidationGroup="EditProduct" />
    <ajaxToolkit:ValidatorCalloutExtender ID="txtNameEditVCE" runat="server" BehaviorID="txtNameEditVCE" TargetControlID="txtNameEditRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <asp:RequiredFieldValidator ID="txtDescriptionEditRFV" runat="server" ErrorMessage="Product Description is a required field." ControlToValidate="txtEditDescription" SetFocusOnError="true" Display="None" ValidationGroup="EditProduct" />
    <ajaxToolkit:ValidatorCalloutExtender ID="txtDescriptionEditVCE" runat="server" BehaviorID="txtDescriptionEditVCE" TargetControlID="txtDescriptionEditRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <asp:RequiredFieldValidator ID="txtTermsEditRFV" runat="server" ErrorMessage="Terms is a required field." ControlToValidate="txtEditTerms" SetFocusOnError="true" Display="None" ValidationGroup="EditProduct" />
    <ajaxToolkit:ValidatorCalloutExtender ID="txtTermsEditVCE" runat="server" BehaviorID="txtTermsEditVCE" TargetControlID="txtTermsEditRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <asp:RequiredFieldValidator ID="editProductAPIsRequiredValidator" runat="server" ErrorMessage="At least one API Container is required." ControlToValidate="lbEditProductApis" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="EditProduct" />
    <ajaxToolkit:ValidatorCalloutExtender ID="editProductAPIsRequiredValidatorExtender" runat="server" TargetControlID="editProductAPIsRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
</asp:Panel>