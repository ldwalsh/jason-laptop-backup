﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddProduct.ascx.cs" Inherits="CW.Website._controls.admin.APISubscribers.AddProduct" %>
<asp:Panel ID="pnlAddProduct" runat="server" DefaultButton="btnAddProduct">
    <script type="text/javascript">
        function onPublishedChangeAdd(published) {
            document.getElementById('<%=publishInfoAdd.ClientID %>').innerText = (published ? "" : "<%=productNotPubliclyAccessible %>");
        }
    </script>
  <h2>Add Product for APIs</h2>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />
    <div class="divForm">
      <label class="label">*Name:</label>
      <asp:TextBox ID="txtName" CssClass="textbox" MaxLength="20" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">*Description:</label>
      <asp:TextBox ID="txtDescription" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>     
    <div class="divForm">
      <label class="label">*Terms:</label>
      <asp:TextBox ID="txtTerms" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Associate API Container(s) to Product:</label>
      <asp:ListBox ID="lbAddProductAPIs" AppendDataBoundItems="false" CssClass="listbox" runat="server" Width="230" SelectionMode="Multiple" />
    </div>
    <div class="divForm">
      <label class="label">Subscription Required:</label>
      <asp:CheckBox ID="ckSubscriptionRequired" Checked="true" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">Approval Required:</label>
      <asp:CheckBox ID="ckApprovalRequired" runat="server" Checked="false" Enabled="false"/>
    </div>
    <div class="divForm">
      <label class="label">Published:</label>
      <asp:CheckBox ID="ckPublished" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label"></label>
      <label class="labelContent" id="publishInfoAdd" runat="server"></label>
    </div>
    <div class="divForm">
      <label class="label">Subscription Limit:</label>
      <asp:TextBox ID="txtSubscriptionLimit" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="addProductNumericUpDownExtender0" runat="server" TargetControlID="txtSubscriptionLimit" Width="100" Step="1"/>
      <label class="labelContent" id="subinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtSubscriptionLimit" FilterType="Numbers" />
    </div>
    <hr />
    <asp:CheckBox ID="enableThrottling" runat="server" Checked="false" Text="Throttling"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtRateLimit" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="addProductNumericUpDownExtender1" runat="server" TargetControlID="txtRateLimit" Width="100" Step="1" />
      <label class="labelContent" id="trlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtRateLimit" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="addProductNumericUpDownExtender2" runat="server" TargetControlID="txtRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="trplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtRenewalPeriod" FilterType="Numbers" />
    </div>
    <hr />
    <asp:CheckBox ID="enableQuota" runat="server" Checked="false" Text="Quota"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtNumberCalls" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="addProductNumericUpDownExtender3" runat="server" TargetControlID="txtNumberCalls" Width="100" Step="1" />
      <label class="labelContent" id="qrlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtNumberCalls" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtQuotaRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="addProductNumericUpDownExtender4" runat="server" TargetControlID="txtQuotaRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="qrplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtQuotaRenewalPeriod" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Bandwidth:</label>
      <asp:TextBox ID="txtKiloBytes" CssClass="textbox" MaxLength="10" Width="100" runat="server"/>&nbsp;KB
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtKiloBytes" FilterType="Numbers" />
    </div>
  </div>

  <asp:LinkButton CssClass="lnkButton" ID="btnAddProduct" runat="server" Text="Add Product"  OnClick="addProductButton_Click" ValidationGroup="AddProduct" />
    
  <asp:RequiredFieldValidator ID="txtNameRFV" runat="server" ErrorMessage="Product Name is a required field." ControlToValidate="txtName" SetFocusOnError="true" Display="None" ValidationGroup="AddProduct" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtNameVCE" runat="server" BehaviorID="txtNameVCE" TargetControlID="txtNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="txtDescriptionRFV" runat="server" ErrorMessage="Product Description is a required field." ControlToValidate="txtDescription" SetFocusOnError="true" Display="None" ValidationGroup="AddProduct" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtDescriptionVCE" runat="server" BehaviorID="txtDescriptionVCE" TargetControlID="txtDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="txtTermsRFV" runat="server" ErrorMessage="Terms is a required field." ControlToValidate="txtTerms" SetFocusOnError="true" Display="None" ValidationGroup="AddProduct" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtTermsVCE" runat="server" BehaviorID="txtTermsVCE" TargetControlID="txtTermsRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addProductAPIsRequiredValidator" runat="server" ErrorMessage="At least one API Container is required." ControlToValidate="lbAddProductAPIs" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="AddProduct" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addProductAPIsRequiredValidatorExtender" runat="server" TargetControlID="addProductAPIsRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>    
