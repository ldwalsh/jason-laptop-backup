﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewAPIs.ascx.cs" Inherits="CW.Website._controls.admin.APISubscribers.ViewAPIs" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Website._controls.admin.APISubscribers" %>
<%@ Import Namespace="CW.Website._controls.admin.APISubscribers.Models" %>

<script type="text/javascript">
    function confirmAPIDeletion(rowIndex, cellIndex) {
        var grid = document.getElementById('<%= grid.ClientID %>');
        var apiName = grid.rows[rowIndex].cells[cellIndex].innerText;
        var message = "Are you sure you wish to delete the API '" + apiName + "' permanently?\r\nPlease note that any user subscribing to this API will no longer be able to access this API.";
        return confirm(message)
    }
</script>
<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>  

<div id="divViewAPIContainer" class="divForm" runat="server">   
    <label class="label">*Select API Container:</label>    
    <asp:DropDownList ID="ddlViewAPIContainers" CssClass="dropdown" 
        AppendDataBoundItems="true" 
        OnSelectedIndexChanged="ddlViewAPIContainers_OnSelectedIndexChanged" 
        AutoPostBack="true" runat="server">    
        <asp:ListItem Value="">Select one...</asp:ListItem>                          
    </asp:DropDownList> 
    <asp:HiddenField ID="suffixes" Value="" runat="server" />
</div> 
<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>       
                                                                                                                
<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="false" />
      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/select.png" ID="btnSelect" runat="server"  CausesValidation="false" ToolTip="View this API" CommandName="Select"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/edit.png" ID="btnEdit" runat="server"  CausesValidation="false" ToolTip="Edit this API" CommandName="Edit"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Name" HeaderText="API Name">
        <ItemTemplate>
            <label title="<%# GetCellContent(Container, PropHelper.G<APIMetaData>(_ => _.Name)) %>"><%# GetCellContent(Container, PropHelper.G<APIMetaData>(_ => _.Name), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Method" HeaderText="Method">
        <ItemTemplate>
            <label title="<%# GetCellContent(Container, PropHelper.G<APIMetaData>(_ => _.Method)) %>"><%# GetCellContent(Container, PropHelper.G<APIMetaData>(_ => _.Method), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="HasThrottling" HeaderText="Has Throttling">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIMetaData>(_ => _.HasThrottling)) %>"><%# GetCellContent(Container, PropHelper.G<APIMetaData>(_ => _.HasThrottling), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="HasQuota" HeaderText="Has Quota">          
        <ItemTemplate>
          <label title="<%# GetCellContent(Container, PropHelper.G<APIMetaData>(_ => _.HasQuota)) %>"><%# GetCellContent(Container, PropHelper.G<APIMetaData>(_ => _.HasQuota), 100) %></label>
        </ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:ImageButton ImageUrl="/_assets/images/delete.png" ID="btnDelete" runat="server" CausesValidation="false" ToolTip="Delete this API" CommandName="Delete"></asp:ImageButton></div>
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>        
  </asp:GridView>                                                                      
</div>
<br />
<br />

<div>
    <script type="text/javascript">
        function onEditAPIChanged(ddl) {
            var publicEndpoint = document.getElementById('<%=lblEditAPIPublicEndpoint.ClientID %>');
            var baseEndpoint = document.getElementById('<%=endpoint.ClientID %>').value;
            var text = ("" == ddl.options[ddl.selectedIndex].value ? "" : ddl.options[ddl.selectedIndex].text);
            if (text.length > 0) {
                text = text.replace(/^\W+/, '');
                text = text.replace(/^\s+/, '');
                text = text.replace(/[^0-9a-zA-Z]/g, '');
            }
            publicEndpoint.innerText = baseEndpoint + text;

            var verb = "", options = "";
            if ("" != ddl.options[ddl.selectedIndex].value) {
                var array = ddl.options[ddl.selectedIndex].value.split('_');
                if (array.length > 0) {
                    verb = array[0];
                    options = array[2] == "true" ? document.getElementById('<%=txtSupportsOptions.ClientID %>').value : "";
                }
            }
            document.getElementById('<%=lblEditAPIVerb.ClientID %>').innerText = verb + options;
        }
    </script>
  <!--SELECT DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptAPIDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>API Details</h2>
          <ul id="apiDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
          <br />
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>

<!--EDIT PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateAPI">                                                                                             
  <div>
    <h2>Edit API</h2>
  </div>

  <div>
    <a id="lnkSetFocusEdit" runat="server"></a>
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnAPIId" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*Name:</label>
      <asp:TextBox ID="txtEditAPIName" CssClass="textbox" MaxLength="20" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">*Description:</label>
      <asp:TextBox ID="txtEditAPIDescription" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>     
    <div class="divForm">
      <label class="label">*API:</label>
      <asp:DropDownList ID="ddlAPIs" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
          <asp:ListItem Value="">Select one...</asp:ListItem>
      </asp:DropDownList>
      &nbsp;<label class="labelContent" id="lblEditAPIVerb" runat="server"></label>
      <asp:HiddenField ID="txtSupportsOptions" runat="server" Value="" />
    </div>     
    <div class="divForm">
      <label class="label">External Endpoint:</label>
      <asp:Label ID="lblEditAPIPublicEndpoint" CssClass="labelContent" runat="server"/>
      <asp:HiddenField ID="endpoint" runat="server" Value="" />
    </div>

    <div id="APIThrottlingAndQuotasArea" runat="server">
    <br />
    <hr />
    <asp:CheckBox ID="enableThrottling" runat="server" Checked="false" Text="Throttling"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtRateLimit" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txtRateLimit" Width="100" Step="1" />
      <label class="labelContent" id="trlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtRateLimit" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" TargetControlID="txtRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="trplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtRenewalPeriod" FilterType="Numbers" />
    </div>
    <hr />
    <asp:CheckBox ID="enableQuota" runat="server" Checked="false" Text="Quota"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtNumberCalls" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="txtNumberCalls" Width="100" Step="1" />
      <label class="labelContent" id="qrlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtNumberCalls" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtQuotaRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender4" runat="server" TargetControlID="txtQuotaRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="qrplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtQuotaRenewalPeriod" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Bandwidth:</label>
      <asp:TextBox ID="txtKiloBytes" CssClass="textbox" MaxLength="10" Width="100" runat="server"/>&nbsp;KB
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtKiloBytes" FilterType="Numbers" />
    </div>
    </div>
    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateAPI" runat="server" Text="Update API"  OnClick="updateAPIButton_Click" ValidationGroup="EditAPI" />

    <!--Ajax Validators-->      
    <asp:RequiredFieldValidator ID="txtEditAPINameRFV" runat="server" ErrorMessage="Name is a required field." ControlToValidate="txtEditAPIName" SetFocusOnError="true" Display="None" ValidationGroup="EditAPI" />
    <ajaxToolkit:ValidatorCalloutExtender ID="txtEditAPINameVCE" runat="server" BehaviorID="txtEditAPINameVCE" TargetControlID="txtEditAPINameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <asp:RequiredFieldValidator ID="txtEditAPIDescriptionRFV" runat="server" ErrorMessage="Description is a required field." ControlToValidate="txtEditAPIDescription" SetFocusOnError="true" Display="None" ValidationGroup="EditAPI" />
    <ajaxToolkit:ValidatorCalloutExtender ID="txtEditAPIDescriptionVCE" runat="server" BehaviorID="txtEditAPIDescriptionVCE" TargetControlID="txtEditAPIDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <asp:RequiredFieldValidator ID="apiEditRequiredValidator" runat="server" ErrorMessage="API is a required field." ControlToValidate="ddlAPIs" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="EditAPI" />
    <ajaxToolkit:ValidatorCalloutExtender ID="apiEditRequiredValidatorExtender" runat="server" TargetControlID="apiEditRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
</asp:Panel>