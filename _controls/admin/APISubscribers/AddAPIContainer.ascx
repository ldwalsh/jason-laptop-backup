﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddAPIContainer.ascx.cs" Inherits="CW.Website._controls.admin.APISubscribers.AddAPIContainer" %>
<asp:Panel ID="pnlAddAPIContainer" runat="server" DefaultButton="btnAddAPIContainer">

    <script type="text/javascript">
        function suffixChangedAdd(text) {
            var publicEndpoint = document.getElementById('<%=lblAddPublicEndpoint.ClientID %>');
            var baseEndpoint = document.getElementById('<%=endpoint.ClientID %>').value;
            if (text.length > 0) {
                text = text.replace(/^\W+/, '');
                text = text.replace(/^\s+/, '');
                text = text.replace(/[^0-9a-zA-Z]/g, '');
            }
            publicEndpoint.innerText = baseEndpoint + text;
        }
    </script>
  <h2>Add API Container</h2>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />
    <div class="divForm">
      <label class="label">*Name:</label>
      <asp:TextBox ID="txtAddName" CssClass="textbox" MaxLength="20" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">*Description:</label>
      <asp:TextBox ID="txtAddDescription" TextMode="MultiLine" Height="100" Width="225" runat="server" />
    </div>     
    <div class="divForm">
      <label class="label">Suffix:</label>
      <asp:TextBox ID="txtAddSuffix" CssClass="textbox" MaxLength="20" runat="server"/>
    </div>
    <div class="divForm">
      <label class="label">External Endpoint:</label>
      <asp:Label ID="lblAddPublicEndpoint" CssClass="labelContent" runat="server"/>
      <asp:HiddenField ID="endpoint" runat="server" Value="" />
    </div>
    <br />
    <hr />
    <asp:CheckBox ID="enableThrottling" runat="server" Checked="false" Text="Throttling"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtRateLimit" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender1" runat="server" TargetControlID="txtRateLimit" Width="100" Step="1" />
      <label class="labelContent" id="trlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtRateLimit" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender2" runat="server" TargetControlID="txtRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="trplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtRenewalPeriod" FilterType="Numbers" />
    </div>
    <hr />
    <asp:CheckBox ID="enableQuota" runat="server" Checked="false" Text="Quota"/>
    <div class="divForm">
      <label class="label">Rate Limit:</label>
      <asp:TextBox ID="txtNumberCalls" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender3" runat="server" TargetControlID="txtNumberCalls" Width="100" Step="1" />
      <label class="labelContent" id="qrlinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtNumberCalls" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Renewal Period:</label>
      <asp:TextBox ID="txtQuotaRenewalPeriod" runat="server"/>
      <ajaxToolkit:NumericUpDownExtender ID="NumericUpDownExtender4" runat="server" TargetControlID="txtQuotaRenewalPeriod" Width="100" Step="1" />
      <label class="labelContent" id="qrplinfo" runat="server"></label>
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtQuotaRenewalPeriod" FilterType="Numbers" />
    </div>
    <div class="divForm">
      <label class="label">Bandwidth:</label>
      <asp:TextBox ID="txtKiloBytes" CssClass="textbox" MaxLength="10" Width="100" runat="server"/>&nbsp;KB
      <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtKiloBytes" FilterType="Numbers" />
    </div>
    <asp:LinkButton CssClass="lnkButton" ID="btnAddAPIContainer" runat="server" Text="Add API Container"  OnClick="addAPIContainerButton_Click" ValidationGroup="AddAPIContainer" />
  </div>

  <asp:RequiredFieldValidator ID="txtAddNameRFV" runat="server" ErrorMessage="API Container Name is a required field." ControlToValidate="txtAddName" SetFocusOnError="true" Display="None" ValidationGroup="AddAPIContainer" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtAddNameVCE" runat="server" BehaviorID="txtAddNameVCE" TargetControlID="txtAddNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="txtAddDescriptionRFV" runat="server" ErrorMessage="API Container Description is a required field." ControlToValidate="txtAddDescription" SetFocusOnError="true" Display="None" ValidationGroup="AddAPIContainer" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtAddDescriptionVCE" runat="server" BehaviorID="txtAddDescriptionVCE" TargetControlID="txtAddDescriptionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>    
