﻿using CW.Common.Constants;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;

namespace CW.Website._controls.admin
{
    public abstract class AdminNavLinks : SiteUserControl
    {
        #region enums

            protected enum AdminNavBooleanChecks 
            {
                DiagnosticsModuleCheck = 0,
                AdminNavCheck = 1, 
                AdminNavCombinedCheck = 2,
                AdminNavOrgCheck = 3,
                ProviderAdminNavCheck = 4, 
                ProviderAdminNavCombinedCheck = 5, 
                ProviderAdminNavOrgCheck = 6,
                KGSAdminNavCheck = 7,
                KGSAdminNavOrgCheck = 8,
                KGSSystemAdminCheck = 9 
            }

        #endregion

        #region fields

            String attributeName = "data-boolean-check";
            Boolean isSuperAdminOrHigher, isSuperAdmin, isProxyClient, isLoggedInUnderProviderClient, isKGSSuperAdminOrHigher, isKGSFullAdminOrHigher, isKGSSystemAdmin, isOrgAdmin;

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                isSuperAdminOrHigher = siteUser.IsSuperAdminOrHigher;
                isSuperAdmin = siteUser.IsSuperAdmin;
                isProxyClient = siteUser.IsProxyClient;
                isLoggedInUnderProviderClient = siteUser.IsLoggedInUnderProviderClient;
                isKGSSuperAdminOrHigher = siteUser.IsKGSSuperAdminOrHigher;
                isKGSFullAdminOrHigher = siteUser.IsKGSFullAdminOrHigher;
                isOrgAdmin = siteUser.IsOrgAdmin;
                isKGSSystemAdmin = siteUser.IsKGSSystemAdmin;

                SetNavVisibility();
            }

        #endregion

        #region methods
            static private AdminNavBooleanChecks[] mChecks = new AdminNavBooleanChecks[] { AdminNavBooleanChecks.DiagnosticsModuleCheck,
                                                                                           AdminNavBooleanChecks.AdminNavCheck,
                                                                                           AdminNavBooleanChecks.AdminNavCombinedCheck, 
                                                                                           AdminNavBooleanChecks.AdminNavOrgCheck,
                                                                                           AdminNavBooleanChecks.ProviderAdminNavCheck,
                                                                                           AdminNavBooleanChecks.ProviderAdminNavCombinedCheck,
                                                                                           AdminNavBooleanChecks.ProviderAdminNavOrgCheck,
                                                                                           AdminNavBooleanChecks.KGSAdminNavCheck,
                                                                                           AdminNavBooleanChecks.KGSAdminNavOrgCheck,
                                                                                           AdminNavBooleanChecks.KGSSystemAdminCheck
                                                                                        };

            private void SetNavItemVisibility(HtmlGenericControl li)
            {
                li.Visible = false; //default to false in case of an item being improperly set via UI

                var booleanCheck = Convert.ToInt32(li.Attributes[attributeName]);
                for (int i = 0, iNum = mChecks.Length; i < iNum && !IsVisible(li, booleanCheck, mChecks[i]); i++) ;
            }
            private void SetNavVisibility()
            {
                ControlHelper.ActOnControlsByType<HtmlGenericControl>(this, SetNavItemVisibility, li => null != li.Attributes[attributeName] );
            }

            private Boolean IsVisible(HtmlGenericControl li, Int32 booleanCheck, AdminNavBooleanChecks enumBooleanCheck)
            {
                if (booleanCheck == (Int32)enumBooleanCheck) li.Visible = Criteria(enumBooleanCheck);

                return li.Visible;
            }

            private Boolean Criteria(AdminNavBooleanChecks enumBooleanCheck)
            {
                switch (enumBooleanCheck)
                {
                    case AdminNavBooleanChecks.DiagnosticsModuleCheck:
                        return siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Diagnostics);
                    case AdminNavBooleanChecks.AdminNavCheck:
                        return isKGSSuperAdminOrHigher || isSuperAdmin;
                    case AdminNavBooleanChecks.AdminNavCombinedCheck:
                        return isKGSSuperAdminOrHigher || (isSuperAdmin && !isLoggedInUnderProviderClient) || (isSuperAdmin && isProxyClient && isLoggedInUnderProviderClient);
                    case AdminNavBooleanChecks.AdminNavOrgCheck:
                        return (isKGSSuperAdminOrHigher && isOrgAdmin) || (isOrgAdmin && !isLoggedInUnderProviderClient) || (isOrgAdmin && isProxyClient && isLoggedInUnderProviderClient);
                    case AdminNavBooleanChecks.ProviderAdminNavCheck:
                        return isKGSSuperAdminOrHigher || (isSuperAdmin && isLoggedInUnderProviderClient);
                    case AdminNavBooleanChecks.ProviderAdminNavCombinedCheck:
                        return isKGSSuperAdminOrHigher || (isSuperAdmin && isProxyClient && isLoggedInUnderProviderClient);
                    case AdminNavBooleanChecks.ProviderAdminNavOrgCheck:
                        return (isKGSSuperAdminOrHigher && isOrgAdmin) || (isOrgAdmin && isProxyClient && isLoggedInUnderProviderClient);
                    case AdminNavBooleanChecks.KGSAdminNavCheck:
                        return isKGSSuperAdminOrHigher;
                    case AdminNavBooleanChecks.KGSAdminNavOrgCheck:
                        return isKGSSuperAdminOrHigher && isOrgAdmin;
                    case AdminNavBooleanChecks.KGSSystemAdminCheck:
                        return isKGSSystemAdmin;
                    default:
                        return false;
                }   
            }

        #endregion
    }
}