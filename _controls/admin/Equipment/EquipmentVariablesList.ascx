﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentVariablesList.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.EquipmentVariablesList" %>
<%@ Import Namespace="CW.Data.Models.EquipmentVariable" %>
<%@ Import Namespace="CW.Utility" %>

<hr />

<h2>Variables List</h2>

<div>
  <asp:Label ID="lblEquipmentVarsEmpty" Visible="false" runat="server" />
</div>

<asp:Repeater ID="rptEquipmentVars" Visible="false" runat="server">
  <HeaderTemplate>
    <ul class="detailsListProperties">
  </HeaderTemplate>
  <ItemTemplate>
    <li>
      <strong><%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.EquipmentVariableDisplayName)) %></strong>

      <dl>
        <%# (!String.IsNullOrWhiteSpace(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.Value)).ToString())) ? "<dt>Value:</dt> <dd>" + Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.Value)) + "</dd>" : String.Empty %>
        <%# (!String.IsNullOrWhiteSpace(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IPDefaultValue)).ToString())) ? "<dt>IP Default Value:</dt> <dd>" + Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IPDefaultValue)) + "</dd>" : String.Empty %>
        <%# (!String.IsNullOrWhiteSpace(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.SIDefaultValue)).ToString())) ? "<dt>SI Default Value:</dt> <dd>" + Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.SIDefaultValue)) + "</dd>" : String.Empty %>
        <%# (!String.IsNullOrWhiteSpace(Convert.ToString(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.EquipmentVariableDescription))))) ? "<dt>Description:</dt> <dd>" + Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.EquipmentVariableDescription)) + "</dd>" : String.Empty %>
      </dl>

    </li>
  </ItemTemplate>
  <FooterTemplate>
    </ul>
  </FooterTemplate>
</asp:Repeater>