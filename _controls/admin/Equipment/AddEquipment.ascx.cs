﻿using CW.Business.Blob.Images;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using CW.Website._controls.admin.ScheduledAnalyses;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class AddEquipment : AdminUserControlBase
    {
        #region constants

            const String equipmentExists = "Equipment with that name already exists.";
            const String addEquipmentSuccess = " equipment addition was successful.";
            const String addEquipmentFailed = "Adding equipment failed. Please contact an administrator.";
            const String addEquipmentError = "Error adding equipment.";
            const String autoAssignAndSchedulingFailed = "Equipment was added successfully but auto assign and scheduling failed. Please contact an administrator.";
            const String autoAssignAndSchedulingError = "Equipment was added successfully but an error occured auto assigning and scheduling analyses for the equipment.";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAddError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindBuildings(ddlAddBuilding);
                    BindEquipmentClasses(ddlAddEquipmentClass);
                    BindEquipmentTypes(ddlAddEquipmentType, -1);
                    BindManufacturers(ddlAddManufacturer, -1);
                    BindManufacturerModels(ddlAddManufacturerModel, -1);
                    BindManufacturerContacts(ddlAddPrimaryContact, -1);
                    BindManufacturerContacts(ddlAddSecondaryContact, -1);
                }           
            }

            #region ddl events

                    protected void ddlAddEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                    {
                        BindEquipmentTypes(ddlAddEquipmentType, Convert.ToInt32(ddlAddEquipmentClass.SelectedValue));
                        BindManufacturers(ddlAddManufacturer, Convert.ToInt32(ddlAddEquipmentClass.SelectedValue));
                    }

                    protected void ddlAddManufacturer_OnSelectedIndexChanged(Object sender, EventArgs e)
                    {
                        BindManufacturerModels(ddlAddManufacturerModel, Convert.ToInt32(ddlAddManufacturer.SelectedValue));
                        BindManufacturerContacts(ddlAddPrimaryContact, Convert.ToInt32(ddlAddManufacturer.SelectedValue));
                        BindManufacturerContacts(ddlAddSecondaryContact, Convert.ToInt32(ddlAddManufacturer.SelectedValue));
                    }

            #endregion

            #region button events

                protected void addEquipmentButton_Click(Object sender, EventArgs e)
                {
                    if (DataMgr.EquipmentDataMapper.DoesEquipmentNameExistForBuilding(txtAddEquipmentName.Text, Convert.ToInt32(ddlAddBuilding.SelectedValue)))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, equipmentExists, lnkSetFocus);
                        return;
                    }
                    else
                    {
                        var mEquipment = new CW.Data.Equipment();

                        LoadAddFormIntoEquipment(mEquipment);

                        try
                        {
                            DataMgr.EquipmentDataMapper.InsertEquipment(mEquipment);

                            if (fileUploaderAdd.File != null) new EquipmentProfileImage(siteUser.CID, DataMgr.OrgBasedBlobStorageProvider, fileUploaderAdd.File.Extension, new EquipmentProfileImage.Args(mEquipment.EID)).Insert(fileUploaderAdd.File.Content);

                            //auto assign and auto schedule analyses
                            try
                            {
                                var assignedAnalyses = DataMgr.AnalysisEquipmentTypeDataMapper.GetAssignedAnalysesByEquipmentTypeIDAndCID(mEquipment.EquipmentTypeID, siteUser.CID);

                                List<ScheduledAnalyse> scheduledAnalyses = new List<ScheduledAnalyse>();
                                List<Analyses_Equipment> analysesEquipment = new List<Analyses_Equipment>();

                                foreach (var a_et in assignedAnalyses)
                                {
                                    if (a_et.AutoAssignAndScheduleDaily || a_et.AutoAssignAndScheduleWeekly || a_et.AutoAssignAndScheduleMonthly || a_et.AutoAssignAndScheduleHalfDay)
                                    {
                                        Analyses_Equipment ae = new Analyses_Equipment();
                                        ae.AID = a_et.AID;
                                        ae.EID = mEquipment.EID;
                                        analysesEquipment.Add(ae);

                                        ScheduledAnalyse sa = new ScheduledAnalyse();
                                        sa.AID = a_et.AID;
                                        sa.EID = mEquipment.EID;
                                        sa.BID = mEquipment.BID;
                                        sa.RunStartDate = DateTime.UtcNow;
                                        sa.RunHalfDay = a_et.AutoAssignAndScheduleHalfDay;
                                        sa.RunDaily = a_et.AutoAssignAndScheduleDaily;
                                        sa.RunWeekly = a_et.AutoAssignAndScheduleWeekly;
                                        sa.RunMonthly = a_et.AutoAssignAndScheduleMonthly;
                                        sa.DateModified = DateTime.UtcNow;
                                        scheduledAnalyses.Add(sa);


                                        //insert all vpoints
                                        var aovpts = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPointTypesByAID(a_et.AID);

                                        foreach (var apvpt in aovpts)
                                        {
                                            var vp = new VPoint();

                                            vp.AID = a_et.AID;
                                            vp.EID = mEquipment.EID;
                                            vp.PointTypeID = apvpt.PointTypeID;

                                            DataMgr.PointDataMapper.InsertVPoint(vp);
                                        }

                                        //insert all vpre points
                                        var aovppts = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPrePointTypesByAID(a_et.AID);

                                        foreach (var apvppt in aovppts)
                                        {
                                            var vpp = new VPrePoint();

                                            vpp.AID = a_et.AID;
                                            vpp.EID = mEquipment.EID;
                                            vpp.PointTypeID = apvppt.PointTypeID;

                                            DataMgr.PointDataMapper.InsertVPrePoint(vpp);
                                        }
                                    }
                                }

                                if (analysesEquipment.Any()) DataMgr.AnalysisEquipmentDataMapper.InsertAnalysisEquipment(analysesEquipment);
                                if (scheduledAnalyses.Any()) DataMgr.ScheduledAnalysisDataMapper.InsertScheduledAnalyses(scheduledAnalyses);
                        
                                LabelHelper.SetLabelMessage(lblResults, mEquipment.EquipmentName + addEquipmentSuccess, lnkSetFocus);
                                SetChangeStateForTabs(KGSEquipmentAdministration.TabMessages.AddEquipment);

                                new ClearCacheScheduledAnalyses().ClearCache(siteUser.CID, new int[] { mEquipment.BID }, siteUser.UID);
                            }
                            catch (SqlException sqlEx)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, autoAssignAndSchedulingError, sqlEx);
                                LabelHelper.SetLabelMessage(lblAddError, autoAssignAndSchedulingFailed, lnkSetFocus);
                            }
                            catch (Exception ex)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, autoAssignAndSchedulingError, ex);
                                LabelHelper.SetLabelMessage(lblAddError, autoAssignAndSchedulingFailed, lnkSetFocus);
                            }
                        }
                        catch (SqlException sqlEx)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, addEquipmentError, sqlEx);
                            LabelHelper.SetLabelMessage(lblAddError, addEquipmentFailed, lnkSetFocus);
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, addEquipmentError, ex);
                            LabelHelper.SetLabelMessage(lblAddError, addEquipmentFailed, lnkSetFocus);
                        }
                    }
                }

            #endregion

        #endregion

        #region methods

            private void LoadAddFormIntoEquipment(CW.Data.Equipment equipment)
            {
                equipment.EquipmentName = txtAddEquipmentName.Text;         
                equipment.BID = Convert.ToInt32(ddlAddBuilding.SelectedValue);
                equipment.ManufacturerModelID = Convert.ToInt32(ddlAddManufacturerModel.SelectedValue);
                equipment.SerialNumber = String.IsNullOrEmpty(txtAddSerialNumber.Text) ? null : txtAddSerialNumber.Text;
                equipment.EquipmentLocation = String.IsNullOrEmpty(txtAddEquipmentLocation.Value) ? null : txtAddEquipmentLocation.Value;
                equipment.Description = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                equipment.EquipmentTypeID = Convert.ToInt32(ddlAddEquipmentType.SelectedValue);
                equipment.ImageExtension = fileUploaderAdd.File == null ? String.Empty : fileUploaderAdd.File.Extension;
                equipment.PrimaryContactID = ddlAddPrimaryContact.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddPrimaryContact.SelectedValue) : null;
                equipment.SecondaryContactID = ddlAddSecondaryContact.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddSecondaryContact.SelectedValue) : null;
                equipment.EquipmentNotes = StringHelper.ClearRadEditorFirefoxBR(editorAddEquipmentNotes.Content);
                equipment.IsActive = true;
                equipment.IsVisible = true;
                equipment.DateModified = DateTime.UtcNow;

                //Additional Information

                equipment.ExternalEquipmentTypeName = String.IsNullOrEmpty(txtAddExternalEquipmentTypeName.Text) ? null : txtAddExternalEquipmentTypeName.Text;
                equipment.CMMSReferenceID = String.IsNullOrEmpty(txtAddCMMSReferenceID.Text) ? null : txtAddCMMSReferenceID.Text;
                equipment.CMMSLocationID = String.IsNullOrEmpty(txtAddCMMSLocationID.Text) ? null : txtAddCMMSLocationID.Text;
                equipment.CMMSSiteID = String.IsNullOrEmpty(txtAddCMMSSiteID.Text) ? null : txtAddCMMSSiteID.Text;
                equipment.CMMSLink = String.IsNullOrEmpty(txtAddCMMSLink.Text) ? null : txtAddCMMSLink.Text;
                equipment.BIMReferenceID = String.IsNullOrEmpty(txtAddBIMReferenceID.Text) ? null : txtAddBIMReferenceID.Text;
                equipment.BIMLink = String.IsNullOrEmpty(txtAddBIMLink.Text) ? null : txtAddBIMLink.Text;
                equipment.DMSReferenceID = String.IsNullOrEmpty(txtAddDMSReferenceID.Text) ? null : txtAddDMSReferenceID.Text;
                equipment.DMSLink = String.IsNullOrEmpty(txtAddDMSLink.Text) ? null : txtAddDMSLink.Text;
                equipment.GISReferenceID = String.IsNullOrEmpty(txtAddGISReferenceID.Text) ? null : txtAddGISReferenceID.Text;
                equipment.GISLink = String.IsNullOrEmpty(txtAddGISLink.Text) ? null : txtAddGISLink.Text;
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    ddl.DataTextField = "BuildingName";
                    ddl.DataValueField = "BID";
                    ddl.DataSource = siteUser.VisibleBuildings;
                    ddl.DataBind();
                }

                private void BindEquipmentClasses(DropDownList ddl)
                {
                    var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };
                    var classes = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    ddl.DataTextField = "EquipmentClassName";
                    ddl.DataValueField = "EquipmentClassID";
                    ddl.DataSource = classes;
                    ddl.DataBind();
                }

                private void BindEquipmentTypes(DropDownList ddl, Int32 equipmentClassID)
                {
                    var lItem = new ListItem() { Text = (equipmentClassID != -1) ? "Select one..." : "Select class first...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    if (equipmentClassID != -1)
                    {
                        ddl.DataTextField = "EquipmentTypeName";
                        ddl.DataValueField = "EquipmentTypeID";
                        ddl.DataSource = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByEquipmentClassID(equipmentClassID);
                        ddl.DataBind();
                    }
                }

                private void BindManufacturers(DropDownList ddl, Int32 equipmentClassID)
                {
                    var lItem = new ListItem() { Text = (equipmentClassID != -1) ? "Select one..." : "Select class first...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    if (equipmentClassID != -1)
                    {
                        ddl.DataTextField = "ManufacturerName";
                        ddl.DataValueField = "ManufacturerID";
                        ddl.DataSource = DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturersAssociatedToEquipmentClassID(equipmentClassID);
                        ddl.DataBind();
                    }
                }

                private void BindManufacturerModels(DropDownList ddl, Int32 manufacturerID)
                {
                    var lItem = new ListItem() { Text = (manufacturerID != -1) ? "Select one..." : "Select maunfacturer first...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    if (manufacturerID != -1)
                    {
                        ddl.DataTextField = "ManufacturerModelName";
                        ddl.DataValueField = "ManufacturerModelID";
                        ddl.DataSource = DataMgr.EquipmentManufacturerModelDataMapper.GetAllManufacturerModelsByManufacturerID(manufacturerID);
                        ddl.DataBind();
                    }
                }

                private void BindManufacturerContacts(DropDownList ddl, Int32 manufacturerID)
                {
                    var lItem = new ListItem() { Text = (manufacturerID != -1) ? "Select one..." : "Select maunfacturer first...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    if (manufacturerID != -1)
                    {
                        ddl.DataTextField = "Name";
                        ddl.DataValueField = "ContactID";
                        ddl.DataSource = DataMgr.EquipmentManufacturerContactDataMapper.GetAllManufacturerContactsByManufacturerID(manufacturerID);
                        ddl.DataBind();
                    }
                }

        #endregion

        #endregion

        public class ClearCacheScheduledAnalyses : IClearCache
        {
            public void ClearCache(int cid, IEnumerable<int> bids, Int32 uid) //uid not used here, just passed in as null
            {
                if (bids != null)
                {
                    foreach (var bid in bids)
                        GridCacher.Clear(AdminUserControlGrid.CacheStore, ViewScheduledAnalyses.CreateCacheKey(cid, bid));
                }
            }
        }
    }
}