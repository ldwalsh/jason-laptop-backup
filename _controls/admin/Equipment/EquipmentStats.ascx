﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentStats.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.EquipmentStats" %>
<%@ Import Namespace="CW.Data.Models.Equipment" %>
<%@ Import Namespace="CW.Utility" %>

<h2>Equipment Stats</h2>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
</p>

<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div> 

<div class="divForm">
  <label class="label">*Building:</label>
  <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentName" HeaderText="Equipment Name">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.EquipmentName)) %>"><%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.EquipmentName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentTypeName" HeaderText="Equipment Type Name">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.EquipmentTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.EquipmentTypeName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="AssociatedEquipmentCount" HeaderText="# of Associated Equipment">
        <ItemTemplate><asp:HyperLink ID="hlEquipmentToEquipment" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.AssociatedEquipmentCount)) %>' /></ItemTemplate>
      </asp:TemplateField>
      
      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="EquipmentVariablesCount" HeaderText="# of Equipment Variables">
        <ItemTemplate><asp:HyperLink ID="hlEquipmentVariables" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.EquipmentVariablesCount)) %>' /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="PointCount" HeaderText="# of Points">
        <ItemTemplate><asp:HyperLink ID="hlPoints" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.PointCount)) %>' /></ItemTemplate>
      </asp:TemplateField>

       <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="AssignedAnalysesCount" HeaderText="# of Assigned Analyses">
        <ItemTemplate><asp:HyperLink ID="hlAnalysesToEquipment" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.AssignedAnalysesCount)) %>' /></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="ScheduledAnalysesCount" HeaderText="# of Sch. Analyses">
        <ItemTemplate><asp:HyperLink ID="hlScheduledAnalyses" runat="server" Text='<%# GetCellContent(Container, PropHelper.G<EquipmentAdministrationStatistics>(_ => _.ScheduledAnalysesCount)) %>' /></ItemTemplate>
      </asp:TemplateField>                                                                                                                                             
    </Columns>
  </asp:GridView>
</div> 