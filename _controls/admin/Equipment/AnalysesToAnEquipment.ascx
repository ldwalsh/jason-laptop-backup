﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AnalysesToAnEquipment.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.AnalysesToAnEquipment" %>

<asp:Panel ID="pnlUpdateAnalysesToAnEquipment" runat="server">

  <h2>Analyses To An Equipment</h2>

  <p>Please select building and then a piece of equipment to select and associate analyses.</p>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAnalysesError" CssClass="errorMessage" runat="server" />
  </div>
  <div class="divForm">   
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>
  </div> 
  <div class="divForm">
    <label class="label">*Select Building:</label>    
    <asp:DropDownList ID="ddlAnalysesBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalysesBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
  </div>
  <div class="divForm">   
    <label class="label">*Select Equipment:</label>    
    <asp:DropDownList ID="ddlAnalysesEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalysesEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
  </div>                                                                                       
  <div id="equipmentAnalyses" visible="false" runat="server">
    <hr />

    <div class="richText">
      <p>Analyses are available based on several criteria:</p>

      <ul>
        <li>Analyses are not custom, or are custom and associated to client.</li>
        <li>Equipment variables that are required are associated with both the equipment and an analysis.</li>
        <li>Point types that are associated with both the equipments points and the required analysis input point types.</li>
      </ul>

      <p>(Note: You cannot unassign an analysis to a piece of equipment if it has already been scheduled.)</p>
      <p>(Note: You cannot unassign an analysis to a piece of equipment if it has already been analyzed and vdata or vpredata exists. A system admin must purge all analyzed data for the analysis and equipment first.)</p>
      <p>(Warning: Please do not assign and schedule an analysis untill is has been completely setup. Assigning, and then scheduling an analysis without the proper variables and point types setup will cause the automation to malfunction.)</p>
      <p>(Warning: Assigning an analysis will generate output associations for all vpoint and vprepoint types associated with the analysis for the selected equipment.)</p>
    </div>
    <div class="divForm">
      <label class="label">Available:</label>    
      <asp:ListBox ID="lbAnalysesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

      <div class="divArrows">
        <asp:ImageButton CssClass="up-arrow"  ID="btnAnalysesUp" CauseValidation="false" OnClick="btnAnalysesUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
        <asp:ImageButton CssClass="down-arrow" ID="btnAnalysesDown" CauseValidation="false" OnClick="btnAnalysesDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
      </div>

      <label class="label">Assigned:</label>
      <asp:ListBox ID="lbAnalysesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateAnalysesToAnEquipment" runat="server" Text="Reassign"  OnClick="updateAnalysesToAnEquipmentButton_Click" ValidationGroup="UpdateAnalysesToAnEquipment" />

  </div>

</asp:Panel>