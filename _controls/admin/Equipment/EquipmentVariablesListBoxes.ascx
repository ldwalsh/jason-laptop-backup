﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentVariablesListBoxes.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.EquipmentVariablesListBoxes" %>

<asp:Panel ID="pnlEquipmentVariablesListBoxes" runat="server" DefaultButton="btnReassignEquipment">
    <hr />
    <p>Equipment variables are available based on the assigned variables to the equipment class.</p>
    <p>(Note: Most often assign variables electricitycost, fuelcost, and pricepoint.)</p>

    <div class="divForm">
      <label class="label">Available:</label>
      <asp:ListBox ID="lbEquipmentVariablesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

      <div class="divArrows">
        <asp:ImageButton ID="btnUp" CausesValidation="false" OnClick="btnUp_ButtonClick" ImageUrl="~/_assets/images/up-arrow.png" runat="server" CssClass="up-arrow" />
        <asp:ImageButton ID="btnDown" CausesValidation="false" OnClick="btnDown_ButtonClick" ImageUrl="~/_assets/images/down-arrow.png" runat="server" CssClass="down-arrow" />
      </div>

      <label class="label">Assigned:</label>
      <asp:ListBox ID="lbEquipmentVariablesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>

    <asp:LinkButton ID="btnReassignEquipment" runat="server" Text="Reassign" OnClick="btnReassignEquipment_ButtonClick" CssClass="lnkButton" ValidationGroup="reassignEquipmentVariables" />
</asp:Panel>