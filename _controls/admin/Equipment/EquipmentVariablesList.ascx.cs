﻿using CW.Data.Models.EquipmentVariable;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._controls.admin.Equipment
{
    public partial class EquipmentVariablesList : SiteUserControl
    {
        #region constants

            const String equipmentVarsEmpty = "No equipment variables have been assigned.";

        #endregion

        #region properties

            public Int32 EID { get; set; }

        #endregion

        #region events

            private void Page_Load()
            {
                rptEquipmentVars.Visible = lblEquipmentVarsEmpty.Visible = false;
            }

        #endregion

        #region methods

            public void BindEquipmentVariables(IEnumerable<GetEquipmentEquipmentVariableData> equipmentVariables = null)
            {
                if (equipmentVariables == null) equipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesWithDataByEID(EID).Where(ev => ev.UserEditable);

                if (equipmentVariables.Any())
                {
                    rptEquipmentVars.Visible = true;
                    rptEquipmentVars.DataSource = equipmentVariables;
                    rptEquipmentVars.DataBind();
                    return;
                }

                lblEquipmentVarsEmpty.Visible = true;
                lblEquipmentVarsEmpty.Text = equipmentVarsEmpty;
            }

        #endregion
    }
}