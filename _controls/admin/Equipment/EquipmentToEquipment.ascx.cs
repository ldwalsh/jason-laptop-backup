﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class EquipmentToEquipment : AdminUserControlBase
    {
        #region constants

            const String unassignEquipmentUpdateSuccessful = "Unassignment of Equipment {0} to equipment {1} was successful.";
            const String assignEquipmentUpdateSuccessful = "Assignment of Equipment {0} to equipment {1} was successful.";
            const String reassignUpdateFailed = "Equipment reassignment failed. Please contact an administrator.";
            const String reassignUpdateExists = "Equipment reassignment failed. The current data set is out of date, please refresh the page.";
            const String noItemsSelected = "No items were selected for reassignment.";
            const String noEquipmentIsAssignable = "No equipment can be assigned to {0}.";
            const Char delimeter = Common.Constants.DataConstants.DefaultDelimiter;

        #endregion

        #region properites

            public override IEnumerable<Enum> ReactToChangeStateList
            {
                get
                {
                    return new Enum[]
                    {
                        KGSEquipmentAdministration.TabMessages.AddEquipment,
                        KGSEquipmentAdministration.TabMessages.EditEquipment,
                        KGSEquipmentAdministration.TabMessages.DeleteEquipment
                    };
                }
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindBuildings(ddlBuilding);

                var adminDropDowns = new AdminDropDowns(RadTabStrip, RadMultiPage, lblResults);
                var currentControl = typeof(EquipmentToEquipment).Name;

                adminDropDowns.SetBuildingDropDownListAndTabIndex(ddlBuilding, currentControl);
                ddlBuilding_OnSelectedIndexChanged(null, null);
                ddlEquipmentClass_OnSelectedIndexChanged(null, null);

                adminDropDowns.SetEquipmentDropDownList(ddlEquipment, currentControl);
                ddlEquipment_OnSelectedIndexChanged(null, null);
            }

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = lblEquipmentError.Visible = false;
                EquipToEquipDownloadArea.DropDownList = ddlBuilding;
            }

            #region ddl events

                protected void ddlBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    var bid = Convert.ToInt32(ddlBuilding.SelectedValue);

                    ControlHelper.ToggleVisibility((bid != -1), new Control[] { EquipToEquipDownloadArea });
                    EquipToEquipDownloadArea.FindControl("lblError").Visible = false;

                    equipment.Visible = false;
                    BindEquipmentClasses(ddlEquipmentClass, bid);
                    BindEquipmentTypes(ddlEquipmentType, bid, - 1);
                    BindEquipment(ddlEquipment, new Int32[] { bid, 0, 0 } );
                }

                protected void ddlEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    var bid = Convert.ToInt32(ddlBuilding.SelectedValue); 
                    var ecid = Convert.ToInt32(ddlEquipmentClass.SelectedValue);

                    equipment.Visible = false;
                    BindEquipmentTypes(ddlEquipmentType, bid, ecid);
                    BindEquipment(ddlEquipment, new Int32[] { bid, ecid, 0 });
                }

                protected void ddlEquipmentType_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    equipment.Visible = false;
                    BindEquipment(ddlEquipment, new Int32[] { Convert.ToInt32(ddlBuilding.SelectedValue), Convert.ToInt32(ddlEquipmentClass.SelectedValue), Convert.ToInt32(ddlEquipmentType.SelectedValue) });
                }

                protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    equipment.Visible = false;

                    var bid = (ddlBuilding.SelectedValue != String.Empty) ? Convert.ToInt32(ddlBuilding.SelectedValue) : -1;
                    var eid = (ddlEquipment.SelectedValue != String.Empty) ? Convert.ToInt32(ddlEquipment.SelectedValue) : -1;
                    
                    if (eid != -1) BindEquipmentListBoxes(eid, bid);
                }

            #endregion

            #region button events

                protected void btnEquipmentUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbEquipmentBottom.SelectedIndex != -1)
                    {
                        lbEquipmentTop.Items.Add(lbEquipmentBottom.SelectedItem);
                        lbEquipmentBottom.Items.Remove(lbEquipmentBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbEquipmentTop);
                }

                protected void btnEquipmentDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbEquipmentTop.SelectedIndex != -1)
                    {
                        lbEquipmentBottom.Items.Add(lbEquipmentTop.SelectedItem);
                        lbEquipmentTop.Items.Remove(lbEquipmentTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbEquipmentBottom);
                }

                protected void updateEquipmentButton_Click(Object sender, EventArgs e)
                {
                    var peid = Convert.ToInt32(ddlEquipment.SelectedValue);
                    var ecid = Convert.ToInt32(ddlEquipmentClass.SelectedValue);
                    var etid = Convert.ToInt32(ddlEquipmentType.SelectedValue);
                    var bid = Convert.ToInt32(ddlBuilding.SelectedValue);
                    var equipmentName = ddlEquipment.SelectedItem.Text;
                    var equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(Convert.ToInt32(bid));
                    var selector = new Func<ListItem, Equipment_Equipment>(li => new Equipment_Equipment { PrimaryEID = peid, AssignedEID = Convert.ToInt32(li.Value.Split(delimeter)[0]) });

                    lblEquipmentError.Text = String.Empty;
                    lblResults.Text = String.Empty;

                    try
                    {
                        var unassignedEquipment = lbEquipmentTop.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, true)).Select(selector).ToList();

                        DataMgr.EquipmentDataMapper.DeleteEquipmentToEquipment(unassignedEquipment);
                        SetMessages(unassignedEquipment, equipmentName, equipment, unassignEquipmentUpdateSuccessful);

                        var assignedEquipment = lbEquipmentBottom.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, false)).Select(selector).ToList();

                        DataMgr.EquipmentDataMapper.InsertEquipmentToEquipment(assignedEquipment);
                        SetMessages(assignedEquipment, equipmentName, equipment, assignEquipmentUpdateSuccessful);

                        if (lblEquipmentError.Text == String.Empty && lblResults.Text == String.Empty)
                        {
                            LabelHelper.SetLabelMessage(lblEquipmentError, noItemsSelected, lnkSetFocus);
                            return;
                        }

                        BindEquipmentListBoxes(peid, bid);

                        if (lblEquipmentError.Text != String.Empty) LabelHelper.SetLabelMessage(lblEquipmentError, lblEquipmentError.Text, lnkSetFocus);
                        if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                    }
                    catch (SqlException sqlEx) when (sqlEx.Number == 2627)
                    {
                        LabelHelper.SetLabelMessage(lblEquipmentError, reassignUpdateExists, lnkSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblEquipmentError, reassignUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment types to equipment type.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEquipmentError, reassignUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment types to equipment type.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();

                    ddlBuilding_OnSelectedIndexChanged(null, null);
                }

            #endregion

            private void SetMessages(List<Equipment_Equipment> processedItems, String equipmentName, IEnumerable<CW.Data.Equipment> items, String message)
            {
                foreach (var item in processedItems)
                {
                    var updatedEquipmentName = items.Where(e => e.EID == item.AssignedEID).Single().EquipmentName;

                    LabelHelper.SetLabelMessage(lblResults, String.Format(message, updatedEquipmentName, equipmentName) + "<br />", lnkSetFocus, true);
                }
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindEquipment(DropDownList ddl, Int32[] ids)
                {
                    var lItem = new ListItem() { Text = (ids[0] != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    IEnumerable<CW.Data.Equipment> data = null;

                    if (ids[0] != -1)
                    {
                        if (ids[2] > 0)
                            data = DataMgr.EquipmentDataMapper.GetAllEquipmentByBIDAndEquipmentTypeID(ids[0], ids[2]);
                        else if (ids[1] > 0)
                            data = DataMgr.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(ids[1], ids[0]);
                        else
                            data = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(ids[0]);
                    }

                    BindDDL(ddl, "EquipmentName", "EID", data, true, lItem);
                }

                private void BindEquipmentClasses(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "View All..." : "Select building first...", Value = (bid != -1) ? "0" : "-1", Selected = true };

                    BindDDL(ddl, "EquipmentClassName", "EquipmentClassID", (bid != -1) ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid) : null, true, lItem);
                }

                private void BindEquipmentTypes(DropDownList ddl, Int32 bid, Int32 ecid)
                {
                    var lItem = new ListItem() { Text = (ecid != -1) ? "View All..." : "Select equipment class first...", Value = (ecid != -1) ? "0" : "-1", Selected = true };

                    BindDDL(ddl, "EquipmentTypeName", "EquipmentTypeID", (ecid > 0) ? DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByBuildingIDAndEquipmentClassID(bid, ecid) : null, true, lItem);
                }

                private void BindEquipmentListBoxes(Int32 peid, Int32 bid)
                {
                    lbEquipmentTop.Items.Clear();
                    lbEquipmentBottom.Items.Clear();

                    var equipmentNotAssociated = DataMgr.EquipmentDataMapper.GetAllEquipmentNotAssociatedByPEID(peid, bid);
                    var equipmentAssociated = DataMgr.EquipmentDataMapper.GetAllEquipmentAssociatedByPEID(peid);
                    var equipmentName = PropHelper.G<CW.Data.Equipment>(e => e.EquipmentName);
                    var eid = PropHelper.G<CW.Data.Equipment>(e => e.EID);

                    if (equipmentNotAssociated.Any()) ControlHelper.SetListBoxData(lbEquipmentTop, equipmentNotAssociated, equipmentName, eid, false);
                    if (equipmentAssociated.Any()) ControlHelper.SetListBoxData(lbEquipmentBottom, equipmentAssociated, equipmentName, eid, true);

                    equipment.Visible = (equipmentNotAssociated.Any() || equipmentAssociated.Any());

                    if (!equipmentNotAssociated.Any() && !equipmentAssociated.Any())
                    {
                        var noEquipmentName = DataMgr.EquipmentDataMapper.GetEquipmentByEID(peid, false, false, false, false, false, false).EquipmentName;

                        lblResults.Text = String.Format(noEquipmentIsAssignable, noEquipmentName);
                        lblResults.Visible = true;
                    }
                }

            #endregion

        #endregion
    }
}