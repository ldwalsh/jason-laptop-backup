﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class AnalysesToAnEquipment : AdminUserControlBase
    {
        #region constants

            const String unassignAnalysesUpdateSuccessful = "Unassignment of analyses {0} to equipment {1} was successful.";
            const String assignAnalysesUpdateSuccessful = "Assignment of analyses {0} to equipment {1} was successful.";
            const String reassignAnalysesUpdateFailed = "Analyses reassignment failed. Please contact an administrator.";
            const String scheduledAnalysisRequiresAnalysis = " analyses could not be unassigned because they have already been scheduled.";
            const String vDataAndVPreDataRequiresAnalysis = " analyses could not be unassigned because vdata and/or vpredata exists for them.";
            const String projectsRequiresAnalysis = " analyses could not be unassigned because projects exists for them.";
            const String tasksRequiresAnalysis = " analyses could not be unassigned becuase tasks exists for them.";
            const String noItemsSelected = "No items were selected for reassignment.";
            const String noAnalysisIsAssignable = "No analysis can be assigned to {0}.";

        #endregion

        #region properites

            public override IEnumerable<Enum> ReactToChangeStateList
            {
                get
                {
                    return new Enum[]
                    {
                        KGSEquipmentAdministration.TabMessages.AddEquipment,
                        KGSEquipmentAdministration.TabMessages.EditEquipment,
                        KGSEquipmentAdministration.TabMessages.DeleteEquipment
                    };
                }
            }

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAnalysesError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindBuildings(ddlAnalysesBuildings);

                    var adminDropDowns = new AdminDropDowns(RadTabStrip, RadMultiPage, lblResults);
                    var currentControl = typeof(AnalysesToAnEquipment).Name;

                    adminDropDowns.SetBuildingDropDownListAndTabIndex(ddlAnalysesBuildings, currentControl);
                    ddlAnalysesBuildings_OnSelectedIndexChanged(null, null);

                    adminDropDowns.SetEquipmentDropDownList(ddlAnalysesEquipment, currentControl);
                    ddlAnalysesEquipment_OnSelectedIndexChanged(null, null);
                }
            }

            #region ddl events

                protected void ddlAnalysesBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    equipmentAnalyses.Visible = false;

                    BindEquipment(ddlAnalysesEquipment, Convert.ToInt32(ddlAnalysesBuildings.SelectedValue));
                }

                protected void ddlAnalysesEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    equipmentAnalyses.Visible = false;

                    var eid = Convert.ToInt32(ddlAnalysesEquipment.SelectedValue);

                    if (eid != -1) BindEquipmentAnalyses(eid);
                }

            #endregion

            #region button events

                protected void btnAnalysesUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbAnalysesBottom.SelectedIndex != -1)
                    {
                        lbAnalysesTop.Items.Add(lbAnalysesBottom.SelectedItem);
                        lbAnalysesBottom.Items.Remove(lbAnalysesBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbAnalysesTop);
                }

                protected void btnAnalysesDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbAnalysesTop.SelectedIndex != -1)
                    {
                        lbAnalysesBottom.Items.Add(lbAnalysesTop.SelectedItem);
                        lbAnalysesTop.Items.Remove(lbAnalysesTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbAnalysesBottom);
                }

                protected void updateAnalysesToAnEquipmentButton_Click(Object sender, EventArgs e)
                {
                    var eid = Convert.ToInt32(ddlAnalysesEquipment.SelectedValue);
                    var equipmentName = ddlAnalysesEquipment.SelectedItem.Text;
                    var analyses = DataMgr.AnalysisDataMapper.GetAllActiveAnalyses();
                    var selector = new Func<ListItem, Analyses_Equipment>(li => new Analyses_Equipment { EID = eid, AID = Convert.ToInt32(li.Value.Split(DataConstants.DefaultDelimiter)[0]) });

                    try
                    {
                        lblAnalysesError.Text = String.Empty;
                        lblResults.Text = String.Empty;

                        UnassignAnalysesForEquipment(eid, selector, analyses, equipmentName);
                        AssignAnalysesForEquipment(eid, selector, analyses, equipmentName);
                       
                        if (lblAnalysesError.Text == String.Empty && lblResults.Text == String.Empty)
                        {
                            LabelHelper.SetLabelMessage(lblAnalysesError, noItemsSelected, lnkSetFocus);
                            return;
                        }

                        BindEquipmentAnalyses(eid);

                        if (lblAnalysesError.Text != String.Empty) LabelHelper.SetLabelMessage(lblAnalysesError, lblAnalysesError.Text, lnkSetFocus);
                        if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblAnalysesError, reassignAnalysesUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning analyses to equipment.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblAnalysesError, reassignAnalysesUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning analyses to equipment.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();

                    //hardset any fields to their initial values
                    ddlAnalysesBuildings_OnSelectedIndexChanged(null, null);
                    ddlAnalysesEquipment_OnSelectedIndexChanged(null, null);
                }

            #endregion

            private void UnassignAnalysesForEquipment(Int32 eid, Func<ListItem, Analyses_Equipment> selector, IEnumerable<Analyse> analyses, String equipmentName)
            {
                var unassignedAnalyses = lbAnalysesTop.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, true)).Select(selector).ToList();

                foreach (var item in unassignedAnalyses)
                {
                    var aid = Convert.ToInt32(item.AID);
                    var analysisName = analyses.Where(a => a.AID == aid).Select(a => a.AnalysisName).First();

                    //if the analysis to equipment exists, delete
                    if (DataMgr.AnalysisEquipmentDataMapper.IsAnalysisAssignedToEquipment(eid, aid))
                    {
                        //check if their is a scheduled analysis for this equipment and analysis.
                        if (!DataMgr.ScheduledAnalysisDataMapper.DoesScheduledAnalysisExist(eid, aid))
                        {
                            //check if project is associated with this equipment and analysis.
                            if (!DataMgr.ProjectDataMapper.DoesProjectAssoicationExistForEIDAndAID(eid, aid))
                            {
                                 //check if task is associated with this equipment and analysis.
                                if (!DataMgr.TasksDataMapper.DoesTasksExistForEIDAndAID(eid, aid))
                                {
                                    bool isUtilityBilling = DataMgr.AnalysisDataMapper.GetAnalysisByAID(aid).First().IsUtilityBilling;

                                    //check if vdata exists for this analysis equipment
                                    //no need to check vpredata as vdata will always exists for a priority.
                                    if (isUtilityBilling || (!isUtilityBilling && !(DataMgr.VAndVPreDataMapper.DoesVDataExistForEIDAndAID(eid, aid))))
                                    {
                                        //check if vdatautilitybilling exists for this analysis equipment
                                        //no need to check vpredatautilitybilling as vdatautilitybilling will always exists for a priority.
                                        if (!isUtilityBilling || (isUtilityBilling && !(DataMgr.VAndVPreDataMapper.DoesVDataUtilityBillingExistForEIDAndAID(DataConstants.UtiliityAnalysisTypes.Utility, eid, aid))))
                                        {
                                            //meaning delete all vpoints for that equipment and analysis
                                            DataMgr.PointDataMapper.DeleteAllVPointsByAIDAndEID(aid, eid);

                                            //meaning delete all vprepoints for that equipment and analysis
                                            DataMgr.PointDataMapper.DeleteAllVPrePointsByAIDAndEID(aid, eid);
                                            DataMgr.AnalysisEquipmentDataMapper.DeleteAnalysisEquipmentByEIDAndAID(eid, aid);

                                            lblResults.Text += String.Format(unassignAnalysesUpdateSuccessful, analysisName, equipmentName) + "<br />";
                                        }
                                        else //DoesVDataUtilityBillingExistForEIDAndAID check else
                                            lblAnalysesError.Text += analysisName + vDataAndVPreDataRequiresAnalysis + "<br />";
                                    }
                                    else //DoesVDataExistForEIDAndAID check else
                                        lblAnalysesError.Text += analysisName + vDataAndVPreDataRequiresAnalysis + "<br />";
                                }
                                else //DoesTasksExistForEIDAndAID check else
                                    lblAnalysesError.Text += analysisName + tasksRequiresAnalysis + "<br />";
                            }
                            else //DoesProjectAssoicationExistForEIDAndAID check else
                                lblAnalysesError.Text += analysisName + projectsRequiresAnalysis + "<br />";
                        }
                        else //DoesScheduledAnalysisExist check else
                            lblAnalysesError.Text += analysisName + scheduledAnalysisRequiresAnalysis + "<br />";
                    }
                }
            }

            private void AssignAnalysesForEquipment(Int32 eid, Func<ListItem, Analyses_Equipment> selector, IEnumerable<Analyse> analyses, String equipmentName)
            {
                var assignedAnalyses = lbAnalysesBottom.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, false)).Select(selector).ToList();

                foreach (var item in assignedAnalyses)
                {
                    var mAnalysisEquipment = new Analyses_Equipment();
                    var aid = Convert.ToInt32(item.AID);
                    var analysisName = analyses.Where(a => a.AID == aid).Select(a => a.AnalysisName).First();

                    mAnalysisEquipment.AID = aid;
                    mAnalysisEquipment.EID = eid;

                    if (!DataMgr.AnalysisEquipmentDataMapper.IsAnalysisAssignedToEquipment(eid, aid))
                    {
                        DataMgr.AnalysisEquipmentDataMapper.InsertAnalysisEquipment(mAnalysisEquipment);

                        var aovpts = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPointTypesByAID(aid);

                        foreach (var apvpt in aovpts)
                        {
                            var vp = new VPoint();

                            vp.AID = aid;
                            vp.EID = eid;
                            vp.PointTypeID = apvpt.PointTypeID;

                            DataMgr.PointDataMapper.InsertVPoint(vp);
                        }

                        var aovppts = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPrePointTypesByAID(aid);

                        foreach (var apvppt in aovppts)
                        {
                            var vpp = new VPrePoint();

                            vpp.AID = aid;
                            vpp.EID = eid;
                            vpp.PointTypeID = apvppt.PointTypeID;

                            DataMgr.PointDataMapper.InsertVPrePoint(vpp);
                        }

                        lblResults.Text += String.Format(assignAnalysesUpdateSuccessful, analysisName, equipmentName) + "<br />";
                    }
                }
            }

            private void ClearListBoxes()
            {
                lbAnalysesTop.Items.Clear();
                lbAnalysesBottom.Items.Clear();
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    ddl.DataTextField = "BuildingName";
                    ddl.DataValueField = "BID";
                    ddl.DataSource = siteUser.VisibleBuildings;
                    ddl.DataBind();
                }

                private void BindEquipment(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    if (bid != -1)
                    {
                        ddl.DataTextField = "EquipmentName";
                        ddl.DataValueField = "EID";
                        ddl.DataSource = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid);
                        ddl.DataBind();
                    }
                }

                private void BindEquipmentAnalyses(Int32 eid)
                {
                    ClearListBoxes();

                    Boolean available = false, assigned = false;

                    var mAnalyses = DataMgr.AnalysisDataMapper.GetAllAvailableActiveAnalysesByCID(siteUser.CID);
                    var mAssignedEquipmentAnalyses = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByEID(eid);
                    var availableUnassignedAnalysesList = new List<Analyse>();
                    var availableAssignedAnalysesList = new List<Analyse>();
                    var analysisName = PropHelper.G<Analyse>(a => a.AnalysisName);
                    var aid = PropHelper.G<Analyse>(a => a.AID);

                    foreach (var analysis in mAnalyses)
                    {
                        available = DataMgr.AnalysisEquipmentDataMapper.IsAnalysisAvailableForEquipment(eid, analysis.AID);
                        assigned = mAssignedEquipmentAnalyses.Where(a => a.AID == analysis.AID).Any();

                        if (available && !assigned) availableUnassignedAnalysesList.Add(analysis);
                        if (available && assigned) availableAssignedAnalysesList.Add(analysis);
                    }

                    if (availableUnassignedAnalysesList.Any()) ControlHelper.SetListBoxData(lbAnalysesTop, availableUnassignedAnalysesList, analysisName, aid, false);
                    if (availableAssignedAnalysesList.Any()) ControlHelper.SetListBoxData(lbAnalysesBottom, availableAssignedAnalysesList, analysisName, aid, true);  

                    equipmentAnalyses.Visible = (availableUnassignedAnalysesList.Any() || availableAssignedAnalysesList.Any());

                    if (!availableUnassignedAnalysesList.Any() && !availableAssignedAnalysesList.Any())
                    {
                        var equipmentName = DataMgr.EquipmentDataMapper.GetEquipmentByEID(eid, false, false, false, false, false, false).EquipmentName;

                        lblResults.Text = String.Format(noAnalysisIsAssignable, equipmentName);
                        lblResults.Visible = true;
                    }
                }

            #endregion

        #endregion
    }
}