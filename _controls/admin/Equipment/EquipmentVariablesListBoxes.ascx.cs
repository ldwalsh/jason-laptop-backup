﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.EquipmentVariable;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class EquipmentVariablesListBoxes : SiteUserControl
    {
        #region constants

            const String equipmentVariableDoesNotExistForEquipment = "Equipment variable {0} could not be unassigned from equipment {0} because it does not exist or was already removed.";
            const String unassignEquipmentVariableUpdateSuccessful = "Unassignment of equipment variable {0} to equipment {1} was successful.";
            const String assignEquipmentVariableUpdateSuccessful = "Assignment of equipment variable {0} to equipment {1} was successful.";
            const String reassignUpdateFailed = "Equipment variables reassignment failed. Please contact an administrator.";
            const String noItemsSelected = "No items were selected for reassignment.";
            const Char delimeter = Common.Constants.DataConstants.DefaultDelimiter;

        #endregion

        #region fields

            private enum Operation { Assign, Unassign };

        #endregion

        #region properties

            public Int32 BID { get; set; }
            public Int32 EID { get; set; }
            public String EquipmentName { get; set; }
            public Boolean HasListBoxItemChanged { get; set; }
            public IEnumerable<GetEquipmentEquipmentVariableData> EquipmentVariablesForRepeater { get; set; }
            public Label LabelResults { get; set; }
            public Label LabelVariablesError { get; set; }
            public HtmlAnchor LinkSetFocus { get; set; }

        #endregion

        #region events

            #region button events

                protected void btnUp_ButtonClick(Object sender, EventArgs e)
                {
                    while (lbEquipmentVariablesBottom.SelectedIndex != -1)
                    {
                        lbEquipmentVariablesTop.Items.Add(lbEquipmentVariablesBottom.SelectedItem);
                        lbEquipmentVariablesBottom.Items.Remove(lbEquipmentVariablesBottom.SelectedItem);
                    }

                    GetUpdatedEquipmentVariables();

                    ControlHelper.SortListBox(lbEquipmentVariablesTop);
                }

                protected void btnDown_ButtonClick(Object sender, EventArgs e)
                {
                    while (lbEquipmentVariablesTop.SelectedIndex != -1)
                    {
                        lbEquipmentVariablesBottom.Items.Add(lbEquipmentVariablesTop.SelectedItem);
                        lbEquipmentVariablesTop.Items.Remove(lbEquipmentVariablesTop.SelectedItem);
                    }

                    GetUpdatedEquipmentVariables();

                    ControlHelper.SortListBox(lbEquipmentVariablesBottom);
                }

                protected void btnReassignEquipment_ButtonClick(Object sender, EventArgs e)
                {
                    var ecid = DataMgr.EquipmentClassDataMapper.GetEquipmentClassIDByEID(EID);
                    var equipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariables();
                    var selector = new Func<ListItem, Equipment_EquipmentVariable>(li => new Equipment_EquipmentVariable { EID = EID, EVID = Convert.ToInt32(li.Value.Split(delimeter)[0]) });

                    try
                    {
                        LabelResults.Text = LabelVariablesError.Text = String.Empty;

                        UnassignEquipmentVariables(selector, equipmentVariables);
                        AssignEquipmentVariables(selector, equipmentVariables);

                        if (LabelResults.Text == String.Empty && LabelVariablesError.Text == String.Empty)
                        {
                            LabelHelper.SetLabelMessage(LabelVariablesError, noItemsSelected, LinkSetFocus);
                            return;
                        }

                        GetUpdatedEquipmentVariables();

                        if (LabelVariablesError.Text != String.Empty) LabelHelper.SetLabelMessage(LabelVariablesError, LabelVariablesError.Text, LinkSetFocus);
                        if (LabelResults.Text != String.Empty)
                        {
                            EquipmentStats.CreateCacheKey(siteUser.CID, BID); //clear cache if there was a successful addition or removal of equipment variable(s).
                            ((AdminSitePageTemp)Page).TabStateMonitor.ChangeState(KGSEquipmentAdministration.TabMessages.EditEquipmentVariables);
                            LabelHelper.SetLabelMessage(LabelResults, LabelResults.Text, LinkSetFocus);
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment variables to equipment.", sqlEx);
                        LabelHelper.SetLabelMessage(LabelVariablesError, reassignUpdateFailed, LinkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment variables to equipment.", ex);
                        LabelHelper.SetLabelMessage(LabelVariablesError, reassignUpdateFailed, LinkSetFocus);
                    }
                }

            #endregion

        #endregion

        #region methods

            private void GetUpdatedEquipmentVariables()
            {
                var items = lbEquipmentVariablesBottom.Items;

                if (items.Count > 0)
                {
                    var selector = new Func<ListItem, GetEquipmentEquipmentVariableData>(li => new GetEquipmentEquipmentVariableData { EVID = Convert.ToInt32(li.Value.Split(delimeter)[0]) });
                    var lbEquipmentVariables = items.Cast<ListItem>().Select(selector).ToList();

                    GetUpdatedEquipmentVariablesForRepeater(lbEquipmentVariables);
                }

                HasListBoxItemChanged = true; //set this to true, even if there is nothing in the bottom list. this is due to if you remove the last item, you want to ensure the corresponding repeater item is removed.
            }

            private void GetUpdatedEquipmentVariablesForRepeater(IEnumerable<GetEquipmentEquipmentVariableData> equipmentVariables)
            {
                var updatedEquipmentVariables = new List<GetEquipmentEquipmentVariableData>();

                foreach (var equipmentVariable in equipmentVariables)
                {
                    var updateEquipmentVariable = DataMgr.EquipmentVariableDataMapper.GetEquipmentVariablesWithDataByEIDAndEVID(EID, equipmentVariable.EVID);

                    if (updateEquipmentVariable != null) updatedEquipmentVariables.Add(updateEquipmentVariable);
                }

                if (updatedEquipmentVariables.Any()) EquipmentVariablesForRepeater = updatedEquipmentVariables.OrderBy(uev => uev.EquipmentVariableDisplayName).ToList();
            }

            private void UnassignEquipmentVariables(Func<ListItem, Equipment_EquipmentVariable> selector, IEnumerable<EquipmentVariable> equipmentVariables)
            {
                GetItemsAndAssignOrUnassign(lbEquipmentVariablesTop, selector, equipmentVariables, true, Operation.Unassign);
            }

            private void AssignEquipmentVariables(Func<ListItem, Equipment_EquipmentVariable> selector, IEnumerable<EquipmentVariable> equipmentVariables)
            {
                GetItemsAndAssignOrUnassign(lbEquipmentVariablesBottom, selector, equipmentVariables, false, Operation.Assign, true);
            }

            private void GetItemsAndAssignOrUnassign(ListBox lb, Func<ListItem, Equipment_EquipmentVariable> selector, IEnumerable<EquipmentVariable> equipmentVariables, Boolean isUnassigned, Operation operation, Boolean unitSystem = false)
            {
                if (lb.Items.Count > 0)
                {
                    if (unitSystem) unitSystem = DataMgr.BuildingDataMapper.GetBuildingSettingsByBID(BID).UnitSystem;

                    var items = lb.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, isUnassigned)).Select(selector).ToList();

                    if (items.Any())
                    {
                        foreach (var item in items)
                        {
                            var evid = Convert.ToInt32(item.EVID);
                            var lItem = lb.Items.Cast<ListItem>().Single(li => Convert.ToInt32(li.Value.Split(delimeter)[0]) == evid);
                            var equipmentVariableDisplayName = equipmentVariables.Single(ev => ev.EVID == evid).EquipmentVariableDisplayName;

                            lItem.Value = lItem.Value.Contains("True") ? lItem.Value.Replace("True", "False") : lItem.Value.Replace("False", "True");

                            if (operation == Operation.Unassign) { UnassignEquipmentVariable(evid, equipmentVariableDisplayName); continue; }
                            if (operation == Operation.Assign) { AssignEquipmentVariable(evid, equipmentVariableDisplayName, unitSystem); continue; }
                        }
                    }
                }
            }

            private void UnassignEquipmentVariable(Int32 evid, String equipmentVariableDisplayName)
            {
                if (!DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableExistForEquipment(EID, evid))
                {
                    LabelHelper.SetLabelMessage(LabelVariablesError, String.Format(equipmentVariableDoesNotExistForEquipment, equipmentVariableDisplayName, EquipmentName) + "<br />", LinkSetFocus, true);
                    return;
                }

                DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariableByEIDAndEVID(EID, evid);
                LabelHelper.SetLabelMessage(LabelResults, String.Format(unassignEquipmentVariableUpdateSuccessful, equipmentVariableDisplayName, EquipmentName) + "<br />", LinkSetFocus, true);
            }

            private void AssignEquipmentVariable(Int32 evid, String equipmentVariableDisplayName, Boolean unitSystem)
            {
                if (!DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableExistForEquipment(EID, evid))
                {
                    var ev = DataMgr.EquipmentVariableDataMapper.GetEquipmentVariable(evid);
                    var equipmentEquipmentVariable = new Equipment_EquipmentVariable()
                    {
                        EID = EID,
                        EVID = evid,
                        Value = unitSystem ? ev.SIDefaultValue : ev.IPDefaultValue,
                        DateModified = DateTime.UtcNow
                    };

                    DataMgr.EquipmentVariableDataMapper.InsertEquipmentEquipmentVariable(equipmentEquipmentVariable);
                    LabelHelper.SetLabelMessage(LabelResults, String.Format(assignEquipmentVariableUpdateSuccessful, equipmentVariableDisplayName, EquipmentName) + "<br />", LinkSetFocus, true);
                }
            }

            #region binders

                public void BindEquipmentVariablesToListBoxes()
                {
                    lbEquipmentVariablesTop.Items.Clear();
                    lbEquipmentVariablesBottom.Items.Clear();

                    var equipmntClassID = DataMgr.EquipmentClassDataMapper.GetEquipmentClassIDByEID(EID);
                    var equipmentVariablesNotAssociated = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesNotAssociatedToEID(EID, equipmntClassID);
                    var equipmentVariablesAssociated = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesAssociatedToEID(EID, equipmntClassID);
                    var equipmentVariableDisplayName = PropHelper.G<EquipmentVariable>(ev => ev.EquipmentVariableDisplayName);
                    var evid = PropHelper.G<EquipmentVariable>(ev => ev.EVID);

                    if (equipmentVariablesNotAssociated.Any()) ControlHelper.SetListBoxData(lbEquipmentVariablesTop, equipmentVariablesNotAssociated, equipmentVariableDisplayName, evid, false);
                    if (equipmentVariablesAssociated.Any()) ControlHelper.SetListBoxData(lbEquipmentVariablesBottom, equipmentVariablesAssociated, equipmentVariableDisplayName, evid, true);
                }

            #endregion

        #endregion
    }
}