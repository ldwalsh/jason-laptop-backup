﻿using CW.Common.Constants;
using CW.Data.Models.EquipmentVariable;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration.equipment;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._controls.admin.Equipment
{
    public partial class EquipmentVariables : AdminUserControlBase
    {
        #region constants

            const String partiallySuccessfulFmt = "{0} out of {1} Equipment variables were updated successfully. Please see the failure(s) below.";
            const String variablesUpdateSuccessful = "Equipment variables update was successful.";
            const String variablesUpdateFailed = "Equipment variables update failed. Please contact an administrator.";
            
        #endregion

        #region fields

            public enum AdminModeEnum { NONKGS, KGS, PROVIDER };

        #endregion

        #region properites

            public static AdminModeEnum AdminMode { get; set; }

            public override IEnumerable<Enum> ReactToChangeStateList
            {
                get
                {
                    return new Enum[]
                        {
                            KGSEquipmentAdministration.TabMessages.AddEquipment,
                            KGSEquipmentAdministration.TabMessages.EditEquipment,
                            KGSEquipmentAdministration.TabMessages.DeleteEquipment
                        };
                }
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                tabHeader.Title = IsKGSOrProvider() ? "Equipment Variables To An Equipment" : "Equipment Variables";
                clientName.Visible = IsKGSOrProvider();
                BindBuildings(ddlBuilding);
                BindEquipment(ddlEquipment, -1);

                var adminDropDowns = new AdminDropDowns(RadTabStrip, RadMultiPage, lblResults);
                var currentControl = typeof(EquipmentVariables).Name;

                adminDropDowns.SetBuildingDropDownListAndTabIndex(ddlBuilding, currentControl);
                ddlBuilding_SelectedIndexChanged(null, null);

                adminDropDowns.SetEquipmentDropDownList(ddlEquipment, currentControl);
                ddlEquipment_SelectedIndexChanged(null, null);

                lblUpdateEquipmentVariables.Text = IsKGSOrProvider() ? "Associated Variables" : "Editable Variables";

                if (!this.parentPage.queryString.IsEmpty) ProcessRequestWithQueryString();
            }

            private void Page_Load()
            {
                lblResults.Visible = lblVariablesError.Visible = false;

                tabHeader.SubTitle = IsKGSOrProvider() ? "Please select a building then a peice of equipment in order to select and assign equipment variables."
                                                       : "Please select a building then a peice of equipment in order to view and edit equipment variables.";

                //set the properties on each post back, otherwise values are lost when referenced in each control
                EquipmentVariablesList.EID = (!String.IsNullOrWhiteSpace(ddlEquipment.SelectedValue)) ? Convert.ToInt32(ddlEquipment.SelectedValue) : -1;
                EquipmentVariablesListBoxes.BID = (!String.IsNullOrWhiteSpace(ddlBuilding.SelectedValue)) ? Convert.ToInt32(ddlBuilding.SelectedValue) : -1;
                EquipmentVariablesListBoxes.EID = (!String.IsNullOrWhiteSpace(ddlEquipment.SelectedValue)) ? Convert.ToInt32(ddlEquipment.SelectedValue) : -1;
                EquipmentVariablesListBoxes.EquipmentName = (ddlEquipment.SelectedItem != null) ? ddlEquipment.SelectedItem.Text : String.Empty;
                EquipmentVariablesListBoxes.LabelResults = lblResults;
                EquipmentVariablesListBoxes.LabelVariablesError = lblVariablesError;
                EquipmentVariablesListBoxes.LinkSetFocus = lnkSetFocus;
            }

            private void Page_PreRender()
            {
                if (Page.IsPostBack)
                {
                    if (IsKGSOrProvider())
                    {
                        if (EquipmentVariablesListBoxes.HasListBoxItemChanged) BindRepeater(EquipmentVariablesListBoxes.EquipmentVariablesForRepeater);
                    }
                }
            }

            #region ddl events

                protected void ddlBuilding_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    HideEquipmentVariableContainers();
                    BindEquipment(ddlEquipment, Convert.ToInt32(ddlBuilding.SelectedValue));
                }

                protected void ddlEquipment_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    HideEquipmentVariableContainers();

                    var eid = Convert.ToInt32(ddlEquipment.SelectedValue);

                    if (eid != -1)
                    {
                        if (ContainsQueryString())
                        {
                            EquipmentVariablesListBoxes.BID = Convert.ToInt32(ddlBuilding.SelectedValue);
                            EquipmentVariablesListBoxes.EID = Convert.ToInt32(ddlEquipment.SelectedValue);
                        }

                        var equipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesWithDataByEID(eid);

                        EquipmentVariablesListBoxes.Visible = IsKGSOrProvider();
                        EquipmentVariablesList.Visible = !IsKGSOrProvider();

                        if (IsKGSOrProvider())
                            EquipmentVariablesListBoxes.BindEquipmentVariablesToListBoxes();
                        else
                            EquipmentVariablesList.BindEquipmentVariables(equipmentVariables);

                        if (equipmentVariables.Any())
                        {
                            if (!IsKGSOrProvider()) 
                                equipmentVariables = equipmentVariables.Where(e_ev => e_ev.UserEditable);

                            BindRepeater(equipmentVariables);
                        }
                    }
                }

            #endregion

            #region button events

                protected void btnUpdateEquipmentVariables_ButtonClick(Object sender, EventArgs e)
                {
                    try
                    {
                        int numToProcess = 0;
                        int numSucessful = 0;
                        foreach (RepeaterItem rptItem in rptVars.Items)
                        {
                            if (rptItem.ItemType == ListItemType.Item || rptItem.ItemType == ListItemType.AlternatingItem)
                            {
                                ++numToProcess;
                                HtmlGenericControl valFailure = rptItem.FindControl("valFailure") as HtmlGenericControl;
                                var id = Convert.ToInt32(((HiddenField)rptItem.FindControl("hdnID")).Value);
                                var value = ((TextBox)rptItem.FindControl("txtEditValue")).Text;

                                try
                                {
                                    DataMgr.EquipmentVariableDataMapper.UpdatePartialEquipmentEquipmentVariable(id, value);
                                    ++numSucessful;
                                    valFailure.InnerHtml = "";
                                    valFailure.Style[HtmlTextWriterStyle.Display] = "none";
                                }
                                catch (CW.Data.Exceptions.ValidationException ex)
                                {
                                    valFailure.Style[HtmlTextWriterStyle.Display] = "block";
                                    valFailure.InnerHtml = ex.Message;
                                }
                            }
                        }

                        if (!IsKGSOrProvider()) EquipmentVariablesList.BindEquipmentVariables(); //no parameter means get the updated data from db for nonkgs "variables list" repeater via this update.
                        if (numToProcess > 0)
                        {
                            if (numToProcess == numSucessful)
                            {
                                LabelHelper.SetLabelMessage(lblResults, variablesUpdateSuccessful, lnkSetFocus);
                            }
                            else
                            {
                                LabelHelper.SetLabelMessage(lblResults, string.Format(partiallySuccessfulFmt, numSucessful, numToProcess), lnkSetFocus);
                            }
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipments equipment variables values.", sqlEx);
                        LabelHelper.SetLabelMessage(lblVariablesError, variablesUpdateFailed, lnkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipments equipment variables values.", ex);
                        LabelHelper.SetLabelMessage(lblVariablesError, variablesUpdateFailed, lnkSetFocus);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();

                    //hardset any fields to their initial values
                    ddlBuilding_SelectedIndexChanged(null, null);
                }

            #endregion

            private void HideEquipmentVariableContainers()
            {
                EquipmentVariablesList.Visible = EquipmentVariablesListBoxes.Visible = pnlUpdateEquipmentVariables.Visible = false;
            }

            private Boolean IsKGSOrProvider()
            {
                if (ContainsQueryString()) return true; //this only means that the page loaded with qs links from another admin page, so default to true (KGS or provider)

                return (AdminMode == AdminModeEnum.KGS || AdminMode == AdminModeEnum.PROVIDER);
            }

            private void ProcessRequestWithQueryString()
            {
                var qs = this.parentPage.queryString.ToObject<EquipmentParamsObject>();

                if (qs == null) return;

                ((RadTabStrip)this.Parent.FindControl("tabContainer")).SelectedIndex = ((RadMultiPage)this.Parent.Parent).SelectedIndex = qs.tab;

                try
                {
                    ddlBuilding.SelectedValue = qs.bid.ToString();
                    ddlBuilding_SelectedIndexChanged(null, null);
                    ddlEquipment.SelectedValue = qs.eid.ToString();
                    ddlEquipment_SelectedIndexChanged(null, null);
                }
                catch { }
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindEquipment(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "EquipmentName", "EID", (bid != -1) ? DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid) : null, true, lItem);
                }

                private void BindRepeater(IEnumerable<GetEquipmentEquipmentVariableData> equipmentVariables)
                {
                    pnlUpdateEquipmentVariables.Visible = (equipmentVariables != null && equipmentVariables.Any());

                    if (pnlUpdateEquipmentVariables.Visible)
                    {
                        rptVars.DataSource = equipmentVariables;
                        rptVars.DataBind();
                    }
                }

                private Boolean ContainsQueryString()
                {
                    var url = HttpContext.Current.Request.Url.PathAndQuery;
                    var index = url.IndexOf('?');

                    return (index > 0);
                }

            #endregion

        #endregion
    }
}