﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentToEquipment.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.EquipmentToEquipment" %>
<%@ Register tagPrefix="CW" tagName="EquipToEquipDownloadArea" src="~/_controls/admin/Equipment/EquipToEquipDownloadArea.ascx" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="Equipment To An Equipment">
  <SubTitleTemplate>
    <p>Please select a building and equipment in order to select and assign equipment.</p>
  </SubTitleTemplate>
  <RightAreaTemplate>
    <CW:EquipToEquipDownloadArea runat="server" ID="EquipToEquipDownloadArea" Visible="false" />
  </RightAreaTemplate>
</CW:TabHeader>

<div>
  <a id="lnkSetFocus" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblEquipmentError" CssClass="errorMessage" runat="server" />
</div>
<div class="divForm">   
  <label class="label">*Select Building:</label>
  <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>
<div class="divForm">
  <label class="label">Equipment Class:</label>
  <asp:DropDownList ID="ddlEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>
<div class="divForm">
  <label class="label">Equipment Type:</label>
  <asp:DropDownList ID="ddlEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentType_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>
<div class="divForm">
  <label class="label">*Equipment:</label>
  <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<div id="equipment" visible="false" runat="server">

  <hr />

  <div class="divForm">
    <label class="label">Available:</label>
    <asp:ListBox ID="lbEquipmentTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    <div class="divArrows">
      <asp:ImageButton CssClass="up-arrow"  ID="btnUp" CausesValidation="false" OnClick="btnEquipmentUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
      <asp:ImageButton CssClass="down-arrow" ID="btnDown" CausesValidation="false" OnClick="btnEquipmentDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
    </div>

    <label class="label">Assigned:</label>
    <asp:ListBox ID="lbEquipmentBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    <p>(Note: You cannot inherit equipment from one buildings to another as the points timezones may be different resulting in analyses being scheduled and ran before all data is captured. Alternatively you can assign a point from existing equipment, as those differing time zones are accounted for.)</p>  
  </div>

  <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipment" runat="server" Text="Reassign" OnClick="updateEquipmentButton_Click" ValidationGroup="EquipmentToEquipment" />

</div>

<!--Ajax Validators-->
<asp:RequiredFieldValidator ID="buildingRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EquipmentToEquipment" />
<ajaxToolkit:ValidatorCalloutExtender ID="buildingVCE" runat="server" BehaviorID="buildingVCE" TargetControlID="buildingRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="equipmentRFV" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="ddlEquipment" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EquipmentToEquipment" />
<ajaxToolkit:ValidatorCalloutExtender ID="equipmentVCE" runat="server" BehaviorID="equipmentVCE" TargetControlID="equipmentRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />