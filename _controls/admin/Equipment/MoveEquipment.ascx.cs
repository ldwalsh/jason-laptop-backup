﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class MoveEquipment : AdminUserControlBase
    {
        #region constants

            const String moveEquipmentSuccessful = "Move of Equipment {0} from Building {1} to Building {2} was successful.";            
            const String hasEquipmentImage = "Cannot move equipment. Please delete equipment image first.";
            const String hasEquipomentToEquipmentAssocaitions = "Cannot move equipment. Please remove equipment to equipment associations first.";
            const String hasDiagnostics = "Cannot move equipment. Please purge all diagnostics first.";
            const String hasUtilityBilling = "Cannot move equipment. Please purge all utility billing diagnostics first.";
            const String moveEquipmentFailed = "Move of equipment {0} failed. Please contact administrator.";

        #endregion

        #region properites

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                lblResults.Visible = false;
                lblEquipmentError.Visible = false;

                if (!Page.IsPostBack)
                {
                    ResetDropdowns();    
                }
            }

            #region ddl events

                protected void ddlBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    int bid = Convert.ToInt32(ddlBuilding.SelectedValue);

                    BindEquipment(ddlEquipment, Convert.ToInt32(ddlBuilding.SelectedValue));
                    BindBuildings(ddlBuildingMove, siteUser.VisibleBuildings.Where(b => b.BID != bid));
                }    

            #endregion

            #region button events              

                protected void moveEquipmentButton_Click(Object sender, EventArgs e)
                {
                    var eid = Convert.ToInt32(ddlEquipment.SelectedValue);
                    var bid = Convert.ToInt32(ddlBuilding.SelectedValue);
                    var bidMove = Convert.ToInt32(ddlBuildingMove.SelectedValue);
                    var equipmentName = ddlEquipment.SelectedItem.Text;
                    var buildingName = ddlBuilding.SelectedItem.Text;
                    var buildingMoveName = ddlBuildingMove.SelectedItem.Text;

                    var equipment = DataMgr.EquipmentDataMapper.GetEquipmentByID(eid);
                    var utilityBillingAIDs = DataMgr.AnalysisDataMapper.GetAllActiveAnalysesByEID(eid, true).Select(a => a.AID);

                    lblEquipmentError.Text = String.Empty;
                    lblResults.Text = String.Empty;

                    //need to check for equipment image
                    //delete any related equipment to equipment assocaitions
                    //no need to check for taged files
                    //can update projects
                    try
                    {
                        //check for equipment image
                        if (String.IsNullOrEmpty(equipment.ImageExtension))
                        {
                            var equipmentAnalyses = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByEID(eid);

                            foreach (Analyses_Equipment ae in equipmentAnalyses)
                            {
                                //check if vdata exists for this analysis equipment
                                //no need to check vpredata as vdata will always exists for a priority.
                                if (!utilityBillingAIDs.Contains(ae.AID) && DataMgr.VAndVPreDataMapper.DoesVDataExistForEIDAndAID(eid, ae.AID))
                                {
                                    LabelHelper.SetLabelMessage(lblEquipmentError, hasDiagnostics, lnkSetFocus);
                                    return;
                                }

                                //check if vdatautilitybilling exists for this analysis equipment
                                //no need to check vpredatautilitybilling as vdatautilitybilling will always exists for a priority.
                                if (utilityBillingAIDs.Contains(ae.AID) && DataMgr.VAndVPreDataMapper.DoesVDataUtilityBillingExistForEIDAndAID(DataConstants.UtiliityAnalysisTypes.Utility, eid, ae.AID))
                                {
                                    LabelHelper.SetLabelMessage(lblEquipmentError, hasUtilityBilling, lnkSetFocus);
                                    return;
                                }
                            }

                            //delete any equipment to equipment associations
                            DataMgr.EquipmentDataMapper.DeleteEquipmentToEquipmentAssociationsByRelatedEID(eid);

                            //update project associations to new buillding
                            DataMgr.ProjectDataMapper.MoveEquipmentProjectAssociations(eid, bidMove);

                            //update scheduled analyses associations to new buillding
                            DataMgr.ScheduledAnalysisDataMapper.MoveEquipmentScheduledAnalysesAssociations(eid, bidMove);

                            //update equipment to new building
                            equipment.BID = bidMove;
                            DataMgr.EquipmentDataMapper.UpdateEquipment(equipment);

                            LabelHelper.SetLabelMessage(lblResults, String.Format(moveEquipmentSuccessful, equipmentName, buildingName, buildingMoveName), lnkSetFocus);

                            //reset equipddl
                            ResetDropdowns();
                        }
                        else
                        {
                            LabelHelper.SetLabelMessage(lblEquipmentError, hasEquipmentImage, lnkSetFocus);
                            return;
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error moving equipment. {0}", equipmentName), sqlEx);
                        LabelHelper.SetLabelMessage(lblEquipmentError, String.Format(moveEquipmentFailed, equipmentName), lnkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error moving equipment. {0}", equipmentName), ex);
                        LabelHelper.SetLabelMessage(lblEquipmentError, String.Format(moveEquipmentFailed, equipmentName), lnkSetFocus);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

            #endregion

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }
                private void BindBuildings(DropDownList ddl, IEnumerable<Building> buildingsSubset)
                {
                    var lItem = new ListItem() { Text = (buildingsSubset.Any()) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };
    
                    BindDDL(ddl, "BuildingName", "BID", buildingsSubset, true, lItem);
                }

                private void BindEquipment(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "EquipmentName", "EID", (bid != -1) ? DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid) : null, true, lItem);
                }            

            #endregion

            private void ResetDropdowns()
            {
                BindBuildings(ddlBuilding);
                BindBuildings(ddlBuildingMove, Enumerable.Empty<Building>());
                BindEquipment(ddlEquipment, -1);
            }

        #endregion
    }
}