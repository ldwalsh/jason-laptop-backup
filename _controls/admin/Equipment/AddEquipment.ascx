﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AddEquipment.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.AddEquipment" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" Namespace="FileUploaderControl" Assembly="FileUploaderControl" %>

<asp:Panel ID="pnlAddEquipment" runat="server" DefaultButton="btnAddEquipment">

  <h2>Add Equipment</h2>  

  <div>

    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />

    <div class="divForm">
      <label class="label">*Equipment Name:</label>
      <asp:TextBox ID="txtAddEquipmentName" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div> 
    <div class="divForm">
      <label class="label">*Building:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlAddBuilding" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Equipment Class:</label>    
      <asp:DropDownList ID="ddlAddEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Equipment Type:</label>    
      <asp:DropDownList ID="ddlAddEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Manufacturer:</label>    
      <asp:DropDownList ID="ddlAddManufacturer" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddManufacturer_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Model:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlAddManufacturerModel" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Serial Number:</label>
      <asp:TextBox ID="txtAddSerialNumber" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Location:</label>                                                         
      <textarea name="txtAddEquipmentLocation" id="txtAddEquipmentLocation" cols="40" rows="5" onkeyup="limitChars(this, 250, 'divAddEquipmentLocationCharInfo')" runat="server" />
      <div id="divAddEquipmentLocationCharInfo"></div>
    </div> 
    <div class="divForm">
      <label class="label">Description:</label>                                                         
      <textarea name="txtAddDescription" id="txtAddDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" runat="server" />
      <div id="divAddDescriptionCharInfo"></div>
    </div>                                                                                                                                                                                                                                                                                                                                                                                                                                         
    <div class="divForm">
      <label class="label">Image (800x600):</label>
      <CW:FileUploader runat="server" ID="fileUploaderAdd"/>
    </div>
    <div class="divForm">   
      <label class="label">Primary Manufacturer Contact:</label>    
      <asp:DropDownList ID="ddlAddPrimaryContact" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>  
    <div class="divForm">   
      <label class="label">Secondary Manufacturer Contact:</label>    
      <asp:DropDownList ID="ddlAddSecondaryContact" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Equipment Notes (Max HTML Characters = 5000):</label>
      <telerik:RadEditor ID="editorAddEquipmentNotes" runat="server" CssClass="editorNarrow" Height="275px" Width="462px" MaxHtmlLength="5000" NewLineMode="Div" ToolsWidth="464px" ToolbarMode="ShowOnFocus" ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" EditModes="Design,Preview"> 
        <CssFiles>
          <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
        </CssFiles>                                                                                                                             
      </telerik:RadEditor>                                                         
    </div>
    <hr />
                                                    
    <h2>Add Additional Equipment Information</h2>
                                                    
    <div class="divFormNarrow">
      <label class="labelWide">External Equipment Type Name:</label>
      <asp:TextBox ID="txtAddExternalEquipmentTypeName" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">CMMS (Computerized Maintenance Mgmt System) Reference/Asset ID:</label>
      <asp:TextBox ID="txtAddCMMSReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">CMMS (Computerized Maintenance Management System) Location ID:</label>
      <asp:TextBox ID="txtAddCMMSLocationID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">CMMS (Computerized Maintenance Management System) Site ID:</label>
      <asp:TextBox ID="txtAddCMMSSiteID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">CMMS (Computerized Maintenance Management System) Link:</label>
      <asp:TextBox ID="txtAddCMMSLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">BIM (Building Information Management) Reference ID:</label>
      <asp:TextBox ID="txtAddBIMReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">BIM (Building Information Management) Link:</label>
      <asp:TextBox ID="txtAddBIMLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">DMS (Document Management Software) Reference ID:</label>
      <asp:TextBox ID="txtAddDMSReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">DMS (Document Management Software) Link:</label>
      <asp:TextBox ID="txtAddDMSLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>  
    <div class="divFormNarrow">
      <label class="labelWide">GIS (Geographical Information System) Reference ID:</label>
      <asp:TextBox ID="txtAddGISReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">GIS (Geographical Information System) Link:</label>
      <asp:TextBox ID="txtAddGISLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnAddEquipment" runat="server" Text="Add Equipment" OnClick="addEquipmentButton_Click" ValidationGroup="AddEquipment" />

  </div>
                                                
  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="addEquipmentNameRequiredValidator" runat="server" ErrorMessage="Equipment Name is a required field." ControlToValidate="txtAddEquipmentName" SetFocusOnError="true" Display="None" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentNameRequiredValidatorExtender" runat="server" BehaviorID="addEquipmentNameRequiredValidatorExtender" TargetControlID="addEquipmentNameRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addBuildingRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlAddBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addBuildingRequiredValidatorExtender" runat="server" BehaviorID="addBuildingRequiredValidatorExtender" TargetControlID="addBuildingRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addEquipmentClassRequiredValidator" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlAddEquipmentClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentClassRequiredValidatorExtender" runat="server" BehaviorID="addEquipmentClassRequiredValidatorExtender" TargetControlID="addEquipmentClassRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addEquipmentTypeRequiredValidator" runat="server" ErrorMessage="Equipment Type is a required field." ControlToValidate="ddlAddEquipmentType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentTypeRequiredValidatorExtender" runat="server" BehaviorID="addEquipmentTypeRequiredValidatorExtender" TargetControlID="addEquipmentTypeRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addManufacturerRequiredValidator" runat="server" ErrorMessage="Manufacturer is a required field." ControlToValidate="ddlAddManufacturer" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addManufacturerRequiredValidatorExtender" runat="server" BehaviorID="addManufacturerRequiredValidatorExtender" TargetControlID="addManufacturerRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addManufacturerModelRequiredValidator" runat="server" ErrorMessage="Model is a required field." ControlToValidate="ddlAddManufacturerModel" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addManufacturerModelRequiredValidatorExtender" runat="server" BehaviorID="addManufacturerModelRequiredValidatorExtender" TargetControlID="addManufacturerModelRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addCMMSLinkRegExValidator" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtAddCMMSLink" SetFocusOnError="true" Display="None"  ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addCMMSLinkRegExValidatorExtender" runat="server" BehaviorID="addCMMSLinkRegExValidatorExtender" TargetControlID="addCMMSLinkRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addBIMLinkRegExValidator" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtAddBIMLink" SetFocusOnError="true" Display="None" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addBIMLinkRegExValidatorExtender" runat="server" BehaviorID="addBIMLinkRegExValidatorExtender" TargetControlID="addBIMLinkRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addDMSLinkRegExValidator" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtAddDMSLink" SetFocusOnError="true" Display="None" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addDMSLinkRegExValidatorExtender" runat="server" BehaviorID="addDMSLinkRegExValidatorExtender" TargetControlID="addDMSLinkRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="addGISLinkRegExValidator" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtAddGISLink" SetFocusOnError="true" Display="None" ValidationGroup="AddEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addGISLinkRegExValidatorExtender" runat="server" BehaviorID="addGISLinkRegExValidatorExtender" TargetControlID="addGISLinkRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
 
</asp:Panel>