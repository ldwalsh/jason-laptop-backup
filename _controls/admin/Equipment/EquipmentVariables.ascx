﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentVariables.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.EquipmentVariables" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<%@ Register TagPrefix="CW" TagName="EquipmentVariablesList" src="~/_controls/admin/Equipment/EquipmentVariablesList.ascx" %>
<%@ Register TagPrefix="CW" TagName="EquipmentVariablesListBoxes" src="~/_controls/admin/Equipment/EquipmentVariablesListBoxes.ascx" %>
<%@ Register TagPrefix="CW" TagName="TabHeader" Src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data.Models.EquipmentVariable" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" />

<div>
  <a id="lnkSetFocus" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblVariablesError" CssClass="errorMessage" runat="server" />
</div>
<div id="clientName" class="divForm" runat="server" visible="false">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div>   
<div class="divForm">
  <label class="label">*Select Building:</label>    
  <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>                                                                                                                                                       
<div class="divForm">   
  <label class="label">*Select Equipment:</label>
  <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_SelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>

<%-- NON-KGS --%>
<CW:EquipmentVariablesList ID="EquipmentVariablesList" runat="server" Visible="false" />

<%-- KGS --%>
<CW:EquipmentVariablesListBoxes ID="EquipmentVariablesListBoxes" runat="server" Visible="false" />

<!--EDIT EQUIPMENT VARIABLES PANEL -->                              
<asp:Panel ID="pnlUpdateEquipmentVariables" runat="server" Visible="false" DefaultButton="btnUpdateEquipmentVariables">        
  <hr />

  <h2><asp:Label ID="lblUpdateEquipmentVariables" runat="server" /></h2>

  <p>(Note: Default values are set on an equipment variable level.)</p>

  <asp:Repeater ID="rptVars" runat="server">
    <ItemTemplate> 
      <div class="divForm">                                                                                                                  
        <label class="label">*<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.EquipmentVariableDisplayName))%>:</label>   
        <asp:TextBox ID="txtEditValue" runat="server" 
            CssClass='<%# Convert.ToBoolean(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? "" : "textbox" %>' 
            Rows='<%# Convert.ToBoolean(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? 5 : 1 %>' 
            Columns='<%# Convert.ToBoolean(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? 40 : 1 %>' 
            TextMode='<%# Convert.ToBoolean(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? TextBoxMode.MultiLine : TextBoxMode.SingleLine %>' 
            MaxLength='<%# Convert.ToBoolean(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) ? 2500 : 25 %>' 
            Text='<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.Value)) %>'
            onkeyup='<%# "limitChars(this, 2500, &#39;divCharInfo" + Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.ID)) + "&#39;)"%>' />
        &nbsp;<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.Unit))  %>
        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.ID)) %>' />
        <asp:HiddenField ID="hdnIsBuildingSettingBasedEngUnitStringOrArray" runat="server" Value='<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray)) %>' />
        <asp:HiddenField ID="hdnIsBuildingSettingBasedEngUnitsBoolean" runat="server" Value='<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitsBoolean)) %>' />
        <asp:HiddenField ID="hdnMinValue" runat="server" Value='<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.MinValue)) %>' />
        <asp:HiddenField ID="hdnMaxValue" runat="server" Value='<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.MaxValue)) %>' />
        <!--TODO: Validator for Bools-->
        <asp:RequiredFieldValidator ID="editValueRFV" runat="server" CssClass="errorMessage" ErrorMessage="Required" ControlToValidate="txtEditValue" SetFocusOnError="true" ValidationGroup="UpdateEquipmentVariables" Display="Dynamic" />
        <div id='valFailure' runat="server" class="errorMessage" style="display:none"></div>
        <asp:Panel Visible='<%# !Convert.ToBoolean(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) %>' runat="server">
          (<%# Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.Info)) %>)
        </asp:Panel>
        <asp:Panel Visible='<%# Convert.ToBoolean(Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.IsBuildingSettingBasedEngUnitStringOrArray))) %>' runat="server">
          (String Format = text, Array Format (max 2500 characters) = {0;1.1;2;3})
          <div id='<%# "divCharInfo" + Eval(PropHelper.G<GetEquipmentEquipmentVariableData>(_ => _.ID))%>'></div>
        </asp:Panel>
      </div>
    </ItemTemplate>
  </asp:Repeater>
                                        
  <asp:LinkButton ID="btnUpdateEquipmentVariables" runat="server" Text="Update" CssClass="lnkButton" OnClick="btnUpdateEquipmentVariables_ButtonClick" ValidationGroup="UpdateEquipmentVariables" />

</asp:Panel>