﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AnalysisToMultipleEquipment.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.AnalysisToMultipleEquipment" %>

<asp:Panel ID="pnlUpdateAnalysisToMultiEquipment" runat="server" DefaultButton="btnUpdateAnalysisToMultiEquipment">

  <h2>Analysis To Multiple Equipment</h2>

  <p>Please select an analysis then on or more equipment to associate analysis.</p>

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAnalysisError" CssClass="errorMessage" runat="server" />               
  </div>
  <div class="divForm">
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>
  </div> 
  <div class="divForm">
    <label class="label">*Select Building:</label>    
    <asp:DropDownList ID="ddlBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
  </div>  
  <div class="divForm">   
    <label class="label">*Equipment Class:</label>    
    <asp:DropDownList ID="ddlEquipmentClasses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClasses_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />    
  </div>
    <div class="divForm">   
    <label class="label">*Select Analysis:</label>    
    <asp:DropDownList ID="ddlAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
  </div>
  <div id="analysisEquipment" visible="false" runat="server">
    <hr />

    <div class="richText">
      <p>Equipment are available based on several criteria:</p>

      <ul>
        <li>Equipment variables that are required are associated with both the equipment and an analysis.</li>
        <li>Point types that are associated with both the equipments points and the required analysis input point types.</li>
      </ul>

      <p>(Note: You cannot unassign an analysis to a piece of equipment if it has already been scheduled.)</p>
      <p>(Note: You cannot unassign an analysis to a piece of equipment if it has already been analyzed and vdata or vpredata exists. A system admin must purge all analyzed data for the analysis and equipment first.)</p>
      <p>(Warning: Please do not assign and schedule an analysis untill is has been completely setup. Assigning, and then scheduling an analysis without the proper variables and point types setup will cause the automation to malfunction.)</p>
      <p>(Warning: Assigning an analysis will generate output associations for all vpoint and vprepoint types associated with the analysis for the selected equipment.)</p>
    </div>                                            
    <div class="divForm">                                       
      <label class="label">Available:</label>    
      <asp:ListBox ID="lbAnalysisEquipmentTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />                                                        

      <div class="divArrows">
        <asp:ImageButton CssClass="up-arrow"  ID="ImageButton3" CauseValidation="false" OnClick="btnAnalysisEquipmentUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
        <asp:ImageButton CssClass="down-arrow" ID="ImageButton4" CauseValidation="false" OnClick="btnAnalysisEquipmentDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
      </div>

      <label class="label">Assigned:</label>
      <asp:ListBox ID="lbAnalysisEquipmentBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateAnalysisToMultiEquipment" runat="server" Text="Reassign"  OnClick="updateAnalysisToMultiEquipmentButton_Click" ValidationGroup="UpdateAnalysisToMultiEquipment" />
                                                
  </div>

</asp:Panel>