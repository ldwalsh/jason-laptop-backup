﻿using CW.Data;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._downloads;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class EquipToEquipDownloadArea : DownloadAreaBase<CW.Data.Equipment, EquipToEquipScheduleGenerator>
    {
        #region fields

            private String fnBid = PropHelper.G<Building>(_ => _.BID);
            private String fnEquipmentList = "fnEquipmentList";

        #endregion

        #region properties

            #region overrides

                protected override Func<Dictionary<String, Object>, IEnumerable<CW.Data.Equipment>> GetData { get { return GetAllEquipmentByBIDWithAssociatedEquip; } }

                protected override Dictionary<String, Object> Parameters
                {
                    get
                    {
                        var items = new Dictionary<String, Object>();
                        var bid = DropDownList.SelectedValue;

                        items.Add(fnBid, DropDownList.SelectedValue);
                        items.Add(PropHelper.G<Building>(_ => _.BuildingName), DropDownList.SelectedItem.Text);
                        items.Add(fnEquipmentList, DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(Convert.ToInt32(bid)));

                        return items;
                    }
                }

            #endregion

        #endregion

        #region methods

            public IEnumerable<CW.Data.Equipment> GetAllEquipmentByBIDWithAssociatedEquip(Dictionary<String, Object> parameters)
            {
                var bid = Convert.ToInt32(parameters[fnBid]);
                var equipmentTypeExp = new Expression<Func<CW.Data.Equipment, Object>>[] { (e => e.EquipmentType) };
                var equipmentClassExp = new Expression<Func<EquipmentType, Object>>[] { (eqt => eqt.EquipmentClass) };
                var equipToEquipExp = new Expression<Func<CW.Data.Equipment, Object>>[] { (e => e.Equipment_Equipments) };

                return DataMgr.EquipmentDataMapper.GetAllEquipmentByBIDWithAssociatedEquip(bid, equipmentTypeExp, equipmentClassExp, equipToEquipExp);
            }

        #endregion
    }
}