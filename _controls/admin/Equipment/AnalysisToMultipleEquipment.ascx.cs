﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class AnalysisToMultipleEquipment : AdminUserControlBase
    {
        #region constants

            const String unassignAnalysisUpdateSuccessful = "Unassignment of equipment {0} to analysis {1} was successful.";
            const String assignAnalysisUpdateSuccessful = "Assignment of equipment {0} to analysis {1} was successful.";
            const String reassignAnalysisUpdateFailed = "Analysis to multiple equipment reassignment failed. Please contact an administrator.";
            const String scheduledAnalysisRequiresAnalysis = " equipment could not be unassigned because it has already been scheduled.";
            const String vDataAndVPreDataRequiresAnalysis = " equipment could not be unassigned becuase vdata and/or vpredata exists for it.";
            const String projectsRequiresAnalysis = " equipment could not be unassigned becuase projects exists for it.";
            const String tasksRequiresAnalysis = " equipment could not be unassigned becuase tasks exists for it.";
            const String noItemsSelected = "No items were selected for reassignment.";
            const String noEquipmentIsAssignable = "No equipment can be assigned to {0}.";

        #endregion

        #region properites

            public override IEnumerable<Enum> ReactToChangeStateList
            {
                get
                {
                    return new Enum[]
                    {
                        KGSEquipmentAdministration.TabMessages.AddEquipment,
                        KGSEquipmentAdministration.TabMessages.EditEquipment,
                        KGSEquipmentAdministration.TabMessages.DeleteEquipment
                    };
                }
            }

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAnalysisError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindBuildings(ddlBuildings);
                    BindEquipmentClasses(ddlEquipmentClasses, -1);
                    BindAnalyses(ddlAnalyses, -1);
                }
            }

            #region ddl events

                protected void ddlBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    ddlEquipmentClasses.SelectedIndex = 0;
                    ddlAnalyses.SelectedIndex = 0;
                    BindEquipmentClasses(ddlEquipmentClasses, Convert.ToInt32(ddlBuildings.SelectedValue));
                    BindAnalyses(ddlAnalyses, -1);
                    SetAnalysisEquipmentPanel();
                }

                protected void ddlEquipmentClasses_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindAnalyses(ddlAnalyses, Convert.ToInt32(ddlEquipmentClasses.SelectedValue));
                    SetAnalysisEquipmentPanel();
                }

                protected void ddlAnalyses_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetAnalysisEquipmentPanel();
                }

            #endregion

            #region button events

                protected void btnAnalysisEquipmentUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbAnalysisEquipmentBottom.SelectedIndex != -1)
                    {
                        lbAnalysisEquipmentTop.Items.Add(lbAnalysisEquipmentBottom.SelectedItem);
                        lbAnalysisEquipmentBottom.Items.Remove(lbAnalysisEquipmentBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbAnalysisEquipmentTop);
                }

                protected void btnAnalysisEquipmentDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbAnalysisEquipmentTop.SelectedIndex != -1)
                    {
                        lbAnalysisEquipmentBottom.Items.Add(lbAnalysisEquipmentTop.SelectedItem);
                        lbAnalysisEquipmentTop.Items.Remove(lbAnalysisEquipmentTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbAnalysisEquipmentBottom);
                }

                protected void updateAnalysisToMultiEquipmentButton_Click(Object sender, EventArgs e)
                {
                    var bid = Convert.ToInt32(ddlBuildings.SelectedValue);
                    var ecid = Convert.ToInt32(ddlEquipmentClasses.SelectedValue);
                    var aid = Convert.ToInt32(ddlAnalyses.SelectedValue);
                    var analysisName = ddlAnalyses.SelectedItem.Text;
                    var equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByCID(siteUser.CID);
                    var selector = new Func<ListItem, Analyses_Equipment>(li => new Analyses_Equipment { AID = aid, EID = Convert.ToInt32(li.Value.Split(DataConstants.DefaultDelimiter)[0]) });

                    try
                    {
                        lblAnalysisError.Text = String.Empty;
                        lblResults.Text = String.Empty;

                        UnassignAnalysisForEquipments(aid, selector, equipment, analysisName);
                        AssignAnalysisForEquipments(aid, selector, equipment, analysisName);
                        
                        if (lblAnalysisError.Text == String.Empty && lblResults.Text == String.Empty)
                        {
                            LabelHelper.SetLabelMessage(lblAnalysisError, noItemsSelected, lnkSetFocus);
                            return;
                        }

                        BindAnalysisEquipment(aid, bid, ecid);

                        if (lblAnalysisError.Text != String.Empty) LabelHelper.SetLabelMessage(lblAnalysisError, lblAnalysisError.Text, lnkSetFocus);
                        if (lblResults.Text != String.Empty)  LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblAnalysisError, reassignAnalysisUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning analysis to multiple equipment.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblAnalysisError, reassignAnalysisUpdateFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning analysis to multiple equipment.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();
                    ddlBuildings_OnSelectedIndexChanged(null, null);
                }

            #endregion

            private void UnassignAnalysisForEquipments(Int32 aid, Func<ListItem, Analyses_Equipment> selector, IEnumerable<CW.Data.Equipment> equipment, String analysisName)
            {
                var unassignedEquipment = lbAnalysisEquipmentTop.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, true)).Select(selector).ToList();

                foreach (var item in unassignedEquipment)
                {
                    var eid = Convert.ToInt32(item.EID);
                    var equipmentName = equipment.Where(e => e.EID == eid).Select(e => e.EquipmentName).First();

                    //if the analysis to equipment exists, delete
                    if (DataMgr.AnalysisEquipmentDataMapper.IsAnalysisAssignedToEquipment(eid, aid))
                    {
                        //check if their is a scheduled analysis for this eqipment and analysis.
                        if (!DataMgr.ScheduledAnalysisDataMapper.DoesScheduledAnalysisExist(eid, aid))
                        {
                            //check if project is associated with this eqipment and analysis.
                            if (!DataMgr.ProjectDataMapper.DoesProjectAssoicationExistForEIDAndAID(eid, aid))
                            {
                                //check if tasks is associated with this eqipment and analysis.
                                if (!DataMgr.TasksDataMapper.DoesTasksExistForEIDAndAID(eid, aid))
                                {
                                    bool isUtilityBilling = DataMgr.AnalysisDataMapper.GetAnalysisByAID(aid).First().IsUtilityBilling;

                                    //check if vdata exists for this analysis equipment
                                    //no need to check vpredata as vdata will always exists for a priority.
                                    if (isUtilityBilling || (!isUtilityBilling && !(DataMgr.VAndVPreDataMapper.DoesVDataExistForEIDAndAID(eid, aid))))
                                    {
                                        //check if vdatautilitybilling exists for this analysis equipment
                                        //no need to check vpredatautilitybilling as vdatautilitybilling will always exists for a priority.                                        
                                        if (!isUtilityBilling || (isUtilityBilling && !(DataMgr.VAndVPreDataMapper.DoesVDataUtilityBillingExistForEIDAndAID(DataConstants.UtiliityAnalysisTypes.Utility, eid, aid))))
                                        {
                                            //meaning delete all vpoints for that equipment and analysis
                                            DataMgr.PointDataMapper.DeleteAllVPointsByAIDAndEID(aid, eid);

                                            //meaning delete all vprepoints for that equipment and analysis
                                            DataMgr.PointDataMapper.DeleteAllVPrePointsByAIDAndEID(aid, eid);
                                            DataMgr.AnalysisEquipmentDataMapper.DeleteAnalysisEquipmentByEIDAndAID(eid, aid);

                                            LabelHelper.SetLabelMessage(lblResults, String.Format(unassignAnalysisUpdateSuccessful, equipmentName, analysisName) + "<br />", lnkSetFocus, true);
                                        }
                                        else //DoesVDataUtilityBillingExistForEIDAndAID check else
                                            LabelHelper.SetLabelMessage(lblAnalysisError, equipmentName + vDataAndVPreDataRequiresAnalysis + "<br />", lnkSetFocus, true);
                                    }
                                    else //DoesVDataExistForEIDAndAID check else
                                        LabelHelper.SetLabelMessage(lblAnalysisError, equipmentName + vDataAndVPreDataRequiresAnalysis + "<br />", lnkSetFocus, true);
                                }
                                else //DoesTasksExistForEIDAndAID check else
                                    LabelHelper.SetLabelMessage(lblAnalysisError, equipmentName + tasksRequiresAnalysis + "<br />", lnkSetFocus, true);
                            }
                            else //DoesProjectAssoicationExistForEIDAndAID check else
                                LabelHelper.SetLabelMessage(lblAnalysisError, equipmentName + projectsRequiresAnalysis + "<br />", lnkSetFocus, true);
                        }
                        else //DoesScheduledAnalysisExist check else
                            LabelHelper.SetLabelMessage(lblAnalysisError, equipmentName + scheduledAnalysisRequiresAnalysis + "<br />", lnkSetFocus, true);
                    }
                }
            }

            private void AssignAnalysisForEquipments(Int32 aid, Func<ListItem, Analyses_Equipment> selector, IEnumerable<CW.Data.Equipment> equipment, String analysisName)
            {
                var assignedEquipment = lbAnalysisEquipmentBottom.Items.Cast<ListItem>().Where(li => ControlHelper.ContainsItem(li, false)).Select(selector).ToList();

                foreach (var item in assignedEquipment)
                {
                    var mAnalysisEquipment = new Analyses_Equipment();
                    var eid = Convert.ToInt32(item.EID);
                    var equipmentName = equipment.Where(e => e.EID == eid).Select(e => e.EquipmentName).First();

                    mAnalysisEquipment.AID = aid;
                    mAnalysisEquipment.EID = eid;

                    if (!DataMgr.AnalysisEquipmentDataMapper.IsAnalysisAssignedToEquipment(eid, aid))
                    {
                        DataMgr.AnalysisEquipmentDataMapper.InsertAnalysisEquipment(mAnalysisEquipment);

                        var aovpts = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPointTypesByAID(aid);

                        foreach (var apvpt in aovpts)
                        {
                            var vp = new VPoint();

                            vp.AID = aid;
                            vp.EID = eid;
                            vp.PointTypeID = apvpt.PointTypeID;

                            DataMgr.PointDataMapper.InsertVPoint(vp);
                        }

                        var aovppts = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPrePointTypesByAID(aid);

                        foreach (var apvppt in aovppts)
                        {
                            var vpp = new VPrePoint();

                            vpp.AID = aid;
                            vpp.EID = eid;
                            vpp.PointTypeID = apvppt.PointTypeID;

                            DataMgr.PointDataMapper.InsertVPrePoint(vpp);
                        }

                        LabelHelper.SetLabelMessage(lblResults, String.Format(assignAnalysisUpdateSuccessful, equipmentName, analysisName) + "<br />", lnkSetFocus, true);
                    }
                }
            }

            private void ClearListBoxes()
            {
                lbAnalysisEquipmentTop.Items.Clear();
                lbAnalysisEquipmentBottom.Items.Clear();
            }

            private void SetAnalysisEquipmentPanel()
            {
                var bid = Convert.ToInt32(ddlBuildings.SelectedValue);
                var aid = Convert.ToInt32(ddlAnalyses.SelectedValue);
                var ecid = Convert.ToInt32(ddlEquipmentClasses.SelectedValue);

                BindAnalysisEquipment(aid, bid, ecid);
            }

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindEquipmentClasses(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "EquipmentClassName", "EquipmentClassID", (bid != -1) ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid) : null, true, lItem);
                }

                private void BindAnalyses(DropDownList ddl, Int32 ecid)
                {
                    var lItem = new ListItem() { Text = (ecid != -1) ? "Select one..." : "Select equipment class first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "AnalysisName", "AID", (ecid != -1) ? DataMgr.AnalysisDataMapper.GetAllAvailableActiveAnalysesByCIDAndECID(siteUser.CID, ecid) : null, true, lItem);
                }

                private void BindAnalysisEquipment(Int32 aid, Int32 bid, Int32 ecid)
                {
                    ClearListBoxes();

                    if (bid > 0)
                    {
                        CW.Business.DataManager dm = DataMgr;
                        var equipments = (ecid != 0) ? dm.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(ecid, bid) : dm.EquipmentDataMapper.GetAllEquipmentByBID(bid);

                        if (aid > 0)
                        {
                            #region parallel code

                                // Step 1: Create ConcurrentQueue objects for query and result queues
                                var equipmentQueue = new ConcurrentQueue<CW.Data.Equipment>();
                                var availableUnassignedEquipmentQueue = new ConcurrentQueue<CW.Data.Equipment>();
                                var availableAssignedEquipmentQueue = new ConcurrentQueue<CW.Data.Equipment>();

                                // Step 2: Populate query queue
                                foreach (var equipment in equipments) equipmentQueue.Enqueue(equipment);

                                Action action = () =>
                                {
                                    CW.Data.Equipment localValue;

                                    while (equipmentQueue.TryDequeue(out localValue))
                                    {
                                        var available = dm.AnalysisEquipmentDataMapper.IsAnalysisAvailableForEquipment(localValue.EID, aid);
                                        var assigned = dm.AnalysisEquipmentDataMapper.IsAnalysisAssignedToEquipment(localValue.EID, aid);

                                        if (available && !assigned) availableUnassignedEquipmentQueue.Enqueue(localValue);
                                        if (available && assigned) availableAssignedEquipmentQueue.Enqueue(localValue);
                                    }
                                };

                                // Step 4: Start 4 concurrent consuming actions.
                                Parallel.Invoke(action, action, action, action);

                                // Step 5: Gather results
                                var availableUnassignedEquipmentList = new List<CW.Data.Equipment>();
                                var availableAssignedEquipmentList = new List<CW.Data.Equipment>();

                                CW.Data.Equipment resultValue;

                                while (availableUnassignedEquipmentQueue.TryDequeue(out resultValue)) availableUnassignedEquipmentList.Add(resultValue);
                                while (availableAssignedEquipmentQueue.TryDequeue(out resultValue)) availableAssignedEquipmentList.Add(resultValue);

                            #endregion

                            var equipmentName = PropHelper.G<CW.Data.Equipment>(e => e.EquipmentName);
                            var eid = PropHelper.G<CW.Data.Equipment>(e => e.EID);

                            Expression<Func<Data.Equipment, Object>> orderByExpression = e => e.EquipmentName;

                            if (availableUnassignedEquipmentList.Any()) ControlHelper.SetListBoxData(lbAnalysisEquipmentTop, availableUnassignedEquipmentList.AsQueryable().OrderBy(orderByExpression), equipmentName, eid, false);
                            if (availableAssignedEquipmentList.Any()) ControlHelper.SetListBoxData(lbAnalysisEquipmentBottom, availableAssignedEquipmentList.AsQueryable().OrderBy(orderByExpression), equipmentName, eid, true);

                            analysisEquipment.Visible = (availableUnassignedEquipmentList.Any() || availableAssignedEquipmentList.Any());

                            if (!availableUnassignedEquipmentList.Any() && !availableAssignedEquipmentList.Any())
                            {
                                lblResults.Text = String.Format(noEquipmentIsAssignable, ddlAnalyses.SelectedItem.Text);
                                lblResults.Visible = true;
                            }
                        }
                    }
                    else
                        analysisEquipment.Visible = false;
                }

            #endregion

        #endregion
    }
}