﻿using CW.Data;
using CW.Data.Models.Equipment;
using CW.Utility;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class EquipmentStats : AdminUserControlGridAdminLinks
    {
        #region fields

            private static String name = "equipment statistic";

        #endregion

        #region properites

            #region overrides

                protected override LinkTypes LinkType { get { return LinkTypes.GridLink; } }

                protected override Dictionary<AdminPage, List<KeyValuePair<String, Object>>> GetLinksAndItemsToAdd()
                {
                    var defaultFields = new List<String>() { PropHelper.G<Building>(_ => _.BID), PropHelper.G<CW.Data.Equipment>(_ => _.EID) };
                    var adminPages = new List<AdminPage>() { AdminPage.AnalysesToEquipment, AdminPage.EquipmentToEquipment, AdminPage.EquipmentVariables, AdminPage.Points, AdminPage.ScheduledAnalyses };

                    return new AdminGridLinks(defaultFields, adminPages).Dict;
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get 
                    { 
                        return PropHelper.F<EquipmentAdministrationStatistics>(_ => _.BID,_ => _.EID, _ => _.EquipmentName, _ => _.EquipmentTypeName, _ => _.AssociatedEquipmentCount, _ => _.PointCount, _ => _.EquipmentVariablesCount, _ => _.AssignedAnalysesCount, _ => _.ScheduledAnalysesCount);
                    }
                }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    DateTime mStartDate, mEndDate;

                    if (ddlBuilding.SelectedValue == String.Empty) return Enumerable.Empty<EquipmentAdministrationStatistics>();

                    var bid = Convert.ToInt32(ddlBuilding.SelectedValue);

                    DateTimeHelper.GenerateDefaultDatesYesterday(out mStartDate, out mEndDate, siteUser.EarliestBuildingTimeZoneID);   

                    return DataMgr.EquipmentDataMapper.GetEquipmentAdministrationStatisticsByBID(siteUser.CID, bid, mStartDate, false, searchCriteria); 
                }

                protected override String NameField { get { return PropHelper.G<EquipmentAdministrationStatistics>(_ => _.EquipmentName); } }

                protected override String IdColumnName { get { return PropHelper.G<EquipmentAdministrationStatistics>(_ => _.EID); } }

                protected override String Name { get { return name; } }

                protected override Type Entity { get { return typeof(EquipmentAdministrationStatistics); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID }); } }

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[]
                        {
                            KGSEquipmentAdministration.TabMessages.AddEquipment,
                            KGSEquipmentAdministration.TabMessages.EditEquipment,
                            KGSEquipmentAdministration.TabMessages.EditEquipmentVariables,
                            KGSEquipmentAdministration.TabMessages.DeleteEquipment
                        };
                    }
                }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid, Int32 bid) { return new GridCacher.CacheKeyInfo(name + "." + typeof(EquipmentAdministrationStatistics).Name, String.Join("|", new String[] { cid.ToString(), bid.ToString() })); }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                BindBuildings(ddlBuilding);         
            }

            private void Page_Load()
            {
                IsDeferredLoading = (!Page.IsPostBack);
            }

            #region ddl events

                protected void ddlBuilding_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    var bid = Convert.ToInt32(ddlBuilding.SelectedValue);
                    var bidSelected = (bid != -1);

                    base.InitializeControl();

                    lblResults.Visible = bidSelected;
                    pnlSearch.Visible = bidSelected;
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();

                    ddlBuilding_SelectedIndexChanged(null, null);

                    base.InitializeControl();

                    lblResults.Visible = pnlSearch.Visible = false;
                }

            #endregion

            #region binders

                private void BindBuildings(DropDownList ddl)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

            #endregion

        #endregion
    }
}