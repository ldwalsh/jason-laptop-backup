﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="MoveEquipment.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.MoveEquipment" %>

<asp:Panel ID="pnlMoveEquipment" runat="server" DefaultButton="btnMoveEquipment">

<h2>Move Equipment</h2>

<p>Please select building and then a piece of equipment to move. Only allowed from a building to building within the same client.</p>
<p>Note:</p>
<ul>
    <li>Existing equipment image will be checked. Please delete first.</li>
    <li>Existing vdata will be checked. Please purge first.
    <li>Related equipment to equipment associations will automatically be DELETED.</li>
    <li>Projects will automatically be moved.</li>
    <li>Analysis Builder views will not checked. Please delete first. They should still function.</li>
    <li>Tasks will not need any updates for the move.</li>
    <li>Documents will not need any updates for the move.</li>
    <li>Raw data will not need any purging for the move.</li>
    <li>Scheduled Analyses will automatically be moved.</li>
    <li>Equipment will be automatically moved.</li>
    <li>The grid cache will not be cleared. Please view after moving rather than before.</li>
</ul> 
<br />   
<div>
  <a id="lnkSetFocus" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblEquipmentError" CssClass="errorMessage" runat="server" />
</div>
<div class="divForm">   
  <label class="label">*Current Building:</label>
  <asp:DropDownList ID="ddlBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
</div>
<div class="divForm">
  <label class="label">*Existing Equipment:</label>
  <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>
<div class="divForm">   
  <label class="label">*Move to Building:</label>
  <asp:DropDownList ID="ddlBuildingMove" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
</div>

<asp:LinkButton CssClass="lnkButton" ID="btnMoveEquipment" runat="server" Text="Move" OnClick="moveEquipmentButton_Click" ValidationGroup="MoveEquipment" />

<!--Ajax Validators-->
<asp:RequiredFieldValidator ID="moveBuildingRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="MoveEquipment" />
<ajaxToolkit:ValidatorCalloutExtender ID="moveBuildingVCE" runat="server" BehaviorID="moveBuildingVCE" TargetControlID="moveBuildingRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="moveEquipmentRFV" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="ddlEquipment" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="MoveEquipment" />
<ajaxToolkit:ValidatorCalloutExtender ID="moveEquipmentVCE" runat="server" BehaviorID="moveEquipmentVCE" TargetControlID="moveEquipmentRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<asp:RequiredFieldValidator ID="moveBuildingMoveRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBuildingMove" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="MoveEquipment" />
<ajaxToolkit:ValidatorCalloutExtender ID="moveBuildingMoveVCE" runat="server" BehaviorID="moveBuildingMoveVCE" TargetControlID="moveBuildingMoveRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>