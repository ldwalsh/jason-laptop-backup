﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="EquipmentVariablesToMultipleEquipment.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.EquipmentVariablesToMultipleEquipment" %>


<asp:Panel ID="pnlAssignEquipmentVariableToMultipleEquipment" runat="server" DefaultButton="btnAssignEquipmentVariableToMultipleEquipment">

<h2>Equipment Variable To Multiple Equipment</h2>
<p>Please select a building, equipment variable, and set a value in order to mass assign an equipment variable. Keep in mind that if an equipments class is not associated with that equipment variable then it cannot be assigned to the equipment.</p>
  

  <div>
    <a id="lnkVariablesSetFocus" runat="server"></a>
    <asp:Label ID="lblVariablesResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblVariablesError" CssClass="errorMessage" runat="server" />
  </div>                                    
  <div class="divForm">
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>  
  </div>   
  <div class="divForm">   
    <label class="label">*Select Building:</label>    
    <asp:DropDownList ID="ddlVariablesBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVariablesBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    <asp:RequiredFieldValidator ID="variablesBuildingsRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlVariablesBuildings" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Variables" />
    <ajaxToolkit:ValidatorCalloutExtender ID="variablesBuildingsRequiredValidatorExtender" runat="server" BehaviorID="variablesBuildingsRequiredValidatorExtender" TargetControlID="variablesBuildingsRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>  
  <div class="divForm">   
    <label class="label">*Select Equipment Variable:</label>    
    <asp:DropDownList ID="ddlVariablesEquipmentVariables" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVariablesEquipmentVariables_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    <asp:RequiredFieldValidator ID="variablesEquipmentVariablesRequiredValidator" runat="server" ErrorMessage="Equipment Variables is a required field." ControlToValidate="ddlVariablesEquipmentVariables" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Variables" />
    <ajaxToolkit:ValidatorCalloutExtender ID="variablesEquipmentVariablesRequiredValidatorExtender" runat="server" BehaviorID="variablesEquipmentVariablesRequiredValidatorExtender" TargetControlID="variablesEquipmentVariablesRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>                                  
  <div class="divForm">                                                                                                                  
    <label class="label">*Value</label>   
    <asp:TextBox CssClass="textbox" ID="txtVariablesValue" runat="server" MaxLength="25" />
    <span id="spanEngUnits" runat="server"></span>                                         
    <div id="info" runat="server"></div>                                         
    <p>(Note: Value set here will be set for the equipment variable on ALL selected equipment.)</p> 

    <!--filter is disabled if engineering unit is "STRING" or "ARRAY"-->
    <asp:RequiredFieldValidator ID="variablesValueRequiredValidator" runat="server" ErrorMessage="Value is a required field." ControlToValidate="txtVariablesValue" SetFocusOnError="true" Display="None" ValidationGroup="Variables" />
    <ajaxToolkit:ValidatorCalloutExtender ID="variablesValueRequiredValidatorExtender" runat="server" BehaviorID="variablesValueRequiredValidatorExtender" TargetControlID="variablesValueRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>
  <div class="divForm">                                                        
    <asp:CheckBox ID="chkVariablesOverride" CssClass="checkbox" runat="server" /> 
    <span>Override value if variable already exists for equipment.</span>
  </div>  
  <div class="divForm">   
    <label class="label">Select Equipment Type:</label>
    <asp:DropDownList ID="ddlVariablesEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVariablesEquipmentType_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />    
  </div>
  <div class="divForm">
    <label class="label">Available Equipment:</label> 
    <div class="divListSearchExtender">
      <ajaxToolkit:ListSearchExtender id="lseVariablesEquipmentTop" runat="server" TargetControlID="lbVariablesEquipmentTop" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true" />   
    </div>

    <asp:ListBox ID="lbVariablesEquipmentTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    <div class="divArrows">
      <asp:ImageButton CssClass="up-arrow"  ID="ImageButton1" CausesValidation="false" OnClick="btnVariablesEquipmentUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
      <asp:ImageButton CssClass="down-arrow" ID="ImageButton2" CausesValidation="false" OnClick="btnVariablesEquipmentDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
    </div>

    <label class="label">*Selected Equipment:</label> 
    <asp:ListBox ID="lbVariablesEquipmentBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />                                                        

  </div>

  <asp:LinkButton CssClass="lnkButton" ID="btnAssignEquipmentVariableToMultipleEquipment" runat="server" Text="Assign"  OnClick="assignEquipmentVariableToMultipleEquipmentButton_Click" ValidationGroup="Variables" />

</asp:Panel>

<hr />

<asp:Panel ID="pnlUnassignEquipmentVariableFromMultipleEquipment" runat="server" DefaultButton="btnUnassignEquipmentVariableFromMultipleEquipment">
 
<h2>Unassign Equipment Variable From Multiple Equipment</h2>
<p>Please select a building, equipment variable, and equipment in order to mass unassign an equipment variable.</p>
  
  <div>
    <a id="lnkUnassignSetFocus" runat="server"></a>
    <asp:Label ID="lblUnassignResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblUnassignError" CssClass="errorMessage" runat="server" />
  </div>                                    
  <div class="divForm">
    <label class="label">Client:</label>
    <label class="labelContent"><%= siteUser.ClientName %></label>  
  </div>   
  <div class="divForm">   
    <label class="label">*Select Building:</label>    
    <asp:DropDownList ID="ddlUnassignBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlUnassignBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    <asp:RequiredFieldValidator ID="unassignBuildingsRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlUnassignBuildings" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Unassign" />
    <ajaxToolkit:ValidatorCalloutExtender ID="unassignBuildingsRequiredValidatorExtender" runat="server" BehaviorID="unassignBuildingsRequiredValidatorExtender" TargetControlID="unassignBuildingsRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>  
  <div class="divForm">   
    <label class="label">*Select Equipment Variable:</label>    
    <asp:DropDownList ID="ddlUnassignEquipmentVariables" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlUnassignEquipmentVariables_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    <asp:RequiredFieldValidator ID="unassignEquipmentVariablesRequiredValidator" runat="server" ErrorMessage="Equipment Variables is a required field." ControlToValidate="ddlUnassignEquipmentVariables" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="Unassign" />
    <ajaxToolkit:ValidatorCalloutExtender ID="unassignEquipmentVariablesRequiredValidatorExtender" runat="server" BehaviorID="unassignEquipmentVariablesRequiredValidatorExtender" TargetControlID="unassignEquipmentVariablesRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  </div>                                  
  <div class="divForm">
    <label class="label">Associated Equipment:</label> 
    <div class="divListSearchExtender">
      <ajaxToolkit:ListSearchExtender id="lseUnassignEquipmentTop" runat="server" TargetControlID="lbUnassignEquipmentTop" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true" />   
    </div>

    <asp:ListBox ID="lbUnassignEquipmentTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    <div class="divArrows">
      <asp:ImageButton CssClass="up-arrow"  ID="ImageButton3" CausesValidation="false" OnClick="btnUnassignEquipmentUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
      <asp:ImageButton CssClass="down-arrow" ID="ImageButton4" CausesValidation="false" OnClick="btnUnassignEquipmentDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
    </div>

    <label class="label">*Selected Associations To Unassign:</label> 
    <asp:ListBox ID="lbUnassignEquipmentBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />                                                        

  </div>

  <asp:LinkButton CssClass="lnkButton" ID="btnUnassignEquipmentVariableFromMultipleEquipment" runat="server" Text="Unassign"  OnClick="unassignEquipmentVariableFromMultipleEquipmentButton_Click" ValidationGroup="Unassign" />

</asp:Panel>
