﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class EquipmentVariablesToMultipleEquipment : AdminUserControlBase
    {
        #region constants

            const String assignEquipmentVariablesNoEquipmentSelected = "No equipment was selected.";
            const String assignEquipmentVariablesFailed = "Equipment variable assignment to multiple equipment failed. Please contact an administrator.";
            const String unassignEquipmentVariablesNoAssociationsSelected = "No associations were selected.";
            const String unassignEquipmentVariablesFailed = "Equipment variable deletion from multiple equipment failed. Please contact an administrator.";            
            const String equipmentVariableDoesNotExistForEquipment = "Equipment variable {0} could not be unassigned from equipment {0} because it does not exist or was already removed.";


        #endregion

        #region properites

            public override IEnumerable<Enum> ReactToChangeStateList
            {
                get
                {
                    return new Enum[]
                    {
                        KGSEquipmentAdministration.TabMessages.AddEquipment,
                        KGSEquipmentAdministration.TabMessages.EditEquipment,
                        KGSEquipmentAdministration.TabMessages.DeleteEquipment
                    };
                }
            }

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblVariablesResults.Visible = lblVariablesError.Visible = false;
                lblUnassignResults.Visible = lblUnassignError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindEquipmentVariables(ddlVariablesEquipmentVariables, ddlUnassignEquipmentVariables);
                    BindBuildings(ddlVariablesBuildings, ddlUnassignBuildings);
                    BindEquipmentVariablesEquipmentTypes(ddlVariablesEquipmentType, -1);
                }
            }

            #region ddl events

                protected void ddlVariablesEquipmentVariables_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    SetValueInfo(Convert.ToInt32(ddlVariablesBuildings.SelectedValue), Convert.ToInt32(ddlVariablesEquipmentVariables.SelectedValue));
                }

                protected void ddlVariablesBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    ClearVariablesEquipmentBoxes();

                    int bid = Convert.ToInt32(ddlVariablesBuildings.SelectedValue);
                    BindVariablesEquipmentListBox(lbVariablesEquipmentTop, bid);
                    if (bid > 0)
                    {
                        var evid = Convert.ToInt32(ddlVariablesEquipmentVariables.SelectedValue);
                        if (evid > 0)
                        {
                            SetValueInfo(bid, evid);
                        }
                    }
                    BindEquipmentVariablesEquipmentTypes(ddlVariablesEquipmentType, Convert.ToInt32(ddlVariablesBuildings.SelectedValue));
                }

                protected void ddlVariablesEquipmentType_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    ClearVariablesEquipmentBoxes();

                    BindEquipmentListBoxByBIDAndEquipmentTypeID(lbVariablesEquipmentTop, Convert.ToInt32(ddlVariablesBuildings.SelectedValue), Convert.ToInt32(ddlVariablesEquipmentType.SelectedValue));
                }

                protected void ddlUnassignEquipmentVariables_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    ClearUnassignEquipmentBoxes();

                    BindUnassignEquipmentListBox(lbUnassignEquipmentTop, Convert.ToInt32(ddlUnassignEquipmentVariables.SelectedValue), Convert.ToInt32(ddlUnassignBuildings.SelectedValue));                 
                }

                protected void ddlUnassignBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    ClearUnassignEquipmentBoxes();

                    BindUnassignEquipmentListBox(lbUnassignEquipmentTop, Convert.ToInt32(ddlUnassignEquipmentVariables.SelectedValue), Convert.ToInt32(ddlUnassignBuildings.SelectedValue));                 
                }

            #endregion

            #region button events

                protected void btnVariablesEquipmentUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbVariablesEquipmentBottom.SelectedIndex != -1)
                    {
                        lbVariablesEquipmentTop.Items.Add(lbVariablesEquipmentBottom.SelectedItem);
                        lbVariablesEquipmentBottom.Items.Remove(lbVariablesEquipmentBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbVariablesEquipmentTop);
                }

                protected void btnVariablesEquipmentDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbVariablesEquipmentTop.SelectedIndex != -1)
                    {
                        lbVariablesEquipmentBottom.Items.Add(lbVariablesEquipmentTop.SelectedItem);
                        lbVariablesEquipmentTop.Items.Remove(lbVariablesEquipmentTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbVariablesEquipmentBottom);
                }

                protected void assignEquipmentVariableToMultipleEquipmentButton_Click(Object sender, EventArgs e)
                {
                    try
                    {
                        if (lbVariablesEquipmentBottom.Items.Count == 0)
                        {
                            LabelHelper.SetLabelMessage(lblVariablesError, assignEquipmentVariablesNoEquipmentSelected, lnkVariablesSetFocus);
                            return;
                        }
                        else
                        {
                            var evid = Convert.ToInt32(ddlVariablesEquipmentVariables.SelectedValue);
                            var equipmentVariableName = ddlVariablesEquipmentVariables.SelectedItem.Text;                            
                             
                            lblVariablesError.Text = String.Empty;
                            lblVariablesResults.Text = String.Empty;

                            var buildingID = Convert.ToInt32(ddlVariablesBuildings.SelectedValue);
                            BuildingSetting buildingSettings = DataMgr.BuildingDataMapper.GetBuildingSettingsByBID(buildingID);
                            CW.Data.Models.EquipmentVariable.GetEquipmentVariableData evData = DataMgr.EquipmentVariableDataMapper.GetFullEquipmentVariableByEVID(evid);
                            
                            foreach (ListItem item in lbVariablesEquipmentBottom.Items)
                            {
                                var mEquipmentVariable = new Equipment_EquipmentVariable();
                                var eid = Convert.ToInt32(item.Value);
                                mEquipmentVariable.EngUnitID = buildingSettings.UnitSystem ? evData.SIEngUnitID : evData.IPEngUnitID; // For insert only; used for validation

                                //check if equipment variable can be assigned, meaning if the equipment variable is assigned to the equipment class for that equipment.
                                if (!DataMgr.EquipmentVariableDataMapper.IsEquipmentVariableAssociatedWithAnEquipmentsEquipmentClass(evid, eid))
                                    lblVariablesError.Text += equipmentVariableName + " cannot be assigned to equipment " + item.Text + " because it is not allowed to be assigned to equipment of that class.<br />";
                                else
                                {
                                    if (chkVariablesOverride.Checked)
                                    {
                                        var equipmentVariable = DataMgr.EquipmentVariableDataMapper.GetEquipmentVariableByEIDAndEVID(eid, evid);

                                        if (equipmentVariable != null)
                                        {
                                            var value = txtVariablesValue.Text;

                                            try
                                            {
                                                DataMgr.EquipmentVariableDataMapper.UpdatePartialEquipmentEquipmentVariable(equipmentVariable.ID, value);
                                                lblVariablesResults.Text += "The value for " + equipmentVariableName + " has been updated to " + value + "<br />";
                                            }
                                            catch (CW.Data.Exceptions.ValidationException ex)
                                            {
                                                lblVariablesResults.Text += (ex.Message + "<br />");
                                            }
                                        }
                                        else
                                        {
                                            mEquipmentVariable.EID = eid;
                                            mEquipmentVariable.EVID = evid;
                                            mEquipmentVariable.Value = txtVariablesValue.Text;
                                            mEquipmentVariable.DateModified = DateTime.UtcNow;                                            

                                            try
                                            {
                                                DataMgr.EquipmentVariableDataMapper.InsertEquipmentEquipmentVariable(mEquipmentVariable);
                                                lblVariablesResults.Text += equipmentVariableName + " has been assigned to " + item.Text + "<br />";
                                            }
                                            catch (CW.Data.Exceptions.ValidationException ex)
                                            {
                                                lblVariablesResults.Text += (ex.Message + "<br />");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableExistForEquipment(eid, evid))
                                            lblVariablesError.Text += equipmentVariableName + " already assigned to " + item.Text + "<br />";
                                        else
                                        {
                                            mEquipmentVariable.EID = eid;
                                            mEquipmentVariable.EVID = evid;
                                            mEquipmentVariable.Value = txtVariablesValue.Text;
                                            mEquipmentVariable.DateModified = DateTime.UtcNow;

                                            try
                                            {
                                                DataMgr.EquipmentVariableDataMapper.InsertEquipmentEquipmentVariable(mEquipmentVariable);
                                                lblVariablesResults.Text += equipmentVariableName + " has been assigned to " + item.Text + "<br />";
                                            }
                                            catch (CW.Data.Exceptions.ValidationException ex)
                                            {
                                                lblVariablesResults.Text += (ex.Message + "<br />");
                                            }
                                        }
                                    }
                                }
                            }

                            if (lblVariablesError.Text != String.Empty) LabelHelper.SetLabelMessage(lblVariablesError, lblVariablesError.Text, lnkVariablesSetFocus);
                            if (lblVariablesResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblVariablesResults, lblVariablesResults.Text, lnkVariablesSetFocus);
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblVariablesError, assignEquipmentVariablesFailed, lnkVariablesSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning equipment variable to multiple equipment.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblVariablesError, assignEquipmentVariablesFailed, lnkVariablesSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning equipment variable to multiple equipment.", ex);
                    }
                }

                protected void btnUnassignEquipmentUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbUnassignEquipmentBottom.SelectedIndex != -1)
                    {
                        lbUnassignEquipmentTop.Items.Add(lbUnassignEquipmentBottom.SelectedItem);
                        lbUnassignEquipmentBottom.Items.Remove(lbUnassignEquipmentBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbUnassignEquipmentTop);
                }

                protected void btnUnassignEquipmentDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbUnassignEquipmentTop.SelectedIndex != -1)
                    {
                        lbUnassignEquipmentBottom.Items.Add(lbUnassignEquipmentTop.SelectedItem);
                        lbUnassignEquipmentTop.Items.Remove(lbUnassignEquipmentTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbUnassignEquipmentBottom);
                }

                protected void unassignEquipmentVariableFromMultipleEquipmentButton_Click(Object sender, EventArgs e)
                {
                    try
                    {
                        if (lbUnassignEquipmentBottom.Items.Count == 0)
                        {
                            LabelHelper.SetLabelMessage(lblUnassignError, unassignEquipmentVariablesNoAssociationsSelected, lnkUnassignSetFocus);
                            return;
                        }
                        else
                        {
                            var evid = Convert.ToInt32(ddlUnassignEquipmentVariables.SelectedValue);
                            var equipmentVariableName = ddlUnassignEquipmentVariables.SelectedItem.Text;

                            lblUnassignResults.Text = String.Empty;

                            foreach (ListItem item in lbUnassignEquipmentBottom.Items)
                            {
                                var eid = Convert.ToInt32(item.Value);

                                if (DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableExistForEquipment(eid, evid))
                                {
                                    DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariableByEIDAndEVID(eid, evid);
                                    lblUnassignResults.Text += equipmentVariableName + " has been unassigned from " + item.Text + "<br />";
                                }
                                else
                                {
                                    lblUnassignError.Text += String.Format(equipmentVariableDoesNotExistForEquipment, equipmentVariableName, item.Text) + "<br />";
                                }
                            }

                            if (lblUnassignError.Text != String.Empty) LabelHelper.SetLabelMessage(lblUnassignError, lblUnassignError.Text, lnkUnassignSetFocus);
                            if (lblUnassignResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblUnassignResults, lblUnassignResults.Text, lnkUnassignSetFocus);
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblUnassignError, unassignEquipmentVariablesFailed, lnkUnassignSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error unassigning equipment variable from multiple equipment.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblUnassignError, unassignEquipmentVariablesFailed, lnkUnassignSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error unassigning equipment variable from multiple equipment.", ex);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    var adminUserControlForm = new AdminUserControlForm(this);

                    adminUserControlForm.ClearTextBoxes();
                    adminUserControlForm.ClearCheckBoxes();
                    adminUserControlForm.ResetDropDownLists();

                    //hardset any fields to their initial values
                    spanEngUnits.InnerText = String.Empty;
                    ddlVariablesBuildings_OnSelectedIndexChanged(null, null);

                    ClearVariablesEquipmentBoxes();
                    ClearUnassignEquipmentBoxes();
                }

            #endregion

            private void ClearVariablesEquipmentBoxes()
            {
                lbVariablesEquipmentTop.Items.Clear();
                lbVariablesEquipmentBottom.Items.Clear();
            }

            private void ClearUnassignEquipmentBoxes()
            {
                lbUnassignEquipmentTop.Items.Clear();
                lbUnassignEquipmentBottom.Items.Clear();
            }

            #region binders

                private void BindEquipmentVariables(DropDownList ddl, DropDownList ddl2)
                {
                    var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

                    var vars = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariables();

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    ddl.DataTextField = "EquipmentVariableDisplayName";
                    ddl.DataValueField = "EVID";
                    ddl.DataSource = vars;
                    ddl.DataBind();

                    ddl2.Items.Clear();
                    ddl2.Items.Add(lItem);

                    ddl2.DataTextField = "EquipmentVariableDisplayName";
                    ddl2.DataValueField = "EVID";
                    ddl2.DataSource = vars;
                    ddl2.DataBind();
                }

                private void BindBuildings(DropDownList ddl, DropDownList ddl2)
                {
                    var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    ddl.DataTextField = "BuildingName";
                    ddl.DataValueField = "BID";
                    ddl.DataSource = siteUser.VisibleBuildings;
                    ddl.DataBind();

                    ddl2.Items.Clear();
                    ddl2.Items.Add(lItem);

                    ddl2.DataTextField = "BuildingName";
                    ddl2.DataValueField = "BID";
                    ddl2.DataSource = siteUser.VisibleBuildings;
                    ddl2.DataBind();
                }

                private void BindEquipmentVariablesEquipmentTypes(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = "View All...", Value = "-1", Selected = true };

                    ddl.Items.Clear();
                    ddl.Items.Add(lItem);

                    ddl.DataTextField = "EquipmentTypeName";
                    ddl.DataValueField = "EquipmentTypeID";
                    ddl.DataSource = (bid != -1) ? DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByBuildingID(bid) : DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypes();
                    ddl.DataBind();
                }

                private void BindVariablesEquipmentListBox(ListBox lb, Int32 bid)
                {
                    lb.Items.Clear();

                    var mEquipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid);

                    if (mEquipment.Any())
                    {
                        lb.DataSource = mEquipment;
                        lb.DataTextField = "EquipmentName";
                        lb.DataValueField = "EID";
                        lb.DataBind();
                    }
                }

                private void BindEquipmentListBoxByBIDAndEquipmentTypeID(ListBox lb, Int32 bid, Int32 equipmentTypeId)
                {
                    lb.Items.Clear();

                    var mEquipment = (equipmentTypeId != -1) ? DataMgr.EquipmentDataMapper.GetAllEquipmentByBIDAndEquipmentTypeID(bid, equipmentTypeId) : DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid);

                    if (mEquipment.Any())
                    {
                        lb.DataSource = mEquipment;
                        lb.DataTextField = "EquipmentName";
                        lb.DataValueField = "EID";
                        lb.DataBind();
                    }
                }

                private void BindUnassignEquipmentListBox(ListBox lb, Int32 evid, Int32 bid)
                {
                    lb.Items.Clear();

                    var mEquipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByBIDAndEVID(bid, evid);

                    if (mEquipment.Any())
                    {
                        lb.DataSource = mEquipment;
                        lb.DataTextField = "EquipmentName";
                        lb.DataValueField = "EID";
                        lb.DataBind();
                    }
                }


            #endregion

            private void SetValueInfo(int buildingID, int equipmentVariableID )
            {
                bool ok = false;
                if (buildingID > 0 && equipmentVariableID > 0)
                {
                    var equipmentVariable = DataMgr.EquipmentVariableDataMapper.GetFullEquipmentVariableByEVID(equipmentVariableID);

                    if (equipmentVariable != null && equipmentVariable.EVID != 0)
                    {
                        BuildingSetting buildingSettings = DataMgr.BuildingDataMapper.GetBuildingSettingsByBID(buildingID);
                        spanEngUnits.InnerText = buildingSettings.UnitSystem ? equipmentVariable.SIEngUnits : equipmentVariable.IPEngUnits;
                        txtVariablesValue.Text = buildingSettings.UnitSystem ? equipmentVariable.SIDefaultValue : equipmentVariable.IPDefaultValue;  // Already in culture format
                        int engUnitId = buildingSettings.UnitSystem ? equipmentVariable.SIEngUnitID : equipmentVariable.IPEngUnitID;
                        info.InnerHtml = BaseVariable.Info(buildingSettings.UnitSystem, engUnitId, txtVariablesValue.Text);
                        ControlHelper.SetTextBoxSingleMultiple(engUnitId, txtVariablesValue);
                        ok = true;
                    }
                }
                if (!ok)
                {
                    info.InnerHtml = String.Empty;
                    txtVariablesValue.Text = String.Empty;
                    spanEngUnits.InnerText = String.Empty;
                }
            }
        #endregion
    }
}