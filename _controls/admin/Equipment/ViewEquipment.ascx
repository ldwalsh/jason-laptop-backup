﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViewEquipment.ascx.cs" Inherits="CW.Website._controls.admin.Equipment.ViewEquipment" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register TagPrefix="CW" Tagname="DropDowns" Src="~/_controls/DropDowns.ascx" %>
<%@ Register TagPrefix="CW" Namespace="FileUploaderControl" Assembly="FileUploaderControl" %>
<%@ Register TagPrefix="CW" TagName="ScheduleDownloadArea" Src="~/_administration/equipment/nonkgs/ScheduleDownloadArea.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Data.Models.Equipment" %>
<%@ Import Namespace="CW.Utility" %>

<script type="text/javascript" src="../../../_assets/scripts/admin.js"></script>

<div style="float:left;">

  <h2 id="H1" runat="server">View Equipment</h2>

  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</div>

<div style="float:right;">
  <CW:ScheduleDownloadArea runat="server" id="scheduleDownloadArea" Visible="false" />
</div>

<div style="clear:both;"></div>

<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div> 
<div class="divForm">
  <asp:Label CssClass="labelContent" ID="lblClientEquipmentCount" runat="server" />
</div>

<CW:DropDowns runat="server" ID="GridDropDowns" validationEnabled="true" ValidationGroup="Search" StartViewMode="Building" ViewMode="EquipmentClass" OnBuildingChanged="GridDropDowns_BuildingChanged" OnEquipmentClassChanged="GridDropDowns_EquipmentClassChanged" />
<br/>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
</asp:Panel>
                                                                                                                                                        
<div id="gridTbl">                                        
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" /> 
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkEdit" Runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentName" HeaderText="Equipment Name">                                                      
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.EquipmentName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.EquipmentName), 25) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingName" HeaderText="Building Name">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.BuildingName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.BuildingName), 25) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentTypeName" HeaderText="Equipment Type Name">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.EquipmentTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.EquipmentTypeName), 25) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ManufacturerModelName" HeaderText="Manufacturer Model">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.ManufacturerModelName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.ManufacturerModelName), 25) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.IsActive)) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="IsVisible" HeaderText="Visible">  
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.IsVisible)) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>
          <asp:HyperLink ID="hlConnect" NavigateUrl='<%# "/Connect.aspx?eid=" + GetCellContent(Container, PropHelper.G<GetEquipmentData>(_ => _.EID)) %>' runat="server" Target="_blank" Text="Connect" />
        </ItemTemplate>
      </asp:TemplateField> 

      <asp:TemplateField>
        <ItemTemplate>
          <asp:LinkButton ID="lnkDelete" Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this equipment permanently?\n\nAlternatively you can deactivate equipment temporarily instead.');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField HeaderText="Actions">
        <ItemTemplate>
          <select id="ddlAction" runat="server" class="dropdownArrowOnly" />
        </ItemTemplate>
      </asp:TemplateField>
    </Columns>        
  </asp:GridView>
</div>                                    
<br />
<br /> 

<div>     
  <!--SELECT EQUIPMENT DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptEquipmentDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Equipment Details</h2>
          <ul id="equipmentDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

      <asp:Repeater ID="rptEquipmentServiceDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <br />
          <h2>Equipment Service Details</h2>
          <ul id="equipmentServiceDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

      <asp:Repeater ID="rptAdditionalEquipmentDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <br />
          <h2>Additional Equipment Details</h2>
          <ul id="additionalEquipmentDetails" class="detailsListWidest">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>                                                                                        
</div>                                     
                                                                     
<!--EDIT EQUIPMENT PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateEquipment">
  <div>
    <h2>Edit Equipment</h2>
  </div>

  <div>
    <a id="lnkSetFocusEdit" runat="server"></a> 
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />                                                                   
    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*Equipment Name:</label>
      <asp:TextBox ID="txtEditEquipmentName" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>                                                          
    </div>   
    <div class="divForm">
      <label class="label">*Building:</label>
      <asp:DropDownList Enabled="false" CssClass="dropdown" ID="ddlEditBuilding" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Equipment Class:</label>    
      <asp:DropDownList ID="ddlEditEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
      <p>(Warning: Changing the class will result in the unreversable removal of all existing associated equipment variables not associated with the changed equipment class. This is because variables are available to an equipment based on the associated variables of an equipment class.)</p>
      <p>(Note: Changing the class will clear the selected Manufacturer and Model.)</p>
      <p>(Note: You cannot change an equipment class if an assigned analysis has ANY required equipment variable regardless if the equipment and new class have them.)</p>
      <p>(TODO: Allow change if the new equipment class has all required equipment variables for all assigned analyses.)</p>
    </div>                                                                                                         
    <div class="divForm">   
      <label class="label">*Equipment Type:</label>    
      <asp:DropDownList ID="ddlEditEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
        <p>(Warning: Changing the type will result in the unreversable removal of all existing associated equipment, and the assocation of this equipment to others.)</p>
    </div>
    <div class="divForm">   
      <label class="label">*Manufacturer:</label>    
      <asp:DropDownList ID="ddlEditManufacturer" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditManufacturer_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Model:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlEditManufacturerModel" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Serial Number:</label>
      <asp:TextBox ID="txtEditSerialNumber" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Location:</label>
      <textarea name="txtEditEquipmentLocation" id="txtEditEquipmentLocation" cols="40" rows="5" onkeyup="limitChars(this, 250, 'divEditEquipmentLocationCharInfo')" runat="server" />
      <div id="divEditEquipmentLocationCharInfo"></div>                                                         
    </div> 
    <div class="divForm">
      <label class="label">Description:</label>
      <textarea name="txtEditDescription" id="txtEditDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" runat="server" />
      <div id="divEditDescriptionCharInfo"></div>                                                         
    </div>

    <!--NESTED UPDATE PANEL NEEDED FOR FILE UPLOADER TRIGGERS-->    
    <asp:UpdatePanel ID="updatePanelNestedUpdate" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">
      <ContentTemplate>                                                      
        <div class="divForm">
          <label class="label">Image (800x600):</label>
          <CW:FileUploader runat="server" ID="fileUploaderEdit"/>
        </div>
      </ContentTemplate>
    </asp:UpdatePanel>

    <div class="divForm">
      <label class="label">Primary Manufacturer Contact:</label>    
      <asp:DropDownList ID="ddlEditPrimaryContact" AppendDataBoundItems="true" CssClass="dropdown" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Secondary Manufacturer Contact:</label>    
      <asp:DropDownList ID="ddlEditSecondaryContact" AppendDataBoundItems="true" CssClass="dropdown" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Equipment Notes (Max HTML Characters = 5000):</label>
      <telerik:RadEditor ID="editorEditEquipmentNotes" runat="server" CssClass="editorNarrow" Height="275px" Width="462px" MaxHtmlLength="5000" NewLineMode="Div" ToolsWidth="464px" ToolbarMode="ShowOnFocus" ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" EditModes="Design,Preview">
        <CssFiles>
          <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
        </CssFiles>                                                                                                                             
      </telerik:RadEditor>                                                         
    </div>
    <div class="divForm">
      <label class="label">*Active:</label>
      <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                        
      <p>(Warning: Setting an equipment as inactive will not allow scheduled analyses to be put in the queue, and no incoming data will be recorded.)</p>
    </div>
    <div class="divForm">
      <label class="label">*Visible:</label>
      <asp:CheckBox ID="chkEditVisible" CssClass="checkbox" runat="server" />                                                        
      <p>(Warning: Setting an equipment as not visible will hide the equipment on non administrative pages.)</p>
    </div>                                           
    <hr />

    <h2>Edit Equipment Serviced Information</h2>
                                                                                     
    <div class="divFormNarrow">
      <label class="labelWide">Last Serviced Date:</label>
      <telerik:RadDatePicker ID="txtEditLastServicedDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>      
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">Last Serviced By:</label>
      <asp:TextBox ID="txtEditLastServicedBy" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">Last Serviced Company:</label>
      <asp:TextBox ID="txtEditLastServicedCompany" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <hr />

    <h2>Edit Additional Equipment Information</h2>
                                 
    <div class="divFormNarrow">
      <label class="labelWide">External Equipment Type Name:</label>
      <asp:TextBox ID="txtEditExternalEquipmentTypeName" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">CMMS (Computerized Maintenance Mgmt System) Reference/Asset ID:</label>
      <asp:TextBox ID="txtEditCMMSReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">CMMS (Computerized Maintenance Management System) Location ID:</label>
      <asp:TextBox ID="txtEditCMMSLocationID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">CMMS (Computerized Maintenance Management System) Site ID:</label>
      <asp:TextBox ID="txtEditCMMSSiteID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">CMMS (Computerized Maintenance Management System) Link:</label>
      <asp:TextBox ID="txtEditCMMSLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">BIM (Building Information Management) Reference ID:</label>
      <asp:TextBox ID="txtEditBIMReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">BIM (Building Information Management) Link:</label>
      <asp:TextBox ID="txtEditBIMLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">DMS (Document Management Software) Reference ID:</label>
      <asp:TextBox ID="txtEditDMSReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">DMS (Document Management Software) Link:</label>
      <asp:TextBox ID="txtEditDMSLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">GIS (Geographical Information System) Reference ID:</label>
      <asp:TextBox ID="txtEditGISReferenceID" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divFormNarrow">
      <label class="labelWide">GIS (Geographical Information System) Link:</label>
      <asp:TextBox ID="txtEditGISLink" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
                                                                                                          
  </div>
                                                
  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="editEquipmentNameRFV" runat="server" ErrorMessage="Equipment Name is a required field." ControlToValidate="txtEditEquipmentName" SetFocusOnError="true" Display="None" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editEquipmentNameVCE" runat="server" BehaviorID="editEquipmentNameVCE" TargetControlID="editEquipmentNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editBuildingRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlEditBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingVCE" runat="server" BehaviorID="editBuildingVCE" TargetControlID="editBuildingRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editEquipmentClassRFV" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlEditEquipmentClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editEquipmentClassVCE" runat="server" BehaviorID="editEquipmentClassVCE" TargetControlID="editEquipmentClassRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editEquipmentTypRFV" runat="server" ErrorMessage="Equipment Type is a required field." ControlToValidate="ddlEditEquipmentType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editEquipmentTypeVCE" runat="server" BehaviorID="editEquipmentTypeVCE" TargetControlID="editEquipmentTypRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                   
  <asp:RequiredFieldValidator ID="editManufacturerRFV" runat="server" ErrorMessage="Manufacturer is a required field." ControlToValidate="ddlEditManufacturer" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editManufacturerVCE" runat="server" BehaviorID="editManufacturerVCE" TargetControlID="editManufacturerRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editManufacturerModelRFV" runat="server" ErrorMessage="Model is a required field." ControlToValidate="ddlEditManufacturerModel" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editManufacturerModelVCE" runat="server" BehaviorID="editManufacturerModelVCE" TargetControlID="editManufacturerModelRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                     
  <asp:RegularExpressionValidator ID="editCMMSLinkREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtEditCMMSLink" SetFocusOnError="true" Display="None" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editCMMSLinkVCE" runat="server" BehaviorID="editCMMSLinkVCE" TargetControlID="editCMMSLinkREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
       
  <asp:RegularExpressionValidator ID="editBIMLinkREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtEditBIMLink" SetFocusOnError="true" Display="None" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editBIMLinkVCE" runat="server" BehaviorID="editBIMLinkVCE" TargetControlID="editBIMLinkREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RegularExpressionValidator ID="editDMSLinkREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtEditDMSLink" SetFocusOnError="true" Display="None" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editDMSLinkVCE" runat="server" BehaviorID="editDMSLinkVCE" TargetControlID="editDMSLinkREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
      
  <asp:RegularExpressionValidator ID="editGISLinkREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtEditGISLink" SetFocusOnError="true" Display="None" ValidationGroup="EditEquipment" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editGISLinkVCE" runat="server" BehaviorID="editGISLinkVCE" TargetControlID="editGISLinkREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipment" runat="server" Text="Update Equipment"  OnClick="updateEquipmentButton_Click" ValidationGroup="EditEquipment" />
  
</asp:Panel>