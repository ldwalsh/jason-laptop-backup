﻿using CW.Business.Blob.Images;
using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Equipment;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Equipment
{
    public partial class ViewEquipment : AdminUserControlGridAdminLinks
    {
        #region constants

            const String noEquipmentImage = "no-equipment-image.png";
            const String pointsExist = "Cannot delete equipment because points are associated. Please unassociate or delete points first.";
            const String vPointsExist = "Cannot delete equipment because vpoints are associated. Please check that all analyses have been unassigned. If this is a utility billing tenant, and no analyses are showing as assigned, try assigning the equipment vairables required for an analyses back on, and then unassigning the analysis.";
            const String filesExist = "Cannot delete equipment because files in the documents are associated. Please delete files first.";
            const String deleteSuccessful = "Equipment deletion was successful.";
            const String deleteFailed = "Equipment deletion failed. Please contact an administrator.";
            const String equipmentExists = "Equipment with that name already exists.";
            const String analysesAssignedInBuilding = "Cannot change building because analyses have been assigned to equipment in this building. Please unassign the analyses from the equipment first.";
            const String updateSuccessful = "Equipment update was successful.";
            const String updateFailed = "Equipment update failed. Please contact an administrator.";            
            const String unallowedPointTypes = "Cannot change type because point's point types are not allowed on changing equipment type.";
            const String unallowedAnalyses = "Cannot change type because existing assigned analyses are not allowed on changing equipment type.";

        #endregion

        #region fields

            private enum EquipmentDetails { Default, Service, Additional };

        #endregion

        #region properties

            #region overrides

                protected override LinkTypes LinkType { get { return LinkTypes.DropDownLink; } }

                protected override Dictionary<AdminPage, List<KeyValuePair<String, Object>>> GetLinksAndItemsToAdd()
                {
                    var defaultFields = new List<String>() { PropHelper.G<Building>(_ => _.BID), PropHelper.G<CW.Data.Equipment>(_ => _.EID) };
                    var adminPages = new List<AdminPage>() { AdminPage.EquipmentToEquipment, AdminPage.EquipmentVariables, AdminPage.Points, AdminPage.ScheduledAnalyses };

                    return new AdminGridLinks(defaultFields, adminPages).Dict;
                }

                protected override Boolean ContainsDropDownListControl { get { return true; } }

                protected override DropDowns DropDownListControl { get { return GridDropDowns; } }

                protected override String CurrentControlName { get { return typeof(ViewEquipment).Name; } }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    var equipment = new QueryManager(DataMgr.ConnectionString, null).Equipment.LoadWith(_ => _.Building).LoadWith<EquipmentType>(_ => _.EquipmentType, _ => _.EquipmentClass).LoadWith(_ => _.EquipmentManufacturerModel).FinalizeLoadWith.GetAll(siteUser.CID).ByBuilding(GridDropDowns.BID);

                    return equipment.Queryable.Select(e => new GetEquipmentData
                    {
                        EID = e.EID,
                        BID = e.BID,
                        EquipmentClassID = e.EquipmentType.EquipmentClassID,
                        EquipmentName = e.EquipmentName,
                        BuildingName = e.Building.BuildingName,
                        EquipmentTypeName = e.EquipmentType.EquipmentTypeName,
                        ManufacturerModelName = e.EquipmentManufacturerModel.ManufacturerModelName,
                        IsActive = e.IsActive,
                        IsVisible = e.IsVisible
                    }).ToList();
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetEquipmentData>(_ => _.EID, _ => _.BID, _ => _.EquipmentClassID, _ => _.EquipmentName, _ => _.BuildingName, _ => _.EquipmentTypeName, _ => _.ManufacturerModelName, _ => _.IsActive, _ => _.IsVisible); }
                }

                protected override String NameField { get { return PropHelper.G<GetEquipmentData>(_ => _.EquipmentName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetEquipmentData>(_ => _.EID); } }

                protected override String Name { get { return typeof(CW.Data.Equipment).Name; } }

                protected override Type Entity { get { return typeof(GetEquipmentData); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID, GridDropDowns.BID }); } }

                public override IEnumerable<Enum> ReactToChangeStateList { get { return new Enum[] { KGSEquipmentAdministration.TabMessages.AddEquipment }; } }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid, Int32 bid) { return new GridCacher.CacheKeyInfo(typeof(CW.Data.Equipment).Name + "." + typeof(GetEquipmentData).Name, String.Join("|", new String[] { cid.ToString(), bid.ToString() })); }

            public String equipmentImageUrl { get; set; }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                lblClientEquipmentCount.Text = ("Client Total Equipment Count: " + DataMgr.EquipmentDataMapper.GetEquipmentCountByCID(siteUser.CID));
            }

            private void Page_Load()
            {
                scheduleDownloadArea.BuildingDropDown = GridDropDowns.GetDDL(DropDowns.ViewModeEnum.Building);
            }

            #region grid events

                #region overrides

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var eid = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());
                        var mEquipment = DataMgr.EquipmentDataMapper.GetEquipmentByEID(eid, true, true, false, true, true, true);

                        BindBuildings(ddlEditBuilding, mEquipment.Building.CID);
                        BindEquipmentClasses(ddlEditEquipmentClass);
                        BindEquipmentTypes(ddlEditEquipmentType, mEquipment.EquipmentType.EquipmentClassID);
                        BindManufacturers(ddlEditManufacturer, ddlEditManufacturerModel, mEquipment.EquipmentType.EquipmentClassID);
                        BindManufacturerModels(ddlEditManufacturerModel, mEquipment.EquipmentManufacturerModel.ManufacturerID);
                        BindManufacturerContacts(ddlEditPrimaryContact, mEquipment.EquipmentManufacturerModel.ManufacturerID);
                        BindManufacturerContacts(ddlEditSecondaryContact, mEquipment.EquipmentManufacturerModel.ManufacturerID);

                        SetEquipmentIntoEditForm(mEquipment);

                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                    protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
                    {
                        var eid = Convert.ToInt32(grid.DataKeys[e.RowIndex].Value);

                        try
                        {
                            //Only allow deletion of a Equipment if no points have been associated (active or not doesnt matter) and no files in the documents are under this equipment
                            var hasPoints = DataMgr.EquipmentPointsDataMapper.ArePointsAssignedToEquipment(eid);

                            if (hasPoints)
                            {
                                LabelHelper.SetLabelMessage(lblDeleteError, pointsExist, lnkSetFocus);
                                return;
                            }
                            else
                            {
                                //vpoints should have already been disassociated and vdata deleted when the analysis was unassigned, but in the odd case that its vpoints for tenants still exist, return message.
                                //This sometimes happens in utility billing for tenant utilities type??
                                var hasVPoints = DataMgr.PointDataMapper.DoAnyVPointsExistForEID(eid);

                                if (hasVPoints)
                                {
                                    LabelHelper.SetLabelMessage(lblDeleteError, vPointsExist, lnkSetFocus);
                                    return;
                                }
                                else
                                {
                                    if (DataMgr.FileDataMapper.DoFilesExistForEID(eid))
                                    {
                                        LabelHelper.SetLabelMessage(lblDeleteError, filesExist, lnkSetFocus);
                                        return;
                                    }
                                    else
                                    {
                                        DataMgr.EquipmentDataMapper.DeleteEquipmentToEquipmentAssociationsByRelatedEID(eid);
                                        DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariablesByEID(eid);
                                        DataMgr.EquipmentDataMapper.DeleteEquipment(siteUser.CID, eid);

                                        ChangeCurrentTab(deleteSuccessful);
                                        SetChangeStateForTabs(KGSEquipmentAdministration.TabMessages.DeleteEquipment);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblDeleteError, deleteFailed, lnkSetFocus);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting equipment.", ex);
                        }
                    }

                #endregion

            #endregion

            #region button events

                protected void updateEquipmentButton_Click(Object sender, EventArgs e)
                {
                    var mEquipment = new CW.Data.Equipment();

                    LoadEditFormIntoEquipment(mEquipment);

                    var eid = mEquipment.EID;
                    int selectedEquipmentType = Convert.ToInt32(ddlEditEquipmentType.SelectedValue);

                    try
                    {
                        //TODO: make equipment name and imgextension hdn variables so we can remove this query
                        var originalEquipment = DataMgr.EquipmentDataMapper.GetEquipmentByID(eid);

                        if (originalEquipment.EquipmentName != mEquipment.EquipmentName)
                        {
                            if (DataMgr.EquipmentDataMapper.DoesEquipmentNameExistForBuilding(mEquipment.EquipmentName, Convert.ToInt32(ddlEditBuilding.SelectedValue)))
                            {
                                LabelHelper.SetLabelMessage(lblEditError, equipmentExists, lnkSetFocusEdit);
                                return;
                            }
                        }

                        //This conditional will never be hit, the DDL for buildings is disabled, remove?
                        if (originalEquipment.BID != Convert.ToInt32(ddlEditBuilding.SelectedValue))
                        {
                            if (DataMgr.AnalysisEquipmentDataMapper.IsAnyAnalysisAssignedToEquipmentInABuilding(originalEquipment.BID))
                            {
                                LabelHelper.SetLabelMessage(lblEditError, analysesAssignedInBuilding, lnkSetFocusEdit);
                                return;
                            }

                            //OTHER CHECKS ARE IRRELIVANT IF WE DONT ALLOW IF ANALYSES ARE ASSIGNED

                            #region TODO

                            //TODO: move or delete equipment images?

                            //TODO: check if points are associated with any other equipment. that means those points would be in another buildings and we cannot move the equipment.

                            //TODO: check if building class changed
                            //int originalBuildingClassID = mDataManager.BuildingClassDataMapper.GetBuildingClassIDByBID(originalEquipment.BID);

                            //TODO: evventually we could check if the new building has all the required variables on all assigned analyses 
                            //and allow the change.

                            //TODO: 
                            //check if any analysis is assigned to equipment where a building variable is required.
                            //if (mDataManager.AnalysisEquipmentDataMapper.IsAnyAnalysisAssignedToEquipmentWhereABuildingVariableIsRequired(mEquipment.EID))
                            //{
                            //    lblEditError.Text = requiredBuildingVariablesOnAssignedAnalyses;
                            //    return;
                            //}
                            #endregion
                        }

                        
                        if (originalEquipment.EquipmentTypeID != selectedEquipmentType)
                        {
                            //check if all of the equipments points point types are allowed on the changing equipment type
                            if (!DataMgr.EquipmentTypeDataMapper.AreAllEquipmentsPointsPointTypesAllowedOnChangingEquipmentType(eid, selectedEquipmentType))
                            {
                                LabelHelper.SetLabelMessage(lblEditError, unallowedPointTypes, lnkSetFocusEdit);
                                return;
                            }                    
                                
                            //check if all of the existing assigned equipment analyses allowed on the changing equipment type
                            var existingAssignedAnalyses = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByEID(eid).Select(a => a.AID);
                            var allowedAnalyses = DataMgr.AnalysisEquipmentTypeDataMapper.GetAssignedAnalysesByEquipmentTypeIDAndCID(selectedEquipmentType, siteUser.CID).Select(a => a.AID);
                            
                            if (existingAssignedAnalyses.Except(allowedAnalyses).Any())
                            {
                                LabelHelper.SetLabelMessage(lblEditError, unallowedAnalyses, lnkSetFocusEdit);
                                return;
                            }
                        }

                        //TODO: this could probably be a hidden variable set on the front end earlier rather then quering here
                        var classID = DataMgr.EquipmentClassDataMapper.GetEquipmentClassIDByEID(eid);

                        if (classID != Convert.ToInt32(ddlEditEquipmentClass.SelectedValue))
                        {
                            DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariablesNotFoundInChangingEquipmentClass(eid, Convert.ToInt32(ddlEditEquipmentClass.SelectedValue));

                            //NOTE: no need to check manufacture model allowed on new class, should be handled by the dropdowns.                            
                        }

                        //this check will cover both type and class, if the class changes, the type ddl is reset and populated with types related only to the newly selected class
                        //only delete invalid associations due to the type change
                        if (originalEquipment.EquipmentTypeID != Convert.ToInt32(ddlEditEquipmentType.SelectedValue)) DataMgr.EquipmentDataMapper.DeleteInvalidEquipmentToEquipmentAssociationsDueToChangingEquipmentType(eid, Convert.ToInt32(ddlEditEquipmentType.SelectedValue));
                        
                        if (fileUploaderEdit.HasChanged)
                        {
                            var image = new EquipmentProfileImage(siteUser.CID, DataMgr.OrgBasedBlobStorageProvider, new EquipmentProfileImage.Args(originalEquipment.EID));

                            if (!String.IsNullOrEmpty(originalEquipment.ImageExtension))
                            {
                                image.SetExtension(originalEquipment.ImageExtension).Delete();
                                mEquipment.ImageExtension = String.Empty;
                            }

                            if (fileUploaderEdit.File != null)
                            {
                                image.SetExtension(fileUploaderEdit.File.Extension).Insert(fileUploaderEdit.File.Content);
                                mEquipment.ImageExtension = fileUploaderEdit.File.Extension;
                            }
                        }

                        DataMgr.EquipmentDataMapper.UpdateEquipment(mEquipment);
                        OperationComplete(OperationType.Update, updateSuccessful, KGSEquipmentAdministration.TabMessages.EditEquipment);
                    }
                    catch (SqlException sqlEx)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment.", sqlEx);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment.", ex);
                    }
                }

            #endregion

            #region ddl events

                protected void ddlEditManufacturer_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindManufacturerModels(ddlEditManufacturerModel, Convert.ToInt32(ddlEditManufacturer.SelectedValue));
                    BindManufacturerContacts(ddlEditPrimaryContact, Convert.ToInt32(ddlEditManufacturer.SelectedValue));
                    BindManufacturerContacts(ddlEditSecondaryContact, Convert.ToInt32(ddlEditManufacturer.SelectedValue));
                }

                protected void ddlEditEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipmentTypes(ddlEditEquipmentType, Convert.ToInt32(ddlEditEquipmentClass.SelectedValue));
                    BindManufacturers(ddlEditManufacturer, ddlEditManufacturerModel, Convert.ToInt32(ddlEditEquipmentClass.SelectedValue));
                }

                protected void GridDropDowns_BuildingChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    Int32 bid;

                    scheduleDownloadArea.Visible = (Int32.TryParse(ea.SelectedValue, out bid) && (ea.SelectedValue != "-1"));
                    scheduleDownloadArea.FindControl("lblError").Visible = false;

                    if (Page.IsPostBack) ResetUI();
                    if (GridDropDowns.ViewMode == DropDowns.ViewModeEnum.Building && !Page.IsPostBack) return;
                    if (ea.SelectedValue == "-1") return;

                    sender.BindEquipmentClassList((ea.SelectedValue == "0") ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses() : DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(Int32.Parse(ea.SelectedValue)), true, true);
                }

                protected void GridDropDowns_EquipmentClassChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    ResetUI();

                    grid.Visible = (ea.SelectedValue != "-1");

                    if (ea.SelectedValue == "-1") return;

                    BindData();
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var equipment = DataMgr.EquipmentDataMapper.GetFullEquipmentByEID(CurrentID).First();
                    var points = StringHelper.ParseStringArrayToCSVString(DataMgr.PointDataMapper.GetAllPointsByEID(CurrentID).Select(ep => ep.PointName).ToArray(), true);
                    var equipmentVariables = StringHelper.ParseStringArrayToCSVString(DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesAssociatedToEID(CurrentID, equipment.EquipmentClassID).Select(ev => ev.EquipmentVariableName).ToArray(), true);
                    var associatedEquipment = StringHelper.ParseStringArrayToCSVString(DataMgr.EquipmentDataMapper.GetAllEquipmentAssociatedByPEID(CurrentID, false).Select(e => e.EquipmentName).ToArray(), true);

                    if (String.IsNullOrEmpty(equipment.ImageExtension))
                        equipmentImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noEquipmentImage;
                    else
                    {
                        equipmentImageUrl =
                        (
                            new EquipmentProfileImage(siteUser.CID,
                                                      DataMgr.OrgBasedBlobStorageProvider,
                                                      equipment.ImageExtension,
                                                      new EquipmentProfileImage.Args(CurrentID))
                                                      .Exists() ? HandlerHelper.EquipmentImageUrl(siteUser.CID, CurrentID, equipment.ImageExtension) : ConfigurationManager.AppSettings["ImageAssetPath"] + noEquipmentImage
                        );
                    }

                    SetEquipmentIntoViewState(equipment, points, equipmentVariables, associatedEquipment);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(EquipmentDetails.Default), rptEquipmentDetails);
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(EquipmentDetails.Service), rptEquipmentServiceDetails);
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(EquipmentDetails.Additional), rptAdditionalEquipmentDetails);
                }
 
            #endregion

            private void SetEquipmentIntoViewState(GetEquipmentData equipment, string points, string equipmentVariables, string associatedEquipment)
            {                                                     
                SetEquipmentDetails(equipment, points, equipmentVariables, associatedEquipment);
                SetEquipmentServiceDetails(equipment);
                SetAdditionalEquipmentDetails(equipment);
            }

            private void SetEquipmentDetails(GetEquipmentData equipment, string points, string equipmentVariables, string associatedEquipment)
            {
                SetKeyValuePairItem("Client Name", equipment.ClientName);
                SetKeyValuePairItem("Building Name", equipment.BuildingName);
                SetKeyValuePairItem("Equipment Name", equipment.EquipmentName);
                SetKeyValuePairItem("Equipment Description", equipment.Description, "<div class='divContentPreWrap'>{0}</div>");
                SetKeyValuePairItem("Equipment Class Name", equipment.EquipmentClassName);
                SetKeyValuePairItem("Class Description", equipment.EquipmentClassDescription);
                SetKeyValuePairItem("Equipment Type Name", equipment.EquipmentTypeName);
                SetKeyValuePairItem("Type Description", equipment.EquipmentTypeDescription);
                SetKeyValuePairItem("Points", points);
                SetKeyValuePairItem("Assigned Equipment Vars.", equipmentVariables);
                SetKeyValuePairItem("Associated Equipment", associatedEquipment);
                SetKeyValuePairItem("Manufacturer Name", (equipment.ManufacturerName == "Unknown") ? equipment.ManufacturerName : "<a href=\"/KGSEquipmentManufacturerAdministration.aspx?mid=" + equipment.ManufacturerID + "\">" + equipment.ManufacturerName + "</a>");
                SetKeyValuePairItem("Manufacturer Model Name", (equipment.ManufacturerModelName == "Unknown") ? equipment.ManufacturerModelName : "<a href=\"/KGSEquipmentManufacturerModelAdministration.aspx?mid=" + equipment.ManufacturerModelID + "\">" + equipment.ManufacturerModelName + "</a>");
                SetKeyValuePairItem("Serial Number", equipment.SerialNumber);
                SetKeyValuePairItem("Equipment Location", equipment.EquipmentLocation, "<div class='divContentPreWrap'>{0}</div>");                
                SetKeyValuePairItem("Equipment Image", equipmentImageUrl, "<img id='imgPreview' src='{0}' alt='" + equipment.EquipmentName + "' class='imgDetailsListPreview' />");
                SetKeyValuePairItem("Primary Manufacturer Contact", equipment.PrimaryContactID.ToString(), "<a href=\"/KGSEquipmentManufacturerContactAdministration.aspx?cid={0}\">" + equipment.PrimaryContactName + "</a>");
                SetKeyValuePairItem("Secondary Manufacturer Contact", equipment.SecondaryContactID.ToString(), "<a href=\"/KGSEquipmentManufacturerContactAdministration.aspx?cid={0}\">" + equipment.SecondaryContactName + "</a>");
                SetKeyValuePairItem("Equipment Notes", equipment.EquipmentNotes);

                SetKeyValuePairItem("Active", equipment.IsActive.ToString());
                SetKeyValuePairItem("Visible", equipment.IsVisible.ToString());

                SetDetailsViewDataToViewState(Convert.ToInt32(EquipmentDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            private void SetEquipmentServiceDetails(GetEquipmentData equipment)
            {
                if (equipment.LastServicedDate != null || !String.IsNullOrEmpty(equipment.LastServicedBy) || !String.IsNullOrEmpty(equipment.LastServicedCompany))
                {
                    if (equipment.LastServicedDate != null) SetKeyValuePairItem("Last Serviced Date", equipment.LastServicedDate.Value.ToShortDateString());
                    SetKeyValuePairItem("Last Serviced By", equipment.LastServicedBy);
                    SetKeyValuePairItem("Last Serviced Company", equipment.LastServicedCompany);

                    SetDetailsViewDataToViewState(Convert.ToInt32(EquipmentDetails.Service));
                }
            }

            private void SetAdditionalEquipmentDetails(GetEquipmentData equipment)
            {
                if (!String.IsNullOrWhiteSpace(equipment.ExternalEquipmentTypeName) || !String.IsNullOrWhiteSpace(equipment.CMMSReferenceID) || !String.IsNullOrWhiteSpace(equipment.CMMSLocationID) || !String.IsNullOrWhiteSpace(equipment.CMMSSiteID) || !String.IsNullOrWhiteSpace(equipment.CMMSLink) || !String.IsNullOrWhiteSpace(equipment.BIMReferenceID)
                    || !String.IsNullOrWhiteSpace(equipment.BIMLink) || !String.IsNullOrWhiteSpace(equipment.DMSReferenceID) || !String.IsNullOrWhiteSpace(equipment.DMSLink) || !String.IsNullOrWhiteSpace(equipment.GISReferenceID)
                    || !String.IsNullOrWhiteSpace(equipment.GISLink))
                {
                    SetKeyValuePairItem("External Equipment Type Name", equipment.ExternalEquipmentTypeName);
                    SetKeyValuePairItem("CMMS (Computerized Maintenance Mgmt System) Reference/Asset ID", equipment.CMMSReferenceID);
                    SetKeyValuePairItem("CMMS (Computerized Maintenance Management System) Location ID", equipment.CMMSLocationID);
                    SetKeyValuePairItem("CMMS (Computerized Maintenance Management System) Site ID", equipment.CMMSSiteID);
                    if (equipment.CMMSLink != null) SetKeyValuePairItem("CMMS (Computerized Maintenance Management System) Link", equipment.CMMSLink, "<a target='_blank' href='" + StringHelper.ExternalLinkCorrector(equipment.CMMSLink) + "'>" + equipment.CMMSLink + "</a>");
                    SetKeyValuePairItem("BIM (Building Information Management) Reference ID", equipment.BIMReferenceID);

                    if(equipment.BIMLink != null) SetKeyValuePairItem("BIM (Building Information Management) Link", equipment.BIMLink, "<a target='_blank' href='" + StringHelper.ExternalLinkCorrector(equipment.BIMLink) + "'>" + equipment.BIMLink + "</a>");
                    SetKeyValuePairItem("DMS (Document Management Software) Reference ID", equipment.DMSReferenceID);
                    if (equipment.DMSLink != null) SetKeyValuePairItem("DMS (Document Management Software) Link", equipment.DMSLink, "<a target='_blank' href='" + StringHelper.ExternalLinkCorrector(equipment.DMSLink) + "'>" + equipment.DMSLink + "</a>");
                    SetKeyValuePairItem("GIS (Geographic Information System) Reference ID", equipment.GISReferenceID);
                    if (equipment.GISLink != null) SetKeyValuePairItem("GIS (Geographic Information System) Link", equipment.GISLink, "<a target='_blank' href='" + StringHelper.ExternalLinkCorrector(equipment.GISLink) + "'>" + equipment.GISLink + "</a>");

                    SetDetailsViewDataToViewState(Convert.ToInt32(EquipmentDetails.Additional));
                }            
            }

            private void SetEquipmentIntoEditForm(CW.Data.Equipment equipment)
            {
                hdnEditID.Value = Convert.ToString(equipment.EID);
                txtEditEquipmentName.Text = equipment.EquipmentName;
                ddlEditBuilding.SelectedValue = Convert.ToString(equipment.BID);
                ddlEditEquipmentClass.SelectedValue = Convert.ToString(equipment.EquipmentType.EquipmentClassID);
                ddlEditEquipmentType.SelectedValue = Convert.ToString(equipment.EquipmentTypeID);
                ddlEditManufacturer.SelectedValue = Convert.ToString(equipment.EquipmentManufacturerModel.ManufacturerID);
                ddlEditManufacturerModel.SelectedValue = Convert.ToString(equipment.ManufacturerModelID);
                txtEditSerialNumber.Text = equipment.SerialNumber;
                txtEditEquipmentLocation.Value = equipment.EquipmentLocation;
                txtEditDescription.Value = equipment.Description;
                
                fileUploaderEdit.Clear();

                if (!String.IsNullOrEmpty(equipment.ImageExtension))
                {
                    var image = new EquipmentProfileImage(equipment, DataMgr.OrgBasedBlobStorageProvider);

                    if (image.Exists()) fileUploaderEdit.SetFile(image.Retrieve(), image.FileName);
                }

                ddlEditPrimaryContact.SelectedValue = (equipment.PrimaryContactID != null) ? Convert.ToString(equipment.PrimaryContactID) : "-1";
                ddlEditSecondaryContact.SelectedValue = (equipment.SecondaryContactID != null) ? Convert.ToString(equipment.SecondaryContactID) : "-1";
                editorEditEquipmentNotes.Content = equipment.EquipmentNotes;
                chkEditActive.Checked = equipment.IsActive;
                chkEditVisible.Checked = equipment.IsVisible;

                //Serviced Information

                txtEditLastServicedBy.Text = equipment.LastServicedBy;
                txtEditLastServicedDate.SelectedDate = equipment.LastServicedDate;
                txtEditLastServicedCompany.Text = equipment.LastServicedCompany;

                //Additional Information

                txtEditExternalEquipmentTypeName.Text = equipment.ExternalEquipmentTypeName;
                txtEditCMMSReferenceID.Text = equipment.CMMSReferenceID;
                txtEditCMMSLocationID.Text = equipment.CMMSLocationID;
                txtEditCMMSSiteID.Text = equipment.CMMSSiteID;
                txtEditCMMSLink.Text = equipment.CMMSLink;
                txtEditBIMReferenceID.Text = equipment.BIMReferenceID;
                txtEditBIMLink.Text = equipment.BIMLink;
                txtEditDMSReferenceID.Text = equipment.DMSReferenceID;
                txtEditDMSLink.Text = equipment.DMSLink;
                txtEditGISReferenceID.Text = equipment.GISReferenceID;
                txtEditGISLink.Text = equipment.GISLink;
            }

            private void LoadEditFormIntoEquipment(CW.Data.Equipment equipment)
            {
                equipment.EID = Convert.ToInt32(hdnEditID.Value);
                equipment.EquipmentName = txtEditEquipmentName.Text;
                equipment.BID = Convert.ToInt32(ddlEditBuilding.SelectedValue);
                equipment.ManufacturerModelID = Convert.ToInt32(ddlEditManufacturerModel.SelectedValue);
                equipment.SerialNumber = String.IsNullOrEmpty(txtEditSerialNumber.Text) ? null : txtEditSerialNumber.Text;
                equipment.EquipmentLocation = String.IsNullOrEmpty(txtEditEquipmentLocation.Value) ? null : txtEditEquipmentLocation.Value;
                equipment.Description = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                equipment.EquipmentTypeID = Convert.ToInt32(ddlEditEquipmentType.SelectedValue);
                equipment.ImageExtension = fileUploaderEdit.File == null ? String.Empty : fileUploaderEdit.File.Extension;
                equipment.PrimaryContactID = ddlEditPrimaryContact.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditPrimaryContact.SelectedValue) : null;
                equipment.SecondaryContactID = ddlEditSecondaryContact.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditSecondaryContact.SelectedValue) : null;
                equipment.EquipmentNotes = StringHelper.ClearRadEditorFirefoxBR(editorEditEquipmentNotes.Content);
                equipment.IsActive = chkEditActive.Checked;             
                equipment.IsVisible = chkEditVisible.Checked;

                //Serviced Information

                equipment.LastServicedBy = String.IsNullOrEmpty(txtEditLastServicedBy.Text) ? null : txtEditLastServicedBy.Text;
                equipment.LastServicedDate = txtEditLastServicedDate.SelectedDate;
                equipment.LastServicedCompany = String.IsNullOrEmpty(txtEditLastServicedCompany.Text) ? null : txtEditLastServicedCompany.Text;

                //Additional Information

                equipment.ExternalEquipmentTypeName = String.IsNullOrEmpty(txtEditExternalEquipmentTypeName.Text) ? null : txtEditExternalEquipmentTypeName.Text;
                equipment.CMMSReferenceID = String.IsNullOrEmpty(txtEditCMMSReferenceID.Text) ? null : txtEditCMMSReferenceID.Text;
                equipment.CMMSLocationID = String.IsNullOrEmpty(txtEditCMMSLocationID.Text) ? null : txtEditCMMSLocationID.Text;
                equipment.CMMSSiteID = String.IsNullOrEmpty(txtEditCMMSSiteID.Text) ? null : txtEditCMMSSiteID.Text;
                equipment.CMMSLink = String.IsNullOrEmpty(txtEditCMMSLink.Text) ? null : txtEditCMMSLink.Text;
                equipment.BIMReferenceID = String.IsNullOrEmpty(txtEditBIMReferenceID.Text) ? null : txtEditBIMReferenceID.Text;
                equipment.BIMLink = String.IsNullOrEmpty(txtEditBIMLink.Text) ? null : txtEditBIMLink.Text;
                equipment.DMSReferenceID = String.IsNullOrEmpty(txtEditDMSReferenceID.Text) ? null : txtEditDMSReferenceID.Text;
                equipment.DMSLink = String.IsNullOrEmpty(txtEditDMSLink.Text) ? null : txtEditDMSLink.Text;
                equipment.GISReferenceID = String.IsNullOrEmpty(txtEditGISReferenceID.Text) ? null : txtEditGISReferenceID.Text;
                equipment.GISLink = String.IsNullOrEmpty(txtEditGISLink.Text) ? null : txtEditGISLink.Text;
            }

            #region binders

                private void BindBuildings(DropDownList ddl, Int32 clientID)
                {
                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindEquipmentClasses(DropDownList ddl)
                {
                    BindDDL(ddl, "EquipmentClassName", "EquipmentClassID", DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses(), true);
                }

                private void BindEquipmentTypes(DropDownList ddl, Int32 equipmentClassID)
                {
                    BindDDL(ddl, "EquipmentTypeName", "EquipmentTypeID", DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByEquipmentClassID(equipmentClassID), true);
                }

                private void BindManufacturers(DropDownList ddlManufacturer, DropDownList ddlModel, Int32 equipmentClassID)
                {
                    BindDDL(ddlManufacturer, "ManufacturerName", "ManufacturerID", DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturersAssociatedToEquipmentClassID(equipmentClassID), true);

                    var lItemModel = new ListItem() { Text = "Select manufacturer first...", Value = "-1", Selected = true };

                    BindDDL(ddlModel, "ManufacturerModelName", "ManufacturerModelID", null, true, lItemModel);
                }

                private void BindManufacturerModels(DropDownList ddl, Int32 manufacturerID)
                {
                    BindDDL(ddl, "ManufacturerModelName", "ManufacturerModelID", DataMgr.EquipmentManufacturerModelDataMapper.GetAllManufacturerModelsByManufacturerID(manufacturerID), true);
                }

                private void BindManufacturerContacts(DropDownList ddl, Int32 manufacturerID)
                {
                    var lItem = new ListItem() { Text = "Optionally select one...", Value = "-1", Selected = true };

                    BindDDL(ddl, "Name", "ContactID", DataMgr.EquipmentManufacturerContactDataMapper.GetAllManufacturerContactsByManufacturerID(manufacturerID), true, lItem);
                }

            #endregion

        #endregion
    }
}