﻿using CW.Website._administration;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using Telerik.Web.UI;

namespace CW.Website._controls.admin
{
    public abstract class AdminSitePageTemp : SitePage, IAdminPage
    {
        #region field(s)

            protected RadTabStrip radTabStrip;

            protected RadMultiPage radMultiPage;

            List<AdminUserControlBase> controls = new List<AdminUserControlBase>();

            int pageViewIndex = 0;

        #endregion

        #region properties

            public TabStateMonitor TabStateMonitor { get; set; }

            public OnNotifyPageOnChange NotifyPageOnChangeEvent;

            public OnGetCachedObject GetCachedObject;

            #region abstract

                protected abstract string DefaultControl { get; }

            #endregion

            #region virtual

                public virtual AdminModeEnum AdminModeEnum { get; set; }

            #endregion

        #endregion

        #region event(s)

            public delegate void OnNotifyPageOnChange(Enum message=null);

            public delegate object OnGetCachedObject(string id);

            #region page event(s)

                void Page_Init(object Sender, EventArgs e)
                {
                    SetVisibleTabs();

                    TabStateMonitor = new TabStateMonitor(this, radTabStrip);

                    Init += ((object sender, EventArgs args) => { TabStateMonitor.OnTabStateChanged += tabStateMonitor_OnTabStateChanged; });
                }

                void Page_FirstLoad(object sender, EventArgs e)
                {
                    SetChangeState();

                    new AdminUserControlTabs(radTabStrip, radMultiPage, DefaultControl).SetActiveTab();
                }

                void Page_Load(object Sender, EventArgs e)
                {
                    pageViewIndex = radTabStrip.MultiPage.SelectedIndex;
            
                    controls = FindControls<AdminUserControlBase>(true).Where(c => c.AddTabForTabReload).ToList();
                }

            #endregion

            #region tab event(s)

                void tabStateMonitor_OnTabStateChanged(TabBase obj, IEnumerable<Enum> messages) => ResetUserControl();

        #endregion

        #endregion

        #region method(s)

            #region virtual

                //we may need to dynamically delete tabs based on user creds, override this in the page class
                protected virtual void SetVisibleTabs() { }

                //we may need to dynamically set the change state outside of the normal page lifecycle
                //override this in the page class
                protected virtual void SetChangeState() { }

            #endregion

            void ResetUserControl()
            {
                if (controls[pageViewIndex] != null) (controls[pageViewIndex]).ResetUserControl();
            }

            public TabStateMonitor GetTabStateMonitor()
            {
                return TabStateMonitor;
            }

            public string GetIdentifierField()
            {
                return null;
            }

            public string GetNameField()
            {
                return null;
            }

        #endregion
    }
}