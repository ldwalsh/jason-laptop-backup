﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Control;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._extensions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Points
{
    public partial class AddExistingPoints : AdminUserControlBase
    {
        #region constants

            const String addExistingPointFailed = " existing point addition failed. ";
            const String addExistingPointSuccess = " existing point addition was successful. ";
            const String addExistingPointExists = " existing point already exists for this equipment. ";
            const String addExistingPointNoFromEquipmentSelected = "No from equipment was selected.";
            const String addExistingPointNoToEquipmentSelected = "No to equipment was selected.";
            const String addExistingPointNoPointsSelected = "No points were selected.";
            const String addExistingPointTypeExists = " existing point with the same type already exists for this equipment. ";
            const String addExistingPointTypeNotAllowed = " point type is not allowed on equipment of that type.  ";

        #endregion

        #region fields

            String bid = PropHelper.G<Building>(b => b.BID);
            String buildingName = PropHelper.G<Building>(b => b.BuildingName);
            String eid = PropHelper.G<CW.Data.Equipment>(e => e.EID);
            String equipmentName = PropHelper.G<CW.Data.Equipment>(e => e.EquipmentName);
            String equipmentClassId = PropHelper.G<EquipmentClass>(ec => ec.EquipmentClassID);
            String equipmentClassName = PropHelper.G<EquipmentClass>(ec => ec.EquipmentClassName);
            String id = PropHelper.G<GetConcatenatedControlData>(cd => cd.ID);
            String concatenatedName = PropHelper.G<GetConcatenatedControlData>(cd => cd.ConcatenatedName);

        #endregion

        #region properties

            #region overrides

            public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[]
                        {
                            KGSPointAdministration.TabMessages.AddPoint,
                            KGSPointAdministration.TabMessages.EditPoint,
                            KGSPointAdministration.TabMessages.DeletePoint
                        };
                    }
                }

            #endregion

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAddExistingError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindBuildings(ddlAddExistingFromBuilding, ddlAddExistingEquipmentPointsFromBuilding, ddlAddExistingToBuilding);

                    //Individual
                    BindEquipmentClass(ddlAddExistingFromEquipmentClass, -1);
                    BindEquipment(ddlAddExistingFromEquipment, new Int32[] { -1 });

                    //multiple
                    BindEquipmentClass(ddlAddExistingEquipmentPointsFromEquipmentClass, -1);

                    //both (shows for both invidual and multiple sections)
                    BindEquipmentClass(ddlAddExistingToEquipmentClass, -1);
                }
            }

            #region ddl events

                //individual
                protected void ddlAddExistingFromBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipmentClass(ddlAddExistingFromEquipmentClass, Convert.ToInt32(ddlAddExistingFromBuilding.SelectedValue));
                    BindEquipment(ddlAddExistingFromEquipment, new Int32[] { Convert.ToInt32(ddlAddExistingFromBuilding.SelectedValue) });
                    BindPoints(lbAddExistingPointsToEquipment, Convert.ToInt32(ddlAddExistingFromEquipment.SelectedValue));
                }

                protected void ddlAddExistingFromEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipment(ddlAddExistingFromEquipment, new Int32[] { Convert.ToInt32(ddlAddExistingFromBuilding.SelectedValue), Convert.ToInt32(ddlAddExistingFromEquipmentClass.SelectedValue) } );
                    BindPoints(lbAddExistingPointsToEquipment, Convert.ToInt32(ddlAddExistingFromEquipment.SelectedValue));
                }

                protected void ddlAddExistingFromEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindPoints(lbAddExistingPointsToEquipment, Convert.ToInt32(ddlAddExistingFromEquipment.SelectedValue));
                }

                //multiple
                protected void ddlAddExistingEquipmentPointsFromBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipmentClass(ddlAddExistingEquipmentPointsFromEquipmentClass, Convert.ToInt32(ddlAddExistingEquipmentPointsFromBuilding.SelectedValue));
                    BindEquipmentListBox(lbAddExistingEquipmentPointsToEquipment, new Int32[] { Convert.ToInt32(ddlAddExistingEquipmentPointsFromBuilding.SelectedValue) });
                }

                protected void ddlAddExistingEquipmentPointsFromEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipmentListBox(lbAddExistingEquipmentPointsToEquipment, new Int32[] 
                    { 
                        Convert.ToInt32(ddlAddExistingEquipmentPointsFromBuilding.SelectedValue), 
                        Convert.ToInt32(ddlAddExistingEquipmentPointsFromEquipmentClass.SelectedValue) 
                    });
                }

                //Both (shows for both invidual and multiple sections)
                protected void ddlAddExistingToBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipmentClass(ddlAddExistingToEquipmentClass, Convert.ToInt32(ddlAddExistingToBuilding.SelectedValue));
                    BindEquipmentListBox(lbAddExistingToEquipment, new Int32[] { Convert.ToInt32(ddlAddExistingToBuilding.SelectedValue) });
                }

                protected void ddlAddExistingToEquipmentClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipmentListBox(lbAddExistingToEquipment, new Int32[] 
                    { 
                        Convert.ToInt32(ddlAddExistingToBuilding.SelectedValue),
                        Convert.ToInt32(ddlAddExistingToEquipmentClass.SelectedValue)
                    });
                }

            #endregion

            #region button events

                protected void addExistingPointsButton_Click(Object sender, EventArgs e)
                {
                    var selectedPointsItems = ListBoxExtension.GetSelectedItems(lbAddExistingPointsToEquipment);
                    var selectedFromEquipmentItems = ListBoxExtension.GetSelectedItems(lbAddExistingEquipmentPointsToEquipment);
                    var selectedToEquipmentItems = ListBoxExtension.GetSelectedItems(lbAddExistingToEquipment);

                    if (selectedToEquipmentItems.Count == 0)
                    {
                        LabelHelper.SetLabelMessage(lblAddExistingError, addExistingPointNoToEquipmentSelected, lnkSetFocus);
                        return;
                    }
                    else
                    {
                        lblAddExistingError.Text = lblResults.Text = String.Empty;

                        switch (rblFromEquipmentMode.SelectedValue)
                        {
                            case "Individual":
                                {
                                    if (selectedPointsItems.Count == 0)
                                    {
                                        LabelHelper.SetLabelMessage(lblAddExistingError, addExistingPointNoPointsSelected, lnkSetFocus);
                                        return;
                                    }
                                    else
                                    {
                                        foreach (var equipment in selectedToEquipmentItems)
                                        {
                                            foreach (var point in selectedPointsItems) AddExistingPoint(equipment.Text, Convert.ToInt32(equipment.Value), point.Text, Convert.ToInt32(point.Value));
                                        }

                                        lbAddExistingPointsToEquipment.ClearSelection();
                                    }

                                    break;
                                }
                            case "Multiple":
                                {
                                    if (selectedFromEquipmentItems.Count == 0)
                                    {
                                        LabelHelper.SetLabelMessage(lblAddExistingError, addExistingPointNoFromEquipmentSelected, lnkSetFocus);
                                        return;
                                    }
                                    else
                                    {
                                        foreach (var equipment in selectedToEquipmentItems)
                                        {
                                            foreach (var fromEquipment in selectedFromEquipmentItems)
                                            {
                                                var points = DataMgr.PointDataMapper.GetAllPointsByEID(Convert.ToInt32(fromEquipment.Value));

                                                foreach (var p in points) AddExistingPoint(equipment.Text, Convert.ToInt32(equipment.Value), p.PointName, p.PID);
                                            }
                                        }

                                        lbAddExistingEquipmentPointsToEquipment.ClearSelection();
                                    }

                                    break;
                                }
                        }
                    }

                    if (lblAddExistingError.Text != String.Empty) LabelHelper.SetLabelMessage(lblAddExistingError, lblAddExistingError.Text, lnkSetFocus);

                    if (lblResults.Text != String.Empty)
                    {
                        LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);

                        OperationComplete(KGSPointAdministration.TabMessages.AddPoint);
                        new ClearCachePoints().ClearCache(siteUser.CID, DataMgr.BuildingDataMapper.GetAllBuildingIDsByCID(siteUser.CID).ToArray(), siteUser.UID);
                    }
                }

            #endregion

            #region rbl events

                protected void rblFromEquipmentMode_OnSelectIndexChanged(Object sender, EventArgs e)
                {
                    switch (rblFromEquipmentMode.SelectedValue)
                    {
                        case "Individual":
                            {
                                addExistingFromBuildingRFV.Enabled = true;
                                addExistingFromEquipmentRFV.Enabled = true;
                                addExistingEquipmentPointsFromBuildingRFV.Enabled = false;
                                divIndividualEquipment.Visible = true;
                                divMulitipleEquipment.Visible = false;
                                break;
                            }
                        case "Multiple":
                            {
                                addExistingFromBuildingRFV.Enabled = false;
                                addExistingFromEquipmentRFV.Enabled = false;
                                addExistingEquipmentPointsFromBuildingRFV.Enabled = true;
                                divIndividualEquipment.Visible = false;
                                divMulitipleEquipment.Visible = true;
                                break;
                            }
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    new AdminUserControlForm(this).ResetDropDownLists();

                    //hardset any fields to their initial values
                    rblFromEquipmentMode.SelectedIndex = 0;
                    rblFromEquipmentMode_OnSelectIndexChanged(null, null);

                    //control(s) from Individual section
                    BindEquipmentClass(ddlAddExistingFromEquipmentClass, -1);
                    BindEquipment(ddlAddExistingFromEquipment, new Int32[] { -1 });
                    BindPoints(lbAddExistingPointsToEquipment, -1);
                    
                    //control(s) from Multiple section
                    BindEquipmentClass(ddlAddExistingEquipmentPointsFromEquipmentClass, -1);
                    BindEquipmentListBox(lbAddExistingEquipmentPointsToEquipment);
                    
                    //control(s) from Both sections
                    BindEquipmentClass(ddlAddExistingToEquipmentClass, -1);
                    BindEquipmentListBox(lbAddExistingToEquipment);
                }

            #endregion

            private void AddExistingPoint(String equipmentName, Int32 eid, String pointName, Int32 pid)
            {
                var equipmentPoint = new Equipment_Point();

                equipmentPoint.EID = eid;
                equipmentPoint.PID = pid;

                if (DataMgr.EquipmentPointsDataMapper.DoesPointAlreadyExistForEquipment(eid, pid))
                    lblAddExistingError.Text += equipmentName + " " + pointName + addExistingPointExists + "<br />";
                else
                {
                    //check if point's point type is allowed for equipments equipment type
                    if (DataMgr.EquipmentTypeDataMapper.IsPointsTypeAssociatedWithEquipmentsType(pid, eid))
                    {
                        //Check if equipment is of class 'group', if so allow multiple of the same point types to be added                    
                        var isEquipmentGroup = DataMgr.EquipmentDataMapper.IsEquipmentAssociatedWithClassGroup(eid);
                        var pointTypeExistsForNonEquipmentGroup = false;
                        var isPointTypeIsUnassigned = DataMgr.PointDataMapper.IsPointAssociatedWithTypeUnassigned(pid);

                        //If not a group and not unassigned check if a point with the same point type already exists for equipmet not of class group                   
                        if (!isEquipmentGroup && !isPointTypeIsUnassigned)
                            pointTypeExistsForNonEquipmentGroup = DataMgr.EquipmentPointsDataMapper.DoesPointTypeExistForEquipment(eid, DataMgr.PointDataMapper.GetPointByPID(pid).PointTypeID);

                        //if equipment is group continue, or if pointtype doesnt exist on equipment not of class group, or if adding an unassigned point                   
                        if (isEquipmentGroup || !pointTypeExistsForNonEquipmentGroup || isPointTypeIsUnassigned)
                        {
                            try
                            {
                                DataMgr.EquipmentPointsDataMapper.InsertEquipment_Point(equipmentPoint);
                                lblResults.Text += equipmentName + " " + pointName + addExistingPointSuccess + "<br />";
                            }
                            catch (SqlException sqlEx)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding existing equipment_point.", sqlEx);
                                lblAddExistingError.Text += equipmentName + " " + pointName + addExistingPointFailed + "<br />";
                            }
                            catch (Exception ex)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding existing equipment_point.", ex);
                                lblAddExistingError.Text += equipmentName + " " + pointName + addExistingPointFailed + "<br />";
                            }
                        }
                        else
                            lblAddExistingError.Text += equipmentName + " " + pointName + addExistingPointTypeExists + "<br />";
                    }
                    else
                    {
                        lblAddExistingError.Text += equipmentName + " " + pointName + addExistingPointTypeNotAllowed + "<br />";
                    }
                }
            }

            #region binders

                private void BindBuildings(DropDownList ddl, DropDownList ddl2, DropDownList ddl3)
                {
                    var buildings = siteUser.VisibleBuildings;

                    BindDDL(ddl, buildingName, bid, buildings, true, null);
                    BindDDL(ddl2, buildingName, bid, buildings, true, null);
                    BindDDL(ddl3, buildingName, bid, buildings, true, null);
                }

                private void BindEquipmentClass(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "View All..." : "Select building first...", Value = "-1", Selected = true };

                    BindDDL(ddl, equipmentClassName, equipmentClassId, (bid != -1) ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid) : null, true, lItem);
                }

                private void BindPoints(ListBoxExtension lbe, Int32 eid)
                {
                    var concatenatedControl = DataMgr.PointDataMapper.GetAllPointsByEIDWithAssociatedEquipment(eid);
    
                    concatenatedControl.SetConcatenatedName(",");

                    BindListBoxExtension(lbe, concatenatedName, id, concatenatedControl);
                }

                private void BindEquipment(DropDownList ddl, Int32[] ids = null)
                {
                    var lItem = new ListItem() { Text = (ids != null && ids.Any() && ids[0] != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };
                    var equipment = (ids.Any() && ids.Count() > 1) ? DataMgr.EquipmentDataMapper.GetAllActiveEquipmentByBuildingIDAndEquipmentClassID(ids[0], ids[1]) : DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(ids[0]);

                    BindDDL(ddl, equipmentName, eid, equipment, true, lItem);
                }

                private void BindEquipmentListBox(ListBoxExtension lbe, Int32[] ids = null)
                {
                    var equipment = Enumerable.Empty<CW.Data.Equipment>();

                    if (ids != null) equipment = (ids.Any() && ids.Count() > 1) ? DataMgr.EquipmentDataMapper.GetAllActiveEquipmentByBuildingIDAndEquipmentClassID(ids[0], ids[1]) : DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(ids[0]);

                    BindListBoxExtension(lbe, equipmentName, eid, equipment);
                }
            
                //should be centralized, but ListBoxExtension class/namespace is not accessible from CW.Utility
                private void BindListBoxExtension(ListBoxExtension lbe, String textField, String valueField, IEnumerable<Object> data, Boolean clearItems = true)
                {
                    if (clearItems) lbe.Items.Clear();

                    if (data.Any())
                    {
                        lbe.DataTextField = textField;
                        lbe.DataValueField = valueField;
                        lbe.DataSource = data;
                        lbe.DataBind();
                    }
                }

            #endregion

        #endregion
    }
}