﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="UnassociateExistingPoints.ascx.cs" Inherits="CW.Website._controls.admin.Points.UnassociateExistingPoints" %>
<%@ Register src="~/_controls/DropDowns.ascx" tagname="DropDowns" tagprefix="CW" %>

<asp:Panel ID="pnlUnassociateExistingPoints" runat="server" DefaultButton="btnUnassociateExistingPoints">

  <h2>Unassociate Existing Points</h2>

  <p>Please select a building then equipment class in order to unassociate existing points from an equipment if they are associated to more than one.</p> 

  <div>
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblUnassociateExistingError" CssClass="errorMessage" runat="server" />

    <div class="divForm">
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>

    <CW:DropDowns runat="server" ID="dropDownsBulkUnassociate" validationGroup="UnassociateExistingPoints" StartViewMode="Building" ViewMode="EquipmentClass" OnBuildingChanged="DropDownsBuildingChanged" OnEquipmentClassChanged="DropDownsEquipmentClassChanged" />

    <div class="divForm">
      <label class="label">Equipment:</label>
      <asp:ListBox ID="lbEquipment" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>

    <div class="divForm">
      <asp:LinkButton  ID="btnPopulateAssociatedPoints" runat="server" CssClass="lnkButton" Text="Refresh" OnClick="PopulateButtonClick" ValidationGroup="UnassociateExistingPoints" />
    </div>

    <div class="divForm">
      <label class="label">Associated Points:</label>
      <asp:ListBox ID="lbAssociatedPoints" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

      <div class="divArrows">
        <asp:ImageButton CssClass="up-arrow" ID="btnUp" ImageUrl="~/_assets/images/up-arrow.png" runat="server" OnClick="BulkUnassociateUpButtonClick" />
        <asp:ImageButton CssClass="down-arrow" ID="btnDown" ImageUrl="~/_assets/images/down-arrow.png" runat="server" OnClick="BulkUnassociateDownButtonClick" />
      </div>

      <label class="label">Points To Unassociate:</label>
      <asp:ListBox ID="lbPointsToUnassociate" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />
    </div>

    <asp:LinkButton ID="btnUnassociateExistingPoints" runat="server" CssClass="lnkButton" Text="Unassociate" OnClick="BulkUnassociateButtonClick" ValidationGroup="UnassociateExistingPoints" />

  </div>

</asp:Panel>