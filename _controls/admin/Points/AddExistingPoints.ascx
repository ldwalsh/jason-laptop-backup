﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AddExistingPoints.ascx.cs" Inherits="CW.Website._controls.admin.Points.AddExistingPoints" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

<asp:Panel ID="pnlAddExistingPoints" runat="server" DefaultButton="btnAddExistingPoints">

  <h2>Add Existing Points</h2>

  <p>Please select a building then equipment in order to add existing points to different pieces of equipment.</p> 
  
  <div>              
    <a id="lnkSetFocus" runat="server"></a>                                       
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddExistingError" CssClass="errorMessage" runat="server" />
                                        
    <br />

    <h3>From Equipment</h3>

    <p>Select the equipment to add the existing points from.</p>                                                                                   

    <div class="divForm">
      <label class="label">*Select From Equipment Mode:</label>
      <asp:RadioButtonList ID="rblFromEquipmentMode" CssClass="radio" OnSelectedIndexChanged="rblFromEquipmentMode_OnSelectIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
        <asp:ListItem Text="Individual" Selected="true" Value="Individual" />
        <asp:ListItem Text="Multiple" Value="Multiple" />
      </asp:RadioButtonList>
      <p>(Note: Multiple mode adds all points from selected equipment.)</p>
    </div>

    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div> 
                                        
    <div id="divIndividualEquipment" runat="server">
      <div class="divForm">   
        <label class="label">*From Building:</label>    
        <asp:DropDownList ID="ddlAddExistingFromBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddExistingFromBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
      </div>  
      <div class="divForm">   
        <label class="label">From Equipment Class:</label>    
        <asp:DropDownList ID="ddlAddExistingFromEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddExistingFromEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" /> 
      </div>
      <div class="divForm">   
        <label class="label">*From Equipment:</label>    
        <asp:DropDownList ID="ddlAddExistingFromEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddExistingFromEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server" /> 
      </div>                                                                                   
      <div class="divForm">   
        <label class="label">*Point:</label>                                                
        <div class="divListSearchExtender">
          <ajaxToolkit:ListSearchExtender id="lseAddExistingPoints" runat="server" TargetControlID="lbAddExistingPointsToEquipment" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true"/>
        </div>    
        <extensions:ListBoxExtension ID="lbAddExistingPointsToEquipment" AppendDataBoundItems="false" runat="server" SelectionMode="Multiple" CssClass="listboxWider" />                                           
      </div> 
    </div>

    <div id="divMulitipleEquipment" runat="server" visible="false">
      <div class="divForm">   
        <label class="label">*From Building:</label>    
        <asp:DropDownList ID="ddlAddExistingEquipmentPointsFromBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddExistingEquipmentPointsFromBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
      </div>
      <div class="divForm">   
        <label class="label">From Equipment Class:</label>    
        <asp:DropDownList ID="ddlAddExistingEquipmentPointsFromEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddExistingEquipmentPointsFromEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
      </div> 
      <div class="divForm">   
        <label class="label">*From Equipment:</label>
        <div class="divListSearchExtender">
          <ajaxToolkit:ListSearchExtender id="lseAddExistingEquipmentPointsToEquipment" runat="server" TargetControlID="lbAddExistingEquipmentPointsToEquipment" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true"/>
        </div>     
        <extensions:ListBoxExtension ID="lbAddExistingEquipmentPointsToEquipment" AppendDataBoundItems="false" runat="server" SelectionMode="Multiple" CssClass="listbox" /> 
      </div> 
    </div>

    <h3>To Equipment</h3>

    <p>Select the equipment to add the existing points to.</p> 
                     
    <div class="divForm">   
      <label class="label">*To Building:</label>    
      <asp:DropDownList ID="ddlAddExistingToBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddExistingToBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">To Equipment Class:</label>    
      <asp:DropDownList ID="ddlAddExistingToEquipmentClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddExistingToEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*To Equipment:</label>                                             
      <div class="divListSearchExtender">
        <ajaxToolkit:ListSearchExtender id="lseAddExisting" runat="server" TargetControlID="lbAddExistingToEquipment" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true"/>
      </div>     
      <extensions:ListBoxExtension ID="lbAddExistingToEquipment" AppendDataBoundItems="false" runat="server" SelectionMode="Multiple" CssClass="listbox" />                                                                  
    </div>
                                                             
    <asp:LinkButton CssClass="lnkButton" ID="btnAddExistingPoints" runat="server" Text="Add Existing Points" OnClick="addExistingPointsButton_Click" ValidationGroup="AddExistingPoints" />

  </div>
                                    
  <!--Ajax Validators-->           
  <asp:RequiredFieldValidator ID="addExistingFromBuildingRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlAddExistingFromBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddExistingPoints" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addExistingFromBuildingVCE" runat="server" BehaviorID="addExistingFromBuildingVCE" TargetControlID="addExistingFromBuildingRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  
  <asp:RequiredFieldValidator ID="addExistingFromEquipmentRFV" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="ddlAddExistingFromEquipment" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddExistingPoints" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addExistingFromEquipmentVCE" runat="server" BehaviorID="addExistingFromEquipmentVCE" TargetControlID="addExistingFromEquipmentRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                           
  <asp:RequiredFieldValidator ID="addExistingEquipmentPointsFromBuildingRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlAddExistingEquipmentPointsFromBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddExistingPoints" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addExistingEquipmentPointsFromBuildingVCE" runat="server" BehaviorID="addExistingEquipmentPointsFromBuildingVCE" TargetControlID="addExistingEquipmentPointsFromBuildingRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addExistingToBuildingRFV" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlAddExistingToBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddExistingPoints" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addExistingToBuildingVCE" runat="server" BehaviorID="addExistingToBuildingVCE" TargetControlID="addExistingToBuildingRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>