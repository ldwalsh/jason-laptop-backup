﻿using CW.Data;
using CW.Data.Models.EquipmentPoints;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._downloads;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Points
{
    public partial class PointDownloadArea : DownloadAreaBase<GetEquipmentPointsData, PointDownloadGenerator>
    {
        #region fields

            private String fnBid = PropHelper.G<Building>(_ => _.BID);
            
        #endregion

        #region properties

            #region overrides

                protected override Func<Dictionary<String, Object>, IEnumerable<GetEquipmentPointsData>> GetData { get { return GetAllPointsByBID; } }

                protected override Dictionary<String, Object> Parameters 
                {
                    get
                    {
                        var items = new Dictionary<String, Object>();

                        items.Add(fnBid, DropDownList.SelectedValue);
                        items.Add(PropHelper.G<Building>(_ => _.BuildingName), DropDownList.SelectedItem.Text);

                        return items;
                    }
                }

            #endregion

        #endregion

        #region methods

            public IEnumerable<GetEquipmentPointsData> GetAllPointsByBID(Dictionary<String, Object> parameters)
            {
                var bid = Convert.ToInt32(parameters[fnBid]);

                return DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointsWithPartialDataByBID(bid);
            }

        #endregion
    }
}