﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Control;
using CW.Utility.Extension;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Points
{
    public partial class BulkEditPoints : AdminUserControlBase
    {
        #region class

            public class BooleanValues
            {
                #region properties

                    public Boolean ChangePointName { get; set; }
                    public Boolean ChangeDSID { get; set; }
                    public Boolean ChangeReferenceID { get; set; }
                    public Boolean ChangePointTypeID { get; set; }
                    public Boolean ChangeTimeZone { get; set; }
                    public Boolean ChangeSamplingInterval { get; set; }
                    public Boolean ChangePower { get; set; }
                    public Boolean ChangeMultiply { get; set; }
                    public Boolean ChangeAddition { get; set; }
                    public Boolean ChangePowerMatlab { get; set; }
                    public Boolean ChangeMultiplyMatlab { get; set; }
                    public Boolean ChangeAdditionMatlab { get; set; }

                #endregion
            }

        #endregion

        #region constants

            const String bulkEditFailed = "Bulk edit points failed. Please contact an administrator.";
            const String noBulkEditPointsSelected = "No bulk edit points were selected.";
            const String noBulkEditFieldsSelected = "No bulk edit fields were selected.";

        #endregion

        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[]
                                    {
                                        KGSPointAdministration.TabMessages.AddPoint,
                                        KGSPointAdministration.TabMessages.EditPoint,
                                        KGSPointAdministration.TabMessages.DeletePoint
                                    };
                    }
                }

            #endregion

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblBulkEditError.Visible = false;

                if (!Page.IsPostBack)
                {
                    var buildings = siteUser.VisibleBuildings;

                    dropDownsBulkEdit.BindBuildingList(buildings);

                    BindAllDDLs();
                }
            }

            #region ddl events

                protected void DropDownsBuildingChanged(Object sender, EventArgs e)
                {
                    ClearListBoxes();

                    if (dropDownsBulkEdit.BID == null)
                    {
                        ResetEquipmentTypeAndEquipmentDDL();
                        ResetPointClassAndPointTypeDDL();
                        return;
                    }

                    dropDownsBulkEdit.BindEquipmentClassList(DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(dropDownsBulkEdit.BID.Value));
                }

                protected void DropDownsEquipmentClassChanged(Object sender, EventArgs e)
                {
                    ClearListBoxes();

                    if (dropDownsBulkEdit.EquipmentClassID == null)
                    {
                        ResetEquipmentTypeAndEquipmentDDL();
                        ResetPointClassAndPointTypeDDL();
                        return;
                    }

                    BindEquipmentTypes(ddlEquipmentType, (Int32)dropDownsBulkEdit.EquipmentClassID);
                    BindBulkEditPoints();
                }

                protected void DDLEquipmentTypeSelectedIndexChanged(Object sender, EventArgs e)
                {
                    ClearListBoxes();

                    if (dropDownsBulkEdit.BID == null)
                    {
                        BindEquipment(ddlEquipment, -1, -1);
                        return;
                    }

                    BindEquipment(ddlEquipment, dropDownsBulkEdit.BID.Value, Convert.ToInt32(ddlEquipmentType.SelectedValue));
                    ResetPointClassAndPointTypeDDL();
                    BindBulkEditPoints();
                }

                protected void DDLEquipmentSelectedIndexChanged(Object sender, EventArgs e)
                {
                    ResetPointClassAndPointTypeDDL();
                    ClearListBoxesAndBindData();
                }

                protected void DDLPointClassSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindPointTypes(ddlPointType, Convert.ToInt32(ddlPointClass.SelectedValue));
                    ClearListBoxesAndBindData();
                }

                protected void DDLPointTypeSelectedIndexChanged(Object sender, EventArgs e)
                {
                    ClearListBoxesAndBindData();
                }

                #region edit form ddl events

                    protected void DDLBulkEditPointClassSelectedIndexChanged(Object sender, EventArgs e)
                    {
                        BindPointTypesEditForm(ddlBulkEditPointType, Convert.ToInt32(ddlBulkEditPointClass.SelectedValue));
                    }

                #endregion

            #endregion

            #region button events

                protected void BulkEditPointsUpButtonClick(Object sender, EventArgs e)
                {
                    while (lbBulkEditPointsBottom.SelectedIndex != -1)
                    {
                        lbBulkEditPointsTop.Items.Add(lbBulkEditPointsBottom.SelectedItem);
                        lbBulkEditPointsBottom.Items.Remove(lbBulkEditPointsBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBulkEditPointsTop);
                }

                protected void BulkEditPointsDownButtonClick(Object sender, EventArgs e)
                {
                    while (lbBulkEditPointsTop.SelectedIndex != -1)
                    {
                        lbBulkEditPointsBottom.Items.Add(lbBulkEditPointsTop.SelectedItem);
                        lbBulkEditPointsTop.Items.Remove(lbBulkEditPointsTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBulkEditPointsBottom);
                }

                protected void BulkEditPointsButtonClick(Object sender, EventArgs e)
                {
                    var errorMsg = "Error during bulk edit points in kgs point administration.";

                    try
                    {
                        if (lbBulkEditPointsBottom.Items.Count == 0)
                        {
                            LabelHelper.SetLabelMessage(lblBulkEditError, noBulkEditPointsSelected, lnkSetFocus);
                            return;
                        }
                        else
                        {
                            var checkBoxList = FindControls<CheckBox>(true).Where(cbl => !cbl.ID.Contains("EditIsSubscriptionBased") && !cbl.ID.Contains("EditActive") && !cbl.ID.Contains("EditVisible")); //quick fix, we can sexy it up later if necessary

                            if (!checkBoxList.Where(cbl => cbl.Checked).Any())
                            {
                                LabelHelper.SetLabelMessage(lblBulkEditError, noBulkEditFieldsSelected, lnkSetFocus);
                                return;
                            }
                            else
                            {
                                Int32 pid, pointTypeID;

                                lblBulkEditError.Text = String.Empty;
                                lblResults.Text = String.Empty;

                                foreach (ListItem item in lbBulkEditPointsBottom.Items)
                                {
                                    var itemValues = item.Value.Split('_');
                                    var findReferenceID = txtBulkEditFindReferenceID.Text;
                                    var replaceReferenceID = txtBulkEditReplaceReferenceID.Text;

                                    if (!Int32.TryParse(itemValues[0], out pid)) { continue; }
                                    if (!Int32.TryParse(itemValues[1], out pointTypeID)) { continue; }

                                    var mPoint = SetPointFields(pid);
                                    var values = SetBooleanFields(pid, pointTypeID);
                                    var allowEdit = (String.IsNullOrWhiteSpace(values.Item2)) ? true : false;
                                    var pointName = item.Text;

                                    if (allowEdit)
                                    {
                                        DataMgr.PointDataMapper.UpdatePartialPoint(mPoint, findReferenceID, replaceReferenceID, values.Item1.ChangePointName, values.Item1.ChangeDSID, values.Item1.ChangeTimeZone, values.Item1.ChangePointTypeID,
                                                                                   values.Item1.ChangeSamplingInterval, chkBulkEditVerifyIsSubscriptionBased.Checked, values.Item1.ChangeReferenceID, values.Item1.ChangePower, values.Item1.ChangeMultiply, values.Item1.ChangeAddition,
                                                                                   values.Item1.ChangePowerMatlab, values.Item1.ChangeMultiplyMatlab, values.Item1.ChangeAdditionMatlab, chkBulkEditVerifyActive.Checked, chkBulkEditVerifyVisible.Checked);

                                        lblResults.Text += pointName + " has been successfully updated.<br />";
                                    }

                                    if (!String.IsNullOrWhiteSpace(values.Item2)) lblBulkEditError.Text += values.Item2 + pointName + "<br />";
                                }

                                if (lblBulkEditError.Text != String.Empty) LabelHelper.SetLabelMessage(lblBulkEditError, lblBulkEditError.Text, lnkSetFocus);

                                if (lblResults.Text != String.Empty)
                                {
                                    LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);

                                    OperationComplete(KGSPointAdministration.TabMessages.AddPoint);
                                    new ClearCachePoints().ClearCache(siteUser.CID, DataMgr.BuildingDataMapper.GetAllBuildingIDsByCID(siteUser.CID).ToArray(), siteUser.UID);
                                }

                                checkBoxList.ForEach(c => c.Checked = false); 
                            }                                                       
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, errorMsg, sqlEx);
                        LabelHelper.SetLabelMessage(lblBulkEditError, bulkEditFailed, lnkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, errorMsg, ex);
                        LabelHelper.SetLabelMessage(lblBulkEditError, bulkEditFailed, lnkSetFocus);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    var adminUserControlForm = new AdminUserControlForm(this);
                    
                    ClearListBoxes();
                    adminUserControlForm.ClearTextBoxes();
                    adminUserControlForm.ClearCheckBoxes();

                    var ddls = new[] 
                    { 
                        dropDownsBulkEdit.GetDDL(DropDowns.ViewModeEnum.Building) ,
                        dropDownsBulkEdit.GetDDL(DropDowns.ViewModeEnum.EquipmentClass)
                    };

                    new DropDownSequencer(ddls).ResetSubDropDownsOf(dropDownsBulkEdit.GetDDL(DropDowns.ViewModeEnum.Building));

                    adminUserControlForm.ResetDropDownLists();
                    DropDownsBuildingChanged(null, null);
                }

            #endregion

            private void BindAllDDLs()
            {
                BindEquipmentTypes(ddlEquipmentType, -1);
                BindEquipment(ddlEquipment, -1, -1);
                BindPointClasses(ddlPointClass);
                BindPointTypes(ddlPointType, -1);

                BindDataSourcesEditForm(ddlBulkEditDataSource);
                BindTimeZonesEditForm(ddlBulkEditTimeZone);
                BindPointClassesEditForm(ddlBulkEditPointClass);
                BindPointTypesEditForm(ddlBulkEditPointType, -1);
            }

            private void ClearListBoxes()
            {
                lbBulkEditPointsTop.Items.Clear();
                lbBulkEditPointsBottom.Items.Clear();
            }

            private void ClearListBoxesAndBindData()
            {
                ClearListBoxes();
                BindBulkEditPoints();
            }

            private Point SetPointFields(Int32 pid)
            {
                var point = new Point();

                point.PID = pid;
                point.PointName = txtBulkEditPointName.Text;
                point.DSID = Convert.ToInt32(ddlBulkEditDataSource.SelectedValue);
                point.TimeZone = ddlBulkEditTimeZone.SelectedValue;
                point.PointTypeID = Convert.ToInt32(ddlBulkEditPointType.SelectedValue);
                point.SamplingInterval = !String.IsNullOrWhiteSpace(txtBulkEditSamplingInterval.Text) ? Convert.ToInt32(txtBulkEditSamplingInterval.Text) : 0;
                point.IsSubscriptionBased = chkBulkEditIsSubscriptionBased.Checked;
                point.Power = !String.IsNullOrWhiteSpace(txtBulkEditPower.Text) ? Convert.ToDouble(txtBulkEditPower.Text) : 0;
                point.Multiply = !String.IsNullOrWhiteSpace(txtBulkEditMultiply.Text) ? Convert.ToDouble(txtBulkEditMultiply.Text) : 0;
                point.Addition = !String.IsNullOrWhiteSpace(txtBulkEditAddition.Text) ? Convert.ToDouble(txtBulkEditAddition.Text) : 0;
                point.PowerMatlab = !String.IsNullOrWhiteSpace(txtBulkEditPowerMatlab.Text) ? Convert.ToDouble(txtBulkEditPowerMatlab.Text) : 0;
                point.MultiplyMatlab = !String.IsNullOrWhiteSpace(txtBulkEditMultiplyMatlab.Text) ? Convert.ToDouble(txtBulkEditMultiplyMatlab.Text) : 0;
                point.AdditionMatlab = !String.IsNullOrWhiteSpace(txtBulkEditAdditionMatlab.Text) ? Convert.ToDouble(txtBulkEditAdditionMatlab.Text) : 0;
                point.IsActive = chkBulkEditActive.Checked;
                point.IsVisible = chkBulkEditVisible.Checked;

                return point;
            }

            private Tuple<BooleanValues, String> SetBooleanFields(Int32 pid, Int32 pointTypeID)
            {
                var changePointTypeIDValues = SetChangePointTypeID(pid, pointTypeID);

                var values = new BooleanValues()
                {
                    ChangePointName = (chkBulkEditVerifyPointName.Checked && !String.IsNullOrEmpty(txtBulkEditPointName.Text)),
                    ChangePointTypeID = changePointTypeIDValues.Item1,
                    ChangeTimeZone = (chkBulkEditVerifyTimeZone.Checked && ddlBulkEditTimeZone.SelectedValue != "-1"),
                    ChangeSamplingInterval = (chkBulkEditVerifySamplingInterval.Checked && !String.IsNullOrEmpty(txtBulkEditSamplingInterval.Text)),                                        
                    ChangePower = (chkBulkEditVerifyPower.Checked && !String.IsNullOrEmpty(txtBulkEditPower.Text)),
                    ChangeMultiply = (chkBulkEditVerifyMultiply.Checked && !String.IsNullOrEmpty(txtBulkEditMultiply.Text)),
                    ChangeAddition = (chkBulkEditVerifyAddition.Checked && !String.IsNullOrEmpty(txtBulkEditAddition.Text)),
                    ChangePowerMatlab = (chkBulkEditVerifyPowerMatlab.Checked && !String.IsNullOrEmpty(txtBulkEditPowerMatlab.Text)),
                    ChangeMultiplyMatlab = (chkBulkEditVerifyMultiplyMatlab.Checked && !String.IsNullOrEmpty(txtBulkEditMultiplyMatlab.Text)),
                    ChangeAdditionMatlab = (chkBulkEditVerifyAdditionMatlab.Checked && !String.IsNullOrEmpty(txtBulkEditAdditionMatlab.Text)),
                };

                values = SetChangeDSIDAndOrChangeReferenceID(pid, values);

                return new Tuple<BooleanValues, String>(values, changePointTypeIDValues.Item2);
            }

            private BooleanValues SetChangeDSIDAndOrChangeReferenceID(Int32 pid, BooleanValues values)
            {
                //bulk edit datasource and referenceid find and replace. replace could be an empty string.
                //if editing both
                if (chkBulkEditVerifyDataSource.Checked && ddlBulkEditDataSource.SelectedValue != "-1" && chkBulkEditVerifyReferenceID.Checked && !String.IsNullOrEmpty(txtBulkEditFindReferenceID.Text))
                {
                    if (!DataMgr.PointDataMapper.DoesPointReferenceIDExistForDataSource(Convert.ToInt32(ddlBulkEditDataSource.SelectedValue), txtBulkEditFindReferenceID.Text))
                    {
                        values.ChangeDSID = true;
                        values.ChangeReferenceID = true;
                    }
                }
                //else if editing one or the other
                else if ((chkBulkEditVerifyDataSource.Checked && ddlBulkEditDataSource.SelectedValue != "-1") || (chkBulkEditVerifyReferenceID.Checked && !String.IsNullOrEmpty(txtBulkEditFindReferenceID.Text)))
                {
                    var currentPoint = DataMgr.PointDataMapper.GetPointByPID(pid);

                    //if bulk edit datasoruce only
                    if (chkBulkEditVerifyDataSource.Checked && ddlBulkEditDataSource.SelectedValue != "-1")
                    {
                        if (!DataMgr.PointDataMapper.DoesPointReferenceIDExistForDataSource(Convert.ToInt32(ddlBulkEditDataSource.SelectedValue), currentPoint.ReferenceID))
                            values.ChangeDSID = true;
                    }

                    //if bulk edit find and replace referenceid. 
                    if (chkBulkEditVerifyReferenceID.Checked && !String.IsNullOrEmpty(txtBulkEditFindReferenceID.Text))
                    {
                        if (!DataMgr.PointDataMapper.DoesPointReferenceIDExistForDataSource(currentPoint.DSID, txtBulkEditFindReferenceID.Text))
                            values.ChangeReferenceID = true;
                    }
                }

                return values;
            }

            private Tuple<Boolean, String> SetChangePointTypeID(Int32 pid, Int32 pointTypeID)
            {
                var values = new Tuple<Boolean, String>(false, String.Empty);

                if (chkBulkEditVerifyPointType.Checked && ddlBulkEditPointType.SelectedValue != "-1")
                {
                    var newPointTypeID = Convert.ToInt32(ddlBulkEditPointType.SelectedValue);
                    var pointTypeExists = false;

                    //if attempt to change point type
                    if ((pointTypeID != newPointTypeID))
                    {
                        //check if new point type is associated with all points equipments types. Have to check all equipment, so dont use the equipment type dropdown value.
                        if (!DataMgr.EquipmentTypeDataMapper.IsPointTypeAssociatedWithAllPointsEquipmentTypes(newPointTypeID, pid))
                        {
                            return new Tuple<Boolean, String>(false, "New point type is not allowed on one ore more associated equipments equipement types. ");
                        }

                        var isPointTypeEditSelectedUnassigned = DataMgr.PointTypeDataMapper.IsPointTypeUnassigned(Convert.ToInt32(ddlBulkEditPointType.SelectedValue));

                        //if attempt to change point type, then check if a point with the same point type already exists for any equipment not of class group associated with the point
                        if (!isPointTypeEditSelectedUnassigned)
                        {
                            var eids = DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointEIDsNotOfClassGroupByPID(pid);

                            foreach (var eid in eids)
                            {
                                if (DataMgr.EquipmentPointsDataMapper.DoesPointTypeExistForEquipment(eid, newPointTypeID))
                                    pointTypeExists = true;
                            }
                        }
                    }
                    
                    values = (pointTypeExists) ? new Tuple<Boolean, String>(false, "Point type exists for point ") : new Tuple<Boolean, String>(true, String.Empty);
                }

                return values;
            }

            private void ResetEquipmentTypeAndEquipmentDDL()
            {
                BindEquipmentTypes(ddlEquipmentType, -1);
                BindEquipment(ddlEquipment, -1, -1);
            }

            private void ResetPointClassAndPointTypeDDL()
            {
                ddlPointClass.SelectedIndex = 0;
                BindPointTypes(ddlPointType, Convert.ToInt32(ddlPointClass.SelectedValue));
            }

            #region binders

                private void BindEquipmentTypes(DropDownList ddl, Int32 equipmentClassID)
                {
                    var lItem = new ListItem() { Text = (equipmentClassID != -1) ? "Optionally select one..." : "Select equipment class first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "EquipmentTypeName", "EquipmentTypeID", (equipmentClassID != -1) ? DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByEquipmentClassID(equipmentClassID) : null, true, lItem);
                }

                private void BindEquipment(DropDownList ddl, Int32 bid, Int32 equipmentTypeID)
                {
                    var fieldsSelected = (bid != -1 && equipmentTypeID != -1);
                    var lItem = new ListItem() { Text = fieldsSelected ? "Optionally select one..." : "Select equipment type first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "EquipmentName", "EID", fieldsSelected ? DataMgr.EquipmentDataMapper.GetAllEquipmentByBIDAndEquipmentTypeID(bid, equipmentTypeID) : null, true, lItem);
                }

                private void BindPointClasses(DropDownList ddl)
                {
                    var lItem = new ListItem() { Text = "Optionally select one...", Value = "-1", Selected = true };

                    BindDDL(ddl, "PointClassName", "PointClassID", DataMgr.PointClassDataMapper.GetAllPointClassesByTypeEnabled(true, false, false), true, lItem);
                }

                private void BindPointTypes(DropDownList ddl, Int32 pointClassID)
                {
                    var lItem = new ListItem() { Text = "Optionally select one...", Value = "-1", Selected = true };

                    BindDDL(ddl, "PointTypeName", "PointTypeID", (pointClassID != -1) ? DataMgr.PointTypeDataMapper.GetAllPointTypesByPointClassIDAndPointEnabled(pointClassID)
                                                                                      : DataMgr.PointTypeDataMapper.GetAllPointTypes(), true, lItem);
                }

                #region edit form binders

                    private void BindDataSourcesEditForm(DropDownList ddl)
                    {
                        BindDDL(ddl, "DataSourceName", "DSID", DataMgr.DataSourceDataMapper.GetAllDataSourcesByCID(siteUser.CID), true);
                    }

                    private void BindTimeZonesEditForm(DropDownList ddl)
                    {
                        BindDDL(ddl, "DisplayName", "Id", TimeZoneInfo.GetSystemTimeZones(), true);
                    }

                    private void BindPointClassesEditForm(DropDownList ddl)
                    {
                        BindDDL(ddl, "PointClassName", "PointClassID", DataMgr.PointClassDataMapper.GetAllPointClassesByTypeEnabled(true, false, false), true);
                    }

                    private void BindPointTypesEditForm(DropDownList ddl, Int32 pointClassID)
                    {
                        var lItem = new ListItem() { Text = (pointClassID != -1) ? "Select one..." : "Select point class first...", Value = "-1", Selected = true };

                        BindDDL(ddl, "PointTypeName", "PointTypeID", (pointClassID != -1) ? DataMgr.PointTypeDataMapper.GetAllPointTypesByPointClassIDAndPointEnabled(pointClassID) : null, true, lItem);
                    }

                    private void BindBulkEditPoints()
                    {
                        var bid = (dropDownsBulkEdit.BID != null) ? dropDownsBulkEdit.BID.Value : -1;
                        var equipmentClassID = (dropDownsBulkEdit.EquipmentClassID != null) ? dropDownsBulkEdit.EquipmentClassID.Value : -1;
                        var equipmentTypeID = (ddlEquipmentType.SelectedValue != "-1") ? (Int32?)Convert.ToInt32(ddlEquipmentType.SelectedValue) : null;
                        var eid = (ddlEquipment.SelectedValue != "-1") ? (Int32?)Convert.ToInt32(ddlEquipment.SelectedValue) : null;
                        var pointClassID = (ddlPointClass.SelectedValue != "-1") ? (Int32?)Convert.ToInt32(ddlPointClass.SelectedValue) : null;
                        var pointTypeID = (ddlPointType.SelectedValue != "-1") ? (Int32?)Convert.ToInt32(ddlPointType.SelectedValue) : null;
                        var data = DataMgr.PointDataMapper.GetAllPointsByIDs(bid, equipmentClassID, equipmentTypeID, eid, pointClassID, pointTypeID);
                        var concatenatedControl = data.Cast<GetConcatenatedControlData>().Select(qd => qd);

                        concatenatedControl.SetConcatenatedName(",");
                        concatenatedControl.SetConcatenatedValue("_");

                        lbBulkEditPointsTop.Items.Clear();

                        lbBulkEditPointsTop.DataTextField = "ConcatenatedName";
                        lbBulkEditPointsTop.DataValueField = "ConcatenatedValue";
                        lbBulkEditPointsTop.DataSource = concatenatedControl;
                        lbBulkEditPointsTop.DataBind();
                    }

                #endregion

            #endregion

        #endregion
    }
}