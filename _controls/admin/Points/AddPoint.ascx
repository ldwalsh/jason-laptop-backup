﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AddPoint.ascx.cs" Inherits="CW.Website._controls.admin.Points.AddPoint" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Panel ID="pnlAddPoint" runat="server" DefaultButton="btnAddPoint">

  <h2>Add New Point</h2>

  <div>
                       
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />

    <div class="divForm">
      <label class="label">*Point Name:</label>
      <asp:TextBox ID="txtAddPointName" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Point Class:</label>    
      <asp:DropDownList ID="ddlAddPointClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddPointClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />                               
      <p>(Note: Only point classes that contain a point type with point enabled are shown.)</p>
    </div>                                                                                                         
    <div class="divForm">   
      <label class="label">*Point Type:</label>    
      <asp:DropDownList ID="ddlAddPointType" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
      <p>(Note: More than one of the same point type cannot be associated to a peice of equipment unless the point is an unassigned type.)</p>
      <p>(Note: More than one of the same point type cannot be associated to a peice of equipment unless the equipment is a group class.)</p>
    </div>   
    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>                                                  
    <div class="divForm">
      <label class="label">*Data Source:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlAddDataSource" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Building (Initial Association):</label>    
      <asp:DropDownList ID="ddlAddBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>                                                                                                         
    <div class="divForm">   
      <label class="label">*Equipment (Initial Association):</label>    
      <asp:DropDownList ID="ddlAddEquipment" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
      <p>(Note: A point must be added to an intial piece of equipment, it cannot be added to the system without an association.)</p>                                                
    </div> 
    <div class="divForm">
      <label class="label">*Time Zone:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlAddTimeZone" AppendDataBoundItems="true" runat="server" />
      <p>(Note: This value is used to accurately insert raw data using this timezone.)</p>
      <p>(Note: The interpreter uses this time zone rather than the buildings times zone because a point can be used for more than one building which could have different timezones.)</p>
      <p>(Note: Analyses are ran based on the latest points timezone associated with the analyses, excluding equipment inheritance.)</p>
    </div>                                                                                                          
    <div class="divForm">
      <label class="label">*ReferenceID:</label>
      <asp:TextBox ID="txtAddReferenceID" CssClass="textbox" MaxLength="250" runat="server" />
      <p>(Note: This id should come from the data source.)</p>
    </div>    
    <div class="divForm">
      <label class="label">*Sampling Interval (Default=5):</label>
      <asp:TextBox CssClass="textbox" ID="txtAddSamplingInterval" Text="5" runat="server" MaxLength="10" />
      <p>(Note: Sampling interval is in minutes, and cannot be negative or zero. It must be set as either 1 minute or any interval divisable by 5.)</p>
      <p>(Note: Sampling interval is used for the following: Performance Dashboard meters, Analysis Builder plot interpolation, Analysis Builder expression synchronization, and Kiosk building performance.)</p>
    </div>  
    <div class="divForm">
      <label class="label">*Subscription Based:</label>
      <asp:CheckBox ID="chkAddIsSubscriptionBased" CssClass="checkbox" runat="server" />                                                        
      <p>(Warning: Setting a point as subscription based will inform the data transfer services to use optimized polling subscriptions which must be configured first.)</p>
    </div>                                                                                                                                                                                                                                           
    <div class="divForm">
      <label class="label">*Rel. Percent Error (Default=0):</label>
      <asp:TextBox CssClass="textbox" ID="txtAddRelPctError" Text="0" runat="server" MaxLength="10" />
    </div>                                                                                                        
    <div class="divForm">
      <br />
      <p>Point equation: multiplier*x^power+addition</p>
      <p>(Warning: Changes to storage power, multiply, or addition will alter the value of the incoming data when stored.)</p>                                                    
      <p>(Warning: Changes to analyses power, multiply, or addition will alter the value of the data when analyized and in the presentation of the alarms module.)</p>                                                   
      <label class="label">*Power (Storage) (Default=1):</label>
      <asp:TextBox CssClass="textbox" ID="txtAddPower" Text="1" runat="server" MaxLength="10" />
    </div>  
    <div class="divForm">
      <label class="label">*Multiply (Storage) (Default=1):</label>
      <asp:TextBox CssClass="textbox" ID="txtAddMultiply" Text="1" runat="server" MaxLength="10" />
    </div>                                                     
    <div class="divForm">
      <label class="label">*Addition (Storage) (Default=0):</label>
      <asp:TextBox CssClass="textbox" ID="txtAddAddition" Text="0" runat="server" MaxLength="10" />
    </div>                                                                                                                                                                                                                      
    <div class="divForm">
      <label class="label">*Power (Analyses) (Default=1):</label>
      <asp:TextBox CssClass="textbox" ID="txtAddPowerMatlab" Text="1" runat="server" MaxLength="10" />
    </div>  
    <div class="divForm">
      <label class="label">*Multiply (Analyses) (Default=1):</label>
      <asp:TextBox CssClass="textbox" ID="txtAddMultiplyMatlab" Text="1" runat="server" MaxLength="10" />
    </div>                                                     
    <div class="divForm">
      <label class="label">*Addition (Analyses) (Default=0):</label>
      <asp:TextBox CssClass="textbox" ID="txtAddAdditionMatlab" Text="0" runat="server" MaxLength="10" />
    </div> 
    <div class="divForm">
      <label class="label">Date Created:</label>
      <telerik:RadDatePicker ID="txtAddDateCreated" runat="server"></telerik:RadDatePicker>        
      <p>(Note: This is an optional field, which is set to today by default. Please enter the correct backdate if you plan on uploading historical data. This cannot be changed later on.)</p>          
    </div> 

    <asp:LinkButton CssClass="lnkButton" ID="btnAddPoint" runat="server" Text="Add Point" OnClick="addPointButton_Click" ValidationGroup="AddPoint" />
        
  </div>
                                        
  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="addPointNameRequiredValidator" runat="server" ErrorMessage="Point Name is a required field." ControlToValidate="txtAddPointName" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPointNameRequiredValidatorExtender" runat="server" BehaviorID="addPointNameRequiredValidatorExtender" TargetControlID="addPointNameRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addPointClassRequiredValidator" runat="server" ErrorMessage="Point Class is a required field." ControlToValidate="ddlAddPointClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPointClassRequiredValidatorExtender" runat="server" BehaviorID="addPointClassRequiredValidatorExtender" TargetControlID="addPointClassRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addPointTypeRequiredValidator" runat="server" ErrorMessage="Point Type is a required field." ControlToValidate="ddlAddPointType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPointTypeRequiredValidatorExtender" runat="server" BehaviorID="addPointTypeRequiredValidatorExtender" TargetControlID="addPointTypeRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addDataSourceRequiredValidator" runat="server" ErrorMessage="Data Source is a required field." ControlToValidate="ddlAddDataSource" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addDataSourceRequiredValidatorExtender" runat="server" BehaviorID="addDataSourceRequiredValidatorExtender" TargetControlID="addDataSourceRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addBuildingRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlAddBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addBuildingRequiredValidatorExtender" runat="server" BehaviorID="addBuildingRequiredValidatorExtender" TargetControlID="addBuildingRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                 
  <asp:RequiredFieldValidator ID="addEquipmentRequiredValidator" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="ddlAddEquipment" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentRequiredValidatorExtender" runat="server" BehaviorID="addEquipmentRequiredValidatorExtender" TargetControlID="addEquipmentRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addTimeZoneRequiredValidator" runat="server" ErrorMessage="Time Zone is a required field." ControlToValidate="ddlAddTimeZone" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addTimeZoneRequiredValidatorExtender" runat="server" BehaviorID="addTimeZoneRequiredValidatorExtender" TargetControlID="addTimeZoneRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addReferenceIDRequiredValidator" runat="server" ErrorMessage="ReferenceID is a required field." ControlToValidate="txtAddReferenceID" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addReferenceIDRequiredValidatorExtender" runat="server" BehaviorID="addReferenceIDRequiredValidatorExtender" TargetControlID="addReferenceIDRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addSamplingIntervalFilteredTextBoxExtender" runat="server" TargetControlID="txtAddSamplingInterval" FilterType="Numbers" />
  <asp:RequiredFieldValidator ID="addSamplingIntervalRequiredValidator" runat="server" ErrorMessage="Sampling Interval is a required field." ControlToValidate="txtAddSamplingInterval" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addSamplingIntervalRequiredValidatorExtender" runat="server" BehaviorID="addSamplingIntervalRequiredValidatorExtender" TargetControlID="addSamplingIntervalRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="addSamplingIntervalCompareValidator" runat="server" ErrorMessage="Sampling Interval must be greater than 0." ControlToValidate="txtAddSamplingInterval" ValueToCompare="0" Type="Integer" Operator="GreaterThan" ValidationGroup="AddPoint" Display="None" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addSamplingIntervalCompareValidatorExtender" runat="server" BehaviorID="addSamplingIntervalCompareValidatorExtender" TargetControlID="addSamplingIntervalCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addRelPctErrorFilteredTextBoxExtender" runat="server" TargetControlID="txtAddRelPctError" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="addRelPctErrorRequiredValidator" runat="server" ErrorMessage="Relative Percent Error is a required field." ControlToValidate="txtAddRelPctError" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addRelPctErrorRequiredValidatorExtender" runat="server" BehaviorID="addRelPctErrorRequiredValidatorExtender" TargetControlID="addRelPctErrorRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="addRelPctErrorCompareValidator" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAddRelPctError" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addRelPctErrorCompareValidatorExtender" runat="server" BehaviorID="addRelPctErrorCompareValidatorExtender" TargetControlID="addRelPctErrorCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addPowerFilteredTextBoxExtender" runat="server" TargetControlID="txtAddPower" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="addPowerRequiredValidator" runat="server" ErrorMessage="Power is a required field." ControlToValidate="txtAddPower" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPowerRequiredValidatorExtender" runat="server" BehaviorID="addPowerRequiredValidatorExtender" TargetControlID="addPowerRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="addPowerCompareValidator" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAddPower" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPowerCompareValidatorExtender" runat="server" BehaviorID="addPowerCompareValidatorExtender" TargetControlID="addPowerCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addMultiplyFilteredTextBoxExtender" runat="server" TargetControlID="txtAddMultiply" FilterType="Custom, Numbers" ValidChars="-.," />                                                                                               
  <asp:RequiredFieldValidator ID="addMultiplyRequiredValidator" runat="server" ErrorMessage="Multiply is a required field." ControlToValidate="txtAddMultiply" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addMultiplyRequiredValidatorExtender" runat="server" BehaviorID="addMultiplyRequiredValidatorExtender" TargetControlID="addMultiplyRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="addMultiplyCompareValidator" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAddMultiply" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addMultiplyCompareValidatorExtender" runat="server" BehaviorID="addMultiplyCompareValidatorExtender" TargetControlID="addMultiplyCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addAdditionFilteredTextBoxExtender" runat="server" TargetControlID="txtAddAddition" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="addAdditionRequiredValidator" runat="server" ErrorMessage="Addition is a required field." ControlToValidate="txtAddAddition" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addAdditionRequiredValidatorExtender" runat="server" BehaviorID="addAdditionRequiredValidatorExtender" TargetControlID="addAdditionRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="addAdditionCompareValidator" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAddAddition" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addAdditionCompareValidatorExtender" runat="server" BehaviorID="addAdditionCompareValidatorExtender" TargetControlID="addAdditionCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addPowerMatlabFilteredTextBoxExtender" runat="server" TargetControlID="txtAddPowerMatlab" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="addPowerMatlabRequiredValidator" runat="server" ErrorMessage="Power is a required field." ControlToValidate="txtAddPowerMatlab" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPowerMatlabRequiredValidatorExtender" runat="server" BehaviorID="addPowerMatlabRequiredValidatorExtender" TargetControlID="addPowerMatlabRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="addPowerMatlabCompareValidator" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAddPowerMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addPowerMatlabCompareValidatorExtender" runat="server" BehaviorID="addPowerMatlabCompareValidatorExtender" TargetControlID="addPowerMatlabCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                                           
  <ajaxToolkit:FilteredTextBoxExtender ID="addMultiplyMatlabFilteredTextBoxExtender" runat="server" TargetControlID="txtAddMultiplyMatlab" FilterType="Custom, Numbers" ValidChars="-.," />                                                                                               
  <asp:RequiredFieldValidator ID="addMultiplyMatlabRequiredValidator" runat="server" ErrorMessage="Multiply is a required field." ControlToValidate="txtAddMultiplyMatlab" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addMultiplyMatlabRequiredValidatorExtender" runat="server" BehaviorID="addMultiplyMatlabRequiredValidatorExtender" TargetControlID="addMultiplyMatlabRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="addMultiplyMatlabCompareValidator" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAddMultiplyMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addMultiplyMatlabCompareValidatorExtender" runat="server" BehaviorID="addMultiplyMatlabCompareValidatorExtender" TargetControlID="addMultiplyMatlabCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addAdditionMatlabFilteredTextBoxExtender" runat="server" TargetControlID="txtAddAdditionMatlab" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="addAdditionMatlabRequiredValidator" runat="server" ErrorMessage="Addition is a required field." ControlToValidate="txtAddAdditionMatlab" SetFocusOnError="true" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addAdditionMatlabRequiredValidatorExtender" runat="server" BehaviorID="addAdditionMatlabRequiredValidatorExtender" TargetControlID="addAdditionMatlabRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="addAdditionMatlabCompareValidator" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtAddAdditionMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="AddPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addAdditionMatlabCompareValidatorExtender" runat="server" BehaviorID="addAdditionMatlabCompareValidatorExtender" TargetControlID="addAdditionMatlabCompareValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>                                    