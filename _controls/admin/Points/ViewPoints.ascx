﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViewPoints.ascx.cs" Inherits="CW.Website._controls.admin.Points.ViewPoints" %>
<%@ Register TagPrefix="CW" TagName="DropDowns" src="~/_controls/DropDowns.ascx" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register tagPrefix="CW" tagName="PointDownloadArea" src="~/_controls/admin/Points/PointDownloadArea.ascx" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Import Namespace="CW.Data.Models.EquipmentPoints" %>
<%@ Import Namespace="CW.Utility" %>

<CW:TabHeader runat="server" ID="tabHeader" Title="View Points">
  <RightAreaTemplate>
    <CW:PointDownloadArea runat="server" ID="PointDownloadArea" Visible="false" />
  </RightAreaTemplate>
</CW:TabHeader>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p> 

<div class="divForm">
  <label class="label">*View Points By Mode:</label>
  <asp:RadioButtonList ID="rblViewPointsByMode" CssClass="radio" OnSelectedIndexChanged="rblViewPointsByMode_SelectIndexChanged" AutoPostBack="true" RepeatDirection="Horizontal" runat="server">
    <asp:ListItem Text="Building"  Value="Building" />
    <asp:ListItem Text="EquipmentClass" Selected="true" Value="EquipmentClass" />
    <asp:ListItem Text="Equipment" Value="Equipment" />
  </asp:RadioButtonList>
</div>
<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div> 
<div class="divForm">
  <asp:Label CssClass="labelContent" ID="lblClientTotalPointCount" runat="server" />
</div>
<div class="divForm">
  <asp:Label CssClass="labelContent" ID="lblClientTotalEquipmentPointAssociationCount" runat="server" />
</div> 

<CW:DropDowns runat="server" ID="dropDowns" validationEnabled="true" ValidationGroup="Search" StartViewMode="Building" ViewMode="Equipment" OnBuildingChanged="dropDowns_BuildingChanged" OnEquipmentClassChanged="dropDowns_EquipmentClassChanged" OnEquipmentChanged="dropDowns_EquipmentChanged" />
<br/>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>
</asp:Panel>
                                                                                   
<div id="gridTbl">                                        
  <asp:GridView ID="grid" EnableViewState="true" runat="server">
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="true" />

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkEdit" Runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointName" HeaderText="Point Name">
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.PointName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.PointName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentName" HeaderText="Equipment Name">                                                      
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.EquipmentName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.EquipmentName), 15) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataSourceName" HeaderText="Data Source Name">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.DataSourceName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.DataSourceName), 15) %></label></ItemTemplate>                                    
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointTypeName" HeaderText="Point Type Name">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.PointTypeName)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.PointTypeName), 15) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingSettingBasedEngUnits" HeaderText="Eng Units">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.BuildingSettingBasedEngUnits)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.BuildingSettingBasedEngUnits), 8) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ReferenceID" HeaderText="ReferenceID">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.ReferenceID)) %>"><%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.ReferenceID), 8) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.IsActive)) %></ItemTemplate>                                                  
      </asp:TemplateField>

      <asp:TemplateField SortExpression="IsVisible" HeaderText="Visible">  
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.IsVisible)) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkDelete" Runat="server" Visible='<%# Convert.ToBoolean(GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.IsLastPointAssociationToEquipment))) %>' CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this point permanently?\n\nAlternatively you can deactivate a point temporarily instead.\n\nAlready analyzed data will not be deleted.\n\nThis check may take a while, as we need to check if any raw data exists for this point since it was created.');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkRemoveAssociation" Runat="server" Visible='<%# !Convert.ToBoolean(GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.IsLastPointAssociationToEquipment))) %>' CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to remove this equipment to point association permanently? Already analyzed data will not be deleted.');" OnClick="removeAssociationButton_Click" CommandArgument='<%# GetCellContent(Container, PropHelper.G<GetEquipmentPointsData>(_ => _.EPID)) %>' Text="Remove Association" />
        </ItemTemplate>
      </asp:TemplateField>                                                                                                                                                                        
    </Columns>        
  </asp:GridView>                                                                         
</div>                                    
<br />
<br />

<div>                                                            
  <!--SELECT POINT DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptPointDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Points Details</h2>
          <ul id="pointDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>                           
</div>                                     
                                                                     
<!--EDIT POINT PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdatePoint">                                                                                             
  <div>
    <h2>Edit Point</h2>
    <p>Any alterations to a point will be applied to every piece of equipment the point is associated with.</p>
    <p>Changes associated with incomming data will take effect at the <strong>top of the hour</strong> when the lookup table is refreshed.</p>
  </div>

  <div>  
    <a id="lnkSetFocusEdit" runat="server"></a>
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />               
    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />                                                
    <div class="divForm">
      <label class="label">*Point Name:</label>
      <asp:TextBox ID="txtEditPointName" CssClass="textbox" MaxLength="50" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Select Point Class:</label>    
      <asp:DropDownList ID="ddlEditPointClass" CssClass="dropdown" OnSelectedIndexChanged="ddlEditPointClass_SelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true" runat="server" />
      <p>(Note: Only point classes that contain a point type with point enabled are shown.)</p>
      <p>(Note: Engineering units are based on the point class.)</p>
    </div>  
    <div class="divForm">   
      <label class="label">*Select Point Type:</label>    
      <asp:DropDownList ID="ddlEditPointType" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
      <p>(Warning: You cannot change the point type if an analysis has been associated to the equipment requiring this points type. Equipment with class group requires at least one point with associated analysis input point type.)</p>                                                    
      <p>(Note: More than one of the same point type cannot be associated to a peice of equipment unless the point is an unassigned type.)</p>
      <p>(Note: More than one of the same point type cannot be associated to a peice of equipment unless the equipment is a group class.)</p>                                                     
      <asp:HiddenField ID="hdnEditPointTypeID" runat="server" Visible="false" />
    </div>
    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>                                                       
    <div class="divForm">   
      <label class="label">Data Source:</label>    
      <asp:HiddenField ID="hdnEditDataSourceID" runat="server" Visible="false" />   
      <asp:DropDownList CssClass="dropdown" ID="ddlEditDataSource" AppendDataBoundItems="true" runat="server" />
      <p>(Warning: Changing the datasource may result in a mis-match for incoming associated data.)</p>
      <p>(Note: You should not change the datasource if raw data exists for this point.)</p>                                                                                                                
    </div>                                                                                                      
    <div class="divForm">   
      <label class="label">Building (Selected Association):</label>    
      <asp:Label ID="lblEditBuilding" CssClass="labelContent" runat="server" />
    </div>                                                                                                                              
    <div class="divForm">   
      <label class="label">Equipment (Selected Association):</label>    
      <asp:Label ID="lblEditEquipment" CssClass="labelContent" runat="server" />
      <asp:HiddenField ID="hdnEditEID" runat="server" Visible="false" />
    </div> 
    <div class="divForm">
      <label class="label">*Time Zone:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlEditTimeZone" AppendDataBoundItems="true" runat="server" />
      <p>(Note: This value is used to accurately insert raw data using this timezone.)</p>
      <p>(Note: The interpreter uses this time zone rather than the buildings times zone because a point can be used for more than one building which could have different timezones.)</p>
      <p>(Note: Analyses are ran based on the latest points timezone associated with the analyses, excluding equipment inheritance.)</p>
      <p>(Warning: Changing the timezone will NOT alter the already existing recorded raw data times.)</p>
    </div>
    <div class="divForm">
      <label class="label">*ReferenceID:</label>
      <asp:TextBox ID="txtEditReferenceID" CssClass="textbox" MaxLength="250" runat="server" />
      <asp:HiddenField ID="hdnEditReferenceID" runat="server" Visible="false" />
      <p>(Warning: Changing the reference id may result in a mis-match for incoming associated data.)</p>
      <p>(Note: You should not change the reference id if raw data exists for this reference id.)</p>
    </div>
    <div class="divForm">
      <label class="label">*Sampling Interval (Default=5):</label>
      <asp:TextBox CssClass="textbox" ID="txtEditSamplingInterval" runat="server" MaxLength="10" />
      <p>(Note: Sampling interval is in minutes, and cannot be negative or zero. It must be set as either 1 minute or any interval divisable by 5.)</p>
      <p>(Note: Sampling interval is used for the following: Performance Dashboard meters, Analysis Builder plot interpolation, Analysis Builder expression synchronization, and Kiosk building performance.)</p>
    </div>  
    <div class="divForm">
      <label class="label">*Subscription Based:</label>
      <asp:CheckBox ID="chkEditIsSubscriptionBased" CssClass="checkbox" runat="server" />                                                        
      <p>(Warning: Setting a point as subscription based will inform the data transfer services to use optimized polling subscriptions which must be configured first.)</p>
    </div>                                                                                                                                                                                                                                               
    <div class="divForm">
      <label class="label">*Rel. Percent Error (Default=0):</label>
      <asp:TextBox CssClass="textbox" ID="txtEditRelPctError" runat="server" MaxLength="10" />
    </div>                                                                                                  
    <div class="divForm">
      <br />

      <p>Point equation: multiplier*x^power+addition</p>
      <p>(Warning: Changes to storage power, multiply, or addition will alter the value of the incoming data when stored.)</p>                                                    
      <p>(Warning: Changes to analyses power, multiply, or addition will alter the value of the data when analyized and in the presentation of the alarms module.)</p>                                                    
      <label class="label">*Power (Storage) (Default=1):</label>
      <asp:TextBox CssClass="textbox" ID="txtEditPower" runat="server" MaxLength="10" />
    </div>
    <div class="divForm">
      <label class="label">*Multiply (Storage) (Default=1):</label>
      <asp:TextBox CssClass="textbox" ID="txtEditMultiply" runat="server" MaxLength="10" />
    </div>                                                     
    <div class="divForm">
      <label class="label">*Addition (Storage) (Default=0):</label>
      <asp:TextBox CssClass="textbox" ID="txtEditAddition" runat="server" MaxLength="10" />
    </div>
    <div class="divForm">
      <label class="label">*Power (Analyses) (Default=1):</label>
      <asp:TextBox CssClass="textbox" ID="txtEditPowerMatlab" runat="server" MaxLength="10" />
    </div> 
    <div class="divForm">
      <label class="label">*Multiply (Analyses) (Default=1):</label>
      <asp:TextBox CssClass="textbox" ID="txtEditMultiplyMatlab" runat="server" MaxLength="10" />
    </div>                                                     
    <div class="divForm">
      <label class="label">*Addition (Analyses) (Default=0):</label>
      <asp:TextBox CssClass="textbox" ID="txtEditAdditionMatlab" runat="server" MaxLength="10" />
    </div>
    <div class="divForm">
      <label class="label">*Active:</label>
      <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                        
      <p>(Warning: Setting a point as inactive will not allow scheduled analyses to be put in the queue requiring this point as input, and no incoming data will be recorded.)</p>
      <p>(Warning: Setting a point as inactive will exclude this point from analyses.</p>  
    </div>
    <div class="divForm">
      <label class="label">*Visible:</label>
      <asp:CheckBox ID="chkEditVisible" CssClass="checkbox" runat="server" />                                                        
      <p>(Warning: Setting a point as not visible will hide the point on non administrative pages.</p>   
      <p>(Warning: Setting a point as not visible will exclude this point from analyses.</p>                                                       
    </div>
                                                                                                                                                                                   
    <asp:LinkButton CssClass="lnkButton" ID="btnUpdatePoint" runat="server" Text="Update Point" OnClick="updatePointButton_Click" ValidationGroup="EditPoint" />

  </div>
                                            
  <!--Ajax Validators-->
  <asp:RequiredFieldValidator ID="editPointNameRFV" runat="server" ErrorMessage="Point Name is a required field." ControlToValidate="txtEditPointName" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPointNameVCE" runat="server" BehaviorID="editPointNameVCE" TargetControlID="editPointNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editPointClassRFV" runat="server" ErrorMessage="Point Class is a required field." ControlToValidate="ddlEditPointClass" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPointClassVCE" runat="server" BehaviorID="editPointClassVCE" TargetControlID="editPointClassRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editPointTypRFV" runat="server" ErrorMessage="Point Type is a required field." ControlToValidate="ddlEditPointType" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPointTypeVCE" runat="server" BehaviorID="editPointTypeVCE" TargetControlID="editPointTypRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editDataSourceRFV" runat="server" ErrorMessage="Data Source is a required field." ControlToValidate="ddlEditDataSource" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editDataSourceVCE" runat="server" BehaviorID="editDataSourceVCE" TargetControlID="editDataSourceRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editTimeZoneRFV" runat="server" ErrorMessage="Time Zone is a required field." ControlToValidate="ddlEditTimeZone" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editTimeZoneVCE" runat="server" BehaviorID="editTimeZoneVCE" TargetControlID="editTimeZoneRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editReferenceIDRFV" runat="server" ErrorMessage="ReferenceID is a required field." ControlToValidate="txtEditReferenceID" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editReferenceIDVCE" runat="server" BehaviorID="editReferenceIDVCE" TargetControlID="editReferenceIDRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="editSamplingIntervalFTE" runat="server" TargetControlID="txtEditSamplingInterval" FilterType="Numbers" />
  <asp:RequiredFieldValidator ID="editSamplingIntervalRFV" runat="server" ErrorMessage="Sampling Interval is a required field." ControlToValidate="txtEditSamplingInterval" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editSamplingIntervalVCE" runat="server" BehaviorID="editSamplingIntervalVCE" TargetControlID="editSamplingIntervalRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editSamplingIntervalCV" runat="server" ErrorMessage="Sampling Interval must be greater than 0." ControlToValidate="txtEditSamplingInterval" ValueToCompare="0" Type="Integer" Operator="GreaterThan" ValidationGroup="EditPoint" Display="None" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editSamplingIntervalCVVCE" runat="server" BehaviorID="editSamplingIntervalCVVCE" TargetControlID="editSamplingIntervalCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                                                                                                                                                                            
  <ajaxToolkit:FilteredTextBoxExtender ID="editRelPctErrorFTE" runat="server" TargetControlID="txtEditRelPctError" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="editRelPctErrorRequiredValidator" runat="server" ErrorMessage="Relative Percent Error is a required field." ControlToValidate="txtEditRelPctError" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editRelPctErrorRequiredValidatorExtender" runat="server" BehaviorID="editRelPctErrorRequiredValidatorExtender" TargetControlID="editRelPctErrorRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editRelPctErrorCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtEditRelPctError" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editRelPctErrorCVE" runat="server" BehaviorID="editRelPctErrorCVE" TargetControlID="editRelPctErrorCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                          
  <ajaxToolkit:FilteredTextBoxExtender ID="editPowerFTE" runat="server" TargetControlID="txtEditPower" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="editPowerRFV" runat="server" ErrorMessage="Power is a required field." ControlToValidate="txtEditPower" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPowerVCE" runat="server" BehaviorID="editPowerVCE" TargetControlID="editPowerRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editPowerCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtEditPower" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPowerCVE" runat="server" BehaviorID="editPowerCVE" TargetControlID="editPowerCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                   
  <ajaxToolkit:FilteredTextBoxExtender ID="editMultiplyFTE" runat="server" TargetControlID="txtEditMultiply" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="editMultiplyRFV" runat="server" ErrorMessage="Multiply is a required field." ControlToValidate="txtEditMultiply" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editMultiplyVCE" runat="server" BehaviorID="editMultiplyVCE" TargetControlID="editMultiplyRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editMultiplyCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtEditMultiply" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editMultiplyCVE" runat="server" BehaviorID="editMultiplyCVE" TargetControlID="editMultiplyCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                  
  <ajaxToolkit:FilteredTextBoxExtender ID="editAdditionFTE" runat="server" TargetControlID="txtEditAddition" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="editAdditionRFV" runat="server" ErrorMessage="Addition is a required field." ControlToValidate="txtEditAddition" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editAdditionVCE" runat="server" BehaviorID="editAdditionVCE" TargetControlID="editAdditionRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editAdditionCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtEditAddition" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editAdditionCVE" runat="server" BehaviorID="editAdditionCVE" TargetControlID="editAdditionCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                             
  <ajaxToolkit:FilteredTextBoxExtender ID="editPowerMatlabFTE" runat="server" TargetControlID="txtEditPowerMatlab" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="editPowerMatlabRFV" runat="server" ErrorMessage="Power is a required field." ControlToValidate="txtEditPowerMatlab" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPowerMatlabVCE" runat="server" BehaviorID="editPowerMatlabVCE" TargetControlID="editPowerMatlabRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editPowerMatlabCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtEditPowerMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editPowerMatlabCVE" runat="server" BehaviorID="editPowerMatlabCVE" TargetControlID="editPowerMatlabCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                  
  <ajaxToolkit:FilteredTextBoxExtender ID="editMultiplyMatlabFTE" runat="server" TargetControlID="txtEditMultiplyMatlab" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="editMultiplyMatlabRFV" runat="server" ErrorMessage="Multiply is a required field." ControlToValidate="txtEditMultiplyMatlab" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editMultiplyMatlabVCE" runat="server" BehaviorID="editMultiplyMatlabVCE" TargetControlID="editMultiplyMatlabRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editMultiplyMatlabCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtEditMultiplyMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editMultiplyMatlabCVE" runat="server" BehaviorID="editMultiplyMatlabCVE" TargetControlID="editMultiplyMatlabCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                 
  <ajaxToolkit:FilteredTextBoxExtender ID="editAdditionMatlabFTE" runat="server" TargetControlID="txtEditAdditionMatlab" FilterType="Custom, Numbers" ValidChars="-.," />
  <asp:RequiredFieldValidator ID="editAdditionMatlabRFV" runat="server" ErrorMessage="Addition is a required field." ControlToValidate="txtEditAdditionMatlab" SetFocusOnError="true" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editAdditionMatlabVCE" runat="server" BehaviorID="editAdditionMatlabVCE" TargetControlID="editAdditionMatlabRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:CompareValidator ID="editAdditionMatlabCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtEditAdditionMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="EditPoint" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editAdditionMatlabCVE" runat="server" BehaviorID="editAdditionMatlabCVE" TargetControlID="editAdditionMatlabCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

</asp:Panel>