﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="PointDownloadArea.ascx.cs" Inherits="CW.Website._controls.admin.Points.PointDownloadArea" %>

<div class="divDownloadBox2">
  <asp:Label ID="lblError" CssClass="errorMessage" Visible="false" runat="server" />

  <div class="divDownload">
    <asp:ImageButton runat="server" CausesValidation="false" ID="imgDownload" CssClass="imgDownload" ImageUrl="/_assets/images/excel-icon.jpg" AlternateText="download" />
    <asp:LinkButton runat="server" CausesValidation="false" ID="lnkDownload" CssClass="lnkDownload" Text="Download Points" />
  </div>
</div>