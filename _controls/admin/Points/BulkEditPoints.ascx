﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BulkEditPoints.ascx.cs" Inherits="CW.Website._controls.admin.Points.BulkEditPoints" %>
<%@ Register src="~/_controls/DropDowns.ascx" tagname="DropDowns" tagprefix="CW" %>

<asp:Panel ID="pnlBulkEditPoints" runat="server" DefaultButton="btnBulkEditPoints"> 

  <h2>Bulk Edit Points</h2>

  <p>Please select a client, building, equpiment class, equipment type in order select and bulk edit point fields. This page only allows the bulk edit of the certain point fields. Points with point type unassigned will be much quicker to bulk edit a point type. If you are doing a bulk edit point type on points with an already assigned point type, try to limit your selection in order not to timeout.</p> 
  
  <div>              
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />                                    
    <asp:Label ID="lblBulkEditError" CssClass="errorMessage" runat="server" />
                                        
    <div class="divForm">
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div> 

    <CW:DropDowns runat="server" ID="dropDownsBulkEdit" validationGroup="BulkEditPoint" validationEnabled="true" StartViewMode="Building" ViewMode="EquipmentClass" OnBuildingChanged="DropDownsBuildingChanged" OnEquipmentClassChanged="DropDownsEquipmentClassChanged" />

    <div class="divForm">   
      <label class="label">Select Equipment Type:</label>    
      <asp:DropDownList ID="ddlEquipmentType" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="DDLEquipmentTypeSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Select Equipment:</label>    
      <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="DDLEquipmentSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Select Point Class:</label>    
      <asp:DropDownList ID="ddlPointClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="DDLPointClassSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Select Point Type:</label>    
      <asp:DropDownList ID="ddlPointType" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="DDLPointTypeSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">

      <label class="label">Available Points:</label> 

      <div class="divListSearchExtender">
        <ajaxToolkit:ListSearchExtender id="lseBulkEditPointsTop" runat="server" TargetControlID="lbBulkEditPointsTop" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true"/>   
      </div>
      <asp:ListBox ID="lbBulkEditPointsTop" AppendDataBoundItems="false" CssClass="listboxWider" runat="server" SelectionMode="Multiple" />

      <div class="divArrowsWider">
        <asp:ImageButton ID="imgUpArrow" CssClass="up-arrow" OnClick="BulkEditPointsUpButtonClick" ImageUrl="~/_assets/images/up-arrow.png" runat="server" CausesValidation="false" />
        <asp:ImageButton ID="imgDownArrow" CssClass="down-arrow" OnClick="BulkEditPointsDownButtonClick" ImageUrl="~/_assets/images/down-arrow.png" runat="server" CausesValidation="false" />
      </div>

      <label class="label">*Selected Points:</label>

      <asp:ListBox ID="lbBulkEditPointsBottom" AppendDataBoundItems="false" CssClass="listboxWider" runat="server" SelectionMode="Multiple" />

    </div>
    <hr />

    <h2>Optional Bulk Edit Fields</h2>

    <p>Please check, and select or fill in all fields you wish to bulk edit. Leave any fields unchecked, blank, and unselected if you do not wish to bulk edit them.</p>

    <div class="divForm">
      <label class="label">New Point Name:</label>
      <asp:TextBox ID="txtBulkEditPointName" CssClass="textbox" MaxLength="50" runat="server" />
      <asp:CheckBox ID="chkBulkEditVerifyPointName" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div> 
    <div class="divForm">
      <label class="label">New Data Source:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlBulkEditDataSource" AppendDataBoundItems="true" runat="server" />
      <asp:CheckBox ID="chkBulkEditVerifyDataSource" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div>
    <div class="divForm">
      <label class="label">New Time Zone:</label>
      <asp:DropDownList CssClass="dropdown" ID="ddlBulkEditTimeZone" AppendDataBoundItems="true" runat="server" />  
      <asp:CheckBox ID="chkBulkEditVerifyTimeZone" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
      <p>(Note: This value is used to accurately insert raw data using this timezone.)</p>
      <p>(Note: The interpreter uses this time zone rather than the buildings times zone because a point can be used for more than one building which could have different timezones.)</p>
      <p>(Warning: Changing the timezone will NOT alter the already existing recorded raw data times.)</p>
    </div>
    <div class="divForm">   
      <label class="label">New Point Class:</label>                                                     
      <asp:DropDownList ID="ddlBulkEditPointClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="DDLBulkEditPointClassSelectedIndexChanged" AutoPostBack="true" runat="server" />
      <p>(Note: Only point classes that contain a point type with point enabled are shown.)</p>
    </div>                                                                                                         
    <div class="divForm">   
      <label class="label">New Point Type:</label>                                                   
      <asp:DropDownList ID="ddlBulkEditPointType" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
      <asp:CheckBox ID="chkBulkEditVerifyPointType" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
      <p>(Note: More than one of the same point type cannot be associated to a peice of equipment unless the point is an unassigned type.)</p>
      <p>(Note: More than one of the same point type cannot be associated to a peice of equipment unless the equipment is a group class.)</p>
    </div> 
    <div class="divForm">
      <label class="label">New Sampling Interval:</label>
      <asp:TextBox CssClass="textbox" ID="txtBulkEditSamplingInterval" runat="server" MaxLength="10" />
      <asp:CheckBox ID="chkBulkEditVerifySamplingInterval" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div>
    <div class="divForm">
      <label class="label">New Subscription Based:</label>
      <asp:CheckBox ID="chkBulkEditIsSubscriptionBased" CssClass="checkbox" runat="server" /> |<asp:CheckBox ID="chkBulkEditVerifyIsSubscriptionBased" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                                   
      <p>(Warning: Setting a point as subscription based will inform the data transfer services to use optimized polling subscriptions which must be configured first.)</p>
    </div>
    <div class="divForm">
      <br />
      <p>Find and replace partial referenceid string.</p>
      <label class="label">Find Partial ReferenceID String:</label>
      <asp:TextBox ID="txtBulkEditFindReferenceID" CssClass="textbox" MaxLength="250" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Replace Partial ReferenceID String:</label>
      <asp:TextBox ID="txtBulkEditReplaceReferenceID" CssClass="textbox" MaxLength="250" runat="server" />
      <asp:CheckBox ID="chkBulkEditVerifyReferenceID" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                                                                                       
      <p>(Warning: Changing the reference id may result in a mis-match for incoming associated data.)</p>
      <p>(Note: You should not change the reference id if raw data exists for this reference id.)</p>
    </div>
    <div class="divForm">
      <br />
      <p>Point equation: multiplier*x^power+addition</p>
      <label class="label">New Power (Storage):</label>                                                 
      <asp:TextBox CssClass="textbox" ID="txtBulkEditPower" runat="server" MaxLength="10" />
      <asp:CheckBox ID="chkBulkEditVerifyPower" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div>  
    <div class="divForm">
      <label class="label">New Multiply (Storage):</label>                                                 
      <asp:TextBox CssClass="textbox" ID="txtBulkEditMultiply" runat="server" MaxLength="10" />
      <asp:CheckBox ID="chkBulkEditVerifyMultiply" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div>
    <div class="divForm">
      <label class="label">New Addition (Storage):</label>                                                 
      <asp:TextBox CssClass="textbox" ID="txtBulkEditAddition" runat="server" MaxLength="10" />
      <asp:CheckBox ID="chkBulkEditVerifyAddition" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div>
    <div class="divForm">
      <label class="label">New Power (Analyses):</label>                                                 
      <asp:TextBox CssClass="textbox" ID="txtBulkEditPowerMatlab" runat="server" MaxLength="10" />
      <asp:CheckBox ID="chkBulkEditVerifyPowerMatlab" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div>
    <div class="divForm">
      <label class="label">New Multiply (Analyses):</label>                                                 
      <asp:TextBox CssClass="textbox" ID="txtBulkEditMultiplyMatlab" runat="server" MaxLength="10" />
      <asp:CheckBox ID="chkBulkEditVerifyMultiplyMatlab" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div>
    <div class="divForm">
      <label class="label">New Addition (Analyses):</label>                                                 
      <asp:TextBox CssClass="textbox" ID="txtBulkEditAdditionMatlab" runat="server" MaxLength="10" />
      <asp:CheckBox ID="chkBulkEditVerifyAdditionMatlab" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>
    </div>
    <div class="divForm">
      <label class="label">New Active:</label>
      <asp:CheckBox ID="chkBulkEditActive" CssClass="checkbox" runat="server" /> |<asp:CheckBox ID="chkBulkEditVerifyActive" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                                   
      <p>(Warning: Setting a point as inactive will not allow scheduled analyses to be put in the queue requiring this point as input, and no incoming data will be recorded.)</p>
    </div>
    <div class="divForm">
      <label class="label">New Visible:</label>
      <asp:CheckBox ID="chkBulkEditVisible" CssClass="checkbox" runat="server" /> |<asp:CheckBox ID="chkBulkEditVerifyVisible" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                                      
      <p>(Warning: Setting a point as not visible will hide the point on non administrative pages.</p>                                                        
    </div>   

    <ajaxToolkit:FilteredTextBoxExtender ID="bulkEditSamplingIntervalFTBE" runat="server" TargetControlID="txtBulkEditSamplingInterval" FilterType="Numbers" />
    <asp:CompareValidator ID="bulkEditSamplingIntervalCV" runat="server" ErrorMessage="Sampling Interval must be greater than 0." ControlToValidate="txtBulkEditSamplingInterval" ValueToCompare="0" Type="Integer" Operator="GreaterThan" ValidationGroup="BulkEditPoint" Display="None" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditSamplingIntervalVCE" runat="server" BehaviorID="bulkEditSamplingIntervalVCE" TargetControlID="bulkEditSamplingIntervalCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <ajaxToolkit:FilteredTextBoxExtender ID="bulkEditPowerFTBE" runat="server" TargetControlID="txtBulkEditPower" FilterType="Custom, Numbers" ValidChars="-.," />
    <asp:CompareValidator ID="bulkEditPowerCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtBulkEditPower" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="BulkEditPoint" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditPowerCVE" runat="server" BehaviorID="bulkEditPowerCVE" TargetControlID="bulkEditPowerCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                                           
    <ajaxToolkit:FilteredTextBoxExtender ID="bulkEditMultiplyFTBE" runat="server" TargetControlID="txtBulkEditMultiply" FilterType="Custom, Numbers" ValidChars="-.," />                                                                                                                                       
    <asp:CompareValidator ID="bulkEditMultiplyCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtBulkEditMultiply" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="BulkEditPoint" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditMultiplyCVE" runat="server" BehaviorID="bulkEditMultiplyCVE" TargetControlID="bulkEditMultiplyCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                          
    <ajaxToolkit:FilteredTextBoxExtender ID="bulkEditAdditionFTBE" runat="server" TargetControlID="txtBulkEditAddition" FilterType="Custom, Numbers" ValidChars="-.," />                                       
    <asp:CompareValidator ID="bulkEditAdditionCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtBulkEditAddition" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="BulkEditPoint" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditAdditionCVE" runat="server" BehaviorID="bulkEditAdditionCVE" TargetControlID="bulkEditAdditionCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <ajaxToolkit:FilteredTextBoxExtender ID="bulkEditPowerMatlabFTBE" runat="server" TargetControlID="txtBulkEditPowerMatlab" FilterType="Custom, Numbers" ValidChars="-.," />
    <asp:CompareValidator ID="bulkEditPowerMatlabCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtBulkEditPowerMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="BulkEditPoint" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditPowerMatlabCVE" runat="server" BehaviorID="bulkEditPowerMatlabCVE" TargetControlID="bulkEditPowerMatlabCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <ajaxToolkit:FilteredTextBoxExtender ID="bulkEditMultiplyMatlabFTBE" runat="server" TargetControlID="txtBulkEditMultiplyMatlab" FilterType="Custom, Numbers" ValidChars="-.," />                                                                                                                                       
    <asp:CompareValidator ID="bulkEditMultiplyMatlabCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtBulkEditMultiplyMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="BulkEditPoint" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditMultiplyMatlabCVE" runat="server" BehaviorID="bulkEditMultiplyMatlabCVE" TargetControlID="bulkEditMultiplyMatlabCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                      
    <ajaxToolkit:FilteredTextBoxExtender ID="bulkEditAdditionMatlabFTBE" runat="server" TargetControlID="txtBulkEditAdditionMatlab" FilterType="Custom, Numbers" ValidChars="-.," />                                       
    <asp:CompareValidator ID="bulkEditAdditionMatlabCV" runat="server" Operator="DataTypeCheck" Type="Double" ControlToValidate="txtBulkEditAdditionMatlab" ErrorMessage="Invalid Numeric Format" Display="None" ValidationGroup="BulkEditPoint" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditAdditionMatlabCVE" runat="server" BehaviorID="bulkEditAdditionMatlabCVE" TargetControlID="bulkEditAdditionMatlabCV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <asp:LinkButton CssClass="lnkButton" ID="btnBulkEditPoints" runat="server" Text="Update Points" OnClick="BulkEditPointsButtonClick" ValidationGroup="BulkEditPoint" />

  </div>

</asp:Panel>