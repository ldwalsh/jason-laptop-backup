﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.EquipmentPoints;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Points
{
    public partial class UnassociateExistingPoints : AdminUserControlBase
    { 
        #region constants

            const String noPointsToUnassociateSelected = "No points to unassociate were add to the points to unassociate list.";
            const String pointCannotBeUnassociated = "Point {0} cannot be unassociated, point is part of analysis or group.";
            const String isLastEquipmentPointAssociationForPoint = "Point {0} cannot be unassociated from equipment {1}, as it is the last equipment point assocation.";
            const String pointUnassociated = "Point {0} has been successfully unassociated.";            
            const String unassociatePointFailed = "Point {0} unassociate failed. Please contact an administrator.";

        #endregion

        #region fields

            String eid = PropHelper.G<CW.Data.Equipment>(e => e.EID);
            String equipmentName = PropHelper.G<CW.Data.Equipment>(e => e.EquipmentName);
            String pointName = PropHelper.G<Point>(p => p.PointName);
            String equipmentPointName = PropHelper.G<GetEquipmentPointsData>(ep => ep.EquipmentPointName);
            String PIDPointTypeIDPointTypeNameEIDEPID = PropHelper.G<GetEquipmentPointsData>(ep => ep.PIDPointTypeIDPointTypeNameEIDEPID);

        #endregion

        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[] 
                        { 
                            KGSPointAdministration.TabMessages.AddPoint,
                            KGSPointAdministration.TabMessages.EditPoint,
                            KGSPointAdministration.TabMessages.DeletePoint
                        };
                    }
                }

            #endregion

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                lblResults.Visible = lblUnassociateExistingError.Visible = false;

                new AdminDropDowns(RadTabStrip, RadMultiPage, lblResults).PopulateAndSetDropDownLists(dropDownsBulkUnassociate, typeof(UnassociateExistingPoints).Name);
            }

            protected void Page_Load(Object sender, EventArgs e)
            {
                AttachToolTips();
            }

            #region ddl events

                protected void DropDownsBuildingChanged(Object sender, EventArgs e)
                {
                    ClearListBoxes();

                    if (dropDownsBulkUnassociate.BID == null) return;

                    dropDownsBulkUnassociate.BindEquipmentClassList(DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(dropDownsBulkUnassociate.BID.Value), false, true);
                    BindEquipmentListBox(lbEquipment, dropDownsBulkUnassociate.BID.Value, -1);
                }

                protected void DropDownsEquipmentClassChanged(Object sender, EventArgs e)
                {
                    ClearListBoxes();

                    if (dropDownsBulkUnassociate.EquipmentClassID == null) return;

                    BindEquipmentListBox(lbEquipment, dropDownsBulkUnassociate.BID.Value, dropDownsBulkUnassociate.EquipmentClassID.Value);
                }

            #endregion

            #region button events

                protected void BulkUnassociateUpButtonClick(Object sender, EventArgs e)
                {
                    while (lbPointsToUnassociate.SelectedIndex != -1)
                    {
                        lbAssociatedPoints.Items.Add(lbPointsToUnassociate.SelectedItem);
                        lbPointsToUnassociate.Items.Remove(lbPointsToUnassociate.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbAssociatedPoints);
                }

                protected void BulkUnassociateDownButtonClick(Object sender, EventArgs e)
                {
                    while (lbAssociatedPoints.SelectedIndex != -1)
                    {
                        lbPointsToUnassociate.Items.Add(lbAssociatedPoints.SelectedItem);
                        lbAssociatedPoints.Items.Remove(lbAssociatedPoints.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbPointsToUnassociate);
                }

                protected void PopulateButtonClick(Object sender, EventArgs e)
                {
                    var eids = lbEquipment.Items.Cast<ListItem>().Where(lb => lb.Selected).Select(lb => Convert.ToInt32(lb.Value));
                    var selectedPointsToUnassociate = ControlHelper.GetListBoxData<GetEquipmentPointsData>(lbPointsToUnassociate, GetEquipmentPoinstData(), (lb => eids.Contains(Convert.ToInt32(lb.Value.Split('|')[3]))));
                    var equipmentPoints = DataMgr.PointDataMapper.GetAllPointsByEIDWithAssociatedEquipment(eids);

                    //rebind any previously selected points to unassociate that have equipment that is still selected, to avoid another db call, Clear lb first. :)
                    lbPointsToUnassociate.Items.Clear();

                    if (selectedPointsToUnassociate.Any())
                    {
                        ControlHelper.BindListBox(lbPointsToUnassociate, pointName, PIDPointTypeIDPointTypeNameEIDEPID, selectedPointsToUnassociate);
                        equipmentPoints = equipmentPoints.Where(ep => !selectedPointsToUnassociate.Select(sp => sp.PID).Contains(ep.PID)).ToList();
                    }

                    ControlHelper.BindListBox(lbAssociatedPoints, equipmentPointName, PIDPointTypeIDPointTypeNameEIDEPID, equipmentPoints);

                    AttachToolTips();
                }

                protected void BulkUnassociateButtonClick(Object sender, EventArgs e)
                {
                    var equipmentPoints = ControlHelper.GetListBoxData<GetEquipmentPointsData>(lbPointsToUnassociate, GetEquipmentPoinstData());

                    lblResults.Text = lblUnassociateExistingError.Text = String.Empty;

                    if (!equipmentPoints.Any())
                    {
                        LabelHelper.SetLabelMessage(lblUnassociateExistingError, noPointsToUnassociateSelected, lnkSetFocus);
                        return;
                    }

                    foreach (var equipmentPoint in equipmentPoints)
                    {
                        var pid = equipmentPoint.PID;
                        var eid = equipmentPoint.EID;
                        var pointTypeID = equipmentPoint.PointTypeID;
                        var epid = equipmentPoint.EPID;
                        var pointName = equipmentPoint.PointName;

                        if (DataMgr.EquipmentPointsDataMapper.IsLastEquipmentPointAssociationForPoint(pid))
                        {
                            var equipmentName = DataMgr.EquipmentDataMapper.GetEquipmentNameByEID(eid);

                            LabelHelper.SetLabelMessage(lblUnassociateExistingError, String.Format(isLastEquipmentPointAssociationForPoint + "<br />", pointName, equipmentName), lnkSetFocus, true);
                        }
                        else
                        {
                            try
                            {
                                DataMgr.EquipmentPointsDataMapper.DeleteEquipmentPoint(epid);
                                lbPointsToUnassociate.Items.Remove(new ListItem() { Text = pointName, Value = equipmentPoint.PIDPointTypeIDPointTypeNameEIDEPID });

                                LabelHelper.SetLabelMessage(lblResults, String.Format(pointUnassociated + "<br />", pointName), lnkSetFocus, true);
                            }
                            catch (Exception ex)
                            {
                                LabelHelper.SetLabelMessage(lblUnassociateExistingError, String.Format(unassociatePointFailed + "<br />", pointName), lnkSetFocus, true);
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error unassociating existing points.", ex);
                            }
                        }

                    }

                    if (!String.IsNullOrWhiteSpace(lblResults.Text))
                    {
                        OperationComplete(KGSPointAdministration.TabMessages.AddPoint);
                        new ClearCachePoints().ClearCache(siteUser.CID, DataMgr.BuildingDataMapper.GetAllBuildingIDsByCID(siteUser.CID).ToArray(), siteUser.UID);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    ClearListBoxes();

                    var ddls = new[] 
                    { 
                        dropDownsBulkUnassociate.GetDDL(DropDowns.ViewModeEnum.Building),
                        dropDownsBulkUnassociate.GetDDL(DropDowns.ViewModeEnum.EquipmentClass)
                    };

                    new DropDownSequencer(ddls).ResetSubDropDownsOf(dropDownsBulkUnassociate.GetDDL(DropDowns.ViewModeEnum.Building));

                    new AdminUserControlForm(this).ResetDropDownLists();
                    DropDownsBuildingChanged(null, null);
                }

            #endregion

            private Func<ListItem, GetEquipmentPointsData> GetEquipmentPoinstData()
            {
                return (lb => new GetEquipmentPointsData
                {
                    PID = Convert.ToInt32(lb.Value.Split('|')[0]),
                    PointName = lb.Text,
                    PointTypeID = Convert.ToInt32(lb.Value.Split('|')[1]),
                    PointTypeName = lb.Value.Split('|')[2],
                    EID = Convert.ToInt32(lb.Value.Split('|')[3]),
                    EPID = Convert.ToInt32(lb.Value.Split('|')[4])
                });
            }

            private void AttachToolTips()
            {
                var delimeter = new Char[] { DataConstants.DefaultDelimiter };
                var index = 2;

                if (lbAssociatedPoints.Items.Count > 0) ControlHelper.AttachToolTipToListBox(lbAssociatedPoints, delimeter, index);
                if (lbPointsToUnassociate.Items.Count > 0) ControlHelper.AttachToolTipToListBox(lbPointsToUnassociate, delimeter, index);
            }

            private void ClearListBoxes()
            {
                lbEquipment.Items.Clear();
                lbAssociatedPoints.Items.Clear();
                lbPointsToUnassociate.Items.Clear();
            }

            #region binders

                private void BindEquipmentListBox(ListBox lb, Int32 bid, Int32 equipmentClassId)
                {
                    lb.Items.Clear();

                    var equipment = (equipmentClassId != -1) ? DataMgr.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(equipmentClassId, bid, (e => e.EquipmentType)) : DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid, (e => e.Equipment_Points));

                    ControlHelper.BindListBox(lb, equipmentName, eid, equipment);
                }

            #endregion

        #endregion
    }
}