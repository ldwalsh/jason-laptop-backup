﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.EquipmentPoints;
using CW.Data.Models.Point;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.Points
{
    public partial class ViewPoints : AdminUserControlGrid
    {
        #region constants

            const String dataExists = "Cannot delete point because data already exists.";
            const String deleteSuccessful = "Point deletion was successful.";
            const String deleteFailed = "Point deletion failed. Please contact an administrator.";
            
            const String removeAssociationSuccess = "Remove point assocation was successful.";
            const String removeAssociationFailed = "Remove point assocation failed. Please contact an administrator.";

            const String updateSuccessful = "Point update was successful.";
            const String updateFailed = "Point update failed. Please contact an administrator.";
            const String dataSourceChangeFailed = "Cannot change datasource because the reference id already exists for this datasource.";
            const String dataExistsReferenceIDChangeFailed = "Cannot change reference id because data already exists. Please contact an administrator.";
            
            const String pointTypeExistsChangeFailed = "Cannot change point type because a point with the same type already exists for one or more pieces of equipment that are not of class group.";
            const String pointTypeNotAllowedOnEquipmentType = "Cannot change point type because this point's point type is not allowed on one or more equipment with a given equipment type.";
            const String referenceIDExistsChangeFailed = "Cannot change reference id because the same reference id already exists for this datasource.";

        #endregion

        #region fields

            private enum PointDetails { Default };

        #endregion

        #region properties

            #region overrides

                protected override Int32 PageSize { get { return 100; } }

                protected override Boolean ContainsDropDownListControl { get { return true; } }

                protected override DropDowns DropDownListControl { get { return dropDowns; } }

                protected override String CurrentControlName { get { return typeof(ViewPoints).Name; } }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
                {
                    var hasSearchCriteria = !String.IsNullOrWhiteSpace(String.Join("", searchCriteria)); // TODO: NP: could control this higher up in the base 
                    var data = Enumerable.Empty<GetEquipmentPointsData>();

                    Int32 bid = -1;

                    if (dropDowns.BID != null) bid = (Int32)dropDowns.BID;

                    if (bid != -1)
                        data = (hasSearchCriteria) ? DataMgr.EquipmentPointsDataMapper.SearchAllEquipmentPointsByBID(searchCriteria.FirstOrDefault(), dropDowns.BID.Value) 
                                                   : DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointsWithPartialDataByBID(bid);

                    return data;
                }

                protected override IEnumerable<String> GridColumnNames
                {
                    get 
                    { 
                        return PropHelper.F<GetEquipmentPointsData>(_ => _.EPID, 
                                                                    _ => _.PID, 
                                                                    _ => _.EID,
                                                                    _ => _.EquipmentClassID,
                                                                    _ => _.PointName, 
                                                                    _ => _.EquipmentName, 
                                                                    _ => _.DataSourceName, 
                                                                    _ => _.PointTypeName,
                                                                    _ => _.BuildingSettingBasedEngUnits,
                                                                    _ => _.ReferenceID,
                                                                    _ => _.IsActive,
                                                                    _ => _.IsVisible,
                                                                    _ => _.IsLastPointAssociationToEquipment); 
                    }
                }

                protected override String NameField { get { return PropHelper.G<GetEquipmentPointsData>(_ => _.PointName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetEquipmentPointsData>(_ => _.EPID); } }

                protected override String Name { get { return typeof(Point).Name; } }

                protected override Type Entity { get { return typeof(GetEquipmentPointsData); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID, dropDowns.BID }); } }

                protected override IEnumerable<String> SearchGridColumnNames { get { return new[] { NameField, PropHelper.G<GetEquipmentPointsData>(_ => _.EquipmentName),  PropHelper.G<GetEquipmentPointsData>(_ => _.DataSourceName), PropHelper.G<GetEquipmentPointsData>(_ => _.PointTypeName),  PropHelper.G<GetEquipmentPointsData>(_ => _.ReferenceID) }; } }

                public override IEnumerable<Enum> ReactToChangeStateList { get { return new Enum[] { KGSPointAdministration.TabMessages.AddPoint }; } }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid, Int32 bid)
            {
                return new GridCacher.CacheKeyInfo(typeof(Point).Name + "." + typeof(GetEquipmentPointsData).Name, String.Join("|", new String[] { cid.ToString(), bid.ToString() }));
            }

            private DropDowns.ViewModeEnum ViewMode
            {
                get { return (DropDowns.ViewModeEnum)Enum.Parse(typeof(DropDowns.ViewModeEnum), rblViewPointsByMode.SelectedValue); }
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                lblClientTotalPointCount.Text = ("Client Total Point Count: " + DataMgr.PointDataMapper.GetAllPointsCountByCID(siteUser.CID));                
                lblClientTotalEquipmentPointAssociationCount.Text = ("Client Total Equipment Point Association Count: " + DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointsCountByCID(siteUser.CID));
            }

            private void Page_Load()
            {
                dropDowns.ViewMode = ViewMode;
                PointDownloadArea.DropDownList = dropDowns.GetDDL(DropDowns.ViewModeEnum.Building);
            }

            #region grid events

                #region overrides

                    protected override void OnGridDataBound(Object sender, GridViewRowEventArgs e)
                    {
                        //check if kgs admin or higher, hide edit and delete column if not
                        //bool kgsAdmin = RoleHelper.IsKGSAdminOrHigher(Session["UserRoleID"].ToString());
                        //gridPoints.Columns[1].Visible = kgsAdmin;
                        //gridPoints.Columns[8].Visible = kgsAdmin;      
                    }

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var epid = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());
                        var mEquipment_Point = DataMgr.EquipmentPointsDataMapper.GetPartialEquipmentPointByEPID(epid);

                        BindPointClasses(ddlEditPointClass);
                        BindPointTypes(ddlEditPointType, mEquipment_Point.PointClassID);
                        BindDataSources(ddlEditDataSource);
                        BindTimeZones(ddlEditTimeZone);

                        SetEquipmentPointIntoEditForm(mEquipment_Point);

                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                    protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
                    {
                        //Only allow deletion of point if no raw data has been recorded this point
                        //and a analysis has not been assigned to the equipment where that points type is required.

                        //**No need to check if analyzed data exists as it wouldnt exist without the raw data, thankfully.**

                        //Additionally, analyzed data should remain in our system unless otherwise purged.
                        //We may develop meta analyses and long term trending on that data.
                        var gs = gridCacher.Get();
                        var epid = Convert.ToInt32(grid.DataKeys[e.RowIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());
                        var pid = Convert.ToInt32(gs.Rows.Single(g => g.ID == epid.ToString()).Cells[PropHelper.G<GetPointsData>(_ => _.PID)]);

                        if (DataMgr.RawDataMapper.DoesRawDataExistForPoint(DataMgr.PointDataMapper.GetPointByPID(pid)))
                        {
                            LabelHelper.SetLabelMessage(lblDeleteError, dataExists, lnkSetFocus);
                            return;
                        }
                        else
                        {                                                            
                            try
                            {
                                DataMgr.EquipmentPointsDataMapper.DeleteAllEquipmentPointsByPID(pid);
                                DataMgr.PointDataMapper.DeletePoint(pid);
                                                                
                                OperationComplete(OperationType.Delete, deleteSuccessful, KGSPointAdministration.TabMessages.DeletePoint);
                            }
                            catch (Exception ex)
                            {
                                LabelHelper.SetLabelMessage(lblDeleteError, deleteFailed, lnkSetFocus);
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting point.", ex);
                            }                            
                        }
                    }

                #endregion

            #endregion

            #region button events

                protected void removeAssociationButton_Click(Object sender, EventArgs e)
                {
                    var linkButton = (LinkButton)sender;
                    var epid = Convert.ToInt32(linkButton.CommandArgument);
                    var equipmentPoint = DataMgr.EquipmentPointsDataMapper.GetPartialEquipmentPointByEPID(epid);

                    //get all buildings for the equipment the point was orginally assocaited with. this must be bfreo the delete.
                    var bids = DataMgr.EquipmentDataMapper.GetAllEquipmentAssociatedWithPID(equipmentPoint.PID).Select(e2 => e2.BID).Distinct().ToArray();

                    try
                    {
                        DataMgr.EquipmentPointsDataMapper.DeleteEquipmentPoint(Convert.ToInt32(linkButton.CommandArgument));
                        OperationComplete(OperationType.Delete, removeAssociationSuccess, KGSPointAdministration.TabMessages.DeletePoint);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblDeleteError, removeAssociationFailed, lnkSetFocus);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error removing point association.", ex);
                    }

                    //need to clear all building caches that the point was associated with on remove of association, because the point could have been associated with other buildings besides the current grid.
                    new ClearCachePoints().ClearCache(siteUser.CID, bids, siteUser.UID);
                }

                protected void updatePointButton_Click(Object sender, EventArgs e)
                {
                    //multiple points can have the same name.

                    var mPoint = new Point();
                    var pointTypeExists = false;

                    LoadEditFormIntoPoint(mPoint);

                    //if attempt to change point type
                    if (mPoint.PointTypeID != Convert.ToInt32(hdnEditPointTypeID.Value))
                    {
                        //----Equipment Type Point Type Not Allowed
                        var equipmentTypesIds = DataMgr.EquipmentDataMapper.GetAllEquipmentAssociatedWithPID(mPoint.PID).Select(eq => eq.EquipmentTypeID).Distinct();
                        var allowedEquipemntTypesIdsByNewPointType = DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypeIDsAssociatedWithPTID(mPoint.PointTypeID);

                        if (equipmentTypesIds.Except(allowedEquipemntTypesIdsByNewPointType).Any())
                        {
                            LabelHelper.SetLabelMessage(lblEditError, pointTypeNotAllowedOnEquipmentType, lnkSetFocusEdit);
                            return;
                        }

                        var isPointTypeEditSelectedUnassigned = DataMgr.PointTypeDataMapper.IsPointTypeUnassigned(Convert.ToInt32(ddlEditPointType.SelectedValue));

                        //if attempt to change point type,
                        //then check if a point with the same point type already exists for any equipment
                        //not of class group associated with the point
                        if ((mPoint.PointTypeID != Convert.ToInt32(hdnEditPointTypeID.Value)) && (!isPointTypeEditSelectedUnassigned))
                        {
                            var eids = DataMgr.EquipmentPointsDataMapper.GetAllEquipmentPointEIDsNotOfClassGroupByPID(mPoint.PID);

                            foreach (int eid in eids)
                            {
                                if (DataMgr.EquipmentPointsDataMapper.DoesPointTypeExistForEquipment(eid, mPoint.PointTypeID)) pointTypeExists = true;
                            }
                        }
                    }

                    if (pointTypeExists)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, pointTypeExistsChangeFailed, lnkSetFocusEdit);
                        return;
                    }
                    else if (mPoint.DSID != Convert.ToInt32(hdnEditDataSourceID.Value) && DataMgr.PointDataMapper.DoesPointReferenceIDExistForDataSource(mPoint.DSID, mPoint.ReferenceID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, dataSourceChangeFailed, lnkSetFocusEdit);
                        return;
                    }
                    else if (mPoint.ReferenceID != hdnEditReferenceID.Value && DataMgr.PointDataMapper.DoesPointReferenceIDExistForDataSource(mPoint.DSID, mPoint.ReferenceID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, referenceIDExistsChangeFailed, lnkSetFocusEdit);
                        return;
                    }
                    else
                    {
                        try
                        {
                            DataMgr.PointDataMapper.UpdatePoint(mPoint);

                            hdnEditPointTypeID.Value = mPoint.PointTypeID.ToString();
                            hdnEditDataSourceID.Value = mPoint.DSID.ToString();
                            hdnEditReferenceID.Value = mPoint.ReferenceID;

                            OperationComplete(OperationType.Update, updateSuccessful, KGSPointAdministration.TabMessages.EditPoint);

                            //need to clear all caches for buildings the points equipment is assocaited with, or the other fields in cache sets will be stale.
                            new ClearCachePoints().ClearCache(siteUser.CID, DataMgr.EquipmentDataMapper.GetAllEquipmentAssociatedWithPID(mPoint.PID).Select(e2 => e2.BID).Distinct().ToArray(), siteUser.UID);
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating point.", ex);
                        }
                    }
                }

            #endregion

            #region ddl events

                protected void dropDowns_BuildingChanged(Object sender, EventArgs e)
                {
                    var bid = dropDowns.BID;
                    var isBIDSelected = (bid != null);
                        
                    ControlHelper.ToggleVisibility(isBIDSelected, new Control[] { PointDownloadArea });    
                    PointDownloadArea.FindControl("lblError").Visible = false;
                    ResetUI();

                    if (!isBIDSelected) return;
                    
                    switch (rblViewPointsByMode.SelectedValue)
                    {
                        case "Building":
                            {
                                grid.Visible = true;
                                BindData();
                                break;
                            }
                        case "EquipmentClass":
                        case "Equipment":
                            {
                                dropDowns.BindEquipmentClassList(DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid.Value), true);
                                break;
                            }
                    }
                }

                protected void dropDowns_EquipmentClassChanged(Object sender, EventArgs e)
                {
                    ResetUI();

                    if (dropDowns.EquipmentClassID == null || dropDowns.BID == null) return;

                    switch (rblViewPointsByMode.SelectedValue)
                    {
                        case "EquipmentClass":
                            {
                                grid.Visible = true;
                                BindData();
                                break;
                            }
                        case "Equipment":
                            {
                                var ecid = dropDowns.EquipmentClassID.Value;
                                var bid = dropDowns.BID.Value;

                                dropDowns.BindEquipmentList(DataMgr.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(ecid, bid), true);
                                break;
                            }
                    }
                }

                protected void dropDowns_EquipmentChanged(Object sender, EventArgs e)
                {
                    ResetUI();

                    if (dropDowns.EID == null) return;

                    grid.Visible = (dropDowns.EID != null);
                    BindData();
                }

                protected void ddlEditPointClass_SelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindPointTypes(ddlEditPointType, Convert.ToInt32(ddlEditPointClass.SelectedValue));
                }

            #endregion

            #region rbl events

                protected void rblViewPointsByMode_SelectIndexChanged(Object sender, EventArgs e)
                {
                    var viewByMode = rblViewPointsByMode.SelectedValue;

                    ResetUI();

                    switch (viewByMode)
                    {
                        case "Building":
                            {
                                if (BindDataIfDDLsHaveValues(dropDowns.BID.HasValue, viewByMode)) return;

                                break;
                            }
                        case "EquipmentClass":
                            {
                                if (BindDataIfDDLsHaveValues(dropDowns.BID.HasValue && dropDowns.EquipmentClassID.HasValue, viewByMode)) return;

                                if (dropDowns.BID.HasValue)
                                {
                                    dropDowns.BindEquipmentClassList(DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(dropDowns.BID.Value), true);
                                    return;
                                }

                                ClearAndCreateDefaultItems(viewByMode);
                                break;
                            }
                        case "Equipment":
                            {
                                if (dropDowns.BID.HasValue && dropDowns.EquipmentClassID.HasValue)
                                {
                                    dropDowns.BindEquipmentList(DataMgr.EquipmentDataMapper.GetAllEquipmentByEquipmentClassAndBuildingID(dropDowns.EquipmentClassID.Value, dropDowns.BID.Value), true);
                                    return;
                                }

                                ClearAndCreateDefaultItems(viewByMode);
                                break;
                            }
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    base.InitializeControl();

                    //special cases
                    pnlSearch.Visible = (dropDowns.BID != null || dropDowns.EquipmentClassID != null || dropDowns.EID != null);
                    PointDownloadArea.Visible = (AdminModeEnum == admin.AdminModeEnum.KGS || dropDowns.BID != null); 
                }

                protected override void SetDataForDetailsViewFromDB()
                {
                    var equipmentPoint = DataMgr.EquipmentPointsDataMapper.GetIEnumerablePartialEquipmentPointByEPID(CurrentID).First();

                    SetEquipmentPointIntoViewState(equipmentPoint);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(PointDetails.Default), rptPointDetails);
                }

            #endregion

            private Boolean BindDataIfDDLsHaveValues(Boolean hasValues, String selectedValue)
            {
                if (hasValues)
                {
                    if (selectedValue == "Building")
                    {
                        if (dropDowns.GetDDL(DropDowns.ViewModeEnum.EquipmentClass).Visible) dropDowns.GetDDL(DropDowns.ViewModeEnum.EquipmentClass).SelectedIndex = 0;
                        if (dropDowns.GetDDL(DropDowns.ViewModeEnum.Equipment).Visible) dropDowns.GetDDL(DropDowns.ViewModeEnum.Equipment).SelectedIndex = 0;
                    }
                    else if (selectedValue == "EquipmentClass")
                    {
                        if (dropDowns.GetDDL(DropDowns.ViewModeEnum.Equipment).Visible) dropDowns.GetDDL(DropDowns.ViewModeEnum.Equipment).SelectedIndex = 0;
                    }

                    grid.Visible = true;
                    BindData();
                }

                return hasValues;
            }

            private void ClearAndCreateDefaultItems(String selectedValue)
            {
                var viewMode = (selectedValue == "EquipmentClass") ? DropDowns.ViewModeEnum.EquipmentClass : DropDowns.ViewModeEnum.Equipment;
                var text = (selectedValue == "EquipmentClass") ? "Select building first..." : "Select equipment class first...";

                DropDownSequencer.ClearAndCreateDefaultItems(dropDowns.GetDDL(viewMode), true, false, false, text);
            }

            private void SetEquipmentPointIntoViewState(GetEquipmentPointsData equipmentPoint)
            {
                SetEquipmentPointDetails(equipmentPoint);
            }

            private void SetEquipmentPointDetails(GetEquipmentPointsData equipmentPoint)
            {                                                                           
                SetKeyValuePairItem("Point Name", equipmentPoint.PointName);
                SetKeyValuePairItem("Point Class", equipmentPoint.PointClassName);
                SetKeyValuePairItem("Point Class Description", equipmentPoint.PointClassDescription);
                SetKeyValuePairItem("Point Type", equipmentPoint.PointTypeName);
                SetKeyValuePairItem("Point Type Description", equipmentPoint.PointTypeDescription);
                SetKeyValuePairItem("Client Name", equipmentPoint.ClientName);
                SetKeyValuePairItem("Building Name", equipmentPoint.BuildingName);
                SetKeyValuePairItem("Equipment Name", equipmentPoint.EquipmentName);
                SetKeyValuePairItem("All Equipment Associations", ParseList(equipmentPoint.AssociatedEquipment));
                SetKeyValuePairItem("Time Zone", equipmentPoint.TimeZone);
                SetKeyValuePairItem("ReferenceID", equipmentPoint.ReferenceID);
                SetKeyValuePairItem("Engineering Units", equipmentPoint.BuildingSettingBasedEngUnits);
                SetKeyValuePairItem("Sampling Interval", equipmentPoint.SamplingInterval + " minutes");
                SetKeyValuePairItem("Subscription Based", equipmentPoint.IsSubscriptionBased.ToString());
                SetKeyValuePairItem("Rel. Percent Error", equipmentPoint.RelPctError.ToString());
                SetKeyValuePairItem("Power (Storage)", equipmentPoint.Power.ToString());
                SetKeyValuePairItem("Multiply (Storage)", equipmentPoint.Multiply.ToString());
                SetKeyValuePairItem("Addition (Storage)", equipmentPoint.Addition.ToString());
                SetKeyValuePairItem("Power (Analyses)", equipmentPoint.PowerMatlab.ToString());
                SetKeyValuePairItem("Multiply (Analyses)", equipmentPoint.MultiplyMatlab.ToString());
                SetKeyValuePairItem("Addition (Analyses)", equipmentPoint.AdditionMatlab.ToString());
                SetKeyValuePairItem("Active", equipmentPoint.IsActive.ToString());
                SetKeyValuePairItem("Visible", equipmentPoint.IsVisible.ToString());

                SetDetailsViewDataToViewState(Convert.ToInt32(PointDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            private void SetEquipmentPointIntoEditForm(GetEquipmentPointsData equipment_point)
            {
                hdnEditID.Value = Convert.ToString(equipment_point.PID);
                txtEditPointName.Text = equipment_point.PointName;
                ddlEditPointClass.SelectedValue = Convert.ToString(equipment_point.PointClassID);
                ddlEditPointType.SelectedValue = Convert.ToString(equipment_point.PointTypeID);
                hdnEditPointTypeID.Value = Convert.ToString(equipment_point.PointTypeID);

                ddlEditDataSource.SelectedValue = equipment_point.DSID.ToString();
                hdnEditDataSourceID.Value = equipment_point.DSID.ToString();

                lblEditBuilding.Text = equipment_point.BuildingName;
                lblEditEquipment.Text = equipment_point.EquipmentName;
                hdnEditEID.Value = Convert.ToString(equipment_point.EID);
                ddlEditTimeZone.SelectedValue = equipment_point.TimeZone;
                txtEditReferenceID.Text = equipment_point.ReferenceID.Trim();
                hdnEditReferenceID.Value = equipment_point.ReferenceID.Trim();
                txtEditSamplingInterval.Text = Convert.ToString(equipment_point.SamplingInterval);
                chkEditIsSubscriptionBased.Checked = equipment_point.IsSubscriptionBased;
                txtEditRelPctError.Text = Convert.ToString(equipment_point.RelPctError);
                txtEditPower.Text = Convert.ToString(equipment_point.Power);
                txtEditMultiply.Text = Convert.ToString(equipment_point.Multiply);
                txtEditAddition.Text = Convert.ToString(equipment_point.Addition);
                txtEditPowerMatlab.Text = Convert.ToString(equipment_point.PowerMatlab);
                txtEditMultiplyMatlab.Text = Convert.ToString(equipment_point.MultiplyMatlab);
                txtEditAdditionMatlab.Text = Convert.ToString(equipment_point.AdditionMatlab);
                chkEditActive.Checked = equipment_point.IsActive;
                chkEditVisible.Checked = equipment_point.IsVisible;
            }

            private void LoadEditFormIntoPoint(Point point)
            {
                point.PID = Convert.ToInt32(hdnEditID.Value);
                point.PointName = txtEditPointName.Text;
                point.PointTypeID = Convert.ToInt32(ddlEditPointType.SelectedValue);
                point.TimeZone = ddlEditTimeZone.SelectedValue;
                point.DSID = Convert.ToInt32(ddlEditDataSource.SelectedValue);
                point.ReferenceID = txtEditReferenceID.Text.Trim();
                point.SamplingInterval = Convert.ToInt32(txtEditSamplingInterval.Text);
                point.IsSubscriptionBased = chkEditIsSubscriptionBased.Checked;
                point.RelPctError = Convert.ToDouble(txtEditRelPctError.Text);
                point.Power = Convert.ToDouble(txtEditPower.Text);
                point.Multiply = Convert.ToDouble(txtEditMultiply.Text);
                point.Addition = Convert.ToDouble(txtEditAddition.Text);
                point.PowerMatlab = Convert.ToDouble(txtEditPowerMatlab.Text);
                point.MultiplyMatlab = Convert.ToDouble(txtEditMultiplyMatlab.Text);
                point.AdditionMatlab = Convert.ToDouble(txtEditAdditionMatlab.Text);
                point.IsActive = chkEditActive.Checked;
                point.IsVisible = chkEditVisible.Checked;
            }

            protected String ParseList(Object equipment)
            {                
                return StringHelper.ParseStringArrayToCSVString(((List<CW.Data.Equipment>)equipment).Select(e => e.EquipmentName).ToArray(), true);
            }

            #region binders

                private void BindPointClasses(DropDownList ddl)
                {
                    BindDDL(ddl, "PointClassName", "PointClassID", DataMgr.PointClassDataMapper.GetAllPointClassesByTypeEnabled(true, false, false), true);
                }

                private void BindPointTypes(DropDownList ddl, Int32 pointClassID)
                {
                    BindDDL(ddl, "PointTypeName", "PointTypeID", (pointClassID != -1) ? DataMgr.PointTypeDataMapper.GetAllPointTypesByPointClassIDAndPointEnabled(pointClassID) : DataMgr.PointTypeDataMapper.GetAllPointTypes(), true);
                }

                private void BindDataSources(DropDownList ddl)
                {
                    BindDDL(ddl, "DataSourceName", "DSID", DataMgr.DataSourceDataMapper.GetAllDataSourcesByCID(siteUser.CID), true);
                }

                private void BindTimeZones(DropDownList ddl)
                {
                    BindDDL(ddl, "DisplayName", "Id", TimeZoneInfo.GetSystemTimeZones(), true);
                }

            #endregion

        #endregion
    }

    public class ClearCachePoints : IClearCache
    {
        public void ClearCache(int cid, IEnumerable<int> bids, Int32 uid) //uid not used here, just passed in as null
        {
            if (bids != null)
            {
                foreach (var bid in bids)
                    GridCacher.Clear(AdminUserControlGrid.CacheStore, ViewPoints.CreateCacheKey(cid, bid));
            }
        }
    }
}