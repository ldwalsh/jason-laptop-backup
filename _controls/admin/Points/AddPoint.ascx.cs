﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Linq;
using CW.Utility.Web;

namespace CW.Website._controls.admin.Points
{
    public partial class AddPoint : AdminUserControlBase
    {
        #region constants

            const String addPointSuccess = " point addition was successful. ";
            const String addPointFailed = " point addition failed. Please contact an administrator. ";
            const String pointTypeExists = "A point with the same type already exists for this equipment. ";
            const String pointTypeNotAllowed = "Point type is not allowed on equipment of that type.";
            const String referenceIDExists = "A point with the same reference id already exists for this datasource.";
            const String maxPointLimit = "Max point limit has been reached. Please contact an administrator for a limit increase.";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAddError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindPointClasses(ddlAddPointClass);
                    BindPointTypes(ddlAddPointType, -1);
                    BindDataSources(ddlAddDataSource);
                    BindBuildings(ddlAddBuilding);
                    BindEquipment(ddlAddEquipment, -1);
                    BindTimeZones(ddlAddTimeZone);
                }
            }

            #region ddl events

                protected void ddlAddPointClass_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindPointTypes(ddlAddPointType, Convert.ToInt32(ddlAddPointClass.SelectedValue));
                }

                protected void ddlAddBuilding_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindEquipment(ddlAddEquipment, Convert.ToInt32(ddlAddBuilding.SelectedValue));
                }

            #endregion

            #region button events

                protected void addPointButton_Click(object sender, EventArgs e)
                {
                    //multiple points can have the same name

                    var mPoint = new Point();
                    var mEquipment_Point = new Equipment_Point();
                    var mBuilding = DataMgr.BuildingDataMapper.GetBuildingByBID(Convert.ToInt32(ddlAddBuilding.SelectedValue), false, false, false, false, false, true);

                    LoadAddFormIntoPoint(mPoint);
                    
                    mEquipment_Point.EID = Convert.ToInt32(ddlAddEquipment.SelectedValue);

                    try
                    {
                        if (mBuilding.BuildingSettings.First().MaxPointLimit > DataMgr.PointDataMapper.GetAllPointsCountByBID(mBuilding.BID))
                        {
                            //check if point type is allowed for equipment type
                            if (DataMgr.EquipmentTypeDataMapper.IsPointTypeAssociatedWithEquipmentsType(mPoint.PointTypeID, mEquipment_Point.EID))
                            {
                                //Check if equipment is of class 'group', if so allow multiple of the same point types to be added
                                var isEquipmentGroup = DataMgr.EquipmentDataMapper.IsEquipmentAssociatedWithClassGroup(mEquipment_Point.EID);
                                var pointTypeExistsForNonEquipmentGroup = false;
                                var isPointTypeAddSelectedUnassigned = DataMgr.PointTypeDataMapper.IsPointTypeUnassigned(Convert.ToInt32(ddlAddPointType.SelectedValue));

                                //If not a group and not unassigned check if a point with the same point type already exists for equipmet not of class group
                                if (!isEquipmentGroup && !isPointTypeAddSelectedUnassigned)
                                    pointTypeExistsForNonEquipmentGroup = DataMgr.EquipmentPointsDataMapper.DoesPointTypeExistForEquipmentNotOfClassGroup(mEquipment_Point.EID, mPoint.PointTypeID);

                                //if equipment is group continue, or if pointtype doesnt exist on equipment not of class group, or if adding an unassigned point                   
                                if (isEquipmentGroup || !pointTypeExistsForNonEquipmentGroup || isPointTypeAddSelectedUnassigned)
                                {
                                    //Check if reference id already exists for this data source
                                    if (DataMgr.PointDataMapper.DoesPointReferenceIDExistForDataSource(mPoint.DSID, mPoint.ReferenceID))
                                    {
                                        LabelHelper.SetLabelMessage(lblAddError, referenceIDExists, lnkSetFocus);
                                        return;
                                    }
                                    else
                                    {
                                        var newPID = DataMgr.PointDataMapper.InsertAndReturnPID(mPoint);

                                        try
                                        {
                                            mEquipment_Point.PID = newPID;
                                            DataMgr.EquipmentPointsDataMapper.InsertEquipment_Point(mEquipment_Point);

                                            LabelHelper.SetLabelMessage(lblResults, mPoint.PointName + addPointSuccess, lnkSetFocus);

                                            OperationComplete(KGSPointAdministration.TabMessages.AddPoint);
                                            new ClearCachePoints().ClearCache(siteUser.CID, new int[] { mBuilding.BID }, siteUser.UID);
                                        }
                                        catch (SqlException sqlEx)
                                        {
                                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment_point.", sqlEx);
                                            LabelHelper.SetLabelMessage(lblAddError, mPoint.PointName + addPointFailed, lnkSetFocus);
                                        }
                                        catch (Exception ex)
                                        {
                                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment_point.", ex);
                                            LabelHelper.SetLabelMessage(lblAddError, mPoint.PointName + addPointFailed, lnkSetFocus);
                                        }
                                    }
                                }
                                else
                                {
                                    LabelHelper.SetLabelMessage(lblAddError, pointTypeExists, lnkSetFocus);
                                    return;
                                }
                            }
                            else
                            {
                                LabelHelper.SetLabelMessage(lblAddError, pointTypeNotAllowed, lnkSetFocus);
                                return;
                            }
                        }
                        else
                        {
                            LabelHelper.SetLabelMessage(lblAddError, maxPointLimit, lnkSetFocus);
                            return;
                        }
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding point.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, mPoint.PointName + addPointFailed, lnkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding point.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, mPoint.PointName + addPointFailed, lnkSetFocus);
                    }
                }

            #endregion

        #endregion

        #region methods

            protected void LoadAddFormIntoPoint(Point point)
            {
                point.PointName = txtAddPointName.Text;
                point.PointTypeID = Convert.ToInt32(ddlAddPointType.SelectedValue);
                point.DSID = Convert.ToInt32(ddlAddDataSource.SelectedValue);
                point.TimeZone = ddlAddTimeZone.SelectedValue;
                point.ReferenceID = txtAddReferenceID.Text.Trim();
                point.SamplingInterval = Convert.ToInt32(txtAddSamplingInterval.Text);
                point.IsSubscriptionBased = chkAddIsSubscriptionBased.Checked;
                point.RelPctError = Convert.ToDouble(txtAddRelPctError.Text);
                point.Power = Convert.ToDouble(txtAddPower.Text);
                point.Multiply = Convert.ToDouble(txtAddMultiply.Text);
                point.Addition = Convert.ToDouble(txtAddAddition.Text);
                point.PowerMatlab = Convert.ToDouble(txtAddPowerMatlab.Text);
                point.MultiplyMatlab = Convert.ToDouble(txtAddMultiplyMatlab.Text);
                point.AdditionMatlab = Convert.ToDouble(txtAddAdditionMatlab.Text);
                point.IsActive = true;
                point.IsVisible = true;
                point.DateCreated = txtAddDateCreated.SelectedDate != null ? (DateTime)txtAddDateCreated.SelectedDate : DateTime.UtcNow;
                point.DateModified = DateTime.UtcNow;
            }	

            #region binders

                private void BindPointClasses(DropDownList ddl)
                {
                    BindDDL(ddl, "PointClassName", "PointClassID", DataMgr.PointClassDataMapper.GetAllPointClassesByTypeEnabled(true, false, false), true);
                }

                private void BindPointTypes(DropDownList ddl, Int32 pointClassID)
                {
                    var lItem = new ListItem() { Text = (pointClassID != -1) ? "Select one..." : "Select class first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "PointTypeName", "PointTypeID", (pointClassID != -1) ? DataMgr.PointTypeDataMapper.GetAllPointTypesByPointClassIDAndPointEnabled(pointClassID) : null, true, lItem);
                }

                private void BindDataSources(DropDownList ddl)
                {
                    BindDDL(ddl, "DataSourceName", "DSID", DataMgr.DataSourceDataMapper.GetAllDataSourcesByCID(siteUser.CID), true);
                }

                private void BindBuildings(DropDownList ddl)
                {
                    var lItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };

                    BindDDL(ddl, "BuildingName", "BID", siteUser.VisibleBuildings, true);
                }

                private void BindEquipment(DropDownList ddl, Int32 bid)
                {
                    var lItem = new ListItem() { Text = (bid != -1) ? "Select one..." : "Select building first...", Value = "-1", Selected = true };

                    BindDDL(ddl, "EquipmentName", "EID", (bid != -1) ? DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid) : null, true, lItem);
                }

                private void BindTimeZones(DropDownList ddl)
                {
                    BindDDL(ddl, "DisplayName", "Id", TimeZoneInfo.GetSystemTimeZones(), true);
                }

            #endregion

        #endregion
    }
}