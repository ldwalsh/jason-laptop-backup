﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="AddDataSource.ascx.cs" Inherits="CW.Website._controls.admin.AddDataSource" %>

<asp:Panel ID="pnlAddDataSource" runat="server" DefaultButton="btnAddDataSource">

  <h2>Add Data Source</h2>

  <div>
      
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblAddError" CssClass="errorMessage" runat="server" />

    <div class="divForm">
      <label class="label">*Data Source Name:</label>
      <asp:TextBox ID="txtAddDataSourceName" CssClass="textbox" MaxLength="100" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Data Transfer Service:</label>    
      <asp:DropDownList ID="ddlAddDataTransferService" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">IP List (CSV):</label>
      <asp:TextBox ID="txtAddIPList" CssClass="textbox" MaxLength="500" runat="server" />
      <p>(Note: Please do not enter 127.0.0.1.)<br />(Warning: If no ip address is set, a data transfer service guid is required for incoming data.)</p> 
    </div>
    <div class="divForm">
      <label class="label">*ReferenceID:</label>
      <asp:TextBox CssClass="textbox" ID="txtAddReferenceID" runat="server" MaxLength="25" />
    </div>
    <div class="divForm">
      <label class="label">Installer:</label>
      <asp:TextBox CssClass="textbox" ID="txtAddInstaller" runat="server" MaxLength="100" />
    </div>
    <div class="divForm">
      <label class="label">Location:</label>                                                         
      <textarea name="txtAddLocation" id="txtAddLocation" cols="40" rows="5" onkeyup="limitChars(this, 250, 'divAddLocationCharInfo')" runat="server" />
      <div id="divAddLocationCharInfo"></div>
    </div>
    <div class="divForm">                                                                                                                  
      <label class="label">*Time Offset (Default=0):</label>   
      <asp:TextBox CssClass="textbox" ID="txtAddTimeOffset" Text="0" runat="server" MaxLength="2" />
      <p>(Note: This value is a corrective hourly offset integer that can be negative. Used to adjust the recorded timestamp on incoming data if the data source is inaccurate in comparison to it's location and timezone.)</p>
      <p>(Note: If this data source is an ADDMEJR, then we must use the offset based off the difference from the clients timezone and the cloud server timezone location.)</p> 
    </div>
    <div class="divForm">                                                                                                                  
      <label class="label">*Analysis Offset (Default=0):</label>                                                           
      <label class="labelContent">(***DEPRICATED/INACTIVE FIELD***)</label>
      <p>(Note: This value is used to adjust when scheduled analyses can be peformed that are associated with equipment associated with this data source. If a datasource location is behind the hosted server time, a positive value must be used to delay running analyses untill all data for the day has been received. Cannot be negative.)</p> 
    </div>
    <div class="divForm">
      <label class="label">Description:</label>
      <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server" />
      <div id="divAddDescriptionCharInfo"></div>
    </div>
    <div class="divForm">
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>
    <div class="divForm">   
      <label class="label">*Vendor:</label>    
      <asp:DropDownList ID="ddlAddVendor" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddVendor_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Vendor Product:</label>    
      <asp:DropDownList ID="ddlAddVendorProduct" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddVendorProduct_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Retrieval Method:</label>    
      <asp:DropDownList ID="ddlAddRetrievalMethod" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Primary Contact:</label>    
      <asp:DropDownList ID="ddlAddPrimaryContact" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Secondary Contact:</label>    
      <asp:DropDownList ID="ddlAddSecondaryContact" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">*Integrated:</label>
      <asp:CheckBox ID="chkAddIntegrated" CssClass="checkbox" OnCheckedChanged="chkAddIntegrated_OnCheckedChanged" AutoPostBack="true" runat="server" />   
      <p>(Note: This is for integrated web services.)</p>
    </div>
    <div class="divForm">   
      <label class="label">BMS Info:</label>    
      <asp:DropDownList ID="ddlAddBMSInfo" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div id="divAddIntegrated" runat="server" visible="false">
                                                               
      <div class="divForm">
        <label class="label">*Integrated Url:</label>
        <asp:TextBox CssClass="textbox" ID="txtAddIntegratedUrl" runat="server" MaxLength="250" />
      </div>
      <div class="divForm">
        <label class="label">*Integrated Username:</label>
        <asp:TextBox CssClass="textbox" ID="txtAddIntegratedUsername" runat="server" MaxLength="100" />
      </div>
      <div class="divForm">
        <label class="label">*Integrated Password:</label>
        <asp:TextBox CssClass="textbox" ID="txtAddIntegratedPassword" runat="server" MaxLength="50" />
      </div>

    </div>
    <div class="divForm">
      <label class="label">Realtime:</label>
      <asp:CheckBox ID="chkAddRealtime" Checked="true" CssClass="checkbox" runat="server" />   
      <p>(Note: This is for automated notifications, determines if it should be full day or last hour.)</p>
      <p>(Note: Uncheck if data is sent from a csv at the end of the day.)</p>
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnAddDataSource" runat="server" Text="Add Data Source"  OnClick="addDataSourceButton_Click" ValidationGroup="AddDataSource" />

  </div>
                                                
  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="addDataSourceNameRFV" runat="server" ErrorMessage="Data Source Name is a required field." ControlToValidate="txtAddDataSourceName" SetFocusOnError="true" Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addDataSourceNameVCE" runat="server" BehaviorID="addDataSourceNameVCE" TargetControlID="addDataSourceNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addIPListFTE" runat="server" TargetControlID="txtAddIPList" FilterType="Custom, Numbers" ValidChars=".," />  
  <asp:RegularExpressionValidator ID="addIPListREV" runat="server" ErrorMessage="Invalid IP Address List Format." ValidationExpression="(\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b)(,\s*\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b)*" ControlToValidate="txtAddIPList" SetFocusOnError="true"  Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addIPListREVVCE" runat="server" BehaviorID="addIPListREVVCE"  TargetControlID="addIPListREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addReferenceIDRFV" runat="server" ErrorMessage="ReferenceID is a required field." ControlToValidate="txtAddReferenceID" SetFocusOnError="true" Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addReferenceIDVCE" runat="server" BehaviorID="addReferenceIDVCE" TargetControlID="addReferenceIDRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="addTimeOffsetFTE" runat="server" TargetControlID="txtAddTimeOffset" FilterType="Custom, Numbers" ValidChars="-" />
  <asp:RequiredFieldValidator ID="addTimeOffsetRFV" runat="server" ErrorMessage="Time Offset is a required field." ControlToValidate="txtAddTimeOffset" SetFocusOnError="true" Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addTimeOffsetVCE" runat="server" BehaviorID="addTimeOffsetVCE" TargetControlID="addTimeOffsetRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:RegularExpressionValidator ID="addTimeOffsetREV" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\d$" ControlToValidate="txtAddTimeOffset" SetFocusOnError="true" Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addTimeOffsetREVVCE" runat="server" BehaviorID="addTimeOffsetREVVCE" TargetControlID="addTimeOffsetREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addVendorRFV" runat="server" ErrorMessage="Vendor is a required field." ControlToValidate="ddlAddVendor" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addVendorVCE" runat="server" BehaviorID="addVendorVCE" TargetControlID="addVendorRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addVendorProductRFV" runat="server" ErrorMessage="Vendor Product is a required field." ControlToValidate="ddlAddVendorProduct" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addVendorProductVCE" runat="server" BehaviorID="addVendorProductVCE" TargetControlID="addVendorProductRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addRetrievalMethodRFV" runat="server" ErrorMessage="Retrieval Method is a required field." ControlToValidate="ddlAddRetrievalMethod" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addRetrievalMethodVCE" runat="server" BehaviorID="addRetrievalMethodVCE" TargetControlID="addRetrievalMethodRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addIntegratedUrlRFV" runat="server" ErrorMessage="Integrated Url is a required field." ControlToValidate="txtAddIntegratedUrl" SetFocusOnError="true" Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addIntegratedUrlVCE" runat="server" BehaviorID="addIntegratedUrlVCE" TargetControlID="addIntegratedUrlRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:RegularExpressionValidator ID="addIntegratedUrlREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtAddIntegratedUrl" SetFocusOnError="true" Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addIntegratedUrlREVVCE" runat="server" BehaviorID="addIntegratedUrlREVVCE" TargetControlID="addIntegratedUrlREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addIntegratedUsernameRFV" runat="server" ErrorMessage="Integrated Username is a required field." ControlToValidate="txtAddIntegratedUsername" SetFocusOnError="true" Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addIntegratedUsernameVCE" runat="server" BehaviorID="addIntegratedUsernameVCE" TargetControlID="addIntegratedUsernameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="addIntegratedPasswordRFV" runat="server" ErrorMessage="Integrated Password is a required field." ControlToValidate="txtAddIntegratedPassword" SetFocusOnError="true" Display="None" ValidationGroup="AddDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="addIntegratedPasswordVCE" runat="server" BehaviorID="addIntegratedPasswordVCE" TargetControlID="addIntegratedPasswordRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  
</asp:Panel>                                                 