﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.DataSource;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin.DataSources
{
    public partial class ViewDataSources : AdminUserControlGrid
    {
        #region constants

            const String pointsExist = "Cannot delete data source because points are associated. Please delete points first.";
            const String deleteSuccessful = "Data source deletion was successful.";
            const String deleteFailed = "Data source deletion failed. Please contact an administrator.";
            const String updateDataSourceSuccessful = "Data source update was successful.";
            const String updateDataSourceFailed = "Data source update failed. Please contact an administrator.";
            const String dataSourceNameExists = "Data source with provided name already exists.";
            const String dataSourceExists = "Data source with provided reference id and ip or dtsid already exists.";
            const String identifierRequired = "Data transfer service or ip is required.";

        #endregion

        #region fields

        private enum DataSourceDetails { Default };

        #endregion

        #region properties

            #region overrides

            	protected override IEnumerable GetEntitiesForGrid(IEnumerable<String> searchCriteria)
            	{
                	return DataMgr.DataSourceDataMapper.GetAllSearchedDataSources(searchCriteria.FirstOrDefault(), siteUser.CID, chkAllClients.Checked);
            	}

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<GetDataSourcesData>(_ => _.DSID, _ => _.DataSourceName, _ => _.ClientName, _ => _.CID, _ => _.IPList, _ => _.DTSName, _ => _.ReferenceID, _ => _.IsIntegrated, _ => _.IsActive); }
                }

                protected override String NameField { get { return PropHelper.G<GetDataSourcesData>(_ => _.DataSourceName); } }

                protected override String IdColumnName { get { return PropHelper.G<GetDataSourcesData>(_ => _.DSID); } }

                protected override String Name { get { return typeof(DataSource).Name; } }

                protected override Type Entity { get { return typeof(GetDataSourcesData); } }

                protected override String CacheHashKey { get { return FormatHashKey(new Object[] { siteUser.CID }); } }

                protected override IEnumerable<String> SearchGridColumnNames { get { return new[] { NameField, PropHelper.G<GetDataSourcesData>(_ => _.IPList), PropHelper.G<GetDataSourcesData>(_ => _.ReferenceID) }; } }

                public override IEnumerable<Enum> ReactToChangeStateList { get { return new Enum[] { KGSDataSourceAdministration.TabMessages.AddDataSource }; } }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid)
            {
                return new GridCacher.CacheKeyInfo(typeof(DataSource).Name + "." + typeof(GetDataSourcesData).Name, cid.ToString());
            }

        #endregion

        #region events

            private void Page_FirstLoad()
            {
                divSuperSearch.Visible = siteUser.IsKGSSuperAdminOrHigher; //security check
            }

            #region grid events

                #region overrides

                    protected override void OnGridEditing(Object sender, GridViewEditEventArgs e)
                    {
                        pnlDetailsView.Visible = false;
                        pnlEdit.Visible = true;

                        var dsid = Convert.ToInt32(grid.DataKeys[e.NewEditIndex].Values[PropHelper.G<GridCacher.GridSet.Row>(_ => _.ID)].ToString());
                        var mDataSource = DataMgr.DataSourceDataMapper.GetDataSourceByID(dsid, true, true);

                        BindVendors(ddlEditVendor);
                        BindBMSInfo(ddlEditBMSInfo);
                        BindDataTransferServices(ddlEditDataTransferService);
                        BindVendorProducts(ddlEditVendorProduct, mDataSource.DataSourceVendorProduct.VendorID);
                        BindRetrievalMethods(ddlEditRetrievalMethod, Convert.ToInt32(mDataSource.VendorProductID));
                        BindVendorContacts(ddlEditPrimaryContact, mDataSource.DataSourceVendorProduct.VendorID);
                        BindVendorContacts(ddlEditSecondaryContact, mDataSource.DataSourceVendorProduct.VendorID);
 
                        SetDataSourceIntoEditForm(mDataSource);
 
                        e.Cancel = true; //Cancels the edit auto grid selected.
                    }

                    protected override void OnGridDeleting(Object sender, GridViewDeleteEventArgs e)
                    {
                        var dsid = Convert.ToInt32(grid.DataKeys[e.RowIndex].Value);

                        try
                        {
                                if (DataMgr.PointDataMapper.ArePointsAssignedToDataSource(dsid))
                                {
                                    LabelHelper.SetLabelMessage(lblDeleteError, pointsExist, lnkSetFocus);
                                    return;
                                }
                                else
                                {
                                    DataMgr.DataSourceDataMapper.DeleteDataSource(dsid);
                                    OperationComplete(OperationType.Delete, deleteSuccessful, KGSDataSourceAdministration.TabMessages.DeleteDataSource);
                                }
                         }
                         catch (Exception ex)
                         {
                                LabelHelper.SetLabelMessage(lblDeleteError, deleteFailed, lnkSetFocus);
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting data source.", ex);
                         }
                     }

                #endregion

            #endregion

            #region button events

                protected void updateDataSourceButton_Click(Object sender, EventArgs e)
                {
                    var mDataSource = new DataSource();

                    LoadEditFormIntoDataSource(mDataSource);

                    if (String.IsNullOrWhiteSpace(mDataSource.IPList) && mDataSource.DTSID == null)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, identifierRequired, lnkSetFocus);
                        return;
                    }

                    if (mDataSource.DTSID != null && DataMgr.DataSourceDataMapper.DoesAnotherDataSourceReferenceIDAndDTSIDExist(mDataSource.ReferenceID, (int)mDataSource.DTSID, mDataSource.DSID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, dataSourceExists, lnkSetFocusEdit);
                        return;
                    }
                    else if (!String.IsNullOrWhiteSpace(mDataSource.IPList) && DataMgr.DataSourceDataMapper.DoesAnotherDataSourceReferenceIDAndIPExist(mDataSource.ReferenceID, StringHelper.ParseCSVString(mDataSource.IPList), mDataSource.DSID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, dataSourceExists, lnkSetFocusEdit);
                        return;
                    }
                    else
                    {
                        try
                        {
                            //TODO: make datasource name a hdn variable so we can remove this query
                            var originalDataSource = DataMgr.DataSourceDataMapper.GetDataSourceByID(mDataSource.DSID);

                            if (originalDataSource.DataSourceName != mDataSource.DataSourceName && DataMgr.DataSourceDataMapper.DoesDataSourceExistForClient(mDataSource.CID, mDataSource.DSID, mDataSource.DataSourceName))
                            {
                                LabelHelper.SetLabelMessage(lblEditError, dataSourceNameExists, lnkSetFocusEdit);
                                return;
                            }

                            DataMgr.DataSourceDataMapper.UpdateDataSource(mDataSource);
                            OperationComplete(OperationType.Update, updateDataSourceSuccessful, KGSDataSourceAdministration.TabMessages.EditDataSource);

                            //update all points in CWdata reference table if reference id or ip changed.
                            //if changed logic is in datamapper.
                            //ReferencePointDataMapper.UpdateReferencePoint(mDataSource);
                        }
                        catch (Exception ex)
                        {
                            LabelHelper.SetLabelMessage(lblEditError, updateDataSourceFailed, lnkSetFocusEdit);
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating data source.", ex);
                        }
                    }
                }

            #endregion

            #region checkbox events

                protected void chkEditIntegrated_OnCheckedChanged(Object sender, EventArgs e)
                {
                    divEditIntegrated.Visible = editIntegratedUrlRFV.Enabled = editIntegratedUsernameRFV.Enabled = editIntegratedPasswordRFV.Enabled = chkEditIntegrated.Checked;
                }

            #endregion

            #region ddl events

                protected void ddlEditVendor_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindVendorProducts(ddlEditVendorProduct, Convert.ToInt32(ddlEditVendor.SelectedValue));
                    BindRetrievalMethods(ddlEditRetrievalMethod, Convert.ToInt32(ddlEditVendorProduct.SelectedValue));
                    BindVendorContacts(ddlEditPrimaryContact, Convert.ToInt32(ddlEditVendor.SelectedValue));
                    BindVendorContacts(ddlEditSecondaryContact, Convert.ToInt32(ddlEditVendor.SelectedValue));
                }

                protected void ddlEditVendorProduct_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindRetrievalMethods(ddlEditRetrievalMethod, Convert.ToInt32(ddlEditVendorProduct.SelectedValue));
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void SetDataForDetailsViewFromDB()
                {
                    var dataSource = DataMgr.DataSourceDataMapper.GetFullDataSourceByID(CurrentID).First();

                    SetDataSourceIntoViewState(dataSource);

                    SelectedID = CurrentID; //make sure to updated the ids to match, until there is another selected index changed later.
                }

                protected override void SetFieldsIntoDetailsView()
                {
                    SetKeyValuePairItemsToRepeater(Convert.ToInt32(DataSourceDetails.Default), rptDataSourceDetails);
                }

            #endregion

            private void SetDataSourceIntoViewState(GetDataSourcesData dataSource)
            {
                SetKeyValuePairItem("Data Source Name", dataSource.DataSourceName);
                SetKeyValuePairItem("Data Transfer Service Name", dataSource.DTSName ?? "Not Assigned");
                SetKeyValuePairItem("Client", dataSource.ClientName);
                SetKeyValuePairItem("IP List", dataSource.IPList);
                SetKeyValuePairItem("Location", dataSource.Location, "<div class='divContentPreWrap'>{0}</div>");
                SetKeyValuePairItem("Time Offset (hours)", dataSource.TimeOffset.ToString());
                SetKeyValuePairItem("Installer", dataSource.Installer);
                SetKeyValuePairItem("Description", dataSource.Description, "<div class='divContentPreWrap'>{0}</div>");
                SetKeyValuePairItem("Vendor", dataSource.VendorName);
                SetKeyValuePairItem("Vendor Product", dataSource.VendorProductName);
                SetKeyValuePairItem("Retrieval Method", dataSource.RetrievalMethod);
                SetKeyValuePairItem("Primary Contact", dataSource.PrimaryContactName, "<a href=\"/KGSDataSourceVendorContactAdministration.aspx?cid=" + dataSource.PrimaryContactID + "\" target='_blank'>{0}</a>");
                SetKeyValuePairItem("Secondary Contact", dataSource.SecondaryContactName, "<a href=\"/KGSDataSourceVendorContactAdministration.aspx?cid=" + dataSource.SecondaryContactID + "\" target='_blank'>{0}</a>");

                SetKeyValuePairItem("Integrated", dataSource.IsIntegrated.ToString());

                if (dataSource.IsIntegrated)
                {
                    SetKeyValuePairItem("Integrated Url", dataSource.IntegratedUrl);
                    SetKeyValuePairItem("Integrated Username", dataSource.IntegratedUsername);
                    SetKeyValuePairItem("Integrated Password", dataSource.IntegratedPassword);
                }

                SetKeyValuePairItem("Realtime", dataSource.IsRealtime.ToString());
                SetKeyValuePairItem("Active", dataSource.IsActive.ToString());

                SetDetailsViewDataToViewState(Convert.ToInt32(DataSourceDetails.Default), true); //True means to clear viewstate since this is a new record
            }

            private void SetDataSourceIntoEditForm(DataSource dataSource)
            {
                hdnEditID.Value = Convert.ToString(dataSource.DSID);
                ddlEditDataTransferService.SelectedValue = dataSource.DTSID.HasValue ? dataSource.DTSID.Value.ToString() : "-1";
                ddlEditBMSInfo.SelectedValue = dataSource.BMSInfoID.HasValue ? dataSource.BMSInfoID.Value.ToString() : "-1";
                txtEditDataSourceName.Text = dataSource.DataSourceName;
                lblClientName.Text = dataSource.Client.ClientName;
                txtEditIPList.Text = dataSource.IPList;
                txtEditReferenceID.Text = dataSource.ReferenceID.Trim();
                txtEditInstaller.Text = dataSource.Installer;
                txtEditLocation.Value = dataSource.Location;
                txtEditTimeOffset.Text = Convert.ToString(dataSource.TimeOffset);
                txtEditDescription.Value = dataSource.Description;
                ddlEditVendor.SelectedValue = Convert.ToString(dataSource.DataSourceVendorProduct.VendorID);
                ddlEditVendorProduct.SelectedValue = Convert.ToString(dataSource.VendorProductID);
                ddlEditRetrievalMethod.SelectedValue = Convert.ToString(dataSource.VendorProductRetrievalMethodID);

                try { ddlEditPrimaryContact.SelectedValue = Convert.ToString(dataSource.PrimaryContactID); } catch { }
                try { ddlEditSecondaryContact.SelectedValue = Convert.ToString(dataSource.SecondaryContactID); } catch { }

                chkEditIntegrated.Checked = dataSource.IsIntegrated;
                divEditIntegrated.Visible = editIntegratedUrlRFV.Enabled = editIntegratedUsernameRFV.Enabled = editIntegratedPasswordRFV.Enabled = dataSource.IsIntegrated;

                if (dataSource.IsIntegrated)
                {
                    txtEditIntegratedUrl.Text = dataSource.IntegratedUrl;
                    txtEditIntegratedUsername.Text = dataSource.IntegratedUsername;
                    txtEditIntegratedPassword.Text = dataSource.IntegratedPassword;
                }

                chkEditActive.Checked = dataSource.IsActive;
                chkEditRealtime.Checked = dataSource.IsRealtime;
            }

            private void LoadEditFormIntoDataSource(DataSource dataSource)
            {
                dataSource.DSID = Convert.ToInt32(hdnEditID.Value);                                
                dataSource.DataSourceName = txtEditDataSourceName.Text;
                dataSource.DTSID = ddlEditDataTransferService.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditDataTransferService.SelectedValue) : null;
                dataSource.IPList = String.IsNullOrEmpty(txtEditIPList.Text) ? null : txtEditIPList.Text;
                dataSource.ReferenceID = txtEditReferenceID.Text.Trim();
                dataSource.BMSInfoID = ddlEditBMSInfo.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditBMSInfo.SelectedValue) : null;
                dataSource.Installer = String.IsNullOrEmpty(txtEditInstaller.Text) ? null : txtEditInstaller.Text;
                dataSource.Location = String.IsNullOrEmpty(txtEditLocation.Value) ? null : txtEditLocation.Value;
                dataSource.TimeOffset = Convert.ToInt32(txtEditTimeOffset.Text);
                dataSource.Description = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                dataSource.VendorProductID = Convert.ToInt32(ddlEditVendorProduct.SelectedValue);
                dataSource.VendorProductRetrievalMethodID = Convert.ToInt32(ddlEditRetrievalMethod.SelectedValue);
                dataSource.PrimaryContactID = ddlEditPrimaryContact.SelectedValue != "-1" ? (Int32?)Convert.ToInt32(ddlEditPrimaryContact.SelectedValue) : null;
                dataSource.SecondaryContactID = ddlEditSecondaryContact.SelectedValue != "-1" ? (Int32?)Convert.ToInt32(ddlEditSecondaryContact.SelectedValue) : null;
                dataSource.IsIntegrated = chkEditIntegrated.Checked;
                dataSource.IsRealtime = chkEditRealtime.Checked;

                if (chkEditIntegrated.Checked)
                {
                    dataSource.IntegratedUrl = txtEditIntegratedUrl.Text;
                    dataSource.IntegratedUsername = txtEditIntegratedUsername.Text;
                    dataSource.IntegratedPassword = txtEditIntegratedPassword.Text;
                }
                else
                {
                    dataSource.IntegratedUrl = String.Empty;
                    dataSource.IntegratedUsername = String.Empty;
                    dataSource.IntegratedPassword = String.Empty;
                }

                dataSource.IsActive = chkEditActive.Checked;
            }

            #region binders

                private void BindBMSInfo(DropDownList ddl)
                {
                    BindDDL(ddl, "BMSInfoName", "BMSInfoID", DataMgr.BMSInfoDataMapper.GetBMSInfoByClientID(null, siteUser.CID), true);
                }

                private void BindDataTransferServices(DropDownList ddl)
                {
                    BindDDL(ddl, "DataTransferServiceName", "DTSID", DataMgr.DataTransferServiceDataMapper.GetAllActiveServicesByCID(siteUser.CID), true);
                }

                private void BindVendors(DropDownList ddl)
                {
                    BindDDL(ddl, "VendorName", "VendorID", DataMgr.DataSourceVendorDataMapper.GetAllVendors(), true);
                }

                private void BindVendorProducts(DropDownList ddl, Int32 vendorID)
                {
                    var lItem = new ListItem() { Text = ((vendorID != -1) ? "Select one..." : "Select vendor first..."), Value = "-1", Selected = true };

                    BindDDL(ddl, "VendorProductName", "VendorProductID", (vendorID != -1) ? DataMgr.DataSourceVendorProductDataMapper.GetAllVendorProductsByVendorID(vendorID) : null, true, lItem);
                }

                private void BindRetrievalMethods(DropDownList ddl, Int32 vendorProductID)
                {
                    var lItem = new ListItem() { Text = ((vendorProductID != -1) ? "Select one..." : "Select vendor product first..."), Value = "-1", Selected = true };

                    BindDDL(ddl, "RetrievalMethod", "VendorProductRetrievalMethodID", (vendorProductID != -1) ? DataMgr.DataSourceVendorProductDataMapper.GetAllActiveDataSourceVendorProductsRetrievalMethodsByProductIDAsDropdownFormat(vendorProductID) : null, true, lItem);
                }

                private void BindVendorContacts(DropDownList ddl, Int32 vendorID)
                {
                    var lItem = new ListItem() { Text = ((vendorID != -1) ? "Optionally select one..." : "Select vendor first..."), Value = "-1", Selected = true };

                    BindDDL(ddl, "Name", "ContactID", (vendorID != -1) ? DataMgr.DataSourceVendorContactDataMapper.GetAllVendorContactsByVendorID(vendorID) : null, true, lItem);
                }

            #endregion

        #endregion
    }
}