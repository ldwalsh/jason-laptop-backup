﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin
{
    public partial class BulkEditDataSources : AdminUserControlBase
    {
        #region constants

            const String bulkEditSuccessful = "Bulk edit data source {0} was successful.";
            const String bulkEditUnableToEditOneOrMoreDataSources = "Cannot bulk edit data source {0} due to a restriction on one or more fields. The most common reason being a data source with that reference id and edited ip or dtsid may already exist.";    
            const String bulkEditFailed = "Bulk edit data sources failed. Please contact an administrator.";
            const String bulkEditError = "Error during bulk edit data sources in kgs data source administration.";
            const String noBulkEditFieldsSelected = "No bulk edit fields were selected.";            
            const String emptyDataTransferServiceFieldRequired = "Data Transfer Service is required to be selected.";
            const String emptyDataBMSInfoFieldRequired = "BMS Info is required to be selected.";

        #endregion

        #region properties

            #region overrides

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[] 
                        {
                            KGSDataSourceAdministration.TabMessages.AddDataSource,
                            KGSDataSourceAdministration.TabMessages.EditDataSource,
                            KGSDataSourceAdministration.TabMessages.DeleteDataSource
                        };
                    }
                }

            #endregion

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblBulkEditError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindBulkEditDataSources();
                    BindDataTransferServices(ddlBulkEditDataTransferService);
                    BindBMSInfo(ddlBulkEditBMSInfo);
                }
            }

            #region button events

                protected void btnBulkEditDataSourcesUpButton_Click(Object sender, EventArgs e)
                {
                    while (lbBulkEditDataSourcesBottom.SelectedIndex != -1)
                    {
                        lbBulkEditDataSourcesTop.Items.Add(lbBulkEditDataSourcesBottom.SelectedItem);
                        lbBulkEditDataSourcesBottom.Items.Remove(lbBulkEditDataSourcesBottom.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBulkEditDataSourcesTop);
                }

                protected void btnBulkEditDataSourcesDownButton_Click(Object sender, EventArgs e)
                {
                    while (lbBulkEditDataSourcesTop.SelectedIndex != -1)
                    {
                        lbBulkEditDataSourcesBottom.Items.Add(lbBulkEditDataSourcesTop.SelectedItem);
                        lbBulkEditDataSourcesTop.Items.Remove(lbBulkEditDataSourcesTop.SelectedItem);
                    }

                    ControlHelper.SortListBox(lbBulkEditDataSourcesBottom);
                }

                protected void bulkEditDataSourcesButton_Click(Object sender, EventArgs e)
                {
                    Int32 dsid;
                    Boolean changeActive = false, changeIPList = false, changeDataTransferService = false, changeBMSInfo = false;

                    try
                    {
                        lblBulkEditError.Text = String.Empty;
                        lblResults.Text = String.Empty;
                                        
                        if (chkBulkEditVerifyDataTransferService.Checked && ddlBulkEditDataTransferService.SelectedValue == "-1")
                        {
                            LabelHelper.SetLabelMessage(lblBulkEditError, emptyDataTransferServiceFieldRequired, lnkSetFocus);
                            return;
                        }

                        if (chkBulkEditVerifyBMSInfo.Checked && ddlBulkEditBMSInfo.SelectedValue == "-1")
                        {
                            LabelHelper.SetLabelMessage(lblBulkEditError, emptyDataBMSInfoFieldRequired, lnkSetFocus);
                            return;
                        }

                        if (chkBulkEditVerifyIPList.Checked || chkBulkEditVerifyActive.Checked || chkBulkEditVerifyDataTransferService.Checked || chkBulkEditVerifyBMSInfo.Checked)
                        {                          
                            foreach (ListItem item in lbBulkEditDataSourcesBottom.Items)
                            {                             
                                var allowEdit = true;
                                var mDataSource = new DataSource();
                                var dataSourceName = item.Text;

                                if (!Int32.TryParse(item.Value, out dsid)) { continue; }

                                mDataSource.DSID = dsid;

                                //bulk edit ip list
                                if (chkBulkEditVerifyIPList.Checked)
                                {
                                    var originalDataSource = DataMgr.DataSourceDataMapper.GetDataSourceByID(dsid);                                    
                                    String[] bulkEditIPList = StringHelper.ParseCSVString(txtBulkEditIPList.Text);
                                    bool newDSUniqueByRefIDAndIP = bulkEditIPList?.Length > 0 &&
                                        !DataMgr.DataSourceDataMapper.DoesAnotherDataSourceReferenceIDAndIPExist(originalDataSource.ReferenceID, bulkEditIPList, dsid);
                                    bool existingDSUniqueByRefIDAndDTSID = originalDataSource.DTSID > 0 &&
                                        !DataMgr.DataSourceDataMapper.DoesAnotherDataSourceReferenceIDAndDTSIDExist(originalDataSource.ReferenceID, (int)originalDataSource.DTSID, originalDataSource.DSID);

                                    if ( (!newDSUniqueByRefIDAndIP && !bulkEditIPList.IsNullOrEmpty()) || //if IP list exists, it must be unique                                         
                                         (!newDSUniqueByRefIDAndIP && !existingDSUniqueByRefIDAndDTSID)) //if not uniquely ID'd by iplist/ref, must be unique by dtsid/refid                                  
                                        allowEdit = false;                                    
                                    else
                                    {
                                        mDataSource.IPList = txtBulkEditIPList.Text.IsNullOrEmpty() ? null : txtBulkEditIPList.Text;
                                        changeIPList = true;
                                    }
                                }

                                //bulk edit active
                                if (chkBulkEditVerifyActive.Checked)
                                {
                                    mDataSource.IsActive = chkBulkEditActive.Checked;
                                    changeActive = true;
                                }

                                if (chkBulkEditVerifyDataTransferService.Checked)
                                {
                                    var originalDataSoruce = DataMgr.DataSourceDataMapper.GetDataSourceByID(dsid);
                                    int? editDTSID = Convert.ToInt32(ddlBulkEditDataTransferService.SelectedValue);
                                    if (editDTSID == null || DataMgr.DataSourceDataMapper.DoesAnotherDataSourceReferenceIDAndDTSIDExist(originalDataSoruce.ReferenceID, (int)editDTSID, dsid))
                                        allowEdit = false;
                                    else
                                    {
                                        mDataSource.DTSID = editDTSID;
                                        changeDataTransferService = true;
                                    }
                                }

                                if (chkBulkEditVerifyBMSInfo.Checked)
                                {
                                    mDataSource.BMSInfoID = Convert.ToInt32(ddlBulkEditBMSInfo.SelectedValue);
                                    changeBMSInfo = true;
                                }

                                if (allowEdit)
                                {
                                    DataMgr.DataSourceDataMapper.UpdatePartialDataSource(mDataSource, changeActive, changeIPList, changeDataTransferService, changeBMSInfo);
                                    lblResults.Text += String.Format(bulkEditSuccessful, dataSourceName) + "<br />";
                                }
                                else
                                    lblBulkEditError.Text += String.Format(bulkEditUnableToEditOneOrMoreDataSources, dataSourceName) + "<br />";
                            }
                        }
                        else
                        {
                            LabelHelper.SetLabelMessage(lblBulkEditError, noBulkEditFieldsSelected, lnkSetFocus);
                            return;
                        }

                        if (lblBulkEditError.Text != String.Empty) LabelHelper.SetLabelMessage(lblBulkEditError, lblBulkEditError.Text, lnkSetFocus);
                        if (lblResults.Text != String.Empty) LabelHelper.SetLabelMessage(lblResults, lblResults.Text, lnkSetFocus);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, bulkEditError, sqlEx);
                        LabelHelper.SetLabelMessage(lblBulkEditError, bulkEditFailed, lnkSetFocus);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, bulkEditError, ex);
                        LabelHelper.SetLabelMessage(lblBulkEditError, bulkEditFailed, lnkSetFocus);
                    }
                }

            #endregion

        #endregion

        #region methods

            #region overrides

                protected override void InitializeControl()
                {
                    BindBulkEditDataSources();
                    BindDataTransferServices(ddlBulkEditDataTransferService);
                    BindBMSInfo(ddlBulkEditBMSInfo);

                    var adminUserControlForm = new AdminUserControlForm(this);
                    
                    adminUserControlForm.ClearTextBoxes();
                    adminUserControlForm.ClearCheckBoxes();
                }

            #endregion

            #region binders

                private void BindBulkEditDataSources()
                {
                    lbBulkEditDataSourcesTop.DataSource = DataMgr.DataSourceDataMapper.GetAllDataSourcesWithPartialDataByCID(siteUser.CID);
                    lbBulkEditDataSourcesTop.DataTextField = "DataSourceName";
                    lbBulkEditDataSourcesTop.DataValueField = "DSID";
                    lbBulkEditDataSourcesTop.DataBind();

                    lbBulkEditDataSourcesBottom.Items.Clear();
                }

                private void BindBMSInfo(DropDownList ddl)
                {
                    BindDDL(ddl, "BMSInfoName", "BMSInfoID", DataMgr.BMSInfoDataMapper.GetBMSInfoByClientID(null, siteUser.CID), true);
                }

                private void BindDataTransferServices(DropDownList ddl)
                {
                    BindDDL(ddl, "DataTransferServiceName", "DTSID", DataMgr.DataTransferServiceDataMapper.GetAllActiveServicesByCID(siteUser.CID), true);
                }

            #endregion

        #endregion
    }
}