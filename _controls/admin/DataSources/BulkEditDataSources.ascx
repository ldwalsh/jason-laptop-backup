﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="BulkEditDataSources.ascx.cs" Inherits="CW.Website._controls.admin.BulkEditDataSources" %>

<asp:Panel ID="pnlBulkEditDataSources" runat="server" DefaultButton="btnBulkEditDataSources">

  <h2>Bulk Edit Data Sources</h2>

  <p>This page only allows the bulk edit of the certain data source fields. Try to limit your selection in order not to timeout.</p> 
  
  <div>              
    <a id="lnkSetFocus" runat="server"></a>
    <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
    <asp:Label ID="lblBulkEditError" CssClass="errorMessage" runat="server" />

    <div class="divForm">   
      <label class="label">Client:</label>
      <label class="labelContent"><%= siteUser.ClientName %></label>
    </div>
    <div id="Div1" class="divForm" runat="server">

      <label class="label">Available Data Sources:</label>

      <div class="divListSearchExtender">
        <ajaxToolkit:ListSearchExtender id="lseBulkEditDataSourcesTop" runat="server" TargetControlID="lbBulkEditDataSourcesTop" PromptText="Type to search" PromptCssClass="listSearchExtender" PromptPosition="Top" QueryPattern="Contains" IsSorted="true"/>
      </div>

      <asp:ListBox ID="lbBulkEditDataSourcesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />                                                        

      <div class="divArrows">
        <asp:ImageButton CssClass="up-arrow"  ID="ImageButton1" OnClick="btnBulkEditDataSourcesUpButton_Click" ImageUrl="~/_assets/images/up-arrow.png" runat="server" />
        <asp:ImageButton CssClass="down-arrow" ID="ImageButton2" OnClick="btnBulkEditDataSourcesDownButton_Click" ImageUrl="~/_assets/images/down-arrow.png" runat="server" />
      </div>

      <label class="label">*Selected Data Sources:</label> 

      <asp:ListBox ID="lbBulkEditDataSourcesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple" />

    </div>
    <hr />
    
    <h2>Optional Bulk Edit Fields</h2>

    <p>Please check, and select or fill in all fields you wish to bulk edit. Leave any fields unchecked, blank, and unselected if you do not wish to bulk edit them.</p>
    
    <div class="divForm">
      <label class="label">IP List (CSV):</label>
      <asp:TextBox ID="txtBulkEditIPList" CssClass="textbox" MaxLength="500" runat="server" />
      |<asp:CheckBox ID="chkBulkEditVerifyIPList" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                                                                                        
      <p>(Note: Please do not enter 127.0.0.1)<br />(Warning: If no ip address is set, a data transfer service guid is required for incoming data.)</p> 
    </div> 

    <ajaxToolkit:FilteredTextBoxExtender ID="bulkEditIPListFTE" runat="server" TargetControlID="txtBulkEditIPList" FilterType="Custom, Numbers" ValidChars=".," />
    <asp:RegularExpressionValidator ID="bulkEditIPListREV" runat="server" ErrorMessage="Invalid IP Address List Format." ValidationExpression="(\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b)(,\s*\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b)*" ControlToValidate="txtBulkEditIPList" SetFocusOnError="true" Display="None" ValidationGroup="BulkEdit" />
    <ajaxToolkit:ValidatorCalloutExtender ID="bulkEditIPListVCE" runat="server" BehaviorID="bulkEditIPListVCE" TargetControlID="bulkEditIPListREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

    <div class="divForm">
      <label class="label">Active:</label>
      <asp:CheckBox ID="chkBulkEditActive" CssClass="checkbox" runat="server" />     
      |<asp:CheckBox ID="chkBulkEditVerifyActive" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                                   
      <p>(Warning: Setting a datasource as inactive will not allow incoming data to be recorded.)</p>
    </div>

    <div class="divForm">
      <label class="label">Data Transfer Service:</label>
      <asp:DropDownList ID="ddlBulkEditDataTransferService" CssClass="dropdown" AppendDataBoundItems="true" runat="server" /> 
      |<asp:CheckBox ID="chkBulkEditVerifyDataTransferService" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                                   
    </div>

    <div class="divForm">
      <label class="label">BMS Info:</label>
      <asp:DropDownList ID="ddlBulkEditBMSInfo" CssClass="dropdown" AppendDataBoundItems="true" runat="server" /> 
      |<asp:CheckBox ID="chkBulkEditVerifyBMSInfo" CssClass="checkbox" runat="server" /><span> - Check to verify bulk edit on this field.</span>                                                   
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnBulkEditDataSources" runat="server" Text="Update Data Sources" OnClick="bulkEditDataSourcesButton_Click" ValidationGroup="BulkEdit" />

  </div>

</asp:Panel>