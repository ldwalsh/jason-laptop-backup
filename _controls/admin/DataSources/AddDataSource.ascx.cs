﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace CW.Website._controls.admin
{
    public partial class AddDataSource : AdminUserControlBase
    {
        #region fields

            private DataSource mDataSource;

        #endregion

        #region constants

            const String addDataSourceSuccess = " data source addition was successful.";
            const String addDataSourceFailed = "Adding data source failed. Please contact an administrator.";
            const String addDataSourceSqlFailed = "Error adding data source.";
            const String dataSourceNameExists = "Data source with provided name already exists.";
            const String dataSourceExists = "Data source with provided reference id and ip or dtsid already exists.";
            const String identifierRequired = "Data transfer service or ip is required.";

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                lblResults.Visible = false;
                lblAddError.Visible = false;

                if (!Page.IsPostBack)
                {
                    BindVendors(ddlAddVendor);
                    BindBMSInfo(ddlAddBMSInfo);
                    BindDataTransferServices(ddlAddDataTransferService);
                    BindVendorProducts(ddlAddVendorProduct, Convert.ToInt32(ddlAddVendor.SelectedValue));
                    BindRetrievalMethods(ddlAddRetrievalMethod, Convert.ToInt32(ddlAddVendorProduct.SelectedValue));
                    BindVendorContacts(ddlAddPrimaryContact, Convert.ToInt32(ddlAddVendor.SelectedValue));
                    BindVendorContacts(ddlAddSecondaryContact, Convert.ToInt32(ddlAddVendor.SelectedValue));
                }
            }

            #region ddl events

                protected void ddlAddVendor_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindVendorProducts(ddlAddVendorProduct, Convert.ToInt32(ddlAddVendor.SelectedValue));
                    BindVendorContacts(ddlAddPrimaryContact, Convert.ToInt32(ddlAddVendor.SelectedValue));
                    BindVendorContacts(ddlAddSecondaryContact, Convert.ToInt32(ddlAddVendor.SelectedValue));
                }

                protected void ddlAddVendorProduct_OnSelectedIndexChanged(Object sender, EventArgs e)
                {
                    BindRetrievalMethods(ddlAddRetrievalMethod, Convert.ToInt32(ddlAddVendorProduct.SelectedValue));
                }

            #endregion

            #region button events

                protected void addDataSourceButton_Click(Object sender, EventArgs e)
                {
                    mDataSource = new DataSource();

                    LoadAddFormIntoDataSource(mDataSource);

                    if (String.IsNullOrWhiteSpace(mDataSource.IPList) && mDataSource.DTSID == null)
                    {
                        LabelHelper.SetLabelMessage(lblAddError, identifierRequired, lnkSetFocus);
                        return;
                    }

                    if (DataMgr.DataSourceDataMapper.DoesDataSourceExistForClient(mDataSource.CID, null, mDataSource.DataSourceName))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, dataSourceNameExists, lnkSetFocus);
                        return;
                    }
                    else if (mDataSource.DTSID != null && DataMgr.DataSourceDataMapper.DoesDataSourceReferenceIDAndDTSIDExist(mDataSource.ReferenceID, (int)mDataSource.DTSID))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, dataSourceExists, lnkSetFocus);
                        return;
                    }
                    else if (!String.IsNullOrWhiteSpace(mDataSource.IPList) && DataMgr.DataSourceDataMapper.DoesDataSourceReferenceIDAndIPExist(mDataSource.ReferenceID, StringHelper.ParseCSVString(mDataSource.IPList)))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, dataSourceExists, lnkSetFocus);
                        return;
                    }
                    else
                    {
                        try
                        {
                            DataMgr.DataSourceDataMapper.InsertDataSource(mDataSource);

                            LabelHelper.SetLabelMessage(lblResults, mDataSource.DataSourceName + addDataSourceSuccess, lnkSetFocus);
                            OperationComplete(KGSDataSourceAdministration.TabMessages.AddDataSource);
                        }
                        catch (SqlException sqlEx)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, addDataSourceSqlFailed, sqlEx);
                            LabelHelper.SetLabelMessage(lblAddError, mDataSource.DataSourceName + addDataSourceFailed, lnkSetFocus);
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, addDataSourceSqlFailed, ex);
                            LabelHelper.SetLabelMessage(lblAddError, mDataSource.DataSourceName + addDataSourceFailed, lnkSetFocus);
                        }
                    }
                }

            #endregion

            #region checkbox events

                protected void chkAddIntegrated_OnCheckedChanged(Object sender, EventArgs e)
                {
                    divAddIntegrated.Visible = addIntegratedUrlRFV.Enabled = addIntegratedUsernameRFV.Enabled = addIntegratedPasswordRFV.Enabled = chkAddIntegrated.Checked;
                }

            #endregion

        #endregion

        #region methods

            private void LoadAddFormIntoDataSource(DataSource dataSource)
            {
                dataSource.DataSourceName = txtAddDataSourceName.Text;
                dataSource.DTSID = ddlAddDataTransferService.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddDataTransferService.SelectedValue) : null;
                dataSource.IPList = String.IsNullOrEmpty(txtAddIPList.Text) ? null : txtAddIPList.Text;
                dataSource.ReferenceID = txtAddReferenceID.Text.Trim();
                dataSource.BMSInfoID = ddlAddBMSInfo.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddBMSInfo.SelectedValue) : null;
                dataSource.Installer = String.IsNullOrEmpty(txtAddInstaller.Text) ? null : txtAddInstaller.Text;
                dataSource.Location = String.IsNullOrEmpty(txtAddLocation.Value) ? null : txtAddLocation.Value;
                dataSource.TimeOffset = Convert.ToInt32(txtAddTimeOffset.Text);
                dataSource.Description = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                dataSource.CID = siteUser.CID;
                dataSource.VendorProductID = Convert.ToInt32(ddlAddVendorProduct.SelectedValue);
                dataSource.VendorProductRetrievalMethodID = Convert.ToInt32(ddlAddRetrievalMethod.SelectedValue);
                dataSource.PrimaryContactID = ddlAddPrimaryContact.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddPrimaryContact.SelectedValue) : null;
                dataSource.SecondaryContactID = ddlAddSecondaryContact.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddSecondaryContact.SelectedValue) : null;
                dataSource.IsIntegrated = chkAddIntegrated.Checked;
                dataSource.IsRealtime = chkAddRealtime.Checked;                

                if (chkAddIntegrated.Checked)
                {
                    dataSource.IntegratedUrl = txtAddIntegratedUrl.Text;
                    dataSource.IntegratedUsername = txtAddIntegratedUsername.Text;
                    dataSource.IntegratedPassword = txtAddIntegratedPassword.Text;
                }

                dataSource.IsActive = true;
                dataSource.DateModified = DateTime.UtcNow;
            }

            #region binders

                private void BindBMSInfo(DropDownList ddl)
                {
                    BindDDL(ddl, "BMSInfoName", "BMSInfoID", DataMgr.BMSInfoDataMapper.GetBMSInfoByClientID(null, siteUser.CID), true);
                }

                private void BindDataTransferServices(DropDownList ddl)
                {
                    BindDDL(ddl, "DataTransferServiceName", "DTSID", DataMgr.DataTransferServiceDataMapper.GetAllActiveServicesByCID(siteUser.CID), true);
                }

                private void BindVendors(DropDownList ddl)
                {
                    BindDDL(ddl, "VendorName", "VendorID", DataMgr.DataSourceVendorDataMapper.GetAllVendors(), true);
                }

                private void BindVendorProducts(DropDownList ddl, Int32 vendorID)
                {
                    var lItem = new ListItem() { Text = ((vendorID != -1) ? "Select one..." : "Select vendor first..."), Value = "-1", Selected = true };

                    BindDDL(ddl, "VendorProductName", "VendorProductID", (vendorID != -1) ? DataMgr.DataSourceVendorProductDataMapper.GetAllVendorProductsByVendorID(vendorID) : null, true, lItem);
                }

                private void BindRetrievalMethods(DropDownList ddl, Int32 vendorProductID)
                {
                    var lItem = new ListItem() { Text = ((vendorProductID != -1) ? "Select one..." : "Select vendor product first..."), Value = "-1", Selected = true };

                    BindDDL(ddl, "RetrievalMethod", "VendorProductRetrievalMethodID", (vendorProductID != -1) ? DataMgr.DataSourceVendorProductDataMapper.GetAllActiveDataSourceVendorProductsRetrievalMethodsByProductIDAsDropdownFormat(vendorProductID) : null, true, lItem);
                }

                private void BindVendorContacts(DropDownList ddl, Int32 vendorID)
                {
                    var lItem = new ListItem() { Text = ((vendorID != -1) ? "Optionally select one..." : "Select vendor first..."), Value = "-1", Selected = true };

                    BindDDL(ddl, "Name", "ContactID", (vendorID != -1) ? DataMgr.DataSourceVendorContactDataMapper.GetAllVendorContactsByVendorID(vendorID) : null, true, lItem);
                }

            #endregion

        #endregion
    }
}