﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ViewDataSources.ascx.cs" Inherits="CW.Website._controls.admin.DataSources.ViewDataSources" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Import Namespace="CW.Data.Models.DataSource" %>
<%@ Import Namespace="CW.Utility" %>

<h2>View Data Sources</h2>

<p>Please notify the development team if you are deleting the last integrated datasource for a client, as the azure instance can be taken down.</p>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
  <asp:Label ID="lblDeleteError" CssClass="errorMessage" runat="server" />
</p>

<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div>
<br/>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <div id="divSuperSearch" runat="server">
    <label class="labelLeft">Search across all clients:</label>
    <asp:CheckBox ID="chkAllClients" runat="server" CssClass="checkbox" /> 
  </div>

  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>
                                                                                                                
<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:CommandField ShowSelectButton="true" />
      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkEdit" Runat="server" CausesValidation="false" CommandName="Edit" Text="Edit" />
        </ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataSourceName" HeaderText="Data Source Name">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.DataSourceName)) %>"><%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.DataSourceName), 30) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ClientName" HeaderText="Client">          
        <ItemTemplate>
            <label title="<%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.ClientName)) %>"><%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.ClientName), 20) %></label></label>
            <asp:HiddenField ID="hdnCID" runat="server" Visible="true" Value='<%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.CID)) %>' />
        </ItemTemplate>        
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="IPList" HeaderText="IP List">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.IPList)) %>"><%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.IPList), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DTSName" HeaderText="DTSName">  
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.DTSName)) %>"><%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.DTSName), 20) %></label></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ReferenceID" HeaderText="ReferenceID"> 
        <ItemTemplate><label title="<%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.ReferenceID)) %>"><%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.ReferenceID), 30) %></label></ItemTemplate>                                                                          
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Integrated">  
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.IsIntegrated)) %></ItemTemplate>                                                  
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Active">  
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<GetDataSourcesData>(_ => _.IsActive)) %></ItemTemplate>                                                  
      </asp:TemplateField>

      <asp:TemplateField>
        <ItemTemplate>                                                
          <asp:LinkButton ID="lnkDelete" Runat="server" CausesValidation="false" OnClientClick="return confirm('Are you sure you wish to delete this data source permanently?\n\nAlternatively you can deactivate a data source temporarily instead.');" CommandName="Delete" Text="Delete" />
        </ItemTemplate>
      </asp:TemplateField>                                                                                                                                                                          
    </Columns>        
  </asp:GridView>                                                                       
</div>
<br />
<br />

<div>
  <!--SELECT DATA SOURCE DETAILS VIEW -->
  <asp:Panel ID="pnlDetailsView" runat="server" Visible="false">
    <div>

      <asp:Repeater ID="rptDataSourceDetails" runat="server" OnItemDataBound="repeater_ItemDataBound">
        <HeaderTemplate>
          <h2>Data Source Details</h2>
          <ul id="dataSourceDetails" class="detailsList">
        </HeaderTemplate>
        <ItemTemplate>
          <CW:UListItem ID="uListItem" runat="server" />
        </ItemTemplate>
        <FooterTemplate>
          </ul>
        </FooterTemplate>
      </asp:Repeater>

    </div>
  </asp:Panel>
</div>                  
                                                                     
<!--EDIT DATASOURCE PANEL -->                              
<asp:Panel ID="pnlEdit" runat="server" Visible="false" DefaultButton="btnUpdateDataSource">                                                                                             
  <div>
    <h2>Edit Data Source</h2>
  </div>

  <div>                                                    
    <a id="lnkSetFocusEdit" runat="server"></a>           
    <asp:Label ID="lblEditError" CssClass="errorMessage" runat="server" />
    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
    <div class="divForm">
      <label class="label">*Data Source Name:</label>
      <asp:TextBox ID="txtEditDataSourceName" CssClass="textbox" MaxLength="100" runat="server" />                                              
    </div>
    <div class="divForm">   
      <label class="label">Data Transfer Service:</label>    
      <asp:DropDownList ID="ddlEditDataTransferService" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">
      <label class="label">Client:</label>
      <asp:Label ID="lblClientName" CssClass="labelContent" runat="server"></asp:Label>
    </div>   
    <div class="divForm">
      <label class="label">IP List (CSV):</label>
      <asp:TextBox ID="txtEditIPList" CssClass="textbox" MaxLength="500" runat="server"></asp:TextBox>                                                                                                        
      <p>(Note: Please do not change to 127.0.0.1)<br />(Warning: Changes may result in an ip mismatch for incoming data if a data transfer service guid is not being used.)</p>                                                        
    </div>                                                                                                                                                                                           
    <div class="divForm">
      <label class="label">*ReferenceID:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditReferenceID" runat="server" MaxLength="25" />
      <p>(Note: The datasource reference id is unique to a control systems db, or a device depending on the method of data trasfer. For device data transfer, multiple devices on the same network must have a unique reference id set directly on the each device.
         For console level db data transfer, a control system db's default reference id is 0, unless more than one control system db exists on the same network producing the same ip. In that circumstance the unique reference id's must be set on the config on the console level.)</p>
    </div>
    <div class="divForm">   
      <label class="label">BMS Info:</label>    
      <asp:DropDownList ID="ddlEditBMSInfo" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>                                                                                                                                                 
    <div class="divForm">
      <label class="label">Installer:</label>
      <asp:TextBox CssClass="textbox" ID="txtEditInstaller" runat="server" MaxLength="100" />
    </div>
    <div class="divForm">
      <label class="label">Location:</label>
      <textarea name="txtEditLocation" id="txtEditLocation" cols="40" rows="5" onkeyup="limitChars(this, 250, 'divEditLocationCharInfo')" runat="server" />
      <div id="divEditLocationCharInfo"></div>
    </div>                                                       
    <div class="divForm">                                                                                                                  
      <label class="label">*Time Offset (Default=0):</label>   
      <asp:TextBox CssClass="textbox" ID="txtEditTimeOffset" runat="server" MaxLength="2" />
      <p>(Note: This value is a corrective hourly offset integer that can be negative. Used to adjust the recorded timestamp on incoming data if the data source is inaccurate in comparison to it's location and timezone.)</p>
      <p>(Note: If this data source is an ADDMEJR, then we must use the offset based off the difference from the clients timezone and the cloud server timezone location.)</p> 
    </div>                                                                                                                                                                                                            
    <div class="divForm">
      <label class="label">Description:</label>
      <textarea name="txtEditDescription" id="txtEditDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" runat="server" />
      <div id="divEditDescriptionCharInfo"></div>                                                           
    </div>                                                     
    <div class="divForm">   
      <label class="label">*Vendor:</label>    
      <asp:DropDownList ID="ddlEditVendor" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditVendor_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>                                                                                                         
    <div class="divForm">   
      <label class="label">*Vendor Product:</label>
      <asp:DropDownList ID="ddlEditVendorProduct" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditVendorProduct_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">*Retrieval Method:</label>    
      <asp:DropDownList ID="ddlEditRetrievalMethod" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
    </div>
    <div class="divForm">   
      <label class="label">Primary Contact:</label>    
      <asp:DropDownList ID="ddlEditPrimaryContact" AppendDataBoundItems="true" CssClass="dropdown" runat="server" />
    </div>    
    <div class="divForm">   
      <label class="label">Secondary Contact:</label>    
      <asp:DropDownList ID="ddlEditSecondaryContact" AppendDataBoundItems="true" CssClass="dropdown" runat="server" />
    </div>    
    <div class="divForm">
      <label class="label">*Integrated:</label>
      <asp:CheckBox ID="chkEditIntegrated" CssClass="checkbox" OnCheckedChanged="chkEditIntegrated_OnCheckedChanged" AutoPostBack="true" runat="server" />   
      <p>(Note: This is for integrated web services.)</p>
      <p>(Warning: Unchecking after actively integrated will clear integrated values and data will no longer be collected via the web services.)</p>
    </div>    
    <div id="divEditIntegrated" runat="server">

      <div class="divForm">
        <label class="label">*Integrated Url:</label>
        <asp:TextBox CssClass="textbox" ID="txtEditIntegratedUrl" runat="server" MaxLength="250" />
      </div>  
      <div class="divForm">
        <label class="label">*Integrated Username:</label>
        <asp:TextBox CssClass="textbox" ID="txtEditIntegratedUsername" runat="server" MaxLength="100" />
      </div>  
      <div class="divForm">
        <label class="label">*Integrated Password:</label>
        <asp:TextBox CssClass="textbox" ID="txtEditIntegratedPassword" runat="server" MaxLength="50" />
      </div>

    </div>
    <div class="divForm">
      <label class="label">Realtime:</label>
      <asp:CheckBox ID="chkEditRealtime" CssClass="checkbox" runat="server" />   
      <p>(Note: This is for automated notifications, determines if it should be full day or last hour.)</p>
      <p>(Note: Uncheck if data is sent from a csv at the end of the day.)</p>
    </div>                                                                                                  
    <div class="divForm">
      <label class="label">*Active:</label>
      <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />   
      <p>(Warning: Setting a datasource as inactive will not allow incoming data to be recorded.)</p>
    </div>

    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateDataSource" runat="server" Text="Update Data Source"  OnClick="updateDataSourceButton_Click" ValidationGroup="EditDataSource" />

  </div>

  <!--Ajax Validators-->      
  <asp:RequiredFieldValidator ID="editDataSourceNameRFV" runat="server" ErrorMessage="Data Source Name is a required field." ControlToValidate="txtEditDataSourceName" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editDataSourceNameVCE" runat="server" BehaviorID="editDataSourceNameVCE" TargetControlID="editDataSourceNameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <ajaxToolkit:FilteredTextBoxExtender ID="editIPListFTE" runat="server" TargetControlID="txtEditIPList" FilterType="Custom, Numbers" ValidChars=".," />  
  <asp:RegularExpressionValidator ID="editIPListREV" runat="server" ErrorMessage="Invalid IP Address List Format." ValidationExpression="(\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b)(,\s*\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b)*" ControlToValidate="txtEditIPList" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editIPListREVVCE" runat="server" BehaviorID="editIPListREVVCE" TargetControlID="editIPListREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editReferenceIDRFV" runat="server" ErrorMessage="ReferenceID is a required field." ControlToValidate="txtEditReferenceID" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editReferenceIDVCE" runat="server" BehaviorID="editReferenceIDVCE" TargetControlID="editReferenceIDRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
    
  <ajaxToolkit:FilteredTextBoxExtender ID="editTimeOffsetFTE" runat="server" TargetControlID="txtEditTimeOffset" FilterType="Custom, Numbers" ValidChars="-" />
  <asp:RequiredFieldValidator ID="editTimeOffsetRFV" runat="server" ErrorMessage="Time Offset is a required field." ControlToValidate="txtEditTimeOffset" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editTimeOffsetVCE" runat="server" BehaviorID="editTimeOffsetVCE" TargetControlID="editTimeOffsetRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:RegularExpressionValidator ID="editTimeOffsetREV" runat="server" ErrorMessage="Invalid Numeric Format." ValidationExpression="^(\d|-)*\d$" ControlToValidate="txtEditTimeOffset" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editTimeOffsetREVVCE" runat="server" BehaviorID="editTimeOffsetREVVCE" TargetControlID="editTimeOffsetREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editVendorRFV" runat="server" ErrorMessage="Vendor is a required field." ControlToValidate="ddlEditVendor" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editVendorVCE" runat="server" BehaviorID="editVendorVCE" TargetControlID="editVendorRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  
  <asp:RequiredFieldValidator ID="editVendorProductRFV" runat="server" ErrorMessage="Vendor Product is a required field." ControlToValidate="ddlEditVendorProduct" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editVendorProductVCE" runat="server" BehaviorID="editVendorProductVCE" TargetControlID="editVendorProductRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editRetrievalMethodRFV" runat="server" ErrorMessage="Retrieval Method is a required field." ControlToValidate="ddlEditRetrievalMethod" SetFocusOnError="true" Display="None" InitialValue="-1" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editRetrievalMethodVCE" runat="server" BehaviorID="editRetrievalMethodVCE" TargetControlID="editRetrievalMethodRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
            
  <asp:RequiredFieldValidator ID="editIntegratedUrlRFV" runat="server" ErrorMessage="Integrated Url is a required field." ControlToValidate="txtEditIntegratedUrl" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editIntegratedUrlVCE" runat="server" BehaviorID="editIntegratedUrlVCE" TargetControlID="editIntegratedUrlRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
  <asp:RegularExpressionValidator ID="editIntegratedUrlREV" runat="server" ErrorMessage="Invalid URL Format." ValidationExpression="((http(s)?://)?([\w-]+\.)+[\w-]+(/[\w- ;,./?%&=]*)?|(http(s)?://local/)([\w-/]*)?)" ControlToValidate="txtEditIntegratedUrl" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editIntegratedUrlREVVCE" runat="server" BehaviorID="editIntegratedUrlREVVCE" TargetControlID="editIntegratedUrlREV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editIntegratedUsernameRFV" runat="server" ErrorMessage="Integrated Username is a required field." ControlToValidate="txtEditIntegratedUsername" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editIntegratedUsernameVCE" runat="server" BehaviorID="editIntegratedUsernameVCE" TargetControlID="editIntegratedUsernameRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />

  <asp:RequiredFieldValidator ID="editIntegratedPasswordRFV" runat="server" ErrorMessage="Integrated Password is a required field." ControlToValidate="txtEditIntegratedPassword" SetFocusOnError="true" Display="None" ValidationGroup="EditDataSource" />
  <ajaxToolkit:ValidatorCalloutExtender ID="editIntegratedPasswordVCE" runat="server" BehaviorID="editIntegratedPasswordVCE" TargetControlID="editIntegratedPasswordRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
</asp:Panel>