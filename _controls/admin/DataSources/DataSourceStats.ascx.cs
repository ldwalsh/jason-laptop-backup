﻿using CW.Data.Models.DataSource;
using CW.Utility;
using CW.Website._administration;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CW.Website._controls.admin.DataSources
{
    public partial class DataSourceStats : AdminUserControlGrid
    {
        #region fields

            private static String name = "data source statistic";

        #endregion

        #region properties

            #region overrides

                protected override IEnumerable<String> GridColumnNames
                {
                    get { return PropHelper.F<DataSourceAdministrationStatistics>(_ => _.DSID, _ => _.DataSourceName, _ => _.PointCount); }
                }

                protected override IEnumerable GetEntitiesForGrid(IEnumerable<string> searchCriteria)
                {
                    return DataMgr.DataSourceDataMapper.GetDataSourceAdministrationStatisticsByCID(siteUser.CID, searchCriteria); 
                }

                protected override String NameField { get { return PropHelper.G<DataSourceAdministrationStatistics>(_ => _.DataSourceName); } }

                protected override String IdColumnName { get { return PropHelper.G<DataSourceAdministrationStatistics>(_ => _.DSID); } }

                protected override String Name { get { return name; } }

                protected override Type Entity { get { return typeof(DataSourceAdministrationStatistics); } }

                protected override String CacheHashKey { get { return FormatHashKey(new object[] { siteUser.CID }); } }

                public override IEnumerable<Enum> ReactToChangeStateList
                {
                    get
                    {
                        return new Enum[] 
                        {
                            KGSDataSourceAdministration.TabMessages.Init,
                            KGSDataSourceAdministration.TabMessages.AddDataSource,
                            KGSDataSourceAdministration.TabMessages.EditDataSource,
                            KGSDataSourceAdministration.TabMessages.DeleteDataSource
                        };
                    }
                }

            #endregion

            public static GridCacher.CacheKeyInfo CreateCacheKey(Int32 cid)
            {
                return new GridCacher.CacheKeyInfo(name + "." + typeof(DataSourceAdministrationStatistics).Name, cid.ToString());
            }

        #endregion

        #region events

            protected void Page_Load(Object sender, EventArgs e)
            {
                IsDeferredLoading = (!Page.IsPostBack);
            }

        #endregion
    }
}