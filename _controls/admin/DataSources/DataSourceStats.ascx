﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="DataSourceStats.ascx.cs" Inherits="CW.Website._controls.admin.DataSources.DataSourceStats" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Data.Models.DataSource" %>

<h2>Data Source Stats</h2>

<p>Inital loading of this tab may take some time.  Subsequent operations (sorting, paging and searching) will process quickly by utilizing cached data.</p>

<p>
  <a id="lnkSetFocus" href="#" runat="server"></a>
  <asp:Label ID="lblResults" CssClass="successMessage" runat="server" />
</p>

<div class="divForm">
  <label class="label">Client:</label>
  <label class="labelContent"><%= siteUser.ClientName %></label>
</div>

<asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
  <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click" />
  <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox" />
  <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
</asp:Panel>

<div id="gridTbl">
  <asp:GridView ID="grid" EnableViewState="true" runat="server"> 
    <PagerSettings PageButtonCount="20" />
    <HeaderStyle CssClass="tblTitle" />
    <AlternatingRowStyle CssClass="tblCol2" />
    <RowStyle CssClass="tblCol1" Wrap="true" />
    <Columns>
      <asp:TemplateField ItemStyle-Wrap="true" SortExpression="DataSourceName" HeaderText="Data Source Name">
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<DataSourceAdministrationStatistics>(_ => _.DataSourceName), 20) %></ItemTemplate>
      </asp:TemplateField>

      <asp:TemplateField ItemStyle-Wrap="true" ItemStyle-CssClass="gridAlignCenter" SortExpression="PointCount" HeaderText="# of Points">
        <ItemTemplate><%# GetCellContent(Container, PropHelper.G<DataSourceAdministrationStatistics>(_ => _.PointCount)) %></ItemTemplate>
      </asp:TemplateField>                                                                                                                                            
    </Columns>
  </asp:GridView>
</div> 