﻿using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using CW.Utility;

namespace CW.Website._controls
{
    public partial class Commodities : SiteUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var service = new OilPricesNetCommoditesService();

            rptCommodities.DataSource = service.GetCommodities(
                    new []{ CommoditiesService.CommodityType.NaturalGas, CommoditiesService.CommodityType.HeatingOil })
                .Select(a => new { DisplayName = GetDisplayName(a.Item1), Value = a.Item2, Percentage = a.Item3, Color = GetColor(a.Item3) });
            rptCommodities.DataBind();
        }

        private string GetDisplayName(CommoditiesService.CommodityType type)
        {
            switch(type)
            {
                case CommoditiesService.CommodityType.Cooper:
                    return "Cooper";
                case CommoditiesService.CommodityType.CrudeOil:
                    return "Crude Oil";
                case CommoditiesService.CommodityType.Gasoline:
                    return "Gasoline";
                case CommoditiesService.CommodityType.Gold:
                    return "Gold";
                case CommoditiesService.CommodityType.HeatingOil:
                    return "Heating Oil";
                case CommoditiesService.CommodityType.NaturalGas:
                    return "Natural Gas";
                case CommoditiesService.CommodityType.Silver:
                    return "Silver";
            }

            return "";
        }

        private string GetColor(string percentage)
        {
            return percentage.StartsWith("-") ? "red" : "green";
        }
    }
}