﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SecondarySystemAdminNav.ascx.cs" Inherits="CW.Website._controls.SecondarySystemAdminNav" %>
<%@ Import Namespace="CW.Website._framework.httpmodules" %>
          
 <div class="sysAdminNav">

  <ul class="dropdown">
    <li><a href="/SystemAdministration.aspx" class="hoverBtn">System Home</a></li>
    <li><a class="hoverBtnNoCursor">System</a>
      <ul class="sub_menu">
        <li><a href="/SystemCache.aspx">System Cache</a></li>
        <li><a href="/SystemLogs.aspx">System Logs</a></li>
        <li><a href="/SystemReports.aspx">System Reports</a></li>
        <li runat="server" data-boolean-check="7"><a href="/SystemSettings.aspx">System Settings</a></li>
        <li><a href="/SystemTest.aspx">System Test</a></li>                   	                      			 
      </ul>
    </li>
    <li runat="server" data-boolean-check="9"><a class="hoverBtnNoCursor">Data Sources</a>
      <ul class="sub_menu">
        <li><a href="/SystemDataSourceVendorProductsRetrievalMethodsAdministration.aspx">Product Retrieval Methods</a></li>
        <li><a href="/SystemRetrievalMethodAdministration.aspx">Retrieval Methods</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Users</a>
      <ul class="sub_menu">
        <li><a href="/SystemUserRoleAdministration.aspx">Roles</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Settings</a>
      <ul class="sub_menu">
        <li><a href="/GlobalSettings.aspx">Global Settings</a></li>
      </ul>
    </li>
    <li runat="server" data-boolean-check="9"><a class="hoverBtnNoCursor">Services</a>
      <ul class="sub_menu">
        <li><a href="/SystemAPISubscriberAdministration.aspx">API Subscribers</a></li>  
        <li><a href="/SystemServiceAdministration.aspx">Data Transfer Services</a></li>  	
        <li><a href="/SystemServiceKeyAdministration.aspx">Service Keys</a></li>
        <li><a href="/SystemBMSInfoAdministration.aspx">BMS Information</a></li>	
        <li><a href="/SystemUploadBMSInfo.aspx">Upload BMS Info</a></li> 
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Other</a>
      <ul class="sub_menu">
        <li runat="server" data-boolean-check="9"><a href="/<%= PathRewriteModule.SystemBackportAdministration %>">Backport Adminstration</a></li>
        <li runat="server" data-boolean-check="9"><a href="/<%= PathRewriteModule.SystemBackportBlobAdministration %>">Backport Blob Adminstration</a></li>
        <li runat="server" data-boolean-check="9"><a href="/<%= PathRewriteModule.SystemBackportDatabaseAdministration %>">Backport Database Adminstration</a></li>
        <li><a href="/<%= PathRewriteModule.SystemContentAdministration %>">Content</a></li>
        <li><a href="/SystemFAQAdministration.aspx">FAQs</a></li>
        <li><a href="/<%= PathRewriteModule.SystemFaultAdministration %>">Faults</a></li>
        <li><a href="/<%= PathRewriteModule.SystemGlobalFileAdministration %>">Global Files</a></li>
        <li><a href="/MatlabAssembly">Matlab Assemblies</a></li>
        <li runat="server" data-boolean-check="9"><a href="/SystemModuleAdministration.aspx">Modules</a></li>
        <li runat="server" data-boolean-check="9"><a href="/SystemQuickLinkAdministration.aspx">Quick Links</a></li>
        <li runat="server" data-boolean-check="9"><a href="/SystemReleaseNotes.aspx">Release Notes</a></li>                
      </ul>
    </li>
  </ul>

</div>         