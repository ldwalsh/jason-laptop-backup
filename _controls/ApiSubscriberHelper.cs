﻿using CW.Business;
using CW.RESTAdapter.MAPIM;
using System.Configuration;
using System.Threading.Tasks;

namespace CW.Website._controls
{
    public class ApiSubscriberHelper
    {
        private int mUserId;
        public string Error { get; private set; }
        public ApiSubscriberHelper(int userId)
        {
            mUserId = userId;
        }
        public async Task<bool> SignalAuthorizationsChangedAsync(DataManager mgr)
        {
            bool ok = false;
            Error = null;
            string subscriberId = mgr.UserDataMapper.GetSubscriberIdFromUserId(mUserId);
            if (!string.IsNullOrEmpty(subscriberId)) // User is an api subscriber?
            {
                // Yes, therefore signal DA to purge cached authz for user
                RESTAPISubscriber restSubscriber = new RESTAPISubscriber(ConfigurationManager.AppSettings["DeploymentID"]);
                ok = await restSubscriber.PurgeSubscriberCacheAsync(subscriberId).ConfigureAwait(false);
            }
            return (ok);
        }

        static public string GeneratePassword()
        {
            int numberOfNonAlphanumericCharacters = (new System.Random()).Next(1, 5);
            return (System.Web.Security.Membership.GeneratePassword(12, numberOfNonAlphanumericCharacters));
        }
    }
}
