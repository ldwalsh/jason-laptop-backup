﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LastMonthsDiagnosticSummary.ascx.cs" Inherits="CW.Website._controls.LastMonthsDiagnosticSummary" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Literal ID="litError" runat="server" />
<asp:Literal ID="litLastMonthsSummary" runat="server" />

<div id="gridTbl">
                                            
  <asp:GridView ID="gridLastMonthsTopPortfolioDiagnosticSummaries" EnableViewState="true" runat="server" DataKeyNames="BID" GridLines="None" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true" EmptyDataRowStyle-CssClass="dockEmptyGridLabel"> 
    <Columns>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Client" Visible="false">  
        <ItemTemplate><%# StringHelper.TrimText(Eval("ClientName"),30) %></ItemTemplate>
      </asp:TemplateField> 
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Building">
        <ItemTemplate><%# StringHelper.TrimText(Eval("BuildingName"),30) %></ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Faults">
        <ItemTemplate><%# Eval("FaultCount") %></ItemTemplate>
      </asp:TemplateField>       
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Avoidable Costs">
        <ItemTemplate><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval("CostSavings"))) %></ItemTemplate>
      </asp:TemplateField>         
      <asp:TemplateField ItemStyle-Wrap="true">
        <ItemTemplate><a href="Diagnostics.aspx?cid=<%# Eval("CID") %>&bid=<%# Eval("BID") %>&rng=<%# Eval("AnalysisRange") %>&sd=<%# Eval("StartDate") %>" class='dockLink'>view</a></ItemTemplate>
      </asp:TemplateField>                                                                                                                                                                         
    </Columns>        
  </asp:GridView> 

</div>