﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SearchCriteria.ascx.cs" Inherits="CW.Website._controls.tasks.SearchCriteria" %>
<%@ Register Assembly="CW.Website" Namespace="CW.Website._extensions" TagPrefix="extensions" %>
<%@ Register TagPrefix="telerik" Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" %>
<%@ Register TagPrefix="CW" TagName="DropDowns" src="~/_controls/DropDowns.ascx" %>

<fieldset class="moduleSearch">
  <legend>Search Criteria <asp:HyperLink ID="lnkSearch" runat="server" CssClass="searchToggle"><asp:Label ID="lblSearch" runat="server"></asp:Label></asp:HyperLink>&nbsp;</legend>
  <ajaxToolkit:CollapsiblePanelExtender ID="criteriaCPE" runat="server" TargetControlID="pnlCriteria" CollapsedSize="0" ExpandControlID="lnkSearch" CollapseControlID="lnkSearch" AutoCollapse="false" AutoExpand="false" ScrollContents="false" ExpandDirection="Vertical" TextLabelID="lblSearch" CollapsedText="+" ExpandedText="-" />

  <asp:Panel ID="pnlCriteria" runat="server" DefaultButton="btnGenerateData" CssClass="criteria">

    <asp:HiddenField ID="hdnTaskId" runat="server" />

    <table>
      <tr>
        <td>

          <div class="divCriteria">
            <div>
              <h3>View By</h3>
            </div>
            <div class="divFormWidest">
              <asp:RadioButtonList ID="rblViewBy" runat="server" CssClass="radio" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="rblViewBy_SelectedIndexChanged">
              <asp:ListItem Value="Building" Text="Building" />
              <asp:ListItem Value="EquipmentClass" Text="Equipment Class" />
              <asp:ListItem Value="Equipment" Text="Equipment" />
              <asp:ListItem Value="Analysis" Text="Analysis" />
              </asp:RadioButtonList>
            </div>
          </div>

          <div class="divCriteria divViewByCol2">
            <CW:DropDowns ID="dropDowns" runat="server" validationEnabled="true" StartViewMode="Building" ViewMode="Analysis" OnBuildingChanged="dropDowns_BuildingChanged" OnEquipmentClassChanged="dropDowns_EquipmentClassChanged" OnEquipmentChanged="dropDowns_EquipmentChanged" ValidationGroup="Tasks" />
          </div>

          <div class="divCriteria">
            <div>
              <h3>Task Info</h3>
            </div>
            <div class="divFormWidest">
              <label>*Issue Start Date:</label>
              <br />

              <telerik:RadDatePicker ID="txtAnalysisStartDate" runat="server" />
              <br />

              <asp:RequiredFieldValidator ID="analysisStartDateRFV" runat="server" CssClass="errorMessage"  ErrorMessage="Issue Start Date is a required field." ControlToValidate="txtAnalysisStartDate" SetFocusOnError="true" Display="None" ValidationGroup="Tasks" />
              <ajaxToolkit:ValidatorCalloutExtender ID="analysisStartDateVCE" runat="server" BehaviorID="analysisDateVCE" TargetControlID="analysisStartDateRFV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="Right" Width="175" />
              <br />

              <label>*Issue End Date:</label>
              <br />

              <telerik:RadDatePicker ID="txtAnalysisEndDate" runat="server" />
              <br />

              <asp:RequiredFieldValidator ID="analysisStartEndDateRFV" runat="server" CssClass="errorMessage"  ErrorMessage="Issue End Date is a required field." ControlToValidate="txtAnalysisEndDate" SetFocusOnError="true" Display="None" ValidationGroup="Tasks" />
              <ajaxToolkit:ValidatorCalloutExtender ID="analysisStartEndDateVCE" runat="server" BehaviorID="analysisStartEndDateVCE" TargetControlID="analysisStartEndDateRFV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="Right" Width="175" />
            </div>
          </div>

          <div class="divCriteria">
            <div><h3>&nbsp;</h3></div>
            <div class="divFormWidest">
              <div>   
                <label id="lblStatus" runat="server">Status:</label>
                <br />

                <asp:ListBox ID="lbStatus" AppendDataBoundItems="false" CssClass="taskInfoStatusBox" runat="server" SelectionMode="Multiple" />
                <asp:RequiredFieldValidator ID="statusRFV" runat="server" ErrorMessage="Status is a required field." ControlToValidate="lbStatus" SetFocusOnError="true" Display="None" InitialValue="" ValidationGroup="Tasks" />
                <ajaxToolkit:ValidatorCalloutExtender ID="statusVCE" runat="server" BehaviorID="statusVCE" TargetControlID="statusRFV" HighlightCssClass="validatorCalloutHighlight" Width="175" />
              </div>
            </div>
          </div>

         <div class="divCriteria">
            <div><h3>&nbsp;</h3></div>
            <div class="divFormWidest">
              <label id="lblAssignee" runat="server">Assignee:</label>
              <br />

              <asp:DropDownList ID="ddlAssignee" CssClass="dropdownNarrower" AppendDataBoundItems="true" runat="server" />
            </div>
          </div>

          <div class="divCriteria">
            <div><h3>&nbsp;</h3></div>
            <div class="divFormWidest">
              <label id="lblSearchFilter" runat="server">Search Filter:</label>
              <br />

              <asp:TextBox ID="txtTaskFilter" CssClass="textboxNarrower" runat="server" />
            </div>
          </div>

        </td>
      </tr>
        
      <tr>
        <td>
          <div style="width: 100%">
            <div>
              <label class="labelLeft">Show details:</label>
              <asp:CheckBox ID="chkShowDetails" runat="server" CssClass="checkbox" Checked="true" />
            </div>
          </div>
        </td>
      </tr>
    </table>

  </asp:Panel>
</fieldset>

<div class="divGenerateButton">
  <asp:LinkButton ID="btnGenerateData" runat="server" CssClass="lnkButton" Text="Generate Data" OnClick="generateData_Click" ValidationGroup="Tasks" />
</div>