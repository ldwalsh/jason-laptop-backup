using CW.Business;
using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CW.Website._controls.tasks
{
    public sealed class VDataMetrics
    {
        #region fields

            DataManager _dataMgr;

            ModelTaskRecord _taskRecord;

        #endregion

        #region properties

            bool HasCostSavings { get; set; }

            VData LowestError { get; set; }

        #endregion

        #region constructor(s)

            public VDataMetrics(ModelTaskRecord taskRecord)
            {
                _dataMgr = taskRecord.DataMgr;

                _taskRecord = taskRecord;
            }

        #endregion

        #region methods

            public void SetMetricsForTaskRecord(string[] metrics)
            {
                if (metrics == null) return;

                if (!string.IsNullOrWhiteSpace(metrics[0])) _taskRecord.AnnualAvoidableCost = metrics[0];

                if (!string.IsNullOrWhiteSpace(metrics[1])) _taskRecord.AnnualAvoidableHeatingUse = metrics[1];

                if (!string.IsNullOrWhiteSpace(metrics[2])) _taskRecord.AnnualAvoidableCoolingUse = metrics[2];

                if (!string.IsNullOrWhiteSpace(metrics[3])) _taskRecord.AnnualAvoidableElectricUse = metrics[3];
            }

            public string[] ObtainMetricsAndCalculateSavings()
            {
                //need to obtain the vpoints by the four metrics necessary for the task, AnnualAvoidableCost, AnnualAvoidableCoolingUse, AnnualAvoidableHeatingUse, 
                //and AnnualAvoidableElectricUse
                var vpoints = _dataMgr.VAndVPreDataMapper.GetVPoints(_taskRecord.Cid, _taskRecord.Bid, _taskRecord.Eid, _taskRecord.Aid).Where
                (
                    vp => vp.PointTypeID == BusinessConstants.PointType.AnnualAvoidableCostPointTypeID ||
                          vp.PointTypeID == BusinessConstants.PointType.AnnualAvoidableCostErrorPointTypeID ||
                          vp.PointTypeID == BusinessConstants.PointType.AnnualHeatingUsePointTypeID ||
                          vp.PointTypeID == BusinessConstants.PointType.AnnualCoolingUsePointTypeID ||
                          vp.PointTypeID == BusinessConstants.PointType.AnnualElectricUsePointTypeID
                );

                if (!vpoints.Any()) return null;

                var date = _taskRecord.AnalysisStartDate.ToString();

                var vData = GetVDataByAnalysisRange(vpoints, date, DataConstants.AnalysisRange.Monthly); //obtain vdata from the past past 12 monthlies

                var vPointCost = GetVPoint(vpoints, BusinessConstants.PointType.AnnualAvoidableCostPointTypeID); //get the avoidable costs if it exists

                var vPointCostError = GetVPoint(vpoints, BusinessConstants.PointType.AnnualAvoidableCostErrorPointTypeID); //get the avoidable costs errors if it exists

                var vPointHeatingUse = GetVPoint(vpoints, BusinessConstants.PointType.AnnualHeatingUsePointTypeID); //get the avoidable costs errors if it exists

                var vPointCoolingUse = GetVPoint(vpoints, BusinessConstants.PointType.AnnualCoolingUsePointTypeID); //get the avoidable costs errors if it exists

                var vPointElectricUse = GetVPoint(vpoints, BusinessConstants.PointType.AnnualElectricUsePointTypeID); //get the avoidable costs errors if it exists

                var costData = FilterCost(vData, vPointCost); //filter down the above vdata, if there is no vdata, value is simply an empty enumerable

                if (!costData.Any()) //there were no avoidable costs in the past 12 monthlies, try the past 12 weeklies next
                {
                    vData = GetVDataByAnalysisRange(vpoints, date, DataConstants.AnalysisRange.Weekly); //obtain vdata from the past past 12 weeklies

                    costData = FilterCost(vData, vPointCost); //filter down the above vdata, if there is no vdata, value is simply an empty enumerable
                }

                var costSavings = GetCostSavings(vData, vPointCostError, costData, vPointCost);

                var metricsAndSavings = new string[]
                {
                        //get the data for each point type from the existing set and put into an array (probably most efficient data structure for this data)
                        HasCostSavings ? Math.Round(costSavings, 2).ToString() : string.Empty,
                        GetMetric(vData, vPointHeatingUse),
                        GetMetric(vData, vPointCoolingUse),
                        GetMetric(vData, vPointElectricUse)
                };

                return metricsAndSavings;
            }

            IEnumerable<VData> GetVDataByAnalysisRange(IEnumerable<VPoint> vpoints, string date, DataConstants.AnalysisRange analysisRange)
            {
                //to ensure we pass the "invariant" culture date to the mapper call, perform the following steps
                //1) Set the thread to invariant, so when the date is parsed, it will return an invariant result
                //2) Parse the actual string value for the date in the users culture so that it matches the value when attempting to parse 
                //3) set the thread back to the users culture
                var userCulture = _taskRecord.CultureName;

                CultureHelper.SetThreadInvariant();

                var analysisStartDate = DateTime.Parse(date, new CultureInfo(userCulture));

                CultureHelper.SetThreadCulture(userCulture);

                return _dataMgr.VAndVPreDataMapper.GetVDataByDateRange
                (
                    vpoints.Select(vp => vp.VPID),
                    DateTimeHelper.GetXMonthsAgo(DateTimeHelper.GetFirstOfMonth(analysisStartDate), 12),
                    DateTimeHelper.GetFirstOfMonth(analysisStartDate),
                    analysisRange
                );
            }

            IEnumerable<VData> FilterCost(IEnumerable<VData> vData, VPoint vPointCost)
            {
                if (!vData.Any() || vPointCost == null) return Enumerable.Empty<VData>();

                return vData.Where(vd => vd.VPID == vPointCost.VPID && GetInvariantValue(vd.RawValue) > 0);
            }

            double GetCostSavings(IEnumerable<VData> vData, VPoint vPointCostError, IEnumerable<VData> costData, VPoint vPointCost)
            {
                if (!vData.Any() || vPointCostError == null || !costData.Any() || vPointCost == null) return 0;

                LowestError = vData.Where(vd => vd.VPID == vPointCostError.VPID && costData.Select(cd => cd.StartDate).Contains(vd.StartDate)).OrderBy(v => GetInvariantValue(v.RawValue)).FirstOrDefault();

                if (LowestError == null) return 0;

                if (!vData.Where(v => v.VPID == vPointCost.VPID && v.StartDate == LowestError.StartDate).Any()) return 0;

                HasCostSavings = true;

                return GetInvariantValue(vData.Where(v => v.VPID == vPointCost.VPID && v.StartDate == LowestError.StartDate).First().RawValue);
            }

            VPoint GetVPoint(IEnumerable<VPoint> vpoints, int id) => vpoints.Where(vp => vp.PointTypeID == id).FirstOrDefault();

            string GetMetric(IEnumerable<VData> vData, VPoint vpoint)
            {
                return (!vData.Any() || vpoint == null || LowestError == null) ? string.Empty : vData.Where(vd => vd.VPID == vpoint.VPID && vd.StartDate == LowestError.StartDate).First().RawValue;
            }

            //convenience method to convert value to its "invariant" representation
            double GetInvariantValue(object val) => Convert.ToDouble(val, CultureInfo.InvariantCulture);

        #endregion
    }
}