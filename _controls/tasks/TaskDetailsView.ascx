﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TaskDetailsView.ascx.cs" Inherits="CW.Website._controls.tasks.TaskDetailsView" %>
<%@ Register TagPrefix="CW" Namespace="CW.Website._controls" Assembly="CW.Website" %>

<div>
  <h2>Task Details</h2>                                                                                                                                            
  <ul class="detailsList">
    <CW:UListItem ID="ulSummary" runat="server" Label="Summary" />
    <CW:UListItem ID="ulBuildingName" runat="server" Label="Building Name" />
    <CW:UListItem ID="ulEquipmentName" runat="server" Label="Equipment Name" />
    <CW:UListItem ID="ulAnalysisName" runat="server" Label="Analysis Name" />
    <CW:UListItem ID="ulAnalysisDate" runat="server" Label="Issue Date" />
    <CW:UListItem ID="ulInterval" runat="server" Label="Interval" />
    <CW:UListItem ID="ulReporter" runat="server" Label="Reporter" />
    <CW:UListItem ID="ulAssigned" runat="server" Label="Assigned" />
    <CW:UListItem ID="ulStatus" runat="server" Label="Status" />
    <CW:UListItem ID="ulDateCompleted" runat="server" Label="Date Completed" Visible="false" />
    <a id="lnkSetFocus" runat="server"></a>
    <CW:UListItem ID="ulDescription" runat="server" Label="Description" Visible="false" />
    <CW:UListItem ID="ulRecommandations" runat="server" Label="Recommendations" Visible="false" />
    <CW:UListItem ID="ulActions" runat="server" Label="Actions" Visible="false" />
    <CW:UListItem ID="ulWorkOrderType" runat="server" Label="Work Type" />
    <CW:UListItem ID="ulWorkOrderNum" runat="server" Label="Work Order Number" Visible="false" />
    <CW:UListItem ID="ulAnnualAvoidableCost" runat="server" Label="Annual Avoidable Cost" Visible="false" />
    <CW:UListItem ID="ulAnnualAvoidableCoolingUse" runat="server" Label="Annual Avoidable Cooling Use" Visible="false" />
    <CW:UListItem ID="ulAnnualAvoidableElectricUse" runat="server" Label="Annual Avoidable Electric Use" Visible="false" />
    <CW:UListItem ID="ulAnnualAvoidableHeatingUse" runat="server" Label="Annual Avoidable Heating Use" Visible="false" />
    <CW:UListItem ID="ulDateCreated" runat="server" Label="Date Created" />
    <CW:UListItem ID="ulLastModified" runat="server" Label="Last Modified" />
  </ul>
</div>