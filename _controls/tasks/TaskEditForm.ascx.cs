﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Website._controls.tasks.Factories;
using CW.Website._controls.tasks.Interfaces;
using CW.Website._controls.tasks.Models;
using System;
using System.Web.UI.WebControls;

namespace CW.Website._controls.tasks
{
    public partial class TaskEditForm : TaskForm
    {
        #region fields

            IEditTaskRecordService _service;

            Label[] _metricLabels;

            TextBox[] _metricFields;

        #endregion

        #region properties

            public ViewTasks ViewTasks { private get; set; }

        #endregion

        #region events

            void Page_Init()
            {
                _service = new TaskRecordServiceFactory(LogMgr).GetEditTaskRecordService();

                _metricLabels = new Label[] { lblCurrencySymbol, lblCoolingEngUnit, lblElectricEngUnit, lblHeatingEngUnit };

                _metricFields = new TextBox[] { txtCost, txtCooling, txtElectric, txtHeating };
            }

            #region button events

                protected void updateTask_Click(object sender, EventArgs e)
                {
                    ClearMessageLabels();

                    var modelTaskRecord = GetModelTaskRecord();

                    _service.RunEditTaskRecordService(modelTaskRecord);

                    UpdateLabels(lblTaskUpdated, modelTaskRecord, CMMSTaskFields);

                    UpdateUI(modelTaskRecord);

                    ViewTasks.BindGrid(); //Need to rebind the grid with updated task data

                    //we need to set destroyed form attributes again, since we're not closing the edit form on submit anymore.
                    SetFormAttributes();

                    SetLowerFocus();

                    UpdatePersistingFields();
                }

            #endregion

        #endregion

        #region methods

            public void SetFormAttributes()
            {
                ValidationGroup = "EditTask";

                LinkButton = btnUpdateTask;

                SetFormFieldsAttributes();

                SetValidatorAttributes();
            }

            public void InitializeForm(TaskRecord taskRecord)
            {
                ClearMessageLabels();

                ClearFormFields(); //clear viewstate fields if another record was "initialized"

                var modelTaskRecord = _service.SetModelTaskRecord(taskRecord);

                var taskDropDowns = new TaskDropDowns();

                ToggleAssignedFields(modelTaskRecord, taskDropDowns);

                SetStatusField(modelTaskRecord, taskDropDowns);

                SetDateCompletedFields(modelTaskRecord);

                SetEditableFields(modelTaskRecord);

                //need explicitly reset due to viewstate
                chkRecalculateMetrics.Checked = false; 
                chkEmail.Checked = true;

                modelTaskRecord.CultureName = siteUser.CultureName;

                _service.SetMetricFieldsToUI(modelTaskRecord, _metricLabels, _metricFields);

                var formFields = SetPersistingFields(modelTaskRecord);

                CMMSTaskFields.InitializeForm(formFields);

                SetLabelFields(modelTaskRecord); //set the "display" fields last
            }

            void ToggleAssignedFields(ModelTaskRecord modelTaskRecord, TaskDropDowns taskDropDowns)
            {
                taskDropDowns.TaskBid = modelTaskRecord.Bid;

                taskDropDowns.BindAssigneeDDL(ddlAssigned, false, true, true);

                SetDDLVisible(true);

                var assignedUid = modelTaskRecord.AssignedUid;

                if (assignedUid == null) return;
                
                if (ddlAssigned.Items.FindByValue(assignedUid.ToString()) != null)
                {
                    ddlAssigned.SelectedValue = assignedUid.ToString();
                
                    return;
                }

                lblAssigned.Text = modelTaskRecord.User1.Email;

                SetDDLVisible(false);
            }

            void SetStatusField(ModelTaskRecord modelTaskRecord, TaskDropDowns taskDropDowns)
            {
                taskDropDowns.BindStatusDDL(ddlStatus);

                ddlStatus.SelectedValue = modelTaskRecord.StatusId.ToString();
            }

            void SetDateCompletedFields(ModelTaskRecord modelTaskRecord)
            {
                txtDateCompleted.SelectedDate = null; //reset if user chose another record, viewstate fun :)

                litDateDetails.Text = string.Empty;

                var dateCompleted = modelTaskRecord.DateCompleted;

                if (dateCompleted.HasValue)
                {
                    var timeZone = modelTaskRecord.TimeZone;

                    var timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone);

                    litDateDetails.Text = $"(UTC Offset: {timeZoneInfo.BaseUtcOffset.Hours}, Time Zone: {timeZone})";

                    txtDateCompleted.SelectedDate = TimeZoneInfo.ConvertTimeFromUtc(dateCompleted.Value, timeZoneInfo);
                }
            }

            void SetEditableFields(ModelTaskRecord modelTaskRecord)
            {
                txtSummary.Text = modelTaskRecord.Summary;

                txtDescription.InnerText = modelTaskRecord.Description;

                txtRecommendations.InnerText = modelTaskRecord.Recommendations;

                txtActions.InnerText = modelTaskRecord.Actions;

                CMMSTaskFields.WorkOrderTypeId = modelTaskRecord.WorkOrderTypeId;
            }

            ViewModelTaskForm SetPersistingFields(ModelTaskRecord modelTaskRecord)
            {
                var formFields = new ViewModelTaskForm()
                {
                    Id = modelTaskRecord.TaskId,

                    ClientTaskId = modelTaskRecord.ClientTaskId,

                    Bid = modelTaskRecord.Bid,

                    Eid = modelTaskRecord.Eid,

                    Aid = modelTaskRecord.Aid,

                    AnalysisStartDate = modelTaskRecord.AnalysisStartDate,

                    AnalysisRange = modelTaskRecord.Interval,

                    ReporterId = modelTaskRecord.ReporterUid,

                    AssignedId = modelTaskRecord.AssignedUid,

                    StatusId = modelTaskRecord.StatusId,

                    TimeZone = modelTaskRecord.TimeZone,

                    SecondaryEmail = modelTaskRecord.SecondaryEmail,

                    CMMSReferenceId = modelTaskRecord.Equipment.CMMSReferenceID,

                    WorkOrderId = modelTaskRecord.WorkOrderId
                };

                PersistFormFields(formFields);

                return formFields;
            }

            void SetLabelFields(ModelTaskRecord modelTaskRecord)
            {
                lblBuildingName.Text = modelTaskRecord.Building.BuildingName;

                lblEquipmentName.Text = modelTaskRecord.Equipment.EquipmentName;

                lblAnalysisName.Text = modelTaskRecord.Analyse.AnalysisName;

                lblAnalysisStartDate.Text = modelTaskRecord.AnalysisStartDate.ToShortDateString();

                lblInterval.Text = modelTaskRecord.Interval;

                lblReporter.Text = modelTaskRecord.ReporterEmail;

                lblDateCreated.Text = _service.GetFormattedString(modelTaskRecord, ModelTaskRecord.DateType.CreatedOrCompleted);

                lblLastModified.Text = _service.GetFormattedString(modelTaskRecord, ModelTaskRecord.DateType.Modified);
            }

            ModelTaskRecord GetModelTaskRecord()
            {
                var taskRecord = new ModelTaskRecord();

                var currentDate = DateTime.UtcNow;

                var description = txtDescription.InnerText.Trim();

                var recommendations = txtRecommendations.InnerText.Trim();

                var actions = txtActions.InnerText.Trim();

                var persistedFormFields = (ViewModelTaskForm)ViewState[viewStateName];

                var assignedId = persistedFormFields.AssignedId;

                taskRecord.TaskId = persistedFormFields.Id;

                taskRecord.ClientTaskId = persistedFormFields.ClientTaskId;

                taskRecord.Cid = siteUser.CID;

                taskRecord.Bid = persistedFormFields.Bid;

                taskRecord.Eid = persistedFormFields.Eid;

                taskRecord.Aid = persistedFormFields.Aid;

                taskRecord.AnalysisStartDate = persistedFormFields.AnalysisStartDate;
               
                taskRecord.Interval = persistedFormFields.AnalysisRange;

                taskRecord.Summary = txtSummary.Text.Trim();

                taskRecord.ReporterUid = persistedFormFields.ReporterId;

                taskRecord.AssignedUid = (ddlAssigned.Visible) ? int.Parse(ddlAssigned.SelectedValue) : assignedId;

                taskRecord.StatusId = int.Parse(ddlStatus.SelectedValue);

                taskRecord.Description = (!string.IsNullOrWhiteSpace(description)) ? description : null;

                taskRecord.Recommendations = (!string.IsNullOrWhiteSpace(recommendations)) ? recommendations : null;

                taskRecord.Actions = (!string.IsNullOrWhiteSpace(actions)) ? actions : null;

                taskRecord.RecalculateMetrics = chkRecalculateMetrics.Checked;

                taskRecord.SendEmail = chkEmail.Checked;

                taskRecord.IsTaskCompleted = (int.Parse(ddlStatus.SelectedValue) == Convert.ToInt16(BusinessConstants.Status.Statuses.Completed));

                taskRecord.DataMgr = DataMgr;

                taskRecord.CultureName = siteUser.CultureName;

                taskRecord.IsSchneiderTheme = siteUser.IsSchneiderTheme;

                //this will either take the user inputs or recalculate them instead based on the check box value being checked or not
                _service.SetMetricFieldsToTaskRecord(taskRecord, _metricFields);

                taskRecord.WorkOrderTypeId = CMMSTaskFields.WorkOrderTypeId;

                taskRecord.TimeZone = persistedFormFields.TimeZone;

                taskRecord.Email = siteUser.Email;

                taskRecord.FullName = siteUser.FullName;

                taskRecord.SecondaryEmail = persistedFormFields.SecondaryEmail;

                taskRecord.EmailStatus = (taskRecord.IsTaskCompleted) ? "Completed" : "Updated";

                taskRecord.TaskMessage = "Task Updated";

                //set the right before the record is processed, to contain the most accurate date/time
                taskRecord.LastModifiedUid = siteUser.UID;

                taskRecord.DateModified = currentDate;

                if (taskRecord.IsTaskCompleted)
                {
                    taskRecord.DateCompleted = !txtDateCompleted.SelectedDate.HasValue ? currentDate : DateTimeHelper.CreateUTCDateTime(txtDateCompleted.SelectedDate.Value);
                }
                else
                {
                    taskRecord.DateCompleted = null;
                }   

                taskRecord.CreateWorkOrder = CMMSTaskFields.CreateWorkOrder;

                taskRecord.WorkOrderCategory = CMMSTaskFields.WorkOrderCategory;

                return taskRecord;
            }

            public void SetLowerFocus() => lnkSetFocusEdit.Focus();

            void SetDDLVisible(bool isDDLVisible)
            {
                var assignedHeaderTxt = "Assigned:";

                lblAssignedHeader.Text = (isDDLVisible) ? $"*{assignedHeaderTxt}" : assignedHeaderTxt;

                ddlAssigned.Visible = ddlAssigned_Validator.Enabled = isDDLVisible;

                lblAssigned.Visible = !isDDLVisible;
            }

            void UpdateUI(ModelTaskRecord modelTaskRecord)
            {
                if (modelTaskRecord.RecalculateMetrics) _service.SetMetricFieldsToUI(modelTaskRecord, _metricLabels, _metricFields);

                lblLastModified.Text = _service.GetFormattedString(modelTaskRecord, ModelTaskRecord.DateType.Modified);

                txtDateCompleted.SelectedDate = (modelTaskRecord.DateCompleted.HasValue) ? modelTaskRecord.DateCompleted : null;
            }

            void UpdatePersistingFields()
            {
                var persistedFormFields = (ViewModelTaskForm)ViewState[viewStateName];

                if (ddlAssigned.Visible) persistedFormFields.AssignedId = int.Parse(ddlAssigned.SelectedValue);

                persistedFormFields.StatusId = int.Parse(ddlStatus.SelectedValue);
            }

        #endregion
    }
}