﻿using System;

namespace CW.Website._controls.tasks.Models
{
    [Serializable]
    public class ViewModelTaskForm
    {
        #region properties

            public int Id { get; set; }

            public int ClientTaskId { get; set; }

            public int Cid { get; set; }

            public int Aeid { get; set; }

            public DateTime AnalysisStartDate { get; set; }

            public string AnalysisRange { get; set; }

            public int Bid { get; set; }

            public string BuildingName { get; set; }

            public int Eid { get; set; }

            public string EquipmentName { get; set; }

            public int Aid { get; set; }

            public string AnalysisName { get; set; }

            public string NotesSummary { get; set; }

            public string CMMSReferenceId { get; set; }

            public string WorkOrderId { get; set; }

            public int? ReporterId { get; set; }

            public int? AssignedId { get; set; }

            public int StatusId { get; set; }

            public string TimeZone { get; set; }

            public string SecondaryEmail { get; set; }

            public string FormType { get; set; }

        #endregion
    }
}