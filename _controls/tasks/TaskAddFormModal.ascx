﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TaskAddFormModal.ascx.cs" Inherits="CW.Website._controls.tasks.TaskAddFormModal" %>
<%@ Register src="CMMSTaskFields.ascx" tagPrefix="CW" tagName="CMMSTaskFields" %>

<ajaxToolkit:ModalPopupExtender ID="createTaskMPE" runat="server" TargetControlID="lnkCreateTaskHidden" PopupControlID="pnlCreateTask" BackgroundCssClass="modalBackgroundLight" CancelControlID="lnkCreateTaskHiddenCancel" Y="42" X="471" BehaviorID="createTaskMPEBehavior" />

<asp:Panel ID="pnlCreateTask" ClientIDMode="Inherit" CssClass="modalPanelLightWide" runat="server" DefaultButton="btnCreateTask">

  <a href="javascript:void(0)" onclick="return HideDiagnosticsModals();" class="modalLinkLight" id="lnkTasksModalCancel">Cancel</a>
  <input id="btnCreateTaskModalCancel" type="button" class="modalButtonSmall" onclick="return HideDiagnosticsModals();" title="X" />  
  <asp:HiddenField ID="hdnDiagnosticsQuickLink" runat="server" />
  <asp:LinkButton ID="lnkCreateTaskHidden" CausesValidation="false" runat="server" />
  <asp:LinkButton ID="lnkCreateTaskHiddenCancel" CausesValidation="false" runat="server" />

  <div id="lblCreatTaskTitle" class="modalTitleLight">Create Task</div>
  <asp:Label ID="lblTaskAdded" runat="server" />
  <asp:Label ID="lblTaskError" runat="server" />

  <asp:Label ID="lblWorkOrderAdded" runat="server" />
  <asp:Label ID="lblWorkOrderError" runat="server" />

  <asp:Label ID="lblEmailSent" runat="server" />
  <asp:Label ID="lblEmailError" runat="server" />
  
  <br />
  <br />

  <div>

    <div class="divFormWide">
      <label class="labelNarrow">Building and Equipment:</label>
      <asp:Label ID="lblBuildingAndEquipment" runat="server" CssClass="labelContent" />
    </div>
    <div class="divFormWide">
      <label class="labelNarrow">Analysis and Date:</label>
      <asp:Label ID="lblAnalysisAndDate" runat="server" CssClass="labelContent" />
    </div>
    <div class="divFormWide">
      <label class="labelNarrow">*Summary:</label>
      <asp:TextBox ID="txtSummary" runat="server" MaxLength="250" />
      <asp:RequiredFieldValidator ID="txtSummary_Validator" runat="server" />
      <ajaxToolkit:ValidatorCalloutExtender ID="txtSummary_ValidatorExtender" runat="server" />
    </div>
    <div class="divFormWide">
      <label class="labelNarrow">*Assignee:</label>
      <asp:DropDownList ID="ddlAssignee" runat="server" />
      <asp:RequiredFieldValidator ID="ddlAssignee_Validator" runat="server" />
      <ajaxToolkit:ValidatorCalloutExtender ID="ddlAssignee_ValidatorExtender" runat="server" />
    </div>
    <div id="createNewUserLinkDiv" runat="server" class="divFormWide" visible="false">
      <asp:HyperLink ID="hlCreateNewUser" runat="server" NavigateUrl="~/UserAdministration.aspx" Text="Create New User" Target="_blank" />
    </div>
    <div class="divFormWide">
      <label class="labelNarrow">Describe the issue (Description):</label>
      <textarea id="txtDescription" onkeyup="limitChars(this, 5000, 'divDescriptionCharInfo')" runat="server" />
      <div id="divDescriptionCharInfo"></div>
    </div>
    <div class="divFormWide">
      <label class="labelNarrow">What should be done (Recommendations):</label>
      <textarea id="txtRecommendations" onkeyup="limitChars(this, 5000, 'divRecommendationsCharInfo')" runat="server" />
      <div id="divRecommendationsCharInfo"></div>
    </div>
    <div class="divFormWide">
      <label class="labelNarrow">What has been done (Actions):</label>
      <textarea id="txtActions" onkeyup="limitChars(this, 5000, 'divActionsCharInfo')" runat="server" />
      <div id="divActionsCharInfo"></div>
    </div>
    <div class="divFormWide">
      <label class="labelNarrow">*Status:</label>
      <asp:DropDownList ID="ddlStatus" runat="server" />
      <asp:RequiredFieldValidator ID="ddlStatus_Validator" runat="server" />
      <ajaxToolkit:ValidatorCalloutExtender ID="ddlStatus_ValidatorExtender" runat="server" />
    </div>

    <CW:CMMSTaskFields ID="CMMSTaskFields" runat="server" DivClass="divFormWide" LabelClass="labelNarrow" />

    <div class="divFormWide">
      <label class="labelNarrow">Email?:</label>
      <asp:CheckBox ID="chkEmail" runat="server" Checked="true" />
    </div>

  </div>

  <div class="divFormModal">
    <asp:LinkButton ID="btnCreateTask" runat="server" UseSubmitBehavior="false" CssClass="modalButton" Text="Create Task" OnClick="createTask_Click" />
  </div>

</asp:Panel>