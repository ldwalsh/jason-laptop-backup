﻿using CW.CMMSIntegration;
using CW.CMMSIntegration.WorkOrderSystems.Factories;
using CW.CMMSIntegration.WorkOrderSystems.Interfaces;
using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._controls.tasks.Models;
using CW.Website._framework;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls.tasks
{
    public partial class CMMSTaskFields : SiteUserControl
    {
        #region field(s)

            IWorkOrderUIService _workOrderUIService;

            string _divClass = "divForm";

            string _labelClass = "label";

        #endregion

        #region properties

            public string DivClass { private get { return _divClass; } set { _divClass = value; } }

            public string LabelClass { private get { return _labelClass; } set { _labelClass = value; } }

            public HtmlGenericControl DivGenerateWO => divGenerateWO;

            public int WorkOrderTypeId
            {
                get { return (divGenerateWO.Visible) ? int.Parse(ddlWorkOrderType.SelectedValue) : 1; }
                set { ddlWorkOrderType.SelectedValue = value.ToString(); }
            }

            public string WorkOrderCategory => ddlWorkOrderCategory.SelectedValue;

            public bool CreateWorkOrder => (divGenerateWO.Visible && chkGenerateWO.Checked);

        #endregion

        #region methods

            public void InitializeForm(ViewModelTaskForm formFields)
            {
                chkGenerateWO.Checked = true;

                divWOType.Visible = divWOCategory.Visible = false;

                _workOrderUIService = new WorkOrderUIServiceFactory(DataMgr.ConnectionString, LogMgr).GetWorkOrderUIService();

                ToggleGenerateCMMSWorkOrder(formFields);

                //only invoke the following methods if the work order fields are actually visible
                if (divGenerateWO.Visible)
                {
                    SetCssClasses();

                    SetWorkOrderTypes(formFields);

                    SetWorkOrderCategories();

                    divWOType.Visible = divWOCategory.Visible = true;
                }
            }

            void ToggleGenerateCMMSWorkOrder(ViewModelTaskForm formFields)
            {
                var settings = _workOrderUIService.GetSettings(ConfigTypeEnum.Building, formFields.Bid);

                if (settings == null) return;

                var isEnabled = settings.IsEnabled;

                //need to determine that if these are building level settings and they are disabled, call GetConfig again at the client level
                if (settings.ConfigType == ConfigTypeEnum.Building && !isEnabled)
                {
                    var clientSettings = _workOrderUIService.GetSettings(ConfigTypeEnum.Client, siteUser.CID);

                    if (clientSettings == null) return; //if there are no settings (client level now), no need to proceed further

                    isEnabled = clientSettings.IsEnabled;
                }

                divGenerateWO.Visible = (isEnabled && !string.IsNullOrWhiteSpace(formFields.CMMSReferenceId) && string.IsNullOrWhiteSpace(formFields.WorkOrderId));
            }

            void SetCssClasses()
            {
                var controls = FindControls<HtmlGenericControl>();

                controls.Where(_ => _.ID.Contains("div")).ForEach(_ => _.AddAttrib("class", DivClass));

                controls.Where(_ => _.ID.Contains("lbl")).ForEach(_ => _.AddAttrib("class", LabelClass));
            }

            void SetWorkOrderTypes(ViewModelTaskForm formFields)
            {
                var settings = _workOrderUIService.GetSettings(ConfigTypeEnum.Building, formFields.Bid);

                var cmWoTypes = (settings == null) ? _workOrderUIService.GetWoTypes() : _workOrderUIService.GetWoTypeMappings(settings);

                var woTypes = cmWoTypes.Select(_ => new { Id = _.Id, Type = (_.Type != null) ? _.Type : _.DefaultType });

                BindDDL(ddlWorkOrderType, nameof(WoTypeMap.Id), nameof(WoTypeMap.Type), woTypes.OrderBy(_ => _.Type));

                if (formFields.FormType == "Add") ddlWorkOrderType.Items.Cast<ListItem>().Single(i => i.Value == "1").Selected = true;
            }

            void SetWorkOrderCategories()
            {
                var categories = new List<Category>();
    
                categories.Add(new Category { Value = "CORRECTIVE", Display = "Corrective" });

                categories.Add(new Category { Value = "PREDICTIVE", Display = "Predictive" });

                categories.Add(new Category { Value = "PREVENTATIVE", Display = "Preventive" });

                BindDDL(ddlWorkOrderCategory, nameof(Category.Value), nameof(Category.Display), categories);
            }

            void BindDDL(DropDownList ddl, string valueField, string textField, IEnumerable<object> data)
            {
                //One of the many joys of web forms, you have to create a default item before data bind, 
                //and in this case remove it after bind, as it's not necessary :)
                ddl.Items.Clear();

                ddl.Items.Add(new ListItem { Text = "Select One...", Value = "-1" });

                ddl.DataValueField = valueField;

                ddl.DataTextField = textField;

                ddl.DataSource = data;

                ddl.DataBind();

                ddl.Items.RemoveAt(0);
            }

        #endregion

        #region class(es)

            public sealed class Category
            {
                public string Value { get; set; }

                public string Display { get; set; }
            }

        #endregion
    }
}