﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="TaskEditForm.ascx.cs" Inherits="CW.Website._controls.tasks.TaskEditForm" %>
<%@ Register TagPrefix="telerik" Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" %>
<%@ Register src="CMMSTaskFields.ascx" tagPrefix="CW" tagName="CMMSTaskFields" %>

<div>
  <h2>Edit Task</h2>
</div>
<div>  
  <asp:Label ID="lblTaskUpdated" runat="server" />
  <asp:Label ID="lblTaskError" runat="server" />

  <asp:Label ID="lblWorkOrderAdded" runat="server" />
  <asp:Label ID="lblWorkOrderError" runat="server" />

  <asp:Label ID="lblEmailSent" runat="server" />
  <asp:Label ID="lblEmailError" runat="server" />
</div>
<div class="divForm">
  <label class="label">*Summary:</label>
  <asp:TextBox ID="txtSummary" runat="server" MaxLength="250" />
  <asp:RequiredFieldValidator ID="txtSummary_Validator" runat="server" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtSummary_ValidatorExtender" runat="server" />
</div>
<div class="divForm">
  <label class="label">Building Name:</label>
  <asp:Label ID="lblBuildingName" runat="server" />
</div>
<div class="divForm">
  <label class="label">Equipment Name:</label>
  <asp:Label ID="lblEquipmentName" runat="server" />
</div>
<div class="divForm">
  <label class="label">Analysis Name:</label>
  <asp:Label ID="lblAnalysisName" runat="server" />
</div>
<div class="divForm">
  <label class="label">Issue Date:</label>
  <asp:Label ID="lblAnalysisStartDate" runat="server" />
</div>
<div class="divForm">
  <label class="label">Interval:</label>
  <asp:Label ID="lblInterval" runat="server" />
</div>
<div class="divForm">
  <label class="label">Reporter:</label>
  <asp:Label ID="lblReporter" runat="server" />
</div>
<div class="divForm">
  <label class="label"><asp:Label ID="lblAssignedHeader" runat="server" /></label>
  <asp:DropDownList ID="ddlAssigned" runat="server" />
  <asp:Label ID="lblAssigned" runat="server" />
  <asp:RequiredFieldValidator ID="ddlAssigned_Validator" runat="server" />
  <ajaxToolkit:ValidatorCalloutExtender ID="ddlAssigned_ValidatorExtender" runat="server" />
</div>
<div class="divForm">
  <label class="label">*Status:</label>
  <asp:DropDownList ID="ddlStatus" runat="server" />
  <asp:RequiredFieldValidator ID="ddlStatus_Validator" runat="server" />
  <ajaxToolkit:ValidatorCalloutExtender ID="ddlStatus_ValidatorExtender" runat="server" />  
</div>
<div class="divForm">
  <label class="label">Date Completed:</label>
  <telerik:RadDatePicker ID="txtDateCompleted" runat="server" />
  <asp:Literal ID="litDateDetails" runat="server" />
</div>
<div class="divForm">
  <label class="label">Description:</label>
  <textarea id="txtDescription" onkeyup="limitChars(this, 5000, 'divDescriptionCharInfo')" runat="server" />
  <div id="divDescriptionCharInfo"></div>
</div>
<div class="divForm">
  <label class="label">Recommendations:</label>
  <textarea id="txtRecommendations" onkeyup="limitChars(this, 5000, 'divRecommendationsCharInfo')" runat="server" />
  <div id="divRecommendationsCharInfo"></div>
</div>
<div class="divForm">
  <label class="label">Actions:</label>
  <textarea id="txtActions" onkeyup="limitChars(this, 5000, 'divActionsCharInfo')" runat="server" />
  <div id="divActionsCharInfo"></div>
</div>

<div class="divForm">
  <label class="label">Recalculate all annual avoidable costs on update:</label>
  <asp:CheckBox ID="chkRecalculateMetrics" runat="server" />    
</div>

<div class="divForm">
  <label class="label">Annual Avoidable Costs:</label>
  <asp:TextBox ID="txtCost" runat="server" MaxLength="50" />
  <asp:Label ID="lblCurrencySymbol" runat="server" CssClass="label" />
  <asp:CustomValidator ID="txtCost_Validator" runat="server" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtCost_ValidatorExtender" runat="server" />
</div>
<div class="divForm">
  <label class="label">Annual Avoidable Cooling Use:</label>
  <asp:TextBox ID="txtCooling" runat="server" MaxLength="50" />
  <asp:Label ID="lblCoolingEngUnit" runat="server" CssClass="label" />
  <asp:CustomValidator ID="txtCooling_Validator" runat="server" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtCooling_ValidatorExtender" runat="server" />
</div>
<div class="divForm">
  <label class="label">Annual Avoidable Electric Use:</label>
  <asp:TextBox ID="txtElectric" runat="server" MaxLength="50" />
  <asp:Label ID="lblElectricEngUnit" runat="server" CssClass="label" />
  <asp:CustomValidator ID="txtElectric_Validator" runat="server" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtElectric_ValidatorExtender" runat="server" />
</div>
<div class="divForm">
  <label class="label">Annual Avoidable Heating Use:</label>
  <asp:TextBox ID="txtHeating" runat="server" MaxLength="50" />
  <asp:Label ID="lblHeatingEngUnit" runat="server" CssClass="label" />
  <asp:CustomValidator ID="txtHeating_Validator" runat="server" />
  <ajaxToolkit:ValidatorCalloutExtender ID="txtHeating_ValidatorExtender" runat="server" />
</div>


<a id="lnkSetFocusEdit" runat="server"></a>

<CW:CMMSTaskFields ID="CMMSTaskFields" runat="server" />

<div class="divForm">
  <label class="label">Date Created:</label>
  <asp:Label ID="lblDateCreated" runat="server" />
</div>
<div class="divForm">
  <label class="label">Last Modified:</label>
  <asp:Label ID="lblLastModified" runat="server" />
</div>

<div class="divForm">
  <label class="label">Email?:</label>
  <asp:CheckBox ID="chkEmail" runat="server" Checked="true" />
</div>

<asp:LinkButton ID="btnUpdateTask" runat="server" CssClass="lnkButton" Text="Update Task" OnClick="updateTask_Click" />