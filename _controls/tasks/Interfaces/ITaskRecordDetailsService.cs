﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using System;

namespace CW.Website._controls.tasks.Interfaces
{
    public interface ITaskRecordDetailsService
    {
        #region method(s)

            ModelTaskRecord GetModelTaskRecord();

            void SetVDataMetricsIfAny(UListItem[] uls);

            void DisplayOptionalField(UListItem li, string item);
  
            string GetFormattedString(DateTime date, ModelTaskRecord.DateType dateType);

        #endregion
    }
}