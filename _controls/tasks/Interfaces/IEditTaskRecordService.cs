﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Data;
using System.Web.UI.WebControls;

namespace CW.Website._controls.tasks.Interfaces
{
    public interface IEditTaskRecordService
    {
        #region method(s)

            ModelTaskRecord SetModelTaskRecord(TaskRecord taskRecord);

            void SetMetricFieldsToUI(ModelTaskRecord taskRecord, Label[] metricLabels, TextBox[] metricFields);

            void SetMetricFieldsToTaskRecord(ModelTaskRecord modelTaskRecord, TextBox[] metricFields);

            string GetFormattedString(ModelTaskRecord taskRecord, ModelTaskRecord.DateType dateType);

            void RunEditTaskRecordService(ModelTaskRecord modelTaskRecord);

        #endregion
    }
}