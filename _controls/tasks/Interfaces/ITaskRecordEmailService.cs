﻿using CW.CMMSIntegration.WorkOrderSystems.Models;

namespace CW.Website._controls.tasks.Interfaces
{
    public interface ITaskRecordEmailService
    {
        #region method(s)

            void Send(ModelTaskRecord taskRecord);

        #endregion
    }
}