﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Data;
using CW.Data.Models.PointType;
using System.Collections.Generic;

namespace CW.Website._controls.tasks.Interfaces
{
    public interface ITaskRecordRepository
    {
        #region method(s)

            BuildingSetting GetBuildingSettings(ModelTaskRecord modelTaskRecord);

            IEnumerable<GetPointTypeData> GetEngUnits(int[] ids);

        #endregion
    }
}