﻿using CW.CMMSIntegration.WorkOrderSystems.Models;

namespace CW.Website._controls.tasks.Interfaces
{
    public interface ICreateTaskRecordService
    {
        #region method(s)

            void RunCreateTaskRecordService(ModelTaskRecord taskRecord);

        #endregion
    }
}