﻿using CW.CMMSIntegration;
using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Data;
using CW.Data.Models;

namespace CW.Website._controls.tasks.Interfaces
{
    public interface ITaskRecordDataTransformService
    {
        #region properties
        
            void TransformCreateData(ModelTaskRecord taskRecord);

            ModelTaskRecord TransformEditDataForUI(TaskRecord taskRecord);

            void TranformEditData(ModelTaskRecord taskRecord);

            GetTasks TransformDataForEmail(ModelTaskRecord taskRecord);

            WoTask TransformWorkOrderData(ModelTaskRecord taskRecord);

        #endregion
    }
}