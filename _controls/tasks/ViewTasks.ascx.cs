﻿using AjaxControlToolkit;
using CW.Business.Query;
using CW.Data;
using CW.Data.Models;
using CW.Utility;
using CW.Utility.Search.Factories;
using CW.Utility.Web;
using CW.Website._administration;
using CW.Website._framework;
using CW.Website._grids;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._controls.tasks
{
    public partial class ViewTasks : SiteUserControl
    {
        #region const

            const string searchCriteriaCollectionKey = "searchCriteriaCollection";
            const string name = "Task";
            const string successMsg = name + " Deleted";
            const string issueDate = "Issue Date";
            const string multiField = "Description/Recommendations/Actions";
            const string currencyCode = "CurrencyCode";
            const string annualAvoidableCosts = "Annual Avoidable Costs";
        
        #endregion

        #region fields

            int cid;
            string clientTaskId = PropHelper.G<TaskRecord>(_ => _.ClientTaskID);
            string buildingName = PropHelper.G<Building>(_ => _.BuildingName);
            string equipmentName = PropHelper.G<Equipment>(_ => _.EquipmentName);
            string reporterUid = PropHelper.G<TaskRecord>(_ => _.ReporterUID);
            string email = PropHelper.G<User>(_ => _.Email);
            string summary = PropHelper.G<TaskRecord>(_ => _.Summary);
            string description = PropHelper.G<TaskRecord>(_ => _.Description);
            string recommendations = PropHelper.G<TaskRecord>(_ => _.Recommendations);
            string actions = PropHelper.G<TaskRecord>(_ => _.Actions);
            string analysisDate = PropHelper.G<TaskRecord>(_ => _.AnalysisStartDate);
            string annualAvoidableCost = PropHelper.G<TaskRecord>(_ => _.AnnualAvoidableCost);
            string annualAvoidableCostInt = PropHelper.G<GetTasks>(_ => _.AnnualAvoidableCostInt);
            string statusName = PropHelper.G<Status>(_ => _.StatusName);
            int[] bids;
        	bool isSingleBuildingCulture = false;

        #endregion

        #region enums

            public enum SearchCriteriaFieldDefs { Bid, EquipmentClassId, Eid, Aid, AnalysisStartDate, AnalysisEndDate, ProjectStatus, SearchFilter, AssigneeUid, TaskId }

        #endregion

        #region properties

            public SearchCriteria SearchCriteria { get; set; }
            string SearchCriteriaFieldData { get { return viewState.Get<string>(searchCriteriaCollectionKey); } set { viewState.Set(searchCriteriaCollectionKey, value); } }
            RadGridBase<GetTasks> RadGrid { get; set; }
            Type Entity => typeof(TaskRecord);
            string Name => name;

            IEnumerable<string> GridColumnNames 
            {
                get 
                {
                    return PropHelper.F<TaskRecord>(_ => _.ClientTaskID,
                                                    _ => _.Analyses_Equipment.Equipment.Building.BuildingName, 
                                                    _ => _.Analyses_Equipment.Equipment.BID,
                                                    _ => _.Analyses_Equipment.Equipment.EquipmentName,
                                                    _ => _.Analyses_Equipment.Equipment.EquipmentType.EquipmentClassID,
                                                    _ => _.Analyses_Equipment.Equipment.EID,
                                                    _ => _.Analyses_Equipment.Analyse.AID,
                                                    _ => _.AnalysisStartDate,
                                                    _ => _.Interval,
                                                    _ => _.ReporterUID, 
                                                    _ => _.User1.Email, 
                                                    _ => _.Summary, 
                                                    _ => _.Description,
                                                    _ => _.Recommendations, 
                                                    _ => _.Actions,
                                                    _ => _.AnnualAvoidableCost,
                                                    _ => _.Status.StatusName,
                                                    _ => _.CID,
                                                    _ => _.Analyses_Equipment.Equipment.Building.TimeZone,
                                                    _ => _.WorkOrderID); 
                } 
            }

            string NameField { get { return PropHelper.G<TaskRecord>(_ => _.Analyses_Equipment.Equipment.Building.BuildingName); } }
            string IdColumnName => PropHelper.G<TaskRecord>(_ => _.TaskID);
            IDictionary<int, int> CulturesDict { get; set; }
            CultureInfo CultureInfo => new CultureInfo(siteUser.CultureName);

        #endregion

        #region events

            void Page_Init()
            {
                if (!siteUser.IsAnonymous)
                {
                    if (!siteUser.IsKGSFullAdminOrHigher) bids = siteUser.VisibleBuildings.Select(_ => _.BID).ToArray();
                }

                WireUpGrid();

                TaskEditForm.ViewTasks = this;

                cid = siteUser.CID;
            }

            void Page_FirstLoad() => SetNewCriteriaToViewState();

            void Page_Load()
            {
                TaskDownloadArea.Criteria = SearchCriteriaFieldData;

                TaskDownloadArea.PnlEdit = pnlEdit;
            }

            void Page_PreRender()
            {
                SetCollapsiblePanelAttributes();

                UpdateAnalysisStartDateGroupHeader();
            }

            #region grid events

                void radGrid_ItemCommand(object source, GridCommandEventArgs e)
                {
                    HidePanels();

                    if (e.CommandName != "Delete") HideMessages();

                    switch (e.CommandName)
                    {
                        case "Select":

                            SetVisiblePanel(pnlDetails);

                            TaskDetailsView.SetTaskDetailsFields(GetTaskRecord(e));

                            TaskDetailsView.SetLowerFocus();                            

                            break;

                        case "Edit":

                            SetVisiblePanel(pnlEdit);

                            TaskEditForm.SetFormAttributes();

                            TaskEditForm.InitializeForm(GetTaskRecord(e));

                            TaskEditForm.SetLowerFocus();               

                            break;
                    }
                }

                void radGrid_DeleteCommand(object source, GridCommandEventArgs e)
                {
                    using (SqlQueryWrapper sqlWrapper = new SqlQueryWrapper(DataMgr.ConnectionString))
                    {
                        sqlWrapper.Run((db) =>
                        {
                            var task = db.TaskRecords.Single(t => t.TaskID == RadGrid.GetId(e));

                            db.TaskRecords.DeleteOnSubmit(task);
                            db.SubmitChanges();
                        });
                    }

                    SetSuccessMsg(successMsg);

                    BindGrid();
                }

                void radGrid_GroupsChanging(object source, GridGroupsChangingEventArgs e) 
                {
                    HideMessages();

                    HidePanels();
                }

                void radGrid_ItemDataBound(object source, GridItemEventArgs e)
                {
                    if (e.Item is GridDataItem)
                    {
                        var item = (GridDataItem)e.Item;
                        var dataItem = (GetTasks)item.DataItem;
                        var clientTaskIdColName = PropHelper.G<TaskRecord>(tr => tr.ClientTaskID);
                        var lblClientTaskId = (Label)item[clientTaskIdColName].Controls[0];
                        var lblAnalysisDate = (Label)item[analysisDate].Controls[0];
                        var lblAnnualAvoidableCost = (Label)item[annualAvoidableCostInt].Controls[0];
                        var currencyCodeItem = item[currencyCode];
                        var lblCurrencyCode = (Label)currencyCodeItem.Controls[0];
                        var cost = dataItem.AnnualAvoidableCostInt;
                        var bid = int.Parse(((HiddenField)currencyCodeItem.Controls[1]).Value);
                        var buttonName = "Buttons2";

                        if (!string.IsNullOrWhiteSpace(dataItem.WorkOrderId))
                        {
                            lblClientTaskId.Text = "<div id='circle'></div>";

                            item[clientTaskIdColName].ToolTip = "Related work order exists for this task.";
                        }

                        lblAnalysisDate.Text = dataItem.AnalysisStartDate.ToShortDateString();

                        lblAnnualAvoidableCost.Text = RadGrid.GetFormattedNumValue(cost, CultureInfo);
                        
                        lblCurrencyCode.Text = (cost != null) ? new RegionInfo(CulturesDict[bid]).ISOCurrencySymbol : string.Empty;

                        var imgDiagnosticsLink = (HtmlImage)item[buttonName].Controls[1];

                        var deleteImageButton = (ImageButton)item[buttonName].Controls[3];
        
                        imgDiagnosticsLink.AddAttrib("onclick", "window.open('" + LinkHelper.BuildDiagnosticsQuickLinkFromTask(dataItem) + "')");
                        imgDiagnosticsLink.AddAttrib("height", 20);
                        imgDiagnosticsLink.AddAttrib("width", 20);
                        imgDiagnosticsLink.AddAttrib("style", "cursor: pointer;");

                        var reporterUid = dataItem.ReporterUid.HasValue ? (int?)dataItem.ReporterUid.Value : null;

                        var isTaskReporter = (reporterUid != null && reporterUid == siteUser.UID);

                        deleteImageButton.Style.Add("visibility", (siteUser.IsFullAdminOrHigher || isTaskReporter) ? "visible" : "hidden");
                    }

                    SetGroupedItem(e.Item);
                }

            #endregion

        #endregion

        #region methods

            void WireUpGrid()
            {
                RadGrid = new RadGridBase<GetTasks>(this, radGrid);

                RadGrid.Entity = Entity;
                RadGrid.GetGridLoader = BindGrid;
                RadGrid.GetEntitiesForGrid = (results) => GetTaskData(results);
                RadGrid.GetTransformData = GetTransformTaskData;
                RadGrid.GetItemCountMessage = SetItemCountMsg;
                RadGrid.GridColumnNames = GridColumnNames;
                RadGrid.NameField = NameField;
                RadGrid.IdColumnName = IdColumnName;
                RadGrid.PageSize = 50;
                RadGrid.Name = Name;
                RadGrid.CacheHashKey = siteUser.CID.ToString();
                RadGrid.RadGridItemCommandHandler = radGrid_ItemCommand;
                RadGrid.RadGridDeleteCommandHandler = radGrid_DeleteCommand;
                RadGrid.RadGridGroupsChangingEventHandler = radGrid_GroupsChanging;
                RadGrid.RadGridItemEventHander = radGrid_ItemDataBound;
                RadGrid.EnableGrouping = RadGrid.EnableItemDataBound = RadGrid.ShowFooter = true;
                RadGrid.CultureInfo = new CultureInfo(siteUser.CultureName);
                RadGrid.DefaultNumVal = "N/A";

                SetColumnAttributes();    
            }

            void SetColumnAttributes()
            {
                var emptyString = string.Empty;

                //the boolean parameters are for enabling/disabling grid sorting/grouping respectively.
                //defaults are sorting = true, grouping = false;
                RadGrid.SetColumnAttribute("Buttons1", emptyString, false, false);
                RadGrid.SetColumnAttribute(clientTaskId, "Task ID");
                RadGrid.SetColumnAttribute(buildingName, typeof(Building).Name, true, true);
                RadGrid.SetColumnAttribute(equipmentName, typeof(Equipment).Name, true, true);
                RadGrid.SetColumnAttribute(email, "Assigned", true, true);
                RadGrid.SetColumnAttribute(summary, summary);
                RadGrid.SetColumnAttribute(description, multiField);
                //RadGrid.SetColumnAttribute(recommendations, recommendations);
                //RadGrid.SetColumnAttribute(actions, actions);
                RadGrid.SetColumnAttribute(analysisDate, issueDate, true, true);
                RadGrid.SetColumnAttribute(annualAvoidableCostInt, annualAvoidableCosts, true, true);
                RadGrid.SetColumnAttribute(currencyCode, "Currency Code", false, false);
                RadGrid.SetColumnAttribute(statusName, typeof(Status).Name);
                RadGrid.SetColumnAttribute("Buttons2", emptyString, false, false);
            }

            void SetColumnFooters()
            {
                var numFormat = "{0:N0}";
                
                RadGrid.SetColumnFooter(buildingName, GridAggregateFunction.Count, $"Total Tasks: {numFormat}");

                var currencyCode = new RegionInfo(CulturesDict.First().Value).ISOCurrencySymbol;

                RadGrid.SetColumnFooter
                (
                    annualAvoidableCostInt,
                    GridAggregateFunction.Sum,
                    (isSingleBuildingCulture) ? $"Total Savings: {numFormat} {currencyCode}" : "Total savings cannot be calculated due to multiple building currencies."
                );
            }

            void UpdateAnalysisStartDateGroupHeader()
            {
                //we need to change the group item header from "AnalysisStartDate" to "Issue Date"
                //unfortunately you must first loop through the group items, and then through all the literal controls
                //the text for the group items are stored in generated literal controls
                if (radGrid.GroupPanel.GroupPanelItems.Count > 0)
                {
                    foreach (var groupItem in radGrid.GroupPanel.GroupPanelItems)
                    {
                        foreach (Literal literal in groupItem.Controls.Cast<Control>().Where(_ => _ is Literal))
                        {
                            if (literal.Text.Contains(analysisDate)) literal.Text = issueDate;

                            if (literal.Text.Contains(description)) literal.Text = multiField;
                        }
                    }
                }
            }

            public void BindGrid()
            {
                RadGrid.GetAndBindData();

                TaskDownloadArea.Visible = RadGrid.Visible = (RadGrid.RowCount > 0);
            }

            IEnumerable GetTaskData(IEnumerable<string> criteria)
            {
                var query = new TaskRecordQuery(DataMgr.ConnectionString, siteUser).LoadWith(_ => _.Status)
                .LoadWith(_ => _.User1)
                .LoadWith<Analyses_Equipment, Equipment, Equipment, Analyses_Equipment>(_ => _.Analyses_Equipment, _ => _.Equipment, _ => _.EquipmentType, _ => _.Building, _ => _.Analyse)
                .FinalizeLoadWith.GetAll(cid);

                var searchCriteriaFieldData = GetSearchCriteriaFieldData();

                if (searchCriteriaFieldData.Bid.HasValue)
                {
                    query.ByBuilding(searchCriteriaFieldData.Bid.Value);

                    isSingleBuildingCulture = true;
                }
                else if (bids != null && bids.Any())
                {
                    query.ByBuildings(bids);

                    isSingleBuildingCulture = bids.Count() == 1;
                }

                if (searchCriteriaFieldData.TaskId.HasValue)
                {
                    var resultByTaskId = query.ByTaskId(searchCriteriaFieldData.TaskId.Value).ToEnumerable();

                    SetCultureDictAndFooter(resultByTaskId);

                    return resultByTaskId;
                }

                query.ByAnalysisDateRange(searchCriteriaFieldData.AnalysisStartDate, searchCriteriaFieldData.AnalysisEndDate);
                query.ByStatus(searchCriteriaFieldData.ProjectStatus.Split(',').Select(int.Parse).ToList());
            
                if (searchCriteriaFieldData.AssigneeUid.HasValue) query.ByAssignee(searchCriteriaFieldData.AssigneeUid.Value);

                if (searchCriteriaFieldData.EquipmentClassId.HasValue) query.ByEquipmentClass(searchCriteriaFieldData.EquipmentClassId.Value);

                if (searchCriteriaFieldData.Eid.HasValue) query.ByEquipment(searchCriteriaFieldData.Eid.Value);

                if (searchCriteriaFieldData.Aid.HasValue) query.ByAnalysis(searchCriteriaFieldData.Aid.Value);

                var results = query.ToEnumerable();
            
                results = FilterTasks(results, searchCriteriaFieldData.SearchFilter);

                SetCultureDictAndFooter(results);

                return results;
            }
        
            //TODO: possible alter the existing filter service to include the ability to filter against a queryable object
            //Also, we are not caching the data in grid cacher, so this will be called instead of grid cacher filtering
            IEnumerable<TaskRecord> FilterTasks(IEnumerable<TaskRecord> results, string searchText)
            {
                //if the search text is empty, simply return existing results, and do not instantiate the filter service
                if (string.IsNullOrWhiteSpace(searchText)) return results;

                //the search results are filtered via a service, which requires the input text (notes filter here), and fields to search against
                var filterSvc = new SearchFilterServiceFactory().GetSearchFilterService();

                filterSvc.ProcessSearchInput(searchText);

                var fields = new [] 
                {
                    nameof(TaskRecord.Summary),
                    nameof(TaskRecord.Recommendations),
                    nameof(TaskRecord.Actions),
                };

                return filterSvc.FilterSubjectsBySearchTerms(results, fields);
            }

            void SetCultureDictAndFooter(IEnumerable<TaskRecord> results)
            {
                if (!results.Any()) return;

                CulturesDict = DataMgr.BuildingDataMapper.GetBuildingSettingsCultureByBids(results.Select(t => t.Analyses_Equipment.Equipment.BID).Distinct().ToArray());

                if (!isSingleBuildingCulture) isSingleBuildingCulture = CulturesDict.GroupBy(_ => _.Value).Count() == 1;

                SetColumnFooters();
            }    

            TaskRecord GetTaskRecord(GridCommandEventArgs e)
            {
                return new TaskRecordQuery(DataMgr.ConnectionString, siteUser).LoadWith(_ => _.Status)
                .LoadWith(_ => _.User)
                .LoadWith(_ => _.User1)
                .LoadWith(_ => _.User2)
                .LoadWith(_ => _.WorkType)
                .LoadWith<Analyses_Equipment, Analyses_Equipment, Equipment>(_ => _.Analyses_Equipment, _ => _.Analyse, _ => _.Equipment, _ => _.Building)
                .FinalizeLoadWith.GetAll(cid).ByTaskId(RadGrid.GetId(e)).ToList().Single();
            }

            IEnumerable<GetTasks> GetTransformTaskData(IEnumerable<GridCacher.GridSet.Row> rows)
            {
                var getTasks = new List<GetTasks>();

                foreach (var row in rows)
                {
                    var getTask = new GetTasks();

                    var timeZone = row.Cells[PropHelper.G<Building>(_ => _.TimeZone)];

                    getTask.ClientTaskID = int.Parse(row.Cells[clientTaskId]);
                    getTask.TaskID = int.Parse(row.ID);
                    getTask.BuildingName = row.Cells[buildingName];
                    getTask.Bid = int.Parse(row.Cells[PropHelper.G<Building>(_ => _.BID)]);
                    getTask.EquipmentName = row.Cells[equipmentName];
                    getTask.EquipmentClassId = int.Parse(row.Cells[PropHelper.G<EquipmentClass>(_ => _.EquipmentClassID)]);
                    getTask.Eid = int.Parse(row.Cells[PropHelper.G<Equipment>(_ => _.EID)]);
                    getTask.Aid = int.Parse(row.Cells[PropHelper.G<Analyse>(_ => _.AID)]);
                    getTask.AnalysisRange = row.Cells[PropHelper.G<TaskRecord>(_ => _.Interval)];
                    getTask.AnalysisStartDate = DateTime.ParseExact(row.Cells[PropHelper.G<TaskRecord>(_ => _.AnalysisStartDate)], "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    getTask.ReporterUid = (!string.IsNullOrWhiteSpace(row.Cells[reporterUid])) ? (int?)int.Parse(row.Cells[reporterUid]) : null;
                    getTask.Email = row.Cells[email];
                    getTask.Summary = row.Cells[summary];
                    getTask.Description = row.Cells[description];
                    getTask.Recommendations = row.Cells[recommendations];
                    getTask.Actions = row.Cells[actions];
                    getTask.StatusName = row.Cells[statusName];
                    getTask.Cid = int.Parse(row.Cells[PropHelper.G<Client>(_ => _.CID)]);
                    getTask.TimeZone = timeZone;
                    getTask.AnnualAvoidableCost = row.Cells[annualAvoidableCost];
                    getTask.AnnualAvoidableCostInt = (!string.IsNullOrWhiteSpace(row.Cells[annualAvoidableCost])) ? (int?)double.Parse(row.Cells[annualAvoidableCost], CultureInfo.InvariantCulture) : null;
                    getTask.WorkOrderId = row.Cells[PropHelper.G<TaskRecord>(_ => _.WorkOrderID)];

                    getTasks.Add(getTask);
                }

                return getTasks;
            }

            protected string TrimCellContents(string value, int maxLength) => StringHelper.TrimText(value, maxLength);

            void SetCollapsiblePanelAttributes()
            {
                foreach (GridDataItem item in RadGrid.Items)
                {
                    var cpeText = "cpe";

                    var cpeDescription = (CollapsiblePanelExtender)item.FindControl($"{cpeText}Description");

                    var cpeRecommendations = (CollapsiblePanelExtender)item.FindControl($"{cpeText}Recommendations");

                    var cpeActions = (CollapsiblePanelExtender)item.FindControl($"{cpeText}Actions");

                    SetCollapsiblePanelAttribute(cpeDescription);

                    SetCollapsiblePanelAttribute(cpeRecommendations);

                    SetCollapsiblePanelAttribute(cpeActions);
                }
            }

            void SetCollapsiblePanelAttribute(CollapsiblePanelExtender cpe)
            {
                var cpeId = cpe.ID.Substring(3);

                var cpeParent = cpe.Parent;

                var cpeLnk = cpeParent.Controls.OfType<HyperLink>().First();

                var cpeLbl = cpeLnk.Controls.OfType<Label>().First();

                var cpePnl = cpeParent.Controls.OfType<Panel>().First();

                cpeLnk.ID = $"lnk{cpeId}";

                cpeLbl.ID = $"lbl{cpeId}";

                cpePnl.ID = $"pnl{cpeId}";

                cpe.TargetControlID = cpePnl.ID;
                cpe.ExpandControlID = cpeLnk.ID;
                cpe.CollapseControlID = cpeLnk.ID;
                cpe.TextLabelID = cpeLbl.ID;
                cpe.CollapsedSize = 0;
                cpe.Collapsed = !SearchCriteria.ShowDetails;
                cpe.AutoCollapse = cpe.AutoExpand = cpe.ScrollContents = false;
                cpe.ExpandDirection = CollapsiblePanelExpandDirection.Vertical;
                cpe.CollapsedText = $"show {cpeId.ToLower()}";
                cpe.ExpandedText = $"hide {cpeId.ToLower()}";

                cpeLnk.CssClass = cpeLbl.CssClass = "toggle";
            }

            public void ClearCriteriaFromViewState() { if (viewState.HasKey(searchCriteriaCollectionKey)) viewState.Remove(searchCriteriaCollectionKey); }

            public void SetNewCriteriaToViewState() 
            {
                var searchCriteriaFielData = SearchCriteria.GetSearchCriteriaFieldData();

                SearchCriteriaFieldData = string.Join("|", SetFieldValue(searchCriteriaFielData.Bid),
                                                           SetFieldValue(searchCriteriaFielData.EquipmentClassId),
                                                           SetFieldValue(searchCriteriaFielData.Eid),
                                                           SetFieldValue(searchCriteriaFielData.Aid),
                                                           SetFieldValue(searchCriteriaFielData.AnalysisStartDate),
                                                           SetFieldValue(searchCriteriaFielData.AnalysisEndDate),
                                                           searchCriteriaFielData.ProjectStatus,
                                                           searchCriteriaFielData.SearchFilter,
                                                           searchCriteriaFielData.AssigneeUid,
                                                           searchCriteriaFielData.TaskId); 
            }

            SearchCriteria.SearchCriteriaFields GetSearchCriteriaFieldData() 
            {
                var searchCriteriaFields = new SearchCriteria.SearchCriteriaFields();

                var searchCriteriaFieldData = SearchCriteriaFieldData.Split('|');

                var offset = siteUser.UserTimeZoneOffset;

                if (!string.IsNullOrWhiteSpace(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.Bid)]))
                    searchCriteriaFields.Bid = int.Parse(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.Bid)]);

                if (!string.IsNullOrWhiteSpace(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.EquipmentClassId)]))
                    searchCriteriaFields.EquipmentClassId = int.Parse(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.EquipmentClassId)]);

                if (!string.IsNullOrWhiteSpace(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.Eid)]))
                    searchCriteriaFields.Eid = int.Parse(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.Eid)]);

                if (!string.IsNullOrWhiteSpace(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.Aid)]))
                    searchCriteriaFields.Aid = int.Parse(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.Aid)]);

                searchCriteriaFields.AnalysisStartDate = DateTime.Parse(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.AnalysisStartDate)]);

                searchCriteriaFields.AnalysisEndDate = DateTime.Parse(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.AnalysisEndDate)]);

                searchCriteriaFields.ProjectStatus = searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.ProjectStatus)];

                if (!string.IsNullOrWhiteSpace(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.SearchFilter)]))
                    searchCriteriaFields.SearchFilter = searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.SearchFilter)];

                if (!string.IsNullOrWhiteSpace(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.AssigneeUid)]))
                    searchCriteriaFields.AssigneeUid = int.Parse(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.AssigneeUid)]);

                if (!string.IsNullOrWhiteSpace(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.TaskId)]))
                    searchCriteriaFields.TaskId = int.Parse(searchCriteriaFieldData[Convert.ToInt32(SearchCriteriaFieldDefs.TaskId)]);

                return searchCriteriaFields;
            }

            string SetFieldValue(object fieldValue) => (fieldValue != null) ? fieldValue.ToString() : string.Empty;

            public void SetItemCountMsg() => lblResults.Text = RadGrid.GetRowCountMsg();

            public void HideMessages() => lblResults.Text = lblError.Text = string.Empty;

            public void HidePanels() => pnlDetails.Visible = pnlEdit.Visible = false;

            private void SetVisiblePanel(Panel pnl) => pnl.Visible = true;

            public void SetSuccessMsg(string successMsg) => SetMessage(lblResults, successMsg);

            public void SetErrorMsg(string errorMsg) => SetMessage(lblError, errorMsg);

            void SetMessage(Label label, string message)
            {
                lnkSetFocus.Focus();

                label.Text = message;
            }

            public void ClearEditItems() => RadGrid.ClearEditItems();

            public void ResetGrid() 
            {
                RadGrid.ResetGrouping();

                RadGrid.ResetSorting();

                RadGrid.ResetPaging();
            }

            void SetGroupedItem(GridItem item)
            {
                //grouped items within the grid default the header for Issue Date to "AnalysisStartDate: mm/dd/yyyy hh:mm:ss" where those values are 00:00:00
                //the items needs to be altered and read as "Issue Date: mm/dd/yyyy", with the date formatted in the users culture
                //also "annual avoidable costs" value needs to be formatted for the users culture
                if (item is GridGroupHeaderItem)
                {
                    var header = (GridGroupHeaderItem)item;

                    var headerText = header.DataCell.Text;

                    if (RadGrid.GroupedItemWasSet(header, headerText, (val) => DateTime.Parse(val).ToShortDateString(), analysisDate, issueDate)) return;

                    if (RadGrid.GroupedItemWasSet(header, headerText, (val) => RadGrid.GetFormattedNumValue(val, CultureInfo), annualAvoidableCosts)) return;
                }
            }
    
        #endregion
    }
}