﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Common.Constants;
using CW.Utility;
using CW.Website._controls.tasks.Factories;
using CW.Website._controls.tasks.Interfaces;
using CW.Website._controls.tasks.Models;
using System;
using System.Web.UI.HtmlControls;
using static CW.Common.Constants.BusinessConstants.Status;

namespace CW.Website._controls.tasks
{
    public partial class TaskAddFormModal : TaskForm
    {
        #region field(s)

            ICreateTaskRecordService _service;

        #endregion

        #region events

            void Page_Init()
            {
                ValidationGroup = "CreateTask";

                LinkButton = btnCreateTask;

                SetFormFieldsAttributes();

                SetValidatorAttributes();

                _service = new TaskRecordServiceFactory(LogMgr).GetCreateTaskRecordService();
            }

            void Page_FirstLoad() => createNewUserLinkDiv.Visible = siteUser.IsSuperAdmin;

            #region button events

                protected void createTask_Click(object sender, EventArgs e)
                {
                    ClearMessageLabels();

                    var taskRecord = GetModelTaskRecord();

                    _service.RunCreateTaskRecordService(taskRecord);

                    UpdateLabels(lblTaskAdded, taskRecord);

                    createTaskMPE.Show();
                }

            #endregion

        #endregion

        #region methods

            public void InitializeForm(ViewModelTaskForm formFields)
            {
                ClearFormFields(); //clear the view state data, in case the user chose another record

                PersistFormFields(formFields); //these fields will be used for inserting the task record

                if (siteUser.CID != formFields.Cid) return;

                //TODO: figure out the best way to update the label on the diagnostics page for this message
                //lblTaskError.Text += "<br />Attempted operation created an invalid state.  Please contact administrator.";

                var emptyString = string.Empty;

                ClearMessageLabels();

                lblTaskError.Text = txtSummary.Text = emptyString;

                foreach (var textArea in FindControls<HtmlTextArea>())
                    textArea.InnerText = emptyString;

                SetFormFieldsData(formFields);

                var taskDropDowns = new TaskDropDowns();

                taskDropDowns.TaskBid = formFields.Bid;

                taskDropDowns.BindAssigneeDDL(ddlAssignee, false, true, true);

                ddlAssignee.SelectedValue = "-1";

                CMMSTaskFields.InitializeForm(formFields);

                taskDropDowns.BindStatusDDL(ddlStatus);

                ddlStatus.SelectedValue = Convert.ToInt32(Statuses.Open).ToString();
            }

            void SetFormFieldsData(ViewModelTaskForm formFields)
            {
                var equipmentName = formFields.EquipmentName;

                var notesSummary = formFields.NotesSummary.Replace("\r\n", string.Empty).Trim();

                lblBuildingAndEquipment.Text = $"{formFields.BuildingName} {equipmentName}";

                lblAnalysisAndDate.Text = $"{formFields.AnalysisName} {formFields.AnalysisStartDate.ToShortDateString()}";

                txtSummary.Text = StringHelper.TrimText($"{equipmentName} {notesSummary}", 247);
                                                                                        
                txtDescription.InnerText = notesSummary;
            }

            ModelTaskRecord GetModelTaskRecord() //This method is kind of long, but unsure of a clean way to shrink it.
            {
                var taskRecord = new ModelTaskRecord();

                var currentDate = DateTime.UtcNow;

                var interval = DataConstants.AnalysisRange.Daily;

                var isTaskCompleted = ddlStatus.SelectedItem.Value == Convert.ToInt32(Statuses.Completed).ToString();

                var persistedFormFields = (ViewModelTaskForm)ViewState[viewStateName];

                var description = txtDescription.InnerText.Trim();

                var recommendations = txtRecommendations.InnerText.Trim();

                var actions = txtActions.InnerText.Trim();

                taskRecord.Bid = persistedFormFields.Bid;

                taskRecord.Eid = persistedFormFields.Eid;

                taskRecord.Aid = persistedFormFields.Aid;

                taskRecord.AnalysisName = persistedFormFields.AnalysisName;

                taskRecord.Summary = txtSummary.Text.Trim();

                taskRecord.Aeid = persistedFormFields.Aeid;

                taskRecord.Uid = siteUser.UID;

                taskRecord.ReporterUid = taskRecord.Uid;

                taskRecord.AssignedUid = int.Parse(ddlAssignee.SelectedValue);

                taskRecord.AnalysisStartDate = persistedFormFields.AnalysisStartDate;

                Enum.TryParse(persistedFormFields.AnalysisRange, out interval);

                taskRecord.Interval = interval.ToString();

                taskRecord.Description = (!string.IsNullOrWhiteSpace(description)) ? description : null;

                taskRecord.Recommendations = (!string.IsNullOrWhiteSpace(recommendations)) ? recommendations : null;

                taskRecord.Actions = (!string.IsNullOrWhiteSpace(actions)) ? actions : null;

                taskRecord.LastModifiedUid = taskRecord.Uid;

                taskRecord.StatusId = int.Parse(ddlStatus.SelectedValue);

                taskRecord.DateCompleted = (isTaskCompleted) ? (DateTime?)currentDate : null;

                taskRecord.DateModified = taskRecord.DateCreated = currentDate;

                taskRecord.FullName = siteUser.FullName;

                taskRecord.CultureName = siteUser.CultureName;

                taskRecord.IsSchneiderTheme = siteUser.IsSchneiderTheme;

                taskRecord.Cid = siteUser.CID;

                taskRecord.WorkOrderTypeId = CMMSTaskFields.WorkOrderTypeId;

                taskRecord.WorkOrderCategory = CMMSTaskFields.WorkOrderCategory;

                taskRecord.CreateWorkOrder = CMMSTaskFields.CreateWorkOrder;
            
                taskRecord.Email = siteUser.Email;

                taskRecord.EmailStatus = "Created";

                taskRecord.TaskMessage = "Task Added";

                taskRecord.SendEmail = chkEmail.Checked;

                taskRecord.IsTaskCompleted = isTaskCompleted;

                taskRecord.DataMgr = DataMgr;

                taskRecord.LogMgr = LogMgr;

                return taskRecord;
            }
        
        #endregion
    }
}