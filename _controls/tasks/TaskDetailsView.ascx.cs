﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Data;
using CW.Website._controls.tasks.Factories;
using CW.Website._controls.tasks.Interfaces;
using CW.Website._framework;
using System.Linq;

namespace CW.Website._controls.tasks
{
    public partial class TaskDetailsView : SiteUserControl
    {
        #region method(s)

            public void SetTaskDetailsFields(TaskRecord taskRecord)
            {
                //need to reset the appropriate list item values back to empty strings between records, 
                //otherwise incorrect values will be maintained (viewstate fun :D)
                ResetFields();

                var service = new TaskRecordDetailsServiceFactory(LogMgr).GetTaskRecordDetailsService(taskRecord, siteUser.CultureName);

                var modelTaskRecord = service.GetModelTaskRecord();

                SetFields(service, modelTaskRecord);
            
                SetOptionalFields(service, modelTaskRecord);

                var metricsUls = GetMetricsUls();

                service.SetVDataMetricsIfAny(metricsUls);
            }
        
            void ResetFields()
            {
                var ids = new string[] 
                {
                    ulDateCompleted.ID,
                    ulDescription.ID,
                    ulRecommandations.ID,
                    ulActions.ID,
                    ulAnnualAvoidableCost.ID,
                    ulAnnualAvoidableCoolingUse.ID,
                    ulAnnualAvoidableElectricUse.ID,
                    ulAnnualAvoidableHeatingUse.ID,
                    ulWorkOrderNum.ID
                };

                FindControls<UListItem>().Where(li => ids.Contains(li.ID)).ToList().ForEach(li => li.Value = string.Empty);
            }

            void SetFields(ITaskRecordDetailsService service, ModelTaskRecord modelTaskRecord)
            {
                ulSummary.Value = modelTaskRecord.Summary.Trim();

                ulBuildingName.Value = modelTaskRecord.Building.BuildingName;

                ulEquipmentName.Value = modelTaskRecord.Equipment.EquipmentName;

                ulAnalysisName.Value = modelTaskRecord.Analyse.AnalysisName;

                ulAnalysisDate.Value = modelTaskRecord.AnalysisStartDate.ToShortDateString();

                ulInterval.Value = modelTaskRecord.Interval;

                ulReporter.Value = modelTaskRecord.ReporterEmail;

                ulAssigned.Value = modelTaskRecord.AssigneeEmail;

                ulStatus.Value = modelTaskRecord.Status.StatusName;

                ulDateCreated.Value = service.GetFormattedString(modelTaskRecord.DateCreated, ModelTaskRecord.DateType.CreatedOrCompleted);

                ulLastModified.Value = service.GetFormattedString(modelTaskRecord.DateModified, ModelTaskRecord.DateType.Modified);

                ulWorkOrderType.Value = modelTaskRecord.WorkOrderType;
            }

            void SetOptionalFields(ITaskRecordDetailsService service, ModelTaskRecord modelTaskRecord)
            {
                ulDateCompleted.Visible = modelTaskRecord.DateCompleted.HasValue;

                ulDateCompleted.Value = string.Empty;

                //special case here, we can't just pass to method with a null check, as this field has other information appended to it
                if (modelTaskRecord.DateCompleted.HasValue)
                    ulDateCompleted.Value = service.GetFormattedString(modelTaskRecord.DateCompleted.Value, ModelTaskRecord.DateType.CreatedOrCompleted);

                service.DisplayOptionalField(ulDescription, modelTaskRecord.Description);

                service.DisplayOptionalField(ulRecommandations, modelTaskRecord.Recommendations);

                service.DisplayOptionalField(ulActions, modelTaskRecord.Actions);

                service.DisplayOptionalField(ulWorkOrderNum, modelTaskRecord.WorkOrderId);
            }

            UListItem[] GetMetricsUls()
            {
                return new UListItem[] { ulAnnualAvoidableCost, ulAnnualAvoidableCoolingUse, ulAnnualAvoidableElectricUse, ulAnnualAvoidableHeatingUse };
            }

            public void SetLowerFocus() => lnkSetFocus.Focus();

        #endregion
    }
}