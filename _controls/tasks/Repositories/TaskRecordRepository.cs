﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Data;
using CW.Data.Models.PointType;
using CW.Website._controls.tasks.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;

namespace CW.Website._controls.tasks.Repositories
{
    public class TaskRecordRepository : ITaskRecordRepository
    {
        #region field(s)

            string _connStr;

        #endregion

        #region constructor(s)

            public TaskRecordRepository(string connStr) { _connStr = connStr; }

        #endregion

        #region inteface method(s)

            public BuildingSetting GetBuildingSettings(ModelTaskRecord modelTaskRecord)
            {
                var id = modelTaskRecord.Bid;

                using (var sqlWrapper = new SqlQueryWrapper(_connStr))
                {
                    return sqlWrapper.Run((db) => db.BuildingSettings.SingleOrDefault(bs => bs.BID == id));
                }
            }

            public IEnumerable<GetPointTypeData> GetEngUnits(Int32[] ids)
            {
                using (var sqlWrapper = new SqlQueryWrapper(_connStr))
                {
                    return sqlWrapper.Run(db =>
                    {
                        var options = new DataLoadOptions();

                        options.LoadWith<PointType>(pt => pt.PointClass);

                        options.LoadWith<PointClass>(pc => pc.EngUnit);

                        options.LoadWith<PointClass>(pc => pc.EngUnit1);

                        db.LoadOptions = options;

                        return db.PointTypes.Where(_ => ids.Contains(_.PointTypeID)).Select(pt => new GetPointTypeData
                        {
                            PointTypeID = pt.PointTypeID,

                            IPEngUnits = pt.PointClass.EngUnit.EngUnits,

                            SIEngUnits = pt.PointClass.EngUnit1.EngUnits

                        }).ToList();
                    });
                }
            }

        #endregion
    }
}