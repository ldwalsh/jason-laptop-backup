﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.PointType;
using CW.Logging;
using CW.Utility;
using CW.Website._controls.tasks.Factories;
using CW.Website._controls.tasks.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CW.Website._controls.tasks.Services
{
    public class TaskRecordDetailsService : ITaskRecordDetailsService
    {
        #region fields

            ModelTaskRecord _modelTaskRecord;
    
            BuildingSetting _buildingSettings;

            int[] _engUnitIds;

            IEnumerable<GetPointTypeData> _engUnits;

            CultureInfo _culture;

            RegionInfo _region;

        #endregion

        #region constructor(s)

            public TaskRecordDetailsService(TaskRecord taskRecord, string culture, ISectionLogManager logger)
            {
                var _repo = new TaskRecordRepositoryFactory().GetTaskRecordRepository();

                _modelTaskRecord = new TaskRecordDataTransformService(logger).TransformTaskRecordDetailsData(taskRecord);

                _buildingSettings = _repo.GetBuildingSettings(_modelTaskRecord);

                _engUnitIds = GetEngUnitIds();

                _engUnits = _repo.GetEngUnits(_engUnitIds);

                _culture = new CultureInfo(culture);

                _region = new RegionInfo(_buildingSettings.LCID);
            }

        #endregion

        #region interface method(s)

            public ModelTaskRecord GetModelTaskRecord() => _modelTaskRecord;

            public void SetVDataMetricsIfAny(UListItem[] uls)
            {
                var numberFormat = "n0";

                DisplayFormattedField(uls[0], _modelTaskRecord.AnnualAvoidableCost, numberFormat);

                DisplayFormattedField(uls[1], _modelTaskRecord.AnnualAvoidableCoolingUse, numberFormat, GetEngUnit(_engUnitIds[0]));

                DisplayFormattedField(uls[2], _modelTaskRecord.AnnualAvoidableElectricUse, numberFormat, GetEngUnit(_engUnitIds[1]));

                DisplayFormattedField(uls[3], _modelTaskRecord.AnnualAvoidableHeatingUse, numberFormat, GetEngUnit(_engUnitIds[2]));
            }

            public void DisplayOptionalField(UListItem li, string item)
            {
                if (string.IsNullOrWhiteSpace(item)) return;

                li.Visible = true;

                li.Value = item.Trim();
            }
        
            public string GetFormattedString(DateTime date, ModelTaskRecord.DateType dateType)
            {
                var timeZone = _modelTaskRecord.TimeZone;

                var offset = TimeZoneInfo.FindSystemTimeZoneById(timeZone).BaseUtcOffset.Hours;

                var convertedDate = DateTimeHelper.GetConvertedDateInGeneralFormat(date, timeZone);

                var email = _modelTaskRecord.SecondaryEmail;

                return (dateType == ModelTaskRecord.DateType.CreatedOrCompleted) ? $"{convertedDate}, (UTC Offset: {offset}, Time Zone: {timeZone})"
                                                                                 : $"{email}, {convertedDate} (UTC Offset: {offset}, Time Zone: {timeZone})";
            }

        #endregion

        #region method(s)

            int[] GetEngUnitIds()
            {
                return new int[]
                {
                    BusinessConstants.PointType.AnnualCoolingUsePointTypeID,
                    BusinessConstants.PointType.AnnualElectricUsePointTypeID,
                    BusinessConstants.PointType.AnnualHeatingUsePointTypeID
                };
            }

            string GetEngUnit(int id)
            {
                var engUnit = _engUnits.Single(eu => eu.PointTypeID == id);

                return (_buildingSettings.UnitSystem) ? engUnit.SIEngUnits : engUnit.IPEngUnits;
            }

            string GetFormattedValue(string item, string format, bool addCurrencyCode = false)
            {
                var value = Convert.ToDecimal(item, CultureInfo.InvariantCulture).ToString(format, _culture);

                return (addCurrencyCode) ? $"{value} {_region.ISOCurrencySymbol}" : value;
            }

            void DisplayFormattedField(UListItem li, string item, string format, string engUnit = null)
            {
                if (string.IsNullOrWhiteSpace(item)) return;

                li.Visible = true;

                var formattedValue = GetFormattedValue(item, format, (engUnit == null));

                li.Value = (engUnit != null) ? $"{formattedValue} {engUnit}" : formattedValue;
            }

        #endregion
    }
}