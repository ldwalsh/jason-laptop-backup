﻿using CW.CMMSIntegration.WorkOrderSystems.Factories;
using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Logging;
using CW.Website._controls.tasks.Interfaces;

namespace CW.Website._controls.tasks.Services
{
    public class CreateTaskRecordService : ICreateTaskRecordService
    {
        #region properties

            ISectionLogManager LogMgr { get; set; }

        #endregion 
        
        #region field(s)

        ITaskRecordDataTransformService _dtsSvc;

            ITaskRecordEmailService _emailSvc;

        #endregion

        #region constructor(s)

            public CreateTaskRecordService(ITaskRecordDataTransformService dtsSvc, ITaskRecordEmailService emailSrv, ISectionLogManager logger)
            {
                LogMgr = logger;

                _dtsSvc = dtsSvc;

                _emailSvc = emailSrv;
            }

        #endregion

        #region interface method(s)

            public void RunCreateTaskRecordService(ModelTaskRecord taskRecord)
            {
                var vDataMetrics = new VDataMetrics(taskRecord);

                var calcMetrics = vDataMetrics.ObtainMetricsAndCalculateSavings();

                vDataMetrics.SetMetricsForTaskRecord(calcMetrics); //this will only set them if they exist for this task

                _dtsSvc.TransformCreateData(taskRecord);

                if (taskRecord.CreateWorkOrder)
                {
                    var woTask = _dtsSvc.TransformWorkOrderData(taskRecord);

                    //the task record is being passed to the service in case there is an error. We need to bubble up the message to the model
                    new WorkOrderServiceFactory(LogMgr).GetWorkOrderService(woTask, taskRecord).RunWorkOrderService();
                }

                if (taskRecord.SendEmail) _emailSvc.Send(taskRecord);
            }

        #endregion
    }
}