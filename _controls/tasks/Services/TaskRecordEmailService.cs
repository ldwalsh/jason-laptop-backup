﻿using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._controls.tasks.Interfaces;
using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace CW.Website._controls.tasks.Services
{
    public class TaskRecordEmailService : ITaskRecordEmailService
    {
        #region const(s)

            const string emailItem = "{0}: {1}<br />";
            const string emailItemBlock = "<strong>{0}:</strong><br />{1}<br /><br />";
            const string emailName = "{0} {1}";

        #endregion

        #region field(s)

            ITaskRecordDataTransformService _dts;

        #endregion

        #region constructor(s)

            public TaskRecordEmailService(ITaskRecordDataTransformService dts) { _dts = dts; }

        #endregion

        #region interface method(s)

            public void Send(ModelTaskRecord taskRecord) => SendTaskRecordEmail(taskRecord);
            
        #endregion

        #region method(s)

            void SendTaskRecordEmail(ModelTaskRecord taskRecord)
            {
                var subject = $"$product Task {taskRecord.EmailStatus} - {taskRecord.Summary}";

                var reporter = taskRecord.User;
                var assignee = taskRecord.User1;

                var formattedBodyTop = new StringBuilder();
                formattedBodyTop.AppendLine("<strong>Do Not Reply to this email. Any updates should be made to the Task within $product</strong>");
                formattedBodyTop.AppendLine($"<br /><br />Task {taskRecord.EmailStatus} by {taskRecord.FullName}");

                var formattedBodyMiddle = FormatEmailBody(taskRecord);
            
                try
                {
                    var email = taskRecord.Email;

                    var from = email;

                    var to = email;

                    if (reporter != null) to = to.Contains(reporter.Email) ? to : $"{to},{reporter.Email}";

                    if (assignee != null) to = to.Contains(assignee.Email) ? to : $"{to},{assignee.Email}";

                    var taskforDiagnosticsLink = _dts.TransformDataForEmail(taskRecord);

                    Email.SendTaskEmail
                    (
                        from,
                        to,
                        subject, formattedBodyTop.ToString(), formattedBodyMiddle.ToString(),
                        LinkHelper.BuildTaskQuickLinkForSingleTask(ConvertToGetTasksModel(taskRecord)),
                        LinkHelper.BuildDiagnosticsQuickLinkFromTask(taskforDiagnosticsLink),
                        taskRecord.IsSchneiderTheme
                    );

                    taskRecord.EmailSent = true;
                }
                catch (Exception ex)
                {
                    taskRecord.EmailFailedMessage = "Sending of email failed. Please contact an administrator.";

                    taskRecord.LogMgr.Log(DataConstants.LogLevel.ERROR, $"Error sending email from user: {taskRecord.Email}.", ex);
                }
            }

            StringBuilder FormatEmailBody(ModelTaskRecord taskRecord)
            {
                var formattedBodyMiddle = new StringBuilder();

                var summary = taskRecord.Summary;

                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { PropHelper.G<TaskRecord>(_ => _.Summary), summary }));
                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { "Task ID", taskRecord.ClientTaskId }));
                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { "Reporter", taskRecord.User.Email }));
                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { "Assignee", taskRecord.User1.Email }));
                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { "Status", (BusinessConstants.Status.Statuses)taskRecord.StatusId }));                            
                if (taskRecord.IsTaskCompleted) SetCompletedTaskInfo(taskRecord, formattedBodyMiddle);
                formattedBodyMiddle.AppendLine("<br />");

                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { typeof(Client).Name, taskRecord.Client.ClientName }));
                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { typeof(Building).Name, taskRecord.Building.BuildingName }));
                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { typeof(Equipment).Name, taskRecord.Equipment.EquipmentName }));
                formattedBodyMiddle.AppendLine("<br />");

                if (!String.IsNullOrWhiteSpace(taskRecord.Description))
                    formattedBodyMiddle.AppendLine(FormatString(emailItemBlock, new Object[] { PropHelper.G<TaskRecord>(_ => _.Description), taskRecord.Description }));

                if (!String.IsNullOrWhiteSpace(taskRecord.Recommendations))
                    formattedBodyMiddle.AppendLine(FormatString(emailItemBlock, new Object[] { PropHelper.G<TaskRecord>(_ => _.Recommendations), taskRecord.Recommendations }));

                if (!String.IsNullOrWhiteSpace(taskRecord.Actions))
                    formattedBodyMiddle.AppendLine(FormatString(emailItemBlock, new Object[] { PropHelper.G<TaskRecord>(_ => _.Actions), taskRecord.Actions }));

                return formattedBodyMiddle;
            }

            void SetCompletedTaskInfo(ModelTaskRecord taskRecord, StringBuilder formattedBodyMiddle)
            {
                var building = taskRecord.Building;

                var timeZone = building.TimeZone;

                var stateOrCountry = (building.StateID.HasValue) ? building.State.StateName : building.Country.CountryName;

                var formattedDate = DateTime.Parse
                (
                    new DateTimeHelper().ConvertTimeFromUTC(taskRecord.DateCompleted.Value, timeZone).ToString(),
                    CultureInfo.GetCultureInfo(taskRecord.User.LCID)
                );

                var hours = TimeZoneInfo.FindSystemTimeZoneById(timeZone).BaseUtcOffset.Hours;

                var displayDate = $"{formattedDate.ToString("g")} (UTC Offset: {hours}, Time Zone: {timeZone}, State/Country: {stateOrCountry})";

                formattedBodyMiddle.AppendLine(FormatString(emailItem, new Object[] { Regex.Replace(PropHelper.G<TaskRecord>(_ => _.DateCompleted), "(\\B[A-Z])", " $1"), displayDate }));
            }

            string FormatString(string formatMsg, params object[] args) => string.Format(formatMsg, args);

            GetTasks ConvertToGetTasksModel(ModelTaskRecord mtr)
            {
                return new GetTasks
                {
                   Cid = mtr.Cid,
                   Bid = mtr.Bid,
                   Eid = mtr.Eid,
                   Aid = mtr.Aid,
                   AnalysisStartDate = mtr.AnalysisStartDate,
                   TaskID = mtr.TaskId,                     
                };
            }

        #endregion
    }
}