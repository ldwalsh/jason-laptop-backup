﻿using CW.CMMSIntegration;
using CW.CMMSIntegration.WorkOrderSystems.Factories;
using CW.CMMSIntegration.WorkOrderSystems.Interfaces;
using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Data;
using CW.Data.Models;
using CW.Logging;
using CW.Website._controls.tasks.Interfaces;
using System.Configuration;

namespace CW.Website._controls.tasks.Services
{
    public class TaskRecordDataTransformService : ITaskRecordDataTransformService
    {
        #region field(s)

        IWorkOrderRepository _repo;

        #endregion

        #region constructor(s)

            public TaskRecordDataTransformService(ISectionLogManager logger)
            {
                var connStr = ConfigurationManager.ConnectionStrings["CWConnectionString"].ConnectionString;

                _repo = new WorkOrderRepositoryFactory(logger).GetWorkOrderRepository(connStr);
            }

        #endregion

        #region interface method(s)

            public void TransformCreateData(ModelTaskRecord modelTaskRecord)
            {
                //this needs to execute first, so it's available when populating the linq to sql object below
                _repo.SetNextClientTaskId(modelTaskRecord);
            
                var linqTaskRecord = TransformCreateToLinqObject(modelTaskRecord);
            
                var newTaskId = _repo.InsertTaskRecord(linqTaskRecord);

                var newTaskRecord = _repo.GetTaskRecord(newTaskId);
            
                UpdateModelObject(newTaskRecord, modelTaskRecord);    
            }

            public ModelTaskRecord TransformEditDataForUI(TaskRecord taskRecord)
            {
                return new ModelTaskRecord()
                {
                    TaskId = taskRecord.TaskID,

                    ClientTaskId = taskRecord.ClientTaskID,

                    Bid = taskRecord.Analyses_Equipment.Equipment.BID,

                    Eid = taskRecord.Analyses_Equipment.EID,

                    Aid = taskRecord.Analyses_Equipment.AID,

                    AnalysisStartDate = taskRecord.AnalysisStartDate,

                    ReporterUid = taskRecord.ReporterUID,

                    AssignedUid = taskRecord.AssignedUID,

                    User1 = taskRecord.User1,

                    StatusId = taskRecord.Status.StatusID,

                    DateCompleted = taskRecord.DateCompleted,

                    TimeZone = taskRecord.Analyses_Equipment.Equipment.Building.TimeZone,

                    Summary = taskRecord.Summary,

                    Description = taskRecord.Description,

                    Recommendations = taskRecord.Recommendations,

                    Actions = taskRecord.Actions,

                    WorkOrderTypeId = taskRecord.WorkTypeID,

                    Building = taskRecord.Analyses_Equipment.Equipment.Building,

                    Equipment = taskRecord.Analyses_Equipment.Equipment,

                    Analyse = taskRecord.Analyses_Equipment.Analyse,

                    ReporterEmail = (taskRecord.User != null) ? taskRecord.User.Email : "No reporter.",

                    SecondaryEmail = (taskRecord.User2 != null) ? taskRecord.User2.Email : "Unknown.",

                    Interval = taskRecord.Interval,

                    WorkOrderId = taskRecord.WorkOrderID,

                    AnnualAvoidableCost = taskRecord.AnnualAvoidableCost,

                    AnnualAvoidableCoolingUse = taskRecord.AnnualAvoidableCoolingUse,

                    AnnualAvoidableHeatingUse = taskRecord.AnnualAvoidableHeatingUse,

                    AnnualAvoidableElectricUse = taskRecord.AnnualAvoidableElectricUse,

                    DateCreated = taskRecord.DateCreated,

                    DateModified = taskRecord.DateModified
                };
            }

            public void TranformEditData(ModelTaskRecord modelTaskRecord)
            {
                _repo.UpdateTaskRecord(modelTaskRecord);

                var updatedTaskRecord = _repo.GetTaskRecord(modelTaskRecord.TaskId);

                UpdateModelObject(updatedTaskRecord, modelTaskRecord);
            }
            
            public GetTasks TransformDataForEmail(ModelTaskRecord modelTaskRecord)
            {
                var taskRecord = new GetTasks();

                taskRecord.Cid = modelTaskRecord.Cid;

                taskRecord.Bid = modelTaskRecord.Bid;

                taskRecord.EquipmentClassId = modelTaskRecord.EquipmentClass.EquipmentClassID;

                taskRecord.Eid = modelTaskRecord.Eid;

                taskRecord.Aid = modelTaskRecord.Aid;

                taskRecord.AnalysisRange = modelTaskRecord.Interval;

                taskRecord.AnalysisStartDate = modelTaskRecord.AnalysisStartDate;

                return taskRecord;
            }

            public WoTask TransformWorkOrderData(ModelTaskRecord modelTaskRecord)
            {
                var equipment = _repo.GetEquipment(modelTaskRecord.Eid);

                return new WoTask
                {
                    VendorId = _repo.GetSettings(modelTaskRecord.Bid).VendorId,

                    Actions = modelTaskRecord.Actions,

                    Equipment = new EquipmentWrapper(equipment),

                    Description = modelTaskRecord.Description,

                    Recommendations = modelTaskRecord.Recommendations,

                    Summary = modelTaskRecord.Summary,

                    Category = modelTaskRecord.WorkOrderCategory
                };    
            }

            public ModelTaskRecord TransformTaskRecordDetailsData(TaskRecord taskRecord)
            {
                return new ModelTaskRecord()
                {
                    Bid = taskRecord.Analyses_Equipment.Equipment.BID,

                    Summary = taskRecord.Summary,

                    Building = taskRecord.Analyses_Equipment.Equipment.Building,

                    Equipment = taskRecord.Analyses_Equipment.Equipment,

                    Analyse = taskRecord.Analyses_Equipment.Analyse,

                    AnalysisStartDate = taskRecord.AnalysisStartDate,

                    Interval = taskRecord.Interval,

                    ReporterEmail = (taskRecord.User != null) ? taskRecord.User.Email : "No reporter.",

                    AssigneeEmail = (taskRecord.User1 != null) ? taskRecord.User1.Email : "No assignee.",

                    Status = taskRecord.Status,

                    DateCompleted = taskRecord.DateCompleted,

                    Description = taskRecord.Description,

                    Recommendations = taskRecord.Recommendations,

                    Actions = taskRecord.Actions,

                    AnnualAvoidableCost = taskRecord.AnnualAvoidableCost,

                    AnnualAvoidableCoolingUse = taskRecord.AnnualAvoidableCoolingUse,

                    AnnualAvoidableElectricUse = taskRecord.AnnualAvoidableElectricUse,

                    AnnualAvoidableHeatingUse = taskRecord.AnnualAvoidableHeatingUse,

                    TimeZone = taskRecord.Analyses_Equipment.Equipment.Building.TimeZone,

                    SecondaryEmail = (taskRecord.User2 != null) ? taskRecord.User2.Email : "Unknown.",

                    DateCreated = taskRecord.DateCreated,

                    DateModified = taskRecord.DateModified,

                    WorkOrderType = taskRecord.WorkType.WorkType1,

                    WorkOrderId = taskRecord.WorkOrderID
                };
            }

        #endregion

        #region method(s)

            TaskRecord TransformCreateToLinqObject(ModelTaskRecord modelTaskRecord)
            {
                var uid = modelTaskRecord.Uid;

                return new TaskRecord()
                {
                    ClientTaskID = modelTaskRecord.ClientTaskId,

                    AEID = modelTaskRecord.Aeid,

                    Summary = modelTaskRecord.Summary,

                    ReporterUID = uid,

                    AssignedUID = modelTaskRecord.AssignedUid,

                    AnalysisStartDate = modelTaskRecord.AnalysisStartDate,

                    Interval = modelTaskRecord.Interval,

                    Description = modelTaskRecord.Description,

                    Recommendations = modelTaskRecord.Recommendations,

                    Actions = modelTaskRecord.Actions,

                    LastModifiedUID = uid,

                    StatusID = modelTaskRecord.StatusId,

                    DateCompleted = modelTaskRecord.DateCompleted,

                    DateModified = modelTaskRecord.DateModified,

                    DateCreated = modelTaskRecord.DateCreated,

                    CID = modelTaskRecord.Cid,

                    WorkTypeID = modelTaskRecord.WorkOrderTypeId,

                    AnnualAvoidableCost = modelTaskRecord.AnnualAvoidableCost,

                    AnnualAvoidableCoolingUse = modelTaskRecord.AnnualAvoidableCoolingUse,

                    AnnualAvoidableElectricUse = modelTaskRecord.AnnualAvoidableElectricUse,

                    AnnualAvoidableHeatingUse = modelTaskRecord.AnnualAvoidableHeatingUse
                };
            }

            TaskRecord TransformEditToLinqObject(ModelTaskRecord modelTaskRecord)
            {
                return new TaskRecord()
                {
                    TaskID = modelTaskRecord.TaskId,

                    AssignedUID = modelTaskRecord.AssignedUid,

                    StatusID = modelTaskRecord.StatusId,

                    DateCompleted = modelTaskRecord.DateCompleted,

                    Description = modelTaskRecord.Description,

                    Recommendations = modelTaskRecord.Recommendations,

                    Actions = modelTaskRecord.Actions,

                    AnnualAvoidableCost = modelTaskRecord.AnnualAvoidableCost,

                    AnnualAvoidableHeatingUse = modelTaskRecord.AnnualAvoidableHeatingUse,

                    AnnualAvoidableCoolingUse = modelTaskRecord.AnnualAvoidableCoolingUse,

                    AnnualAvoidableElectricUse = modelTaskRecord.AnnualAvoidableElectricUse,

                    DateModified = modelTaskRecord.DateModified,

                    LastModifiedUID = modelTaskRecord.LastModifiedUid
                };
            }

            void UpdateModelObject(TaskRecord newTaskRecord, ModelTaskRecord modelTaskRecord)
            {
                modelTaskRecord.TaskId = newTaskRecord.TaskID;

                modelTaskRecord.User = newTaskRecord.User;

                modelTaskRecord.User1 = newTaskRecord.User1;

                modelTaskRecord.Client = newTaskRecord.Client;

                modelTaskRecord.Building = newTaskRecord.Analyses_Equipment.Equipment.Building;

                modelTaskRecord.Equipment = newTaskRecord.Analyses_Equipment.Equipment;

                modelTaskRecord.EquipmentClass = newTaskRecord.Analyses_Equipment.Equipment.EquipmentType.EquipmentClass;

                modelTaskRecord.State = newTaskRecord.Analyses_Equipment.Equipment.Building.State;

                modelTaskRecord.Country = newTaskRecord.Analyses_Equipment.Equipment.Building.Country;
            }

        #endregion
    }
}