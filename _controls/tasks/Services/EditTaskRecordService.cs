﻿using CW.CMMSIntegration.WorkOrderSystems.Factories;
using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.PointType;
using CW.Logging;
using CW.Utility;
using CW.Website._controls.tasks.Factories;
using CW.Website._controls.tasks.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.tasks.Services
{
    public class EditTaskRecordService : IEditTaskRecordService
    {
        #region properties
        
            ISectionLogManager LogMgr { get; set; }
        #endregion

        #region field(s)

        ITaskRecordDataTransformService _dtsSvc;

            ITaskRecordEmailService _emailSvc;

            ITaskRecordRepository _repo;

            int[] _pointTypeIds = new int[]
            {
                BusinessConstants.PointType.AnnualCoolingUsePointTypeID,

                BusinessConstants.PointType.AnnualElectricUsePointTypeID,

                BusinessConstants.PointType.AnnualHeatingUsePointTypeID
            };

            IEnumerable<GetPointTypeData> _engUnits;

            CultureInfo _userCulture;

        #endregion

        #region constructor(s)

            public EditTaskRecordService(ITaskRecordDataTransformService dtsSvc, ITaskRecordEmailService emailSrv, ISectionLogManager logger)
            {
                LogMgr = logger;

                _dtsSvc = dtsSvc;

                _emailSvc = emailSrv;

                _repo = new TaskRecordRepositoryFactory().GetTaskRecordRepository();

                _engUnits = _repo.GetEngUnits(_pointTypeIds);
            }

        #endregion

        #region interface method(s)

            public ModelTaskRecord SetModelTaskRecord(TaskRecord taskRecord) => _dtsSvc.TransformEditDataForUI(taskRecord);
        
            public void SetMetricFieldsToUI(ModelTaskRecord modelTaskRecord, Label[] metricLabels, TextBox[] metricFields)
            {
                var buildingSettings = _repo.GetBuildingSettings(modelTaskRecord);

                var unitSystem = buildingSettings.UnitSystem;

                var taskMetrics = new string[]
                {
                    modelTaskRecord.AnnualAvoidableCoolingUse,
                    modelTaskRecord.AnnualAvoidableElectricUse,
                    modelTaskRecord.AnnualAvoidableHeatingUse,
                };

                _userCulture = new CultureInfo(modelTaskRecord.CultureName);
            
                //true = get invariant value (from table column value) and convert to user culture
                metricFields[0].Text = GetValueIfExists(modelTaskRecord.AnnualAvoidableCost, true);

                metricLabels[0].Text = new CultureInfo(buildingSettings.LCID).NumberFormat.CurrencySymbol;

                for (var i = 1; i <= taskMetrics.Length; i++)
                {
                    var taskMetric = taskMetrics[i - 1];

                    metricFields[i].Text = GetValueIfExists(taskMetric, true);

                    metricLabels[i].Text = GetEngUnit(taskMetric, _pointTypeIds[i - 1], unitSystem);
                }
            }

            public void SetMetricFieldsToTaskRecord(ModelTaskRecord modelTaskRecord, TextBox[] metricFields)
            {
                if (modelTaskRecord.RecalculateMetrics)
                {
                    var vDataMetrics = new VDataMetrics(modelTaskRecord);

                    var calcMetrics = vDataMetrics.ObtainMetricsAndCalculateSavings();

                    vDataMetrics.SetMetricsForTaskRecord(calcMetrics);

                    return;
                }

                //convert the user culturized value to invariant before setting table value
                modelTaskRecord.AnnualAvoidableCost = GetValueIfExists(metricFields[0].Text.Trim());

                modelTaskRecord.AnnualAvoidableCoolingUse = GetValueIfExists(metricFields[1].Text.Trim());

                modelTaskRecord.AnnualAvoidableHeatingUse = GetValueIfExists(metricFields[2].Text.Trim());

                modelTaskRecord.AnnualAvoidableElectricUse = GetValueIfExists(metricFields[3].Text.Trim());
            }

            public string GetFormattedString(ModelTaskRecord modelTaskRecord, ModelTaskRecord.DateType dateType)
            {
                var timeZone = modelTaskRecord.TimeZone;

                var offset = TimeZoneInfo.FindSystemTimeZoneById(timeZone).BaseUtcOffset.Hours;

                var isCreatedOrCompleted = (dateType == ModelTaskRecord.DateType.CreatedOrCompleted);

                var date = (isCreatedOrCompleted) ? modelTaskRecord.DateCreated : modelTaskRecord.DateModified;

                var convertedDate = DateTimeHelper.GetConvertedDateInGeneralFormat(date, timeZone);

                var email = modelTaskRecord.SecondaryEmail;

                return (isCreatedOrCompleted) ? $"{convertedDate}, (UTC Offset: {offset}, Time Zone: {timeZone})"
                                              : $"{email}, {convertedDate} (UTC Offset: {offset}, Time Zone: {timeZone})";
            }

            public void RunEditTaskRecordService(ModelTaskRecord modelTaskRecord)
            {
                _dtsSvc.TranformEditData(modelTaskRecord);

                if (modelTaskRecord.CreateWorkOrder)
                {
                    var woTask = _dtsSvc.TransformWorkOrderData(modelTaskRecord);

                    //the task record is being passed to the service in case there is an error. We need to bubble up the message to the model
                    new WorkOrderServiceFactory(LogMgr).GetWorkOrderService(woTask, modelTaskRecord).RunWorkOrderService();
                }

                if (modelTaskRecord.SendEmail) _emailSvc.Send(modelTaskRecord);               
            }

        #endregion

        #region method(s)

            string GetValueIfExists(string item, bool isInvariant = false)
            {
                if (string.IsNullOrWhiteSpace(item)) return null;

                var invariantCulture = CultureInfo.InvariantCulture;

                var parsedItem = double.Parse(item, isInvariant ? invariantCulture : _userCulture);

                return isInvariant ? parsedItem.ToString("n0", _userCulture) : parsedItem.ToString(invariantCulture);
            }

            string GetEngUnit(string item, int pointTypeId, bool isUnitSystem)
            {
                var engUnit = _engUnits.Single(eu => eu.PointTypeID == pointTypeId);

                return (isUnitSystem) ? engUnit.SIEngUnits : engUnit.IPEngUnits;
            }

        #endregion
    }
}