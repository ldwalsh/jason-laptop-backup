﻿using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.tasks
{
    public sealed class TaskDropDowns : SiteUserControl
    {
        #region properties

            public Int32? TaskBid { get; set; }

        #endregion

        #region methods

            public void BindStatusDDL(DropDownList ddl, Boolean includeViewAll = false, Boolean includeDefault = true)
            {
                var statuses = new StatusQuery(DataMgr.ConnectionString, siteUser).FinalizeLoadWith.GetAll().ByTaskStatus().ToList();

                BindDDL(ddl, PropHelper.G<Status>(_ => _.StatusName), PropHelper.G<Status>(_ => _.StatusID), statuses, includeViewAll, includeDefault);
            }

            public void BindAssigneeDDL(DropDownList ddl, Boolean includeViewAll = false, Boolean includeDefault = true, Boolean alwaysRebind = false)
            {
                var restrictedUserRoleID = Convert.ToInt32(BusinessConstants.UserRole.UserRoleEnum.RestrictedUser);
                var users = (siteUser.IsLoggedInUnderProviderClient) ? DataMgr.UserDataMapper.GetAllUsersWithMinimumRoleByProvider(siteUser.CID, siteUser.ProviderID.Value, restrictedUserRoleID, TaskBid)
                                                                     : DataMgr.UserDataMapper.GetUsersClientsUsersWithMinimumRole(siteUser.CID, restrictedUserRoleID, TaskBid);

                BindDDL(ddl, PropHelper.G<User>(_ => _.Email), PropHelper.G<User>(_ => _.UID), users.OrderBy(e => e.Email), includeViewAll, includeDefault, alwaysRebind);
            }

            public void BindDDL(DropDownList ddl, String textField, String valueField, IEnumerable<Object> data, Boolean includeViewAll = false, Boolean includeDefault = true, Boolean alwaysRebind = false)
            {
                if (ddl.Items.Count <= 1 || alwaysRebind)
                {
                    ddl.AppendDataBoundItems = true;

                    DropDownSequencer.ClearAndCreateDefaultItems(ddl, includeDefault, includeViewAll, false);

                    if (data.Any())
                    {
                        ddl.DataTextField = textField;
                        ddl.DataValueField = valueField;
                        ddl.DataSource = data;
                        ddl.DataBind();
                    }
                }
            }

        #endregion
    }
}