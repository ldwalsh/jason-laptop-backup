﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="ViewTasks.ascx.cs" Inherits="CW.Website._controls.tasks.ViewTasks" %>
<%@ Register tagPrefix="CW" tagName="TaskDownloadArea" src="~/_controls/Tasks/TaskDownloadArea.ascx" %>
<%@ Register TagPrefix="CW" TagName="TaskEditForm" src="~/_controls/tasks/TaskEditForm.ascx" %>
<%@ Register TagPrefix="CW" TagName="TaskDetailsView" src="~/_controls/tasks/TaskDetailsView.ascx" %>
<%@ Register TagPrefix="telerik" Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Data" %>
<%@ Import Namespace="CW.Utility" %>

<div class="taskDownloadArea">
  <CW:TaskDownloadArea runat="server" ID="TaskDownloadArea" />
</div>

<p class="taskMessagesLayout">
  <a id="lnkSetFocus" href="#" runat="server" />
  <asp:Label ID="lblResults" runat="server" CssClass="successMessage" />
  <asp:Label ID="lblError" runat="server" CssClass="errorMessage" />
</p>

<div>
  <telerik:RadGrid ID="radGrid" runat="server">
    <MasterTableView>
      <Columns>

        <telerik:GridTemplateColumn Uniquename="Buttons1" ItemStyle-CssClass="gridAlignCenter">
          <ItemTemplate>
            <asp:ImageButton runat="server" CommandName="Select" ImageUrl="~/_assets/images/select.png" />
            <asp:ImageButton runat="server" CommandName="Edit" ImageUrl="~/_assets/images/edit.png" />
          </ItemTemplate>
        </telerik:GridTemplateColumn>

        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="gridAlignCenter" UniqueName="ClientTaskID">
          <ItemTemplate><asp:Label runat="server" /><label><%# Eval(PropHelper.G<TaskRecord>(_ => _.ClientTaskID)) %></label></ItemTemplate>
        </telerik:GridTemplateColumn>

        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" UniqueName="BuildingName" DataField="TaskID">
          <ItemTemplate><label title="<%# Eval(PropHelper.G<Building>(_ => _.BuildingName)) %>"><%# TrimCellContents(Eval(PropHelper.G<Building>(_ => _.BuildingName)).ToString(), 14) %></label></ItemTemplate>
        </telerik:GridTemplateColumn>

        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" UniqueName="EquipmentName">
          <ItemTemplate><label title="<%# Eval(PropHelper.G<Equipment>(_ => _.EquipmentName)) %>"><%# TrimCellContents(Eval(PropHelper.G<Equipment>(_ => _.EquipmentName)).ToString(), 16) %></label></ItemTemplate>
        </telerik:GridTemplateColumn>

        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="tasksLayout640HiddenColumn" ItemStyle-CssClass="tasksLayout640HiddenColumn" FooterStyle-CssClass="tasksLayout640HiddenColumn" UniqueName="Email">
          <ItemTemplate><label title="<%# Eval(PropHelper.G<User>(_ => _.Email)) %>"><%# TrimCellContents(Eval(PropHelper.G<User>(_ => _.Email)).ToString(), 16) %></label></ItemTemplate>
        </telerik:GridTemplateColumn>

        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" UniqueName="Summary">
          <ItemTemplate><label title="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Summary)) %>"><%# TrimCellContents(Eval(PropHelper.G<TaskRecord>(_ => _.Summary)).ToString(), 30) %></label></ItemTemplate>
        </telerik:GridTemplateColumn>

        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="tasksLayout960HiddenColumn" ItemStyle-CssClass="tasksLayout960HiddenColumn" FooterStyle-CssClass="tasksLayout960HiddenColumn" UniqueName="Description">
          <ItemTemplate>
            <div runat="server">
            <asp:HyperLink ToolTip="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Description)) %>" runat="server"><asp:Label runat="server" /></asp:HyperLink>
            <ajaxToolkit:CollapsiblePanelExtender ID="cpeDescription" runat="Server" />
            <asp:Panel runat="server" CssClass="taskCpPadding">
              <label title="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Description)) %>"><%# TrimCellContents(Eval(PropHelper.G<TaskRecord>(_ => _.Description)).ToString(), 500) %></label>
            </asp:Panel>
            </div>
            <div runat="server">
            <asp:HyperLink ToolTip="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Recommendations)) %>" runat="server"><asp:Label runat="server" /></asp:HyperLink>
            <ajaxToolkit:CollapsiblePanelExtender ID="cpeRecommendations" runat="Server" />
            <asp:Panel runat="server" CssClass="taskCpPadding">
              <label title="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Recommendations)) %>"><%# TrimCellContents(Eval(PropHelper.G<TaskRecord>(_ => _.Recommendations)).ToString(), 500) %></label>
            </asp:Panel>
            </div>
            <div runat="server">
            <asp:HyperLink ToolTip="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Actions)) %>" runat="server"><asp:Label runat="server" /></asp:HyperLink>
            <ajaxToolkit:CollapsiblePanelExtender ID="cpeActions" runat="Server" />
            <asp:Panel runat="server" CssClass="taskCpPadding">
              <label title="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Actions)) %>"><%# TrimCellContents(Eval(PropHelper.G<TaskRecord>(_ => _.Actions)).ToString(), 500) %></label>
            </asp:Panel>
            </div>
          </ItemTemplate>              
        </telerik:GridTemplateColumn>

<%--    <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="tasksLayout960HiddenColumn" ItemStyle-CssClass="tasksLayout960HiddenColumn" FooterStyle-CssClass="tasksLayout960HiddenColumn" UniqueName="Recommendations">
          <ItemTemplate>
            <asp:HyperLink ToolTip="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Recommendations)) %>" runat="server"><asp:Label runat="server" /></asp:HyperLink>
            <ajaxToolkit:CollapsiblePanelExtender ID="cpeRecommendations" runat="Server" />
            <asp:Panel runat="server">
              <label title="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Recommendations)) %>"><%# TrimCellContents(Eval(PropHelper.G<TaskRecord>(_ => _.Recommendations)).ToString(), 500) %></label>
            </asp:Panel>
          </ItemTemplate>              
        </telerik:GridTemplateColumn>--%>

<%--        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="tasksLayout960HiddenColumn" ItemStyle-CssClass="tasksLayout960HiddenColumn" FooterStyle-CssClass="tasksLayout960HiddenColumn" UniqueName="Actions">
          <ItemTemplate>
            <asp:HyperLink ToolTip="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Actions)) %>" runat="server"><asp:Label runat="server" /></asp:HyperLink>
            <ajaxToolkit:CollapsiblePanelExtender ID="cpeActions" runat="Server" />
            <asp:Panel runat="server">
              <label title="<%# Eval(PropHelper.G<TaskRecord>(_ => _.Actions)) %>"><%# TrimCellContents(Eval(PropHelper.G<TaskRecord>(_ => _.Actions)).ToString(), 500) %></label>
            </asp:Panel>    
          </ItemTemplate>
        </telerik:GridTemplateColumn>--%>

        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" UniqueName="AnalysisStartDate" DataType="System.DateTime">
          <ItemTemplate><asp:Label runat="server" /></ItemTemplate>
        </telerik:GridTemplateColumn>

        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" Uniquename="AnnualAvoidableCostInt" DataType="System.Int32" DataField="AnnualAvoidableCostInt">
          <ItemTemplate><asp:Label runat="server" /></ItemTemplate>
        </telerik:GridTemplateColumn>
          
        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" Uniquename="CurrencyCode">
          <ItemTemplate><asp:Label runat="server" /><asp:HiddenField runat="server" Value='<%# Eval(PropHelper.G<Building>(_ => _.BID)) %>' /></ItemTemplate>
        </telerik:GridTemplateColumn>
        
        <telerik:GridTemplateColumn HeaderStyle-HorizontalAlign="Center" Uniquename="StatusName">
          <ItemTemplate><label><%# Eval(PropHelper.G<TaskRecord>(_ => _.Status.StatusName)) %></label></ItemTemplate>
        </telerik:GridTemplateColumn>

        <telerik:GridTemplateColumn Uniquename="Buttons2" ItemStyle-CssClass="gridAlignCenter">
          <ItemTemplate>
            <img runat="server" src="../../_assets/styles/images/diagnostics.png" class="imgDiagnosticsLink" alt="View Diagnostic" title="View Diagnostic" />
            <asp:ImageButton UniqueName="Delete" runat="server" CommandName="Delete" ImageUrl="~/_assets/images/delete.png" OnClientClick="javascript:if(!confirm('Are you sure you wish to delete this task permanently?')){return false;}" />
          </ItemTemplate>
        </telerik:GridTemplateColumn>

      </Columns>
    </MasterTableView>
  </telerik:RadGrid>
</div>
<br />
<asp:Panel ID="pnlDetails" runat="server" Visible="false">
  <CW:TaskDetailsView ID="TaskDetailsView" runat="server" />
</asp:Panel>

<asp:Panel ID="pnlEdit" runat="server" Visible="false">
  <CW:TaskEditForm ID="TaskEditForm" runat="server" />
</asp:Panel>