﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="CMMSTaskFields.ascx.cs" Inherits="CW.Website._controls.tasks.CMMSTaskFields" %>

<div id="divGenerateWO" runat="server" visible="false">
  <label id="lblGenerateWO" runat="server">Generate Work Order:</label>
  <asp:CheckBox ID="chkGenerateWO" runat="server" Checked="true" />
</div>

<div id="divWOType" runat="server" visible="false">
  <label id="lblWOType" runat="server">Work Type:</label>
  <asp:DropDownList ID="ddlWorkOrderType" runat="server" AppendDataBoundItems="true" />
</div>

<div id="divWOCategory" runat="server" visible="false">
  <label id="lblCategory" runat="server">Category:</label>
  <asp:DropDownList ID="ddlWorkOrderCategory" runat="server" AppendDataBoundItems="true" />
</div>