﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace CW.Website._controls.tasks {
    
    
    public partial class TaskDetailsView {
        
        /// <summary>
        /// ulSummary control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulSummary;
        
        /// <summary>
        /// ulBuildingName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulBuildingName;
        
        /// <summary>
        /// ulEquipmentName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulEquipmentName;
        
        /// <summary>
        /// ulAnalysisName control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulAnalysisName;
        
        /// <summary>
        /// ulAnalysisDate control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulAnalysisDate;
        
        /// <summary>
        /// ulInterval control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulInterval;
        
        /// <summary>
        /// ulReporter control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulReporter;
        
        /// <summary>
        /// ulAssigned control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulAssigned;
        
        /// <summary>
        /// ulStatus control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulStatus;
        
        /// <summary>
        /// ulDateCompleted control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulDateCompleted;
        
        /// <summary>
        /// lnkSetFocus control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlAnchor lnkSetFocus;
        
        /// <summary>
        /// ulDescription control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulDescription;
        
        /// <summary>
        /// ulRecommandations control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulRecommandations;
        
        /// <summary>
        /// ulActions control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulActions;
        
        /// <summary>
        /// ulWorkOrderType control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulWorkOrderType;
        
        /// <summary>
        /// ulWorkOrderNum control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulWorkOrderNum;
        
        /// <summary>
        /// ulAnnualAvoidableCost control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulAnnualAvoidableCost;
        
        /// <summary>
        /// ulAnnualAvoidableCoolingUse control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulAnnualAvoidableCoolingUse;
        
        /// <summary>
        /// ulAnnualAvoidableElectricUse control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulAnnualAvoidableElectricUse;
        
        /// <summary>
        /// ulAnnualAvoidableHeatingUse control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulAnnualAvoidableHeatingUse;
        
        /// <summary>
        /// ulDateCreated control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulDateCreated;
        
        /// <summary>
        /// ulLastModified control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::CW.Website._controls.UListItem ulLastModified;
    }
}
