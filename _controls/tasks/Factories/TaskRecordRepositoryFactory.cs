﻿using CW.Website._controls.tasks.Interfaces;
using CW.Website._controls.tasks.Repositories;
using System.Configuration;

namespace CW.Website._controls.tasks.Factories
{
    public class TaskRecordRepositoryFactory
    {
        #region method(s)

            public ITaskRecordRepository GetTaskRecordRepository()
            {
                var connStr = ConfigurationManager.ConnectionStrings["CWConnectionString"].ConnectionString;

                return new TaskRecordRepository(connStr);
            }

        #endregion
    }
}