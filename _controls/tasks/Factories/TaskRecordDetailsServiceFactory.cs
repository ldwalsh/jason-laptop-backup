﻿using CW.Data;
using CW.Logging;
using CW.Website._controls.tasks.Interfaces;
using CW.Website._controls.tasks.Services;

namespace CW.Website._controls.tasks.Factories
{
    public class TaskRecordDetailsServiceFactory
    {
        
        private ISectionLogManager LogMgr { get; set; }

        #region constructor
        
            public TaskRecordDetailsServiceFactory(ISectionLogManager logger)
            {
                LogMgr = logger;
            }
        
        #endregion

        #region method(s)

            public ITaskRecordDetailsService GetTaskRecordDetailsService(TaskRecord taskRecord, string culture)
            {
                return new TaskRecordDetailsService(taskRecord, culture, LogMgr);
            }
            
        #endregion
    }
}