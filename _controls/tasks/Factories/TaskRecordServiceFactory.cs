﻿using CW.Logging;
using CW.Website._controls.tasks.Interfaces;
using CW.Website._controls.tasks.Services;

namespace CW.Website._controls.tasks.Factories
{
    public class TaskRecordServiceFactory
    {
        #region properties
        
            ISectionLogManager LogMgr { get; set; }
        #endregion

        #region field(s)

        ITaskRecordDataTransformService dts;

            ITaskRecordEmailService emailSvc;

        #endregion

        #region constructor(s)

            public TaskRecordServiceFactory(ISectionLogManager logger)
            {
                LogMgr = logger;

                dts = new TaskRecordDataTransformService(logger);

                emailSvc = new TaskRecordEmailService(dts);
            }

        #endregion

        #region method(s)

            public ICreateTaskRecordService GetCreateTaskRecordService() => new CreateTaskRecordService(dts, emailSvc, LogMgr);

            public IEditTaskRecordService GetEditTaskRecordService() => new EditTaskRecordService(dts, emailSvc, LogMgr);
            
        #endregion
    }
}