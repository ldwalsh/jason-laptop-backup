﻿using AjaxControlToolkit;
using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Data;
using CW.Website._controls.tasks.Models;
using CW.Website._framework;
using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._controls.tasks
{
    public abstract class TaskForm : SiteUserControl
    {

        #region const(s)

            protected const string viewStateName = nameof(ViewModelTaskForm);

        #endregion

        #region field(s)

            protected Label lblWorkOrderAdded;

            protected Label lblWorkOrderError;

            protected Label lblEmailSent;

            protected Label lblEmailError;

        #endregion

        #region properties

            protected string ValidationGroup { get; set; }

            protected LinkButton LinkButton { get; set; }

            protected TaskRecord TaskRecord { get; set; }

        #endregion

        #region methods

            protected void SetFormFieldsAttributes()
            {
                foreach (var ddl in FindControls<DropDownList>())
                {
                    ddl.CssClass = "dropdown";

                    ddl.AppendDataBoundItems = true;
                }

                foreach (var lbl in FindControls<Label>())
                {
                    lbl.CssClass = "labelContent";

                    var lblId = lbl.ID;

                    if (lblId.Contains("Error")) lbl.CssClass = "errorMessage";

                    if (lblId.Contains("Added") || lblId.Contains("Updated") || lblId.Contains("Sent")) lbl.CssClass = "successMessage";
                }

                foreach (var tb in FindControls<TextBox>())
                {
                    tb.CssClass = "textbox";
                }

                foreach (var ta in FindControls<HtmlTextArea>())
                {
                    ta.Cols = 40;

                    ta.Rows = 5;
                }

                LinkButton.ValidationGroup = ValidationGroup;
            }

            protected void SetValidatorAttributes()
            {
                foreach (var v in FindControls<RequiredFieldValidator>())
                {
                    var controlToValidate = FindControl(v.ID.Split('_').First());

                    v.Display = ValidatorDisplay.None;

                    v.SetFocusOnError = true;

                    if (controlToValidate.GetType() == typeof(DropDownList)) v.InitialValue = "-1";

                    v.ValidationGroup = ValidationGroup;

                    v.ControlToValidate = controlToValidate.ID;

                    v.ErrorMessage = $"{controlToValidate.ID.Substring(3, controlToValidate.ID.Length - 3)} is a required field.";
                }

                foreach (var cv in FindControls<CustomValidator>())
                {
                    var controlToValidate = FindControl(cv.ID.Split('_').First());

                    cv.Display = ValidatorDisplay.None;

                    cv.ValidationGroup = ValidationGroup;

                    cv.ControlToValidate = controlToValidate.ID;

                    cv.ClientValidationFunction = "validateNumber";

                    cv.ErrorMessage = "Please use a whole number with thousands separator. No decimal.";
                }

                foreach (var ve in FindControls<ValidatorCalloutExtender>())
                {
                    ve.TargetControlID = ve.ID.Split(new[] { "Extender", }, StringSplitOptions.None)[0];

                    ve.BehaviorID = ve.ID;

                    ve.HighlightCssClass = "validatorCalloutHighlight";

                    ve.Width = Unit.Pixel(175);
                }
            }

            protected void PersistFormFields(ViewModelTaskForm formFields) => ViewState[viewStateName] = formFields;

            protected void ClearFormFields() => ViewState[viewStateName] = null;

            protected void ClearMessageLabels()
            {
                var labels = new Label[] { lblWorkOrderAdded, lblWorkOrderError, lblEmailSent, lblEmailError };

                foreach (var label in labels)
                    label.Text = string.Empty;
            }

            protected void UpdateLabels(Label lblTask, ModelTaskRecord model, CMMSTaskFields cmmsFields = null)
            {
                lblTask.Text = model.TaskMessage;

                SetWorkOrderMessage(lblTask, model, cmmsFields);

                SetEmailMessage(model);
            }

            void SetWorkOrderMessage(Label lblTask, ModelTaskRecord model, CMMSTaskFields cmmsFields)
            {
                var error = model.WorkOrderErrorMessage;

                if (!string.IsNullOrWhiteSpace(error))
                {
                    lblWorkOrderError.Text = $"<br /><br />{error}"; // CMMSError.Replace("{0}", CMMSTaskFields.ErrorMessage);

                    return;
                }

                if (model.WorkOrderCreated)
                {
                    lblWorkOrderAdded.Text = $"<br /><br />Work Order Generated, ID is {model.WorkOrderId}";

                    //web forms hack, we are keeping the "edit" form open, so we need to hide the work order fields after creation
                    if (lblTask.ID.Contains("Updated") && cmmsFields != null) cmmsFields.DivGenerateWO.Visible = false;
                }
            }

            void SetEmailMessage(ModelTaskRecord model)
            {
                var error = model.EmailFailedMessage;

                if (!string.IsNullOrWhiteSpace(error))
                {
                    lblEmailError.Text = $"<br /><br />{error}";

                    return;
                }

                if (model.EmailSent) lblEmailSent.Text = $"<br /><br />Email Sent To {model.User1.Email}";
            }

        #endregion
    }
}