﻿using CW.Business.Query;
using CW.CMMSIntegration.WorkOrderSystems.Models;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models;
using CW.Data.Models.PointType;
using CW.Reporting._fileservices;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._controls.tasks.Factories;
using CW.Website._controls.tasks.Services;
using CW.Website._downloads;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._controls.tasks
{
    public partial class TaskDownloadArea : DownloadAreaBase<GetTasks, TaskDownloadGenerator>
    {
        #region fields

            static string analysisStartDate = PropHelper.G<TaskRecord>(_ => _.AnalysisStartDate);

            string fnCid = PropHelper.G<Client>(_ => _.CID);
            string fnTaskId = PropHelper.G<TaskRecord>(_ => _.TaskID);
            string fnAnalysisStartDate = analysisStartDate;
            string fnAnalysisEndDate = analysisStartDate.Replace("Start", "End");
            string fnStatusId = PropHelper.G<Status>(_ => _.StatusID);
            string fnSearchFilter = "SearchFilter";
            string fnAssignedUid = PropHelper.G<TaskRecord>(_ => _.AssignedUID);
            string fnBid = PropHelper.G<Building>(_ => _.BID);
            string fnEquipmentClassId = PropHelper.G<EquipmentClass>(_ => _.EquipmentClassID);
            string fnEid = PropHelper.G<Equipment>(_ => _.EID);
            string fnAid = PropHelper.G<Analyse>(_ => _.AID);
            IEnumerable<GetPointTypeData> _engUnits;
            int annualCoolingUseId = BusinessConstants.PointType.AnnualCoolingUsePointTypeID;
            int annualElectricUseId = BusinessConstants.PointType.AnnualElectricUsePointTypeID;
            int annualHeatingUseId = BusinessConstants.PointType.AnnualHeatingUsePointTypeID;

        #endregion

        #region properties

            #region overrides

                protected override Func<Dictionary<string, object>, IEnumerable<GetTasks>> GetData => GetAllTaskByParameters;

                protected override Dictionary<string, object> Parameters
                {
                    get
                    {
                        var criteriaItems = Criteria.Split('|');

                        var items = new Dictionary<string, object>();
                       
                        items.Add(fnCid, siteUser.CID);
                        items.Add(PropHelper.G<Client>(_ => _.ClientName), siteUser.ClientName);
                        items.Add(fnTaskId, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.TaskId)]);
                        items.Add(fnAnalysisStartDate, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.AnalysisStartDate)]);
                        items.Add(fnAnalysisEndDate, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.AnalysisEndDate)]);
                        items.Add(fnStatusId, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.ProjectStatus)]);
                        items.Add(fnSearchFilter, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.SearchFilter)]);
                        items.Add(fnAssignedUid, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.AssigneeUid)]);
                        items.Add(fnBid, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.Bid)]);
                        items.Add(fnEquipmentClassId, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.EquipmentClassId)]);
                        items.Add(fnEid, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.Eid)]);
                        items.Add(fnAid, criteriaItems[Convert.ToInt32(ViewTasks.SearchCriteriaFieldDefs.Aid)]);

                        return items;
                    }
                }

                IDictionary<int, Tuple<int, bool>> BuildingSettingsDict { get; set; }
                IEnumerable<GetPointTypeData> EngUnits => _engUnits = _engUnits ?? DataMgr.PointTypeDataMapper.GetEngUnitsByPointTypeIDs(new int[] { annualCoolingUseId, annualElectricUseId, annualHeatingUseId });

            #endregion

            public Panel PnlEdit { private get; set; }

        #endregion

        #region methods

            #region overrides

                protected override void Download_Click(object sender, EventArgs ea)
                {
                    PnlEdit.Visible = false; //annoying hack due to us leaving the edit form open after editing a record.

                    base.Download_Click(sender, ea);
                }

            #endregion

            public IEnumerable<GetTasks> GetAllTaskByParameters(Dictionary<string, object> parameters)
            {
                var cid = Convert.ToInt32(parameters[fnCid]);
                var bid = (!string.IsNullOrEmpty(parameters[fnBid].ToString())) ? (int?)int.Parse(parameters[fnBid].ToString()) : null;
                var eid = (!string.IsNullOrEmpty(parameters[fnEid].ToString())) ? (int?)int.Parse(parameters[fnEid].ToString()) : null;
                var aid = (!string.IsNullOrEmpty(parameters[fnAid].ToString())) ? (int?)int.Parse(parameters[fnAid].ToString()) : null;
                var asd = DateTime.Parse(parameters[fnAnalysisStartDate].ToString());
                var aed = DateTime.Parse(parameters[fnAnalysisEndDate].ToString());
                var taskId = (!string.IsNullOrEmpty(parameters[fnTaskId].ToString())) ? (int?)int.Parse(parameters[fnTaskId].ToString()) : null;
                var statusIds = parameters[fnStatusId].ToString().Split(',').Select(int.Parse).ToList();
                var searchFilter = (!string.IsNullOrEmpty(parameters[fnSearchFilter].ToString())) ? parameters[fnSearchFilter].ToString() : string.Empty;
                var assigneeUid = (!string.IsNullOrEmpty(parameters[fnAssignedUid].ToString())) ? (int?)int.Parse(parameters[fnAssignedUid].ToString()) : null;
                var equipmentClassId = (!string.IsNullOrEmpty(parameters[fnEquipmentClassId].ToString())) ? (int?)int.Parse(parameters[fnEquipmentClassId].ToString()) : null;
                
                var query = new TaskRecordQuery(DataMgr.ConnectionString, siteUser).LoadWith(_ => _.Status).LoadWith(_ => _.User1)
                .LoadWith<Analyses_Equipment, Equipment, Equipment, Analyses_Equipment>(_ => _.Analyses_Equipment, _ => _.Equipment, _ => _.EquipmentType, _ => _.Building,_ => _.Analyse)
                .FinalizeLoadWith
                .GetAll(cid);

                if (taskId.HasValue) return GetTransformTaskData(query.ByTaskId(taskId.Value).ToEnumerable());

                query.ByAnalysisDateRange(asd, aed);
                query.ByStatus(statusIds);

                if (!string.IsNullOrWhiteSpace(searchFilter)) query.Search(searchFilter.Trim().Split(' '));

                if (assigneeUid.HasValue) query.ByAssignee(assigneeUid.Value);

                if (bid.HasValue) query.ByBuilding(bid.Value);

                if (equipmentClassId.HasValue) query.ByEquipmentClass(equipmentClassId.Value);

                if (eid.HasValue) query.ByEquipment(eid.Value);

                if (aid.HasValue) query.ByAnalysis(aid.Value);

                var results = query.ToEnumerable();

                BuildingSettingsDict = DataMgr.BuildingDataMapper.GetBuildingSettingsForDownloadByBids(results.Select(t => t.Analyses_Equipment.Equipment.BID).ToArray());

                return GetTransformTaskData(results);
            }

            IEnumerable<GetTasks> GetTransformTaskData(IEnumerable<TaskRecord> tasks)
            {
                var getTasks = new List<GetTasks>();                

                foreach (var task in tasks)
                {
                    var getTask = new GetTasks();

                    var bid = task.Analyses_Equipment.Equipment.BID;

                    var lcid = BuildingSettingsDict[bid].Item1;

                    var cost = task.AnnualAvoidableCost;

                    var coolingUse = task.AnnualAvoidableCoolingUse;

                    var heatingUse = task.AnnualAvoidableHeatingUse;

                    var electricUse = task.AnnualAvoidableElectricUse;

                    getTask.ClientTaskID = task.ClientTaskID;
                    getTask.TaskID = task.TaskID;
                    getTask.BuildingName = task.Analyses_Equipment.Equipment.Building.BuildingName;
                    getTask.EquipmentName = task.Analyses_Equipment.Equipment.EquipmentName;
                    getTask.Email = (task.User1 != null) ? task.User1.Email : string.Empty;
                    getTask.Summary = string.Join("|", task.Summary, LinkHelper.GetBaseUrl(Request, true) + LinkHelper.BuildTaskQuickLinkForSingleTask(task));
                    getTask.Description = task.Description;
                    getTask.Recommendations = task.Recommendations;
                    getTask.Actions = task.Actions;
                    getTask.StatusName = task.Status.StatusName;
                    getTask.Cid = task.CID;
                    getTask.Bid = bid;
                    getTask.EquipmentClassId = task.Analyses_Equipment.Equipment.EquipmentType.EquipmentClassID;
                    getTask.Eid = task.Analyses_Equipment.EID;
                    getTask.Aid = task.Analyses_Equipment.AID;
                    getTask.AnalysisRange = Enum.GetName(typeof(DataConstants.AnalysisRange), DataConstants.AnalysisRange.Daily);
                    getTask.AnalysisStartDateDisplay = task.AnalysisStartDate.ToString("d", CultureInfo.InvariantCulture);
                    getTask.AnalysisStartDate = task.AnalysisStartDate;
                    if (task.DateCompleted.HasValue)
                    {
                        var service = new TaskRecordDetailsServiceFactory(LogMgr).GetTaskRecordDetailsService(task, siteUser.CultureName);
                        //TaskRecordDetailsService taskRecordService = new TaskRecordDetailsService(task, );
                        getTask.DateCompleted = service.GetFormattedString(task.DateCompleted.Value, ModelTaskRecord.DateType.CreatedOrCompleted);
                    }                    
                    getTask.DiagnosticLink = string.Join("|", "link", LinkHelper.GetBaseUrl(Request, true) + LinkHelper.BuildDiagnosticsQuickLinkFromTask(getTask));
                    getTask.AnnualAvoidableCost = cost;
                    getTask.AnnualAvoidableCostCurrencyCode = (!string.IsNullOrWhiteSpace(cost)) ? new RegionInfo(BuildingSettingsDict[bid].Item1).ISOCurrencySymbol : null;
                    getTask.AnnualAvoidableCoolingUse = coolingUse;
                    getTask.AnnualAvoidableCoolingUseEngUnit = GetEngUnit(coolingUse, annualCoolingUseId, bid);
                    getTask.AnnualAvoidableHeatingUse = heatingUse;
                    getTask.AnnualAvoidableHeatingUseEgnUnit = GetEngUnit(heatingUse, annualHeatingUseId, bid);
                    getTask.AnnualAvoidableElectricUse = electricUse;
                    getTask.AnnualAvoidableElectricUseEngUnit = GetEngUnit(electricUse, annualElectricUseId, bid);
                    getTask.WorkOrderId = $"{task.WorkOrderID}|ignoreparse";

                    getTasks.Add(getTask);
                }

                return getTasks;
            }

            string GetEngUnit(string item, int pointTypeId, int bid)
            {
                if (string.IsNullOrWhiteSpace(item)) return null;

                var engUnit = EngUnits.Single(eu => eu.PointTypeID == pointTypeId);

                return (BuildingSettingsDict[bid].Item2) ? engUnit.SIEngUnits : engUnit.IPEngUnits; //Item2 = UnitSystem (boolean)
            }

        #endregion
    }
}