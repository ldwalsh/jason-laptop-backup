﻿using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace CW.Website._controls.tasks
{
    public partial class SearchCriteria : SiteUserControl
    {
        #region consts

            const string divCssClass = "dropDownPanel";
            const string dropDownsClssClass = "dropdownNarrow";
            const string dropDownsLabelsCssClass = "dropDownLabel";
            
        #endregion

        #region fields

            LinkHelper.ViewByMode selectedMode;
            DropDownSequencer dropDownSequencer;
            Dictionary<LinkHelper.ViewByMode, DropDowns.ViewModeEnum> viewModeDict;
            public DropDowns dropDowns;
            int initialStatusID = Convert.ToInt32(BusinessConstants.Status.Statuses.Open);
            TaskDropDowns taskDropDowns;
            DateTime? analysisStartDateQs, analysisEndDateQs;

        #endregion

        #region struct

        public struct SearchCriteriaFields
            {
                public int? Bid { get; set; }
                public int? EquipmentClassId { get; set; }
                public int? Eid { get; set; }
                public int? Aid { get; set; }
                public DateTime AnalysisStartDate { get; set; }
                public DateTime AnalysisEndDate { get; set; }
                public string ProjectStatus { get; set; }
                public string SearchFilter { get; set; }
                public int? AssigneeUid { get; set; }
                public int? TaskId { get; set; }
            }

        #endregion

        #region properties

            public ViewTasks ViewTasks { get; set; }
            DropDownList BuildingDDL { get { return dropDowns.GetDDL(DropDowns.ViewModeEnum.Building); } }
            DropDownList EquipmentClassDDL { get { return dropDowns.GetDDL(DropDowns.ViewModeEnum.EquipmentClass); } }
            DropDownList EquipmentDDL { get { return dropDowns.GetDDL(DropDowns.ViewModeEnum.Equipment); } }
            DropDownList AnalysisDDL { get { return dropDowns.GetDDL(DropDowns.ViewModeEnum.Analysis); } }
            int? Bid { get { return IsItemSelected(BuildingDDL) ? (int?)int.Parse(BuildingDDL.SelectedValue) : null; } }
            int? EquipmentClassId { get { return IsItemSelected(EquipmentClassDDL) ? (int?)int.Parse(EquipmentClassDDL.SelectedValue) : null; } }
            int? Eid { get { return IsItemSelected(EquipmentDDL) ? (int?)int.Parse(EquipmentDDL.SelectedValue) : null; } }
            public bool ShowDetails { get { return chkShowDetails.Checked; } }
           
        #endregion

        #region events

            void Page_Init()
            {
                dropDownSequencer = new DropDownSequencer(new[] { BuildingDDL, EquipmentClassDDL, EquipmentDDL, AnalysisDDL });
                dropDowns.DropDownsStylesDict = new Dictionary<DropDowns.ElementEnum, String>();

                dropDowns.DropDownsStylesDict.Add(DropDowns.ElementEnum.Div, divCssClass);
                dropDowns.DropDownsStylesDict.Add(DropDowns.ElementEnum.Label, dropDownsLabelsCssClass);
                dropDowns.DropDownsStylesDict.Add(DropDowns.ElementEnum.DropDown, dropDownsClssClass);

                viewModeDict = new Dictionary<LinkHelper.ViewByMode, DropDowns.ViewModeEnum>();

                viewModeDict.Add(LinkHelper.ViewByMode.Building, DropDowns.ViewModeEnum.Building);
                viewModeDict.Add(LinkHelper.ViewByMode.EquipmentClass, DropDowns.ViewModeEnum.EquipmentClass);
                viewModeDict.Add(LinkHelper.ViewByMode.Equipment, DropDowns.ViewModeEnum.Equipment);
                viewModeDict.Add(LinkHelper.ViewByMode.Analysis, DropDowns.ViewModeEnum.Analysis);

                taskDropDowns = new TaskDropDowns();
            }

            void Page_FirstLoad() 
            {
                Initialize();

                if (HttpContext.Current.Request.QueryString.Keys.Count > 0) GetQSValuesAndSetFilters();
            }

            protected void Page_AsyncPostbackLoad(object sender, EventArgs e) { GetSelectedModeAndSetViewMode(); }

            #region button events

                protected void generateData_Click(object sender, EventArgs e)
                {
                    ViewTasks.ClearCriteriaFromViewState();
                    ViewTasks.SetNewCriteriaToViewState();

                    HideFormAndMessaging();

                    ViewTasks.ResetGrid();
                    ViewTasks.BindGrid();
                    ViewTasks.SetItemCountMsg();
                }

            #endregion

            #region ddl events

                protected void dropDowns_BuildingChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    HideFormAndMessaging();

                    if (ea.SelectedIndex == 0) return;

                    if (dropDowns.ViewMode > DropDowns.ViewModeEnum.Client) BindEquipmentClasses();
                }

                protected void dropDowns_EquipmentClassChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    HideFormAndMessaging();

                    if (ea.SelectedIndex == 0) return;

                    if (dropDowns.ViewMode > DropDowns.ViewModeEnum.Building) BindEquipment();                    
                }

                protected void dropDowns_EquipmentChanged(DropDowns sender, DropDowns.ItemSelectedEventArgs ea)
                {
                    HideFormAndMessaging();

                    if (ea.SelectedIndex == 0) return;

                    if (dropDowns.ViewMode > DropDowns.ViewModeEnum.EquipmentClass) BindAnalyses();
                }

            #endregion

            #region radio button list events

                protected void rblViewBy_SelectedIndexChanged(object sender, EventArgs e)
                {
                    GetSelectedModeAndSetViewMode();

                    HideFormAndMessaging();

                    if (selectedMode > LinkHelper.ViewByMode.Building && EquipmentClassDDL.Items.Count == 1) BindEquipmentClasses();

                    switch (selectedMode)
                    {
                        case LinkHelper.ViewByMode.Building:
                            dropDownSequencer.ResetSubDropDownsOf(BuildingDDL);
                            return;
                        case LinkHelper.ViewByMode.EquipmentClass:
                            dropDownSequencer.ResetSubDropDownsOf(EquipmentClassDDL);
                            return;
                        case LinkHelper.ViewByMode.Equipment:
                            dropDownSequencer.ResetSubDropDownsOf(EquipmentDDL);
                            return;
                    }
                }

            #endregion

        #endregion

        #region methods

            #region binders

                void BindBuildings()
                {
                    dropDownSequencer.ResetSubDropDownsOf(BuildingDDL);

                    taskDropDowns.BindDDL( BuildingDDL, PropHelper.G<Building>(_ => _.BuildingName), PropHelper.G<Building>(_ => _.BID), siteUser.VisibleBuildings, true);

                    BuildingDDL.SelectedIndex = 1;
                }

                void BindEquipmentClasses()
                {
                    var equipmentClasses = (IsItemSelected(BuildingDDL)) ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID((int)Bid) : DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();

                    dropDownSequencer.ResetSubDropDownsOf(BuildingDDL);

                    taskDropDowns.BindDDL(EquipmentClassDDL, PropHelper.G<EquipmentClass>(_ => _.EquipmentClassName), PropHelper.G<EquipmentClass>(_ => _.EquipmentClassID), equipmentClasses, true);
                }

                void BindEquipment()
                {
                    var equipment = new QueryManager(DataMgr.ConnectionString, siteUser).Equipment.LoadWith<EquipmentType>(_ => _.EquipmentType, _ => _.EquipmentClass).FinalizeLoadWith.GetVisibleOnly().ByBuilding(Bid).ByClass(EquipmentClassId).ToList();

                    dropDownSequencer.ResetSubDropDownsOf(EquipmentClassDDL);

                    taskDropDowns.BindDDL(EquipmentDDL, PropHelper.G<Equipment>(_ => _.EquipmentName), PropHelper.G<Equipment>(_ => _.EID), equipment, true);
                }

                void BindAnalyses()
                {
                    var analyses = new QueryManager(DataMgr.ConnectionString, siteUser).Analysis.FinalizeLoadWith.GetVisibleOnly().ByBuilding(Bid).ByEquipmentClass(EquipmentClassId).ByEquipment(Eid).ToList();

                    dropDownSequencer.ResetSubDropDownsOf(EquipmentDDL);

                    taskDropDowns.BindDDL(AnalysisDDL, PropHelper.G<Analyse>(_ => _.AnalysisName), PropHelper.G<Analyse>(_ => _.AID), analyses, true);
                }

            #endregion

            void Initialize()
            {
                var initialMode = LinkHelper.ViewByMode.Building;
                var date = DateTime.UtcNow;

                rblViewBy.SelectedValue = initialMode.ToString();

                SetDropDownsViewMode(initialMode);

                BindBuildings();
            
                txtAnalysisStartDate.SelectedDate = date.AddYears(-1);
                txtAnalysisEndDate.SelectedDate = date;

                ControlHelper.BindListBox(lbStatus, PropHelper.G<Status>(_ => _.StatusName), PropHelper.G<Status>(_ => _.StatusID), new StatusQuery(DataMgr.ConnectionString, siteUser).FinalizeLoadWith.GetAll().ByTaskStatus().ToList());

                taskDropDowns.BindAssigneeDDL(ddlAssignee, true, false);

                lbStatus.Items.Cast<ListItem>().Single(_ => _.Value == initialStatusID.ToString()).Selected = true;  
            }

            void HideFormAndMessaging()
            {
                ViewTasks.HideMessages();
                ViewTasks.HidePanels();
                ViewTasks.ClearEditItems();
            }

            void GetQSValuesAndSetFilters()
            {
                var url = HttpContext.Current.Request.Url.PathAndQuery;
                var index = url.IndexOf('?');

                if (index > 0)
                {
                    var qs = new QueryString(url.Substring(index + 1));
                    var qsCID = qs.Get("cid", typeof(int));

                    if (qsCID == null) Response.Redirect("/Home.aspx");

                    if (qsCID != null && Int32.Parse(qsCID.ToString()) != siteUser.CID) Response.Redirect("/Home.aspx");

                    var qsBID = qs.Get("bid", typeof(int));
                    var qsEquipmentClassId = qs.Get("equipmentClassId", typeof(int));
                    var qsEID = qs.Get("eid", typeof(int));
                    var qsAID = qs.Get("aid", typeof(int));
                    var qsAnalysisStartDate = qs.Get("asd", typeof(string));
                    var qsAnalysisEndDate = qs.Get("aed", typeof(string));
                    var qsTaskId = qs.Get("taskId", typeof(int));
                    var viewMode = GetDropDownsViewModeByQSValue(qsBID, qsEquipmentClassId, qsEID, qsAID);

                    rblViewBy.SelectedValue = viewMode.ToString();

                    SetDropDownsViewMode(viewMode);

                    if (qsBID != null) BuildingDDL.SelectedValue = qsBID.ToString();

                    if (qsEID != null)
                    {
                        BindEquipmentClasses();

                        EquipmentClassDDL.SelectedValue = (qsEquipmentClassId != null) ? int.Parse(qsEquipmentClassId.ToString()).ToString() 
                                                                                       : DataMgr.EquipmentDataMapper.GetEquipmentByEID(int.Parse(qsEID.ToString()), false, false, false, false, true, true).EquipmentType.EquipmentClassID.ToString();

                        BindEquipment();

                        EquipmentDDL.SelectedValue = qsEID.ToString();
                    }

                    if (qsAID != null)
                    {
                        BindAnalyses();

                        AnalysisDDL.SelectedValue = qsAID.ToString();
                    }

                    if (qsAnalysisStartDate != null) analysisStartDateQs = DateTime.Parse(qsAnalysisStartDate.ToString(), CultureInfo.InvariantCulture);

                    if (qsAnalysisEndDate != null) analysisEndDateQs = DateTime.Parse(qsAnalysisEndDate.ToString(), CultureInfo.InvariantCulture);

                    if (qsTaskId != null) hdnTaskId.Value = qsTaskId.ToString();
                }
            }

            LinkHelper.ViewByMode GetDropDownsViewModeByQSValue(object bid, object equipmentClassId, object eid, object aid)
            {
                if (aid != null) return LinkHelper.ViewByMode.Analysis;
                if (eid != null) return LinkHelper.ViewByMode.Equipment;
                if (equipmentClassId != null) return LinkHelper.ViewByMode.EquipmentClass;
                if (bid != null) return LinkHelper.ViewByMode.Building;

                return LinkHelper.ViewByMode.Building;
            }

            void SetDropDownsViewMode(LinkHelper.ViewByMode mode) { dropDowns.ViewMode = viewModeDict[mode]; }

            void GetSelectedModeAndSetViewMode()
            {
                selectedMode = EnumerableHelper.ParseEnum<LinkHelper.ViewByMode>(rblViewBy.SelectedValue);
                SetDropDownsViewMode(selectedMode);
            }

            bool IsItemSelected(DropDownList ddl, int? index = null) { return ddl.SelectedIndex > ((index.HasValue) ? index.Value : 1); }

            public SearchCriteriaFields GetSearchCriteriaFieldData()
            {
                var searchFieldCriteriaFields = new SearchCriteriaFields();

                if (IsItemSelected(BuildingDDL)) searchFieldCriteriaFields.Bid = new int?(int.Parse(BuildingDDL.SelectedValue));

                if (IsItemSelected(EquipmentClassDDL)) searchFieldCriteriaFields.EquipmentClassId = new int?(int.Parse(EquipmentClassDDL.SelectedValue));

                if (IsItemSelected(EquipmentDDL)) searchFieldCriteriaFields.Eid = new int?(int.Parse(EquipmentDDL.SelectedValue));

                if (IsItemSelected(AnalysisDDL)) searchFieldCriteriaFields.Aid = new int?(int.Parse(AnalysisDDL.SelectedValue));

                searchFieldCriteriaFields.AnalysisStartDate = txtAnalysisStartDate.SelectedDate.Value;
                searchFieldCriteriaFields.AnalysisEndDate = txtAnalysisEndDate.SelectedDate.Value;

                if (analysisStartDateQs.HasValue)
                {
                    var analysisStartDate = analysisStartDateQs.Value;

                    searchFieldCriteriaFields.AnalysisStartDate = analysisStartDate;

                    txtAnalysisStartDate.SelectedDate = analysisStartDate;
                }

                if (analysisEndDateQs.HasValue)
                {
                    var analysisEndDate = analysisEndDateQs.Value;

                    searchFieldCriteriaFields.AnalysisEndDate = analysisEndDate;

                    txtAnalysisEndDate.SelectedDate = analysisEndDate;
                }

                searchFieldCriteriaFields.ProjectStatus = string.Join(",", lbStatus.Items.Cast<ListItem>().Where(item => item.Selected).Select(item => item.Value).ToList());

                searchFieldCriteriaFields.SearchFilter = txtTaskFilter.Text.Trim();

                if (IsItemSelected(ddlAssignee, 0)) searchFieldCriteriaFields.AssigneeUid = new int?(int.Parse(ddlAssignee.SelectedValue));

                if (!string.IsNullOrWhiteSpace(hdnTaskId.Value)) //set it only once
                {
                    searchFieldCriteriaFields.TaskId = new int?(int.Parse(hdnTaskId.Value));

                    hdnTaskId.Value = string.Empty;
                }

                return searchFieldCriteriaFields;
            }

        #endregion
    }
}