﻿using CW.Data.Models.Building;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._controls
{
    public partial class WorldwideImpactMap : SiteUserControl
    {
        #region Properties

            public string MapString { get; set; }
       
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //no code is required in the page load for this control, functionality is handled via properties/methods
        }

        #region WIDGET

            /// <summary>
            /// Sets initial google impact map string
            /// </summary>
            private void SetGoogleImpactMapString()
            {
                IEnumerable<GetBuildingsDataMapFormat> countries = DataMgr.BuildingDataMapper.GetAllBuildingsInDataMapFormat();

                MapString = "google.visualization.arrayToDataTable([['Address', 'Buildings', 'Points'],";

                foreach (GetBuildingsDataMapFormat country in countries)
                {
                    //['US', 222 ,23002],['JAPAN', 2, 332],['INDIA', 1, 11445],['CHINA', 1, 55], ['ITALY', 1, 6656],])";
                    MapString += String.Format("['{0}', {1}, {2}],", country.CountryAlpha2Code, country.BuildingCount, country.PointCount);
                }

                MapString += "])";
            }

        #endregion

    }
}