﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Commodities.ascx.cs" Inherits="CW.Website._controls.Commodities" %>

<style>
    
    .tabular-list td {
        border-bottom: 1px solid lightgrey;
        padding-top: 5px;
        padding-bottom: 5px;
    }
    
    .tabular-list td.green
    {
        color: green;
    } 

    .tabular-list td.red
    {
        color: red;
    } 

</style>
<asp:Repeater ID="rptCommodities" runat="server">
      <HeaderTemplate>
          <table id="energy" class="tabular-list">
              <tbody>
      </HeaderTemplate>
      <ItemTemplate>
                <tr>
                    <td><%# Eval("DisplayName") %></td>
                    <td><%# Eval("Value") %></td>
                    <td class="<%# Eval("Color") %>"><%# Eval("Percentage") %></td>
                </tr>
      </ItemTemplate>
      <FooterTemplate>
            </tbody>
          </table>
      </FooterTemplate>
</asp:Repeater>