﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="CW.Website._controls.Footer" %>
    
 <div class="inner" runat="server">

    <div id="divFooterLeft" class="divFooterLeft" runat="server">
        &nbsp;
    </div>
    <div id="divFooterRight" class="divFooterMultiLogoRight" runat="server">
        <img class="imgOrganizationLogo" id="imgOrganizationLogo" src="images/cw-header-secondary.png" runat="server" />
    </div>
    <div id="divFooterBottom" class="divFooterBottom" runat="server">
        <asp:Literal ID="litFooterContent" runat="server"></asp:Literal>
    </div>


</div>