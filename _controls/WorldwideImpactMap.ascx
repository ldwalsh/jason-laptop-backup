﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WorldwideImpactMap.ascx.cs" Inherits="CW.Website._controls.WorldwideImpactMap" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>

<div id="worldwideImpactMap" class="widgetBox" runat="server" visible="false">

  <div class="widgetTop">Worldwide Impact Map</div>
  
  <div class="widgetBody">

    <telerik:RadScriptBlock ID="radScriptBlock2" runat="server">                                                   
    
      <script type='text/javascript'>
      google.load('visualization', '1', { 'packages': ['geochart'] });
      google.setOnLoadCallback(drawRegionsMap);

      function drawRegionsMap() {
        var data = <%= MapString %>;

        var options = {
            colorAxis: { colors: ['#F2E7D3', '#C48000'] },
        };

        var chart = new google.visualization.GeoChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      };
      </script>
  
    </telerik:RadScriptBlock>

    <div id="chart_div" style="width: 100%; height: 100%;"></div>

  </div> <!--end of widgetBody div-->

</div> <!--end of worldwideImpactMap div-->