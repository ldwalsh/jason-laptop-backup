﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuildingDataPointsPieChart.ascx.cs" Inherits="CW.Website._controls.BuildingDataPointsPieChart" %>


<script type="text/javascript">
    function onChartPostRenderResize(sender, args) {
        try {
            document.getElementById('chart_div2').style.width = '100%';
        } catch (e) {
        }
    }
</script>    

<asp:Literal ID="litError" runat="server" />

<div id="buildingDataPointsPieChart">

  <input type="text" id="buildingPointPieChartString" value="<%= BuildingPointPieChartString %>" style="display: none;" />

  <div id="chart_div2" style="width: 100%; height: 100%;"></div>

</div>

<script type="text/javascript">
    onChartPostRenderResize();
</script>	
