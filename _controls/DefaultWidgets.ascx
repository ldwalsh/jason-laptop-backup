﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DefaultWidgets.ascx.cs" Inherits="CW.Website._controls.DefaultWidgets" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
<%@ Register src="~/_controls/Commodities.ascx" tagname="Commodities" tagprefix="CW" %>
<%@ Register src="~/_controls/News.ascx" tagname="News" tagprefix="CW" %>
<%@ Import Namespace="CW.Utility" %>
              
<input type="button" id="btnUILoad" value="LoadDefaultWidgets" style="display: none;" />

<div class="homeColumns dockWrapper">

  <div>
    
    <telerik:RadDockLayout ID="radDockLayout1" runat="server">
      <telerik:RadDockZone ID="radDockZone1" runat="server">
                        
        <telerik:RadDock ID="radDockBuildingMap" CssClass="radDockLarge" Title="Building Map" runat="server">
          <ContentTemplate>
            <div id="updateProgressBuildingMap" class="updateProgressDivForWidgets"></div>
            <div id="buildingMap"></div>
		  </ContentTemplate>
        </telerik:RadDock>
          
      </telerik:RadDockZone>
    </telerik:RadDockLayout>    

  </div>
  
  <div class="homeLeftColumn">
                  
    <telerik:RadDockLayout ID="radDockLayout2" runat="server">
      <telerik:RadDockZone ID="radDockZone2" runat="server">             
                                                      
        <telerik:RadDock ID="radDockLastMonthsDiagnosticSummary" CssClass="radDockMedium" Title="Last Month's Top Portfolio Diagnostic Summaries" runat="server">
          <ContentTemplate>
            <div id="updateProgressLastMonthsDiagnosticSummary" class="updateProgressDivForWidgets"></div>
            <div id="lastMonthsDiagnosticSummary"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockLastMonthsDiagnosticResults" CssClass="radDockMedium" Title="Last Month's Top Portfolio Diagnostic Results" runat="server">
          <ContentTemplate>
            <div id="updateProgressLastMonthsDiagnosticResults" class="updateProgressDivForWidgets"></div>
            <div id="lastMonthsDiagnosticResults"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockRecentTasks" CssClass="radDockMedium" Title="Recent Open Tasks" runat="server">
          <ContentTemplate>
            <div id="updateProgressRecentTasks" class="updateProgressDivForWidgets"></div>
            <div id="recentTasks"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockTopProjects" CssClass="radDockMedium" Title="Top Open Projects" runat="server">
          <ContentTemplate>
            <div id="updateProgressTopProjects" class="updateProgressDivForWidgets"></div>
            <div id="topProjects"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockUtilityConsumptionComparison" CssClass="radDockMedium" Title="Recent Building Utility Consumption" runat="server">
          <ContentTemplate>
			<div id="updateConsumptionComparison" class="updateProgressDivForWidgets"></div>
			<div id="divConsumptionComparison"></div>
			<script type="text/javascript" src="_assets/scripts/consumptionWidget.js"></script>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockBuildingDataPointsPieChart" CssClass="radDockMedium" Title="Building Data Points" runat="server">
          <ContentTemplate>
            <div id="updateProgressBuildingDataPointsPieChart" class="updateProgressDivForWidgets"></div>
            <div id="buildingDataPointsPieChart"></div>
          </ContentTemplate>
        </telerik:RadDock>

      </telerik:RadDockZone>
    </telerik:RadDockLayout>

  </div>
  <div class="homeRightColumn">

    <telerik:RadDockLayout ID="radDockLayout3" runat="server">
      <telerik:RadDockZone ID="radDockZone3" runat="server">

        <telerik:RadDock ID="radDockPast30DaysAvoidableCosts" CssClass="radDockMedium" Title="Past 30 Days Portfolio Avoidable Costs <a id='past30DaysAvoidableCostsLink' class='dockHeaderLink' href='javascript:' onclick='redirectUrl(event);' data-url='/CommissioningDashboard.aspx'>commissioning dashboard</a>" runat="server">
          <ContentTemplate>
            <div id="updateProgressPast30DaysAvoidableCosts" class="updateProgressDivForWidgets"></div>
            <div id="past30DaysAvoidableCosts"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockYesterdaysDiagnosticSummary" CssClass="radDockMedium" Title="Yesterday's Top Portfolio Diagnostic Summaries" runat="server">
          <ContentTemplate>
            <div id="updateProgressYesterdaysDiagnosticSummary" class="updateProgressDivForWidgets"></div>
            <div id="yesterdaysDiagnosticSummary"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockYesterdaysDiagnosticResults" CssClass="radDockMedium" Title="Yesterday's Top Portfolio Diagnostic Results" runat="server">
          <ContentTemplate>
            <div id="updateProgressYesterdaysDiagnosticResults" class="updateProgressDivForWidgets"></div>
            <div id="yesterdaysDiagnosticResults"></div>
          </ContentTemplate>
        </telerik:RadDock>
                      
        <telerik:RadDock ID="radDockPast30DaysPortfolioPriorities" CssClass="radDockMedium" Title="Past 30 Days Portfolio Priorities <a id='Past30DaysPortfolioPrioritiesLink' class='dockHeaderLink' href='javascript:' onclick='redirectUrl(event);' data-url='/CommissioningDashboard.aspx'>commissioning dashboard</a>" runat="server">
          <ContentTemplate>
            <div id="updateProgressPast30DaysPortfolioPriorities" class="updateProgressDivForWidgets"></div>
            <div id="past30DaysPortfolioPriorities"></div>
          </ContentTemplate>
        </telerik:RadDock>

        <telerik:RadDock ID="radDockQuickLinks" CssClass="radDockMedium" Title="Quick Links <a id='DockQuickLinksLink' class='dockHeaderLink' href='javascript:' onclick='redirectUrl(event);' data-url='/MyAccount.aspx?tab=1'>edit</a>" runat="server">
          <ContentTemplate>
            <div id="updateProgressQuickLinks" class="updateProgressDivForWidgets"></div>
            <div id="quickLinks"></div>
          </ContentTemplate>
        </telerik:RadDock>

      </telerik:RadDockZone>
    </telerik:RadDockLayout>

  </div>

  <div>

   <telerik:RadDockLayout ID="radDockLayout4" runat="server">
      <telerik:RadDockZone ID="radDockZone4" runat="server">
                
        <telerik:RadDock ID="radDockFaultsSparkLine" CssClass="radDockLarge" Title="Building Faults" runat="server">
            <ContentTemplate>
            <div id="updateProgressFaults" class="updateProgressDivForWidgets"></div>
            <div id="faults"></div>
            </ContentTemplate>
        </telerik:RadDock>
                 
      </telerik:RadDockZone>
    </telerik:RadDockLayout>

  </div>

</div> <!--end of homeColumns div-->
               
<hr />

<telerik:RadDockLayout ID="radDockLayout5" runat="server">
  <telerik:RadDockZone ID="radDockZone5" runat="server">

    <telerik:RadDock ID="radDockNews" CssClass="radDockLarge" Title="Internal Organization News" runat="server">
      <ContentTemplate>
        <CW:News ID="News" runat="server" />
      </ContentTemplate>
    </telerik:RadDock>
                 
  </telerik:RadDockZone>
</telerik:RadDockLayout>