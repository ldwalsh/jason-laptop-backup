﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BuildingMap.ascx.cs" Inherits="CW.Website._controls.BuildingMap" %>

<asp:Literal ID="litError" runat="server" />

<div id="buildingMap">

  <input type="text" id="geocodeCredentialsKey" value='<%= GeocodeCredentialsKey %>' style="display: none;" />
  <input type="text" id="userDataforBuildingMap" value='<%= UserDataforBuildingMap %>' style="display: none;" />
  <input type="text" id="userCulture" value='<%= siteUser.CultureName %>' style="display: none;" />

  <div id="mapDiv" class="<%= IsProviderView ? "buildingMapFull" : "buildingMap" %>"></div>

</div>