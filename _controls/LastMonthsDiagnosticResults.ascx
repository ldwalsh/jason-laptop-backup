﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LastMonthsDiagnosticResults.ascx.cs" Inherits="CW.Website._controls.LastMonthsDiagnosticResults" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Literal ID="litError" runat="server" />

<div id="gridTbl">

    <asp:GridView ID="gridLastMonthsTopPortfolioDiagnosticsResults" EnableViewState="true" runat="server" DataKeyNames="BID" GridLines="None" AllowPaging="false" AllowSorting="false" AutoGenerateColumns="false" HeaderStyle-CssClass="tblTitle" RowStyle-CssClass="tblCol1" AlternatingRowStyle-CssClass="tblCol2" RowStyle-Wrap="true" EmptyDataRowStyle-CssClass="dockEmptyGridLabel"> 
    <Columns>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Client" Visible="false">  
        <ItemTemplate><%# StringHelper.TrimText(Eval("ClientName"),30) %></ItemTemplate>
      </asp:TemplateField> 
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Building">  
        <ItemTemplate><%# StringHelper.TrimText(Eval("BuildingName"),30) %></ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Equipment">  
        <ItemTemplate><%# StringHelper.TrimText(Eval("EquipmentName"), 30) %></ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Notes Summary">  
        <ItemTemplate><%# Eval("NotesSummary") %></ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Avoidable Costs">  
        <ItemTemplate><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval("CostSavings"))) %></ItemTemplate>
      </asp:TemplateField>
      <asp:TemplateField HeaderText="E">                                          
        <ItemTemplate>                                                                                                                                              
          <%# String.IsNullOrEmpty(Convert.ToString(Eval("EnergyPriority"))) ? 0 : (Convert.ToDecimal(Eval("EnergyPriority").ToString()) > 10 ? 10 : Math.Ceiling(Convert.ToDecimal(Eval("EnergyPriority").ToString())))%>
        </ItemTemplate> 
      </asp:TemplateField>
      <asp:TemplateField HeaderText="C">  
        <ItemTemplate>                                                                                                 
          <%# String.IsNullOrEmpty(Convert.ToString(Eval("ComfortPriority"))) ? 0 : (Convert.ToDecimal(Eval("ComfortPriority").ToString()) > 10 ? 10 : Math.Ceiling(Convert.ToDecimal(Eval("ComfortPriority").ToString())))%>
        </ItemTemplate> 
      </asp:TemplateField>                                                                                                                       
      <asp:TemplateField HeaderText="M">  
        <ItemTemplate>                                                   
          <%# String.IsNullOrEmpty(Convert.ToString(Eval("MaintenancePriority"))) ? 0 : (Convert.ToDecimal(Eval("MaintenancePriority").ToString()) > 10 ? 10 : Math.Ceiling(Convert.ToDecimal(Eval("MaintenancePriority").ToString())))%>
        </ItemTemplate> 
      </asp:TemplateField>       
      <asp:TemplateField ItemStyle-Wrap="true">  
        <ItemTemplate><a href="Diagnostics.aspx?cid=<%# Eval("CID") %>&bid=<%# Eval("BID") %>&ecid=<%# Eval("EquipmentClassID") %>&eid=<%# Eval("EID") %>&aid=<%# Eval("AID") %>&rng=<%# Eval("AnalysisRange") %>&sd=<%# Eval("StartDate") %>" class='dockLink'>view</a></ItemTemplate>
      </asp:TemplateField>                     
    </Columns>        
  </asp:GridView>

</div>