﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SearchCriteria.ascx.cs" Inherits="CW.Website._controls.SearchCriteria" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

<div class="dockTopWide">
             
  <asp:HyperLink ID="lnkSelection" runat="server" CssClass="toggle">

    <asp:Label ID="lblSelection" runat="server" CssClass="toggle" />

  </asp:HyperLink>
    
  <br />

  <ajaxToolkit:CollapsiblePanelExtender ID="cpeSearchCriteria" runat="server" />

  <asp:Panel ID="pnlSelection" runat="server">

    <div class="divDockFormWrapper">

      <div class="divDockFormFirst">
        <label class="labelBold">Building Type(s):</label>
        <asp:DropDownList ID="ddlBuildingTypes" 
                          runat="server" 
                          AppendDataBoundItems="true" 
                          OnSelectedIndexChanged="ddlBuildingTypes_OnSelectedIndexChanged" 
                          AutoPostBack="true" 
                          CausesValidation="false" 
                          CssClass="dropdown" 
        />
      </div>   
          
      <div class="divDockForm" runat="server">
        <label class="labelBold">
          Building Group(s): 
          <asp:LinkButton ID="lnkAll1" runat="server" OnClick="lnkAll_Click" />
        </label>
        <extensions:ListBoxExtension ID="lbeBuildingGroups"
                                     runat="server" 
                                     SelectionMode="Multiple" 
                                     OnSelectedIndexChanged="lbeBuildingGroups_SelectedIndexChanged"
                                     AutoPostBack="true"
                                     CausesValidation="false" 
                                     CssClass="listboxShortWide lbeBuildingGroups" 
        />
      </div>                            

      <div class="divDockForm">
        <label class="labelBold">
          Building(s):
          <asp:LinkButton ID="lnkAll2" runat="server" OnClick="lnkAll_Click" />
        </label> 
        <extensions:ListBoxExtension ID="lbeBuildings" runat="server" SelectionMode="Multiple" CssClass="listboxShortWide lbeBuildings" />                                        
      </div>
            
      <div class="divDockForm">
        <label class="labelBold">Range (months):</label>
        <asp:DropDownList ID="ddlRanges" runat="server" CssClass="dropdownNarrowest">
          <asp:ListItem Selected="True" Value="1" Text="1" />
          <asp:ListItem Value="3" Text="3" />
          <asp:ListItem Value="6" Text="6" />
          <asp:ListItem Value="12" Text="12" /> 
          <asp:ListItem Value="18" Text="18" />
          <asp:ListItem Value="24" Text="24" />
          <asp:ListItem Value="36" Text="36" />
        </asp:DropDownList> 
      </div>

      <div class="divDockForm" runat="server">
        <label class="labelBold">Unit(s):</label>
        <asp:DropDownList ID="ddlUnits" 
                          runat="server" 
                          AppendDataBoundItems="true" 
                          OnSelectedIndexChanged="ddlUnits_SelectedIndexChanged" 
                          AutoPostBack="true"
                          CausesValidation="false"
                          CssClass="dropdownNarrowest" />
      </div>
  
      <div class="divDockForm">
        <label class="labelBold">Currency(ies):</label>
        <asp:DropDownList ID="ddlCurrencies" 
                          runat="server" 
                          AppendDataBoundItems="true" 
                          OnSelectedIndexChanged="ddlCurrencies_SelectedIndexChanged"
                          AutoPostBack="true"
                          CausesValidation="false"
                          CssClass="dropdownNarrow" />
      </div>
                                               
      <div class="divDockForm">     
        <span class="labelBold">Refresh Data:</span>      
        <asp:LinkButton ID="btnGenerate" runat="server" CausesValidation="false" CssClass="lnkBtnRefreshDock" />
      </div> 
  
    </div> <!--end of divDockFormWrapper div-->
                            
  </asp:Panel>

</div> <!--end dockTopWide div-->