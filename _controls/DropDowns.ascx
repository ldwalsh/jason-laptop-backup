﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="DropDowns.ascx.cs" Inherits="CW.Website._controls.DropDowns" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

<div id="divClient" runat="server" class="divForm">
    <label id="lblClient" class="label" runat="server">*Select Client:</label>
    <asp:DropDownList ID="ddlClient" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlClient_OnSelectedIndexChanged" AutoPostBack="true" />
    <asp:PlaceHolder id="ClientPlaceHolder" runat="server" />
</div>
<asp:RequiredFieldValidator ID="clientRequiredValidator" runat="server" ErrorMessage="Client is a required field." ControlToValidate="ddlClient" SetFocusOnError="true" Display="None" InitialValue="-1" />
<ajaxToolkit:ValidatorCalloutExtender ID="clientRequiredValidatorExtender" runat="server" TargetControlID="clientRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<div id="divBuilding" runat="server" class="divForm">
    <label id="lblBuilding" class="label" runat="server">*Select Building:</label>
    <asp:DropDownList ID="ddlBuilding" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuilding_OnSelectedIndexChanged" AutoPostBack="true" />
    <asp:PlaceHolder id="BuildingPlaceHolder" runat="server" />
</div>
<asp:RequiredFieldValidator ID="buildingRequiredValidator" runat="server" ErrorMessage="Building is a required field." ControlToValidate="ddlBuilding" SetFocusOnError="true" Display="None" InitialValue="-1" />
<ajaxToolkit:ValidatorCalloutExtender ID="buildingRequiredValidatorExtender" runat="server" TargetControlID="buildingRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<div id="divEquipmentClass" runat="server" class="divForm">
    <label id="lblEquipmentClass" class="label" runat="server">*Select Equipment Class:</label>
    <asp:DropDownList ID="ddlEquipmentClass" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentClass_OnSelectedIndexChanged" AutoPostBack="true" />
</div>
<asp:RequiredFieldValidator ID="equipmentClassRequiredValidator" runat="server" ErrorMessage="Equipment Class is a required field." ControlToValidate="ddlEquipmentClass" SetFocusOnError="true" Display="None" InitialValue="-1" />
<ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassRequiredValidatorExtender" runat="server" TargetControlID="equipmentClassRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<div id="divEquipment" runat="server" class="divForm">
    <label id="lblEquipment" class="label" runat="server">*Select Equipment:</label>
    <extensions:DropDownExtension ID="ddlEquipment" runat="server" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" />
</div>
<asp:RequiredFieldValidator ID="equipmentRequiredValidator" runat="server" ErrorMessage="Equipment is a required field." ControlToValidate="ddlEquipment" SetFocusOnError="true" Display="None" InitialValue="-1" />
<ajaxToolkit:ValidatorCalloutExtender ID="equipmentRequiredValidatorExtender" runat="server" TargetControlID="equipmentRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

<div id="divAnalysis" runat="server" class="divForm">
    <label id="lblAnalysis" class="label" runat="server">*Select Analysis:</label>
    <extensions:DropDownExtension ID="ddlAnalysis" runat="server" AppendDataBoundItems="true" />
</div>
<asp:RequiredFieldValidator ID="analysisRequiredValidator" runat="server" ErrorMessage="Analysis is a required field." ControlToValidate="ddlAnalysis" SetFocusOnError="true" Display="None" InitialValue="-1" />
<ajaxToolkit:ValidatorCalloutExtender ID="analysisRequiredValidatorExtender" runat="server" TargetControlID="analysisRequiredValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
