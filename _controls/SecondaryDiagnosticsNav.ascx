﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SecondaryDiagnosticsNav.ascx.cs" Inherits="CW.Website._controls.SecondaryDiagnosticsNav" %>

<!-- Currently not being used -->


<!--
<div class="diagnosticsNav">   
    <a class="hoverBtn" href="~/Diagnostics.aspx" id="lnkDiagnostics" runat="server">Diagnostics</a>
    <a class="hoverBtn" href="~/Building.aspx" id="lnkBuilding" runat="server">Building</a>
    <a class="hoverBtn" href="~/Equipment.aspx" runat="server">Equipment</a>
    <a class="hoverBtn" href="~/Analysis.aspx" runat="server">Analysis</a>
    <a class="hoverBtn" href="~/#" runat="server">Notes</a> 
</div>
-->

<div class="diagnosticsNav">   
    <ul class="dropdown">
        <li><a href="Diagnostics.aspx" class="hoverBtn">Diagnostics</a></li>
        <li><a class="hoverBtn">Notes</a></li>
    </ul>
</div>