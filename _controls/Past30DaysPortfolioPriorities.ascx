<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Past30DaysPortfolioPriorities.ascx.cs" Inherits="CW.Website._controls.Past30DaysPortfolioPriorities" %>

<script type="text/javascript">
    function onChartPostRenderResize(sender, args) {
            try {
                document.getElementById('Past30DaysPortfolioPriorities_prioritiesChart').style.width = '100%';
            } catch (e) {
            }
        }
</script>         

<asp:Literal ID="litError" runat="server" />

<asp:Chart ID="prioritiesChart" OnPrePaint="prioiritiesChart_PrePaint" CssClass="widgetAspChart" runat="server" ImageType="Png" EnableViewState="true" Width="504px" Visible="false">                            
                  
  <ChartAreas>
    <asp:ChartArea Name="prioritiesChartArea" BackColor="White">                                         
      <AxisY LineColor="#666666" LineWidth="1" Title="Total Incidents" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
        <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
        <MajorGrid Enabled="false" /> 
        <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
      </AxisY>

      <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="Average Priority" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
        <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
        <MajorGrid Enabled="false" /> 
        <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
      </AxisY2>

      <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
        <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 7.5pt, style=Bold" />
        <MajorGrid Enabled="false" />  
        <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
      </AxisX>                        
    </asp:ChartArea>
  </ChartAreas>

  <Legends>                                                            
    <asp:Legend Font="Arial, 8.25pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666" IsTextAutoFit="true" />
    <asp:Legend Name="Hidden" Enabled="false" />
  </Legends>
</asp:Chart>
    
<asp:Label ID="lblPortfolioPriorities" CssClass="dockMessageLabel" runat="server" Text="No priorities." Visible="false" />

<script type="text/javascript">
    onChartPostRenderResize();
</script>	