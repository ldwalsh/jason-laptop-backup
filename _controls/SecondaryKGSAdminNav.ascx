﻿<%@ Control Language="C#" AutoEventWireup="false" CodeBehind="SecondaryKGSAdminNav.ascx.cs" Inherits="CW.Website._controls.SecondaryKGSAdminNav" %>
<%@ Import Namespace="CW.Website._framework.httpmodules" %>

<div class="kgsAdminNav">

  <ul class="dropdown">
    <li><a href="/KGSAdministration.aspx" class="hoverBtn">Admin Home</a></li>
    <li><a class="hoverBtnNoCursor">Data Sources</a>
      <ul class="sub_menu">
        <li><a href="/KGSDataSourceAdministration.aspx">Data Sources</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSDataSourceVendorAdministration.aspx">Data Source Vendors</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSDataSourceVendorContactAdministration.aspx">Data Source Vendor Contacts</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSDataSourceVendorProductAdministration.aspx">Data Source Vendor Products</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Buildings</a>
      <ul class="sub_menu">
        <li><a href="/KGSBuildingAdministration.aspx">Buildings</a></li>   
        <li runat="server" data-boolean-check="7"><a href="/KGSBuildingClassAdministration.aspx">Building Classes</a></li>
        <li runat="server" data-boolean-check="7"><a href="/<%= PathRewriteModule.KGSBuildingGroupAdministration %>">Building Groups</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSBuildingTypeAdministration.aspx">Building Types</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSBuildingVariableAdministration.aspx">Building Variables</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Equipment</a>
      <ul class="sub_menu">
        <li><a href="/KGSEquipmentAdministration.aspx">Equipment</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSEquipmentClassAdministration.aspx">Equipment Classes</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSEquipmentManufacturerAdministration.aspx">Equipment Manufacturers</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSEquipmentManufacturerContactAdministration.aspx">Equip. Manufacturer Contacts</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSEquipmentManufacturerModelAdministration.aspx">Equip. Manufacturer Models</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSEquipmentTypeAdministration.aspx">Equipment Types</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSEquipmentVariableAdministration.aspx">Equipment Variables</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Points</a>
      <ul class="sub_menu">
        <li><a href="/KGSPointAdministration.aspx">Points</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSPointClassAdministration.aspx">Point Classes</a></li>
        <li runat="server" data-boolean-check="7"><a href="/KGSPointTypeAdministration.aspx">Point Types</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Analyses</a>
      <ul class="sub_menu">
        <li runat="server" data-boolean-check="7"><a href="/KGSAnalysisAdministration.aspx">Analyses</a></li>
        <li><a href="/KGSScheduledAnalysesAdministration.aspx">Scheduled Analyses</a></li>
      </ul>
    </li>
    <li><a class="hoverBtnNoCursor">Users</a>
      <ul class="sub_menu">
        <li><a href="/KGSUserAdministration.aspx">Users</a></li>
        <li runat="server" data-boolean-check="7"><a href="/<%= PathRewriteModule.KGSUserGroupAdministration %>">User Groups</a></li> 
      </ul>
    </li>
    <li runat="server" data-boolean-check="7"><a href="/<%= PathRewriteModule.KGSClientAdministration %>" class="hoverBtn">Clients</a></li>
    <li runat="server" data-boolean-check="7"><a class="hoverBtnNoCursor">Providers</a>
      <ul class="sub_menu">
        <li><a href="/<%= PathRewriteModule.KGSProviderAdministration %>">Providers</a></li>
        <li><a href="/<%= PathRewriteModule.KGSProviderClientAdministration %>">Providers Clients</a></li> 
      </ul>
    </li>
    <li runat="server" data-boolean-check="8"><a href="/<%= PathRewriteModule.KGSOrganizationAdministration %>" class="hoverBtn">Organizations</a></li>
    <li runat="server" data-boolean-check="7"><a class="hoverBtnNoCursor">Other</a>
      <ul class="sub_menu">
        <li><a href="/KGSEngUnitsAdministration.aspx">Engineering Units</a></li> 
        <li><a href="/KGSFileClassAdministration.aspx">File Classes</a></li>
        <li><a href="/KGSFileTypeAdministration.aspx">File Types</a></li>
      </ul>
    </li>
    <li runat="server" data-boolean-check="7"><a class="hoverBtnNoCursor">System</a>
      <ul class="sub_menu">
        <li><a href="/KGSSystemTest.aspx">System Test</a></li>
        <li><a href="/KGSUploadBuildings.aspx">Upload Buildings</a></li>
        <li><a href="/KGSUploadBuildingVariables.aspx">Upload Building Variables</a></li>
        <li><a href="/KGSUploadDataSources.aspx">Upload Data Sources</a></li>
        <li><a href="/KGSUploadEquipment.aspx">Upload Equipment</a></li>
        <li><a href="/KGSUploadEquipmentPoints.aspx">Upload Equipment Points</a></li>
        <li><a href="/KGSUploadEquipmentToEquipment.aspx">Upload Equipment To Equipment</a></li>
        <li><a href="/KGSUploadEquipmentVariables.aspx">Upload Equipment Variables</a></li>
        <li><a href="/KGSUploadPoints.aspx">Upload Points</a></li>
      </ul>
    </li>
  </ul>

</div>