﻿using CW.Common.Constants;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI.DataVisualization.Charting;

namespace CW.Website._controls
{
    public partial class Past30DaysPortfolioPriorities : ModularSiteUserControl
    {
        #region fields

            private enum ChartType { Energy, Maintenance, Comfort }

            private DateTime startDate, endDate;

        #endregion

        #region properties

            protected override DiagnosticsResultTypes DiagnosticResultType { get { return DiagnosticsResultTypes.Past30Days; } }

        #endregion

        #region methods

            #region overrides

                protected override void Initialize()
                {
                    DateTimeHelper.GenerateDefaultDatesYesterday(out startDate, out endDate, EarliestBuildingTimeZoneID);
                }

                protected override void BindControl()
                {
                    RetrieveAndBindDiagnostics();
                }

                protected override IEnumerable<DiagnosticsResult> QueryDiagnostics()
                {
                    return GetConcatenatedResults(DateTimeHelper.GetXDaysAgo(endDate, 30), endDate, DataConstants.AnalysisRange.Daily, false);
                }

                protected override void BindDiagnostics(IEnumerable<DiagnosticsResult> results)
                {
                    var hasResults = results.Any();

                    if (hasResults)
                        BindPriortiesChart(results);

                    prioritiesChart.Visible = (hasResults) ? true : false;
                    lblPortfolioPriorities.Visible = !hasResults;
                }

                protected override void SetError()
                {
                    litError.Text = BusinessConstants.Message.IntermittentLoadError;
                }

            #endregion

            private void BindPriortiesChart(IEnumerable<DiagnosticsResult> past30DaysBuildingDiagnostics)
            {
                prioritiesChart.Series.Clear();

                //energy/////////////////////////////////
                var energyDiagnostics = past30DaysBuildingDiagnostics
                                       .GroupBy(d => d.StartDate)
                                       .Select(s => new DiagnosticsResult
                                       {
                                           StartDate = s.Key,
                                           TotalPriority = s.Where(e => e.EnergyPriority != 0).Count(),
                                           EnergyPriority = s.Where(e => e.EnergyPriority != 0).Any() ? s.Where(e => e.EnergyPriority != 0).Average(e => e.EnergyPriority) : 0,
                                       }).OrderBy(s => s.StartDate);

                if (energyDiagnostics.Any())
                    BindPrioritiesSeries(ChartType.Energy, energyDiagnostics, "Energy Area Series", "Average Energy Priority", ColorHelper.ConvertHexToSystemColor("#F0CF73"), "EnergyPriority",
                                         "Total Energy Incidents", "Energy Line Series", ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.EnergyYellow), "TotalPriority", "Energy Point Series");

                //maintenance/////////////////////////////////
                var maintenanceDiagnostics = past30DaysBuildingDiagnostics
                                             .GroupBy(d => d.StartDate)
                                             .Select(s => new DiagnosticsResult
                                             {
                                                 StartDate = s.Key,
                                                 TotalPriority = s.Where(m => m.MaintenancePriority != 0).Count(),
                                                 MaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                                             }).OrderBy(s => s.StartDate);

                if (maintenanceDiagnostics.Any())
                    BindPrioritiesSeries(ChartType.Maintenance, maintenanceDiagnostics, "Maintenance Area Series", "Average Maintenance Priority", Color.FromArgb(140, ColorHelper.ConvertHexToSystemColor("#9AC1ED")), "MaintenancePriority",
                                         "Total Maintenance Incidents", "Maintenance Line Series", ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue), "TotalPriority", "Maintenance Point Series");

                //comfort chart/////////////////////////////////
                var comfortDiagnostics = past30DaysBuildingDiagnostics
                                         .GroupBy(d => d.StartDate)
                                         .Select(s => new DiagnosticsResult
                                         {
                                             StartDate = s.Key,
                                             TotalPriority = s.Where(c => c.ComfortPriority != 0).Count(),
                                             ComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                                         }).OrderBy(s => s.StartDate);

                if (comfortDiagnostics.Any())
                    BindPrioritiesSeries(ChartType.Comfort, comfortDiagnostics, "Comfort Area Series", "Average Comfort Priority", Color.FromArgb(100, ColorHelper.ConvertHexToSystemColor("#F5BCCC")), "ComfortPriority",
                                         "Total Comfort Incidents", "Comfort Line Series", ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.FuchsiaRed), "TotalPriority", "Comfort Point Series");
            }

            private void BindPrioritiesSeries(ChartType chartType, IOrderedEnumerable<DiagnosticsResult> diagnosticsResults, String seriesName, String seriesLegendText, Color seriesColor, String seriesDataBindXYText, String series2LegendText,
                                              String series2Name, Color series2And3Color, String series2And3DataBindXYText, String series3Name)
            {
                prioritiesChart.ChartAreas["prioritiesChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days; //set axis intervaltype
                prioritiesChart.ChartAreas["prioritiesChartArea"].AxisX.LabelStyle.Format = "d"; //set axis format:  g = short date and time, d = short date

                if (chartType == ChartType.Comfort)
                {
                    prioritiesChart.ChartAreas["prioritiesChartArea"].AxisX.LabelStyle.Interval = 2;
                    //set major tick interval to match
                    prioritiesChart.ChartAreas["prioritiesChartArea"].AxisX.MajorTickMark.Interval = 2;
                }

                //area chart
                var series = new Series
                {
                    ChartType = SeriesChartType.Area,
                    Name = seriesName,
                    LegendText = seriesLegendText,
                    Color = seriesColor
                };

                series.YAxisType = AxisType.Secondary;
                series.Points.DataBindXY(diagnosticsResults.ToArray(), "StartDate", diagnosticsResults.ToArray(), seriesDataBindXYText);

                //interpolate
                prioritiesChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                prioritiesChart.Series.Add(series);

                //line
                series = new Series
                {
                    ChartType = SeriesChartType.FastLine,
                    BorderWidth = 3,
                    Name = series2Name,
                    LegendText = series2LegendText,
                    Color = series2And3Color
                };

                //add data to series
                series.Points.DataBindXY(diagnosticsResults.ToArray(), "StartDate", diagnosticsResults.ToArray(), series2And3DataBindXYText);

                //interpolate
                prioritiesChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                prioritiesChart.Series.Add(series);

                //point
                series = new Series
                {
                    ChartType = SeriesChartType.Point,
                    MarkerStyle = MarkerStyle.Circle,
                    ShadowColor = Color.DimGray,
                    ShadowOffset = 4,
                    MarkerSize = 6,
                    MarkerBorderWidth = 3,
                    MarkerBorderColor = series2And3Color,
                    MarkerColor = Color.White,
                    Name = series3Name,
                    ToolTip = "x: #VALX{d}, y: #VALY",
                    Legend = "Hidden"
                };

                series.Points.DataBindXY(diagnosticsResults.ToArray(), "StartDate", diagnosticsResults.ToArray(), series2And3DataBindXYText);

                //interpolate
                prioritiesChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Days, series);
                series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                prioritiesChart.Series.Add(series);
            }

            protected void prioiritiesChart_PrePaint(Object sender, ChartPaintEventArgs e)
            {
                prioritiesChart.Legends[0].Position.Width = 90;
                prioritiesChart.Legends[0].Position.X = 5;
                prioritiesChart.Legends[0].Position.Y = 80;
            }

        #endregion
    }
}