﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNavExpander.ascx.cs" Inherits="CW.Website._controls.LeftNavExapander" %>

<input type="checkbox" id="nav-trigger" class="nav-trigger" />

<label for="nav-trigger" class="btn-label">
        <span class="top"></span><span class="middle"></span><span class="bottom"></span><span class="label">MENU</span>
</label>

<div class="divLeftNav divLeftNavExpander">
    <div>
        <a title="Home Page" href="/Home.aspx">
            <div class="moduleIcon homeModuleIconWhite">
                <h1>Home</h1>       
            </div>                    
        </a>
    </div>
    <hr />
    <asp:Repeater ID="rptModules" runat="server">
        <HeaderTemplate>                                                               
        </HeaderTemplate>
        <ItemTemplate>   
            <div>
                <a title='<%# Eval("ModuleDescription") %>' href='<%# "/" + Eval("ModuleLink") %>'>
                    <div class='<%# "moduleIcon" + " " + Eval("ModuleName").ToString().Replace(" ", "").ToLower() + "ModuleIconWhite" %>'>
                        <h1><%# Eval("ModuleName") %></h1>       
                    </div>                    
                </a>
            </div>
            <hr />                                                                                                                                                                                                                                                           
        </ItemTemplate>
        <FooterTemplate>
        </FooterTemplate>
    </asp:Repeater>
</div>
