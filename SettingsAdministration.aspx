﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SettingsSettingsAdministration.aspx.cs" Inherits="CW.Website.SettingsAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
    
        <div id="intro" class="pod">
        	<div class="top"> </div>
            <div id="middle" class="middle">                    	                  	
            	<h1>Settings</h1>                                       
                <div class="richText">
                    <asp:Literal ID="litAdminSettingsBody" runat="server"></asp:Literal> 
                </div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
               <div>                                                                             
                    <hr />
                    <asp:Panel ID="pnlUpdateSettings" runat="server" DefaultButton="btnUpdateSettings"> 
                    <div>
                        <a id="lnkSetFocus" runat="server"></a>
                        <asp:Label ID="lblSettingsError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                    </div>                                                                                                                                                                                                                                                                                     
                    <div id="settings" visible="true" runat="server">                                                                               
                        <div class="divFormLeftmost">
                            <label>News Body (Max HTML Characters = 5000):</label>                                 
                            <telerik:RadEditor ID="editorNewsBody" 
                                                runat="server" 
                                                CssClass="editorNewLine"                                                  
                                                Height="475px" 
                                                Width="674px"
                                                MaxHtmlLength="5000"
                                                NewLineMode="Div"
                                                ToolsWidth="676px"                                                                                                                                       
                                                ToolbarMode="ShowOnFocus"        
                                                ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags" 
                                                EditModes="Design,Preview"                                                                                               
                                                >   
                                                <CssFiles>
                                                    <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                                </CssFiles>                                                                                
                             </telerik:RadEditor>
                             <!--<div class="textareaWideCharacterDiv" id="divEditorNewsBody"></div>-->
                        </div>
                        
                        <asp:LinkButton CssClass="lnkButton" ID="btnUpdateSettings" runat="server" Text="Update" OnClick="updateSettingsButton_Click" ValidationGroup="UpdateSettings"></asp:LinkButton>
                    </div>
                    </asp:Panel>                                                               
                </div>                                                                 
             </div>
        </div>                
</asp:Content>


                    
                  
