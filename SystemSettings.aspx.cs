﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using CW.Data;
using CW.Business;
using CW.Utility;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Logging;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class SystemSettings : SitePage
    {
        #region Properties

        const string updateSystemSettingsSuccessful = "System settings update was successful.";
        const string updateSystemSettingsFailed = "System settings update failed. Please contact an administrator.";

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check kgs system admin.
            if (!siteUser.IsKGSSystemAdmin)
                Response.Redirect("/Home.aspx");  

            //if the page is not a postback
            if (!Page.IsPostBack)
            {
                SetSettingsIntoEditForm();
            }
        }

        #endregion

        #region Get/Set Fields and Data

        protected void SetSettingsIntoEditForm()
        {
            //get and set system settings
            SystemSetting systemSettings = DataMgr.SystemSettingsDataMapper.GetSystemSettings();

            txtQACacheCIDs.Text = systemSettings.QACacheCIDs;
        }

        #endregion

        #region Load System Settings

        protected void LoadEditFormIntoSystemSetting(SystemSetting systemSetting)
        {
            //only one record, so id is not needed to be set. 
            //instead the first record is updated. shown in datamapper.

            systemSetting.QACacheCIDs = txtQACacheCIDs.Text;           
        }

        #endregion

        #region Tab Events

        #endregion

        #region Button Events

        /// <summary>
        /// Update settings Button on click.
        /// </summary>
        protected void updateSettingButton_Click(object sender, EventArgs e)
        {
            //create and load the form into the system settings
            SystemSetting mSystemSetting = new SystemSetting();
            LoadEditFormIntoSystemSetting(mSystemSetting);

            try
            {
                DataMgr.SystemSettingsDataMapper.UpdateSystemSetting(mSystemSetting);
                LabelHelper.SetLabelMessage(lblError, updateSystemSettingsSuccessful, lnkSetFocusHome);
            }
            catch (Exception ex)
            {
                LabelHelper.SetLabelMessage(lblError, updateSystemSettingsFailed, lnkSetFocusHome);
                LogMgr.Log(Common.Constants.DataConstants.LogLevel.ERROR, "Unable to update system settings.", ex);
            }
        }

        #endregion
    }
}
