﻿using CW.Utility;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Analyses.nonkgs;
using System;

namespace CW.Website
{
    public partial class AnalysisAdministration : AdminSitePageTemp
    {
        #region events

            private void Page_FirstLoad()
            {
                var globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings(); //set admin global settings. Make sure to decode from ascii to html 
        
                litAdminAnalysesBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.AdminAnalysesBody);
            }

        #endregion

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewAnalyses).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.NONKGS; } }

            #endregion

        #endregion
    }
}
