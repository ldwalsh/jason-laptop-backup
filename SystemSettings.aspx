﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemSettings.aspx.cs" Inherits="CW.Website.SystemSettings" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                 	                  	        	
        	<h1>System Settings</h1>                        
            <div class="richText">
                The system settings page is used to configure and manage critical system related settings and options.
            </div>   
             <div class="updateProgressDiv">
                 <asp:UpdateProgress ID="updateProgressTop" runat="server">
                     <ProgressTemplate>
                        <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                     </ProgressTemplate>
                 </asp:UpdateProgress>
             </div>
             <div class="administrationControls">
                 
                <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit">
                    <div>
                        <a id="lnkSetFocusHome" runat="server"></a>
                        <asp:Label ID="lblError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                    </div>

                    <div class="divForm">
                        <label class="label">QA Cache CID's (CSV):</label>
                        <asp:TextBox ID="txtQACacheCIDs" CssClass="textbox" runat="server" />

                          <ajaxToolkit:FilteredTextBoxExtender ID="qaCacheCIDsFilteredTextBoxExtender" runat="server" TargetControlID="txtQACacheCIDs" FilterType="Custom, Numbers" ValidChars="," />                          
                          <asp:RegularExpressionValidator ID="qaCacheCIDsRegExValidator" runat="server" ErrorMessage="Invalid CSV List Format." ValidationExpression="^[0-9]+(,[0-9]+)*$" ControlToValidate="txtQACacheCIDs" SetFocusOnError="true"  Display="None" ValidationGroup="Settings" />
                          <ajaxToolkit:ValidatorCalloutExtender ID="qaCacheCIDsRegExValidatorExtender" runat="server" BehaviorID="qaCacheCIDsRegExValidatorExtender"  TargetControlID="qaCacheCIDsRegExValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />

                    </div>
                    
                    <asp:LinkButton CssClass="lnkButton" ID="btnSubmit" runat="server" Text="Update" OnClick="updateSettingButton_Click" ValidationGroup="Settings"></asp:LinkButton>
                </asp:Panel>

             </div>

</asp:Content>            