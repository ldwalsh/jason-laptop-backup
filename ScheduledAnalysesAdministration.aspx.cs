﻿using CW.Utility;
using CW.Website._controls.admin;
using CW.Website._controls.admin.ScheduledAnalyses;
using System;

namespace CW.Website
{
    public partial class ScheduledAnalysesAdministration : AdminSitePageTemp
    {
        #region events

            private void Page_FirstLoad()
            {           
                //set admin global settings
                //Make sure to decode from ascii to html 
                var globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();

                litAdminScheduledAnalysesBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.AdminScheduledAnalysesBody);
            }

        #endregion

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewScheduledAnalyses).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.NONKGS; } }

            #endregion

        #endregion
    }
}
