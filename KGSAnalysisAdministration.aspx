﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSAnalysisAdministration.aspx.cs" Inherits="CW.Website.KGSAnalysisAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
    
                  	                  	
            	<h1>KGS Analysis Administration</h1>                        
                <div class="richText">The kgs analysis administration area is used to view, add, and edit analyses.</div>                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>                                                                    
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Analyses"></telerik:RadTab>
                            <telerik:RadTab Text="Add Analysis"></telerik:RadTab>
                            <telerik:RadTab Text="Build. Vars"></telerik:RadTab>
                            <telerik:RadTab Text="Equip. Vars"></telerik:RadTab>
                            <telerik:RadTab Text="Input Point Types"></telerik:RadTab>
                            <telerik:RadTab Text="Input VPoint Types"></telerik:RadTab>   
                            <telerik:RadTab Text="Output VPoint Types"></telerik:RadTab>    
                            <telerik:RadTab Text="Output VPrePoint Types"></telerik:RadTab>    
                            <telerik:RadTab Text="View Analysis Equip."></telerik:RadTab>                                                      
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">     
                                    <h2>View Analyses</h2> 
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                            
                                    </p>   
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>          
                                                                                                                
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridAnalyses"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="AID"  
                                             GridLines="None"                                      
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridAnalyses_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridAnalyses_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridAnalyses_Sorting"   
                                             OnSelectedIndexChanged="gridAnalyses_OnSelectedIndexChanged"                                                                                                             
                                             OnRowDeleting="gridAnalyses_Deleting"
                                             OnRowEditing="gridAnalyses_Editing"
                                             OnDataBound="gridAnalyses_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="AnalysisName" HeaderText="Analysis Name">  
                                                    <ItemTemplate><label title="<%# Eval("AnalysisName") %>"><%# StringHelper.TrimText(Eval("AnalysisName"),24) %></label></ItemTemplate>     
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Assembly File Name">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("AssemblyFileName"),20) %></ItemTemplate>     
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="AnalysisBaseFileName" HeaderText="Base File Name">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("AnalysisBaseFileName"),20) %></ItemTemplate>     
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="AnalysisFunctionName" HeaderText="Function Name">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("AnalysisFunctionName"),24) %></ItemTemplate>     
                                                </asp:TemplateField> 
                                                <asp:TemplateField SortExpression="IsMeta" HeaderText="Meta">  
                                                    <ItemTemplate><%# Eval("IsMeta")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="IsCustom" HeaderText="Custom">  
                                                    <ItemTemplate><%# Eval("IsCustom")%></ItemTemplate>                                                  
                                                </asp:TemplateField> 
                                                <asp:TemplateField SortExpression="IsUtility" HeaderText="Utility">  
                                                    <ItemTemplate><%# Eval("IsUtilityBilling")%></ItemTemplate>                                                  
                                                </asp:TemplateField>                                                                                                                                                                      
                                                <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this analysis permanently?\n\nAlternatively you can deactivate an analysis temporarily instead.');"
                                                               CommandName="Delete">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                                                          
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT ANALYSIS DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvAnalysis" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Analysis Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Analysis Name: </strong>" + Eval("AnalysisName") + "</li>"%> 
                                                            <%# "<li><strong>Assembly File Name: </strong>" + Eval("AssemblyFileName") + "</li>"%> 
                                                            <%# "<li><strong>Analysis Base File Name: </strong>" + Eval("AnalysisBaseFileName") + "</li>"%> 
                                                            <%# "<li><strong>Analysis Function Name: </strong>" + Eval("AnalysisFunctionName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("AnalysisTeaser"))) ? "" : "<li><strong>Analysis Teaser: </strong><div class='divContentPreWrap'>" + Eval("AnalysisTeaser") + "</div></li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("AnalysisDescription"))) ? "" : "<li><strong>Analysis Description: </strong><div class='divContentPreWrap'>" + Eval("AnalysisDescription") + "</div></li>"%>
                                                            <%# "<li><strong>Meta: </strong>" + Eval("IsMeta") + "</li>"%>
                                                            <%# "<li><strong>Custom: </strong>" + Eval("IsCustom") + "</li>"%>
                                                            <%# "<li><strong>Utility: </strong>" + Eval("IsUtilityBilling") + "</li>"%>
                                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>
                                                            <%# "<li><strong>Visible: </strong>" + Eval("IsVisible") + "</li>"%>
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>                                     
                                                                     
                                    <!--EDIT ANALYSIS PANEL -->                              
                                    <asp:Panel ID="pnlEditAnalysis" runat="server" Visible="false" DefaultButton="btnUpdateAnalysis">                                                                                             
                                              <div>
                                                    <h2>Edit Analysis</h2>
                                              </div>  
                                              <div> 
                                                    <a id="lnkSetFocusEdit" runat="server"></a>                                                     
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <asp:HiddenField ID="hdnEditIsCustom" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*Analysis Name:</label>
                                                        <asp:TextBox ID="txtEditAnalysisName" CssClass="textbox" MaxLength="150" runat="server"></asp:TextBox>                                                                                                            
                                                    </div>                                                    
                                                    <div class="divForm">   
                                                        <label class="label">*Assembly File:</label>   
                                                        <asp:DropDownList CssClass="dropdown" ID="ddlEditAssemblyFile" runat="server">                               
                                                        </asp:DropDownList> 
                                                        <p>(Warning: Changing the analysis assembly file may result in a miscommunication with the internally called matlab assembly.)</p>
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Analysis Base File Name:</label>
                                                        <asp:TextBox ID="txtEditAnalysisBaseFileName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>   
                                                        <p>(Warning: Changing the analysis base file name may result in a miscommunication with the internally called matlab base file.)</p>                                                                                                         
                                                    </div>                                                                                    
                                                    <div class="divForm">
                                                        <label class="label">*Analysis Function Name:</label>
                                                        <asp:TextBox ID="txtEditAnalysisFunctionName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                    
                                                        <p>(Warning: Changing the analysis function name may result in a miscommunication with the internally called matlab mFile analysis function.)</p>
                                                        <p>(Note: If you are updating to a differnt analysis function, the old analyzed data will still be associated.)</p>
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Analysis Teaser:</label>
                                                        <textarea name="txtEditAnalysisTeaser" id="txtEditAnalysisTeaser" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditAnalysisTeaserCharInfo')" runat="server"></textarea>
                                                        <div id="divEditAnalysisTeaserCharInfo"></div>    
                                                        <p>(Note: The teaser is what shows on analysis dropdown hovers, non kgs administration grids, diagnostic drilldown link hovers, equipment analyses list, and automated diagnostic emails.)</p>
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Analysis Description:</label>
                                                        <textarea name="txtEditAnalysisDescription" id="txtEditAnalysisDescription" cols="40" rows="5" onkeyup="limitChars(this, 5000, 'divEditAnalysisDescriptionCharInfo')" runat="server"></textarea>
                                                        <div id="divEditAnalysisDescriptionCharInfo"></div>                                                            
                                                    </div>  
                                                    <div class="divForm">
                                                        <label class="label">*Meta:</label>
                                                        <asp:CheckBox ID="chkEditMeta" Enabled="false" CssClass="checkbox" runat="server" />
                                                        <p>(Note: This propery cannot be changed.)</p>                                                        
                                                    </div> 
                                                    <div class="divForm">
                                                        <label class="label">*Custom:</label>
                                                        <asp:CheckBox ID="chkEditCustom" Enabled="false" CssClass="checkbox" runat="server" />
                                                        <p>(Note: This propery cannot be changed.)</p>                                                        
                                                    </div>      
                                                    <div id="divEditClientsListBoxs" runat="server" visible="false">
                                                        <hr />
                                                        <h2>Assign Clients</h2>
                                                        <p>(Note: You cannot remove an analysis from a client until all equipment have been disassociated.")
                                                        </p> 
                                                        <div class="divForm">                                                        
                                                            <label class="label">Client Access:</label>                                                                                                                                       
                                                            <asp:ListBox ID="lbEditClientsTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>
                                                            <div class="divArrows">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnEditClientsUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditClientsUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnEditClientsDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditClientsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                            </div>
                                                            <asp:ListBox ID="lbEditClientsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>                                                    
                                                        </div>
                                                    </div> 
                                                    <div class="divForm">
                                                        <label class="label">*Utility:</label>
                                                        <asp:CheckBox ID="chkEditUtility" Enabled="false" CssClass="checkbox" runat="server" />
                                                        <p>(Note: This propery cannot be changed.)</p>                                                        
                                                    </div>                                                                                                                                                                                                                                                                                                                                         
                                                    <div class="divForm">
                                                        <label class="label">*Active:</label>
                                                        <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />
                                                        <p>TODO: (Warning: Setting an analyses as inactive will disallow all those scheduled analyses to be put in the queue and ran.)</p>                                                        
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Visible:</label>
                                                        <asp:CheckBox ID="chkEditVisible" CssClass="checkbox" runat="server" />
                                                        <p>(Warning: Setting an analyses as not visible will hide the analysis on non administrative pages.)</p>                                                        
                                                    </div>                                
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateAnalysis" runat="server" Text="Update Analysis"  OnClick="updateAnalysisButton_Click" ValidationGroup="EditAnalysis"></asp:LinkButton>    
                                                </div>
                                                
                                                <!--Ajax Validators-->      
                                                <asp:RequiredFieldValidator ID="editAnalysisNameRequiredValidator" runat="server"
                                                    ErrorMessage="Analysis Name is a required field."
                                                    ControlToValidate="txtEditAnalysisName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditAnalysis">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editAnalysisNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editAnalysisNameRequiredValidatorExtender"
                                                    TargetControlID="editAnalysisNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                  
                                                <asp:RequiredFieldValidator ID="editAnalysisBaseFileNameRequiredValidator" runat="server"
                                                    ErrorMessage="Analysis Base File Name is a required field."
                                                    ControlToValidate="txtEditAnalysisBaseFileName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditAnalysis">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editAnalysisBaseFileNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editAnalysisBaseFileNameRequiredValidatorExtender"
                                                    TargetControlID="editAnalysisBaseFileNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>   
                                                <asp:RequiredFieldValidator ID="editAnalysisFunctionNameRequiredValidator" runat="server"
                                                    ErrorMessage="Analysis Function Name is a required field."
                                                    ControlToValidate="txtEditAnalysisFunctionName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditAnalysis">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editAnalysisFunctionNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editAnalysisFunctionNameRequiredValidatorExtender"
                                                    TargetControlID="editAnalysisFunctionNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                                    </asp:Panel>
                                                                                                         
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                    <asp:Panel ID="pnlAddAnalysis" runat="server" DefaultButton="btnAddAnalysis"> 
                                              <h2>Add Analysis</h2> 
                                              <div>     
                                                    <a id="lnkSetFocusAdd" runat="server"></a>                                               
                                                    <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <div class="divForm">
                                                        <label class="label">*Analysis Name:</label>
                                                        <asp:TextBox ID="txtAddAnalysisName" CssClass="textbox" MaxLength="150" runat="server"></asp:TextBox>                                                    
                                                    </div>                                                  
                                                    <div class="divForm">   
                                                        <label class="label">*Assembly File:</label>   
                                                        <asp:DropDownList CssClass="dropdown" ID="ddlAddAssemblyFile" AppendDataBoundItems="true" runat="server" >  
                                                             <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                                        </asp:DropDownList> 
                                                        <p>(Warning: The matlab assembly file refers to the internally called matlab assembly, and will not work if incorrect.)</p>                                              
                                                    </div>                                                       
                                                    <div class="divForm">
                                                        <label class="label">*Analysis Base File Name:</label>
                                                        <asp:TextBox ID="txtAddAnalysisBaseFileName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>      
                                                        <p>(Warning: The analysis base file name refers to the internally called matlab base file, and will not work if incorrect.)</p>                                              
                                                    </div>                                                                                                
                                                    <div class="divForm">
                                                        <label class="label">*Analysis Function Name:</label>
                                                        <asp:TextBox ID="txtAddAnalysisFunctionName" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>                                                                                                        
                                                        <p>(Warning: The analysis function name refers to the internally called matlab analysis function, and will not work if incorrect.)</p>
                                                    </div>   
                                                    <div class="divForm">
                                                         <label class="label">Analysis Teaser:</label>
                                                         <textarea name="txtAddAnalysisTeaser" id="txtAddAnalysisTeaser" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divAddAnalysisTeaserCharInfo')" runat="server"></textarea>
                                                         <div id="divAddAnalysisTeaserCharInfo"></div> 
                                                         <p>(Note: The teaser is what shows on analysis dropdown hovers, non kgs administration grids, diagnostic drilldown link hovers, equipment analyses list, and automated diagnostic emails.)</p>
                                                    </div>                                                                                                                                                                                         
                                                    <div class="divForm">
                                                         <label class="label">Analysis Description:</label>
                                                         <textarea name="txtAddAnalysisDescription" id="txtAddAnalysisDescription" cols="40" rows="5" onkeyup="limitChars(this, 5000, 'divAddAnalysisDescriptionCharInfo')" runat="server"></textarea>
                                                         <div id="divAddAnalysisDescriptionCharInfo"></div>  
                                                    </div>   
                                                    <div class="divForm">
                                                        <label class="label">*Meta:</label>
                                                        <asp:CheckBox ID="chkAddMeta" CssClass="checkbox" runat="server" /><%--TODO: OnCheckedChanged="chkAddMeta_OnCheckedChanged" AutoPostBack="true" --%>
                                                        <p>(Note: This propery cannot be changed later.)</p>
                                                    </div>   
                                                    <div id="divAddSelectVPointDataRanges" runat="server" visible="false">
                                                        <hr />
                                                        <h2>Select VPoint Data Ranges</h2>
                                                        <div class="divForm">  
                                                        </div>
                                                        <hr />
                                                    </div>                                                                                                                                               
                                                    <div class="divForm">
                                                        <label class="label">*Custom:</label>
                                                        <asp:CheckBox ID="chkAddCustom" CssClass="checkbox" OnCheckedChanged="chkAddCustom_OnCheckedChanged" AutoPostBack="true" runat="server" />                                                 
                                                        <p>(Note: This propery cannot be changed later.)</p>
                                                    </div>                                                                                                                                                                                                                                                                                           
                                                    <div id="divAddClientsListBoxs" runat="server" visible="false">
                                                        <hr />
                                                        <h2>Assign Clients</h2>
                                                        <div class="divForm">                                                        
                                                            <label class="label">Client Access:</label>                                                                                                                                       
                                                            <asp:ListBox ID="lbAddClientsTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>
                                                            <div class="divArrows">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnAddClientsUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnAddClientsUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnAddClientsDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnAddClientsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                            </div>
                                                            <asp:ListBox ID="lbAddClientsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>                                                    
                                                        </div>
                                                        <hr />
                                                    </div> 
                                                    <div class="divForm">
                                                        <label class="label">*Utility:</label>
                                                        <asp:CheckBox ID="chkAddUtility" CssClass="checkbox" runat="server" />                                                 
                                                        <p>(Note: This propery cannot be changed later.)</p>
                                                    </div> 
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnAddAnalysis" runat="server" Text="Add Analysis"  OnClick="addAnalysisButton_Click" ValidationGroup="AddAnalysis"></asp:LinkButton>    
                                                </div>
                                                
                                                <!--Ajax Validators-->      
                                                <asp:RequiredFieldValidator ID="addAnalysisNameRequiredValidator" runat="server"
                                                    ErrorMessage="Analysis Name is a required field."
                                                    ControlToValidate="txtAddAnalysisName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddAnalysis">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addAnalysisNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addAnalysisNameRequiredValidatorExtender"
                                                    TargetControlID="addAnalysisNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RequiredFieldValidator ID="addAssemblyFileRequiredValidator" runat="server"
                                                    ErrorMessage="Assembly File is a required field."
                                                    ControlToValidate="ddlAddAssemblyFile"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    InitialValue="-1"
                                                    ValidationGroup="AddAnalysis">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addAssemblyFileRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addAssemblyFileRequiredValidatorExtender"
                                                    TargetControlID="addAssemblyFileRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RequiredFieldValidator ID="addAnalysisBaseFileNameRequiredValidator" runat="server"
                                                    ErrorMessage="Analysis Base File Name is a required field."
                                                    ControlToValidate="txtAddAnalysisBaseFileName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddAnalysis">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addAnalysisBaseFileNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addAnalysisBaseFileNameRequiredValidatorExtender"
                                                    TargetControlID="addAnalysisBaseFileNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>
                                                <asp:RequiredFieldValidator ID="addAnalysisFunctionNameRequiredValidator" runat="server"
                                                    ErrorMessage="Analysis Function Name is a required field."
                                                    ControlToValidate="txtAddAnalysisFunctionName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="AddAnalysis">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addAnalysisFunctionNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addAnalysisFunctionNameRequiredValidatorExtender"
                                                    TargetControlID="addAnalysisFunctionNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                                                                                      
                                    </asp:Panel>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                            </telerik:RadPageView>     
                            <telerik:RadPageView ID="RadPageView3" runat="server">                                                                                                                                                                                                                                  
                                        <h2>Building Variables</h2>
                                        <p>
                                            Please select an analysis in order to select and assign building variables.
                                        </p>
                                        <div>
                                            <a id="lnkSetFocusBuildingVariables" runat="server"></a>
                                            <asp:Label ID="lblBuildingVariablesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div id="Div1" class="divForm" runat="server">   
                                            <label class="label">*Select Analysis:</label>    
                                            <asp:DropDownList ID="ddlBuildingVariablesAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildingVariablesAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="buildingVariables" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                All building variables are avaialable to an analysis. Please select only which apply.
                                            </p>                                           
                                            <div id="Div4" class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbBuildingVariablesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnBuildingVariablesUp"  OnClick="btnBuildingVariablesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnBuildingVariablesDown"  OnClick="btnBuildingVariablesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbBuildingVariablesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBuildingVariables" runat="server" Text="Reassign"  OnClick="updateBuildingVariablesButton_Click" ValidationGroup="UpdateBuildingVariables"></asp:LinkButton>    
                                        </div>                                                                                                    
                            </telerik:RadPageView>                                                       
                            <telerik:RadPageView ID="RadPageView4" runat="server">                                                                                                                                                                                                                                  
                                        <h2>Equipment Variables</h2>
                                        <p>
                                            Please select an analysis in order to select and assign equipment variables.
                                        </p>
                                        <div>
                                            <a id="lnkSetFocusEquipmentVariables" runat="server"></a>
                                            <asp:Label ID="lblEquipmentVariablesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Analysis:</label>    
                                            <asp:DropDownList ID="ddlEquipmentVariablesAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipmentVariablesAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="equipmentVariables" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                All equipment variables are avaialable to an analysis. Please select only which apply.
                                            </p>                                           
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbEquipmentVariablesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnEquipmentVariablesUp"  OnClick="btnEquipmentVariablesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnEquipmentVariablesDown"  OnClick="btnEquipmentVariablesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbEquipmentVariablesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentVariables" runat="server" Text="Reassign"  OnClick="updateEquipmentVariablesButton_Click" ValidationGroup="UpdateEquipmentVariables"></asp:LinkButton>    
                                        </div>                                                                                               
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView5" runat="server">                                                                                                                                                                                                                                  
                                    <h2>Input Point Types</h2>
                                    <p>
                                        Please select an analysis in order to select and assign input point types.
                                    </p>
                                        <div>
                                            <a id="lnkSetFocusTypes" runat="server"></a>
                                            <asp:Label ID="lblInputPointTypesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Analysis:</label>    
                                            <asp:DropDownList ID="ddlInputPointTypesAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlInputPointTypesAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="inputPointTypes" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                All Input Point Types are avaialable to an analysis. Please select only which apply.
                                            </p>                                           
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbInputPointTypesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnInputPointTypesUp"  OnClick="btnInputPointTypesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnInputPointTypesDown"  OnClick="btnInputPointTypesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbInputPointTypesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateInputPointTypes" runat="server" Text="Reassign"  OnClick="updateInputPointTypesButton_Click" ValidationGroup="UpdateInputPointTypes"></asp:LinkButton>    
                                        </div>                                                                                                                       
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView6" runat="server">                                                                                                                                                                                                      
                                    <h2>Input VPoint Types</h2>
                                    <p>
                                        Please select an analysis in order to select and assign input vpoint types. These inputs are only used for meta analyses.
                                    </p>
                                        <div>
                                            <a id="lnkSetFocusVPointTypes" runat="server"></a>
                                            <asp:Label ID="lblInputVPointTypesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div id="Div2" class="divForm" runat="server">   
                                            <label class="label">*Select Analysis:</label>    
                                            <asp:DropDownList ID="ddlInputVPointTypesAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlInputVPointTypesAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="inputVPointTypes" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                All Input VPoint Types are avaialable to an analysis. Please select only which apply.
                                            </p>                                           
                                            <div id="Div3" class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbInputVPointTypesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnInputVPointTypesUp"  OnClick="btnInputVPointTypesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnInputVPointTypesDown"  OnClick="btnInputVPointTypesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbInputVPointTypesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateInputVPointTypes" runat="server" Text="Reassign"  OnClick="updateInputVPointTypesButton_Click" ValidationGroup="UpdateInputVPointTypes"></asp:LinkButton>    
                                        </div>                                                                                                                      
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView7" runat="server">                                                                                                                                                                                                                                       
                                    <h2>Output VPoint Types</h2>
                                    <p>
                                        Please select an analysis in order to select and assign output vpoint types.
                                    </p>
                                    <p>
                                        (Warning: Assigning analysis ouput vpoint types will generate output vpoint associations for all equipment already associated with the selected analysis. This may take a while, please be patient.)
                                    </p>
                                    <p>
                                        (Warning: Unassigning analysis output vpoint types will remove output vpoint associations for all equipment already associated with the selected analysis.
                                        Once removed, all existing vpoint data for that vpoint type will not be available for all equipment with that analysis. This is irreversable.)
                                    </p>
                                    <p>
                                        (Note: You cannot remove an analysis output vpoint type if vdata or vpredata has already been generated for it.)
                                    </p>
                                    <p>
                                        (Note: Typically assigned variables - ComfortPriority, EnergyPriority, MaintenancePriority, CostSavings)
                                    </p> 
                                        <div>
                                            <a id="lnkSetFocusVPoint" runat="server"></a>
                                            <asp:Label ID="lblOutputVPointTypesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Analysis:</label>    
                                            <asp:DropDownList ID="ddlOutputVPointTypesAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlOutputVPointTypesAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="outputVPointTypes" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                All Output VPoint Types are available to an analysis. Please select only which apply.
                                            </p>                                           
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbOutputVPointTypesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnOutputVPointTypesUp"  OnClick="btnOutputVPointTypesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnOutputVPointTypesDown"  OnClick="btnOutputVPointTypesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbOutputVPointTypesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdatOutputVPointTypes" runat="server" Text="Reassign"  OnClick="updateOutputVPointTypesButton_Click" ValidationGroup="UpdateOutputVPointTypes"></asp:LinkButton>    
                                        </div>                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView8" runat="server">                                                                                                                                                                                                    
                                    <h2>Output VPrePoint Types</h2>
                                    <p>
                                       Please select an analysis in order to select and assign output vprepoint types.
                                    </p>
                                    <p>
                                        (Warning: Assigning analysis ouput vprepoint types will generate output vprepoint associations for all equipment already associated with the selected analysis. This may take a while, please be patient.)
                                    </p>
                                    <p>
                                        (Warning: Unassigning analysis output vprepoint types will remove output vprepoint associations for all equipment already associated with the selected analysis.
                                        Once removed, all existing vprepoint data for that vprepoint type will not be available for all equipment with that analysis. This is irreversable.)
                                    </p>
                                    <p>
                                        (Note: You cannot remove an analysis output vprepoint type if vdata or vpredata has already been generated for it.)
                                    </p>
                                        <div>
                                            <a id="lnkSetFocusVPrePoint" runat="server"></a>
                                            <asp:Label ID="lblOutputVPrePointTypesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        </div>
                                        <div class="divForm" runat="server">   
                                            <label class="label">*Select Analysis:</label>    
                                            <asp:DropDownList ID="ddlOutputVPrePointTypesAnalyses" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlOutputVPrePointTypesAnalyses_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>                                                                                                                                                                                                                                                                                      
                                        <div id="outputVPrePointTypes" visible="false" runat="server">                                            
                                            <hr /> 
                                            <p>
                                                All Output VPrePoint Types are available to an analysis. Please select only which apply.
                                            </p>                                           
                                            <div class="divForm" runat="server">                                       
                                                <label class="label">Available:</label> 
                                                <asp:ListBox ID="lbOutputVPrePointTypesTop" AppendDataBoundItems="false" CssClass="listbox" runat="server"  SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnOutputVPrePointTypesUp"  OnClick="btnOutputVPrePointTypesUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnOutputVPrePointTypesDown"  OnClick="btnOutputVPrePointTypesDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned:</label>
                                                <asp:ListBox ID="lbOutputVPrePointTypesBottom" AppendDataBoundItems="false" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                            </div>
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateOutputVPrePointTypes" runat="server" Text="Reassign"  OnClick="updateOutputVPrePointTypesButton_Click" ValidationGroup="UpdateOutputVPrePointTypes"></asp:LinkButton>    
                                        </div>                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView9" runat="server">                                                                                                                                                                                                       
                                    <h2>View Analysis Equipment</h2>
                                    <p>
                                    	Please select an analysis to view all equipment that is assigned to that analysis.    
                                    </p>
                                    <div>
                                        <a id="lnkSetFocusAnalyses" runat="server"></a>
                                        <asp:Label ID="lblAnalysesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                    </div>
                                    <div class="divForm" runat="server">   
                                        <label class="label">*Select Analysis:</label>    
                                            <asp:DropDownList ID="ddlAnalysisEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAnalysisEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                    </div>                                                                                          
                                    <div id="analysisEquipment" visible="false" runat="server">                                            
                                        <hr />   
                                        <h2>Equipment List</h2>
                                        <p>Building Name, Equipment Name: Equipment Description if available</p>
                                        <div>
                                            <asp:Label ID="lblAnalysisEquipmentEmpty" Visible="false" runat="server"></asp:Label>                  
                                        </div>                                                                                                                                                                                                                                                                                                                   
                                        <asp:Repeater ID="rptAnalysisEquipment" Visible="false" runat="server">
                                            <HeaderTemplate>                                                   
                                                <ul class="detailsListDefinitions">
                                            </HeaderTemplate>
                                            <ItemTemplate>                                                                                                                                                                                                                                              
                                                    <%# "<li><strong>" + Eval("Building.Client.ClientName") + ", " + Eval("Building.BuildingName") + ", " + Eval("EquipmentName") + "</strong>:<span>" + (String.IsNullOrEmpty(Convert.ToString(Eval("Description"))) ? "" : Eval("Description")) + "</span></li>"%>                                                   
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul>
                                            </FooterTemplate>
                                        </asp:Repeater>   
                                    </div>
                            </telerik:RadPageView>                                                                                                                                                                                        
                        </telerik:RadMultiPage>
                   </div>                                                                 
                                                            
</asp:Content>


                    
                  
