﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSUploadBuildings.aspx.cs" Inherits="CW.Website.KGSUploadBuildings" %>
<%@ Register src="~/_controls/upload/UploadHeader.ascx" tagname="UploadHeader" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/UploadFormat.ascx" tagname="UploadFormat" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupsBuildingUpload.ascx" tagname="IDLookupsBuildingUpload" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/Upload.ascx" tagname="Upload" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <CW:UploadHeader ID="UploadHeader" runat="server" />
  <CW:UploadFormat ID="UploadFormat" runat="server" />
  <CW:IDLookupsBuildingUpload ID="IDLookupsBuildingUpload" runat="server" />
  <CW:Upload ID="Upload" runat="server" />

</asp:Content>