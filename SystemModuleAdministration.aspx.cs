﻿using CW.Common.Constants;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class SystemModuleAdministration : SitePage
    {
        #region Properties

            private CW.Data.Module mModule;
            const string addModuleSuccess = " module addition was successful.";
            const string addModuleFailed = "Adding module failed. Please contact an administrator.";
            const string addModuleExists = "Cannot add module because the module already exists.";
            const string updateSuccessful = "Module update was successful.";
            const string updateFailed = "Module update failed. Please contact an administrator.";
            const string updateModuleExists = "Cannot update module because the modules already exists.";     
            //const string deleteSuccessful = "Module deletion was successful.";
            //const string deleteFailed = "Module deletion failed. Please contact an administrator.";            
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "ModuleName";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs system admin.
                if (!siteUser.IsKGSSystemAdmin)
                    Response.Redirect("/Home.aspx");   

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindModules();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetModuleIntoEditForm(CW.Data.Module module)
            {
                //ID
                hdnEditID.Value = Convert.ToString(module.ModuleID);
                //Module Name
                txtEditModuleName.Text = module.ModuleName;
                //Description
                txtEditDescription.Value = module.ModuleDescription;
                //Link
                txtEditModuleLink.Text = module.ModuleLink;
                //SortOrder
                txtEditModuleSortOrder.Text = Convert.ToString(module.SortOrder);
                //Active
                chkEditActive.Checked = module.IsActive;
            }

            #endregion

        #region Load and Bind Fields

        #endregion

        #region Load Module

            protected void LoadAddFormIntoModule(CW.Data.Module module)
            {
                //Module Name
                module.ModuleName = txtAddModuleName.Text;
                //Description
                module.ModuleDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                //Link
                module.ModuleLink = txtAddModuleLink.Text;
                //Sort Order
                module.SortOrder = Convert.ToInt16(txtAddModuleSortOrder.Text);

                module.IsActive = true;
                module.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoModule(CW.Data.Module module)
            {
                //ID
                module.ModuleID = Convert.ToInt32(hdnEditID.Value);
                //Module Name
                module.ModuleName = txtEditModuleName.Text;
                //Description
                module.ModuleDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //Link 
                module.ModuleLink = txtEditModuleLink.Text;
                //Sort Order
                module.SortOrder = Convert.ToInt16(txtEditModuleSortOrder.Text);

                module.IsActive = chkEditActive.Checked;
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add module Button on click.
            /// </summary>
            protected void addModuleButton_Click(object sender, EventArgs e)
            {
                mModule = new CW.Data.Module();

                //load the form into the module
                LoadAddFormIntoModule(mModule);

                try
                {
                    if (!DataMgr.ModuleDataMapper.DoesModuleExist(mModule.ModuleName))
                    {
                        //increment sort order for modules if placed in position that already exists
                        if (DataMgr.ModuleDataMapper.DoesModuleSortOrderExist(null, mModule.SortOrder))
                        {
                            foreach (CW.Data.Module module in DataMgr.ModuleDataMapper.GetAllModulesWithSortOrderEqualOrGreaterThan(mModule.SortOrder))
                            {
                                module.SortOrder++;
                                DataMgr.ModuleDataMapper.UpdateModule(module);
                            }
                        }

                        //insert new module
                        DataMgr.ModuleDataMapper.InsertModule(mModule);

                        LabelHelper.SetLabelMessage(lblAddError, mModule.ModuleName + addModuleSuccess, lnkSetFocusAdd);
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblAddError, addModuleExists, lnkSetFocusAdd);
                    }
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding module.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addModuleFailed, lnkSetFocusAdd); 
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding module.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addModuleFailed, lnkSetFocusAdd); 
                }
                
            }

            /// <summary>
            /// Update module Button on click. Updates module data.
            /// </summary>
            protected void updateModuleButton_Click(object sender, EventArgs e)
            {
                CW.Data.Module mModule = new CW.Data.Module();

                //load the form into the module
                LoadEditFormIntoModule(mModule);

                //try to update the module              
                try
                {
                    if (!DataMgr.ModuleDataMapper.DoesModuleExist(mModule.ModuleID, mModule.ModuleName))
                    {
                        //increment sort order for modules if placed in position that already exists
                        if (DataMgr.ModuleDataMapper.DoesModuleSortOrderExist(mModule.ModuleID, mModule.SortOrder))
                        {
                            foreach (CW.Data.Module module in DataMgr.ModuleDataMapper.GetAllModulesWithSortOrderEqualOrGreaterThan(mModule.SortOrder))
                            {
                                module.SortOrder++;
                                DataMgr.ModuleDataMapper.UpdateModule(module);
                            }
                        }

                        DataMgr.ModuleDataMapper.UpdateModule(mModule);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                        //Bind modules again
                        BindModules();
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateModuleExists, lnkSetFocusView);
                    }
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating module.", ex);
                }
             
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindModules();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindModules();
            }

        #endregion

        #region Dropdown events

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind module grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind modules to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindModules();
                }
            }

        #endregion

        #region Grid Events

            protected void gridModules_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridModules_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridModules_Editing(object sender, GridViewEditEventArgs e)
            {
                pnlEditModule.Visible = true;

                int moduleID = Convert.ToInt32(gridModules.DataKeys[e.NewEditIndex].Values["ModuleID"]);

                CW.Data.Module mModule = DataMgr.ModuleDataMapper.GetModule(moduleID);

                //Set module data
                SetModuleIntoEditForm(mModule);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridModules_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedModules(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridModules.DataSource = SortDataTable(dataTable, true);
                gridModules.PageIndex = e.NewPageIndex;
                gridModules.DataBind();
            }

            protected void gridModules_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridModules.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedModules(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridModules.DataSource = SortDataTable(dataTable, false);
                gridModules.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<CW.Data.Module> QueryModules()
            {
                try
                {
                    //get all modules 
                    return DataMgr.ModuleDataMapper.GetAllModules();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving modules.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving modules.", ex);
                    return null;
                }
            }

            private IEnumerable<CW.Data.Module> QuerySearchedModules(string searchText)
            {
                try
                {
                    //get all modules 
                    return DataMgr.ModuleDataMapper.GetAllSearchedModules(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving modules.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving modules.", ex);
                    return null;
                }
            }

            private void BindModules()
            {
                //query modules
                IEnumerable<CW.Data.Module> variables = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryModules() : QuerySearchedModules(txtSearch.Text);

                int count = variables.Count();

                gridModules.DataSource = variables;

                // bind grid
                gridModules.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedModules(string[] searchText)
            //{
            //    //query modules
            //    IEnumerable<CW.Data.Module> variables = QuerySearchedModules(searchText);

            //    int count = variables.Count();

            //    gridModules.DataSource = variables;

            //    // bind grid
            //    gridModules.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} module found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} modules found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No modules found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
      
        #endregion
    }
}

