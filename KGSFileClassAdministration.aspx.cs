﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSFileClassAdministration : SitePage
    {
        #region Properties

            private FileClass mFileClass;
            const string addFileClassSuccess = " file class addition was successful.";
            const string addFileClassFailed = "Adding file class failed. Please contact an administrator.";
            const string fileClassExists = "A file class with that name already exists.";
            const string updateSuccessful = "File class update was successful.";
            const string updateFailed = "File class update failed. Please contact an administrator.";
            const string deleteSuccessful = "File class deletion was successful.";
            const string deleteFailed = "File class deletion failed. Please contact an administrator.";
            const string fileTypeExists = "Cannot delete file class because file type is associated. Please disassociate from file type first.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "FileClassName";

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    //bind grid
                    BindFileClasses();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetFileClassIntoEditForm(CW.Data.FileClass fileClass)
            {
                //ID
                hdnEditID.Value = Convert.ToString(fileClass.FileClassID);
                //File Class Name
                txtEditFileClassName.Text = fileClass.FileClassName;
                
                //File Description
                txtEditDescription.Value = String.IsNullOrEmpty(fileClass.FileClassDescription) ? null : fileClass.FileClassDescription;
            }

        #endregion

        #region Get Fields and Data
                   
        #endregion

        #region Load and Bind Fields


            /// <summary>
            /// Binds a dropdownlist with all file classes
            /// </summary>
            /// <param name="ddl"></param>
            private void BindFileClasses(DropDownList ddl)
            {
                ddl.DataTextField = "FileClassName";
                ddl.DataValueField = "FileClassID";
                ddl.DataSource = DataMgr.FileClassDataMapper.GetAllFileClasses();
                ddl.DataBind();
            }

        #endregion

        #region Load File Class

            protected void LoadAddFormIntoFileClass(FileClass fileClass)
            {
                //File Class Name
                fileClass.FileClassName = txtAddFileClassName.Text;
                fileClass.FileClassDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                fileClass.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoFileClass(FileClass fileClass)
            {
                //ID
                fileClass.FileClassID = Convert.ToInt32(hdnEditID.Value);
                //File Class Name
                fileClass.FileClassName = txtEditFileClassName.Text;
                fileClass.FileClassDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                fileClass.DateModified = DateTime.UtcNow;
            }

        #endregion
        
        #region Dropdown Events
        
        #endregion

        #region Button Events

            /// <summary>
            /// Add File class Button on click.
            /// </summary>
            protected void addFileClassButton_Click(object sender, EventArgs e)
            {
                //check that file class does not already exist
                if (!DataMgr.FileClassDataMapper.DoesFileClassExist(txtAddFileClassName.Text))
                {
                    mFileClass = new FileClass();

                    //load the form into the file class
                    LoadAddFormIntoFileClass(mFileClass);

                    try
                    {
                        //insert new file class
                        DataMgr.FileClassDataMapper.InsertFileClass(mFileClass);

                        LabelHelper.SetLabelMessage(lblAddError, mFileClass.FileClassName + addFileClassSuccess, lnkSetFocusAdd);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding file class.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, mFileClass.FileClassName + addFileClassFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding file class.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, mFileClass.FileClassName + addFileClassFailed, lnkSetFocusAdd);
                    }
                }
                else
                {
                    //file class exists
                    LabelHelper.SetLabelMessage(lblAddError, fileClassExists, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update file class Button on click. Updates file class data.
            /// </summary>
            protected void updateFileClassButton_Click(object sender, EventArgs e)
            {
                FileClass mFileClass = new FileClass();

                //load the form into the file class
                LoadEditFormIntoFileClass(mFileClass);
                
                //check if the fileclass name exists for another fileclass if trying to change
                if (DataMgr.FileClassDataMapper.DoesFileClassExist(mFileClass.FileClassID, mFileClass.FileClassName))
                {
                    LabelHelper.SetLabelMessage(lblEditError, fileClassExists, lnkSetFocusView);
                }
                //update the File class
                else
                {
                    //try to update the file class              
                    try
                    {
                        DataMgr.FileClassDataMapper.UpdateFileClass(mFileClass);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                        //Bind file classes again
                        BindFileClasses();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating file class.", ex);
                    }
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindFileClasses();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindFileClasses();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind file class grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind file classes to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindFileClasses();
                }
            }

        #endregion

        #region Grid Events

            protected void gridFileClasses_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridFileClasses_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }
            protected void gridFileClasses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditFileClass.Visible = false;
                dtvFileClass.Visible = true;
                
                int fileClassID = Convert.ToInt32(gridFileClasses.DataKeys[gridFileClasses.SelectedIndex].Values["FileClassID"]);

                //set data source
                dtvFileClass.DataSource = DataMgr.FileClassDataMapper.GetFullFileClassByFileClassID(fileClassID);
                //bind file variable to details view
                dtvFileClass.DataBind();
            }

            protected void gridFileClasses_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvFileClass.Visible = false;
                pnlEditFileClass.Visible = true;

                int fileClassID = Convert.ToInt32(gridFileClasses.DataKeys[e.NewEditIndex].Values["FileClassID"]);

                FileClass mFileClass = DataMgr.FileClassDataMapper.GetFileClass(fileClassID);

                //Set File Variable data
                SetFileClassIntoEditForm(mFileClass);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridFileClasses_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows fileClassid
                int fileClassID = Convert.ToInt32(gridFileClasses.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a file class if no file type
                    //has been associated.                    

                    //check if file type is associated to that file class
                    if (DataMgr.FileTypeDataMapper.IsFileTypeAssociatedWithFileClass(fileClassID))
                    {
                        lblErrors.Text = fileTypeExists;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //delete file class
                        DataMgr.FileClassDataMapper.DeleteFileClass(fileClassID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting file class.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryFileClasses());
                gridFileClasses.PageIndex = gridFileClasses.PageIndex;
                gridFileClasses.DataSource = SortDataTable(dataTable as DataTable, true);
                gridFileClasses.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);
                
                //hide edit form if shown
                pnlEditFileClass.Visible = false;
            }

            protected void gridFileClasses_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedFileClasses(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridFileClasses.DataSource = SortDataTable(dataTable, true);
                gridFileClasses.PageIndex = e.NewPageIndex;
                gridFileClasses.DataBind();
            }

            protected void gridFileClasses_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridFileClasses.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedFileClasses(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridFileClasses.DataSource = SortDataTable(dataTable, false);
                gridFileClasses.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<FileClass> QueryFileClasses()
            {
                try
                {
                    //get all file classes 
                    return DataMgr.FileClassDataMapper.GetAllFileClasses();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving file classes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving file classes.", ex);
                    return null;
                }
            }

            private IEnumerable<FileClass> QuerySearchedFileClasses(string searchText)
            {
                try
                {
                    //get all file classes 
                    return DataMgr.FileClassDataMapper.GetAllSearchedFileClasses(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving file classes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving file classes.", ex);
                    return null;
                }
            }

            private void BindFileClasses()
            {
                //query file classes
                IEnumerable<FileClass> classes = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryFileClasses() : QuerySearchedFileClasses(txtSearch.Text);

                int count = classes.Count();

                gridFileClasses.DataSource = classes;

                // bind grid
                gridFileClasses.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedFileClasses(string[] searchtxt)
            //{
            //    //query file classes
            //    IEnumerable<FileClass> classes = QuerySearchedFileClasses(searchtxt);

            //    int count = classes.Count();

            //    gridFileClasses.DataSource = classes;

            //    // bind grid
            //    gridFileClasses.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} file class found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} file classes found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No file classes found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }
      
        #endregion
    }
}

