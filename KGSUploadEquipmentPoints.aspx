﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" AutoEventWireup="false" CodeBehind="KGSUploadEquipmentPoints.aspx.cs" Inherits="CW.Website.KGSUploadEquipmentPoints" %>
<%@ Register src="~/_controls/upload/UploadHeader.ascx" tagname="UploadHeader" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/UploadFormat.ascx" tagname="UploadFormat" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupsEquipmentPointUpload.ascx" tagname="IDLookupsEquipmentPointUpload" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/Upload.ascx" tagname="Upload" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <CW:UploadHeader ID="UploadHeader" runat="server" />
  <CW:UploadFormat ID="UploadFormat" runat="server" />
  <CW:IDLookupsEquipmentPointUpload ID="IDLookupsEquipmentPointUpload" runat="server" />
  <CW:Upload ID="Upload" runat="server" />

</asp:Content>