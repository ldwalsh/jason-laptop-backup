﻿using System;
using CW.Utility;
using CW.Website._framework;
using CW.Utility.Web;

namespace CW.Website._documents
{
    public sealed class DocumentsParamsObject: QueryString.IParamsObject
    {
        #region QueryString.IParamsObject

            Boolean QueryString.IParamsObject.RequirePropertyAttribute
            {
                get {return false;}
            }

        #endregion

        //public  Int32
        //        cid;

        //[
        //QueryString.Property(Requires="cid")
        //]
        public  Int32
                bid;

        [
        QueryString.Property(Optional=true, Requires="bid")
        ]
        public  Int32?
                eid;
    }
}