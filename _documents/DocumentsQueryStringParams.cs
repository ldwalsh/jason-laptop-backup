﻿using System;
using CW.Website._framework;

namespace CW.Website._documents
{
    public sealed class RepositoryQueryStringParams: QueryString.IQueryStringParams
    {
        [
        QueryString.QueryStringPropery
        ]
        public Int32
                cid;

        [
        QueryString.QueryStringPropery(requires = "cid")
        ]
        public Int32
                bid;

        [
        QueryString.QueryStringPropery(optional = true, requires = "cid")
        ]
        public Int32?
                eid;
    }
}