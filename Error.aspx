﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Error.master" EnableViewState="true" AutoEventWireup="true" CodeBehind="Error.aspx.cs" Inherits="CW.Website.Error" %>

<asp:Content ID="plcCopyContentJavaScript" ContentPlaceHolderID="plcCopyJavaScript" runat="server">
  
  <asp:HiddenField ID="hdnEmail" runat="server" />
  <asp:HiddenField ID="hdnBody" runat="server" />

  <script type="text/javascript">
      document.addEventListener('DOMContentLoaded', function () {
          var email = document.getElementById('<%= hdnEmail.ClientID %>').value;
          var subject = "Unhandled Exception Occurred";
          var body = document.getElementById('<%= hdnBody.ClientID %>').value;
          var links = document.getElementsByClassName("sendErrorReportLink");

          window.format = function (s, arg0, arg1, arg2) {

              for (var i = 1; i < arguments.length; i++)
                  s = s.replace(new RegExp("\\{" + (i - 1) + "\\}", "g"), arguments[i]);

              return s;
          }

          for (var l = 0; l < links.length; l++)
              links[l].href = format("mailto:{0}?subject={1}&body={2}", email, subject, encodeURIComponent(body));
      });
  </script>

</asp:Content>

<asp:Content ID="plcCopyContentPage" ContentPlaceHolderID="plcCopyPage" runat="server">
  <div style="text-align: center;">
    <h1 runat="server" id="headerErrorPage"></h1>
    <br />
    <p><asp:Literal ID="litOperationIdMessage" runat="server" /></p>
    <a class="sendErrorReportLink">Send Error Report</a>
  </div>
</asp:Content>

<asp:Content ID="plcCopyContentIframe" ContentPlaceHolderID="plcCopyIframe" runat="server">
  <div style="text-align: center;">
    <div runat="server" id="headerErrorIframe"></div>
    <a class="sendErrorReportLink">Send Error Report</a>
  </div>
</asp:Content>