﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/_masters/EnergyDashboard.master" AutoEventWireup="false" CodeBehind="EnergyDashboard.aspx.cs" Inherits="CW.Website.EnergyDashboard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <script type="text/javascript">    

        //Fixed with Telerik v2015.1.401.45
        //function onClientSeriesHovered(sender, args) {                        
        //    //set values after telerk sets them.
        //    setTimeout(function () {
        //        var element = sender.get_element().children[1];
        //        var paddingAndMargin = 8;
        //        var left = (parseInt(element.style.left));
        //        var tooltipWidth = parseInt(element.clientWidth);

        //        if ((left + tooltipWidth) > 940) {                  
        //            var newLeft = (left - tooltipWidth - paddingAndMargin) + "px";
        //            element.style.left = newLeft;
        //        }
        //    }, 425);            
        //}

        function onChartPostRenderResize(sender, args) {
            try {
                document.getElementById('<%=buildingOverviewTotalYearChart.ClientID %>').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('<%=buildingOverviewDrillDownYearChart.ClientID %>').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('<%=buildingOverviewTotalMonthChart.ClientID %>').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('<%=buildingOverviewDrillDownMonthChart.ClientID %>').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('<%=buildingOverviewTotalWeekChart.ClientID %>').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('<%=buildingOverviewDrillDownWeekChart.ClientID %>').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('<%=buildingOverviewTotalYesterdayChart.ClientID %>').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('<%=buildingOverviewDrillDownYesterdayChart.ClientID %>').style.width = '100%';
            } catch (e) {
            }
        }

    </script>
<telerik:RadAjaxManager ID="radAjaxManager" runat="server">  
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnGenerate">
                <UpdatedControls>                    
                    <telerik:AjaxUpdatedControl ControlID="pnlConsumption" LoadingPanelID="lpNested" />     
                    <telerik:AjaxUpdatedControl ControlID="pnlComparison" LoadingPanelID="lpNested" />           
                    <telerik:AjaxUpdatedControl ControlID="pnlConsumptionStatistics" LoadingPanelID="lpNested" />   
                    <telerik:AjaxUpdatedControl ControlID="pnlComparisonStatistics" LoadingPanelID="lpNested" />                
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="pnlMeterSelections">
                <UpdatedControls>                    
                    <telerik:AjaxUpdatedControl ControlID="pnlMeterStatistics" LoadingPanelID="lpNested" />                 
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="pnlMeterButtonsAndCharts">
                <UpdatedControls>                    
                    <telerik:AjaxUpdatedControl ControlID="pnlMeterStatistics" LoadingPanelID="lpNested" />                 
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
 </telerik:RadAjaxManager>

<telerik:RadAjaxLoadingPanel ID="lpNested" runat="server" Skin="Default" />  
     
<div id="energyDashboard" class="energyDashboard">					   
	<div id="dashboardHeader" class="dashboardHeader"> 
		<div class="performanceModuleIcon">
        </div>
		<div class="headerTitle">
			<h1>Energy Dashboard</h1>
		</div>
		<div class="headerSpacer"></div>
		<div class="headerClientInfo"> 
			<div class="headerClientImage">     
				<asp:Image ID="imgHeaderClientImage" CssClass="imgHeader" runat="server" />
			</div>          
			<div class="headerClientName">
				<label id="lblHeaderClientName" runat="server"></label>
			</div>
		</div>
        <div class="headerSpacer"></div>
        <div class="headerContent">
        <asp:Literal ID="litDashboardBody" runat="server"></asp:Literal>  
        </div>
	</div>

	<div id="dashboardBody" class="dashboardBody dockWrapper">
				            
        <telerik:RadDockLayout ID="radDockLayout" runat="server" EnableLayoutPersistence="false">
            <telerik:RadDockZone ID="radDockZone1" BorderStyle="None" runat="server" FitDocks="false" Orientation="Horizontal">                                        
                
                <telerik:RadDock ID="radDock1" CssClass="radDockLarge" DefaultCommands="ExpandCollapse" Title="Monthly Utility Consumption" runat="server" EnableRoundedCorners="true" Pinned="true" Resizable="false">                    
                    <ContentTemplate>
                    
                    <telerik:RadAjaxPanel ID="radUpdatePanelNested1" runat="server" UpdateMode="Conditional" LoadingPanelID="lpNested">   
  
                        <!--todo user control-->        
                        <div class="dockTopWide">                
                        <asp:HyperLink ID="lnkSelection" runat="server" CssClass="toggle"><asp:Label ID="lblSelection" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                        <br />
                        <ajaxToolkit:CollapsiblePanelExtender ID="consumptionCollapsiblePanelExtender" runat="Server"
                                        TargetControlID="pnlSelection"
                                        CollapsedSize="0"
                                        Collapsed="false" 
                                        ExpandControlID="lnkSelection"
                                        CollapseControlID="lnkSelection"
                                        AutoCollapse="false"
                                        AutoExpand="false"
                                        ScrollContents="false"
                                        ExpandDirection="Vertical"
                                        TextLabelID="lblSelection"
                                        CollapsedText="show selections"
                                        ExpandedText="hide selections" 
                                        />
                        <asp:Panel ID="pnlSelection" runat="server">
                            <div class="divDockFormWrapper">
                                <div class="divDockFormFirst">
                                    <label class="labelBold">*Comparison:</label>
                                    <asp:DropDownList CssClass="dropdown" ID="ddlComparison" OnSelectedIndexChanged="ddlComparison_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                                        
                                        <asp:ListItem Value="Baseline">Historical Baseline</asp:ListItem>   
                                        <asp:ListItem Selected="True" Value="Previous">Previous Year</asp:ListItem>                                       
                                    </asp:DropDownList> 
                                </div>
                                <div class="divDockForm">
                                    <label class="labelBold">*Utility:</label>
                                    <asp:DropDownList CssClass="dropdown" ID="ddlUtility" runat="server">
                                        <asp:ListItem Selected="True" Value="Total">Total</asp:ListItem> 
                                        <asp:ListItem Value="980|1293">Chilled Water</asp:ListItem> 
                                        <asp:ListItem Value="953|1294">Electric</asp:ListItem> 
                                        <asp:ListItem Value="976|1295">Gas</asp:ListItem> 
                                        <asp:ListItem Value="1269|1296">Hot Water</asp:ListItem>                                         
                                        <asp:ListItem Value="977|1297">Oil</asp:ListItem> 
                                        <asp:ListItem Value="978|1303">Propane</asp:ListItem> 
                                        <asp:ListItem Value="979|1298">Steam</asp:ListItem> 
                                        <asp:ListItem Value="981|1299">Water</asp:ListItem> 
                                    </asp:DropDownList> 
                                </div>
                                <div class="divDockForm">
                                    <label class="labelBold">*Building Type:</label>
                                    <asp:DropDownList ID="ddlBuildingTypes" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildingTypes_OnSelectedIndexChanged" AutoPostBack="true" runat="server" />
                                </div>                     
                                <div class="divDockForm">
                                    <label class="labelBold">*Building(s):</label>
                                    <extensions:ListBoxExtension ID="lbBuildings" runat="server" SelectionMode="Multiple" CssClass="listboxShortWide" />                                        
                                </div>   
                                <div class="divDockForm">
                                    <label class="labelBold">*Range (months):</label>
                                    <asp:DropDownList CssClass="dropdownNarrowest" ID="ddlRange" runat="server">
                                        <asp:ListItem Selected="True" Value="1">1</asp:ListItem> 
                                        <asp:ListItem Value="3">3</asp:ListItem> 
                                        <asp:ListItem Value="6">6</asp:ListItem> 
                                        <asp:ListItem Value="12">12</asp:ListItem> 
                                        <asp:ListItem Value="18">18</asp:ListItem> 
                                        <asp:ListItem Value="24">24</asp:ListItem> 
                                        <asp:ListItem Value="36">36</asp:ListItem> 
                                    </asp:DropDownList> 
                                </div>   
                                <div class="divDockForm">
                                    <label class="labelBold">*Metric:</label>
                                    <asp:DropDownList CssClass="dropdownNarrow" ID="ddlMetric" runat="server">
                                        <asp:ListItem Selected="True" Value="1">Energy</asp:ListItem>                                         
                                    </asp:DropDownList> 
                                        <!--<asp:ListItem Value="2">Cost</asp:ListItem> -->
                                </div>
                                <div class="divDockForm">
                                    <label class="labelBold">Normalization:</label>                                    
                                    <div>
                                        <asp:CheckBox ID="chkArea" CssClass="checkbox" runat="server"/><label class="labelCheckbox"> - Area</label>
                                    </div>
                                    <div id="divWeather" runat="server">
                                        <asp:CheckBox ID="chkWeather" CssClass="checkbox" runat="server"/><label class="labelCheckbox"> - Weather</label>
                                    </div>
                                </div>
                                <div id="divUnits" class="divDockForm" runat="server">
                                    <label class="labelBold">*Units:</label>
                                    <asp:DropDownList CssClass="dropdownNarrowest" ID="ddlUnits" OnSelectedIndexChanged="ddlUnits_OnSelectedIndexChanged"  AutoPostBack="true" runat="server">                                                                               
                                    </asp:DropDownList>           
                                </div>                                                
                                <div class="divDockForm">     
                                    <span class="labelBold">Refresh Data:</span>      
                                    <asp:LinkButton CssClass="lnkBtnRefreshDock" ID="btnGenerate" runat="server" Text=""  OnClick="generateButton_Click" ValidationGroup="Dashboard"></asp:LinkButton>
                                </div>   
                            </div>
                            
                        </asp:Panel>
                        </div>

                    </telerik:RadAjaxPanel>

                        <hr />

                    <asp:Panel ID="pnlConsumption" runat="server">
                        
                        <div class="dockBodyWide">
                                    
                            <div id="divConsumptionChart" runat="server">
							  <asp:Label ID="lblConsumptionResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
							  <telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" ResizeWithParentPane="true">
                              <telerik:RadPane Width="100%" runat="server" Scrolling="None">
                              <telerik:RadHtmlChart runat="server" Height="400px" ID="RadHtmlChart1">
                               <PlotArea>                                
                                    <Series>                                                      
                                    </Series>
                                    <XAxis>                                    
                                         <LabelsAppearance DataFormatString="MMM yyyy" RotationAngle="-45">
                                             <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" />
                                         </LabelsAppearance>
                                         <TitleAppearance Text="">
                                         </TitleAppearance>
                                    </XAxis>
                                    <YAxis>                                    
                                         <LabelsAppearance RotationAngle="0">
                                             <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" />
                                         </LabelsAppearance>                                        
                                         <TitleAppearance Text="">
                                             <TextStyle FontSize="9pt" Bold="true" FontFamily="Arial" Color="#454545" /> 
                                         </TitleAppearance>                                        
                                    </YAxis>                                   
                               </PlotArea>                                 
                               <Legend>
                                   <Appearance BackgroundColor="White" Position="Top">
                                       <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" />
                                   </Appearance>
                               </Legend>
                               <ChartTitle Text="">
                               </ChartTitle>
                            </telerik:RadHtmlChart>
                            </telerik:RadPane>  
                            </telerik:RadSplitter>                                                             
                         </div>
                    </div>  
                    </asp:Panel>             

                    </ContentTemplate>
                </telerik:RadDock>

                <telerik:RadDock  ID="radDock2" CssClass="radDockLarge" DefaultCommands="ExpandCollapse" Title="Monthly Utility Comparison" DockMode="Docked" runat="server" EnableRoundedCorners="true" Resizable="false">
                    <ContentTemplate>

                        <asp:Panel ID="pnlComparison" runat="server">

                         <div id="divComparisonChart" runat="server" visible="true">
						    <asp:Label ID="lblComparisonResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>

                            <telerik:RadSplitter ID="RadSplitter2" runat="server" Width="100%" ResizeWithParentPane="true">
                            <telerik:RadPane ID="RadPane1" Width="100%" runat="server" Scrolling="None">
                            <telerik:RadHtmlChart runat="server" Height="400px" ID="RadHtmlChart2" >
                               <PlotArea>                                  
                                    <Series>               
                                    </Series>
                                    <XAxis>                                    
                                         <LabelsAppearance DataFormatString="MMM yyyy" RotationAngle="-45">
                                             <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" />
                                         </LabelsAppearance>
                                         <TitleAppearance Text="">
                                         </TitleAppearance>
                                    </XAxis>
                                    <YAxis>                                    
                                         <LabelsAppearance DataFormatString="{0}%" RotationAngle="0">
                                             <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" />
                                         </LabelsAppearance>
                                         <TitleAppearance Text="">
                                         </TitleAppearance>                                        
                                    </YAxis>
                               </PlotArea>
                               <Legend>
                                   <Appearance BackgroundColor="White" Position="Top">
                                       <TextStyle FontSize="8pt" Bold="true" FontFamily="Arial" Color="#666666" />
                                   </Appearance>
                               </Legend>
                               <ChartTitle Text="">
                               </ChartTitle>
                            </telerik:RadHtmlChart>
                            </telerik:RadPane>
                            </telerik:RadSplitter>
                        </div>

                    </asp:Panel>

                    </ContentTemplate>
                </telerik:RadDock>

                <telerik:RadDock ID="radDock3" CssClass="radDockMedium" DefaultCommands="ExpandCollapse" DockMode="Docked" runat="server" Title="Consumption Statistics" EnableRoundedCorners="true" Resizable="false">
                    <ContentTemplate>
                     
                        <asp:Panel ID="pnlConsumptionStatistics" runat="server">
                        
                        <asp:Label ID="lblConsumptionStatistics" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
                        <div id="divConsumptionStatistics" runat="server">
                        <div>
                            <span>Total Overall Consumption: </span>
                            <span id="spanTotalConsumption" runat="server"></span>
                        </div>       
                        <div>
                            <span>Total Peak Month Consumption: </span>
                            <span id="spanPeakMonthConsumption" runat="server"></span>
                        </div>    
                        <div>
                            <span>Total Greatest Building Consumption: </span>
                            <span id="spanGreatestBuildingConsumption" runat="server"></span>
                        </div>  
                        </div>
                        </asp:Panel>

                    </ContentTemplate>
                </telerik:RadDock>

                <telerik:RadDock ID="radDock4" CssClass="radDockMedium" DefaultCommands="ExpandCollapse" DockMode="Docked" runat="server" Title="Comparison Statistics" EnableRoundedCorners="true" Resizable="false">
                    <ContentTemplate>
                     
                        <asp:Panel ID="pnlComparisonStatistics" runat="server">
						
                        <asp:Label ID="lblComparisonStatistics" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
                        <div id="divComparisonStatistics" runat="server">
                            <div>
                                <span>Total Overall Percentage Comparison: </span>
                                <span id="spanTotalComparison" runat="server"></span>
                            </div>       
                            <!--<div>
                                <span>Total Summed Percentages (Not to scale): </span>
                                <span id="spanPeakMonthComparison" runat="server"></span>
                            </div>-->
                        </div>

                        </asp:Panel>

                    </ContentTemplate>
                </telerik:RadDock>
                                
                <telerik:RadDock ID="radDock5" CssClass="radDockLarge" DefaultCommands="ExpandCollapse" DockMode="Docked" runat="server" Title="Building Electric Meters" EnableRoundedCorners="true" Resizable="false">
                    <ContentTemplate>

                    <telerik:RadAjaxPanel ClientEvents-OnResponseEnd="onChartPostRenderResize();" ID="radUpdatePanelNested5" runat="server" UpdateMode="Conditional" LoadingPanelID="lpNested">   
  
                        <!--todo user control-->        
                        <div class="dockTopWide">                
                        <asp:HyperLink ID="lnkSelection5" runat="server" CssClass="toggle"><asp:Label ID="lblSelection5" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                        <br />
                        <ajaxToolkit:CollapsiblePanelExtender ID="meterCollapsiblePanelExtender" runat="Server"
                                        TargetControlID="pnlMeterSelections"
                                        CollapsedSize="0"
                                        Collapsed="false" 
                                        ExpandControlID="lnkSelection5"
                                        CollapseControlID="lnkSelection5"
                                        AutoCollapse="false"
                                        AutoExpand="false"
                                        ScrollContents="false"
                                        ExpandDirection="Vertical"
                                        TextLabelID="lblSelection5"
                                        CollapsedText="show selections"
                                        ExpandedText="hide selections" 
                                        />
                        <asp:Panel ID="pnlMeterSelections" runat="server">
                            <div class="divDockFormWrapperShort">

                                <div class="divDockForm">
                                    <label class="labelBold">*Building:</label>
                                    <asp:DropDownList ID="ddlMeterBuilding" CssClass="dropdown" AppendDataBoundItems="true" runat="server" />
                                </div>
                                <div class="divDockForm">
                                    <label class="labelBold">*Normalization:</label>
                                    <asp:DropDownList CssClass="dropdown" ID="ddlMeterNormalization" runat="server">
                                        <asp:ListItem Selected="True" Value="1">None</asp:ListItem> 
                                        <asp:ListItem Value="2">Area</asp:ListItem>  
                                    </asp:DropDownList> 
                                </div>
                                <div class="divDockForm">  
                                    <label class="labelBold">Refresh Data:</label>          
                                    <asp:LinkButton CssClass="lnkBtnRefreshDock" ID="btnGenerateMeter" runat="server" Text=""  OnClick="generateMeterButton_Click" ValidationGroup="Dashboard"></asp:LinkButton>
                                </div>
                            </div>
                        </asp:Panel>
                        </div>

                        <hr />

                        <asp:Panel ID="pnlMeterButtonsAndCharts" runat="server">
                                    
                                <div class="divDockFormWrapperShortest">
                                <div class="divChartTypeButtons">								
                                    <asp:LinkButton CssClass="lnkBtnSmallestDock" ID="btnBuildingTotal" runat="server" Text="Total" OnClick="buildingTotalsButton_Click" />
									<asp:LinkButton CssClass="lnkBtnSmallestDock" ID="btnBuildingDrillDown" runat="server" Text="Drill Down" OnClick="buildingDrillDownButton_Click" />									                                    
								</div>
                                <div class="divButtonsSpacer">|</div>
                                <div class="divChartTypeButtons">									
                                    <asp:LinkButton CssClass="lnkBtnSmallestDock" ID="btnBuildingYesterday" runat="server" Text="Yesterday" OnClick="buildingYesterdayButton_Click" />
                                    <asp:LinkButton CssClass="lnkBtnSmallestDock" ID="btnBuildingWeek" runat="server" Text="Week" OnClick="buildingWeekButton_Click" />
									<asp:LinkButton CssClass="lnkBtnSmallestDock" ID="btnBuildingMonth" runat="server" Text="Month" OnClick="buildingMonthButton_Click" />									
                                    <asp:LinkButton CssClass="lnkBtnSmallestDock" ID="btnBuildingYear" runat="server" Text="Year" OnClick="buildingYearButton_Click" />
								</div>
                                </div>

                                <asp:Label ID="lblBuildingOverviewDefault" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>

                                <div id="divBuildingOverviewTotalYearResults" runat="server">
							        <asp:Label ID="lblBuildingOverviewTotalYearResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
														
                                    <asp:Chart ID="buildingOverviewTotalYearChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="500px" Width="1050px">
                                        <Titles>
                                            <asp:Title Name="buildingOverviewTotalYearTitle" ForeColor="#333333" Font="Arial MS, 9pt, style=Bold"></asp:Title>
                                        </Titles>                              
                                        <Series></Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="buildingOverviewTotalYearChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                                
                                                <AxisY LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MinorGrid LineColor="#CCCCCC" Enabled="true" />
                                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size=".5" />
                                                </AxisY>
                                                <AxisX Interval="1" LineColor="#666666" IsLabelAutoFit="false">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" IsStaggered="true" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>

                                <div id="divBuildingOverviewDrillDownYearResults" runat="server">
							        <asp:Label ID="lblBuildingOverviewDrillDownYearResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
														
                                    <asp:Chart ID="buildingOverviewDrillDownYearChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="500px" Width="1050px">
                                        <Titles>
                                            <asp:Title Name="buildingOverviewDrillDownYearTitle" ForeColor="#333333" Font="Arial MS, 9pt, style=Bold"></asp:Title>
                                        </Titles>
                                        <Legends>
                                            <asp:Legend Enabled="true" Alignment="Center" ForeColor="#666666" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 7pt, style=Bold"></asp:Legend>
                                        </Legends>                                
                                        <Series></Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="buildingOverviewDrillDownYearChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">   
                                                <AxisY LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MinorGrid LineColor="#CCCCCC" Enabled="true" />
                                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size=".5" />
                                                </AxisY>                                               
                                                <AxisX Interval="1" LineColor="#666666" IsLabelAutoFit="false">
                                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" IsStaggered="false" TruncatedLabels="false" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>

                                <div id="divBuildingOverviewTotalMonthResults" runat="server">
							        <asp:Label ID="lblBuildingOverviewTotalMonthResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
                                
                                    <asp:Chart ID="buildingOverviewTotalMonthChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="500px" Width="1050px">
                                        <Titles>
                                            <asp:Title Name="buildingOverviewTotalMonthTitle" ForeColor="#333333" Font="Arial MS, 9pt, style=Bold"></asp:Title>
                                        </Titles>                              
                                        <Series></Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="buildingOverviewTotalMonthChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                                <AxisY LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MinorGrid LineColor="#CCCCCC" Enabled="true" />
                                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size=".5" />
                                                </AxisY> 
                                                <AxisX Interval="1" LineColor="#666666" IsLabelAutoFit="false">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" IsStaggered="true" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>

                                <div id="divBuildingOverviewDrillDownMonthResults" runat="server">
							        <asp:Label ID="lblBuildingOverviewDrillDownMonthResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
                                
                                    <asp:Chart ID="buildingOverviewDrillDownMonthChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="500px" Width="1050px">
                                        <Titles>
                                            <asp:Title Name="buildingOverviewDrillDownMonthTitle" ForeColor="#333333" Font="Arial MS, 9pt, style=Bold"></asp:Title>
                                        </Titles>
                                        <Legends>
                                            <asp:Legend Enabled="true" Alignment="Center" ForeColor="#666666" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 7pt, style=Bold"></asp:Legend>
                                        </Legends>                                
                                        <Series></Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="buildingOverviewDrillDownMonthChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                                
                                                <AxisY LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MinorGrid LineColor="#CCCCCC" Enabled="true" />
                                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size=".5" />
                                                </AxisY> 
                                                <AxisX Interval="1" LineColor="#666666" IsLabelAutoFit="false">
                                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" IsStaggered="false" TruncatedLabels="false" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>

                                <div id="divBuildingOverviewTotalWeekResults" runat="server">
							        <asp:Label ID="lblBuildingOverviewTotalWeekResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
								
                                    <asp:Chart ID="buildingOverviewTotalWeekChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="500px" Width="1050px">
                                        <Titles>
                                            <asp:Title Name="buildingOverviewTotalWeekTitle" ForeColor="#333333" Font="Arial MS, 9pt, style=Bold"></asp:Title>
                                        </Titles>                                
                                        <Series></Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="buildingOverviewTotalWeekChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">                                                
                                                <AxisY LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MinorGrid LineColor="#CCCCCC" Enabled="true" />
                                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size=".5" />
                                                </AxisY> 
                                                <AxisX Interval="1" LineColor="#666666" IsLabelAutoFit="false">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" IsStaggered="true" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>

                                <div id="divBuildingOverviewDrillDownWeekResults" runat="server">
							        <asp:Label ID="lblBuildingOverviewDrillDownWeekResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
								
                                    <asp:Chart ID="buildingOverviewDrillDownWeekChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="500px" Width="1050px">
                                        <Titles>
                                            <asp:Title Name="buildingOverviewDrillDownWeekTitle" ForeColor="#333333" Font="Arial MS, 9pt, style=Bold"></asp:Title>
                                        </Titles>
                                        <Legends>
                                            <asp:Legend Enabled="true" Alignment="Center" ForeColor="#666666" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 7pt, style=Bold"></asp:Legend>
                                        </Legends>                                
                                        <Series></Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="buildingOverviewDrillDownWeekChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                                <AxisY LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MinorGrid LineColor="#CCCCCC" Enabled="true" />
                                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size=".5" />
                                                </AxisY> 
                                                <AxisX Interval="1" LineColor="#666666" IsLabelAutoFit="false">
                                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" IsStaggered="false" TruncatedLabels="false" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>


                                <div id="divBuildingOverviewTotalYesterdayResults" runat="server">
							        <asp:Label ID="lblBuildingOverviewTotalYesterdayResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
								
                                    <asp:Chart ID="buildingOverviewTotalYesterdayChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="500px" Width="1050px">
                                        <Titles>
                                            <asp:Title Name="buildingOverviewTotalYesterdayTitle" ForeColor="#333333" Font="Arial MS, 9pt, style=Bold"></asp:Title>
                                        </Titles>                                
                                        <Series></Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="buildingOverviewTotalYesterdayChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                                <AxisY LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MinorGrid LineColor="#CCCCCC" Enabled="true" />
                                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size=".5" />
                                                </AxisY> 
                                                <AxisX Interval="1" LineColor="#666666" IsLabelAutoFit="false">                                               
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" IsStaggered="true" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" />
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>

                                <div id="divBuildingOverviewDrillDownYesterdayResults" runat="server">
							        <asp:Label ID="lblBuildingOverviewDrillDownYesterdayResults" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>
								
                                    <asp:Chart ID="buildingOverviewDrillDownYesterdayChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="500px" Width="1050px">
                                        <Titles>
                                            <asp:Title Name="buildingOverviewDrillDownYesterdayTitle" ForeColor="#333333" Font="Arial MS, 9pt, style=Bold"></asp:Title>
                                        </Titles>
                                        <Legends>
                                            <asp:Legend Enabled="true" Alignment="Center" ForeColor="#666666" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 7pt, style=Bold"></asp:Legend>
                                        </Legends>                                
                                        <Series></Series>
                                        <ChartAreas>
                                            <asp:ChartArea Name="buildingOverviewDrillDownYesterdayChartArea" BorderColor="#CCCCCC" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                                <AxisY LineColor="#666666" LineWidth="1" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size=".5" />
                                                </AxisY> 
                                                <AxisX LineColor="#666666" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" IsStaggered="false" TruncatedLabels="false" />
                                                    <MajorGrid LineColor="#999999" />
                                                    <MajorTickMark LineColor="#666666" />                                                    
                                                </AxisX>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                    </asp:Chart>
                                </div>                                                       

                        </asp:Panel>

                    </telerik:RadAjaxPanel>

                    </ContentTemplate>
                </telerik:RadDock>

                <telerik:RadDock ID="radDock6" CssClass="radDockMedium" DefaultCommands="ExpandCollapse" DockMode="Docked" runat="server" Title="Building Electric Meter Statistics" EnableRoundedCorners="true" Resizable="false">
                    <ContentTemplate>
                     
                        <asp:Panel ID="pnlMeterStatistics" runat="server">
                            <asp:Label ID="lblBuildingOverviewStatisticsDefault" CssClass="dockMessageLabel" runat="server" Text="" Visible="false"></asp:Label>

                            <div>
                                <asp:Label ID="lblBuildingMeterSum" runat="server"></asp:Label>
                            </div>       
                        
                        </asp:Panel>

                    </ContentTemplate>
                </telerik:RadDock>

          </telerik:RadDockZone>
           
        </telerik:RadDockLayout>
                
    </div>
                      
	<div id="dashboardFooter" class="dashboardFooter">             
	</div>

</div>		
    
    
    <script type="text/javascript">
        onChartPostRenderResize();
    </script>	
    	
</asp:Content>
