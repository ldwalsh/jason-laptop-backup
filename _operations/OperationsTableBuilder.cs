﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website._operations
{
    public class TableBuilder
    {
        public Table table;

        public TableBuilder(string cssClass = "")
        {
            table = new Table();

            if (!String.IsNullOrEmpty(cssClass))
                table.Attributes.Add("class", cssClass);
        }

        public void AddRow(TableRow tr)
        {
            table.Rows.Add(tr);
        }

        public void AddCell(TableRow tr, string content, string cssClass = "", List<Control> controls = null)
        {
            TableCell tc = new TableCell();
            tc.Text = content;

            if (!String.IsNullOrEmpty(cssClass))
                tc.Attributes.Add("class", cssClass);

            if (controls != null)
            {
                foreach (Control c in controls)
                {
                    tc.Controls.Add(c);
                    tc.Controls.Add(new LiteralControl("<br />"));
                }
            }
            tr.Cells.Add(tc);
        }

        public void AddHeaderCell(TableHeaderRow thr, string content, string cssClass = "", int? colSpan = null)
        {
            TableHeaderCell thc = new TableHeaderCell();
            thc.Text = content;

            if (!String.IsNullOrEmpty(cssClass))
                thc.Attributes.Add("class", cssClass);

            if (colSpan != null)
                thc.ColumnSpan = (int)colSpan;

            thr.Cells.Add(thc);
        }
    }

}