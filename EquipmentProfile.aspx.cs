﻿using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Equipment;
using CW.Data.Models.EquipmentVariable;
using CW.Data.Models.Projects;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._diagnostics;
using CW.Website._documents;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class EquipmentProfile : SitePage
    {
        #region const

            String controlIdFormat = "{0}{1}";
            String hlCssClass = "blockLink";

        #endregion
        
        #region Properties

            const string equipmentVarsEmpty = "No equipment variables have been assigned.";

        #endregion

        #region fields

            private Int32 openStatusId = Convert.ToInt32(BusinessConstants.Status.Statuses.Open);
            int inProcessStatusId = Convert.ToInt32(BusinessConstants.Status.Statuses.InProcess);
            private List<TaskRecord> Tasks = new List<TaskRecord>();

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page
                //event validation is disabled in the page declaration due to a viewstate issue with the dropdownextender on page refresh.
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    int eid;

                    BindBuildingDropdownList(ddlBuildingsInitial);
                    BindBuildingDropdownList(ddlBuildings);

                    //if eid querystring value exists, bind profile
                    if (!String.IsNullOrEmpty(Request.QueryString["eid"]) && Int32.TryParse(Request.QueryString["eid"], out eid))
                    {
                        //check if equipment is associated with client
                        if (DataMgr.EquipmentDataMapper.IsEquipmentAssociatedWithClient(eid, siteUser.CID))
                        {
                            PanelVisibilityHelper(true);

                            GetEquipmentData equipment = DataMgr.EquipmentDataMapper.GetFullEquipmentByEID(eid).First();
                            ddlBuildings.SelectedValue = equipment.BID.ToString();
                            ddlBuildings_OnSelectedIndexChanged(ddlBuildings, null);
                            ddlEquipmentClasses.SelectedValue = equipment.EquipmentClassID.ToString();
                            ddlEquipmentClasses_OnSelectedIndexChanged(ddlEquipmentClasses, null);
                            ddlEquipment.SelectedValue = eid.ToString();
                            ddlEquipment_OnSelectedIndexChanged(ddlEquipment, null);                            
                        }
                        else
                        {
                            PanelVisibilityHelper(false);
                        }
                    }
                    else
                    {
                        PanelVisibilityHelper(false);
                    }

                    ModuleBlockVisibilityHelper();
                }
            }

        #endregion

        #region Load and Bind Fields

            private void BindBuildingDropdownList(DropDownList ddl)
            {
                //bool isKGSAdminOrHigher = RoleHelper.IsKGSAdminOrHigher(Session["UserRoleID"].ToString());

                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "BuildingName";
                ddl.DataValueField = "BID";

                //get all visible buildings by client id. get all regardless of active, as they once may have been active.                   
                IEnumerable<Building> buildings = siteUser.VisibleBuildings; //to get LoadWithState... BuildingDataMapper.GetAllVisibleBuildingsAssociatedToUserID(siteUser.UID, Convert.ToInt32(siteUser.CID), siteUser.IsRestricted);  
                ddl.DataSource = buildings;
                ddl.DataBind();

                //populate address in dropdown items by added a title attribute
                int counter = 1;
                foreach (Building b in buildings)
                {
                    //state not in visiblebiuldings
                    string fullAddress = String.Format("{0}, {1}", b.Address, b.City);

                    //need to temporarily enable if disabled
                    ddl.Enabled = true;

                    ddl.Items[counter].Attributes.Add("title", fullAddress);
                    counter++;
                }
            }

            /// <summary>
            /// Binds a dropdownlist with all equipment classes
            /// </summary>
            /// <param name="ddl"></param>
            private void BindEquipmentClasses(DropDownList ddl, int bid)
            {
                ddl.DataTextField = "EquipmentClassName";
                ddl.DataValueField = "EquipmentClassID";
                ddl.DataSource = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid);
                ddl.DataBind();
            }

            private void BindEquipmentDropdownList(DropDownList ddl, int bid, int equipmentClassID)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "EquipmentName";
                ddl.DataValueField = "EID";

                //get all visible equipment, regardless of its active state because we stll need to plot if it was once active.
                IEnumerable<Equipment> equipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBIDAndEquipmentClassIDWithClassAndType(bid, equipmentClassID);
                ddl.DataSource = equipment;
                ddl.DataBind();

                //populate class and type in dropdown items by added a title attribute
                int counter = 1;
                foreach (Equipment e in equipment)
                {
                    string classType = String.Format("{0}, {1}", e.EquipmentType.EquipmentClass.EquipmentClassName, e.EquipmentType.EquipmentTypeName);

                    ddl.Items[counter].Attributes.Add("title", classType);
                    counter++;
                }
            }

        #endregion

        #region Dropdown Events

            protected void ddlBuildingsInitial_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int bid = Convert.ToInt32(ddlBuildingsInitial.SelectedValue);

                //intial
                BindEquipmentClasses(ddlEquipmentClassesInitial, bid);

                //clear equipment
                ListItem lItem = new ListItem();
                lItem.Text = "Select equipment class first...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddlEquipmentInitial.Items.Clear();
                ddlEquipmentInitial.Items.Add(lItem);

                //non intial
                BindEquipmentClasses(ddlEquipmentClasses, bid);

                //clear equipment
                ListItem lItem2 = new ListItem();
                lItem2.Text = "Select equipment class first...";
                lItem2.Value = "-1";
                lItem2.Selected = true;

                ddlEquipment.Items.Clear();
                ddlEquipment.Items.Add(lItem2);
            }

            protected void ddlBuildings_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int bid = Convert.ToInt32(ddlBuildings.SelectedValue);

                //clear equipment class
                ListItem lItem = new ListItem();
                lItem.Text = "Select building first...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddlEquipmentClasses.Items.Clear();
                ddlEquipmentClasses.Items.Add(lItem);

                BindEquipmentClasses(ddlEquipmentClasses, bid);

                //clear equipment
                ListItem lItem2 = new ListItem();
                lItem2.Text = "Select equip. class first...";
                lItem2.Value = "-1";
                lItem2.Selected = true;

                ddlEquipment.Items.Clear();
                ddlEquipment.Items.Add(lItem2);
            }

            protected void ddlEquipmentClassesInitial_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindEquipmentDropdownList(ddlEquipmentInitial, Convert.ToInt32(ddlBuildingsInitial.SelectedValue), Convert.ToInt32(ddlEquipmentClassesInitial.SelectedValue));

                BindEquipmentDropdownList(ddlEquipment, Convert.ToInt32(ddlBuildingsInitial.SelectedValue), Convert.ToInt32(ddlEquipmentClassesInitial.SelectedValue));
            }

            protected void ddlEquipmentClasses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindEquipmentDropdownList(ddlEquipment, Convert.ToInt32(ddlBuildings.SelectedValue), Convert.ToInt32(ddlEquipmentClasses.SelectedValue));
            }

            protected void ddlEquipmentInitial_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlEquipmentInitial.SelectedValue == "-1") return;

                int eid = Convert.ToInt32(ddlEquipmentInitial.SelectedValue);
                int bid = Convert.ToInt32(ddlBuildingsInitial.SelectedValue);
                int equipmentClassID = Convert.ToInt32(ddlEquipmentClassesInitial.SelectedValue);

                ddlBuildings.SelectedValue = bid.ToString();
                ddlEquipmentClasses.SelectedValue = equipmentClassID.ToString();
                ddlEquipment.SelectedValue = eid.ToString();

                lnkBack.NavigateUrl = "/BuildingProfile.aspx?bid=" + bid.ToString();

                PanelVisibilityHelper(true);

                BindProfile(siteUser.CID, bid, equipmentClassID, eid);
            }

            protected void ddlEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlEquipment.SelectedValue == "-1") return;

                lnkBack.NavigateUrl = "/BuildingProfile.aspx?bid=" + ddlBuildings.SelectedValue;

                BindProfile(siteUser.CID, Convert.ToInt32(ddlBuildings.SelectedValue), Convert.ToInt32(ddlEquipmentClasses.SelectedValue), Convert.ToInt32(ddlEquipment.SelectedValue));
            }

        #endregion

        #region Button Events

            /// <summary>
			/// Show serviced edit Button on click.
			/// </summary>
            protected void showEditEquipmentServicedPanelButton_Click(object sender, EventArgs e)
            {
                servicedDetailsCollapsiblePanelExtender.Collapsed = false;
                servicedDetailsCollapsiblePanelExtender.ClientState = "false";
                dtvEquipmentServiced.Visible = false;
                pnlEditEquipmentServiced.Visible = true;
            }

            /// <summary>
            /// Update serviced information
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateServicedButton_Click(object sender, EventArgs e)
            {
                Equipment equipment = new Equipment();

                equipment.EID = Convert.ToInt32(ddlEquipment.SelectedValue);

                //LastServicedBy
                equipment.LastServicedBy = String.IsNullOrEmpty(txtEditLastServicedBy.Text) ? null : txtEditLastServicedBy.Text;
                //LastServicedDate
                equipment.LastServicedDate = txtEditLastServicedDate.SelectedDate;
                //LastServicedCompany
                equipment.LastServicedCompany = String.IsNullOrEmpty(txtEditLastServicedCompany.Text) ? null : txtEditLastServicedCompany.Text;
                
                DataMgr.EquipmentDataMapper.UpdateEquipmentServicedInformation(equipment);

                List<Equipment> list = new List<Equipment>();
                list.Add(equipment);

                dtvEquipmentServiced.DataSource = list; 
                dtvEquipmentServiced.DataBind();

                dtvEquipmentServiced.Visible = true;
                pnlEditEquipmentServiced.Visible = false;
            }

            /// <summary>
            /// Cancels edit template, and shows itemtemplate button on click.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void cancelServicedButton_Click(object sender, EventArgs e)
            {
                dtvEquipmentServiced.Visible = true;
                pnlEditEquipmentServiced.Visible = false;
            }

        #endregion

        #region ListView Events

            protected void lvTasks_ItemDataBound(Object sender, ListViewItemEventArgs e)
            {
                var hlTask = (HyperLink)e.Item.FindControl($"lnk{typeof(TaskRecord).Name}");
                var lblSummary = (Label)e.Item.FindControl($"lbl{PropHelper.G<TaskRecord>(_ => _.Summary)}");
                var task = (TaskRecord)e.Item.DataItem;
                var timeZone = task.Analyses_Equipment.Equipment.Building.TimeZone;
                var taskSummary = task.Summary;
                
                hlTask.NavigateUrl = LinkHelper.BuildTaskQuickLinkForSingleTask(task);
                hlTask.Text = task.AnalysisStartDate.ToShortDateString();
                hlTask.Target = "_blank";
                hlTask.CssClass = hlCssClass;

                lblSummary.Text = $"{new string(taskSummary.Take(25).ToArray())}...";
                lblSummary.ToolTip = taskSummary;
            }

        #endregion

        #region Profile Events

            private void BindProfile(int cid, int bid, int equipmentClassID, int eid)
            {
                //do equipment based binds
                DoEquipmentBasedBinds(eid);

                //Bind equipment varaiables list
                BindEquipmentVariables(eid);

                DateTime sd;
                DateTime ed;

                List<Int32> openStatusIds = new List<int>() { openStatusId, inProcessStatusId };
                
                DateTimeHelper.GenerateDefaultDatesYesterday(out sd, out ed, siteUser.EarliestBuildingTimeZoneID);
                
                //Get Tasks
                if (divTasksBlock.Visible)
                    Tasks = new TaskRecordQuery(DataMgr.ConnectionString, siteUser).LoadWith<Analyses_Equipment, Analyses_Equipment, Equipment>(_ => _.Analyses_Equipment, _ => _.Equipment, _ => _.Analyse, _ => _.Building)
                                                         .FinalizeLoadWith.GetAll(siteUser.CID).ByStatus(openStatusIds).ByEquipment(eid).Queryable.OrderByDescending(_ => _.DateCreated).Take(10).ToList();

                BindModuleLinks(cid, bid, equipmentClassID, eid, sd, ed);

                BindModuleLists(cid, bid, equipmentClassID, eid, sd, ed);

                if (siteUser.IsSuperAdminOrHigher)
                {
                    //set edit variables href link
                    lnkEditVars.HRef = "EquipmentAdministration?tab=2&bid=" + bid + "&eid=" + eid;
                }
                else
                {
                    lnkEditVars.Visible = false;
                }
            }

            private void BindModuleLinks(int cid, int bid, int equipmentClassID, int eid, DateTime sd, DateTime ed)
            {                
                //lnkReports.HRef =
                //    QueryString.Create
                //    (
                //        "~/Reports.aspx?",
                //        new ReportsParamsObject
                //        {
                //            cid = cid,
                //            bid = bid,
                //            ecid = equipmentClassID,
                //            eid = eid,
                //        }
                //    );

                lnkDocuments.HRef =
                    QueryString.Create
                    (
                        "~/Documents.aspx?",
                        new DocumentsParamsObject
                        {
                            //cid = cid,
                            bid = bid,
                            eid = eid,
                        }
                    );

                lnkDiagnostics.Attributes.Add("href", "Diagnostics.aspx?cid=" + cid + "&bid=" + bid + "&ecid=" + equipmentClassID + "&eid=" + eid + "&rng=" + DataConstants.AnalysisRange.Daily + "&sd=" + sd + "&ed=" + ed);
                //lnkDocuments.Attributes.Add("href", "Documents.aspx?cid=" + cid + "&bid=" + bid + "&eid=" + eid);
                lnkReports.Attributes.Add("href", "Reports.aspx?cid=" + cid + "&bid=" + bid + "&ecid=" + equipmentClassID + "&eid=" + eid);
                lnkProjects.Attributes.Add("href", "Projects.aspx?cid=" + cid + "&bid=" + bid + "&ecid=" + equipmentClassID + "&eid=" + eid);
                lnkTasks.Attributes.Add("href", LinkHelper.BuildTaskQuickLinkForGoLink(bid, equipmentClassID, eid, siteUser.CID, Tasks));
            }

            private void BindModuleLists(int cid, int bid, int ecid, int eid, DateTime sd, DateTime ed)
            {
                if (divDiagnosticsBlock.Visible)
                {
                    //Diagnostics top 10
                    var pb = new DiagnosticsBinder(siteUser, DataMgr);
                    var bp = new _diagnostics.DiagnosticsBinder.BindingParameters();
                    bp.cid = cid;
                    bp.bids = new List<int>(new int[] { bid });
                    bp.equipmentClassID = ecid;
                    bp.equipmentID = eid;
                    bp.range = DataConstants.AnalysisRange.Daily;
                    bp.start = sd;
                    bp.end = ed;

                    var results = pb.BuildTopDiagnosticsResults(bp, Convert.ToByte("10"));

                    if (results.Any())
                    {
                        lvDiagnostics.DataSource = results;
                        lvDiagnostics.DataBind();
                    }

                    divDiagnosticsTop10.Visible = results.Any();
                }

                if (divDocumentsBlock.Visible)
                {
                    //Documents top 10 viewed
                    var results2 = DataMgr.FileDataMapper.GetAllTopViewedFilesByEID(eid, 10);

                    if (results2.Any())
                    {
                        lvDocuments.DataSource = results2;
                        lvDocuments.DataBind();
                    }

                    divDocumentsTop10.Visible = results2.Any();
                }

                if (divProjectsBlock.Visible)
                {
                    //projects top 10
                    var results3 = DataMgr.ProjectDataMapper.GetTopGroupedProjectAssociations(new ProjectsInputs() { CID = cid, EID = eid }, 10);

                    if (results3.Any())
                    {
                        lvProjects.DataSource = results3;
                        lvProjects.DataBind();
                    }

                    divProjectsTop10.Visible = results3.Any();
                }

                if (divTasksBlock.Visible)
                {   
                    var hasTasks = Tasks.Any();

                    if (hasTasks)
                    {
                        lvTasks.DataSource = Tasks;
                        lvTasks.DataBind();
                    }

                    divTasksTop10.Visible = hasTasks;
                }
            }

            /// <summary>
            /// Binds equipment variables repeater list by eid           
            /// </summary>
            /// <param name="eid"></param>
            private void BindEquipmentVariables(int eid)
            {
                //get equipment variables associated to equipment 
                IEnumerable<GetEquipmentEquipmentVariableData> mEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesWithDataByEID(eid);

                if (mEquipmentVariables.Any())
                {
                    //bind equipment variabels to repeater
                    rptEquipmentVars.DataSource = mEquipmentVariables;
                    rptEquipmentVars.DataBind();
                    rptEquipmentVars.Visible = true;

                    lblEquipmentVarsEmpty.Visible = false;
                }
                else
                {
                    rptEquipmentVars.Visible = false;
                    lblEquipmentVarsEmpty.Text = equipmentVarsEmpty;
                    lblEquipmentVarsEmpty.Visible = true;
                }

                //show info
                equipmentVariables.Visible = true;
            }

            /// <summary>
            /// Do Binds by eid           
            /// </summary>
            /// <param name="eid"></param>
            private void DoEquipmentBasedBinds(int eid)
            {
                IEnumerable<GetEquipmentData>  equipment = DataMgr.EquipmentDataMapper.GetFullEquipmentByEID(eid);

                header.InnerText = "Equipment Profile: " + equipment.First().EquipmentName;

                BindEquipmentTeaser(equipment);

                BindEquipmentDetails(equipment);

                BindServicedDetails(equipment);

                BindAdditionalDetails(equipment);

                BindExternalLinks(equipment);
            }

            /// <summary>
            /// Binds equipment teaser  
            /// </summary>
            /// <param name="equipment"></param>
            private void BindEquipmentTeaser(IEnumerable<GetEquipmentData> equipment)
            {
                GetEquipmentData e = equipment.First();

                //literal
                litTeaser.Text = String.Format("<div>Equipment <strong class='profileTeaserColor'>{0}</strong> of Type <strong class='profileTeaserColor'>{1}</strong> is located <strong class='profileTeaserColor'>{2}</strong> in Building <strong class='profileTeaserColor'>{3}</strong></div>", e.EquipmentName, e.EquipmentTypeName, e.EquipmentLocation, e.BuildingName);
                litTeaser.Text += String.IsNullOrEmpty(e.Description) ? "" : "<br /><div><strong>Description: </strong>" + e.Description + "</div>";                     
            }

            /// <summary>
            /// Binds equipment details  
            /// </summary>
            /// <param name="equipment"></param>
            private void BindEquipmentDetails(IEnumerable<GetEquipmentData> equipment)
            {
                GetEquipmentData e = equipment.First();

                imgProfile.Visible = true;
                imgProfile.Alt = e.EquipmentName;
                imgProfile.Src = HandlerHelper.EquipmentImageUrl(e.CID, e.EID, e.ImageExtension);

                //radImgProfile.DefaultImageVirtualPath = "~/" + ConfigurationManager.AppSettings["ImageAssetPath"] + "no-equipment-image.png";

                //// Before doing a blob lookup, check if image extension exists in relational database
                //// Save on transactions ($$$)
                //if (!String.IsNullOrEmpty(e.ImageExtension))
                //{
                //    var blob = mDataManager.EquipmentDataMapper.GetEquipmentImage(e.BID, e.EID, e.ImageExtension);

                //    radImgProfile.DataValue = blob != null ? blob.Image : null;
                //}

                //// Before doing a blob lookup, check if image extension exists in relational database
                //// Save on transactions ($$$)
                //if (String.IsNullOrEmpty(e.ImageExtension))
                //{
                //    DisplayNoEquipmentImage();
                //}
                //else
                //{
                //    //new EquipmentBlobDataContext(mAccount, e.EID).GetBlob(EquipmentBlobName.GenerateForProfileImage(e.BID, e.EID, e.ImageExtension));
                //    var blob = mDataManager.EquipmentDataMapper.GetEquipmentImage(e.BID, e.EID, e.ImageExtension);

                //    if (blob == null)
                //    {
                //        DisplayNoEquipmentImage();
                //    }
                //    else
                //    {
                //        imgProfile.Src = TempFileHandler.GetURL(sessionState, TempFileHandler.CreateTempFile(sessionState, String.Empty, blob.Image));
                //        imgProfile.Alt = e.EquipmentName;
                //    }
                //}

                //set data source
                dtvEquipment.DataSource = new List<GetEquipmentData>( equipment );
                //bind Equipment to details view
                dtvEquipment.DataBind();
            }

            //private void DisplayNoEquipmentImage()
            //{
            //    imgProfile.Src = ConfigurationManager.AppSettings["ImageAssetPath"] + "no-equipment-image.png";
            //    imgProfile.Alt = "no equipment image";
            //}

            /// <summary>
            /// Binds servcied equipment details       
            /// </summary>
            /// <param name="equipment"></param>
            private void BindServicedDetails(IEnumerable<GetEquipmentData> equipment)
            {
                //set data source
                dtvEquipmentServiced.DataSource = equipment;
                //bind Equipment to details view
                dtvEquipmentServiced.DataBind();

                GetEquipmentData firstEquipment = equipment.First();

                //LastServicedBy
                txtEditLastServicedBy.Text = firstEquipment.LastServicedBy;
                //LastServicedDate
                txtEditLastServicedDate.SelectedDate = firstEquipment.LastServicedDate;
                //LastServicedCompany
                txtEditLastServicedCompany.Text = firstEquipment.LastServicedCompany;
            }

            /// <summary>
            /// Binds additional equipment details       
            /// </summary>
            /// <param name="equipment"></param>
            private void BindAdditionalDetails(IEnumerable<GetEquipmentData> equipment)
            {
                //set data source
                dtvEquipmentAdditional.DataSource = equipment;
                //bind Equipment to details view
                dtvEquipmentAdditional.DataBind();
            }

            /// <summary>
            /// Binds external links     
            /// </summary>
            /// <param name="equipment"></param>
            private void BindExternalLinks(IEnumerable<GetEquipmentData> equipment)
            {
                //set data source
                dtvExternalLinks.DataSource = equipment;
                //bind Equipment to details view
                dtvExternalLinks.DataBind();
            }

        #endregion

        #region Helper Methods

            protected void PanelVisibilityHelper(bool hideInitialState)
            {
                pnlInitialSelection.Visible = !hideInitialState;
                pnlProfile.Visible = hideInitialState;                
            }

            protected void ModuleBlockVisibilityHelper()
            {
                divDiagnosticsBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Diagnostics));
                divReportsBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Reporting));
                divDocumentsBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Documents));
                divProjectsBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Projects));
                divTasksBlock.Visible = siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Tasks));
            }

            String FormatControlString(params Object[] args) { return String.Format(controlIdFormat, args); }

        #endregion
    }
}