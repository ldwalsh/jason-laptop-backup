﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Windows.Controls;

using CW.Data;
using CW.Business;
using CW.Data.AzureStorage.Models;
using CW.Data.Models;
using CW.Data.Models.Diagnostics;
using CW.Data.Models.Raw;
using CW.Data.Models.Equipment;
using CW.Data.Models.EquipmentVariable;
using CW.Data.Models.Building;

namespace CW.Website
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDashboardService2" in both code and config file together.
    [Obsolete]
    [ServiceContract]
    public interface IDashboardService
    {
        [OperationContract]
        IEnumerable<GetRawPlotData> GetLiveData(int userID, int buildingID);

        [OperationContract]
        IEnumerable<Building> GetBuildingData();

        [OperationContract]
        List<object> GetBuildingImage(int buidlingID, string buildingExtension, string buildingName);

        //DEPRICATED RAR
        //[OperationContract]
        //List<List<object>> GetAllClientData(string userRoleID);

        //DEPRICATED RAR
        //[OperationContract]
        //List<List<object>> GetAllProviderAdminClientData(int userID);

        [OperationContract]
        GetBuildingsData GetFullBuildingData(int buildingID);

        [OperationContract]
        IEnumerable<GetEquipmentData> GetFullEquipmentData(int equipmentID);

        [OperationContract]
        IEnumerable<GetEquipmentEquipmentVariableData> GetAllEquipmentVariablesByEID(int equipmentID);

        [OperationContract]
        List<List<object>> GetAllEquipmentClassData();

        [OperationContract]
        List<List<object>> GetEquipmentClassInBuildingData(int buildingID, int equimentClassID);

        [OperationContract]
        List<EngUnit> GetPointEngUnit(List<int> PIDs);

        [OperationContract]
        List<string[]> GetPointInfo(List<int> PIDs);

        [OperationContract]
        List<string[]> GetPointInfoByEID(int eid);

        [OperationContract]
        List<List<double>> GetPerformance(int bid, int uid, string roleID, int cid, DateTime currentDate);

        [OperationContract]
        IEnumerable<DiagnosticsResult> GetBuildingPerformance(int bid, int uid, int cid, DateTime startDate, DateTime endDate, CommonDataConstants.AnalysisRange range, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);

        //[OperationContract]
        //IEnumerable<Priority> GetPast52WeeksBuildingPerformance(int bid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);
        
        //[OperationContract]
        //IEnumerable<Priority> GetPast36MonthsBuildingPerformance(int bid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);

        //[OperationContract]
        //List<List<object>> GetPastMonthStatistics(int bid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);

        //[OperationContract]
        //List<List<object>> GetPastMonthEquipmentClassStatistics(int bid, int uid, string roleID, int cid, DateTime currentDate, int classID, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);

        //[OperationContract]
        //List<List<object>> GetPastMonthEquipmentStatistics(int bid, int eid, int uid, string roleID, int clientID, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);

        //[OperationContract]
        //List<List<object>> GetPastYearStatistics(int bid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);

        //[OperationContract]
        //List<List<object>> GetPastYearEquipmentClassStatistics(int bid, int uid, string roleID, int cid, DateTime currentDate, int classID, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);

        //[OperationContract]
        //List<List<object>> GetPastYearEquipmentStatistics(int bid, int eid, int uid, string roleID, int cid, DateTime currentDate, bool isKGSFullAdminOrHigher, bool isSuperAdminOrFullAdminOrFullUser);

        //DEPRICATED RAR
        //[OperationContract]
        //Client GetClientData(string clientID);

        [OperationContract]
        IEnumerable<Point> GetAllPointsByEID(int eid);

        [OperationContract]
        IEnumerable<GetRawPlotData> GetLiveDataForPIDs(List<int> pids);

        [OperationContract]
        IEnumerable<GetRawPlotData> GetPast30DataForPIDs(IEnumerable<int> pids, DateTime startDate);

        [OperationContract]
        List<GetRawPlotData> UpdateLiveDataForPIDs(List<int> pids);

        //DEPRICATED DASHBOARD OVERHAUL
        //[OperationContract]
        //string GetDashboardContent();
    }
}
