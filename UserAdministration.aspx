﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="UserAdministration.aspx.cs" Inherits="CW.Website.UserAdministration" %>
<%@ Register tagPrefix="CW" tagName="UserDownloadArea" src="~/_controls/admin/Users/UserDownloadArea.ascx" %>
<%@ Register tagPrefix="CW" tagName="UserAuditDownloadArea" src="~/_controls/admin/Users/UserAuditDownloadArea.ascx" %>
<%@ Register tagPrefix="CW" tagName="TabHeader" src="~/_administration/TabHeader.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="CW.Website" Namespace="CW.Website._extensions" TagPrefix="extensions" %>
<%@ Import Namespace="CW.Utility" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                     	                  	
            	<h1>User Administration</h1>                        
                <div class="richText">
                    <asp:Literal ID="litAdminUsersBody" runat="server"></asp:Literal> 
                </div>  
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Users"></telerik:RadTab>
                            <telerik:RadTab Text="Add User"></telerik:RadTab>
                            <telerik:RadTab Text="Users Audit"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                <CW:TabHeader runat="server" ID="tabHeader" Title="View Users">
                                  <RightAreaTemplate>
                                    <CW:UserDownloadArea runat="server" ID="UserDownloadArea" />
                                  </RightAreaTemplate>
                                </CW:TabHeader>

                                    <p>
                                        <a id="lnkSetFocusView" href="#" runat="server"></a>
                                        <asp:Label ID="lblResults" runat="server" CssClass="successMessage" />
                                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false" />
                                    </p>    
                                                         
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" visible="false">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>
                                    <!-- NOTE: Do not move controls around in the grid as on row databound uses column and control positions to show and hide controls. -->                                                                     
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridUsers"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="UID"  
                                             GridLines="None"                                      
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridUsers_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridUsers_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridUsers_Sorting"   
                                             OnSelectedIndexChanged="gridUsers_OnSelectedIndexChanged"                                                                                                             
                                             OnRowDeleting="gridUsers_Deleting" 
                                             OnRowEditing="gridUsers_Editing"
                                             OnDataBound="gridUsers_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"     
                                             OnRowCommand="gridUsers_OnRowCommand"                                                                              
                                             > 
                                             <Columns>
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <div><asp:LinkButton runat="server" CausesValidation="false" Text="Edit User" CommandName="Edit"></asp:LinkButton></div>
                                                        <div><asp:LinkButton runat="server" CausesValidation="false" Text="Edit User Client Roles" CommandArgument="EditClientRoles" CommandName="EditClientRoles"></asp:LinkButton></div>
                                                        <div><asp:LinkButton runat="server" CausesValidation="false" Text="Edit User Client Modules" CommandArgument="EditClientModules" CommandName="EditClientModules"></asp:LinkButton></div>                                                                              
                                                        <div><asp:LinkButton runat="server" CausesValidation="false" Text="Edit User Client QuickLinks" CommandArgument="EditClientQuickLinks" CommandName="EditClientQuickLinks"></asp:LinkButton></div>                                                        
                                                        <div><asp:LinkButton runat="server" CausesValidation="false" Text="Edit User Client Buildings" CommandArgument="EditBuildings" CommandName="EditBuildings"></asp:LinkButton></div>                                               
                                                        <div><asp:LinkButton Visible="false" runat="server" CausesValidation="false" Text="Edit User Client Building Modules" CommandArgument="EditBuildingModules" CommandName="EditBuildingModules"></asp:LinkButton></div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                               
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Email" HeaderText="Email">                                                      
                                                    <ItemTemplate><label title="<%# Eval("Email") %>"><%# StringHelper.TrimText(Eval("Email"),40) %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FirstName" HeaderText="First Name">                                                      
                                                    <ItemTemplate><label title="<%# Eval("FirstName") %>"><%# StringHelper.TrimText(Eval("FirstName"),15) %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="LastName" HeaderText="Last Name">                                                      
                                                    <ItemTemplate><label title="<%# Eval("LastName") %>"><%# StringHelper.TrimText(Eval("LastName"),20) %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PrimaryRoleName" HeaderText="Primary Role">  
                                                    <ItemTemplate><%# String.IsNullOrEmpty(Convert.ToString(Eval("PrimaryRoleName"))) ? "" : Eval("PrimaryRoleName")%>                                                                                                                
                                                        <asp:HiddenField ID="hdnPrimaryRoleID" runat="server" Visible="true" Value='<%# Eval("PrimaryRoleID")%>' />
                                                        <!--<asp:HiddenField ID="hdnLowestRoleID" runat="server" Visible="true" Value='# Eval("LowestRoleID")' />-->
                                                    </ItemTemplate>                                               
                                                </asp:TemplateField>
                                                <asp:TemplateField SortExpression="IsActive" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this user permanently?\n\nAlternatively you can deactivate a user temporarily instead.');"
                                                               CommandName="Delete" Text="Delete"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView> 
                                                                          
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT USER DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvUser" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>User Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# Eval("FirstName") == null && Eval("LastName") == null ? "" : "<li><strong>Name: </strong><span class=\"\">" + (String.IsNullOrEmpty(Convert.ToString(Eval("FirstName"))) ? "" : Eval("FirstName") + " ") + (String.IsNullOrEmpty(Convert.ToString(Eval("LastName"))) ? "" : Eval("LastName")) + "</span></li>"%> 
                                                            <%# "<li><strong>Email: </strong><a href=\"mailto:" + Eval("Email") + "\">" + Eval("Email") + "</a></li>"%>                                                               
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Address"))) ? "" :"<li><strong>Address: </strong>" + Eval("Address") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("City"))) ? "" :"<li><strong>City: </strong>" + Eval("City") + "</li>"%> 
                                                            <%# "<li><strong>Country: </strong>" + Eval("CountryName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("StateName"))) ? "" : "<li><strong>State: </strong>" + Eval("StateName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Zip"))) ? "" : "<li><strong>Zip: </strong>" + Eval("Zip") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("Phone"))) ? "" : "<li><strong>Phone: </strong>" + Eval("Phone") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("MobilePhone"))) ? "" : "<li><strong>Mobile Phone: </strong>" + Eval("MobilePhone") + "</li>"%>
                                                            <%# "<li><string>Culture: </strong>" + CultureHelper.GetCultureDisplayName(Convert.ToInt32(Eval("LCID"))) + "</li>"%>
                                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>                                                	                           
                                                            <%# ((System.Collections.Generic.List<CW.Data.Models.Client.GetClientsWithRoleFormat>)Eval("Clients")).Count > 0 ? (((System.Collections.Generic.List<CW.Data.Models.Client.GetClientsWithRoleFormat>)Eval("Clients")).Count > 1 ? "<li><strong>List of Clients with Role: </strong>" + GenerateClientList((System.Collections.Generic.List<CW.Data.Models.Client.GetClientsWithRoleFormat>)Eval("Clients")) + "</li>" : "<li><strong>Client and Role: </strong>" + GenerateClientList((System.Collections.Generic.List<CW.Data.Models.Client.GetClientsWithRoleFormat>)Eval("Clients")) + "</li>") : "<li><strong>Primary Role: </strong>" + Eval("PrimaryRoleName") + "</li>"%>
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                                                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>                                     
                                                                     
                                    <!--EDIT USER PANEL -->                              
                                    <asp:Panel ID="pnlEditUsers" runat="server" Visible="false" DefaultButton="btnUpdateUser">                                                                                             
                                              <div>
                                                    <h2>Edit User</h2>
                                              </div>  
                                              <div> 
                                                    <a id="lnkEditSetFocusView" href="#" runat="server"></a>                                                   
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server" />              
                                                    <asp:HiddenField ID="hdnEditUID" runat="server" Visible="false" />
                                                    <asp:HiddenField ID="hdnEditOID" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*First Name:</label>
                                                        <asp:TextBox ID="txtEditFirstName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                        <asp:RequiredFieldValidator ID="editFirstNameRequiredValidator" runat="server"
                                                            ErrorMessage="First Name is a required field."
                                                            ControlToValidate="txtEditFirstName"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editFirstNameRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editFirstNameRequiredValidatorExtender"
                                                            TargetControlID="editFirstNameRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender> 
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Last Name:</label>
                                                        <asp:TextBox ID="txtEditLastName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                                                                        
                                                        <asp:RequiredFieldValidator ID="editLastNameRequiredValidator" runat="server"
                                                            ErrorMessage="Last Name is a required field."
                                                            ControlToValidate="txtEditLastName"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editLastNameRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editLastNameRequiredValidatorExtender"
                                                            TargetControlID="editLastNameRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div>
                                                    <div class="divForm">
                                                        <div class="label" style="display: inline !important">
                                                            Email:
                                                        </div>
                                                        <div id="emailAddress" class="label" style="display: inline!important; position:relative!important; text-align:left!important">
                                                            <label id="txtEmail" runat="server"></label>
                                                        </div>
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">Address:</label>
                                                        <asp:TextBox ID="txtEditAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                              
                                                    </div>                                                                                                                                                                                           
                                                    <div class="divForm">
                                                         <label class="label">City:</label>
                                                         <asp:TextBox CssClass="textbox" ID="txtEditCity" runat="server" MaxLength="100"></asp:TextBox>                                                        
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">*Country:</label>
                                                         <asp:DropDownList CssClass="dropdown" ID="ddlEditCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                
                                                         </asp:DropDownList>
                                                         <asp:RequiredFieldValidator ID="editCountryRequiredValidator" runat="server"
                                                            ErrorMessage="Country is a required field." 
                                                            ControlToValidate="ddlEditCountry"  
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            InitialValue="-1"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                         <ajaxToolkit:ValidatorCalloutExtender ID="editCountryRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editCountryRequiredValidatorExtender" 
                                                            TargetControlID="editCountryRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender> 
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">State (Required based on Country):</label>
                                                         <asp:DropDownList ID="ddlEditState" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                         <asp:ListItem Value="-1">Select country first...</asp:ListItem>
                                                         </asp:DropDownList>  
                                                         <asp:RequiredFieldValidator ID="editStateRequiredValidator" runat="server"
                                                            ErrorMessage="State is a required field." 
                                                            ControlToValidate="ddlEditState"  
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            InitialValue="-1"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editStateRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editStateRequiredValidatorExtender" 
                                                            TargetControlID="editStateRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div>
                                                    <div class="divForm">
                                                         <label class="label">Zip Code (Required based on Country):</label>
                                                         <telerik:RadMaskedTextBox ID="txtEditZip" CssClass="textbox" runat="server" 
                                                                PromptChar="" ValidationGroup="EditUser"></telerik:RadMaskedTextBox>    
                                                         <asp:RequiredFieldValidator ID="editZipRequiredValidator" runat="server"
                                                            ErrorMessage="Zip Code is a required field."
                                                            ControlToValidate="txtEditZip"          
                                                            SetFocusOnError="true"
                                                            Display="None"
                                                            ValidationGroup="EditUser"
                                                            Enabled="false">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editZipRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editZipRequiredValidatorExtender"
                                                            TargetControlID="editZipRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                        <asp:RegularExpressionValidator ID="editZipRegExValidator" runat="server"
                                                            ErrorMessage="Invalid Zip Format."
                                                            ValidationExpression="\d{5}"
                                                            ControlToValidate="txtEditZip"
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditUser"
                                                            Enabled="false">
                                                            </asp:RegularExpressionValidator>  
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editZipRegExValidatorExtender" runat="server"
                                                            BehaviorID="editZipRegExValidatorExtender"
                                                            TargetControlID="editZipRegExValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div>                                                    
                                                    <div class="divForm">
                                                         <label class="label">Phone:</label>
                                                         <telerik:RadMaskedTextBox ID="txtEditPhone" CssClass="textbox" runat="server"
                                                            Mask="(###)###-####" ValidationGroup="EditUser"></telerik:RadMaskedTextBox>     
                                                         <asp:RegularExpressionValidator ID="editPhoneRegExValidator" runat="server"
                                                            ErrorMessage="Invalid Phone Format."
                                                            ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                                            ControlToValidate="txtEditPhone"
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditUser">
                                                            </asp:RegularExpressionValidator>                         
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editPhoneRegExValidatorExtender" runat="server"
                                                            BehaviorID="editPhoneRegExValidatorExtender" 
                                                            TargetControlID="editPhoneRegExValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>
                                                    </div>  
                                                    <div class="divForm">
                                                         <label class="label">Mobile Phone:</label>
                                                         <telerik:RadMaskedTextBox ID="txtEditMobilePhone" CssClass="textbox" runat="server"
                                                            Mask="(###)###-####" ValidationGroup="EditUser"></telerik:RadMaskedTextBox>   
                                                         <asp:RegularExpressionValidator ID="editMobilePhoneRegExValidator" runat="server"
                                                            ErrorMessage="Invalid Phone Format."
                                                            ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                                            ControlToValidate="txtEditMobilePhone"
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            ValidationGroup="EditUser">
                                                            </asp:RegularExpressionValidator>                         
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editMobilePhoneRegExValidatorExtender" runat="server"
                                                            BehaviorID="editMobilePhoneRegExValidatorExtender" 
                                                            TargetControlID="editMobilePhoneRegExValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender> 
                                                    </div>   
                                                    <div class="divForm">
                                                        <label class="label">*Culture:</label>
                                                        <asp:DropDownList CssClass="dropdown" ID="ddlEditCulture" runat="server">  
                                                        </asp:DropDownList>
                                                        <p>(Note: Changes will take effect on next user login.)</p>
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Active:</label>
                                                        <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                        
                                                    </div>                   
                          

                                                    <hr />
                                                    <h2>Reset Password</h2>
                                                    <div id="externallyManaged" runat="server" style="display: none">
                                                        Passwords are managed by your company and cannot be accessed here. Please contact your company's administrator.
                                                    </div>
                                                    <div id="internallyManaged" runat="server" style="display: none">
                                                        <p>Enter a new password only if you wish to reset it.</p>
                                                        <div class="divForm">                                                          
                                                            <label class="label">New Password:</label>                                                                                          
                                                            <asp:TextBox ID="txtEditNewPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>                                            
                                                            <asp:RegularExpressionValidator ID="editNewPasswordRegExValidator" runat="server"
                                                                ErrorMessage="Password must be 8-20 characters and include at least one non-letter character."
									                            ValidationExpression="^(?=.*[^a-zA-Z]).{8,20}$"
                                                                ControlToValidate="txtEditNewPassword"
                                                                SetFocusOnError="true" 
                                                                Display="None" 
                                                                ValidationGroup="EditUser">
                                                                </asp:RegularExpressionValidator>                         
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="editNewPaswordRegExValidatorExtender" runat="server"
                                                                BehaviorID="editNewPasswordRegExValidatorExtender" 
                                                                TargetControlID="editNewPasswordRegExValidator"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                Width="175">
                                                                </ajaxToolkit:ValidatorCalloutExtender>
                                                        </div>
                                                        <div class="divForm">                                                          
                                                            <label class="label">Confirm New Password:</label>  
                                                            <asp:TextBox ID="txtEditConfirmNewPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="editConfirmNewPasswordRegExValidator" runat="server"
                                                                ErrorMessage="Confirm Password must 8-20 characters and include at least one non-letter character."
									                            ValidationExpression="^(?=.*[^a-zA-Z]).{8,20}$"
                                                                ControlToValidate="txtEditConfirmNewPassword"
                                                                SetFocusOnError="true" 
                                                                Display="None" 
                                                                ValidationGroup="EditUser">
                                                                </asp:RegularExpressionValidator>                         
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmNewPasswordRegExValidatorExtender" runat="server"
                                                                BehaviorID="editConfirmNewPasswordRegExValidatorExtender" 
                                                                TargetControlID="editConfirmNewPasswordRegExValidator"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                Width="175">
                                                                </ajaxToolkit:ValidatorCalloutExtender>
                                                            <asp:RequiredFieldValidator ID="editConfirmNewPasswordRequiredValidator" runat="server"
                                                                ErrorMessage="Confirm New Password is a required field."
                                                                ControlToValidate="txtEditConfirmNewPassword"          
                                                                SetFocusOnError="true"
                                                                Display="None"
                                                                ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmNewPasswordRequiredValidatorExtender" runat="server"
                                                                BehaviorID="editConfirmNewPasswordRequiredValidatorExtender"
                                                                TargetControlID="editConfirmNewPasswordRequiredValidator"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender> 
                                                            <asp:CompareValidator ID="editConfirmNewPasswordCompareValidator" runat="server"
                                                                ErrorMessage="The passwords do not match. Passwords are case sensitive." 
                                                                ControlToValidate="txtEditConfirmNewPassword"
                                                                ControlToCompare="txtEditNewPassword" 
                                                                Operator="Equal"
                                                                Display="None"
                                                                ValidationGroup="EditUser">
                                                                </asp:CompareValidator>
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmNewPasswordCompareValidatorExtender" runat="server"
                                                                BehaviorID="editConfirmNewPasswordCompareValidatorExtender" 
                                                                TargetControlID="editConfirmNewPasswordCompareValidator"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                Width="175">
                                                                </ajaxToolkit:ValidatorCalloutExtender>   
                                                        </div>  
                                                        <div class="divForm">
                                                          <label class="label">Regenerate And Email Temporary Password:</label>
                                                          <asp:LinkButton ID="btnRegenratePassword" runat="server" CssClass="lnkButton" Text="Regenerate/Email" OnClick="regenratePassword_Click" CausesValidation="false" />
                                                        </div>
                                                    </div>
                                                    <hr />
                                                    <h2>Role Change</h2>
                                                    <div class="divForm">   
                                                        <label class="label">*Change Primary User Role:</label>
                                                        <asp:HiddenField ID="hdnEditPrimaryRoleID" runat="server" Visible="false" />
                                                        <asp:CheckBox ID="chkEditRole" CssClass="checkbox" Checked="false" runat="server" OnCheckedChanged="chkEditRole_OnCheckedChanged" AutoPostBack="true" /><br />                                                         
                                                        <extensions:DropDownExtension ID="ddlEditRole" Enabled="false" CssClass="dropdown" OnSelectedIndexChanged="ddlEditRole_OnSelectedIndexChanged" AppendDataBoundItems="true" AutoPostBack="true" runat="server">                                                                
                                                        </extensions:DropDownExtension>                                                          
                                                        <!--<p>(Note: If no role is pre selected here, it's because the user has different roles per client. A provider user may have different roles because of the max role for a client despite how the user was added orginially.)</p>-->
                                                        <p>(Warning: Changing the users role will clear all associated clients, modules, quicklinks, buildings, analysis builder views, referring domain auto logins, and organization admin access associated with that user, if any.")
                                                        </p> 
                                                        <asp:RequiredFieldValidator ID="editRoleRequiredValidator" runat="server"
                                                            ErrorMessage=" User Role is a required field." 
                                                            ControlToValidate="ddlEditRole"  
                                                            SetFocusOnError="true" 
                                                            Display="None" 
                                                            InitialValue="-1"
                                                            ValidationGroup="EditUser">
                                                            </asp:RequiredFieldValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="editRoleRequiredValidatorExtender" runat="server"
                                                            BehaviorID="editRoleRequiredValidatorExtender" 
                                                            TargetControlID="editRoleRequiredValidator"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            Width="175">
                                                            </ajaxToolkit:ValidatorCalloutExtender>  
                                                    </div>
                                                    <div class="divForm">
                                                        <label class="label">*Organization Admin:</label>
                                                        <asp:CheckBox ID="chkEditOrganizationAdmin" CssClass="checkbox" runat="server" />   
                                                        <p>
                                                        (Note: Only super admins may be an organization admin.)
                                                        </p>                                                    
                                                    </div>                                                                                                                                  
                                                    <div id="divEditClientsListBoxs" runat="server" visible="false">
                                                        <hr />
                                                        <h2>Assign Clients</h2>
                                                        <p>Clients are shown/available based on organization and role selected above.</p>
                                                        <p>(Warning: Removing a client WILL clear all client modules, client quicklinks, buildings, and analysis builder views associated with that client for this user, if any.")</p>
                                                        <div class="divForm">                                                        
                                                            <label class="label">Client Access:</label>                                                                                                                                       
                                                            <asp:ListBox ID="lbEditClientsTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>
                                                            <div class="divArrows">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnEditClientsUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditClientsUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnEditClientsDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditClientsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                            </div>
                                                            <asp:ListBox ID="lbEditClientsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                            </asp:ListBox>                                                    
                                                        </div>
                                                                                                            
                                                    </div>                                                                                                                                    
                                                      
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateUser" runat="server" Text="Update User" OnClientClick="confirmRequired('ctl00_plcCopy_txtEditNewPassword', 'ctl00_plcCopy_editConfirmNewPasswordRequiredValidator', 'editConfirmNewPasswordRequiredValidatorExtender_popupTable')" OnClick="updateUserButton_Click" ValidationGroup="EditUser"></asp:LinkButton>

                                                                                                                                                                                                                  
                                                </div>

                                     <!--Ajax Validators are next to each form element, because they are not always visiable/in use-->                                 
                                    
                                    </asp:Panel>   
                                                                      
                                    <!--EDIT USER CLIENT ROLES PANEL -->                              
                                    <asp:Panel ID="pnlEditClientRoles" runat="server" Visible="false" DefaultButton="btnUpdateClientRoles">                                                                                             
                                        <div>
                                            <h2>Edit User Client Roles</h2>
                                        </div> 
                                        <a id="lnkEditClientRolesSetFocusView" href="#" runat="server"></a>        
                                        <asp:Label ID="lblEditClientRolesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnEditClientRolesOID" runat="server" Visible="false" /> 
                                        <asp:HiddenField ID="hdnEditClientRolesPrimaryRoleID" runat="server" Visible="false" /> 
                                        <asp:HiddenField ID="hdnEditClientRolesUID" runat="server" Visible="false" /> 
                                        <div class="divForm">   
                                            <label class="label">*Client:</label>   
                                            <asp:DropDownList ID="ddlEditClientRolesClient" OnSelectedIndexChanged="ddlEditClientRolesClient_OnSelectedIndexChanged" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server">                                                                
                                                <asp:ListItem Value="-1" >Select one...</asp:ListItem>  
                                            </asp:DropDownList> 
                                            <asp:RequiredFieldValidator ID="editClientRolesClientRequiredValidator" runat="server"
                                                ErrorMessage="Client is a required field." 
                                                ControlToValidate="ddlEditClientRolesClient"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="EditClientRoles">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="editClientRolesClientRequiredValidatorExtender" runat="server"
                                                BehaviorID="editClientRolesClientRequiredValidatorExtender" 
                                                TargetControlID="editClientRolesClientRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>
                                        <div class="divForm">   
                                            <label class="label">*Change Secondary User Role:</label>   
                                            <asp:HiddenField ID="hdnEditClientRolesSecondaryRoleID" runat="server" Visible="false" />                                                        
                                            <asp:CheckBox ID="chkEditClientRolesRole" CssClass="checkbox" Checked="false" runat="server" OnCheckedChanged="chkEditClientRolesRole_OnCheckedChanged" AutoPostBack="true" /><br />
                                            <extensions:DropDownExtension ID="ddlEditClientRolesRole" Enabled="false" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                                                                
                                            </extensions:DropDownExtension>                                                                       
                                            <p>(Warning: Changing the users role MAY clear all buildings, quicklinks, and analysis builder views associated with that user, if any.")
                                            </p> 
                                            <asp:RequiredFieldValidator ID="editClientRolesRoleRequiredValidator" runat="server"
                                                ErrorMessage=" Max User Role is a required field." 
                                                ControlToValidate="ddlEditClientRolesRole"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="EditClientRoles">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="editClientRolesRoleRequiredValidatorExtender" runat="server"
                                                BehaviorID="editClientRolesRoleRequiredValidatorExtender" 
                                                TargetControlID="editClientRolesRoleRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>  
                                        </div>                                                 
                                        <asp:LinkButton CssClass="lnkButton" ID="btnUpdateClientRoles" runat="server" Text="Update Roles"  OnClick="updateClientRolesButton_Click" ValidationGroup="EditClientRoles"></asp:LinkButton>

                                     </asp:Panel>
                                                 
                                    <!--EDIT USER CLIENT MODULES PANEL -->                              
                                    <asp:Panel ID="pnlEditClientModules" runat="server" Visible="false" DefaultButton="btnUpdateClientModules">                                                                                             
                                        <div>
                                            <h2>Edit User Client Modules</h2>
                                        </div>  
                                        <a id="lnkEditClientModulesSetFocusView" href="#" runat="server"></a>     
                                        <asp:Label ID="lblEditClientModulesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnEditClientModulesUID" runat="server" Visible="false" />
                                        <asp:HiddenField ID="hdnEditClientModulesPrimaryRoleID" runat="server" Visible="false" />
                                        <asp:HiddenField ID="hdnEditClientModulesSecondaryRoleID" runat="server" Visible="false" />
                                        <asp:HiddenField ID="hdnEditClientModulesUnrestricted" runat="server" Visible="false" />
                                        <div class="divForm">   
                                            <label class="label">*Client:</label>    
                                            <asp:DropDownList ID="ddlEditClientModulesClient" OnSelectedIndexChanged="ddlEditClientModulesClient_OnSelectedIndexChanged" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server">                                                                
                                                <asp:ListItem Value="-1" >Select one...</asp:ListItem>  
                                            </asp:DropDownList> 
                                            <asp:RequiredFieldValidator ID="editClientModulesClientRequiredValidator" runat="server"
                                                ErrorMessage="Client is a required field." 
                                                ControlToValidate="ddlEditClientModulesClient"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="EditClientModules">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="editClientModulesClientRequiredValidatorExtender" runat="server"
                                                BehaviorID="editClientModulesClientRequiredValidatorExtender" 
                                                TargetControlID="editClientModulesClientRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>  
                                        <asp:Panel ID="pnlIsModuleRestricted" Visible="false" CssClass="divForm" 
                                            runat="server">
                                            <label class="label">Module restricted mode:</label>    
                                            <asp:CheckBox ID="chkIsModuleRestricted" CssClass="dropdown" 
                                                OnCheckedChanged="chkIsModuleRestricted_OnCheckedChanged" AutoPostBack="true" 
                                                runat="server" />
                                            <p>(Note: If module restricted mode is off, the user has access to all modules for that client or provider client relationship.)</p>
                                        </asp:Panel>  
                                        <div id="divEditModulesListBoxs" runat="server" visible="false">
                                            <hr />
                                            <h2>Assign Modules</h2>  
                                            <div class="divForm">                                                        
                                                <label class="label">Available Modules:</label>                                                                                                                                      
                                                <asp:ListBox ID="lbEditClientModulesTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="ImageButton7" 
                                                        ImageUrl="_assets/images/up-arrow.png" runat="server" 
                                                        OnClick="btnEditClientModulesUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="ImageButton8" 
                                                        ImageUrl="_assets/images/down-arrow.png" runat="server" 
                                                        OnClick="btnEditClientModulesDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned Modules:</label>  
                                                <asp:ListBox ID="lbEditClientModulesBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>                                                    
                                            </div>
                                        </div>
                                        <asp:LinkButton CssClass="lnkButton" ID="btnUpdateClientModules" runat="server" Text="Update Modules"  OnClick="updateClientModulesButton_Click" ValidationGroup="EditClientModules"></asp:LinkButton>
                                     </asp:Panel>

                                    <!--EDIT USER CLIENT QUICKILINKS PANEL -->                              
                                    <asp:Panel ID="pnlEditClientQuickLinks" runat="server" Visible="false" DefaultButton="btnUpdateClientQuickLinks">                                                                                             
                                        <div>
                                            <h2>Edit User Client QuickLinks</h2>
                                            <p>Quick Links are available based on role, and client selected above. Also based on the associated clients modules.</p>
                                        </div> 
                                        <a id="lnkEditClientQuickLinksSetFocusView" href="#" runat="server"></a>      
                                        <asp:Label ID="lblEditClientQuickLinksError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnEditClientQuickLinksUID" runat="server" Visible="false" />
                                        <asp:HiddenField ID="hdnEditClientQuickLinksPrimaryRoleID" runat="server" Visible="false" />
                                        <div class="divForm">   
                                            <label class="label">*Client:</label>    
                                            <asp:DropDownList ID="ddlEditClientQuickLinksClient" OnSelectedIndexChanged="ddlEditClientQuickLinksClient_OnSelectedIndexChanged" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server">                                                                
                                                <asp:ListItem Value="-1" >Select one...</asp:ListItem>  
                                            </asp:DropDownList> 
                                            <asp:RequiredFieldValidator ID="editClientQuickLinksClientRequiredValidator" runat="server"
                                                ErrorMessage="Client is a required field." 
                                                ControlToValidate="ddlEditClientQuickLinksClient"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="EditClientQuickLinks">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="editClientQuickLinksClientRequiredValidatorExtender" runat="server"
                                                BehaviorID="editClientQuickLinksClientRequiredValidatorExtender" 
                                                TargetControlID="editClientQuickLinksClientRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>  

                                        <hr />
                                        <h2>Assign Quick Links</h2>
                                        <div class="divForm">                                                        
                                            <label class="label">Available Quick Links:</label>                                                                                                                                       
                                            <asp:ListBox ID="lbEditClientQuickLinksTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                            </asp:ListBox>
                                            <div class="divArrows">
                                            <asp:ImageButton CssClass="up-arrow"  ID="ImageButton1" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditClientQuickLinksUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                            <asp:ImageButton CssClass="down-arrow" ID="ImageButton2" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditClientQuickLinksDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                            </div>
                                            <label class="label">Assigned Quick Links:</label>   
                                            <asp:ListBox ID="lbEditClientQuickLinksBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                            </asp:ListBox>                                                    
                                        </div>
                                        <asp:LinkButton CssClass="lnkButton" ID="btnUpdateClientQuickLinks" runat="server" Text="Update QuickLinks"  OnClick="updateClientQuickLinksButton_Click" ValidationGroup="EditClientQuickLinks"></asp:LinkButton>              
                                     </asp:Panel>  

                                    <!--EDIT USER CLIENT BUILDING GROUPS\BUILDNGS PANEL -->                                                     
                                    <asp:Panel ID="pnlEditClientBuildings" runat="server" Visible="false" DefaultButton="btnUpdateClientBuildings">                                                                                             
                                        <div>
                                            <h2>Edit User Buildings\Building Groups</h2>
                                            <p>Buildings and Building Groups are shown/available based on role and client selected above.</p>
                                        </div> 
                                        <a id="lnkEditClientBuildingsSetFocusView" href="#" runat="server"></a>     
                                        <asp:Label ID="lblEditClientBuildingsError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label> 
                                        <asp:HiddenField ID="hdnEditClientBuildingsUID" runat="server" Visible="false" />
                                        <asp:HiddenField ID="hdnEditClientBuildingsOID" runat="server" Visible="false" />
                                        <asp:HiddenField ID="hdnEditClientBuildingsPrimaryRoleID" runat="server" Visible="false" />
                                        <asp:HiddenField ID="hdnEditClientBuildingsSecondaryRoleID" runat="server" Visible="false" />
                                        <asp:HiddenField ID="hdnEditClientBuildingsUnrestricted" runat="server" Visible="false" />
                                        <div class="divForm">   
                                            <label class="label">*Client:</label>    
                                            <asp:DropDownList ID="ddlEditClientBuildingsClient" OnSelectedIndexChanged="ddlEditClientBuildingsClient_OnSelectedIndexChanged" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server">                                                                
                                                <asp:ListItem Value="-1" >Select one...</asp:ListItem>  
                                            </asp:DropDownList> 
                                            <asp:RequiredFieldValidator ID="editClientBuildingsClientRequiredValidator" runat="server"
                                                ErrorMessage="Client is a required field." 
                                                ControlToValidate="ddlEditClientBuildingsClient"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="EditClientBuildings">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="editClientBuildingsClientRequiredValidatorExtender" runat="server"
                                                BehaviorID="editClientBuildingsClientRequiredValidatorExtender" 
                                                TargetControlID="editClientBuildingsClientRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>  
                                        <asp:Panel ID="pnlIsBuildingRestricted" Visible="false" CssClass="divForm" runat="server">
                                            <label class="label">Building restricted mode:</label>    
                                            <asp:CheckBox ID="chkIsBuildingRestricted" CssClass="dropdown" OnCheckedChanged="chkIsBuildingRestricted_OnCheckedChanged" AutoPostBack="true" runat="server" />
                                            <p>(Note: If buildings restricted mode is off, the user has access to all buildings for that client or all buildings allowed for that provided client relationship.)</p>
                                        </asp:Panel>
                                        <div id="divEditBuildingGroupsListBoxs" runat="server" visible="false">                                                    
                                            <hr />
                                            <h2>Assign Building Groups</h2>  
                                            <div class="divForm">        
                                                <label class="label">Available Building Groups:</label>                                                                                                                                       
                                                <asp:ListBox ID="lbEditClientBuildingGroupsTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="ImageButton3" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditClientBuildingGroupsUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="ImageButton4" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditClientBuildingGroupsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned Building Groups:</label>  
                                                <asp:ListBox ID="lbEditClientBuildingGroupsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>                                                    
                                            </div>
                                        </div>
                                        <div id="divEditBuildingsListBoxs" runat="server" visible="false">
                                            <hr />
                                            <h2>Assign Buildings</h2>  
                                            <div class="divForm">                                                        
                                                <label class="label">Available Buildings:</label>                                                                                                                                      
                                                <asp:ListBox ID="lbEditClientBuildingsTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="ImageButton5" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditClientBuildingsUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="ImageButton6" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditClientBuildingsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Assigned Buildings:</label>  
                                                <asp:ListBox ID="lbEditClientBuildingsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>                                                    
                                            </div>
                                        </div>
                                        <asp:LinkButton CssClass="lnkButton" ID="btnUpdateClientBuildings" runat="server" Text="Update Buildings"  OnClick="updateClientBuildingsButton_Click" ValidationGroup="EditClientBuildings"></asp:LinkButton>
                                     </asp:Panel>  
                                     
                                     <!--EDIT USER CLIENT BUILDING MODULES PANEL -->                              
                                    <asp:Panel ID="pnlEditClientBuildingModules" runat="server" Visible="false" DefaultButton="btnUpdateClientBuildingModules">                                                                                             
                                        <div>
                                            <h2>Edit User Building Modules</h2>
                                        </div>  
                                        <a id="lnkEditClientBuildingModulesSetFocusView" href="#" runat="server"></a>     
                                        <asp:Label ID="lblEditClientBuildingModulesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                                        <asp:HiddenField ID="hdnEditClientBuildingModulesUID" runat="server" Visible="false" />
                                        <div class="divForm">   
                                            <label class="label">*Client:</label>    
                                            <asp:DropDownList ID="ddlEditClientBuildingModulesClient" OnSelectedIndexChanged="ddlEditClientBuildingModulesClient_OnSelectedIndexChanged" CssClass="dropdown" AutoPostBack="true" AppendDataBoundItems="true" runat="server">                                                                
                                                <asp:ListItem Value="-1" >Select one...</asp:ListItem>  
                                            </asp:DropDownList> 
                                            <asp:RequiredFieldValidator ID="editClientBuildingModulesClientRequiredValidator" runat="server"
                                                ErrorMessage="Client is a required field." 
                                                ControlToValidate="ddlEditClientBuildingModulesClient"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="EditClientBuildingModules">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="editClientBuildingModulesClientRequiredValidatorExtender" runat="server"
                                                BehaviorID="editClientBuildingModulesClientRequiredValidatorExtender" 
                                                TargetControlID="editClientBuildingModulesClientRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>  
                                        <asp:LinkButton CssClass="lnkButton" ID="btnUpdateClientBuildingModules" runat="server" Text="Update Modules"  OnClick="updateClientBuildingModulesButton_Click" ValidationGroup="EditClientBuildingModules"></asp:LinkButton>
                                     </asp:Panel>                                                                        
                                                                                                                                                                            
                           </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddUser" runat="server" DefaultButton="btnAddUser">  
                                    <h2>Add User</h2> 
                                    <div>        
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                            
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                  
                                        <div class="divForm">
                                            <label class="label">*Organization Name:</label>
                                            <asp:Label ID="lblAddOrganizationName" CssClass="labelContent" runat="server"></asp:Label>
                                            <p id="lblAddOrgnizationNameNote" runat="server">
                                            (NOTE: Please make sure you are logged into the correct organization before adding a user.)
                                            </p>
                                        </div>                          
                                        <div class="divForm">
                                            <label class="label">*First Name:</label>
                                            <asp:TextBox ID="txtAddFirstName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                            <asp:RequiredFieldValidator ID="addFirstNameRequiredValidator" runat="server"
                                                ErrorMessage="First Name is a required field."
                                                ControlToValidate="txtAddFirstName"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addFirstNameRequiredValidatorExtender" runat="server"
                                                BehaviorID="addFirstNameRequiredValidatorExtender"
                                                TargetControlID="addFirstNameRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Last Name:</label>
                                            <asp:TextBox ID="txtAddLastName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                                                                        
                                            <asp:RequiredFieldValidator ID="addLastNameRequiredValidator" runat="server"
                                                ErrorMessage="Last Name is a required field."
                                                ControlToValidate="txtAddLastName"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addLastNameRequiredValidatorExtender" runat="server"
                                                BehaviorID="addLastNameRequiredValidatorExtender"
                                                TargetControlID="addLastNameRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>  
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Email:</label>
                                            <asp:TextBox ID="txtAddEmail" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="addEmailRequiredValidator" runat="server"
                                                ErrorMessage="Email is a required field." 
                                                ControlToValidate="txtAddEmail"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addEmailRequiredValidatorExtender" runat="server"
                                                BehaviorID="addEmailRequiredValidatorExtender" 
                                                TargetControlID="addEmailRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addEmailRegExValidator" runat="server"
                                                ErrorMessage="Invalid Email Format."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ControlToValidate="txtAddEmail"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addEmailRegExValidatorExtender" runat="server"
                                                BehaviorID="addEmailRegExValidatorExtender" 
                                                TargetControlID="addEmailRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Confirm Email:</label>
                                            <asp:TextBox ID="txtAddConfirmEmail" CssClass="textbox" MaxLength="100" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="addConfirmEmailRequiredValidator" runat="server"
                                                ErrorMessage="Confirm Email is a required field." 
                                                ControlToValidate="txtAddConfirmEmail"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmEmailRequiredValidatorExtender" runat="server"
                                                BehaviorID="addConfirmEmailRequiredValidatorExtender" 
                                                TargetControlID="addConfirmEmailRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addConfirmEmailRegExValidator" runat="server"
                                                ErrorMessage="Invalid Email Format."
                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ControlToValidate="txtAddConfirmEmail"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmEmailRegExValidatorExtender" runat="server"
                                                BehaviorID="addConfirmEmailRegExValidatorExtender" 
                                                TargetControlID="addConfirmEmailRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:CompareValidator ID="addConfirmEmailCompareValidator" runat="server"
                                                ErrorMessage="The emails do not match." 
                                                ControlToValidate="txtAddConfirmEmail"
                                                ControlToCompare="txtAddEmail" 
                                                Operator="Equal"
                                                Display="None"
                                                ValidationGroup="AddUser">
                                                </asp:CompareValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addConfirmEmailCompareValidatorExtender" runat="server"
                                                BehaviorID="addConfirmEmailCompareValidatorExtender" 
                                                TargetControlID="addConfirmEmailCompareValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Address:</label>
                                            <asp:TextBox ID="txtAddAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                        </div>                                                                                                                                                                                           
                                        <div class="divForm">
                                             <label class="label">City:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddCity" runat="server" MaxLength="100"></asp:TextBox>   
                                        </div>
                                        <div class="divForm">
                                                <label class="label">*Country:</label>
                                                <asp:DropDownList CssClass="dropdown" ID="ddlAddCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                    <asp:ListItem Value="-1">Select one...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="addCountryRequiredValidator" runat="server"
                                                    ErrorMessage="Country is a required field." 
                                                    ControlToValidate="ddlAddCountry"  
                                                    SetFocusOnError="true" 
                                                    Display="None" 
                                                    InitialValue="-1"
                                                    ValidationGroup="AddUser">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="addCountryRequiredValidatorExtender" runat="server"
                                                    BehaviorID="addCountryRequiredValidatorExtender" 
                                                    TargetControlID="addCountryRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender> 
                                        </div>  
                                        <div class="divForm">
                                             <label class="label">State (Required based on Country):</label>
                                            <asp:DropDownList CssClass="dropdown" ID="ddlAddState" AppendDataBoundItems="true" runat="server">  
                                                <asp:ListItem Value="-1">Select country first...</asp:ListItem>                             
                                            </asp:DropDownList>
                                             <asp:RequiredFieldValidator ID="addStateRequiredValidator" runat="server"
                                                ErrorMessage="State is a required field." 
                                                ControlToValidate="ddlAddState"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addStateRequiredValidatorExtender" runat="server"
                                                BehaviorID="addStateRequiredValidatorExtender" 
                                                TargetControlID="addStateRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>                                                
                                        </div>
                                        <div class="divForm">
                                             <label class="label">Zip Code (Required based on Country):</label>
                                             <telerik:RadMaskedTextBox ID="txtAddZip" CssClass="textbox" runat="server" 
                                                Mask="aaaaaaaaaa" PromptChar="" ValidationGroup="AddUser"></telerik:RadMaskedTextBox>   
                                             <asp:RequiredFieldValidator ID="addZipRequiredValidator" runat="server"
                                                ErrorMessage="Zip Code is a required field."
                                                ControlToValidate="txtAddZip"          
                                                SetFocusOnError="true"
                                                Display="None"
                                                ValidationGroup="AddUser"
                                                Enabled="false">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addZipRequiredValidatorExtender" runat="server"
                                                BehaviorID="addZipRequiredValidatorExtender"
                                                TargetControlID="addZipRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                            <asp:RegularExpressionValidator ID="addZipRegExValidator" runat="server"
                                                ErrorMessage="Invalid Zip Format."
                                                ValidationExpression="\d{5}"
                                                ControlToValidate="txtAddZip"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser"
                                                Enabled="false">
                                                </asp:RegularExpressionValidator> 
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addZipRegExValidatorExtender" runat="server"
                                                BehaviorID="addZipRegExValidatorExtender"
                                                TargetControlID="addZipRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>                                                    
                                        <div class="divForm">
                                             <label class="label">Phone:</label>
                                             <telerik:RadMaskedTextBox ID="txtAddPhone" CssClass="textbox" runat="server"
                                                Mask="(###)###-####" ValidationGroup="AddUser"></telerik:RadMaskedTextBox>   
                                             <asp:RegularExpressionValidator ID="addPhoneRegExValidator" runat="server"
                                                ErrorMessage="Invalid Phone Format."
                                                ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                                ControlToValidate="txtAddPhone"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="AddPhoneRegExValidatorExtender" runat="server"
                                                BehaviorID="addPhoneRegExValidatorExtender" 
                                                TargetControlID="addPhoneRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>
                                        <div class="divForm">
                                             <label class="label">Mobile Phone:</label>
                                             <telerik:RadMaskedTextBox ID="txtAddMobilePhone" CssClass="textbox" runat="server"
                                                Mask="(###)###-####" ValidationGroup="AddUser"></telerik:RadMaskedTextBox>    
                                             <asp:RegularExpressionValidator ID="addMobilePhoneRegExValidator" runat="server"
                                                ErrorMessage="Invalid Phone Format."
                                                ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                                ControlToValidate="txtAddMobilePhone"
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                ValidationGroup="AddUser">
                                                </asp:RegularExpressionValidator>                         
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addMobilePhoneRegExValidatorExtender" runat="server"
                                                BehaviorID="addMobilePhoneRegExValidatorExtender" 
                                                TargetControlID="addMobilePhoneRegExValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>
                                        <div class="divForm">
                                                <label class="label">*Culture:</label>
                                                <asp:DropDownList CssClass="dropdown" ID="ddlAddCulture" runat="server">  
                                                </asp:DropDownList>
                                        </div>
                                        <div class="divForm">   
                                            <label class="label">*Primary User Role:</label>    
                                            <extensions:DropDownExtension ID="ddlAddRole" OnSelectedIndexChanged="ddlAddRole_OnSelectedIndexChanged" CssClass="dropdown" AppendDataBoundItems="true" AutoPostBack="true" runat="server">    
                                                <asp:ListItem Value="-1" >Select Organization first...</asp:ListItem>                           
                                            </extensions:DropDownExtension> 
                                            <asp:RequiredFieldValidator ID="addRoleRequiredValidator" runat="server"
                                                ErrorMessage="User Role is a required field." 
                                                ControlToValidate="ddlAddRole"  
                                                SetFocusOnError="true" 
                                                Display="None" 
                                                InitialValue="-1"
                                                ValidationGroup="AddUser">
                                                </asp:RequiredFieldValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender ID="addRoleRequiredValidatorExtender" runat="server"
                                                BehaviorID="addRoleRequiredValidatorExtender" 
                                                TargetControlID="addRoleRequiredValidator"
                                                HighlightCssClass="validatorCalloutHighlight"
                                                Width="175">
                                                </ajaxToolkit:ValidatorCalloutExtender>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">*Organization Admin:</label>
                                            <asp:CheckBox ID="chkAddOrganizationAdmin" CssClass="checkbox" Enabled="false" runat="server" />   
                                            <p>
                                            (NOTE: Only super admins may be an organization admin.)
                                            </p>                                                    
                                        </div>                                                                                                 
                                                                                                            
                                        <div class="divForm">  
                                            <label class="label">*Password:</label>  
                                            <p id="externallyManagedNewUser" runat="server" style="display: none">
                                            (NOTE: Passwords are managed by your company and cannot be accessed here. Please contact your company's administrator.)
                                            </p> 
                                            <p id="internallyManagedNewUser" runat="server" style="display: none">
                                            (NOTE: New user account creation passwords are auto-generated and emailed.)
                                            </p> 
                                        </div>
                                                                                                                                                              
                                        <div id="divAddClientsListBoxs" runat="server" visible="false">
                                            <hr />
                                            <h2>Assign Clients</h2>
                                            <p>Clients are shown/available based on role selected above.</p>
                                            <div class="divForm">                                            
                                                <label class="label">Client Access:</label>                                                                                                                                       
                                                <asp:ListBox ID="lbAddClientsTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnAddClientsUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnAddClientsUpButton_Click" CausesValidation="false"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnAddClientsDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnAddClientsDownButton_Click" CausesValidation="false"></asp:ImageButton>                                                    
                                                </div>
                                                <asp:ListBox ID="lbAddClientsBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>                                                    
                                            </div>
                                        </div>                                                                    
                                                                                                                                                                                              
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddUser" runat="server" Text="Add User"  OnClick="addUserButton_Click" ValidationGroup="AddUser"></asp:LinkButton>                                                                                   
                                    </div>
                                    
                                    <!--Ajax Validators are next to each form element, because they are not always visiable/in use-->    
                                </asp:Panel>                                                                                                                                                                         
                            </telerik:RadPageView>  
                            <telerik:RadPageView ID="RadPageView3" runat="server">
                                <CW:TabHeader runat="server" ID="tabHeaderAudit" Title="Users Audit">
                                  <RightAreaTemplate>
                                    <CW:UserAuditDownloadArea runat="server" ID="UserAuditDownloadArea" />
                                  </RightAreaTemplate>
                                </CW:TabHeader>

                                <p>
                                    <a id="lnkUserAuditSetFocusView" href="#" runat="server"></a>
                                    <asp:Label ID="lblUsersAuditResults" runat="server" Text=""></asp:Label>
                                    <asp:Label ID="lblUsersAuditErrors" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>
                                </p>
                                <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridUsersAudit"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="AuditID,UID"  
                                             GridLines="None"                                      
                                             PageSize="100" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridUsersAudit_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridUsersAudit_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridUsersAudit_Sorting"   
                                             OnDataBound="gridUsersAudit_OnDataBound"
                                             AutoGenerateColumns="false"                                               
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2"   
                                             RowStyle-Wrap="true"                                                                                   
                                             > 
                                             <Columns>                                                                                                 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="Email" HeaderText="Email">                                                                                              
                                                    <ItemTemplate><label title="<%# Eval("Email") %>"><%# StringHelper.TrimText(Eval("Email"),25) %></label></ItemTemplate>                                                                             
                                                </asp:TemplateField>                                                  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="FirstName" HeaderText="First Name">                                                      
                                                    <ItemTemplate><label title="<%# Eval("FirstName") %>"><%# StringHelper.TrimText(Eval("FirstName"),15) %></label></ItemTemplate>                                                                      
                                                </asp:TemplateField>    
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="LastName" HeaderText="Last Name">  
                                                    <ItemTemplate><label title="<%# Eval("LastName") %>"><%# StringHelper.TrimText(Eval("LastName"),15) %></label></ItemTemplate>                                    
                                                </asp:TemplateField>                                                                                                                                             
                                                <asp:BoundField SortExpression="LoginSuccessTotalCount" HeaderText="Login Count" DataField="LoginSuccessTotalCount" DataFormatString="{0:D}" />
                                                <asp:BoundField SortExpression="LoginFailedAttemptCount" HeaderText="Failed Count" DataField="LoginFailedAttemptCount" DataFormatString="{0:D}" />
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="IsActive" HeaderText="Active">  
                                                    <ItemTemplate><%# Convert.ToBoolean(Eval("IsActive")) %></ItemTemplate>                                    
                                                </asp:TemplateField>
                                                 <asp:TemplateField ItemStyle-Wrap="true" SortExpression="IsLocked" HeaderText="Locked">  
                                                    <ItemTemplate><%# Convert.ToBoolean(Eval("IsLocked")) %></ItemTemplate>                                    
                                                </asp:TemplateField> 
                                                <asp:BoundField SortExpression="DateCreated" HeaderText="User Created" DataField="DateCreated" DataFormatString="{0:g}" />
                                                <asp:BoundField SortExpression="DateModified" HeaderText="Last Login Attempt" DataField="DateModified" DataFormatString="{0:g}" />                                                                                                                                                                                                                  
                                             </Columns>        
                                         </asp:GridView>
                                    </div>
                         </telerik:RadPageView>                                                                                                                   
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>


                    
                  
