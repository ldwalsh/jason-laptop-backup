﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO;

using System.Threading;
using System.Globalization;

using CW.Data;
using CW.Data.Collections;
using CW.Business;
using CW.Utility;
using CW.Website._framework;
using CW.Data.Models.Diagnostics;
using CW.Data.Models.User;
using CW.Common.Constants;
using CW.Data.Models.Projects;
using CW.Website._customreports;
using CW.Data.Models.Raw;
using CW.Business.Results;
using CW.Data.Helpers;
using CW.Business.Blob.Images;
using CW.Logging;
using CW.Data.Models;
using CW.Website.DependencyResolution;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using CW.Business.Query;

using Telerik.Web.UI;
using EO.Pdf;

using CS = CW.Common.Constants.BusinessConstants.Content.ContentSections;
//need to declare this here, "Content" conflicts with System.Web.UI.WebControls.Content
using Content = CW.Data.Content;
using Charting = System.Web.UI.DataVisualization.Charting;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class BureauReportv2 : Page
    {
        #region Fields

        const string buildingCultureError = "Selected buildings must be of the same culture.";
        const string noBuildingsError = "No buildings were selected.";

        const string bureauInitializationFailed = "Bureau report initialization failed. Please contact an administrator.";
        const string bureauGenerationFailed = "Bureau report generation failed. Please contact an administrator.";
        const string downloadFailed = "Bureau report download failed. Please contact an administrator.";

        const string invisibleElementIds = "hdn_container;" +
                                            "btnAutomatedSummary;editorAutomatedSummary;btnExpertSummary;editorExpertSummary;" +
                                            "btnBuildingSummary;btnEquipmentClassSummary;" +
                                            "btnEnergyTrend;editorEnergyTrend;btnMaintenanceTrend;editorMaintenanceTrend;btnComfortTrend;editorComfortTrend;" +
                                            "btnTopEnergy;btnTopMaintenance;btnTopComfort;" +
                                            "btnVentilationSummary;btnHeatingSummary;btnCoolingSummary;btnPlantSummary;btnZoneSummary;btnSpecialtySummary;" +
                                            "btnFrequentAlarms;btnLongestAlarms;btnTotalAlarms;" +
                                            "btnProjects;btnOpenTasks;btnCompletedTasks;" +
                                            "divDownload;";

        public DataManager mDataManager;
        public SiteUser siteUser;
        public static bool isScheduled = false;

        //whole form
        private const string seTagline = "<h2>Make the most of your energy<sup>SM</sup></h2>";
        private const string kgsTagline = "<h2>Making buildings better</h2>";
        private enum ReportTheme { KGS, SE };
        private int cid, uid, oid, buildingLCID;
        private string languageCultureName, userCulture, cultureFormat, userTitle, automatedSummary, expertSummary, energy, maintenance, comfort;
        private bool showClientLogos = false, isSETheme = false;
        private bool includeCoverPage = false, includeContentsPage = false, includeExpertSummary = false, includeBuildingSummaryReport = false, includeEquipmentClassSummaryReport = false, includeBuildingTrendsSummaryReport = false, includeBuildingTopIssuesSummaryReport = false, includeVentilationEquipmentReport = false, includeHeatingEquipmentReport = false, includeCoolingEquipmentReport = false, includePlantEquipmentReport = false, includeZoneEquipmentReport = false, includeSpecialtyEquipmentReport = false, includePerformanceReport = false, includeAlarmReport = false, includeProjectReport = false, includeTaskReport = false, includeTopDiagnosticsDetails = false;
        //, includeHiddenIssues = false
        public bool includeTopDiagnosticsFigures = false;
        private int? topVentilation, topHeating, topCooling, topPlant, topZone, topSpecialty, topProjects, topTasks;
        private string tempsd, temped, temppsd, tempped;
        private DateTime sd, ed, psd, ped;
        private List<int> bids;
        private Building mBuilding;
        private Client mClient;
        private Organization mUserOrg;
        private GetUserWithCountryAndState mUser;
        private double reportNumberOfDays;
        private double previousReportNumberOfDays;
        private Guid guid;
        private IEnumerable<Content> mContent;
        
        //portfolio wide
        private int energyFaultCountAccrossWholePeriod, previousEnergyFaultCountAccrossWholePeriod, maintenanceFaultCountAccrossWholePeriod, previousMaintenanceFaultCountAccrossWholePeriod, comfortFaultCountAccrossWholePeriod, previousComfortFaultCountAccrossWholePeriod;
        private double totalCost, previousTotalCost, intervalCost, intervalPreviousCost;
        private double totalCostPercent, intervalCostPercent, intervalEnergyPercent, intervalMaintenancePercent, intervalComfortPercent;
        private string totalCostPercentString, intervalCostPercentString, intervalEnergyPercentString, intervalMaintenancePercentString, intervalComfortPercentString;
        private double intervalEnergyIncidents, intervalMaintenanceIncidents, intervalComfortIncidents;
        private double intervalPreviousEnergyIncidents, intervalPreviousMaintenanceIncidents, intervalPreviousComfortIncidents;

        //ventilation only
        private int ventilationEnergyFaultCountAccrossWholePeriod, previousVentilationEnergyFaultCountAccrossWholePeriod, ventilationMaintenanceFaultCountAccrossWholePeriod, previousVentilationMaintenanceFaultCountAccrossWholePeriod, ventilationComfortFaultCountAccrossWholePeriod, previousVentilationComfortFaultCountAccrossWholePeriod;
        private double ventilationTotalCost, previousVentilationTotalCost, intervalVentilationCost, intervalPreviousVentilationCost;
        private double intervalVentilationEnergyIncidents, intervalVentilationMaintenanceIncidents, intervalVentilationComfortIncidents;
        private double intervalPreviousVentilationEnergyIncidents, intervalPreviousVentilationMaintenanceIncidents, intervalPreviousVentilationComfortIncidents;

        //heating only
        private int heatingEnergyFaultCountAccrossWholePeriod, previousHeatingEnergyFaultCountAccrossWholePeriod, heatingMaintenanceFaultCountAccrossWholePeriod, previousHeatingMaintenanceFaultCountAccrossWholePeriod, heatingComfortFaultCountAccrossWholePeriod, previousHeatingComfortFaultCountAccrossWholePeriod;
        private double heatingTotalCost, previousHeatingTotalCost, intervalHeatingCost, intervalPreviousHeatingCost;
        private double intervalHeatingEnergyIncidents, intervalHeatingMaintenanceIncidents, intervalHeatingComfortIncidents;
        private double intervalPreviousHeatingEnergyIncidents, intervalPreviousHeatingMaintenanceIncidents, intervalPreviousHeatingComfortIncidents;

        //cooling only
        private int coolingEnergyFaultCountAccrossWholePeriod, previousCoolingEnergyFaultCountAccrossWholePeriod, coolingMaintenanceFaultCountAccrossWholePeriod, previousCoolingMaintenanceFaultCountAccrossWholePeriod, coolingComfortFaultCountAccrossWholePeriod, previousCoolingComfortFaultCountAccrossWholePeriod;
        private double coolingTotalCost, previousCoolingTotalCost, intervalCoolingCost, intervalPreviousCoolingCost;
        private double intervalCoolingEnergyIncidents, intervalCoolingMaintenanceIncidents, intervalCoolingComfortIncidents;
        private double intervalPreviousCoolingEnergyIncidents, intervalPreviousCoolingMaintenanceIncidents, intervalPreviousCoolingComfortIncidents;

        //plant only
        private int plantEnergyFaultCountAccrossWholePeriod, previousPlantEnergyFaultCountAccrossWholePeriod, plantMaintenanceFaultCountAccrossWholePeriod, previousPlantMaintenanceFaultCountAccrossWholePeriod, plantComfortFaultCountAccrossWholePeriod, previousPlantComfortFaultCountAccrossWholePeriod;
        private double plantTotalCost, previousPlantTotalCost, intervalPlantCost, intervalPreviousPlantCost;
        private double intervalPlantEnergyIncidents, intervalPlantMaintenanceIncidents, intervalPlantComfortIncidents;
        private double intervalPreviousPlantEnergyIncidents, intervalPreviousPlantMaintenanceIncidents, intervalPreviousPlantComfortIncidents;

        //zone only
        private int zoneEnergyFaultCountAccrossWholePeriod, previousZoneEnergyFaultCountAccrossWholePeriod, zoneMaintenanceFaultCountAccrossWholePeriod, previousZoneMaintenanceFaultCountAccrossWholePeriod, zoneComfortFaultCountAccrossWholePeriod, previousZoneComfortFaultCountAccrossWholePeriod;
        private double zoneTotalCost, previousZoneTotalCost, intervalZoneCost, intervalPreviousZoneCost;
        private double intervalZoneEnergyIncidents, intervalZoneMaintenanceIncidents, intervalZoneComfortIncidents;
        private double intervalPreviousZoneEnergyIncidents, intervalPreviousZoneMaintenanceIncidents, intervalPreviousZoneComfortIncidents;

        //specialty only
        private int specialtyEnergyFaultCountAccrossWholePeriod, previousSpecialtyEnergyFaultCountAccrossWholePeriod, specialtyMaintenanceFaultCountAccrossWholePeriod, previousSpecialtyMaintenanceFaultCountAccrossWholePeriod, specialtyComfortFaultCountAccrossWholePeriod, previousSpecialtyComfortFaultCountAccrossWholePeriod;
        private double specialtyTotalCost, previousSpecialtyTotalCost, intervalSpecialtyCost, intervalPreviousSpecialtyCost;
        private double intervalSpecialtyEnergyIncidents, intervalSpecialtyMaintenanceIncidents, intervalSpecialtyComfortIncidents;
        private double intervalPreviousSpecialtyEnergyIncidents, intervalPreviousSpecialtyMaintenanceIncidents, intervalPreviousSpecialtyComfortIncidents;

        #endregion

        #region Properties

            private ISectionLogManager Logger { get; set; }
            private ContentHelper CH { get; set; }
            private readonly string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["CWConnectionString"].ConnectionString;
        #endregion

        #region Page Events

        protected void Page_Init(object sender, EventArgs e)
        {
            Logger = IoC.Resolve<ISectionLogManager>();            
            CH = new ContentHelper(Logger);

            try
            {
                siteUser = SiteUser.Current;
                mDataManager = DataManager.Get(System.Web.HttpContext.Current.Session.SessionID); // using data manager from site user so we inherit storage account context.
            }
            catch
            {                
            } 


            //if the page is not a postback
            if (!Page.IsPostBack)
            {
                try
                {
                    //*FIRST* GET FORM GET OR POST DATA
                    if (Request.QueryString != null && Request.QueryString.HasKeys())
                    {
                        GetQueryStringData();                                
                    }
                    else
                    {                        
                        GetFormPostData();
                    }

                    // This is for scheduled bureau report when storage account context is not set, such as SiteUser.SwitchClient();
                    if(siteUser == null || siteUser.IsAnonymous)
                        mDataManager.SetOrgBasedStorageAccounts(new StorageAccountCriteria() { CID = cid, IsCommon = false, CWEnvironment = mDataManager.CWEnv, StorageAcctLevel = StorageAccountLevel.Primary });

                    //*SECOND* Check non anonymous. kgs super admin or higher or provider with super admin access to the client
                    if(!isScheduled)
                    {
                        //Check kgs super admin or higher, or super admin non provider, or super admin provider proxy.
                        if (siteUser.IsAnonymous)
                            Response.Redirect("/Home.aspx?referrer=" + Request.Url.PathAndQuery);
                        else if (!siteUser.IsFullAdminOrHigher)
                            Response.Redirect("/Home.aspx");                   
                    }

                    //*THIRD* Initialize Culture
                    DelayedInitializeCulture();                   

                    //*FOURTH* Set content
                    GetAndSetInitialContent();

                    //set initial base tag
                    //this.Header.Controls.AddAt(0, new BaseTag(Request)); 

                    //dynamically add themed css and favicons based on se theme check
                    HtmlHead head = (HtmlHead)Page.Header;
                    HtmlLink link = new HtmlLink();
                    link.Attributes.Add("href", isSETheme ? Page.ResolveClientUrl("/_assets/styles/themes/ba.css") : Page.ResolveClientUrl("/_assets/styles/themes/cw.css"));
                    link.Attributes.Add("type", "text/css");
                    link.Attributes.Add("rel", "stylesheet");
                    head.Controls.Add(link);

                    link = new HtmlLink();
                    link.Attributes.Add("href", isSETheme ? Page.ResolveClientUrl("/_assets/styles/themes/images/se-favicon.ico") : Page.ResolveClientUrl("/_assets/styles/themes/images/cw-favicon.ico"));
                    link.Attributes.Add("type", "image/x-icon");
                    link.Attributes.Add("rel", "shortcut icon");
                    head.Controls.Add(link); 
                }
                catch (Exception ex)
                {
                    lblError.Text = bureauInitializationFailed;
                    pnlError.Visible = true;
                    pnlReport.Visible = false;

                    Logger.Log(DataConstants.LogLevel.ERROR, "Bureau report initialization failed.", ex);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {      
            isSETheme = String.IsNullOrEmpty(hdnIsSETheme.Value) ? isSETheme : Convert.ToBoolean(hdnIsSETheme.Value);
            languageCultureName = String.IsNullOrEmpty(hdnLanguageCultureName.Value) ? languageCultureName : hdnLanguageCultureName.Value;
            isScheduled = String.IsNullOrEmpty(hdnIsScheduled.Value) ? isScheduled : Convert.ToBoolean(hdnIsScheduled.Value);

            GetAndSetPostbackContent();

            //if the page is not a postback
            if (!Page.IsPostBack)
            {
                try
                {
                    if (bids.Any())
                    {
                        //check if buildings are all of same culture
                        if (mDataManager.BuildingDataMapper.AreBuildingSettingsCulturesTheSame(bids))
                        {
                            //SET COMMON REPORT CONTENT AND FORMAT-----
                            SetCommonReportData();

                            //SET COVER PAGE---
                            SetCoverPage();

                            //SET CONTENTS PAGE---
                            SetContentsPage();

                            //SET EXPERT SUMMARY AND INPUTS---
                            SetExpertSummaryAndInputsVisiblity(mUserOrg);

                            //GET DIAGNOSTIC DATA-----
                            IEnumerable<DiagnosticsResult> diagnosticsResults = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(
                                new DiagnosticsResultsInputs()
                                {
                                    AnalysisRange = DataConstants.AnalysisRange.Daily,
                                    StartDate = sd,
                                    EndDate = ed,
                                    CID = cid,
                                },
                                bids);
                            IEnumerable<DiagnosticsResult> previousDiagnosticsResults = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(
                                                                    new DiagnosticsResultsInputs()
                                                                    {
                                                                        AnalysisRange = DataConstants.AnalysisRange.Daily,
                                                                        StartDate = psd,
                                                                        EndDate = ped,
                                                                        CID = cid
                                                                    }, bids);
                            IEnumerable<DiagnosticsResult> ventilationDiagnosticsResults = diagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.AirHandlerEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.FanEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.VentilationSystemEquipmentClassID);
                            IEnumerable<DiagnosticsResult> previousVentilationDiagnosticsResults = previousDiagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.AirHandlerEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.FanEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.VentilationSystemEquipmentClassID);
                            IEnumerable<DiagnosticsResult> heatingDiagnosticsResults = diagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.BoilerEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.HeatingSystemEquipmentClassID);
                            IEnumerable<DiagnosticsResult> previousHeatingDiagnosticsResults = previousDiagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.BoilerEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.HeatingSystemEquipmentClassID);
                            IEnumerable<DiagnosticsResult> coolingDiagnosticsResults = diagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.ChillerEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.CoolingSystemEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.HeatRejectionEquipmentClassID);
                            IEnumerable<DiagnosticsResult> previousCoolingDiagnosticsResults = previousDiagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.ChillerEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.CoolingSystemEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.HeatRejectionEquipmentClassID);
                            IEnumerable<DiagnosticsResult> plantDiagnosticsResults = diagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.DomesticWaterSystemEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.DualTemperatureWaterSystemEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.HeatExchangerEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.HeatRecoverySystemEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.PumpEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.StorageEquipmentClassID);
                            IEnumerable<DiagnosticsResult> previousPlantDiagnosticsResults = previousDiagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.DomesticWaterSystemEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.DualTemperatureWaterSystemEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.HeatExchangerEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.HeatRecoverySystemEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.PumpEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.StorageEquipmentClassID);
                            IEnumerable<DiagnosticsResult> zoneDiagnosticsResults = diagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.ZoneEquipmentEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.ZoneGroupEquipmentClassID);
                            IEnumerable<DiagnosticsResult> previousZoneDiagnosticsResults = previousDiagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.ZoneEquipmentEquipmentClassID || ec.EquipmentClassID == BusinessConstants.EquipmentClass.ZoneGroupEquipmentClassID);
                            IEnumerable<DiagnosticsResult> specialtyDiagnosticsResults = diagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.SpecialtyEquipmentEquipmentClassID);
                            IEnumerable<DiagnosticsResult> previousSpecialtyDiagnosticsResults = previousDiagnosticsResults.Where(ec => ec.EquipmentClassID == BusinessConstants.EquipmentClass.SpecialtyEquipmentEquipmentClassID);


                            //PERFORM TOTAL CALCULATIONS
                            PerformPortfolioTotalCalculations(diagnosticsResults, previousDiagnosticsResults);
                            PerformVentilationTotalCalculations(ventilationDiagnosticsResults, previousVentilationDiagnosticsResults);
                            PerformHeatingTotalCalculations(heatingDiagnosticsResults, previousHeatingDiagnosticsResults);
                            PerformCoolingTotalCalculations(coolingDiagnosticsResults, previousCoolingDiagnosticsResults);
                            PerformPlantTotalCalculations(plantDiagnosticsResults, previousPlantDiagnosticsResults);
                            PerformZoneTotalCalculations(zoneDiagnosticsResults, previousZoneDiagnosticsResults);
                            PerformSpecialtyTotalCalculations(specialtyDiagnosticsResults, previousSpecialtyDiagnosticsResults);

                            //AUTOMATED SUMMARY
                            //set content after tokens have been created.
                            //use url decode because of form post.
                            litAutomatedSummary.Text = TokenReplacer(Server.UrlDecode(automatedSummary));

                            //SET PERFROMANCE SUMMARY AND INPUTS------
                            SetPerformanceSummaryAndInputsVisiblity();

                            //SET BUILDING AND EQUIPMENT CLASS SUMMARY GRIDS AND CHARTS-----
                            SetBuildingAndEquipmentClassSummaryGridsAndCharts(diagnosticsResults, previousDiagnosticsResults);

                            //SET TOP DIAGNOSTIC GRIDS AND DIAGNOSTIC RESULTS-----
                            SetTopDiagnosticGridsAndResults(diagnosticsResults);

                            //SET PRORITIES TREND CHARTS-----
                            SetPrioritiesTrendCharts(diagnosticsResults);

                            //SET PRIORITIES PERIOD CHARTS-----
                            SetPrioritiesPeriodCharts();

                            //SET PRIORITIES TREND CONTENT-----
                            SetPrioritiesTrendContent();


                            Dictionary<Chart, int> charts = new Dictionary<Chart, int>();

                            #region VENTILATION

                            //SET VENTILATION SUMMARY GRID AND PIE CHARTS-----
                            charts = new Dictionary<Chart, int>();
                            charts.Add(ventilationSummaryAirHandlersPieChart, BusinessConstants.EquipmentClass.AirHandlerEquipmentClassID);
                            charts.Add(ventilationSummaryFansPieChart, BusinessConstants.EquipmentClass.FanEquipmentClassID);
                            charts.Add(ventilationSummaryVentilationSystemsPieChart, BusinessConstants.EquipmentClass.VentilationSystemEquipmentClassID);
                            SetEquipmentSummaryGridAndPieCharts(ventilationDiagnosticsResults, previousVentilationDiagnosticsResults, includeVentilationEquipmentReport, topVentilation, divContentsVentilationSummaryPage, divVentilationSummaryPage, gridVentilationSummary, charts);

                            //SET VENTILATION PRIORITIES TREND CHARTS
                            SetEquipmentPrioritiesTrendCharts(ventilationDiagnosticsResults, includeVentilationEquipmentReport, ventilationEnergyChart, ventilationMaintenanceChart, ventilationComfortChart);

                            //SET VENTILATION PRIORITIES PERIOD CHARTS
                            SetEquipmentPrioritiesPeriodCharts(includeVentilationEquipmentReport,
                                ventilationEnergyPeriodChart, new Pair(intervalVentilationCost, intervalPreviousVentilationCost),
                                ventilationMaintenancePeriodChart, new Pair(intervalVentilationMaintenanceIncidents, intervalPreviousVentilationMaintenanceIncidents),
                                ventilationComfortPeriodChart, new Pair(intervalVentilationComfortIncidents, intervalPreviousVentilationComfortIncidents));

                            #endregion

                            #region HEATING

                            //SET HEATING SUMMARY GRID AND PIE CHARTS-----
                            charts = new Dictionary<Chart, int>();
                            charts.Add(heatingSummaryBoilersPieChart, BusinessConstants.EquipmentClass.BoilerEquipmentClassID);
                            charts.Add(heatingSummaryHeatingSystemsPieChart, BusinessConstants.EquipmentClass.HeatingSystemEquipmentClassID);
                            SetEquipmentSummaryGridAndPieCharts(heatingDiagnosticsResults, previousHeatingDiagnosticsResults, includeHeatingEquipmentReport, topHeating, divContentsHeatingSummaryPage, divHeatingSummaryPage, gridHeatingSummary, charts);

                            //SET HEATING PRIORITIES TREND CHARTS
                            SetEquipmentPrioritiesTrendCharts(heatingDiagnosticsResults, includeHeatingEquipmentReport, heatingEnergyChart, heatingMaintenanceChart, heatingComfortChart);

                            //SET HEATING PRIORITIES PERIOD CHARTS
                            SetEquipmentPrioritiesPeriodCharts(includeHeatingEquipmentReport,
                                heatingEnergyPeriodChart, new Pair(intervalHeatingCost, intervalPreviousHeatingCost),
                                heatingMaintenancePeriodChart, new Pair(intervalHeatingMaintenanceIncidents, intervalPreviousHeatingMaintenanceIncidents),
                                heatingComfortPeriodChart, new Pair(intervalHeatingComfortIncidents, intervalPreviousHeatingComfortIncidents));

                            #endregion

                            #region COOLING

                            //SET COOLING SUMMARY GRID AND PIE CHARTS-----
                            charts = new Dictionary<Chart, int>();
                            charts.Add(coolingSummaryChillersPieChart, BusinessConstants.EquipmentClass.ChillerEquipmentClassID);
                            charts.Add(coolingSummaryCoolingSystemsPieChart, BusinessConstants.EquipmentClass.CoolingSystemEquipmentClassID);
                            charts.Add(coolingSummaryHeatRejectionPieChart, BusinessConstants.EquipmentClass.HeatRejectionEquipmentClassID);
                            SetEquipmentSummaryGridAndPieCharts(coolingDiagnosticsResults, previousCoolingDiagnosticsResults, includeCoolingEquipmentReport, topCooling, divContentsCoolingSummaryPage, divCoolingSummaryPage, gridCoolingSummary, charts);

                            //SET COOLING PRIORITIES TREND CHARTS
                            SetEquipmentPrioritiesTrendCharts(coolingDiagnosticsResults, includeCoolingEquipmentReport, coolingEnergyChart, coolingMaintenanceChart, coolingComfortChart);

                            //SET COOLING PRIORITIES PERIOD CHARTS
                            SetEquipmentPrioritiesPeriodCharts(includeCoolingEquipmentReport,
                                coolingEnergyPeriodChart, new Pair(intervalCoolingCost, intervalPreviousCoolingCost),
                                coolingMaintenancePeriodChart, new Pair(intervalCoolingMaintenanceIncidents, intervalPreviousCoolingMaintenanceIncidents),
                                coolingComfortPeriodChart, new Pair(intervalCoolingComfortIncidents, intervalPreviousCoolingComfortIncidents));


                            #endregion

                            #region PLANT

                            //SET PLANT SUMMARY GRID AND PIE CHARTS-----
                            charts = new Dictionary<Chart, int>();
                            charts.Add(plantSummaryDomesticWaterSystemsPieChart, BusinessConstants.EquipmentClass.DomesticWaterSystemEquipmentClassID);
                            charts.Add(plantSummaryDualTemperatureWaterSystemsPieChart, BusinessConstants.EquipmentClass.DualTemperatureWaterSystemEquipmentClassID);
                            charts.Add(plantSummaryHeatExchangerPieChart, BusinessConstants.EquipmentClass.HeatExchangerEquipmentClassID);
                            charts.Add(plantSummaryHeatRecoverySystemsPieChart, BusinessConstants.EquipmentClass.HeatRecoverySystemEquipmentClassID);
                            charts.Add(plantSummaryPumpsPieChart, BusinessConstants.EquipmentClass.PumpEquipmentClassID);
                            charts.Add(plantSummaryStoragePieChart, BusinessConstants.EquipmentClass.StorageEquipmentClassID);
                            SetEquipmentSummaryGridAndPieCharts(plantDiagnosticsResults, previousPlantDiagnosticsResults, includePlantEquipmentReport, topPlant, divContentsPlantSummaryPage, divPlantSummaryPage, gridPlantSummary, charts);

                            //SET PLANT PRIORITIES TREND CHARTS
                            SetEquipmentPrioritiesTrendCharts(plantDiagnosticsResults, includePlantEquipmentReport, plantEnergyChart, plantMaintenanceChart, plantComfortChart);

                            //SET PLANT PRIORITIES PERIOD CHARTS
                            SetEquipmentPrioritiesPeriodCharts(includePlantEquipmentReport,
                                plantEnergyPeriodChart, new Pair(intervalPlantCost, intervalPreviousPlantCost),
                                plantMaintenancePeriodChart, new Pair(intervalPlantMaintenanceIncidents, intervalPreviousPlantMaintenanceIncidents),
                                plantComfortPeriodChart, new Pair(intervalPlantComfortIncidents, intervalPreviousPlantComfortIncidents));


                            #endregion

                            #region ZONE

                            //SET ZONE SUMMARY GRID AND PIE CHARTS-----
                            charts = new Dictionary<Chart, int>();
                            charts.Add(zoneSummaryZoneEquipmentPieChart, BusinessConstants.EquipmentClass.ZoneEquipmentEquipmentClassID);
                            charts.Add(zoneSummaryZoneGroupsPieChart, BusinessConstants.EquipmentClass.ZoneGroupEquipmentClassID);
                            SetEquipmentSummaryGridAndPieCharts(zoneDiagnosticsResults, previousZoneDiagnosticsResults, includeZoneEquipmentReport, topZone, divContentsZoneSummaryPage, divZoneSummaryPage, gridZoneSummary, charts);

                            //SET ZONE PRIORITIES TREND CHARTS
                            SetEquipmentPrioritiesTrendCharts(zoneDiagnosticsResults, includeZoneEquipmentReport, zoneEnergyChart, zoneMaintenanceChart, zoneComfortChart);

                            //SET ZONE PRIORITIES PERIOD CHARTS
                            SetEquipmentPrioritiesPeriodCharts(includeZoneEquipmentReport,
                                zoneEnergyPeriodChart, new Pair(intervalZoneCost, intervalPreviousZoneCost),
                                zoneMaintenancePeriodChart, new Pair(intervalZoneMaintenanceIncidents, intervalPreviousZoneMaintenanceIncidents),
                                zoneComfortPeriodChart, new Pair(intervalZoneComfortIncidents, intervalPreviousZoneComfortIncidents));


                            #endregion

                            #region SPECIALTY

                            //SET SPECIALTY SUMMARY GRID AND PIE CHARTS-----
                            charts = new Dictionary<Chart, int>();
                            charts.Add(specialtySummarySpecialtyEquipmentPieChart, BusinessConstants.EquipmentClass.SpecialtyEquipmentEquipmentClassID);
                            SetEquipmentSummaryGridAndPieCharts(specialtyDiagnosticsResults, previousSpecialtyDiagnosticsResults, includeSpecialtyEquipmentReport, topSpecialty, divContentsSpecialtySummaryPage, divSpecialtySummaryPage, gridSpecialtySummary, charts);

                            //SET SPECIALTY PRIORITIES TREND CHARTS
                            SetEquipmentPrioritiesTrendCharts(specialtyDiagnosticsResults, includeSpecialtyEquipmentReport, specialtyEnergyChart, specialtyMaintenanceChart, specialtyComfortChart);

                            //SET SPECIALTY PRIORITIES PERIOD CHARTS
                            SetEquipmentPrioritiesPeriodCharts(includeSpecialtyEquipmentReport,
                                specialtyEnergyPeriodChart, new Pair(intervalSpecialtyCost, intervalPreviousSpecialtyCost),
                                specialtyMaintenancePeriodChart, new Pair(intervalSpecialtyMaintenanceIncidents, intervalPreviousSpecialtyMaintenanceIncidents),
                                specialtyComfortPeriodChart, new Pair(intervalSpecialtyComfortIncidents, intervalPreviousSpecialtyComfortIncidents));


                            #endregion


                            //SET ALARMS GRIDS-----
                            SetAlarmsGrids();

                            //SET PROJECTS GRID-----
                            SetProjectsGrid();

                            //SET TASKS GRIDS-----
                            SetTasksGrids();

                            pnlError.Visible = false;
                            pnlReport.Visible = true;
                        }
                        else
                        {
                            lblError.Text = buildingCultureError;
                            pnlError.Visible = true;
                            pnlReport.Visible = false;
                        }
                    }
                    else
                    {
                        lblError.Text = noBuildingsError;
                        pnlError.Visible = true;
                        pnlReport.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    lblError.Text = bureauGenerationFailed;
                    pnlError.Visible = true;
                    pnlReport.Visible = false;

                    Logger.Log(DataConstants.LogLevel.ERROR, "Bureau report generation failed.", ex);
                }
            }
        }

        #endregion

        #region Perform Total Calculations

        protected void PerformPortfolioTotalCalculations(IEnumerable<DiagnosticsResult> diagnosticsResults, IEnumerable<DiagnosticsResult> previousDiagnosticsResults)
        {
            totalCost = Math.Ceiling(diagnosticsResults.Sum(d => d.CostSavings));
            previousTotalCost = Math.Ceiling(previousDiagnosticsResults.Sum(d => d.CostSavings));
            energyFaultCountAccrossWholePeriod = diagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
            previousEnergyFaultCountAccrossWholePeriod = previousDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
            maintenanceFaultCountAccrossWholePeriod = diagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
            previousMaintenanceFaultCountAccrossWholePeriod = previousDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
            comfortFaultCountAccrossWholePeriod = diagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
            previousComfortFaultCountAccrossWholePeriod = previousDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();


            //TODO culture for currencty hex
            //total avoidable cost
            litAvoidableCostTotalValue.Text = String.Format("<span class='valueLargeColored'>{0}</span>", CultureHelper.FormatCurrencyAsString(buildingLCID, totalCost));

            //change in avoidable cost
            totalCostPercent = Math.Round((((totalCost - previousTotalCost) / NumericHelper.CleanseDivideByZero(previousTotalCost)) * 100), 1, MidpointRounding.AwayFromZero);
            totalCostPercentString = Math.Abs(totalCostPercent).ToString() + "%";
            litAvoidableCostChangeValue.Text = String.Format("<span class='valueLargeColored'>{0}</span>", (previousTotalCost - totalCost == 0 ? "N/A" : totalCostPercentString));
            divAvoidableCostChangeString.InnerHtml = CH.GetSafeContentItem(mContent, (previousTotalCost > totalCost ? "DecreaseLastReportParagraph" : (previousTotalCost == totalCost ? "UnchangedLastReportParagraph" : "IncreaseLastReportParagraph")), this.GetType().Name, isSETheme);
            imgAvoidableCostsArrow.Src = (previousTotalCost > totalCost ? "_assets/styles/themes/customReportImages/arrow-down" : (previousTotalCost == totalCost ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");


            //calculate percentages, incidents, and trend summaries-----

            //round up to nearest cost int.
            intervalCost = Math.Ceiling(totalCost / reportNumberOfDays);
            intervalPreviousCost = Math.Ceiling(previousTotalCost / previousReportNumberOfDays);
            intervalCostPercent = Math.Round((((intervalCost - intervalPreviousCost) / NumericHelper.CleanseDivideByZero(intervalPreviousCost)) * 100), 1, MidpointRounding.AwayFromZero);
            intervalCostPercentString = Math.Abs(intervalCostPercent).ToString() + "%";

            spanCostTrendSummary.InnerText = CH.GetAndFormatSafeContentItem(mContent, "CostTrendSummaryParagraph", new[] { CultureHelper.FormatCurrencyAsString(buildingLCID, intervalCost) }, this.GetType().Name, isSETheme); // Costs {1}.</span>", CultureHelper.FormatCurrencyAsString(lcid, intervalCost), (intervalPreviousCost > intervalCost ? "decreasing by " + intervalCostPercent : (intervalPreviousCost == intervalCost ? "unchanged" : "increasing by " + intervalCostPercent))

            imgCostArrow.Src = (intervalPreviousCost > intervalCost ? "_assets/styles/themes/customReportImages/arrow-down" : (intervalPreviousCost == intervalCost ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");

            //TODO: based on avg priority per day or total faults per day?
            intervalEnergyPercent = Math.Round(((((energyFaultCountAccrossWholePeriod / reportNumberOfDays) - (previousEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays)) / (previousEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays)) * 100), 1, MidpointRounding.AwayFromZero);
            intervalEnergyPercentString = Math.Round(((Math.Abs((energyFaultCountAccrossWholePeriod / reportNumberOfDays) - (previousEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays)) / (previousEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays)) * 100), 1, MidpointRounding.AwayFromZero).ToString() + "%";
            intervalEnergyIncidents = Math.Round(energyFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            intervalPreviousEnergyIncidents = Math.Round(previousEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

            string[] eInputs = { (intervalPreviousEnergyIncidents == intervalEnergyIncidents) ? String.Empty : intervalEnergyPercentString, intervalEnergyIncidents.ToString() };
            spanEnergyTrendSummary.InnerText = CH.GetAndFormatSafeContentItem(mContent, (intervalPreviousEnergyIncidents > intervalEnergyIncidents ? "PriorityTrendSummaryDecreasingParagraph" : (intervalPreviousEnergyIncidents == intervalEnergyIncidents ? "PriorityTrendSummaryUnchangedParagraph" : "PriorityTrendSummaryIncreasingParagraph")), eInputs, this.GetType().Name, isSETheme);

            imgEnergyArrow.Src = (intervalPreviousEnergyIncidents > intervalEnergyIncidents ? "_assets/styles/themes/customReportImages/arrow-down" : (intervalPreviousEnergyIncidents == intervalEnergyIncidents ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");


            intervalMaintenancePercent = Math.Round(((((maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays) - (previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays)) / (previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays)) * 100), 1, MidpointRounding.AwayFromZero);
            intervalMaintenancePercentString = Math.Round(((Math.Abs((maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays) - (previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays)) / (previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays)) * 100), 1, MidpointRounding.AwayFromZero).ToString() + "%";
            intervalMaintenanceIncidents = Math.Round(maintenanceFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            intervalPreviousMaintenanceIncidents = Math.Round(previousMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

            string[] mInputs = { (intervalPreviousMaintenanceIncidents == intervalMaintenanceIncidents) ? String.Empty : intervalMaintenancePercentString, intervalMaintenanceIncidents.ToString() };
            spanMaintenanceTrendSummary.InnerText = CH.GetAndFormatSafeContentItem(mContent, (intervalPreviousMaintenanceIncidents > intervalMaintenanceIncidents ? "PriorityTrendSummaryDecreasingParagraph" : (intervalPreviousMaintenanceIncidents == intervalMaintenanceIncidents ? "PriorityTrendSummaryUnchangedParagraph" : "PriorityTrendSummaryIncreasingParagraph")), mInputs, this.GetType().Name, isSETheme);

            imgMaintenanceArrow.Src = (intervalPreviousMaintenanceIncidents > intervalMaintenanceIncidents ? "_assets/styles/themes/customReportImages/arrow-down" : (intervalPreviousMaintenanceIncidents == intervalMaintenanceIncidents ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");


            intervalComfortPercent = Math.Round(((((comfortFaultCountAccrossWholePeriod / reportNumberOfDays) - (previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays)) / (previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays)) * 100), 1, MidpointRounding.AwayFromZero);
            intervalComfortPercentString = Math.Round(((Math.Abs((comfortFaultCountAccrossWholePeriod / reportNumberOfDays) - (previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays)) / (previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays)) * 100), 1, MidpointRounding.AwayFromZero).ToString() + "%";
            intervalComfortIncidents = Math.Round(comfortFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            intervalPreviousComfortIncidents = Math.Round(previousComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

            string[] cInputs = { (intervalPreviousComfortIncidents == intervalComfortIncidents) ? String.Empty : intervalComfortPercentString, intervalComfortIncidents.ToString() };
            spanComfortTrendSummary.InnerText = CH.GetAndFormatSafeContentItem(mContent, (intervalPreviousComfortIncidents > intervalComfortIncidents ? "PriorityTrendSummaryDecreasingParagraph" : (intervalPreviousComfortIncidents == intervalComfortIncidents ? "PriorityTrendSummaryUnchangedParagraph" : "PriorityTrendSummaryIncreasingParagraph")), cInputs, this.GetType().Name, isSETheme);

            imgComfortArrow.Src = (intervalPreviousComfortIncidents > intervalComfortIncidents ? "_assets/styles/themes/customReportImages/arrow-down" : (intervalPreviousComfortIncidents == intervalComfortIncidents ? "_assets/styles/themes/customReportImages/arrow-right" : "_assets/styles/themes/customReportImages/arrow-up")) + (isSETheme ? "-se.png" : ".png");
        }

        protected void PerformVentilationTotalCalculations(IEnumerable<DiagnosticsResult> ventilationDiagnosticsResults, IEnumerable<DiagnosticsResult> previousVentilationDiagnosticsResults)
        {
            if (includeVentilationEquipmentReport)
            {
                ventilationTotalCost = Math.Ceiling(ventilationDiagnosticsResults.Sum(d => d.CostSavings));
                previousVentilationTotalCost = Math.Ceiling(previousVentilationDiagnosticsResults.Sum(d => d.CostSavings));
                ventilationEnergyFaultCountAccrossWholePeriod = ventilationDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                previousVentilationEnergyFaultCountAccrossWholePeriod = previousVentilationDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                ventilationMaintenanceFaultCountAccrossWholePeriod = ventilationDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                previousVentilationMaintenanceFaultCountAccrossWholePeriod = previousVentilationDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                ventilationComfortFaultCountAccrossWholePeriod = ventilationDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
                previousVentilationComfortFaultCountAccrossWholePeriod = previousVentilationDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();

                //calculate percentages, incidents, and trend summaries-----

                intervalVentilationCost = Math.Ceiling(ventilationTotalCost / reportNumberOfDays);
                intervalPreviousVentilationCost = Math.Ceiling(previousVentilationTotalCost / previousReportNumberOfDays);

                //TODO: based on avg priority per day or total faults per day?
                intervalVentilationEnergyIncidents = Math.Round(ventilationEnergyFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousVentilationEnergyIncidents = Math.Round(previousVentilationEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalVentilationMaintenanceIncidents = Math.Round(ventilationMaintenanceFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousVentilationMaintenanceIncidents = Math.Round(previousVentilationMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalVentilationComfortIncidents = Math.Round(ventilationComfortFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousVentilationComfortIncidents = Math.Round(previousVentilationComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            }
        }

        protected void PerformHeatingTotalCalculations(IEnumerable<DiagnosticsResult> heatingDiagnosticsResults, IEnumerable<DiagnosticsResult> previousHeatingDiagnosticsResults)
        {
            if (includeHeatingEquipmentReport)
            {
                heatingTotalCost = Math.Ceiling(heatingDiagnosticsResults.Sum(d => d.CostSavings));
                previousHeatingTotalCost = Math.Ceiling(previousHeatingDiagnosticsResults.Sum(d => d.CostSavings));
                heatingEnergyFaultCountAccrossWholePeriod = heatingDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                previousHeatingEnergyFaultCountAccrossWholePeriod = previousHeatingDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                heatingMaintenanceFaultCountAccrossWholePeriod = heatingDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                previousHeatingMaintenanceFaultCountAccrossWholePeriod = previousHeatingDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                heatingComfortFaultCountAccrossWholePeriod = heatingDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
                previousHeatingComfortFaultCountAccrossWholePeriod = previousHeatingDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();

                //calculate percentages, incidents, and trend summaries-----

                intervalHeatingCost = Math.Ceiling(heatingTotalCost / reportNumberOfDays);
                intervalPreviousHeatingCost = Math.Ceiling(previousHeatingTotalCost / previousReportNumberOfDays);

                //TODO: based on avg priority per day or total faults per day?
                intervalHeatingEnergyIncidents = Math.Round(heatingEnergyFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousHeatingEnergyIncidents = Math.Round(previousHeatingEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalHeatingMaintenanceIncidents = Math.Round(heatingMaintenanceFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousHeatingMaintenanceIncidents = Math.Round(previousHeatingMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalHeatingComfortIncidents = Math.Round(heatingComfortFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousHeatingComfortIncidents = Math.Round(previousHeatingComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            }
        }

        protected void PerformCoolingTotalCalculations(IEnumerable<DiagnosticsResult> coolingDiagnosticsResults, IEnumerable<DiagnosticsResult> previousCoolingDiagnosticsResults)
        {
            if (includeCoolingEquipmentReport)
            {
                coolingTotalCost = Math.Ceiling(coolingDiagnosticsResults.Sum(d => d.CostSavings));
                previousCoolingTotalCost = Math.Ceiling(previousCoolingDiagnosticsResults.Sum(d => d.CostSavings));
                coolingEnergyFaultCountAccrossWholePeriod = coolingDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                previousCoolingEnergyFaultCountAccrossWholePeriod = previousCoolingDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                coolingMaintenanceFaultCountAccrossWholePeriod = coolingDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                previousCoolingMaintenanceFaultCountAccrossWholePeriod = previousCoolingDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                coolingComfortFaultCountAccrossWholePeriod = coolingDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
                previousCoolingComfortFaultCountAccrossWholePeriod = previousCoolingDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();

                //calculate percentages, incidents, and trend summaries-----

                intervalCoolingCost = Math.Ceiling(coolingTotalCost / reportNumberOfDays);
                intervalPreviousCoolingCost = Math.Ceiling(previousCoolingTotalCost / previousReportNumberOfDays);

                //TODO: based on avg priority per day or total faults per day?
                intervalCoolingEnergyIncidents = Math.Round(coolingEnergyFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousCoolingEnergyIncidents = Math.Round(previousCoolingEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalCoolingMaintenanceIncidents = Math.Round(coolingMaintenanceFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousCoolingMaintenanceIncidents = Math.Round(previousCoolingMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalCoolingComfortIncidents = Math.Round(coolingComfortFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousCoolingComfortIncidents = Math.Round(previousCoolingComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            }
        }

        protected void PerformPlantTotalCalculations(IEnumerable<DiagnosticsResult> plantDiagnosticsResults, IEnumerable<DiagnosticsResult> previousPlantDiagnosticsResults)
        {
            if (includePlantEquipmentReport)
            {
                plantTotalCost = Math.Ceiling(plantDiagnosticsResults.Sum(d => d.CostSavings));
                previousPlantTotalCost = Math.Ceiling(previousPlantDiagnosticsResults.Sum(d => d.CostSavings));
                plantEnergyFaultCountAccrossWholePeriod = plantDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                previousPlantEnergyFaultCountAccrossWholePeriod = previousPlantDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                plantMaintenanceFaultCountAccrossWholePeriod = plantDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                previousPlantMaintenanceFaultCountAccrossWholePeriod = previousPlantDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                plantComfortFaultCountAccrossWholePeriod = plantDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
                previousPlantComfortFaultCountAccrossWholePeriod = previousPlantDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();

                //calculate percentages, incidents, and trend summaries-----

                intervalPlantCost = Math.Ceiling(plantTotalCost / reportNumberOfDays);
                intervalPreviousPlantCost = Math.Ceiling(previousPlantTotalCost / previousReportNumberOfDays);

                //TODO: based on avg priority per day or total faults per day?
                intervalPlantEnergyIncidents = Math.Round(plantEnergyFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousPlantEnergyIncidents = Math.Round(previousPlantEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalPlantMaintenanceIncidents = Math.Round(plantMaintenanceFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousPlantMaintenanceIncidents = Math.Round(previousPlantMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalPlantComfortIncidents = Math.Round(plantComfortFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousPlantComfortIncidents = Math.Round(previousPlantComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            }
        }

        protected void PerformZoneTotalCalculations(IEnumerable<DiagnosticsResult> zoneDiagnosticsResults, IEnumerable<DiagnosticsResult> previousZoneDiagnosticsResults)
        {
            if (includeZoneEquipmentReport)
            {
                zoneTotalCost = Math.Ceiling(zoneDiagnosticsResults.Sum(d => d.CostSavings));
                previousZoneTotalCost = Math.Ceiling(previousZoneDiagnosticsResults.Sum(d => d.CostSavings));
                zoneEnergyFaultCountAccrossWholePeriod = zoneDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                previousZoneEnergyFaultCountAccrossWholePeriod = previousZoneDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                zoneMaintenanceFaultCountAccrossWholePeriod = zoneDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                previousZoneMaintenanceFaultCountAccrossWholePeriod = previousZoneDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                zoneComfortFaultCountAccrossWholePeriod = zoneDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
                previousZoneComfortFaultCountAccrossWholePeriod = previousZoneDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();

                //calculate percentages, incidents, and trend summaries-----

                intervalZoneCost = Math.Ceiling(zoneTotalCost / reportNumberOfDays);
                intervalPreviousZoneCost = Math.Ceiling(previousZoneTotalCost / previousReportNumberOfDays);

                //TODO: based on avg priority per day or total faults per day?
                intervalZoneEnergyIncidents = Math.Round(zoneEnergyFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousZoneEnergyIncidents = Math.Round(previousZoneEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalZoneMaintenanceIncidents = Math.Round(zoneMaintenanceFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousZoneMaintenanceIncidents = Math.Round(previousZoneMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalZoneComfortIncidents = Math.Round(zoneComfortFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousZoneComfortIncidents = Math.Round(previousZoneComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            }
        }

        protected void PerformSpecialtyTotalCalculations(IEnumerable<DiagnosticsResult> specialtyDiagnosticsResults, IEnumerable<DiagnosticsResult> previousSpecialtyDiagnosticsResults)
        {
            if (includeSpecialtyEquipmentReport)
            {
                specialtyTotalCost = Math.Ceiling(specialtyDiagnosticsResults.Sum(d => d.CostSavings));
                previousSpecialtyTotalCost = Math.Ceiling(previousSpecialtyDiagnosticsResults.Sum(d => d.CostSavings));
                specialtyEnergyFaultCountAccrossWholePeriod = specialtyDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                previousSpecialtyEnergyFaultCountAccrossWholePeriod = previousSpecialtyDiagnosticsResults.Where(d => d.EnergyPriority > 0).Count();
                specialtyMaintenanceFaultCountAccrossWholePeriod = specialtyDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                previousSpecialtyMaintenanceFaultCountAccrossWholePeriod = previousSpecialtyDiagnosticsResults.Where(d => d.MaintenancePriority > 0).Count();
                specialtyComfortFaultCountAccrossWholePeriod = specialtyDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();
                previousSpecialtyComfortFaultCountAccrossWholePeriod = previousSpecialtyDiagnosticsResults.Where(d => d.ComfortPriority > 0).Count();

                //calculate percentages, incidents, and trend summaries-----

                intervalSpecialtyCost = Math.Ceiling(specialtyTotalCost / reportNumberOfDays);
                intervalPreviousSpecialtyCost = Math.Ceiling(previousSpecialtyTotalCost / previousReportNumberOfDays);

                //TODO: based on avg priority per day or total faults per day?
                intervalSpecialtyEnergyIncidents = Math.Round(specialtyEnergyFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousSpecialtyEnergyIncidents = Math.Round(previousSpecialtyEnergyFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalSpecialtyMaintenanceIncidents = Math.Round(specialtyMaintenanceFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousSpecialtyMaintenanceIncidents = Math.Round(previousSpecialtyMaintenanceFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);

                intervalSpecialtyComfortIncidents = Math.Round(specialtyComfortFaultCountAccrossWholePeriod / reportNumberOfDays, 1, MidpointRounding.AwayFromZero);
                intervalPreviousSpecialtyComfortIncidents = Math.Round(previousSpecialtyComfortFaultCountAccrossWholePeriod / previousReportNumberOfDays, 1, MidpointRounding.AwayFromZero);
            }
        }

        #endregion

        #region Set Data Methods

        private void SetCommonReportData()
        {
            //set user org info
            mUserOrg = mDataManager.OrganizationDataMapper.GetOrganizationByID(oid);

            //set client
            mClient = mDataManager.ClientDataMapper.GetClient(cid);
            hdnCID.Value = cid.ToString();

            //set theme
            hdnIsSETheme.Value = isSETheme.ToString();

            //set language name
            hdnLanguageCultureName.Value = languageCultureName;

            //mulltiple vs single building
            if (bids.Count() > 1)
            {
                //buildings
                IEnumerable<Building> buildings = mDataManager.BuildingDataMapper.GetBuildingsByBIDList(bids, true);

                //header and new page client and user title
                headerClient1.InnerText = headerClient2.InnerText = newPageClient1.InnerText = newPageClient2.InnerText = newPageClient3.InnerText = newPageClient4.InnerText = newPageClient5.InnerText = newPageClient6.InnerText = newPageClient7.InnerText = newPageClient8.InnerText = newPageClient9.InnerText = newPageClient10.InnerText = newPageClient11.InnerText = newPageClient12.InnerText = newPageClient13.InnerText = newPageClient14.InnerText = newPageClient15.InnerText = newPageClient16.InnerText = mClient.ClientName;

                //building info
                clientName.InnerText = mClient.ClientName;
                buildingName.InnerText = CH.GetSafeContentItem(mContent, "MultipleLabel", this.GetType().Name, isSETheme);
                buildingLocation.InnerText = CH.GetSafeContentItem(mContent, "MultipleLabel", this.GetType().Name, isSETheme);
                buildingType.InnerText = CH.GetSafeContentItem(mContent, "VariesLabel", this.GetType().Name, isSETheme);
                buildingCount.InnerText = buildings.Count().ToString();
                buildingYear.InnerText = CH.GetSafeContentItem(mContent, "VariesLabel", this.GetType().Name, isSETheme);
                buildingArea.InnerText = buildings.Sum(b => b.Sqft).ToString();

                //set lcid
                buildingLCID = buildings.First().BuildingSettings.First().LCID;
            }
            else
            {
                //get building
                mBuilding = mDataManager.BuildingDataMapper.GetBuildingByBID(bids.First(), true, true, true, true, true, true);

                //header and new page client and user title
                headerClient1.InnerText = headerClient2.InnerText = newPageClient1.InnerText = newPageClient2.InnerText = newPageClient3.InnerText = newPageClient4.InnerText = newPageClient5.InnerText = newPageClient6.InnerText = newPageClient7.InnerText = newPageClient8.InnerText = newPageClient9.InnerText = newPageClient10.InnerText = newPageClient11.InnerText = newPageClient12.InnerText = newPageClient13.InnerText = newPageClient14.InnerText = newPageClient15.InnerText = newPageClient16.InnerText = mBuilding.Client.ClientName;

                //building info
                clientName.InnerText = mBuilding.Client.ClientName;
                buildingName.InnerText = mBuilding.BuildingName;
                buildingLocation.InnerText = mBuilding.Address;
                buildingType.InnerText = mBuilding.BuildingType.BuildingTypeName;
                buildingCount.InnerText = "1";
                buildingYear.InnerText = mBuilding.YearBuilt.ToString();
                buildingArea.InnerText = mBuilding.Sqft.ToString();

                //set lcid
                buildingLCID = mBuilding.BuildingSettings.First().LCID;
            }

            //set report period days
            reportNumberOfDays = DateTimeHelper.GetDiferenceBetweenDateTimesInDays(sd, ed) + 1;
            previousReportNumberOfDays = DateTimeHelper.GetDiferenceBetweenDateTimesInDays(psd, ped) + 1;

            //header user title
            if (!String.IsNullOrEmpty(userTitle))
                headerUserTitle1.InnerText = headerUserTitle2.InnerText = newPageUserTitle1.InnerText = newPageUserTitle2.InnerText = newPageUserTitle3.InnerText = newPageUserTitle4.InnerText = newPageUserTitle5.InnerText = newPageUserTitle6.InnerText = newPageUserTitle7.InnerText = newPageUserTitle8.InnerText = newPageUserTitle9.InnerText = newPageUserTitle10.InnerText = newPageUserTitle11.InnerText = newPageUserTitle12.InnerText = newPageUserTitle13.InnerText = newPageUserTitle14.InnerText = newPageUserTitle15.InnerText = newPageUserTitle16.InnerText = " - " + userTitle;

            //set theme 
            form1.Attributes["class"] = "bureau" + (isSETheme ? ReportTheme.SE : ReportTheme.KGS);

            //header image
            headerImage1.Src = headerImage2.Src = isSETheme ? "_assets/styles/themes/customReportImages/magnify-se.png" : "_assets/styles/themes/images/cw-gear-large.png";

            //header title
            headerTitle1.InnerText = headerTitle2.InnerText = TokenVariableHelper.FormatSpecialThemeBasedTokens(isSETheme, "$product");

            //period
            litHeaderPeriod1.Text = litHeaderPeriod2.Text = CreatePeriodContent(sd, ed);

            if (sd != psd || ed != ped)
            {
                divComparedTo1.Visible = divComparedTo2.Visible = true;
                litHeaderPreviousPeriod1.Text = litHeaderPreviousPeriod2.Text = CreatePeriodContent(psd, ped);
            }

            //newpage images
            //imgNewPageBuilding13.Src =
            imgNewPageBuilding1.Src = imgNewPageBuilding2.Src = imgNewPageBuilding3.Src = imgNewPageBuilding4.Src = imgNewPageBuilding5.Src = imgNewPageBuilding6.Src = imgNewPageBuilding7.Src = imgNewPageBuilding8.Src = imgNewPageBuilding9.Src = imgNewPageBuilding10.Src = imgNewPageBuilding11.Src = imgNewPageBuilding12.Src = imgNewPageBuilding14.Src = imgNewPageBuilding15.Src = imgNewPageBuilding16.Src = isSETheme ? "_assets/styles/themes/customReportImages/magnify-small-se.png" : "_assets/styles/themes/images/cw-gears-large.png";

            //taglines
            litTagline1.Text = litTagline2.Text = isSETheme ? seTagline : kgsTagline;

            imgCoverThemed.Src = isSETheme ? "_assets/styles/themes/images/se-header.png" : "_assets/styles/themes/images/kgs-header.png";
            imgBottomPage1.Src = imgBottomPage2.Src = imgBottomPageFinal.Src = isSETheme ? "_assets/styles/themes/images/se-header-secondary.png" : "_assets/styles/themes/images/kgs-header-no-shadow.png";         
        }

        private void SetCoverPage()
        {
            divCoverPage.Visible = includeCoverPage;

            hdnShowClientLogos.Value = showClientLogos.ToString();

            if (showClientLogos && includeCoverPage && !String.IsNullOrEmpty(mClient.ImageExtension))
            {
                var image = new ClientProfileImage(mClient, mDataManager.OrgBasedBlobStorageProvider);

                if (image.Exists())
                {
                    var byteArray = image.Retrieve();

                    if (isScheduled)
                    {
                        radCoverClient.DataValue = byteArray;
                    }
                    else
                    {
                        radCoverClient.ImageUrl = HandlerHelper.ClientImageUrl(cid, mClient.ImageExtension);
                        hdnCoverClientByteArray.Value = Convert.ToBase64String(byteArray);
                    }
                }
                else
                {
                    radCoverClient.Visible = false;
                }
            }
            else
            {
                radCoverClient.Visible = false;
            }
        }

        private void SetContentsPage()
        {
            divContentsPage.Visible = includeContentsPage;
        }

        /// <summary>
        /// set expert summary and inputs visiblity.
        /// </summary>
        private void SetExpertSummaryAndInputsVisiblity(Organization userOrg)
        {
            divContentsExpertSummaryPage.Visible = divExpertSummaryPage.Visible = includeExpertSummary;
            gridComfort.Columns[3].Visible = includeExpertSummary;
            gridMaintenance.Columns[3].Visible = includeExpertSummary;
            gridEnergy.Columns[3].Visible = includeExpertSummary;

            if (includeExpertSummary)
            {
                //user
                mUser = mDataManager.UserDataMapper.GetUserWithCountryAndState(uid);

                //prepared by
                preparedBy.InnerText = String.Format("{0} - {1}", mUser.FirstName + " " + mUser.LastName, userOrg.OrganizationName);
                userAddress.InnerText = String.Format("{0}, {1}, {2} {3}", mUser.Address, mUser.City, mUser.StateName, mUser.Zip);
                userPhone.InnerText = String.IsNullOrEmpty(mUser.Phone) ? mUser.MobilePhone : mUser.Phone;
                userEmail.InnerText = mUser.Email;

                //expert content
                litExpertSummary.Text = Server.UrlDecode(TokenReplacer(expertSummary));
            }

            divEnergyTrendAnalysis.Visible = includeExpertSummary;
            divMaintenanceTrendAnalysis.Visible = includeExpertSummary;
            divComfortTrendAnalysis.Visible = includeExpertSummary;
        }


        private void SetPerformanceSummaryAndInputsVisiblity()
        {
            //utility consumption

            //utility cost

            //utility container 
            divUtilityContainer.Visible = includePerformanceReport;
        }

        /// <summary>
        /// sets the buiding and equipment class summery grids.
        /// </summary>
        /// <param name="diagnosticsResults"></param>
        private void SetBuildingAndEquipmentClassSummaryGridsAndCharts(IEnumerable<DiagnosticsResult> diagnosticsResults, IEnumerable<DiagnosticsResult> previousDiagnosticsResults)
        {
            IEnumerable<DiagnosticsBureauReportResult> buildingResults = Enumerable.Empty<DiagnosticsBureauReportResult>();
            IEnumerable<DiagnosticsBureauReportResult> equipmentClassResults = Enumerable.Empty<DiagnosticsBureauReportResult>();

            divContentsBuildingSummaryPage.Visible = divBuildingSummaryPage.Visible = includeBuildingSummaryReport;
            divContentsEquipmentClassSummaryPage.Visible = divEquipmentClassSummaryPage.Visible = includeEquipmentClassSummaryReport;

            if (includeBuildingSummaryReport)
            {
                buildingResults = CustomReportsGridResultsHelper.CalculateAndMergeBuildingGridResults(diagnosticsResults, reportNumberOfDays, previousDiagnosticsResults, previousReportNumberOfDays);

                gridBuildingSummary.DataSource = buildingResults;
                gridBuildingSummary.DataBind();

                //Set footer totals
                if (buildingResults.Any())
                {
                    ((Label)gridBuildingSummary.FooterRow.FindControl("lblGridTotalEquipmentCount")).Text = buildingResults.Sum(b => b.EquipmentCount).ToString();
                    ((Label)gridBuildingSummary.FooterRow.FindControl("lblGridTotalEquipmentWithFaultsCount")).Text = buildingResults.Sum(b => b.EquipmentWithFaultsCount).ToString();
                    ((Label)gridBuildingSummary.FooterRow.FindControl("lblGridTotalCostSavingsCultureFormated")).Text = CultureHelper.FormatCurrencyAsString(buildingLCID, buildingResults.Sum(b => b.TotalCostSavings).ToString());
                }
            }

            if (includeEquipmentClassSummaryReport)
            {
                equipmentClassResults = CustomReportsGridResultsHelper.CalculateAndMergeEquipmentClassGridResults(diagnosticsResults, reportNumberOfDays, previousDiagnosticsResults, previousReportNumberOfDays);

                gridEquipmentClassSummary.DataSource = equipmentClassResults;
                gridEquipmentClassSummary.DataBind();

                if (equipmentClassResults.Any())
                {
                    //Set footer totals
                    ((Label)gridEquipmentClassSummary.FooterRow.FindControl("lblGridTotalEquipmentCount")).Text = equipmentClassResults.Sum(b => b.EquipmentCount).ToString();
                    ((Label)gridEquipmentClassSummary.FooterRow.FindControl("lblGridTotalEquipmentWithFaultsCount")).Text = equipmentClassResults.Sum(b => b.EquipmentWithFaultsCount).ToString();
                    ((Label)gridEquipmentClassSummary.FooterRow.FindControl("lblGridTotalCostSavingsCultureFormated")).Text = CultureHelper.FormatCurrencyAsString(buildingLCID, equipmentClassResults.Sum(b => b.TotalCostSavings).ToString());
                }
            }

            //set pie charts
            SetBuildingAndEquipmentClassSummaryPieCharts(buildingResults, equipmentClassResults);

            //set column charts
            SetBuildingAndEquipmentClassSummaryColumnCharts(buildingResults, equipmentClassResults);
        }

        private void SetBuildingAndEquipmentClassSummaryPieCharts(IEnumerable<DiagnosticsBureauReportResult> buildingResults, IEnumerable<DiagnosticsBureauReportResult> equipmentClassResults)
        {
            int pointCounter = 0;

            if (includeBuildingSummaryReport)
            {
                if (isScheduled) CustomReportsChartHelper.SwitchChartImageStorage(buildingSummaryPieChart, "buildingSummaryPieChart");

                //BUILDINGS-----
                //group buildings with no cost and add as other.
                DiagnosticsBureauReportResult otherBuildingResult = buildingResults
                                                                                        .Where(b => b.TotalCostSavings == 0)
                                                                                        .GroupBy(g => g.TotalCostSavings)
                                                                                        .Select(s => new DiagnosticsBureauReportResult
                                                                                        {
                                                                                            BuildingName = CH.GetSafeContentItem(mContent, "OtherChartLegend", this.GetType().Name, isSETheme),
                                                                                            TotalCostSavings = s.Key,
                                                                                        }).SingleOrDefault();

                List<DiagnosticsBureauReportResult> pieBuildingResults = buildingResults.Where(b => b.TotalCostSavings > 0).ToList();

                pieBuildingResults = pieBuildingResults.OrderByDescending(b => b.TotalCostSavings).ToList();

                if (otherBuildingResult != null)
                    pieBuildingResults.Add(otherBuildingResult);

                buildingSummaryPieChart.Series.Clear();

                var seriesBuilding = new Series
                {
                    ChartType = SeriesChartType.Pie,
                    Name = "Pie Series",
                    XValueMember = "BuildingName",
                    YValueMembers = "TotalCostSavings",
                };

                buildingSummaryPieChart.Series.Add(seriesBuilding);
                buildingSummaryPieChart.DataSource = pieBuildingResults;
                buildingSummaryPieChart.DataBind();

                string building = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
                string cost = CH.GetSafeContentItem(mContent, "Cost", this.GetType().Name, isSETheme);

                foreach (DataPoint p in buildingSummaryPieChart.Series.First().Points)
                {
                    p.LegendText = StringHelper.TrimText(p.AxisLabel, 15);
                    p.ToolTip = p.LegendToolTip = building + " = " + p.AxisLabel + ", " + cost + " = " + p.YValues.First().ToString();
                    p.AxisLabel = "";

                    if (pointCounter < ColorHelper.SEChartingColorsSystem.Count())
                        p.Color = ColorHelper.SEChartingColorsSystem.ElementAtOrDefault(pointCounter);
                    else if (pointCounter - ColorHelper.SEChartingColorsSystem.Count() < ColorHelper.ChartingSystemColors.Count())
                        p.Color = ColorHelper.ChartingSystemColors.ElementAtOrDefault(pointCounter);

                    pointCounter++;
                }


                //add title
                buildingSummaryPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "BuildingSummaryPieChartTitle", this.GetType().Name, isSETheme);

                //add cost annotation
                CustomReportsChartHelper.AddCostAnnotation(buildingSummaryPieChart, CH.GetSafeContentItem(mContent, "TotalCost", this.GetType().Name, isSETheme), CultureHelper.FormatCurrencyAsString(buildingLCID, pieBuildingResults.Sum(t => t.TotalCostSavings)));                
            }

            if (includeEquipmentClassSummaryReport)
            {
                if (isScheduled) CustomReportsChartHelper.SwitchChartImageStorage(equipmentClassSummaryPieChart, "equipmentClassSummaryPieChart");

                //EQUIPMENT CLASSES-----
                //group equipment classes with no cost and add as other.
                DiagnosticsBureauReportResult otherEquipmentClassResult = equipmentClassResults
                                                                            .Where(b => b.TotalCostSavings == 0)
                                                                            .GroupBy(g => g.TotalCostSavings)
                                                                            .Select(s => new DiagnosticsBureauReportResult
                                                                            {
                                                                                EquipmentClassName = CH.GetSafeContentItem(mContent, "OtherChartLegend", this.GetType().Name, isSETheme),
                                                                                TotalCostSavings = s.Key,
                                                                            }).SingleOrDefault();

                List<DiagnosticsBureauReportResult> pieEquipmentClassResults = equipmentClassResults.Where(b => b.TotalCostSavings > 0).ToList();

                pieEquipmentClassResults = pieEquipmentClassResults.OrderByDescending(b => b.TotalCostSavings).ToList();

                if (otherEquipmentClassResult != null)
                    pieEquipmentClassResults.Add(otherEquipmentClassResult);

                equipmentClassSummaryPieChart.Series.Clear();

                var seriesEquipment = new Series
                {
                    ChartType = SeriesChartType.Pie,
                    Name = "Pie Series",
                    XValueMember = "EquipmentClassName",
                    YValueMembers = "TotalCostSavings",
                };

                equipmentClassSummaryPieChart.Series.Add(seriesEquipment);
                equipmentClassSummaryPieChart.DataSource = pieEquipmentClassResults;
                equipmentClassSummaryPieChart.DataBind();

                pointCounter = 0;
                string equipmentClass = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
                string cost = CH.GetSafeContentItem(mContent, "Cost", this.GetType().Name, isSETheme);

                foreach (DataPoint p in equipmentClassSummaryPieChart.Series.First().Points)
                {
                    p.LegendText = StringHelper.TrimText(p.AxisLabel, 15);
                    p.ToolTip = p.LegendToolTip = equipmentClass + " = " + p.AxisLabel + ", " + cost + " = " + p.YValues.First().ToString();
                    p.AxisLabel = "";

                    if (pointCounter < ColorHelper.SEChartingColorsSystem.Count())
                        p.Color = ColorHelper.SEChartingColorsSystem.ElementAtOrDefault(pointCounter);
                    else if (pointCounter - ColorHelper.SEChartingColorsSystem.Count() < ColorHelper.ChartingSystemColors.Count())
                        p.Color = ColorHelper.ChartingSystemColors.ElementAtOrDefault(pointCounter);

                    pointCounter++;
                }

                //add title
                equipmentClassSummaryPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "EquipmentClassSummaryPieChartTitle", this.GetType().Name, isSETheme);

                //add cost annotation
                CustomReportsChartHelper.AddCostAnnotation(equipmentClassSummaryPieChart, CH.GetSafeContentItem(mContent, "TotalCost", this.GetType().Name, isSETheme), CultureHelper.FormatCurrencyAsString(buildingLCID, pieEquipmentClassResults.Sum(c => c.TotalCostSavings)));
            }
        }

        private void SetBuildingAndEquipmentClassSummaryColumnCharts(IEnumerable<DiagnosticsBureauReportResult> buildingResults, IEnumerable<DiagnosticsBureauReportResult> equipmentClassResults)
        {
            if (includeBuildingSummaryReport)
            {
                if (isScheduled) CustomReportsChartHelper.SwitchChartImageStorage(buildingSummaryStackedColumnChart, "buildingSummaryStackedColumnChart");

                //group buildings with no equipmentfaults and add as other.
                DiagnosticsBureauReportResult otherBuildingResult = buildingResults
                                                                        .Where(b => b.PercentEquipmentWithoutFaults == 100)
                                                                        .GroupBy(g => g.PercentEquipmentWithoutFaults)
                                                                        .Select(s => new DiagnosticsBureauReportResult
                                                                        {
                                                                            BuildingName = CH.GetSafeContentItem(mContent, "OtherChartLegend", this.GetType().Name, isSETheme),
                                                                            PercentEquipmentWithFaults = s.First().PercentEquipmentWithFaults,
                                                                            PercentEquipmentWithoutFaults = s.Key,
                                                                        }).SingleOrDefault();

                List<DiagnosticsBureauReportResult> columnBuildingResults = buildingResults.Where(b => b.PercentEquipmentWithoutFaults < 100).ToList();

                columnBuildingResults = columnBuildingResults.OrderByDescending(b => b.PercentEquipmentWithFaults).ToList();

                if (otherBuildingResult != null)
                    columnBuildingResults.Add(otherBuildingResult);

                buildingSummaryStackedColumnChart.Series.Clear();

                var seriesEquipmentWithFaults = new Series
                {
                    ChartType = SeriesChartType.StackedColumn,
                    Name = CH.GetSafeContentItem(mContent, "PercentEquipmentWithFaults", this.GetType().Name, isSETheme),
                    XValueMember = "BuildingName",
                    YValueMembers = "PercentEquipmentWithFaults",
                    Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange),
                };

                var seriesEquipmentWithoutFaults = new Series
                {
                    ChartType = SeriesChartType.StackedColumn,
                    Name = CH.GetSafeContentItem(mContent, "PercentEquipmentWithoutFaults", this.GetType().Name, isSETheme),
                    XValueMember = "BuildingName",
                    YValueMembers = "PercentEquipmentWithoutFaults",
                    Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue),
                };

                buildingSummaryStackedColumnChart.Series.Add(seriesEquipmentWithFaults);
                buildingSummaryStackedColumnChart.Series.Add(seriesEquipmentWithoutFaults);
                buildingSummaryStackedColumnChart.DataSource = columnBuildingResults;
                buildingSummaryStackedColumnChart.DataBind();

                string building = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
                string ewof = CH.GetSafeContentItem(mContent, "EquipmentWithoutFaults", this.GetType().Name, isSETheme);
                string ewf = CH.GetSafeContentItem(mContent, "EquipmentWithFaults", this.GetType().Name, isSETheme);
                
                foreach (Series s in buildingSummaryStackedColumnChart.Series)
                {
                    foreach (DataPoint p in s.Points)
                    {
                        p.AxisLabel = p.LegendText = StringHelper.TrimText(p.AxisLabel, 15);

                        if (s.YValueMembers == "PercentEquipmentWithoutFaults")
                        {
                            p.ToolTip = p.LegendToolTip = building + " = " + p.AxisLabel + ", " + ewof + " = " + p.YValues.First().ToString() + "%";
                        }
                        else
                        {
                            p.ToolTip = p.LegendToolTip = building + " = " + p.AxisLabel + ", " + ewf + " = " + p.YValues.First().ToString() + "%";
                        }
                    }
                }

                //add title
                buildingSummaryStackedColumnChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "BuildingSummaryStackedColumnChartTitle", this.GetType().Name, isSETheme);
            }

            if (includeEquipmentClassSummaryReport)
            {
                if (isScheduled) CustomReportsChartHelper.SwitchChartImageStorage(equipmentClassSummaryStackedColumnChart, "equipmentClassSummaryStackedColumnChart");

                //group equipmentclasses with no equipmentfaults and add as other.
                DiagnosticsBureauReportResult otherEquipmentClassResult = equipmentClassResults
                                                                            .Where(b => b.PercentEquipmentWithoutFaults == 100)
                                                                            .GroupBy(g => g.PercentEquipmentWithoutFaults)
                                                                            .Select(s => new DiagnosticsBureauReportResult
                                                                            {
                                                                                EquipmentClassName = CH.GetSafeContentItem(mContent, "OtherChartLegend", this.GetType().Name, isSETheme),
                                                                                PercentEquipmentWithoutFaults = s.Key,
                                                                                PercentEquipmentWithFaults = s.First().PercentEquipmentWithFaults,
                                                                            }).SingleOrDefault();

                List<DiagnosticsBureauReportResult> columnEquipmentClassResults = equipmentClassResults.Where(b => b.PercentEquipmentWithoutFaults < 100).ToList();

                columnEquipmentClassResults = columnEquipmentClassResults.OrderByDescending(b => b.PercentEquipmentWithFaults).ToList();

                if (otherEquipmentClassResult != null)
                    columnEquipmentClassResults.Add(otherEquipmentClassResult);

                equipmentClassSummaryStackedColumnChart.Series.Clear();

                var seriesEquipmentWithFaults = new Series
                {
                    ChartType = SeriesChartType.StackedColumn,
                    Name = CH.GetSafeContentItem(mContent, "PercentEquipmentWithFaults", this.GetType().Name, isSETheme),
                    XValueMember = "EquipmentClassName",
                    YValueMembers = "PercentEquipmentWithFaults",
                    Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange),
                };

                var seriesEquipmentWithoutFaults = new Series
                {
                    ChartType = SeriesChartType.StackedColumn,
                    Name = CH.GetSafeContentItem(mContent, "PercentEquipmentWithoutFaults", this.GetType().Name, isSETheme),
                    XValueMember = "EquipmentClassName",
                    YValueMembers = "PercentEquipmentWithoutFaults",
                    Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue),
                };

                equipmentClassSummaryStackedColumnChart.Series.Add(seriesEquipmentWithFaults);
                equipmentClassSummaryStackedColumnChart.Series.Add(seriesEquipmentWithoutFaults);
                equipmentClassSummaryStackedColumnChart.DataSource = columnEquipmentClassResults;
                equipmentClassSummaryStackedColumnChart.DataBind();

                string ec = CH.GetSafeContentItem(mContent, "EquipmentClass", this.GetType().Name, isSETheme);
                string ewof = CH.GetSafeContentItem(mContent, "EquipmentWithoutFaults", this.GetType().Name, isSETheme);
                string ewf = CH.GetSafeContentItem(mContent, "EquipmentWithFaults", this.GetType().Name, isSETheme);

                foreach (Series s in equipmentClassSummaryStackedColumnChart.Series)
                {
                    foreach (DataPoint p in s.Points)
                    {
                        p.AxisLabel = p.LegendText = StringHelper.TrimText(p.AxisLabel, 15);

                        if (s.YValueMembers == "PercentEquipmentWithoutFaults")
                        {
                            p.ToolTip = p.LegendToolTip = ec + " = " + p.AxisLabel + ", " + ewf + " = " + p.YValues.First().ToString() + "%";
                        }
                        else
                        {
                            p.ToolTip = p.LegendToolTip = ec + " = " + p.AxisLabel + ", " + ewof + " = " + p.YValues.First().ToString() + "%";
                        }
                    }
                }

                //add title
                equipmentClassSummaryStackedColumnChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "EquipmentClassSummaryStackedColumnChartTitle", this.GetType().Name, isSETheme);
            }
        }


        /// <summary>
        /// sets the top daily portfolio diagnostic grids excluding previous grid duplicates.
        /// sets the top individual weekely portfolio diagnostic results based on the top daily, if the weeklies ran.       
        /// </summary>
        /// <param name="diagnosticsResults"></param>
        private void SetTopDiagnosticGridsAndResults(IEnumerable<DiagnosticsResult> diagnosticsResults)
        {
            divContentsBuildingTopIssuesPage.Visible = divBuildingTopIssuesPage.Visible = includeBuildingTopIssuesSummaryReport;
            divContentsTopDiagnostics.Visible = divTopDiagnostics.Visible = includeTopDiagnosticsDetails;
            hdnIncludeTopDiagnosticsFigures.Value = includeTopDiagnosticsFigures.ToString();

            if (includeBuildingTopIssuesSummaryReport || includeTopDiagnosticsDetails)
            {
                IEnumerable<DiagnosticsBureauReportResult> topEnergyResults = diagnosticsResults.Where(d => d.EnergyPriority != 0)
                                            .GroupBy(g => new { g.AID, g.EID })
                                            .Select(s => new DiagnosticsBureauReportResult
                                            {
                                                AID = s.Key.AID,
                                                EID = s.Key.EID,
                                                BID = s.First().BID,
                                                BuildingName = s.First().BuildingName,
                                                EquipmentName = s.First().EquipmentName,
                                                NotesSummary = s.OrderByDescending(c => c.CostSavings).First().NotesSummary,
                                                TotalCostSavings = s.Sum(c => c.CostSavings),
                                                TotalCostSavingsCultureFormated = CultureHelper.FormatCurrencyAsString(s.First().LCID, s.Sum(c => c.CostSavings)),
                                                //filter out 0's before averaging. cannot be empty.
                                                AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? Math.Round(s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority), 1, MidpointRounding.AwayFromZero) : 0,
                                                AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? Math.Round(s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority), 1, MidpointRounding.AwayFromZero) : 0,
                                                //already filtered.
                                                AverageEnergyPriority = Math.Round(s.Average(e => e.EnergyPriority), 1, MidpointRounding.AwayFromZero),
                                                Occurrences = s.Count(),
                                            })
                                            .OrderByDescending(c => c.TotalCostSavings).Take(5);


                //exclude already used top energy results                
                IEnumerable<DiagnosticsBureauReportResult> topMaintenanceResults = diagnosticsResults.Where(d => d.MaintenancePriority != 0)
                                                        .GroupBy(g => new { g.AID, g.EID })
                                                        .Select(s => new DiagnosticsBureauReportResult
                                                        {
                                                            AID = s.Key.AID,
                                                            EID = s.Key.EID,
                                                            BID = s.First().BID,
                                                            BuildingName = s.First().BuildingName,
                                                            EquipmentName = s.First().EquipmentName,
                                                            NotesSummary = s.OrderByDescending(m => m.MaintenancePriority).First().NotesSummary,
                                                            //filter out 0's before averaging. cannot be empty.
                                                            AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? Math.Round(s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority), 1, MidpointRounding.AwayFromZero) : 0,
                                                            AverageEnergyPriority = s.Where(e => e.EnergyPriority != 0).Any() ? Math.Round(s.Where(e => e.EnergyPriority != 0).Average(e => e.EnergyPriority), 1, MidpointRounding.AwayFromZero) : 0,
                                                            //already filtered                                                                                                                       
                                                            AverageMaintenancePriority = Math.Round(s.Average(m => m.MaintenancePriority), 1, MidpointRounding.AwayFromZero),
                                                            Occurrences = s.Count(),
                                                            TotalPriority = s.Sum(m => m.MaintenancePriority),
                                                            //MaintenancePriority = s.OrderByDescending(m => m.MaintenancePriority).First().MaintenancePriority,
                                                        })
                                                        .OrderByDescending(p => p.TotalPriority)
                                                        .Except(topEnergyResults)
                                                        .Take(5);

                //exclued already used top maintenance results
                IEnumerable<DiagnosticsBureauReportResult> topComfortResults = diagnosticsResults.Where(d => d.ComfortPriority != 0)
                                                        .GroupBy(g => new { g.AID, g.EID })
                                                        .Select(s => new DiagnosticsBureauReportResult
                                                        {
                                                            AID = s.Key.AID,
                                                            EID = s.Key.EID,
                                                            BID = s.First().BID,
                                                            BuildingName = s.First().BuildingName,
                                                            EquipmentName = s.First().EquipmentName,
                                                            NotesSummary = s.OrderByDescending(c => c.ComfortPriority).First().NotesSummary,
                                                            //filter out 0's before averaging. cannot be empty.
                                                            AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? Math.Round(s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority), 1, MidpointRounding.AwayFromZero) : 0,
                                                            AverageEnergyPriority = s.Where(e => e.EnergyPriority != 0).Any() ? Math.Round(s.Where(e => e.EnergyPriority != 0).Average(e => e.EnergyPriority), 1, MidpointRounding.AwayFromZero) : 0,
                                                            //already filtered                                                                                                                       
                                                            AverageComfortPriority = Math.Round(s.Average(c => c.ComfortPriority), 1, MidpointRounding.AwayFromZero),
                                                            Occurrences = s.Count(),
                                                            TotalPriority = s.Sum(c => c.ComfortPriority),
                                                            //ComfortPriority = s.OrderByDescending(c => c.ComfortPriority).First().ComfortPriority,                                                            
                                                        })
                                                        .OrderByDescending(p => p.TotalPriority)
                                                        .Except(topMaintenanceResults)
                                                        .Take(5);

                if (includeBuildingTopIssuesSummaryReport)
                {
                    gridEnergy.DataSource = topEnergyResults;
                    gridEnergy.DataBind();

                    gridMaintenance.DataSource = topMaintenanceResults;
                    gridMaintenance.DataBind();

                    gridComfort.DataSource = topComfortResults;
                    gridComfort.DataBind();
                }

                if (includeTopDiagnosticsDetails)
                    SetTopDiagnosticResults(topEnergyResults, topMaintenanceResults, topComfortResults);
            }
        }

        private void SetTopDiagnosticResults(IEnumerable<DiagnosticsBureauReportResult> topEnergyResults, IEnumerable<DiagnosticsBureauReportResult> topMaintenanceResults, IEnumerable<DiagnosticsBureauReportResult> topComfortResults)
        {
            if (includeTopDiagnosticsDetails)
            {
                List<DiagnosticsBureauReportResult> topDiagnosticResultsList = new List<DiagnosticsBureauReportResult>();

                foreach (DiagnosticsBureauReportResult result in topEnergyResults.Union(topMaintenanceResults).Union(topComfortResults))
                {
                    DateTime startOfWeek = DateTimeHelper.GetLastSunday(ed);

                    //get analysis again for weekly
                    DiagnosticsBureauReportResult diagnosticsResult = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(
                        new DiagnosticsResultsInputs()
                        {
                            AID = result.AID,
                            BID = result.BID,
                            CID = cid,
                            EID = result.EID,
                            AnalysisRange = DataConstants.AnalysisRange.Weekly,
                            StartDate = startOfWeek,
                            EndDate = startOfWeek
                        }).Select(d => new DiagnosticsBureauReportResult { AID = d.AID, AnalysisName = d.AnalysisName, AnalysisRange = d.AnalysisRange, AverageComfortPriority = d.ComfortPriority, AverageEnergyPriority = d.EnergyPriority, AverageMaintenancePriority = d.MaintenancePriority, BID = d.BID, BuildingName = d.BuildingName, CID = d.CID, ClientName = d.ClientName, EID = d.EID, EquipmentName = d.EquipmentName, FigureBlobExt = d.FigureBlobExt, LCID = d.LCID, Notes = d.Notes, StartDate = d.StartDate, TotalCostSavings = d.CostSavings }).FirstOrDefault();


                    if (diagnosticsResult != null)
                    {
                        diagnosticsResult.TotalCostSavingsCultureFormated = CultureHelper.FormatCurrencyAsString(diagnosticsResult.LCID, diagnosticsResult.TotalCostSavings);                        
                        
                        if(includeTopDiagnosticsFigures && diagnosticsResult.FigureBlobExt != null)
                            diagnosticsResult.FigureByteArray =  new CW.Business.Blob.Images.FigureImage(diagnosticsResult.AnalysisRange, diagnosticsResult, mDataManager.OrgBasedBlobStorageProvider).Retrieve();
						//diagnosticsResult.FigureByteArray = mDataManager.FigureAndNoteDataMapper.GetFigureBlobImage(diagnosticsResult.AnalysisRange, diagnosticsResult.EID, diagnosticsResult.AID, diagnosticsResult.StartDate, Path.GetExtension(diagnosticsResult.FigureBlobName));

                        //get points? maybe not needed.

                        //get variables? maybe not needed.

                        topDiagnosticResultsList.Add(diagnosticsResult);
                    }
                }

                rptTopDiagnostics.DataSource = topDiagnosticResultsList;
                rptTopDiagnostics.DataBind();
            }
        }

        /// <summary>
        /// sets the priorities trend content.
        /// </summary>
        private void SetPrioritiesTrendContent()
        {
            if (includeBuildingTrendsSummaryReport)
            {
                litEnergy.Text = TokenReplacer(Server.UrlDecode(energy));
                litMaintenance.Text = TokenReplacer(Server.UrlDecode(maintenance));
                litComfort.Text = TokenReplacer(Server.UrlDecode(comfort));
            }
        }

        /// <summary>
        /// sets the priorities trend charts.
        /// </summary>
        /// <param name="diagnosticsResults"></param>
        private void SetPrioritiesTrendCharts(IEnumerable<DiagnosticsResult> diagnosticsResults)
        {
            divContentsBuildingTrendsPage.Visible = divBuildingTrendsPage.Visible = includeBuildingTrendsSummaryReport;

            if (includeBuildingTrendsSummaryReport)
            {
                if (isScheduled)
                {
                    CustomReportsChartHelper.SwitchChartImageStorage(energyChart, "energyChart");
                    CustomReportsChartHelper.SwitchChartImageStorage(maintenanceChart, "maintenanceChart");
                    CustomReportsChartHelper.SwitchChartImageStorage(comfortChart, "comfortChart");
                }

                //energy chart
                IEnumerable<DiagnosticsBureauReportResult> energyDiagnostics = diagnosticsResults.GroupBy(d => d.StartDate)
                                                                                     .Select(s => new DiagnosticsBureauReportResult
                                                                                     {
                                                                                         StartDate = s.Key,
                                                                                         Occurrences = s.Where(e => e.EnergyPriority != 0).Count(),
                                                                                         TotalCostSavings = s.Sum(c => c.CostSavings)
                                                                                     }).OrderBy(s => s.StartDate);

                if (energyDiagnostics.Any())
                {
                    energyChart.Series.Clear();

                    //set axis intervaltype
                    energyChart.ChartAreas["energyChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                    //set axis format:  g = short date and time, d = short date
                    energyChart.ChartAreas["energyChartArea"].AxisX.LabelStyle.Format = "d";

                    //area chart
                    var series = new Series
                    {
                        ChartType = SeriesChartType.Area,
                        Name = "Area Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        LegendText = CH.GetSafeContentItem(mContent, "TotalEnergyIncidents", this.GetType().Name, isSETheme),
                        Color = Color.FromArgb(120, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.EnergyYellow)),
                    };

                    series.YAxisType = AxisType.Secondary;
                    series.Points.DataBindXY(energyDiagnostics.ToArray(), "StartDate", energyDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    energyChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    energyChart.Series.Add(series);

                    //line
                    series = new Series
                    {
                        ChartType = SeriesChartType.FastLine,
                        BorderWidth = 3,
                        Name = "Line Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        LegendText = CH.GetSafeContentItem(mContent, "AvoidableCost", this.GetType().Name, isSETheme),
                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange)
                    };

                    //add data to series
                    series.Points.DataBindXY(energyDiagnostics.ToArray(), "StartDate", energyDiagnostics.ToArray(), "TotalCostSavings");

                    //interpolate
                    energyChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    energyChart.Series.Add(series);

                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        ShadowColor = Color.DimGray,
                        ShadowOffset = 4,
                        MarkerSize = 6,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange),
                        MarkerColor = Color.White,
                        //BorderWidth = 6,
                        Name = "Point Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        Legend = "Hidden",
                        //BorderColor = Color.Gold,
                        //Color = Color.White,                            
                    };
                    series.Points.DataBindXY(energyDiagnostics.ToArray(), "StartDate", energyDiagnostics.ToArray(), "TotalCostSavings");

                    //interpolate
                    energyChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    energyChart.Series.Add(series);
                }


                //maintenance chart
                IEnumerable<DiagnosticsBureauReportResult> maintenanceDiagnostics = diagnosticsResults
                                                                                     .GroupBy(d => d.StartDate)
                                                                                     .Select(s => new DiagnosticsBureauReportResult
                                                                                     {
                                                                                         StartDate = s.Key,
                                                                                         Occurrences = s.Where(m => m.MaintenancePriority != 0).Count(),
                                                                                         AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                                                                                     }).OrderBy(s => s.StartDate);

                if (maintenanceDiagnostics.Any())
                {
                    maintenanceChart.Series.Clear();

                    //set axis intervaltype
                    maintenanceChart.ChartAreas["maintenanceChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                    //set axis format:  g = short date and time, d = short date
                    maintenanceChart.ChartAreas["maintenanceChartArea"].AxisX.LabelStyle.Format = "d";

                    //area chart
                    var series = new Series
                    {
                        ChartType = SeriesChartType.Area,
                        Name = "Area Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        LegendText = CH.GetSafeContentItem(mContent, "AverageMaintenancePriority", this.GetType().Name, isSETheme),
                        Color = Color.FromArgb(120, Color.DarkGray),
                    };

                    series.YAxisType = AxisType.Secondary;
                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "AverageMaintenancePriority");

                    //interpolate
                    maintenanceChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    maintenanceChart.Series.Add(series);


                    //line
                    series = new Series
                    {
                        ChartType = SeriesChartType.FastLine,
                        BorderWidth = 3,
                        Name = "Line Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        LegendText = CH.GetSafeContentItem(mContent, "TotalMaintenanceIncidents", this.GetType().Name, isSETheme),
                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray)
                    };

                    //add data to series
                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    maintenanceChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    maintenanceChart.Series.Add(series);


                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        ShadowColor = Color.DimGray,
                        ShadowOffset = 4,
                        MarkerSize = 6,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray),
                        MarkerColor = Color.White,
                        //BorderWidth = 12,
                        Name = "Point Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        Legend = "Hidden",
                        //BorderColor = Color.DimGray,
                        //Color = Color.White
                    };
                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    maintenanceChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    maintenanceChart.Series.Add(series);
                }

                //comfort chart
                IEnumerable<DiagnosticsBureauReportResult> comfortDiagnostics = diagnosticsResults
                                                                                     .GroupBy(d => d.StartDate)
                                                                                     .Select(s => new DiagnosticsBureauReportResult
                                                                                     {
                                                                                         StartDate = s.Key,
                                                                                         Occurrences = s.Where(c => c.ComfortPriority != 0).Count(),
                                                                                         AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                                                                                     }).OrderBy(s => s.StartDate);

                if (comfortDiagnostics.Any())
                {
                    comfortChart.Series.Clear();

                    //set axis intervaltype
                    comfortChart.ChartAreas["comfortChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                    //set axis format:  g = short date and time, d = short date
                    comfortChart.ChartAreas["comfortChartArea"].AxisX.LabelStyle.Format = "d";

                    //area chart
                    var series = new Series
                    {
                        ChartType = SeriesChartType.Area,
                        Name = "Area Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        LegendText = CH.GetSafeContentItem(mContent, "AverageComfortPriority", this.GetType().Name, isSETheme),
                        Color = Color.FromArgb(120, Color.LightSteelBlue),
                    };

                    series.YAxisType = AxisType.Secondary;
                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "AverageComfortPriority");

                    //interpolate
                    comfortChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    comfortChart.Series.Add(series);


                    //line
                    series = new Series
                    {
                        ChartType = SeriesChartType.FastLine,
                        BorderWidth = 3,
                        Name = "Line Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        LegendText = CH.GetSafeContentItem(mContent, "TotalComfortIncidents", this.GetType().Name, isSETheme),
                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue)
                    };

                    //add data to series
                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    comfortChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    comfortChart.Series.Add(series);


                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        ShadowColor = Color.DimGray,
                        ShadowOffset = 4,
                        MarkerSize = 6,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue),
                        MarkerColor = Color.White,
                        //BorderWidth = 12,
                        Name = "Point Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        Legend = "Hidden"
                        //BorderColor = Color.CornflowerBlue,
                        //Color = Color.White
                    };

                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    comfortChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    comfortChart.Series.Add(series);
                }
            }
        }


        /// <summary>
        /// sets the priorities period charts.
        /// </summary>
        private void SetPrioritiesPeriodCharts()
        {
            if (includeBuildingTrendsSummaryReport)
            {
                if (isScheduled)
                {
                    CustomReportsChartHelper.SwitchChartImageStorage(energyPeriodChart, "energyPeriodChart");
                    CustomReportsChartHelper.SwitchChartImageStorage(maintenancePeriodChart, "maintenancePeriodChart");
                    CustomReportsChartHelper.SwitchChartImageStorage(comfortPeriodChart, "comfortPeriodChart");
                }

                //energy period
                energyPeriodChart.Series.Clear();

                var series = new Series
                {
                    ChartType = SeriesChartType.Column,
                    Name = "Column Series",
                };

                series["PixelPointWidth"] = "80";

                series.Points.AddY(intervalPreviousCost);
                series.Points[0].Color = Color.FromArgb(185, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.EnergyYellow));
                series.Points[0].AxisLabel = CH.GetSafeContentItem(mContent, "Previous", this.GetType().Name, isSETheme);
                series.Points.AddY(intervalCost);
                series.Points[1].Color = Color.FromArgb(185, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange));
                series.Points[1].AxisLabel = CH.GetSafeContentItem(mContent, "Current", this.GetType().Name, isSETheme);

                energyPeriodChart.Series.Add(series);

                //maintenance period
                maintenancePeriodChart.Series.Clear();

                series = new Series
                {
                    ChartType = SeriesChartType.Column,
                    Name = "Column Series",
                };

                series["PixelPointWidth"] = "80";

                series.Points.AddY(intervalPreviousMaintenanceIncidents);
                series.Points[0].Color = Color.FromArgb(185, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MediumGray));
                series.Points[0].AxisLabel = CH.GetSafeContentItem(mContent, "Previous", this.GetType().Name, isSETheme);
                series.Points[0].MarkerBorderWidth = 10;
                series.Points[0].MarkerBorderColor = Color.White;
                series.Points.AddY(intervalMaintenanceIncidents);
                series.Points[1].Color = Color.FromArgb(185, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray));
                series.Points[1].AxisLabel = CH.GetSafeContentItem(mContent, "Current", this.GetType().Name, isSETheme);
                series.Points[1].MarkerBorderWidth = 10;
                series.Points[1].MarkerBorderColor = Color.White;

                maintenancePeriodChart.Series.Add(series);

                //comfort period
                comfortPeriodChart.Series.Clear();

                series = new Series
                {
                    ChartType = SeriesChartType.Column,
                    Name = "Column Series",
                };

                series["PixelPointWidth"] = "80";

                series.Points.AddY(intervalPreviousComfortIncidents);
                series.Points[0].Color = Color.FromArgb(185, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.SkyBlue));
                series.Points[0].AxisLabel = CH.GetSafeContentItem(mContent, "Previous", this.GetType().Name, isSETheme);
                series.Points[0].MarkerSize = 40;
                series.Points.AddY(intervalComfortIncidents);
                series.Points[1].Color = Color.FromArgb(185, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue));
                series.Points[1].AxisLabel = CH.GetSafeContentItem(mContent, "Current", this.GetType().Name, isSETheme);
                series.Points[1].MarkerSize = 40;

                comfortPeriodChart.Series.Add(series);
            }
        }


        /// <summary>
        /// sets the top projects grid.
        /// </summary>
        private void SetProjectsGrid()
        {
            divContentsProjects.Visible = divProjects.Visible = includeProjectReport;

            if (includeProjectReport)
            {
                IEnumerable<GetGroupedProjectAssociationsData> projects = Enumerable.Empty<GetGroupedProjectAssociationsData>();

                projects = mDataManager.ProjectDataMapper.GetTopGroupedProjectAssociations(new ProjectsInputs() { CID = cid, BIDArray = bids.ToArray() }, (int)topProjects);

                gridTopOpenProjects.DataSource = projects;
                gridTopOpenProjects.DataBind();
            }
        }


        /// <summary>
        /// sets the top open and closed tasks grids.
        /// </summary>
        private void SetTasksGrids()
        {
            divContentsTasks.Visible = divTasks.Visible = includeTaskReport;

            List<Int32> openStatusIds = new List<Int32>() { Convert.ToInt32(BusinessConstants.Status.Statuses.Open), Convert.ToInt32(BusinessConstants.Status.Statuses.InProcess) };

            if (includeTaskReport)
            {             
                var openTasks = new TaskRecordQuery(ConnectionString, siteUser).LoadWith(_ => _.User1).LoadWith<Analyses_Equipment, Analyses_Equipment, Equipment>(_ => _.Analyses_Equipment, _ => _.Equipment, _ => _.Analyse, _ => _.Building).FinalizeLoadWith.GetAll(cid)
                    .ByBuildings(bids.ToArray()).ByStatus(openStatusIds).Top((int)topTasks, (d => d.DateCreated)).ToEnumerable();
                var completedTasks = new TaskRecordQuery(ConnectionString, siteUser).LoadWith(_ => _.User1).LoadWith<Analyses_Equipment, Analyses_Equipment, Equipment>(_ => _.Analyses_Equipment, _ => _.Equipment, _ => _.Analyse, _ => _.Building).FinalizeLoadWith.GetAll(cid)
                    .ByBuildings(bids.ToArray()).ByStatus(Convert.ToInt32(BusinessConstants.Status.Statuses.Completed)).ByCompletedDateRange(sd, ed).Top((int)topTasks, (d => (DateTime)d.DateCompleted)).ToEnumerable();

                //OPEN TASKS ListView                
                lvTopOpenTasks.DataSource = openTasks;
                lvTopOpenTasks.DataBind();

                //COMPLETED TASKS ListView                
                lvTopCompletedTasks.DataSource = completedTasks;
                lvTopCompletedTasks.DataBind();
            }
        }


        /// <summary>
        /// sets the alarm grids
        /// </summary>
        private void SetAlarmsGrids()
        {
            divContentsAlarms.Visible = divAlarms.Visible = includeAlarmReport;

            if (includeAlarmReport)
            {
                TimeZoneInfo tzi = mDataManager.BuildingDataMapper.GetLastestBuildingTimeZoneByCID(cid);

                //sd, ed assumed local time
                DateTimeHelper dth = IoC.Resolve<DateTimeHelper>();
                DateTime sdReset = sd;
                DateTime edReset = ed;
                DateTime sdMax = dth.ConvertTimeFromUTC(DateTime.UtcNow.AddDays(-BusinessConstants.Alarm.AlarmsDailyCacheInDays), tzi.Id).Date;

                //if out of cache range, get recent 30 days
                if (ed < sdMax)
                {
                    edReset = dth.ConvertTimeFromUTC(DateTime.UtcNow, tzi.Id).Date;
                    sdReset = edReset.AddDays(-30);
                }
                //else if sd out of range, set it to max cache range
                else if (sd < sdMax)
                {
                    sdReset = sdMax;
                }

                //set period literal
                litHeaderPeriodAlarm.Text = CreatePeriodContent(sdReset, edReset);


                var inputs = new AlarmRawDataInputs()
                {
                    AlarmWidget = BusinessConstants.Alarm.AlarmWidgetsEnum.TopTenMostFrequentAlarms,
                    CID = cid,
                    StartDateLocal = sdReset,
                    EndDateLocal = edReset,
                    Duration = Duration.Daily,
                    BIDs = bids
                };

                var drs = new DateRangeSetter();
                drs.SetRangeFromLocalDate(Duration.Daily, inputs.StartDateLocal, inputs.EndDateLocal, null);

                //frequent grid
                var func = mDataManager.AlarmDataMapper.GetAlarmCalcFunc(inputs.AlarmWidget);

                var results = Enumerable.Empty<GetBuildingRawDataWithCalc>();
                foreach (var dr in drs.Ranges)
                {
                    inputs.StartDateLocal = dr.StartDateLocal;
                    inputs.EndDateLocal = dr.EndDateLocal;

                    var grm = new AlarmResultsManager(mDataManager, func, inputs);
                    grm.HasGetFromStorage = false;
                    results = results.Concat(grm.Get().ToList());
                }

                results = results.Where(d => bids.Contains(d.BID));

                gridTopTenMostFrequentAlarms.DataSource = (from ad in results
                                                           group ad by ad.PID into gd
                                                           select new GetBuildingRawDataWithCalc
                                                           {
                                                               PID = gd.Key,
                                                               PointName = gd.FirstOrDefault().PointName,
                                                               EID = gd.FirstOrDefault().EID,
                                                               EquipmentName = gd.FirstOrDefault().EquipmentName,
                                                               BID = gd.FirstOrDefault().BID,
                                                               BuildingName = gd.FirstOrDefault().BuildingName,
                                                               NumOfOccurrences = gd.Sum(x => x.NumOfOccurrences),
                                                           }).Where(bad => bad.NumOfOccurrences > 0).OrderByDescending(ad => ad.NumOfOccurrences).Take(10);

                gridTopTenMostFrequentAlarms.DataBind();


                //total grid
                inputs.AlarmWidget = BusinessConstants.Alarm.AlarmWidgetsEnum.TopTenTotalTimeInAlarm;

                func = mDataManager.AlarmDataMapper.GetAlarmCalcFunc(inputs.AlarmWidget);

                results = Enumerable.Empty<GetBuildingRawDataWithCalc>();
                foreach (var dr in drs.Ranges)
                {
                    inputs.StartDateLocal = dr.StartDateLocal;
                    inputs.EndDateLocal = dr.EndDateLocal;

                    var grm = new AlarmResultsManager(mDataManager, func, inputs);
                    grm.HasGetFromStorage = false;
                    results = results.Concat(grm.Get().ToList());
                }

                results = results.Where(d => bids.Contains(d.BID));

                var selectedAlarmData = results.GroupBy(x => new { x.PID, x.BID }).ToDictionary(x => x.Key.PID + "-" + x.Key.BID, x => x.Sum(y => y.TimeInAlarm));

                gridTopTenTotalTimeInAlarm.DataSource = (from sad in results
                                                         group sad by sad.PID into gd
                                                         select new GetBuildingRawDataWithCalc
                                                         {
                                                             PID = gd.Key,
                                                             PointName = gd.FirstOrDefault().PointName,
                                                             EID = gd.FirstOrDefault().EID,
                                                             EquipmentName = gd.FirstOrDefault().EquipmentName,
                                                             BID = gd.FirstOrDefault().BID,
                                                             BuildingName = gd.FirstOrDefault().BuildingName,
                                                             //TimeInAlarm = (currentAlarmData.ContainsKey(gd.Key + "-" + gd.FirstOrDefault().BID)) ? currentAlarmData[gd.Key + "-" + gd.FirstOrDefault().BID] : 0,
                                                             SelectedRangeTimeInAlarm = (selectedAlarmData.ContainsKey(gd.Key + "-" + gd.FirstOrDefault().BID)) ? selectedAlarmData[gd.Key + "-" + gd.FirstOrDefault().BID] : 0
                                                         }).OrderByDescending(sad => sad.SelectedRangeTimeInAlarm).Where(sad => sad.SelectedRangeTimeInAlarm > 0).Take(10);
                gridTopTenTotalTimeInAlarm.DataBind();
            }
        }


        #endregion

        #region Set Equipment Data Methods

        /// <summary>
        /// sets the equipment summery grids.
        /// </summary>
        /// <param name="specifiedDiagnosticsResults"></param>
        private void SetEquipmentSummaryGridAndPieCharts(IEnumerable<DiagnosticsResult> specifiedDiagnosticsResults, IEnumerable<DiagnosticsResult> specifiedPreviousDiagnosticsResults, bool specifiedInclusion, int? specifiedTop, HtmlGenericControl specifiedContentsDiv, HtmlGenericControl specifiedDiv, GridView specifiedGridView, Dictionary<Chart, int> specifiedPieCharts)
        {
            specifiedContentsDiv.Visible = specifiedDiv.Visible = specifiedInclusion;

            if (specifiedInclusion)
            {
                IEnumerable<DiagnosticsBureauReportResult> equipmentGridResults = CustomReportsGridResultsHelper.CalculateAndMergeEquipmentGridResults(specifiedDiagnosticsResults, reportNumberOfDays, specifiedPreviousDiagnosticsResults, previousReportNumberOfDays);

                if (specifiedTop != null)
                    equipmentGridResults = equipmentGridResults.Take((int)specifiedTop);

                specifiedGridView.DataSource = equipmentGridResults;
                specifiedGridView.DataBind();

                if (equipmentGridResults.Any())
                {
                    //Set footer totals                
                    ((Label)specifiedGridView.FooterRow.FindControl("lblGridTotalCostSavingsCultureFormated")).Text = CultureHelper.FormatCurrencyAsString(buildingLCID, equipmentGridResults.Sum(b => b.TotalCostSavings).ToString());
                }

                IEnumerable<DiagnosticsBureauReportResult> equipmentPieResults = specifiedDiagnosticsResults
                                            .GroupBy(g => g.EquipmentClassID)
                                            .Select(s => new DiagnosticsBureauReportResult
                                            {
                                                EquipmentClassID = s.Key,
                                                EquipmentClassName = s.First().EquipmentClassName,
                                                EquipmentCount = s.Select(e => e.EID).Distinct().Count(),
                                                EquipmentWithFaultsCount = s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count(),

                                                PercentEquipmentWithMaintenanceFaults = Math.Round((Convert.ToDouble(s.Where(d => d.MaintenancePriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                                                PercentEquipmentWithEnergyFaults = Math.Round((Convert.ToDouble(s.Where(d => d.EnergyPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                                                PercentEquipmentWithComfortFaults = Math.Round((Convert.ToDouble(s.Where(d => d.ComfortPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),
                                                PercentEquipmentWithFaults = Math.Round((Convert.ToDouble(s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100, 1, MidpointRounding.AwayFromZero),

                                                //equipment that have diagnostics with no priority can still have diagnostics with a priority. must go minus off equipment with faults.
                                                PercentEquipmentWithoutFaults = Math.Round(100 - ((Convert.ToDouble(s.Where(d => d.TotalPriority != 0).Select(e => e.EID).Distinct().Count()) / Convert.ToDouble(s.Select(e => e.EID).Distinct().Count())) * 100), 1, MidpointRounding.AwayFromZero),

                                                //filter out 0's before averaging. cannot be empty.
                                                //AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                                                //AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                                                //AverageEnergyPriority = s.Where(e => e.EnergyPriority != 0).Any() ? s.Where(e => e.EnergyPriority != 0).Average(e => e.EnergyPriority) : 0,
                                            }).OrderBy(e => e.EquipmentClassName);

                //set pie charts
                SetEquipmentSummaryPieCharts(equipmentPieResults, specifiedPieCharts);
            }
        }

        private void SetEquipmentSummaryPieCharts(IEnumerable<DiagnosticsBureauReportResult> equipmentPieResults, Dictionary<Chart, int> specifiedPieCharts)
        {
            Chart lastChartWithDataKey = null;
            int pointCounter = 0;

            foreach (var item in specifiedPieCharts)
            {
                pointCounter = 0;
                Chart chart = item.Key;
                int equipmentClassConstant = item.Value;

                if (isScheduled) CustomReportsChartHelper.SwitchChartImageStorage(chart, chart.ID);

                DiagnosticsBureauReportResult result = equipmentPieResults.Where(ec => ec.EquipmentClassID == equipmentClassConstant)
                                                         .SingleOrDefault();

                chart.Series.Clear();

                var series = new Series
                {
                    ChartType = SeriesChartType.Pie,
                    Name = "Pie Series",
                };

                chart.Series.Add(series);

                if (result != null)
                {
                    chart.Width = 207;
                    chart.Series[0].Points.AddY(result.PercentEquipmentWithoutFaults);
                    chart.Series[0].Points.AddY(result.PercentEquipmentWithFaults);
                    chart.DataBind();

                    foreach (DataPoint p in chart.Series.First().Points)
                    {
                        //set label and tooltip if > 0%
                        if (p.YValues.First() > 0)
                        {
                            p.Label = p.ToolTip = p.YValues.First().ToString() + "%";
                            p.LabelBackColor = Color.White;
                            p.Font = new Font("Arial", 7);
                        }

                        p.Color = (pointCounter == 0) ? ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue) : ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange);

                        pointCounter++;
                    }

                    lastChartWithDataKey = chart;
                }
                else
                {
                    chart.Visible = false;

                    //add empty values so legend creation still gets generated
                    //chart.Series[0].Points.AddY(0);
                    //chart.Series[0].Points.AddY(0);
                }
            }

            //set legend on last chart that has data.
            if (lastChartWithDataKey != null)
            {
                lastChartWithDataKey.Width = 398;
                lastChartWithDataKey.Legends.First().Enabled = true;

                pointCounter = 0;
                foreach (DataPoint p in lastChartWithDataKey.Series.First().Points)
                {
                    p.LegendText = pointCounter == 0 ? CH.GetSafeContentItem(mContent, "PercentEquipmentWithoutFaults", this.GetType().Name, isSETheme) : CH.GetSafeContentItem(mContent, "PercentEquipmentWithFaults", this.GetType().Name, isSETheme);
                    pointCounter++;
                }
            }
        }

        /// <summary>
        /// sets the equipment priorities trend charts.
        /// </summary>
        /// <param name="specifiedDiagnosticsResults"></param>
        private void SetEquipmentPrioritiesTrendCharts(IEnumerable<DiagnosticsResult> specifiedDiagnosticsResults, bool specifiedInclusion, Chart specifiedEnergyChart, Chart specifiedMaintenanceChart, Chart specifiedComfortChart)
        {
            if (specifiedInclusion)
            {
                if (isScheduled)
                {
                    CustomReportsChartHelper.SwitchChartImageStorage(specifiedEnergyChart, "specifiedEnergyChart");
                    CustomReportsChartHelper.SwitchChartImageStorage(specifiedMaintenanceChart, "specifiedMaintenanceChart");
                    CustomReportsChartHelper.SwitchChartImageStorage(specifiedComfortChart, "specifiedComfortChart");
                }

                //energy chart
                IEnumerable<DiagnosticsBureauReportResult> energyDiagnostics = specifiedDiagnosticsResults
                                                                                    .GroupBy(d => d.StartDate)
                                                                                    .Select(s => new DiagnosticsBureauReportResult
                                                                                    {
                                                                                        StartDate = s.Key,
                                                                                        Occurrences = s.Where(e => e.EnergyPriority != 0).Count(),
                                                                                        TotalCostSavings = s.Sum(c => c.CostSavings)
                                                                                    }).OrderBy(s => s.StartDate);

                if (energyDiagnostics.Any())
                {
                    specifiedEnergyChart.Series.Clear();

                    //set axis intervaltype
                    specifiedEnergyChart.ChartAreas["energyChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                    //set axis format:  g = short date and time, d = short date
                    specifiedEnergyChart.ChartAreas["energyChartArea"].AxisX.LabelStyle.Format = "d";

                    //area chart
                    var series = new Series
                    {
                        ChartType = SeriesChartType.Area,
                        Name = "Area Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        LegendText = CH.GetSafeContentItem(mContent, "TotalEnergyIncidents", this.GetType().Name, isSETheme),
                        Color = Color.FromArgb(120, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.EnergyYellow)),
                    };

                    series.YAxisType = AxisType.Secondary;
                    series.Points.DataBindXY(energyDiagnostics.ToArray(), "StartDate", energyDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    specifiedEnergyChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedEnergyChart.Series.Add(series);

                    //line
                    series = new Series
                    {
                        ChartType = SeriesChartType.FastLine,
                        BorderWidth = 3,
                        Name = "Line Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        LegendText = CH.GetSafeContentItem(mContent, "AvoidableCost", this.GetType().Name, isSETheme),
                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange)
                    };

                    //add data to series
                    series.Points.DataBindXY(energyDiagnostics.ToArray(), "StartDate", energyDiagnostics.ToArray(), "TotalCostSavings");

                    //interpolate
                    specifiedEnergyChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedEnergyChart.Series.Add(series);

                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        ShadowColor = Color.DimGray,
                        ShadowOffset = 4,
                        MarkerSize = 6,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange),
                        MarkerColor = Color.White,
                        //BorderWidth = 6,
                        Name = "Point Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        Legend = "Hidden",
                        //BorderColor = Color.Gold,
                        //Color = Color.White,                            
                    };
                    series.Points.DataBindXY(energyDiagnostics.ToArray(), "StartDate", energyDiagnostics.ToArray(), "TotalCostSavings");

                    //interpolate
                    specifiedEnergyChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedEnergyChart.Series.Add(series);
                }


                //maintenance chart
                IEnumerable<DiagnosticsBureauReportResult> maintenanceDiagnostics = specifiedDiagnosticsResults
                                                                                     .GroupBy(d => d.StartDate)
                                                                                     .Select(s => new DiagnosticsBureauReportResult
                                                                                     {
                                                                                         StartDate = s.Key,
                                                                                         Occurrences = s.Where(m => m.MaintenancePriority != 0).Count(),
                                                                                         AverageMaintenancePriority = s.Where(m => m.MaintenancePriority != 0).Any() ? s.Where(m => m.MaintenancePriority != 0).Average(m => m.MaintenancePriority) : 0,
                                                                                     }).OrderBy(s => s.StartDate);

                if (maintenanceDiagnostics.Any())
                {
                    specifiedMaintenanceChart.Series.Clear();

                    //set axis intervaltype
                    specifiedMaintenanceChart.ChartAreas["maintenanceChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                    //set axis format:  g = short date and time, d = short date
                    specifiedMaintenanceChart.ChartAreas["maintenanceChartArea"].AxisX.LabelStyle.Format = "d";

                    //area chart
                    var series = new Series
                    {
                        ChartType = SeriesChartType.Area,
                        Name = "Area Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        LegendText = CH.GetSafeContentItem(mContent, "AverageMaintenancePriority", this.GetType().Name, isSETheme),
                        Color = Color.FromArgb(120, Color.DarkGray),
                    };

                    series.YAxisType = AxisType.Secondary;
                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "AverageMaintenancePriority");

                    //interpolate
                    specifiedMaintenanceChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedMaintenanceChart.Series.Add(series);


                    //line
                    series = new Series
                    {
                        ChartType = SeriesChartType.FastLine,
                        BorderWidth = 3,
                        Name = "Line Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        LegendText = CH.GetSafeContentItem(mContent, "TotalMaintenanceIncidents", this.GetType().Name, isSETheme),
                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray)
                    };

                    //add data to series
                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    specifiedMaintenanceChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedMaintenanceChart.Series.Add(series);


                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        ShadowColor = Color.DimGray,
                        ShadowOffset = 4,
                        MarkerSize = 6,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray),
                        MarkerColor = Color.White,
                        //BorderWidth = 12,
                        Name = "Point Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        Legend = "Hidden",
                        //BorderColor = Color.DimGray,
                        //Color = Color.White
                    };
                    series.Points.DataBindXY(maintenanceDiagnostics.ToArray(), "StartDate", maintenanceDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    specifiedMaintenanceChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedMaintenanceChart.Series.Add(series);
                }

                //comfort chart
                IEnumerable<DiagnosticsBureauReportResult> comfortDiagnostics = specifiedDiagnosticsResults
                                                                                     .GroupBy(d => d.StartDate)
                                                                                     .Select(s => new DiagnosticsBureauReportResult
                                                                                     {
                                                                                         StartDate = s.Key,
                                                                                         Occurrences = s.Where(c => c.ComfortPriority != 0).Count(),
                                                                                         AverageComfortPriority = s.Where(c => c.ComfortPriority != 0).Any() ? s.Where(c => c.ComfortPriority != 0).Average(c => c.ComfortPriority) : 0,
                                                                                     }).OrderBy(s => s.StartDate);

                if (comfortDiagnostics.Any())
                {
                    specifiedComfortChart.Series.Clear();

                    //set axis intervaltype
                    specifiedComfortChart.ChartAreas["comfortChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                    //set axis format:  g = short date and time, d = short date
                    specifiedComfortChart.ChartAreas["comfortChartArea"].AxisX.LabelStyle.Format = "d";

                    //area chart
                    var series = new Series
                    {
                        ChartType = SeriesChartType.Area,
                        Name = "Area Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        LegendText = CH.GetSafeContentItem(mContent, "AverageComfortPriority", this.GetType().Name, isSETheme),
                        Color = Color.FromArgb(120, Color.LightSteelBlue),
                    };

                    series.YAxisType = AxisType.Secondary;
                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "AverageComfortPriority");

                    //interpolate
                    specifiedComfortChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedComfortChart.Series.Add(series);


                    //line
                    series = new Series
                    {
                        ChartType = SeriesChartType.FastLine,
                        BorderWidth = 3,
                        Name = "Line Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        LegendText = CH.GetSafeContentItem(mContent, "TotalComfortIncidents", this.GetType().Name, isSETheme),
                        Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue)
                    };

                    //add data to series
                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    specifiedComfortChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedComfortChart.Series.Add(series);


                    //point
                    series = new Series
                    {
                        ChartType = SeriesChartType.Point,
                        MarkerStyle = MarkerStyle.Circle,
                        ShadowColor = Color.DimGray,
                        ShadowOffset = 4,
                        MarkerSize = 6,
                        MarkerBorderWidth = 3,
                        MarkerBorderColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue),
                        MarkerColor = Color.White,
                        //BorderWidth = 12,
                        Name = "Point Series",
                        //ToolTip = "X: #VALX{d}, Y: #VALY",
                        //LegendToolTip = ,
                        //LegendText = ,
                        Legend = "Hidden"
                        //BorderColor = Color.CornflowerBlue,
                        //Color = Color.White
                    };

                    series.Points.DataBindXY(comfortDiagnostics.ToArray(), "StartDate", comfortDiagnostics.ToArray(), "Occurrences");

                    //interpolate
                    specifiedComfortChart.DataManipulator.InsertEmptyPoints(1, Charting.IntervalType.Days, series);
                    series.EmptyPointStyle.CustomProperties = "EmptyPointValue = Zero";

                    specifiedComfortChart.Series.Add(series);
                }
            }
        }

        /// <summary>
        /// sets the equipment priorities period charts.
        /// </summary>
        private void SetEquipmentPrioritiesPeriodCharts(bool specifiedInclusion, Chart specifiedEnergyPeriodChart, Pair energyIntervals, Chart specifiedMaintenancePeriodChart, Pair maintenanceIntervals, Chart specifiedComfortPeriodChart, Pair comfortIntervals)
        {
            if (specifiedInclusion)
            {
                if (isScheduled)
                {
                    CustomReportsChartHelper.SwitchChartImageStorage(specifiedEnergyPeriodChart, "specifiedEnergyPeriodChart");
                    CustomReportsChartHelper.SwitchChartImageStorage(specifiedMaintenancePeriodChart, "specifiedMaintenancePeriodChart");
                    CustomReportsChartHelper.SwitchChartImageStorage(specifiedComfortPeriodChart, "specifiedComfortPeriodChart");
                }

                //energy period
                specifiedEnergyPeriodChart.Series.Clear();

                var series = new Series
                {
                    ChartType = SeriesChartType.Column,
                    Name = "Column Series",
                };

                series["PixelPointWidth"] = "80";

                series.Points.AddY(energyIntervals.Second);
                series.Points[0].Color = Color.FromArgb(175, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.EnergyYellow));
                series.Points[0].AxisLabel = CH.GetSafeContentItem(mContent, "Previous", this.GetType().Name, isSETheme);
                series.Points.AddY(energyIntervals.First);
                series.Points[1].Color = Color.FromArgb(175, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.HoneysuckleOrange));
                series.Points[1].AxisLabel = CH.GetSafeContentItem(mContent, "Current", this.GetType().Name, isSETheme);

                specifiedEnergyPeriodChart.Series.Add(series);

                //maintenance period
                specifiedMaintenancePeriodChart.Series.Clear();

                series = new Series
                {
                    ChartType = SeriesChartType.Column,
                    Name = "Column Series",
                };

                series["PixelPointWidth"] = "80";

                series.Points.AddY(maintenanceIntervals.Second);
                series.Points[0].Color = Color.FromArgb(175, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MediumGray));
                series.Points[0].AxisLabel = CH.GetSafeContentItem(mContent, "Previous", this.GetType().Name, isSETheme);
                series.Points[0].MarkerBorderWidth = 10;
                series.Points[0].MarkerBorderColor = Color.White;
                series.Points.AddY(maintenanceIntervals.First);
                series.Points[1].Color = Color.FromArgb(175, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.MaintenanceGray));
                series.Points[1].AxisLabel = CH.GetSafeContentItem(mContent, "Current", this.GetType().Name, isSETheme);
                series.Points[1].MarkerBorderWidth = 10;
                series.Points[1].MarkerBorderColor = Color.White;

                specifiedMaintenancePeriodChart.Series.Add(series);

                //ventilation comfort period
                specifiedComfortPeriodChart.Series.Clear();

                series = new Series
                {
                    ChartType = SeriesChartType.Column,
                    Name = "Column Series",
                };

                series["PixelPointWidth"] = "80";

                series.Points.AddY(comfortIntervals.Second);
                series.Points[0].Color = Color.FromArgb(175, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.SkyBlue));
                series.Points[0].AxisLabel = CH.GetSafeContentItem(mContent, "Previous", this.GetType().Name, isSETheme);
                series.Points[0].MarkerSize = 40;
                series.Points.AddY(comfortIntervals.First);
                series.Points[1].Color = Color.FromArgb(175, ColorHelper.ConvertHexToSystemColor(ColorHelper.SEColors.ComfortBlue));
                series.Points[1].AxisLabel = CH.GetSafeContentItem(mContent, "Current", this.GetType().Name, isSETheme);
                series.Points[1].MarkerSize = 40;

                specifiedComfortPeriodChart.Series.Add(series);
            }
        }

        #endregion

        #region Chart Methods

        protected void ChartEnergy_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(energyChart);
        }
        protected void ChartMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(maintenanceChart);
        }
        protected void ChartComfort_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(comfortChart);
        }

        protected void ChartVentilationEnergy_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(ventilationEnergyChart);
        }
        protected void ChartVentilationMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(ventilationMaintenanceChart);
        }
        protected void ChartVentilationComfort_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(ventilationComfortChart);
        }

        protected void ChartHeatingEnergy_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(heatingEnergyChart);
        }
        protected void ChartHeatingMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(heatingMaintenanceChart);
        }
        protected void ChartHeatingComfort_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(heatingComfortChart);
        }

        protected void ChartCoolingEnergy_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(coolingEnergyChart);
        }
        protected void ChartCoolingMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(coolingMaintenanceChart);
        }
        protected void ChartCoolingComfort_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(coolingComfortChart);
        }

        protected void ChartPlantEnergy_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(plantEnergyChart);
        }
        protected void ChartPlantMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(plantMaintenanceChart);
        }
        protected void ChartPlantComfort_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(plantComfortChart);
        }

        protected void ChartZoneEnergy_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(zoneEnergyChart);
        }
        protected void ChartZoneMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(zoneMaintenanceChart);
        }
        protected void ChartZoneComfort_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(zoneComfortChart);
        }

        protected void ChartSpecialtyEnergy_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(specialtyEnergyChart);
        }
        protected void ChartSpecialtyMaintenance_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(specialtyMaintenanceChart);
        }
        protected void ChartSpecialtyComfort_PrePaint(object sender, ChartPaintEventArgs e)
        {
            CustomReportsChartHelper.PrePaintLegends(specialtyComfortChart);
        }

        #endregion

        #region Button Events

        protected void automatedSummaryButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleRadEdit(editorAutomatedSummary, litAutomatedSummary);
        }
        protected void expertSummaryActionsButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleRadEdit(editorExpertSummary, litExpertSummary);
        }
        protected void energyTrendButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleRadEdit(editorEnergyTrend, litEnergy);
        }
        protected void maintenanceTrendButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleRadEdit(editorMaintenanceTrend, litMaintenance);
        }
        protected void comfortTrendButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleRadEdit(editorComfortTrend, litComfort);
        }

        protected void energyGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridEnergy);
        }
        protected void maintenanceGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridMaintenance);
        }
        protected void comfortGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridComfort);
        }

        protected void buildingSummaryGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridBuildingSummary);
        }
        protected void equipmentClassSummaryGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridEquipmentClassSummary);
        }
        protected void ventilationSummaryGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridVentilationSummary);
        }
        protected void heatingSummaryGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridHeatingSummary);
        }
        protected void coolingSummaryGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridCoolingSummary);
        }
        protected void plantSummaryGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridPlantSummary);
        }
        protected void zoneSummaryGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridZoneSummary);
        }
        protected void specialtySummaryGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridSpecialtySummary);
        }
        protected void frequentAlarmsGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridTopTenMostFrequentAlarms);
        }
        protected void totalAlarmsGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridTopTenTotalTimeInAlarm);
        }
        protected void projectsGridEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleGridEdit(gridTopOpenProjects);
        }
        protected void openTasksLVEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleListViewEdit(lvTopOpenTasks);
        }
        protected void completedTasksLVEditButton_Click(object sender, EventArgs e)
        {
            CustomReportsToggleHelper.ToggleListViewEdit(lvTopCompletedTasks);
        }

        protected void btnDownloadButton_Click(object sender, EventArgs e)
        {
            //Check if logged in
            if (SiteUser.Current.IsAnonymous)
            {
                //in order to refer back to module after logging in.
                //also for qr code reader redirect to profile pages after loggging in.
                Response.Redirect("/Home.aspx?referrer=" + Request.Url.PathAndQuery);
            }

            try
            {
                DownloadButtonActions(hdn_container.Value);
            }
            catch (Exception ex)
            {
                lblDownloadError.Text = downloadFailed;
                lblDownloadError.Visible = true;
                Logger.Log(DataConstants.LogLevel.ERROR, "Bureau report download failed.", ex);
            }
        }

        #endregion

        #region Repeater Events

        public void rptTopDiagnostics_OnItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                try
                {
                    //weekly diagnostic details content
                    HtmlGenericControl lblDiagnosticIssueClientName = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueClientName"));
                    HtmlGenericControl lblDiagnosticIssueBuildingName = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueBuildingName"));
                    HtmlGenericControl lblDiagnosticIssueEquipmentName = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueEquipmentName"));
                    HtmlGenericControl lblDiagnosticIssueAnalysisName = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueAnalysisName"));
                    HtmlGenericControl lblDiagnosticIssueStartDate = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueStartDate"));
                    HtmlGenericControl lblDiagnosticIssueAnalysisRange = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueAnalysisRange"));
                    HtmlGenericControl lblDiagnosticIssueCostSavings = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueCostSavings"));
                    HtmlGenericControl lblDiagnosticIssueEnergy = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueEnergy"));
                    HtmlGenericControl lblDiagnosticIssueMaintenance = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueMaintenance"));
                    HtmlGenericControl lblDiagnosticIssueComfort = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueComfort"));
                    HtmlGenericControl lblDiagnosticIssueNotes = ((HtmlGenericControl)e.Item.FindControl("lblDiagnosticIssueNotes"));

                    ((HtmlGenericControl)e.Item.FindControl("topRecentWeeklyDiagnosticIssueHeader")).InnerText = CH.GetSafeContentItem(mContent, "TopRecentWeeklyDiagnosticIssueDetailsHeader", this.GetType().Name, isSETheme);
                                        
                    lblDiagnosticIssueClientName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ClientName", lblDiagnosticIssueClientName.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueBuildingName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "BuildingName", lblDiagnosticIssueBuildingName.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueEquipmentName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "EquipmentName", lblDiagnosticIssueEquipmentName.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueAnalysisName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "AnalysisName", lblDiagnosticIssueAnalysisName.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueStartDate.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Date", lblDiagnosticIssueStartDate.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueAnalysisRange.InnerText = CH.GetAndPrependSafeContentItem(mContent, "DisplayInterval", lblDiagnosticIssueAnalysisRange.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueCostSavings.InnerText = CH.GetAndPrependSafeContentItem(mContent, "CostSavings", lblDiagnosticIssueCostSavings.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueEnergy.InnerText = CH.GetAndPrependSafeContentItem(mContent, "EnergyPriorityFull", lblDiagnosticIssueEnergy.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueMaintenance.InnerText = CH.GetAndPrependSafeContentItem(mContent, "MaintenancePriorityFull", lblDiagnosticIssueMaintenance.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueComfort.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ComfortPriorityFull", lblDiagnosticIssueComfort.InnerText, this.GetType().Name, isSETheme);
                    lblDiagnosticIssueNotes.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Notes", lblDiagnosticIssueNotes.InnerText, this.GetType().Name, isSETheme);
                }
                catch (Exception ex)
                {
                    Logger.Log(DataConstants.LogLevel.ERROR, "Bureau report content retrieval failed. Weekly diagnostic issues repeater.", ex);
                }
            }
        }

        #endregion

        #region List View Events

        protected void lvTopOpenTasks_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewOnDataBound(sender, e);    
        }

        protected void lvTopCompletedTasks_OnItemDataBound(object sender, ListViewItemEventArgs e)
        {
            ListViewOnDataBound(sender, e);            
        }

        protected void lvTopOpenTasks_OnLayoutCreated(object sender, EventArgs e)
        {
            ((HtmlGenericControl)lvTopOpenTasks.FindControl("spanTopOpenTasksTableHeader")).InnerText = CH.GetSafeContentItem(mContent, "TopOpenTasksTitle", this.GetType().Name, isSETheme);
        }

        protected void lvTopCompletedTasks_OnLayoutCreated(object sender, EventArgs e)
        {            
            ((HtmlGenericControl)lvTopCompletedTasks.FindControl("spanTopCompletedTasksTableHeader")).InnerText = CH.GetSafeContentItem(mContent, "TopCompletedTasksTitle", this.GetType().Name, isSETheme);
        }

        protected void ListViewOnDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.ItemType == ListViewItemType.DataItem)
            {
                int counter = 1;

                while (counter < e.Item.Controls.Count)
                {
                    ((HtmlGenericControl)e.Item.Controls[counter]).InnerText = CH.GetSafeContentItem(mContent, ((HtmlGenericControl)e.Item.Controls[counter]).InnerText, this.GetType().Name, isSETheme);

                    counter = counter + 6;
                }
            }
            else if (e.Item.ItemType == ListViewItemType.EmptyItem)
            {
                ((HtmlGenericControl)e.Item.Controls[0]).InnerText = CH.GetSafeContentItem(mContent, "NoDataDefaultMessage", this.GetType().Name, isSETheme);
            }
        }

        #endregion

        #region Grid Events

        //protected void gridTopOpenTasks_OnDataBound(object sender, EventArgs e)
        //{
        //    if (gridTopOpenTasks.HeaderRow != null)
        //    {
        //        //set headers
        //        HtmlTable tbl = (HtmlTable)gridTopOpenTasks.HeaderRow.FindControl("tblOpenTasksHeader");

        //        tbl.Rows[0].Cells[0].InnerText = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
        //        tbl.Rows[0].Cells[1].InnerText = CH.GetSafeContentItem(mContent, "Equipment", this.GetType().Name, isSETheme);
        //        tbl.Rows[0].Cells[2].InnerText = CH.GetSafeContentItem(mContent, "Summary", this.GetType().Name, isSETheme);
        //        tbl.Rows[0].Cells[3].InnerText = CH.GetSafeContentItem(mContent, "Assignee", this.GetType().Name, isSETheme);
        //        tbl.Rows[0].Cells[4].InnerText = CH.GetSafeContentItem(mContent, "DateCreated", this.GetType().Name, isSETheme);
        //        tbl.Rows[0].Cells[5].InnerText = CH.GetSafeContentItem(mContent, "DiagnosticDate", this.GetType().Name, isSETheme);
        //        tbl.Rows[1].Cells[0].InnerText = CH.GetSafeContentItem(mContent, "Description", this.GetType().Name, isSETheme);
        //        tbl.Rows[1].Cells[1].InnerText = CH.GetSafeContentItem(mContent, "Recommendations", this.GetType().Name, isSETheme);
        //        tbl.Rows[1].Cells[2].InnerText = CH.GetSafeContentItem(mContent, "Actions", this.GetType().Name, isSETheme);
        //    }
        //}

        //protected void gridTopCompletedTasks_OnDataBound(object sender, EventArgs e)
        //{
        //    if (gridTopCompletedTasks.HeaderRow != null)
        //    {
        //        //set headers
        //        HtmlTable tbl = (HtmlTable)gridTopCompletedTasks.HeaderRow.FindControl("tblCompletedTasksHeader"); 

        //        tbl.Rows[0].Cells[0].InnerText = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
        //        tbl.Rows[0].Cells[1].InnerText = CH.GetSafeContentItem(mContent, "Equipment", this.GetType().Name, isSETheme);                                                
        //        tbl.Rows[0].Cells[2].InnerText = CH.GetSafeContentItem(mContent, "Summary", this.GetType().Name, isSETheme);
        //        tbl.Rows[0].Cells[3].InnerText = CH.GetSafeContentItem(mContent, "Assignee", this.GetType().Name, isSETheme); 
        //        tbl.Rows[0].Cells[4].InnerText = CH.GetSafeContentItem(mContent, "DateCreated", this.GetType().Name, isSETheme);
        //        tbl.Rows[0].Cells[5].InnerText = CH.GetSafeContentItem(mContent, "DateCompleted", this.GetType().Name, isSETheme);
        //        tbl.Rows[1].Cells[0].InnerText = CH.GetSafeContentItem(mContent, "Description", this.GetType().Name, isSETheme);
        //        tbl.Rows[1].Cells[1].InnerText = CH.GetSafeContentItem(mContent, "Recommendations", this.GetType().Name, isSETheme);
        //        tbl.Rows[1].Cells[2].InnerText = CH.GetSafeContentItem(mContent, "Actions", this.GetType().Name, isSETheme);
        //    }
        //}

        #endregion

        #region Culture and Content Helper Methods

        protected void DelayedInitializeCulture()
        {
            if (cultureFormat != userCulture)
            {
                UICulture = Culture = userCulture;

                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(userCulture);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(userCulture);

                base.InitializeCulture();

                CultureInfo tempCulture = CultureInfo.GetCultureInfo(cultureFormat);
                tempsd = Convert.ToDateTime(tempsd).ToString(tempCulture);
                temped = Convert.ToDateTime(temped).ToString(tempCulture);
                temppsd = Convert.ToDateTime(temppsd).ToString(tempCulture);
                tempped = Convert.ToDateTime(tempped).ToString(tempCulture);
            }
                
            UICulture = Culture = cultureFormat;

            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureFormat);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureFormat);

            base.InitializeCulture();

            //set dates based on culture now
            sd = Convert.ToDateTime(tempsd);
            ed = Convert.ToDateTime(temped);
            psd = Convert.ToDateTime(temppsd);
            ped = Convert.ToDateTime(tempped);            
        }

        private void GetAndSetInitialContent()
        {
            //get content            
            mContent = mDataManager.ContentDataMapper.GetSafeContentSections(new int[] { CS.Global, CS.Administration, CS.Bureau, CS.Alarms, CS.Projects, CS.Tasks }, languageCultureName);

            if (isScheduled)
                automatedSummary = CH.GetSafeContentItem(mContent, "AutomatedSummaryDefaultValue", this.GetType().Name);

            //period and compared headers
            divPeriodHeader1.InnerText = CH.GetAndPrependSafeContentItem(mContent, "PeriodHeader", divPeriodHeader1.InnerText, this.GetType().Name, isSETheme);
            divPeriodHeader2.InnerText = CH.GetAndPrependSafeContentItem(mContent, "PeriodHeader", divPeriodHeader2.InnerText, this.GetType().Name, isSETheme);
            divPeriodHeader3.InnerText = CH.GetAndPrependSafeContentItem(mContent, "PeriodHeader", divPeriodHeader3.InnerText, this.GetType().Name, isSETheme);
            divComparedTo1.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ComparedToHeader", divComparedTo1.InnerText, this.GetType().Name, isSETheme);
            divComparedTo2.InnerText = CH.GetAndPrependSafeContentItem(mContent, "ComparedToHeader", divComparedTo2.InnerText, this.GetType().Name, isSETheme);


            //secondary headers
            headerSecondaryTitle1.InnerText = CH.GetSafeContentItem(mContent, "BureauReportHeader", this.GetType().Name, isSETheme);
            headerSecondaryTitle2.InnerText = CH.GetSafeContentItem(mContent, "PortfolioSummaryHeader", this.GetType().Name, isSETheme);

            //facility
            divFacilityTitle.InnerText = CH.GetSafeContentItem(mContent, "FacilityTitle", this.GetType().Name, isSETheme);
            lblFacilityCustomer.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Customer", lblFacilityCustomer.InnerText, this.GetType().Name, isSETheme);
            lblFacilityLocation.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Location", lblFacilityLocation.InnerText, this.GetType().Name, isSETheme);
            lblFacilityYear.InnerText = CH.GetAndPrependSafeContentItem(mContent, "BuildingYearBuilt", lblFacilityYear.InnerText, this.GetType().Name, isSETheme);
            lblFacilityArea.InnerText = CH.GetAndPrependSafeContentItem(mContent, "TotalArea", lblFacilityArea.InnerText, this.GetType().Name, isSETheme);
            lblFacilityBuildingName.InnerText = CH.GetAndPrependSafeContentItem(mContent, "BuildingName", lblFacilityBuildingName.InnerText, this.GetType().Name, isSETheme);
            lblFacilityBuildingType.InnerText = CH.GetAndPrependSafeContentItem(mContent, "BuildingType", lblFacilityBuildingType.InnerText, this.GetType().Name, isSETheme);
            lblFacilityNumBuildings.InnerText = CH.GetAndPrependSafeContentItem(mContent, "NumBuildingsTableHeader", lblFacilityNumBuildings.InnerText, this.GetType().Name, isSETheme);

            //table of contents, and new page titles
            newPageTitle1.InnerText = CH.GetSafeContentItem(mContent, "TableOfContentsPageHeader", this.GetType().Name, isSETheme);
            lblPortfolioSummaryPage.InnerText = CH.GetSafeContentItem(mContent, "PorfolioSummaryPageHeader", this.GetType().Name, isSETheme);
            lblExpertSummaryPage.InnerText = newPageTitle2.InnerText = CH.GetSafeContentItem(mContent, "ExpertSummaryPageHeader", this.GetType().Name, isSETheme);
            lblBuildingSummaryPage.InnerText = newPageTitle3.InnerText = CH.GetSafeContentItem(mContent, "BuildingSummaryPageHeader", this.GetType().Name, isSETheme);
            lblEquipmentClassSummaryPage.InnerText = newPageTitle4.InnerText = CH.GetSafeContentItem(mContent, "EquipmentClassSummaryPageHeader", this.GetType().Name, isSETheme);
            lblBuildingTrendsPage.InnerText = newPageTitle5.InnerText = CH.GetSafeContentItem(mContent, "BuildingTrendsSummaryPageHeader", this.GetType().Name, isSETheme);
            lblBuildingTopIssuesPage.InnerText = newPageTitle6.InnerText = CH.GetSafeContentItem(mContent, "TopDiagnosticSummaryPageHeader", this.GetType().Name, isSETheme);
            lblVentilationSummaryPage.InnerText = newPageTitle7.InnerText = CH.GetSafeContentItem(mContent, "VentilationSummaryPageHeader", this.GetType().Name, isSETheme);
            lblHeatingSummaryPage.InnerText = newPageTitle8.InnerText = CH.GetSafeContentItem(mContent, "HeatingSummaryPageHeader", this.GetType().Name, isSETheme);
            lblCoolingSummaryPage.InnerText = newPageTitle9.InnerText = CH.GetSafeContentItem(mContent, "CoolingSummaryPageHeader", this.GetType().Name, isSETheme);
            lblPlantSummaryPage.InnerText = newPageTitle10.InnerText = CH.GetSafeContentItem(mContent, "PlantSummaryPageHeader", this.GetType().Name, isSETheme);
            lblZoneSummaryPage.InnerText = newPageTitle11.InnerText = CH.GetSafeContentItem(mContent, "ZoneSummaryPageHeader", this.GetType().Name, isSETheme);
            lblSpecialtySummaryPage.InnerText = newPageTitle12.InnerText = CH.GetSafeContentItem(mContent, "SpecialtySummaryPageHeader", this.GetType().Name, isSETheme);
            lblAlarms.InnerText = newPageTitle13.InnerText = CH.GetSafeContentItem(mContent, "AlarmsSummaryPageHeader", this.GetType().Name, isSETheme);
            lblProjects.InnerText = newPageTitle14.InnerText = CH.GetSafeContentItem(mContent, "ProjectsSummaryPageHeader", this.GetType().Name, isSETheme);
            lblTasks.InnerText = newPageTitle15.InnerText = CH.GetSafeContentItem(mContent, "TasksSummaryPageHeader", this.GetType().Name, isSETheme);
            lblTopDiagnostics.InnerText = newPageTitle16.InnerText = CH.GetSafeContentItem(mContent, "DiagnosticDetailsSummaryPageHeader", this.GetType().Name, isSETheme);

            //utility common
            divTotalUtilityTotalLabel1.InnerText = divTotalUtilityTotalLabel2.InnerText = divAvoidableUtilityTotalLabel1.InnerText = CH.GetSafeContentItem(mContent, "TotalThisPeriodLabel", this.GetType().Name, isSETheme);

            //utility
            divTotalUtilityTitle.InnerText = CH.GetSafeContentItem(mContent, "TotalUtilityTitle", this.GetType().Name, isSETheme);
            divAvoidableUtilityTitle.InnerText = CH.GetSafeContentItem(mContent, "AvoidableUtilityTitle", this.GetType().Name, isSETheme);

            //period
            divPeriodTrendSummaryTitle.InnerText = CH.GetSafeContentItem(mContent, "PeriodTrendSummaryTitle", this.GetType().Name, isSETheme);
            spanPeriodAvoidableCost.InnerText = CH.GetAndPrependSafeContentItem(mContent, "AvoidableCost", spanPeriodAvoidableCost.InnerText, this.GetType().Name, isSETheme);
            spanPeriodEnergy.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Energy", spanPeriodEnergy.InnerText, this.GetType().Name, isSETheme);
            spanPeriodMaintenance.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Maintenance", spanPeriodMaintenance.InnerText, this.GetType().Name, isSETheme);
            spanPeriodComfort.InnerText = CH.GetAndPrependSafeContentItem(mContent, "Comfort", spanPeriodComfort.InnerText, this.GetType().Name, isSETheme);

            //summary
            summaryHeader.InnerText = CH.GetSafeContentItem(mContent, "SummaryHeader", this.GetType().Name, isSETheme);
            divExpertSummaryHeader.InnerText = CH.GetSafeContentItem(mContent, "ExpertSummaryHeader", this.GetType().Name, isSETheme);
            spanBuildingSummary.InnerText = CH.GetSafeContentItem(mContent, "BuildingSummaryTableHeader", this.GetType().Name, isSETheme);
            spanEquipmentClassSummary.InnerText = CH.GetSafeContentItem(mContent, "EquipmentClassSummaryTableHeader", this.GetType().Name, isSETheme);

            //summary grids
            gridBuildingSummary.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
            gridEquipmentClassSummary.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "EquipmentClass", this.GetType().Name, isSETheme);
            gridBuildingSummary.Columns[1].HeaderText = gridEquipmentClassSummary.Columns[1].HeaderText = CH.GetSafeContentItem(mContent, "NumEquipmentTableHeader", this.GetType().Name, isSETheme);
            gridBuildingSummary.Columns[2].HeaderText = gridEquipmentClassSummary.Columns[2].HeaderText = CH.GetSafeContentItem(mContent, "NumEquipmentWithFaultsTableHeader", this.GetType().Name, isSETheme);
            gridBuildingSummary.Columns[3].HeaderText = gridEquipmentClassSummary.Columns[3].HeaderText = CH.GetSafeContentItem(mContent, "Cost", this.GetType().Name, isSETheme);
            gridBuildingSummary.Columns[4].HeaderText = gridEquipmentClassSummary.Columns[4].HeaderText = CH.GetSafeContentItem(mContent, "PercentEquipmentWithEnergyFaultsTableHeader", this.GetType().Name, isSETheme);
            gridBuildingSummary.Columns[5].HeaderText = gridEquipmentClassSummary.Columns[5].HeaderText = CH.GetSafeContentItem(mContent, "PercentEquipmentWithMaintenanceFaultsTableHeader", this.GetType().Name, isSETheme);
            gridBuildingSummary.Columns[6].HeaderText = gridEquipmentClassSummary.Columns[6].HeaderText = CH.GetSafeContentItem(mContent, "PercentEquipmentWithComfortFaultsTableHeader", this.GetType().Name, isSETheme);

            //prepared by
            divPrepartedByTitle.InnerText = CH.GetAndPrependSafeContentItem(mContent, "PreparedByTitle", divPrepartedByTitle.InnerText, this.GetType().Name, isSETheme);

            //common grids
            gridBuildingSummary.EmptyDataText = gridEquipmentClassSummary.EmptyDataText =
            gridVentilationSummary.EmptyDataText = gridHeatingSummary.EmptyDataText = gridCoolingSummary.EmptyDataText = gridPlantSummary.EmptyDataText = gridZoneSummary.EmptyDataText = gridSpecialtySummary.EmptyDataText =
            gridTopTenMostFrequentAlarms.EmptyDataText = gridTopTenTotalTimeInAlarm.EmptyDataText = gridTopOpenProjects.EmptyDataText =
                CH.GetSafeContentItem(mContent, "NoDataDefaultMessage", this.GetType().Name, isSETheme);

            gridEnergy.EmptyDataText = gridMaintenance.EmptyDataText = gridComfort.EmptyDataText =
                CH.GetSafeContentItem(mContent, "NoIssuesDefaultMessage", this.GetType().Name, isSETheme);


            //trends
            totalEnergyTrendsHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalEnergyTrendsHeader", this.GetType().Name, isSETheme);
            totalMaintenanceTrendsHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalMaintenanceTrendsHeader", this.GetType().Name, isSETheme);
            totalComfortTrendsHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalComfortTrendsHeader", this.GetType().Name, isSETheme);
            maintenanceChart.ChartAreas[0].AxisY.Title = comfortChart.ChartAreas[0].AxisY.Title = CH.GetSafeContentItem(mContent, "TotalIncidents", this.GetType().Name, isSETheme);
            maintenanceChart.ChartAreas[0].AxisY2.Title = comfortChart.ChartAreas[0].AxisY2.Title = CH.GetSafeContentItem(mContent, "AveragePriority", this.GetType().Name, isSETheme); 
            energyChart.ChartAreas[0].AxisY.Title = CH.GetSafeContentItem(mContent, "AvoidableCost", this.GetType().Name, isSETheme);
            energyChart.ChartAreas[0].AxisY2.Title = CH.GetSafeContentItem(mContent, "TotalIncidents", this.GetType().Name, isSETheme);            
            energyPeriodChart.ChartAreas[0].AxisY.Title = CH.GetSafeContentItem(mContent, "AverageAvoidableCost", this.GetType().Name, isSETheme);
            maintenancePeriodChart.ChartAreas[0].AxisY.Title = comfortPeriodChart.ChartAreas[0].AxisY.Title = CH.GetSafeContentItem(mContent, "AverageIncidents", this.GetType().Name, isSETheme);

            //top diagnostics
            topDiagnosticIssuesHeader.InnerText = CH.GetSafeContentItem(mContent, "TopDiagnosticIssuesHeader", this.GetType().Name, isSETheme);
            spanTopEnergyIssuesTableHeader.InnerText = CH.GetSafeContentItem(mContent, "Energy", this.GetType().Name, isSETheme);
            spanTopMaintenanceIssuesTableHeader.InnerText = CH.GetSafeContentItem(mContent, "Maintenance", this.GetType().Name, isSETheme);
            spanTopComfortIssuesTableHeader.InnerText = CH.GetSafeContentItem(mContent, "Comfort", this.GetType().Name, isSETheme);

            //top diagnostics grid headers
            gridEnergy.Columns[0].HeaderText = gridMaintenance.Columns[0].HeaderText = gridComfort.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
            gridEnergy.Columns[1].HeaderText = gridMaintenance.Columns[1].HeaderText = gridComfort.Columns[1].HeaderText = CH.GetSafeContentItem(mContent, "Equipment", this.GetType().Name, isSETheme);
            gridEnergy.Columns[2].HeaderText = gridMaintenance.Columns[2].HeaderText = gridComfort.Columns[2].HeaderText = CH.GetSafeContentItem(mContent, "Notes", this.GetType().Name, isSETheme);
            gridEnergy.Columns[3].HeaderText = gridMaintenance.Columns[3].HeaderText = gridComfort.Columns[3].HeaderText = CH.GetSafeContentItem(mContent, "Actions", this.GetType().Name, isSETheme);
            gridEnergy.Columns[4].HeaderText = CH.GetSafeContentItem(mContent, "Cost", this.GetType().Name, isSETheme);
            gridEnergy.Columns[8].HeaderText = gridMaintenance.Columns[7].HeaderText = gridComfort.Columns[7].HeaderText = CH.GetSafeContentItem(mContent, "Occurrences", this.GetType().Name, isSETheme);

            //equipment table headers
            spanVentilationTableHeader.InnerText = CH.GetSafeContentItem(mContent, "VentilationTableHeader", this.GetType().Name, isSETheme);
            spanHeatingTableHeader.InnerText = CH.GetSafeContentItem(mContent, "HeatingTableHeader", this.GetType().Name, isSETheme);
            spanCoolingTableHeader.InnerText = CH.GetSafeContentItem(mContent, "CoolingTableHeader", this.GetType().Name, isSETheme);
            spanPlantTableHeader.InnerText = CH.GetSafeContentItem(mContent, "PlantTableHeader", this.GetType().Name, isSETheme);
            spanZoneTableHeader.InnerText = CH.GetSafeContentItem(mContent, "ZoneTableHeader", this.GetType().Name, isSETheme);
            spanSpecialtyTableHeader.InnerText = CH.GetSafeContentItem(mContent, "SpecialtyTableHeader", this.GetType().Name, isSETheme);

            //euipment trend headers
            ventilationEnergyHeader.InnerText = heatingEnergyHeader.InnerText = coolingEnergyHeader.InnerText = plantEnergyHeader.InnerText = zoneEnergyHeader.InnerText = specialtyEnergyHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalEnergyTrendsHeader", this.GetType().Name, isSETheme);
            ventilationMaintenanceHeader.InnerText = heatingMaintenanceHeader.InnerText = coolingMaintenanceHeader.InnerText = plantMaintenanceHeader.InnerText = zoneMaintenanceHeader.InnerText = specialtyMaintenanceHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalMaintenanceTrendsHeader", this.GetType().Name, isSETheme);
            ventilationComfortHeader.InnerText = heatingComfortHeader.InnerText = coolingComfortHeader.InnerText = plantComfortHeader.InnerText = zoneComfortHeader.InnerText = specialtyComfortHeader.InnerText = CH.GetSafeContentItem(mContent, "TotalComfortTrendsHeader", this.GetType().Name, isSETheme);

            //equipment charts common
            ventilationEnergyChart.ChartAreas[0].AxisY.Title = ventilationMaintenanceChart.ChartAreas[0].AxisY.Title = ventilationComfortChart.ChartAreas[0].AxisY.Title =
            heatingEnergyChart.ChartAreas[0].AxisY.Title = heatingMaintenanceChart.ChartAreas[0].AxisY.Title = heatingComfortChart.ChartAreas[0].AxisY.Title =
            coolingEnergyChart.ChartAreas[0].AxisY.Title = coolingMaintenanceChart.ChartAreas[0].AxisY.Title = coolingComfortChart.ChartAreas[0].AxisY.Title =
            plantEnergyChart.ChartAreas[0].AxisY.Title = plantMaintenanceChart.ChartAreas[0].AxisY.Title = plantComfortChart.ChartAreas[0].AxisY.Title =
            zoneEnergyChart.ChartAreas[0].AxisY.Title = zoneMaintenanceChart.ChartAreas[0].AxisY.Title = zoneComfortChart.ChartAreas[0].AxisY.Title =
            specialtyEnergyChart.ChartAreas[0].AxisY.Title = specialtyMaintenanceChart.ChartAreas[0].AxisY.Title = specialtyComfortChart.ChartAreas[0].AxisY.Title =
                CH.GetSafeContentItem(mContent, "TotalIncidents", this.GetType().Name, isSETheme);

            ventilationEnergyChart.ChartAreas[0].AxisY2.Title = heatingEnergyChart.ChartAreas[0].AxisY2.Title = coolingEnergyChart.ChartAreas[0].AxisY2.Title = plantEnergyChart.ChartAreas[0].AxisY2.Title = zoneEnergyChart.ChartAreas[0].AxisY2.Title = specialtyEnergyChart.ChartAreas[0].AxisY2.Title =
                CH.GetSafeContentItem(mContent, "AvoidableCost", this.GetType().Name, isSETheme);

            ventilationMaintenanceChart.ChartAreas[0].AxisY2.Title = ventilationComfortChart.ChartAreas[0].AxisY2.Title =
            heatingMaintenanceChart.ChartAreas[0].AxisY2.Title = heatingComfortChart.ChartAreas[0].AxisY2.Title =
            coolingMaintenanceChart.ChartAreas[0].AxisY2.Title = coolingComfortChart.ChartAreas[0].AxisY2.Title =
            plantMaintenanceChart.ChartAreas[0].AxisY2.Title = plantComfortChart.ChartAreas[0].AxisY2.Title =
            zoneMaintenanceChart.ChartAreas[0].AxisY2.Title = zoneComfortChart.ChartAreas[0].AxisY2.Title =
            specialtyMaintenanceChart.ChartAreas[0].AxisY2.Title = specialtyComfortChart.ChartAreas[0].AxisY2.Title =
                CH.GetSafeContentItem(mContent, "AveragePriority", this.GetType().Name, isSETheme);

            ventilationEnergyPeriodChart.ChartAreas[0].AxisY.Title = heatingEnergyPeriodChart.ChartAreas[0].AxisY.Title = coolingEnergyPeriodChart.ChartAreas[0].AxisY.Title = plantEnergyPeriodChart.ChartAreas[0].AxisY.Title = zoneEnergyPeriodChart.ChartAreas[0].AxisY.Title = specialtyEnergyPeriodChart.ChartAreas[0].AxisY.Title =
                CH.GetSafeContentItem(mContent, "AverageAvoidableCost", this.GetType().Name, isSETheme);

            ventilationMaintenancePeriodChart.ChartAreas[0].AxisY.Title = ventilationComfortPeriodChart.ChartAreas[0].AxisY.Title =
            heatingMaintenancePeriodChart.ChartAreas[0].AxisY.Title = heatingComfortPeriodChart.ChartAreas[0].AxisY.Title =
            coolingMaintenancePeriodChart.ChartAreas[0].AxisY.Title = coolingComfortPeriodChart.ChartAreas[0].AxisY.Title =
            plantMaintenancePeriodChart.ChartAreas[0].AxisY.Title = plantComfortPeriodChart.ChartAreas[0].AxisY.Title =
            zoneMaintenancePeriodChart.ChartAreas[0].AxisY.Title = zoneComfortPeriodChart.ChartAreas[0].AxisY.Title =
            specialtyMaintenancePeriodChart.ChartAreas[0].AxisY.Title = specialtyComfortPeriodChart.ChartAreas[0].AxisY.Title =
                CH.GetSafeContentItem(mContent, "AverageIncidents", this.GetType().Name, isSETheme);

            //equipment grids common
            gridCoolingSummary.Columns[0].HeaderText = gridHeatingSummary.Columns[0].HeaderText = gridPlantSummary.Columns[0].HeaderText = gridSpecialtySummary.Columns[0].HeaderText = gridVentilationSummary.Columns[0].HeaderText = gridZoneSummary.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
            gridCoolingSummary.Columns[1].HeaderText = gridHeatingSummary.Columns[1].HeaderText = gridPlantSummary.Columns[1].HeaderText = gridSpecialtySummary.Columns[1].HeaderText = gridVentilationSummary.Columns[1].HeaderText = gridZoneSummary.Columns[1].HeaderText = CH.GetSafeContentItem(mContent, "Equipment", this.GetType().Name, isSETheme);
            gridCoolingSummary.Columns[2].HeaderText = gridHeatingSummary.Columns[2].HeaderText = gridPlantSummary.Columns[2].HeaderText = gridSpecialtySummary.Columns[2].HeaderText = gridVentilationSummary.Columns[2].HeaderText = gridZoneSummary.Columns[2].HeaderText = CH.GetSafeContentItem(mContent, "Analysis", this.GetType().Name, isSETheme);
            gridCoolingSummary.Columns[3].HeaderText = gridHeatingSummary.Columns[3].HeaderText = gridPlantSummary.Columns[3].HeaderText = gridSpecialtySummary.Columns[3].HeaderText = gridVentilationSummary.Columns[3].HeaderText = gridZoneSummary.Columns[3].HeaderText = CH.GetSafeContentItem(mContent, "FaultOccurrences", this.GetType().Name, isSETheme);
            gridCoolingSummary.Columns[4].HeaderText = gridHeatingSummary.Columns[4].HeaderText = gridPlantSummary.Columns[4].HeaderText = gridSpecialtySummary.Columns[4].HeaderText = gridVentilationSummary.Columns[4].HeaderText = gridZoneSummary.Columns[4].HeaderText = CH.GetSafeContentItem(mContent, "Cost", this.GetType().Name, isSETheme);
            gridCoolingSummary.Columns[5].HeaderText = gridHeatingSummary.Columns[5].HeaderText = gridPlantSummary.Columns[5].HeaderText = gridSpecialtySummary.Columns[5].HeaderText = gridVentilationSummary.Columns[5].HeaderText = gridZoneSummary.Columns[5].HeaderText = CH.GetSafeContentItem(mContent, "EnergyPriority", this.GetType().Name, isSETheme);
            gridCoolingSummary.Columns[6].HeaderText = gridHeatingSummary.Columns[6].HeaderText = gridPlantSummary.Columns[6].HeaderText = gridSpecialtySummary.Columns[6].HeaderText = gridVentilationSummary.Columns[6].HeaderText = gridZoneSummary.Columns[6].HeaderText = CH.GetSafeContentItem(mContent, "MaintenancePriority", this.GetType().Name, isSETheme);
            gridCoolingSummary.Columns[7].HeaderText = gridHeatingSummary.Columns[7].HeaderText = gridPlantSummary.Columns[7].HeaderText = gridSpecialtySummary.Columns[7].HeaderText = gridVentilationSummary.Columns[7].HeaderText = gridZoneSummary.Columns[7].HeaderText = CH.GetSafeContentItem(mContent, "ComfortPriority", this.GetType().Name, isSETheme);

            //equipment pie charts
            ventilationSummaryAirHandlersPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "AirHandlers", this.GetType().Name, isSETheme);
            ventilationSummaryFansPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "Fans", this.GetType().Name, isSETheme);
            ventilationSummaryVentilationSystemsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "VentilationSystems", this.GetType().Name, isSETheme);

            coolingSummaryChillersPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "Chillers", this.GetType().Name, isSETheme);
            coolingSummaryCoolingSystemsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "CoolingSystems", this.GetType().Name, isSETheme);
            coolingSummaryHeatRejectionPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "HeatRejection", this.GetType().Name, isSETheme);
            heatingSummaryBoilersPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "Boilers", this.GetType().Name, isSETheme);
            heatingSummaryHeatingSystemsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "HeatingSystems", this.GetType().Name, isSETheme);
            plantSummaryDomesticWaterSystemsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "DomesticWaterSystems", this.GetType().Name, isSETheme);
            plantSummaryDualTemperatureWaterSystemsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "DualTemperatureWaterSystems", this.GetType().Name, isSETheme);
            plantSummaryHeatExchangerPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "HeatExchanger", this.GetType().Name, isSETheme);
            plantSummaryHeatRecoverySystemsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "HeatRecoverySystems", this.GetType().Name, isSETheme);
            plantSummaryPumpsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "Pumps", this.GetType().Name, isSETheme);
            plantSummaryStoragePieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "Storage", this.GetType().Name, isSETheme);
            specialtySummarySpecialtyEquipmentPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "SpecialtyEquipment", this.GetType().Name, isSETheme);
            zoneSummaryZoneEquipmentPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "ZoneEquipment", this.GetType().Name, isSETheme);
            zoneSummaryZoneGroupsPieChart.Titles[0].Text = CH.GetSafeContentItem(mContent, "ZoneGroups", this.GetType().Name, isSETheme);

            //alarms
            spanTopMostFrequentAlarmsTableHeader.InnerText = CH.GetSafeContentItem(mContent, "TopMostFrequentAlarmsTitle", this.GetType().Name, isSETheme);
            spanTopTotalTimeInAlarmTableHeader.InnerText = CH.GetSafeContentItem(mContent, "TopTotalTimeInAlarmTitle", this.GetType().Name, isSETheme);

            gridTopTenMostFrequentAlarms.Columns[0].HeaderText = gridTopTenTotalTimeInAlarm.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "Alarm", this.GetType().Name, isSETheme);
            gridTopTenMostFrequentAlarms.Columns[1].HeaderText = gridTopTenTotalTimeInAlarm.Columns[1].HeaderText = CH.GetSafeContentItem(mContent, "Equipment", this.GetType().Name, isSETheme);
            gridTopTenMostFrequentAlarms.Columns[2].HeaderText = gridTopTenTotalTimeInAlarm.Columns[2].HeaderText = CH.GetSafeContentItem(mContent, "Building", this.GetType().Name, isSETheme);
            gridTopTenMostFrequentAlarms.Columns[3].HeaderText = gridTopTenTotalTimeInAlarm.Columns[3].HeaderText = CH.GetSafeContentItem(mContent, "Notes", this.GetType().Name, isSETheme);
            gridTopTenMostFrequentAlarms.Columns[4].HeaderText = CH.GetSafeContentItem(mContent, "Occurrences", this.GetType().Name, isSETheme);
            gridTopTenTotalTimeInAlarm.Columns[4].HeaderText = CH.GetSafeContentItem(mContent, "TotalTimeInAlarmTableHeader", this.GetType().Name, isSETheme);

            //projects
            spanTopOpenProjectsTableHeader.InnerText = CH.GetSafeContentItem(mContent, "TopOpenProjectsTitle", this.GetType().Name, isSETheme);

            gridTopOpenProjects.Columns[0].HeaderText = CH.GetSafeContentItem(mContent, "Project", this.GetType().Name, isSETheme);
            gridTopOpenProjects.Columns[1].HeaderText = CH.GetSafeContentItem(mContent, "EquipmentAnalyses", this.GetType().Name, isSETheme);
            gridTopOpenProjects.Columns[2].HeaderText = CH.GetSafeContentItem(mContent, "Notes", this.GetType().Name, isSETheme);
            gridTopOpenProjects.Columns[3].HeaderText = CH.GetSafeContentItem(mContent, "TargetStartDate", this.GetType().Name, isSETheme);
            gridTopOpenProjects.Columns[4].HeaderText = CH.GetSafeContentItem(mContent, "ProjectedSavings", this.GetType().Name, isSETheme);            
        }

        private void GetAndSetPostbackContent()
        {
            //sets content that gets lost on postback   
         
            IEnumerable<Content> tempContent = mDataManager.ContentDataMapper.GetSafeContentSections(new int[] { CS.Global }, languageCultureName);

            //title
            title.Text = Utility.TokenVariableHelper.FormatSpecialThemeBasedTokens(isSETheme, title.Text).ToUpper();

            //download
            btnDownload.Text = CH.GetSafeContentItem(tempContent, "Download", this.GetType().Name, isSETheme);
            spanDownload.InnerText = CH.GetSafeContentItem(tempContent, "DownloadParagraph", this.GetType().Name, isSETheme);
        }

        private string TokenReplacer(string content)
        {
            return TokenVariableHelper.FormatSpecialBureauTokens(isSETheme, content, sd.ToString("MMM d, yyyy"), ed.ToString("MMM d, yyyy"), psd.ToString("MMM d, yyyy"), ped.ToString("MMM d, yyyy"), totalCostPercent, CultureHelper.FormatCurrencyAsString(buildingLCID, totalCost), CultureHelper.FormatCurrencyAsString(buildingLCID, previousTotalCost), intervalCostPercent, CultureHelper.FormatCurrencyAsString(buildingLCID, intervalCost), intervalEnergyIncidents.ToString(), intervalEnergyPercent, intervalMaintenanceIncidents.ToString(), intervalMaintenancePercent, intervalComfortIncidents.ToString(), intervalComfortPercent);
        }

        private string CreatePeriodContent(DateTime sd, DateTime ed)
        {
            return String.Format("<div class='labelMedium'>{0} - {1}</div>", sd.ToString("MMM d, yyyy"), ed.ToString("MMM d, yyyy"));
        }

        #endregion

        #region GET and POST Request Data

        private void GetFormPostData()
        {
            foreach (string key in Request.Form.AllKeys)
            {
                if (key == null) continue;

                //ignore cw header bar client ddl and module ddl

                //previous start date
                if (key.Contains("$txtPreviousStartDate"))
                {
                    temppsd = Request.Form[key];
                }
                //previous end date
                else if (key.Contains("$txtPreviousEndDate"))
                {
                    tempped = Request.Form[key];
                }
                //start date
                else if (key.Contains("$txtStartDate"))
                {
                    tempsd = Request.Form[key];
                }
                //end date
                else if (key.Contains("$txtEndDate"))
                {
                    temped = Request.Form[key];
                }
                //threshold
                //else if (key.Contains("$ddlThreshold"))
                //{
                //    th = Convert.ToInt32(Request.Form[key]);
                //}
                //cid 
                else if (key.Contains("$hdnCID"))
                {
                    cid = Convert.ToInt32(Request.Form[key]);
                }
                //uid
                else if (key.Contains("$hdnUID"))
                {
                    uid = Convert.ToInt32(Request.Form[key]);
                }
                //oid
                else if (key.Contains("$hdnOID"))
                {
                    oid = Convert.ToInt32(Request.Form[key]);
                }
                //user culture name
                else if (key.Contains("$hdnUserCulture"))
                {
                    userCulture = Request.Form[key];
                }
                //building lb
                else if (key.Contains("$lbBuildings"))
                {
                    bids = Request.Form[key].Split(',').Select(x => Int32.Parse(x)).ToList();
                }
                //theme ddl
                else if (key.Contains("$ddlTheme"))
                {
                    isSETheme = Convert.ToBoolean(Convert.ToInt32(Request.Form[key]));
                }
                //language ddl
                else if (key.Contains("$ddlLanguage"))
                {
                    languageCultureName = Request.Form[key];
                }
                //culture ddl
                else if (key.Contains("$ddlCulture"))
                {
                    cultureFormat = CultureHelper.GetCultureName(Convert.ToInt32(Request.Form[key]));
                }
                //client logos
                else if (key.Contains("$chkShowClientLogos"))
                {
                    //form post will post value as "on" only if it is selected, otherwise it wont be in the request.
                    showClientLogos = true;
                }
                else if (key.Contains("$chkIncludeCoverPage"))
                {
                    includeCoverPage = true;
                }
                else if (key.Contains("$chkIncludeContentsPage"))
                {
                    includeContentsPage = true;
                }
                else if (key.Contains("$chkIncludeExpertSummary"))
                {
                    includeExpertSummary = true;
                }
                else if (key.Contains("$chkIncludeBuildingSummaryReport"))
                {
                    includeBuildingSummaryReport = true;
                }
                else if (key.Contains("$chkIncludeEquipmentClassSummaryReport"))
                {
                    includeEquipmentClassSummaryReport = true;
                }
                else if (key.Contains("$chkIncludeBuildingTrendsSummaryReport"))
                {
                    includeBuildingTrendsSummaryReport = true;
                }
                else if (key.Contains("$chkIncludeBuildingTopIssuesSummaryReport"))
                {
                    includeBuildingTopIssuesSummaryReport = true;
                }
                else if (key.Contains("$chkIncludeVentilationEquipmentReport"))
                {
                    includeVentilationEquipmentReport = true;
                }
                else if (key.Contains("$chkIncludeHeatingEquipmentReport"))
                {
                    includeHeatingEquipmentReport = true;
                }
                else if (key.Contains("$chkIncludeCoolingEquipmentReport"))
                {
                    includeCoolingEquipmentReport = true;
                }
                else if (key.Contains("$chkIncludePlantEquipmentReport"))
                {
                    includePlantEquipmentReport = true;
                }
                else if (key.Contains("$chkIncludeZoneEquipmentReport"))
                {
                    includeZoneEquipmentReport = true;
                }
                else if (key.Contains("$chkIncludeSpecialtyEquipmentReport"))
                {
                    includeSpecialtyEquipmentReport = true;
                }
                else if (key.Contains("$ddlTopVentilation"))
                {
                    topVentilation = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$ddlTopHeating"))
                {
                    topHeating = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$dllTopCooling"))
                {
                    topCooling = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$ddlTopPlant"))
                {
                    topPlant = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$ddlTopZone"))
                {
                    topZone = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$ddlTopSpecialty"))
                {
                    topSpecialty = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$chkIncludePerformanceReport"))
                {
                    includePerformanceReport = true;
                }
                else if (key.Contains("$chkIncludeAlarmReport"))
                {
                    includeAlarmReport = true;
                }
                else if (key.Contains("$chkIncludeProjectReport"))
                {
                    includeProjectReport = true;
                }
                else if (key.Contains("$ddlTopProjects"))
                {
                    topProjects = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$chkIncludeTaskReport"))
                {
                    includeTaskReport = true;
                }
                else if (key.Contains("$ddlTopTasks"))
                {
                    topTasks = Convert.ToInt32(Request.Form[key]);
                }
                else if (key.Contains("$chkIncludeTopDiagnosticsDetails"))
                {
                    includeTopDiagnosticsDetails = true;
                }
                else if (key.Contains("$chkIncludeTopDiagnosticsFigures"))
                {
                    includeTopDiagnosticsFigures = true;
                }
                //else if (key.Contains("$chkIncludeHiddenIssues"))
                //{
                //    includeHiddenIssues = true;
                //}
                //user title
                else if (key.Contains("$txtUserTitle"))
                {
                    userTitle = Request.Form[key];
                }
                //automated summary
                else if (key.Contains("$editorAutomatedSummary"))
                {
                    automatedSummary = Request.Form[key];
                }
                //expert summary
                else if (key.Contains("$editorExpertSummary"))
                {
                    expertSummary = Request.Form[key];
                }
                //energy                                
                else if (key.Contains("$editorEnergyTrend"))
                {
                    energy = Request.Form[key];
                }
                //maintenance
                else if (key.Contains("$editorMaintenanceTrend"))
                {
                    maintenance = Request.Form[key];
                }
                //comfort
                else if (key.Contains("$editorComfortTrend"))
                {
                    comfort = Request.Form[key];
                }
            }
        }

        private void GetQueryStringData()
        {
            foreach (string key in Request.QueryString.AllKeys)
            {
                if (String.IsNullOrEmpty(key)) continue;

                //previous start date
                if (key.Contains("PreviousStartDate"))
                {
                    temppsd = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                //previous end date
                else if (key.Contains("PreviousEndDate"))
                {
                    tempped = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                //start date
                else if (key.Contains("StartDate"))
                {
                    tempsd = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                //end date
                else if (key.Contains("EndDate"))
                {
                    temped = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                //threshold
                //else if (key.Contains("$ddlThreshold"))
                //{
                //    th = Convert.ToInt32(HttpUtility.HtmlDecode(Request.Form[key]);
                //}
                //cid 
                else if (key.Contains("CID"))
                {
                    cid = Convert.ToInt32(Request.QueryString[key]);
                }
                //uid
                else if (key.Contains("UID"))
                {
                    uid = Convert.ToInt32(Request.QueryString[key]);
                }
                //oid
                else if (key.Contains("OID"))
                {
                    oid = Convert.ToInt32(Request.QueryString[key]);
                }
                //user culture name
                else if (key.Contains("UserCulture"))
                {
                    userCulture = Request.QueryString[key];
                }
                //building lb
                else if (key.Contains("Buildings"))
                {
                    bids = HttpUtility.HtmlDecode(Request.QueryString[key]).Split(',').Select(x => Int32.Parse(x)).ToList();
                }
                //theme ddl
                else if (key.Contains("Theme"))
                {
                    isSETheme = Convert.ToBoolean(Convert.ToInt32(Request.QueryString[key]));
                }
                //language ddl
                else if (key.Contains("Language"))
                {
                    languageCultureName = Request.QueryString[key];
                }
                //culture ddl
                else if (key.Contains("Culture"))
                {
                    cultureFormat = CultureHelper.GetCultureName(Convert.ToInt32(Request.QueryString[key]));
                }
                //client logos
                else if (key.Contains("ShowClientLogos"))
                {
                    //form post will post value as "on" only if it is selected, otherwise it wont be in the request.
                    showClientLogos = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeCoverPage"))
                {
                    includeCoverPage = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeContentsPage"))
                {
                    includeContentsPage = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeBuildingSummaryReport"))
                {
                    includeBuildingSummaryReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeEquipmentClassSummaryReport"))
                {
                    includeEquipmentClassSummaryReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeBuildingTrendsSummaryReport"))
                {
                    includeBuildingTrendsSummaryReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeBuildingTopIssuesSummaryReport"))
                {
                    includeBuildingTopIssuesSummaryReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeVentilationEquipmentReport"))
                {
                    includeVentilationEquipmentReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeHeatingEquipmentReport"))
                {
                    includeHeatingEquipmentReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeCoolingEquipmentReport"))
                {
                    includeCoolingEquipmentReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludePlantEquipmentReport"))
                {
                    includePlantEquipmentReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeZoneEquipmentReport"))
                {
                    includeZoneEquipmentReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeSpecialtyEquipmentReport"))
                {
                    includeSpecialtyEquipmentReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("TopVentilation"))
                {
                    topVentilation = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("TopHeating"))
                {
                    topHeating = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("TopCooling"))
                {
                    topCooling = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("TopPlant"))
                {
                    topPlant = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("TopZone"))
                {
                    topZone = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("TopSpecialty"))
                {
                    topSpecialty = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("IncludePerformanceReport"))
                {
                    includePerformanceReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeAlarmReport"))
                {
                    includeAlarmReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeProjectReport"))
                {
                    includeProjectReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("TopProjects"))
                {
                    topProjects = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeTaskReport"))
                {
                    includeTaskReport = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("TopTasks"))
                {
                    topTasks = Convert.ToInt32(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeTopDiagnosticsDetails"))
                {
                    includeTopDiagnosticsDetails = Convert.ToBoolean(Request.QueryString[key]);
                }
                else if (key.Contains("IncludeTopDiagnosticsFigures"))
                {
                    includeTopDiagnosticsFigures = Convert.ToBoolean(Request.QueryString[key]);
                }
                //user title
                else if (key.Contains("UserTitle"))
                {
                    userTitle = HttpUtility.HtmlDecode(Request.QueryString[key]);
                }
                else if (key.Contains("IsScheduled"))
                {
                    isScheduled = Convert.ToBoolean(Request.QueryString[key]);
                }
            }
        }

        #endregion

        #region Helper Methods

        private void DownloadButtonActions(string html)
        {
            cid = Convert.ToInt32(hdnCID.Value);
            isSETheme = Convert.ToBoolean(hdnIsSETheme.Value);

            Boolean result = false;
            guid = Guid.NewGuid();

            string tempCoverClientLogo = "", tempBuildingPieChart = "", tempBuildingColumnChart = "", tempEquipmentClassPieChart = "", tempEquipmentClassColumnChart = "",
                tempEnergyChart = "", tempMaintenanceChart = "", tempComfortChart = "", tempEnergyPeriodChart = "", tempMaintenancePeriodChart = "", tempComfortPeriodChart = "",
                tempVentilationAirHandlersPieChart = "", tempVentilationFansPieChart = "", tempVentilationVentilationSytemsPieChart = "", tempVentilationEnergyChart = "",
                tempVentilationMaintenanceChart = "", tempVentilationComfortChart = "", tempVentilationEnergyPeriodChart = "", tempVentilationMaintenancePeriodChart = "",
                tempVentilationComfortPeriodChart = "", tempHeatingBoilersPieChart = "", tempHeatingHeatingSytemsPieChart = "", tempHeatingEnergyChart = "", tempHeatingMaintenanceChart = "",
                tempHeatingComfortChart = "", tempHeatingEnergyPeriodChart = "", tempHeatingMaintenancePeriodChart = "", tempHeatingComfortPeriodChart = "", tempCoolingChillersPieChart = "",
                tempCoolingCoolingSytemsPieChart = "", tempCoolingHeatRejectionPieChart = "", tempCoolingEnergyChart = "", tempCoolingMaintenanceChart = "", tempCoolingComfortChart = "",
                tempCoolingEnergyPeriodChart = "", tempCoolingMaintenancePeriodChart = "", tempCoolingComfortPeriodChart = "", tempPlantDomesticWaterSystemsPieChart = "",
                tempPlantDualTemperatureWaterSystemsPieChart = "", tempPlantHeatExchangerPieChart = "", tempPlantHeatRecoverySystemsPieChart = "", tempPlantPumpsPieChart = "",
                tempPlantStoragePieChart = "", tempPlantEnergyChart = "", tempPlantMaintenanceChart = "", tempPlantComfortChart = "", tempPlantEnergyPeriodChart = "", tempPlantMaintenancePeriodChart = "",
                tempPlantComfortPeriodChart = "", tempZoneZoneEquipmentPieChart = "", tempZoneZoneGroupsPieChart = "", tempZoneEnergyChart = "", tempZoneMaintenanceChart = "", tempZoneComfortChart = "",
                tempZoneEnergyPeriodChart = "", tempZoneMaintenancePeriodChart = "", tempZoneComfortPeriodChart = "", tempSpecialtySpecialtyEquipmentPieChart = "", tempSpecialtyEnergyChart = "",
                tempSpecialtyMaintenanceChart = "", tempSpecialtyComfortChart = "", tempSpecialtyEnergyPeriodChart = "", tempSpecialtyMaintenancePeriodChart = "", tempSpecialtyComfortPeriodChart = "";

            string[] tempTopDiagnosticFigures = new string[15];


                tempCoverClientLogo = Server.MapPath("\\tempCustomReports\\bureau_coverclientlogo_" + guid + ".png");

                tempBuildingPieChart = Server.MapPath("\\tempCustomReports\\bureau_buildingpie_" + guid + ".png");
                tempBuildingColumnChart = Server.MapPath("\\tempCustomReports\\bureau_buildingcolumn_" + guid + ".png");
                tempEquipmentClassPieChart = Server.MapPath("\\tempCustomReports\\bureau_equipmentclasspie_" + guid + ".png");
                tempEquipmentClassColumnChart = Server.MapPath("\\tempCustomReports\\bureau_equipmentclasscolumn_" + guid + ".png");

                tempEnergyChart = Server.MapPath("\\tempCustomReports\\bureau_energy_" + guid + ".png");
                tempMaintenanceChart = Server.MapPath("\\tempCustomReports\\bureau_maintenance_" + guid + ".png");
                tempComfortChart = Server.MapPath("\\tempCustomReports\\bureau_comfort_" + guid + ".png");
                tempEnergyPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_energyperiod_" + guid + ".png");
                tempMaintenancePeriodChart = Server.MapPath("\\tempCustomReports\\bureau_maintenanceperiod_" + guid + ".png");
                tempComfortPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_comfortperiod_" + guid + ".png");

                //ventilation
                tempVentilationAirHandlersPieChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationairhandlerspie_" + guid + ".png");
                tempVentilationFansPieChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationfanspie_" + guid + ".png");
                tempVentilationVentilationSytemsPieChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationventilationsystemspie_" + guid + ".png");
                tempVentilationEnergyChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationenergy_" + guid + ".png");
                tempVentilationMaintenanceChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationmaintenance_" + guid + ".png");
                tempVentilationComfortChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationcomfort_" + guid + ".png");
                tempVentilationEnergyPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationenergyperiod_" + guid + ".png");
                tempVentilationMaintenancePeriodChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationmaintenanceperiod_" + guid + ".png");
                tempVentilationComfortPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_ventilationcomfortperiod_" + guid + ".png");

                //heating
                tempHeatingBoilersPieChart = Server.MapPath("\\tempCustomReports\\bureau_heatingboilerspie_" + guid + ".png");
                tempHeatingHeatingSytemsPieChart = Server.MapPath("\\tempCustomReports\\bureau_heatingheatingsystemspie_" + guid + ".png");
                tempHeatingEnergyChart = Server.MapPath("\\tempCustomReports\\bureau_heatingenergy_" + guid + ".png");
                tempHeatingMaintenanceChart = Server.MapPath("\\tempCustomReports\\bureau_heatingmaintenance_" + guid + ".png");
                tempHeatingComfortChart = Server.MapPath("\\tempCustomReports\\bureau_heatingcomfort_" + guid + ".png");
                tempHeatingEnergyPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_heatingenergyperiod_" + guid + ".png");
                tempHeatingMaintenancePeriodChart = Server.MapPath("\\tempCustomReports\\bureau_heatingmaintenanceperiod_" + guid + ".png");
                tempHeatingComfortPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_heatingcomfortperiod_" + guid + ".png");

                //cooling
                tempCoolingChillersPieChart = Server.MapPath("\\tempCustomReports\\bureau_coolingchillerspie_" + guid + ".png");
                tempCoolingCoolingSytemsPieChart = Server.MapPath("\\tempCustomReports\\bureau_coolingcoolingsystemspie_" + guid + ".png");
                tempCoolingHeatRejectionPieChart = Server.MapPath("\\tempCustomReports\\bureau_coolingheatrejectionpie_" + guid + ".png");
                tempCoolingEnergyChart = Server.MapPath("\\tempCustomReports\\bureau_coolingenergy_" + guid + ".png");
                tempCoolingMaintenanceChart = Server.MapPath("\\tempCustomReports\\bureau_coolingmaintenance_" + guid + ".png");
                tempCoolingComfortChart = Server.MapPath("\\tempCustomReports\\bureau_coolingcomfort_" + guid + ".png");
                tempCoolingEnergyPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_coolingenergyperiod_" + guid + ".png");
                tempCoolingMaintenancePeriodChart = Server.MapPath("\\tempCustomReports\\bureau_coolingmaintenanceperiod_" + guid + ".png");
                tempCoolingComfortPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_coolingcomfortperiod_" + guid + ".png");

                //plant
                tempPlantDomesticWaterSystemsPieChart = Server.MapPath("\\tempCustomReports\\bureau_plantdomesticwatersystemspie_" + guid + ".png");
                tempPlantDualTemperatureWaterSystemsPieChart = Server.MapPath("\\tempCustomReports\\bureau_plantdualtemperaturewatersystemspie_" + guid + ".png");
                tempPlantHeatExchangerPieChart = Server.MapPath("\\tempCustomReports\\bureau_plantheatexchangerpie_" + guid + ".png");
                tempPlantHeatRecoverySystemsPieChart = Server.MapPath("\\tempCustomReports\\bureau_plantheatrecoverysystemspie_" + guid + ".png");
                tempPlantPumpsPieChart = Server.MapPath("\\tempCustomReports\\bureau_plantpumpspie_" + guid + ".png");
                tempPlantStoragePieChart = Server.MapPath("\\tempCustomReports\\bureau_plantstoragepie_" + guid + ".png");
                tempPlantEnergyChart = Server.MapPath("\\tempCustomReports\\bureau_plantenergy_" + guid + ".png");
                tempPlantMaintenanceChart = Server.MapPath("\\tempCustomReports\\bureau_plantmaintenance_" + guid + ".png");
                tempPlantComfortChart = Server.MapPath("\\tempCustomReports\\bureau_plantcomfort_" + guid + ".png");
                tempPlantEnergyPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_plantenergyperiod_" + guid + ".png");
                tempPlantMaintenancePeriodChart = Server.MapPath("\\tempCustomReports\\bureau_plantmaintenanceperiod_" + guid + ".png");
                tempPlantComfortPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_plantcomfortperiod_" + guid + ".png");

                //zone
                tempZoneZoneEquipmentPieChart = Server.MapPath("\\tempCustomReports\\bureau_zonezoneequipmentpie_" + guid + ".png");
                tempZoneZoneGroupsPieChart = Server.MapPath("\\tempCustomReports\\bureau_zonezonegroupspie_" + guid + ".png");
                tempZoneEnergyChart = Server.MapPath("\\tempCustomReports\\bureau_zoneenergy_" + guid + ".png");
                tempZoneMaintenanceChart = Server.MapPath("\\tempCustomReports\\bureau_zonemaintenance_" + guid + ".png");
                tempZoneComfortChart = Server.MapPath("\\tempCustomReports\\bureau_zonecomfort_" + guid + ".png");
                tempZoneEnergyPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_zoneenergyperiod_" + guid + ".png");
                tempZoneMaintenancePeriodChart = Server.MapPath("\\tempCustomReports\\bureau_zonemaintenanceperiod_" + guid + ".png");
                tempZoneComfortPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_zonecomfortperiod_" + guid + ".png");

                //specialty
                tempSpecialtySpecialtyEquipmentPieChart = Server.MapPath("\\tempCustomReports\\bureau_specialtyspecialtyequipmentpie_" + guid + ".png");
                tempSpecialtyEnergyChart = Server.MapPath("\\tempCustomReports\\bureau_specialtyenergy_" + guid + ".png");
                tempSpecialtyMaintenanceChart = Server.MapPath("\\tempCustomReports\\bureau_specialtymaintenance_" + guid + ".png");
                tempSpecialtyComfortChart = Server.MapPath("\\tempCustomReports\\bureau_specialtycomfort_" + guid + ".png");
                tempSpecialtyEnergyPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_specialtyenergyperiod_" + guid + ".png");
                tempSpecialtyMaintenancePeriodChart = Server.MapPath("\\tempCustomReports\\bureau_specialtymaintenanceperiod_" + guid + ".png");
                tempSpecialtyComfortPeriodChart = Server.MapPath("\\tempCustomReports\\bureau_specialtycomfortperiod_" + guid + ".png");

                //top diagnostic figures                
                for (int i = 0; i < 15; i++)
                {
                    tempTopDiagnosticFigures[i] = Server.MapPath("\\tempCustomReports\\bureau_topdiagnosticfigure" + (i + 1).ToString() + "_" + guid + ".png");
                }
            

            //Essential Objects-----------
            EssentialObjectsHelper eo = new EssentialObjectsHelper(invisibleElementIds, Logger);
            
            //Instanciate
            PdfDocument pdf = new PdfDocument();
            MemoryStream ms = new MemoryStream();

            //Attachments
            //PdfAttachment attachment = new PdfAttachment(Server.MapPath("\\_assets\\images\\no-building-image.png"));
            //pdf.Attachments.Add(attachment);
            //pdf.Portfolio.InitialView = PdfPortfolioView.Detail;
            //byte[]  bt = mDataManager.FigureAndNoteDataMapper.GetFigureBlobImage(DataConstants.AnalysisRange.Weekly, 26, 15, DateTimeHelper.GetLastSunday(DateTime.UtcNow.AddDays(-7)), ".png");
            //PdfAttachment attachment = new PdfAttachment("test1", bt, "");
            //pdf.Attachments.Add(attachment);
            //attachment = new PdfAttachment("test2", bt, "image/png");
            //pdf.Attachments.Add(attachment);
            //pdf.Portfolio.InitialView = PdfPortfolioView.Detail;

            //client image
            CustomReportsFileHelper.SaveFile(tempCoverClientLogo, hdnShowClientLogos, hdnCoverClientByteArray);

            //Get charts to write out locally
            CustomReportsFileHelper.SaveFile(tempBuildingPieChart, divBuildingSummaryPage, buildingSummaryPieChart);
            CustomReportsFileHelper.SaveFile(tempBuildingColumnChart, divBuildingSummaryPage, buildingSummaryStackedColumnChart);

            CustomReportsFileHelper.SaveFile(tempEquipmentClassPieChart, divEquipmentClassSummaryPage, equipmentClassSummaryPieChart);
            CustomReportsFileHelper.SaveFile(tempEquipmentClassColumnChart, divEquipmentClassSummaryPage, equipmentClassSummaryStackedColumnChart);

            CustomReportsFileHelper.SaveFile(tempEnergyChart, divBuildingTrendsPage, energyChart);
            CustomReportsFileHelper.SaveFile(tempMaintenanceChart, divBuildingTrendsPage, maintenanceChart);
            CustomReportsFileHelper.SaveFile(tempComfortChart, divBuildingTrendsPage, comfortChart);
            CustomReportsFileHelper.SaveFile(tempEnergyPeriodChart, divBuildingTrendsPage, energyPeriodChart);
            CustomReportsFileHelper.SaveFile(tempMaintenancePeriodChart, divBuildingTrendsPage, maintenancePeriodChart);
            CustomReportsFileHelper.SaveFile(tempComfortPeriodChart, divBuildingTrendsPage, comfortPeriodChart);

            CustomReportsFileHelper.SaveFile(tempVentilationAirHandlersPieChart, divVentilationSummaryPage, ventilationSummaryAirHandlersPieChart);
            CustomReportsFileHelper.SaveFile(tempVentilationFansPieChart, divVentilationSummaryPage, ventilationSummaryFansPieChart);
            CustomReportsFileHelper.SaveFile(tempVentilationVentilationSytemsPieChart, divVentilationSummaryPage, ventilationSummaryVentilationSystemsPieChart);
            CustomReportsFileHelper.SaveFile(tempVentilationEnergyChart, divVentilationSummaryPage, ventilationEnergyChart);
            CustomReportsFileHelper.SaveFile(tempVentilationMaintenanceChart, divVentilationSummaryPage, ventilationMaintenanceChart);
            CustomReportsFileHelper.SaveFile(tempVentilationComfortChart, divVentilationSummaryPage, ventilationComfortChart);
            CustomReportsFileHelper.SaveFile(tempVentilationEnergyPeriodChart, divVentilationSummaryPage, ventilationEnergyPeriodChart);
            CustomReportsFileHelper.SaveFile(tempVentilationMaintenancePeriodChart, divVentilationSummaryPage, ventilationMaintenancePeriodChart);
            CustomReportsFileHelper.SaveFile(tempVentilationComfortPeriodChart, divVentilationSummaryPage, ventilationComfortPeriodChart);

            CustomReportsFileHelper.SaveFile(tempHeatingBoilersPieChart, divHeatingSummaryPage, heatingSummaryBoilersPieChart);
            CustomReportsFileHelper.SaveFile(tempHeatingHeatingSytemsPieChart, divHeatingSummaryPage, heatingSummaryHeatingSystemsPieChart);
            CustomReportsFileHelper.SaveFile(tempHeatingEnergyChart, divHeatingSummaryPage, heatingEnergyChart);
            CustomReportsFileHelper.SaveFile(tempHeatingMaintenanceChart, divHeatingSummaryPage, heatingMaintenanceChart);
            CustomReportsFileHelper.SaveFile(tempHeatingComfortChart, divHeatingSummaryPage, heatingComfortChart);
            CustomReportsFileHelper.SaveFile(tempHeatingEnergyPeriodChart, divHeatingSummaryPage, heatingEnergyPeriodChart);
            CustomReportsFileHelper.SaveFile(tempHeatingMaintenancePeriodChart, divHeatingSummaryPage, heatingMaintenancePeriodChart);
            CustomReportsFileHelper.SaveFile(tempHeatingComfortPeriodChart, divHeatingSummaryPage, heatingComfortPeriodChart);

            CustomReportsFileHelper.SaveFile(tempCoolingChillersPieChart, divCoolingSummaryPage, coolingSummaryChillersPieChart);
            CustomReportsFileHelper.SaveFile(tempCoolingCoolingSytemsPieChart, divCoolingSummaryPage, coolingSummaryCoolingSystemsPieChart);
            CustomReportsFileHelper.SaveFile(tempCoolingHeatRejectionPieChart, divCoolingSummaryPage, coolingSummaryHeatRejectionPieChart);
            CustomReportsFileHelper.SaveFile(tempCoolingEnergyChart, divCoolingSummaryPage, coolingEnergyChart);
            CustomReportsFileHelper.SaveFile(tempCoolingMaintenanceChart, divCoolingSummaryPage, coolingMaintenanceChart);
            CustomReportsFileHelper.SaveFile(tempCoolingComfortChart, divCoolingSummaryPage, coolingComfortChart);
            CustomReportsFileHelper.SaveFile(tempCoolingEnergyPeriodChart, divCoolingSummaryPage, coolingEnergyPeriodChart);
            CustomReportsFileHelper.SaveFile(tempCoolingMaintenancePeriodChart, divCoolingSummaryPage, coolingMaintenancePeriodChart);
            CustomReportsFileHelper.SaveFile(tempCoolingComfortPeriodChart, divCoolingSummaryPage, coolingComfortPeriodChart);

            CustomReportsFileHelper.SaveFile(tempPlantDomesticWaterSystemsPieChart, divPlantSummaryPage, plantSummaryDomesticWaterSystemsPieChart);
            CustomReportsFileHelper.SaveFile(tempPlantDualTemperatureWaterSystemsPieChart, divPlantSummaryPage, plantSummaryDualTemperatureWaterSystemsPieChart);
            CustomReportsFileHelper.SaveFile(tempPlantHeatExchangerPieChart, divPlantSummaryPage, plantSummaryHeatExchangerPieChart);
            CustomReportsFileHelper.SaveFile(tempPlantHeatRecoverySystemsPieChart, divPlantSummaryPage, plantSummaryHeatRecoverySystemsPieChart);
            CustomReportsFileHelper.SaveFile(tempPlantPumpsPieChart, divPlantSummaryPage, plantSummaryPumpsPieChart);
            CustomReportsFileHelper.SaveFile(tempPlantStoragePieChart, divPlantSummaryPage, plantSummaryStoragePieChart);
            CustomReportsFileHelper.SaveFile(tempPlantEnergyChart, divPlantSummaryPage, plantEnergyChart);
            CustomReportsFileHelper.SaveFile(tempPlantMaintenanceChart, divPlantSummaryPage, plantMaintenanceChart);
            CustomReportsFileHelper.SaveFile(tempPlantComfortChart, divPlantSummaryPage, plantComfortChart);
            CustomReportsFileHelper.SaveFile(tempPlantEnergyPeriodChart, divPlantSummaryPage, plantEnergyPeriodChart);
            CustomReportsFileHelper.SaveFile(tempPlantMaintenancePeriodChart, divPlantSummaryPage, plantMaintenancePeriodChart);
            CustomReportsFileHelper.SaveFile(tempPlantComfortPeriodChart, divPlantSummaryPage, plantComfortPeriodChart);

            CustomReportsFileHelper.SaveFile(tempZoneZoneEquipmentPieChart, divZoneSummaryPage, zoneSummaryZoneEquipmentPieChart);
            CustomReportsFileHelper.SaveFile(tempZoneZoneGroupsPieChart, divZoneSummaryPage, zoneSummaryZoneGroupsPieChart);
            CustomReportsFileHelper.SaveFile(tempZoneEnergyChart, divZoneSummaryPage, zoneEnergyChart);
            CustomReportsFileHelper.SaveFile(tempZoneMaintenanceChart, divZoneSummaryPage, zoneMaintenanceChart);
            CustomReportsFileHelper.SaveFile(tempZoneComfortChart, divZoneSummaryPage, zoneComfortChart);
            CustomReportsFileHelper.SaveFile(tempZoneEnergyPeriodChart, divZoneSummaryPage, zoneEnergyPeriodChart);
            CustomReportsFileHelper.SaveFile(tempZoneMaintenancePeriodChart, divZoneSummaryPage, zoneMaintenancePeriodChart);
            CustomReportsFileHelper.SaveFile(tempZoneComfortPeriodChart, divZoneSummaryPage, zoneComfortPeriodChart);

            CustomReportsFileHelper.SaveFile(tempSpecialtySpecialtyEquipmentPieChart, divSpecialtySummaryPage, specialtySummarySpecialtyEquipmentPieChart);
            CustomReportsFileHelper.SaveFile(tempSpecialtyEnergyChart, divSpecialtySummaryPage, specialtyEnergyChart);
            CustomReportsFileHelper.SaveFile(tempSpecialtyMaintenanceChart, divSpecialtySummaryPage, specialtyMaintenanceChart);
            CustomReportsFileHelper.SaveFile(tempSpecialtyComfortChart, divSpecialtySummaryPage, specialtyComfortChart);
            CustomReportsFileHelper.SaveFile(tempSpecialtyEnergyPeriodChart, divSpecialtySummaryPage, specialtyEnergyPeriodChart);
            CustomReportsFileHelper.SaveFile(tempSpecialtyMaintenancePeriodChart, divSpecialtySummaryPage, specialtyMaintenancePeriodChart);
            CustomReportsFileHelper.SaveFile(tempSpecialtyComfortPeriodChart, divSpecialtySummaryPage, specialtyComfortPeriodChart);

            int topDiagnosticCounter = 0;
            foreach (RepeaterItem item in rptTopDiagnostics.Items)
            {
                foreach (Control c in item.Controls)
                {
                    //if (c is RadBinaryImage && topDiagnosticCounter < 15)
                    //{
                    //    RadBinaryImage img = (RadBinaryImage)c;

                    //    CustomReportsFileHelper.SaveFile(tempTopDiagnosticFigures[topDiagnosticCounter], hdnIncludeTopDiagnosticsFigures, img);

                    //    topDiagnosticCounter++;
                    //}
                    if (c is HiddenField && topDiagnosticCounter < 15)
                    {
                        CustomReportsFileHelper.SaveFile(tempTopDiagnosticFigures[topDiagnosticCounter], hdnIncludeTopDiagnosticsFigures, (HiddenField)c);

                        topDiagnosticCounter++;
                    }
                }
            }

            html = ReplacePaths(html);


            //User Button Convert
            HtmlToPdfResult htmlToPdfResult = HtmlToPdf.ConvertHtml(html, pdf, eo.options);

            //Set Bookmarks and Anchors
            //NOTE: div and lbl id's for this are hardcoded in the BureauReportNotificationProcessor
            List<Tuple<string, string>> htmlBookmarkTags = new List<Tuple<string, string>>();
            List<Tuple<string, string>> htmlAnchorTags = new List<Tuple<string, string>>();

            //Order matters        
            htmlBookmarkTags.Add(Tuple.Create(divPortfolioSummaryPage.ID, lblPortfolioSummaryPage.ID)); //use label for bookmark for portfolio, the title doenst contain all content.
            htmlAnchorTags.Add(Tuple.Create(lblPortfolioSummaryPage.ID, divPortfolioSummaryPage.ID));

            if (divExpertSummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divExpertSummaryPage.ID, lblExpertSummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblExpertSummaryPage.ID, divExpertSummaryPage.ID));
            }
            if (divBuildingSummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divBuildingSummaryPage.ID, lblBuildingSummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblBuildingSummaryPage.ID, divBuildingSummaryPage.ID));
            }
            if (divEquipmentClassSummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divEquipmentClassSummaryPage.ID, lblEquipmentClassSummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblEquipmentClassSummaryPage.ID, divEquipmentClassSummaryPage.ID));
            }
            if (divBuildingTrendsPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divBuildingTrendsPage.ID, lblBuildingTrendsPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblBuildingTrendsPage.ID, divBuildingTrendsPage.ID));
            }
            if (divBuildingTopIssuesPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divBuildingTopIssuesPage.ID, lblBuildingTopIssuesPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblBuildingTopIssuesPage.ID, divBuildingTopIssuesPage.ID));
            }
            if (divVentilationSummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divVentilationSummaryPage.ID, lblVentilationSummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblVentilationSummaryPage.ID, divVentilationSummaryPage.ID));
            }
            if (divHeatingSummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divHeatingSummaryPage.ID, lblHeatingSummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblHeatingSummaryPage.ID, divHeatingSummaryPage.ID));
            }
            if (divCoolingSummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divCoolingSummaryPage.ID, lblCoolingSummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblCoolingSummaryPage.ID, divCoolingSummaryPage.ID));
            }
            if (divPlantSummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divPlantSummaryPage.ID, lblPlantSummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblPlantSummaryPage.ID, divPlantSummaryPage.ID));
            }
            if (divZoneSummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divZoneSummaryPage.ID, lblZoneSummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblZoneSummaryPage.ID, divZoneSummaryPage.ID));
            }
            if (divSpecialtySummaryPage.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divSpecialtySummaryPage.ID, lblSpecialtySummaryPage.ID));
                htmlAnchorTags.Add(Tuple.Create(lblSpecialtySummaryPage.ID, divSpecialtySummaryPage.ID));
            }
            //if (divPerformance.Visible)
            if (divAlarms.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divAlarms.ID, lblAlarms.ID));
                htmlAnchorTags.Add(Tuple.Create(lblAlarms.ID, divAlarms.ID));
            }
            if (divProjects.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divProjects.ID, lblProjects.ID));
                htmlAnchorTags.Add(Tuple.Create(lblProjects.ID, divProjects.ID));
            }
            if (divTasks.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divTasks.ID, lblTasks.ID));
                htmlAnchorTags.Add(Tuple.Create(lblTasks.ID, divTasks.ID));
            }
            if (divTopDiagnostics.Visible)
            {
                htmlBookmarkTags.Add(Tuple.Create(divTopDiagnostics.ID, lblTopDiagnostics.ID));
                htmlAnchorTags.Add(Tuple.Create(lblTopDiagnostics.ID, divTopDiagnostics.ID));
            }

            //Generate bookmarks and anchors from set
            eo.GenerateBookmarks(htmlToPdfResult, pdf, htmlBookmarkTags);

            if (divContentsPage.Visible)
                eo.GenerateAnchors(htmlToPdfResult, htmlAnchorTags);

            //Save 
            pdf.Save(ms);


            //byte[] pdfBytes = ms.GetBuffer();
            byte[] pdfBytes = ms.ToArray();

            try
            {
                result = new FilePublishService
                    (
                        mDataManager.ClientDataMapper.UpdateClientAccountSettings,
                        mDataManager.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                        siteUser.IsKGSFullAdminOrHigher,
                        siteUser.IsSchneiderTheme,
                        siteUser.Email,
                        new CustomReportFileGenerator(pdfBytes),
                        Logger
                    ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(isSETheme, "$productBureauReport.pdf", true));
            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                LabelHelper.SetLabelMessage(lblError, "Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                return;
            }

            //clean up temp files
            CustomReportsFileHelper.DeleteFile(tempCoverClientLogo, hdnShowClientLogos);

            CustomReportsFileHelper.DeleteFile(tempBuildingPieChart, divBuildingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempBuildingColumnChart, divBuildingSummaryPage);

            CustomReportsFileHelper.DeleteFile(tempEquipmentClassPieChart, divEquipmentClassSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempEquipmentClassColumnChart, divEquipmentClassSummaryPage);

            CustomReportsFileHelper.DeleteFile(tempEnergyChart, divBuildingTrendsPage);
            CustomReportsFileHelper.DeleteFile(tempMaintenanceChart, divBuildingTrendsPage);
            CustomReportsFileHelper.DeleteFile(tempComfortChart, divBuildingTrendsPage);
            CustomReportsFileHelper.DeleteFile(tempEnergyPeriodChart, divBuildingTrendsPage);
            CustomReportsFileHelper.DeleteFile(tempMaintenancePeriodChart, divBuildingTrendsPage);
            CustomReportsFileHelper.DeleteFile(tempComfortPeriodChart, divBuildingTrendsPage);

            CustomReportsFileHelper.DeleteFile(tempVentilationAirHandlersPieChart, divVentilationSummaryPage, ventilationSummaryAirHandlersPieChart);
            CustomReportsFileHelper.DeleteFile(tempVentilationFansPieChart, divVentilationSummaryPage, ventilationSummaryFansPieChart);
            CustomReportsFileHelper.DeleteFile(tempVentilationVentilationSytemsPieChart, divVentilationSummaryPage, ventilationSummaryVentilationSystemsPieChart);
            CustomReportsFileHelper.DeleteFile(tempVentilationEnergyChart, divVentilationSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempVentilationMaintenanceChart, divVentilationSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempVentilationComfortChart, divVentilationSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempVentilationEnergyPeriodChart, divVentilationSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempVentilationMaintenancePeriodChart, divVentilationSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempVentilationComfortPeriodChart, divVentilationSummaryPage);

            CustomReportsFileHelper.DeleteFile(tempHeatingBoilersPieChart, divHeatingSummaryPage, heatingSummaryBoilersPieChart);
            CustomReportsFileHelper.DeleteFile(tempHeatingHeatingSytemsPieChart, divHeatingSummaryPage, heatingSummaryHeatingSystemsPieChart);
            CustomReportsFileHelper.DeleteFile(tempHeatingEnergyChart, divHeatingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempHeatingMaintenanceChart, divHeatingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempHeatingComfortChart, divHeatingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempHeatingEnergyPeriodChart, divHeatingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempHeatingMaintenancePeriodChart, divHeatingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempHeatingComfortPeriodChart, divHeatingSummaryPage);

            CustomReportsFileHelper.DeleteFile(tempCoolingChillersPieChart, divCoolingSummaryPage, coolingSummaryChillersPieChart);
            CustomReportsFileHelper.DeleteFile(tempCoolingCoolingSytemsPieChart, divCoolingSummaryPage, coolingSummaryCoolingSystemsPieChart);
            CustomReportsFileHelper.DeleteFile(tempCoolingHeatRejectionPieChart, divCoolingSummaryPage, coolingSummaryHeatRejectionPieChart);
            CustomReportsFileHelper.DeleteFile(tempCoolingEnergyChart, divCoolingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempCoolingMaintenanceChart, divCoolingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempCoolingComfortChart, divCoolingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempCoolingEnergyPeriodChart, divCoolingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempCoolingMaintenancePeriodChart, divCoolingSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempCoolingComfortPeriodChart, divCoolingSummaryPage);

            CustomReportsFileHelper.DeleteFile(tempPlantDomesticWaterSystemsPieChart, divPlantSummaryPage, plantSummaryDomesticWaterSystemsPieChart);
            CustomReportsFileHelper.DeleteFile(tempPlantDualTemperatureWaterSystemsPieChart, divPlantSummaryPage, plantSummaryDualTemperatureWaterSystemsPieChart);
            CustomReportsFileHelper.DeleteFile(tempPlantHeatExchangerPieChart, divPlantSummaryPage, plantSummaryHeatExchangerPieChart);
            CustomReportsFileHelper.DeleteFile(tempPlantHeatRecoverySystemsPieChart, divPlantSummaryPage, plantSummaryHeatRecoverySystemsPieChart);
            CustomReportsFileHelper.DeleteFile(tempPlantPumpsPieChart, divPlantSummaryPage, plantSummaryPumpsPieChart);
            CustomReportsFileHelper.DeleteFile(tempPlantStoragePieChart, divPlantSummaryPage, plantSummaryStoragePieChart);
            CustomReportsFileHelper.DeleteFile(tempPlantEnergyChart, divPlantSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempPlantMaintenanceChart, divPlantSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempPlantComfortChart, divPlantSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempPlantEnergyPeriodChart, divPlantSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempPlantMaintenancePeriodChart, divPlantSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempPlantComfortPeriodChart, divPlantSummaryPage);

            CustomReportsFileHelper.DeleteFile(tempZoneZoneEquipmentPieChart, divZoneSummaryPage, zoneSummaryZoneEquipmentPieChart);
            CustomReportsFileHelper.DeleteFile(tempZoneZoneGroupsPieChart, divZoneSummaryPage, zoneSummaryZoneGroupsPieChart);
            CustomReportsFileHelper.DeleteFile(tempZoneEnergyChart, divZoneSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempZoneMaintenanceChart, divZoneSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempZoneComfortChart, divZoneSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempZoneEnergyPeriodChart, divZoneSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempZoneMaintenancePeriodChart, divZoneSummaryPage);
            CustomReportsFileHelper.DeleteFile(tempZoneComfortPeriodChart, divZoneSummaryPage);

            CustomReportsFileHelper.DeleteFile(tempSpecialtySpecialtyEquipmentPieChart, divSpecialtySummaryPage, specialtySummarySpecialtyEquipmentPieChart);
            CustomReportsFileHelper.DeleteFile(tempSpecialtyEnergyChart, divSpecialtySummaryPage);
            CustomReportsFileHelper.DeleteFile(tempSpecialtyMaintenanceChart, divSpecialtySummaryPage);
            CustomReportsFileHelper.DeleteFile(tempSpecialtyComfortChart, divSpecialtySummaryPage);
            CustomReportsFileHelper.DeleteFile(tempSpecialtyEnergyPeriodChart, divSpecialtySummaryPage);
            CustomReportsFileHelper.DeleteFile(tempSpecialtyMaintenancePeriodChart, divSpecialtySummaryPage);
            CustomReportsFileHelper.DeleteFile(tempSpecialtyComfortPeriodChart, divSpecialtySummaryPage);

            for (int i = 0; i < 15; i++)
            {
                CustomReportsFileHelper.DeleteFile(tempTopDiagnosticFigures[i], hdnIncludeTopDiagnosticsFigures);
            }


            //TODO: move to scheduled reports later.
            //Clean up old chart files incase they didnt get deleted on post temp creation. 
            //DirectoryInfo di = new DirectoryInfo(Server.MapPath("\\temp\\"));
            //FileInfo[] files = di.GetFiles();
            //foreach (FileInfo fi in files)
            //{
            //    if (fi.CreationTimeUtc < DateTime.UtcNow.AddHours(-24))
            //        fi.Delete();
            //}


            if (result) return;
        }

        #endregion

        #region PDF Helper Methods

        public string ReplacePaths(string html)
        {
            //set base url to current instance for this request.
            //string baseString = "id=\"baseTag\"";
            //html = html.Replace(baseString, baseString + " " + "href=\"https://" + RoleEnvironment.CurrentRoleInstance.InstanceEndpoints.Values.First().IPEndpoint.ToString() + "/\"");

            string tempCoverClientLogoPath = "src=\"/tempCustomReports/bureau_coverclientlogo_" + guid + ".png\"";

            string tempBuildingPiePath = "src=\"/tempCustomReports/bureau_buildingpie_" + guid + ".png\"";
            string tempBuildingColumnPath = "src=\"/tempCustomReports/bureau_buildingcolumn_" + guid + ".png\"";
            string tempEquipmentClassPiePath = "src=\"/tempCustomReports/bureau_equipmentclasspie_" + guid + ".png\"";
            string tempEquipmentClassColumnPath = "src=\"/tempCustomReports/bureau_equipmentclasscolumn_" + guid + ".png\"";

            string tempEnergyPath = "src=\"/tempCustomReports/bureau_energy_" + guid + ".png\"";
            string tempMaintenancePath = "src=\"/tempCustomReports/bureau_maintenance_" + guid + ".png\"";
            string tempComfortPath = "src=\"/tempCustomReports/bureau_comfort_" + guid + ".png\"";
            string tempEnergyPeriodPath = "src=\"/tempCustomReports/bureau_energyperiod_" + guid + ".png\"";
            string tempMaintenancePeriodPath = "src=\"/tempCustomReports/bureau_maintenanceperiod_" + guid + ".png\"";
            string tempComfortPeriodPath = "src=\"/tempCustomReports/bureau_comfortperiod_" + guid + ".png\"";

            //ventilation
            string tempVentilationAirHandlersPiePath = "src=\"/tempCustomReports/bureau_ventilationairhandlerspie_" + guid + ".png\"";
            string tempVentilationFansPiePath = "src=\"/tempCustomReports/bureau_ventilationfanspie_" + guid + ".png\"";
            string tempVentilationVentilationSystemsPiePath = "src=\"/tempCustomReports/bureau_ventilationventilationsystemspie_" + guid + ".png\"";
            string tempVentilationEnergyPath = "src=\"/tempCustomReports/bureau_ventilationenergy_" + guid + ".png\"";
            string tempVentilationMaintenancePath = "src=\"/tempCustomReports/bureau_ventilationmaintenance_" + guid + ".png\"";
            string tempVentilationComfortPath = "src=\"/tempCustomReports/bureau_ventilationcomfort_" + guid + ".png\"";
            string tempVentilationEnergyPeriodPath = "src=\"/tempCustomReports/bureau_ventilationenergyperiod_" + guid + ".png\"";
            string tempVentilationMaintenancePeriodPath = "src=\"/tempCustomReports/bureau_ventilationmaintenanceperiod_" + guid + ".png\"";
            string tempVentilationComfortPeriodPath = "src=\"/tempCustomReports/bureau_ventilationcomfortperiod_" + guid + ".png\"";

            //heating
            string tempHeatingBoilersPiePath = "src=\"/tempCustomReports/bureau_heatingboilerspie_" + guid + ".png\"";
            string tempHeatingHeatingSystemsPiePath = "src=\"/tempCustomReports/bureau_heatingheatingsystemspie_" + guid + ".png\"";
            string tempHeatingEnergyPath = "src=\"/tempCustomReports/bureau_heatingenergy_" + guid + ".png\"";
            string tempHeatingMaintenancePath = "src=\"/tempCustomReports/bureau_heatingmaintenance_" + guid + ".png\"";
            string tempHeatingComfortPath = "src=\"/tempCustomReports/bureau_heatingcomfort_" + guid + ".png\"";
            string tempHeatingEnergyPeriodPath = "src=\"/tempCustomReports/bureau_heatingenergyperiod_" + guid + ".png\"";
            string tempHeatingMaintenancePeriodPath = "src=\"/tempCustomReports/bureau_heatingmaintenanceperiod_" + guid + ".png\"";
            string tempHeatingComfortPeriodPath = "src=\"/tempCustomReports/bureau_heatingcomfortperiod_" + guid + ".png\"";

            //cooling
            string tempCoolingChillersPiePath = "src=\"/tempCustomReports/bureau_coolingchillerspie_" + guid + ".png\"";
            string tempCoolingCoolingSystemsPiePath = "src=\"/tempCustomReports/bureau_coolingcoolingsystemspie_" + guid + ".png\"";
            string tempCoolingHeatRejectionPiePath = "src=\"/tempCustomReports/bureau_coolingheatrejectionpie_" + guid + ".png\"";
            string tempCoolingEnergyPath = "src=\"/tempCustomReports/bureau_coolingenergy_" + guid + ".png\"";
            string tempCoolingMaintenancePath = "src=\"/tempCustomReports/bureau_coolingmaintenance_" + guid + ".png\"";
            string tempCoolingComfortPath = "src=\"/tempCustomReports/bureau_coolingcomfort_" + guid + ".png\"";
            string tempCoolingEnergyPeriodPath = "src=\"/tempCustomReports/bureau_coolingenergyperiod_" + guid + ".png\"";
            string tempCoolingMaintenancePeriodPath = "src=\"/tempCustomReports/bureau_coolingmaintenanceperiod_" + guid + ".png\"";
            string tempCoolingComfortPeriodPath = "src=\"/tempCustomReports/bureau_coolingcomfortperiod_" + guid + ".png\"";

            //plant
            string tempPlantDomesticWaterSystemsPiePath = "src=\"/tempCustomReports/bureau_plantdomesticwatersystemspie_" + guid + ".png\"";
            string tempPlantDualTemperatureWaterSystemsPiePath = "src=\"/tempCustomReports/bureau_plantdualtemperaturewatersystemspie_" + guid + ".png\"";
            string tempPlantHeatExchangerPiePath = "src=\"/tempCustomReports/bureau_plantheatexchangerpie_" + guid + ".png\"";
            string tempPlantHeatRecoverySystemsPiePath = "src=\"/tempCustomReports/bureau_plantheatrecoverysystemspie_" + guid + ".png\"";
            string tempPlantPumpsPiePath = "src=\"/tempCustomReports/bureau_plantpumpspie_" + guid + ".png\"";
            string tempPlantStoragePiePath = "src=\"/tempCustomReports/bureau_plantstoragepie_" + guid + ".png\"";
            string tempPlantEnergyPath = "src=\"/tempCustomReports/bureau_plantenergy_" + guid + ".png\"";
            string tempPlantMaintenancePath = "src=\"/tempCustomReports/bureau_plantmaintenance_" + guid + ".png\"";
            string tempPlantComfortPath = "src=\"/tempCustomReports/bureau_plantcomfort_" + guid + ".png\"";
            string tempPlantEnergyPeriodPath = "src=\"/tempCustomReports/bureau_plantenergyperiod_" + guid + ".png\"";
            string tempPlantMaintenancePeriodPath = "src=\"/tempCustomReports/bureau_plantmaintenanceperiod_" + guid + ".png\"";
            string tempPlantComfortPeriodPath = "src=\"/tempCustomReports/bureau_plantcomfortperiod_" + guid + ".png\"";

            //zone
            string tempZoneZoneEquipmentPiePath = "src=\"/tempCustomReports/bureau_zonezoneequipmentpie_" + guid + ".png\"";
            string tempZoneZoneGroupsPiePath = "src=\"/tempCustomReports/bureau_zonezonegroupspie_" + guid + ".png\"";
            string tempZoneEnergyPath = "src=\"/tempCustomReports/bureau_zoneenergy_" + guid + ".png\"";
            string tempZoneMaintenancePath = "src=\"/tempCustomReports/bureau_zonemaintenance_" + guid + ".png\"";
            string tempZoneComfortPath = "src=\"/tempCustomReports/bureau_zonecomfort_" + guid + ".png\"";
            string tempZoneEnergyPeriodPath = "src=\"/tempCustomReports/bureau_zoneenergyperiod_" + guid + ".png\"";
            string tempZoneMaintenancePeriodPath = "src=\"/tempCustomReports/bureau_zonemaintenanceperiod_" + guid + ".png\"";
            string tempZoneComfortPeriodPath = "src=\"/tempCustomReports/bureau_zonecomfortperiod_" + guid + ".png\"";

            //specialty
            string tempSpecialtySpecialtyEquipmentPiePath = "src=\"/tempCustomReports/bureau_specialtyspecialtyequipmentpie_" + guid + ".png\"";
            string tempSpecialtyEnergyPath = "src=\"/tempCustomReports/bureau_specialtyenergy_" + guid + ".png\"";
            string tempSpecialtyMaintenancePath = "src=\"/tempCustomReports/bureau_specialtymaintenance_" + guid + ".png\"";
            string tempSpecialtyComfortPath = "src=\"/tempCustomReports/bureau_specialtycomfort_" + guid + ".png\"";
            string tempSpecialtyEnergyPeriodPath = "src=\"/tempCustomReports/bureau_specialtyenergyperiod_" + guid + ".png\"";
            string tempSpecialtyMaintenancePeriodPath = "src=\"/tempCustomReports/bureau_specialtymaintenanceperiod_" + guid + ".png\"";
            string tempSpecialtyComfortPeriodPath = "src=\"/tempCustomReports/bureau_specialtycomfortperiod_" + guid + ".png\"";

            //top diagnostic figures
            string[] tempTopDiagnosticFigures = new string[15];
            for (int i = 0; i < 15; i++)
            {
                tempTopDiagnosticFigures[i] = "src=\"tempCustomReports/bureau_topdiagnosticfigure" + (i + 1).ToString() + "_" + guid + ".png\"";
            }
            
            html = CustomReportsFileHelper.ReplacePath(tempCoverClientLogoPath, html, hdnShowClientLogos, radCoverClient);

            html = CustomReportsFileHelper.ReplacePath(tempBuildingPiePath, html, divBuildingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempBuildingColumnPath, html, divBuildingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempEquipmentClassPiePath, html, divEquipmentClassSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempEquipmentClassColumnPath, html, divEquipmentClassSummaryPage);

            html = CustomReportsFileHelper.ReplacePath(tempEnergyPath, html, divBuildingTrendsPage);
            html = CustomReportsFileHelper.ReplacePath(tempEnergyPeriodPath, html, divBuildingTrendsPage);
            html = CustomReportsFileHelper.ReplacePath(tempMaintenancePath, html, divBuildingTrendsPage);
            html = CustomReportsFileHelper.ReplacePath(tempMaintenancePeriodPath, html, divBuildingTrendsPage);
            html = CustomReportsFileHelper.ReplacePath(tempComfortPath, html, divBuildingTrendsPage);
            html = CustomReportsFileHelper.ReplacePath(tempComfortPeriodPath, html, divBuildingTrendsPage);

            html = CustomReportsFileHelper.ReplacePath(tempVentilationAirHandlersPiePath, html, divVentilationSummaryPage, ventilationSummaryAirHandlersPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempVentilationFansPiePath, html, divVentilationSummaryPage, ventilationSummaryFansPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempVentilationVentilationSystemsPiePath, html, divVentilationSummaryPage, ventilationSummaryVentilationSystemsPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempVentilationEnergyPath, html, divVentilationSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempVentilationEnergyPeriodPath, html, divVentilationSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempVentilationMaintenancePath, html, divVentilationSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempVentilationMaintenancePeriodPath, html, divVentilationSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempVentilationComfortPath, html, divVentilationSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempVentilationComfortPeriodPath, html, divVentilationSummaryPage);

            html = CustomReportsFileHelper.ReplacePath(tempHeatingBoilersPiePath, html, divHeatingSummaryPage, heatingSummaryBoilersPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempHeatingHeatingSystemsPiePath, html, divHeatingSummaryPage, heatingSummaryHeatingSystemsPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempHeatingEnergyPath, html, divHeatingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempHeatingEnergyPeriodPath, html, divHeatingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempHeatingMaintenancePath, html, divHeatingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempHeatingMaintenancePeriodPath, html, divHeatingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempHeatingComfortPath, html, divHeatingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempHeatingComfortPeriodPath, html, divHeatingSummaryPage);

            html = CustomReportsFileHelper.ReplacePath(tempCoolingChillersPiePath, html, divCoolingSummaryPage, coolingSummaryChillersPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempCoolingCoolingSystemsPiePath, html, divCoolingSummaryPage, coolingSummaryCoolingSystemsPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempCoolingHeatRejectionPiePath, html, divCoolingSummaryPage, coolingSummaryHeatRejectionPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempCoolingEnergyPath, html, divCoolingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempCoolingEnergyPeriodPath, html, divCoolingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempCoolingMaintenancePath, html, divCoolingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempCoolingMaintenancePeriodPath, html, divCoolingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempCoolingComfortPath, html, divCoolingSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempCoolingComfortPeriodPath, html, divCoolingSummaryPage);

            html = CustomReportsFileHelper.ReplacePath(tempPlantDomesticWaterSystemsPiePath, html, divPlantSummaryPage, plantSummaryDomesticWaterSystemsPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempPlantDualTemperatureWaterSystemsPiePath, html, divPlantSummaryPage, plantSummaryDualTemperatureWaterSystemsPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempPlantHeatExchangerPiePath, html, divPlantSummaryPage, plantSummaryHeatExchangerPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempPlantHeatRecoverySystemsPiePath, html, divPlantSummaryPage, plantSummaryHeatRecoverySystemsPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempPlantPumpsPiePath, html, divPlantSummaryPage, plantSummaryPumpsPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempPlantStoragePiePath, html, divPlantSummaryPage, plantSummaryStoragePieChart);
            html = CustomReportsFileHelper.ReplacePath(tempPlantEnergyPath, html, divPlantSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempPlantEnergyPeriodPath, html, divPlantSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempPlantMaintenancePath, html, divPlantSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempPlantMaintenancePeriodPath, html, divPlantSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempPlantComfortPath, html, divPlantSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempPlantComfortPeriodPath, html, divPlantSummaryPage);

            html = CustomReportsFileHelper.ReplacePath(tempZoneZoneEquipmentPiePath, html, divZoneSummaryPage, zoneSummaryZoneEquipmentPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempZoneZoneGroupsPiePath, html, divZoneSummaryPage, zoneSummaryZoneGroupsPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempZoneEnergyPath, html, divZoneSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempZoneEnergyPeriodPath, html, divZoneSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempZoneMaintenancePath, html, divZoneSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempZoneMaintenancePeriodPath, html, divZoneSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempZoneComfortPath, html, divZoneSummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempZoneComfortPeriodPath, html, divZoneSummaryPage);

            html = CustomReportsFileHelper.ReplacePath(tempSpecialtySpecialtyEquipmentPiePath, html, divSpecialtySummaryPage, specialtySummarySpecialtyEquipmentPieChart);
            html = CustomReportsFileHelper.ReplacePath(tempSpecialtyEnergyPath, html, divSpecialtySummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempSpecialtyEnergyPeriodPath, html, divSpecialtySummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempSpecialtyMaintenancePath, html, divSpecialtySummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempSpecialtyMaintenancePeriodPath, html, divSpecialtySummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempSpecialtyComfortPath, html, divSpecialtySummaryPage);
            html = CustomReportsFileHelper.ReplacePath(tempSpecialtyComfortPeriodPath, html, divSpecialtySummaryPage);

            int topDiagnosticCounter = 0;
            foreach (RepeaterItem item in rptTopDiagnostics.Items)
            {
                foreach (Control c in item.Controls)
                {
                    //if (c is System.Web.UI.WebControls.Image && topDiagnosticCounter < 15)
                    //{
                    //    System.Web.UI.WebControls.Image img = (System.Web.UI.WebControls.Image)c;

                    //    html = CustomReportsFileHelper.ReplacePath(tempTopDiagnosticFigures[topDiagnosticCounter], html, hdnIncludeTopDiagnosticsFigures, img);

                    //    topDiagnosticCounter++;
                    //}
                    if (c is RadBinaryImage && topDiagnosticCounter < 15)
                    {
                        RadBinaryImage radImg = (RadBinaryImage)c;

                        html = CustomReportsFileHelper.ReplacePath(tempTopDiagnosticFigures[topDiagnosticCounter], html, hdnIncludeTopDiagnosticsFigures, radImg);

                        topDiagnosticCounter++;
                    }
                }
            }

            return html;
        }

        protected bool GetInclude()
        {
            return includeTopDiagnosticsFigures;
        }

        #endregion  
    }
}
