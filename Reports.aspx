﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Reports.master" AutoEventWireup="false" CodeBehind="Reports.aspx.cs" Inherits="CW.Website.ReportsPage" EnableEventValidation="true" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="Reports" TagName="ReportCriteria" Src="~/_reports/ReportCriteria.ascx" %>
<%@ Register TagPrefix="Reports" TagName="ChartRegion" Src="~/_reports/ChartRegion.ascx" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <script type="text/javascript" src="_assets/scripts/dates.js"></script>
    <script type="text/javascript">
        function ClearViewstate() {
            //document.getElementById('__VIEWSTATE').value = "";
        }

        function EnableControlValidators() {
            // Enable validators (specific) in left & right controls
            if ('undefined' != typeof (ctl00_plcCopy_reportCriteriaLeft)) {
                ctl00_plcCopy_reportCriteriaLeft.SetStateForValidators(false);
            }
            if ('undefined' != typeof (ctl00_plcCopy_reportCriteriaRight)) {
                var ctl = $('#ctl00_plcCopy_chkCompareWith');
                if (null != ctl) {
                    if (ctl.prop('checked')) {
                        ctl00_plcCopy_reportCriteriaRight.SetStateForValidators(false);
                    }
                }
            }
        }

        function onChartPostRenderResize(sender, args) {
            try {
                document.getElementById('ctl00_plcCopy_priChartRegion_rawChart').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('ctl00_plcCopy_secChartRegion_rawChart').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('ctl00_plcCopy_priChartRegion_radChart').style.width = '100%';
            } catch (e) {
            }
            try {
                document.getElementById('ctl00_plcCopy_secChartRegion_radChart').style.width = '100%';
            } catch (e) {
            }
        }

    </script>

    <div class="reportingModuleIcon">
        <h1>Reporting</h1>
    </div>
    <div class="richText">
        <asp:Literal ID="litReportsBody" runat="server"></asp:Literal>
    </div>

    <telerik:RadAjaxPanel ClientEvents-OnResponseEnd="onChartPostRenderResize();" ID="radUpdatePanelMain" runat="server" UpdateMode="Conditional" LoadingPanelID="lp1">
        <telerik:RadAjaxLoadingPanel ID="lp1" runat="server" Skin="Default" />

        <asp:Panel ID="reportDiv" CssClass="reportDiv" runat="server" DefaultButton="btnGenerateData">
            <hr />
            <div class="reportTop">
                <div class="reportTopLeft">
                    <h2>Primary Chart</h2>
                </div>
                <div class="reportTopCenter">
                    <label>Compare With:</label>
                    <asp:CheckBox ID="chkCompareWith" CssClass="checkbox" OnCheckedChanged="chkCompareWith_OnCheckedChanged" Checked="false" AutoPostBack="true" runat="server" />
                </div>
                <div class="reportTopRight">
                    <h2>Secondary Chart</h2>
                </div>
            </div>
            <Reports:ReportCriteria ID="reportCriteriaLeft" runat="server" />
            <Reports:ReportCriteria ID="reportCriteriaRight" runat="server" enabled="false" />

            <!--Use bottomright popup validator positions for this column because the browser window will not always have space for the right most positioning. 
            Since the button is below, the browser window will always have room for the botom validator positioning.-->

            <div runat="server" class="reportBottom">
                <div class="divGenerateButton">
                    <asp:LinkButton ID="btnGenerateData" ValidationGroup="Reporting" OnClientClick="EnableControlValidators()" CssClass="lnkButton" OnClick="generateButton_Click" Text="Generate Report" runat="server" />
                </div>
                <hr />


                <!-- Nested update panel needed for async post back trigeers for download and avoid loss of chart images and viewstate-->
                <asp:UpdatePanel ID="updatePanelNested" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imgEmail" />
                        <asp:AsyncPostBackTrigger ControlID="lnkEmail" />
                        <asp:AsyncPostBackTrigger ControlID="imgPdfDownload" />
                        <asp:AsyncPostBackTrigger ControlID="lnkPdfDownload" />
                    </Triggers>
                    <ContentTemplate>

                        <asp:Label ID="lblError" CssClass="errorMessage" runat="server" Text="" Visible="false"></asp:Label>
                        <a id="lnkSetFocusMessage" runat="server"></a>
                        <div id="divDownloadBox" class="divDownloadBoxNoTopMargin" visible="false" runat="server">
                            <div id="divEmail" class="divEmail" visible="false" runat="server">
                                <asp:ImageButton ID="imgEmail" CssClass="imgEmail" ImageUrl="_assets/images/email-icon.jpg" OnClick="emailButton_Click" AlternateText="email" runat="server" />
                                <asp:LinkButton ID="lnkEmail" CssClass="lnkEmail" OnClick="emailButton_Click" runat="server" Text="Email Me Report"></asp:LinkButton>
                            </div>
                            <div id="divPdfDownload" class="divDownload" visible="false" runat="server">
                                <asp:ImageButton ID="imgPdfDownload" CssClass="imgDownload" ImageUrl="_assets/images/pdf-icon.jpg" OnClick="downloadPdfButton_Click" AlternateText="download" runat="server" />
                                <asp:LinkButton ID="lnkPdfDownload" CssClass="lnkDownload" OnClick="downloadPdfButton_Click" runat="server" Text="Download Report"></asp:LinkButton>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>

                <telerik:RadAjaxPanel ID="updatePanelCharts2" runat="server" UpdateMode="Conditional">
                    <Reports:ChartRegion runat="server" ID="priChartRegion" />
                    <Reports:ChartRegion runat="server" ID="secChartRegion" />
                </telerik:RadAjaxPanel>

                <div id="divReportComments" class="divReportComments" visible="false" runat="server">
                    <br />
                    <h2>Additional Report Comments (Max HTML Characters = 5000)</h2>
                    <telerik:RadEditor ID="editorReportComments" runat="server" CssClass="editorNewLine" Height="475px" Width="674px" MaxHtmlLength="5000" NewLineMode="Div" ToolsWidth="676px" ToolbarMode="ShowOnFocus" ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags">
                        <CssFiles>
                            <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                        </CssFiles>
                    </telerik:RadEditor>
                </div>
                <!--Chart is required in comments, microsoft charting bug.-->
                <!--<asp:chart id="Chart2" runat="server" Height="1px" Width="1px"></asp:chart>-->
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>
        </asp:Panel>

    </telerik:RadAjaxPanel>

    <script type="text/javascript">
        onChartPostRenderResize();
    </script>

</asp:Content>
