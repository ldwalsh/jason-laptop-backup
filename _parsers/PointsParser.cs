﻿using CW.Business;
using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Logging;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using PH = CW.Utility.PropHelper;

namespace CW.Website._parsers
{
    public class PointsParser : FileParserBase
    {
        #region STATIC

            public static FieldDefBase[] FieldDefs, FieldDefsOverride;

            // field names
            private readonly static String fnDSID = PH.G<DataSource>(_ => _.DSID);
            private readonly static String fnEID = PH.G<Equipment>(_ => _.EID);
            private readonly static String fnReferenceID = PH.G<Point>(_ => _.ReferenceID);
            private readonly static String fnPID = PH.G<Point>(_ => _.PID);
            private readonly static String fnPointName = PH.G<Point>(_ => _.PointName);
            private readonly static String fnPointTypeID = PH.G<Point>(_ => _.PointTypeID);
            private readonly static String fnSamplingInterval = PH.G<Point>(_ => _.SamplingInterval);
            private readonly static String fnRelPctError = PH.G<Point>(_ => _.RelPctError);
            private readonly static String fnPower = PH.G<Point>(_ => _.Power);
            private readonly static String fnMultiply = PH.G<Point>(_ => _.Multiply);
            private readonly static String fnAddition = PH.G<Point>(_ => _.Addition);
            private readonly static String fnPowerMatlab = PH.G<Point>(_ => _.PowerMatlab);
            private readonly static String fnMultiplyMatlab = PH.G<Point>(_ => _.MultiplyMatlab);
            private readonly static String fnAdditionMatlab = PH.G<Point>(_ => _.AdditionMatlab);
            private readonly static String fnTimeZone = PH.G<Point>(_ => _.TimeZone);
            private readonly static String fnDateCreated = PH.G<Point>(_ => _.DateCreated);

            static PointsParser()
            {
                FieldDefs = new FieldDefBase[] 
				{
					new FieldDef<Int32>("DataSource ID", fnDSID, "required", GetFieldType(fnDSID, typeof(DataSource))){IsDistinctionField=true,}, 
					new FieldDef<Int32>("Equipment ID", fnEID, "required", GetFieldType(fnEID, typeof(Equipment))),
					new FieldDef<String>("Point Reference ID", fnReferenceID, "required", GetFieldType(fnReferenceID, typeof(Point))){IsDistinctionField=true,},
                    new FieldDef<String>("Point Name", fnPointName, "required", GetFieldType(fnPointName, typeof(Point))),
					new FieldDef<Int32>("Point Type ID", fnPointTypeID, "required", GetFieldType(fnPointTypeID, typeof(Point))),
                    new FieldDef<Int32>("Sampling Interval", fnSamplingInterval, "required", GetFieldType(fnSamplingInterval, typeof(Point))),
                    new FieldDef<Double>("RelPctError", fnRelPctError, "required", GetFieldType(fnRelPctError, typeof(Point))),
                    new FieldDef<Double>("Power (Storage)", fnPower, "required", GetFieldType(fnPower, typeof(Point))),
                    new FieldDef<Double>("Multiply (Storage)", fnMultiply, "required", GetFieldType(fnMultiply, typeof(Point))),
                    new FieldDef<Double>("Addition (Storage)", fnAddition, "required", GetFieldType(fnAddition, typeof(Point))),
                    new FieldDef<Double>("Power (Analyses)", fnPowerMatlab, "required", GetFieldType(fnPowerMatlab, typeof(Point))),
                    new FieldDef<Double>("Multiply (Analyses)", fnMultiplyMatlab, "required", GetFieldType(fnMultiplyMatlab, typeof(Point))),
                    new FieldDef<Double>("Addition (Analyses)", fnAdditionMatlab, "required", GetFieldType(fnAdditionMatlab, typeof(Point))),
                    new FieldDef<String>("Time Zone", fnTimeZone, "required and must be exact spelling as in system", GetFieldType(fnTimeZone, typeof(Point))),
                    new FieldDef<DateTime>("Date Created", fnDateCreated, String.Format("optional but must be date time format (example) {0}, local time expected", DateTime.UtcNow.ToString()), GetFieldType(fnDateCreated, typeof(Point)), 0, 0, 0, false, true) //defaulted
				};

                FieldDefsOverride = new FieldDefBase[] 
				{
					new FieldDef<Int32>("DataSource ID", fnDSID, "required", GetFieldType(fnDSID, typeof(DataSource))){IsDistinctionField=true,}, 
					new FieldDef<Int32>("Equipment ID", fnEID, "required", GetFieldType(fnEID, typeof(Equipment))),
					new FieldDef<String>("Point Reference ID", fnReferenceID, "required", GetFieldType(fnReferenceID, typeof(Point))){IsDistinctionField=true,},
                    new FieldDef<String>("Point Name", fnPointName, "required", GetFieldType(fnPointName, typeof(Point))),
					new FieldDef<Int32>("Point Type ID", fnPointTypeID, "required", GetFieldType(fnPointTypeID, typeof(Point))),
                    new FieldDef<Int32>("Sampling Interval", fnSamplingInterval, "required", GetFieldType(fnSamplingInterval, typeof(Point))),
                    new FieldDef<Double>("RelPctError", fnRelPctError, "required", GetFieldType(fnRelPctError, typeof(Point))),
                    new FieldDef<Double>("Power (Storage)", fnPower, "required", GetFieldType(fnPower, typeof(Point))),
                    new FieldDef<Double>("Multiply (Storage)", fnMultiply, "required", GetFieldType(fnMultiply, typeof(Point))),
                    new FieldDef<Double>("Addition (Storage)", fnAddition, "required", GetFieldType(fnAddition, typeof(Point))),
                    new FieldDef<Double>("Power (Analyses)", fnPowerMatlab, "required", GetFieldType(fnPowerMatlab, typeof(Point))),
                    new FieldDef<Double>("Multiply (Analyses)", fnMultiplyMatlab, "required", GetFieldType(fnMultiplyMatlab, typeof(Point))),
                    new FieldDef<Double>("Addition (Analyses)", fnAdditionMatlab, "required", GetFieldType(fnAdditionMatlab, typeof(Point))),
                    new FieldDef<String>("Time Zone", fnTimeZone, "required and must be exact spelling as in system", GetFieldType(fnTimeZone, typeof(Point))),
                    new FieldDef<Int32>("Point ID", fnPID, "required override only", GetFieldType(fnPID, typeof(Point))),
				};
            }

        #endregion

        #region class

            public class BuildingPointInfo
            {
                public Int32 DBPointCount { get; set; }
                public Int32 FilePointCount { get; set; }
                public Int32 RowIndex { get; set; }
            }

        #endregion

        #region field

            private readonly DataManager mDataManager;
            private readonly ISectionLogManager mLogger;
            private readonly List<Int32> pids;
            private readonly List<PointType> pointTypes;
            private readonly List<Building> buildings;    
            private readonly IList<BuildingSetting> buildingSettings;
            private Dictionary<Int32, BuildingPointInfo> bidAndPointsCount;
            private List<Int32> bidsFlaggedForPointCount;

        #endregion

        #region constructor

            public PointsParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) :
                base(fileContent, (overrideExistingValues) ? FieldDefsOverride : FieldDefs, typeof(Point), overrideExistingValues, userInfo)
            {
                var cid = SiteUser.Current.CID;

                this.mDataManager = dataManager;
                this.mLogger = logger ?? new NullSectionLogManager();
                this.pids = mDataManager.PointDataMapper.GetAllPointsByCID(cid).Select(p => p.PID).ToList();
                this.pointTypes = mDataManager.PointTypeDataMapper.GetAllPointTypes().ToList();
                this.buildings = mDataManager.BuildingDataMapper.GetAllBuildingsByCID(cid).ToList();
                this.buildingSettings = new BuildingSettingQuery(mDataManager.ConnectionString, SiteUser.Current).FinalizeLoadWith.ByClient(cid).ToList();
                this.bidAndPointsCount = new Dictionary<Int32, BuildingPointInfo>();
                this.bidsFlaggedForPointCount = new List<Int32>();
            }

            public PointsParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, FieldDefBase[] fieldDefs, ISectionLogManager logger, UserInfo userInfo) : 
            base(fileContent, fieldDefs, typeof(Point), overrideExistingValues, userInfo)
            {
                this.mDataManager = dataManager;
                this.mLogger = logger ?? new NullSectionLogManager();
            }
            
        #endregion

        #region methods

            #region override

                public override void ParseRow(String[] rowContent, Int32 rowIndex)
                {
                    var pid = (OverrideExistingValues) ? GetIdFromParseValue(rowContent[LookupDict[fnPID]]) : 0;
                    var point = mDataManager.PointDataMapper.GetPointByPID(pid);

                    if (HasEntityExceptions(point, rowIndex, FieldDefs, rowContent)) return;

                    SetEntityAndPrimaryIDToRow(point, rowContent, fnPID);

                    if (pid != 0)
                    {
                        if (!IsValidPIDForClient(rowIndex, pids, pid, fnPID)) return;
                    }

                    var fieldDefs = OverrideExistingValues ? FieldDefsOverride : FieldDefs;
                    var dataSource = GetForeignKeyFieldData(rowIndex, rowContent, fnDSID, (id) => mDataManager.DataSourceDataMapper.GetDataSourceByID(id));
                    var equipment = GetForeignKeyFieldData(rowIndex, rowContent, fnEID, (id) => mDataManager.EquipmentDataMapper.GetEquipmentByEID(id, false, true, false, false, false, true));
                    var dsid = GetIdFromParseValue(rowContent[LookupDict[fnDSID]]);
                    var eid = GetIdFromParseValue(rowContent[LookupDict[fnEID]]);
                    var bid = (equipment != null) ? ((Equipment)equipment).BID : 0;

                    if (!IsEntityAndCidValid(dataSource, typeof(DataSource), rowContent, fieldDefs, rowIndex)) return;
                    if (!IsEntityAndCidValid(equipment, typeof(Equipment), rowContent, fieldDefs, rowIndex)) return;
                    if (!TimeZoneInfo.GetSystemTimeZones().Where(tz => tz.Id == rowContent[LookupDict[fnTimeZone]]).Any()) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnTimeZone), rowContent[LookupDict[fnTimeZone]]);
                    if (!OverrideExistingValues && bid != 0) CheckIfExceedsPointCount(rowIndex, bid);

                    var referenceID = rowContent[LookupDict[fnReferenceID]];

                    if (LookupDict.ContainsKey("PointTypeID"))
                    {
                        CheckForeignKeyField(rowIndex, rowContent, fnPointTypeID, (id) => mDataManager.PointTypeDataMapper.GetPointType(id));

                        var validFields = (Exceptions.Where(e => e.Row == rowIndex).Count() == 0) ? true : false;

                        if (validFields)
                        {
                            var pointTypeId = GetIdFromParseValue(rowContent[LookupDict[fnPointTypeID]]);
                            var fieldsToCheck = new Dictionary<String, Object>() { { fnEID, eid }, { fnPointTypeID, pointTypeId }, { fnDSID, dsid }, { fnReferenceID, referenceID }, { fnPID, pid } };

                            if (!OverrideExistingValues)
                                CheckFieldsBeforeInsert(rowContent, rowIndex, fieldsToCheck, (Equipment)equipment);
                            else if (OverrideExistingValues)
                                CheckFieldsBeforeUpdate(rowContent, rowIndex, fieldsToCheck, point, (Equipment)equipment);
                        }
                    }

                    if (Exceptions.Count == 0 && bid != 0) UpdateBIDList(bid);
                }

                public override void ParseComplete(Int32 rowCount)
                {
                    foreach (var item in bidAndPointsCount)
                    {
                        if (item.Value.RowIndex != 0)
                        {
                            var totalPointCount = item.Value.DBPointCount + item.Value.FilePointCount;
                            var maxPointLimit = buildingSettings.Single(_ => _.BID == item.Key).MaxPointLimit;
                            var sbMessage = new StringBuilder().Append(String.Format("Exceeded point count limit of {0} for building {1}.", maxPointLimit, buildings.Single(_ => _.BID == item.Key).BuildingName));

                            SetCountMessage(sbMessage, " File contains {0} {1} for this building.", item.Value.FilePointCount);
                            SetCountMessage(sbMessage, " Building has {0} existing {1}.", item.Value.DBPointCount);

                            GenerateInvalidGenericException(item.Value.RowIndex, sbMessage.ToString());
                        }
                    }
                }

                protected override void InsertRow(Row row)
                {
                    try
                    {
                        var point = CreatePoint(row);
                        var pidPrimaryKey = mDataManager.PointDataMapper.InsertAndReturnPID(point);
                        var equipmentPoint = new Equipment_Point();

                        equipmentPoint.EID = Convert.ToInt32(row.DataRow[LookupDict[fnEID]]);
                        equipmentPoint.PID = pidPrimaryKey;

                        mDataManager.EquipmentPointsDataMapper.InsertEquipment_Point(equipmentPoint);
                        InsertCount++;
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting point.", row.DataRow[LookupDict[fnPointName]]); }
                }

                protected override void UpdateRow(Row row)
                {
                    var rowIndex = row.RowIndex;
                    var pointName = row.DataRow[LookupDict[fnPointName]];

                    try
                    {
                        var point = CreatePoint(row);

                        mDataManager.PointDataMapper.UpdatePoint(point);
                        OverridesSB.AppendLine(String.Format("Row#&nbsp;{0} Updated Values for Point ID {1} and {2} {3}", rowIndex, point.PID, GetDisplayName(fnPointName), pointName));
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, rowIndex, sqlEx, "Error updating point.", pointName); }
                }

            #endregion

            private Boolean IsEntityAndCidValid(Object entity, Type entityType, String[] rowContent, FieldDefBase[] fieldDefs, Int32 rowIndex)
            {
                if (entity == null)
                {
                    var fnFieldName = (entityType == typeof(DataSource)) ? fnDSID : fnEID;

                    GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnFieldName), rowContent[LookupDict[fnFieldName]]);
                    return false; 
                }

                var cid = (entityType == typeof(DataSource)) ? ((DataSource)entity).CID : ((Equipment)entity).Building.CID;

                if (!IsValidCIDForClient(fieldDefs.Single(_ => _.FieldName == fnDSID).DisplayName, rowIndex, entity, cid)) return false;
                
                return true; 
            }

            private void CheckFieldsBeforeInsert(String[] rowContent, Int32 rowIndex, Dictionary<String, Object> fields, Equipment equipment)
            {
                var dsid = (Int32)fields[fnDSID];
                var eid = (Int32)fields[fnEID];
                var pointTypeId = (Int32)fields[fnPointTypeID];
                var referenceId = (String)fields[fnReferenceID];
                var pointTypeExistsForNonEquipmentGroup = false;

                //check if point type is allowed for equipment type
                if (!mDataManager.EquipmentTypeDataMapper.IsPointTypeAssociatedWithEquipmentsType(pointTypeId, eid))
                {
                    Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.pointTypeNotAllowed, GetDisplayName(fnPointTypeID), pointTypeId, pointTypes.Where(pt => pt.PointTypeID == pointTypeId).First().PointTypeName, GetDisplayName(fnEID), eid, equipment.EquipmentName, equipment.EquipmentTypeID, equipment.EquipmentType.EquipmentTypeName));
                    return;
                }

                //TODO: Might not be needed as IsEntityAndCidValid checks for dsid and eid should take care of this.
                //if dsid and eid do not exist for the same client, if so generation exception
                if (!mDataManager.ClientDataMapper.DoesSameClientExistForDSIDAndEID(dsid, eid))
                {
                    Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.invalidClientRelationship, GetDisplayName(fnDSID), dsid, GetDisplayName(fnEID), eid, equipment.EquipmentName));
                    return;
                }

                var isEquipmentGroup = mDataManager.EquipmentDataMapper.IsEquipmentAssociatedWithClassGroup(eid);                
                var isPointTypeUnassigned = mDataManager.PointTypeDataMapper.IsPointTypeUnassigned(pointTypeId);

                //if not in equipment group and point type is assigned check if a point with the same point type already exists for equipmet not of class group
                if (!isEquipmentGroup && !isPointTypeUnassigned) pointTypeExistsForNonEquipmentGroup = mDataManager.EquipmentPointsDataMapper.DoesPointTypeExistForEquipmentNotOfClassGroup(eid, pointTypeId);

                //if equipment is not group and if pointtype exists on non equipment class group, and if assigned, generate exception
                if (!isEquipmentGroup && pointTypeExistsForNonEquipmentGroup && !isPointTypeUnassigned)
                {
                    Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.pointTypeExists, GetDisplayName(fnPointTypeID), pointTypeId, pointTypes.Where(pt => pt.PointTypeID == pointTypeId).First().PointTypeName, GetDisplayName(fnEID), eid, equipment.EquipmentName));
                    return;
                }

                //Check if reference id already exists for this data source, if so generate exception
                if (mDataManager.PointDataMapper.DoesPointReferenceIDExistForDataSource(dsid, referenceId))
                    Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.referenceIDExists, GetDisplayName(fnPointName), rowContent[LookupDict[fnPointName]], GetDisplayName(fnDSID), dsid));
            }

            private void CheckFieldsBeforeUpdate(String[] rowContent, Int32 rowIndex, Dictionary<String, Object> fields, Point point, Equipment equipment)
            {
                var pid = (Int32)fields[fnPID];
                var dsid = (Int32)fields[fnDSID];
                var eid = (Int32)fields[fnEID];
                var pointTypeId = (Int32)fields[fnPointTypeID];
                var referenceId = (String)fields[fnReferenceID];

                if (point == null)
                {
                    Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.noPointExists, GetDisplayName(fnPID), pid));
                    return;
                }

                if (dsid != point.DSID || referenceId != point.ReferenceID)
                {
                    if (mDataManager.PointDataMapper.DoesPointReferenceIDExistForDataSource(dsid, referenceId))
                        Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.referenceIDExists, GetDisplayName(fnPointName), rowContent[LookupDict[fnPointName]], GetDisplayName(fnDSID), dsid));

                    //TODO: Might not be needed as IsEntityAndCidValid checks for dsid and eid should take care of this.
                    //can only change to a datasource for the same client.
                    if (dsid != point.DSID && !mDataManager.ClientDataMapper.DoesSameClientExistForDSIDAndEID(dsid, eid))
                        Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.invalidClientRelationship, GetDisplayName(fnDSID), dsid, GetDisplayName(fnEID), eid, equipment.EquipmentName));
                }

               CheckPointAndPointType(rowIndex, eid, pid, pointTypeId, point, equipment);
            }

            private void CheckPointAndPointType(Int32 rowIndex, Int32 eid, Int32 pid, Int32 pointTypeId, Point point, Equipment equipment)
            {
                var hasPointTypeChanged = pointTypeId != point.PointTypeID;

                if (hasPointTypeChanged)
                {
                    //check if new point type is allowed for equipment type
                    var equipmentTypesIds = mDataManager.EquipmentDataMapper.GetAllEquipmentAssociatedWithPID(pid).Select(eq => eq.EquipmentTypeID).Distinct();
                    var allowedEquipemntTypesIdsByNewPointType = mDataManager.EquipmentTypeDataMapper.GetAllEquipmentTypeIDsAssociatedWithPTID(pointTypeId);

                    if (equipmentTypesIds.Except(allowedEquipemntTypesIdsByNewPointType).Any())
                    {
                        Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.pointTypeNotAllowed, GetDisplayName(fnPointTypeID), pointTypeId, pointTypes.Where(pt => pt.PointTypeID == pointTypeId).First().PointTypeName, GetDisplayName(fnEID), eid, equipment.EquipmentName, equipment.EquipmentTypeID, equipment.EquipmentType.EquipmentTypeName));
                        return;
                    }

                    if (!mDataManager.PointTypeDataMapper.IsPointTypeUnassigned(pointTypeId))
                    {
                        var doesPointTypeExists = false;
                        var eids = mDataManager.EquipmentPointsDataMapper.GetAllEquipmentPointEIDsNotOfClassGroupByPID(pid);

                        foreach (var e in eids)
                            doesPointTypeExists = mDataManager.EquipmentPointsDataMapper.DoesPointTypeExistForEquipment(e, pointTypeId);

                        if (doesPointTypeExists) Exceptions.Add(new InvalidTemplateException(rowIndex, BusinessConstants.ParserMessage.pointTypeExists, GetDisplayName(fnPointTypeID), pointTypeId, pointTypes.Where(pt => pt.PointTypeID == pointTypeId).First().PointTypeName, GetDisplayName(fnEID), eid, equipment.EquipmentName));
                    }
                }
            }

            private void CheckIfExceedsPointCount(Int32 rowIndex, Int32 bid)
            {
                if (!bidAndPointsCount.ContainsKey(bid))
                {
                    var eids = mDataManager.EquipmentDataMapper.GetEquipmentByBID(bid).Select(e => e.EID);

                    bidAndPointsCount.Add(bid, new BuildingPointInfo() { DBPointCount = mDataManager.EquipmentPointsDataMapper.GetEquipmentPointCountByEIDs(eids) });
                }
                
                bidAndPointsCount[bid].FilePointCount = bidAndPointsCount[bid].FilePointCount + 1;
                CheckIfLimitReached(bidAndPointsCount[bid], bid);
                SetRowIndexIfLimitReached(bid, rowIndex);
            }

            private void CheckIfLimitReached(BuildingPointInfo buildingPointInfo, Int32 bid)
            {
                var totalPointCount = buildingPointInfo.DBPointCount + buildingPointInfo.FilePointCount;
                var maxPointLimit = buildingSettings.Single(_ => _.BID == bid).MaxPointLimit;

                if (totalPointCount > maxPointLimit && !bidsFlaggedForPointCount.Contains(bid)) bidsFlaggedForPointCount.Add(bid);
            }

            private void SetRowIndexIfLimitReached(Int32 bid, Int32 rowIndex)
            {
                if (bidsFlaggedForPointCount.Contains(bid) && bidAndPointsCount[bid].RowIndex == 0) bidAndPointsCount[bid].RowIndex = rowIndex;
            }

            private void SetCountMessage(StringBuilder sbMessage, String errorMsg, Int32 count) 
            {
                if (count > 0) sbMessage.Append(String.Format(errorMsg, count, ((count == 1) ? "point" : "points"))); 
            }

            private Point CreatePoint(Row row)
            {
                var dataRow = row.DataRow;
                var point = new Point();
                var entityPoint = ((Point)row.Entity);

                if (!OverrideExistingValues)
                {
                    point.IsSubscriptionBased = false;    

                    try
                    {
                        var dataCreated = dataRow[LookupDict[fnDateCreated]];

                        if (dataCreated != null)
                        {
                            var tzi = TimeZoneInfo.FindSystemTimeZoneById(dataRow[LookupDict[fnTimeZone]].ToString());

                            point.DateCreated = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(dataCreated), tzi);
                        }
                    }
                    catch { point.DateCreated = DateTime.UtcNow; }
                }
                else
                    point.PID = (Int32)row.PrimaryId;

                SetParserField(dataRow[LookupDict[fnDSID]], fnDSID, point);
                SetParserField(dataRow[LookupDict[fnReferenceID]], fnReferenceID, point);
                SetParserField(dataRow[LookupDict[fnPointName]], fnPointName, point);
                SetParserField(dataRow[LookupDict[fnPointTypeID]], fnPointTypeID, point);
                SetParserField(dataRow[LookupDict[fnSamplingInterval]], fnSamplingInterval, point);
                SetParserField(dataRow[LookupDict[fnRelPctError]], fnRelPctError, point);
                SetParserField(dataRow[LookupDict[fnPower]], fnPower, point);
                SetParserField(dataRow[LookupDict[fnMultiply]], fnMultiply, point);
                SetParserField(dataRow[LookupDict[fnAddition]], fnAddition, point);
                SetParserField(dataRow[LookupDict[fnPowerMatlab]], fnPowerMatlab, point);
                SetParserField(dataRow[LookupDict[fnMultiplyMatlab]], fnMultiplyMatlab, point);
                SetParserField(dataRow[LookupDict[fnAdditionMatlab]], fnAdditionMatlab, point);
                SetParserField(dataRow[LookupDict[fnTimeZone]], fnTimeZone, point);

                point.IsActive = OverrideExistingValues ? entityPoint.IsActive : true;
                point.IsVisible = OverrideExistingValues ? entityPoint.IsVisible : true;

                return point;
            }

        #endregion
    }
}