﻿using AjaxControlToolkit;
using CW.Reporting._fileservices;
using CW.Utility.Web;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using System;
using System.Reflection;
using System.Text;
using System.Web.UI.WebControls;

namespace CW.Website._parsers
{
    public abstract class ParserPageBase<T> : SitePage where T : FileParserBase
    {
        #region fields

            private T parser;
            private ListItem listItem = new ListItem() { Text = "Select one...", Value = "-1", Selected = true };
    
            protected FileUpload csvFileUpload;
            protected CheckBox chkOverrideValues;
            protected Literal litUploadFieldList;
            protected Literal litUploadOverrideFieldList;
            protected Label lblMessages;
            protected CollapsiblePanelExtender messagesCollapsiblePanelExtender;
            protected Label lblErrors;
            protected Label lblOverrides;
            protected Label lblResult;
            
        #endregion

        #region properties

            #region abstract

                protected abstract String DisplayName { get; }
                protected abstract String FileName { get; }

            #endregion

            #region virtual

                protected virtual String OverrideFileName { get; set; }

            #endregion

            private Boolean IsOverrideChecked { get { return (chkOverrideValues != null) ? chkOverrideValues.Checked : false; } }

        #endregion

        #region methods

            #region virtual

                protected virtual void ConfigureParser(T parser) { }

            #endregion

            protected void HideLabels()
            {
                lblErrors.Visible = false;
                lblResult.Visible = false;
                lblOverrides.Visible = false;

                lblMessages.Visible = false;
                messagesCollapsiblePanelExtender.Collapsed = true;
                messagesCollapsiblePanelExtender.ClientState = "true";
            }

            protected void ButtonClick(Object sender, EventArgs e)
            {
                CreateParser();
                ExecuteParser();
            }

            private void CreateParser()
            {
                parser = (T)Activator.CreateInstance(typeof(T), new Object[] { csvFileUpload.FileContent, DataMgr, IsOverrideChecked, LogMgr });

                ConfigureParser(parser);
            }

            private void ExecuteParser()
            {
                var executor = new FileParserExecuter(parser, DisplayName, IsOverrideChecked, LogMgr);

                executor.ExecuteParseAndSave();

                DisplayMessages(executor.Messages);
            }

            private void DisplayMessages(FileParserMessages messages)
            {
                lblMessages.Visible = true;
                messagesCollapsiblePanelExtender.Collapsed = false;
                messagesCollapsiblePanelExtender.ClientState = "false";

                if (messages.HasMessage(FileParserMessages.MessageType.Exception))
                {
                    lblErrors.Visible = true;
                    lblErrors.Text = messages.GetMessage(FileParserMessages.MessageType.Exception, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Error))
                {
                    lblErrors.Visible = true;
                    lblErrors.Text = messages.GetMessage(FileParserMessages.MessageType.Error, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Update))
                {
                    lblOverrides.Visible = true;
                    lblOverrides.Text = messages.GetMessage(FileParserMessages.MessageType.Update, FileParserMessages.MessageFormat.Html);
                }

                if (messages.HasMessage(FileParserMessages.MessageType.Insert))
                {
                    lblResult.Visible = true;
                    lblResult.Text = messages.GetMessage(FileParserMessages.MessageType.Insert, FileParserMessages.MessageFormat.Html);
                }
            }

            protected void DownloadFileTemplateClick(Object sender, EventArgs e)
            {
                CreateDownloadFile(GetFieldDefs("FieldDefs"), FileName);
            }

            protected void DownloadOverrideFileTemplateClick(Object sender, EventArgs e)
            {
                CreateDownloadFile(GetFieldDefs("FieldDefsOverride"), OverrideFileName);
            }

            private void CreateDownloadFile(FileParserBase.FieldDefBase[] fieldDefs, String fileName)
            {
                var fieldNamesForFile = Encoding.UTF8.GetBytes(FileParserBase.FormatFieldNamesForOutput(",", "TemplateDownload", fieldDefs));

                var fileHandler = new DownloadRequestHandler.HandledFileAttacher();
                ((IFileAttacher)fileHandler).AttachForDownload((fileName != String.Empty) ? fileName : "UploadFileTemplate.csv", fieldNamesForFile);
            }

            private FileParserBase.FieldDefBase[] GetFieldDefs(String fieldName)
            {
                return (FileParserBase.FieldDefBase[])typeof(T).GetField(fieldName, BindingFlags.Static | BindingFlags.Public).GetValue(null);
            }

            protected void BindUploadFieldsList()
            {
                if (litUploadFieldList != null) litUploadFieldList.Text = FileParserBase.FormatFieldNamesForOutput("<br />", "FieldList", GetFieldDefs("FieldDefs"));

                if (litUploadOverrideFieldList != null) litUploadOverrideFieldList.Text = FileParserBase.FormatFieldNamesForOutput("<br />", "FieldList", GetFieldDefs("FieldDefsOverride"));
            }

            protected void BindDDL(DropDownList ddl, String dataTextField, String dataValueField, Object data, Boolean clearItems = false)
            {
                if (clearItems)
                {
                    ddl.Items.Clear();
                    ddl.Items.Add(listItem);
                }

                ddl.DataTextField = dataTextField;
                ddl.DataValueField = dataValueField;
                ddl.DataSource = data;
                ddl.DataBind();
            }

            protected void BindGrid(GridView grid, Object data)
            {
                grid.DataSource = data;
                grid.DataBind();
            }

        #endregion
    }
}