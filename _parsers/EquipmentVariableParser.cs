﻿using CW.Business;
using CW.Data;
using CW.Logging;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using PH = CW.Utility.PropHelper;

namespace CW.Website._parsers
{
    public class EquipmentVariableParser : FileParserBase
    {
        #region const

            const String equipmentVariableNotAssociated = "The existing building variable with ID of {0} is associated to {1} {2} but NOT associated to Equipment Class ID {3}.";
            const String entityTypeName = "Equipment to equipment class relationship";
            const String propertyName = "Equipment Class ID {0} and  ";

        #endregion

        #region STATIC

            public static FieldDefBase[] FieldDefs;

            private readonly static String fnEID = PH.G<Equipment>(_ => _.EID);
            private readonly static String fnEquipmentVariableDisplayName = PH.G<EquipmentVariable>(_ => _.EquipmentVariableDisplayName);
            private readonly static String fnValue = PH.G<Equipment_EquipmentVariable>(_ => _.Value);

            static EquipmentVariableParser()
            {
                FieldDefs = new FieldDefBase[] 
				{
					new FieldDef<Int32>("Equipment ID", fnEID, "required", GetFieldType(fnEID, typeof(Equipment))){IsDistinctionField=true,},
					new FieldDef<String>("Equipment Variable Display Name", fnEquipmentVariableDisplayName, GetFieldType(fnEquipmentVariableDisplayName, typeof(EquipmentVariable)).ToLower() + " - required", String.Empty, 0, 50, 0, true){IsDistinctionField=true,}, //UIRequired
                    new FieldDef<String>(fnValue, fnValue, "required", GetFieldType(fnValue, typeof(EquipmentVariable))),
				};
            }

        #endregion

        #region field

            private readonly DataManager mDataManager;
            private readonly ISectionLogManager mLogger;
            private readonly Func<Int32, Equipment> equipmentGetter;
            private readonly Func<String, EquipmentVariable> equipmentVariableGetter;
            private readonly Func<Int32, Int32, Equipment_EquipmentVariable> equipmentEquipmentVariableGetter;
            private readonly Func<Int32, EquipmentClass> equipmentClassGetter;
 
        #endregion

        #region constructor

            public EquipmentVariableParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) : 
            base(fileContent, FieldDefs, typeof(Equipment_EquipmentVariable), overrideExistingValues, userInfo)
            {
                this.mDataManager = dataManager;
                this.mLogger = logger;
                this.equipmentGetter = (id) => mDataManager.EquipmentDataMapper.GetEquipmentByEID(id, false, true, false, false, false, false);
                this.equipmentVariableGetter = (name) => mDataManager.EquipmentVariableDataMapper.GetEquipmentVariableWithEngUnitByName(name, true);
                this.equipmentEquipmentVariableGetter = (eid, evid) => mDataManager.EquipmentVariableDataMapper.GetEquipmentVariableByEIDAndEVID(eid, evid);
                this.equipmentClassGetter = (id) => mDataManager.EquipmentClassDataMapper.GetEquipmentClassByEquipmentTypeID(id);
            }

        #endregion

        #region methods

            #region override

                public override void ParseRow(String[] rowContent, Int32 rowIndex)
                {
                    var eid = GetIdFromParseValue(rowContent[LookupDict[fnEID]]);
                    var equipment = new Equipment();
                    var equipmentVariable = new EquipmentVariable();
                    var equipmentError = GetEntityByID<Equipment>(rowIndex, eid, equipmentGetter, out equipment);
                    var equipmentVariableError = GetEntityByName<EquipmentVariable>(rowIndex, rowContent[LookupDict[fnEquipmentVariableDisplayName]], equipmentVariableGetter, out equipmentVariable);

                    if (equipmentError != null) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnEID), rowContent[LookupDict[fnEID]]);

                    if (equipment != null)
                    {
                        if (!IsValidCIDForClient(FieldDefs.Single(_ => _.FieldName == fnEquipmentVariableDisplayName).DisplayName, rowIndex, equipment, equipment.Building.CID)) return;
                    }

                    if (equipmentVariableError != null) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnEquipmentVariableDisplayName), rowContent[LookupDict[fnEquipmentVariableDisplayName]]);

                    if (equipment != null && equipmentVariable != null)
                    {
                        var equipmentEquipmentVariable = new Equipment_EquipmentVariable();
                        var equipmentEquipmentVariableError = GetEntityByIDs<Equipment_EquipmentVariable>(rowIndex, equipment.EID, equipmentVariable.EVID, equipmentEquipmentVariableGetter, out equipmentEquipmentVariable);

                        if (HasEntityExceptions(equipmentEquipmentVariable, rowIndex, FieldDefs, rowContent)) return;

                        SetEntityAndPrimaryIDToRow(equipmentEquipmentVariable, rowContent, PropHelper.G<Equipment_EquipmentVariable>(_ => _.ID));

                        CheckEquipmentClass(rowIndex, equipment, equipmentVariable, rowContent);

                        if (Exceptions.Count == 0) UpdateBIDList(equipment.BID);
                    }
                }

                protected override void InsertRow(Row row)
                {
                    try
                    {
                        var equipmentVariable = CreateEquipmentVariable(row);

                        mDataManager.EquipmentVariableDataMapper.InsertEquipmentEquipmentVariable(equipmentVariable);
                        InsertCount++;
                    }
                    catch (CW.Data.Exceptions.ValidationException ex)
                    {
                        Exceptions.Add(new InvalidGenericException(row.RowIndex, ex.Message));
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting equipment variable.", row.DataRow[LookupDict[fnEquipmentVariableDisplayName]]); }
                }

                protected override void UpdateRow(Row row)
                {
                    var rowIndex = row.RowIndex;
                    var equipmentVariableName = row.DataRow[LookupDict[fnEquipmentVariableDisplayName]];

                    try
                    {
                        var equipmentVariable = CreateEquipmentVariable(row);

                        mDataManager.EquipmentVariableDataMapper.UpdatePartialEquipmentEquipmentVariable(equipmentVariable);
                        OverridesSB.AppendLine(String.Format("Row#&nbsp;{0} Updated Value for {1} {2} and {3} {4} to {5}", rowIndex, GetDisplayName(fnEID), equipmentVariable.EID, GetDisplayName(fnEquipmentVariableDisplayName), equipmentVariableName,
                                                                                                                           row.DataRow[LookupDict[fnValue]]));
                    }
                    catch (CW.Data.Exceptions.ValidationException ex)
                    {
                        Exceptions.Add(new InvalidGenericException(rowIndex, ex.Message));
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, rowIndex, sqlEx, "Error updating equipment variable.", equipmentVariableName); }
                }

            #endregion

            private void CheckEquipmentClass(Int32 rowIndex, Equipment equipment, EquipmentVariable equipmentVariable, String[] rowContent)
            {
                var equipmentClass = new EquipmentClass();
                var equipmentClassError = GetEntityByID<EquipmentClass>(rowIndex, equipment.EquipmentTypeID, equipmentClassGetter, out equipmentClass);

                if (equipmentClassError == null)
                {
                    if (!mDataManager.EquipmentVariableDataMapper.DoesEquipmentVariableExistForEquipmentClass(equipmentClass.EquipmentClassID, equipmentVariable.EVID))
                    {
                        if (equipmentVariable != null) GenerateInvalidGenericException(rowIndex, equipmentVariableNotAssociated, equipmentVariable.EVID, GetDisplayName(fnEID), equipment.EID, equipmentClass.EquipmentClassID);

                        var propertyValue = String.Format("{0} {1}", GetDisplayName(fnEquipmentVariableDisplayName), rowContent[LookupDict[fnEquipmentVariableDisplayName]]);

                        Exceptions.Add(new EntityNotFoundException(rowIndex, entityTypeName, String.Format(propertyName, equipmentClass.EquipmentClassID), propertyValue, "DoesEquipmentVariableExistForEquipmentClass"));
                    }
                }
            }

            private Equipment_EquipmentVariable CreateEquipmentVariable(Row row)
            {
                var dataRow = row.DataRow;
                var equipmentEquipmentVariable = new Equipment_EquipmentVariable();

                if (!OverrideExistingValues)
                {
                    equipmentEquipmentVariable.EID = Convert.ToInt32(dataRow[LookupDict[fnEID]]);
                    equipmentEquipmentVariable.EVID = mDataManager.EquipmentVariableDataMapper.GetEquipmentVariableByName(dataRow[LookupDict[fnEquipmentVariableDisplayName]], true).EVID;
                }
                else
                    equipmentEquipmentVariable.ID = (Int32)row.PrimaryId;

                SetParserField(dataRow[LookupDict[fnValue]], fnValue, equipmentEquipmentVariable);

                return equipmentEquipmentVariable;
            }

        #endregion
    }
}