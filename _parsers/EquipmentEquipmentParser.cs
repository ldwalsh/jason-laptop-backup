﻿using CW.Business;
using CW.Data;
using CW.Logging;
using CW.Utility.Web;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using PH = CW.Utility.PropHelper;


namespace CW.Website._parsers
{
    public class EquipmentEquipmentParser : FileParserBase
    {
        #region const

            const String buildingMismatch = "{0} {1} is not assigned to the same building as the {2} {3}.";
            const String noEquipmentTypeRelationship = "Equipment type to equipment type relationship does not exist for {0} {1} and {2} {3}.";

        #endregion

        #region static

            public static FieldDefBase[] FieldDefs;

            private readonly static String fnEID = PH.G<Equipment>(_ => _.EID);
            private readonly static String fnPrimaryEID = PH.G<Equipment_Equipment>(_ => _.PrimaryEID);
            private readonly static String fnAssignedEID = PH.G<Equipment_Equipment>(_ => _.AssignedEID);

            static EquipmentEquipmentParser()
            {
                FieldDefs = new FieldDefBase[]
				{
					new FieldDef<Int32>("Primary EID", fnPrimaryEID, "required", GetFieldType(fnEID, typeof(Equipment))){IsDistinctionField=true,},
                    new FieldDef<Int32>("Assigned EID", fnAssignedEID, "required", GetFieldType(fnEID, typeof(Equipment))){IsDistinctionField=true,}
				};
            }

        #endregion

        #region fields

            private readonly DataManager mDataManager;
            private readonly ISectionLogManager mLogger;
            private readonly Func<Int32, Equipment> equipmentGetter;

        #endregion

        #region constructor

            public EquipmentEquipmentParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) : 
            base(fileContent, FieldDefs, typeof(Equipment_Equipment), overrideExistingValues, userInfo)
            {
                this.mDataManager = dataManager;
                this.mLogger = logger;
                this.equipmentGetter = (id) => mDataManager.EquipmentDataMapper.GetEquipmentByEID(id, false, true, false, false, false, true);
            }

        #endregion

        #region methods

            #region override

                public override void ParseRow(String[] rowContent, Int32 rowIndex)
                {
                    var primaryEID = GetIdFromParseValue(rowContent[LookupDict[fnPrimaryEID]]);
                    var assignedEID = GetIdFromParseValue(rowContent[LookupDict[fnAssignedEID]]);
                    var equipmentPrimary = CheckClientAndGetEquipmentIfExists(rowIndex, primaryEID, fnPrimaryEID, rowContent);
                    var equipmentAssigned = CheckClientAndGetEquipmentIfExists(rowIndex, assignedEID, fnAssignedEID, rowContent);

                    if (equipmentPrimary != null && equipmentAssigned != null)
                    {
                        var equipmentEquipment = mDataManager.EquipmentDataMapper.GetEquipmentToEquipmentByPrimaryEIDAndAssignedEID(primaryEID, assignedEID);

                        if (HasEntityExceptions(equipmentEquipment, rowIndex, FieldDefs, rowContent)) return;

                        if (equipmentPrimary.BID != equipmentAssigned.BID) 
                            GenerateInvalidGenericException(rowIndex, buildingMismatch, GetDisplayName(fnPrimaryEID), rowContent[LookupDict[fnPrimaryEID]], GetDisplayName(fnAssignedEID), rowContent[LookupDict[fnAssignedEID]]);

                        if (!mDataManager.EquipmentTypeDataMapper.DoesEquipmentTypeToEquipmentTypeExist(equipmentPrimary.EquipmentTypeID, equipmentAssigned.EquipmentTypeID))
                            GenerateInvalidGenericException(rowIndex, noEquipmentTypeRelationship, GetDisplayName(fnPrimaryEID), rowContent[LookupDict[fnPrimaryEID]], GetDisplayName(fnAssignedEID), rowContent[LookupDict[fnAssignedEID]]);
                    }

                    if (Exceptions.Count == 0)
                    {
                        UpdateBIDList(equipmentPrimary.BID);
                        UpdateBIDList(equipmentAssigned.BID);
                    }
                }

                protected override void InsertRow(Row row)
                {
                    try
                    {
                        var equipmentEquipment = CreateEquipmentEquipment(row);

                        mDataManager.EquipmentDataMapper.InsertEquipmentToEquipment(equipmentEquipment);
                        InsertCount++;
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting equipment to equipment.", row.DataRow[LookupDict["PrimaryEID"]]); }
                }

            #endregion

            private Equipment CheckClientAndGetEquipmentIfExists(Int32 rowIndex, Int32 eid, String fieldName, String[] rowContent)
            {
                var equipment = new Equipment();
                var equipmentError = GetEntityByID<Equipment>(rowIndex, eid, equipmentGetter, out equipment);

                if (equipmentError != null)
                {
                    GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fieldName), rowContent[LookupDict[fieldName]]);
                    return null;
                }
                else
                {
                    if (!IsValidCIDForClient(FieldDefs.Single(_ => _.FieldName == fieldName).DisplayName, rowIndex, equipment, equipment.Building.CID)) return null;
                }

                return equipment;
            }

            private Equipment_Equipment CreateEquipmentEquipment(Row row)
            {
                var dataRow = row.DataRow;
                var equipmentEquipment = new Equipment_Equipment();

                SetParserField(dataRow[LookupDict[fnPrimaryEID]], fnPrimaryEID, equipmentEquipment);
                SetParserField(dataRow[LookupDict[fnAssignedEID]], fnAssignedEID, equipmentEquipment);

                return equipmentEquipment;
            }

        #endregion
    }
}