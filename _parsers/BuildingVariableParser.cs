﻿using CW.Business;
using CW.Logging;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using PH = CW.Utility.PropHelper;

namespace CW.Website._parsers
{
    public class BuildingVariableParser : FileParserBase
    {
        #region const

            const String buildingVariableNotAssociated = "The existing building variable with ID of {0} is associated to {1} {2} but NOT associated to Equipment Class ID {3}.";
            const String entityTypeName ="Building to building class relationship";
            const String propertyName = "Building Class ID {0} and ";

        #endregion

        #region STATIC

            public static FieldDefBase[] FieldDefs;

            private readonly static String fnBID = PH.G<Building>(_ => _.BID);
            private readonly static String fnBuildingVariableDisplayNamee = PH.G<BuildingVariable>(_ => _.BuildingVariableDisplayName);
            private readonly static String fnValue = PH.G<Buildings_BuildingVariable>(_ => _.Value);

            static BuildingVariableParser()
            {
                FieldDefs = new FieldDefBase[] 
				{
					new FieldDef<Int32>("Building ID", fnBID, "required", GetFieldType(fnBID, typeof(Building))){IsDistinctionField=true,},
					new FieldDef<String>("Building Variable Display Name", fnBuildingVariableDisplayNamee, GetFieldType(fnBuildingVariableDisplayNamee, typeof(BuildingVariable)).ToLower() + " - required", String.Empty, 0, 50, 0, true){IsDistinctionField=true,}, //UIRequired
                    new FieldDef<String>(fnValue, fnValue, "required", GetFieldType(fnValue, typeof(BuildingVariable))),
				};
            }

        #endregion

        #region field

            private readonly DataManager mDataManager;
            private readonly ISectionLogManager mLogger;
            private readonly Func<Int32, Building> buildingGetter;
            private readonly Func<String, BuildingVariable> buildingVariableGetter;
            private readonly Func<Int32, Int32, Buildings_BuildingVariable> buildingBuildingVariableGetter;
            private readonly Func<Int32, BuildingClass> buildingClassGetter;

        #endregion

        #region constructor

            public BuildingVariableParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) : 
            base(fileContent, FieldDefs, typeof(Buildings_BuildingVariable), overrideExistingValues, userInfo)
            {
                this.mDataManager = dataManager;
                this.mLogger = logger;
                this.buildingGetter = (id) => mDataManager.BuildingDataMapper.GetBuildingByBID(id, false, false, false, false, false, false);
                this.buildingVariableGetter = (displayName) => mDataManager.BuildingVariableDataMapper.GetBuildingVariableWithEngUnitByName(displayName, true);
                this.buildingBuildingVariableGetter = (bid, bvid) => mDataManager.BuildingVariableDataMapper.GetBuildingVariableByBIDAndBVID(bid, bvid);
                this.buildingClassGetter = (id) => mDataManager.BuildingClassDataMapper.GetBuildingClassByBuildingTypeID(id);
            }

        #endregion

        #region methods

            #region override

                public override void ParseRow(String[] rowContent, Int32 rowIndex)
                {
                    var bid = GetIdFromParseValue(rowContent[LookupDict[fnBID]]);
                    var building = new Building();
                    var buildingVariable = new BuildingVariable();
                    var buildingError = GetEntityByID<Building>(rowIndex, bid, buildingGetter, out building);
                    var buildingVariableError = GetEntityByName<BuildingVariable>(rowIndex, rowContent[LookupDict[fnBuildingVariableDisplayNamee]], buildingVariableGetter, out buildingVariable);

                    if (buildingError != null) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnBID), rowContent[LookupDict[fnBID]]);
                    if (!IsValidCIDForClient(FieldDefs.Single(_ => _.FieldName == fnBuildingVariableDisplayNamee).DisplayName, rowIndex, building, PropHelper.G<Client>(_ => _.CID))) return;
                    if (buildingVariableError != null) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnBuildingVariableDisplayNamee), rowContent[LookupDict[fnBuildingVariableDisplayNamee]]);

                    if (buildingError == null && buildingVariableError == null)
                    {
                        var buildingBuildingVariable = new Buildings_BuildingVariable();
                        var buildingBuildingVariableError = GetEntityByIDs<Buildings_BuildingVariable>(rowIndex, building.BID, buildingVariable.BVID, buildingBuildingVariableGetter, out buildingBuildingVariable);

                        if (HasEntityExceptions(buildingBuildingVariable, rowIndex, FieldDefs, rowContent)) return;

                        SetEntityAndPrimaryIDToRow(buildingBuildingVariable, rowContent, PropHelper.G<Buildings_BuildingVariable>(_ => _.ID));

                        var buildingClass = new BuildingClass();
                        var buildingClassError = GetEntityByID<BuildingClass>(rowIndex, building.BuildingTypeID, buildingClassGetter, out buildingClass);

                        if (buildingClassError == null)
                        {
                            if (!mDataManager.BuildingVariableDataMapper.DoesBuildingVariableExistForBuildingClass(buildingClass.BuildingClassID, buildingVariable.BVID))
                            {
                                if (buildingVariable != null) GenerateInvalidGenericException(rowIndex, buildingVariableNotAssociated, buildingVariable.BVID, GetDisplayName(fnBID), building.BID, buildingClass.BuildingClassID);

                                var propertyValue = String.Format("{0} {1}", GetDisplayName(fnBuildingVariableDisplayNamee), rowContent[LookupDict[fnBuildingVariableDisplayNamee]]);

                                Exceptions.Add(new EntityNotFoundException(rowIndex, entityTypeName, String.Format(propertyName, buildingClass.BuildingClassID), propertyValue, "DoesBuildingVariableExistForBuildingClass"));
                            }
                        }
                    }
                }

                protected override void InsertRow(Row row)
                {
                    try
                    {
                        var buidlingVariable = CreateBuildingVariable(row);    

                        mDataManager.BuildingVariableDataMapper.InsertBuildingBuildingVariable(buidlingVariable);
                        InsertCount++;
                    }
                    catch (CW.Data.Exceptions.ValidationException ex)
                    {
                        Exceptions.Add(new InvalidGenericException(row.RowIndex, ex.Message));
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting building variable.", row.DataRow[LookupDict[fnBuildingVariableDisplayNamee]]); }
                }

                protected override void UpdateRow(Row row)
                {
                    var rowIndex = row.RowIndex;
                    var buildingVariableName = row.DataRow[LookupDict[fnBuildingVariableDisplayNamee]];

                    try
                    {
                        var buildingVariable = CreateBuildingVariable(row);

                        mDataManager.BuildingVariableDataMapper.UpdatePartialBuildingBuildingVariable(buildingVariable);
                        OverridesSB.AppendLine(String.Format("Row#&nbsp;{0} Updated Value for {1} {2} and {3} {4} to {5}<br />", rowIndex, GetDisplayName(fnBID), buildingVariable.BID, GetDisplayName(fnBuildingVariableDisplayNamee), buildingVariableName, row.DataRow[LookupDict[fnValue]]));
                    }
                    catch (CW.Data.Exceptions.ValidationException ex)
                    {
                        Exceptions.Add(new InvalidGenericException(rowIndex, ex.Message));
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, rowIndex, sqlEx, "Error updating building variable.", buildingVariableName); }
                }

            #endregion

            private Buildings_BuildingVariable CreateBuildingVariable(Row row)
            {
                var dataRow = row.DataRow;
                var buildingBuildingVariable = new Buildings_BuildingVariable();

                if (!OverrideExistingValues)
                {
                    buildingBuildingVariable.BID = Convert.ToInt32(dataRow[LookupDict[fnBID]]);
                    buildingBuildingVariable.BVID = mDataManager.BuildingVariableDataMapper.GetBuildingVariableByName(dataRow[LookupDict[fnBuildingVariableDisplayNamee]], true).BVID;
                }
                else
                    buildingBuildingVariable.ID = (Int32)row.PrimaryId;

                SetParserField(dataRow[LookupDict[fnValue]], fnValue, buildingBuildingVariable);

                return buildingBuildingVariable;
            }

        #endregion
    }
}