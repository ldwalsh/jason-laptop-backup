﻿using CW.Business;
using CW.Common.Constants;
using CW.Logging;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website.DependencyResolution;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using PH = CW.Utility.PropHelper;


namespace CW.Website._parsers
{
    public class BuildingsParser : FileParserBase
    {
        #region STATIC

            public static FieldDefBase[] FieldDefs;

            private readonly static String fnBuildingName = PH.G<Building>(_ => _.BuildingName);
            private readonly static String fnCountryAlpha2Code = PH.G<Building>(_ => _.CountryAlpha2Code);
            private readonly static String fnStateID = PH.G<Building>(_ => _.StateID);
            private readonly static String fnCity = PH.G<Building>(_ => _.City);
            private readonly static String fnAddress = PH.G<Building>(_ => _.Address);
            private readonly static String fnZip = PH.G<Building>(_ => _.Zip);
            private readonly static String fnTimeZone = PH.G<Building>(_ => _.TimeZone);
            private readonly static String fnPhone = PH.G<Building>(_ => _.Phone);
            private readonly static String fnSqft = PH.G<Building>(_ => _.Sqft);
            private readonly static String fnFloors = PH.G<Building>(_ => _.Floors);
            private readonly static String fnYearBuilt = PH.G<Building>(_ => _.YearBuilt);
            private readonly static String fnBuildingTypeID = PH.G<Building>(_ => _.BuildingTypeID);
            private readonly static String fnBuildingOwnership = PH.G<Building>(_ => _.BuildingOwnership);
            private readonly static String fnBuildingShape = PH.G<Building>(_ => _.BuildingShape);
            private readonly static String fnMainCoolingSystem = PH.G<Building>(_ => _.MainCoolingSystem);
            private readonly static String fnMainHeatingSystem = PH.G<Building>(_ => _.MainHeatingSystem);
            private readonly static String fnRoofConstruction = PH.G<Building>(_ => _.RoofConstruction);
            private readonly static String fnEnvelopeGlassPercentage = PH.G<Building>(_ => _.EnvelopeGlassPercentage);
            private readonly static String fnOperatingHours = PH.G<Building>(_ => _.OperatingHours);
            private readonly static String fnComputers = PH.G<Building>(_ => _.Computers);
            private readonly static String fnLocation = PH.G<Building>(_ => _.Location);
            private readonly static String fnDescription = PH.G<Building>(_ => _.Description);
            private readonly static String fnSetupContactName = PH.G<Building>(_ => _.SetupContactName);
            private readonly static String fnEnergyBureauContactName = PH.G<Building>(_ => _.EnergyBureauContactName);
            private readonly static String fnLatitude = PH.G<Building>(_ => _.Latitude);
            private readonly static String fnLongitude = PH.G<Building>(_ => _.Longitude);
            private readonly static String fnWeatherGFID = PH.G<Building>(_ => _.WeatherGFID);

            static BuildingsParser()
            {
                FieldDefs = new FieldDefBase[] 
				{
					//Make sure that the max value changes are included in the KGS Admin pages as well
					new FieldDef<String>("Building Name", fnBuildingName, "required and unique", GetFieldType(fnBuildingName, typeof(Building))){IsDistinctionField=true,},
					new FieldDef<String>("Country Alpha2 Code", fnCountryAlpha2Code, "required", GetFieldType(fnCountryAlpha2Code, typeof(Building))),
					new FieldDef<Int32>("State ID",fnStateID, "required if country has state/providences, else can be empty", GetFieldType(fnStateID, typeof(Building))),
					new FieldDef<String>("City", fnCity, "required", GetFieldType(fnCity, typeof(Building))),
					new FieldDef<String>("Address", fnAddress, "required", GetFieldType(fnAddress, typeof(Building))),
					new FieldDef<String>("Zip", fnZip, "required if country is united states, else can be empty", GetFieldType(fnZip, typeof(Building))),
					new FieldDef<String>("Time Zone", fnTimeZone, "required", GetFieldType(fnTimeZone, typeof(Building))),
					new FieldDef<String>("Phone", fnPhone, "can be empty, must be 10 digits with no other characters", GetFieldType(fnPhone, typeof(Building))),
					new FieldDef<Int32>("Sqft", fnSqft, "required, max 9 digits and must be greater than 0", GetFieldType(fnSqft, typeof(Building)), OperatorType.GreaterThan, 1000000),
					new FieldDef<Int32>("Floors", fnFloors, "can be empty, max 3 digits", GetFieldType(fnFloors, typeof(Building)), OperatorType.GreaterThan, 999),
					new FieldDef<Int32>("Year Built", fnYearBuilt, "can be empty, must be 4 digits", GetFieldType(fnYearBuilt, typeof(Building)), OperatorType.GreaterThan, 2200),
					new FieldDef<Int32>("Building Type ID", fnBuildingTypeID, "required", GetFieldType(fnBuildingTypeID, typeof(Building))),
					new FieldDef<String>("Building Ownership",fnBuildingOwnership, "can be empty, must be exact case sensitive spelling of dropdown option", GetFieldType(fnBuildingOwnership, typeof(Building))),
					new FieldDef<String>("Building Shape", fnBuildingShape, "can be empty, must be exact case sensitive spelling of dropdown option", GetFieldType(fnBuildingShape, typeof(Building))),
					new FieldDef<String>("Main Cooling System", fnMainCoolingSystem, "can be empty, must be exact case sensitive spelling of dropdown option", GetFieldType(fnMainCoolingSystem, typeof(Building))),
					new FieldDef<String>("Main Heating System", fnMainHeatingSystem, "can be empty, must be exact case sensitive spelling of dropdown option", GetFieldType(fnMainHeatingSystem, typeof(Building))),
					new FieldDef<String>("Roof Construction", fnRoofConstruction, "can be empty, must be exact case sensitive spelling of dropdown option", GetFieldType(fnRoofConstruction, typeof(Building))),
					new FieldDef<Int32>("Envelope Glass Percentage", fnEnvelopeGlassPercentage, "required, must be from 0 - 100", GetFieldType(fnEnvelopeGlassPercentage, typeof(Building)), OperatorType.GreaterThan, 100),
					new FieldDef<Int32>("Operating Hours", fnOperatingHours, "required, must be from 0 - 168", GetFieldType(fnOperatingHours, typeof(Building)), OperatorType.GreaterThan, 168),
					new FieldDef<Int32>("Computers", fnComputers, "required, must be from 0 - 10000", GetFieldType(fnComputers, typeof(Building)), OperatorType.GreaterThan, 10000),
					new FieldDef<String>("Location", fnLocation, "can be empty", GetFieldType(fnLocation, typeof(Building))),
					new FieldDef<String>("Description", fnDescription, "can be empty", GetFieldType(fnDescription, typeof(Building))),
					new FieldDef<String>("Setup Contact Name", fnSetupContactName, "can be empty", GetFieldType(fnSetupContactName, typeof(Building))),
					new FieldDef<String>("Energy Bureau Contact Name", fnEnergyBureauContactName, "can be empty", GetFieldType(fnEnergyBureauContactName, typeof(Building))),
					new FieldDef<Double>("Latitude", fnLatitude, "can be empty", GetFieldType(fnLatitude, typeof(Building))),
					new FieldDef<Double>("Longitude", fnLongitude, "can be empty", GetFieldType(fnLongitude, typeof(Building))),
                    new FieldDef<Int32>("Weather GFID", fnWeatherGFID, "can be empty", GetFieldType(fnWeatherGFID, typeof(Building)))
				};
            }

        #endregion

        #region field

            private readonly DataManager mDataManager;
            private readonly String[] mCountryAlpha2Codes;
            private readonly IEnumerable<State> mStates;
            private readonly ISectionLogManager mLogger;

        #endregion

        #region constructor

            public BuildingsParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) : base(fileContent, FieldDefs, typeof(Building), overrideExistingValues, userInfo)
            {
                this.mDataManager = dataManager;
                this.mLogger = logger ?? new NullSectionLogManager();
                this.mCountryAlpha2Codes = mDataManager.CountryDataMapper.GetAllCountries().Select(c => c.Alpha2Code).ToArray();
                this.mStates = mDataManager.StateDataMapper.GetAllStates().ToArray();
            }

        #endregion

        #region methods

            #region override

                public override void ParseRow(String[] rowContent, Int32 rowIndex)
                {
                    var building = mDataManager.BuildingDataMapper.GetBuildingByCIDAndBuildingName(userInfo.ClientId, rowContent[LookupDict[fnBuildingName]]); //TODO: maybe get all buildings for the client (constructor) and filter down to the item in memory?

                    if (HasEntityExceptions(building, rowIndex, FieldDefs, rowContent)) return;

                    SetEntityAndPrimaryIDToRow(building, rowContent, PropHelper.G<Building>(_ => _.BID));
                    
                    var hasBuildingOwnershipMatch = BusinessConstants.Building.BuildingOwnershipList.Where(bo => bo.ToLower() == rowContent[LookupDict[fnBuildingOwnership]].ToLower()).Any();
                    var hasBuildingShapeMatch = BusinessConstants.Building.BuildingShapesList.Where(bs => bs.ToLower() == rowContent[LookupDict[fnBuildingShape]].ToLower()).Any();
                    var hasMainCoolingSystemMatch = BusinessConstants.Building.MainCoolingSystemList.Where(mcs => mcs.ToLower() == rowContent[LookupDict[fnMainCoolingSystem]].ToLower()).Any();
                    var hasMainHeatingSystemMatch = BusinessConstants.Building.MainHeatingSystemList.Where(mhs => mhs.ToLower() == rowContent[LookupDict[fnMainHeatingSystem]].ToLower()).Any();
                    var hasRoofConstructionMatch = BusinessConstants.Building.RoofConstructionList.Where(rc => rc.ToLower() == rowContent[LookupDict[fnRoofConstruction]].ToLower()).Any();

                    if (!String.IsNullOrWhiteSpace(rowContent[LookupDict[fnBuildingOwnership]]) && !hasBuildingOwnershipMatch)
                        GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnBuildingOwnership), rowContent[LookupDict[fnBuildingOwnership]]);

                    if (!String.IsNullOrWhiteSpace(rowContent[LookupDict[fnBuildingShape]]) && !hasBuildingShapeMatch)
                        GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnBuildingShape), rowContent[LookupDict[fnBuildingShape]]);

                    if (!String.IsNullOrWhiteSpace(rowContent[LookupDict[fnMainCoolingSystem]]) && !hasMainCoolingSystemMatch)
                        GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnMainCoolingSystem), rowContent[LookupDict[fnMainCoolingSystem]]);

                    if (!String.IsNullOrWhiteSpace(rowContent[LookupDict[fnMainHeatingSystem]]) && !hasMainHeatingSystemMatch)
                        GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnMainHeatingSystem), rowContent[LookupDict[fnMainHeatingSystem]]);

                    if (!String.IsNullOrWhiteSpace(rowContent[LookupDict[fnRoofConstruction]]) && !hasRoofConstructionMatch)
                        GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnRoofConstruction), rowContent[LookupDict[fnRoofConstruction]]);

                    if ((!String.IsNullOrWhiteSpace(rowContent[LookupDict[fnLongitude]]) && String.IsNullOrWhiteSpace(rowContent[LookupDict[fnLatitude]])) ||
                        (String.IsNullOrWhiteSpace(rowContent[LookupDict[fnLatitude]]) && !String.IsNullOrWhiteSpace(rowContent[LookupDict[fnLongitude]])))
                        GenerateInvalidGenericException(rowIndex, "When uploading with Latitude/Longitude, please ensure both values are entered.");

                    var stateIDs = mStates.Select(s => s.StateID).ToArray();

                    if (!mCountryAlpha2Codes.Contains(rowContent[LookupDict[fnCountryAlpha2Code]])) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnCountryAlpha2Code), rowContent[LookupDict[fnCountryAlpha2Code]]);

                    if (stateIDs.Any() && !stateIDs.Contains(Convert.ToInt32(rowContent[LookupDict[fnStateID]]))) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnStateID), rowContent[LookupDict[fnStateID]]);

                    if (rowContent[LookupDict[fnCountryAlpha2Code]] == BusinessConstants.Countries.UnitedStatesAlpha2Code && (rowContent[LookupDict[fnZip]].Length != 5 || !rowContent[LookupDict[fnZip]].All(Char.IsDigit)))
                        GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnZip), rowContent[LookupDict[fnZip]]);

                    if (!TimeZoneInfo.GetSystemTimeZones().Where(tz => tz.Id == rowContent[LookupDict[fnTimeZone]]).Any()) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnTimeZone), rowContent[LookupDict[fnTimeZone]]);

                    var weatherGFID = String.IsNullOrEmpty(rowContent[LookupDict[fnWeatherGFID]]) || rowContent[LookupDict[fnWeatherGFID]].Equals("NULL") ? null : (Int32?)Convert.ToInt32(rowContent[LookupDict[fnWeatherGFID]]);
                    var weatherFile = (weatherGFID != null) ? mDataManager.GlobalFileDataMapper.GetGlobalFileItem<GlobalFilesLookup>((gfl => gfl.GFID == Convert.ToInt32(rowContent[LookupDict[fnWeatherGFID]])), (gfl => gfl)) : null;

                    if (weatherGFID != null && weatherFile == null) GenerateInvalidFieldValueException(rowIndex, GetDisplayName(fnWeatherGFID), weatherGFID.ToString());
                }

                protected override void InsertRow(Row row)
                {
                    try
                    {
                        var building = CreateBuilding(row);
                        var bidPrimaryKey = mDataManager.BuildingDataMapper.InsertBuildingAndReturnBID(building);
                        var mCountry = mDataManager.CountryDataMapper.GetCountryByAlpha2Code(building.CountryAlpha2Code);
                        var buildingSettings = new BuildingSetting();

                        //other default values are in the db as default values or bindings.
                        buildingSettings.BID = bidPrimaryKey;
                        buildingSettings.LCID = mCountry.DefaultLCID;
                        buildingSettings.UnitSystem = mCountry.DefaultUnitSystem;
                        buildingSettings.AutomatedPriorityDailyEmailsUserGroupsGUID = Guid.NewGuid();
                        buildingSettings.AutomatedPriorityMonthlyEmailsUserGroupsGUID = Guid.NewGuid();
                        buildingSettings.AutomatedPriorityWeeklyEmailsUserGroupsGUID = Guid.NewGuid();
                        buildingSettings.SubscriptionStartDate = DateTime.UtcNow;
                        buildingSettings.SubscriptionPlan = BusinessConstants.BuildingSettings.DefaultSubscriptionPlan;
                        buildingSettings.MaxPointLimit = 0;

                        mDataManager.BuildingDataMapper.InsertBuildingSettings(buildingSettings);
                        InsertCount++;
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting building.", row.DataRow[LookupDict[fnBuildingName]]); }
                }

                protected override void UpdateRow(Row row)
                {
                    var rowIndex = row.RowIndex;
                    var buildingName = row.DataRow[LookupDict[fnBuildingName]];

                    try
                    {
                        var buildingItem = CreateBuilding(row);
                    
                        mDataManager.BuildingDataMapper.UpdateBuildingForOverride(buildingItem);
                        OverridesSB.AppendLine(String.Format("Row#&nbsp;{0} Updated Values for Building ID {1} and {2} {3}<br />", rowIndex, buildingItem.BID, GetDisplayName(fnBuildingName), buildingName));
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, rowIndex, sqlEx, "Error updating building.", buildingName); }
                }

            #endregion

            private Building CreateBuilding(Row row)
            {
                var dataRow = row.DataRow;
                var building = new Building();
                var entityBuilding = ((Building)row.Entity);

                if (!OverrideExistingValues)
                {
                    building.CID = userInfo.ClientId;
                    building.BuildingName = dataRow[LookupDict[fnBuildingName]].Trim();
                }
                else
                    building.BID = (Int32)row.PrimaryId;

                SetParserFields(dataRow, building);

                building.IsActive = OverrideExistingValues ? entityBuilding.IsActive : true;
                building.IsVisible = OverrideExistingValues ? entityBuilding.IsVisible : true;

                SetLongitudeAndLatitude(dataRow, building);

                return building;
            }

            private void SetParserFields(String[] dataRow, Building building)
            {
                SetParserField(dataRow[LookupDict[fnCountryAlpha2Code]], fnCountryAlpha2Code, building);
                SetParserField(dataRow[LookupDict[fnStateID]], fnStateID, building);
                SetParserField(dataRow[LookupDict[fnCity]], fnCity, building);
                SetParserField(dataRow[LookupDict[fnAddress]], fnAddress, building);
                SetParserField(dataRow[LookupDict[fnZip]], fnZip, building);
                SetParserField(dataRow[LookupDict[fnTimeZone]], fnTimeZone, building);
                SetParserField(dataRow[LookupDict[fnPhone]], fnPhone, building);
                SetParserField(dataRow[LookupDict[fnSqft]], fnSqft, building);
                SetParserField(dataRow[LookupDict[fnFloors]], fnFloors, building);
                SetParserField(dataRow[LookupDict[fnYearBuilt]], fnYearBuilt, building);
                SetParserField(dataRow[LookupDict[fnBuildingTypeID]], fnBuildingTypeID, building);
                SetParserField(dataRow[LookupDict[fnBuildingOwnership]], fnBuildingOwnership, building);
                SetParserField(dataRow[LookupDict[fnBuildingShape]], fnBuildingShape, building);
                SetParserField(dataRow[LookupDict[fnMainCoolingSystem]], fnMainCoolingSystem, building);
                SetParserField(dataRow[LookupDict[fnMainHeatingSystem]], fnMainHeatingSystem, building);
                SetParserField(dataRow[LookupDict[fnRoofConstruction]], fnRoofConstruction, building);
                SetParserField(dataRow[LookupDict[fnEnvelopeGlassPercentage]], fnEnvelopeGlassPercentage, building);
                SetParserField(dataRow[LookupDict[fnOperatingHours]], fnOperatingHours, building);
                SetParserField(dataRow[LookupDict[fnComputers]], fnComputers, building);
                SetParserField(dataRow[LookupDict[fnLocation]], fnLocation, building);
                SetParserField(dataRow[LookupDict[fnDescription]], fnDescription, building);
                SetParserField(dataRow[LookupDict[fnSetupContactName]], fnSetupContactName, building);
                SetParserField(dataRow[LookupDict[fnEnergyBureauContactName]], fnEnergyBureauContactName, building);
                SetParserField(dataRow[LookupDict[fnWeatherGFID]], fnWeatherGFID, building);
            }

            private void SetLongitudeAndLatitude(String[] dataRow, Building building)
            {
                //Geocode (longitude/latitude) lookup can be done here, for either inserts or updates
                var longitude = dataRow[LookupDict[fnLongitude]];
                var latitude = dataRow[LookupDict[fnLatitude]];

                if (String.IsNullOrEmpty(longitude) && String.IsNullOrEmpty(latitude))
                {
                    var stateNameFormated = String.Empty;

                    if (!String.IsNullOrWhiteSpace(building.StateID.ToString())) stateNameFormated = ", " + StringHelper.ReplaceSpecialCharactersForQueryString(mDataManager.StateDataMapper.GetStateByID((Int32)building.StateID).StateName);

                    var coordinates = IoC.Resolve<BingMapsHelper>().GetGeocodeData(String.Format("{0}, {1}{2}, {3}",
                                                                                         StringHelper.ReplaceSpecialCharactersForQueryString(building.Address),
                                                                                         StringHelper.ReplaceSpecialCharactersForQueryString(building.City),
                                                                                         stateNameFormated,
                                                                                         building.CountryAlpha2Code));

                    if (coordinates != null)
                    {
                        SetParserField(coordinates[0].ToString(), fnLatitude, building);
                        SetParserField(coordinates[1].ToString(), fnLongitude, building);
                    }
                }
                else
                {
                    SetParserField(latitude, fnLatitude, building);
                    SetParserField(longitude, fnLongitude, building);
                }
            }

        #endregion
    }
}