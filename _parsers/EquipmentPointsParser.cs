﻿using CW.Business;
using CW.Data;
using CW.Logging;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using PH = CW.Utility.PropHelper;

namespace CW.Website._parsers
{
    public class EquipmentPointsParser : FileParserBase
    {
        #region const
            const String existingPointTypeExistsForNonEquipmentGroup = "{2} {3} existing point with the same type already exists for this equipment {0} {1}.";       
            const String notAssociatedWithSameBuilding = "{0} {1} is not associated with the same building as the provided {2} {3}.";
            const String containsSameValue = "{0} and {1} contain the same value {2}.";
            const String equipmentInDifferentBuildings = "{0} {1} and {2} {3} belong to equipment in different buildings.";
            const String pointTypeNotAllowedOnEquipmentType = "{2} {3} point type is not allowed on equipment {0} {1} of that type.";

        #endregion

        #region STATIC

            public static FieldDefBase[] FieldDefs, FieldDefsOverride;

            private readonly static String fnEID = PH.G<Equipment>(_ => _.EID);
            private readonly static String fnExistingEID = "Existing" + PH.G<Equipment>(_ => _.EID);
            private readonly static String fnPID = PH.G<Point>(_ => _.PID);

            static EquipmentPointsParser()
            {
                FieldDefs = new FieldDefBase[] 
				{
					new FieldDef<Int32>("EID", fnEID, "required", GetFieldType(fnEID, typeof(Equipment))){IsDistinctionField=true,},
					new FieldDef<Int32>("PID", fnPID, "required", GetFieldType(fnPID, typeof(Point))){IsDistinctionField=true,},
				};

                FieldDefsOverride = new FieldDefBase[] 
				{
					new FieldDef<Int32>("EID",fnEID, "required", GetFieldType(fnEID, typeof(Equipment))){IsDistinctionField=true,},
					new FieldDef<Int32>("Existing EID",	fnExistingEID, "required", GetFieldType(fnEID, typeof(Equipment)), 0, 0, 0, true){IsDistinctionField=true,},
					new FieldDef<Int32>("PID", fnPID, "required", GetFieldType(fnPID, typeof(Point))){IsDistinctionField=true,},
				};
            }

        #endregion

        #region field

            private readonly DataManager mDataManager;
            private readonly ISectionLogManager mLogger;
            private readonly List<Int32> pids;

        #endregion

        #region constructor

            public EquipmentPointsParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) : 
            base(fileContent, (overrideExistingValues) ? FieldDefsOverride : FieldDefs, typeof(Equipment_Point), overrideExistingValues, userInfo)
            {
                this.mDataManager = dataManager;
                this.mLogger = logger ?? new NullSectionLogManager();
                this.pids = mDataManager.PointDataMapper.GetAllPointsByCID(SiteUser.Current.CID).Select(p => p.PID).ToList();
            }

        #endregion

        #region methods

            #region override

                public override void ParseRow(String[] rowContent, Int32 rowIndex)
                {
                    var fieldDefs = OverrideExistingValues ? FieldDefsOverride : FieldDefs;
                    var newEID = GetIdFromParseValue(rowContent[LookupDict[fnEID]]);
                    var existingEID = OverrideExistingValues ? GetIdFromParseValue(rowContent[LookupDict[fnExistingEID]]) : 0;
                    var pid = GetIdFromParseValue(rowContent[LookupDict[fnPID]]);
                    var newEquipment = GetEquipmentById(newEID);
                    var existingEquipment = new Equipment();
                    var existingEquipmentPoint = new Equipment_Point();

                    if (!IsEntityValid(newEquipment, rowIndex, fieldDefs, rowContent, fnEID)) return;
                    if (!IsValidPIDForClient(rowIndex, pids, pid, fnPID)) return;

                    var newEquipmentPoint = mDataManager.EquipmentPointsDataMapper.GetEquipmentPointByEIDAndPID(newEID, pid);

                    if (OverrideExistingValues)
                    {
                        existingEquipment = GetEquipmentById(existingEID);

                        if (!IsEntityValid(existingEquipment, rowIndex, fieldDefs, rowContent, fnExistingEID)) return;

                        if (newEquipmentPoint != null)
                        {
                            Exceptions.Add(new EntryAlreadyExistsException(rowIndex, GetDistinctionFields(FieldDefs, rowContent)));
                            return;
                        }

                        existingEquipmentPoint = mDataManager.EquipmentPointsDataMapper.GetEquipmentPointByEIDAndPID(existingEID, pid);

                        SetEntityAndPrimaryIDToRow(existingEquipmentPoint, rowContent, PropHelper.G<Equipment_Point>(_ => _.EPID));

                        OverrideChecks(rowContent, rowIndex, existingEID, newEID);                    
                    }

                    var criteria = OverrideExistingValues ? new Func<FieldDefBase, Boolean>(fd => fd.FieldName == fnExistingEID || fd.FieldName == fnPID) : new Func<FieldDefBase, Boolean>(fd => fd.FieldName == fnEID || fd.FieldName == fnPID);
                    
                    if (HasEntityExceptions((OverrideExistingValues ? existingEquipmentPoint : newEquipmentPoint), rowIndex, fieldDefs, rowContent, criteria)) return;
                    
                    InsertOrUpdateChecks(rowContent, rowIndex, newEID, pid, newEquipment.BID);

                    if (Exceptions.Count == 0)
                    {
                        UpdateBIDList(newEquipment.BID);

                        if (OverrideExistingValues) UpdateBIDList(existingEquipment.BID);
                    }
                }

                protected override void InsertRow(Row row)
                {
                    try
                    {
                        var equipmentPoint = CreateEquipmentPoint(row);

                        mDataManager.EquipmentPointsDataMapper.InsertEquipment_Point(equipmentPoint);
                        InsertCount++;
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting equipment point.", row.DataRow[LookupDict[fnEID]]); }
                }

                protected override void UpdateRow(Row row)
                {
                    var rowIndex = row.RowIndex;
                    var equipmentPoint = CreateEquipmentPoint(row);

                    try
                    {
                        mDataManager.EquipmentPointsDataMapper.UpdateEquipment_Point(equipmentPoint);
                        OverridesSB.AppendLine(String.Format("Row#&nbsp;{0} Updated Values for {1} {2} and {3} {4}<br />", rowIndex, GetDisplayName(fnEID), equipmentPoint.EID, GetDisplayName(fnPID), equipmentPoint.PID));
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, rowIndex, sqlEx, "Error updating equipment point.", equipmentPoint.EID.ToString()); }
                }

            #endregion

            private Equipment GetEquipmentById(Int32 eid)
            {
                return mDataManager.EquipmentDataMapper.GetEquipmentByEID(eid, false, true, false, false, false, false);
            }

            private Boolean IsEntityValid(Equipment equipment, Int32 rowIndex, FieldDefBase[] fieldDefs, String[] rowContent, String fieldName)
            {
                if (equipment == null)
                {
                    GenerateInvalidGenericException(rowIndex, invalidGenericExceptionFormat, GetDisplayName(fieldName), rowContent[LookupDict[fieldName]], GetDisplayName(fieldName));
                    return false;
                }
                else
                    if (!IsValidCIDForClient(fieldDefs.Single(_ => _.FieldName == fieldName).DisplayName, rowIndex, equipment, equipment.Building.CID)) return false;

                return true;
            }

            private void InsertOrUpdateChecks(String[] rowContent, Int32 rowIndex, Int32 eid, Int32 pid, Int32 bid)
            {
                //Check if equipment is of class 'group', if so allow multiple of the same point types to be added
                var paramFields = new Object[] { GetDisplayName(fnEID), eid, GetDisplayName(fnPID), pid };

                //check if points point type is allowed for equipments equipment type
                if (!mDataManager.EquipmentTypeDataMapper.IsPointsTypeAssociatedWithEquipmentsType(pid, eid))
                {
                    GenerateInvalidGenericException(rowIndex, pointTypeNotAllowedOnEquipmentType, paramFields);
                    return;
                }

                var pointTypeExistsForNonEquipmentGroup = false;                
                var isEquipmentGroup = mDataManager.EquipmentDataMapper.IsEquipmentAssociatedWithClassGroup(eid);                
                var isPointAssociatedWithPointTypeUnassigned = mDataManager.PointDataMapper.IsPointAssociatedWithTypeUnassigned(pid);

                //If not a group and not unassigned, check if a point with the same point type already exists for equipmet not of class group
                if (!isEquipmentGroup && !isPointAssociatedWithPointTypeUnassigned)
                    pointTypeExistsForNonEquipmentGroup = mDataManager.EquipmentPointsDataMapper.DoesPointTypeExistForEquipmentNotOfClassGroup(eid, mDataManager.PointDataMapper.GetPointByPID(pid).PointTypeID);

                //if equipment is group continue, or if pointtype doesnt exist on equipment not of class group, or if adding an unassigned point                   
                if (!(isEquipmentGroup || !pointTypeExistsForNonEquipmentGroup || isPointAssociatedWithPointTypeUnassigned))
                {
                    GenerateInvalidGenericException(rowIndex, existingPointTypeExistsForNonEquipmentGroup, paramFields);
                }

                //TODO: Maybe allow cross buildings soon
                if (bid != 0)
                {
                    var associatedEquipment = mDataManager.EquipmentDataMapper.GetAllEquipmentAssociatedWithPID(pid).First();

                    if (associatedEquipment.BID != bid) GenerateInvalidGenericException(rowIndex, notAssociatedWithSameBuilding, paramFields);
                }                                
            }

            private void OverrideChecks(String[] rowContent, Int32 rowIndex, Int32 existingEID, Int32 eid)
            {
                if (existingEID != 0 && eid == existingEID)
                {
                    GenerateInvalidGenericException(rowIndex, containsSameValue, GetDisplayName(fnEID), GetDisplayName(fnExistingEID), eid);
                    return;
                }
                else
                {
                    var equipmentItems = mDataManager.EquipmentDataMapper.GetAllEquipmentByEIDs(new Int32[] { existingEID, eid });

                    //TODO: Maybe allow cross buildings soon
                    if (equipmentItems != null && equipmentItems.Count() == 2 && (equipmentItems.First().BID != equipmentItems.Last().BID))
                        GenerateInvalidGenericException(rowIndex, equipmentInDifferentBuildings, GetDisplayName(fnExistingEID), existingEID, GetDisplayName(fnEID), eid);
                }
            }

            private Equipment_Point CreateEquipmentPoint(Row row)
            {
                var dataRow = row.DataRow;
                var equipmentPoint = new Equipment_Point();

                if (OverrideExistingValues) equipmentPoint.EPID = (Int32)row.PrimaryId;

                SetParserField(dataRow[LookupDict[fnEID]], fnEID, equipmentPoint);
                SetParserField(dataRow[LookupDict[fnPID]], fnPID, equipmentPoint);
                
                return equipmentPoint;
            }

        #endregion
    }
}