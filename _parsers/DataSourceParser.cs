﻿using CW.Business;
using CW.Data;
using CW.Logging;
using CW.Utility;
using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using PH = CW.Utility.PropHelper;

namespace CW.Website._parsers
{
    public class DataSourceParser : FileParserBase
    {
        #region STATIC

            public static FieldDefBase[] FieldDefs;

            readonly static string fnDataSourceName = PH.G<DataSource>(_ => _.DataSourceName);
            readonly static string fnIPList = PH.G<DataSource>(_ => _.IPList);
            readonly static string fnReferenceID = PH.G<DataSource>(_ => _.ReferenceID);
            readonly static string fnVendorProductID = PH.G<DataSource>(_ => _.VendorProductID);
            readonly static string fnDescription = PH.G<DataSource>(_ => _.Description);
            readonly static string fnLocation = PH.G<DataSource>(_ => _.Location);
            readonly static string fnTimeOffset = PH.G<DataSource>(_ => _.TimeOffset);
            readonly static string fnInstaller = PH.G<DataSource>(_ => _.Installer);
            readonly static string fnVendorProductRetrievalMethodID = PH.G<DataSource>(_ => _.VendorProductRetrievalMethodID);
            readonly static string fnPrimaryContactID = PH.G<DataSource>(_ => _.PrimaryContactID);
            readonly static string fnSecondaryContactID = PH.G<DataSource>(_ => _.SecondaryContactID);
            readonly static string fnDTSID = PH.G<DataSource>(_ => _.DTSID);
            readonly static string fnBMSInfoID = PH.G<DataSource>(_ => _.BMSInfoID);

            static DataSourceParser()
            {
                FieldDefs = new FieldDefBase[]
				{
					new FieldDef<string>("Data Source Name",fnDataSourceName, "required and unique", GetFieldType(fnDataSourceName, typeof(DataSource))){IsDistinctionField=true,},
					new FieldDef<string>("IP List",	fnIPList, "can be empty, csv with ip format, ip or dtsid requried", GetFieldType(fnIPList, typeof(DataSource))),
					new FieldDef<string>("Reference ID", fnReferenceID, "required", GetFieldType(fnReferenceID, typeof(DataSource))),
					new FieldDef<int>("Vendor Product ID", fnVendorProductID, "required", GetFieldType(fnVendorProductID, typeof(DataSource))),
					new FieldDef<string>("Description",	fnDescription, "can be empty", GetFieldType(fnDescription, typeof(DataSource))),
					new FieldDef<string>("Location", fnLocation, "can be empty", GetFieldType(fnLocation, typeof(DataSource))),
					new FieldDef<int>("Time Offset", fnTimeOffset, "required, default value is 0", GetFieldType(fnTimeOffset, typeof(DataSource))),
					new FieldDef<string>("Installer", fnInstaller, "can be empty", GetFieldType(fnInstaller, typeof(DataSource))),
					new FieldDef<int>("Vendor Product Retrieval Method ID", fnVendorProductRetrievalMethodID, "required", GetFieldType(fnVendorProductRetrievalMethodID, typeof(DataSource))),
					new FieldDef<int>("Primary Contact ID", fnPrimaryContactID, "can be empty", GetFieldType(fnPrimaryContactID, typeof(DataSource))),
					new FieldDef<int>("Secondary Contact ID", fnSecondaryContactID, "can be empty", GetFieldType(fnSecondaryContactID, typeof(DataSource))),
                    new FieldDef<int>("Data Transfer Services ID", fnDTSID, "can be empty, ip or dtsid requried", GetFieldType(fnDTSID, typeof(DataSource))),
                    new FieldDef<int>("BMS Info ID", fnBMSInfoID, "can be empty", GetFieldType(fnBMSInfoID, typeof(DataSource)))
                };
            }

        #endregion

        #region field

            readonly DataManager mDataManager;
            readonly ISectionLogManager mLogger;
            List<string> mDataTransferServiceGUIDList;
            //IDictionary<string, DataTransferServiceLookup> mClientDataTransferServiceGuidDataStoreLookup;
            List<RefIDAndIdentifier> mPreviousRefIDAndIdentifierCollection;
            List<string> mBMSInfoIDParsedList;
            //IDictionary<string, int> mClientBMSInfoIDDataStoreLookup;
        
        #endregion

        #region constructor

            public DataSourceParser(Stream fileContent, DataManager dataManager, bool overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) : 
            base(fileContent, FieldDefs, typeof(DataSource), overrideExistingValues, userInfo)
            {
                mDataManager = dataManager;

                mLogger = logger ?? new NullSectionLogManager();

                mDataTransferServiceGUIDList = new List<string>();

                mBMSInfoIDParsedList = new List<string>();

                mPreviousRefIDAndIdentifierCollection = new List<RefIDAndIdentifier>();
            }

        #endregion

        #region methods

            #region override

                public override void SetIDsFromInput(string[] rowContent) { }

                public override void SetDataFromIDLists()
                {
                    // DataSource by cid and data source name
                    //mDataManager.DataSourceDataMapper.GetDataSourceByCIDAndDataSourceName(userInfo.ClientId, rowContent[LookupDict[fnDataSourceName]]);

                    // DataSourceVendorProducts_DataSourceRetrievalMethod by fnVendorProductRetrievalMethodID
                    //mDataManager.DataSourceVendorProductDataMapper.GetDataSourceVendorProductRetrievalMethodByID(id));

                    // DataSourceVendorContact by fnPrimaryContactID a
                    //mDataManager.DataSourceVendorContactDataMapper.GetContact(id));
                    
                    // DataSourceVendorContact by fnSecondaryContactID
                    //mDataManager.DataSourceVendorContactDataMapper.GetContact(id));

                    //mDataManager.DataSourceDataMapper.DoesAnotherDataSourceReferenceIDAndIPExist(refID, ipList, dsid)
                }
                
                public override void ParseRow(string[] rowContent, int rowIndex)
                {
                    var dataSource = mDataManager.DataSourceDataMapper.GetDataSourceByCIDAndDataSourceName(userInfo.ClientId, rowContent[LookupDict[fnDataSourceName]]);

                    if (HasEntityExceptions(dataSource, rowIndex, FieldDefs, rowContent)) return;

                    SetEntityAndPrimaryIDToRow(dataSource, rowContent, PH.G<DataSource>(_ => _.DSID));

                    if (string.IsNullOrWhiteSpace(rowContent[LookupDict[fnDTSID]]) && string.IsNullOrWhiteSpace(rowContent[LookupDict[fnIPList]]))
                    {
                        GenerateFieldIsRequiredException(rowIndex, GetDisplayName(fnIPList) + " or " + GetDisplayName(fnDTSID));
                        return;
                    }

                    CheckForeignKeyField(rowIndex, rowContent, fnVendorProductID, (id) => mDataManager.DataSourceVendorProductDataMapper.GetVendorProduct(id));
                    
                    CheckForeignKeyFieldCascading<DataSourceVendorProducts_DataSourceRetrievalMethod>(rowIndex, rowContent, fnVendorProductID, fnVendorProductRetrievalMethodID, (id, id2) => mDataManager.DataSourceVendorProductDataMapper.GetDataSourceVendorProductsRetrievalMethodsByProductID(id).Where(vp => vp.VendorProductRetrievalMethodID == id2));

                    CheckForeignKeyField(rowIndex, rowContent, fnPrimaryContactID, (id) => mDataManager.DataSourceVendorContactDataMapper.GetContact(id));
            
                    CheckForeignKeyField(rowIndex, rowContent, fnSecondaryContactID, (id) => mDataManager.DataSourceVendorContactDataMapper.GetContact(id));

                    if (!string.IsNullOrWhiteSpace(rowContent[LookupDict[fnDTSID]])) CheckForeignKeyField(rowIndex, rowContent, fnDTSID, (id) => mDataManager.DataTransferServiceDataMapper.DoesDTSExist(id));

                    if (!string.IsNullOrWhiteSpace(rowContent[LookupDict[fnBMSInfoID]])) CheckForeignKeyField(rowIndex, rowContent, fnBMSInfoID, (id) => mDataManager.BMSInfoDataMapper.DoesBMSInfoExist(id));

                    var str = StringHelper.ParseCSVString(rowContent[LookupDict[fnIPList]].Trim());

                    if (str == null) str = new string[] {};

                    foreach (var s in str)
                    {
                        if (!StringHelper.ValidateIPAddress(s)) Exceptions.Add(new InvalidFieldValueException(rowIndex, fnIPList, s));
                    }
    
                    if (DoesRecordExist(rowContent, rowIndex)) return;
                }

                protected override void InsertRow(Row row)
                {
                    try
                    {
                        var dataSource = CreateDataSource(row);

                        mDataManager.DataSourceDataMapper.InsertDataSource(dataSource);

                        InsertCount++;
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting data source.", row.DataRow[LookupDict[fnDataSourceName]]); }
                }

                protected override void UpdateRow(Row row)
                {
                    var rowIndex = row.RowIndex;

                    var dataSourceName = row.DataRow[LookupDict[fnDataSourceName]];

                    try
                    {
                        var dataSource = CreateDataSource(row);

                        mDataManager.DataSourceDataMapper.UpdateDataSource(dataSource);

                        OverridesSB.AppendLine($"Row#&nbsp;{rowIndex} Updated Values for {GetDisplayName(fnDataSourceName)} {dataSourceName}<br />");
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error updating data source.", dataSourceName); }
                }

            #endregion

            bool DoesRecordExist(string[] rowContent, int rowIndex)
            {
                var refID = StringHelper.TrimLeadingTrailingWhiteSpace(rowContent[LookupDict[fnReferenceID]]);

                var ipList = StringHelper.ParseCSVString(StringHelper.TrimLeadingTrailingWhiteSpace(rowContent[LookupDict[fnIPList]]));
                var dtsid = StringHelper.TrimLeadingTrailingWhiteSpace(rowContent[LookupDict[fnDTSID]]);

                if (ipList != null)
                {
                    var fieldNamesAndVals = new KeyValuePair<string, string>[]
                    {
                            new KeyValuePair<string, string>(GetDisplayName(fnReferenceID), rowContent[LookupDict[fnReferenceID]]),
                            new KeyValuePair<string, string>(GetDisplayName(fnIPList), rowContent[LookupDict[fnIPList]])
                    };

                    //if update, check if another record has reference id / ip pair.
                    if (OverrideExistingValues) return HasExistingReferenceIdAndIPForInsert(rowContent, refID, ipList, rowIndex, fieldNamesAndVals);

                    //if insert, check if reference id / ip pair exists.
                    if (HasExistingReferenceIdAndIPForUpdate(refID, ipList, rowIndex, fieldNamesAndVals)) return true;

                    mPreviousRefIDAndIdentifierCollection.Add(new RefIDAndIdentifier { RefID = refID, Identifier = rowContent[LookupDict[fnIPList]] });
                }
                else
                {
                    var fieldNamesAndVals = new KeyValuePair<string, string>[]
                    {
                            new KeyValuePair<string, string>(GetDisplayName(fnReferenceID), rowContent[LookupDict[fnReferenceID]]),
                            new KeyValuePair<string, string>(GetDisplayName(fnDTSID), rowContent[LookupDict[fnDTSID]])
                    };
            
                    //if update, check if another record has reference id / dtsid pair.
                    if (OverrideExistingValues) return HasExistingReferenceIdAndDTSIDForInsert(rowContent, refID, Convert.ToInt32(dtsid), rowIndex, fieldNamesAndVals);

                    //if insert, check if reference id / dtsid pair exists.
                    if (HasExistingReferenceIdAndDTSIDForUpdate(refID, Convert.ToInt32(dtsid), rowIndex, fieldNamesAndVals)) return true;

                    mPreviousRefIDAndIdentifierCollection.Add(new RefIDAndIdentifier { RefID = refID, Identifier = dtsid });
                }               

                return false;
            }

            bool HasExistingReferenceIdAndIPForInsert(string[] rowContent, string refId, string[] ipList, int rowIndex, KeyValuePair<string, string>[] fieldNamesAndVals)
            {
                var key = CreateUniqueIdenString(rowContent);

                if (rows.ContainsKey(key))
                {
                    var dsid = (int)rows[key].PrimaryId;

                    if (mDataManager.DataSourceDataMapper.DoesAnotherDataSourceReferenceIDAndIPExist(refId, ipList, dsid))
                    {
                        Exceptions.Add(new EntryAlreadyExistsException(rowIndex, fieldNamesAndVals));

                        return true;
                    }
                }

                return false;
            }

            bool HasExistingReferenceIdAndDTSIDForInsert(string[] rowContent, string refId, int dtsid, int rowIndex, KeyValuePair<string, string>[] fieldNamesAndVals)
            {
                var key = CreateUniqueIdenString(rowContent);

                if (rows.ContainsKey(key))
                {
                    var dsid = (int)rows[key].PrimaryId;

                    if (mDataManager.DataSourceDataMapper.DoesAnotherDataSourceReferenceIDAndDTSIDExist(refId, dtsid, dsid))
                    {
                        Exceptions.Add(new EntryAlreadyExistsException(rowIndex, fieldNamesAndVals));

                        return true;
                    }
                }

                return false;
            }

            bool HasExistingReferenceIdAndIPForUpdate(string refId, string[] ipList, int rowIndex, KeyValuePair<string, string>[] fieldNamesAndVals)
            {
                if (CheckPreviousRefIDAndIPCollection(refId, ipList))
                {
                    mPreviousRefIDAndIdentifierCollection.Add(new RefIDAndIdentifier { RefID = refId, Identifier = StringHelper.ParseStringArrayToCSVString(ipList) }); //still need to add to collection

                    Exceptions.Add(new DuplicateEntryException(rowIndex, fieldNamesAndVals));

                    return true;
                }
                else if (mDataManager.DataSourceDataMapper.DoesDataSourceReferenceIDAndIPExist(refId, ipList))
                {
                    mPreviousRefIDAndIdentifierCollection.Add(new RefIDAndIdentifier { RefID = refId, Identifier = StringHelper.ParseStringArrayToCSVString(ipList) }); //still need to add to collection

                    Exceptions.Add(new EntryAlreadyExistsException(rowIndex, fieldNamesAndVals));

                    return true;
                }

                return false;
            }

            bool HasExistingReferenceIdAndDTSIDForUpdate(string refId, int dtsid, int rowIndex, KeyValuePair<string, string>[] fieldNamesAndVals)
            {
                if (CheckPreviousRefIDAndDTSIDCollection(refId, dtsid))
                {
                    mPreviousRefIDAndIdentifierCollection.Add(new RefIDAndIdentifier { RefID = refId, Identifier = dtsid.ToString() });

                    Exceptions.Add(new DuplicateEntryException(rowIndex, fieldNamesAndVals));

                    return true;
                }
                else if (mDataManager.DataSourceDataMapper.DoesDataSourceReferenceIDAndDTSIDExist(refId, dtsid))
                {
                    mPreviousRefIDAndIdentifierCollection.Add(new RefIDAndIdentifier { RefID = refId, Identifier = dtsid.ToString() });

                    Exceptions.Add(new EntryAlreadyExistsException(rowIndex, fieldNamesAndVals));

                    return true;
                }

                return false;
            }

            DataSource CreateDataSource(Row row)
            {
                var dataRow = row.DataRow;

                var dataSource = new DataSource();

                var entityDataSource = ((DataSource)row.Entity);

                if (!OverrideExistingValues)
                {
                    dataSource.CID = userInfo.ClientId;

                    dataSource.DataSourceName = StringHelper.TrimLeadingTrailingWhiteSpace(dataRow[LookupDict[fnDataSourceName]]);
                }
                else
                    dataSource.DSID = (int)row.PrimaryId;
                
                SetParserField(dataRow[LookupDict[fnIPList]], fnIPList, dataSource);
                SetParserField(dataRow[LookupDict[fnReferenceID]], fnReferenceID, dataSource);
                SetParserField(dataRow[LookupDict[fnVendorProductID]], fnVendorProductID, dataSource);
                SetParserField(dataRow[LookupDict[fnDescription]], fnDescription, dataSource);
                SetParserField(dataRow[LookupDict[fnLocation]], fnLocation, dataSource);
                SetParserField(dataRow[LookupDict[fnTimeOffset]], fnTimeOffset, dataSource);
                SetParserField(dataRow[LookupDict[fnInstaller]], fnInstaller, dataSource);
                SetParserField(dataRow[LookupDict[fnVendorProductRetrievalMethodID]], fnVendorProductRetrievalMethodID, dataSource);
                SetParserField(dataRow[LookupDict[fnPrimaryContactID]], fnPrimaryContactID, dataSource);
                SetParserField(dataRow[LookupDict[fnSecondaryContactID]], fnSecondaryContactID, dataSource);
                SetParserField(dataRow[LookupDict[fnDTSID]], fnDTSID, dataSource);
                SetParserField(dataRow[LookupDict[fnBMSInfoID]], fnBMSInfoID, dataSource);

                dataSource.IsIntegrated = OverrideExistingValues ? entityDataSource.IsIntegrated : false;
                dataSource.IsActive = OverrideExistingValues ? entityDataSource.IsActive : true;
                dataSource.IsRealtime = OverrideExistingValues ? entityDataSource.IsRealtime : true;

                return dataSource;
            }

            bool CheckPreviousRefIDAndIPCollection(string refID, string[] ipList)
            {
                var existingRefIds = mPreviousRefIDAndIdentifierCollection.Where(r => r.RefID == refID);

                foreach (RefIDAndIdentifier item in existingRefIds)
                {
                    foreach (string ip in ipList)
                    {
                        if (Array.IndexOf(item.Identifier.Split(','), ip) != -1) return true;
                    }
                }

                return false;
            }
            bool CheckPreviousRefIDAndDTSIDCollection(string refID, int dtsid)
            {
                var existingRefIds = mPreviousRefIDAndIdentifierCollection.Where(r => r.RefID == refID);

                foreach (RefIDAndIdentifier item in existingRefIds)
                {                    
                    if (item.Identifier == dtsid.ToString()) return true;                    
                }

                return false;
            }

            class RefIDAndIdentifier
            {
                public string RefID { get; set; }
                public string Identifier { get; set; }
            }

        #endregion
    }
}