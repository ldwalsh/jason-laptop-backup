﻿using CW.Utility.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using CW.Business;
using CW.Logging;
using CW.Data;
using PH = CW.Utility.PropHelper;
using CW.Utility;
using System.Data.SqlClient;
using System.Configuration;

namespace CW.Website._parsers
{
    public class BMSInfoParser : FileParserBase
    {
        #region STATIC
            public static FieldDefBase[] FieldDefs;

            private readonly static String fnBMSInfoName = PH.G<BMSInfo>(_ => _.BMSInfoName);
            private readonly static String fnEndpoint = PH.G<BMSInfo>(_ => _.Endpoint);
            private readonly static String fnUsername = PH.G<BMSInfo>(_ => _.Username);
            private readonly static String fnPassword = PH.G<BMSInfo>(_ => _.Password);
            private readonly static String fnExtraInfo = PH.G<BMSInfo>(_ => _.ExtraInfo);            

        static BMSInfoParser()
            {
                FieldDefs = new FieldDefBase[]
                {
                    new FieldDef<String>("BMS Info Name", fnBMSInfoName, "required and unique", GetFieldType(fnBMSInfoName, 
                        typeof(BMSInfo))) {IsDistinctionField=true }, //what is isdistinctionfield? PK?
                    new FieldDef<String>("Endpoint", fnEndpoint, "required", GetFieldType(fnEndpoint, typeof(BMSInfo))), 
                    new FieldDef<String>("Username", fnUsername, "can be empty", GetFieldType(fnUsername, typeof(BMSInfo))), 
                    new FieldDef<String>("Password", fnPassword, "can be empty", GetFieldType(fnPassword, typeof(BMSInfo))), 
                    new FieldDef<String>("Extra Info", fnExtraInfo, "can be empty", GetFieldType(fnExtraInfo, typeof(BMSInfo)))
                }; 
            }

        #endregion

        #region fields

            private readonly DataManager mDataManager;
            private readonly String[] mCountryAlpha2Codes;
            private readonly IEnumerable<State> mStates;
            private readonly ISectionLogManager mLogger;

        #endregion

        #region constructor

        public BMSInfoParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, 
            ISectionLogManager logger, UserInfo userInfo) : base(fileContent, FieldDefs, typeof(BMSInfo), overrideExistingValues, userInfo)
        {
            this.mDataManager = dataManager;
            this.mLogger = logger ?? new NullSectionLogManager();
            this.mCountryAlpha2Codes = mDataManager.CountryDataMapper.GetAllCountries().Select(c => c.Alpha2Code).ToArray();
            this.mStates = mDataManager.StateDataMapper.GetAllStates().ToArray();
        }

        #endregion

        public override void ParseRow(String[] rowContent, Int32 rowIndex)
        {
            var bmsInfo = mDataManager.BMSInfoDataMapper.GetBMSInfoByCIDAndBMSInfoName(userInfo.ClientId, rowContent[LookupDict[fnBMSInfoName]]);

            if (HasEntityExceptions(bmsInfo, rowIndex, FieldDefs, rowContent)) return;            

            SetEntityAndPrimaryIDToRow(bmsInfo, rowContent, PropHelper.G<BMSInfo>(_ => _.BMSInfoID));
        }

        protected override void InsertRow(Row row)
        {
            try
            {
                var bmsInfo = createBMSInfo(row);

                if (mDataManager.BMSInfoDataMapper.DoesBMSInfoNameExistForClient(userInfo.ClientId, bmsInfo.BMSInfoName, bmsInfo.BMSInfoID))
                {
                    Exceptions.Add(new DuplicateEntryException(row.RowIndex, new List<KeyValuePair<String, String>>
                        { new System.Collections.Generic.KeyValuePair<String, String>(((BMSInfo)bmsInfo).BMSInfoName, " ")}));
                    return;
                }
                var bmsIDPrimaryKey = mDataManager.BMSInfoDataMapper.InsertBMSInfo(bmsInfo, ConfigurationManager.AppSettings["Paraphrase"]);
                InsertCount++;
            }
            catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting BMSInfo.", 
                row.DataRow[LookupDict[fnBMSInfoName]]); }
        }

        protected override void UpdateRow(Row row)
        {
            var rowIndex = row.RowIndex;
            var bmsInfoName = row.DataRow[LookupDict[fnBMSInfoName]];

            try
            {
                var bmsInfoItem = createBMSInfo(row);

                mDataManager.BMSInfoDataMapper.UpdateBMSInfo(bmsInfoItem, ConfigurationManager.AppSettings["Paraphrase"]);
                OverridesSB.AppendLine(String.Format("Row#&nbsp;{0} Updated Values for BMSInfo ID {1} and {2} {3}<br />", 
                    rowIndex, bmsInfoItem.BMSInfoID, GetDisplayName(fnBMSInfoName), bmsInfoName));                
            }
            catch(SqlException sqlEx) { LogSQLError(mLogger, rowIndex, sqlEx, "Error updating bmsInfo.", bmsInfoName); }
        }

        private BMSInfo createBMSInfo(Row row)
        {
            var dataRow = row.DataRow;
            var bmsInfo = new BMSInfo();            

            if (!OverrideExistingValues)
            {
                bmsInfo.CID = userInfo.ClientId;
                bmsInfo.BMSInfoName = dataRow[LookupDict[fnBMSInfoName]].Trim();
            }
            else
                bmsInfo.BMSInfoID =  (Int32) row.PrimaryId;

            SetParserFields(dataRow, bmsInfo);

            return bmsInfo;
        }

        private void SetParserFields(String[] dataRow, BMSInfo bmsInfo)
        {
            SetParserField(dataRow[LookupDict[fnEndpoint]], fnEndpoint, bmsInfo);
            SetParserField(dataRow[LookupDict[fnUsername]], fnUsername, bmsInfo);
            SetParserField(dataRow[LookupDict[fnPassword]], fnPassword, bmsInfo);
            SetParserField(dataRow[LookupDict[fnExtraInfo]], fnExtraInfo, bmsInfo);
            SetParserField(dataRow[LookupDict[fnBMSInfoName]], fnBMSInfoName, bmsInfo);
        }
    }
}