﻿using CW.Business;
using CW.Data;
using CW.Data.Models.EquipmentManufacturerModel;
using CW.Logging;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using PH = CW.Utility.PropHelper;

namespace CW.Website._parsers
{
    public class EquipmentParser : FileParserBase
    {
        #region const

            const String manufacturerModelNotAllowedForEquipmentClass = "{0} {1} is not associated with a Manufacturer ID that is allowed for Equipment Class {2}.";
            const String unallowedPointTypes = "Cannot change equipment {0} type because points point types are not allowed on changing equipment type {1}.";
            const String autoAssignAndSchedulingError = "Equipment was added successfully but an error occured auto assigning and scheduling analyses for the equipment.";

        #endregion

        #region static

            public static FieldDefBase[] FieldDefs, FieldDefsOverride;

            private readonly static String fnBID = PH.G<Building>(_ => _.BID);
            private readonly static String fnEID = PH.G<Equipment>(_ => _.EID);
            private readonly static String fnEquipmentName = PH.G<Equipment>(_ => _.EquipmentName);
            private readonly static String fnNewEquipmentName = "New" + PH.G<Equipment>(_ => _.EquipmentName);
            private readonly static String fnEquipmentLocation = PH.G<Equipment>(_ => _.EquipmentLocation);
            private readonly static String fnManufacturerModelID = PH.G<EquipmentManufacturerModel>(_ => _.ManufacturerModelID);
            private readonly static String fnSerialNumber = PH.G<Equipment>(_ => _.SerialNumber);
            private readonly static String fnDescription = PH.G<Equipment>(_ => _.Description);
            private readonly static String fnEquipmentTypeID = PH.G<EquipmentType>(_ => _.EquipmentTypeID);
            private readonly static String fnPrimaryContactID = PH.G<Equipment>(_ => _.PrimaryContactID);
            private readonly static String fnSecondaryContactID = PH.G<Equipment>(_ => _.SecondaryContactID);
            private readonly static String fnCMMSReferenceID = PH.G<Equipment>(_ => _.CMMSReferenceID);
            private readonly static String fnCMMSLink = PH.G<Equipment>(_ => _.CMMSLink);
            private readonly static String fnCMMSLocationID = PH.G<Equipment>(_ => _.CMMSLocationID);
            private readonly static String fnCMMSSiteID = PH.G<Equipment>(_ => _.CMMSSiteID);

        static EquipmentParser()
            {
                FieldDefs = new FieldDefBase[] 
				{
					new FieldDef<Int32>("Building ID", fnBID, "required", GetFieldType(fnBID, typeof(Building))){IsDistinctionField=true,},
					new FieldDef<String>("Equipment Name", fnEquipmentName, "required and unique", GetFieldType(fnEquipmentName, typeof(Equipment))){IsDistinctionField=true,},
					new FieldDef<String>("Equipment Location", fnEquipmentLocation, "can be empty", GetFieldType(fnEquipmentLocation, typeof(Equipment))),
					new FieldDef<Int32>("Manufacturer Model ID", fnManufacturerModelID, "required", GetFieldType(fnManufacturerModelID, typeof(EquipmentManufacturerModel))),
					new FieldDef<String>("Serial Number", fnSerialNumber, "can be empty", GetFieldType(fnSerialNumber, typeof(Equipment))),
					new FieldDef<String>("Description", fnDescription, "can be empty", GetFieldType(fnDescription, typeof(Equipment))),
					new FieldDef<Int32>("Equipment Type ID", fnEquipmentTypeID, "required", GetFieldType(fnEquipmentTypeID, typeof(EquipmentType))),
					new FieldDef<Int32>("Primary Contact ID", fnPrimaryContactID, "can be empty", GetFieldType(fnPrimaryContactID, typeof(Equipment))),
					new FieldDef<Int32>("Secondary Contact ID", fnSecondaryContactID, "can be empty", GetFieldType(fnSecondaryContactID, typeof(Equipment))),
                    new FieldDef<String>("CMMS Reference ID", fnCMMSReferenceID, "can be empty", GetFieldType(fnCMMSReferenceID, typeof(Equipment))),
                    new FieldDef<String>("CMMS Link", fnCMMSLink, "can be empty", GetFieldType(fnCMMSLink, typeof(Equipment))),
                    new FieldDef<String>("CMMS Location ID", fnCMMSLocationID, "can be empty", GetFieldType(fnCMMSLocationID, typeof(Equipment))),
                    new FieldDef<String>("CMMS Site ID", fnCMMSSiteID, "can be empty", GetFieldType(fnCMMSSiteID, typeof(Equipment)))
                };

                FieldDefsOverride = new FieldDefBase[] 
				{
                    new FieldDef<Int32>("Building ID", fnBID, "required", GetFieldType(fnBID, typeof(Building))){IsDistinctionField=true,IsOptionalUniqueField=true,},
                    new FieldDef<String>("Original Equipment Name", fnEquipmentName, "required and unique", GetFieldType(fnEquipmentName, typeof(Equipment))){IsDistinctionField=true,},   
					new FieldDef<String>("New Equipment Name", fnNewEquipmentName, "optional and unique", GetFieldType(fnEquipmentName, typeof(Equipment)), 0, GetValueMaxLengthForFieldDef(fnEquipmentName, typeof(Equipment)), 0, false){IsOptionalUniqueField=true,},
					new FieldDef<String>("Equipment Location", fnEquipmentLocation, "can be empty", GetFieldType(fnEquipmentLocation, typeof(Equipment))),
					new FieldDef<Int32>("Manufacturer Model ID", fnManufacturerModelID, "can be empty", GetFieldType(fnManufacturerModelID, typeof(EquipmentManufacturerModel)), 0, 0, 0, false, true), //default because of the db required check
					new FieldDef<String>("Serial Number", fnSerialNumber, "can be empty", GetFieldType(fnSerialNumber, typeof(Equipment))),
					new FieldDef<String>("Description", fnDescription, "can be empty", GetFieldType(fnDescription, typeof(Equipment))),
					new FieldDef<Int32>("Equipment Type ID", fnEquipmentTypeID, "can be empty", GetFieldType(fnEquipmentTypeID, typeof(EquipmentType)), 0, 0, 0, false, true), //default because of the db required check
					new FieldDef<Int32>("Primary Contact ID", fnPrimaryContactID, "can be empty", GetFieldType(fnPrimaryContactID, typeof(Equipment))),
					new FieldDef<Int32>("Secondary Contact ID", fnSecondaryContactID, "can be empty", GetFieldType(fnSecondaryContactID, typeof(Equipment))),
                    new FieldDef<String>("CMMS Reference ID", fnCMMSReferenceID, "can be empty", GetFieldType(fnCMMSReferenceID, typeof(Equipment))),
                    new FieldDef<String>("CMMS Link", fnCMMSLink, "can be empty", GetFieldType(fnCMMSLink, typeof(Equipment))),
                    new FieldDef<String>("CMMS Location ID", fnCMMSLocationID, "can be empty", GetFieldType(fnCMMSLocationID, typeof(Equipment))),
                    new FieldDef<String>("CMMS Site ID", fnCMMSSiteID, "can be empty", GetFieldType(fnCMMSSiteID, typeof(Equipment)))
                };
            }

        #endregion

        #region fields

            private readonly DataManager mDataManager;
            private readonly ISectionLogManager mLogger;
            private readonly IEnumerable<GetEquipmentManufacturerModelData> equipmentManufacturerModels;
            private readonly IEnumerable<EquipmentType> equipmentTypes;
            private readonly IEnumerable<EquipmentClass> equipmentClasses;
            private readonly IEnumerable<EquipmentManufacturerContact> equipmentManufacturerContacts;
            private readonly Func<Int32, Building> buildingGetter;
            private readonly Func<Int32, List<Equipment>> equipmentGetter;
            private Int32 cid;

        #endregion

        #region constructor

            public EquipmentParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) : 
            base(fileContent, (overrideExistingValues) ? FieldDefsOverride : FieldDefs, typeof(Equipment), overrideExistingValues, userInfo)
            {
                this.cid = SiteUser.Current.CID;

                this.mDataManager = dataManager;
                this.mLogger = logger ?? new NullSectionLogManager();
                this.equipmentManufacturerModels = mDataManager.EquipmentManufacturerModelDataMapper.GetAllFullManufacturerModels();
                this.equipmentTypes = mDataManager.EquipmentTypeDataMapper.GetAllEquipmentTypes();
                this.equipmentClasses = mDataManager.EquipmentClassDataMapper.GetAllEquipmentClasses();
                this.equipmentManufacturerContacts = mDataManager.EquipmentManufacturerContactDataMapper.GetAllContacts();
                this.buildingGetter = (id) => mDataManager.BuildingDataMapper.GetBuildingByBID(id, false, false, false, false, false, false);
                this.equipmentGetter = (id) => (List<Equipment>)mDataManager.EquipmentDataMapper.GetEquipmentByBID(id);
            }

        #endregion

        #region methods

            #region override

                public override void ParseRow(String[] rowContent, Int32 rowIndex)
                {
                    var bid = GetIdFromParseValue(rowContent[LookupDict[fnBID]]);
                    var equipmentName = rowContent[LookupDict[fnEquipmentName]];
                    var building = mDataManager.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false);
                    var equipmentList = new List<Equipment>();
                    var equipmentException = GetEntityByID<List<Equipment>>(rowIndex, bid, equipmentGetter, out equipmentList);
                    var equipment = equipmentList.FirstOrDefault(e => e.BID == bid && e.EquipmentName == equipmentName);

                    if (OverrideExistingValues) //special case here that needs additional check for the "new" equipment to ensure it doesn't exist already
                    {
                        var newEquipmentName = rowContent[LookupDict[fnNewEquipmentName]];
                        var newEquipment = equipmentList.FirstOrDefault(e => e.BID == bid && e.EquipmentName == newEquipmentName && e.EID != equipment.EID);

                        if (newEquipment != null)
                        {
                            Exceptions.Add(new EntryAlreadyExistsException(rowIndex, GetDistinctionFields(FieldDefsOverride, rowContent, (fd => fd.FieldName == fnBID || fd.FieldName == fnNewEquipmentName))));
                            return;
                        }
                    }

                    if (HasEntityExceptions(equipment, rowIndex, (OverrideExistingValues ? FieldDefsOverride : FieldDefs), rowContent)) return;

                    SetEntityAndPrimaryIDToRow(equipment, rowContent, fnEID);

                    if (!IsValidCIDForClient(FieldDefs.Single(_ => _.FieldName == fnBID).DisplayName, rowIndex, building, PropHelper.G<Client>(_ => _.CID))) return;

                    CheckRemainingFields(rowContent, rowIndex, equipment); //check for valid foreign key fields

                    if (Exceptions.Count == 0) UpdateBIDList(bid);
                }

                protected override void InsertRow(Row row)
                {
                    try
                    {
                        var equipment = CreateEquipment(row);
                        var eidPrimaryKey = mDataManager.EquipmentDataMapper.InsertEquipment(equipment);

                        //auto assign and auto schedule analyses
                        try
                        {
                            var assignedAnalyses = mDataManager.AnalysisEquipmentTypeDataMapper.GetAssignedAnalysesByEquipmentTypeIDAndCID(equipment.EquipmentTypeID, cid);

                            List<ScheduledAnalyse> scheduledAnalyses = new List<ScheduledAnalyse>();
                            List<Analyses_Equipment> analysesEquipment = new List<Analyses_Equipment>();

                            foreach (var a_et in assignedAnalyses)
                            {
                                if (a_et.AutoAssignAndScheduleDaily || a_et.AutoAssignAndScheduleWeekly || a_et.AutoAssignAndScheduleMonthly || a_et.AutoAssignAndScheduleHalfDay)
                                {
                                    Analyses_Equipment ae = new Analyses_Equipment();
                                    ae.AID = a_et.AID;
                                    ae.EID = equipment.EID;
                                    analysesEquipment.Add(ae);

                                    ScheduledAnalyse sa = new ScheduledAnalyse();
                                    sa.AID = a_et.AID;
                                    sa.EID = equipment.EID;
                                    sa.BID = equipment.BID;
                                    sa.RunStartDate = DateTime.UtcNow;
                                    sa.RunHalfDay = a_et.AutoAssignAndScheduleHalfDay;
                                    sa.RunDaily = a_et.AutoAssignAndScheduleDaily;
                                    sa.RunWeekly = a_et.AutoAssignAndScheduleWeekly;
                                    sa.RunMonthly = a_et.AutoAssignAndScheduleMonthly;
                                    sa.DateModified = DateTime.UtcNow;
                                    scheduledAnalyses.Add(sa);

                                    //insert all vpoints
                                    var aovpts = mDataManager.AnalysisPointTypeDataMapper.GetAllOutputVPointTypesByAID(a_et.AID);

                                    foreach (var apvpt in aovpts)
                                    {
                                        var vp = new VPoint();

                                        vp.AID = a_et.AID;
                                        vp.EID = equipment.EID;
                                        vp.PointTypeID = apvpt.PointTypeID;

                                        mDataManager.PointDataMapper.InsertVPoint(vp);
                                    }

                                    //insert all vpre points
                                    var aovppts = mDataManager.AnalysisPointTypeDataMapper.GetAllOutputVPrePointTypesByAID(a_et.AID);

                                    foreach (var apvppt in aovppts)
                                    {
                                        var vpp = new VPrePoint();

                                        vpp.AID = a_et.AID;
                                        vpp.EID = equipment.EID;
                                        vpp.PointTypeID = apvppt.PointTypeID;

                                        mDataManager.PointDataMapper.InsertVPrePoint(vpp);
                                    }
                                }
                            }

                            if (analysesEquipment.Any()) mDataManager.AnalysisEquipmentDataMapper.InsertAnalysisEquipment(analysesEquipment);
                            if (scheduledAnalyses.Any()) mDataManager.ScheduledAnalysisDataMapper.InsertScheduledAnalyses(scheduledAnalyses);                            
                        }
                        catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, autoAssignAndSchedulingError, row.DataRow[LookupDict[fnEquipmentName]]); }

                        InsertCount++;
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error inserting equipment.", row.DataRow[LookupDict[fnEquipmentName]]); }
                }

                protected override void UpdateRow(Row row)
                {
                    var rowIndex = row.RowIndex;
                    var equipment = CreateEquipment(row);
                    var equipmentName = equipment.EquipmentName;

                    try
                    {
                        var entityEquipment = ((Equipment)row.Entity);

                        if ((equipment.EquipmentTypeID != 0) && (entityEquipment.EquipmentTypeID != equipment.EquipmentTypeID))
                        {
                            //check if all of the equipments points point types are allowed on the changing equipment type
                            if (!mDataManager.EquipmentTypeDataMapper.AreAllEquipmentsPointsPointTypesAllowedOnChangingEquipmentType(equipment.EID, equipment.EquipmentTypeID))
                            {
                                GenerateInvalidGenericException(rowIndex, unallowedPointTypes, equipmentName, equipment.EquipmentTypeID);
                                return;
                            }

                            var existingClassID = mDataManager.EquipmentClassDataMapper.GetEquipmentClassByEquipmentTypeID(entityEquipment.EquipmentTypeID).EquipmentClassID;
                            var newClassID = mDataManager.EquipmentClassDataMapper.GetEquipmentClassByEquipmentTypeID(equipment.EquipmentTypeID).EquipmentClassID;

                            if (existingClassID != newClassID)
                            {
                                mDataManager.EquipmentVariableDataMapper.DeleteEquipmentVariablesNotFoundInChangingEquipmentClass(equipment.EID, newClassID);                                
                            }

                            mDataManager.EquipmentDataMapper.DeleteInvalidEquipmentToEquipmentAssociationsDueToChangingEquipmentType(equipment.EID, equipment.EquipmentTypeID);
                        }

                        mDataManager.EquipmentDataMapper.UpdateEquipmentForOverride(equipment);
                        OverridesSB.AppendLine(String.Format("Row#&nbsp;{0} Updated Values for {1} {2} and {3} {4}<br />", rowIndex, GetDisplayName(fnBID), equipment.BID, GetDisplayName(fnEquipmentName), equipmentName));
                        
                    }
                    catch (SqlException sqlEx) { LogSQLError(mLogger, row.RowIndex, sqlEx, "Error updating equipment.", equipmentName); }
                }

            #endregion

            private void CheckRemainingFields(String[] rowContent, Int32 rowIndex, Equipment equipment)
            {
                var contactIds = equipmentManufacturerContacts.Select(emc => emc.ContactID);
                var mmid = GetIdIfItemExists(equipmentManufacturerModels.Select(emm => emm.ManufacturerModelID), rowIndex, rowContent, fnManufacturerModelID);
                var etid = GetIdIfItemExists(equipmentTypes.Select(et => et.EquipmentTypeID), rowIndex, rowContent, fnEquipmentTypeID);
                var pcid = GetIdIfItemExists(contactIds, rowIndex, rowContent, fnPrimaryContactID);
                var scid = GetIdIfItemExists(contactIds, rowIndex, rowContent, fnSecondaryContactID);
                var manId = 0;
                var ecid = 0;

                if (Exceptions.Where(e => e.Row == rowIndex).Count() > 0) return;

                if (mmid != null) manId = equipmentManufacturerModels.Single(emm => emm.ManufacturerModelID == mmid).ManufacturerID;
                if (etid != null) ecid = equipmentTypes.Single(et => et.EquipmentTypeID == etid).EquipmentClassID;
                
                //checks if new equipment manufacture model allowed on class.  
                //checks if changed manaufature model allowed on existing class.
                //checks if changed equipment type allowed with exsting manufacturer
                //checks if changed eqiupment type and manufacturermodel allowed together.
                if (manId != 0 || ecid != 0)
                {
                    //gets original value if not being overridden/changed in order to perform the check
                    if (ecid == 0) ecid = equipmentTypes.Single(et => et.EquipmentTypeID == equipment.EquipmentTypeID).EquipmentClassID;
                    if (manId == 0) manId = equipmentManufacturerModels.Single(emm => emm.ManufacturerModelID == equipment.ManufacturerModelID).ManufacturerID;

                    if (!mDataManager.EquipmentManufacturerDataMapper.DoesEquipmentManufacturerExistForEquipmentClass(ecid, manId))
                        GenerateInvalidGenericException(rowIndex, manufacturerModelNotAllowedForEquipmentClass, GetDisplayName(fnManufacturerModelID), mmid, equipmentClasses.Single(ec => ec.EquipmentClassID == ecid).EquipmentClassName);
                }
            }

            private Int32? GetIdIfItemExists(IEnumerable<Int32> items, Int32 rowIndex, String[] rowContent, String fieldName)
            {
                var id = GetIdFromParseValue(rowContent[LookupDict[fieldName]]);

                if (id != 0) //check here for id, since these ids are optional when overriding data.
                {
                    if (!items.Contains(id))
                    {
                        GenerateInvalidGenericException(rowIndex, invalidGenericExceptionFormat, GetDisplayName(fieldName), rowContent[LookupDict[fieldName]], GetDisplayName(fieldName));
                        return null;
                    }

                    return id;
                }

                return null;
            }

            private Equipment CreateEquipment(Row row)
            {
                var dataRow = row.DataRow;
                var equipment = new Equipment();
                var entityEquipment = ((Equipment)row.Entity);

                if (OverrideExistingValues) equipment.EID = (Int32)row.PrimaryId;

                if (!OverrideExistingValues)
                {
                    equipment.BID = Convert.ToInt32(dataRow[LookupDict[fnBID]]);
                    equipment.EquipmentName = StringHelper.TrimLeadingTrailingWhiteSpace(dataRow[LookupDict[fnEquipmentName]]);
                }
                else if (!String.IsNullOrWhiteSpace(dataRow[LookupDict[fnNewEquipmentName]]))
                    equipment.EquipmentName = dataRow[LookupDict[fnNewEquipmentName]];
                
                //TODO: check on building variables before we can update BID.
                //TODO: check on equipment variables beforew we can update equipment type 
                SetParserField(dataRow[LookupDict[fnEquipmentLocation]], fnEquipmentLocation, equipment);
                SetParserField(dataRow[LookupDict[fnManufacturerModelID]], fnManufacturerModelID, equipment);
                SetParserField(dataRow[LookupDict[fnSerialNumber]], fnSerialNumber, equipment);
                SetParserField(dataRow[LookupDict[fnDescription]], fnDescription, equipment);
                SetParserField(dataRow[LookupDict[fnEquipmentTypeID]], fnEquipmentTypeID, equipment);
                SetParserField(dataRow[LookupDict[fnPrimaryContactID]], fnPrimaryContactID, equipment);
                SetParserField(dataRow[LookupDict[fnSecondaryContactID]], fnSecondaryContactID, equipment);
                SetParserField(dataRow[LookupDict[fnCMMSReferenceID]], fnCMMSReferenceID, equipment);
                SetParserField(dataRow[LookupDict[fnCMMSLink]], fnCMMSLink, equipment);
                SetParserField(dataRow[LookupDict[fnCMMSLocationID]], fnCMMSLocationID, equipment);
                SetParserField(dataRow[LookupDict[fnCMMSSiteID]], fnCMMSSiteID, equipment);

                equipment.IsActive = OverrideExistingValues ? entityEquipment.IsActive : true;
                equipment.IsVisible = OverrideExistingValues ? entityEquipment.IsVisible : true;

                return equipment;
            }

        #endregion
    }
}