﻿using CW.Business;
using CW.Logging;
using CW.Data;
using System;
using System.IO;
using PH = CW.Utility.PropHelper;

namespace CW.Website._parsers
{
    public class PointTypingParser : PointsParser
    {
        #region STATIC

            public static FieldDefBase[] PointTypeFieldDefs;

            private readonly static String fnDataSourceID = PH.G<DataSource>(_ => _.DSID);
            private readonly static String fnEquipmentID = PH.G<Equipment>(_ => _.EID);
            private readonly static String fNPointReferenceID = PH.G<Point>(_ => _.ReferenceID);
            private readonly static String fNPointName = PH.G<Point>(_ => _.PointName);
            private readonly static String fNSamplingInterval = PH.G<Point>(_ => _.SamplingInterval);
            private readonly static String fNRelPctError = PH.G<Point>(_ => _.RelPctError);
            private readonly static String fNTimeZone = PH.G<Point>(_ => _.TimeZone);
            private readonly static String fNPointDateCreated = PH.G<Point>(_ => _.DateCreated);
            
            static PointTypingParser()
            {
                PointTypeFieldDefs =
                new FieldDefBase[] 
				{
					new FieldDef<Int32>("DataSource ID", fnDataSourceID, "required", GetFieldType(fnDataSourceID, typeof(Point))){IsDistinctionField=true,},
                    new FieldDef<Int32>("Equipment ID", fnEquipmentID, "required", GetFieldType(fnEquipmentID, typeof(Equipment))),
					new FieldDef<String>("Point Reference ID", fNPointReferenceID, "required", GetFieldType(fNPointReferenceID, typeof(Point))){IsDistinctionField=true,},
                    new FieldDef<String>("Point Name", fNPointName, "required", GetFieldType(fNPointName, typeof(Point))),
                    new FieldDef<Int32>("Sampling Interval", fNSamplingInterval, "required", GetFieldType(fNSamplingInterval, typeof(Point))),
                    new FieldDef<Double>("RelPctError", fNRelPctError, "required", GetFieldType(fNRelPctError, typeof(Point))),
                    new FieldDef<String>("Time Zone", fNTimeZone, "required and must be exact spelling as in system", GetFieldType(fNTimeZone, typeof(Point))),
                    new FieldDef<DateTime>("Date Created", fNPointDateCreated, String.Format("optional but must be date time format (example) {0}, local time expected", DateTime.UtcNow.ToString()), GetFieldType(fNPointDateCreated, typeof(Point)), 0, 0, 0, false, true) //defaulted
				};
            }

        #endregion

        #region field

            private readonly DataManager mDataManager;

        #endregion

        #region constructor

            public PointTypingParser(Stream fileContent, DataManager dataManager, Boolean overrideExistingValues, ISectionLogManager logger, UserInfo userInfo) : base(fileContent, dataManager, overrideExistingValues, PointTypeFieldDefs, logger, userInfo)
            {
                this.mDataManager = dataManager;
            }

        #endregion
    }
}