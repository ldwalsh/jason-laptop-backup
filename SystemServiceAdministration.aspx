﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="False" CodeBehind="SystemServiceAdministration.aspx.cs" Inherits="CW.Website.SystemServiceAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                 	                  	
            	<h1>System Service Administration</h1>                        
                <div class="richText">The system service administration area is used to view, add, and edit data transfer service configs.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Data Transfer Services"></telerik:RadTab>
                            <telerik:RadTab Text="Add Data Transfer Service"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Data Transfer Service</h2>
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p>    
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>      
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridServices"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="DTSID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridServices_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridServices_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridServices_Sorting"  
                                             OnSelectedIndexChanged="gridServices_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridServices_Editing"                                                                                                                                                                                   
                                             OnRowDeleting="gridServices_Deleting"
                                             OnDataBound="gridServices_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>       
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Data Transfer Service Name">  
                                                    <ItemTemplate><%# Eval("DataTransferServiceName") %></ItemTemplate>                                              
                                                </asp:TemplateField> 
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Data Transfer Service GUID">  
                                                    <ItemTemplate><%# Eval("DataTransferServiceGuid") %></ItemTemplate>                                              
                                                </asp:TemplateField> 
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Service Key Name">  
                                                    <ItemTemplate><%# Eval("ServiceKeyName") %></ItemTemplate>                                              
                                                </asp:TemplateField> 
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Retrieval Method">  
                                                    <ItemTemplate><%# Eval("RetrievalMethod") %></ItemTemplate>                                              
                                                </asp:TemplateField> 
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="IP List">  
                                                    <ItemTemplate><%# Eval("IPList") %></ItemTemplate>                                              
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>                                        
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this service config permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT SERVICE DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvService" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Service Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Data Transfer Service Name: </strong>" + Eval("DataTransferServiceName") + "</li>"%>
                                                            <%# "<li><strong>Data Transfer Service GUID: </strong>" + Eval("DataTransferServiceGuid") + "</li>"%>
                                                            <%# "<li><strong>Service Key Name: </strong>" + Eval("ServiceKeyName") + "</li>"%>
                                                            <%# "<li><strong>Client: </strong>" + Eval("ClientName") + "</li>"%>
                                                            <%# "<li><strong>Retrieval Method: </strong>" + Eval("RetrievalMethod") + "</li>"%> 
                                                            <%# "<li><strong>Config Content: </strong>" %>
                                                            <asp:HyperLink ID="lnkConfig" runat="server" CssClass="toggle"><asp:Label ID="lblConfig" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                                                            <ajaxToolkit:CollapsiblePanelExtender ID="configCollapsiblePanelExtender" runat="Server"
                                                                            TargetControlID="pnlConfig"
                                                                            CollapsedSize="0"
                                                                            Collapsed="true" 
                                                                            ExpandControlID="lnkConfig"
                                                                            CollapseControlID="lnkConfig"
                                                                            AutoCollapse="false"
                                                                            AutoExpand="false"
                                                                            ScrollContents="false"
                                                                            ExpandDirection="Vertical"
                                                                            TextLabelID="lblConfig"
                                                                            CollapsedText="show config"
                                                                            ExpandedText="hide config" 
                                                                            />
                                                              <asp:Panel ID="pnlConfig" runat="server">
                                                              <xmp class='divContentPreWrap'><%# Server.HtmlDecode(Eval("ConfigContent").ToString()) %></xmp>
                                                              </asp:Panel>                                                             
                                                            <%# "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("IPList"))) ? "" : "<li><strong>IPList: </strong>" + Eval("IPList") + "</li>"%>
                                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>  
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("BMSInfoName"))) ? "" : "<li><strong>BMS Info Name: </strong>" + Eval("BMSInfoName") + "</li>"%>
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT SERVICE PANEL -->                              
                                    <asp:Panel ID="pnlEditService" runat="server" Visible="false" DefaultButton="btnUpdateService">                                                                                             
                                        <div>
                                            <h2>Edit Service</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*DataTransferServiceName:</label>
                                                <asp:TextBox ID="txtEditDataTransferServiceName" CssClass="textbox" MaxLength="250" runat="server"></asp:TextBox>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*DataTransferServiceGUID:</label>
                                                <asp:TextBox ID="txtEditDataTransferServiceGUID" CssClass="textbox" MaxLength="32" runat="server"></asp:TextBox>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Service Key:</label>
                                                <asp:DropDownList CssClass="dropdown" ID="ddlEditServiceKey" runat="server" AppendDataBoundItems="true">             
                                                </asp:DropDownList>
                                            </div>  
                                            <div class="divForm">   
                                                <label class="label">*Retrieval Method:</label>    
                                                <asp:DropDownList ID="ddlEditRetrievalMethod" CssClass="dropdown" runat="server">                                                                                                                                                                                            
                                                </asp:DropDownList> 
                                            </div>  
                                            <div class="divForm">
                                                <label class="label">*Config Content:</label>
                                                <asp:TextBox ValidateRequestMode="Disabled" TextMode="MultiLine" ID="txtEditConfigContent" spellcheck="false" runat="server" Height="300" Width="480"></asp:TextBox>
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">IPList:</label>
                                                <asp:TextBox CssClass="textbox" ID="txtEditIPList" MaxLength="500" runat="server"></asp:TextBox>                                      
                                            </div>  
                                            <div class="divForm">
                                                <label class="label">*Active:</label>
                                                <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                    
                                            </div>
                                            <div class="divForm">
                                                <label class="label">BMS Info:</label>
                                                <asp:DropDownList CssClass="dropdown" ID="ddlEditBMSInfo" runat="server" AppendDataBoundItems="true">                                                                                          
                                                </asp:DropDownList>
                                            </div>                                                                                                 
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateService" runat="server" Text="Update Service"  OnClick="updateServiceButton_Click" ValidationGroup="EditService" ></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->
                                   <asp:RequiredFieldValidator ID="editDataTransferServiceNameRequiredValidator" runat="server"
                                        ErrorMessage="Data Transfer Service Name is a required field."
                                        ControlToValidate="txtEditDataTransferServiceName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditService">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editDataTransferServiceNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editDataTransferServiceNameRequiredValidatorExtender"
                                        TargetControlID="editDataTransferServiceNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">    
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                   <asp:RequiredFieldValidator ID="editDataTransferServiceGUIDRequiredValidator" runat="server"
                                        ErrorMessage="Data Transfer Service GUID is a required field."
                                        ControlToValidate="txtEditDataTransferServiceGUID"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditServiceConfig">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editDataTransferServiceGUIDRequiredValidatorExtender" runat="server"
                                        BehaviorID="editDataTransferServiceGUIDRequiredValidatorExtender"
                                        TargetControlID="editDataTransferServiceGUIDRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                     </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editServiceKeyRequiredValidator" runat="server"
                                        ErrorMessage="Service Key is a required field." 
                                        ControlToValidate="ddlEditServiceKey"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditServiceConfig">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editServiceKeyRequiredValidatorExtender" runat="server"
                                        BehaviorID="editServiceKeyRequiredValidatorExtender" 
                                        TargetControlID="editServiceKeyRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="editRetrievalMethodRequiredValidator" runat="server"
                                        ErrorMessage="Retrieval Method is a required field." 
                                        ControlToValidate="ddlEditRetrievalMethod"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditDataSource">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editRetrievalMethodRequiredValidatorExtender" runat="server"
                                        BehaviorID="editRetrievalMethodRequiredValidatorExtender" 
                                        TargetControlID="editRetrievalMethodRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>                                                                              
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server"> 
                                <asp:Panel ID="pnlAddService" runat="server" DefaultButton="btnAddService"> 
                                    <h2>Add Service</h2>
                                    <div>             
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                       
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                       
                                        <div class="divForm">
                                            <label class="label">*DataTransferServiceName:</label>
                                            <asp:TextBox CssClass="textbox" ID="txtAddDataTransferServiceName" Columns="50" MaxLength="250" runat="server"></asp:TextBox>                                      
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">*DataTransferServiceGUID:</label>
                                            <label class="labelContent">This field is auto generated.</label>
                                        </div>   
                                        <div class="divForm">
                                            <label class="label">*Service Key:</label>
                                            <asp:DropDownList CssClass="dropdown" ID="ddlAddServiceKey" AppendDataBoundItems="true" runat="server">  
                                            <asp:ListItem Value="-1">Select client first...</asp:ListItem>                             
                                            </asp:DropDownList>
                                        </div> 
                                        <div class="divForm">   
                                            <label class="label">*Retrieval Method:</label>    
                                            <asp:DropDownList ID="ddlAddRetrievalMethod" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                                                                    
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem> 
                                            </asp:DropDownList> 
                                        </div>   
                                        <div class="divForm">
                                            <label class="label">*Config Content:</label>
                                            <asp:TextBox ValidateRequestMode="Disabled" TextMode="MultiLine" ID="txtAddConfigContent" spellcheck="false" runat="server" Height="300" Width="480"></asp:TextBox>
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">IPList:</label>
                                            <asp:TextBox CssClass="textbox" ID="txtAddIPList" MaxLength="500" runat="server"></asp:TextBox>                                      
                                        </div>
                                        <div class="divForm">
                                            <label class="label">BMS Info:</label>
                                            <asp:DropDownList CssClass="dropdown" ID="ddlAddBMSInfo" runat="server" AppendDataBoundItems="true"></asp:DropDownList>
                                        </div>                                                                                         
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddService" runat="server" Text="Add Config"  OnClick="addServiceButton_Click" ValidationGroup="AddService"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->       
                                    <asp:RequiredFieldValidator ID="addDataTransferServiceNameRequiredValidator" runat="server"
                                        ErrorMessage="Data Transfer Service Name is a required field."
                                        ControlToValidate="txtAddDataTransferServiceName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddService">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addDataTransferServiceNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addDataTransferServiceNameRequiredValidatorExtender"
                                        TargetControlID="addDataTransferServiceNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                    <asp:RequiredFieldValidator ID="addServiceKeyRequiredValidator" runat="server"
                                        ErrorMessage="Service Key is a required field."
                                        ControlToValidate="ddlAddServiceKey"
                                        SetFocusOnError="true"
                                        Display="None"
                                        InitialValue="-1"
                                        ValidationGroup="AddService">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addServiceKeyRequiredValidatorExtender" runat="server"
                                        BehaviorID="addServiceKeyRequiredValidatorExtender" 
                                        TargetControlID="addServiceKeyRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="addRetrievalMethodRequiredValidator" runat="server"
                                        ErrorMessage="Retrieval Method is a required field." 
                                        ControlToValidate="ddlAddRetrievalMethod"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddService">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addRetrievalMethodRequiredValidatorExtender" runat="server"
                                        BehaviorID="addRetrievalMethodRequiredValidatorExtender" 
                                        TargetControlID="addRetrievalMethodRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>        
                                </asp:Panel>                                                                                                                                              
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                   </div>                                                                 
               
</asp:Content>


                    
                  
