﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSPointTypeAdministration.aspx.cs" Inherits="CW.Website.KGSPointTypeAdministration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <h1>KGS Point Type Administration</h1>
    <div class="richText">The kgs point type administration area is used to view, add, and edit point types.</div>
    <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
            <ProgressTemplate>
                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div>
    <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage">
            <Tabs>
                <telerik:RadTab Text="View Point Types"></telerik:RadTab>
                <telerik:RadTab Text="Add Point Type"></telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
            <telerik:RadPageView ID="RadPageView1" runat="server">
                <h2>View Point Types</h2>
                <p>
                    <a id="lnkSetFocusView" href="#" runat="server"></a>
                    <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblErrors" CssClass="errorMessage" runat="server" Visible="false"></asp:Label>
                </p>
                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                    <asp:LinkButton ID="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                    <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                    <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click" />
                </asp:Panel>

                <!--please check the gridPointTypes_OnDataBound method if any cells are moved-->
                <div id="gridTbl">
                    <asp:GridView
                        ID="gridPointTypes"
                        EnableViewState="true"
                        runat="server"
                        DataKeyNames="PointTypeID"
                        GridLines="None"
                        PageSize="20" PagerSettings-PageButtonCount="20"
                        OnRowCreated="gridPointTypes_OnRowCreated"
                        AllowPaging="true" OnPageIndexChanging="gridPointTypes_PageIndexChanging"
                        AllowSorting="true" OnSorting="gridPointTypes_Sorting"
                        OnSelectedIndexChanged="gridPointTypes_OnSelectedIndexChanged"
                        OnRowDeleting="gridPointTypes_Deleting"
                        OnRowEditing="gridPointTypes_Editing"
                        OnDataBound="gridPointTypes_OnDataBound"
                        AutoGenerateColumns="false"
                        HeaderStyle-CssClass="tblTitle"
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"
                        RowStyle-Wrap="true">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" />
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="PointTypeName" HeaderText="Point Type">
                                <ItemTemplate>
                                    <label title="<%# Eval("PointTypeName") %>"><%# StringHelper.TrimText(Eval("PointTypeName"),25) %></label></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Point Class" SortExpression="PointClassName">
                                <ItemTemplate>
                                    <label title="<%# Eval("PointClassName") %>"><%# StringHelper.TrimText(Eval("PointClassName"),20) %></label>
                                    <asp:HiddenField ID="hdnPointClassID" runat="server" Visible="true" Value='<%# Eval("PointClassID")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="IP Eng. Units" SortExpression="IPEngUnits">
                                <ItemTemplate><%# Eval("IPEngUnits") %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SI Eng. Units" SortExpression="SIEngUnits">
                                <ItemTemplate><%# Eval("SIEngUnits") %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Point Enabled" SortExpression="PointEnabled">
                                <ItemTemplate><%# Eval("PointEnabled") %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Wrap="true" HeaderText="VPoint Enabled" SortExpression="VPointEnabled">
                                <ItemTemplate><%# Eval("VPointEnabled") %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Wrap="true" HeaderText="VPrePoint Enabled" SortExpression="VPrePointEnabled">
                                <ItemTemplate><%# Eval("VPrePointEnabled") %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Unassigned" SortExpression="IsUnassigned">
                                <ItemTemplate><%# Eval("IsUnassigned") %></ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" CausesValidation="false"
                                        OnClientClick="return confirm('Are you sure you wish to delete this point type permanently?');"
                                        CommandName="Delete">Delete</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>
                <br />
                <br />
                <div>

                    <!--SELECT POINT TYPE DETAILS VIEW -->
                    <asp:DetailsView ID="dtvPointType" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                        <Fields>
                            <asp:TemplateField
                                ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none"
                                ItemStyle-BorderWidth="0"
                                ItemStyle-BorderColor="White"
                                ItemStyle-BorderStyle="none"
                                ItemStyle-Width="100%"
                                HeaderStyle-Width="1"
                                HeaderStyle-BorderWidth="0"
                                HeaderStyle-BorderColor="White"
                                HeaderStyle-BorderStyle="none">
                                <ItemTemplate>
                                    <div>
                                        <h2>Point Type Details</h2>
                                        <ul class="detailsList">
                                            <%# "<li><strong>Point Type Name: </strong>" + Eval("PointTypeName") + "</li>"%>
                                            <%# "<li><strong>Display Name: </strong>" + Eval("DisplayName") + "</li>"%>
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PointTypeDescription"))) ? "" : "<li><strong>Point Type Description: </strong>" + Eval("PointTypeDescription") + "</li>"%>
                                            <%# "<li><strong>Point Class Name: </strong>" + Eval("PointClassName") + "</li>"%>
                                            <%# "<li><strong>IP Eng. Units: </strong>" + Eval("IPEngUnits") + "</li>"%>
                                            <%# "<li><strong>SI Eng. Units: </strong>" + Eval("SIEngUnits") + "</li>"%>
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("PointEnabled"))) ? "" : "<li><strong>Point Enabled: </strong>" + Eval("PointEnabled") + "</li>"%>
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VPointEnabled"))) ? "" : "<li><strong>VPoint Enabled: </strong>" + Eval("VPointEnabled") + "</li>"%>
                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("VPrePointEnabled"))) ? "" : "<li><strong>VPrePoint Enabled: </strong>" + Eval("VPrePointEnabled") + "</li>"%>
                                            <%# "<li><strong>Unassigned: </strong>" + Eval("IsUnassigned") + "</li>"%>
                                        </ul>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </div>

                <!--EDIT EQUIPMENT PANEL -->
                <asp:Panel ID="pnlEditPointType" runat="server" Visible="false" DefaultButton="btnUpdatePointType">
                    <div>
                        <h2>Edit Point Type</h2>
                    </div>
                    <div>
                        <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>
                        <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                        <div class="divForm">
                            <label class="label">*Point Type Name:</label>
                            <asp:TextBox ID="txtEditPointTypeName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                            <p>(Warning: Analysis functions are directly dependant on the name's spelling.)</p>
                        </div>
                        <div class="divForm">
                            <label class="label">*Display Name:</label>
                            <asp:TextBox ID="txtEditDisplayName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>
                        </div>
                        <div class="divForm">
                            <label class="label">Description:</label>
                            <textarea name="txtEditPointTypeDescription" id="txtEditPointTypeDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditPointTypeDescriptionCharInfo')" runat="server"></textarea>
                            <div id="divEditPointTypeDescriptionCharInfo"></div>
                        </div>
                        <div class="divForm">
                            <label class="label">*Point Class Name:</label>
                            <asp:Label ID="lblEditPointClass" CssClass="labelContent" runat="server"></asp:Label>
                            <p>(TODO: We might be able to allow for this to be an editable dropdown.)</p>
                        </div>
                        <div class="divForm">
                            <label class="label">*Unassigned:</label>
                            <asp:Label CssClass="labelContent" ID="lblEditUnassigned" runat="server"></asp:Label>
                            <p>(Note: A point type unassigned field cannot be changed.)</p>
                        </div>
                        <div class="divForm">
                            <p>
                                A point type can only be point enabled or vpoint and vprepoint enabled.
                            </p>
                            <label class="label">*Point Enabled:</label>
                            <asp:CheckBox CssClass="checkbox" ID="chkEditPointEnabled" OnCheckedChanged="chkEditPointEnabled_OnCheckedChanged" AutoPostBack="true" runat="server" />
                        </div>
                        <div class="divForm">
                            <label class="label">*VPoint Enabled:</label>
                            <asp:CheckBox CssClass="checkbox" ID="chkEditVPointEnabled" OnCheckedChanged="chkEditVPointEnabled_OnCheckedChanged" AutoPostBack="true" runat="server" />
                        </div>
                        <div class="divForm">
                            <label class="label">*VPrePoint Enabled:</label>
                            <asp:CheckBox CssClass="checkbox" ID="chkEditVPrePointEnabled" OnCheckedChanged="chkEditVPrePointEnabled_OnCheckedChanged" AutoPostBack="true" runat="server" />
                        </div>
                        <asp:LinkButton CssClass="lnkButton" ID="btnUpdatePointType" runat="server" Text="Update Point Type" OnClick="updatePointTypeButton_Click" ValidationGroup="EditPointType"></asp:LinkButton>
                    </div>

                    <!--Ajax Validators-->
                    <asp:RequiredFieldValidator ID="editPointTypeNameRequiredValidator" runat="server"
                        ErrorMessage="Point Type Name is a required field."
                        ControlToValidate="txtEditPointTypeName"
                        SetFocusOnError="true"
                        Display="None"
                        ValidationGroup="EditPointType">
                    </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="editPointTypeNameRequiredValidatorExtender" runat="server"
                        BehaviorID="editPointTypeNameRequiredValidatorExtender"
                        TargetControlID="editPointTypeNameRequiredValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>
                    <asp:RequiredFieldValidator ID="editDisplayNameRequiredValidator" runat="server"
                        ErrorMessage="Display Name is a required field."
                        ControlToValidate="txtEditDisplayName"
                        SetFocusOnError="true"
                        Display="None"
                        ValidationGroup="EditPointType">
                    </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="editDisplayNameRequiredValidatorExtender" runat="server"
                        BehaviorID="editDisplayNameRequiredValidatorExtender"
                        TargetControlID="editDisplayNameRequiredValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>
                </asp:Panel>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView2" runat="server">
                <asp:Panel ID="pnlAddPointType" runat="server" DefaultButton="btnAddPointType">
                    <h2>Add Point Type</h2>
                    <div>
                        <a id="lnkSetFocusAdd" runat="server"></a>
                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>

                        <div class="divForm">
                            <label class="label">*Point Type Name:</label>
                            <asp:TextBox ID="txtAddPointTypeName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                            <p>(Note: Name is used in analysis functions.)</p>
                        </div>
                        <div class="divForm">
                            <label class="label">*Display Name:</label>
                            <asp:TextBox ID="txtAddDisplayName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>
                        </div>
                        <div class="divForm">
                            <label class="label">Description:</label>
                            <textarea name="txtAddPointTypeDescription" id="txtAddPointTypeDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divAddPointTypeDescriptionCharInfo')" runat="server"></textarea>
                            <div id="divAddPointTypeDescriptionCharInfo"></div>
                        </div>
                        <div class="divForm">
                            <label class="label">*Point Class:</label>
                            <asp:DropDownList ID="ddlAddPointClass" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                <asp:ListItem Value="-1">Select one...</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="divForm">
                            <label class="label">*Unassigned:</label>
                            <asp:CheckBox CssClass="checkbox" ID="chkAddUnassigned" runat="server" />
                            <p>(Warning: A point type unassigned field cannot be changed.)</p>
                            <p>(Note: Setting a point type as unassigned will allow for multiple unassigned types to be applied to an equipment, and faster point uploads for monitoring when diagnostics are not required.)</p>
                        </div>
                        <div class="divForm">
                            <p>
                                A point type can only be point enabled or vpoint and vprepoint enabled.
                            </p>
                            <label class="label">*Point Enabled:</label>
                            <asp:CheckBox CssClass="checkbox" ID="chkAddPointEnabled" OnCheckedChanged="chkAddPointEnabled_OnCheckedChanged" AutoPostBack="true" runat="server" />
                        </div>
                        <div class="divForm">
                            <label class="label">*VPoint Enabled:</label>
                            <asp:CheckBox CssClass="checkbox" ID="chkAddVPointEnabled" OnCheckedChanged="chkAddVPointEnabled_OnCheckedChanged" AutoPostBack="true" runat="server" />
                        </div>
                        <div class="divForm">
                            <label class="label">*VPrePoint Enabled:</label>
                            <asp:CheckBox CssClass="checkbox" ID="chkAddVPrePointEnabled" OnCheckedChanged="chkAddVPrePointEnabled_OnCheckedChanged" AutoPostBack="true" runat="server" />
                        </div>
                        <asp:LinkButton CssClass="lnkButton" ID="btnAddPointType" runat="server" Text="Add Type" OnClick="addPointTypeButton_Click" ValidationGroup="AddPointType"></asp:LinkButton>
                    </div>

                    <!--Ajax Validators-->
                    <asp:RequiredFieldValidator ID="addPointTypeNameRequiredValidator" runat="server"
                        ErrorMessage="Point Type Name is a required field."
                        ControlToValidate="txtAddPointTypeName"
                        SetFocusOnError="true"
                        Display="None"
                        ValidationGroup="AddPointType">
                    </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="addPointTypeNameRequiredValidatorExtender" runat="server"
                        BehaviorID="addPointTypeNameRequiredValidatorExtender"
                        TargetControlID="addPointTypeNameRequiredValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>
                    <asp:RequiredFieldValidator ID="addDisplayNameRequiredValidator" runat="server"
                        ErrorMessage="Display Name is a required field."
                        ControlToValidate="txtAddDisplayName"
                        SetFocusOnError="true"
                        Display="None"
                        ValidationGroup="AddPointType">
                    </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="addDisplayNameRequiredValidatorExtender" runat="server"
                        BehaviorID="addDisplayNameRequiredValidatorExtender"
                        TargetControlID="addDisplayNameRequiredValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>  
                    <asp:RequiredFieldValidator ID="addPointClassRequiredValidator" runat="server"
                        ErrorMessage="Point Class is a required field."
                        ControlToValidate="ddlAddPointClass"
                        SetFocusOnError="true"
                        Display="None"
                        InitialValue="-1"
                        ValidationGroup="AddPointType">
                    </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="addPointClassRequiredValidatorExtender" runat="server"
                        BehaviorID="addPointClassRequiredValidatorExtender"
                        TargetControlID="addPointClassRequiredValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>
                </asp:Panel>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>

</asp:Content>




