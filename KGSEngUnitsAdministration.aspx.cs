﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSEngUnitsAdministration : SitePage
    {
        #region Properties

            private EngUnit mEngUnit;
            
            const string addEngUnitSuccess = " engineering unit addition was successful.";
            const string addEngUnitFailed = "Adding engineering unit failed. Please contact an administrator.";
            const string updateSuccessful = "Engineering unit update was successful.";
            const string updateFailed = "Engineering unit update failed. Please contact an administrator.";
            const string engUnitExists = "Engineering unit with provided reference id and ip already exists.";
            const string pointClassesExist = "Cannot delete engineering unit because point classes are associated. Please disassociate points classes.";
            const string equipVarExist = "Cannot delete engineering unit because equipment variables are associated. Please disassociate equipment variables.";
            const string buildVarExist = "Cannot delete engineering unit because buidling variables are associated. Please disassociate building variables.";
            const string deleteSuccessful = "Engineering unit deletion was successful.";
            const string deleteFailed = "Engineering unit deletion failed. Please contact an administrator.";            
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "EngUnits";            

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindEngUnits();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetEngUnitIntoEditForm(EngUnit engUnit)
            {
                //ID
                hdnEditID.Value = Convert.ToString(engUnit.EngUnitID);
                //DataSource Name
                txtEditEngUnit.Text = engUnit.EngUnits;
                //Description
                txtEditDescription.Value = engUnit.EngUnitsDescription; 
                //Min
                txtEditMinValue.Text = Convert.ToString(engUnit.MinValue);
                //Max
                txtEditMaxValue.Text = Convert.ToString(engUnit.MaxValue);
            }

        #endregion

        #region Load Eng Unit

            protected void LoadAddFormIntoEngUnit(EngUnit engUnit)
            {
                //DataSource Name
                engUnit.EngUnits = txtAddEngUnit.Text;
                //Description
                engUnit.EngUnitsDescription = txtAddDescription.Value;
                //Min
                engUnit.MinValue = Convert.ToDouble(txtAddMinValue.Text);
                //Max
                engUnit.MaxValue = Convert.ToDouble(txtAddMaxValue.Text);

                engUnit.DateModified = DateTime.UtcNow;
                engUnit.DateCreated = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoEngUnit(EngUnit engUnit)
            {
                //ID
                engUnit.EngUnitID = Convert.ToInt32(hdnEditID.Value);
                //Eng Unit
                engUnit.EngUnits = txtEditEngUnit.Text;
                //Description
                engUnit.EngUnitsDescription = txtEditDescription.Value;
                //Min
                engUnit.MinValue = Convert.ToDouble(txtEditMinValue.Text);
                //Max
                engUnit.MaxValue = Convert.ToDouble(txtEditMaxValue.Text);
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Add engineering unit Button on click.
            /// </summary>
            protected void addEngUnitButton_Click(object sender, EventArgs e)
            {
                mEngUnit = new EngUnit();

                //load the form into the eng unit
                LoadAddFormIntoEngUnit(mEngUnit);

                //check if reference id / ip exists
                if (DataMgr.EngineeringUnitsDataMapper.DoesEngUnitExist(mEngUnit.EngUnits, mEngUnit.EngUnitID))
                {
                    LabelHelper.SetLabelMessage(lblAddError, engUnitExists, lnkSetFocusAdd);
                }
                else
                {
                    try
                    {
                        //insert new engineering unit
                        DataMgr.EngineeringUnitsDataMapper.InsertEngineeringUnit(mEngUnit);
                        LabelHelper.SetLabelMessage(lblAddError, mEngUnit.EngUnits + addEngUnitSuccess, lnkSetFocusAdd);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding engineering unit.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addEngUnitFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding engineering unit.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addEngUnitFailed, lnkSetFocusAdd);
                    }
                }
            }


            /// <summary>
            /// Update eng unit Button on click.
            /// </summary>
            protected void updateEngUnitButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the eng unit
                EngUnit mEngUnit = new EngUnit();
                LoadEditFormIntoEngUnit(mEngUnit);

                //check if reference id / ip exists
                if (DataMgr.EngineeringUnitsDataMapper.DoesEngUnitExist(mEngUnit.EngUnits, mEngUnit.EngUnitID))
                {
                    LabelHelper.SetLabelMessage(lblEditError, engUnitExists, lnkSetFocusView);
                }
                else
                {
                    //try to update the engineering unit             
                    try
                    {
                        DataMgr.EngineeringUnitsDataMapper.UpdateEngineeringUnit(mEngUnit);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                        //Bind engineering units again
                        BindEngUnits();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating engineering unit.", ex);
                    }
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindEngUnits();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindEngUnits();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind buildings grid after tab changed back from add buidling tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindEngUnits();
                }
            }

        #endregion

        #region Grid Events

            protected void gridEngUnits_OnDataBound(object sender, EventArgs e)
            {                                     
            }

            protected void gridEngUnits_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditEngUnit.Visible = false;
                dtvEngUnit.Visible = true;
                
                int engUnitID = Convert.ToInt32(gridEngUnits.DataKeys[gridEngUnits.SelectedIndex].Values["EngUnitID"]);

                //set data source
                dtvEngUnit.DataSource = DataMgr.EngineeringUnitsDataMapper.GetFullEngUnitByID(engUnitID);
                //bind data source to details view
                dtvEngUnit.DataBind();
            }

            protected void gridEngUnits_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridEngUnits_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvEngUnit.Visible = false;
                pnlEditEngUnit.Visible = true;

                int engUnitID = Convert.ToInt32(gridEngUnits.DataKeys[e.NewEditIndex].Values["EngUnitID"]);

                EngUnit mEngUnit = DataMgr.EngineeringUnitsDataMapper.GetEngineeringUnit(engUnitID);
                
                //Set eng unit data
                SetEngUnitIntoEditForm(mEngUnit);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridEngUnits_Deleting(object sender, GridViewDeleteEventArgs e)
            {                
                //get deleting rows datasource id
                int engUnitID = Convert.ToInt32(gridEngUnits.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a eng unit if no point classes have
                    //been associated. Or if any equip vars have been associated
                                           
                    //check if point classes are assigned to that eng unit                    
                    if (DataMgr.EngineeringUnitsDataMapper.ArePointClassesAssignedToEngUnit(engUnitID))
                    {
                        lblErrors.Text = pointClassesExist;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else if (DataMgr.EngineeringUnitsDataMapper.AreEquipVarsAssignedToEngUnit(engUnitID))
                    {
                        lblErrors.Text = equipVarExist;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else if (DataMgr.EngineeringUnitsDataMapper.AreBuildVarsAssignedToEngUnit(engUnitID))
                    {
                        lblErrors.Text = buildVarExist;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        DataMgr.EngineeringUnitsDataMapper.DeleteEngineeringUnit(engUnitID);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();

                        pnlEditEngUnit.Visible = false;
                        dtvEngUnit.Visible = false;
                    }   
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting engineering unit.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryEngUnits());
                gridEngUnits.PageIndex = gridEngUnits.PageIndex;
                gridEngUnits.DataSource = SortDataTable(dataTable as DataTable, true);
                gridEngUnits.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditEngUnit.Visible = false;  
            }

            protected void gridEngUnits_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEngUnits(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridEngUnits.DataSource = SortDataTable(dataTable, true);
                gridEngUnits.PageIndex = e.NewPageIndex;
                gridEngUnits.DataBind();
            }

            protected void gridEngUnits_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridEngUnits.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEngUnits(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridEngUnits.DataSource = SortDataTable(dataTable, false);
                gridEngUnits.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<EngUnit> QueryEngUnits()
            {
                try
                {
                    //get all eng units 
                    return DataMgr.EngineeringUnitsDataMapper.GetAllEngineeringUnits();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving engineering units.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving engineering units.", ex);
                    return null;
                }
            }

            private IEnumerable<EngUnit> QuerySearchedEngUnits(string searchText)
            {
                try
                {
                    //get all eng units 
                    return DataMgr.EngineeringUnitsDataMapper.GetAllSearchedEngUnits(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving engineering units.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving engineering units.", ex);
                    return null;
                }
            }

            private void BindEngUnits()
            {
                //query data sources
                IEnumerable<EngUnit> engUnits = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryEngUnits() : QuerySearchedEngUnits(txtSearch.Text);

                int count = engUnits.Count();

                gridEngUnits.DataSource = engUnits;

                // bind grid
                gridEngUnits.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedEngUnits(string[] searchText)
            //{
            //    //query data sources
            //    IEnumerable<GetEngUnitsData> engUnits = QuerySearchedEngUnits(searchText);

            //    int count = engUnits.Count();

            //    gridEngUnits.DataSource = engUnits;

            //    // bind grid
            //    gridEngUnits.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} eng unit found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} eng units found.", count);
                }
                else
                {
                    lblResults.Text = "No eng units found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

        #endregion
    }
}

