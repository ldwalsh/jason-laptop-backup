﻿using CW.Common.Constants;
using CW.Data;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSBuildingClassAdministration: SitePage
    {
        #region Properties

            private BuildingClass mBuildingClass;
            const string addBuildingClassSuccess = " building class addition was successful.";
            const string addBuildingClassFailed = "Adding building class failed. Please contact an administrator.";
            const string buildingClassExists = "A building class with that name already exists.";
            const string updateSuccessful = "Building class update was successful.";
            const string updateFailed = "Building class update failed. Please contact an administrator.";
            const string deleteSuccessful = "Building class deletion was successful.";
            const string deleteFailed = "Building class deletion failed. Please contact an administrator.";
            const string buildingTypeExists = "Cannot delete building class because building type is associated. Please disassociate from building type first.";
            const string buildingVariableExists = "Cannot delete building class because building variable is associated. Please disassociate from building variable first."; 

            const string initialSortDirection = "ASC";
            const string initialSortExpression = "BuildingClassName";

            const string reassignVariablesUpdateSuccessful = "Building variables reassignment was successful.";
            const string reassignVariablesUpdateFailed = "Building variables reassignment failed. Please contact an administrator.";
            const string buildingRequiresBuildingVariables = "One or more building variables could not be unassigned because an associated building with that class requires them.";

            const string reassignVariableUpdateSuccessful = "Building variable reassignment was successful.";
            const string reassignVariableUpdateFailed = "Building variable reassignment failed. Please contact an administrator.";
            const string buildingRequiresBuildingVariable = "One or more building classes could not be unassigned because an associated building with that class requires that variable.";

            
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindBuildingClasses();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    //bind dropdowns
                    BindBuildingClasses(ddlVariablesBuildingClass);
                    BindBuildingVariables(ddlVariableBuildingVariable);

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Binds dropdownlist with all building classes
            /// </summary>
            /// <param name="ddl"></param>
            private void BindBuildingClasses(DropDownList ddl)
            {
                ddl.DataTextField = "BuildingClassName";
                ddl.DataValueField = "BuildingClassID";
                ddl.DataSource = DataMgr.BuildingClassDataMapper.GetAllBuildingClasses();
                ddl.DataBind();
            }

            /// <summary>
            /// Binds dropdownlist with all building variables
            /// </summary>
            /// <param name="ddl"></param>
            private void BindBuildingVariables(DropDownList ddl)
            {
                ddl.DataTextField = "BuildingVariableDisplayName";
                ddl.DataValueField = "BVID";
                ddl.DataSource = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariables();
                ddl.DataBind();
            }

            /// <summary>
            /// Binds top and bottom list boxes with building variables by buildingClassID
            /// 
            /// top: all not yet associated to buildingClassID
            /// bottom: all currently associated to buildingClassID
            /// </summary>
            /// <param name="buildingClassID"></param>
            private void BindBuildingVariablesListBoxes(int buildingClassID)
            {
                //get building variables associated to buildingClassID 
                IEnumerable<BuildingVariable> mBuildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesAssociatedToBuildingClassID(buildingClassID);

                if (mBuildingVariables.Any())
                {
                    //bind building variables to bottom listbox
                    lbVariablesBottom.DataSource = mBuildingVariables;
                    lbVariablesBottom.DataTextField = "BuildingVariableDisplayName";
                    lbVariablesBottom.DataValueField = "BVID";
                    lbVariablesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbVariablesBottom.Items.Clear();
                }

                //get building variables not associated to buildingClassID
                mBuildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesNotAssociatedToBuildingClassID(buildingClassID);

                if (mBuildingVariables.Any())
                {
                    //bind building variables to top listbox
                    lbVariablesTop.DataSource = mBuildingVariables;
                    lbVariablesTop.DataTextField = "BuildingVariableDisplayName";
                    lbVariablesTop.DataValueField = "BVID";
                    lbVariablesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbVariablesTop.Items.Clear();
                }

                buildingVariables.Visible = true;
            }

            /// <summary>
            /// Binds top and bottom list boxes with building classes by BVID
            /// 
            /// top: all not yet associated to BVID
            /// bottom: all currently associated to BVID
            /// </summary>
            /// <param name="bvid"></param>
            private void BindBuildingClassesListBoxes(int bvid)
            {
                //get building classes associated to BVID 
                IEnumerable<BuildingClass> mBuildingClasses = DataMgr.BuildingClassDataMapper.GetAllBuildingClassesAssociatedToBVID(bvid);

                if (mBuildingClasses.Any())
                {
                    //bind building classes to bottom listbox
                    lbClassesBottom.DataSource = mBuildingClasses;
                    lbClassesBottom.DataTextField = "BuildingClassName";
                    lbClassesBottom.DataValueField = "BuildingClassID";
                    lbClassesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbClassesBottom.Items.Clear();
                }

                //get building classes not associated to bvid
                mBuildingClasses = DataMgr.BuildingClassDataMapper.GetAllBuildingClassesNotAssociatedToBVID(bvid);

                if (mBuildingClasses.Any())
                {
                    //bind building classes to top listbox
                    lbClassesTop.DataSource = mBuildingClasses;
                    lbClassesTop.DataTextField = "BuildingClassName";
                    lbClassesTop.DataValueField = "BuildingClassID";
                    lbClassesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbClassesTop.Items.Clear();
                }

                buildingClasses.Visible = true;
            }

        #endregion
        
        #region Set Fields and Data

            protected void SetBuildingClassIntoEditForm(BuildingClass buildingClass)
            {
                //ID
                hdnEditID.Value = Convert.ToString(buildingClass.BuildingClassID);
                //Building Class Name
                txtEditBuildingClassName.Text = buildingClass.BuildingClassName;

                //Building Variable Description
                txtEditDescription.Value = String.IsNullOrEmpty(buildingClass.BuildingClassDescription) ? null : buildingClass.BuildingClassDescription;
            }

        #endregion

        #region Load Building Class

            protected void LoadAddFormIntoBuildingClass(BuildingClass buildingClass)
            {
                //Building Class Name
                buildingClass.BuildingClassName = txtAddBuildingClassName.Text;
                buildingClass.BuildingClassDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                buildingClass.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoBuildingClass(BuildingClass buildingClass)
            {
                //ID
                buildingClass.BuildingClassID = Convert.ToInt32(hdnEditID.Value);
                //Building Class Name
                buildingClass.BuildingClassName = txtEditBuildingClassName.Text;
                buildingClass.BuildingClassDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                buildingClass.DateModified = DateTime.UtcNow;
            }

        #endregion

        #region Dropdown Events

            /// <summary>
            /// ddlVariablesBuildingClass on selected index changed, binds building variables list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlVariablesBuildingClass_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int buildingClassID = Convert.ToInt32(ddlVariablesBuildingClass.SelectedValue);

                if (buildingClassID != -1)
                {
                    BindBuildingVariablesListBoxes(buildingClassID);
                }
                else
                {
                    //hide building variables
                    buildingVariables.Visible = false;
                }

                //hide error message
                lblVariablesError.Visible = false;
            }

            /// <summary>
            /// ddlVariableBuildingVariable on selected index changed, binds building classes list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlVariableBuildingVariables_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int bvid = Convert.ToInt32(ddlVariableBuildingVariable.SelectedValue);

                if (bvid != -1)
                {
                    BindBuildingClassesListBoxes(bvid);
                }
                else
                {
                    //hide building classes
                    buildingClasses.Visible = false;
                }

                //hide error message
                lblVariableError.Visible = false;
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Add building class Button on click.
            /// </summary>
            protected void addBuildingClassButton_Click(object sender, EventArgs e)
            {
                //check that building class does not already exist
                if (!DataMgr.BuildingClassDataMapper.DoesBuildingClassExist(txtAddBuildingClassName.Text))
                {
                    mBuildingClass = new BuildingClass();

                    //load the form into the building class
                    LoadAddFormIntoBuildingClass(mBuildingClass);

                    try
                    {
                        //insert new building class
                        DataMgr.BuildingClassDataMapper.InsertBuildingClass(mBuildingClass);

                        LabelHelper.SetLabelMessage(lblAddError, mBuildingClass.BuildingClassName + addBuildingClassSuccess, lnkSetFocusAdd);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building class.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addBuildingClassFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building class.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addBuildingClassFailed, lnkSetFocusAdd);
                    }
                }
                else
                {
                    //building class exists
                    LabelHelper.SetLabelMessage(lblAddError, buildingClassExists, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update building class Button on click. Updates building class data.
            /// </summary>
            protected void updateBuildingClassButton_Click(object sender, EventArgs e)
            {
                BuildingClass mBuildingClass = new BuildingClass();

                //load the form into the Building class
                LoadEditFormIntoBuildingClass(mBuildingClass);

                //check if the buildingclass name exists for another buildingclass if trying to change
                if (DataMgr.BuildingClassDataMapper.DoesBuildingClassExist(mBuildingClass.BuildingClassID, mBuildingClass.BuildingClassName))
                {
                    LabelHelper.SetLabelMessage(lblEditError, buildingClassExists, lnkSetFocusEdit);
                }
                //update the building class
                else
                {
                    //try to update the Building class              
                    try
                    {
                        DataMgr.BuildingClassDataMapper.UpdateBuildingClass(mBuildingClass);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusEdit);

                        //Bind Building classes again
                        BindBuildingClasses();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building class.", ex);
                    }
                }
            }

            /// <summary>
            /// Update building variables button click, reassigns variables to building class based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateBuildingVariablesButton_Click(object sender, EventArgs e)
            {
                int buildingClassID = Convert.ToInt32(ddlVariablesBuildingClass.SelectedValue);
                int bvid;
                bool buildVarInUse = false;
               
                try
                {
                    //inserting each building class variable in the bottom listbox if they dont exist
                    foreach (ListItem item in lbVariablesBottom.Items)
                    {
                        //declare new building class variable
                        BuildingClasses_BuildingVariable mBuildingClassVariable = new BuildingClasses_BuildingVariable();

                        bvid = Convert.ToInt32(item.Value);
                        mBuildingClassVariable.BuildingClassID = buildingClassID;
                        mBuildingClassVariable.BVID = bvid;

                        //if the building class variable doesnt already exist, insert
                        if (!DataMgr.BuildingVariableDataMapper.DoesBuildingVariableExistForBuildingClass(buildingClassID, bvid))
                        {
                            DataMgr.BuildingVariableDataMapper.InsertBuildingVariable(mBuildingClassVariable);
                        }
                    }

                    //deleting each building class variable in the top listbox if they exist
                    foreach (ListItem item in lbVariablesTop.Items)
                    {
                        bvid = Convert.ToInt32(item.Value);

                        //if the building class variable exists, delete
                        if (DataMgr.BuildingVariableDataMapper.DoesBuildingVariableExistForBuildingClass(buildingClassID, bvid))
                        {
                            //checks if the building class variable is associated to any building with class id
                            //Note: On building to building variable page it checks if an analysis is assigned to the equipment that required that building variable.
                            if (!DataMgr.BuildingVariableDataMapper.IsBuildingVariableAssociatedToBuildingByBuildingClassID(bvid, buildingClassID))
                            {
                                DataMgr.BuildingVariableDataMapper.DeleteBuildingVariableByBuildingClassIDAndBVID(buildingClassID, bvid);
                            }
                            else
                            {
                                buildVarInUse = true;
                            }
                        }
                    }

                    LabelHelper.SetLabelMessage(lblVariablesError, buildVarInUse ? buildingRequiresBuildingVariables : reassignVariablesUpdateSuccessful, lnkSetFocusVariables);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning building variables to building class.", sqlEx);
                    LabelHelper.SetLabelMessage(lblVariablesError, reassignVariablesUpdateFailed, lnkSetFocusVariables);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning building variables to building class.", ex);
                    LabelHelper.SetLabelMessage(lblVariablesError, reassignVariablesUpdateFailed, lnkSetFocusVariables);
                }
            }

            /// <summary>
            /// on button up click, remove building variables from bottom (unassociate to build class)
            /// </summary>
            protected void btnVariablesUpButton_Click(object sender, EventArgs e)
            {
                while (lbVariablesBottom.SelectedIndex != -1)
                {
                    lbVariablesTop.Items.Add(lbVariablesBottom.SelectedItem);
                    lbVariablesBottom.Items.Remove(lbVariablesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add building variables to bottom (associate to build class)
            /// </summary>
            protected void btnVariablesDownButton_Click(object sender, EventArgs e)
            {
                while (lbVariablesTop.SelectedIndex != -1)
                {
                    {
                        lbVariablesBottom.Items.Add(lbVariablesTop.SelectedItem);
                        lbVariablesTop.Items.Remove(lbVariablesTop.SelectedItem);
                    }
                }
            }


            /// <summary>
            /// Update building variable button click, reassigns variable to building classes based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateBuildingVariableButton_Click(object sender, EventArgs e)
            {
                int bvid = Convert.ToInt32(ddlVariableBuildingVariable.SelectedValue);
                int buildingClassID;
                bool buildVarInUse = false;

                try
                {
                    //inserting each building class variable in the bottom listbox if they dont exist
                    foreach (ListItem item in lbClassesBottom.Items)
                    {
                        //declare new building class variable
                        BuildingClasses_BuildingVariable mBuildingClassVariable = new BuildingClasses_BuildingVariable();
                
                        buildingClassID = Convert.ToInt32(item.Value);
                        mBuildingClassVariable.BuildingClassID = buildingClassID;
                        mBuildingClassVariable.BVID = bvid;

                        //if the building class variable doesnt already exist, insert
                        if (!DataMgr.BuildingVariableDataMapper.DoesBuildingVariableExistForBuildingClass(buildingClassID, bvid))
                        {
                            DataMgr.BuildingVariableDataMapper.InsertBuildingVariable(mBuildingClassVariable);
                        }
                    }

                    //deleting each building class variable in the top listbox if they exist
                    foreach (ListItem item in lbClassesTop.Items)
                    {
                        buildingClassID = Convert.ToInt32(item.Value);

                        //if the building class variable exists, delete
                        if (DataMgr.BuildingVariableDataMapper.DoesBuildingVariableExistForBuildingClass(buildingClassID, bvid))
                        {
                            //checks if the building class variable is associated to any building with class id
                            //Note: On building to building variable page it checks if an analysis is assigned to the equipment that required that building variable.
                            if (!DataMgr.BuildingVariableDataMapper.IsBuildingVariableAssociatedToBuildingByBuildingClassID(bvid, buildingClassID))
                            {
                                DataMgr.BuildingVariableDataMapper.DeleteBuildingVariableByBuildingClassIDAndBVID(buildingClassID, bvid);
                            }
                            else
                            {
                                buildVarInUse = true;
                            }
                        }
                    }

                    LabelHelper.SetLabelMessage(lblVariableError, buildVarInUse ? buildingRequiresBuildingVariable : reassignVariableUpdateSuccessful, lnkSetFocusVariable);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning building variable to building classes.", sqlEx);
                    LabelHelper.SetLabelMessage(lblVariableError, reassignVariableUpdateFailed, lnkSetFocusVariable);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning building variable to building classes.", ex);
                    LabelHelper.SetLabelMessage(lblVariableError, reassignVariableUpdateFailed, lnkSetFocusVariable);
                }
            }

            /// <summary>
            /// on button up click, remove building classes from bottom (unassociate with build var)
            /// </summary>
            protected void btnClassesUpButton_Click(object sender, EventArgs e)
            {
                while (lbClassesBottom.SelectedIndex != -1)
                {
                    lbClassesTop.Items.Add(lbClassesBottom.SelectedItem);
                    lbClassesBottom.Items.Remove(lbClassesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add building classes to bottom (associate to build var)
            /// </summary>
            protected void btnClassesDownButton_Click(object sender, EventArgs e)
            {
                while (lbClassesTop.SelectedIndex != -1)
                {
                    {
                        lbClassesBottom.Items.Add(lbClassesTop.SelectedItem);
                        lbClassesTop.Items.Remove(lbClassesTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindBuildingClasses();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindBuildingClasses();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind building class grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind building classes to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindBuildingClasses();
                }
            }

        #endregion

        #region Grid Events

            protected void gridBuildingClasses_OnDataBound(object sender, EventArgs e)
            {                         
            }

            protected void gridBuildingClasses_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridBuildingClasses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditBuildingClass.Visible = false;
                dtvBuildingClass.Visible = true;
                
                int buildingClassID = Convert.ToInt32(gridBuildingClasses.DataKeys[gridBuildingClasses.SelectedIndex].Values["BuildingClassID"]);

                //set data source
                dtvBuildingClass.DataSource = DataMgr.BuildingClassDataMapper.GetBuildingClassByIDAsEnumerable(buildingClassID);
                //bind building class to details view
                dtvBuildingClass.DataBind();
            }

            protected void gridBuildingClasses_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvBuildingClass.Visible = false;
                pnlEditBuildingClass.Visible = true;

                int buildingClassID = Convert.ToInt32(gridBuildingClasses.DataKeys[e.NewEditIndex].Values["BuildingClassID"]);

                BuildingClass mBuildingClass = DataMgr.BuildingClassDataMapper.GetBuildingClassByID(buildingClassID);

                //Set Building Class data
                SetBuildingClassIntoEditForm(mBuildingClass);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridBuildingClasses_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows buildingClassid
                int buildingClassID = Convert.ToInt32(gridBuildingClasses.DataKeys[e.RowIndex].Value);

                try
                {
                    //Only allow deletion of a building class if no building type
                    //has been associated.                    

                    //check if building type is associated to that building class
                    if (DataMgr.BuildingTypeDataMapper.IsBuildingTypeAssociatedWithBuildingClass(buildingClassID))
                    {
                        lblErrors.Text = buildingTypeExists;
                    }
                    //checks if any variables are still associated to the class                        
                    else if (DataMgr.BuildingVariableDataMapper.IsAnyBuildingVariableAssociatedToBuildingClass(buildingClassID))
                    {
                        lblErrors.Text = buildingVariableExists;
                    }
                    else
                    {
                        //delete building class
                        DataMgr.BuildingClassDataMapper.DeleteBuildingClass(buildingClassID);

                        lblErrors.Text = deleteSuccessful;
                    }

                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting building class.", ex);
                }

                lblErrors.Visible = true;
                lnkSetFocusView.Focus();

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryBuildingClasses());
                gridBuildingClasses.PageIndex = gridBuildingClasses.PageIndex;
                gridBuildingClasses.DataSource = SortDataTable(dataTable as DataTable, true);
                gridBuildingClasses.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditBuildingClass.Visible = false;
            }

            protected void gridBuildingClasses_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBuildingClasses(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridBuildingClasses.DataSource = SortDataTable(dataTable, true);
                gridBuildingClasses.PageIndex = e.NewPageIndex;
                gridBuildingClasses.DataBind();
            }

            protected void gridBuildingClasses_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridBuildingClasses.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBuildingClasses(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridBuildingClasses.DataSource = SortDataTable(dataTable, false);
                gridBuildingClasses.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<BuildingClass> QueryBuildingClasses()
            {
                try
                {
                    //get all building classes 
                    return DataMgr.BuildingClassDataMapper.GetAllBuildingClasses();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building classes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building classes.", ex);
                    return null;
                }
            }

            private IEnumerable<BuildingClass> QuerySearchedBuildingClasses(string textSearch)
            {
                try
                {
                    //get all building classes 
                    return DataMgr.BuildingClassDataMapper.GetAllSearchedBuildingClasses(textSearch);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building classes.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building classes.", ex);
                    return null;
                }
            }

            private void BindBuildingClasses()
            {
                //query building classes
                IEnumerable<BuildingClass> classes = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryBuildingClasses() : QuerySearchedBuildingClasses(txtSearch.Text);

                int count = classes.Count();

                gridBuildingClasses.DataSource = classes;

                // bind grid
                gridBuildingClasses.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedBuildingClasses(string[] searchText)
            //{
            //    //query building classes
            //    IEnumerable<BuildingClass> classes = QuerySearchedBuildingClasses(searchText);

            //    int count = classes.Count();

            //    gridBuildingClasses.DataSource = classes;

            //    // bind grid
            //    gridBuildingClasses.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} building class found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} building classes found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No building classes found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }    

        #endregion
    }
}

