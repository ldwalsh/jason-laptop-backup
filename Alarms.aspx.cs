﻿using CW.Data;
using CW.Utility;
using CW.Website._framework;
using CW.Common.Constants;
using EO.Pdf;
using System;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Telerik.Web.UI.Dock;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class Alarms : SitePage
    {
        #region Fields

        private const int range = BusinessConstants.Alarm.AlarmsDailyCacheInDays;

        #endregion

        #region Properties

        private SiteUser mSiteUser;

        #endregion

        #region Page Events

            protected override void OnLoad(EventArgs e)
            {
                mSiteUser = siteUser;

                if (!Page.IsPostBack)
                {
                    SetClientInfoAndImages();

                    //Load module global settings content, make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litAlarmsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.AlarmsBody);

                    //dynamically create all common events and settings for each radDockZone and radDock
                    foreach (var radDockZone in this.FindControls<RadDockZone>())
                    {
                        radDockZone.BorderStyle = BorderStyle.None;
                        radDockZone.FitDocks = false;
                        radDockZone.Orientation = Orientation.Horizontal;
                    }

                    foreach (var radDock in this.FindControls<RadDock>())
                    {
                        radDock.DefaultCommands = DefaultCommands.ExpandCollapse;
                        radDock.EnableAnimation = true;
                        radDock.OnClientCommand = "OnClientCommand";
                        radDock.OnClientInitialize = "OnClientInitialize";
                        radDock.EnableRoundedCorners = true;
                        radDock.DockMode = DockMode.Docked;
                        radDock.Resizable = false;

                        if (radDock.ID != "radDockSearchAlarms")
                        {
                            radDock.OnClientDragStart = "OnClientDragStart";
                            radDock.AllowedZones = new string[] { "radDockZone1", "radDockZone2" };
                            radDock.CssClass = "radDockMedium";
                        }
                        else
                        {
                            radDock.EnableDrag = false;
                            radDock.CssClass = "radDockLarge";
                        }
                    }

                    //set the available range for the calendar start and end dates (92 days)
                    DateTime yesterday;
                    DateTime today;

                    DateTimeHelper.GenerateDefaultDateYesterday(out yesterday, siteUser.UserTimeZoneOffset);
                    DateTimeHelper.GenerateDefaultDateCurrentDay(out today, siteUser.UserTimeZoneOffset);

                    txtStartDate.MinDate = txtEndDate.MinDate = today.AddDays(-range);

                    txtStartDate.MaxDate = yesterday;
                    txtEndDate.MaxDate = today;

                    txtStartDate.SelectedDate = yesterday.Date;
                    txtEndDate.SelectedDate = today.Date;
                }
            }

        #endregion

        #region Button Events

            protected void btnDownloadButton_Click(object sender, EventArgs e)
            {                
                var invisibleElementIds =   imgPdfDownload.ClientID + ";" + radDockBuildingMap.ClientID + ";refreshData;refreshBtn;hdn_container;buildingMap;topTenMostFrequentAlarms;todaysTopTenLongestStandingAlarms;equipmentClassAlarmSummary;alarmIncidenceGraph;topTenTotalTimeInAlarm;buildingAlarmSummary;searchAlarms;";

                var eo = new EssentialObjectsHelper(invisibleElementIds, LogMgr);
                eo.CreatePdfHeader(siteUser.IsSchneiderTheme);
                eo.CreatePdfFooter(siteUser.IsSchneiderTheme, "Alarms Report");

                var pdfBytes = eo.ConvertHtmlAndReturnBytes(Server.HtmlDecode(hdn_container.Value));

                Boolean result;

                try
                {
                    result = new FilePublishService
                    (
                        DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                        DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                        siteUser.IsKGSFullAdminOrHigher,
                        siteUser.IsSchneiderTheme,
                        siteUser.Email,
                        new ModulePDFGenerator(pdfBytes),
                        LogMgr
                    ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productAlarms.pdf", true));
                }
                catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
                {
                    DisplayErrorOutput("Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                    return;
                }

                if (result) return;
            }

         #endregion

        #region Helper Methods

            private void SetClientInfoAndImages()
            {
                var client = DataMgr.ClientDataMapper.GetClient(mSiteUser.CID);

                if (client == null) return;

                imgHeaderClientImage.AlternateText = string.Empty;
                imgHeaderClientImage.Visible = true;
                imgHeaderClientImage.ImageUrl = HandlerHelper.ClientImageUrl(client.CID, client.ImageExtension);

                lblHeaderClientName.InnerText = mSiteUser.ClientName;
            }

        #endregion
    }
}