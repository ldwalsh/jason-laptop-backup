﻿<%@ Page Language="C#" EnableEventValidation="false" MasterPageFile="~/_masters/Operations.Master" AutoEventWireup="false" CodeBehind="Operations.aspx.cs" Inherits="CW.Website.Operations" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <script type="text/javascript">

        function autoUpdate()
        {
            setInterval(autoClick, 600000);
        }

        function autoClick()
        {
            var prm = Sys.WebForms.PageRequestManager.getInstance();

            if (!prm.get_isInAsyncPostBack()) document.getElementById("ctl00_plcCopy_btnGenerate").click()
        }

    </script>


 <telerik:RadAjaxManager ID="radAjaxManager" runat="server">  
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="btnGenerate">
                <UpdatedControls>                   
                    <telerik:AjaxUpdatedControl ControlID="radUpdatePanelNested2" />                     
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
 </telerik:RadAjaxManager>

 <telerik:RadAjaxLoadingPanel ID="lp" runat="server" Skin="Default" />  

 <div id="operationsDashboard" class="operationsDashboard">

    <div id="dashboardHeader" class="dashboardHeader">  

      <div class="operationsModuleIcon"></div>      
      <div class="headerTitle">
	    Operations
	  </div>
      <div class="headerSpacer"></div>
	  <div class="headerClientInfo noMobile">
        <div class="headerClientImage">     
          <asp:Image ID="imgHeaderClientImage" CssClass="imgHeader" runat="server" />
        </div>
        <div class="headerClientName">
		  <label id="lblHeaderClientName" runat="server" />
	    </div>
	  </div>
      <div class="headerSpacer"></div>
      <div class="headerContent">
          <asp:Literal ID="litBody" runat="server" />
      </div>            
    </div> <!--end of dashboardHeader div-->
    
    <hr />
        
    <telerik:RadAjaxPanel ID="radUpdatePanelNested" ValidateRequestMode="Disabled" runat="server" UpdateMode="Conditional" LoadingPanelID="lpNested">
    <telerik:RadAjaxLoadingPanel ID="lpNested" runat="server" Skin="Default" />

    <div class="dashboardFilters">
        <asp:Panel ID="pnlFilters" runat="server">

          <div class="dockWrapper">
            <div class="divDockFormWrapperMedium">
            <div class="divDockForm">
              <label class="labelBold">Building:</label>
              <asp:DropDownList ID="ddlBuildings" CssClass="dropdown" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlBuildings_SelectedIndexChanged" />           
                <asp:RequiredFieldValidator ID="buildingRequiredValidator" runat="server"
                    ErrorMessage="Building is a required field." 
                    ControlToValidate="ddlBuildings"  
                    SetFocusOnError="true" 
                    Display="None" 
                    InitialValue="-1"
                    ValidationGroup="Dashboard">
                    </asp:RequiredFieldValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="buildingRequiredValidatorExtender" runat="server"
                    BehaviorID="buildingRequiredValidatorExtender" 
                    TargetControlID="buildingRequiredValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>    
            </div>

            <div class="divDockForm">
              <label class="labelBold">Equipment Class:</label>
              <asp:DropDownList ID="ddlEquipmentClass" CssClass="dropdown" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlEquipmentClass_SelectedIndexChanged" />
                <asp:RequiredFieldValidator ID="equipmentClassRequiredValidator" runat="server"
                    ErrorMessage="Equipment Class is a required field." 
                    ControlToValidate="ddlEquipmentClass"  
                    SetFocusOnError="true" 
                    Display="None" 
                    InitialValue="-1"
                    ValidationGroup="Dashboard">
                    </asp:RequiredFieldValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="equipmentClassRequiredValidatorExtender" runat="server"
                    BehaviorID="equipmentClassRequiredValidatorExtender" 
                    TargetControlID="equipmentClassRequiredValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>    
            </div>
            <div class="divDockForm">
              <label class="labelBold">Equipment Type:</label>
              <asp:DropDownList ID="ddlEquipmentType" CssClass="dropdown" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlEquipmentType_SelectedIndexChanged" />
                <asp:RequiredFieldValidator ID="equipmentTypeRequiredValidator" runat="server"
                    ErrorMessage="Equipment Type is a required field." 
                    ControlToValidate="ddlEquipmentType"  
                    SetFocusOnError="true" 
                    Display="None" 
                    InitialValue="-1"
                    ValidationGroup="Dashboard">
                    </asp:RequiredFieldValidator>
                <ajaxToolkit:ValidatorCalloutExtender ID="equipmentTypeRequiredValidatorExtender" runat="server"
                    BehaviorID="equipmentTypeRequiredValidatorExtender" 
                    TargetControlID="equipmentTypeRequiredValidator"
                    HighlightCssClass="validatorCalloutHighlight"
                    Width="175">
                    </ajaxToolkit:ValidatorCalloutExtender>    
            </div>
            <div id="divEquipment" class="divDockForm" runat="server">
              <label class="labelBold">Equipment:</label>
              <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" runat="server" AppendDataBoundItems="true" />                
            </div>
            <div class="divDockForm noMobile">
                <label class="labelBold">*Columns:</label>
                <extensions:ListBoxExtension ID="lbColumns" runat="server" SelectionMode="Multiple" CssClass="listboxShortestNarrow">
                    <asp:ListItem Text="Point Name" Value="1" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Point Type" Value="2"></asp:ListItem>                    
                    <asp:ListItem Text="Current Value" Value="3" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Current Time" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Prev. Value" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Prev. Time" Value="6"></asp:ListItem>
                    <asp:ListItem Text="Min/Max" Value="7"></asp:ListItem>
                    <asp:ListItem Text="Eng. Unit" Value="8"></asp:ListItem>
                    <asp:ListItem Text="Past Ten Chart" Value="9"></asp:ListItem>                                        
                    <asp:ListItem Text="Links" Value="10"></asp:ListItem>                    
                </extensions:ListBoxExtension>
            </div>
            <div class="divDockForm noMobile">
                <label class="labelBold">Charts:</label>                                    
                <asp:CheckBox ID="chkGraphs" CssClass="checkbox" runat="server"/>
            </div>
            <div class="divDockForm">     
                <span class="labelBold">Refresh Data:</span>      
                <asp:LinkButton CssClass="lnkBtnRefreshDock" ID="btnGenerate" runat="server" Text="" OnClick="generateButton_Click" ValidationGroup="Dashboard"></asp:LinkButton>
            </div> 
            
            <asp:ImageButton ID="imgPdfDownload" Enabled="false" CssClass="imgDashboardDownload" ImageUrl="../../_assets/images/pdf-icon.jpg"  OnClientClick="refreshHtml();" OnClick="btnDownloadButton_Click" AlternateText="download" runat="server" CausesValidation="false" />
          
            <input type="hidden" runat="server" id="hdn_container" />
          </div>
          </div>

        </asp:Panel>      
    </div>

    </telerik:RadAjaxPanel>

    <br />

    <telerik:RadAjaxPanel ID="radUpdatePanelNested2" runat="server" UpdateMode="Conditional" LoadingPanelID="lpNested">

    <div id="dashboardBody" class="dashboardBody">

        <asp:Panel CssClass="dashboardMessage" ID="pnlMessage" Visible="true" runat="server">
            <asp:Label ID="lblMessage" runat="server" Text="Please make selections in order to view current raw operational data."></asp:Label>
        </asp:Panel>        

        <div id="pdfOutput">
        <asp:Panel ID="pnlDashboard" Visible="false" runat="server">
            
            <div>
                <asp:PlaceHolder ID="phTables" runat="server"></asp:PlaceHolder>
            </div>

            <div class="clear"></div>

            <div>
                <asp:Literal ID="litChart" runat="server"></asp:Literal>
                <asp:HiddenField ID="hdnChart" runat="server" />
            </div>                  

        </asp:Panel>
        </div> <!-- end of pdfOutput div for EO-->

      </div> <!--end of dashboardBody div-->
    
      <asp:Literal ID="litPerf" Visible="false" runat="server"></asp:Literal>        
          
    </telerik:RadAjaxPanel>

  </div> <!--end of operationsdashboards div-->

  <div id="dashboardFooter" class="dashboardFooter">             
  </div>

  <script language="javascript" type="text/javascript">
        function refreshHtml() {
            document.getElementById('ctl00_plcCopy_hdn_container').value = document.head.innerHTML + document.getElementById('pdfOutput').innerHTML;
            //document.head.innerHTML + document.body.innerHTML;
        }

        document.getElementById('ctl00_plcCopy_hdn_container').value = document.head.innerHTML + document.getElementById('pdfOutput').innerHTML;
        //document.head.innerHTML + document.body.innerHTML;
  </script>

</asp:Content>