﻿<%@ Page Language="C#" MasterPageFile="~/_masters/BuildingProfile.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="BuildingProfile.aspx.cs" Inherits="CW.Website.BuildingProfile" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="CW.Website" Namespace="CW.Website._extensions" TagPrefix="extensions" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
  
    <div class="buildingProfilesModuleIcon">
        <h1 id="header" runat="server">Building Profile</h1>   
    </div>
    <div class="richText">           
    </div>                                      
  
    <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
            <ProgressTemplate>
                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
            </ProgressTemplate>
        </asp:UpdateProgress>  
    </div>
       
    <asp:Panel id="pnlInitialSelection" runat="server">        

        <!--Building Selection-->                               
        <div class="divForm">   
            <label class="label">*Select Building:</label> 
            <extensions:DropDownExtension ID="ddlBuildingsInitial" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildingsInitial_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
            </extensions:DropDownExtension> 
        </div> 
        <hr /> 

        <telerik:RadTileList runat="server" ID="BuildingRadTileList" 
                RenderMode="Mobile"
                EnableDragAndDrop="false" 
                SelectionMode="None" AutoPostBack="true"
                OnTileDataBound="RadTileList_OnTileDataBound"
                OnTileClick="BuildingRadTileList_OnTileClick"                            
                >
            <DataBindings>
            <CommonTileBinding DataNameField="BID" TileType="RadContentTemplateTile" Shape="Square" DataGroupNameField="BuildingTypeName" />                                                       
                <ContentTemplateTileBinding>                                
                    <ContentTemplate>
                            <div class="contentTemplateClass contentTemplateClass<%#DataBinder.Eval(Container.DataItem, "CostThresholdValue")%>">
                                <div class="contentTileHeader"><%#StringHelper.TrimText(DataBinder.Eval(Container.DataItem, "BuildingName"), 15)%></div>
                                <div class="contentTileHeader2">Yesterday's Details</div>                           
                                <strong>Cost:</strong>
                                <%#DataBinder.Eval(Container.DataItem, "YesterdayTotalCost")%>
                                <br />
                                <strong>Energy Score:</strong>
                                <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "YesterdayEnergyScore"))%>
                                <br />
                                <strong>Maintenance Score:</strong>
                                <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "YesterdayMaintenanceScore"))%>
                                <br />
                                <strong>Comfort Score:</strong>
                                <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "YesterdayComfortScore"))%>
                            </div>
                    </ContentTemplate>
                </ContentTemplateTileBinding>                                                  
                <TilePeekTemplate>                                
                        <div class="peekTemplateClass">
                        <strong>Address:</strong>
                        <%#StringHelper.TrimText(DataBinder.Eval(Container.DataItem, "Address"), 19)%>
                        <br />
                        <strong>City:</strong>
                        <%#StringHelper.TrimText(DataBinder.Eval(Container.DataItem, "City"), 21)%>
                        <br />
                        <strong>Square Footage:</strong>
                        <%#DataBinder.Eval(Container.DataItem, "Sqft")%>
                        <br />
                        <strong>Year Built:</strong>
                        <%#DataBinder.Eval(Container.DataItem, "YearBuilt")%>
                            <br />
                        <strong>Operating Hours:</strong>
                        <%#DataBinder.Eval(Container.DataItem, "OperatingHours")%>
                            <br /><br />
                        <strong>Equipment:</strong>
                        <%#DataBinder.Eval(Container.DataItem, "EquipmentCount")%>
                        <br />
                        <strong>Points:</strong>
                        <%#DataBinder.Eval(Container.DataItem, "PointCount")%>
                        <br />
                        <strong>Building Vars:</strong>
                        <%#DataBinder.Eval(Container.DataItem, "BuildingVariablesCount")%>
                    </div>
                </TilePeekTemplate>
            </DataBindings>   
            <Groups>
            </Groups>            
        </telerik:RadTileList>          
                 
    </asp:Panel>

    <asp:Panel id="pnlProfile" runat="server">

        <!--Profile Left Column-->
        <div class="profileLeftColumn">                
            <!--Profile Top-->     
            <div>                                                                     
                <div class="divProfileImage">  
                    <img id="imgProfile" class="profileImage" runat="server" />
                </div>                                                
                <div class="profileTeaser">
                    <!--Building teaser -->                            
                    <asp:Literal ID="litTeaser" runat="server"></asp:Literal>
                </div>
                <!--<div class="divProfileInteractive">
                    <div><a href="#">Check in</a></div>
                    <br />
                    <div><a href="#">Like</a></div>
                </div>-->
            </div>
         
            <div class="block">
                <div class="blockHeader">
                    <span class="blockTitle">Details</span>
                    <asp:HyperLink ID="lnkDetails" runat="server" CssClass="toggle"><asp:Label ID="lblDetails" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>                    
                </div>
                <div class="blockBody">
                <ajaxToolkit:CollapsiblePanelExtender ID="detailsCollapsiblePanelExtender" runat="Server"
                            TargetControlID="pnlDetails"
                            CollapsedSize="0"
                            Collapsed="false" 
                            ExpandControlID="lnkDetails"
                            CollapseControlID="lnkDetails"
                            AutoCollapse="false"
                            AutoExpand="false"
                            ScrollContents="false"
                            ExpandDirection="Vertical"
                            TextLabelID="lblDetails"
                            CollapsedText="show details"
                            ExpandedText="hide details" 
                            />
                    <asp:Panel ID="pnlDetails" CssClass="blockCollapsiblePanel" runat="server">
                        <!--BUILDING DETAILS VIEW -->
                            <asp:DetailsView ID="dtvBuilding" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                <Fields>
                                    <asp:TemplateField  
                                    ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                    ItemStyle-BorderWidth="0" 
                                    ItemStyle-BorderColor="White" 
                                    ItemStyle-BorderStyle="none"
                                    ItemStyle-Width="100%"
                                    HeaderStyle-Width="1" 
                                    HeaderStyle-BorderWidth="0" 
                                    HeaderStyle-BorderColor="White" 
                                    HeaderStyle-BorderStyle="none"
                                    >
                                        <ItemTemplate>
                                            <div>                                                                                                                                        
                                                <ul class="detailsList">
                                                    <%# "<li><strong>Building Name: </strong>" + Eval("BuildingName") + "</li>"%>                             
                                                    <%# "<li><strong>Address: </strong>" + Eval("Address") + "</li>"%> 
                                                    <%# "<li><strong>City: </strong>" + Eval("City") + "</li>"%>
                                                    <%# "<li><strong>Country: </strong>" + Eval("CountryName") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("StateName"))) ? "" : "<li><strong>State: </strong>" + Eval("StateName") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Zip"))) ? "" : "<li><strong>Zip: </strong>" + Eval("Zip") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Latitude"))) ? "" : "<li><strong>Latitude: </strong>" + Eval("Latitude") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Longitude"))) ? "" : "<li><strong>Longitude: </strong>" + Eval("Longitude") + "</li>"%>
                                                    <%# "<li><strong>Time Zone: </strong>" + Eval("TimeZone") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Phone"))) ? "" : "<li><strong>Phone: </strong>" + Eval("Phone") + "</li>"%> 
                                                    <%# "<li><strong>Square Footage: </strong>" + Eval("Sqft") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Floors"))) ? "" : "<li><strong>Floors: </strong>" + Eval("Floors") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("YearBuilt"))) ? "" : "<li><strong>Year Built: </strong>" + Eval("YearBuilt") + "</li>"%> 
                                                    <%# "<li><strong>Building Class: </strong>" + Eval("BuildingClassName") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("BuildingClassDescription"))) ? "" : "<li><strong>Building Class Description: </strong>" + Eval("BuildingClassDescription") + "</li>"%>
                                                    <%# "<li><strong>Building Type: </strong>" + Eval("BuildingTypeName") + "</li>"%>                                                                                 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("BuildingTypeDescription"))) ? "" : "<li><strong>Building Type Description: </strong>" + Eval("BuildingTypeDescription") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("BuildingOwnership"))) ? "" : "<li><strong>Building Ownership: </strong>" + Eval("BuildingOwnership") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("BuildingShape"))) ? "" : "<li><strong>Building Shape: </strong>" + Eval("BuildingShape") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("MainCoolingSystem"))) ? "" : "<li><strong>Main Cooling System: </strong>" + Eval("MainCoolingSystem") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("MainHeatingSystem"))) ? "" : "<li><strong>Main Heating System: </strong>" + Eval("MainHeatingSystem") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("RoofConstruction"))) ? "" : "<li><strong>Roof Construction: </strong>" + Eval("RoofConstruction") + "</li>"%>
                                                    <%# "<li><strong>Envelope Glass Percentage: </strong>" + Eval("EnvelopeGlassPercentage") + "</li>"%>
                                                    <%# "<li><strong>Operating Hours/Week (Max 168): </strong>" + Eval("OperatingHours") + "</li>"%>                                                                
                                                    <%# "<li><strong>Computers: </strong>" + Eval("Computers") + "</li>"%>
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Occupants"))) ? "" : "<li><strong>Occupants: </strong>" + Eval("Occupants") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Location"))) ? "" : "<li><strong>Location: </strong><div class='divContentPreWrap'>" + Eval("Location") + "</div></li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("Description"))) ? "" : "<li><strong>Description: </strong><div class='divContentPreWrap'>" + Eval("Description") + "</div></li>"%>
                                                    <!-- no need to display building image since its already on the profile -->
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("PrimaryContactName"))) ? "" : "<li><strong>Primary Contact Name: </strong>" + Eval("PrimaryContactName") + "</li>"%> 
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("PrimaryContactEmail"))) ? "" : "<li><strong>Primary Contact Email: </strong><a href=\"mailto:" + Eval("PrimaryContactEmail") + "\">" + Eval("PrimaryContactEmail") + "</a></li>"%>   
                                                    <%# String.IsNullOrEmpty(Convert.ToString(Eval("PrimaryContactPhone"))) ? "" : "<li><strong>Primary Contact Phone: </strong>" + Eval("PrimaryContactPhone") + "</li>"%>                                                                                 
                                                    </ul>  
                                            </div>             
                                        </ItemTemplate>                                             
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>   
                                    
                    </asp:Panel>
                    </div>
            </div>
            <div class="block">
                <div class="blockHeader">
                    <span class="blockTitle">Additional Details</span>
                    <asp:HyperLink ID="lnkAdditionalDetails" runat="server" CssClass="toggle"><asp:Label ID="lblAdditionalDetails" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                </div>
                <div class="blockBody">
                <ajaxToolkit:CollapsiblePanelExtender ID="additionalDetailsCollapsiblePanelExtender" runat="Server"
                            TargetControlID="pnlAdditionalDetails"
                            CollapsedSize="0"
                            Collapsed="true" 
                            ExpandControlID="lnkAdditionalDetails"
                            CollapseControlID="lnkAdditionalDetails"
                            AutoCollapse="false"
                            AutoExpand="false"
                            ScrollContents="false"
                            ExpandDirection="Vertical"
                            TextLabelID="lblAdditionalDetails"
                            CollapsedText="show additional details"
                            ExpandedText="hide additional details" 
                            />
                    <asp:Panel ID="pnlAdditionalDetails" CssClass="blockCollapsiblePanel" runat="server">
                    <!--BUILDING ADDITONAL DETAILS VIEW -->
                            <asp:DetailsView ID="dtvBuildingAdditional" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                <Fields>
                                    <asp:TemplateField  
                                    ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                    ItemStyle-BorderWidth="0" 
                                    ItemStyle-BorderColor="White" 
                                    ItemStyle-BorderStyle="none"
                                    ItemStyle-Width="100%"
                                    HeaderStyle-Width="1" 
                                    HeaderStyle-BorderWidth="0" 
                                    HeaderStyle-BorderColor="White" 
                                    HeaderStyle-BorderStyle="none"
                                    >
                                        <ItemTemplate>
                                            <div>                                            
                                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("SASReferenceID"))) && String.IsNullOrEmpty(Convert.ToString(Eval("SASLink"))) ? "<span>No additional details provided.<span>" : "<ul class='detailsListSeperateLines'>" + (String.IsNullOrEmpty(Convert.ToString(Eval("SASReferenceID"))) ? "" : "<li><strong>SAS (Space Accounting Software) ReferenceID:</strong><div>" + Eval("SASReferenceID") + "</div></li>") + (String.IsNullOrEmpty(Convert.ToString(Eval("SASLink"))) ? "" : "<li><strong>SAS (Space Accounting Software) Link:</strong><div><a target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("SASLink").ToString()) + "'>" + Eval("SASLink") + "</a></div></li>") + "</ul>"%>
                                            </div>             
                                        </ItemTemplate>                                             
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>   
                                    
                    </asp:Panel>
                    </div>
            </div>          
        </div> 
                  
        <!--Profile Right Column-->   
        <div class="profileRightColumn">

                <div class="block">                        
                    <div class="blockHeader">
                        <span class="blockTitle">Profile Selection</span>
                        <asp:LinkButton ID="lnkBack" runat="server" OnClick="btnBack_Click">back to buildings</asp:LinkButton>
                    </div>                      
                    <div class="blockBody">
                        <div class="divFormThreeColumn">   
                            <label class="labelThreeColumn">*Building:</label> 
                            <extensions:DropDownExtension ID="ddlBuildings" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                            </extensions:DropDownExtension> 
                        </div>      
                        </div>                      
                </div>    

                <!--TODO: show blocks based on module access level-->
                <div id="divDiagnosticsBlock" class="block" runat="server">
                    <div class="blockHeader"><span class="blockTitle">Diagnostics</span><a id="lnkDiagnostics" target="_blank" href="Diagnostics.aspx" runat="server">go</a></div>
                    <div id='divDiagnosticsTop10' runat='server' class="blockBody">
                        <span>Yesterday's Top Results</span>
                        <ul>
                        <asp:ListView ID="lvDiagnostics" runat="server">
                            <ItemTemplate>
                                <li><a target="_blank" href="Diagnostics.aspx?cid=<%# Eval("CID") %>&bid=<%# Eval("BID") %>&ecid=<%# Eval("EquipmentClassID") %>&eid=<%# Eval("EID") %>&aid=<%# Eval("AID") %>&rng=<%# Eval("AnalysisRange") %>&sd=<%# Eval("StartDate") %>" class='blockLink'><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval("CostSavings"))) %></a> - <%# Eval("NotesSummary")%></li>
                            </ItemTemplate>
                        </asp:ListView>
                        </ul>
                    </div>
                </div>
                <div id="divDocumentsBlock" class="block" runat="server">
                    <div class="blockHeader"><span class="blockTitle">Documents</span><a id="lnkDocuments" target="_blank" href="Documents.aspx" runat="server">go</a></div>
                    <div id='divDocumentsTop10' runat='server' class="blockBody">
                        <span>Top Viewed</span>
                        <ul>
                        <asp:ListView ID="lvDocuments" runat="server">
                            <ItemTemplate>
                                <li><a href="Documents.aspx?bid=<%# Eval("BID") %>" target="_blank" class='blockLink'><%# Eval("DisplayName") %></a></li>
                            </ItemTemplate>
                        </asp:ListView>
                        </ul>
                    </div>
                </div> 
                <div id="divProjectsBlock" class="block" runat="server">
                    <div class="blockHeader"><span class="blockTitle">Projects</span><a id="lnkProjects" target="_blank" href="Projects.aspx" runat="server">go</a></div>
                    <div id='divProjectsTop10' runat='server' class="blockBody">
                        <span>Top Open</span>
                        <ul>
                        <asp:ListView ID="lvProjects" runat="server">
                            <ItemTemplate>
                                <li><a href="Projects.aspx?cid=<%# Eval("CID") %>&bid=0&sfd=<%# Eval("TargetStartDate") %>" target="_blank" class='blockLink'><%# CultureHelper.FormatCurrencyAsString(Convert.ToInt32(Eval("LCID")), Convert.ToString(Eval("ProjectedSavings"))) %></a> - <%# Eval("ProjectName") %></li>
                            </ItemTemplate>
                        </asp:ListView>
                        </ul>
                    </div>
                </div>                    
                <div id="divReportsBlock" class="block" runat="server">
                    <div class="blockHeader"><span class="blockTitle">Reports</span><a id="lnkReports" href="Reports.aspx" target="_blank" runat="server">go</a></div>
                    <div class="blockBody"></div>
                </div>
                <div id="divEternalLinksBlock" class="block">
                    <div class="blockHeader"><span class="blockTitle">External Links</span></div>
                    <div class="blockBody">
                        <!--<span>Some secondary header</span>
                            <span class='spanCentered'>Some secondary header centered</span>
                            <ul>
                                <li><label></label><a class='blockLink'>Test</a></li>
                                <li><label></label><a class='blockLink'>Test</a></li>
                                <li><label></label><a class='blockLink'>Test</a></li>
                                <li><label></label><a class='blockLink'>Test</a></li>
                            </ul>-->
                        <asp:DetailsView ID="dtvExternalLinks" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                <Fields>
                                    <asp:TemplateField  
                                    ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                    ItemStyle-BorderWidth="0" 
                                    ItemStyle-BorderColor="White" 
                                    ItemStyle-BorderStyle="none"
                                    ItemStyle-Width="100%"
                                    HeaderStyle-Width="1" 
                                    HeaderStyle-BorderWidth="0" 
                                    HeaderStyle-BorderColor="White" 
                                    HeaderStyle-BorderStyle="none"
                                    >
                                        <ItemTemplate>                                          
                                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("SASLink"))) ? "<span class='spanCentered'>No external links provided.</span>" : "<ul>" + (String.IsNullOrEmpty(Convert.ToString(Eval("SASLink"))) ? "" : "<li><label>SAS Link:</label><a class='blockLink' target='_blank' href='" + StringHelper.ExternalLinkCorrector(Eval("SASLink").ToString()) + "'>" + StringHelper.TrimText(Eval("SASLink").ToString(), 25) + "</a></li>") + "</ul>"%>
                                        </ItemTemplate>                                             
                                    </asp:TemplateField>
                                </Fields>
                            </asp:DetailsView>  
                    </div>
                </div>

        </div>

        <div style="display: block; clear: both;"></div>

        <!--Profile Bottom-->
        <div class="block">
            <div class="blockHeader">
                <span class="blockTitle">Equipment</span>
                <asp:HyperLink ID="lnkEquipment" runat="server" CssClass="toggle"><asp:Label ID="lblEquipment" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>                    
            </div>
            <div class="blockBody">
            <ajaxToolkit:CollapsiblePanelExtender ID="equipmentCollapsiblePanelExtender" runat="Server"
                        TargetControlID="pnlEquipment"
                        CollapsedSize="0"
                        Collapsed="false" 
                        ExpandControlID="lnkEquipment"
                        CollapseControlID="lnkEquipment"
                        AutoCollapse="false"
                        AutoExpand="false"
                        ScrollContents="false"
                        ExpandDirection="Vertical"
                        TextLabelID="lblEquipment"
                        CollapsedText="show details"
                        ExpandedText="hide details" 
                        />
                <asp:Panel ID="pnlEquipment" CssClass="blockCollapsiblePanel" runat="server">
                    <!--EQUIPMENT TILE LIST-->                                                                                        
                    <telerik:RadTileList runat="server" ID="EquipmentRadTileList" 
                                RenderMode="Mobile"
                                EnableDragAndDrop="false"
                                SelectionMode="None" AutoPostBack="true"
                                OnTileDataBound="RadTileList_OnTileDataBound"
                                OnTileClick="EquipmentRadTileList_OnTileClick"                 
                                >
                            <DataBindings>
                            <CommonTileBinding DataNameField="EID" TileType="RadContentTemplateTile" Shape="Square" DataGroupNameField="EquipmentClassName" />                                                       
                                <ContentTemplateTileBinding>                                
                                    <ContentTemplate>
                                        <div class="contentTemplateClass contentTemplateClass<%#DataBinder.Eval(Container.DataItem, "CostThresholdValue")%>">
                                            <div class="contentTileHeader"><%#StringHelper.TrimText(DataBinder.Eval(Container.DataItem, "EquipmentName"), 15)%></div>
                                            <div class="contentTileHeader2">Yesterday's Details</div>                           
                                            <strong>Cost:</strong>
                                            <%#DataBinder.Eval(Container.DataItem, "YesterdayTotalCost")%>
                                            <br />
                                            <strong>Energy Score:</strong>
                                            <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "YesterdayEnergyScore"))%>
                                            <br />
                                            <strong>Maintenance Score:</strong>
                                            <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "YesterdayMaintenanceScore"))%>
                                            <br />
                                            <strong>Comfort Score:</strong>
                                            <%# Convert.ToDecimal(DataBinder.Eval(Container.DataItem, "YesterdayComfortScore"))%>
                                        </div>
                                    </ContentTemplate>
                                </ContentTemplateTileBinding>                                                  
                                <TilePeekTemplate>                                
                                        <div class="peekTemplateClass">
                                        <strong>Location:</strong>
                                        <%#DataBinder.Eval(Container.DataItem, "EquipmentLocation")%>
                                        <br /><br />
                                        <strong>Associated Equipment:</strong>
                                        <%#DataBinder.Eval(Container.DataItem, "AssociatedEquipmentCount")%>
                                        <br />
                                        <strong>Points:</strong>
                                        <%#DataBinder.Eval(Container.DataItem, "PointCount")%>
                                        <br />
                                        <strong>Equipment Vars:</strong>
                                        <%#DataBinder.Eval(Container.DataItem, "EquipmentVariablesCount")%>
                                    </div>
                                </TilePeekTemplate>
                            </DataBindings>   
                            <Groups>
                            </Groups>            
                        </telerik:RadTileList>          
                </asp:Panel>
            </div>
        </div> 	 
            
    </asp:Panel>
                 	        	
</asp:Content>                