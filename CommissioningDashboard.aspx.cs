﻿using CW.Data;
using CW.Utility;
using CW.Website._framework;
using iCL;
using iCLib;
using System;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class CommissioningDashboard : SitePage
    {
        #region fields

            private SiteUser mSiteUser;
            private int uid;
            private int cid;
            private int roleID;
            private bool isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser;

            const string noClientImage = "no-client-image.png";
            
        #endregion

        #region Page Events

            private void Page_FirstInit()
            {
                ScriptIncluder.GetInstance(this) // wont be necessary to specifiy each reauired iCL resource in future
                    .Add<core.coreClass>()
                    .Add<core.coreEnum>()
                    .Add<core.ArrayPrototype>()
                    .Add<core.String>()
                    .Add<core.asp.SessionKeepAlive>()
                    .Add<core.Document>()
                    .Add<core.Event>()
                    .Add<core.TypedArray>()
                    .Add<core.asp.webForm.AsyncPostbackManager>()
                    .Add<core.HTMLAnchorPrototype>()
                    .Add<core.HTMLCollectionPrototype>()
                    .Add<core.HTMLSelectElement>()
                    .Add<core.Function>() //will be removed in future
                    .Add<core.DateRange>()
                    .Add<core.ui.controls.ContextMenu>()
                    .Add<core.Url>();
            }

            protected override void OnLoad(EventArgs e)
            {
                mSiteUser = siteUser;

                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litDashboardBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.CommissioningDashboardBody);

                    //set and bind appropriate fields/ddls
                    SetClientInfoAndImages();
                    BindDropdownLists();

                    //dynamically create all common events and settings for each radDockZone and radDock
                    foreach (RadDockZone radDockZone in this.FindControls<RadDockZone>())
                    {
                        radDockZone.BorderStyle = BorderStyle.None;
                        radDockZone.FitDocks = false;
                        radDockZone.Orientation = Orientation.Vertical;
                    }

                    foreach (RadDock radDock in this.FindControls<RadDock>())
                    {
                        radDock.DefaultCommands = Telerik.Web.UI.Dock.DefaultCommands.ExpandCollapse;
                        radDock.EnableAnimation = true;
                        radDock.EnableRoundedCorners = true;
                        radDock.DockMode = DockMode.Docked;
                        radDock.Resizable = false;

                        if (radDock.Title.Contains("Building"))
                        {
                            radDock.CssClass = "radDockMedium";
                            radDock.EnableDrag = false;
                        }
                        else
                            radDock.CssClass = "radDockLarge";                            
                    }

                   
                    if (!String.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                    {
                        ddlBuildings.SelectedValue = Request.QueryString["bid"];
                        ddlBuildings_SelectedIndexChanged(null, null);

                        if (!String.IsNullOrWhiteSpace(Request.QueryString["int"]) && !String.IsNullOrWhiteSpace(Request.QueryString["rng"]))
                        {
                            ddlInterval.SelectedValue = Request.QueryString["int"];
                            ddlInterval_SelectedIndexChanged(null, null);
                            ddlRange.SelectedValue = Request.QueryString["rng"];
                        }
                    }
                }

                //set global info
                uid = mSiteUser.UID;
                roleID = mSiteUser.RoleID;
                cid = mSiteUser.CID;
                isKGSFullAdminOrHigher = mSiteUser.IsKGSFullAdminOrHigher;
                isSuperAdminOrFullAdminOrFullUser = mSiteUser.IsSuperAdminOrFullAdminOrFullUser;
            }

        #endregion

        #region Bind Methods

            private void BindDropdownLists()
            {
                BindBuildingDropdownList();
                BindEquipmentClasses();
                BindEquipment();
                BindRange();
            }

            private void BindBuildingDropdownList()
            {
                ddlBuildings.Items.Add
                    (
                        new ListItem
                        {
                            Text = "View All",
                            Value = "-1",
                        }
                    );

                ddlBuildings.DataTextField = "BuildingName";
                ddlBuildings.DataValueField = "BID";
                ddlBuildings.DataSource = mSiteUser.VisibleBuildings.OrderBy(b => b.BuildingName);

                ddlBuildings.DataBind();
            }

            private void BindEquipmentClasses(int bid = -1)
            {
                //clear the values from the postback to readd the filtered items if a building was selected, also reset interval and range
                if (Page.IsPostBack)
                {
                    ddlEquipmentClass.Items.Clear();
                    ResetIntervalAndRange();
                }

                ddlEquipmentClass.Items.Add
                   (
                       new ListItem
                       {
                           Text = "View All",
                           Value = "-1",
                       }
                   );

                if (bid != -1)
                {
                    ddlEquipmentClass.DataTextField = "EquipmentClassName";
                    ddlEquipmentClass.DataValueField = "EquipmentClassID";
                    ddlEquipmentClass.DataSource = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid);
                }

                ddlEquipmentClass.DataBind();
            }

            private void BindEquipment(int bid = -1, int equipmentClassID = -1)
            {
                //clear the values from the postback to readd the filtered items if an equipment class was selected, also reset interval and range
                if (Page.IsPostBack)
                {
                    ddlEquipment.Items.Clear();
                    ResetIntervalAndRange();
                }

                ddlEquipment.Items.Add
                   (
                       new ListItem
                       {
                           Text = "View All",
                           Value = "-1",
                       }
                   );

                if (bid != -1 && equipmentClassID != -1)
                {
                    ddlEquipment.DataTextField = "EquipmentName";
                    ddlEquipment.DataValueField = "EID";
                    ddlEquipment.DataSource = DataMgr.EquipmentDataMapper.GetAllActiveEquipmentByBuildingIDAndEquipmentClassID(bid, equipmentClassID);
                }

                ddlEquipment.DataBind();
            }

            private void BindRange()
            {
                //clear the values from the postback to readd the items based on the value selected in the ddlInterval control
                if (Page.IsPostBack)
                    ddlRange.Items.Clear();

                switch (ddlInterval.SelectedValue)
                {
                    case "Daily":
                        ddlRange.Items.Add(new ListItem { Text = "Past 30 Days", Value = "Past30Days" });
                        ddlRange.Items.Add(new ListItem { Text = "Past 60 Days", Value = "Past60Days" });
                        ddlRange.Items.Add(new ListItem { Text = "Past 90 Days", Value = "Past90Days" });
                        break;
                    case "Weekly":
                        ddlRange.Items.Add(new ListItem { Text = "Past 10 Weeks", Value = "Past10Weeks" });
                        ddlRange.Items.Add(new ListItem { Text = "Past 25 Weeks", Value = "Past25Weeks" });
                        ddlRange.Items.Add(new ListItem { Text = "Past 52 Weeks", Value = "Past52Weeks" });
                        break;
                    case "Monthly":
                        ddlRange.Items.Add(new ListItem { Text = "Past 12 Months", Value = "Past12Months" });
                        ddlRange.Items.Add(new ListItem { Text = "Past 24 Months", Value = "Past24Months" });
                        ddlRange.Items.Add(new ListItem { Text = "Past 36 Months", Value = "Past36Months" });
                        break;
                }
            }

        #endregion

        #region DropDownList Events

            protected void ddlBuildings_SelectedIndexChanged(object sender, EventArgs e)
            {
                BindEquipmentClasses(Convert.ToInt32(ddlBuildings.SelectedValue));
                BindEquipment(Convert.ToInt32(ddlBuildings.SelectedValue), Convert.ToInt32(ddlEquipmentClass.SelectedValue));
            }

            protected void ddlEquipmentClass_SelectedIndexChanged(object sender, EventArgs e)
            {
                BindEquipment(Convert.ToInt32(ddlBuildings.SelectedValue), Convert.ToInt32(ddlEquipmentClass.SelectedValue));
            }

            protected void ddlInterval_SelectedIndexChanged(object sender, EventArgs e)
            {
                BindRange();
            }

        #endregion

        #region Button Events

            protected void btnDownloadButton_Click(object sender, EventArgs e)
            {
                var invisibleElementIds = "refreshData;refreshBtn;hdn_container;" + imgPdfDownload.ClientID + ";";
                EssentialObjectsHelper eo = new EssentialObjectsHelper(invisibleElementIds, "CustomChartImage.axd", LogMgr);

                eo.CreatePdfHeader(siteUser.IsSchneiderTheme);
                eo.CreatePdfFooter(siteUser.IsSchneiderTheme, "Commissioning Dashboard Report");

                Byte[] pdfBytes = eo.ConvertHtmlAndReturnBytes(Server.HtmlDecode(hdn_container.Value), "\\tempCommissioningDashboard\\", "chart", ".png", DataMgr.HourlyCacheRepository);

                Boolean result;

                try
                {
                    result = new FilePublishService
                    (
                        DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                        DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                        siteUser.IsKGSFullAdminOrHigher,
                        siteUser.IsSchneiderTheme,
                        siteUser.Email,
                        new ModulePDFGenerator(pdfBytes),
                        LogMgr
                    ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productCommissioningDashboard.pdf", true));

                }
                catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
                {
                    DisplayErrorOutput("Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                    return;
                }

                eo.RemoveTempFiles();

                if (result) return;
            }

        #endregion

        #region Helper Methods

            private void SetClientInfoAndImages()
            {
                var client = DataMgr.ClientDataMapper.GetClient(mSiteUser.CID);

                if (client == null) return;

                imgHeaderClientImage.AlternateText = mSiteUser.ClientName;
                imgHeaderClientImage.Visible = true;
                imgHeaderClientImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;

                if (!String.IsNullOrEmpty(client.ImageExtension))
                    imgHeaderClientImage.ImageUrl = HandlerHelper.ClientImageUrl(client.CID, client.ImageExtension);

                lblHeaderClientName.InnerText = mSiteUser.ClientName;
            }

            private void ResetIntervalAndRange()
            {
                ddlInterval.SelectedIndex = 0;
                BindRange();
            }

        #endregion
    }
}