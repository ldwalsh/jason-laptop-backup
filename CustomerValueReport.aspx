﻿<%@ Page Language="C#" EnableViewState="true" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="CustomerValueReport.aspx.cs" Inherits="CW.Website.CustomerValueReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title id="title" runat="server">$product</title>   
    <style type="text/css">
		    @import url("/_assets/styles/themes/cvr.css");
	</style>
</head>
<body id="body" runat="server">
    <form class="" id="form1" runat="server">
        
        <telerik:RadScriptManager ID="ScriptManager" EnableScriptCombine="true" EnableCdn="true" EnableScriptGlobalization="true" AsyncPostBackTimeout="600" runat="server" />
           
        <asp:UpdatePanel ID="updatePanel" ValidateRequestMode="Disabled" runat="server" UpdateMode="Conditional">
        <ContentTemplate>     

        <input type="hidden" runat="server" id="hdn_container" />
        <asp:HiddenField ID="hdnIsSETheme" runat="server" />
        <asp:HiddenField ID="hdnCID" runat="server" />
        <asp:HiddenField ID="hdnLanguageCultureName" runat="server" />
        <asp:HiddenField ID="hdnShowClientLogos" runat="server" />
        <asp:HiddenField ID="hdnIncludeTopDiagnosticsFigures" runat="server" />
        <asp:HiddenField ID="hdnIsScheduled" runat="server" />

        <asp:Panel ID="pnlError" runat="server" Visible="false">
            <asp:Label ID="lblError" CssClass="errorMessage"  runat="server"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="pnlReport" runat="server">                 

        <div id="divCoverPage" runat="server">
        
            <div class="coverTop">
                <span id="spanCoverHeader" class="coverHeader" runat="server"></span>                                  
            </div>
                       
            <div class="section sectionCoverLogos">
                <asp:HiddenField ID="hdnCoverClientByteArray" runat="server" />
                <telerik:RadBinaryImage ID="radCoverClient" runat="server" AlternateText="logo" />                
            </div>

            <div class="coverMiddle">
                <div id="divCoverPreparedFor" class="coverPreparedFor" runat="server">:</div>    
                <div id="divCoverCustomerName" class="coverCustomerName" runat="server"></div>  
                <div id="divCoverDates" class="coverDates" runat="server"></div>
            </div>

            <div class="bottomPage">
                <img id="imgBottomPage" alt="logo" src="" runat="server" />
            </div>

            <div style="page-break-before: always"></div>
        </div>
        
        <div id="divContentsPage" runat="server">
        
            <div class="newPage">
                <div class="newPageCustomerAndTitle">
                    <span id="newPageCustomer1" runat="server"></span> -
                    <span id="newPageHeader1" runat="server"></span>
                </div>
                <div  id="newPageDates1" class="newPageDates" runat="server"></div>
                <img id="img1" src="" alt="logo" runat="server" />
            </div>
            <hr />
            
            <div class="section section1" runat="server">
                <div id="divContentsCustomerPage" runat="server">
                    <label class="contentsLabel" id="lblCustomerPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsKeyIndicatorsPage" runat="server">
                    <label class="contentsLabel" id="lblKeyIndicatorsPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsItemsRequiringAttentionPage" runat="server">
                    <label class="contentsLabel" id="lblItemsRequiringAttentionPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsPerformancePage" runat="server">
                    <label class="contentsLabel" id="lblPerformancePage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsServiceDetailsPage" runat="server">
                    <label class="contentsLabel" id="lblServiceDetailsPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsCustomerSatisfactionPage" runat="server">
                    <label class="contentsLabel" id="lblCustomerSatisfactionPage" runat="server"></label>
                    <br /><br />
                </div>
                <div id="divContentsSitePage" runat="server">
                    <label class="contentsLabel" id="lblSitePage" runat="server"></label>
                </div>            
            </div>

            <div style="page-break-before: always"></div>

        </div>

        <div id="divCustomerPage" runat="server">
            <div class="newPage">
                <div class="newPageCustomerAndTitle">
                    <span id="newPageCustomer2" runat="server"></span> -
                    <span id="newPageHeader2" runat="server"></span>
                </div>
                <div  id="newPageDates2" class="newPageDates" runat="server"></div>
                <img id="img2" src="" alt="logo" runat="server" />
            </div>
            <hr />

            <div class="section section2">

                <!--<div class="contentBlack" runat="server"></div>
                <div class="contentPaddedLightGreen" runat="server"></div>
                <div class="contentPaddedBlue" runat="server"></div>
                <div class="contentGray" runat="server"></div>-->

                <h2 id="customerDetailsHeader" runat="server"></h2>   
                <table class="tableContainer">
                    <tr>
                        <td class="tableTwoColumn">
                            <div class="divForm">
                                <label id="lblCutomerName" class="labelBoldColored" runat="server">:</label>
                                <div id="divCustomerName" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblCustomerLocation" class="labelBoldColored" runat="server">:</label>
                                <div id="divCustomerLocation" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblCustomerPhone" class="labelBoldColored" runat="server">:</label>
                                <div id="divCustomerPhone" class="contentBlack" runat="server"></div>
                            </div>                          
                        </td>
                        <td class="tableTwoColumn">
                            <div class="divForm">
                                <label id="lblContactName" class="labelBoldColored" runat="server">:</label>
                                <div id="divContactName" class="contentPaddedBlue" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblContactEmail" class="labelBoldColored" runat="server">:</label>
                                <div id="divContactEmail" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblContactPhone" class="labelBoldColored" runat="server">:</label>
                                <div id="divContactPhone" class="contentBlack" runat="server"></div>
                            </div>
                        </td>
                    </tr>
                </table>
                <hr class="hrDotted" />

                <h2 id="contractDetailsHeader" runat="server"></h2>  
                <table class="tableContainer">
                    <tr>
                        <td class="tableTwoColumn">
                            <div class="divForm">
                                <label id="lblServicePlan" class="labelBoldColored" runat="server">:</label>
                                <div id="divServicePlan" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblContractStartDate" class="labelBoldColored" runat="server">:</label>
                                <div id="divContractStartDate" class="contentGray" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblContractEndDate" class="labelBoldColored" runat="server">:</label>
                                <div id="divContractEndDate" class="contentGray" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblAdditionalServices" class="labelBoldColored" runat="server">:</label>
                                <div id="divAdditionalServices" class="contentBlack" runat="server"></div>
                            </div>
                        </td>
                        <td class="tableTwoColumn">
                            <div class="divForm">
                                <label id="lblBuildingCount" class="labelBoldColored" runat="server">:</label>
                                <div id="divBuildingCount" class="contentGray" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblBuildingList" class="labelBoldColored" runat="server">:</label>
                                <div id="divBuildingList" class="contentBlack" runat="server"></div>
                            </div>
                        </td>
                    </tr>
                </table>
                <hr class="hrDotted" />

                <h2 id="supportTeamHeader" runat="server"></h2>  
                <table class="tableContainer">
                    <tr>
                        <td class="tableTwoColumn">
                            <div class="divForm">
                                <label id="lblPrimaryServiceTechnicianName" class="labelBoldColored" runat="server">:</label>
                                <div id="divPrimaryServiceTechnicianName" class="contentBlue" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblPrimaryServiceTechnicianEmail" class="labelBoldColored" runat="server">:</label>
                                <div id="divPrimaryServiceTechnicianEmail" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblPrimaryServiceTechnicianPhone" class="labelBoldColored" runat="server">:</label>
                                <div id="divPrimaryServiceTechnicianPhone" class="contentBlack" runat="server"></div>
                            </div>

                            <div class="divForm">
                                <label id="lblSecondaryServiceTechnicianName" class="labelBoldColored" runat="server">:</label>
                                <div id="divSecondaryServiceTechnicianName" class="contentBlue" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblSecondaryServiceTechnicianEmail" class="labelBoldColored" runat="server">:</label>
                                <div id="divSecondaryServiceTechnicianEmail" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblSecondaryServiceTechnicianPhone" class="labelBoldColored" runat="server">:</label>
                                <div id="divSecondaryServiceTechnicianPhone" class="contentBlack" runat="server"></div>
                            </div>

                            <div class="divForm">
                                <label id="lblSalesName" class="labelBoldColored" runat="server">:</label>
                                <div id="divSalesName" class="contentBlue" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblSalesEmail" class="labelBoldColored" runat="server">:</label>
                                <div id="divSalesEmail" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblSalesPhone" class="labelBoldColored" runat="server">:</label>
                                <div id="divSalesPhone" class="contentBlack" runat="server"></div>
                            </div>
                        </td>
                        <td class="tableTwoColumn">
                            <div class="divForm">
                                <label id="lblRemoteCenterEngineerName" class="labelBoldColored" runat="server">:</label>
                                <div id="divRemoteCenterEngineerName" class="contentBlue" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblRemoteCenterEngineerEmail" class="labelBoldColored" runat="server">:</label>
                                <div id="divRemoteCenterEngineerEmail" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblRemoteCenterEngineerPhone" class="labelBoldColored" runat="server">:</label>
                                <div id="divRemoteCenterEngineerPhone" class="contentBlack" runat="server"></div>
                            </div>

                            <div class="divForm">
                                <label id="lblAdminName" class="labelBoldColored" runat="server">:</label>
                                <div id="divAdminName" class="contentBlue" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblAdminEmail" class="labelBoldColored" runat="server">:</label>
                                <div id="divAdminEmail" class="contentBlack" runat="server"></div>
                            </div>
                            <div class="divForm">
                                <label id="lblAdminPhone" class="labelBoldColored" runat="server">:</label>
                                <div id="divAdminPhone" class="contentBlack" runat="server"></div>
                            </div>
                        </td>
                    </tr>                                                                                                                               
                </table>
                <hr class="hrDotted" />               

                <div id="serviceContent" class="serviceContent" runat="server"></div>

            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divKeyIndicatorsPage" runat="server">
            <div class="newPage">
                <div class="newPageCustomerAndTitle">
                    <span id="newPageCustomer3" runat="server"></span> -
                    <span id="newPageHeader3" runat="server"></span>
                </div>
                <div  id="newPageDates3" class="newPageDates" runat="server"></div>
                <img id="img3" src="" alt="logo" runat="server" />
            </div>
            <hr />

            <div class="section section3">
                <h2 id="keyIndicatorsHeader" runat="server"></h2>                            

                <div id="divRating" class="container" runat="server">
                    <div class="containerInner">
                        <div class="divForm">
                            <label id="lblRatingSystem" class="labelBoldColored" runat="server">:</label><div id="divRatingValue" class="contentBlack" runat="server"></div>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="containerInner">
                        <span id="keyIndicatorsNote" class="keyIndicatorsNote" runat="server">*</span>
                        <div class="coloredContainer orangeContainer">
                            <div id="divAlarmsLabel" class="keyIndicatorLabel" runat="server">:</div>
                            <img id="imgAlarmsArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                        </div>
                        <div id="divEnergyContainer" class="coloredContainer yellowContainer" runat="server">
                            <div id="divEnergyLabel" class="keyIndicatorLabel" runat="server">:</div>
                            <img id="imgEnergyArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                        </div>
                        <div class="coloredContainer blueContainer">
                            <div id="divMaintenanceLabel" class="keyIndicatorLabel" runat="server">:</div>
                            <img id="imgMaintenanceArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                        </div>
                        <div class="coloredContainer redContainer">
                            <div id="divComfortLabel" class="keyIndicatorLabel" runat="server">:</div>
                            <img id="imgComfortArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                        </div>
                        <br />

                         <div class="coloredContainerWide greenContainer">                                                                                                          
                            <div id="divCostLabel" class="keyIndicatorLabel" runat="server">:</div>  
                            <br />                                                      
                            <img id="imgCostArrow" class="arrowIcon" src="" alt="arrow" runat="server" />
                            <span id="spanCostTrendSummary" class="costSummary" runat="server"></span>    
                        </div>

                        <br /><br />
                        <div id="keyIndicatorsNote2" runat="server">*</div>
                    </div>
                </div>

                <div class="container">
                    <div class="containerInner"">
                        <ul>
                            <li id="liNumAlarmsNotified" class="listItemBullet" runat="server"><label id="lblNumAlarmsNotified" runat="server">:</label><span id="spanNumAlarmsNotified" runat="server"></span></li>
                            <li id="liNumAlarmsFixed" class="listItemBullet" runat="server"><label id="lblNumAlarmsFixed" runat="server">:</label><span id="spanNumAlarmsFixed" runat="server"></span></li>
                            <li id="liNumAlarmsAction" class="listItemBullet" runat="server"><label id="lblNumAlarmsAction" runat="server">:</label><span id="spanNumAlarmsAction" runat="server"></span></li>
                            <li id="liNumIssuesFixed" class="listItemBullet" runat="server"><label id="lblNumIssuesFixed" runat="server">:</label><span id="spanNumIssuesFixed" runat="server"></span></li>
                            <li id="liResponseTime" class="listItemBullet" runat="server"><label id="lblResponseTime" runat="server">:</label><span id="spanResponseTime" runat="server"></span></li>
                            <li id="liOnsiteRepairTime" class="listItemBullet" runat="server"><label id="lblOnsiteRepairTime" runat="server">:</label><span id="spanOnsiteRepairTime" runat="server"></span></li>
                            <li id="liMeanRepairTime" class="listItemBullet" runat="server"><label id="lblMeanRepairTime" runat="server">:</label><span id="spanMeanRepairTime" runat="server"></span></li>                            
                        </ul>
                    </div>
                </div>
            </div>

            <div style="page-break-before: always"></div>            
        </div>

        <div id="divItemsRequiringAttentionPage" runat="server">
            <div class="newPage">
                <div class="newPageCustomerAndTitle">
                    <span id="newPageCustomer4" runat="server"></span> -
                    <span id="newPageHeader4" runat="server"></span>
                </div>
                <div id="newPageDates4" class="newPageDates" runat="server"></div>
                <img id="img4" src="" alt="logo" runat="server" />
            </div>
            <hr />

            <div class="section section4">
                <h2 id="itemsRequiringAttentionHeader" runat="server"></h2>                                     
                
                <div id="gridTbl" class="gridFieldQuotes">
                    <div class="tblPreHeader">                    
                        <span id="spanFieldQuotesTitle" runat="server"></span>
                        <asp:ImageButton ID="btnFieldQuotes" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="fieldQuotesGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridFieldQuotes"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                               
                        >
                        <Columns>                            
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter textColumnWidth">
                                <ItemTemplate>                                    
                                    <asp:Label runat="server" Text='<%# Eval("Field Quote Number") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("Field Quote Number")%>' MaxLength="25"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Quote Date")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("Quote Date")%>' MaxLength="10"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="contentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("Focus Areas"), 1500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="440px" Visible="false" Text='<%# StringHelper.TrimText(Eval("Focus Areas"), 1500)%>' MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                   
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Amount") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>                                                        
                                                 
                        </Columns>     
                    </asp:GridView>
                </div> 
                
                                       
            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divPerformancePage" runat="server">
            <div class="newPage">
                <div class="newPageCustomerAndTitle" runat="server">
                    <span id="newPageCustomer5" runat="server"></span> -
                    <span id="newPageHeader5" runat="server"></span>
                </div>
                <div id="newPageDates5" class="newPageDates" runat="server"></div>
                <img id="img5" src="" alt="logo" runat="server" />
            </div>
            <hr />

            <div class="section section5">
                <h2 id="performanceHeader" runat="server"></h2>      
                
                <div style="page-break-inside: avoid">         
                <div class="containerChart">
                    <div class="containerChartInner">
                    <h3 id="totalEnergyTrendsHeader" runat="server"></h3>
                    <asp:Chart ID="energyChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartEnergy_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="820px" Height="280px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Name="energyChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>
                                <AxisY2 LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY2>
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>                        
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                            <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                        </Legends>
                    </asp:Chart>  
                    </div>               
                </div>

                <div class="divEnergyWorkOrders">
                    <asp:ListView ID="lvEnergyWorkOrders" runat="server">                        
                        <ItemTemplate>
                            <div>
                                <label><%# Convert.ToDateTime(Eval("Date")).ToShortDateString() %></label>: <span><%# Eval("FormatedText") %></span>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
                </div>

                <br />

                <div style="page-break-inside: avoid">         
                <div class="containerChart">
                    <div class="containerChartInner">
                    <h3 id="totalMaintenanceTrendsHeader" runat="server"></h3>
                    <asp:Chart ID="maintenanceChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartMaintenance_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="820px" Height="280px">                            
                        <Series></Series>                
                        <ChartAreas>
                            <asp:ChartArea Name="maintenanceChartArea" BackColor="White">                                         
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>
                                <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY2>
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid LineColor="#CCCCCC" Enabled="true" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>                        
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                            <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                        </Legends>
                    </asp:Chart>  
                    </div>               
                </div>

                <div class="divMaintenanceWorkOrders">
                    <asp:ListView ID="lvMaintenanceWorkOrders" runat="server">                        
                        <ItemTemplate>
                            <div>
                                <label><%# Convert.ToDateTime(Eval("Date")).ToShortDateString() %></label>: <span><%# Eval("FormatedText") %></span>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
                </div>

                <br />

                <div style="page-break-inside: avoid">
                <div class="containerChart">
                    <div class="containerChartInner">
                    <h3 id="totalComfortTrendsHeader" runat="server"></h3>
                    <asp:Chart ID="comfortChart" CssClass="trendChart" ImageStorageMode="UseHttpHandler" OnPrePaint="ChartComfort_PrePaint" runat="server" ImageType="Png" EnableViewState="true" Width="820px" Height="280px">                             
                        <Series></Series>
                        <ChartAreas>
                            <asp:ChartArea Name="comfortChartArea" BackColor="White">                                                                 
                                <AxisY LineColor="#666666" LineWidth="1" Title="" TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY>
                                <AxisY2 Interval="1" Maximum="10" LineColor="#666666" LineWidth="1" Title=""  TitleForeColor="#666666" TitleFont="Arial, 10pt, style=Bold" IsLabelAutoFit="false" IsStartedFromZero="true">
                                    <LabelStyle ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid Enabled="false" /> 
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="OutsideArea" Size="1" />
                                </AxisY2>
                                <AxisX IsMarginVisible="false" LineColor="#666666" IsLabelAutoFit="false">
                                    <LabelStyle Angle="-45" ForeColor="#666666" Font="Arial, 8pt, style=Bold" />
                                    <MajorGrid LineColor="#CCCCCC" Enabled="true" />
                                    <MajorTickMark LineColor="#666666" TickMarkStyle="AcrossAxis" Size="3" />
                                </AxisX>
                            </asp:ChartArea>
                        </ChartAreas>
                        <Legends>
                            <asp:Legend Font="Arial, 7.5pt, style=Bold" ForeColor="#666666" Name="Default" Docking="Bottom" BorderColor="#666666"></asp:Legend>
                            <asp:Legend Name="Hidden" Enabled="false"></asp:Legend>
                        </Legends>
                    </asp:Chart>
                    </div>
                </div>
                
                <div class="divComfortWorkOrders">
                    <asp:ListView ID="lvComfortWorkOrders" runat="server">                        
                        <ItemTemplate>
                            <div>
                                <label><%# Convert.ToDateTime(Eval("Date")).ToShortDateString() %></label>: <span><%# Eval("FormatedText") %></span>
                            </div>
                        </ItemTemplate>
                    </asp:ListView>
                </div>  
                </div>  
                                                                                               
            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divServiceDetailsPage" runat="server">
            <div class="newPage">
                <div class="newPageCustomerAndTitle" runat="server">
                    <span id="newPageCustomer6" runat="server"></span> -
                    <span id="newPageHeader6" runat="server"></span>
                </div>
                <div id="newPageDates6" class="newPageDates" runat="server"></div>
                <img id="img6" src="" alt="logo" runat="server" />
            </div>
            <hr />

            <div class="section section6">
                <h2 id="serviceDetailsHeader" runat="server"></h2>                    
                    
                <div id="gridTbl" class="gridWorkOrders">
                    <div class="tblPreHeader">                    
                        <span id="spanWorkOrdersTitle" runat="server"></span>
                        <asp:ImageButton ID="btnWorkOrders" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="workOrdersGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridWorkOrders"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                               
                        >
                        <Columns>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Date")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("Date")%>' MaxLength="10"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                           
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter textColumnWidth">
                                <ItemTemplate>                                    
                                    <asp:Label runat="server" Text='<%# Eval("Work Order #") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("Work Order #")%>' MaxLength="25"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
			                <asp:TemplateField HeaderText="" ItemStyle-CssClass="textColumnWidth">
                                <ItemTemplate>                                    
                                    <asp:Label runat="server" Text='<%# Eval("Engineer Name") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("Engineer Name")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="contentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("Problem Description"), 500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="200px" Visible="false" Text='<%# StringHelper.TrimText(Eval("Problem Description"), 500)%>' MaxLength="500"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="contentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("Action Taken"), 1500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="400px" Visible="false" Text='<%# StringHelper.TrimText(Eval("Action Taken"), 1500)%>' MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>      
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="contentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("Impact Achieved"), 1500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="400px" Visible="false" Text='<%# StringHelper.TrimText(Eval("Impact Achieved"), 1500)%>' MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                                                                                                    
                                                 
                        </Columns>     
                    </asp:GridView>
                </div> 
                                        
            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divCustomerSatisfactionPage" runat="server">
            <div class="newPage">
                <div class="newPageCustomerAndTitle" runat="server">
                    <span id="newPageCustomer7" runat="server"></span> -
                    <span id="newPageHeader7" runat="server"></span>
                </div>
                <div id="newPageDates7" class="newPageDates" runat="server"></div>
                <img id="img7" src="" alt="logo" runat="server" />
            </div>
            <hr />

            <div class="section section7">
                <h2 id="customerSatisfactionHeader" runat="server"></h2>    
                
                    
                <div id="gridTbl" class="gridSurveys">
                    <div class="tblPreHeader">                    
                        <span id="spanSurveysTitle" runat="server"></span>
                        <asp:ImageButton ID="btnSurveys" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="surveysGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridSurveys"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                              
                        >
                        <Columns>                          
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="textColumnWidth">
                                <ItemTemplate>                                    
                                    <asp:Label runat="server" Text='<%# Eval("Technician") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("Technician")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
			                <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Jan") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Jan") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Feb") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Feb") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Mar") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Mar") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Apr") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Apr") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("May") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("May") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Jun") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Jun") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Jul") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Jul") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Aug") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Aug") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Sep") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Sep") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Oct") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Oct") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Nov") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Nov") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Dec") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Dec") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Total") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>                                                                                                                                       
                                                 
                        </Columns>     
                    </asp:GridView>
                </div> 
                
                <br />

                <div id="gridTbl" class="gridFeedback">
                    <div class="tblPreHeader">                    
                        <span id="spanFeedbackTitle" runat="server"></span>
                        <asp:ImageButton ID="btnFeedback" CssClass="editCircle" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="feedbackGridEditButton_Click" runat="server" />
                    </div>
                    <asp:GridView 
                        ID="gridFeedback"   
                        EnableViewState="true"           
                        runat="server"                                                                                                           
                        AutoGenerateColumns="false"                                              
                        HeaderStyle-CssClass="tblTitle" 
                        RowStyle-CssClass="tblCol1"
                        AlternatingRowStyle-CssClass="tblCol2"        
                        ShowHeaderWhenEmpty="true"
                        EmptyDataRowStyle-CssClass="tblEmptyRow"                                                                                                                                                
                        >
                        <Columns>                                                   
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter textColumnWidth">
                                <ItemTemplate>                                    
                                    <asp:Label runat="server" Text='<%# Eval("Work Order #") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("Work Order #")%>' MaxLength="25"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField> 
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Date")%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="80px" Visible="false" Text='<%# Eval("Date")%>' MaxLength="10"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>      
			                <asp:TemplateField HeaderText="" ItemStyle-CssClass="textColumnWidth">
                                <ItemTemplate>                                    
                                    <asp:Label runat="server" Text='<%# Eval("Tech") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="100px" Visible="false" Text='<%# Eval("Tech")%>' MaxLength="100"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Tech Score") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Tech Score") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="gridAlignCenter numberColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# Eval("Overall Customer Satisfaction Score") %>'></asp:Label>
                                    <asp:TextBox runat="server" Width="60px" Visible="false" Text='<%# Eval("Overall Customer Satisfaction Score") %>' MaxLength="10"></asp:TextBox>                                    
                                </ItemTemplate>                           
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="contentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("Reasons for Customer Satisfaction Score"), 1500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="400px" Visible="false" Text='<%# StringHelper.TrimText(Eval("Reasons for Customer Satisfaction Score"), 1500)%>' MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="contentColumnWidth">
                                <ItemTemplate>
                                    <asp:Label runat="server" Text='<%# StringHelper.TrimText(Eval("Action Taken"), 1500)%>'></asp:Label>
                                    <asp:TextBox runat="server" Width="400px" Visible="false" Text='<%# StringHelper.TrimText(Eval("Action Taken"), 1500)%>' MaxLength="1500"></asp:TextBox> 
                                </ItemTemplate>
                            </asp:TemplateField>                                                                                   
                        </Columns>     
                    </asp:GridView>
                </div> 
                
                                        
            </div>

            <div style="page-break-before: always"></div>
        </div>

        <div id="divSitePage" runat="server">
            <div class="newPage">
                <div class="newPageCustomerAndTitle">
                    <span id="newPageCustomer8" runat="server"></span> -
                    <span id="newPageHeader8" runat="server"></span>
                </div>
                <div  id="newPageDates8" class="newPageDates" runat="server"></div>
                <img id="img8" src="" alt="logo" runat="server" />
            </div>
            <hr />

            <div class="section section8">
                <h2 id="siteHeader" runat="server"></h2>               

                 <div class="container">           
                    <h3 id="breakdownHeader" runat="server"></h3>
                    <div class="containerInner">
                        <asp:Chart ID="siteBreakdownPieChart" CssClass="pieChart" runat="server" ImageType="Png" EnableViewState="true" Width="812px" Height="512px">                            
                            <Series></Series>                
                            <ChartAreas>
                                <asp:ChartArea Area3DStyle-Enable3D="true" Area3DStyle-Inclination="60" Name="siteBreakdownPieChartArea" BackColor="White">                                                                                 
                                </asp:ChartArea>
                            </ChartAreas>
                            <Titles>
                                <asp:Title Font="Arial, 12pt, style=Bold" ForeColor="#666666" Alignment="TopLeft" Text=""></asp:Title>
                            </Titles>
                        </asp:Chart> 
                    </div>
                </div>

                <div class="container">           
                    <h3 id="remarksHeader" runat="server"></h3>
                    <div class="containerInner"">
                        <div class="richText">
                            <asp:ImageButton ID="btnRemarks" CssClass="editCircle editCircleGreyMarginTopLarge" ImageUrl="_assets/styles/themes/customReportImages/circle-grey.png" OnClick="remarksButton_Click" runat="server" />
                            <asp:Literal ID="litRemarks" runat="server"></asp:Literal>               
                            <telerik:RadEditor ID="editorRemarks" 
                                        runat="server" 
                                        CssClass="editorNewLine"                                                  
                                        Height="400px" 
                                        Width="786px"
                                        MaxHtmlLength="10000"
                                        NewLineMode="Div"
                                        ToolsWidth="812px"                                                                                                                                       
                                        ToolbarMode="ShowOnFocus"        
                                        ToolsFile="~/_assets/xml/CustomRadEditorToolsMinimum.xml" ContentFilters="RemoveScripts,FixUlBoldItalic,FixEnclosingP,IECleanAnchors,MozEmStrong,ConvertFontToSpan,ConvertToXhtml,IndentHTMLContent,EncodeScripts,OptimizeSpans,PdfExportFilter,ConvertInlineStylesToAttributes,ConvertTags"
                                        Visible="false"
                                        EnableResize="false"
                                        >   
                                        <CssFiles>
                                            <telerik:EditorCssFile Value="~/_assets/styles/radEditor.css" />
                                        </CssFiles>                                                                                
                            </telerik:RadEditor> 
                        </div>
                    </div>
                </div>
            </div>

            <div style="page-break-before: always"></div>
        </div>


        </asp:Panel>

        <asp:UpdatePanel ID="updatePanelNested" EnableViewState="false" runat="server" UpdateMode="Conditional">
            <ContentTemplate>             
                <div id="divDownload" class="divDownload" runat="server">                    
                    <asp:Label ID="lblDownloadError" Visible="false" CssClass="errorMessage" runat="server"></asp:Label>
                    <br />
                    <asp:Button ID="btnDownload" runat="server" OnClientClick="refreshHtml();" OnClick="btnDownloadButton_Click" CausesValidation="false" Text=""></asp:Button> - <span id="spanDownload" runat="server"></span>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <script language="javascript" type="text/javascript">
            function refreshHtml() {
                document.getElementById('<%= hdn_container.ClientID %>').value = document.head.innerHTML + document.body.innerHTML;
            }

            document.getElementById('<%= hdn_container.ClientID %>').value = document.head.innerHTML + document.body.innerHTML;
        </script>

        </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
