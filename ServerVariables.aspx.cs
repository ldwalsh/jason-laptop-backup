﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.UI;

namespace CW.Website
{
    public partial class ServerVariables : Page
    {
        public NameValueCollection GetServerVariables()
        {
            return HttpContext.Current.Request.ServerVariables;
        }
          
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}