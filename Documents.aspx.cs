﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;


using Microsoft.WindowsAzure.Storage;

using CW.Business;
using CW.Data;
using CW.Data.AzureStorage;
using CW.Data.AzureStorage.DataContexts.Blob;
using CW.Data.AzureStorage.Helpers;
using CW.Common.Helpers;
using CW.Utility;
using CW.Website._framework;
using CW.Website._documents;
using CW.Data.Models.File;

using Telerik.Web.UI;
using CW.Common.Constants;
using CW.Business.Blob;
using CW.Logging;
using CW.Data.Models;
using CW.Data.Collections;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class Documents: SitePage
    {
        #region Properties

            public string email;
            public FilesLookup mFile;
            const string addFileSuccess = " file addition was successful.";
            const string updateFileSuccess = " file update was successful.";
            const string updateFileFailed = " failed to update. Please contact an administrator.";
            const string fileExists = " file with that name already exists.";
            const string fileNotFound = "Error retrieving file. File not found. Please contact your system admin for more help.";           
            const string uploadFailed = " failed to upload. Please contact an administrator.";
            const string uploadSuccess = " uploaded successfully.";
            const string uploadInvalidDirectory = "Invalid upload directory. Please select again above.";
            const string deleteFailed = " failed to delete. Please contact an administrator.";
            const string deleteSuccess = " deleted successfully.";
            const string cannotRenameToNothing = "Cannot rename file to an empty string.";

            //private AzureBlobDataContext mBlobContext = null;

            private IList<GetFileData> ManagerFiles = Enumerable.Empty<GetFileData>().ToList();
            private IList<GetFileData> TaggedFiles = Enumerable.Empty<GetFileData>().ToList();
            private IList<GetFileData> UntaggedFiles = Enumerable.Empty<GetFileData>().ToList();

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //hide labels
                lblManageError.Visible = false;
                lblTagError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    ViewState.Clear();
                    
                    //bind the ddls
                    BindFileClasses(ddlManageFileClass);                    
                    BindFileClasses(ddlTaggerFileClass);
                    BindBuildings(ddlTaggerBuilding);
                    BindBuildings(ddlManageBuilding);  
                    
                    // bind sub directories to the treeview
                    BindToTree();

                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litDocumentsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.DocumentsBody);
                }
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Binds a dropdown for file all classes
            /// </summary>
            /// <param name="ddl"></param>
            private void BindFileClasses(DropDownList ddl)
            {
                ddl.DataTextField = "FileClassName";
                ddl.DataValueField = "FileClassID";
                ddl.DataSource = DataMgr.FileClassDataMapper.GetAllFileClasses();
                ddl.DataBind();
            }

            /// <summary>
            /// Binds a dropdown list by visible buildings
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="clientID"></param>
            private void BindBuildings(DropDownList ddl)
            {
                ddl.DataTextField = "BuildingName";
                ddl.DataValueField = "BID";
                ddl.DataSource = siteUser.VisibleBuildings;
                ddl.DataBind();
            }

            /// <summary>
            /// Binds equipment dropdown list by building id for equipment
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="bid"></param>
            private void BindEquipment(DropDownList ddl,int bid)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "View all or optionally select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "EquipmentName";
                ddl.DataValueField = "EID";
                ddl.DataSource = DataMgr.EquipmentDataMapper.GetAllEquipmentByBID(bid);
                ddl.DataBind();
            }

            /// <summary>
            /// Binds a dropdown list by class id for filetypes
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="fileClassID"></param>
            private void BindFileTypes(DropDownList ddl, int fileClassID, bool optionalText)
            {
                ListItem lItem = new ListItem();                
                lItem.Text = optionalText ? "View all or optionally select one..." : "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "FileTypeName";
                ddl.DataValueField = "FileTypeID";
                ddl.DataSource = DataMgr.FileTypeDataMapper.GetAllFileTypesByFileClassID(fileClassID);
                ddl.DataBind(); 
            }

        #endregion

        #region Load File

            /// <summary>
            /// Loads add file into a filelookup for upload
            /// </summary>
            /// <param name="file"></param>
            /// <param name="uploadedFile"></param>
            protected void LoadAddFormIntoFile(FilesLookup file, UploadedFile uploadedFile, int fileTypeID)
            {
                //File Name
                file.DisplayName = StringHelper.RemoveText(uploadedFile.FileName.Split('.')[0], 50);

                //Client ID
                file.CID = siteUser.CID;

                //Extension
                file.Extension = uploadedFile.GetExtension();

                //Size stored in bytes               
                file.FileSize = uploadedFile.ContentLength;

                //File Type
                file.FileTypeID = fileTypeID;

                file.DateModified = DateTime.UtcNow;
            }
        
        #endregion

        #region Dropdown Events

            /// <summary>
            /// On manage building ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManageBuilding_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlManageBuilding.SelectedValue != "-1")
                {
                    ddlManageFileClass.Items.FindByValue("-1").Text = ddlManageFileClass.SelectedValue == "-1" ? "View all or optionally select one..." : "Select one...";

                    //bind equipment
                    BindEquipment(ddlManageEquipment, Convert.ToInt32(ddlManageBuilding.SelectedValue));
                }
                else
                {
                    ddlManageBuilding.Items.FindByValue("-1").Text = ddlManageFileClass.SelectedValue != "-1" ? "View all or optionally select one..." : "Select one...";
                    ddlManageFileClass.Items.FindByValue("-1").Text = "Select one...";

                    ListItem lItem = new ListItem();
                    lItem.Text = "Select building first...";
                    lItem.Value = "-1";
                    lItem.Selected = true;

                    ddlManageEquipment.Items.Clear();
                    ddlManageEquipment.Items.Add(lItem);
                }

                BindManagerGrid();
            }

            /// <summary>
            /// On manage client equipment selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManageEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindManagerGrid();
            }


            /// <summary>
            /// On manage file class ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManageFileClass_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlManageFileClass.SelectedValue != "-1")
                {
                    ddlManageBuilding.Items.FindByValue("-1").Text = ddlManageBuilding.SelectedValue == "-1" ? "View all or optionally select one..." : "Select one...";

                    //bind file types
                    BindFileTypes(ddlManageFileType, Convert.ToInt32(ddlManageFileClass.SelectedValue), true);
                }
                else
                {
                    ddlManageFileClass.Items.FindByValue("-1").Text = ddlManageBuilding.SelectedValue != "-1" ? "View all or optionally select one..." : "Select one...";
                    ddlManageBuilding.Items.FindByValue("-1").Text = "Select one...";

                    ListItem lItem = new ListItem();
                    lItem.Text = "Select class first...";
                    lItem.Value = "-1";
                    lItem.Selected = true;

                    ddlManageFileType.Items.Clear();
                    ddlManageFileType.Items.Add(lItem);
                }

                BindManagerGrid();
            }

            /// <summary>
            /// On manage file type ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManageFileType_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindManagerGrid();
            }


            /// <summary>
            /// On load manage add file class ddl
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManageAddFileClass_OnLoad(object sender, EventArgs e)
            {
                DropDownList ddl = sender as DropDownList;

                //in order not to keep querying and appending since onload gets hit twice
                if (ddl.Items.Count == 1)
                {
                    //bind itself                
                    BindFileClasses(ddl);
                }
            }

            /// <summary>
            /// On manage add file class ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManageAddFileClass_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                DropDownList ddlClass = sender as DropDownList;
                DropDownList ddlType = (DropDownList)ddlClass.Parent.FindControl("ddlManageAddFileType");

                //bind file types                
                BindFileTypes(ddlType, Convert.ToInt32(ddlClass.SelectedValue), false);
            }


            /// <summary>
            /// On load manage edit file class ddl
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManageEditFileClass_OnLoad(object sender, EventArgs e)
            {
                DropDownList ddl = sender as DropDownList;

                //in order not to keep querying and appending since onload gets hit twice
                if (ddl.Items.Count == 1)
                {
                    //bind itself                
                    BindFileClasses(ddl);
                }
            }

            /// <summary>
            /// On manage edit file class ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlManageEditFileClass_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                DropDownList ddlClass = sender as DropDownList;
                DropDownList ddlType = (DropDownList)ddlClass.Parent.FindControl("ddlManageEditFileType");

                //bind file types                
                BindFileTypes(ddlType, Convert.ToInt32(ddlClass.SelectedValue), false);
            }

            /// <summary>
            /// On tag file class ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlTaggerFileClass_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                    //bind types
                    BindFileTypes(ddlTaggerFileType, Convert.ToInt32(ddlTaggerFileClass.SelectedValue), true);

                    BindTaggerGrids();
            }

            /// <summary>
            /// On tag file type ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlTaggerFileType_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                    BindTaggerGrids();
            }

            /// <summary>
            /// On tag building ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlTaggerBuilding_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                //try remove item in dropdown "select one" if exists
                ddlTaggerBuilding.Items.Remove(ddlTaggerBuilding.Items.FindByValue("-1"));

                ddlTaggerEquipment.Enabled = true;
                ddlTaggerFileClass.Enabled = true;
                ddlTaggerFileType.Enabled = true;

                //bind equipment ddl
                BindEquipment(ddlTaggerEquipment, Convert.ToInt32(ddlTaggerBuilding.SelectedValue));

                BindTaggerGrids();
            }

            /// <summary>
            /// On tag equipment ddl selection change
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlTaggerEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                    BindTaggerGrids();
            }

        #endregion

        #region Button Events

            /// <summary>
            /// On manage search files button click
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void manageSearchButton_Click(object sender, EventArgs e)
            {
                //bind grid by type, building, and equipment selected
                if (Convert.ToInt32(ddlManageFileType.SelectedValue) != -1 && Convert.ToInt32(ddlManageEquipment.SelectedValue) != -1)
                {
                    mrgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByEIDAndFileType(Convert.ToInt32(ddlManageEquipment.SelectedValue), Convert.ToInt32(ddlManageFileType.SelectedValue));
                    mrgFolderItems.DataBind();
                }
                else if (Convert.ToInt32(ddlManageFileType.SelectedValue) != -1 && Convert.ToInt32(ddlManageBuilding.SelectedValue) != -1)
                {
                    mrgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByBIDAndFileType(Convert.ToInt32(ddlManageBuilding.SelectedValue), Convert.ToInt32(ddlManageFileType.SelectedValue));
                    mrgFolderItems.DataBind();
                }
                else if (Convert.ToInt32(ddlManageEquipment.SelectedValue) != -1)
                {
                    mrgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByEID(Convert.ToInt32(ddlManageEquipment.SelectedValue));
                    mrgFolderItems.DataBind();
                }
                else if (Convert.ToInt32(ddlManageBuilding.SelectedValue) != -1)
                {
                    mrgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByBID(Convert.ToInt32(ddlManageBuilding.SelectedValue));
                    mrgFolderItems.DataBind();
                }
                else if (Convert.ToInt32(ddlManageFileType.SelectedValue) != -1)
                {
                    mrgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByFileTypeID(Convert.ToInt32(ddlManageFileType.SelectedValue));
                    mrgFolderItems.DataBind();
                }
                else
                {
                    //clear grid
                    //mrgFolderItems.MasterTableView.CommandItemSettings.ShowAddNewRecordButton = false;
                    mrgFolderItems.DataSource = Enumerable.Empty<FilesLookup>();
                    mrgFolderItems.Rebind();
                }
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// DEPRICATED - OLD AJAX TABS
            /// On Init for Tabs
            /// </summary>
            //protected void loadTabs(object sender, EventArgs e)
            //{
            //}

            /// <summary>
            /// DEPRICATED - OLD AJAX TABS
            /// Tab changed event
            /// </summary>
            //protected void onActiveTabChanged(object sender, EventArgs e)
            //{
            //    //if (TabContainer1.ActiveTabIndex == 0)
            //    //{                    
            //    //}
            //}

        #endregion

        #region Documents Browser/Tree Methods and Events

            /// <summary>
            /// Binds data to documents tree
            /// </summary>
            private void BindToTree()
            {
                var p = queryString.ToObject<DocumentsParamsObject>();

                documentsTree.AppendDataBoundItems = true;

                //client node
                //var client = clientService.Clients.Where(c => c.CID == siteUser.CID).First();
                //var clientNode = new RadTreeNode(client.ClientName, client.CID.ToString());

                //if ((p != null) && (client.CID == p.cid))
                //{
                //    clientNode.Expanded = true;
                //}

                //documentsTree.Nodes.Add(clientNode);

                    foreach (var building in siteUser.VisibleBuildings)
                    {
                        int buildingFileCount =  DataMgr.FileDataMapper.GetFileCountByBID(building.BID);

                        string buildingNodeName = String.Format("{0} - ({1})", building.BuildingName, buildingFileCount);

                        var buildingNode = new RadTreeNode(buildingNodeName, building.BID.ToString());

                        documentsTree.Nodes.Add(buildingNode);
                        //clientNode.Nodes.Add(buildingNode);

                        if ((p != null) && (building.BID == p.bid))
                        {
                            buildingNode.Expanded = true;

                            if (p.eid == null)
                            {
                                SelectNode(buildingNode);
                            }
                        }

                        var equipment = DataMgr.FileDataMapper.GetAllFilesEquipmentByBID(building.BID);

                        foreach (var e in equipment)
                        {
                            string equipmentNodeName = String.Format("{0} - ({1})", e.EquipmentName, e.FileCount);

                            var equipmentNode = new RadTreeNode(equipmentNodeName, e.EID.ToString());

                            buildingNode.Nodes.Add(equipmentNode);

                            if ((p != null) && (e.EID == p.eid))
                            {
                                SelectNode(equipmentNode);
                            }
                        }

                        //dont show/remove building node if the building doesnt have files and no equipment under it has files.
                        if (buildingFileCount == 0 && equipment.Count() == 0)
                        {
                            documentsTree.Nodes.Remove(buildingNode);
                        }

                        //foreach (var equipment in EquipmentDataMapper.GetAllEquipmentByBID(building.BID))
                        //{
                        //    var equipmentNode = new RadTreeNode(equipment.EquipmentName, equipment.EID.ToString());

                        //    buildingNode.Nodes.Add(equipmentNode);

                        //    if ((p != null) && (equipment.EID == p.eid))
                        //    {
                        //        SelectNode(equipmentNode);
                        //    }
                        //}
                    }
            }

            private void SelectNode(RadTreeNode node)
            {
                documentsTreeNodeClick(documentsTree, new RadTreeNodeEventArgs(node));

                node.Selected = true;

                node.Focus();
            }

            /// <summary>
            /// on reposity tree node click
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void documentsTreeNodeClick(object sender, RadTreeNodeEventArgs e)
            {
                if (e.Node.ParentNode == null)
                {
                    //get files attached to this building
                    // bind the gridview to the datasource

                    rgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByBID(Convert.ToInt32(e.Node.Value));
                    rgFolderItems.DataBind();
                }
                else
                {
                    //get files attached to this equipment
                    // bind the gridview to the datasource

                    rgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByEID(Convert.ToInt32(e.Node.Value));
                    rgFolderItems.DataBind();
                }
            }

            /// <summary>
            /// Get the appropriate icon on data bind
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="args"></param>
            protected void rgFolderItems_ItemDataBound(object sender, GridItemEventArgs args)
            {
                if (args.Item.ItemType == GridItemType.Item || args.Item.ItemType == GridItemType.AlternatingItem)
                {
                    GetFileData item = (GetFileData)args.Item.DataItem;
                    Image image = (Image)args.Item.FindControl("imgIcon");

                    if (item.Extension.ToUpper() == ".JPG" || item.Extension.ToUpper() == ".JPEG")
                    {
                        image.ImageUrl = @"_assets/images/image.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".GIF")
                    {
                        image.ImageUrl = @"_assets/images/gif.gif";
                    }
                    else if (item.Extension.ToUpper() == ".DOC")
                    {
                        image.ImageUrl = @"_assets/images/doc.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".DOCX")
                    {
                        image.ImageUrl = @"_assets/images/docx.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".PDF")
                    {
                        image.ImageUrl = @"_assets/images/pdf.gif";
                    }
                    else if (item.Extension.ToUpper() == ".TXT")
                    {
                        image.ImageUrl = @"_assets/images/text.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".XLS")
                    {
                        image.ImageUrl = @"_assets/images/xls.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".XLSX")
                    {
                        image.ImageUrl = @"_assets/images/xlsx.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".PPT")
                    {
                        image.ImageUrl = @"_assets/images/ppt.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".PPTX")
                    {
                        image.ImageUrl = @"_assets/images/pptx.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".DWG")
                    {
                        image.ImageUrl = @"_assets/images/dwg.gif";
                    }
                    else if (item.Extension.ToUpper() == ".CAD")
                    {
                        image.ImageUrl = @"_assets/images/cad.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".MAT")
                    {
                        image.ImageUrl = @"_assets/images/mat.gif";
                    }
                    else
                    {
                        image.ImageUrl = @"_assets/images/default.gif";
                    }
                }
            }

            /// <summary>
            /// On Click command for download
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="args"></param>
            protected void rgFolderItems_ItemCommand(object sender, GridCommandEventArgs args)
            {
                // handle either opening the item or rebinding the grid
                if (args.CommandName == "ItemClick")
                {
                    String clientID;
                    String nodeID;
                    String type;
                    int bid;

                    if (documentsTree.SelectedNode.ParentNode == null)
                    {
                        //for building
                        nodeID = documentsTree.SelectedNode.Value;
                        type = "b";

                        bid = Convert.ToInt32(nodeID);
                    }
                    else
                    {
                        //for equipment
                        nodeID = documentsTree.SelectedNode.Value;
                        type = "e";

                        bid = Convert.ToInt32(documentsTree.SelectedNode.ParentNode.Value);
                    }
                    

                    //get the correct file
                    GetFileData file;
                    if (type.Equals("b"))
                    {
                        file = DataMgr.FileDataMapper.GetAllFilesByBID(Convert.ToInt32(nodeID)).Where(f => f.FID.ToString() == args.CommandArgument.ToString()).First();
                    }
                    else
                    {
                        file = DataMgr.FileDataMapper.GetAllFilesByEID(Convert.ToInt32(nodeID)).Where(f => f.FID.ToString() == args.CommandArgument.ToString()).First();
                    }


                    if (siteUser.IsKGSFullAdminOrHigher)
                    {
                        var byteArray = new DocumentFile(siteUser.CID, file.Extension, DataMgr.OrgBasedBlobStorageProvider, new DocumentFile.Args(file.BlobGUID, null)).Retrieve();

                        if (byteArray != null)
                        {
                            this.Response.ClearContent();
                            this.Response.ClearHeaders();
                            this.Response.BufferOutput = true;
                            this.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.DisplayName + file.Extension + "\"");
                            this.Response.AddHeader("Content-Length", byteArray.Length.ToString());
                            this.Response.ContentType = "application/octet-stream";
                            this.Response.BinaryWrite(byteArray);
                            this.Response.Flush();
                            this.Response.End();
                        }
                        else
                        {
                            //file not found
                            lblBrowserError.Text = fileNotFound;
                            lblBrowserError.Visible = true;
                            lnkSetFocusBrowser.Focus();
                        }
                    }
                    else
                    {
                        //get client setting
                        ClientSetting mClientSetting = DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID);

                        //set new updated download amount as currentMonthlyDocumentDownload plus new file size
                        long updatedDownload = mClientSetting.CurrentMonthlyDocumentDownloads + file.FileSize;

                        //check that the new potential download amount is less than or equal to the maxdownload amount
                        if (updatedDownload <= mClientSetting.MaxMonthlyDocumentDownloads)
                        {
                            //get byte array from blob and return response to client
                            //DocumentsBlobDataContext blobContext = new DocumentsBlobDataContext(mAccount, Convert.ToInt32(clientID));

                            var byteArray = new DocumentFile(siteUser.CID, file.Extension, DataMgr.OrgBasedBlobStorageProvider, new DocumentFile.Args(file.BlobGUID, null)).Retrieve();

                            if (byteArray != null)
                            {
                                this.Response.ClearContent();
                                this.Response.ClearHeaders();
                                this.Response.BufferOutput = true;
                                this.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.DisplayName + file.Extension + "\"");
                                this.Response.AddHeader("Content-Length", byteArray.Length.ToString());
                                this.Response.ContentType = "application/octet-stream";
                                this.Response.BinaryWrite(byteArray);
                                this.Response.Flush();

                                //increase current monthly document download amount                            
                                mClientSetting.CurrentMonthlyDocumentDownloads = updatedDownload;

                                //increase currrent total document download amount
                                mClientSetting.CurrentTotalDocumentDownloads = mClientSetting.CurrentTotalDocumentDownloads + file.FileSize;

                                DataMgr.ClientDataMapper.UpdateClientAccountSettings(mClientSetting);

                                if (type.Equals("b"))
                                {
                                    DataMgr.FileDataMapper.UpdateFileBuildingViewCount(file.FID, file.BID);
                                }
                                else
                                {
                                    DataMgr.FileDataMapper.UpdateFileEquipmentViewCount(file.FID, file.EID);
                                }
                            }
                            else
                            {
                                //file not found
                                lblBrowserError.Text = fileNotFound;
                                lblBrowserError.Visible = true;
                                lnkSetFocusBrowser.Focus();
                            }
                        }
                        else
                        {
                            //max limit reached
                            lblBrowserError.Text = "Cannot download file, maximum monthly download limit reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.";
                            lblBrowserError.Visible = true;
                            lnkSetFocusBrowser.Focus();
                        }
                    }
                }
            }

            /// <summary>
            /// On need data source for documents view grid
            /// </summary>
            /// <param name="source"></param>
            /// <param name="e"></param>
            protected void rgFolderItems_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
            {
                if (documentsTree.SelectedNode != null)
                {
                    if (documentsTree.SelectedNode.ParentNode == null)
                    {
                        //get files attached to this building
                        // bind the gridview to the datasource
                        rgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByBID(Convert.ToInt32(documentsTree.SelectedNode.Value));
                    }
                    else
                    {
                        //get files attached to this equipment
                        // bind the gridview to the datasource
                        rgFolderItems.DataSource = DataMgr.FileDataMapper.GetAllFilesByEID(Convert.ToInt32(documentsTree.SelectedNode.Value));
                    }
                }
                else
                {
                    rgFolderItems.DataSource = Enumerable.Empty<GetFileData>();
                }
            }

        #endregion

        #region File Manger Methods and Events

            /// <summary>
            /// On item databound, get appropriate file icon for grid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="args"></param>
            protected void mrgFolderItems_ItemDataBound(object sender, GridItemEventArgs args)
            {
                if (args.Item.ItemType == GridItemType.Item || args.Item.ItemType == GridItemType.AlternatingItem)
                {
                    GetFileData item = (GetFileData)args.Item.DataItem;
                    Image image = (Image)args.Item.FindControl("imgIcon");

                    if (item.Extension.ToUpper() == ".JPG" || item.Extension.ToUpper() == ".JPEG")
                    {
                        image.ImageUrl = @"_assets/images/image.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".GIF")
                    {
                        image.ImageUrl = @"_assets/images/gif.gif";
                    }
                    else if (item.Extension.ToUpper() == ".DOC")
                    {
                        image.ImageUrl = @"_assets/images/doc.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".DOCX")
                    {
                        image.ImageUrl = @"_assets/images/docx.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".PDF")
                    {
                        image.ImageUrl = @"_assets/images/pdf.gif";
                    }
                    else if (item.Extension.ToUpper() == ".TXT")
                    {
                        image.ImageUrl = @"_assets/images/text.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".XLS")
                    {
                        image.ImageUrl = @"_assets/images/xls.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".XLSX")
                    {
                        image.ImageUrl = @"_assets/images/xlsx.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".PPT")
                    {
                        image.ImageUrl = @"_assets/images/ppt.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".PPTX")
                    {
                        image.ImageUrl = @"_assets/images/pptx.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".DWG")
                    {
                        image.ImageUrl = @"_assets/images/dwg.gif";
                    }
                    else if (item.Extension.ToUpper() == ".CAD")
                    {
                        image.ImageUrl = @"_assets/images/cad.jpg";
                    }
                    else if (item.Extension.ToUpper() == ".MAT")
                    {
                        image.ImageUrl = @"_assets/images/mat.gif";
                    }
                    else
                    {
                        image.ImageUrl = @"_assets/images/default.gif";
                    }
                }
                //insert mode, require
                else if (args.Item is GridDataInsertItem && args.Item.IsInEditMode){

                    DropDownList ddlClass = ((DropDownList)args.Item.FindControl("ddlManageAddFileClass"));

                    BindFileClasses(ddlClass);
                }
                //edit mode
                else if (args.Item is GridEditableItem && args.Item.IsInEditMode)
                {
                    GridEditableItem item = args.Item as GridEditableItem;

                    DropDownList ddlClass = ((DropDownList)args.Item.FindControl("ddlManageEditFileClass"));

                    BindFileClasses(ddlClass);

                    //set class and type edit dropdowns from hidden values
                    string fileClassValue = ((HiddenField)args.Item.FindControl("hdnManageEditFileClassID")).Value;

                    ((DropDownList)args.Item.FindControl("ddlManageEditFileClass")).SelectedValue = fileClassValue;

                    DropDownList ddlType = ((DropDownList)args.Item.FindControl("ddlManageEditFileType"));
                    
                    BindFileTypes(ddlType, Convert.ToInt32(fileClassValue), false);

                    ddlType.SelectedValue = ((HiddenField)args.Item.FindControl("hdnManageEditFileTypeID")).Value;                   
                }
            }

            /// <summary>
            /// Currently not being used.
            /// On click command
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="args"></param>
            protected void mrgFolderItems_ItemCommand(object sender, GridCommandEventArgs args)
            {
                // Not used right now, but for download on item click
                // handle either opening the item or rebinding the grid
                if (args.CommandName == "ItemClick")
                {
                    //get client id
                    int cid = siteUser.CID;

                    //get FID for selected file
                    GridEditableItem item = (GridEditableItem)mrgFolderItems.SelectedItems[0];
                    string key = item.KeyValues.Split('"')[1];

                    //get file
                    FilesLookup file = DataMgr.FileDataMapper.GetFile(Convert.ToInt32(key));

                    //DocumentsBlobDataContext blobContext = new DocumentsBlobDataContext(mAccount, cid);

                    //get byte array to return to client
                    var byteArray = new DocumentFile(cid, file.Extension, DataMgr.OrgBasedBlobStorageProvider, new DocumentFile.Args(file.BlobGUID, null)).Retrieve();

                    this.Response.ClearContent();
                    this.Response.ClearHeaders();
                    this.Response.BufferOutput = true;
                    this.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.DisplayName + file.Extension + "\"");
                    this.Response.AddHeader("Content-Length", byteArray.Length.ToString());
                    this.Response.ContentType = "application/octet-stream";
                    this.Response.BinaryWrite(byteArray);
                    this.Response.Flush();
                }
            }
            
            /// <summary>
            /// On need data source
            /// </summary>
            /// <param name="source"></param>
            /// <param name="e"></param>
            protected void mrgFolderItems_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
            {
                mrgFolderItems.DataSource = GetManagerFiles();
            }

            /// <summary>
            /// On update
            /// </summary>
            /// <param name="source"></param>
            /// <param name="e"></param>
            protected void mrgFolderItems_UpdateCommand(object source, GridCommandEventArgs e)
            {
                //grab the FID
                GridEditableItem item = (GridEditableItem)mrgFolderItems.EditItems[0];
                string key = item.KeyValues.Split('"')[1];

                //get existing file                
                FilesLookup existingFile = DataMgr.FileDataMapper.GetFile(Convert.ToInt32(key));

                //get new type
                int fileTypeID = Convert.ToInt32(((DropDownList)e.Item.FindControl("ddlManageEditFileType")).SelectedValue);

                //grab the uploader
                GridEditableItem editedItem = e.Item as GridEditableItem;
                RadAsyncUpload radUpload = (RadAsyncUpload)e.Item.FindControl("ReplaceFileUploader");

                //grab the rename
                TextBox editName = (TextBox)item.FindControl("editName");

                //if replacing
                if (radUpload.UploadedFiles.Count > 0)
                {
                    try
                    {
                        int cid = siteUser.CID;

                        //replace the file in the blob
                        //DocumentsBlobDataContext blobContext = new DocumentsBlobDataContext(mAccount, cid);
                        UploadedFile replaceFile = radUpload.UploadedFiles[0];

                        //already existing file size
                        long oldFileSize = existingFile.FileSize;
                        string oldFileExtension = existingFile.Extension;
                        string oldFileDisplayName = existingFile.DisplayName;

                        FilesLookup mFile = existingFile;

                        //Size stored in bytes              
                        mFile.FileSize = replaceFile.ContentLength;

                        //name
                        mFile.DisplayName = !String.IsNullOrEmpty(editName.Text) ? editName.Text : StringHelper.RemoveText(replaceFile.FileName.Split('.')[0], 50); ;

                        //type
                        mFile.FileTypeID = fileTypeID;

                        mFile.Extension = replaceFile.GetExtension();

                        //check that file does not already exist
                        if (mFile.DisplayName == oldFileDisplayName || !DataMgr.FileDataMapper.DoesFileExist(mFile.DisplayName, cid))
                        {
                            //get client setting
                            ClientSetting mClientSetting = DataMgr.ClientDataMapper.GetClientSettingsByClientID(cid);

                            //calculate new potential amounts
                            long updatedUpload = mClientSetting.CurrentMonthlyDocumentUploads + mFile.FileSize;
                            long updatedTotalStorage = mClientSetting.CurrentTotalDocumentStorage + mFile.FileSize - oldFileSize;

                            //check that the updatedTotalStorage is less than or equal to the maxtotalDocumentStorage amount
                            if (updatedTotalStorage <= mClientSetting.MaxTotalDocumentStorage)
                            {
                                //check that the updatedUpload amount is less than or equal tot he MaxmonthlyDocumentUpload amount
                                if (updatedUpload <= mClientSetting.MaxMonthlyDocumentUploads)
                                {
                                    var criteria = new StorageAccountCriteria() { CID = cid, CWEnvironment = DataMgr.CWEnv, StorageAcctLevel = StorageAccountLevel.Primary };

                                    if (mFile.Extension != oldFileExtension)
                                    {
                                        DocumentFile d = new DocumentFile(cid, oldFileExtension, DataMgr.OrgBasedBlobStorageProvider, new DocumentFile.Args(existingFile.BlobGUID, null));                                        
                                        d.Delete();

                                        d = new DocumentFile(cid, mFile.Extension, DataMgr.OrgBasedBlobStorageProvider, new DocumentFile.Args(existingFile.BlobGUID, replaceFile.ContentType));                                        
                                        d.Insert(StreamHelper.ReadAll(replaceFile.InputStream));

                                        //DataMgr.FileDataMapper.PutDocumentsBlob(cid, file.BlobGUID.ToString(), CommonHelper.ReadAll(replaceFile.InputStream), replaceFile.ContentType);
                                        //blobContext.PutBlob(file.BlobGUID.ToString(),
                                        //                    CommonHelper.ReadAll(replaceFile.InputStream),
                                        //                    replaceFile.ContentType);
                                    }
                                    else
                                    {
                                        DocumentFile d = new DocumentFile(cid, mFile.Extension, DataMgr.OrgBasedBlobStorageProvider, new DocumentFile.Args(existingFile.BlobGUID, replaceFile.ContentType));

                                        d.Upsert(StreamHelper.ReadAll(replaceFile.InputStream));
                                    }

                                    DataMgr.FileDataMapper.UpdateFile(mFile);

                                    //update client setting
                                    mClientSetting.CurrentTotalDocumentStorage = updatedTotalStorage;
                                    mClientSetting.CurrentMonthlyDocumentUploads = updatedUpload;
                                    mClientSetting.CurrentTotalDocumentUploads = mClientSetting.CurrentTotalDocumentUploads + existingFile.FileSize;

                                    DataMgr.ClientDataMapper.UpdateClientAccountSettings(mClientSetting);

                                    LabelHelper.SetLabelMessage(lblManageError, mFile.DisplayName + updateFileSuccess, lnkSetFocusManage);
                                }

                                else
                                {
                                    LabelHelper.SetLabelMessage(lblManageError, "Cannot upload file, monthly upload limit reached.", lnkSetFocusManage);
                                }
                            }
                            else
                            {
                                LabelHelper.SetLabelMessage(lblManageError, "Cannot upload file, total documents storage cap reached.", lnkSetFocusManage);
                            }
                        }
                        else
                        {
                            //file exists
                            LabelHelper.SetLabelMessage(lblManageError, mFile.DisplayName + " " + fileExists, lnkSetFocusManage);
                        }
                    }
                    catch
                    {
                        LabelHelper.SetLabelMessage(lblManageError, mFile.DisplayName + updateFileFailed, lnkSetFocusManage);
                    }
                }
                //else just rename or change type
                else
                {                    
                    if (!String.IsNullOrEmpty(editName.Text))
                    {
                        string name = existingFile.DisplayName;

                        //check that file does not already exist if name change
                        if (editName.Text != existingFile.DisplayName)
                        {
                            if (!DataMgr.FileDataMapper.DoesFileExist(editName.Text, siteUser.CID))
                            {
                                try
                                {
                                    //update
                                    DataMgr.FileDataMapper.UpdatePartialFile(Convert.ToInt32(key), editName.Text, fileTypeID);
                                    LabelHelper.SetLabelMessage(lblManageError, existingFile.DisplayName + updateFileSuccess, lnkSetFocusManage); 
                                }
                                catch
                                {
                                    LabelHelper.SetLabelMessage(lblManageError, existingFile.DisplayName + updateFileFailed, lnkSetFocusManage); 
                                }
                            }
                            else
                            {
                                //file exists
                                LabelHelper.SetLabelMessage(lblManageError, fileExists, lnkSetFocusManage); 
                            }
                        }
                        else
                        {
                            DataMgr.FileDataMapper.UpdatePartialFile(Convert.ToInt32(key), existingFile.DisplayName, fileTypeID);
                        }
                    }
                    else
                    {
                        //cannot rename a file to an empty string
                        LabelHelper.SetLabelMessage(lblManageError, cannotRenameToNothing, lnkSetFocusManage); 
                    }
                }
            }

            /// <summary>
            /// On insert
            /// </summary>
            /// <param name="source"></param>
            /// <param name="e"></param>
            protected void mrgFolderItems_InsertCommand(object source, GridCommandEventArgs e)
            {                
                int cid = siteUser.CID;

                int fileTypeID = Convert.ToInt32(((DropDownList)e.Item.FindControl("ddlManageAddFileType")).SelectedValue);

                //clear previous message
                lblManageError.Text = "";

                //if ddls are filled out
                if (fileTypeID != -1)
                {
                    //DocumentsBlobDataContext blobContext = new DocumentsBlobDataContext(mAccount, clientID);

                    GridEditableItem editedItem = e.Item as GridEditableItem;
                    RadAsyncUpload radUpload = (RadAsyncUpload)e.Item.FindControl("AddFileUploader");

                    //grab uploaded files from uploader
                    UploadedFileCollection files = radUpload.UploadedFiles;

                    if (files.Count > 0)
                    {
                        int counter = 1;

                        foreach (UploadedFile file in files)
                        {
                            mFile = new FilesLookup();

                            //load the form into the file before files exists check for name trim
                            LoadAddFormIntoFile(mFile, file, fileTypeID);
                            
                            ClientSetting mClientSetting = DataMgr.ClientDataMapper.GetClientSettingsByClientID(cid);

                            //check that file does not already exist
                            if (!DataMgr.FileDataMapper.DoesFileExist(file.FileName.Split('.')[0], cid))
                            {
                                //calculate the new potential amounts
                                long updatedUpload = mClientSetting.CurrentMonthlyDocumentUploads + mFile.FileSize;
                                long updatedTotalStorage = mClientSetting.CurrentTotalDocumentStorage + mFile.FileSize;

                                //check that the updatedTotalStorage is less than or equal to the maxtotalDocumentStorage amount
                                if (updatedTotalStorage <= mClientSetting.MaxTotalDocumentStorage)
                                {
                                    //check that the updatedUpload amount is less than or equal tot he MaxmonthlyDocumentUpload amount
                                    if (updatedUpload <= mClientSetting.MaxMonthlyDocumentUploads)
                                    {
                                        try
                                        {
                                            //insert new file, returns newly created guid
                                            mFile.BlobGUID = DataMgr.FileDataMapper.InsertFileAndReturnGUID(mFile);

                                            try
                                            {
                                                var criteria = new StorageAccountCriteria() { CID = cid, CWEnvironment = DataMgr.CWEnv, StorageAcctLevel = StorageAccountLevel.Primary };
                                                new DocumentFile(cid, mFile.Extension, DataMgr.OrgBasedBlobStorageProvider, new DocumentFile.Args(mFile.BlobGUID, file.ContentType)).Insert(StreamHelper.ReadAll(file.InputStream));
												//DataMgr.FileDataMapper.PutDocumentsBlob(cid, mFile.BlobGUID.ToString(), CommonHelper.ReadAll(file.InputStream), file.ContentType);
                                                //blobContext.PutBlob(mFile.BlobGUID.ToString(), CommonHelper.ReadAll(file.InputStream), file.ContentType );

                                                //update client setting
                                                mClientSetting.CurrentTotalDocumentStorage = updatedTotalStorage;
                                                mClientSetting.CurrentMonthlyDocumentUploads = updatedUpload;
                                                mClientSetting.CurrentTotalDocumentUploads = mClientSetting.CurrentTotalDocumentUploads + mFile.FileSize;

                                                DataMgr.ClientDataMapper.UpdateClientAccountSettings(mClientSetting);

                                                LabelHelper.SetLabelMessage(lblManageError, mFile.DisplayName + addFileSuccess, lnkSetFocusManage, true, counter > 1);
                                            }
                                            catch (Exception ex)
                                            {
                                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error uploading file.", ex);
                                                LabelHelper.SetLabelMessage(lblManageError, mFile.DisplayName + " " + BusinessConstants.Message.FaliedTransaction, lnkSetFocusManage, true, counter > 1);
                                            }
                                        }
                                        catch (SqlException sqlEx)
                                        {
                                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding files.", sqlEx);
                                            LabelHelper.SetLabelMessage(lblManageError, mFile.DisplayName + " " + BusinessConstants.Message.FaliedTransaction, lnkSetFocusManage, true, counter > 1);
                                        }
                                        catch (Exception ex)
                                        {
                                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding file.", ex);
                                            LabelHelper.SetLabelMessage(lblManageError, mFile.DisplayName + " " + BusinessConstants.Message.FaliedTransaction, lnkSetFocusManage, true, counter > 1);
                                        }
                                    }
                                    else
                                    {
                                        LabelHelper.SetLabelMessage(lblManageError, "Cannot upload file " + mFile.DisplayName + ", monthly upload limit reached.", lnkSetFocusManage, true, counter > 1);

                                        //return on limit reached
                                        return;
                                    }
                                }
                                else
                                {
                                    LabelHelper.SetLabelMessage(lblManageError, "Cannot upload file " + mFile.DisplayName + ", total documents storage cap reached.", lnkSetFocusManage, true, counter > 1);

                                    //return on limit reached
                                    return;
                                }
                            }
                            else
                            {
                                //file exists
                                LabelHelper.SetLabelMessage(lblManageError, file.FileName.Split('.')[0] + " " + fileExists, lnkSetFocusManage, true, counter > 1);
                            }

                            counter++;
                        }

                        //rebind only if the search terms includes class and type
                        if (Convert.ToInt32(ddlManageFileType.SelectedValue) != -1)
                        {
                            mrgFolderItems.DataSource = GetManagerFiles();
                            mrgFolderItems.DataBind();
                        }
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblManageError, "Please select a file to upload.", lnkSetFocusManage);

                        return;
                    }
                }
                else
                {
                    LabelHelper.SetLabelMessage(lblManageError, "Please select a file class and file type.", lnkSetFocusManage);

                    return;
                }
            }

            /// <summary>
            /// On delete
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void mrgFolderItems_DeleteCommand(object sender, GridCommandEventArgs e)
            {
                //get deleting row file name
                GridEditableItem editedItem = e.Item as GridEditableItem;
                string key = editedItem.KeyValues.Split('"')[1];
                int fid = Convert.ToInt32(key);

                int cid = siteUser.CID;
                // client settings
                ClientSetting mClientSetting = DataMgr.ClientDataMapper.GetClientSettingsByClientID(cid);

                //grab file
                FilesLookup file = DataMgr.FileDataMapper.GetFile(fid);

				//calculate new potential amount
                long updatedTotalStorage = mClientSetting.CurrentTotalDocumentStorage - file.FileSize;

                //DocumentsBlobDataContext blobContext = new DocumentsBlobDataContext(mAccount, clientID);

                try
                {
                    //try to delete file from blob storage
                    //blobContext.DeleteBlob(file.BlobGUID.ToString());

					new DocumentFile(cid, file.Extension, DataMgr.OrgBasedBlobStorageProvider, new DocumentFile.Args(file.BlobGUID, null)).Delete();
					//DataMgr.FileDataMapper.DeleteDocumentsBlob(cid, file.BlobGUID.ToString());

                    //update client setting
                    mClientSetting.CurrentTotalDocumentStorage = updatedTotalStorage;

                    DataMgr.ClientDataMapper.UpdateClientAccountSettings(mClientSetting);

                    try
                    {
                        //Remove tags
                        DataMgr.FileDataMapper.UntagFileFromAll(file.FID);

                        try
                        {
                            //delete file based on tree selected node for location path                
                            DataMgr.FileDataMapper.DeleteFile(fid);
                            LabelHelper.SetLabelMessage(lblManageError, file.DisplayName + deleteSuccess, lnkSetFocusManage);
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error deleting documents file: FID={0}.", fid), ex);
                            LabelHelper.SetLabelMessage(lblManageError, file.DisplayName + deleteFailed, lnkSetFocusManage);
                        }                        
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error untagging documents file: FID={0}.", fid), ex);
                        LabelHelper.SetLabelMessage(lblManageError, file.DisplayName + deleteFailed, lnkSetFocusManage);
                    }                    
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error deleting documents file: FID={0} from blob storage.", fid), ex);
                    LabelHelper.SetLabelMessage(lblManageError, file.DisplayName + deleteFailed, lnkSetFocusManage);
                }

                //if (Convert.ToInt32(ddlManageFileType.SelectedValue) != -1)
                //{
                //    mrgFolderItems.DataSource = FileDataMapper.GetAllFilesByFileTypeIDAndCID(Convert.ToInt32(ddlManageFileType.SelectedValue), clientID);
                //    mrgFolderItems.DataBind();
                //}
                //else
                //{
                //    mrgFolderItems.DataSource = Enumerable.Empty<FilesLookup>();
                //    mrgFolderItems.DataBind();
                //}
                mrgFolderItems.DataSource = GetManagerFiles();
                mrgFolderItems.DataBind();
            }

            /// <summary>
            /// Get manager files
            /// </summary>
            /// <returns></returns>
            protected IList<GetFileData> GetManagerFiles()
            {
                if (ManagerFiles.Any())
                {
                    return ManagerFiles;
                }
                else
                {
                    int? bid = Convert.ToInt32(ddlManageBuilding.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlManageBuilding.SelectedValue) : null;
                    int? eid = Convert.ToInt32(ddlManageEquipment.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlManageEquipment.SelectedValue) : null;
                    int? classID = Convert.ToInt32(ddlManageFileClass.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlManageFileClass.SelectedValue) : null;
                    int? typeID = Convert.ToInt32(ddlManageFileType.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlManageFileType.SelectedValue) : null;

                    IList<GetFileData> results = new List<GetFileData>();

                    if (eid != null && typeID != null)
                    {
                        results = DataMgr.FileDataMapper.GetAllFilesByEIDAndFileType((int)eid, (int)typeID).ToList();
                    }
                    else if (bid != null && typeID != null)
                    {
                        results = DataMgr.FileDataMapper.GetAllFilesByBIDAndFileType((int)bid, (int)typeID).ToList();
                    }
                    if (eid != null && classID != null)
                    {
                        results = DataMgr.FileDataMapper.GetAllFilesByEIDAndFileClass((int)eid, (int)classID).ToList();
                    }
                    else if (bid != null && classID != null)
                    {
                        results = DataMgr.FileDataMapper.GetAllFilesByBIDAndFileClass((int)bid, (int)classID).ToList();
                    }
                    else if (eid != null)
                    {
                        results = DataMgr.FileDataMapper.GetAllFilesByEID((int)eid).ToList();
                    }
                    else if (bid != null)
                    {
                        results = DataMgr.FileDataMapper.GetAllFilesByBID((int)bid).ToList();
                    }
                    else if (typeID != null)
                    {
                        results = DataMgr.FileDataMapper.GetAllFilesByFileTypeIDAndCID((int)typeID, siteUser.CID).ToList();
                    }
                    else if (classID != null)
                    {
                        results = DataMgr.FileDataMapper.GetAllFilesByFileClassIDAndCID((int)classID, siteUser.CID).ToList();
                    }
                    else
                    {
                        results = Enumerable.Empty<GetFileData>().ToList();
                    }

                    ManagerFiles = results;

                    return results;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            protected void BindManagerGrid()
            {
                mrgFolderItems.DataSource = GetManagerFiles();
                mrgFolderItems.DataBind();
            }

        #endregion

        #region File Tagger Methods and Events

        /// <summary>
        /// Definition for untagged files
        /// </summary>
        //protected IList<FileDataMapper.GetFileData> UntaggedFiles
        //{
        //    get
        //    {
        //        if (Convert.ToInt32(ddlTagBuilding.SelectedValue) != -1 && Convert.ToInt32(ddlTagFileType.SelectedValue) != -1)
        //        {
        //            if (Convert.ToInt32(ddlTagEquipment.SelectedValue) == -1)
        //            {
        //                return GetUntaggedFilesByBID(Convert.ToInt32(ddlTagBuilding.SelectedValue));
        //            }
        //            else
        //            {
        //                return GetUntaggedFilesByEID(Convert.ToInt32(ddlTagBuilding.SelectedValue));
        //            }
        //        }
        //        else
        //        {
        //            return Enumerable.Empty<FileDataMapper.GetFileData>().ToList();
        //        }
        //        /*try
        //        {
        //            object obj = Session["UntaggedFiles"];
        //            if (obj == null)
        //            {
        //                //either bind by BID or EID
        //                if (Convert.ToInt32(ddlTagEquipment.SelectedValue.ToString()) == -1)
        //                {
        //                    obj = GetUntaggedFilesByBID(Convert.ToInt32(ddlTagBuilding.SelectedValue.ToString()));
        //                }
        //                else
        //                {
        //                    obj = GetUntaggedFilesByEID(Convert.ToInt32(ddlTagBuilding.SelectedValue.ToString()));
        //                }
        //                if (obj != null)
        //                {
        //                    Session["UntaggedFiles"] = obj;
        //                }
        //                else
        //                {
        //                    obj = new List<FileDataMapper.GetFileData>();
        //                }
        //            }
        //            return (IList<FileDataMapper.GetFileData>)obj;
        //        }
        //        catch
        //        {
        //            Session["UntaggedFiles"] = null;
        //        }
        //        return new List<FileDataMapper.GetFileData>();*/
        //    }
        //    set { grdUntaggedFiles.DataSource = value; grdUntaggedFiles.Rebind(); }
        //}

        /// <summary>
        /// Definition for tagged files
        /// </summary>
        //protected IList<FileDataMapper.GetFileData> TaggedFiles
        //{
        //    get
        //    {
        //        return GetTaggedFiles();
                
        //        /*
        //         * Maybe use this method for optimizaiton? Reliability issues, sometimes a file would get tagged but not move grids
        //        try
        //        {
        //            object obj = Session["TaggedFiles"];
        //            if (obj == null)
        //            {
        //                //get by BID or EID
        //                if (Convert.ToInt32(ddlTagEquipment.SelectedValue.ToString()) == -1)
        //                {
        //                    obj = GetTaggedFilesByBID(Convert.ToInt32(ddlTagBuilding.SelectedValue.ToString()));
        //                }
        //                else
        //                {
        //                    obj = GetTaggedFilesByEID(Convert.ToInt32(ddlTagEquipment.SelectedValue.ToString()));
        //                }
        //                if (obj != null)
        //                {
        //                    Session["TaggedFiles"] = obj;
        //                }
        //                else
        //                {
        //                    obj = new List<FileDataMapper.GetFileData>();
        //                }

        //            }
        //            return (IList<FileDataMapper.GetFileData>)obj;
        //        }
        //        catch
        //        {
        //            Session["TaggedFiles"] = null;
        //        }
        //        return new List<FileDataMapper.GetFileData>();*/
        //    }
        //    set { grdTaggedFiles.DataSource = value; grdTaggedFiles.Rebind(); }// Session["TaggedFiles"] = value; } 
        //}


        /// <summary>
        /// get untagged files
        /// </summary>
        /// <returns></returns>
        protected IList<GetFileData> GetUntaggedFiles()
        {
            if (UntaggedFiles.Any())
            {
                return UntaggedFiles;
            }
            else
            {
                int? classID = Convert.ToInt32(ddlTaggerFileClass.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlTaggerFileClass.SelectedValue) : null;
                int? typeID = Convert.ToInt32(ddlTaggerFileType.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlTaggerFileType.SelectedValue) : null;

                IList<GetFileData> results = new List<GetFileData>();

                //get all files already tagged 
                IList<GetFileData> alreadyTagged = GetTaggedFiles();

                //get all files untagged
                if (typeID != null)
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByFileTypeIDAndCID((int)typeID, siteUser.CID).ToList();
                }
                else if (classID != null)
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByFileClassIDAndCID((int)classID, siteUser.CID).ToList();
                }
                else
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByCID(siteUser.CID).ToList();
                }

                //remove already tagged files
                results = results.Except(alreadyTagged, new FileComparer()).ToList();
                //foreach (FileDataMapper.GetFileData file in alreadyTagged)
                //{
                //    if (results.Where(f => f.FID.Equals(file.FID)).Any())
                //    {
                //        results.Remove(results.Where(f => f.FID.Equals(file.FID)).First());
                //    }
                //}

                UntaggedFiles = results;

                return results;
            }
        }

        /// <summary>
        /// Get tagged files
        /// </summary>
        /// <returns></returns>
        protected IList<GetFileData> GetTaggedFiles()
        {
            if (TaggedFiles.Any())
            {
                return TaggedFiles;
            }
            else
            {
                int? bid = Convert.ToInt32(ddlTaggerBuilding.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlTaggerBuilding.SelectedValue) : null;
                int? eid = Convert.ToInt32(ddlTaggerEquipment.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlTaggerEquipment.SelectedValue) : null;
                int? classID = Convert.ToInt32(ddlTaggerFileClass.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlTaggerFileClass.SelectedValue) : null;
                int? typeID = Convert.ToInt32(ddlTaggerFileType.SelectedValue) != -1 ? (int?)Convert.ToInt32(ddlTaggerFileType.SelectedValue) : null;

                IList<GetFileData> results = new List<GetFileData>();

                if (eid != null && typeID != null)
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByEIDAndFileType((int)eid, (int)typeID).ToList();
                }
                else if (bid != null && typeID != null)
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByBIDAndFileType((int)bid, (int)typeID).ToList();
                }
                else if (eid != null && classID != null)
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByEIDAndFileClass((int)eid, (int)classID).ToList();
                }
                else if (bid != null && classID != null)
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByBIDAndFileClass((int)bid, (int)classID).ToList();
                }
                else if (eid != null)
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByEID((int)eid).ToList();
                }
                else if (bid != null)
                {
                    results = DataMgr.FileDataMapper.GetAllFilesByBID((int)bid).ToList();
                }
                //cant be tagged without eid or bid
                else
                {
                    results = Enumerable.Empty<GetFileData>().ToList();
                }

                TaggedFiles = results;

                return results;
            }
        }


        /// <summary>
        /// on untagged need data source
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void grdUntaggedFiles_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            //only get untagged files after a building is selected and not on page first load
            grdUntaggedFiles.DataSource = Convert.ToInt32(ddlTaggerBuilding.SelectedValue) != -1 ? GetUntaggedFiles() : UntaggedFiles;
        }

        /// <summary>
        /// On tagged need data source
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void grdTaggedFiles_NeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            //only get tagged files after a building is selected and not on page first load
            grdTaggedFiles.DataSource = Convert.ToInt32(ddlTaggerBuilding.SelectedValue) != -1 ? GetTaggedFiles() : TaggedFiles;
        }

        /// <summary>
        /// on row drop from untagged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdUntaggedFiles_RowDrop(object sender, GridDragDropEventArgs e)
        {
            if (string.IsNullOrEmpty(e.HtmlElement))
            {
                if (e.DraggedItems[0].OwnerGridID == grdUntaggedFiles.ClientID)
                {
                    // items are drag from untagged to tagged
                    if ((e.DestDataItem == null && TaggedFiles.Count == 0) ||
                        e.DestDataItem != null && e.DestDataItem.OwnerGridID == grdTaggedFiles.ClientID)
                    {
                        IList<GetFileData> taggedFiles = GetTaggedFiles();
                        IList<GetFileData> untaggedFiles = GetUntaggedFiles();
                        int destinationIndex = -1;
                        if(e.DestDataItem != null)
                        {
                            GetFileData file = DataMgr.FileDataMapper.GetFullFile(Convert.ToInt32(e.DestDataItem.GetDataKeyValue("FID").ToString()));
                            destinationIndex = (file != null) ? taggedFiles.IndexOf(file) : -1;
                        }
                       
                        //for each of the dragged items
                        foreach (GridDataItem draggedItem in e.DraggedItems)
                        {
                            //grab file
                            GetFileData tmpFile = DataMgr.FileDataMapper.GetFullFile(Convert.ToInt32(draggedItem.GetDataKeyValue("FID").ToString()));

                            if (tmpFile != null)
                            {
                                if (destinationIndex > -1)
                                {
                                    if (e.DropPosition == GridItemDropPosition.Below)
                                    {
                                        destinationIndex += 1;
                                    }
                                    taggedFiles.Insert(destinationIndex, tmpFile);
                                }
                                else
                                {
                                    //tag file
                                    if (Convert.ToInt32(ddlTaggerEquipment.SelectedValue) == -1)
                                    {
                                        FilesLookup_Building newTag = new FilesLookup_Building();
                                        newTag.BID = Convert.ToInt32(ddlTaggerBuilding.SelectedValue);
                                        newTag.FID = tmpFile.FID;
                                        DataMgr.FileDataMapper.TagFileToBuilding(newTag);
                                    }
                                    else
                                    {
                                        //baseline .MAT file check
                                        if (tmpFile.Extension.ToUpper() == BusinessConstants.File.MatExtension &&  tmpFile.FileTypeID == BusinessConstants.FileType.BaselineFilesFileTypeID)
                                        {
                                            if (DataMgr.FileDataMapper.DoesBaselineMatFileExistForEID(Convert.ToInt32(ddlTaggerEquipment.SelectedValue)))
                                                continue;

                                            //TODO: maybe notify the user of this
                                        }

                                        FilesLookup_Equipment newTag = new FilesLookup_Equipment();
                                        newTag.EID = Convert.ToInt32(ddlTaggerEquipment.SelectedValue);
                                        newTag.FID = tmpFile.FID;
                                        DataMgr.FileDataMapper.TagFileToEquipment(newTag);
                                    }
                                    taggedFiles.Add(tmpFile);
                                }

                                untaggedFiles.Remove(untaggedFiles.Where(f=>f.FID.Equals(tmpFile.FID)).First());
                            }
                        }

                        TaggedFiles = taggedFiles;
                        UntaggedFiles = untaggedFiles;
                        grdUntaggedFiles.Rebind();
                        grdTaggedFiles.Rebind();
                    }
                    else if (e.DestDataItem != null && e.DestDataItem.OwnerGridID == grdUntaggedFiles.ClientID)
                    {
                        //reorder items in pending grid
                        /*IList<FileDataMapper.GetFileData> untaggedFiles = UntaggedFiles;
                        FileDataMapper.GetFileData file = FileDataMapper.GetFullFile(Convert.ToInt32(e.DestDataItem.GetDataKeyValue("FID").ToString()));
                        int destinationIndex = untaggedFiles.IndexOf(file);

                        if (e.DropPosition == GridItemDropPosition.Above && e.DestDataItem.ItemIndex > e.DraggedItems[0].ItemIndex)
                        {
                            destinationIndex -= 1;
                        }
                        if (e.DropPosition == GridItemDropPosition.Below && e.DestDataItem.ItemIndex < e.DraggedItems[0].ItemIndex)
                        {
                            destinationIndex += 1;
                        }

                        List<FileDataMapper.GetFileData> filesToMove = new List<FileDataMapper.GetFileData>();
                        foreach (GridDataItem draggedItem in e.DraggedItems)
                        {
                            FileDataMapper.GetFileData tmpFile = FileDataMapper.GetFullFile(Convert.ToInt32(draggedItem.GetDataKeyValue("FID").ToString()));
                            if (tmpFile != null)
                                filesToMove.Add(tmpFile);
                        }

                        foreach (FileDataMapper.GetFileData fileToMove in filesToMove)
                        {
                            untaggedFiles.Remove(fileToMove);
                            untaggedFiles.Insert(destinationIndex, fileToMove);
                        }
                        UntaggedFiles = untaggedFiles;
                        grdUntaggedFiles.Rebind();

                        int destinationItemIndex = destinationIndex - (grdUntaggedFiles.PageSize * grdUntaggedFiles.CurrentPageIndex);
                        e.DestinationTableView.Items[destinationItemIndex].Selected = true;*/
                    }
                }
            }
        }

        /// <summary>
        /// on row drop from tagged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdTaggedFiles_RowDrop(object sender, GridDragDropEventArgs e)
        {
            if (string.IsNullOrEmpty(e.HtmlElement))
            {
                if (e.DraggedItems[0].OwnerGridID == grdTaggedFiles.ClientID)
                {
                    // items are drag from tagged to untagged
                    if ((e.DestDataItem == null && UntaggedFiles.Count == 0) ||
                        e.DestDataItem != null && e.DestDataItem.OwnerGridID == grdUntaggedFiles.ClientID)
                    {
                        IList<GetFileData> taggedFiles = GetTaggedFiles();
                        IList<GetFileData> untaggedFiles = GetUntaggedFiles();
                        int destinationIndex = -1;
                        if (e.DestDataItem != null)
                        {
                            GetFileData file = DataMgr.FileDataMapper.GetFullFile(Convert.ToInt32(e.DestDataItem.GetDataKeyValue("FID").ToString()));
                            destinationIndex = (file != null) ? taggedFiles.IndexOf(file) : -1;
                        }


                        foreach (GridDataItem draggedItem in e.DraggedItems)
                        {
                            GetFileData tmpFile = DataMgr.FileDataMapper.GetFullFile(Convert.ToInt32(draggedItem.GetDataKeyValue("FID").ToString()));

                            if (tmpFile != null)
                            {
                                if (destinationIndex > -1)
                                {
                                    if (e.DropPosition == GridItemDropPosition.Below)
                                    {
                                        destinationIndex += 1;
                                    }
                                    untaggedFiles.Insert(destinationIndex, tmpFile);
                                }
                                else
                                {
                                    //untag file
                                    if (Convert.ToInt32(ddlTaggerEquipment.SelectedValue) == -1)
                                    {
                                        DataMgr.FileDataMapper.UntagFileFromBuilding(tmpFile.FID, Convert.ToInt32(ddlTaggerBuilding.SelectedValue));
                                    }
                                    else
                                    {
                                        DataMgr.FileDataMapper.UntagFileFromEquipment(tmpFile.FID, Convert.ToInt32(ddlTaggerEquipment.SelectedValue));
                                    }
                                    untaggedFiles.Add(tmpFile);
                                }

                                taggedFiles.Remove(taggedFiles.Where(f => f.FID.Equals(tmpFile.FID)).First());
                            }
                        }

                        TaggedFiles = taggedFiles;
                        UntaggedFiles = untaggedFiles;
                        grdUntaggedFiles.Rebind();
                        grdTaggedFiles.Rebind();                         
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        protected void BindTaggerGrids()
        {
            grdTaggedFiles.DataSource = GetTaggedFiles();
            grdUntaggedFiles.DataSource = GetUntaggedFiles();
            grdTaggedFiles.DataBind();
            grdUntaggedFiles.DataBind();
        }

        protected void ClearTaggerDropdownSelections()
        {
            ddlTaggerBuilding.SelectedValue = "-1";
            ddlTaggerEquipment.SelectedValue = "-1";
            ddlTaggerFileClass.SelectedValue = "-1";
            ddlTaggerFileType.SelectedValue = "-1";
        }

        #endregion

        #region Helper Methods  

        #endregion

        #region Classes

            /// <summary>
            /// Documents File data
            /// </summary>
            public class FileData
            {
                public string DisplayName
                {
                    get;
                    set;
                }
                public string Extension
                {
                    get;
                    set;
                }
                public DateTime DateModified
                {
                    get;
                    set;
                }
            }

        #endregion

        #region IEQUALITY COMPARERS

            public class FileComparer : IEqualityComparer<GetFileData>
            {
                public bool Equals(GetFileData x, GetFileData y)
                {
                    return x.FID == y.FID;
                }

                public int GetHashCode(GetFileData obj)
                {
                    return obj.CID.GetHashCode();
                }
            }

        #endregion
    }
}

