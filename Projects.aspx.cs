﻿using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Data.Helpers;
using CW.Data.Models.AnalysisEquipment;
using CW.Data.Models.Projects;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using CW.Website._projects;
using CW.Website.entity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class Projects : SiteModulePage
    {
        #region Properties

        private Project mProject;
        private DropDownSequencer dropDownSequence;
        private ClientEntity clientEntity;
        private LinkHelper.ViewByMode currentMode;

        const string addProjectSuccess = " project addition was successful.";
        const string addProjectFailed = "Adding project failed. Check manual numerical input culture format. Please contact an administrator.";
        const string equipmentAnalysesRequired = "One or more equipment analyses are required for a project.";
        const string equipmentAnalysesCulture = " However, all selected equipment analyses must be from buildings that use the same currency as well as your seleced currency type to project savings and payback.";
        const string updateSuccessful = "Project update was successful.";
        const string updateFailed = "Project update failed. Check manual numerical input culture format. Please contact an administrator.";
        const string deleteSuccessful = "Project deletion was successful.";
        const string deleteFailed = "Project deletion failed. Please contact an administrator.";
        const string initialSortDirection = "ASC";
        const string initialSortExpression = "ProjectName";

        //gets and sets the gridview viewstate sort direction for the dataview
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? string.Empty; }
            set { ViewState["SortDirection"] = value; }
        }

        //gets and sets the gridview viewstate sort expression for the dataview
        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        private ProjectsBinder.BindingParameters bindingParameters
        {
            get { return viewState.Get<ProjectsBinder.BindingParameters>("BindingParameters"); }
            set { viewState.Set("BindingParameters", value); }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //logged in security check in master page

            //hide labels
            lblEditError.Visible = false;
            lblAddError.Visible = false;

            //datamanger in sitepage

            //logged in security check in master page

            dropDownSequence = new DropDownSequencer(new[] { ddlBuildings, ddlEquipmentClasses, ddlEquipment, ddlAnalysis });

            clientEntity = new ClientEntity(siteUser.CID, String.Empty);            
        }

        protected void Page_FirstLoad(Object sender, EventArgs e)
        {
            //clear the viewstate and set the initial viewstate settings
            ViewState.Clear();
            GridViewSortDirection = initialSortDirection;
            GridViewSortExpression = initialSortExpression;

            sessionState["Search"] = String.Empty;

            BindBuildingDropdownList();
            dropDownSequence.ResetSubDropDownsOf(ddlBuildings);

            BindProjectStatusDropdownList(ddlProjectStatus, ddlAddProjectStatus, ddlEditProjectStatus);
            BindUsersDropdownList(ddlEditInternalUserAssigned, ddlAddInternalUserAssigned, ddlEditOwner, ddlAddOwner);
            BindEquipmentAnalysesListBoxes();
            BindCurrencies(ddlEditCurrency, ddlAddCurrency);

            if (Request.QueryString.HasKeys())
            {
                ProcessRequestWithQueryString();
            }
            else
            {
                ProcessRequest();
            }

            rblViewBy.SelectedValue = currentMode.ToString();

            VisibilityHelper(currentMode);

            //set admin global settings
            //Make sure to decode from ascii to html 
            GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
            litProjectsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.ProjectsBody);

            //set validator datetime formats
            //dateEarlyCompareValidator.ValueToCompare = new DateTime(2010, 1, 1).ToShortDateString();
        }

        protected void Page_AsyncPostbackLoad(Object sender, EventArgs e)
        {
            currentMode = EnumerableHelper.ParseEnum<LinkHelper.ViewByMode>(rblViewBy.SelectedValue);
        }

        #endregion

        #region Set Fields and Data

        protected void SetProjectIntoEditForm(GetFullGroupedProjectAssociationsData project)
        {
            //ID
            hdnEditID.Value = Convert.ToString(project.ProjectID);
            //Project Name
            txtEditProjectName.Text = project.ProjectName;
            //Description
            txtEditDescription.Value = project.Description;
            //Reference Info
            txtEditReferenceInfo.Value = project.ReferenceInfo;
            //Culture
            ddlEditCurrency.SelectedValue = Convert.ToString(project.LCID);
            //Estimated Project Cost
            txtEditEstimatedProjectCost.Text = hdnEditEstimatedProjectCost.Value = CultureHelper.FormatNumberFromInvariant(project.LCID, project.EstimatedProjectCost);               
            
            //Projected Savings        
            txtEditProjectedSavings.Text = CultureHelper.FormatNumberFromInvariant(project.LCID, project.ProjectedSavings);
            //Payback
            txtEditPayback.Text = CultureHelper.FormatNumberFromInvariant(project.Payback);

            //Project Owner
            if (project.ProjectOwnerID != null)
            {
                //try to set user in dropdown, but that provider or client might not have that user... else just set to label.
                try
                {
                    ddlEditOwner.SelectedValue = project.ProjectOwnerID.ToString();
                    ddlEditOwner.Visible = true;
                    lblEditOwner.Visible = false;
                }
                catch
                {
                    lblEditOwner.Text = project.ProjectOwnerFullName;
                    ddlEditOwner.Visible = false;
                    lblEditOwner.Visible = true;
                }
            }    

            //Internal Project Assigned
            if (project.InternalProjectAssignedID != null)
            {
                //try to set user in dropdown, but that provider or client might not have that user... else just set to label.
                try
                {
                    ddlEditInternalUserAssigned.SelectedValue = project.InternalProjectAssignedID.ToString();
                    ddlEditInternalUserAssigned.Visible = true;
                    lblEditInternalUserAssigned.Visible = false;
                }
                catch
                {
                    lblEditInternalUserAssigned.Text = project.InternalProjectAssignedFullName;
                    ddlEditInternalUserAssigned.Visible = false;
                    lblEditInternalUserAssigned.Visible = true;
                }
            }

            //External Project Assigned
            txtEditExternalUserAssigned.Text = project.ExternalProjectAssigned;
            //Target StartDate
            txtEditTargetStartDate.SelectedDate = project.TargetStartDate;
            //Target Completion Date
            txtEditTargetCompletionDate.SelectedDate = project.TargetCompletionDate;

            //Project Status
            ddlEditProjectStatus.SelectedValue = project.ProjectStatusID.ToString();
            //Project Priority
            ddlEditProjectPriority.SelectedValue = project.ProjectPriority;

            //Date Created
            //hdnEditDateCreated.Value = project.DateCreated.ToString();
        }

        protected void SetNewProjectIntoAddForm()
        {
            ControlHelper.DropDownListControl.SelectItem(ddlAddCurrency, bindingParameters.lcid ?? BusinessConstants.Culture.DefaultCultureLCID);

            txtAddTargetStartDate.SelectedDate = bindingParameters.startFrom;

            BuildingSetting bs = DataMgr.BuildingDataMapper.GetBuildingSettingsByBID(bindingParameters.bids.First());

            //TODO: maybe pass aeid from diagnostics grid to avoid this query
            Analyses_Equipment ae = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByEIDAndAID((int)bindingParameters.equipmentID, (int)bindingParameters.analysisID);

            ListItem item = lbAddTop.Items.FindByValue(ae.AEID.ToString());

            if (item != null)
            {
                lbAddTop.Items.Remove(item);
                lbAddBottom.Items.Add(item);
            }
        }

        protected void SetEditedValues(Project project)
        {
            hdnEditEstimatedProjectCost.Value = txtEditEstimatedProjectCost.Text;
            hdnEditCurrentAEIDs.Value = String.Join(",", lbEditBottom.Items.Cast<ListItem>().ToArray().Select(ae => ae.Value).ToArray());

            //Projected Savings        
            txtEditProjectedSavings.Text = CultureHelper.FormatNumberFromInvariant(project.LCID, project.ProjectedSavings);
            //Payback
            txtEditPayback.Text = CultureHelper.FormatNumberFromInvariant(project.Payback);

            //reset checkboxes
            chkEditProjectedSavings.Checked = chkEditPayback.Checked = false;
        }

        #endregion

        #region Load and Bind Fields

        private void BindBuildingDropdownList()
        {
            //TEMP: remove view all on buildings because we need a buidlings timezone.
            //we would need to pass a list of building instead into the queries and then do a foreach to get each timezone.
            DropDownSequencer.ClearAndCreateDefaultItems(ddlBuildings, true, true, false);

            ddlBuildings.DataTextField = "BuildingName";
            ddlBuildings.DataValueField = "BID";

            
            //get all buildings by client id. kgs and provider will have selected client.  get all regardless of visible or active, as they once may have been visible or active.                   
            //if session clientid empty then admin
            var buildings = siteUser.VisibleBuildings;

            ddlBuildings.DataSource = buildings;
            ddlBuildings.DataBind();

            //populate address in dropdown items by added a title attribute
            int counter = 2;
            foreach (var b in buildings)
            {
                ddlBuildings.Items[counter].Attributes.Add("title", String.Format("{0}, {1}, {2} {3}", b.Address, b.City, (b.StateID != null ? b.State.StateName : ""), b.Zip));
                counter++;
            }
        }

        private void BindEquipmentClassesDropDownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipmentClasses, true, true, false);

            ddlEquipmentClasses.DataTextField = "EquipmentClassName";
            ddlEquipmentClasses.DataValueField = "EquipmentClassID";

            ddlEquipmentClasses.DataSource =
            (
                (ddlBuildings.SelectedValue == "0") ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses() : DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(Int32.Parse(ddlBuildings.SelectedValue))
            );

            ddlEquipmentClasses.DataBind();
        }

        private void BindEquipmentDropdownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipment, true, true, false);

            ddlEquipment.DataTextField = "EquipmentName";
            ddlEquipment.DataValueField = "EID";

            //
            //get all visible equipment, regardless of its active state because we stll need to plot if it was once active.

            var bid = ((ddlBuildings.SelectedValue == "0") ? null : new Int32?(Int32.Parse(ddlBuildings.SelectedValue)));
            var classID = ((ddlEquipmentClasses.SelectedValue == "0") ? null : new Int32?(Int32.Parse(ddlEquipmentClasses.SelectedValue)));

            var equipment = QueryManager.Equipment.LoadWith<EquipmentType>(_ => _.EquipmentType, _ => _.EquipmentClass).FinalizeLoadWith.GetVisibleOnly().ByBuilding(bid).ByClass(classID).ToList();

            ddlEquipment.DataSource = equipment;

            ddlEquipment.DataBind();

            //
            //populate class and type in dropdown items by adding a title attribute

            var counter = 2;

            foreach (var e in equipment)
            {
                ddlEquipment.Items[counter].Attributes.Add("title", String.Format("{0}, {1}", e.EquipmentType.EquipmentClass.EquipmentClassName, e.EquipmentType.EquipmentTypeName));

                counter++;
            }
        }

        private void BindAnalysisDropdownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlAnalysis, true, true, false);

            ddlAnalysis.DataTextField = "AnalysisName";
            ddlAnalysis.DataValueField = "AID";

            //
            //get all visible analyses, regardless of its active state because we stll need to plot if it was once active.

            IEnumerable<Analyse> analyses = Enumerable.Empty<Analyse>();

            if (ddlBuildings.SelectedValue == "0" && ddlEquipmentClasses.SelectedValue == "0" && ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByCID(siteUser.CID);
            }
            else if (ddlBuildings.SelectedValue == "0" && ddlEquipmentClasses.SelectedValue != "0" && ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByCIDAndEquipmentClassID(siteUser.CID, Int32.Parse(ddlEquipmentClasses.SelectedValue));
            }
            else if (ddlEquipmentClasses.SelectedValue == "0" && ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByBID(Int32.Parse(ddlBuildings.SelectedValue));
            }
            else if (ddlEquipmentClasses.SelectedValue != "0" && ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByBIDAndEquipmentClassID(Int32.Parse(ddlBuildings.SelectedValue), Int32.Parse(ddlEquipmentClasses.SelectedValue));
            }
            else
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByEID(Int32.Parse(ddlEquipment.SelectedValue));
            }

            ddlAnalysis.DataSource = analyses;

            ddlAnalysis.DataBind();

            //populate analysis teaser in dropdown items by added a title attribute
            int counter = 2;
            foreach (Analyse a in analyses)
            {
                ddlAnalysis.Items[counter].Attributes.Add("title", a.AnalysisTeaser);
                counter++;
            }
        }

        private void BindEquipmentAnalysesListBoxes()
        {
            //get a_e by client ID
            var analysesEquipment = DataMgr.AnalysisEquipmentDataMapper.GetAllAnalysesEquipmentByCIDWithDropdownFormat(siteUser.CID);

            //bind all a_e for client on add tab
            lbAddTop.DataSource = analysesEquipment;
            lbAddTop.DataTextField = "AnalysisEquipmentConcatinated";
            lbAddTop.DataValueField = "AEID";
            lbAddTop.DataBind();
        }

        private void BindProjectStatusDropdownList(DropDownList ddl1, DropDownList ddl2, DropDownList ddl3)
        {
            IList<Status> projectStatus = DataMgr.ProjectDataMapper.GetAllProjectStatus();

            ddl1.DataTextField = "StatusName";
            ddl1.DataValueField = "StatusID";
            ddl1.DataSource = projectStatus;
            ddl1.DataBind();

            ddl2.DataTextField = "StatusName";
            ddl2.DataValueField = "StatusID";
            ddl2.DataSource = projectStatus;
            ddl2.SelectedValue = "2";
            ddl2.DataBind();

            ddl3.DataTextField = "StatusName";
            ddl3.DataValueField = "StatusID";
            ddl3.DataSource = projectStatus;
            ddl3.DataBind();
        }

        private void BindUsersDropdownList(DropDownList ddl1, DropDownList ddl2, DropDownList ddl3, DropDownList ddl4)
        {            
            var restrictedUserRoleID = Convert.ToInt32(BusinessConstants.UserRole.UserRoleEnum.RestrictedUser);
            var users = siteUser.IsLoggedInUnderProviderClient ? DataMgr.UserDataMapper.GetAllUsersWithMinimumRoleByProvider(siteUser.CID, siteUser.ProviderID.Value, restrictedUserRoleID).ToList() : DataMgr.UserDataMapper.GetUsersClientsUsersWithMinimumRole(siteUser.CID, restrictedUserRoleID);
            users = users.OrderBy(e => e.Email);

            ddl1.DataTextField = "Email";
            ddl1.DataValueField = "UID";
            ddl1.DataSource = users;
            ddl1.DataBind();

            ddl2.DataTextField = "Email";
            ddl2.DataValueField = "UID";
            ddl2.DataSource = users; 
            ddl2.DataBind();

            ddl3.DataTextField = "Email";
            ddl3.DataValueField = "UID";
            ddl3.DataSource = users;
            ddl3.DataBind();

            ddl4.DataTextField = "Email";
            ddl4.DataValueField = "UID";
            ddl4.DataSource = users;
            ddl4.DataBind();

            try
            {
                //try to set initial selected user as current user. kgs users wont be in the list.
                ddl4.SelectedValue = siteUser.UID.ToString();
            }
            catch
            {
            }
        }

        private void BindCurrencies(DropDownList ddl1, DropDownList ddl2)
        {
            List<ListItem> listItems = new List<ListItem>();

            foreach (CultureInfo item in CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.LCID))
            {
                //if (item.IsNeutralCulture != true)
                //{
                    RegionInfo region = new RegionInfo(item.LCID);
                    string CurrencyName = region.CurrencyEnglishName;
                    string CurrenctSymbol = region.ISOCurrencySymbol;
                    ListItem li = new ListItem(CurrencyName + " (" + CurrenctSymbol + ")", item.LCID.ToString());
                    
                    //check whether the currency has already been added to the list or not
                    if (!listItems.Where(i => i.Text == li.Text).Any())
                        listItems.Add(li);
                //}
            }

            //sort the dropdownlist items alphabatically
            ddl1.DataSource = ddl2.DataSource = listItems.OrderBy(item => item.Text);
            ddl1.DataTextField = ddl2.DataTextField = "Text";
            ddl1.DataValueField = ddl2.DataValueField = "Value";
            ddl1.DataBind();
            ddl2.DataBind();

            //select for add ddl
            ddl2.SelectedValue = BusinessConstants.Culture.DefaultCultureLCID.ToString();
        }

        #endregion

        #region Load Projects

        protected void LoadAddFormIntoProject(Project project, bool cultureCheck)
        {
            int lcid = Convert.ToInt32(ddlAddCurrency.SelectedValue);

            //Project Name
            project.ProjectName = txtAddProjectName.Text;
            //Project Description
            project.Description = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
            //Project ReferenceInfo
            project.ReferenceInfo = String.IsNullOrEmpty(txtAddReferenceInfo.Value) ? null : txtAddReferenceInfo.Value;
            //Culture
            project.LCID = lcid;
            //Estimaged Project Cost
            project.EstimatedProjectCost = String.IsNullOrEmpty(txtAddEstimatedProjectCost.Text) ? "" : CultureHelper.FormatNumberToInvariantString(txtAddEstimatedProjectCost.Text, lcid);
            //Project Owner
            project.ProjectOwnerID = ddlAddOwner.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddOwner.SelectedValue) : null;
            //Internal Project Assigned
            project.ProjectAssignedID = ddlAddInternalUserAssigned.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddInternalUserAssigned.SelectedValue) : null;
            //External Project Assigned
            project.ProjectAssigned = txtAddExternalUserAssigned.Text;
            //Target Start Date
            project.TargetStartDate = txtAddTargetStartDate.SelectedDate;
            //Target End Date
            project.TargetCompletionDate = txtAddTargetCompletionDate.SelectedDate;
            //Project Status
            project.ProjectStatusID = Convert.ToInt32(ddlAddProjectStatus.SelectedValue);
            //Project Priority
            project.ProjectPriority = ddlAddProjectPriority.Text;

            project.DateModified = DateTime.UtcNow;

            //recalculate
            if (cultureCheck)
            {
                //Projected Savings
                if(!chkAddProjectedSavings.Checked)
                    project.ProjectedSavings = String.IsNullOrEmpty(txtAddProjectedSavings.Text) ? "" : CultureHelper.FormatNumberToInvariantString(txtAddProjectedSavings.Text, lcid);
                //Payback
                if(!chkAddPayback.Checked)
                    project.Payback = String.IsNullOrEmpty(txtAddPayback.Text) ? "" : CultureHelper.FormatNumberToInvariantString(txtAddPayback.Text);

                if (chkAddProjectedSavings.Checked || chkAddPayback.Checked)
                {
                    //caclculate savigns and payback. pass in project values just set.
                    string[] savingsAndPayback = CalculateSavingsAndPayback(project, lbAddBottom.Items, (DateTime)project.TargetStartDate);

                    //Projected Savings
                    project.ProjectedSavings = savingsAndPayback[0];
                    //Payback
                    project.Payback = savingsAndPayback[1];
                }
            }
            else if (!cultureCheck)
            {
                project.ProjectedSavings = project.Payback = "";
            }
        }

        protected void LoadEditFormIntoProject(Project project, bool cultureCheck, bool analysisEquipmentAssociationsChanged)
        {
            int lcid = Convert.ToInt32(ddlEditCurrency.SelectedValue);

            //ID
            project.ProjectID = Convert.ToInt32(hdnEditID.Value);
            //Project Name
            project.ProjectName = txtEditProjectName.Text;
            //Project Description
            project.Description = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
            //Project ReferenceInfo
            project.ReferenceInfo = String.IsNullOrEmpty(txtEditReferenceInfo.Value) ? null : txtEditReferenceInfo.Value;
            //Project Culture
            project.LCID = lcid;
            //Estimaged Project Cost
            project.EstimatedProjectCost = String.IsNullOrEmpty(txtEditEstimatedProjectCost.Text) ? "" : CultureHelper.FormatNumberToInvariantString(txtEditEstimatedProjectCost.Text, lcid);
            //Project Owner
            project.ProjectOwnerID = ddlEditOwner.SelectedValue != "-1" && ddlEditOwner.Visible ? (int?)Convert.ToInt32(ddlEditOwner.SelectedValue) : null;
            //Internal Project Assigned
            project.ProjectAssignedID = ddlEditInternalUserAssigned.SelectedValue != "-1" && ddlEditInternalUserAssigned.Visible ? (int?)Convert.ToInt32(ddlEditInternalUserAssigned.SelectedValue) : null;
            //External Project Assigned
            project.ProjectAssigned = txtEditExternalUserAssigned.Text;
            //Target Start Date
            project.TargetStartDate = txtEditTargetStartDate.SelectedDate;
            //Target End Date
            project.TargetCompletionDate = txtEditTargetCompletionDate.SelectedDate;

            //Project Status
            project.ProjectStatusID = Convert.ToInt32(ddlEditProjectStatus.SelectedValue);
            //Project Priority
            project.ProjectPriority = ddlEditProjectPriority.Text;

            //recalculate
            if (cultureCheck)
            {
                //Projected Savings
                if (!chkEditProjectedSavings.Checked)
                    project.ProjectedSavings = String.IsNullOrEmpty(txtEditProjectedSavings.Text) ? "" : CultureHelper.FormatNumberToInvariantString(txtEditProjectedSavings.Text, lcid);
                //Payback
                if (!chkEditPayback.Checked)
                    project.Payback = String.IsNullOrEmpty(txtEditPayback.Text) ? "" : CultureHelper.FormatNumberToInvariantString(txtEditPayback.Text);

                if (chkEditProjectedSavings.Checked || chkEditPayback.Checked)
                {
                    //caclculate savigns and payback. pass in project values just set.
                    string[] savingsAndPayback = CalculateSavingsAndPayback(project, lbEditBottom.Items, (DateTime)project.TargetStartDate);

                    //Projected Savings
                    project.ProjectedSavings = savingsAndPayback[0];
                    //Payback
                    project.Payback = savingsAndPayback[1];
                }
            }
            else if (!cultureCheck)
            {
                project.ProjectedSavings = project.Payback = "";
            }
        }

        #endregion

        #region Dropdown and Checkbox Events

        protected void ddlBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequence.ResetSubDropDownsOf(ddlBuildings);

            if (ddlBuildings.SelectedIndex == -1) return;

            BindEquipmentClassesDropDownList();
        }

        protected void ddlEquipmentClasses_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequence.ResetSubDropDownsOf(ddlEquipmentClasses);

            if (ddlEquipmentClasses.SelectedIndex == -1) return;

            BindEquipmentDropdownList();
        }

        protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequence.ResetSubDropDownsOf(ddlEquipment);

            if (ddlEquipment.SelectedIndex == -1) return;

            BindAnalysisDropdownList();
        }

        protected void rblViewBy_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            VisibilityHelper(EnumerableHelper.ParseEnum<LinkHelper.ViewByMode>(rblViewBy.SelectedValue));
        }

        #endregion

        #region Button Events

        /// <summary>
        /// on add button up click, remove analyses equipment from project
        /// </summary>
        protected void btnAddEquipmentAnalysesUpButton_Click(object sender, EventArgs e)
        {
            while (lbAddBottom.SelectedIndex != -1)
            {
                lbAddTop.Items.Add(lbAddBottom.SelectedItem);
                lbAddBottom.Items.Remove(lbAddBottom.SelectedItem);
            }
        }

        /// <summary>
        /// on add button down click, add analyses equipment in project
        /// </summary>
        protected void btnAddEquipmentAnalysesDownButton_Click(object sender, EventArgs e)
        {
            while (lbAddTop.SelectedIndex != -1)
            {
                {
                    lbAddBottom.Items.Add(lbAddTop.SelectedItem);
                    lbAddTop.Items.Remove(lbAddTop.SelectedItem);
                }
            }
        }

        /// <summary>
        /// on edit button down click, add buildings not in group
        /// </summary>
        protected void btnEditEquipmentAnalysesDownButton_Click(object sender, EventArgs e)
        {
            //ListBox tempTopListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbTop");
            //ListBox tempBottomListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbBottom");

            while (lbEditTop.SelectedIndex != -1)
            {
                {
                    lbEditBottom.Items.Add(lbEditTop.SelectedItem);
                    lbEditTop.Items.Remove(lbEditTop.SelectedItem);
                }
            }
        }

        /// <summary>
        /// on edit button up click, add buildings not in group
        /// </summary>
        protected void btnEditEquipmentAnalysesUpButton_Click(object sender, EventArgs e)
        {
            //ListBox tempTopListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbTop");
            //ListBox tempBottomListBox = (ListBox)gridBuildingGroups.Rows[gridBuildingGroups.EditIndex].Cells[2].FindControl("lbBottom");

            while (lbEditBottom.SelectedIndex != -1)
            {
                lbEditTop.Items.Add(lbEditBottom.SelectedItem);
                lbEditBottom.Items.Remove(lbEditBottom.SelectedItem);
            }
        }

        /// <summary>
        /// Add project Button on click.
        /// </summary>
        protected void addProjectButton_Click(object sender, EventArgs e)
        {
            int projectID;
            bool cultureCheck = false;

            mProject = new Project();

            try
            {
                if (lbAddBottom.Items.Count > 0)
                {
                    cultureCheck = CultureCurrencyCheck(lbAddBottom.Items, Convert.ToInt32(ddlAddCurrency.SelectedValue));

                    //load the form into the project
                    LoadAddFormIntoProject(mProject, cultureCheck);

                    //insert new project
                    projectID = DataMgr.ProjectDataMapper.InsertProject(mProject);

                    try
                    {
                        //addes each projectassociation 
                        //no need to check if they already exists since its the first addition

                        foreach (ListItem item in lbAddBottom.Items)
                        {
                            //declare new project association
                            ProjectAssociation mProjectAssociation = new ProjectAssociation();

                            //TODO: figure out way to store these ids so we can eliminate this query
                            //get ids
                            Analyses_Equipment ae = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByAEID(Convert.ToInt32(item.Value));

                            //set values                                
                            mProjectAssociation.CID = siteUser.CID;
                            mProjectAssociation.BID = ae.Equipment.BID;
                            mProjectAssociation.EID = ae.EID;
                            mProjectAssociation.AID = ae.AID;
                            mProjectAssociation.AEID = ae.AEID;
                            mProjectAssociation.ProjectID = projectID;

                            //insert
                            DataMgr.ProjectDataMapper.InsertProjectAssociation(mProjectAssociation);
                        }

                        lblAddError.Text = mProject.ProjectName + addProjectSuccess + (cultureCheck ? "" : equipmentAnalysesCulture);
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding project assocations for projects module.", sqlEx);
                        lblAddError.Text = addProjectFailed;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding project assocations for projects module.", ex);
                        lblAddError.Text = addProjectFailed;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                }
                else
                {
                    lblAddError.Text = equipmentAnalysesRequired;
                    lblAddError.Visible = true;
                    lnkSetFocusAdd.Focus();
                }
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding project.", sqlEx);
                lblAddError.Text = addProjectFailed;
                lblAddError.Visible = true;
                lnkSetFocusAdd.Focus();

            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding project.", ex);
                lblAddError.Text = addProjectFailed;
                lblAddError.Visible = true;
                lnkSetFocusAdd.Focus();
            }
        }

        /// <summary>
        /// Update Proejct Button on click.
        /// </summary>
        protected void updateProjectButton_Click(object sender, EventArgs e)
        {
            //create and load the form into the project
            Project mProject = new Project();
            bool cultureCheck = false;

            //try to update the project       
            try
            {
                if (lbEditBottom.Items.Count > 0)
                {
                    cultureCheck = CultureCurrencyCheck(lbEditBottom.Items, Convert.ToInt32(ddlEditCurrency.SelectedValue));

                    bool analysisEquipmentAssociationsChanged = DidAnalysisEquipmentAssociationsChange(lbEditBottom.Items, hdnEditCurrentAEIDs.Value);

                    LoadEditFormIntoProject(mProject, cultureCheck, analysisEquipmentAssociationsChanged);

                    //update project
                    DataMgr.ProjectDataMapper.UpdateProject(mProject);

                    if (analysisEquipmentAssociationsChanged)
                    {
                        //deletes all project associations
                        DataMgr.ProjectDataMapper.DeleteAllProjectAssociationsByProjectID(mProject.ProjectID);

                        try
                        {
                            //inserting each project association id in the bottom listbox
                            foreach (ListItem item in lbEditBottom.Items)
                            {
                                //declare new project association and 
                                ProjectAssociation mProjectAssociation = new ProjectAssociation();

                                //TODO: figure out way to store these ids so we can eliminate this query
                                //get ids
                                Analyses_Equipment ae = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByAEID(Convert.ToInt32(item.Value));

                                //set values                                
                                mProjectAssociation.CID = siteUser.CID;
                                mProjectAssociation.BID = ae.Equipment.BID;
                                mProjectAssociation.EID = ae.EID;
                                mProjectAssociation.AID = ae.AID;
                                mProjectAssociation.AEID = ae.AEID;
                                mProjectAssociation.ProjectID = mProject.ProjectID;

                                //insert
                                DataMgr.ProjectDataMapper.InsertProjectAssociation(mProjectAssociation);
                            }                        
                        }
                        catch (SqlException sqlEx)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating project associations for projects module.", sqlEx);
                            lblEditError.Text = updateFailed;
                            lblEditError.Visible = true;
                            lnkSetFocusEdit.Focus();

                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating project associations for projects module.", ex);
                            lblEditError.Text = updateFailed;
                            lblEditError.Visible = true;
                            lnkSetFocusEdit.Focus();
                        }
                    }

                    //Set hdnvalues and calculated values
                    SetEditedValues(mProject);

                    //Bind projects again
                    BindProjects();

                    lblEditError.Text = updateSuccessful + (cultureCheck ? "" : equipmentAnalysesCulture);
                    lblEditError.Visible = true;
                    lnkSetFocusEdit.Focus();
                }
                else
                {
                    lblEditError.Text = equipmentAnalysesRequired;
                    lblEditError.Visible = true;
                    lnkSetFocusEdit.Focus();
                }
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding project.", sqlEx);
                lblEditError.Text = updateFailed;
                lblEditError.Visible = true;
                lnkSetFocusEdit.Focus();

            }
            catch (Exception ex)
            {
                lblEditError.Text = updateFailed;
                lblEditError.Visible = true;
                lnkSetFocusEdit.Focus();

                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating project.", ex);
            }
        }

        protected void generateButton_Click(Object sender, EventArgs e)
        {
            //clear existing gridviewsortexpression as its by the combination of comfort,energy,maintenance,and cost savings. 
            //gridViewSortExpression = String.Empty;

            bindingParameters =
            new ProjectsBinder.BindingParameters
            {
                cid = clientEntity.ID,
                bids = ProjectsBinder.BindingParameters.ToInt(ddlBuildings.SelectedValue) != null ? new List<int>(new int[] { Convert.ToInt32(ddlBuildings.SelectedValue) }) : siteUser.VisibleBuildings.Select(b => b.BID).ToList(),
                equipmentClassID = ProjectsBinder.BindingParameters.ToInt(ddlEquipmentClasses.SelectedValue),
                equipmentID = ProjectsBinder.BindingParameters.ToInt(ddlEquipment.SelectedValue),
                analysisID = ProjectsBinder.BindingParameters.ToInt(ddlAnalysis.SelectedValue),
                startFrom = ((DateTime)txtStartDateFrom.SelectedDate).AddHours(-siteUser.UserTimeZoneOffset),
                startTo = ((DateTime)txtStartDateTo.SelectedDate).AddHours(24).AddHours(-siteUser.UserTimeZoneOffset),
                projectStatusID = ProjectsBinder.BindingParameters.ToInt(ddlProjectStatus.SelectedValue),
                projectPriority = ProjectsBinder.BindingParameters.ToString(ddlProjectPriority.SelectedValue),
                filter = txtTextFilter.Text,
            };

            BindProjects();
        }

        #endregion

        #region Tab Events

        /// <summary>
        /// Tab changed event, bind building group grid after tab changed back from add user tab
        /// </summary>
        protected void onTabClick(object sender, EventArgs e)
        {
            //if active tab index is 0, bind projects to grid
            if (radTabStrip.MultiPage.SelectedIndex == 0)
            {
                BindProjects();
            }
        }

        #endregion

        #region Grid Events

        protected void gridProjects_OnDataBound(object sender, EventArgs e)
        {
            //GridViewRowCollection rows = gridProjects.Rows;

            ////no need to hide links if user role is is not super admin or higher on this page

            //foreach (GridViewRow row in rows)
            //{
            //    //TODO: for bildings, equipment, and analyses

            //    //Bind buildings for project
            //    IEnumerable<CW.Data.Building> bldgs = mDataManager.BuildingDataMapper.GetAllBuildingsByProjectID(Convert.ToInt32(gridProjects.DataKeys[row.RowIndex].Value));
                
            //    //if not edit bind to literal
            //    if (((row.RowState & DataControlRowState.Edit) == 0) && bldgs.Any())
            //    {
            //        StringBuilder s = new StringBuilder();
            //        s.Append("<ul>");

            //        //Optimized loop                         
            //        using (IEnumerator<CW.Data.Building> list = bldgs.GetEnumerator())
            //        {
            //            while (list.MoveNext())
            //            {
            //                CW.Data.Building bldg = (CW.Data.Building)list.Current;
            //                s.Append("<li>" + bldg.BuildingName + "</li>");
            //            }
            //        }

            //        s.Append("</ul>");

            //        //Add list of buidings to literal
            //        try
            //        {
            //            ((Literal)row.Cells[3].Controls[0]).Text = s.ToString();
            //        }
            //        catch
            //        {
            //        }
            //    }            
            //}
        }

        protected void gridProjects_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            pnlEditProject.Visible = false;
            dtvProject.Visible = true;

            int projectID = Convert.ToInt32(gridProjects.DataKeys[gridProjects.SelectedIndex].Values["ProjectID"]);

            PopulateProjectInfo(projectID);
        }

        protected void gridProjects_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                //Note: The autogenerated pager table is not shown with just one page.

                Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                tblPager.CssClass = "pageTable";

                TableRow theRow = tblPager.Rows[0];

                LinkButton ctrlPrevious = new LinkButton();
                ctrlPrevious.CommandArgument = "Prev";
                ctrlPrevious.CommandName = "Page";
                ctrlPrevious.Text = "« Previous Page";
                ctrlPrevious.CssClass = "pageLink";

                TableCell cellPreviousPage = new TableCell();
                cellPreviousPage.Controls.Add(ctrlPrevious);
                tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                LinkButton ctrlNext = new LinkButton();
                ctrlNext.CommandArgument = "Next";
                ctrlNext.CommandName = "Page";
                ctrlNext.Text = "Next Page »";
                ctrlNext.CssClass = "pageLink";


                TableCell cellNextPage = new TableCell();
                cellNextPage.Controls.Add(ctrlNext);
                tblPager.Rows[0].Cells.Add(cellNextPage);
            }
        }

        protected void gridProjects_Editing(object sender, GridViewEditEventArgs e)
        {
            dtvProject.Visible = false;
            pnlEditProject.Visible = true;

            IOrderedDictionary dka = gridProjects.DataKeys[e.NewEditIndex].Values;

            int projectID = Convert.ToInt32(dka["ProjectID"]);
            int cid = Convert.ToInt32(dka["CID"]);

            //Get single group project
            GetFullGroupedProjectAssociationsData mProject = DataMgr.ProjectDataMapper.GetFullGroupedProjectAssociationsByProjectID(projectID);

            //Set project data
            SetProjectIntoEditForm(mProject);

            //clear listboxes just incase no a_e to bind
            lbEditBottom.Items.Clear();
            lbEditTop.Items.Clear();

            if (mProject.AnalysisEquipments.Any())
            {
                //bind a_e in project to bottom listbox
                lbEditBottom.DataSource = mProject.AnalysisEquipments;
                lbEditBottom.DataTextField = "AnalysisEquipmentConcatinated";
                lbEditBottom.DataValueField = "AEID";
                lbEditBottom.DataBind();

                hdnEditCurrentAEIDs.Value = String.Join(",", mProject.AnalysisEquipments.Select(ae => ae.AEID).ToArray());
            }

            //Get unassociated project analyses_equipments
            var mAnalysesEquipments = DataMgr.AnalysisEquipmentDataMapper.GetAllAnalysesEquipmentNotAssociatedToProjectIDForClientWithDropdownFormat(projectID, cid);

            if (mAnalysesEquipments.Any())
            {
                //bind building not in a project to top listbox
                lbEditTop.DataSource = mAnalysesEquipments;
                lbEditTop.DataTextField = "AnalysisEquipmentConcatinated";
                lbEditTop.DataValueField = "AEID";
                lbEditTop.DataBind();
            }

            //Cancels the edit auto grid selected.
            e.Cancel = true;
        }

        protected void gridProjects_Deleting(object sender, GridViewDeleteEventArgs e)
        {
            //get deleting rows projectid. uses the first key in the datagrid.
            int projectID = Convert.ToInt32(gridProjects.DataKeys[e.RowIndex].Value);

            try
            {
                //delete all project associations first.
                //foreign key will restrict deleting the project first.
                DataMgr.ProjectDataMapper.DeleteAllProjectAssociationsByProjectID(projectID);
                DataMgr.ProjectDataMapper.DeleteProject(projectID);

                lblErrorTop.Text = deleteSuccessful;
            }
            catch (Exception ex)
            {
                lblErrorTop.Text = deleteFailed;
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting project.", ex);
            }

            lblErrorTop.Visible = true;
            lnkSetFocusView.Focus();

            //Bind data again keeping current page and sort mode
            DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryProjects());
            gridProjects.PageIndex = gridProjects.PageIndex;
            gridProjects.DataSource = SortDataTable(dataTable as DataTable, true);
            gridProjects.DataBind();

            SetGridCountLabel(dataTable.Rows.Count);

            //hide edit form if shown
            pnlEditProject.Visible = false;
        }

        protected void gridProjects_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedProjects(sessionState["Search"].ToString()));

            //maintain current sort direction and expresstion on paging
            gridProjects.DataSource = SortDataTable(dataTable, true);
            gridProjects.PageIndex = e.NewPageIndex;
            gridProjects.DataBind();
        }

        protected void gridProjects_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Reset the edit index.
            gridProjects.EditIndex = -1;

            DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedProjects(sessionState["Search"].ToString()));

            GridViewSortExpression = e.SortExpression;

            gridProjects.DataSource = SortDataTable(dataTable, false);
            gridProjects.DataBind();
        }

        #endregion

        #region Helper Methods

        private IEnumerable<GetGroupedProjectAssociationsData> QueryProjects()
        {
            try
            {
                //get all projects by client id                    
                return DataMgr.ProjectDataMapper.GetAllGroupedProjectAssociations(ProjectInputBuilder());
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving projects in projects module.", sqlEx);
                return null;
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving projects in projects module.", ex);
                return null;
            }
        }

        private IEnumerable<GetGroupedProjectAssociationsData> QuerySearchedProjects(string searchText)
        {
            try
            {
                //get all projects by client id                    
                return DataMgr.ProjectDataMapper.GetAllGroupedSearchedProjectAssociations(searchText, ProjectInputBuilder());
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving searched projects in projects module.", sqlEx);
                return null;
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving searched projects in projects module.", ex);
                return null;
            }
        }

        private void BindProjects()
        {
            //query projects
            IEnumerable<GetGroupedProjectAssociationsData> projects = String.IsNullOrEmpty(bindingParameters.filter) ? QueryProjects() : QuerySearchedProjects(bindingParameters.filter);

            int count = projects.Count();

            //maintain sort--- doesnt work!
            //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
            //gridUsers.Sort(GridViewSortExpression, tempSD);

            projects = BuildDiagnosticLinks(projects);

            gridProjects.DataSource = projects;

            // bind grid
            gridProjects.DataBind();

            SetGridCountLabel(count);
        }

        private void SetGridCountLabel(int count)
        {
            //check if grid empty 
            if (count == 1)
            {
                lblResults.Text = String.Format("{0} projects found.", count);
            }
            else if (count > 1)
            {
                lblResults.Text = String.Format("{0} projects found.", count);
            }
            else
            {                
                lblResults.Text = "No projects found.";
            }
        }

        /// <summary>
        /// Helper method to switch sort direction
        /// </summary>
        /// <returns>Alternated Sort Direction</returns>
        private string GetSortDirection()
        {
            //switch sorting directions
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    ViewState["SortDirection"] = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    ViewState["SortDirection"] = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        /// <summary>
        /// Sorts the data table based on view state.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="isPageIndexChanging"></param>
        /// <returns>DataView</returns>
        protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
        {
            if (dataTable != null && dataTable.Rows.Count != 0)
            {
                DataView dataView = new DataView(dataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        //maintains the currect viewstate sorting for paging
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        //reverses the sort direction on sort
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                    }
                }
                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        private void ProcessRequestWithQueryString()
        {
            var qs = queryString.ToObject<ProjectsParamsObject>();

            if (qs == null) return;

            if (qs.cid != siteUser.CID) return;

            var modes =
                new Dictionary<String, LinkHelper.ViewByMode>
                {
                    {PropHelper.G<ProjectsParamsObject>(x => x.aid),  LinkHelper.ViewByMode.Analysis},
                    {PropHelper.G<ProjectsParamsObject>(x => x.eid),  LinkHelper.ViewByMode.Equipment},
                    {PropHelper.G<ProjectsParamsObject>(x => x.ecid), LinkHelper.ViewByMode.EquipmentClass},
					{PropHelper.G<ProjectsParamsObject>(x => x.bid),  LinkHelper.ViewByMode.Building},
                };

            LinkHelper.ViewByMode? mode = null;

            foreach (var m in modes)
            {
                if (queryString.Get<Int32>(m.Key) == null)
                {
                    if (mode == null) continue;

                    return;
                }

                if (mode == null)
                {
                    mode = m.Value;
                }
            }

            if (mode == null) return;

            currentMode = mode.Value;

            bindingParameters =
            new ProjectsBinder.BindingParameters
            {                
                //createdFrom = qs.cfd, //qs.cfd.AddHours(-siteUser.TimeZoneOffset),
                //ticks not passed through querystring
                //createdTo = (qs.ctd == null && qs.cfd != null ? qs.cfd.Value.AddSeconds(1) : qs.ctd), //qs.ctd != null ? ((DateTime)qs.ctd).AddHours(-siteUser.TimeZoneOffset) : qs.cfd.AddDays(1).AddHours(-siteUser.TimeZoneOffset),
                startFrom = qs.sfd,
                startTo = (qs.std == null) ? qs.sfd.AddSeconds(1) : (DateTime)qs.std,
                cid = qs.cid,
                bids = ((qs.bid == 0) ? siteUser.VisibleBuildings.Select(b => b.BID).ToList() : new List<Int32>(new[] { qs.bid.Value })), //revise not to use 0
                equipmentClassID = qs.ecid,
                equipmentID = qs.eid,
                analysisID = qs.aid,
                lcid = qs.lcid,
            };

            if (bindingParameters.startFrom == DateTime.MinValue || bindingParameters.startTo == null)
            {
                DateTime sf, st;
                DateTimeHelper.GenerateDefaultDatesPastXMonthsEndingToday(out sf, out st, siteUser.UserTimeZoneOffset, 12);
                bindingParameters.startFrom = sf;
                bindingParameters.startTo = st;
            }  

            if (qs.tab == 2)
            {
                SetNewProjectIntoAddForm();                
                radTabStrip.SelectedIndex = radMultiPage.SelectedIndex = 1;

                ProcessRequest();
            }
            else{
                BindProjects();

                ApplyBindingParameters();
            }


            //TODO: maybe show project details if projectid is passed?

            //if has bid, and eid and aid are not null then show details
            //if (bindingParameters.bids.Any() && bindingParameters.equipmentID != null && bindingParameters.analysisID != null)
            //{
            //    PopulateProjectInfo(bindingParameters.cid, bindingParameters.bids.First(), (int)bindingParameters.equipmentID, (int)bindingParameters.analysisID, bindingParameters.startFrom, false);
            //}
        }

        private void ProcessRequest()
        {
            currentMode = LinkHelper.ViewByMode.Building;
           
            bindingParameters = new ProjectsBinder.BindingParameters
            {
                cid = siteUser.CID,
                bids = siteUser.VisibleBuildings.Select(b => b.BID).ToList(),                
            };            

            if (bindingParameters.startFrom == DateTime.MinValue || bindingParameters.startTo == null)
            {
                DateTime sf, st;
                DateTimeHelper.GenerateDefaultDatesPastXMonthsEndingToday(out sf, out st, siteUser.UserTimeZoneOffset, 12);
                bindingParameters.startFrom = sf;
                bindingParameters.startTo = st;              
            }            

            //apply to front end with user timezone dates
            ApplyBindingParameters();
                        
            //reset dates to utc for first bind
            (bindingParameters.startFrom).AddHours(-siteUser.UserTimeZoneOffset);
            (bindingParameters.startTo).AddHours(24).AddHours(-siteUser.UserTimeZoneOffset);

            BindProjects();            
        }

        private void VisibilityHelper(LinkHelper.ViewByMode mode)
        {
            switch (mode)
            {
                case LinkHelper.ViewByMode.Building:
                    {
                        pnlEquipmentClass.Visible = false;
                        equipmentClassRequiredValidator.Enabled = false;

                        pnlEquipment.Visible = false;
                        equipmentRequiredValidator.Enabled = false;

                        pnlAnalysis.Visible = false;
                        analysisRequiredValidator.Enabled = false;

                        dropDownSequence.ResetSubDropDownsOf(ddlBuildings, true);

                        break;
                    }
                case LinkHelper.ViewByMode.EquipmentClass:
                    {
                        pnlEquipmentClass.Visible = true;
                        equipmentClassRequiredValidator.Enabled = true;

                        pnlEquipment.Visible = false;
                        equipmentRequiredValidator.Enabled = false;

                        pnlAnalysis.Visible = false;
                        analysisRequiredValidator.Enabled = false;

                        dropDownSequence.ResetSubDropDownsOf(ddlEquipmentClasses, true);

                        break;
                    }
                case LinkHelper.ViewByMode.Equipment:
                    {
                        pnlEquipmentClass.Visible = true;
                        equipmentClassRequiredValidator.Enabled = true;

                        pnlEquipment.Visible = true;
                        equipmentRequiredValidator.Enabled = true;

                        pnlAnalysis.Visible = false;
                        analysisRequiredValidator.Enabled = false;

                        dropDownSequence.ResetSubDropDownsOf(ddlEquipment, true);

                        break;
                    }
                case LinkHelper.ViewByMode.Analysis:
                    {
                        pnlEquipmentClass.Visible = true;
                        equipmentClassRequiredValidator.Enabled = true;

                        pnlEquipment.Visible = true;
                        equipmentRequiredValidator.Enabled = true;

                        pnlAnalysis.Visible = true;
                        analysisRequiredValidator.Enabled = true;

                        break;
                    }
            }
        }

        private void ApplyBindingParameters()
        {
            txtStartDateFrom.SelectedDate = bindingParameters.startFrom;
            txtStartDateTo.SelectedDate = bindingParameters.startTo;

            ControlHelper.DropDownListControl.SelectItem(ddlBuildings, bindingParameters.bids.Count == 1 ? bindingParameters.bids.First().ToString() : "0");
            ControlHelper.DropDownListControl.SelectItem(ddlEquipmentClasses, bindingParameters.equipmentClassID ?? 0);
            ControlHelper.DropDownListControl.SelectItem(ddlEquipment, bindingParameters.equipmentID ?? 0);

            ddlAnalysis.SelectedValue = bindingParameters.analysisID.ToString();
        }

        protected void PopulateProjectInfo(int projectID, bool setFocus = true)
        {
            //get based on submitted range, not by selected at the moment
            //var analysisRange = bindingParameters.range;
            
            //set data source
            dtvProject.DataSource = BuildDiagnosticLinks(DataMgr.ProjectDataMapper.GetFullGroupedProjectAssociationsByProjectIDAsIEnumerable(projectID));
            //bind project to details view
            dtvProject.DataBind();
        }

        protected ProjectsInputs ProjectInputBuilder()
        {
            return new ProjectsInputs
            {
                AID = bindingParameters.analysisID,
                BID = bindingParameters.bids.Count() > 1 ? null : (int?)bindingParameters.bids.First(),
                CID = bindingParameters.cid,
                EID = bindingParameters.equipmentID,
                EquipmentClassID = bindingParameters.equipmentClassID,
                StartDateFrom = bindingParameters.startFrom,
                StartDateTo = bindingParameters.startTo,
                ProjectStatusID = bindingParameters.projectStatusID,
                ProjectPriority = bindingParameters.projectPriority,
                Filter = bindingParameters.filter
            };
        }

        /// <summary>
        /// caclcualtes projected annual cost savings and projected payback years.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="aeids"></param>
        /// <param name="startDate"></param>
        /// <returns></returns>
        private string[] CalculateSavingsAndPayback(Project project, ListItemCollection aeids, DateTime startDate)
        {
            //temp set thread as invariat culture for calculations.
            CultureHelper.SetThreadInvariant();

            //we should have already checked that all buildings for all equipmet are of the same culture as the one the use selected.
            double projectedCostSavings = 0;
            bool hasCostSavings = false;

            if (String.IsNullOrEmpty(project.ProjectedSavings))
            {
                foreach (ListItem li in aeids)
                {
                    Analyses_Equipment ae = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByAEID(Convert.ToInt32(li.Value));

                    //we use startDate because all a_e associations have no relation to any specific datetime.
                    //get past year diagnostics results for aeid
                    //IEnumerable<DiagnosticsResult> results = mDataManager.DiagnosticsDataMapper.GetDiagnosticsResults(
                    //    new DiagnosticsResultsInputs()
                    //    {
                    //        AnalysisRange = DataConstants.AnalysisRange.Monthly,
                    //        StartDate = DateTimeHelper.GetXMonthsAgo(DateTimeHelper.GetFirstOfMonth(startDate), 12),
                    //        EndDate = DateTimeHelper.GetFirstOfMonth(startDate),                        
                    //        CID = siteUser.CID,
                    //        BID = ae.Equipment.BID,
                    //        EID = ae.EID,
                    //        AID = ae.AID,                        
                    //    }
                    //);

                    //get vpoints
                    IEnumerable<VPoint> vpoints = DataMgr.VAndVPreDataMapper.GetVPoints(siteUser.CID, ae.Equipment.BID, ae.EID, ae.AID).Where(v => v.PointTypeID == BusinessConstants.PointType.AnnualAvoidableCostPointTypeID || v.PointTypeID == BusinessConstants.PointType.AnnualAvoidableCostErrorPointTypeID);

                    //has vpoints
                    if (vpoints.Any() && vpoints.Count() == 2)
                    {
                        IEnumerable<VData> costData = Enumerable.Empty<VData>();
                        IEnumerable<VData> errorData = Enumerable.Empty<VData>();
                        VData lowestError;

                        VPoint vPointCost = vpoints.Where(v => v.PointTypeID == BusinessConstants.PointType.AnnualAvoidableCostPointTypeID).FirstOrDefault();
                        VPoint vPointCostError = vpoints.Where(v => v.PointTypeID == BusinessConstants.PointType.AnnualAvoidableCostErrorPointTypeID).FirstOrDefault();

                        //get past 12 monthlies
                        IEnumerable<VData> vData = DataMgr.VAndVPreDataMapper.GetVDataByDateRange(vpoints.Select(v => v.VPID), DateTimeHelper.GetXMonthsAgo(DateTimeHelper.GetFirstOfMonth(startDate), 12), DateTimeHelper.GetFirstOfMonth(startDate), DataConstants.AnalysisRange.Monthly);

                        if (vData.Any())
                        {
                            //filter cost
                            costData = vData.Where(v => v.VPID == vPointCost.VPID && Convert.ToDouble(v.RawValue) > 0);
                        }

                        if (!costData.Any())
                        {
                            //get past 12 weeklies
                            vData = DataMgr.VAndVPreDataMapper.GetVDataByDateRange(vpoints.Select(v => v.VPID), DateTimeHelper.GetXWeeksAgo(DateTimeHelper.GetLastSunday(startDate), 12), DateTimeHelper.GetLastSunday(startDate), DataConstants.AnalysisRange.Weekly);

                            if (vData.Any())
                            {
                                //filter cost
                                costData = vData.Where(v => v.VPID == vPointCost.VPID && Convert.ToDouble(v.RawValue) > 0);
                            }
                        }

                        if (costData.Any())
                        {
                            //filter error by filtered cost. And then get lowest error.
                            lowestError = vData.Where(v => v.VPID == vPointCostError.VPID && costData.Select(c => c.StartDate).Contains(v.StartDate)).OrderBy(v => Convert.ToDouble(v.RawValue)).First();

                            double tempCost = 0;

                            try
                            {
                                tempCost = Convert.ToDouble(vData.Where(v => v.VPID == vPointCost.VPID && v.StartDate == lowestError.StartDate).First().RawValue);
                                hasCostSavings = true;
                            }
                            catch
                            {
                            }

                            projectedCostSavings = projectedCostSavings + tempCost;
                        }
                    }
                }
            }
            else
            {
                projectedCostSavings = Convert.ToDouble(project.ProjectedSavings);
                if (projectedCostSavings > 0) hasCostSavings = true;
            }

            string[] returnString = new string[] { hasCostSavings ? Math.Round(projectedCostSavings, 2).ToString() : "", (!hasCostSavings || projectedCostSavings == 0 || String.IsNullOrEmpty(project.EstimatedProjectCost)) ? "" : NumericHelper.RoundPrecisionInvariant((CultureHelper.FormatNumber(BusinessConstants.Culture.DefaultCultureLCID, project.EstimatedProjectCost) / projectedCostSavings).ToString(), 2) };

            //set thread back 
            CultureHelper.SetThreadCulture(siteUser.CultureName);

            return returnString;
        }

        /// <summary>
        /// make sure each aeid is for a building with the same culture currency as the user selected culture currency
        /// </summary>
        /// <param name="aeids"></param>
        /// <param name="lcid"></param>
        /// <returns></returns>
        private bool CultureCurrencyCheck(ListItemCollection aeids, int lcid)
        {
            RegionInfo selectedRegion = new RegionInfo(lcid);

            foreach (ListItem li in aeids)
            {
                RegionInfo buildingRegion = new RegionInfo(DataMgr.BuildingDataMapper.GetBuildingSettingsByAEID(Convert.ToInt32(li.Value)).LCID);

                if (buildingRegion.ISOCurrencySymbol != selectedRegion.ISOCurrencySymbol)
                    return false;
            }

            return true;
        }


        /// <summary>
        /// Checks if any analysis equipment associations chagned
        /// </summary>
        /// <param name="newAEIDs"></param>
        /// <param name="currentAEIDs"></param>
        /// <returns></returns>
        private bool DidAnalysisEquipmentAssociationsChange(ListItemCollection newAEIDs, string currentAEIDs)
        {
            string[] currentArray = currentAEIDs.Split(',');
            string[] newArray = newAEIDs.Cast<ListItem>().ToArray().Select(ae => ae.Value).ToArray();
            
            if (newArray.Count() != currentArray.Count())
                return true;

            if (!newArray.SequenceEqual(currentArray))
                return true;

            return false;
        }

        private IEnumerable<GetGroupedProjectAssociationsData> BuildDiagnosticLinks(IEnumerable<GetGroupedProjectAssociationsData> projects)
        {
            foreach (GetGroupedProjectAssociationsData p in projects)
            {
                int counter = 0;

                foreach(GetAnalysisEquipmentData ae in p.AnalysisEquipmentData)
                {
                    p.AnalysisEquipmentNameArray[counter] = String.Format("<a href='Diagnostics.aspx?cid={0}&bid={1}&ecid=0&eid={2}&aid={3}&sd={4}&rng=Daily' target='_blank'>{5}</a>", p.CID, ae.BID, ae.EID, ae.AID, p.TargetStartDate != null ? ((DateTime)p.TargetStartDate).ToShortDateString() : "", p.AnalysisEquipmentNameArray[counter]);
                    counter++;
                }
            }

            return projects;
        }

        #endregion
    }
}

