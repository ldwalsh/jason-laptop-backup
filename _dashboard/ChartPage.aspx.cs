﻿using CW.Utility;
using CW.Website._framework;
using CW.Website._masters;
using CW.Data.Helpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI;
using Telerik.Charting;
using Telerik.Charting.Styles;
using DataManager = CW.Business.DataManager;
using Unit = System.Web.UI.WebControls.Unit;
using CW.Logging;

namespace CW.Website.Dashboard
{
    public partial class ChartPage: SitePage
    {
		#region CLASS

			private class FontDeserializer: JSSerializer.TypeDeserializer
			{
				public override Object Deserialize(String json)
				{
					var props = json.Split(';');
					
					return new Font(props[0], Single.Parse(props[1], CultureInfo.InvariantCulture), (FontStyle)Enum.Parse(typeof(FontStyle), props[2]));
				}

				public override Type Type
				{
					get {return typeof(Font);}
				}
			}

		#endregion

		#region STATIC

            private static ChartSeriesItem CreateChartSeriesItem(CommissioningDashboardDataStore.ISeriesPointInfo spi)
            {
				var item = new ChartSeriesItem(spi.Y, spi.YLabel);

				var activeRegion = item.ActiveRegion;
				
				activeRegion.Attributes	= String.Format("data=\"{0}\"", new[]{spi.Data});
				activeRegion.Tooltip	= spi.FigureTip;
				activeRegion.Url		= spi.Url;

				if (spi.YLabel == null)
				{
					item.Label.Visible = false;
				}
				else
				{
					//move out to chart properties
					var a = item.Label;

					a.Appearance.LabelLocation			        = StyleSeriesItemLabel.ItemLabelLocation.Inside;
					a.Appearance.Position.AlignedPosition       = AlignedPositions.Center;
                    a.TextBlock.Appearance.TextProperties.Color = Color.White;
				}

                return item;
            }

			private static ChartAxisItem createChartAxisItem(CommissioningDashboardDataStore.ISeriesPointInfo spi)
			{
				var item = new ChartAxisItem(spi.XLabel);
						
				item.ActiveRegion.Tooltip = spi.XLabelTip;
				
				item.TextBlock.Visible = spi.ShowXAxisLabel;

				return item;
			}

            private String GetCultureSpecificCostForBuilding(Int32 bid, Double cost)
            {
                var lcid = DataMgr.BuildingDataMapper.GetBuildingSettingsByBID(bid).LCID;
				
				return String.Format(CultureInfo.GetCultures(CultureTypes.AllCultures).Single(_=> _.LCID == lcid), "{0:C0}", cost);
            }
					
		#endregion

        #region method

            #region override

            protected override void InitializeCulture()
				{
					if (siteUser.IsAnonymous)
					{
						//TODO: set user culture based on browser
					}
					else
					{
						UICulture = Culture= siteUser.CultureName;

						Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(siteUser.CultureName);
						Thread.CurrentThread.CurrentUICulture = new CultureInfo(siteUser.CultureName);
					}

					base.InitializeCulture();
				}
						
			#endregion

                protected void Page_Init(Object sender, EventArgs ea)
                {
                    litScripts.Text = ReferenceSetter.Get("jquery");
                }

			protected void Page_Load(Object sender, EventArgs e)
			{
				if (IsPostBack) return;

				var args = ((Request.Form.Count > 1) ? Request.Form : Request.QueryString);

                var dataKey = new JSSerializer(args["plotInfo"]).Deserialize<CommissioningDashboardDataStore.FilteredDataKey>(JSSerializer.DeserializeFlags.IgnoreCase|JSSerializer.DeserializeFlags.Coerce);

                //TODO: combine with dataKey
                //set chart appearance and series by using querystring argument (as json object)
                var plotInfo = new JSSerializer(args["plotInfo"]).Deserialize<CommissioningDashboardDataStore.PlotInfoo>(JSSerializer.DeserializeFlags.IgnoreCase|JSSerializer.DeserializeFlags.Coerce);

				dataKey.ChartType			= plotInfo.ChartType;
				dataKey.PlotRange			= plotInfo.PlotRange;
				dataKey.PlotType			= plotInfo.PlotType;
                dataKey.ChartTypeGrouping	= plotInfo.ChartTypeGrouping;
                dataKey.DateKey             = DateTime.Now.ToShortDateString();

                var plots = new CommissioningDashboardDataStore(siteUser, DataMgr.DefaultCacheRepository, DataMgr, LogMgr).GetFilteredData(siteUser.CID, dataKey);

				if (plots.Count == 0)
				{
					Response.Write(0);
					Response.End();

					return;
				}

                SetChartAppearanceAndSeries(plots, dataKey);
				
				SetChartProperties(new JSSerializer(args["chartProps"]), plots, dataKey.ChartTypeGrouping);
			}

            private void SetChartAppearanceAndSeries
			(
				IDictionary<CommissioningDashboardDataStore.SeriesInfo,IList<CommissioningDashboardDataStore.ISeriesPointInfo>> plots,
				CommissioningDashboardDataStore.FilteredDataKey dataKey
			)
            {
                Double yMax = 0;

				var labels = new List<String>();

                foreach (var p in plots)
                {
                    var series = p.Key;

                    //FillType = FillType.Solid, FillType = FillType.Hatch, FillSettings = { HatchStyle = HatchStyle.Horizontal }
                    var chartSeries = new ChartSeries{Name= series.Name, Type= ChartSeriesType.Line, Appearance= {FillStyle= {SecondColor= Color.LightSlateGray}, Border= {Width= 0}}};

                    chartSeries.Appearance.PointMark.Visible = true; //Point visibility controlled by color
                    chartSeries.Appearance.PointMark.Figure  = series.PointFigure.ToString();

                    chartSeries.Appearance.PointMark.Border.Width = 2;
                    chartSeries.Appearance.PointMark.Border.Color = series.PointColor;

                    chartSeries.Appearance.PointMark.FillStyle.MainColor = Color.White;
                    chartSeries.Appearance.PointMark.FillStyle.FillType  = FillType.Solid;

                    chartSeries.Appearance.LineSeriesAppearance.Color = series.LineColor;
                    chartSeries.Appearance.LineSeriesAppearance.Width = 2;
					
					foreach (var point in p.Value)
                    {
                        yMax = Math.Max(point.Y, yMax);

                        chartSeries.AddItem(CreateChartSeriesItem(point));

						if (labels.Contains(point.XString)) continue;
						
						labels.Add(point.XString);

						chart.PlotArea.XAxis.AddItem(createChartAxisItem(point));
                    }

					chart.AddChartSeries(chartSeries);

                    if
                    (	//cleanup when SeriesInfo constructors cleaned up! revise checking agains EnumName?
                        (series.EnumName != null) &&
                        (series.EnumName.GetType() == typeof(CommissioningDashboardDataStore.ChartTypeEnum)) &&
                        (Convert.ToInt32(series.EnumName) == (Int32)CommissioningDashboardDataStore.ChartTypeEnum.Cost)
                    )
                    {
                        var costSeries = (CommissioningDashboardDataStore.CostSeriesInfo)series;
                        hdnCostTotal.Value += (String.IsNullOrWhiteSpace(hdnCostTotal.Value) ? String.Empty : " | ") + GetCultureSpecificCostForBuilding(costSeries.BID, costSeries.Total);
                    }                    
                }

				chart.PlotArea.YAxis.MaxValue = Math.Ceiling(yMax) == 10 ? Math.Ceiling(yMax) : Math.Ceiling(yMax) + (10 - NumericHelper.GetLastDigit(Math.Ceiling(yMax)));
            }

            private void SetChartProperties
			(
				JSSerializer jss,
				IDictionary<CommissioningDashboardDataStore.SeriesInfo,IList<CommissioningDashboardDataStore.ISeriesPointInfo>> plots,
				CommissioningDashboardDataStore.ChartTypeGroupingEnum chartTypeGrouping
			)
            {
                jss.AddTypeDeserializer(new FontDeserializer());
                jss.AddTypeDeserializer(_=> Color.FromName(_));
                jss.AddTypeDeserializer(_=> Unit.Parse(_));
                jss.AddTypeDeserializer(Telerik.Charting.Styles.Unit.Parse);
                
                jss.Deserialize(chart);

                chart.ID = (chart.ID + new Random().Next(100, 999));
				
				chart.Legend.Visible = (plots.Count > 1);
				
                chart.PlotArea.YAxis.Step = chart.PlotArea.YAxis.MaxValue / 10;

                chart.PlotArea.Appearance.Dimensions.Margins.Left.Type  = UnitType.Pixel;
                chart.PlotArea.Appearance.Dimensions.Margins.Left.Value = (22 + chart.PlotArea.YAxis.MaxValue.ToString().Length * 8);

                chart.ChartTitle.TextBlock.Appearance.TextProperties.Color = ColorHelper.ConvertHexToSystemColor("#212121");
                chart.ChartTitle.Appearance.Dimensions.Margins.Left.Type  = UnitType.Pixel;
                chart.ChartTitle.Appearance.Dimensions.Margins.Left.Value = chart.PlotArea.Appearance.Dimensions.Margins.Left.Value;

                chart.Style.Add("overflow", "hidden");
            }

        #endregion
    }
}