﻿///
///<reference path="../../$CommissioningDashboard.js" />

$.enum
(
	{
		namespace:	CommissioningDashboard.PerformanceData,
		name:		"ChartTypeEnum",
	},

	function()
	{
		/// <field name="Faults" />
		/// <field name="Priorities" />
		/// <field name="Cost" />
	},	
	[
		"Faults", "Priorities", "Cost",
	]
)
