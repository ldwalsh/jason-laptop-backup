﻿///
///<reference path="../../$CommissioningDashboard.js" />

$.enum
(
	{
		namespace:	CommissioningDashboard.PerformanceData,
		name:		"PriorityTypeEnum",
	},

	function()
	{
		/// <field name="Comfort" />
		/// <field name="Energy" />
		/// <field name="Maintenance" />
	},
	[
		"Comfort", "Energy", "Maintenance",
	]
)
