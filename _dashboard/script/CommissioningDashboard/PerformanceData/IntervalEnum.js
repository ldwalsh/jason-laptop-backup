﻿///
///<reference path="../../$CommissioningDashboard.js" />

$.enum
(
	{
		namespace:	CommissioningDashboard.PerformanceData,
		name:		"IntervalEnum"
	},

	function()
	{
		/// <field name="Daily" />
		/// <field name="Weekly" />
		/// <field name="Monthly" />
	},
	[
		"Daily", "Weekly", "Monthly",
	]
)
