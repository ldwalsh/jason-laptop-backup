﻿///
///<reference path="../../$CommissioningDashboard.js" />

$.enum
(
	{
		namespace:	CommissioningDashboard.PerformanceData,
		name:		"PlotRangeEnum",
	},

	function()
	{
		/// <field name="Past30Days" />
		/// <field name="Past60Days" />
		/// <field name="Past90Days" />
		/// <field name="Past10Weeks" />
		/// <field name="Past25Weeks" />
		/// <field name="Past52Weeks" />
		/// <field name="Past12Months" />
		/// <field name="Past24Months" />
		/// <field name="Past36Months" />
	},
	[
		"Past30Days", "Past60Days", "Past90Days", "Past10Weeks", "Past25Weeks", "Past52Weeks", "Past12Months", "Past24Months", "Past36Months",
	]
)
