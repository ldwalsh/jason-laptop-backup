﻿///
///<reference path="../$CommissioningDashboard.js" />

$.enum
(
	{
		namespace:	window.CommissioningDashboard,
		name:		"ViewModeEnum",
	},

	function()
	{
		/// <field name="PerformanceData" />
		/// <field name="ClientStats" />
		/// <field name="BuildingStats" />
	},
	[
		"PerformanceData", "ClientStats", "BuildingStats",
	]
)
