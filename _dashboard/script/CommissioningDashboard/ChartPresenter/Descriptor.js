﻿///
///<reference path="../../$CommissioningDashboard.js" />

$.class
(
	{
		namespace: CommissioningDashboard.ChartPresenter,
	},

	function Descriptor()
	{
		this.bid				= Number.VAR;
		this.equipmentClassId	= Number.VAR;
		this.eid				= Number.VAR;
		this.plotType			= null;
		this.interval			= null;
		this.chartType			= null;
		this.plotRange			= null;
	
		this.toString =
			function()
			{
				var value = [(this.bid || ""), (this.equipmentClassId || ""), (this.eid || ""), this.plotType.valueOf(), this.chartType.valueOf(), this.plotRange.valueOf(),]
			
				return value.join("|")
			}

		
		$.class.instantiate2(this)
	}
)
