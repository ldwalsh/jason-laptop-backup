﻿///
///<reference path="../../$CommissioningDashboard.js" />

$.enum
(
	{
		namespace:	window.CommissioningDashboard.ChartPresenter,
		name:		"FigureEnum",
	},

	function()
	{
		///<field name="Circle" />
		///<field name="Cross" />
		///<field name="Diamond" />
		///<field name="Ellipse" />
		///<field name="Rectangle" />
		///<field name="Star3" />
		///<field name="Star5" />
		///<field name="Star6" />
		///<field name="Star7" />
		///<field name="Triangle" />
	},
	[
		"Circle", "Cross", "Diamond", "Ellipse", "Rectangle", "Star3", "Star5", "Star6", "Star7", "Triangle",
	]
)
