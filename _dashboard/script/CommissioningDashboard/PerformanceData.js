﻿///
///<reference path="../$CommissioningDashboard.js" />

$.class
(
	{
		namespace: CommissioningDashboard,
	},

	function PerformanceData(commissioningDashboard)
	{
		///<param name="commissioningDashboard" type="CommissioningDashboard" />
			
		var $cstr = this.constructor;

		var IntervalEnum     = $cstr.IntervalEnum;
		var PlotRangeEnum    = $cstr.PlotRangeEnum;
		var PlotTypeEnum     = $cstr.PlotTypeEnum;
		var HoverButtonGroup = CommissioningDashboard.HoverButtonGroup;

		this.commissioningDashboard = CommissioningDashboard.VAR;
		this._chartPresenter		= null; //$cstr.parent.ChartPresenter.VAR;
		this.chartType              = $cstr.ChartTypeEnum.Cost;
		this.plotType				= PlotTypeEnum.Building;
		this.interval				= IntervalEnum.Daily;
		this.plotRange				= PlotRangeEnum.Past30Days;
		this.filterKey				= Object.VAR;

		this._chartTypeButtonGroup	= $cstr.parent.HoverButtonGroup.VAR;
		this._plotTypeButtonGroup	= $cstr.parent.HoverButtonGroup.VAR;
		this._intervalButtonGroup	= $cstr.parent.HoverButtonGroup.VAR;		
		this._plotRangeDButtonGroup	= $cstr.parent.HoverButtonGroup.VAR;
		this._plotRangeWButtonGroup	= $cstr.parent.HoverButtonGroup.VAR;
		this._plotRangeYButtonGroup	= $cstr.parent.HoverButtonGroup.VAR;
		
		this.Cstr =
			function()
			{
				this.filterKey = {bid:this.commissioningDashboard.bid, equipmentClassID:null, eid:null}

				this.commissioningDashboard.equipmentClassChanged = this._commissioningDashboard_equipmentClassChanged;
				this.commissioningDashboard.eidChanged			  = this._commissioningDashboard_eidChanged;

				this._chartPresenter = new CommissioningDashboard.ChartPresenter(this, HTMLElement.getById(new RegExp("divPerformanceChartIFrames$")))

				this._initButtons() //default buttons

				this._plotTypeButtonGroupClick(0) //default chart
			}
	
		this._initButtons =
			function()
			{
				var $HoverButton = CommissioningDashboard.HoverButton;

				this._plotTypeButtonGroup =
					new HoverButtonGroup
					(
						[
							new $HoverButton("btnBuilding")		 .SET({selected:true}),
							new $HoverButton("btnEquipmentClass").SET({enabled:false}),
							new $HoverButton("btnEquipment")	 .SET({enabled:false})
						],
						[0,1,2],
						this._plotTypeButtonGroupClick
					)
					
				this._chartTypeButtonGroup =
					new HoverButtonGroup
					(
						[new $HoverButton("btnFaults"), new $HoverButton("btnPriorities"), new $HoverButton("btnCostSavings").SET({selected:true}),],
						[0,1,2],
						function(index)
						{
							this.chartType = $.enum.getValue($cstr.ChartTypeEnum, index)
						
							this._chartPresenter.displayChart()
					
						}.bind(this)
					)

				this._intervalButtonGroup =
					new HoverButtonGroup
					(
						[new $HoverButton("btnDaily").SET({selected:true}), new $HoverButton("btnWeekly"), new $HoverButton("btnMonthly"),],
						[0,1,2],
						this._intervalButtonGroupClick
					)

				this._plotRangeDButtonGroup =
					new HoverButtonGroup
					(
						[new $HoverButton("btnPast30Days").SET({selected:true}), new $HoverButton("btnPast60Days"), new $HoverButton("btnPast90Days")],
						[0,1,2],
						function(index)
						{
							this.plotRange = $.enum.getValue(PlotRangeEnum, index)
						
							this._chartPresenter.displayChart()
					
						}.bind(this)
					)

				this._plotRangeWButtonGroup =
					new HoverButtonGroup
					(
						[new $HoverButton("btnPast10Weeks"), new $HoverButton("btnPast25Weeks"), new $HoverButton("btnPast52Weeks")],
						[3,4,5], //user enums
						function(index)
						{
							this.plotRange = $.enum.getValue(PlotRangeEnum, index)
						
							this._chartPresenter.displayChart()
						
						}.bind(this)
					)

				this._plotRangeYButtonGroup =
					new HoverButtonGroup
					(
						[new $HoverButton("btnPast12Months"), new $HoverButton("btnPast24Months"), new $HoverButton("btnPast36Months")],
						[6,7,8], //user enums
						function(index)
						{
							this.plotRange = $.enum.getValue(PlotRangeEnum, index)
						
							this._chartPresenter.displayChart()
					
						}.bind(this)
					)
			}

		this._commissioningDashboard_equipmentClassChanged =
			function(equipmentClassID)
			{
				var hbs = this._plotTypeButtonGroup.hoverButtons;

				if (equipmentClassID == null)
				{
					hbs[PlotTypeEnum.EquipmentClass.valueOf()].enabled = false
				
					if (this._plotTypeButtonGroup.selected != PlotTypeEnum.Building)
					{
						hbs[PlotTypeEnum.Building.valueOf()].selected = true;

						this._displayChart(PlotTypeEnum.Building)
					}
				}
				else
				{
					hbs[PlotTypeEnum.EquipmentClass.valueOf()].enabled = true;
			    
					this._plotTypeButtonGroup.toggle(PlotTypeEnum.EquipmentClass)
				
					this._displayChart(PlotTypeEnum.EquipmentClass) //shoud be called buy toggle()?
				}
			
				hbs[PlotTypeEnum.Equipment.valueOf()].enabled = false;
			}

		this._commissioningDashboard_eidChanged =
			function(eid)
			{
				var hbs = this._plotTypeButtonGroup.hoverButtons;
			
				if (eid == null)
				{
					hbs[PlotTypeEnum.Equipment.valueOf()].enabled = false;
				
					if (this._plotTypeButtonGroup.selected == PlotTypeEnum.Equipment)
					{			    
						hbs[PlotTypeEnum.EquipmentClass.valueOf()].selected = true;
				
						this._displayChart(PlotTypeEnum.EquipmentClass)
					}
				}
				else
				{
					hbs[PlotTypeEnum.Equipment.valueOf()].enabled = true;
				
					this._plotTypeButtonGroup.toggle(PlotTypeEnum.Equipment)
				
					this._displayChart(PlotTypeEnum.Equipment) //shoud be called buy toggle()?
				}
			}

		this._intervalButtonGroupClick =
			function(index)
			{
				this.interval = $.enum.getValue(IntervalEnum, index)
			
				this._plotRangeDButtonGroup.displayed = this._plotRangeWButtonGroup.displayed = this._plotRangeYButtonGroup.displayed = false;
			
				var bg;
			
				switch (index)
				{
					case IntervalEnum.Daily.valueOf():
					{
						this.plotRange = PlotRangeEnum.Past30Days;
					
						bg = this._plotRangeDButtonGroup;
					
						break;
					}
					case IntervalEnum.Weekly.valueOf():
					{
						this.plotRange = PlotRangeEnum.Past10Weeks;
						
						bg = this._plotRangeWButtonGroup;
					
						break;
					}
					case IntervalEnum.Monthly.valueOf():
					{
						this.plotRange = PlotRangeEnum.Past12Months;
					
						bg = this._plotRangeYButtonGroup;
					
						break;
					}
					default: throw new $.InvalidOperationError()
				}
			
				bg.displayed = true;

				bg.toggle(0)
			
				this._chartPresenter.displayChart()
			}
	
		this._plotTypeButtonGroupClick =
			function(value)
			{
				this._displayChart(value ? $.enum.getValue(PlotTypeEnum, value) : (this.filterKey.bid ? PlotTypeEnum.Building : PlotTypeEnum.Portfolio))
			}

		this._displayChart =
			function(plotType)
			{
				this.plotType = plotType;
			
				this._chartPresenter.displayChart()
			}


		$.class.instantiate2(this)
	}
)
