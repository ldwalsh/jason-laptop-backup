﻿///
///<reference path="../$CommissioningDashboard.js" />

$.class
(
	{
		namespace: CommissioningDashboard,

	},

	function HoverButtonGroup(hoverButtons, values, onClick)
	{
		///<param name="hoverButtons" type="Array" />
		///<param name="values" type="Object" />
		///<param name="onClick" type="Function" />

		var $cstr = this.constructor;

		this.hoverButtons	= $.TArray.VAR($cstr.parent.HoverButton)
		this.values			= Object.VAR;
		this.onClick		= Function.VAR;
		this._selected		= null;

		this.Cstr =
			function()
			{
				for (var i=0; i<this.hoverButtons.length; i++)
				{
					var hb = this.hoverButtons.item(i)

					//hb.anchor.event.attach.click(this._hoverButton_Click)

					jQuery(hb.anchor).click({value:hb, index:i}, this._hoverButton_Click)
				}
			}
	
		this.reset =
			function()
			{
				for (var i=0; i<this.hoverButtons.length; i++)
				{
					this.hoverButtons.item(i).selected = false;
				}
			}
	
		this.toggle =
			function(hoverButton_OR_index)
			{
				var hoverButton = ((hoverButton_OR_index instanceof CommissioningDashboard.HoverButton) ? hoverButton_OR_index : this.hoverButtons[hoverButton_OR_index.valueOf()])

				for (var i=0; i<this.hoverButtons.length; i++)
				{
					var hb = this.hoverButtons.item(i)

					hb.selected = (hb == hoverButton)
				}
			}

		this._hoverButton_Click =
			function(e)
			{
				if (e.target.__disabled) return;
			
				this.toggle(e.data.value)
			
				this.onClick((this.values == null) ? e.target : this.values[e.data.index])
		    
				this._selected = this.values[e.data.index]
			}

		this.displayed_get =
			function()
			{
				return (this.hoverButtons[0].anchor.parentNode.style.display == "")
			}

		this.displayed_set =
			function(value)
			{
				this.hoverButtons[0].anchor.parentNode.style.display = (value ? "" : "none")
			}
	
		this.selected_get =
			function()
			{
				return this._selected;
			}


		$.class.instantiate2(this)
	}
)
