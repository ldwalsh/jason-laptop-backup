﻿///
///<reference path="../$CommissioningDashboard.js" />

$.class
(
	{
		namespace: CommissioningDashboard,
	},

	function HoverButton(anchor)
	{
		var $cstr = this.constructor;

		this.anchor     = HTMLAnchorElement.VAR;
		this.hoverGroup = $cstr.parent.HoverButtonGroup.VAR;

		this._stateInactive = "lnkBtnSmallDashboard";
		this._stateActive   = "lnkBtnSmallDashboardActive";
		this._stateDisabled = "lnkBtnSmallDashboardDisabled";

		this.Cstr =
			function()
			{
				if (String.IS(this.anchor))
				{
					this.anchor = HTMLElement.getById(new RegExp(this.anchor + "$"))
				}
			}

		this.toggle =
			function(active)
			{
				///<param name="active" type="Boolean" />
			
				if (!this.enabled) return;
			
				this.anchor.className = (active ? this._stateActive : this._stateInactive)
			}

		this.enabled_get =
			function()
			{
				return (this.anchor.className != this._stateDisabled)
			}

		this.enabled_set =
			function(value)
			{
				///<param name="value" type="Boolean" />

				if (value == this.enabled) return;

				this.anchor.enabled = value;
				
				this.anchor.className = (value ? "lnkBtnSmallDashboard" : this._stateDisabled)
				
				if (value) return;
				
				this.selected = false;
			}

		this.selected_get =
			function()
			{
				return (this.anchor.className == this._stateActive)
			}

		this.selected_set =
			function(value)
			{
				if (!this.enabled || (value == this.selected)) return;
				
				this.anchor.className = (value ? this._stateActive : this._stateInactive)
			}
			

		$.class.instantiate2(this)
	}
)
