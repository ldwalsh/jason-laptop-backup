﻿///
///<reference path="../$CommissioningDashboard.js" />

$.class
(
	{
		namespace: CommissioningDashboard,
	},

	function ChartPresenter(performanceData, div)
	{
		var PlotTypeEnum = CommissioningDashboard.PerformanceData.PlotTypeEnum;

		var $cstr = this.constructor;
		
		this._performanceData	= CommissioningDashboard.PerformanceData.VAR;
		this._div				= HTMLDivElement.VAR;
		this._charts			= {}
		this._currentChart		= null;
		
		this.displayChart =
			function()
			{
				if (CommissioningDashboard.ajaxDisableDiv)
				{
					document.body.appendChild(CommissioningDashboard.ajaxDisableDiv)
				}

				var cD = this._performanceData.commissioningDashboard;
			
				var d = new CommissioningDashboard.ChartPresenter.Descriptor();
			
				d.bid              = cD.bid;
				d.equipmentClassId = ([PlotTypeEnum.EquipmentClass, PlotTypeEnum.Equipment].contains(this._performanceData.plotType) ? cD.equipmentClassID : null)
				d.eid              = ((this._performanceData.plotType == PlotTypeEnum.Equipment) ? cD.eid : null)
				
				d.plotType  = this._performanceData.plotType;
				d.interval  = this._performanceData.interval;
				d.chartType = this._performanceData.chartType;
				d.plotRange = this._performanceData.plotRange;
			
				//

				if (this._charts[d.toString()] != null)
				{
					this._toggle(d)
				
					return;
				}

				var chart = new $cstr.Chart(d, this._div)
				
				chart.onLoad.attach
				(
					function(ea) //using 
					{
						this._charts[d.toString()] = (chart.isEmpty ? $cstr.Chart.EmptyChart : chart)
				
						this._toggle(d)

					}.bind(this)
				)
				
				chart.build()
			}

		this._toggle =
			function(d)
			{
				if (this._currentChart)
				{
					this._currentChart.display(false)
				}

				this._currentChart = $cstr.Chart.CAST(this._charts[d.toString()])
				
				this._currentChart.display()

				//

				var spnCostTotal = $gId("spnCostTotal")

				spnCostTotal.innerHTML = this._currentChart.totalCost;

				spnCostTotal.parentNode.parentNode.toggleVisibility(!!this._currentChart.totalCost, true) //revise to not use true overload... will fix inheritance bug in toggleVisibility

				//

				if (CommissioningDashboard.ajaxDisableDiv && CommissioningDashboard.ajaxDisableDiv.parentNode)
				{
					CommissioningDashboard.ajaxDisableDiv.removeElement()
				}
			}


		$.class.instantiate2(this)
	}
)




$.class
(
	{
		namespace: CommissioningDashboard.ChartPresenter,
		statics:
		{
			_emptyChart:null,

			EmptyChart_:
			function()
			{
				if (!this._emptyChart)
				{
					var c = Object.create(new this)
					
					c.display = function()
					{
						var div = HTMLElement.getById("divEmptyDataSet")

						div.toggleDisplay()

						//div.toggleDisplay.getArgsCaller(true).bind(div)
					}

					this._emptyChart = c;
				}

				return this._emptyChart;
			},
		},
	},

	function Chart(descriptor, div)
	{
		var $cstr = this.constructor;

		this._descriptor	= null; //use concrete type here
		this._div			= HTMLDivElement.VAR;
		this.onLoad			= $.Event;
		this.isEmpty		= true;
		this.display		= Function.VAR;
		this.totalCost		= Number.VAR;
		
		this._loaded =
			function(dE)
			{
				///<param name="dE" type="$.DomEvent" />

				var iFrame = HTMLIFrameElement.CAST(dE.target)

				if (iFrame.contentDocument.documentElement.textContent != "0")
				{
					this.isEmpty = false;

					document.disableContextMenu.apply(iFrame.contentWindow.document)

					var cM =
						new $.ui.controls.ContextMenu
							($gId("ulContextMenu").clone(), iFrame.contentDocument.getElementsByTagName("area")).SET({mouseButton:$.ui.controls.ContextMenu.ButtonEnum.Any})

					cM.onClick = this._cM_onClick;

					//

					if (this._descriptor.chartType == CommissioningDashboard.PerformanceData.ChartTypeEnum.Cost)
					{
						this.totalCost = iFrame.contentWindow.document.getElementById("hdnCostTotal").value;
					}
				}
					
				this.onLoad.raise()
			}

		this._cM_onClick =
			function(eA)
			{
				///<param name="eA" type="$.ui.ContextMenu.ClickEventArgs" />

				var url = this._createDiagnosticsURL(eA.element.getAttribute("data-x"))
										
				if (eA.menuItem == "Open here")
				{
					window.location.href = url;
				}
				else
				{
					window.open(url, null, "location=1,resizable=1,scrollbars=1,toolbar=1")
				}
			}

		this._createDiagnosticsURL =
			function(date)
			{
				var url = "/Diagnostics.aspx?cid={0}&bid={1}&rng={2}&sd={3}&ed={3}";

				var d = this._descriptor;
						
				if (d.equipmentClassId)
				{
					url += "&ecid={4}";
				}
						
				if (d.eid)
				{
					url += "&eid={5}";
				}
				
				return url.format(window.page.cid, (d.bid || 0), d.interval.valueOf(), date, d.equipmentClassId, d.eid)
			}

		this._buildTitle =			
			function()
			{
				var title =	String.fromInterCap(this._descriptor.plotType.toString() + " " + this._descriptor.chartType.toString() + " for " + this._descriptor.plotRange.toString())
												
				title = title.replace("Cost", "Avoidable Energy Costs")				
				title = title.replace("Priorities", "Average Priorities")

				return title;
			}

		this._getYAxisTitle =
			function(chartType)
			{
				if (chartType == CommissioningDashboard.PerformanceData.ChartTypeEnum.Priorities) return "Average Priorities";

				return chartType.toString()

				//(this._descriptor.chartType == CommissioningDashboard.PerformanceData.ChartTypeEnum.Cost)
				//	? /*(" (" + String.fromCharCode(parseInt(page.buildingCurrencyHex.substr(3), 16)) + ")")*/ "" : ""
			}
			
		this.build =
			function()
			{
				var chartProps =
					{
						Appearance:{Border:{Width:0}},
						Height:"410px",
						Width:"778px",
						//AutoTextWrap: true, Appearance: {ImageQuality: "AntiAlias"}, ClientSettings:{ScrollMode: "Both", EnableZoom: true},
						Legend:
						{
							Appearance:
							{
								//Border:{/*Color: "Gold", PenStyle: "Solid"*/ Width: 0},
								Dimensions:
								{
									Margins:{Top:-4, Right:10},
									Paddings:{Top:0, Right:0, Bottom:0, Left:0}
								},
								ItemMarkerAppearance:{Figure:CommissioningDashboard.ChartPresenter.FigureEnum.Rectangle.valueOf()},
								Overflow:"Row",
								Position:{AlignedPosition:"TopRight"}
							}
						},
						ChartTitle:
						{
							Appearance:
							{
								//Border:{Color: "Gold", PenStyle: "Solid"},
								Dimensions:{Margins:{Top:0}, Paddings:{Top:-5, Bottom:0, Left:-4}},
								Position:{AlignedPosition:"TopLeft"}
							},
							TextBlock:{Text:this._buildTitle(), Appearance:{TextProperties:{Color:"Black", Font:"Arial;11;Bold"}}}
						},
						PlotArea:
						{
							Appearance:{Dimensions:{Margins:{Top:20, Right:1}}, FillStyle:{MainColor:"White", SecondColor:"White"}},
							XAxis:
							{
								Appearance:
								{
									CustomFormat:"d",
									//LabelAppearance: {RotationAngle: 15},
									MajorGridLines:{Color:"LightGray", Visible:true},
									TextAppearance:{TextProperties:{Color:"Black", Font:"Arial;8;Regular"}},
									ValueFormat:"LongDate"
								},
								AxisLabel:{TextBlock:{Appearance:{TextProperties:{Color:"Black"}}, Text:"Date"}, Visible:true},
								IsZeroBased:false
							},
							YAxis:
							{
								AutoScale:false,
								Appearance: {MajorGridLines:{Color:"LightGray", Visible:true}, TextAppearance:{TextProperties:{Color:"Black"}}},
								AxisLabel: {Visible:true, TextBlock:{Appearance:{TextProperties:{Color:"Black"}}, Text:this._getYAxisTitle(this._descriptor.chartType),}}
								//IsZeroBased:false, MaxValue:90, Step:10
							}
						}
					}

				var iFrame =
					HTMLElement.create
					(
						HTMLIFrameElement,
						{
							parentNode:this._div,
							src:
							"_dashboard/ChartPage.aspx?chartProps={0}&plotInfo={1}".format
							(
								JSON.stringify(chartProps),
								JSON.stringify
								(
									this._descriptor,
									function(key, value)
									{
										///<param name="key" type="String" />
										///<param name="key" type="Object" />

										if (value == null) return value;

										if (value.enum) return value.valueOf()

										return value;
									}
								)
							),
							style:{display:"none", width:"778px", height:"410px"},
						},
						{load:this._loaded}
					)
				
				this.display = iFrame.toggleDisplay.bind(iFrame) //remove bind!
			}


		$.class.instantiate2(this)
	}
)
