﻿///
///<reference path="$CommissioningDashboard.js" />

$.class
(
	{
		namespace: window,
		statics:
		{
			ajaxDisableDiv:HTMLDivElement.VAR,
		},
	},	

	function CommissioningDashboard()
	{
		var $webForm = $.asp.webForm;

		var $cstr = this.constructor;

		this.performanceData		= null;	
		this.bid					= Number.VAR;
		this.equipmentClassID		= Number.VAR;
		this.eid					= Number.VAR;
		this.equipmentClassChanged	= $.Event;
		this.eidChanged				= $.Event;
		this._asyncPostbackManager	= $webForm.AsyncPostbackManager.VAR;
		this._buildingScrollerId	= "rrBuildingScroller";
					
		this.init =
			function()
			{
				this._asyncPostbackManager = new $webForm.AsyncPostbackManager(HTMLElement.getById(/radUpdatePanelMainPanel$/).id, "ctl00_plcCopy_ctl00")

				if (this._asyncPostbackManager.isInRequest)
				{
					this._waitForUpdatePanelLoad()
				}
				else
				{
					if (HTMLSelectElement.getById(/ddlBuilding$/).value != "-1")
					{
						this._asyncPostbackManager_pageLoaded(this._buildingScrollerId)
					}

					this._asyncPostbackManager.pageLoading = this._asyncPostbackManager_pageLoading;
					this._asyncPostbackManager.pageLoaded  = this._asyncPostbackManager_pageLoaded;
				}
			}

		this._waitForUpdatePanelLoad =
			function()
			{
				if (this._asyncPostbackManager.isInPostback)
				{	
					arguments.callee.applyAsync(this)

					return;
				}

				this._asyncPostbackManager.pageLoading = this._asyncPostbackManager_pageLoading;
				this._asyncPostbackManager.pageLoaded  = this._asyncPostbackManager_pageLoaded;

				this._asyncPostbackManager_pageLoaded(this._buildingScrollerId)
			}

		this._asyncPostbackManager_pageLoading =
			function()
			{
				var rad = HTMLElement.getById(/radUpdatePanelMain$/)

				if (!rad) return;

				$cstr.ajaxDisableDiv = rad.clone()

				this._asyncPostbackManager.pageLoading.detach(arguments.callee)
			}
			
		this._asyncPostbackManager_pageLoaded =
			function(targetElementId)
			{
				switch (targetElementId.split("$").getLast())
				{
					case this._buildingScrollerId:
					case "ddlBuilding":
					{
						var val = HTMLSelectElement.getById(/ddlBuilding$/).value;

						if (val == "-1") break; //remove?

						this.bid = ((val == "all") ? null : parseInt(val))
					}
					case "btnPerformance":
					{
						this.equipmentClassChanged.detachAll()
						this.eidChanged			  .detachAll()

						this.performanceData = new CommissioningDashboard.PerformanceData(this)
					
						break;	                
					}
					case "ddlPerformanceEquipmentClass":
					{
						var ddlPerformanceEquipmentClass = HTMLElement.getById(/ddlPerformanceEquipmentClass$/)

						this.equipmentClassID = (ddlPerformanceEquipmentClass.selectedIndex ? parseInt(ddlPerformanceEquipmentClass.value) : null)
					
						this.equipmentClassChanged.raise(this.equipmentClassID)
					
						break;
					}
					case "ddlPerformanceEquipment":
					{
						var ddlPerformanceEquipment = HTMLElement.getById(/ddlPerformanceEquipment$/)

						this.eid = (ddlPerformanceEquipment.selectedIndex ? parseInt(ddlPerformanceEquipment.value) : null)
					
						this.eidChanged.raise(this.eid)
					
						break;
					}
				}
			}


		$.class.instantiate2(this)
	}
)
