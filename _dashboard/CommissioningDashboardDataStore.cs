﻿using CW.Business;
using CW.Logging;
using CW.Data.Interfaces.State;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CW.Caching;

namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
		#region CLASS

			public class SyncWithClientSideAttribute: Attribute
			{
				//Must remain in sync with client side enums
			}

			public class DictionaryManager
			{
				public static Object LockObjectA = new Object(); //revise
				public static Object LockObjectB = new Object(); //revise
				public static Object LockObjectC = new Object(); //revise
			}

			public class DictionaryManager<T>: DictionaryManager
			{
				private readonly Object	 lockObject;

				private readonly Func<T> tryGetData;
				private readonly Action	 getData;

				public DictionaryManager(Object lockObject, Func<T> tryGetData, Action getData)
				{
					this.lockObject = lockObject;

					this.tryGetData = tryGetData;
					this.getData	= getData;
				}

				public T GetItem()
				{
					tryGetData:

					T data = tryGetData();

					if (data == null)
					{
						lock (lockObject)
						{
							data = tryGetData();

							if (data == null)
							{
								getData();
							}

							goto tryGetData;
						}
					}

					return data;
				}
			}

		#endregion

		#region CONST

			private const String FILTERED_DATA = "CommisionDashboardFilteredData";

		#endregion

		#region field

			private readonly ISiteUser    siteUser;
			private readonly ICacheStore cacheStore;
            private readonly ISectionLogManager logger;
            private readonly DataManager dataManager;

		#endregion

		#region constructor

			public CommissioningDashboardDataStore(ISiteUser siteUser, ICacheStore cacheStore, DataManager dm, ISectionLogManager logger)
			{
				this.siteUser    = siteUser;
                this.logger      = logger;
                this.cacheStore = cacheStore;
                this.dataManager = dm;
			}

		#endregion

		#region method

			public IDictionary<SeriesInfo,IList<ISeriesPointInfo>> GetFilteredData(Int32 cid, FilteredDataKey dataKey)
			{
				var filteredData =
					new DictionaryManager<IDictionary<Int32,IDictionary<FilteredDataKey,IDictionary<SeriesInfo,IList<ISeriesPointInfo>>>>>
						(
							DictionaryManager.LockObjectA,
							()=>{return cacheStore.Get<IDictionary<Int32,IDictionary<FilteredDataKey,IDictionary<SeriesInfo,IList<ISeriesPointInfo>>>>>(FILTERED_DATA);},
							()=>{cacheStore.Set(FILTERED_DATA, new Dictionary<Int32,IDictionary<FilteredDataKey,IDictionary<SeriesInfo,IList<ISeriesPointInfo>>>>());}
						
						).GetItem();

				var filteredDataByClient =
					new DictionaryManager<IDictionary<FilteredDataKey,IDictionary<SeriesInfo,IList<ISeriesPointInfo>>>>
						(
							DictionaryManager.LockObjectB,
							()=>
							{
								IDictionary<FilteredDataKey,IDictionary<SeriesInfo,IList<ISeriesPointInfo>>> data;

								filteredData.TryGetValue(cid, out data);

								return data;
							},
							()=>{filteredData.Add(cid, new Dictionary<FilteredDataKey,IDictionary<SeriesInfo,IList<ISeriesPointInfo>>>());}
						
						).GetItem();			

				return
				new DictionaryManager<IDictionary<SeriesInfo,IList<ISeriesPointInfo>>>
					(
						DictionaryManager.LockObjectC,
						()=>
						{
							IDictionary<SeriesInfo,IList<ISeriesPointInfo>> data;

							filteredDataByClient.TryGetValue(dataKey, out data);
							
							return data;
						},
						()=>{filteredDataByClient.Add(dataKey, createSeriesInfoDictionary(dataKey));}
						
					).GetItem();
			}

			public IDictionary<SeriesInfo,IList<ISeriesPointInfo>> createSeriesInfoDictionary(FilteredDataKey dataKey)
			{
				return
				(IDictionary<SeriesInfo,IList<ISeriesPointInfo>>)GetType()
				.GetMethod("doCompute", (BindingFlags.Instance|BindingFlags.NonPublic))
				.MakeGenericMethod((dataKey.ChartTypeGrouping == ChartTypeGroupingEnum.Date) ? new[]{typeof(DateTime)} : new[]{typeof(Int32)})
				.Invoke(this, new Object[]{new DiagnosticComputeEngine(dataManager, siteUser, logger), dataKey});
			}

			private IDictionary<SeriesInfo,IList<ISeriesPointInfo>> doCompute<T>(DiagnosticComputeEngine dce, FilteredDataKey dataKey)
			{
				return dce.Compute<T>(dataKey).ToDictionary(_=>_.Key, _=>(IList<ISeriesPointInfo>)_.Value.Select(__=>(ISeriesPointInfo)__).ToList());
			}
							
		#endregion

	}
}