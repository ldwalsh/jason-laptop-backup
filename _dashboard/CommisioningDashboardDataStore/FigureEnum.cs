﻿
namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public enum FigureEnum
        {
			None,
            Circle,
            Cross,
            Diamond,
            Ellipse,
            Rectangle,
            Star3,
            Star5,
            Star6,
            Star7,
            Triangle,
        }
    }
}