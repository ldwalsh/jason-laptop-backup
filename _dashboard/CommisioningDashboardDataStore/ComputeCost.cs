﻿using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
		public class ComputeCost: ComputeBase<DateTime>
		{
			#region field

				private Double costSum;
				private Int32  lcid;

			#endregion

			#region constructor

                public ComputeCost(FilteredDataKey dataKey, DateTime startDate, DateTime endDate): base(dataKey, startDate, endDate)
				{
                    plots.Add
                    (
                        new CostSeriesInfo(){ColorHex =ColorHelper.SEColors.SpruceGreen, PointFigure= FigureEnum.Circle, BID= dataKey.BID.Value},
                        new List<SeriesPointInfo<DateTime>>()
                    );
				}

			#endregion

			#region method

				public override void RangeBegin(DateTime date)
				{
					costSum = 0;
				}

				public override void NextResult(DiagnosticsResult diagnosticResult)
				{
					costSum += diagnosticResult.CostSavings;

					((CostSeriesInfo)plots.Last().Key).Total += diagnosticResult.CostSavings;
				}

				public override void LastRangeResult(DiagnosticsResult dr)
				{
					lcid = dr.LCID;
				}

				public override void RangeEnd(DateTime date, Int32 index)
				{
					var dateString = date.ToShortDateString();
					var plotRange  = DataKey.PlotRange;

                    if (lcid == 0) return;

					plots.First().Value.Add
					(
						new SeriesPointInfo<DateTime>(date, costSum, DiagnosticComputeEngine.GetDateStep(plotRange, index))
							{
								FigureTip	= String.Format("x: {0} y: {1}", new[]{DiagnosticComputeEngine.GetFigureTipDate(plotRange, date), CultureHelper.FormatCurrencyAsString(lcid, costSum)}),
								Data		= String.Join("|", String.Empty, dateString, dateString),
								XFormat		= DiagnosticComputeEngine.GetShorterDateFormat(plotRange),
								XLabelTip	= DiagnosticComputeEngine.GetXLabelTipDate(plotRange, date),
							}
					);
				}

			#endregion
		}
	}
}