﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public class UnfilteredDataKey
        {
            #region CLASS

                public enum StateEnum
                {
                    Loading,
                    Loaded
                }

            #endregion

            #region constructor

                public UnfilteredDataKey(Int32 cid, IEnumerable<Int32> bids)
                {
                    this.cid = cid;
                    this.bids = bids;

					CreatedStamp = DateTime.UtcNow;
                }

            #endregion

            #region method

				public override Int32 GetHashCode()
				{
					var hash = (cid.GetHashCode() * 1000);
                    
					foreach (var bid in bids)
					{
						hash += bid.GetHashCode();
					}

					return hash;
				}

				public override Boolean Equals(Object obj)
				{
					if (!(obj is UnfilteredDataKey)) return false;

					var k = (UnfilteredDataKey)obj;

					return ((k.cid == cid) && (k.bids.Count() == bids.Count()) && !k.bids.Except(bids).Any());
				}

            #endregion

            #region property

                public DateTime CreatedStamp
                {
                    private set;
                    get;
                }

                public StateEnum State
                {
                    set;
                    get;
                }

                public Int32 cid
                {
                    private set;
                    get;
                }

                public IEnumerable<Int32> bids
                {
                    private set;
                    get;
                }

            #endregion
        }
    }
}