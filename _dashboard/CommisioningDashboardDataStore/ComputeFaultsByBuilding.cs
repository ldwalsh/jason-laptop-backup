﻿using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public class ComputeFaultsByBuilding: ComputeBase<Int32>
        {
            #region field

                private Int32 comfortCount;
                private Int32 energyCount;
                private Int32 maintenanceCount;
                private Int32 totalCount;

				private DiagnosticsResult lastResult;

            #endregion

            #region constructor

                public ComputeFaultsByBuilding(FilteredDataKey dataKey, DateTime startDate, DateTime endDate): base(dataKey, startDate, endDate)
                {
					new Dictionary<String,String>
					{
						{"Unique Faults",	ColorHelper.SEColors.DarkGray		},
						{"Comfort",			ColorHelper.SEColors.FuchsiaRed		},
						{"Energy",			ColorHelper.SEColors.SunflowerYellow},
						{"Maintenance",		ColorHelper.SEColors.SkyBlue		},
					
					}.ToList().ForEach(_=>plots.Add(new SeriesInfo{Name=_.Key, ColorHex=_.Value, PointFigure=FigureEnum.Circle}, new List<SeriesPointInfo<Int32>>()));
                }

            #endregion

            #region method

                #region override

                    public override void RangeBegin(Int32 bid)
                    {
                        totalCount = comfortCount = energyCount = maintenanceCount = 0;
                    }

                    public override void NextResult(DiagnosticsResult dr)
                    {
                        AddResult(dr.ComfortPriority,	  ref comfortCount	  );
                        AddResult(dr.EnergyPriority,	  ref energyCount	  );
                        AddResult(dr.MaintenancePriority, ref maintenanceCount);

                        if ((comfortCount + energyCount + maintenanceCount) == 0) return;

                        totalCount++;
                    }

                    public override void LastRangeResult(DiagnosticsResult dr)
					{
						new[]{totalCount, comfortCount, energyCount, maintenanceCount}
							.Select((v,i)=> new{v=v, i=i})
							.ToList()
							.ForEach
							(
								_=> plots.ElementAt(_.i).Value.Add							
									(
										new SeriesPointInfo<Int32>(dr.BID, _.v)
											{
												FigureTip	= dr.BuildingName,
												XLabel		= StringHelper.TrimText(dr.BuildingName, 12),
												XLabelTip	= dr.BuildingName,
												Data		= String.Join("|", dr.BID, StartDate.ToShortDateString(), EndDate.ToShortDateString()),
											}
									)
							);
                    }

                    public override IDictionary<SeriesInfo,IList<SeriesPointInfo<Int32>>> Complete()
                    {
						//if nothing to plot (all points amount to 0) than return empty collection						
						if (plots.Keys.Sum(_=> plots[_].Sum(__=>__.Y)) == 0) return new Dictionary<SeriesInfo,IList<SeriesPointInfo<Int32>>>();

						var sorted = plots.ElementAt(0).Value.OrderByDescending(_=> _.Y).Take(5).ToList();

						//order all the fault series by joining to sorted list
						plots.Keys.ToList().ForEach(k=> plots[k] = plots[k].Join(sorted, _=> _.X, _=> _.X, (o,i)=> new{o=o, i=i}).OrderByDescending(_=>_.i.Y).Select(_=>_.o).ToList());

                        plots.Values.ToList().ForEach(_=> _.ToList().ForEach(__=> __.FigureTip = String.Format("x: {0} y: {1}", __.FigureTip, __.Y)));
                        
						return base.Complete();
                    }

                #endregion

                private void AddResult(Double priorityValue, ref Int32 count)
                {
                    if (priorityValue == 0) return;

                    count++;
                }

            #endregion
        }
    }
}