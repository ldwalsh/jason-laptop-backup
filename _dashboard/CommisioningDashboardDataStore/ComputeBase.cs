﻿using CW.Data.Models.Diagnostics;
using System;
using System.Collections.Generic;

namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
        public abstract class ComputeBase<T>
		{
			#region field

				public readonly FilteredDataKey DataKey;
				
				protected readonly DateTime StartDate;
				protected readonly DateTime EndDate;

				protected readonly IDictionary<SeriesInfo,IList<SeriesPointInfo<T>>> plots;

			#endregion

			#region constructor

				protected ComputeBase(FilteredDataKey dataKey, DateTime startDate, DateTime endDate)
				{
					DataKey  = dataKey;

                    StartDate = startDate;
                    EndDate   = endDate;

					plots = new Dictionary<SeriesInfo,IList<SeriesPointInfo<T>>>();
				}

			#endregion

			#region method

				public virtual void Init(IEnumerable<DiagnosticsResult> results)
				{
				}
				
				public virtual void RangeBegin(T date)
				{
				}

				public virtual void RangeBegin(T date, Int32 index)
				{
				}

				public virtual void FirstRangeResult(DiagnosticsResult diagnosticResult)
				{
				}

				public virtual void NextResult(DiagnosticsResult diagnosticResult)
				{
				}

				public virtual void LastRangeResult(DiagnosticsResult diagnosticResult)
				{
				}

				public virtual void RangeEnd(T date)
				{
				}

				public virtual void RangeEnd(T date, Int32 index)
				{
				}

				public virtual IDictionary<SeriesInfo,IList<SeriesPointInfo<T>>> Complete()
				{
					return plots;
				}

			#endregion
		}
	}
}