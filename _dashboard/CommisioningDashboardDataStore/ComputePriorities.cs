﻿using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
		public class ComputePriorities: ComputeBase<DateTime>
		{
			#region STATIC

				private static Double calcPointValue(ref Double sum, ref Int32 count)
				{
					return ((sum == 0) ? 0 : Math.Round((sum / count), 1));
				}

			#endregion

			#region field

				private Double comfortSum;
				private Double energySum;
				private Double maintenanceSum;

				private Int32  comfortCount;
				private Int32  energyCount;
				private Int32  maintenanceCount;
				private Int32  totalCount;

				private DiagnosticsResult lastResult;

			#endregion

			#region constructor

				public ComputePriorities(FilteredDataKey dataKey, DateTime startDate, DateTime endDate): base(dataKey, startDate, endDate)
				{
					new Dictionary<String,String>
						{
							{"Total",		ColorHelper.SEColors.DarkGray		},
							{"Comfort",		ColorHelper.SEColors.FuchsiaRed		},
							{"Energy",		ColorHelper.SEColors.SunflowerYellow},
							{"Maintenance",	ColorHelper.SEColors.SkyBlue		},
						
						}.ToList().ForEach(_=>plots.Add(new SeriesInfo{Name=_.Key, ColorHex=_.Value, PointFigure=FigureEnum.Circle}, new List<SeriesPointInfo<DateTime>>()));
				}

			#endregion

			#region method

				#region override

					public override void RangeBegin(DateTime date)
					{
						comfortSum = energySum = maintenanceSum = 0;

						comfortCount = energyCount = maintenanceCount = totalCount = 0;
					}

					public override void NextResult(DiagnosticsResult dr)
					{
						AddResult(dr.ComfortPriority, ref comfortSum, ref comfortCount);
						AddResult(dr.EnergyPriority, ref energySum, ref energyCount);
						AddResult(dr.MaintenancePriority, ref maintenanceSum, ref maintenanceCount);
					}

					public override void LastRangeResult(DiagnosticsResult dr)
					{
						lastResult = dr;
					}

					public override void RangeEnd(DateTime date, Int32 index)
					{
						if (lastResult == null) return;

						var c = calcPointValue(ref comfortSum,	   ref comfortCount	   );
						var e = calcPointValue(ref energySum,	   ref energyCount	   );
						var m = calcPointValue(ref maintenanceSum, ref maintenanceCount);

						var lcid = lastResult.LCID;

						plots.ElementAt(0).Value.Add(createPointSeriesInfo(date, ((totalCount == 0) ? 0 : (c + e + m)), lcid, index));

						new[]{c, e, m}.Select((v,i)=> new{v=v, i=i}).ToList().ForEach(_=>plots.ElementAt(_.i+1).Value.Add(createPointSeriesInfo(date, _.v, lcid, index)));
					}

					public override IDictionary<SeriesInfo,IList<SeriesPointInfo<DateTime>>> Complete()
					{
						if (plots.ElementAt(0).Value.Sum(_=>_.Y) == 0)
						{
							plots.Clear();
						}

						return base.Complete();
					}

				#endregion

				private void AddResult(Double priorityValue, ref Double sum, ref Int32 count)
				{
					if (priorityValue == 0) return;

					sum += priorityValue;

					count++;

					totalCount++;
				}

				private SeriesPointInfo<DateTime> createPointSeriesInfo(DateTime date, Double value, Int32 lcid, Int32 index)
				{
					var plotRange	   = DataKey.PlotRange;
					var dateString	   = date.ToShortDateString();

					return
					new SeriesPointInfo<DateTime>(date, value, DiagnosticComputeEngine.GetDateStep(plotRange, index))
						{
							FigureTip	= String.Format("x: {0} y: {1}", new[]{DiagnosticComputeEngine.GetFigureTipDate(plotRange, date), CultureHelper.FormatNumber(lcid, value)}),
							Data		= String.Join("|", "", dateString, dateString),
							XLabelTip	= DiagnosticComputeEngine.GetXLabelTipDate(plotRange, date),
							XFormat		= DiagnosticComputeEngine.GetShorterDateFormat(plotRange),
						};
				}

			#endregion
		}
	}
}