﻿using System;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public struct PlotInfoo
        {
            public ChartTypeEnum ChartType { set; get; }

            public ChartTypeGroupingEnum ChartTypeGrouping { set; get; }

            public PlotTypeEnum PlotType { set; get; }

            public PlotRangeEnum PlotRange { set; get; }
        }
    }
}