﻿using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public class ComputePortfolioCostByBuilding: ComputeBase<Int32>
        {
            #region field

                private readonly IDictionary<Int32,KeyValuePair<SeriesInfo,IList<SeriesPointInfo<Int32>>>> costSeries;
				private readonly IDictionary<Int32,Int32>												   lookupLCID;

            #endregion

            #region constructor

				public ComputePortfolioCostByBuilding(FilteredDataKey dataKey, DateTime startDate, DateTime endDate): base(dataKey, startDate, endDate)
				{
                    costSeries = new Dictionary<Int32,KeyValuePair<SeriesInfo,IList<SeriesPointInfo<Int32>>>>();
					lookupLCID = new Dictionary<Int32,Int32>();

                    var seriesInfo = new SeriesInfo(ChartTypeGroupingEnum.Building){ColorHex = ColorHelper.SEColors.SpruceGreen};

                    costSeries.Add(0, new KeyValuePair<SeriesInfo,IList<SeriesPointInfo<Int32>>>(seriesInfo, new List<SeriesPointInfo<Int32>>()));
				}

			#endregion

            #region method

                #region override

                    public override void NextResult(DiagnosticsResult dr)
                    {
                        if (!costSeries[0].Value.Any() || (costSeries[0].Value.Last().X != dr.BID))
                        {
                            costSeries[0].Value.Add
							(
								new SeriesPointInfo<Int32>(dr.BID, 0)
									{
										FigureTip	= dr.BuildingName,										
										XLabel		= StringHelper.TrimText(dr.BuildingName, 12),
										XLabelTip	= dr.BuildingName,
										YLabel		= CultureHelper.GetCurrencyISOSymbol(dr.LCID),
										Data		= String.Join("|", dr.BID, StartDate.ToShortDateString(), EndDate.ToShortDateString()),
									}
							);

							lookupLCID.Add(dr.BID, dr.LCID);
                        }

                        costSeries[0].Value.Last().Y = (costSeries[0].Value.Last().Y + dr.CostSavings);
                    }

                    public override IDictionary<SeriesInfo,IList<SeriesPointInfo<Int32>>> Complete()
                    {                        
						if (costSeries[0].Value.Any())
                        {
							var pl = costSeries[0].Value.OrderByDescending(_=>_.Y).Take(10);

							foreach (var item in pl)
							{
								item.FigureTip = String.Format("x: {0} y: {1}", item.FigureTip, CultureHelper.FormatCurrencyAsString(lookupLCID[Convert.ToInt32(item.X)], item.Y));
							}

                            plots.Add(costSeries[0].Key, pl.ToList());
                        }

                        return base.Complete();
                    }

                #endregion

            #endregion
        }
    }
}