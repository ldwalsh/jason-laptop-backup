﻿using System;
using System.Drawing;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
		public interface ISeriesPointInfo
		{
			String XString			{get;}
			String XLabel			{get;}
			String XLabelTip		{get;}

			Boolean ShowXAxisLabel	{get;}

			Double Y				{get;}
			String YLabel			{get;}

			Color  Color			{get;}
			String FigureTip		{get;}
			String Url				{get;}
			String Data				{get;}
				
			FigureEnum Figure		{get;} //http://www.telerik.com/community/forums/aspnet-ajax/chart/radchart-custom-figure-for-pointmark.aspx			
		}
	}
}