﻿using System;
using System.Collections.Generic;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public struct FilteredDataKey
        {
			#region field

				public Int32? BID{set;get;}

				public Int32? EquipmentClassID{set;get;}

				public Int32? EID{set;get;}

				public ChartTypeEnum ChartType {set;get;}

				public ChartTypeGroupingEnum ChartTypeGrouping { set; get; }

				public PlotTypeEnum PlotType {set;get;}

				public PlotRangeEnum PlotRange {set;get;}

				public String DateKey { set; get; }

			#endregion

			#region method

				public Int32 GetRangeValue()
				{
					return Int32.Parse(PlotRange.ToString().Substring(4,2)); //revise to be less brittle
				}

				public String GetRangeInterval()
				{
					var s = PlotRange.ToString().ToLower();

					return (s.Contains("days") ? "Daily" : (s.Contains("weeks") ? "Weekly" : "Monthly"));
				}

			#endregion
        }
    }
}