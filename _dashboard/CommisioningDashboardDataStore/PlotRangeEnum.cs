﻿
namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
		[
		SyncWithClientSide
		]
		public enum PlotRangeEnum
		{
			Past30Days,
			Past60Days,
			Past90Days,
			Past10Weeks,
			Past25Weeks,
			Past52Weeks,
			Past12Months,
			Past24Months,
			Past36Months,
		}
	}
}