﻿using System;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public class CostSeriesInfo: SeriesInfo //this class can be deprecated... the particular Compute Cost implementation can handle internally
        {
			#region field

				public Int32  BID;
				public Double Total;

			#endregion

			#region constructor

				public CostSeriesInfo(): base(ChartTypeEnum.Cost)
				{
					Total = 0;
				}

			#endregion
        }
    }
}