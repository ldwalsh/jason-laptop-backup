﻿
namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
		[
		SyncWithClientSide
		]
		public enum ChartTypeEnum
		{
			Faults,
			Priorities,
			Cost,
		}
	}
}