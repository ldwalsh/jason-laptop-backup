﻿using System;
using System.Drawing;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public class SeriesPointInfo<T>: ISeriesPointInfo
        {
			#region field

				public T X;

			#endregion

			#region constructor

				public SeriesPointInfo(T x, Double y, Boolean showXAxisLabel = true)
				{
					X = x;
					Y = y;

					ShowXAxisLabel = showXAxisLabel;

					switch (typeof(T).Name)
					{
						case "DateTime":
						{
							XLabel = ((DateTime)(Object)x).ToShortDateString();

							break;
						}
						case "Int32":
						{
							XLabel = Convert.ToInt32(x).ToString();

							break;
						}
						default: throw new Exception();
					}
				}

			#endregion

			#region property

				public String XLabel			{get;set;}
				public String XLabelTip			{get;set;}

				public Boolean ShowXAxisLabel	{get;set;}

				public Double Y					{get;set;}
				public String YLabel			{get;set;}

				public Color  Color				{get;set;}
				public String FigureTip			{get;set;}
				public String Url				{get;set;}
				public String Data				{get;set;}
				
				public FigureEnum Figure		{get;set;} //http://www.telerik.com/community/forums/aspnet-ajax/chart/radchart-custom-figure-for-pointmark.aspx			

				public String XString
				{
					get	{return X.ToString();}
				}

				public String XFormat
				{
					set
					{
						switch (typeof(T).Name)
						{
							case "DateTime":
							{
								XLabel = ((DateTime)(Object)X).ToString(value);

								break;
							}

							default: throw new NotImplementedException();
						}
					}
				}

			#endregion
        }
    }
}