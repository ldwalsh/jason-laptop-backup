﻿using CW.Business;
using CW.Common.Constants;
using CW.Logging;
using CW.Data.Models.Diagnostics;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
		public class DiagnosticComputeEngine
		{
			#region CLASS

				public class GetDiagnosticResultsArgs
				{
					public DateTime DateStart;
					public DateTime DateEnd;

					public DataConstants.AnalysisRange AnalysisRange;
				}

			#endregion

			#region STATIC

				private static DateTime IncrementDate(DateTime dateTime, DataConstants.AnalysisRange analysisRange)
				{
					switch (analysisRange)
					{
						case DataConstants.AnalysisRange.Daily:	  return dateTime.AddDays  (1);
						case DataConstants.AnalysisRange.Weekly:  return dateTime.AddDays  (7);
						case DataConstants.AnalysisRange.Monthly: return dateTime.AddMonths(1);
					}

					throw new Exception();
				}

				public static Boolean GetDateStep(PlotRangeEnum plotRange, Int32 index) //rename/revise
				{
					switch (plotRange)
					{
						case PlotRangeEnum.Past30Days:	 return ((index % 3) == 0);
						case PlotRangeEnum.Past60Days:	 return ((index % 6) == 0);
						case PlotRangeEnum.Past90Days:	 return ((index % 9) == 0);

						case PlotRangeEnum.Past10Weeks:	 return true;
						case PlotRangeEnum.Past25Weeks:
						case PlotRangeEnum.Past52Weeks:	 return ((index % 4) == 0);
						
						case PlotRangeEnum.Past12Months: return true;
						case PlotRangeEnum.Past24Months:
						case PlotRangeEnum.Past36Months: return ((index % 2) == 0);
					}

					throw new Exception();
				}

				public static String GetShorterDateFormat(PlotRangeEnum plotRange)
				{
					switch (plotRange)
					{
						case PlotRangeEnum.Past30Days:
						case PlotRangeEnum.Past60Days:
						case PlotRangeEnum.Past90Days:	 return DateTimeFormatInfo.CurrentInfo.ShortDatePattern;

						case PlotRangeEnum.Past10Weeks:
						case PlotRangeEnum.Past25Weeks:
						case PlotRangeEnum.Past52Weeks:	 return DateTimeFormatInfo.CurrentInfo.ShortDatePattern;
						
						case PlotRangeEnum.Past12Months: return "MMM";
						case PlotRangeEnum.Past24Months:
						case PlotRangeEnum.Past36Months: return "MMM yy";
					}

					throw new Exception();
				}

				public static String GetFigureTipDate(PlotRangeEnum range, DateTime date)
				{
					switch (range)
					{
						case PlotRangeEnum.Past30Days:
						case PlotRangeEnum.Past60Days:
						case PlotRangeEnum.Past90Days:	 return date.ToShortDateString();

						case PlotRangeEnum.Past10Weeks:
						case PlotRangeEnum.Past25Weeks:
						case PlotRangeEnum.Past52Weeks:	 return (date.ToShortDateString() + " " + date.AddDays(6).ToShortDateString());

						case PlotRangeEnum.Past12Months:
						case PlotRangeEnum.Past24Months:
						case PlotRangeEnum.Past36Months: return date.ToString("MMMM yyyy");
					}

					throw new Exception();
				}

				public static String GetXLabelTipDate(PlotRangeEnum range, DateTime date)
				{
					switch (range)
					{
						case PlotRangeEnum.Past30Days:
						case PlotRangeEnum.Past60Days:
						case PlotRangeEnum.Past90Days:	 return null;

						case PlotRangeEnum.Past10Weeks:
						case PlotRangeEnum.Past25Weeks:
						case PlotRangeEnum.Past52Weeks:	 return (date.ToShortDateString() + " " + date.AddDays(6).ToShortDateString());

						case PlotRangeEnum.Past12Months:
						case PlotRangeEnum.Past24Months:
						case PlotRangeEnum.Past36Months: return date.ToString("MMMM yyyy");
					}

					throw new Exception();
				}

			#endregion

			#region field

				private readonly DataManager dataManager;
				private readonly ISiteUser	 siteUser;
                private readonly ISectionLogManager logMgr;

			#endregion

			#region constructor

				public DiagnosticComputeEngine(DataManager dataManager, ISiteUser siteUser, ISectionLogManager logMgr)
				{
					this.dataManager = dataManager;
					this.siteUser	 = siteUser;
                    this.logMgr      = logMgr;
				}

			#endregion

			#region method

				private IEnumerable<DiagnosticsResult> GetDiagnosticResults(FilteredDataKey dataKey, GetDiagnosticResultsArgs args)
				{
					if (!siteUser.VisibleBuildings.Any()) return Enumerable.Empty<DiagnosticsResult>();

					args.AnalysisRange = (DataConstants.AnalysisRange)Enum.Parse(typeof(DataConstants.AnalysisRange), dataKey.GetRangeInterval());
						
					Func<DateTime,Int32,DateTime> getXAgoDelegate = null;
					Func<DateTime,DateTime>		  getEndDate	  = null;

					switch (args.AnalysisRange)
					{
						case DataConstants.AnalysisRange.Daily:
						{
							getXAgoDelegate = DateTimeHelper.GetXDaysAgo;
                            getEndDate		= (DateTime d) => {return d;};

							break;
						}
						case DataConstants.AnalysisRange.Weekly:
						{
							getXAgoDelegate = DateTimeHelper.GetXWeeksAgo;
                            getEndDate		= DateTimeHelper.GetLastSunday;

							break;
						}
						case DataConstants.AnalysisRange.Monthly:
						{
							getXAgoDelegate = DateTimeHelper.GetXMonthsAgo;
                            getEndDate		= DateTimeHelper.GetFirstOfMonth;

							break;
						}
					}

					var today = new DateTimeHelper(logMgr).ConvertTimeFromUTC
						(
							DateTime.UtcNow,
							((dataKey.BID == null) ? siteUser.VisibleBuildings.First().TimeZone : dataManager.BuildingDataMapper.GetBuildingTimeZoneByBID(dataKey.BID.Value)),
							"ConvertTimeFromUTC not in utc in Commissioning Dashboard GetFilteredData"
														
						).Date;

                    args.DateEnd   = getEndDate(today);
					args.DateStart = getXAgoDelegate(args.DateEnd, dataKey.GetRangeValue());

					return
					dataManager.DiagnosticsDataMapper.GetDashboardPerformance
					(
                        new DiagnosticsResultsInputs()
                        {
                            AnalysisRange = args.AnalysisRange,
							StartDate	  = args.DateStart,
							EndDate		  = args.DateEnd,
							
							CID			  = siteUser.CID,
                        },
						((dataKey.BID == null) ? siteUser.VisibleBuildings.Select(_=>_.BID).ToList() : new List<Int32>{dataKey.BID.Value}),
						siteUser.IsKGSFullAdminOrHigher,
						siteUser.IsSuperAdminOrFullAdminOrFullUser
					);
				}

                public IDictionary<SeriesInfo,IList<SeriesPointInfo<T>>> Compute<T>(FilteredDataKey dataKey)
				{
                    if (dataKey.ChartTypeGrouping == ChartTypeGroupingEnum.Building)
					{
						dataKey.BID = null;
					}

                    var args	= new GetDiagnosticResultsArgs();
                    var results = GetDiagnosticResults(dataKey, args);

                    Object compute;
                    
					var computeArgs = new Object[]{dataKey, args.DateStart, args.DateEnd};

                    switch (dataKey.ChartType)
                    {
                        case ChartTypeEnum.Cost:
                        {
							compute =
							Activator.CreateInstance
							(
								((dataKey.ChartTypeGrouping == ChartTypeGroupingEnum.Building) ?
									typeof(ComputePortfolioCostByBuilding) : ((dataKey.BID == null) ? typeof(ComputePortfolioCost) : typeof(ComputeCost))), //revise
								computeArgs
							);

                            break;
                        }
                        case ChartTypeEnum.Faults:
                        {
							compute =
							Activator.CreateInstance
							(
								((dataKey.ChartTypeGrouping == ChartTypeGroupingEnum.Building) ? typeof(ComputeFaultsByBuilding) : typeof(ComputeFaults)),
								computeArgs
							);

							break;
                        }
                        case ChartTypeEnum.Priorities:
                        {
                            compute = Activator.CreateInstance(typeof(ComputePriorities), computeArgs);

                            break;
                        }
                        default: throw new Exception();
                    }

                    switch (dataKey.PlotType)
                    {
                        case PlotTypeEnum.EquipmentClass:
                        {
                            results = results.Where(_=> (_.EquipmentClassID == dataKey.EquipmentClassID));
                                
							break;
                        }
                        case PlotTypeEnum.Equipment:
                        {
                            results = results.Where(_=> (_.EID == dataKey.EID));

                            break;
                        }
                    }

                    if (dataKey.ChartTypeGrouping != ChartTypeGroupingEnum.Building)
                    {
                        results = results.OrderBy(_=> _.StartDate).ThenBy(_=> _.BID);

						var computeBase = (ComputeBase<DateTime>)compute;

						if (!results.Any()) goto complete;

                        var currentDate = args.DateStart;						
						var index		= 0;

                        while (currentDate < results.First().StartDate)
                        {
							index++;

                            computeBase.RangeBegin(currentDate);
							computeBase.RangeBegin(currentDate, index);
                            
							computeBase.RangeEnd(currentDate);
							computeBase.RangeEnd(currentDate, index);

                            currentDate = IncrementDate(currentDate, args.AnalysisRange);
                        }

                        currentDate = DateTime.MinValue;

                        DiagnosticsResult lastResult = null;						

                        foreach (var dr in results)
                        {
							if (dr.StartDate > currentDate)
                            {
                                if (currentDate != DateTime.MinValue)
                                {
                                    computeBase.LastRangeResult(lastResult);
                                    
									computeBase.RangeEnd(currentDate);
									computeBase.RangeEnd(currentDate, index);
                                }

								index++;

                                computeBase.RangeBegin(dr.StartDate);
								computeBase.RangeBegin(dr.StartDate, index);

                                computeBase.FirstRangeResult(dr);
                            }

                            computeBase.NextResult(dr);

                            currentDate = dr.StartDate;
                            lastResult  = dr;
                        }

                        computeBase.LastRangeResult(lastResult);
                        
						computeBase.RangeEnd(currentDate);
						computeBase.RangeEnd(currentDate, index);

                        currentDate = IncrementDate(currentDate, args.AnalysisRange);

                        while (currentDate < args.DateEnd)
                        {
							index++;

                            computeBase.RangeBegin(currentDate);
							computeBase.RangeBegin(currentDate, index);

                            computeBase.RangeEnd(currentDate);
							computeBase.RangeEnd(currentDate, index);

                            currentDate = IncrementDate(currentDate, args.AnalysisRange);
                        }

						complete:

						return (IDictionary<SeriesInfo,IList<SeriesPointInfo<T>>>)computeBase.Complete();
                    }
                    else
                    {
                        results = results.OrderBy(_=> _.BID);

                        var computeBase = (ComputeBase<Int32>)compute;
                       
                        if (!results.Any()) goto complete;

						computeBase.Init(results);

                        DiagnosticsResult lastResult = null;

                        var bid   = 0;						
						var index = 0;

						foreach (var dr in results)
                        {
                            if (dr.BID != bid)
                            {
                                if (bid != 0)
                                {
                                    computeBase.LastRangeResult(lastResult);
                                    
									computeBase.RangeEnd(lastResult.BID);
									computeBase.RangeEnd(lastResult.BID, index);
                                }

								index++;

                                computeBase.RangeBegin(dr.BID);
								computeBase.RangeBegin(dr.BID, index);

                                computeBase.FirstRangeResult(dr);
                            }

                            computeBase.NextResult(dr);
                            
							bid = dr.BID;
                            
							lastResult = dr;
                        }

                        computeBase.LastRangeResult(lastResult);
                        
						computeBase.RangeEnd(lastResult.BID);
						computeBase.RangeEnd(lastResult.BID, index);

						complete:

                        return (IDictionary<SeriesInfo,IList<SeriesPointInfo<T>>>)computeBase.Complete();
                    }
				}

			#endregion
		}
	}
}