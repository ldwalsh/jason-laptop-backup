﻿using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
		public class ComputePortfolioCost: ComputeBase<DateTime>
		{
			#region field

				private readonly CostSeriesInfo costSeriesInfoTemplate;

				private readonly IDictionary<Int32,KeyValuePair<SeriesInfo,IList<SeriesPointInfo<DateTime>>>> costSeries;
				private readonly IDictionary<Int32,Double> costSums;

			#endregion

			#region constructor

                public ComputePortfolioCost(FilteredDataKey dataKey, DateTime startDate, DateTime endDate) : base(dataKey, startDate, endDate)
				{
					costSeriesInfoTemplate = new CostSeriesInfo{PointFigure=FigureEnum.Circle};

					costSeries = new Dictionary<Int32,KeyValuePair<SeriesInfo,IList<SeriesPointInfo<DateTime>>>>();
                    costSums   = new Dictionary<Int32,Double>();
				}

			#endregion

			#region method

                private SeriesInfo CreateSeriesInfo(Int32 cLCID, Int32 bid)
				{
                    var colorPallet = ColorHelper.SEChartingCostColors;
					var seriesIndex = costSeries.Count;

					var si = (CostSeriesInfo)costSeriesInfoTemplate.ToCopy(); //revise

					si.BID		= bid;
					si.ColorHex	= ((seriesIndex < colorPallet.Length) ? colorPallet[seriesIndex] : colorPallet.Last());
                    si.Id		= (si.Name + " LCID:" + cLCID);
					si.Name		= (si.Name + " " + CultureHelper.GetCurrencyISOSymbol(cLCID));

					return si;
				}

				#region override

                    public override void NextResult(DiagnosticsResult dR)
                    {
                        var cLCID = dR.LCID;

                        if (!costSeries.Keys.Contains(dR.LCID))
                        {
                            costSeries.Add(cLCID, new KeyValuePair<SeriesInfo,IList<SeriesPointInfo<DateTime>>>(CreateSeriesInfo(dR.LCID, dR.BID), new List<SeriesPointInfo<DateTime>>()));
                            
							costSums.Add(cLCID, 0);
                        }

                        costSums[cLCID] += dR.CostSavings;

                        ((CostSeriesInfo)costSeries[cLCID].Key).Total += dR.CostSavings;
                    }
				
					public override void RangeEnd(DateTime date, Int32 index)
					{
						var dateString = date.ToShortDateString();
						var plotRange  = DataKey.PlotRange;

						foreach (var c in costSums.Keys.ToList())
						{
							costSeries[c].Value.Add
							(
								new SeriesPointInfo<DateTime>(date, costSums[c], DiagnosticComputeEngine.GetDateStep(plotRange, index))
									{
										FigureTip	= String.Format("x: {0} y: {1}", new[]{DiagnosticComputeEngine.GetFigureTipDate(plotRange, date), CultureHelper.FormatCurrencyAsString(c, costSums[c])}),
										Data		= String.Join("|", String.Empty, dateString, dateString),
										XFormat		= DiagnosticComputeEngine.GetShorterDateFormat(plotRange),
										XLabelTip	= DiagnosticComputeEngine.GetXLabelTipDate(plotRange, date),
									}
							);

							costSums[c] = 0;
						}
					}

					public override IDictionary<SeriesInfo,IList<SeriesPointInfo<DateTime>>> Complete()
					{
						foreach (var cs in costSeries)
						{
							plots.Add(cs.Value);
						}

						return base.Complete();
					}

				#endregion

			#endregion
		}
	}
}