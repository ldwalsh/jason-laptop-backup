﻿using CW.Utility;
using System;
using System.Drawing;

namespace CW.Website.Dashboard
{
    public partial class CommissioningDashboardDataStore
    {
        public class SeriesInfo
        {
            #region field

				#region readonly

					public readonly Enum EnumName;

				#endregion

                private String	id;
				private String	name;

                public Color		LineColor;
                public Color		PointColor;
                public Int32		LineWidth;
                public FigureEnum	PointFigure;

            #endregion

			#region constructor

				public SeriesInfo(){}

				public SeriesInfo(Enum enumName)
				{
					EnumName = enumName;
				}

			#endregion

			#region method

				#region override

					public override Int32 GetHashCode()
					{
						return Id.GetHashCode();
					}

					public override Boolean Equals(Object obj)
					{
						if (!(obj is SeriesInfo)) return false;

						return (Name == ((SeriesInfo)obj).Name);
					}

				#endregion

				public virtual SeriesInfo ToCopy() //revise?
				{
					return (SeriesInfo)MemberwiseClone();
				}

			#endregion
                
            #region property

				public String Id
				{
					set
					{
						id = value;
					}
					get
					{
						if (id == null) return Name;

						return id;
					}
				}
				
				public String Name
				{
					set
					{
						name = value;
					}
					get
					{
						if (String.IsNullOrEmpty(name)) return EnumName.ToString(); //revise?

						return name;
					}
				}

				public String LineColorHex
				{
					set
					{
						LineColor = ColorHelper.ConvertHexToSystemColor(value);
					}
				}

				public String PointColorHex
				{
					set
					{
						PointColor = ColorHelper.ConvertHexToSystemColor(value);
					}
				}

				public String ColorHex
				{
					set
					{
						LineColorHex = value;
						PointColorHex = value;
					}
				}

            #endregion
        }
    }
}