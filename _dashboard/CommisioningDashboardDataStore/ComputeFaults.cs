﻿using CW.Data.Models.Diagnostics;
using CW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website.Dashboard
{
	public partial class CommissioningDashboardDataStore
	{
		public class ComputeFaults: ComputeBase<DateTime>
		{
			#region field

				private Int32  comfortCount;
				private Int32  energyCount;
				private Int32  maintenanceCount;
				private Int32  totalCount;

				DiagnosticsResult lastResult;

			#endregion

			#region constructor

                public ComputeFaults(FilteredDataKey dataKey, DateTime startDate, DateTime endDate): base(dataKey, startDate, endDate)
				{
					new Dictionary<String,String>()
						{
							{"Unique Faults",	ColorHelper.SEColors.DarkGray		},
							{"Comfort",			ColorHelper.SEColors.FuchsiaRed		},
							{"Energy",			ColorHelper.SEColors.SunflowerYellow},
							{"Maintenance",		ColorHelper.SEColors.SkyBlue		},
						
						}.ToList().ForEach(_=> plots.Add(new SeriesInfo{Name=_.Key, ColorHex=_.Value, PointFigure=FigureEnum.Circle}, new List<SeriesPointInfo<DateTime>>()));
				}

			#endregion

			#region method

				#region override

					public override void RangeBegin(DateTime date)
					{
						totalCount = comfortCount = energyCount = maintenanceCount = 0;		
					}

					public override void NextResult(DiagnosticsResult dr)
					{
						AddResult(dr.ComfortPriority, ref comfortCount);
						AddResult(dr.EnergyPriority, ref energyCount);
						AddResult(dr.MaintenancePriority, ref maintenanceCount);

						if ((comfortCount + energyCount + maintenanceCount) == 0) return;
							
						totalCount++;
					}

					public override void LastRangeResult(DiagnosticsResult dr)
					{
						lastResult = dr;
					}

					public override void RangeEnd(DateTime date, Int32 index)
					{
						if (lastResult == null) return;

						new[]{totalCount, comfortCount, energyCount, maintenanceCount}
							.Select((v,i)=> new{v=v, i=i})
							.ToList()
							.ForEach(_=>plots.ElementAt(_.i).Value.Add(createPointSeriesInfo(date, _.v, lastResult.LCID, index)));
					}

					public override IDictionary<SeriesInfo,IList<SeriesPointInfo<DateTime>>> Complete()
					{
						return (plots.ElementAt(0).Value.Any() ? base.Complete() : new Dictionary<SeriesInfo,IList<SeriesPointInfo<DateTime>>>());
					}

				#endregion

				private void AddResult(Double priorityValue, ref Int32 count)
				{
					if (priorityValue == 0) return;

					count++;
				}

				private SeriesPointInfo<DateTime> createPointSeriesInfo(DateTime date, Double value, Int32 lcid, Int32 index)
				{
					var dateString = date.ToShortDateString();
					var plotRange  = DataKey.PlotRange;
					
					return
					new SeriesPointInfo<DateTime>(date, value, DiagnosticComputeEngine.GetDateStep(plotRange, index))
						{
							FigureTip	= String.Format("x: {0} y: {1}", new[]{DiagnosticComputeEngine.GetFigureTipDate(plotRange, date), CultureHelper.FormatNumber(lcid, value, false)}),
							Data		= String.Join("|", String.Empty, dateString, dateString),
							XFormat		= DiagnosticComputeEngine.GetShorterDateFormat(plotRange),
							XLabelTip	= DiagnosticComputeEngine.GetXLabelTipDate(plotRange, date),
						};
				}

			#endregion
		}
	}
}