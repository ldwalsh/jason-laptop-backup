﻿using System;
using JSAsset;
using CW.Website.DependencyResolution;
using CW.Common.Config;

namespace CW.Website.Dashboard
{
	public class CommissioningDashboardAsset: EmbeddedAssetBase
	{
		public override String RelativePath
		{
			get {return @"_dashboard/script/";}
		}

		public override String[] Dependencies
		{
			get {return new[]{"iCL"};}
		}

        public override String InitFunction
        {
            get {return "loadCommissioningDashboard";}
        }

		public override string ScriptRelativePath
		{
			get {return "_dashboard/";}
		}

        public override Int64 Version
        {
            get {return Int64.Parse(IoC.Resolve<IConfigManager>().GetConfigurationSetting("versionNumber", new DateTime().Ticks.ToString()).Replace(".", String.Empty));}
        }
    }
}