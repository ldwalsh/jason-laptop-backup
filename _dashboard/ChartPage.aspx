﻿<%@ Page Language="C#" EnableViewState="false" AutoEventWireup="false" CodeBehind="ChartPage.aspx.cs" Inherits="CW.Website.Dashboard.ChartPage" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

  <head>
    <title></title>
    <asp:Literal ID="litScripts" runat="server" />
  </head>

  <body style="margin:0px;">
    <form runat="server"> 

    <telerik:RadScriptManager ID="ScriptManager" EnableScriptCombine="true" EnableCdn="true" EnableScriptGlobalization="true" AsyncPostBackTimeout="600" runat="server" />

    <telerik:RadChart runat="server" ID="chart" EnableHandlerDetection="false" />
    <asp:HiddenField id="hdnCostTotal" runat="server" ClientIDMode="Static" />                

    </form>
  </body>

</html>