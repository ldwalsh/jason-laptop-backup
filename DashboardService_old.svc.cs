﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;
using System.Web.UI;


using CW.Data;
using CW.Business;
using CW.Utility;
using CW.Data.AzureStorage.Models;
using CW.Data.AzureStorage;
using CW.Website._framework;
using Microsoft.WindowsAzure;

using Microsoft.Samples.ServiceHosting.AspProviders;
using CW.Data.AzureStorage.Helpers;
using System.Windows.Controls;
using CW.Data.AzureStorage.DataContexts.Blob;
using CW.Data.AzureStorage.Models.Blob;
using System.Configuration;
//using Microsoft.WindowsAzure.StorageClient;


namespace CW.Website
{
	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class DashboardService : System.Web.UI.Page, IDashboardService
    {
        #region Properties
            private DataManager mDataManager = null;
            private CloudStorageAccount mAccount = null;
            const string noBuildingImage = "no-building-image.png";
        #endregion

        public DashboardService()
        {
            mDataManager = new DataManager();
        }

        #region Client Methods

        //DEPRICATED RAR
        //List<List<object>> IDashboardService.GetAllClientData(string userRoleID)
        //{
        //    List<List<object>> result = new List<List<object>>();
        //    IEnumerable<Client> clients = ClientDataMapper.GetAllClients();

        //    foreach (Client c in clients)
        //    {
        //        List<object> clientData = new List<object>();
        //        clientData.Add(c.ClientName);
        //        clientData.Add(c.CID);
        //        result.Add(clientData);
        //    }
        //    return result;
        //}

        //DEPRICATED RAR
        //List<List<object>> IDashboardService.GetAllProviderAdminClientData(int uid)
        //{
        //    List<List<object>> result = new List<List<object>>();

        //    //RAR commented out for build
        //    //IEnumerable<ClientDataMapper.GetClientsDataViewFormat> clients = ClientDataMapper.GetAllActiveClientsAssociatedToUserID(Convert.ToInt32(uid));

        //    //foreach (ClientDataMapper.GetClientsDataViewFormat c in clients)
        //    //{
        //    //    List<object> clientData = new List<object>();
        //    //    clientData.Add(c.ClientName);
        //    //    clientData.Add(c.CID);
        //    //    result.Add(clientData);
        //    //}
        //    return result;
        //}

        //DEPRICATED RAR
        //Client IDashboardService.GetClientData(string cid)
        //{
        //    return ClientDataMapper.GetClient(Convert.ToInt32(cid));
        //}

        #endregion

        #region Building Methods


        IEnumerable<Building> IDashboardService.GetBuildingData()
        {
            //NO ACCESS TO SESSION FROM HERE
            //return SiteUser.current.VisibleBuildings;

            //DEPRICATED RAR
            //return BuildingDataMapper.GetAllVisibleBuildingsByClientID(Convert.ToInt32(clientID));

            return null;
        }

        BuildingDataMapper.GetBuildingsData IDashboardService.GetFullBuildingData(int buildingID)
        {
            return BuildingDataMapper.GetFullBuildingByID(buildingID).First();
        }

        List<object> IDashboardService.GetBuildingImage(int buildingID, string imageExtension, string buildingName )
        {
            List<object> result = new List<object>();
            mAccount = CloudConfiguration.GetStorageAccount(CommonConstants.ATS_STORAGE_KEY);

            BuildingBlobDataContext blobContext = new BuildingBlobDataContext(mAccount, buildingID);
            BuildingBlob buildingBlob = blobContext.GetBlob(BlobHelper.GenerateBuildingProfileImageBlobName(buildingID, imageExtension));

            if (buildingBlob == null)
            {
                result.Add(ConfigurationManager.AppSettings["SilverlightImageAssetPath"] + noBuildingImage);
            }
            else{

                result.Add(buildingBlob.Image);                
            }

            //always add the actual buidling name
            result.Add(buildingName);

            return result;
        }

        #endregion

        #region Equipment Methods

        IEnumerable<EquipmentDataMapper.GetEquipmentData> IDashboardService.GetFullEquipmentData(int equipmentID)
        {
            return EquipmentDataMapper.GetFullEquipmentByID(equipmentID);
        }

        IEnumerable<EquipmentVariableDataMapper.GetEquipmentEquipmentVariableData> IDashboardService.GetAllEquipmentVariablesByEID(int equipmentID)
        {
            return EquipmentVariableDataMapper.GetAllEquipmentVariablesWithDataByEID(equipmentID);
        }

        List<List<object>> IDashboardService.GetAllEquipmentClassData()
        {
            List<List<object>> result = new List<List<object>>();
            IEnumerable<EquipmentClass> ec = EquipmentClassDataMapper.GetAllEquipmentClasses();

            foreach (EquipmentClass c in ec)
            {
                List<object> classes = new List<object>();
                classes.Add(c.EquipmentClassName);
                classes.Add(c.EquipmentClassID);
                result.Add(classes);
            }
            return result;
        }

        List<List<object>> IDashboardService.GetEquipmentClassInBuildingData(int buildingID, int equimentClassID)
        {
            List<List<object>> result = new List<List<object>>();
            IEnumerable<Equipment> equiment = EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentClassID(buildingID, equimentClassID);

            foreach (Equipment e in equiment)
            {
                List<object> i = new List<object>();

                i.Add(e.EquipmentName);
                i.Add(e.EID);
                result.Add(i);
            }

            return result;
        }

        #endregion

        #region Point Methods

        List<EngUnit> IDashboardService.GetPointEngUnit(List<int> PIDs)
        {
            List<EngUnit> eus = new List<EngUnit>();

            foreach (int PID in PIDs)
            {
                Point p = PointDataMapper.GetPointByPID(PID);
                PointType pt = mDataManager.PointTypeDataMapper.GetPointType(p.PointTypeID);
                PointClass pc = PointClassDataMapper.GetPointClass(pt.PointClassID);
                EngUnit eu = EngineeringUnitsDataMapper.GetEngineeringUnit(pc.EngUnitID);
                eus.Add(eu);
            }

            return eus;
        }

        List<string[]> IDashboardService.GetPointInfo(List<int> PIDs)
        {
            List<string[]> points = new List<string[]>();

            //foreach (int PID in PIDs)
            //{
            //    string[] point = new string[4];
            //    PointTable p = PointDataMapper.GetPointByPID(PID);
            //    point[0] = p.PointName;
            //    point[1] = EquipmentDataMapper.GetEquipmentNameByEID(EquipmentPointsDataMapper.GetPartialEquipmentPointByPID(p.PID).EID);

            //    PointType pt = PointTypeDataMapper.GetPointType(p.PointTypeID);
            //    PointClass pc = PointClassDataMapper.GetPointClass(pt.PointClassID);
            //    EngUnit eu = EngineeringUnitsDataMapper.GetEngineeringUnit(pc.EngUnitID);
            //    point[2] = eu.EngUnitID.ToString();
            //    point[3] = eu.EngUnits;

            //    points.Add(point);
            //}

            return points;
        }

        List<string[]> IDashboardService.GetPointInfoByEID(int eid)
        {
            IEnumerable<Point> eqPoints = PointDataMapper.GetAllPointsByEID(eid);
            List<string[]> points = new List<string[]>();

            IEnumerable<PointType> pointTypes = PointTypeDataMapper.GetAllPointTypes();
            IEnumerable<PointClassDataMapper.GetPointClassData> pointClasses = PointClassDataMapper.GetAllFullPointClasses();

            foreach (Point p in eqPoints)
            {
                string[] point = new string[5];
                point[0] = p.PointName;

                PointType pt = pointTypes.Where(t => t.PointTypeID == p.PointTypeID).First();
                PointClassDataMapper.GetPointClassData pc = pointClasses.Where(c => c.PointClassID == pt.PointClassID).First();
                point[1] = pc.PointClassName;
                point[2] = pc.EngUnitID.ToString();
                point[3] = pc.EngUnits;
                point[4] = pt.PointTypeName;

                points.Add(point);
            }

            return points;
        }

        IEnumerable<Point> IDashboardService.GetAllPointsByEID(int eid)
        {
            return PointDataMapper.GetAllVisiblePointsByEID(eid);
        }

        #endregion

        #region Statistic Methods

        List<List<double>> IDashboardService.GetStatistics(int buildingID, int uid, string roleID, int cid, DateTime? startDate)
        {
            List<double> daily = new List<double>();
            List<double> weekly = new List<double>();
            List<double> monthly = new List<double>();
            //is kgs admin or higher
            
            //get building data for the last day                                    
            IEnumerable<PortfolioDataMapper.PortfolioResult> d = mDataManager.PortfolioDataMapper.GetPortfolioResults(CommonConstants.AnalysisRange.Daily, startDate.Value.Date, startDate.Value.AddDays(1).Date, uid, cid, buildingID, null, null, null);
            IEnumerable<PortfolioDataMapper.PortfolioResult> w = mDataManager.PortfolioDataMapper.GetPortfolioResults(CommonConstants.AnalysisRange.Weekly, DateTimeHelper.GetLastSunday(startDate.Value.Date), startDate.Value.AddDays(7).Date, uid, cid, buildingID, null, null, null);
            IEnumerable<PortfolioDataMapper.PortfolioResult> m = mDataManager.PortfolioDataMapper.GetPortfolioResults(CommonConstants.AnalysisRange.Monthly, DateTimeHelper.GetFirstOfMonth(startDate.Value.Date), startDate.Value.AddMonths(1).Date, uid, cid, buildingID, null, null, null);
            daily.Add(d.Count());

            double dcost = 0;
            double denergy = 0;
            double dcomfort = 0;
            double dmaintenance = 0;
            foreach (PortfolioDataMapper.PortfolioResult i in d)
            {
                dcost += i.CostSavings;
                denergy += i.EnergyPriority;
                dcomfort += i.ComfortPriority;
                dmaintenance += i.MaintenancePriority;
            }

            daily.Add(dcost);
            daily.Add(denergy);
            daily.Add(dcomfort);
            daily.Add(dmaintenance);

            weekly.Add(w.Count());

            double wcost = 0;
            double wenergy = 0;
            double wcomfort = 0;
            double wmaintenance = 0;
            foreach (PortfolioDataMapper.PortfolioResult i in w)
            {
                wcost += i.CostSavings;
                wenergy += i.EnergyPriority;
                wcomfort += i.ComfortPriority;
                wmaintenance += i.MaintenancePriority;
            }

            weekly.Add(wcost);
            weekly.Add(wenergy);
            weekly.Add(wcomfort);
            weekly.Add(wmaintenance);

            monthly.Add(m.Count());

            double mcost = 0;
            double menergy = 0;
            double mcomfort = 0;
            double mmaintenance = 0;
            foreach (PortfolioDataMapper.PortfolioResult i in m)
            {
                mcost += i.CostSavings;
                menergy += i.EnergyPriority;
                mcomfort += i.ComfortPriority;
                mmaintenance += i.MaintenancePriority;
            }

            monthly.Add(mcost);
            monthly.Add(menergy);
            monthly.Add(mcomfort);
            monthly.Add(mmaintenance);

            List<List<double>> result = new List<List<double>>();
            result.Add(daily);
            result.Add(weekly);
            result.Add(monthly);

            return result;
        }

        List<List<object>> IDashboardService.GetPastMonthStatistics(int buildingID, int uid, string roleID, int cid, DateTime? startDate, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser)
        {
            List<List<object>> result = new List<List<object>>();

            //get for the last 30 days
            DateTime start = startDate.Value.AddDays(-30).Date;

            IEnumerable<Priority> dailys = mDataManager.PortfolioDataMapper.GetDashboardResultsFiltered(CommonConstants.AnalysisRange.Daily, startDate.Value.AddDays(-30).Date, startDate.Value.Date, uid, cid, buildingID, null, null, null, Convert.ToBoolean(isKGSFullAdminOrHigher), Convert.ToBoolean(isSuperAdminOrFullAdminOrFullUser));

            while (start.CompareTo(startDate.Value.Date) != 0)
            {
                List<object> daily = new List<object>();
                //IEnumerable<sp_AID_EID_DailyResult> d = BuildingDataMapper.GetDaily(buildingID, userID, isKGSAdminOrHigher, RoleHelper.IsClientSuperAdminOrFullAdminOrFullUser(roleID), clientID, start.Date, start.Date.AddDays(1).Date);
                IEnumerable<Priority> d = dailys.Where(r => r.StartDate.Equals(start));
                daily.Add(d.Count());

                double dcost = 0;
                double denergy = 0;
                double dcomfort = 0;
                double dmaintenance = 0;
                foreach (Priority i in d)
                {
                    dcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
                    denergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
                    dcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
                    dmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;

                }

                daily.Add(dcost);
                daily.Add(denergy);
                daily.Add(dcomfort);
                daily.Add(dmaintenance);
                daily.Add(start.Date);

                result.Add(daily);

                start = start.AddDays(1);
            }

            return result;
        }

        List<List<object>> IDashboardService.GetPastMonthEquipmentClassStatistics(int buildingID, int uid, string roleID, int cid, DateTime? startDate, int classID, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser)
        {
            List<List<object>> result = new List<List<object>>();

            //get for the last 30 days
            DateTime start = startDate.Value.AddDays(-30).Date;

            IEnumerable<Priority> dailys = mDataManager.PortfolioDataMapper.GetDashboardResultsFiltered(CommonConstants.AnalysisRange.Daily, startDate.Value.AddDays(-30).Date, startDate.Value.Date, uid, cid, buildingID, null, null, null, Convert.ToBoolean(isKGSFullAdminOrHigher), Convert.ToBoolean(isSuperAdminOrFullAdminOrFullUser));
            IEnumerable<Equipment> validClass = EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentClassID(buildingID, classID);

            while (start.CompareTo(startDate.Value.Date) != 0)
            {
                List<object> daily = new List<object>();
                //IEnumerable<sp_AID_EID_DailyResult> d = BuildingDataMapper.GetDaily(buildingID, userID, isKGSAdminOrHigher, RoleHelper.IsClientSuperAdminOrFullAdminOrFullUser(roleID), clientID, start.Date, start.Date.AddDays(1).Date);
                IEnumerable<Priority> d = dailys.Where(r => r.StartDate.Equals(start));

                double dcount = 0;
                double dcost = 0;
                double denergy = 0;
                double dcomfort = 0;
                double dmaintenance = 0;
                foreach (Priority i in d)
                {
                    if (validClass.Where(e => e.EID == i.EID).Any())
                    {
                        dcount++;
                        dcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
                        denergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
                        dcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
                        dmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;
                    }

                }

                daily.Add(dcount);
                daily.Add(dcost);
                daily.Add(denergy);
                daily.Add(dcomfort);
                daily.Add(dmaintenance);
                daily.Add(start.Date);

                result.Add(daily);

                start = start.AddDays(1);
            }

            return result;
        }

        List<List<object>> IDashboardService.GetPastMonthEquipmentStatistics(int bid, int eid, int userID, string roleID, int clientID, DateTime? startDate, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser)
        {
            List<List<object>> result = new List<List<object>>();

            //get for the last 30 days
            DateTime start = startDate.Value.AddDays(-30).Date;

            IEnumerable<Priority> dailys = mDataManager.PortfolioDataMapper.GetDashboardResultsFiltered(CommonConstants.AnalysisRange.Daily, start, startDate.Value.Date, userID, clientID, bid, null, eid, null, Convert.ToBoolean(isKGSFullAdminOrHigher), Convert.ToBoolean(isSuperAdminOrFullAdminOrFullUser));

            while (start.CompareTo(startDate.Value.Date) != 0)
            {
                List<object> daily = new List<object>();
                IEnumerable<Priority> d = dailys.Where(r => r.StartDate.Equals(start.Date));//EquipmentDataMapper.GetDaily(EID, userID, isKGSAdminOrHigher, RoleHelper.IsClientSuperAdminOrFullAdminOrFullUser(roleID), clientID, start.Date, start.Date.AddDays(1).Date);
                daily.Add(d.Count());

                double dcost = 0;
                double denergy = 0;
                double dcomfort = 0;
                double dmaintenance = 0;
                foreach (Priority i in d)
                {
                    dcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
                    denergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
                    dcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
                    dmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;

                }

                daily.Add(dcost);
                daily.Add(denergy);
                daily.Add(dcomfort);
                daily.Add(dmaintenance);
                daily.Add(start.Date);

                result.Add(daily);

                start = start.AddDays(1);
            }

            return result;
        }

        List<List<object>> IDashboardService.GetPastYearStatistics(int buildingID, int userID, string roleID, int clientID, DateTime startDate, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser)
        {
            List<List<object>> result = new List<List<object>>();

            //get for the past year
            DateTime start = DateTimeHelper.GetFirstOfMonth(startDate.AddYears(-1).Date);

            while (start.CompareTo(startDate.Date) <= 0)
            {
                List<object> monthly = new List<object>();

                IEnumerable<Priority> m = mDataManager.PortfolioDataMapper.GetDashboardResultsFiltered(CommonConstants.AnalysisRange.Monthly, start.Date, start.Date.AddMonths(1).Date, userID, clientID, buildingID, null, null, null, Convert.ToBoolean(isKGSFullAdminOrHigher), Convert.ToBoolean(isSuperAdminOrFullAdminOrFullUser));
                monthly.Add(m.Count());

                double mcost = 0;
                double menergy = 0;
                double mcomfort = 0;
                double mmaintenance = 0;

                foreach (Priority i in m)
                {
                    mcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
                    menergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
                    mcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
                    mmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;

                }

                monthly.Add(mcost);
                monthly.Add(menergy);
                monthly.Add(mcomfort);
                monthly.Add(mmaintenance);
                monthly.Add(DateTimeHelper.GetFirstOfMonth(start.Date));

                result.Add(monthly);

                start = start.AddMonths(1);
            }

            return result;
        }

        List<List<object>> IDashboardService.GetPastYearEquipmentClassStatistics(int buildingID, int userID, string roleID, int clientID, DateTime startDate, int classID, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser)
        {
            List<List<object>> result = new List<List<object>>();

            //get for the past year
            DateTime start = DateTimeHelper.GetFirstOfMonth(startDate.AddYears(-1).Date);

            IEnumerable<Equipment> validClass = EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentClassID(buildingID, classID);

            while (start.CompareTo(startDate.Date) <= 0)
            {
                List<object> monthly = new List<object>();

                IEnumerable<Priority> m = mDataManager.PortfolioDataMapper.GetDashboardResultsFiltered(CommonConstants.AnalysisRange.Monthly, start.Date, start.Date.AddMonths(1).Date, userID, clientID, buildingID, null, null, null, Convert.ToBoolean(isKGSFullAdminOrHigher), Convert.ToBoolean(isSuperAdminOrFullAdminOrFullUser));

                double mcount = 0;
                double mcost = 0;
                double menergy = 0;
                double mcomfort = 0;
                double mmaintenance = 0;

                foreach (Priority i in m)
                {
                    if (validClass.Where(e => e.EID == i.EID).Any())
                    {
                        mcount++;
                        mcost += Convert.ToString(i.CostSavings) == "NaN" ? 0 : i.CostSavings;
                        menergy += Convert.ToString(i.EnergyPriority) == "NaN" ? 0 : i.EnergyPriority;
                        mcomfort += Convert.ToString(i.ComfortPriority) == "NaN" ? 0 : i.ComfortPriority;
                        mmaintenance += Convert.ToString(i.MaintenancePriority) == "NaN" ? 0 : i.MaintenancePriority;
                    }

                }

                monthly.Add(mcount);
                monthly.Add(mcost);
                monthly.Add(menergy);
                monthly.Add(mcomfort);
                monthly.Add(mmaintenance);
                monthly.Add(DateTimeHelper.GetFirstOfMonth(start.Date));

                result.Add(monthly);

                start = start.AddMonths(1);
            }

            return result;
        }

        List<List<object>> IDashboardService.GetPastYearEquipmentStatistics(int bid, int eid, int userID, string roleID, int clientID, DateTime startDate, string isKGSFullAdminOrHigher, string isSuperAdminOrFullAdminOrFullUser)
        {
            List<List<object>> result = new List<List<object>>();

            //get for the past year
            DateTime start = DateTimeHelper.GetFirstOfMonth(startDate.AddYears(-1).Date);

            while (start.CompareTo(startDate.Date) <= 0)
            {
                List<object> monthly = new List<object>();

                IEnumerable<Priority> m = mDataManager.PortfolioDataMapper.GetDashboardResultsFiltered(CommonConstants.AnalysisRange.Monthly, start.Date, start.Date.AddMonths(1).Date, userID, clientID, bid, null, eid, null, Convert.ToBoolean(isKGSFullAdminOrHigher), Convert.ToBoolean(isSuperAdminOrFullAdminOrFullUser));

                double mfaults = 0;
                double mcost = 0;
                double menergy = 0;
                double mcomfort = 0;
                double mmaintenance = 0;
                foreach (Priority i in m)
                {
                    mfaults++;
                    mcost += i.CostSavings;
                    menergy += i.EnergyPriority;
                    mcomfort += i.ComfortPriority;
                    mmaintenance += i.MaintenancePriority;

                }

                monthly.Add(mfaults);
                monthly.Add(mcost);
                monthly.Add(menergy);
                monthly.Add(mcomfort);
                monthly.Add(mmaintenance);
                monthly.Add(DateTimeHelper.GetFirstOfMonth(start.Date));

                result.Add(monthly);

                start = start.AddMonths(1);
            }

            return result;
        }

        #endregion

        #region Live Data Methods

        IEnumerable<RawDataMapper.GetRawPlotData> IDashboardService.GetLiveData(int userID, int buildingID)
        {
            List<RawDataMapper.GetRawPlotData> datas = new List<RawDataMapper.GetRawPlotData>();

            int[] gauges;

            //get gauges
            gauges = UserDataMapper.GetAllUserGauges(userID);

            //for each gauge                
            foreach (int pid in gauges)
            {
                //get point data
                //EquipmentPointsDataMapper.GetEquipmentPointsData point = EquipmentPointsDataMapper.GetPartialEquipmentPointByPID(pid);

                if (PointDataMapper.IsPointAssociatedWithBuilding(pid, buildingID))
                {
                    //get last live raw data record for point
                    RawDataMapper.GetRawPlotData liveDataPointRecord = mDataManager.RawDataMapper.GetConvertedLastRawPlotDataRecordForPID(pid);

                    if (liveDataPointRecord != null)
                    {
                        datas.Add(liveDataPointRecord);
                    }
                    else
                    {
                        RawDataMapper.GetRawPlotData emptyData = new RawDataMapper.GetRawPlotData();
                        emptyData.PID = pid;
                        datas.Add(emptyData);
                    }
                }
            }

            return datas;
        }

        IEnumerable<RawDataMapper.GetRawPlotData> IDashboardService.GetLiveDataForPIDs(List<int> pids)
        {
            IEnumerable<RawDataMapper.GetRawPlotData> result = mDataManager.RawDataMapper.GetConvertedCurrentDaysRawPlotDataForPIDs(pids);
            return result;
        }

        List<RawDataMapper.GetRawPlotData> IDashboardService.UpdateLiveDataForPIDs(List<int> pids)
        {
            List<RawDataMapper.GetRawPlotData> updates = new List<RawDataMapper.GetRawPlotData>();

            foreach (int pid in pids)
            {
                updates.Add((RawDataMapper.GetRawPlotData)mDataManager.RawDataMapper.GetConvertedLastRawPlotDataRecordForPID(pid));
            }

            return updates;

        }

        IEnumerable<RawDataMapper.GetRawPlotData> IDashboardService.GetPast30DataForPIDs(IEnumerable<int> pids, DateTime startDate)
        {
            IEnumerable<RawDataMapper.GetRawPlotData> data = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids.ToList(), startDate.AddDays(-7).Date, startDate.Date);

            return data;
        }

        #endregion

        #region Global Setting Methods

        string IDashboardService.GetDashboardContent()
        {
            return GlobalDataMapper.GetGlobalSettings().DashboardBody;
        }

        #endregion

    }
}
