﻿<%@ Language="C#" EnableEventValidation="false" MasterPageFile="~/_masters/PerformanceDashboard.Master" AutoEventWireup="false" CodeBehind="PerformanceDashboardNew.aspx.cs" Inherits="CW.Website.PerformanceDashboardNew" Debug="false"%>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/SearchCriteria.ascx" tagname="SearchCriteria" tagprefix="CW" %>
<%@ Register src="~/_controls/performance/MonthlyUtilityConsumption.ascx" tagname="MonthlyUtilityConsumption" tagprefix="CW" %>
<%@ Register src="~/_controls/performance/EnergyConsumption.ascx" tagname="EnergyConsumption" tagprefix="CW" %>
<%@ Register src="~/_controls/performance/SignificantEnergyUsers.ascx" tagname="SignificantEnergyUsers" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
  
  <script type="text/javascript" src="/_assets/scripts/performanceDashboardHighcharts.js"></script>
  <script type="text/javascript" src="/_assets/scripts/perfDashboardSEUWidget.js"></script>
  <script type="text/javascript" src="/_assets/scripts/hiChartsTable.js"></script>
  <script type="text/javascript" src="/_assets/scripts/jquery.tablesorter.min.js"></script>
  <script type="text/javascript" src="https://code.highcharts.com/highcharts.js"></script>
  <script type="text/javascript" src="https://code.highcharts.com/modules/exporting.js"></script>  
  <script type="text/javascript" src="https://code.highcharts.com/modules/drilldown.js"></script>
  <script type="text/javascript">

      Sys.UI.Point = function Sys$UI$Point(x, y) {

          x = Math.round(x);

          y = Math.round(y);                

          var e = Function._validateParams(arguments, [

              { name: "x", type: Number, integer: true },

              { name: "y", type: Number, integer: true }

          ]);

          if (e) throw e;

          this.x = x;

          this.y = y;         
          
      }

      Sys.UI.Bounds = function Sys$UI$Bounds(x, y, width, height) {
          /// <summary locid="M:J#Sys.UI.Bounds.#ctor" />
          /// <param name="x" type="Number" integer="true"></param>
          /// <param name="y" type="Number" integer="true"></param>
          /// <param name="width" type="Number" integer="true"></param>
          /// <param name="height" type="Number" integer="true"></param>
          /// <field name="x" type="Number" integer="true" locid="F:J#Sys.UI.Bounds.x"></field>
          /// <field name="y" type="Number" integer="true" locid="F:J#Sys.UI.Bounds.y"></field>
          /// <field name="width" type="Number" integer="true" locid="F:J#Sys.UI.Bounds.width"></field>
          /// <field name="height" type="Number" integer="true" locid="F:J#Sys.UI.Bounds.height"></field>

          x = Math.round(x);

          y = Math.round(y);

          width = Math.round(width);

          height = Math.round(height);

          var e = Function._validateParams(arguments, [
              { name: "x", type: Number, integer: true },
              { name: "y", type: Number, integer: true },
              { name: "width", type: Number, integer: true },
              { name: "height", type: Number, integer: true }
          ]);
          if (e) throw e;
          this.x = x;
          this.y = y;
          this.height = height;
          this.width = width;
      }

 </script>

 <telerik:RadAjaxLoadingPanel ID="lpNested" runat="server" Skin="Default" />

 <telerik:RadAjaxPanel ID="radUpdatePanelNested" runat="server" UpdateMode="Conditional" LoadingPanelID="lpNested"> 

   <div id="performanceDashboard" class="performanceDashboard">

     <div id="dashboardHeader" class="dashboardHeader"> 
       
        <div class="performanceModuleIcon"></div>

		<div class="headerTitle">
	      <h1>Performance Dashboard</h1>
		</div>

		<div class="headerSpacer"></div>
		
        <div class="headerClientInfo"> 

	      <div class="headerClientImage">     
		    <asp:Image ID="imgHeaderClientImage" runat="server" CssClass="imgHeader" />
		  </div>
                      
		  <div class="headerClientName">
		    <label id="lblHeaderClientName" runat="server"></label>
		  </div>

		</div> <!-- end of headerClientInfo div -->

        <div class="headerSpacer"></div>

        <div class="headerContent">
          <asp:Literal ID="litDashboardBody" runat="server" />
        </div> 

	  </div> <!--end of dashboardHeader div-->

    </div> <!-- end of performanceDashboard div -->

    <hr />

    <div id="dashboardBody" class="dashboardBody dockWrapper">

      <CW:SearchCriteria ID="searchCriteria" runat="server" />

      <telerik:RadDockLayout ID="radDockLayout" runat="server" EnableLayoutPersistence="false">

        <telerik:RadDockZone ID="radDockZone" BorderStyle="None" runat="server" FitDocks="false" Orientation="Horizontal" ClientIDMode="Static">                                        
          
          <%--********************************ENERGY CONSUMPTION WIDGET**************************************--%>
          <telerik:RadDock ID="radDockEnergyConsumption" runat="server" Title="Energy Consumption">
                               
            <ContentTemplate>

              <CW:EnergyConsumption ID="energyConsumption" runat="server" />

            </ContentTemplate>

          </telerik:RadDock>
                  
          <%--********************************MONTHLY UTILITY CONSUMPTION WIDGET**************************************--%>
          <telerik:RadDock ID="radDockMonthyUtilityConsumption" runat="server" Title="Monthly Utility Consumption">
                              
            <ContentTemplate>

              <CW:MonthlyUtilityConsumption ID="monthlyUtilityConsumption" runat="server" />

            </ContentTemplate>

          </telerik:RadDock>

          <%--********************************SIGNIFICANT ENERGY USER WIDGET**************************************--%>
          
          <telerik:RadDock ID="radDockSignificantEnergyUsers" runat="server" Title="Significant Energy Users">
                              
            <ContentTemplate>
            
                <CW:SignificantEnergyUsers ID="significantEnergyUsers" runat="server" />

            </ContentTemplate>

          </telerik:RadDock>
               
        </telerik:RadDockZone>
      
      </telerik:RadDockLayout>  

    </div>

  </telerik:RadAjaxPanel>

</asp:Content>