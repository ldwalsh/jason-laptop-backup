﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI;

namespace CW.Website
{
    public partial class Error404: Page
    {
        private static readonly String[] error404;

        static Error404()
        {
            error404 =
            new[]
            {
                "You are in the wrong location, but our gears are still cranking away.",
                "You are in the wrong location, but your buildings haven't moved.",
                "You are in the wrong location, but your data is still crunching.",
            };
        }

        protected void Page_Load(Object sender, EventArgs e)
        {
            //Random.Next(0,2) will be either 0 or 1
            var random = new Random();            

            //
            //error handled by IIS
            headerErrorPage.InnerText = headerErrorIframe.InnerText = error404[random.Next(0, (error404.Length))];

            return;
        }
    }
}