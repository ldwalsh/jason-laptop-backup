﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Tasks.Master" AutoEventWireup="false" CodeBehind="Tasks.aspx.cs" Inherits="CW.Website.Tasks" %>
<%@ Register TagPrefix="CW" TagName="SearchCriteria" src="~/_controls/tasks/SearchCriteria.ascx" %>
<%@ Register TagPrefix="CW" TagName="ViewTasks" src="~/_controls/tasks/ViewTasks.ascx" %>
<%@ Register TagPrefix="telerik" Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
    
  <script type="text/javascript">
      function validateNumber(src, args) {

          if (args.Value == '' || args.Value == undefined) {

              args.IsValid = true; // prevent from calling webservice when unnecessary

              return;
          }

          $.ajax({
              type: "POST",
              url: "Tasks.aspx/IsNumberValid",
              contentType: "application/json; charset=utf-8",
              data: "{ 'num':  '" + args.Value + "'}",
              dataType: "json",
              async: false,
              success: function (msg) {
                  args.IsValid = msg.d;
              }
          });

      }
  </script>

  <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Skin="Default" />

  <telerik:RadAjaxPanel ID="radAjaxPanelTasks" runat="server" LoadingPanelID="loadingPanel">

    <div class="tasksModuleIcon">
      <h1>Tasks</h1>
    </div>

    <div class="richText">
      <asp:Literal ID="litTasksBody" runat="server" />
    </div>

    <div>
      <CW:SearchCriteria ID="SearchCriteria" runat="server" />
    </div>

    <div>
      <CW:ViewTasks ID="ViewTasks" runat="server" />
    </div>

  </telerik:RadAjaxPanel>

</asp:Content>