﻿<%@ Page Language="C#" MasterPageFile="~/_masters/ProviderAdmin.Master" AutoEventWireup="false" CodeBehind="ProviderUploadEquipmentToEquipment.aspx.cs" Inherits="CW.Website.ProviderUploadEquipmentToEquipment" %>
<%@ Register src="~/_controls/upload/UploadHeader.ascx" tagname="UploadHeader" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/UploadFormat.ascx" tagname="UploadFormat" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupsEquipmentEquipmentUpload.ascx" tagname="IDLookupsEquipmentEquipmentUpload" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/Upload.ascx" tagname="Upload" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <CW:UploadHeader ID="UploadHeader" runat="server" />
  <CW:UploadFormat ID="UploadFormat" runat="server" />
  <CW:IDLookupsEquipmentEquipmentUpload ID="IDLookupsEquipmentEquipmentUpload" runat="server" />
  <CW:Upload ID="Upload" runat="server" />

</asp:Content>