﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Documents.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="Documents.aspx.cs" Inherits="CW.Website.Documents" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                	                  	
            <div class="documentsModuleIcon">
                <h1>Documents</h1>                        
            </div>
            <div class="richText">
                <asp:Literal ID="litDocumentsBody" runat="server"></asp:Literal> 
            </div>               
                              
            <div class="administrationControls">

            <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
                <Tabs>
                    <telerik:RadTab Text="File Browser"></telerik:RadTab>
                    <telerik:RadTab Text="File Manager"></telerik:RadTab>
                    <telerik:RadTab Text="File Tagger"></telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                <telerik:RadPageView ID="RadPageView1" runat="server">
                    <h2>File Browser</h2> 
                        <p>
                            The file browser allows you to view and download your tagged files by building and equipment.
                        </p>

                    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
                        <script type="text/javascript">
                            // <![CDATA[
                            //On insert and update buttons click temporarily disables ajax to perform upload actions
                            function OnRequestStart(target, arguments) {
                                if (arguments.get_eventTarget().indexOf("btnDownload") > -1) {
                                    arguments.set_enableAjax(false);
                                }
                            }
                            // ]]>
                        </script>

                    </telerik:RadCodeBlock> 
                    <div class="documentsControls">
                        <telerik:RadAjaxLoadingPanel ID="radAjaxLoadingBrowser" runat="server" Skin="Default" />
                        <telerik:RadAjaxPanel ID="RadAjaxPanel3" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional" ClientEvents-OnRequestStart="OnRequestStart" LoadingPanelID="radAjaxLoadingBrowser">                                  

                            <a id="lnkSetFocusBrowser" runat="server"></a>                                              
                            <asp:Label ID="lblBrowserError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>        
                                                                                                                                                              
                                                    
                                <div class="documentsTree">
                                    <telerik:RadTreeView runat="server" ID="documentsTree" OnNodeClick="documentsTreeNodeClick">
                                                        
                                    </telerik:RadTreeView>
                                </div>

                                <%-- Optional for image action later<asp:ImageButton 
                                    ID="" runat="server" 
                                    ImageAlign="AbsMiddle" 
                                    />--%>
                                <div class="documentsGrid">
                                <telerik:RadGrid ID="rgFolderItems" runat="server"
                                OnItemDataBound="rgFolderItems_ItemDataBound"
                                OnNeedDataSource="rgFolderItems_NeedDataSource"
                                OnItemCommand="rgFolderItems_ItemCommand"
                                AutoGenerateColumns="false"
                                AllowPaging="true" 
                                AllowSorting="true"
                                PageSize="20"
                                Height="366px"
                                >
                                    <MasterTableView DataKeyNames="FID">
                                    <NoRecordsTemplate>
                                    <div class="divDocumentsGridNoRecords">
                                        No files to display.
                                    </div>
                                    </NoRecordsTemplate>
                                        <Columns>
                                        <telerik:GridTemplateColumn SortExpression="DisplayName" HeaderText="Name">
                                        <ItemTemplate>
                                            <asp:Image ID="imgIcon" runat="server"                                                
                                                ImageAlign="AbsMiddle" />                                                                                      
                                            <asp:Label 
                                                ID="btnItemName" runat="server" 
                                                Text='<%# Eval("DisplayName") + "" + Eval("Extension") %>'  /> 
                                        </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Size(MB)" SortExpression="FileSize">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# MathHelper.ConvertBytesToMB((long)Eval("FileSize"), 2) %>' ></asp:Label>
                                        </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                         <telerik:GridTemplateColumn SortExpression="FileClassName" HeaderText="File Class">
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("FileClassName") %>' ></asp:Label>
                                        </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn SortExpression="FileTypeName" HeaderText="File Type">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("FileTypeName") %>' ></asp:Label>
                                        </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn SortExpression="DateModified" HeaderText="Date Modified">
                                        <ItemTemplate>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("DateModified") %>' ></asp:Label>
                                        </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                            <asp:LinkButton 
                                                ID="btnDownload" runat="server" 
                                                Text="Download"
                                                CommandName="ItemClick"
                                                CommandArgument='<%# Eval("FID") %>'  /> 
                                        </ItemTemplate>                                                               
                                        </telerik:GridTemplateColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <PagerStyle Mode="NextPrevAndNumeric" />
                                    <ClientSettings EnableRowHoverStyle="true">
                                    </ClientSettings>
                                </telerik:RadGrid>
                                </div>

                            </telerik:RadAjaxPanel>                 
                            </div>      
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageView2" runat="server">
                    <h2>File Manager</h2>   
                        <p>
                            The file manager allows you to add, edit, replace, and delete files.
                        </p>

                    <script type="text/javascript">
                        //<![CDATA[
                        function validationAddFailed(sender, eventArgs) {
                            $("#lblAddError").append("Validation failed for '" + eventArgs.get_fileName() + "'. ").fadeIn("slow");
                        }

                        function validationReplaceFailed(sender, eventArgs) {
                            $("#lblReplaceError").append("Validation failed for '" + eventArgs.get_fileName() + "'.").fadeIn("slow");
                        }
                        //]]>
                    </script>  
                    <!--NESTED UPDATE PANEL NEEDED FOR FILE UPLOADER TRIGGERS-->  
                    <telerik:RadAjaxLoadingPanel ID="radAjaxLoadingManager" runat="server" Skin="Default" />
                    <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" LoadingPanelID="radAjaxLoadingManager">

                            <a id="lnkSetFocusManage" runat="server"></a>                                              
                            <asp:Label ID="lblManageError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>     

                            <fieldset class="documentsSearchManager">
                            <legend>Search Criteria</legend>
                             
                                
                                
                            <div class="divForm">   
                                <label class="label">File Class:</label>    
                                <asp:DropDownList ID="ddlManageFileClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlManageFileClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                </asp:DropDownList> 
                            </div>                                                                                                         
                            <div class="divForm">   
                                <label class="label">File Type:</label>    
                                <asp:DropDownList ID="ddlManageFileType" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlManageFileType_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                                                                
                                    <asp:ListItem Value="-1">Select class first...</asp:ListItem>                             
                                </asp:DropDownList> 
                            </div>                
                            <div class="divForm">   
                                <label class="label">Building (Shows tagged files only):</label>    
                                <asp:DropDownList ID="ddlManageBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlManageBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="-1">Select one...</asp:ListItem>                                     
                                </asp:DropDownList> 
                            </div>
                            <div class="divForm">   
                                <label class="label">Equipment (Shows tagged files only):</label>    
                                <asp:DropDownList ID="ddlManageEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlManageEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="-1">Select building first...</asp:ListItem>                                 
                                </asp:DropDownList> 
                            </div>                                                    

 
                            </fieldset>
                            <br /> 
                        
                        <%--Optional instead of image later<asp:ImageButton 
                            ID="" runat="server" 
                            ImageAlign="AbsMiddle" 
                            CommandArgument='' />--%>
                            <telerik:RadGrid ID="mrgFolderItems" runat="server"
                                OnItemDataBound="mrgFolderItems_ItemDataBound"
                                OnNeedDataSource="mrgFolderItems_NeedDataSource"
                                OnItemCommand="mrgFolderItems_ItemCommand"
                                AutoGenerateColumns="false"
                                OnUpdateCommand="mrgFolderItems_UpdateCommand"
                                OnInsertCommand="mrgFolderItems_InsertCommand"
                                OnDeleteCommand="mrgFolderItems_DeleteCommand"
                                PageSize="20"   
                                AllowPaging="true"
                                AllowSorting="true"
                                CssClass="gridFolderItems">                                
                                <MasterTableView DataKeyNames="FID" EditMode="InPlace" CommandItemDisplay="Top" CommandItemSettings-ShowAddNewRecordButton="true" CommandItemSettings-AddNewRecordText="Add file(s)">
                                    <NoRecordsTemplate>
                                    <div class="divDocumentsGridNoRecords">
                                        No files to display.
                                    </div>
                                    </NoRecordsTemplate>
                                    <Columns>
                                    <telerik:GridEditCommandColumn ButtonType="ImageButton" UniqueName="EditCommandColumn">
                                        <ItemStyle CssClass="myImageButton"/>
                                    </telerik:GridEditCommandColumn>
                                    <telerik:GridTemplateColumn HeaderText="Name" SortExpression="DisplayName" UniqueName="Upload">
                                    <ItemTemplate>
                                        <asp:Image ID="imgIcon" runat="server"                                                
                                                ImageAlign="AbsMiddle" />     
                                        <asp:Label
                                            ID="btnItemName" runat="server" 
                                            Text='<%# Eval("DisplayName") %>' 
                                            CommandArgument='<%# Eval("DisplayName") %>'/> 
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <br />
                                        Rename:
                                        <br />
                                        <asp:TextBox ID="editName" runat="server" MaxLength="50" Text='<%# Eval("DisplayName") %>'>
                                        </asp:TextBox>
                                        <br />
                                        <br />
                                        Replace if desired:
                                        <br />
                                        <telerik:RadAsyncUpload ID="ReplaceFileUploader" runat="server"
                                            AllowedFileExtensions="doc,docx,xls,xlsx,ppt,pptx,txt,pdf,jpg,jpeg,gif,png,dwg,cad,mat"
                                            MaxFileSize="10485760" 
                                            MaxFileInputsCount="1"
                                            OnClientValidationFailed="validationReplaceFailed">
                                        </telerik:RadAsyncUpload>
                                        <p>
                                            Select file to upload (doc,docx,xls,xlsx,ppt,pptx,txt,pdf,jpg,jpeg,gif,png,dwg,cad,mat)
                                        </p>
                                        <p>
                                            File size upload limit 10 MB.
                                        </p>                                                                   
                                        <label clientidmode="Static" id="lblReplaceError" class="errorMessage"></label>
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        <telerik:RadAsyncUpload ID="AddFileUploader" runat="server"
                                            AllowedFileExtensions="doc,docx,xls,xlsx,ppt,pptx,txt,pdf,jpg,jpeg,gif,png,dwg,cad,mat" 
                                            MaxFileSize="10485760"  
                                            EnableInlineProgress="true"
                                            MultipleFileSelection="Automatic"
                                            OnClientValidationFailed="validationAddFailed">
                                        </telerik:RadAsyncUpload>
                                        <p>
                                            Select file(s) to upload (doc,docx,xls,xlsx,ppt,pptx,txt,pdf,jpg,jpeg,gif,png,dwg,cad,mat)
                                        </p>
                                        <p>
                                            File size upload limit 10 MB.
                                        </p>
                                        <label clientidmode="Static" id="lblAddError" class="errorMessage" runat="server"></label>
                                    </InsertItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Extension" SortExpression="Extension">
                                    <ItemTemplate>
                                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Extension") %>' ></asp:Label>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Size(MB)" SortExpression="FileSize">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# (String.IsNullOrEmpty(Convert.ToString(Eval("FileSize"))) ? "" : MathHelper.ConvertBytesToMB((long)Eval("FileSize"), 2).ToString()) %>' ></asp:Label>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn SortExpression="FileTypeName" HeaderText="File Type">
                                    <ItemTemplate>
                                        <asp:Label ID="Label4" runat="server" Text='<%# Eval("FileTypeName") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="hdnManageEditFileClassID" Value='<%# Eval("FileClassID") %>' runat="server" />
                                        <asp:HiddenField ID="hdnManageEditFileTypeID" Value='<%# Eval("FileTypeID") %>' runat="server" />
                                        <div>   
                                            <label>File Class:</label><br /> 
                                            <asp:DropDownList ID="ddlManageEditFileClass" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlManageEditFileClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                    <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                            <br />
                                            <asp:RequiredFieldValidator ID="manageEditFileClassRequiredValidator" runat="server"
                                                ErrorMessage="File Class is a required field." 
                                                ControlToValidate="ddlManageEditFileClass"  
                                                SetFocusOnError="true" 
                                                InitialValue="-1"
                                                >
                                                </asp:RequiredFieldValidator>
                                        </div>                                                                                                         
                                        <div>   
                                            <label>File Type:</label><br />    
                                            <asp:DropDownList ID="ddlManageEditFileType" CssClass="dropdownNarrow" AppendDataBoundItems="true" runat="server">                                                                
                                                <asp:ListItem Value="-1">Select class first...</asp:ListItem>                             
                                            </asp:DropDownList>
                                            <br />
                                            <asp:RequiredFieldValidator ID="manageEditFileTypeRequiredValidator" runat="server"
                                                ErrorMessage="File Type is a required field." 
                                                ControlToValidate="ddlManageEditFileType"  
                                                SetFocusOnError="true" 
                                                InitialValue="-1"                                                  
                                                >
                                                </asp:RequiredFieldValidator>
                                        </div>   
                                    </EditItemTemplate>
                                    <InsertItemTemplate>
                                        
                                    <!--NESTED UPDATE PANEL NEEDED FOR FILE UPLOADER NOT LOSING FILE ON POSTBACK-->    
                                    <asp:UpdatePanel ID="updatePanelAddFiles" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                        <div>   
                                            <label>File Class:</label><br /> 
                                            <asp:DropDownList ID="ddlManageAddFileClass" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlManageAddFileClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>   
                                            </asp:DropDownList> 
                                            <br />
                                            <asp:RequiredFieldValidator ID="manageAddFileClassRequiredValidator" runat="server"
                                                CssClass="errorMessage"
                                                ErrorMessage="File Class is a required field." 
                                                ControlToValidate="ddlManageAddFileClass"  
                                                SetFocusOnError="true" 
                                                InitialValue="-1"
                                                >
                                                </asp:RequiredFieldValidator>
                                        </div>                                                                                                         
                                        <div>   
                                            <label>File Type:</label><br />    
                                            <asp:DropDownList ID="ddlManageAddFileType" CssClass="dropdownNarrow" AppendDataBoundItems="true" runat="server">                                                                
                                                <asp:ListItem Value="-1">Select class first...</asp:ListItem>                             
                                            </asp:DropDownList>
                                            <br />
                                            <asp:RequiredFieldValidator ID="manageAddFileTypeRequiredValidator" runat="server"
                                                CssClass="errorMessage"
                                                ErrorMessage="File Type is a required field." 
                                                ControlToValidate="ddlManageAddFileType"  
                                                SetFocusOnError="true" 
                                                InitialValue="-1"                                                  
                                                >
                                                </asp:RequiredFieldValidator>
                                        </div>    
                                        </ContentTemplate>
                                    </asp:UpdatePanel>                      

                                    </InsertItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn SortExpression="DateModified" HeaderText="Date Modified">
                                    <ItemTemplate>
                                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("DateModified") %>' ></asp:Label>
                                    </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridButtonColumn ConfirmText="Delete this file?" 
                                        ConfirmDialogType="RadWindow"
                                        ConfirmTitle="Delete" 
                                        ButtonType="ImageButton" 
                                        ItemStyle-HorizontalAlign="Center" 
                                        CommandName="Delete" Text="Delete"
                                        UniqueName="DeleteColumn">
                                    </telerik:GridButtonColumn>
                                    </Columns>
                                    <CommandItemSettings ShowRefreshButton="false"/>
                                                                
                                </MasterTableView>
                                <PagerStyle Mode="NextPrevAndNumeric" />
                                <ClientSettings EnableRowHoverStyle="true">
                                </ClientSettings>
                            </telerik:RadGrid>                                                                                                                                               
                                                
                    </telerik:RadAjaxPanel>   
                </telerik:RadPageView>
                <telerik:RadPageView ID="RadPageView3" runat="server">
                      <h2>File Tagger</h2>   
                        <p >
                            The file tagger allows you to tag and untag file associations with your buildings and equipment. A file can be tagged to any number of buildings and equipment.
                            To begin please select the building or equipment you wish to tag or untag files to. To narrow down the list of untagged files you may select the type of files you wish to tag or untag. 
                            The grids below will populate with your file associations, and you will be able to drag and drop from and to each grid in order to tag or untag.                                               
                        </p>
                        <div>  
                                                        
                        <telerik:RadAjaxManager runat="server" ID="radAjax">
                            <AjaxSettings>
                                <telerik:AjaxSetting AjaxControlID="grdUntaggedFiles">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="grdUntaggedFiles"/>
                                        <telerik:AjaxUpdatedControl ControlID="grdTaggedFiles"/>
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                                <telerik:AjaxSetting AjaxControlID="grdTaggedFiles">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="grdTaggedFiles"/>
                                        <telerik:AjaxUpdatedControl ControlID="grdUntaggedFiles"/>
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                                <telerik:AjaxSetting AjaxControlID="UseDragColumnCheckBox">
                                    <UpdatedControls>
                                        <telerik:AjaxUpdatedControl ControlID="grdUntaggedFiles"/>
                                        <telerik:AjaxUpdatedControl ControlID="grdTaggedFiles"/>
                                    </UpdatedControls>
                                </telerik:AjaxSetting>
                            </AjaxSettings>
                        </telerik:RadAjaxManager>
                        <telerik:RadScriptBlock runat="server" ID="scriptBlock">

                            <script type="text/javascript">

                                function onRowDropping(sender, args) {
                                    if (sender.get_id() == "<%=grdUntaggedFiles.ClientID %>") {
                                        var node = args.get_destinationHtmlElement();
                                        if (!isChildOf('<%=grdTaggedFiles.ClientID %>', node) && !isChildOf('<%=grdUntaggedFiles.ClientID %>', node)) {
                                            args.set_cancel(true);
                                        }
                                    }
                                    else if (sender.get_id() == "<%=grdTaggedFiles.ClientID %>") {
                                        var node = args.get_destinationHtmlElement();
                                        if (!isChildOf('<%=grdUntaggedFiles.ClientID %>', node) && !isChildOf('<%=grdTaggedFiles.ClientID %>', node)) {
                                            args.set_cancel(true);
                                        }
                                    }
                            }
                            function isChildOf(parentId, element) {
                                while (element) {
                                    if (element.id && element.id.indexOf(parentId) > -1) {
                                        return true;
                                    }
                                    element = element.parentNode;
                                }
                                return false;
                            }

                            </script>

                        </telerik:RadScriptBlock>                             
                              
                        <telerik:RadAjaxLoadingPanel ID="radAjaxLoadingTagger" runat="server" Skin="Default" /> 
                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="radAjaxLoadingTagger">

                                <a id="lnkSetFocusTag" runat="server"></a>                                              
                                <asp:Label ID="lblTagError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>  

                        <fieldset class="documentsSearchTagger">
                            <legend>Search Criteria</legend>    
                            <div class="divForm">   
                                <label class="label">*Building:</label>    
                                <asp:DropDownList ID="ddlTaggerBuilding" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlTaggerBuilding_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="-1">Select one...</asp:ListItem>                                     
                                </asp:DropDownList> 
                            </div>
                            <div class="divForm">   
                                <label class="label">Equipment:</label>    
                                <asp:DropDownList Enabled="false" ID="ddlTaggerEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlTaggerEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="-1">Select building first...</asp:ListItem>                                 
                                </asp:DropDownList> 
                            </div>
                            <div class="divForm">   
                                <label class="label">File Class:</label>    
                                <asp:DropDownList Enabled="false" ID="ddlTaggerFileClass" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlTaggerFileClass_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                        <asp:ListItem Value="-1">View all or optionally select one...</asp:ListItem>                                 
                                </asp:DropDownList> 
                            </div>                                                                                                         
                            <div class="divForm">   
                                <label class="label">File Type:</label>    
                                <asp:DropDownList Enabled="false" ID="ddlTaggerFileType" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlTaggerFileType_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                    <asp:ListItem Value="-1">Select class first...</asp:ListItem>                             
                                </asp:DropDownList> 
                            </div>   
                                                                                                              
                        </fieldset>
                      
                        <br />
                                      
                                <div>
                                    <div class="divDocumentsTaggerLeft">
                                        <h2>
                                            Untagged Files
                                        </h2>
                                        <telerik:RadGrid runat="server" 
                                            ID="grdUntaggedFiles"
                                            OnNeedDataSource="grdUntaggedFiles_NeedDataSource"
                                            AllowPaging="true" 
                                            AllowSorting="true"
                                            OnRowDrop="grdUntaggedFiles_RowDrop" 
                                            AllowMultiRowSelection="true"
                                            PageSize="20"                                                 
                                            AutoGenerateColumns="false">
                                            <MasterTableView DataKeyNames="FID">
                                                <NoRecordsTemplate>
                                                <div class="divDocumentsGridNoRecords">
                                                    No files to display.
                                                </div>
                                                </NoRecordsTemplate>
                                                <Columns>
                                                    <telerik:GridDragDropColumn HeaderStyle-Width="18px" Visible="true" />
                                                    <telerik:GridBoundColumn HeaderText="Display Name" DataField="DisplayName" Visible="true" ></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Extension" DataField="Extension" HeaderStyle-Width="75px" Visible="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Date Modified" DataField="DateModified" Visible="true"></telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                            <ClientSettings EnableRowHoverStyle="true" AllowRowsDragDrop="true" ReorderColumnsOnClient="true">
                                                <Selecting AllowRowSelect="true" EnableDragToSelectRows="false"/>
                                                <ClientEvents OnRowDropping="onRowDropping" />
                                                <Scrolling AllowScroll="true" UseStaticHeaders="true"/>
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </div>
                                    <div class="divDocumentsTaggerRight">
                                        <h2>
                                            Tagged Files
                                        </h2>
                                        <telerik:RadGrid runat="server" 
                                            AllowPaging="true" 
                                            AllowSorting="true"
                                            ID="grdTaggedFiles" 
                                            OnNeedDataSource="grdTaggedFiles_NeedDataSource"
                                            OnRowDrop="grdTaggedFiles_RowDrop" 
                                            AllowMultiRowSelection="true" 
                                            PageSize="20"
                                            AutoGenerateColumns="false">
                                            <MasterTableView DataKeyNames="FID">
                                                <NoRecordsTemplate>
                                                <div class="divDocumentsGridNoRecords">
                                                    No files to display.
                                                </div>
                                                </NoRecordsTemplate>
                                                <Columns>
                                                    <telerik:GridDragDropColumn HeaderStyle-Width="18px" Visible="true" />
                                                    <telerik:GridBoundColumn HeaderText="Display Name" DataField="DisplayName" Visible="true" ></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Extension" DataField="Extension" HeaderStyle-Width="75px" Visible="true"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Date Modified" DataField="DateModified" Visible="true"></telerik:GridBoundColumn>
                                                </Columns>
                                            </MasterTableView>
                                            <PagerStyle Mode="NextPrevAndNumeric" />
                                            <ClientSettings EnableRowHoverStyle="true" AllowRowsDragDrop="true" ReorderColumnsOnClient="true">
                                                <Selecting AllowRowSelect="true" EnableDragToSelectRows="false" />
                                                <ClientEvents OnRowDropping="onRowDropping" />
                                                <Scrolling AllowScroll="true" UseStaticHeaders="true"/>
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </div>
                                    <div style="clear: both;">
                                        <!-- -->
                                    </div>
                                </div>
                                

                        </telerik:RadAjaxPanel>
                        </div>                     
                </telerik:RadPageView>
            </telerik:RadMultiPage>

            </div>       
                       
</asp:Content>


                    
                  
