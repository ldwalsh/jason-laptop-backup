﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Analysis;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSAnalysisAdministration: SitePage
    {
        #region Properties

            private Analyse mAnalysis;
            const string addAnalysisSuccess = " analysis addition was successful.";
            const string addAnalysisFailed = "Adding analysis failed. Please contact an administrator.";
            const string analysisExists = "Analysis with that name already exists.";
            const string updateSuccessful = "Analysis update was successful.";
            const string updateFailed = "Analysis update failed. Please contact an administrator.";

            const string reassignNoChangeDetected = "No change detected so no action has been processed.";
            
            const string assigningClientsFailed = " Error assigning clients to the analysis.";
            const string reassigningClientsFailed = " Error reassigning clients to the analysis.";

            const string reassignBuildingVariablesUpdateSuccessful = "Building variables reassignment was successful.";
            const string reassignBuildingVariablesUpdateFailed = "Building variables reassignment failed. Please contact an administrator.";

            const string buildingVariablesUpdateSuccessful = "Building variables update was successful.";
            const string buildingVariablesUpdateFailed = "Building variables update failed. Please contact an administrator.";

            const string reassignEquipmentVariablesUpdateSuccessful = "Equipment variables reassignment was successful.";
            const string reassignEquipmentVariablesUpdateFailed = "Equipment variables reassignment failed. Please contact an administrator.";

            const string equipmentVariablesUpdateSuccessful = "Equipment variables update was successful.";
            const string equipmentVariablesUpdateFailed = "Equipment variables update failed. Please contact an administrator.";
            
            const string reassignInputPointTypesUpdateSuccessful = "Input point types reassignment was successful.";
            const string reassignInputPointTypesUpdateFailed = "Input point types reassignment failed. Please contact an administrator.";
            
            const string inputPointTypesUpdateSuccessful = "Input point types update was successful.";
            const string inputPointTypesUpdateFailed = "Input point types update failed. Please contact an administrator.";

            const string reassignInputVPointTypesUpdateSuccessful = "Input vpoint types reassignment was successful.";
            const string reassignInputVPointTypesUpdateFailed = "Input vpoint types reassignment failed. Please contact an administrator.";

            const string inputVPointTypesUpdateSuccessful = "Input vpoint types update was successful.";
            const string inputVPointTypesUpdateFailed = "Input vpoint types update failed. Please contact an administrator.";
            
            const string reassignOutputVPointTypesUpdateSuccessful = "Output vpoint types reassignment was successful.";
            const string reassignOutputVPointTypesUpdateFailed = "Output vpoint types reassignment failed. Please contact an administrator.";
            
            const string reassignOutputVPrePointTypesUpdateSuccessful = "Output vprepoint types reassignment was successful.";
            const string reassignOutputVPrePointTypesUpdateFailed = "Output vprepoint types reassignment failed. Please contact an administrator.";

            const string outputVPointTypesVDataExists = "Cannot unassign output vpoint types because vdata exists for one ore more of the vpoint types.";
            const string outputVPrePointTypesVPreDataExists = "Cannot unassign output vprepoint types because vpredata exists for one ore more of the vprepoint types.";

            const string vOrVPreDataExists = "Cannot delete Analysis because v or vpre data exists for one ore more pieces of equipment.";
            const string equipmentExist = "Cannot delete Analysis because its associated to one or more pieces of equipment. Please disassociate from equipment first.";
            const string equipmentTypesExist = "Cannot delete analysis because one or more equipment types are associated. Please disassociate from analysis first.";
            const string deleteSuccessful = "Analysis deletion was successful.";
            const string deleteFailed = "Analysis deletion failed. Please contact an administrator.";            
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "AnalysisName";

            const string analysisEquipmentEmpty = "No equipment have this analysis assigned.";

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //datamanger in sitepage

                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");         

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindAnalyses();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindAnalyses(ddlBuildingVariablesAnalyses, ddlEquipmentVariablesAnalyses, ddlInputPointTypesAnalyses, ddlOutputVPointTypesAnalyses, ddlOutputVPrePointTypesAnalyses, ddlAnalysisEquipment);
                    BindAnalysesMetaOnly(ddlInputVPointTypesAnalyses);

                    BindMatlabAssemblies(ddlAddAssemblyFile, ddlEditAssemblyFile);

                    sessionState["Search"] = String.Empty;
                }                
            }

        #endregion

        #region Set Fields and Data

            protected void SetAnalysisIntoEditForm(Analyse analysis)
            {
                //ID
                hdnEditID.Value = Convert.ToString(analysis.AID);
                //Analysis Name
                txtEditAnalysisName.Text = analysis.AnalysisName;
                //Assembly File
                ddlEditAssemblyFile.SelectedValue = Convert.ToString(analysis.MAID);
                //Analysis Base File Name
                txtEditAnalysisBaseFileName.Text = analysis.AnalysisBaseFileName;
                //Analysis Function Name
                txtEditAnalysisFunctionName.Text = analysis.AnalysisFunctionName;
                //Analysis Teaser
                txtEditAnalysisTeaser.Value = analysis.AnalysisTeaser;
                //Analysis Description
                txtEditAnalysisDescription.Value = analysis.AnalysisDescription;

                //Meta
                chkEditMeta.Checked = analysis.IsMeta;

                //TODO: set meta fields

                //Custom
                chkEditCustom.Checked = analysis.IsCustom;
                //Utility
                chkEditUtility.Checked = analysis.IsUtilityBilling;
                //Active
                chkEditActive.Checked = analysis.IsActive;
                //Visible
                chkEditVisible.Checked = analysis.IsVisible;

                //clear client listboxes, just in case
                lbEditClientsTop.Items.Clear();
                lbEditClientsBottom.Items.Clear();

                //VPoint DataRanges depending on meta
                if (analysis.IsMeta)
                {
                }

                //Client Access depending on custom
                if (analysis.IsCustom)
                {
                    //bind clients for analysis
                    IEnumerable<Client> clients = DataMgr.ClientDataMapper.GetAllClientsAssociatedToAID(analysis.AID);

                    if (clients.Any())
                    {
                        //bind clients associated to user to bottom listbox
                        lbEditClientsBottom.DataSource = clients;
                        lbEditClientsBottom.DataTextField = "ClientName";
                        lbEditClientsBottom.DataValueField = "CID";
                        lbEditClientsBottom.DataBind();
                    }

                    //get clients not for analysis
                    IEnumerable<Client> clients2 = DataMgr.ClientDataMapper.GetAllClientsNotAssociatedToAID(analysis.AID);

                    if (clients2.Any())
                    {
                        //bind clients not associated to user to top listbox                        
                        lbEditClientsTop.DataSource = clients2;
                        lbEditClientsTop.DataTextField = "ClientName";
                        lbEditClientsTop.DataValueField = "CID";
                        lbEditClientsTop.DataBind();
                    }

                    divEditClientsListBoxs.Visible = true;
                }
                else
                {
                    divEditClientsListBoxs.Visible = false;
                }
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Binds a dropdown list with all analyses
            /// </summary>
            /// <param name="ddl"></param>
            private void BindAnalyses(DropDownList ddl, DropDownList ddl2, DropDownList ddl3, DropDownList ddl4, DropDownList ddl5, DropDownList ddl6)
            {
                IEnumerable<Analyse> analyses = DataMgr.AnalysisDataMapper.GetAllAnalyses();

                ddl.DataTextField = "AnalysisName";
                ddl.DataValueField = "AID";
                ddl.DataSource = analyses;
                ddl.DataBind();

                ddl2.DataTextField = "AnalysisName";
                ddl2.DataValueField = "AID";
                ddl2.DataSource = analyses;
                ddl2.DataBind();

                ddl3.DataTextField = "AnalysisName";
                ddl3.DataValueField = "AID";
                ddl3.DataSource = analyses;
                ddl3.DataBind();

                ddl4.DataTextField = "AnalysisName";
                ddl4.DataValueField = "AID";
                ddl4.DataSource = analyses;
                ddl4.DataBind();

                ddl5.DataTextField = "AnalysisName";
                ddl5.DataValueField = "AID";
                ddl5.DataSource = analyses;
                ddl5.DataBind();

                ddl6.DataTextField = "AnalysisName";
                ddl6.DataValueField = "AID";
                ddl6.DataSource = analyses;
                ddl6.DataBind();
            }

            /// <summary>
            /// Binds a dropdown list with all analyses
            /// </summary>
            /// <param name="ddl"></param>
            private void BindAnalysesMetaOnly(DropDownList ddl)
            {                          
                ddl.DataTextField = "AnalysisName";
                ddl.DataValueField = "AID";
                ddl.DataSource = DataMgr.AnalysisDataMapper.GetAllMetaAnalyses();
                ddl.DataBind();
            }

            /// <summary>
            /// Binds top and bottom list boxes with building variables by aid
            /// 
            /// top: all not yet associated to aid
            /// bottom: all currently associated to aid
            /// </summary>
            /// <param name="aid"></param>       
            private void BindBuildingVariables(int aid)
            {
                //get building variables associated to analysis 
                IEnumerable<BuildingVariable> mBuildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesAssociatedToAID(aid);

                if (mBuildingVariables.Any())
                {
                    //bind building variabels to bottom listbox
                    lbBuildingVariablesBottom.DataSource = mBuildingVariables;
                    lbBuildingVariablesBottom.DataTextField = "BuildingVariableDisplayName";
                    lbBuildingVariablesBottom.DataValueField = "BVID";
                    lbBuildingVariablesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbBuildingVariablesBottom.Items.Clear();
                }

                //get building variables not assiciated to analysis
                mBuildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesNotAssociatedToAID(aid);

                if (mBuildingVariables.Any())
                {
                    //bind building variabels to top listbox
                    lbBuildingVariablesTop.DataSource = mBuildingVariables;
                    lbBuildingVariablesTop.DataTextField = "BuildingVariableDisplayName";
                    lbBuildingVariablesTop.DataValueField = "BVID";
                    lbBuildingVariablesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbBuildingVariablesTop.Items.Clear();
                }

                buildingVariables.Visible = true;
            }


            /// <summary>
            /// Binds top and bottom list boxes with equipment variables by aid
            /// 
            /// top: all not yet associated to aid
            /// bottom: all currently associated to aid
            /// </summary>
            /// <param name="aid"></param>       
            private void BindEquipmentVariables(int aid)
            {
                //get equipment variables associated to analysis 
                IEnumerable<EquipmentVariable> mEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesAssociatedToAID(aid);

                if (mEquipmentVariables.Any())
                {
                    //bind equipment variabels to bottom listbox
                    lbEquipmentVariablesBottom.DataSource = mEquipmentVariables;
                    lbEquipmentVariablesBottom.DataTextField = "EquipmentVariableDisplayName";
                    lbEquipmentVariablesBottom.DataValueField = "EVID";
                    lbEquipmentVariablesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbEquipmentVariablesBottom.Items.Clear();
                }

                //get equipment variables not assiciated to analysis
                mEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesNotAssociatedToAID(aid);

                if (mEquipmentVariables.Any())
                {
                    //bind equipment variabels to top listbox
                    lbEquipmentVariablesTop.DataSource = mEquipmentVariables;
                    lbEquipmentVariablesTop.DataTextField = "EquipmentVariableDisplayName";
                    lbEquipmentVariablesTop.DataValueField = "EVID";
                    lbEquipmentVariablesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbEquipmentVariablesTop.Items.Clear();
                }

                equipmentVariables.Visible = true;
            }


            /// <summary>
            /// Binds top and bottom list boxes with input point types by aid
            /// 
            /// top: all not yet associated to aid
            /// bottom: all currently associated to aid
            /// </summary>
            /// <param name="aid"></param>       
            private void BindInputPointTypes(int aid)
            {
                //get input point types associated to analysis 
                IEnumerable<PointType> mInputPointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllInputPointTypesAssociatedToAID(aid);

                if (mInputPointTypes.Any())
                {
                    //bind input point types to bottom listbox                    
                    lbInputPointTypesBottom.DataSource = mInputPointTypes;
                    lbInputPointTypesBottom.DataTextField = "PointTypeName";
                    lbInputPointTypesBottom.DataValueField = "PointTypeID";
                    lbInputPointTypesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbInputPointTypesBottom.Items.Clear();
                }

                //get input point types not assiciated to analysis, except for unassigned point class/type
                mInputPointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllInputPointTypesNotAssociatedToAIDExceptTypeUnassigneds(aid);

                if (mInputPointTypes.Any())
                {
                    //bind input point types  to top listbox
                    lbInputPointTypesTop.DataSource = mInputPointTypes;
                    lbInputPointTypesTop.DataTextField = "PointTypeName";
                    lbInputPointTypesTop.DataValueField = "PointTypeID";
                    lbInputPointTypesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbInputPointTypesTop.Items.Clear();
                }

                inputPointTypes.Visible = true;    
            }

            /// <summary>
            /// Binds top and bottom list boxes with input vpoint types by aid
            /// 
            /// top: all not yet associated to aid
            /// bottom: all currently associated to aid
            /// </summary>
            /// <param name="aid"></param>       
            private void BindInputVPointTypes(int aid)
            {
                //get input vpoint types associated to analysis 
                IEnumerable<PointType> mInputVPointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllInputVPointTypesAssociatedToAID(aid);

                if (mInputVPointTypes.Any())
                {
                    //bind input vpoint types to bottom listbox                    
                    lbInputVPointTypesBottom.DataSource = mInputVPointTypes;
                    lbInputVPointTypesBottom.DataTextField = "PointTypeName";
                    lbInputVPointTypesBottom.DataValueField = "PointTypeID";
                    lbInputVPointTypesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbInputVPointTypesBottom.Items.Clear();
                }

                //get input vpoint types not assiciated to analysis, except for unassigned point class/type
                mInputVPointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllInputVPointTypesNotAssociatedToAIDExceptTypeUnassigneds(aid);

                if (mInputVPointTypes.Any())
                {
                    //bind input vpoint types  to top listbox
                    lbInputVPointTypesTop.DataSource = mInputVPointTypes;
                    lbInputVPointTypesTop.DataTextField = "PointTypeName";
                    lbInputVPointTypesTop.DataValueField = "PointTypeID";
                    lbInputVPointTypesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbInputVPointTypesTop.Items.Clear();
                }

                inputVPointTypes.Visible = true;
            }


            /// <summary>
            /// Binds top and bottom list boxes with output vpoint types by aid
            /// 
            /// top: all not yet associated to aid
            /// bottom: all currently associated to aid
            /// </summary>
            /// <param name="aid"></param>       
            private void BindOutputVPointTypes(int aid)
            {
                //get output vpoint types associated to analysis 
                IEnumerable<PointType> mOutputVPointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPointTypesAssociatedToAID(aid);

                if (mOutputVPointTypes.Any())
                {
                    //bind output vpoint types to bottom listbox                    
                    lbOutputVPointTypesBottom.DataSource = mOutputVPointTypes;
                    lbOutputVPointTypesBottom.DataTextField = "PointTypeName";
                    lbOutputVPointTypesBottom.DataValueField = "PointTypeID";
                    lbOutputVPointTypesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbOutputVPointTypesBottom.Items.Clear();
                }

                //get ouptput vpoint types not assiciated to analysis
                mOutputVPointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPointTypesNotAssociatedToAID(aid);

                if (mOutputVPointTypes.Any())
                {
                    //bind output vpoint types  to top listbox
                    lbOutputVPointTypesTop.DataSource = mOutputVPointTypes;
                    lbOutputVPointTypesTop.DataTextField = "PointTypeName";
                    lbOutputVPointTypesTop.DataValueField = "PointTypeID";
                    lbOutputVPointTypesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbOutputVPointTypesTop.Items.Clear();
                }

                outputVPointTypes.Visible = true;
            }

            /// <summary>
            /// Binds top and bottom list boxes with output vprepoint types by aid
            /// 
            /// top: all not yet associated to aid
            /// bottom: all currently associated to aid
            /// </summary>
            /// <param name="aid"></param>       
            private void BindOutputVPrePointTypes(int aid)
            {
                //get output vprepoint types associated to analysis 
                IEnumerable<PointType> mOutputVPrePointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPrePointTypesAssociatedToAID(aid);
               
                if (mOutputVPrePointTypes.Any())
                {
                    //bind output vprepoint types to bottom listbox                    
                    lbOutputVPrePointTypesBottom.DataSource = mOutputVPrePointTypes;
                    lbOutputVPrePointTypesBottom.DataTextField = "PointTypeName";
                    lbOutputVPrePointTypesBottom.DataValueField = "PointTypeID";
                    lbOutputVPrePointTypesBottom.DataBind();
                }
                else
                {
                    //clear current list
                    lbOutputVPrePointTypesBottom.Items.Clear();
                }

                //get ouptput vprepoint types not assiciated to analysis
                mOutputVPrePointTypes = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPrePointTypesNotAssociatedToAID(aid);

                if (mOutputVPrePointTypes.Any())
                {
                    //bind output vprepoint types  to top listbox
                    lbOutputVPrePointTypesTop.DataSource = mOutputVPrePointTypes;
                    lbOutputVPrePointTypesTop.DataTextField = "PointTypeName";
                    lbOutputVPrePointTypesTop.DataValueField = "PointTypeID";
                    lbOutputVPrePointTypesTop.DataBind();
                }
                else
                {
                    //clear current list
                    lbOutputVPrePointTypesTop.Items.Clear();
                }

                outputVPrePointTypes.Visible = true;
            }


            /// <summary>
            /// Shows all equipment with the specified analysis assigned
            /// </summary>
            /// <param name="eid"></param>
            private void BindAnalysisEquipment(int aid)
            {
                IEnumerable<Equipment> equipment = DataMgr.EquipmentDataMapper.GetAllEquipmentByAID(aid, true);

                //bool assigned = false;

                ////get all Equipment in system
                //IEnumerable<Equipment> mEquipment = EquipmentDataMapper.GetAllEquipment();

                ////declare new ienumerable for available assigned equipment
                //List<Equipment> availableAssignedEquipmentList = new List<Equipment>();

                ////for each ienumerable equipment                           
                //using (IEnumerator<Equipment> list = mEquipment.GetEnumerator())
                //{
                //    int counter = 1;
                //    while (list.MoveNext())
                //    {
                //        //current equipment
                //        Equipment item = (Equipment)list.Current;

                //        assigned = AnalysisEquipmentDataMapper.IsAnalysisAssignedToEquipment(item.EID, aid);

                //        //if analysis is avaialble assigned to the equipment                            
                //        if (assigned)
                //        {
                //            availableAssignedEquipmentList.Add(item);
                //        }

                //        counter++;
                //    }
                //}

                if (equipment.Any())
                {
                    //bind assigned equipment to repeater
                    rptAnalysisEquipment.DataSource = equipment;
                    rptAnalysisEquipment.DataBind();
                    rptAnalysisEquipment.Visible = true;
                    lblAnalysisEquipmentEmpty.Visible = false;
                }
                else
                {
                    rptAnalysisEquipment.Visible = false;
                    lblAnalysisEquipmentEmpty.Text = analysisEquipmentEmpty;
                    lblAnalysisEquipmentEmpty.Visible = true;
                }

                analysisEquipment.Visible = true;
            }


            /// <summary>
            /// Binds two dropdown lists of matlab assembly files
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="ddl2"></param>
            private void BindMatlabAssemblies(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<MatlabAssembly> matlabAssemblies = DataMgr.MatlabAssemblyDataMapper.GetAllAssemblies();

                ddl.DataTextField = "FileName";
                ddl.DataValueField = "MAID";
                ddl.DataSource = matlabAssemblies;
                ddl.DataBind();

                ddl2.DataTextField = "FileName";
                ddl2.DataValueField = "MAID";
                ddl2.DataSource = matlabAssemblies;
                ddl2.DataBind();
            }
           
        #endregion

        #region Load Analysis

            protected void LoadAddFormIntoAnalysis(Analyse analysis)
            {
                //Analysis Name
                analysis.AnalysisName = txtAddAnalysisName.Text;
                //Matlab Assembly               
                analysis.MAID = Convert.ToInt32(ddlAddAssemblyFile.SelectedValue);
                //Analysis Base File Name               
                analysis.AnalysisBaseFileName = txtAddAnalysisBaseFileName.Text;
                //Analysis Function Name 
                analysis.AnalysisFunctionName = txtAddAnalysisFunctionName.Text;
                //Analysis Teaser
                analysis.AnalysisTeaser = String.IsNullOrEmpty(txtAddAnalysisTeaser.Value) ? null : txtAddAnalysisTeaser.Value;
                //Analysis Description
                analysis.AnalysisDescription = String.IsNullOrEmpty(txtAddAnalysisDescription.Value) ? null : txtAddAnalysisDescription.Value;

                analysis.IsCustom = chkAddCustom.Checked;
                analysis.IsMeta = chkAddMeta.Checked;
                analysis.IsUtilityBilling = chkAddUtility.Checked;

                //if meta 
                if (chkAddMeta.Checked)
                {
                    //TODO: set other meta vpoint data range fields
                }

                analysis.IsActive = true;
                analysis.IsVisible = true;
                analysis.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoAnalysis(Analyse analysis)
            {
                //ID
                analysis.AID = Convert.ToInt32(hdnEditID.Value);
                //Analysis Name
                analysis.AnalysisName = txtEditAnalysisName.Text;
                //Matlab Assembly
                analysis.MAID = Convert.ToInt32(ddlEditAssemblyFile.SelectedValue);
                //Analysis Base File Name
                analysis.AnalysisBaseFileName = txtEditAnalysisBaseFileName.Text;
                //Analysis Function Name
                analysis.AnalysisFunctionName = txtEditAnalysisFunctionName.Text;
                //Analysis Teaser               
                analysis.AnalysisTeaser = String.IsNullOrEmpty(txtEditAnalysisTeaser.Value) ? null : txtEditAnalysisTeaser.Value;
                //Analysis Description               
                analysis.AnalysisDescription = String.IsNullOrEmpty(txtEditAnalysisDescription.Value) ? null : txtEditAnalysisDescription.Value;

                //if meta 
                if (chkEditMeta.Checked)
                {
                    //TODO: set other meta vpoint data range fields
                }

                //Active
                analysis.IsActive = chkEditActive.Checked;

                //Visible
                analysis.IsVisible = chkEditVisible.Checked;
            }

        #endregion

        #region Dropdown and Checkbox Events

            /// <summary>
            /// chkAddCustom on checked changes, shows or hides client listboxes for applying a custom analysis to one or more clients
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void chkAddCustom_OnCheckedChanged(object sender, EventArgs e)
            {
                //show listboxes if custom is checked
                divAddClientsListBoxs.Visible = chkAddCustom.Checked;

                //clear listboxes
                lbAddClientsTop.Items.Clear();
                lbAddClientsBottom.Items.Clear();

                if (chkAddCustom.Checked)
                {
                    //rebind listboxes again to listboxs on edit user page
                    //get clients not associated to aid but available
                    IEnumerable<Client> clients = DataMgr.ClientDataMapper.GetAllClients();

                    //bind clients to top listbox
                    lbAddClientsTop.DataSource = clients;
                    lbAddClientsTop.DataTextField = "ClientName";
                    lbAddClientsTop.DataValueField = "CID";
                    lbAddClientsTop.DataBind();
                }
            }

            /// <summary>
            /// chkAddMeta on checked changes, shows or hides meta vpoint data range selections
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void chkAddMeta_OnCheckedChanged(object sender, EventArgs e)
            {
                //show listboxes if meta is checked
                //divAddSelectVPointDataRanges.Visible = chkAddMeta.Checked;
            }

            /// <summary>
            /// ddlBuildingVariablesAnalyses on selected index changed, binds building variables list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlBuildingVariablesAnalyses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlBuildingVariablesAnalyses.SelectedValue);

                if (aid != -1)
                {
                    BindBuildingVariables(aid);
                }
                else
                {
                    //hide building variables
                    buildingVariables.Visible = false;
                }

                //hide error message
                lblBuildingVariablesError.Visible = false;
            }

            /// <summary>
            /// ddlEquipmentVariablesAnalyses on selected index changed, binds equipment variables list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlEquipmentVariablesAnalyses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlEquipmentVariablesAnalyses.SelectedValue);

                if (aid != -1)
                {
                    BindEquipmentVariables(aid);
                }
                else
                {
                    //hide equipment variables
                    equipmentVariables.Visible = false;                    
                }

                //hide error message
                lblEquipmentVariablesError.Visible = false;
            }

            /// <summary>
            /// ddlInputPointTypesAnalyses on selected index changed, binds input point types list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlInputPointTypesAnalyses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlInputPointTypesAnalyses.SelectedValue);

                if (aid != -1)
                {
                    BindInputPointTypes(aid);
                }
                else
                {
                    //hide input point types
                    inputPointTypes.Visible = false;
                }

                //hide error messageroy
                lblInputPointTypesError.Visible = false;
            }

            /// <summary>
            /// ddlInputaVPointTypesAnalyses on selected index changed, binds input point types list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlInputVPointTypesAnalyses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlInputVPointTypesAnalyses.SelectedValue);

                if (aid != -1)
                {
                    BindInputVPointTypes(aid);
                }
                else
                {
                    //hide input vpoint types
                    inputVPointTypes.Visible = false;
                }

                //hide error message
                lblInputVPointTypesError.Visible = false;
            }


            /// <summary>
            /// ddlOutputVPointTypesAnalyses on selected index changed, binds output vpoint types list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlOutputVPointTypesAnalyses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlOutputVPointTypesAnalyses.SelectedValue);

                if (aid != -1)
                {
                    BindOutputVPointTypes(aid);
                }
                else
                {
                    //hide output vpoint types
                    outputVPointTypes.Visible = false;
                }

                //hide error message
                lblOutputVPointTypesError.Visible = false;
            }

            /// <summary>
            /// ddlOutputVPrePointTypesAnalyses on selected index changed, binds output vprepoint types list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlOutputVPrePointTypesAnalyses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlOutputVPrePointTypesAnalyses.SelectedValue);

                if (aid != -1)
                {
                    BindOutputVPrePointTypes(aid);
                }
                else
                {
                    //hide output vprepoint types
                    outputVPrePointTypes.Visible = false;
                }

                //hide error message
                lblOutputVPrePointTypesError.Visible = false;
            }

            /// <summary>
            /// ddlAnalysesEquipment on selected index changed, binds equipment analyses list boxes
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlAnalysisEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlAnalysisEquipment.SelectedValue);

                if (aid != -1)
                {
                    BindAnalysisEquipment(aid);
                }
                else
                {
                    //hide equipment analyses
                    analysisEquipment.Visible = false;
                }

                //hide error message
                lblAnalysesError.Visible = false;
            }


        #endregion

        #region Button Events

            /// <summary>
            /// Add Analysis Button on click.
            /// </summary>
            protected void addAnalysisButton_Click(object sender, EventArgs e)
            {
                bool analysisClientInsertSuccess = true;

                //check if analysis name exists in system already.
                if (!DataMgr.AnalysisDataMapper.DoesAnalysisNameExist(null, txtAddAnalysisName.Text))
                {
                    mAnalysis = new Analyse();

                    //load the form into the Analysis
                    LoadAddFormIntoAnalysis(mAnalysis);

                    try
                    {
                        //if custom 
                        if (chkAddCustom.Checked)
                        {
                            //insert new Analysis and return new aid
                            int newAID = DataMgr.AnalysisDataMapper.InsertAndReturnAID(mAnalysis);

                            //try to insert the client to analysis
                            try
                            {
                                //inserting each client in the bottom listbox.
                                //no need to check if they exist already since its the first addition                                
                                foreach (ListItem item in lbAddClientsBottom.Items)
                                {
                                    //declare new analyses_clients
                                    Analyses_Client mAnalysisClient = new Analyses_Client();

                                    mAnalysisClient.CID = Convert.ToInt32(item.Value);
                                    mAnalysisClient.AID = newAID;

                                    DataMgr.AnalysisClientsDataMapper.InsertAnalysisClient(mAnalysisClient);
                                }
                            }
                            catch (SqlException sqlEx)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning clients to custom analysis in kgs user administration.", sqlEx);
                                analysisClientInsertSuccess = false;
                            }
                            catch (Exception ex)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning clients to custom analysis in kgs user administration.", ex);
                                analysisClientInsertSuccess = false;
                            }
                        }
                        else
                        {
                            //insert new Analysis
                            DataMgr.AnalysisDataMapper.InsertAnalysis(mAnalysis);
                        }

                        if (!analysisClientInsertSuccess)
                        {
                            LabelHelper.SetLabelMessage(lblAddError, assigningClientsFailed, lnkSetFocusAdd);
                        }
                        else
                        {
                            LabelHelper.SetLabelMessage(lblAddError, mAnalysis.AnalysisName + addAnalysisSuccess, lnkSetFocusAdd);
                        }
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding Analysis", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addAnalysisFailed, lnkSetFocusAdd); 
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding Analysis", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addAnalysisFailed, lnkSetFocusAdd); 
                    }
                }
                else
                {
                    LabelHelper.SetLabelMessage(lblAddError, analysisExists, lnkSetFocusAdd); 
                }
            }

            /// <summary>
            /// Update Analysis Button on click. Updates analysis data.
            /// </summary>
            protected void updateAnalysisButton_Click(object sender, EventArgs e)
            {
                bool analysesClientEditSuccess = true;

                //create and load the form into the analysis
                Analyse mAnalysis = new Analyse();
                LoadEditFormIntoAnalysis(mAnalysis);

                //try update the analysis data
                try
                {
                    //check that is doenst already exist for another analysis                          
                    if (DataMgr.AnalysisDataMapper.DoesAnalysisNameExist(mAnalysis.AID, mAnalysis.AnalysisName))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, analysisExists, lnkSetFocusEdit);
                        return;
                    }

                    //update analysis
                    DataMgr.AnalysisDataMapper.UpdateAnalysis(mAnalysis);

                    //DEPRICATED
                    //remove all scheduled analysis for this analysis if isActive is unchecked. (inactive/false)
                    //if (!mAnalysis.IsActive)
                    //{
                    //    ScheduledAnalysisDataMapper.DeleteAllSchduledAnalysesByAID(mAnalysis.AID);
                    //}

                    if (chkEditCustom.Checked)
                    {
                        //deletes all analyses clients
                        DataMgr.AnalysisClientsDataMapper.DeleteAnalysesClientsByAID(Convert.ToInt32(mAnalysis.AID));

                        //assign analysis clients if clients exist in listbox---
                        if (lbEditClientsBottom.Items.Count > 0)
                        {
                            //declare new analyses_client. 
                            //quicker to delete all, and then insert all...
                            //rather then checkif if each exists then insert, and if exist delete

                            try
                            {
                                //inserting each client in the bottom listbox if they dont exist
                                foreach (ListItem item in lbEditClientsBottom.Items)
                                {
                                    int cid = Convert.ToInt32(item.Value);

                                    Analyses_Client mAnalysesClient = new Analyses_Client();
                                    mAnalysesClient.CID = cid;
                                    mAnalysesClient.AID = mAnalysis.AID;
                                    DataMgr.AnalysisClientsDataMapper.InsertAnalysisClient(mAnalysesClient);
                                }
                            }
                            catch (SqlException sqlEx)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning clients to a custom analysis in kgs user administration.", sqlEx);
                                analysesClientEditSuccess = false;
                            }
                            catch (Exception ex)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning clients to a custom analysis in kgs user administration.", ex);
                                analysesClientEditSuccess = false;
                            }
                        }
                    }


                    if (!analysesClientEditSuccess)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, reassigningClientsFailed, lnkSetFocusEdit); 
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusEdit); 
                    }

                    //Bind Analyses again
                    BindAnalyses();
                }
                catch (SqlException sqlEx)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit); 
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updateing Analysis.", sqlEx);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit); 
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updateing Analysis.", ex);
                }
            }

            /// <summary>
            /// on add Clients button up click, add clients
            /// </summary>
            protected void btnAddClientsUpButton_Click(object sender, EventArgs e)
            {
                while (lbAddClientsBottom.SelectedIndex != -1)
                {
                    lbAddClientsTop.Items.Add(lbAddClientsBottom.SelectedItem);
                    lbAddClientsBottom.Items.Remove(lbAddClientsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on add Clients button down click, add clients 
            /// </summary>
            protected void btnAddClientsDownButton_Click(object sender, EventArgs e)
            {
                while (lbAddClientsTop.SelectedIndex != -1)
                {
                    {
                        lbAddClientsBottom.Items.Add(lbAddClientsTop.SelectedItem);
                        lbAddClientsTop.Items.Remove(lbAddClientsTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on edit Clients button up click, add clients
            /// </summary>
            protected void btnEditClientsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientsBottom.SelectedIndex != -1)
                {
                    lbEditClientsTop.Items.Add(lbEditClientsBottom.SelectedItem);
                    lbEditClientsBottom.Items.Remove(lbEditClientsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit Clients button down click, add clients
            /// </summary>
            protected void btnEditClientsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientsTop.SelectedIndex != -1)
                {
                    lbEditClientsBottom.Items.Add(lbEditClientsTop.SelectedItem);
                    lbEditClientsTop.Items.Remove(lbEditClientsTop.SelectedItem);
                }
            }

            /// <summary>
            /// Update building variables button click, reassigns variables to analysis based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateBuildingVariablesButton_Click(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlBuildingVariablesAnalyses.SelectedValue);
                int bvid;

                try
                {
                    //inserting each building variable in the bottom listbox if they dont exist
                    foreach (ListItem item in lbBuildingVariablesBottom.Items)
                    {
                        bvid = Convert.ToInt32(item.Value);

                        //if the building variable doesnt already exist, insert
                        if (!DataMgr.BuildingVariableDataMapper.DoesBuildingVariableExistForAnalysis(aid, bvid))
                        {
                            //declare new analysis building variable
                            Analyses_BuildingVariable mAnalysisBuildingVariable = new Analyses_BuildingVariable();

                            mAnalysisBuildingVariable.AID = aid;
                            mAnalysisBuildingVariable.BVID = bvid;

                            mAnalysisBuildingVariable.DateModified = DateTime.UtcNow;

                            DataMgr.BuildingVariableDataMapper.InsertAnalysisBuildingVariable(mAnalysisBuildingVariable);
                        }
                    }

                    //deleting each building variable in the top listbox if they exist
                    foreach (ListItem item in lbBuildingVariablesTop.Items)
                    {
                        bvid = Convert.ToInt32(item.Value);
                        Analyses_BuildingVariable mAnalysisBuildingVariable = DataMgr.BuildingVariableDataMapper.GetBuildingVariableForAnalysis(aid, bvid);

                        //if the building variable exists, delete
                        if (mAnalysisBuildingVariable != null)
                        {
                            DataMgr.BuildingVariableDataMapper.DeleteBuildingVariableByAIDAndBVID(aid, bvid);
                        }
                    }

                    LabelHelper.SetLabelMessage(lblBuildingVariablesError, reassignBuildingVariablesUpdateSuccessful, lnkSetFocusBuildingVariables); 
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning building variables to analysis.", sqlEx);
                    LabelHelper.SetLabelMessage(lblBuildingVariablesError, reassignBuildingVariablesUpdateFailed, lnkSetFocusBuildingVariables); 
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning building variables to analysis.", ex);
                    LabelHelper.SetLabelMessage(lblBuildingVariablesError, reassignBuildingVariablesUpdateFailed, lnkSetFocusBuildingVariables); 
                }
            }

            /// <summary>
            /// Update equipment variables button click, reassigns variables to analysis based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateEquipmentVariablesButton_Click(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlEquipmentVariablesAnalyses.SelectedValue);
                int evid;

                //declare new analysis equipment variable
                Analyses_EquipmentVariable mAnalysisEquipmentVariable = null;

                try
                {
                    //inserting each equipment vairbale in the bottom listbox if they dont exist
                    foreach (ListItem item in lbEquipmentVariablesBottom.Items)
                    {
                        evid = Convert.ToInt32(item.Value);                                                          

                        //if the equipment variable doesnt already exist, insert
                        if (!DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableExistForAnalysis(aid, evid))
                        {
                            mAnalysisEquipmentVariable = new Analyses_EquipmentVariable();
                            mAnalysisEquipmentVariable.AID = aid;
                            mAnalysisEquipmentVariable.EVID = evid;

                            mAnalysisEquipmentVariable.DateModified = DateTime.UtcNow;

                            DataMgr.EquipmentVariableDataMapper.InsertAnalysisEquipmentVariable(mAnalysisEquipmentVariable);
                        }
                    }

                    //deleting each equipment variable in the top listbox if they exist
                    foreach (ListItem item in lbEquipmentVariablesTop.Items)
                    {
                        evid = Convert.ToInt32(item.Value);
                        mAnalysisEquipmentVariable = DataMgr.EquipmentVariableDataMapper.GetEquipmentVariableForAnalysis(aid, evid);
                        
                        //if the equipment variable exists, delete
                        if (mAnalysisEquipmentVariable != null)
                        {
                            DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariableByAIDAndEVID(aid, evid);
                        }
                    }

                    LabelHelper.SetLabelMessage(lblEquipmentVariablesError, reassignEquipmentVariablesUpdateSuccessful, lnkSetFocusEquipmentVariables); 
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment variables to analysis.", sqlEx);
                    LabelHelper.SetLabelMessage(lblEquipmentVariablesError, reassignEquipmentVariablesUpdateFailed, lnkSetFocusEquipmentVariables); 
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning equipment variables to analysis.", ex);
                    LabelHelper.SetLabelMessage(lblEquipmentVariablesError, reassignEquipmentVariablesUpdateFailed, lnkSetFocusEquipmentVariables); 
                }
            }

            /// <summary>
            /// Update input point types button click, reassigns input point types to analysis based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateInputPointTypesButton_Click(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlInputPointTypesAnalyses.SelectedValue);
                int pointTypeID;

                try
                {
                    //inserting each input point type in the bottom listbox if they dont exist
                    foreach (ListItem item in lbInputPointTypesBottom.Items)
                    {
                        pointTypeID = Convert.ToInt32(item.Value);

                        //if the input point type doesnt already exist, insert
                        if (!DataMgr.AnalysisPointTypeDataMapper.DoesInputPointTypeExistForAnalysis(aid, pointTypeID))
                        {
                            Analyses_InputPointType mInputPointType = new Analyses_InputPointType();
                            mInputPointType.AID = aid;
                            mInputPointType.PointTypeID = pointTypeID;
                            mInputPointType.DateModified = DateTime.UtcNow;
                            DataMgr.AnalysisPointTypeDataMapper.InsertInputPointType(mInputPointType);
                        }
                    }

                    //deleting each input point type in the top listbox if they exist
                    foreach (ListItem item in lbInputPointTypesTop.Items)
                    {
                        pointTypeID = Convert.ToInt32(item.Value);

                        //if the input point type exists, delete
                        if (DataMgr.AnalysisPointTypeDataMapper.DoesInputPointTypeExistForAnalysis(aid, pointTypeID))
                        {
                            DataMgr.AnalysisPointTypeDataMapper.DeleteInputPointTypeByAIDAndPointTypeID(aid, pointTypeID);  
                        }
                    }

                    LabelHelper.SetLabelMessage(lblInputPointTypesError, reassignInputPointTypesUpdateSuccessful, lnkSetFocusTypes); 
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning input point types to analysis.", sqlEx);
                    LabelHelper.SetLabelMessage(lblInputPointTypesError, reassignInputPointTypesUpdateFailed, lnkSetFocusTypes); 
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning input point types to analysis.", ex);
                    LabelHelper.SetLabelMessage(lblInputPointTypesError, reassignInputPointTypesUpdateFailed, lnkSetFocusTypes); 
                }
            }

            /// <summary>
            /// Update input vpoint types button click, reassigns input vpoint types to analysis based on selection
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateInputVPointTypesButton_Click(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlInputVPointTypesAnalyses.SelectedValue);
                int pointTypeID;

                //declare new input vpoint type

                try
                {
                    //inserting each input vpoint type in the bottom listbox if they dont exist
                    foreach (ListItem item in lbInputVPointTypesBottom.Items)
                    {
                        pointTypeID = Convert.ToInt32(item.Value);

                        //if the input vpoint type doesnt already exist, insert
                        if (!DataMgr.AnalysisPointTypeDataMapper.DoesInputVPointTypeExistForAnalysis(aid, pointTypeID))
                        {
                            Analyses_InputVPointType mInputVPointType = new Analyses_InputVPointType();
                            mInputVPointType.AID = aid;
                            mInputVPointType.PointTypeID = pointTypeID;
                            mInputVPointType.DateModified = DateTime.UtcNow;
                            DataMgr.AnalysisPointTypeDataMapper.InsertInputVPointType(mInputVPointType);
                        }
                    }

                    //deleting each input vpoint type in the top listbox if they exist
                    foreach (ListItem item in lbInputVPointTypesTop.Items)
                    {
                        pointTypeID = Convert.ToInt32(item.Value);

                        //if the input vpoint type exists, delete
                        if (DataMgr.AnalysisPointTypeDataMapper.DoesInputVPointTypeExistForAnalysis(aid, pointTypeID))
                        {
                            DataMgr.AnalysisPointTypeDataMapper.DeleteInputVPointTypeByAIDAndPointTypeID(aid, pointTypeID);
                        }
                    }

                    LabelHelper.SetLabelMessage(lblInputVPointTypesError, reassignInputVPointTypesUpdateSuccessful, lnkSetFocusVPointTypes); 
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning input vpoint types to analysis.", sqlEx);
                    LabelHelper.SetLabelMessage(lblInputVPointTypesError, reassignInputVPointTypesUpdateFailed, lnkSetFocusVPointTypes); 
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning input vpoint types to analysis.", ex);
                    LabelHelper.SetLabelMessage(lblInputVPointTypesError, reassignInputVPointTypesUpdateFailed, lnkSetFocusVPointTypes); 
                }
            }

            /// <summary>
            /// Update output vpoint types button click, reassigns output vpoint types to analysis based on selection,
            /// and reassigns output vpoint types to equipment already associated with that analysis.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateOutputVPointTypesButton_Click(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlOutputVPointTypesAnalyses.SelectedValue);
               
                Func<int, IEnumerable<PointType>> pointTypeAssociated = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPointTypesAssociatedToAID;
                Func<int, IEnumerable<PointType>> pointTypeNotAssociated = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPointTypesNotAssociatedToAID;

                var insertRequests = GetAddedPointTypes(lbOutputVPointTypesBottom.Items, pointTypeAssociated, aid);
                var deleteRequests = GetAddedPointTypes(lbOutputVPointTypesTop.Items, pointTypeNotAssociated, aid);

                if(!insertRequests.Any() && !deleteRequests.Any())
                {
                    LabelHelper.SetLabelMessage(lblOutputVPointTypesError, reassignNoChangeDetected, lnkSetFocusVPoint);
                    return;
                }

                try
                {
                    //get all equipment associated to the analysis.
                    Analyses_Equipment[] aes = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByAID(aid);

                    //inserting each NEW output vpoint type in the bottom listbox if they dont exist
                    foreach (PointType pt in insertRequests)
                    {
                        //declare new output vpoint type
                        Analyses_OutputVPointType outputVPointType = new Analyses_OutputVPointType();

                        InsertVPoint(aid, outputVPointType, aes, pt);                     
                    }

                    //deleting each NEW output vpoint type in the top listbox if they exist
                    foreach (PointType pt in deleteRequests)
                    {
                        var errorMessage = DeleteVPoint(aid, aes, pt);   

                        if(!String.IsNullOrWhiteSpace(errorMessage))
                        {
                            LabelHelper.SetLabelMessage(lblOutputVPointTypesError, outputVPointTypesVDataExists + " " + errorMessage, lnkSetFocusVPoint);
                            return;
                        }
                    }

                    LabelHelper.SetLabelMessage(lblOutputVPointTypesError, reassignOutputVPointTypesUpdateSuccessful, lnkSetFocusVPoint); 
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning output vpoint types to analysis.", sqlEx);
                    LabelHelper.SetLabelMessage(lblOutputVPointTypesError, reassignOutputVPointTypesUpdateFailed, lnkSetFocusVPoint);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning output vpoint types to analysis.", ex);
                    LabelHelper.SetLabelMessage(lblOutputVPointTypesError, reassignOutputVPointTypesUpdateFailed, lnkSetFocusVPoint);
                }  
            }

            /// <summary>
            /// Update output vprepoint types button click, reassigns output vprepoint types to analysis based on selection,
            /// and reassigns output vprepoint types to equipment already associated with that analysis.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void updateOutputVPrePointTypesButton_Click(object sender, EventArgs e)
            {
                int aid = Convert.ToInt32(ddlOutputVPrePointTypesAnalyses.SelectedValue);

                Func<int, IEnumerable<PointType>> pointTypeAssociated = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPrePointTypesAssociatedToAID;
                Func<int, IEnumerable<PointType>> pointTypeNotAssociated = DataMgr.AnalysisPointTypeDataMapper.GetAllOutputVPrePointTypesNotAssociatedToAID;

                var insertRequests = GetAddedPointTypes(lbOutputVPrePointTypesBottom.Items, pointTypeAssociated, aid);
                var deleteRequests = GetAddedPointTypes(lbOutputVPrePointTypesTop.Items, pointTypeNotAssociated, aid);

                if (!insertRequests.Any() && !deleteRequests.Any())
                {
                    LabelHelper.SetLabelMessage(lblOutputVPrePointTypesError, reassignNoChangeDetected, lnkSetFocusVPoint);
                    return;
                }

                try
                {
                    //get all equipment associated to the analysis.
                    Analyses_Equipment[] aes = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByAID(aid);

                    //inserting each NEW output vprepoint type in the bottom listbox if they dont exist
                    foreach (PointType pt in insertRequests)
                    {
                        //declare new output vprepoint type
                        Analyses_OutputVPrePointType OutputVPrePointType = new Analyses_OutputVPrePointType();

                        InsertVPrePoint(aid, OutputVPrePointType, aes, pt);
                    }

                    //deleting each NEW output vprepoint type in the top listbox if they exist
                    foreach (PointType pt in deleteRequests)
                    {
                        var errorMessage = DeleteVPrePoint(aid, aes, pt);

                        if (!String.IsNullOrWhiteSpace(errorMessage))
                        {
                            LabelHelper.SetLabelMessage(lblOutputVPrePointTypesError, outputVPrePointTypesVPreDataExists, lnkSetFocusVPrePoint);
                            return;
                        }
                    }

                    LabelHelper.SetLabelMessage(lblOutputVPrePointTypesError, reassignOutputVPrePointTypesUpdateSuccessful, lnkSetFocusVPrePoint);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning output vprepoint types to analysis.", sqlEx);
                    LabelHelper.SetLabelMessage(lblOutputVPrePointTypesError, reassignOutputVPrePointTypesUpdateFailed, lnkSetFocusVPrePoint);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning output vprepoint types to analysis.", ex);
                    LabelHelper.SetLabelMessage(lblOutputVPrePointTypesError, reassignOutputVPrePointTypesUpdateFailed, lnkSetFocusVPrePoint);
                }                  
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindAnalyses();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text = String.Empty;
                BindAnalyses();
            }

        #endregion

        #region ListBox Up/Down Button Events

            /// <summary>
            /// on button up click, remove building variables from bottom (unassociate to analysis)
            /// </summary>
            protected void btnBuildingVariablesUpButton_Click(object sender, EventArgs e)
            {
                while (lbBuildingVariablesBottom.SelectedIndex != -1)
                {
                    lbBuildingVariablesTop.Items.Add(lbBuildingVariablesBottom.SelectedItem);
                    lbBuildingVariablesBottom.Items.Remove(lbBuildingVariablesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add building variables to bottom (associate to analysis)
            /// </summary>
            protected void btnBuildingVariablesDownButton_Click(object sender, EventArgs e)
            {
                while (lbBuildingVariablesTop.SelectedIndex != -1)
                {
                    {
                        lbBuildingVariablesBottom.Items.Add(lbBuildingVariablesTop.SelectedItem);
                        lbBuildingVariablesTop.Items.Remove(lbBuildingVariablesTop.SelectedItem);
                    }
                }
            }


            /// <summary>
            /// on button up click, remove equipment variables from bottom (unassociate to analysis)
            /// </summary>
            protected void btnEquipmentVariablesUpButton_Click(object sender, EventArgs e)
            {
                while (lbEquipmentVariablesBottom.SelectedIndex != -1)
                {
                    lbEquipmentVariablesTop.Items.Add(lbEquipmentVariablesBottom.SelectedItem);
                    lbEquipmentVariablesBottom.Items.Remove(lbEquipmentVariablesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add equipment variables to bottom (associate to analysis)
            /// </summary>
            protected void btnEquipmentVariablesDownButton_Click(object sender, EventArgs e)
            {
                while (lbEquipmentVariablesTop.SelectedIndex != -1)
                {
                    {
                        lbEquipmentVariablesBottom.Items.Add(lbEquipmentVariablesTop.SelectedItem);
                        lbEquipmentVariablesTop.Items.Remove(lbEquipmentVariablesTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on button up click, remove input point types from bottom (unassociate to analysis)
            /// </summary>
            protected void btnInputPointTypesUpButton_Click(object sender, EventArgs e)
            {
                while (lbInputPointTypesBottom.SelectedIndex != -1)
                {
                    lbInputPointTypesTop.Items.Add(lbInputPointTypesBottom.SelectedItem);
                    lbInputPointTypesBottom.Items.Remove(lbInputPointTypesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add input point types to bottom (associate to analysis)
            /// </summary>
            protected void btnInputPointTypesDownButton_Click(object sender, EventArgs e)
            {
                while (lbInputPointTypesTop.SelectedIndex != -1)
                {
                    {
                        lbInputPointTypesBottom.Items.Add(lbInputPointTypesTop.SelectedItem);
                        lbInputPointTypesTop.Items.Remove(lbInputPointTypesTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on button up click, remove input vpoint types from bottom (unassociate to analysis)
            /// </summary>
            protected void btnInputVPointTypesUpButton_Click(object sender, EventArgs e)
            {
                while (lbInputVPointTypesBottom.SelectedIndex != -1)
                {
                    lbInputVPointTypesTop.Items.Add(lbInputVPointTypesBottom.SelectedItem);
                    lbInputVPointTypesBottom.Items.Remove(lbInputVPointTypesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add input vpoint types to bottom (associate to analysis)
            /// </summary>
            protected void btnInputVPointTypesDownButton_Click(object sender, EventArgs e)
            {
                while (lbInputVPointTypesTop.SelectedIndex != -1)
                {
                    {
                        lbInputVPointTypesBottom.Items.Add(lbInputVPointTypesTop.SelectedItem);
                        lbInputVPointTypesTop.Items.Remove(lbInputVPointTypesTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on button up click, remove output vpoint types from bottom (unassociate to analysis)
            /// </summary>
            protected void btnOutputVPointTypesUpButton_Click(object sender, EventArgs e)
            {
                while (lbOutputVPointTypesBottom.SelectedIndex != -1)
                {
                    lbOutputVPointTypesTop.Items.Add(lbOutputVPointTypesBottom.SelectedItem);
                    lbOutputVPointTypesBottom.Items.Remove(lbOutputVPointTypesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add output vpoint types to bottom (associate to analysis)
            /// </summary>
            protected void btnOutputVPointTypesDownButton_Click(object sender, EventArgs e)
            {
                while (lbOutputVPointTypesTop.SelectedIndex != -1)
                {
                    {
                        lbOutputVPointTypesBottom.Items.Add(lbOutputVPointTypesTop.SelectedItem);
                        lbOutputVPointTypesTop.Items.Remove(lbOutputVPointTypesTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on button up click, remove output vprepoint types from bottom (unassociate to analysis)
            /// </summary>
            protected void btnOutputVPrePointTypesUpButton_Click(object sender, EventArgs e)
            {
                while (lbOutputVPrePointTypesBottom.SelectedIndex != -1)
                {
                    lbOutputVPrePointTypesTop.Items.Add(lbOutputVPrePointTypesBottom.SelectedItem);
                    lbOutputVPrePointTypesBottom.Items.Remove(lbOutputVPrePointTypesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on button down click, add output vprepoint types to bottom (associate to analysis)
            /// </summary>
            protected void btnOutputVPrePointTypesDownButton_Click(object sender, EventArgs e)
            {
                while (lbOutputVPrePointTypesTop.SelectedIndex != -1)
                {
                    {
                        lbOutputVPrePointTypesBottom.Items.Add(lbOutputVPrePointTypesTop.SelectedItem);
                        lbOutputVPrePointTypesTop.Items.Remove(lbOutputVPrePointTypesTop.SelectedItem);
                    }
                }
            }
               
        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind Analysis grid after tab changed back from add Analysis tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindAnalyses();
                }
            }

        #endregion

        #region Grid Events

            protected void gridAnalyses_OnDataBound(object sender, EventArgs e)
            {
                //check if kgs admin or higher, hide edit and delete column if not
                //bool kgsAdmin = RoleHelper.IsKGSAdminOrHigher(Session["UserRoleID"].ToString());
                //gridAnalyses.Columns[1].Visible = kgsAdmin;
                //gridAnalyses.Columns[7].Visible = kgsAdmin;                                         
            }

            protected void gridAnalyses_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditAnalysis.Visible = false;
                dtvAnalysis.Visible = true;

                int aid = Convert.ToInt32(gridAnalyses.DataKeys[gridAnalyses.SelectedIndex].Values["AID"]);               

                //set data source
                dtvAnalysis.DataSource = DataMgr.AnalysisDataMapper.GetAnalysisByAID(aid);
                //bind Analysis to details view
                dtvAnalysis.DataBind();
            }

            protected void gridAnalyses_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridAnalyses_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvAnalysis.Visible = false;
                pnlEditAnalysis.Visible = true;

                int aid = Convert.ToInt32(gridAnalyses.DataKeys[e.NewEditIndex].Values["AID"]);

                Analyse mAnalysis = DataMgr.AnalysisDataMapper.GetAnalysis(aid);
            
                //Set Analysis data
                SetAnalysisIntoEditForm(mAnalysis);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridAnalyses_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows analysisID
                int aid = Convert.ToInt32(gridAnalyses.DataKeys[e.RowIndex].Value);

                try
                {
                    //Check analysis to equipmenttypes exist
                    if (DataMgr.AnalysisEquipmentTypeDataMapper.IsAnalysisAssociatedWithEquipmentTypes(aid))
                    {
                        lblErrors.Text = equipmentTypesExist;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                        return;
                    }


                    //***THIS MIGHT NOT BE NESSISARY ANYMORE BECAUSE YOU HAVE TO REMOVE ALL EQUIPMENT FROM THE ANAlYSES BEFORE YOU CAN DISASSOCAITE THE EQUIPMENT TYPES
                    //***EITHER WAY THE Analyses_Equipmet below SHOULD BE EMPTY, so no HARM.
                    //check if ANY analyzed data exists for this analysis.                    
                    //its harder to check if any priorityrows exist for this aid based on our partition and row key configuration.
                    //so the only thing we can do is prevent deletion if any vdata exits for any vpoint for this analysis.
                    //no need to check vpredata as vdata will always exists for a priority.
                    //you can just get all analyses_equipment and check each one because we prevent an analysis,
                    //from being unassigned to an equipment if vdata exists.
                
                    //get all equipment associated to the analysis.
                    Analyses_Equipment[] aes = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByAID(aid);
                    
                    bool exists = false;
                    bool isUtilityBilling = DataMgr.AnalysisDataMapper.GetAnalysisByAID(aid).First().IsUtilityBilling;

                    //check if vdata exists for each analysis equipment.
                    //no need to check vpredata as vdata will always exists for a priority.
                    //break out of foreach loop on first find of data

                    if (!isUtilityBilling)
                    {
                        foreach (Analyses_Equipment ae in aes)
                        {
                            if (DataMgr.VAndVPreDataMapper.DoesVDataExistForEIDAndAID(ae.EID, ae.AID))
                            {
                                exists = true;
                                break;
                            }
                        }
                    }                                   
                    
                    //check if vdatautilitybilling exists for each analysis equipment.
                    //no need to check vpredata as vdata will always exists for a priority.
                    //break out of foreach loop on first find of data
                    if (isUtilityBilling)
                    { 
                        foreach (Analyses_Equipment ae in aes)
                        {
                            if (DataMgr.VAndVPreDataMapper.DoesVDataUtilityBillingExistForEIDAndAID(DataConstants.UtiliityAnalysisTypes.Utility, ae.EID, ae.AID))
                            {
                                exists = true;
                                break;
                            }
                        }
                    }

                    if (exists)
                    {
                        lblErrors.Text = vOrVPreDataExists;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //Only allow deletion of an analysis if no equipment has
                        //been associated (active or not doesnt matter).  
                        //And if no analyses have been scheduled, should be checked on the 
                        //analysis to equipment association tab on the equipment page.

                        //check if any equipment has been associated to the analysis
                        bool hasEquipment = DataMgr.AnalysisEquipmentDataMapper.IsAnyEquipmentAssociatedToAnalysis(aid);

                        if (hasEquipment)
                        {
                            lblErrors.Text = equipmentExist;
                            lblErrors.Visible = true;
                            lnkSetFocusView.Focus();
                        }
                        else
                        {
                            //delete all building variables for analysis
                            DataMgr.BuildingVariableDataMapper.DeleteBuildingVariablesByAID(aid);
                            //delete all equipment variables for analysis
                            DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariablesByAID(aid);
                            //delete all input point,output vpoint,and output vprepoint types for analysis
                            DataMgr.AnalysisPointTypeDataMapper.DeleteInputVPointTypesByAID(aid);
                            //AnalysisPointTypeDataMapper.DeleteInputVPrePointTypesByAID(analysisID);
                            DataMgr.AnalysisPointTypeDataMapper.DeleteInputPointTypesByAID(aid);
                            DataMgr.AnalysisPointTypeDataMapper.DeleteOutputVPointTypesByAID(aid);
                            DataMgr.AnalysisPointTypeDataMapper.DeleteOutputVPrePointTypesByAID(aid);
                            DataMgr.PointDataMapper.DeleteAllVPointsByAID(aid);
                            DataMgr.PointDataMapper.DeleteAllVPrePointsByAID(aid);

                            //maybe check iscustom bit instead to avoid this query, but safer to just clean up anyways.
                            //delete all custom client analysis relationships
                            DataMgr.AnalysisClientsDataMapper.DeleteAnalysesClientsByAID(aid);

                            //delete analysis
                            DataMgr.AnalysisDataMapper.DeleteAnalysis(aid);

                            lblErrors.Text = deleteSuccessful;
                            lblErrors.Visible = true;
                            lnkSetFocusView.Focus();

                            pnlEditAnalysis.Visible = false;
                            dtvAnalysis.Visible = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting analysis.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryAnalyses());
                gridAnalyses.PageIndex = gridAnalyses.PageIndex;
                gridAnalyses.DataSource = SortDataTable(dataTable as DataTable, true);
                gridAnalyses.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditAnalysis.Visible = false;
            }

            protected void gridAnalyses_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedAnalyses(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridAnalyses.DataSource = SortDataTable(dataTable, true);
                gridAnalyses.PageIndex = e.NewPageIndex;
                gridAnalyses.DataBind();
            }

            protected void gridAnalyses_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridAnalyses.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedAnalyses(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridAnalyses.DataSource = SortDataTable(dataTable, false);
                gridAnalyses.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetAnalysisData> QueryAnalyses()
            {
                try
                {
                    //get all analyses 
                    return DataMgr.AnalysisDataMapper.GetAllFullAnalyses();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving analyses.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving analyses.", ex);
                    return null;
                }
            }

            private IEnumerable<GetAnalysisData> QuerySearchedAnalyses(string searchText)
            {
                try
                {
                    //get all analyses
                    //only show visible analyses to the client
                    return DataMgr.AnalysisDataMapper.GetAllFullSearchedAnalyses(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving analyses.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving analyses.", ex);
                    return null;
                }
            }

            private void BindAnalyses()
            {
                //query equipment
                IEnumerable<GetAnalysisData> analyses = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryAnalyses() : QuerySearchedAnalyses(txtSearch.Text);

                var count = analyses.Count();

                gridAnalyses.DataSource = analyses.ToList();
                gridAnalyses.DataBind();

                SetGridCountLabel(count);

                dtvAnalysis.Visible = (count > 0);
            }

            //private void BindSearchedAnalyses(string[] searchText)
            //{
            //    //query data sources
            //    IEnumerable<Analyse> analyses = QuerySearchedAnalyses(searchText);

            //    int count = analyses.Count();

            //    gridAnalyses.DataSource = analyses;

            //    // bind grid
            //    gridAnalyses.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(Int32 count)
            {
                var formattedName = LanguageHelper.GetFormattedName("analysis", (count == 1));
                var formattedTextNoResults = String.Format("No {0} found.", formattedName);
                var formattedTextResults = String.Format("{0} {1} found.", count, formattedName);

                if (count == 0)
                {
                    LabelHelper.SetLabelMessage(lblResults, formattedTextNoResults, null);
                    return;
                }

                LabelHelper.SetLabelMessage(lblResults, formattedTextResults, null);
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            private IEnumerable<PointType> GetAddedPointTypes(ListItemCollection items, Func<int, IEnumerable<PointType>> pointTypeLookup, int aid)
            {
                var selectedPointTypes = ConvertListItemToVPointList(items);
                var initialPointTypes = pointTypeLookup(aid).ToList();

                return selectedPointTypes.Where(pt => !initialPointTypes.Select(cpt => cpt.PointTypeID).Contains(pt.PointTypeID)).Select(pt => pt).ToList();
            }

            private IEnumerable<PointType> ConvertListItemToVPointList(ListItemCollection items)
            {
                return items.Cast<ListItem>().Select(item =>
                    new PointType()
                    {
                        PointTypeName = item.Text,
                        PointTypeID = Convert.ToInt32(item.Value)
                    }
                ).ToList();
            }

            private string DeleteVPoint(int aid, Analyses_Equipment[] aes, PointType pointType)
            {
                string errorMessage = "";

                //if the output vpoint type exists, delete
                //if (mDataManager.AnalysisPointTypeDataMapper.DoesOutputVPointTypeExistForAnalysis(aid, pointType.PointTypeID))
                //{
                    //check if vdata exists for each analysis equipment.
                    //break out of foreach loop on first find of data
                    foreach (Analyses_Equipment ae in aes)
                    {
                        if (DataMgr.VAndVPreDataMapper.DoesVDataExistForEIDAndAIDAndPointTypeID(ae.EID, ae.AID, pointType.PointTypeID))
                        {
                            errorMessage = String.Format("VData exists for the following: EquipmentID {0}, AnalysisID {1}, PointType {2}({3})",
                                ae.EID, ae.AID, pointType.PointTypeName, pointType.PointTypeID);
                            break;
                        }
                    }

                    //check if vdatautilitybilling exists for each analysis equipment.
                    //break out of foreach loop on first find of data
                    if (String.IsNullOrWhiteSpace(errorMessage) && DataMgr.AnalysisDataMapper.GetAnalysisByAID(aid).First().IsUtilityBilling)
                    {
                        foreach (Analyses_Equipment ae in aes)
                        {
                            if (DataMgr.VAndVPreDataMapper.DoesVDataUtilityBillingExistForEIDAndAIDAndPointTypeID(DataConstants.UtiliityAnalysisTypes.Utility, ae.EID, ae.AID, pointType.PointTypeID))
                            {
                                errorMessage = String.Format("VData Utility Billing exists for the following: EquipmentID {0}, AnalysisID {1}, PointType {2}({3})",
                                    ae.EID, ae.AID, pointType.PointTypeName, pointType.PointTypeID);
                                break;
                            }
                        }
                    }

                    if (String.IsNullOrWhiteSpace(errorMessage))
                    {
                        //no need to check if an analysis is assigned to a equipment that requires that point type,
                        //because a virtual point type is never assigned to a equipment directly.
                        DataMgr.AnalysisPointTypeDataMapper.DeleteOutputVPointTypeByAIDAndPointTypeID(aid, pointType.PointTypeID);

                        //delete all output vpointtypes in vpointtable for analysis by aid and pointtypeid.
                        //meaning delete all vpoints for that pointtypeid and analysis for every equipment.
                        DataMgr.PointDataMapper.DeleteAllVPointsByAIDAndPointTypeID(aid, pointType.PointTypeID);
                    }
                //}

                return errorMessage;
            }

            private void InsertVPoint(int aid, Analyses_OutputVPointType outputVPointType, Analyses_Equipment[] aes, PointType pointType)
            {
                //if the output vpoint type doesnt already exist, insert
                if (!DataMgr.AnalysisPointTypeDataMapper.DoesOutputVPointTypeExistForAnalysis(aid, pointType.PointTypeID))
                {
                    outputVPointType.AID = aid;
                    outputVPointType.PointTypeID = pointType.PointTypeID;
                    DataMgr.AnalysisPointTypeDataMapper.InsertOutputVPointType(outputVPointType);
                }

                //for each analysis equipment
                foreach (Analyses_Equipment ae in aes)
                {
                    //check if output vpointtype exists in vpointtable, else create it.                            
                    if (!DataMgr.PointDataMapper.DoesVPointExistByIDs(aid, ae.EID, pointType.PointTypeID))
                    {
                        VPoint vp = new VPoint();
                        vp.AID = aid;
                        vp.EID = ae.EID;
                        vp.PointTypeID = pointType.PointTypeID;

                        DataMgr.PointDataMapper.InsertVPoint(vp);
                    }
                }
            }

            private string DeleteVPrePoint(int aid, Analyses_Equipment[] aes, PointType pointType)
            {
                string errorMessage = "";

                //if the output vprepoint type exists, delete
                //if (mDataManager.AnalysisPointTypeDataMapper.DoesOutputVPrePointTypeExistForAnalysis(aid, pointType.PointTypeID))
                //{
                    //check if vpredata exists for each analysis equipment.
                    //break out of foreach loop on first find of data
                    foreach (Analyses_Equipment ae in aes)
                    {
                        if (DataMgr.VAndVPreDataMapper.DoesVPreDataExistForEIDAndAID(ae.EID, ae.AID))
                        {
                            errorMessage = String.Format("VPreData exists for the following: EquipmentID {0}, AnalysisID {1}, PointType {2}({3})",
                                ae.EID, ae.AID, pointType.PointTypeName, pointType.PointTypeID);
                            break;
                        }
                    }
                    //check if vpredatautilitybilling exists for each analysis equipment.
                    //break out of foreach loop on first find of data
                    if (String.IsNullOrWhiteSpace(errorMessage) && DataMgr.AnalysisDataMapper.GetAnalysisByAID(aid).First().IsUtilityBilling)
                    {
                        foreach (Analyses_Equipment ae in aes)
                        {
                            if (DataMgr.VAndVPreDataMapper.DoesVPreDataUtilityBillingExistForEIDAndAID(DataConstants.UtiliityAnalysisTypes.Utility, ae.EID, ae.AID))
                            {
                                errorMessage = String.Format("VData Utility Billing exists for the following: EquipmentID {0}, AnalysisID {1}, PointType {2}({3})",
                                    ae.EID, ae.AID, pointType.PointTypeName, pointType.PointTypeID);
                                break;
                            }
                        }
                    }

                    if (String.IsNullOrWhiteSpace(errorMessage))
                    {
                        //no need to check if an analysis is assigned to a equipment that requires that point type,
                        //because a virtual predicted point type is never assigned to a equipment directly.
                        DataMgr.AnalysisPointTypeDataMapper.DeleteOutputVPrePointTypeByAIDAndPointTypeID(aid, pointType.PointTypeID);

                        //delete all output vpointtypes in vpointtable for analysis by aid and pointtypeid.
                        //meaning delete all vpoints for that pointtypeid and analysis for every equipment.
                        DataMgr.PointDataMapper.DeleteAllVPrePointsByAIDAndPointTypeID(aid, pointType.PointTypeID);
                    }
                //}
                return errorMessage;
            }

            private void InsertVPrePoint(int aid, Analyses_OutputVPrePointType outputVPrePointType, Analyses_Equipment[] aes, PointType pointType)
            {
                //if the output vprepoint type doesnt already exist, insert
                if (!DataMgr.AnalysisPointTypeDataMapper.DoesOutputVPrePointTypeExistForAnalysis(aid, pointType.PointTypeID))
                {
                    outputVPrePointType.AID = aid;
                    outputVPrePointType.PointTypeID = pointType.PointTypeID;
                    DataMgr.AnalysisPointTypeDataMapper.InsertOutputVPrePointType(outputVPrePointType);
                }

                //for each analysis equipment
                foreach (Analyses_Equipment ae in aes)
                {
                    //check if output vprepointtype exists in vpointtable, else create it.                            
                    if (!DataMgr.PointDataMapper.DoesVPrePointExistByIDs(aid, ae.EID, pointType.PointTypeID))
                    {
                        VPrePoint vpp = new VPrePoint();
                        vpp.AID = aid;
                        vpp.EID = ae.EID;
                        vpp.PointTypeID = pointType.PointTypeID;

                        DataMgr.PointDataMapper.InsertVPrePoint(vpp);
                    }
                }
            }

        #endregion
    }
}