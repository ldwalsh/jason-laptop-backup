﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Specialized;
using System.Collections.Generic;

using CW.Business;
using CW.Data.AzureStorage;
using CW.Data;
using CW.Data.AzureStorage.Helpers;
using CW.Data.Collections;
using CW.Website._framework;
using CW.Data.Models.Diagnostics;
using CW.Business.Results;
using CW.Utility;
using CW.Data.Helpers;
using System.Reflection;
using CW.Common.Helpers;
using CW.Common.Constants;
using CW.Data.Models.Raw;
using CW.Data.Interfaces.Serialization;
using CW.Data.Serialization;
using CW.Data.Interfaces.State;
using CW.Data.Extensions;
using System.Diagnostics;
using CW.Data.AzureStorage.Models.Queue;
using System.Text;
using CW.Data.Models.Alarm;
using CW.Logging;
using System.Linq.Expressions;
using CW.Data.Models.Utility;
using CW.Business.Messaging.Schedule;
using CW.Business.Query;
using CW.Data.Models.Building;
using CW.Utility.Web;
using CW.Caching;

namespace CW.Website
{
    public class CacheOperation
    {
        public string ClientName { get; set; }
        public int CID { get; set; }
        public string BuildingName { get; set; }
        public int BID { get; set; }
        public string CacheType { get; set; }
        public string Range { get; set; }
        public long TimeElasped { get; set; }
        public long Size { get; set; }
        public int Rows { get; set; }
    }

    public partial class SystemCache : SitePage
    {
        #region Enums

            public enum CacheType
            {
                Diagnostics = 0,
                AlarmRawData = 1,
            }

        #endregion

        #region Fields

            const string initialSortDirection = "ASC";
            const string initialSortExpression = "ClientName";

            //private List<BuildingDiagnosticsCacheStatus> buildingCacheList;
            private IEnumerable<Building> mVisibleBuildings;

        
        #endregion
            
        #region Properties

            public IEnumerable<Building> VisibleBuildings
            {
                get
                {
                    if (mVisibleBuildings == null)
                    {
                        mVisibleBuildings = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID, new Expression<Func<Building, Object>>[] { (b => b.Client) }).OrderBy(b => b.BuildingName).OrderBy(b => b.Client.ClientName);
                    }

                    return mVisibleBuildings;
                }
                set { mVisibleBuildings = value; }
            }

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                ResetMessaging();

                if (!Page.IsPostBack)
                {
                    BindDiagnosticCache();
                    BindViewCache();
                    BindInterpreterCache();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    lblErrors.Visible = false;
                }
            }

        #endregion

        #region Grid Events

            #region Diagnostics

                protected void gridBuildingDiagnosticsCache_OnDataBound(object sender, EventArgs e)
                {
                }

                protected void gridBuildingDiagnosticsCache_OnRowCreated(object sender, GridViewRowEventArgs e)
                {
                    CreatePager(e);
                }

                protected void gridBuildingDiagnosticsCache_Deleting(object sender, GridViewDeleteEventArgs e)
                {
                    IOrderedDictionary dka = gridBuildingDiagnosticsCache.DataKeys[e.RowIndex].Values;

                    int cid = Convert.ToInt32(dka["CID"]);
                    int bid = Convert.ToInt32(dka["BID"]);
                    DateTime startDate = Convert.ToDateTime(dka["StartDate"]);
                    DateTime endDate = Convert.ToDateTime(dka["EndDate"]);
                    DataConstants.AnalysisRange range = AnalysisHelper.GetAnalysisRange(dka["AnalysisRange"].ToString());
                    String buildingName = dka["BuildingName"].ToString();
                    String clientName = dka["ClientName"].ToString();
                    string resultMessage = String.Format("{0}, {1} for {2} analysis range for {3}-{4}",
                        clientName, buildingName, AnalysisHelper.GetAnalysisRangeTextValue(range), startDate.ToShortDateString(), endDate.ToShortDateString());

                    var query = new BuildingSettingQuery(DataMgr.ConnectionString, null);
                    var buildingSetting = query.LoadWith(bs => bs.Building).FinalizeLoadWith.GetById(bid);
                    var buildingOffset = BuildingOffset.CreateFromLocalTime(buildingSetting, endDate);

                    // Refresh row item cache
                    try
                    {
                        var scheduler = new ScheduleExecutorCacheDiag(DataMgr, null, LogMgr);
                        var inputs = new ScheduleInputsCacheDiag(range, CacheAction.Refresh, TimeSpan.Zero, startDate == endDate, new BuildingOffsetCollection(buildingOffset, true), ScheduleInputs.ScheduleType.Building);
                        scheduler.Execute(inputs);

                        BindDiagnosticCache();

                        lblResults.Text = "Refresh successfully scheduled for " + resultMessage;
                    }
                    catch (Exception ex)
                    {
                        lblErrors.Visible = true; 
                        lblErrors.Text = "Refresh failed for " + resultMessage + ExceptionHelper.WriteExceptionMessage(ex);
                    }
                }

                protected void gridBuildingDiagnosticsCache_PageIndexChanging(object sender, GridViewPageEventArgs e)
                {
                    DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryDiagnosticCache());

                    //maintain current sort direction and expresstion on paging
                    gridBuildingDiagnosticsCache.DataSource = SortDataTable(dataTable, true);
                    gridBuildingDiagnosticsCache.PageIndex = e.NewPageIndex;
                    gridBuildingDiagnosticsCache.DataBind();
                }

                protected void gridBuildingDiagnosticsCache_Sorting(object sender, GridViewSortEventArgs e)
                {
                    //Reset the edit index.
                    //gridBuildingDiagnosticsCache.EditIndex = -1;

                    DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryDiagnosticCache());

                    GridViewSortExpression = e.SortExpression;

                    gridBuildingDiagnosticsCache.DataSource = SortDataTable(dataTable, false);
                    gridBuildingDiagnosticsCache.DataBind();
                }

                protected void gridBuildingDiagnosticsCache_SelectedIndexChanged(object sender, EventArgs e)
                {
                    IOrderedDictionary dka = gridBuildingDiagnosticsCache.DataKeys[gridBuildingDiagnosticsCache.SelectedIndex].Values;

                    int cid = Convert.ToInt32(dka["CID"]);
                    int bid = Convert.ToInt32(dka["BID"]);
                    DateTime startDate = Convert.ToDateTime(dka["StartDate"]);
                    DateTime endDate = Convert.ToDateTime(dka["EndDate"]);
                    DataConstants.AnalysisRange range = AnalysisHelper.GetAnalysisRange(dka["AnalysisRange"].ToString());
                    String buildingName = dka["BuildingName"].ToString();
                    String clientName = dka["ClientName"].ToString();

                    string resultMessage = String.Format("{0}, {1} for {2} analysis range for {3}-{4}", clientName, buildingName, AnalysisHelper.GetAnalysisRangeTextValue(range), startDate.ToString(AzureConstants.DATETIME_INVARIANT_DAY_FORMAT), endDate.ToString(AzureConstants.DATETIME_INVARIANT_DAY_FORMAT));

                    // Get cache count
                    try
                    {
                        Stopwatch sw = new Stopwatch();
                        sw.Start();
                        var drm = new DiagnosticsResultsManager(DataMgr, new DiagnosticsResultsInputs(cid, bid, null, null, null, false, startDate, endDate, range, null));
                        sw.Stop();

                        lblResults.Text = String.Format("{0} items from cache for {1} in {2} ms", drm.GetResultSetCount(), resultMessage, sw.ElapsedMilliseconds);
                    }
                    catch (Exception ex)
                    {
                        lblErrors.Visible = true; 
                        lblErrors.Text = "Retrieving cache failed for " + resultMessage + ExceptionHelper.WriteExceptionMessage(ex);
                    }
                }

            #endregion

            #region Alarm

                protected void gridAlarmCache_OnDataBound(object sender, EventArgs e)
                {
                }

                protected void gridAlarmCache_OnRowCreated(object sender, GridViewRowEventArgs e)
                {
                    CreatePager(e);
                }

                protected void gridAlarmCache_Deleting(object sender, GridViewDeleteEventArgs e)
                {
                    IOrderedDictionary dka = gridAlarmCache.DataKeys[e.RowIndex].Values;

                    int cid = Convert.ToInt32(dka["CID"]);
                    DateTime startDateLocal = Convert.ToDateTime(dka["StartDate"]);
                    DateTime endDateLocal = Convert.ToDateTime(dka["EndDate"]);
                    Duration duration = null;
                    Duration.TryParseFromValue(dka["Duration"].ToString(), out duration);
                    String clientName = dka["ClientName"].ToString();

                    string resultMessage = String.Format("{0} for {1} duration for {2}-{3}", clientName, duration.DisplayName,
                    startDateLocal.ToShortDateString(), startDateLocal.ToShortDateString());

                    var query = new BuildingSettingQuery(DataMgr.ConnectionString, null);
                    var buildingOffsets = query.LoadWith(bs => bs.Building).FinalizeLoadWith.ByClient(cid).ToEnumerable().Select(bs=> BuildingOffset.CreateFromLocalTime(bs, startDateLocal));

                    var clientMaxOffset = buildingOffsets.OrderBy(bo => bo.TotalOffset.TotalHours).First();

                    var buffer = TimeSpan.FromHours(2);
                    var timespan = DateTime.UtcNow.Subtract(clientMaxOffset.DateTimeUTC.Add(clientMaxOffset.RawDataDelay).Add(buffer));

                    if (timespan.TotalMilliseconds > 0)
                    {
                        lblAlarmResults.Text = String.Format("Raw Data is delayed {0} hrs, please wait {1} hrs to try again ", clientMaxOffset.RawDataDelay.TotalHours, timespan.TotalHours);
                        return;
                    }

                    // Refresh row item cache
                    try
                    {
                        var scheduleInputs = new ScheduleInputsCacheAlarm(new BuildingOffsetCollection(clientMaxOffset, true), TimeSpan.Zero, ScheduleInputs.ScheduleType.Client);
                        var scheduler = new ScheduleExecutorCacheAlarm(DataMgr, DataMgr.ClientDataMapper.GetCIDsWithClientModule, null, LogMgr);
                        scheduler.Execute(scheduleInputs);

                        lblAlarmResults.Text = "Scheduled Refresh successful for " + resultMessage;
                    }
                    catch (Exception ex)
                    {
                        lblAlarmErrors.Visible = true;
                        lblAlarmErrors.Text = "Scheduled Refresh failed for " + resultMessage + ExceptionHelper.WriteExceptionMessage(ex);
                    }
                }

                protected void gridAlarmCache_PageIndexChanging(object sender, GridViewPageEventArgs e)
                {
                    DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryAlarmCache());

                    //maintain current sort direction and expresstion on paging
                    gridAlarmCache.DataSource = SortDataTable(dataTable, true);
                    gridAlarmCache.PageIndex = e.NewPageIndex;
                    gridAlarmCache.DataBind();
                }

                protected void gridAlarmCache_Sorting(object sender, GridViewSortEventArgs e)
                {
                    //Reset the edit index.
                    //gridBuildingDiagnosticsCache.EditIndex = -1;

                    DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryAlarmCache());

                    GridViewSortExpression = e.SortExpression;

                    gridAlarmCache.DataSource = SortDataTable(dataTable, false);
                    gridAlarmCache.DataBind();
                }

                protected void gridAlarmCache_SelectedIndexChanged(object sender, EventArgs e)
                {
                    IOrderedDictionary dka = gridAlarmCache.DataKeys[gridAlarmCache.SelectedIndex].Values;

                    int cid = Convert.ToInt32(dka["CID"]);
                    DateTime startDate = Convert.ToDateTime(dka["StartDate"]);
                    DateTime endDate = Convert.ToDateTime(dka["EndDate"]);
                    Duration duration = Duration.FromDisplayName<Duration>(dka["Duration"].ToString());
                    String clientName = dka["ClientName"].ToString();
                    string resultMessage = String.Format("{0} for {1} duration for {2}-{3}", clientName, duration.DisplayName, startDate.ToString(AzureConstants.DATETIME_INVARIANT_DAY_FORMAT), endDate.ToString(AzureConstants.DATETIME_INVARIANT_DAY_FORMAT));

                    var inputs = new AlarmRawDataInputs(){  CID = cid, Duration = duration, EndDateLocal = endDate, StartDateLocal = startDate};

                    // Get cache count
                    try
                    {
                        string format = "{0} items from cache for widget {1} for {2} in {3} {4} ms {5}";

                        Stopwatch sw = new Stopwatch();
                        StringBuilder sb = new StringBuilder();
                        DataManager dm = DataMgr;
                        foreach(var value in Enum.GetValues(typeof(BusinessConstants.Alarm.AlarmWidgetsEnum)))
                        {
                            inputs.AlarmWidget = (BusinessConstants.Alarm.AlarmWidgetsEnum)value;

                            sw.Restart();
                            var arm = new AlarmResultsManager(dm, DataMgr.AlarmDataMapper.GetAlarmCalcFunc(inputs.AlarmWidget), inputs);
                            arm.HasGetFromStorage = false;
                            sb.AppendLine(string.Format(format, arm.GetResultSetCount(),Enum.GetName(typeof(BusinessConstants.Alarm.AlarmWidgetsEnum), value), 
                                value, resultMessage, sw.ElapsedMilliseconds, arm.Key)); 
                            sw.Stop();
                        }

                        lblAlarmResults.Text = sb.Replace("\n", "<br>").ToString();
                    }
                    catch (Exception ex)
                    {
                        lblAlarmErrors.Visible = true;
                        lblAlarmErrors.Text = "Retrieving cache failed for " + resultMessage + ExceptionHelper.WriteExceptionMessage(ex);
                    }
                }

                #endregion

            #region Kiosk
        
                protected void gridKioskCache_OnDataBound(object sender, EventArgs e)
                {
                }

                protected void gridKioskCache_OnRowCreated(object sender, GridViewRowEventArgs e)
                {
                    CreatePager(e);
                }

                protected void gridKioskCache_Deleting(object sender, GridViewDeleteEventArgs e)
                {
                    IOrderedDictionary dka = gridKioskCache.DataKeys[e.RowIndex].Values;

                    int cid = Convert.ToInt32(dka["CID"]);
                    KioskCacheType cacheType = (KioskCacheType)Convert.ToInt32(dka["TypeID"]);
                    string cacheTypeAsString = Enum.GetName(typeof(KioskCacheType), cacheType);
                    string clientName = dka["ClientName"].ToString();
                    string resultMessage = String.Format("Kiosk Cache data refreshed for {0} ({1})", clientName, cacheTypeAsString);

                    // Refresh row item cache
                    try
                    {
                        BindKioskCache();

                        var scheduler = new ScheduleExecutorCacheKiosk(DataMgr, DataMgr.ModuleDataMapper.DoesModuleExistForClient, null, LogMgr);
                        scheduler.Execute(new ScheduleInputsCacheKiosk(cid, new[] { cacheType }, ScheduleInputs.ScheduleType.Client));

                        lblKioskResults.Text = "Refresh successful for " + resultMessage;
                    }
                    catch (Exception ex)
                    {
                        lblKioskErrors.Visible = true;
                        lblKioskErrors.Text = "Refresh failed for " + resultMessage + ExceptionHelper.WriteExceptionMessage(ex);
                    }
                }

                protected void gridKioskCache_PageIndexChanging(object sender, GridViewPageEventArgs e)
                {
                    DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryKioskCache());

                    //maintain current sort direction and expresstion on paging
                    gridBuildingDiagnosticsCache.DataSource = SortDataTable(dataTable, true);
                    gridBuildingDiagnosticsCache.PageIndex = e.NewPageIndex;
                    gridBuildingDiagnosticsCache.DataBind();
                }

                protected void gridKioskCache_Sorting(object sender, GridViewSortEventArgs e)
                {
                    //Reset the edit index.
                    //gridBuildingDiagnosticsCache.EditIndex = -1;

                    DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryKioskCache());

                    GridViewSortExpression = e.SortExpression;

                    gridKioskCache.DataSource = SortDataTable(dataTable, false);
                    gridKioskCache.DataBind();
                }

                protected void gridKioskCache_SelectedIndexChanged(object sender, EventArgs e)
                {
                    IOrderedDictionary dka = gridKioskCache.DataKeys[gridKioskCache.SelectedIndex].Values;

                    int cid = Convert.ToInt32(dka["CID"]);
                    KioskCacheType cacheType = (KioskCacheType)Convert.ToInt32(dka["TypeID"]);
                    String clientName = dka["ClientName"].ToString();

                    // Get cache count
                    try
                    {
                        bool isFound = true;
                        int count = 0;

                        Stopwatch sw = new Stopwatch();
                        sw.Start();

                        switch (cacheType)
                        {
                            case KioskCacheType.Building:
                                count = GetKioskBuildingResultsManager(cid).GetResultSetCount();
                                break;
                            case KioskCacheType.BuildingStatistics:
                                count = GetBuildingStatisticsResultsManager().GetResultSetCount();
                                break;
                            case KioskCacheType.BuildingUtility:
                                count = GetBuildingUtilitiesResultsManager().GetResultSetCount();
                                break;
                            default:
                                isFound = false;
                                break;
                        }

                        sw.Stop();

                        if (!isFound)
                        {
                            lblKioskResults.Text = "Count retrieval unsuccessful for " + clientName + ", Type ID not found " + cacheType;
                            return;
                        }

                        lblKioskResults.Text = String.Format("Retrieved {0} items from cached for {1} in {2} ms", count, clientName, sw.ElapsedMilliseconds);
                    }
                    catch (Exception ex)
                    {
                        lblKioskErrors.Visible = true;
                        lblKioskErrors.Text = "Retrieving cache failed for " + clientName + ExceptionHelper.WriteExceptionMessage(ex);
                    }
                }

                #region private

                    private BuildingStatisticsResultsManager GetBuildingStatisticsResultsManager()
                    {
                        var bsm = new BuildingStatisticsResultsManager(DataMgr, new KioskBuildingStatsInputs() { Creds = siteUser, EquipmentClassLabelDict = BusinessConstants.EquipmentClass.KioskEquipmentClassTotalLabelDictionary });
                        return bsm;
                    }

                    private BuildingUtilitiesResultsManager GetBuildingUtilitiesResultsManager()
                    {
                        DateTime utcYesterday = DateTime.UtcNow.AddDays(-1);

                        var burm = new BuildingUtilitiesResultsManager(DataMgr, new UtilityBuildingInputs() { Creds = siteUser, StartDate = new DateTime(utcYesterday.Year, utcYesterday.Month, utcYesterday.Day) });
                        return burm;
                    }

                    private KioskBuildingResultsManager GetKioskBuildingResultsManager(int cid)
                    {
                        var uriString = siteUser.IsSchneiderTheme ? ConfigurationManager.AppSettings["SchneiderElectricSiteAddress"] : ConfigurationManager.AppSettings["SiteAddress"];
                        var host = Utility.UrlHelper.GetHost(uriString);
                        var kbrm = new KioskBuildingResultsManager(
                        DataMgr, new KioskBuildingInputs()
                        {
                            CID = cid,
                            ColorPointTypeIDs = BusinessConstants.PointType.KioskColorPointTypeIDs,
                            EnergyUsePointTypeIDs = BusinessConstants.PointType.KioskEnergyUsePointTypeIDs,
                            EnergyPointClassIDs = BusinessConstants.PointClass.KioskTopLevelPointClassIDs,
                            PowerPointClassIDs = EnumHelper.ToArray<BusinessConstants.PointClass.PowerPointClassEnum>(),
                            Creds = siteUser
                        }, (clientID, bid, imageExt) => { return HandlerHelper.BuildingImageUrl(clientID, bid, imageExt); });
                        return kbrm;
                    }

                #endregion

            #endregion

            #region View Cache and Performance

                protected void gridViewCache_OnDataBound(object sender, EventArgs e)
                {
                }

                protected void gridViewCache_OnRowCreated(object sender, GridViewRowEventArgs e)
                {
                    CreatePager(e);
                }
                protected void gridViewCache_PageIndexChanging(object sender, GridViewPageEventArgs e)
                {
                    //maintain current sort direction and expresstion on paging
                    gridViewCache.DataSource = SortDataTable(QueryViewCache(), true);
                    gridViewCache.PageIndex = e.NewPageIndex;
                    gridViewCache.DataBind();

                }

                protected void gridViewCache_Sorting(object sender, GridViewSortEventArgs e)
                {
                    GridViewSortExpression = e.SortExpression;

                    gridBuildingDiagnosticsCache.DataSource = SortDataTable(QueryViewCache(), false);
                    gridBuildingDiagnosticsCache.DataBind();
                }

            #endregion

        #endregion

        #region Dropdown Events

            protected void ddlClients_SelectedIndexChanged(object sender, EventArgs e)
            {
                Bind<DataSource>(ddlDataSources, PropHelper.G<DataSource>(ds=>ds.DataSourceName), PropHelper.G<DataSource>(ds=>ds.DSID),
                    DataMgr.DataSourceDataMapper.GetAllDataSourcesByCID(Convert.ToInt32(ddlClients.SelectedValue)), new ListItem("All", "-1")); 
            }

        #endregion

        #region Button Events

            protected void btnViewCacheSubmit_Click(object sender, EventArgs e)
            {
                gridViewCache.DataSource = SortDataTable(QueryViewCache(), false);
                gridViewCache.DataBind();
            }

            protected void btnInterpreterCacheSubmit_Click(object sender, EventArgs e)
            {
                int clientID = Convert.ToInt32(ddlClients.SelectedValue);
                int dataSourceID = Convert.ToInt32(ddlDataSources.SelectedValue);
                InterpreterLookupCacheMessage msg;
                string cacheAddType;
                string cacheAddValue;

                if (clientID == -1)
                {
                    lblInterpreterError.Text = "Please select Client";
                    return;
                }

                if (dataSourceID == -1)
                {
                    cacheAddType = "Client";
                    cacheAddValue = ddlClients.SelectedItem.Text;
                    msg = new InterpreterLookupCacheMessage(clientID, InterpreterLookupCacheMessage.UpdateByEnum.UpdateByCID);
                }
                else
                {
                    cacheAddType = "Data Source";
                    cacheAddValue = ddlDataSources.SelectedItem.Text;
                    msg = new InterpreterLookupCacheMessage(dataSourceID, InterpreterLookupCacheMessage.UpdateByEnum.UpdateByDSID);
                }

                DataMgr.InterpreterDataMapper.AddInterpreterLookupCacheMessage(msg);

                lblInterpreterResults.Text = String.Format("Successfully scheduled {0}: {1} to be refreshed", cacheAddType, cacheAddValue); 
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind equipment variable grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind equipment variables to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindDiagnosticCache();
                }
                else if (radTabStrip.MultiPage.SelectedIndex == 1)
                {                    
                    BindKioskCache();
                }
                else if (radTabStrip.MultiPage.SelectedIndex == 2)
                {
                    BindAlarmCache();
                }

            }

        #endregion

        #region Search events

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchDiagonsticsButton_Click(object sender, EventArgs e)
            {
                Session["Search"] = txtSearch.Text;
                BindDiagnosticCache();
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchAlarmButton_Click(object sender, EventArgs e)
            {
                Session["AlarmSearch"] = txtAlarmSearch.Text;
                BindAlarmCache();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewDiagnosticsAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                Session["Search"] = String.Empty;
                BindDiagnosticCache();
            }

            protected void viewAlarmAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                Session["Search"] = String.Empty;
                BindAlarmCache();
            }

            //protected void searchCacheButton_Click(object sender, EventArgs e)
            //{
            //    Session["SearchView"] = txtSearch.Text;
            //    BindViewCache();
            //}

            //protected void viewCacheAll_Click(object sender, EventArgs e)
            //{
            //    txtSearch.Text = "";
            //    Session["SearchView"] = String.Empty;
            //    BindViewCache();
            //}

        #endregion

        #region Cache Retrieval

            private DataTable QueryViewCache()
            {
                DataTable dt = new DataTable();

                // optional right now
                //if (!HasInputError(ddlBuildings, "Building"))
                //    return;

                if (HasInputError(ddlCacheTypes, "Cache Type"))
                    return dt;

                CacheType cacheType = (CacheType)Enum.Parse(typeof(CacheType), ddlCacheTypes.SelectedValue);

                switch (cacheType)
                {
                    case CacheType.AlarmRawData:
                        dt = EnumerableHelper.ConvertIEnumerableToDataTable(GetFromCache<GetBuildingRawData>(siteUser.CID, Convert.ToInt32(ddlBuildings.SelectedValue), cacheType));
                        break;
                    case CacheType.Diagnostics:
                        dt = EnumerableHelper.ConvertIEnumerableToDataTable(GetFromCache<DiagnosticsResult>(siteUser.CID, Convert.ToInt32(ddlBuildings.SelectedValue), cacheType));
                        break;
                }

                return dt;
            }

            private IEnumerable<CacheOperation> GetFromCache<T>(int cid, int bid, CacheType cacheType)
            {
                Stopwatch sw = new Stopwatch();
                var cacheOperations = new List<CacheOperation>();

                if (HasDefaultSetting(ddlBuildings))
                {
                    var buildings = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(cid);

                    foreach (Building building in buildings)
                    {
                        cacheOperations.Add(ExecuteCacheCommand<T>(building.CID, building.BID, cacheType, sw));
                    }
                }
                else
                {
                    cacheOperations.Add(ExecuteCacheCommand<T>(cid, bid, cacheType, sw));
                }

                return cacheOperations;
            }

            private CacheOperation ExecuteCacheCommand<T>(int cid, int bid, CacheType cacheType, Stopwatch sw)
            {
                IDataSerializer serializer = new Formatter();
                ICacheStore cacheStore = DataMgr.ProcessedDataCacheRepository;
                string buildingName = DataMgr.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false).BuildingName;
                string key = CreateCacheKey(cid, bid, cacheType);
                sw.Restart();
                //var rsm = new ResultSetManager<T>(AzureConstants.AZURE_SINGLE_CACHE_QUOTA - AzureConstants.AZURE_SINGLE_CACHE_BUFFER, key, serializer);
                List<T> result = new List<T>();

                try
                {
                    var r = cacheStore.Get<IEnumerable<T>>(key);
                        //rsm.Get(cacheStore.Get<int?>, cacheStore.Get<IEnumerable<T>>) ?? Enumerable.Empty<T>();
                    result = r.ToList();
                }
                catch (Exception ex)
                {
                    lblErrors.Text = lblErrors.Text + String.Format("<br/><br/>Unable to get cache for Building Name:{0}", buildingName);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error on Cache Get for Building Name:{0} BID:{1}", buildingName, bid), ex);
                }
                sw.Stop();

                CW.Data.Serialization.SerializationSizer ss = new CW.Data.Serialization.SerializationSizer();
                serializer.Serialize<List<T>>(result, ss);


                return
                    new CacheOperation()
                    {
                        ClientName = siteUser.ClientName,
                        CacheType = Enum.GetName(typeof(CacheType), cacheType),
                        Range = "Daily",
                        Size = ss.Length,
                        Rows = result.Count(),
                        TimeElasped = sw.ElapsedMilliseconds,
                        BID = bid,
                        CID = cid,
                        BuildingName = buildingName
                    };
            }

            private string CreateCacheKey(int cid, int bid, CacheType cacheType)
            {
                string key = "";
                DateTime yesterdayForBuilding = GetYesterdaysByBuildingTimeZone(bid);
                DateTime startDate = DateTime.MaxValue;

                // just doing Daily for yesterday for this page right now
                switch (cacheType)
                {
                    case CacheType.Diagnostics:
                        startDate = DateTimeHelper.GetXDaysAgo(yesterdayForBuilding, BusinessConstants.Diagnostics.DiagnosticsDailyCacheInDays);

                        var dri = new DiagnosticsResultsInputs()
                        {
                            AnalysisRange = Common.Constants.DataConstants.AnalysisRange.Daily,
                            StartDate = startDate,
                            EndDate = yesterdayForBuilding,
                            CID = cid,
                            BID = bid
                        };

                        key = new DiagnosticsResultsStateKey(dri, dri.StartDate, dri.EndDate).ToString();
                        break;
                    case CacheType.AlarmRawData:
                        startDate = yesterdayForBuilding;

                        var ardi = new AlarmRawDataInputs()
                        {
                            CID = cid,
                        };

                        key = new AlarmDataStateKey(ardi).ToString();

                        break;
                }
                return key;
            }

        #endregion

        #region Helper Methods

            private static string RefreshAlarmCacheResults(DataManager dm, AlarmRawDataInputs inputs, BusinessConstants.Alarm.AlarmWidgetsEnum widget, IEnumerable<GetBuildingRawDataWithCalc> data)
            {
                inputs.AlarmWidget = widget;
                var grm = new AlarmResultsManager(dm, null, inputs);

                try
                {
                    grm.SetData(data);
                    grm.Refresh();

                    string msg = String.Format("Saved Results {0} results for Widget {1} Alarm raw data for CID: {2} Cache Keys: {3}, {4}",
                                            grm.GetResultSetCount(),
                                            Enum.GetName(typeof(BusinessConstants.Alarm.AlarmWidgetsEnum), inputs.AlarmWidget),
                                            inputs.CID, new AlarmDataStateKey(inputs).ToString(), new AlarmDataStateKey(inputs).ToString());

                    return msg;
                }
                catch (Exception ex)
                {
                    if (ex.IsFatal())
                    {
                        throw;
                    }

                    string msg = String.Format("Saved Results failed for {0} results for Widget {1} Alarm raw data for CID: {2} Cache Keys: {3}, {4}",
                                                grm.GetResultSetCount(),
                                                Enum.GetName(typeof(BusinessConstants.Alarm.AlarmWidgetsEnum), inputs.AlarmWidget),
                                                inputs.CID, new AlarmDataStateKey(inputs).ToString(), new AlarmDataStateKey(inputs).ToString());

                    return msg;
                }
            }


            private static void CreatePager(GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            private void ResetMessaging()
            {
                lblResults.Text = "";
                lblErrors.Text = "";
                lblErrors.Visible = false;

                lblAlarmResults.Text = "";
                lblAlarmErrors.Text = "";
                lblAlarmErrors.Visible = false;
            }

            private IEnumerable<BuildingDiagnosticsCacheStatus> QueryDiagnosticCache(string[] searchString = null)
            {
                var dailyRange = DataConstants.AnalysisRange.Daily;
                var weeklyRange = DataConstants.AnalysisRange.Weekly;
                var monthlyRange = DataConstants.AnalysisRange.Monthly;
                var halfDayRange = DataConstants.AnalysisRange.HalfDay;

                var cacheStatusList = new List<BuildingDiagnosticsCacheStatus>();

                foreach (Building building in VisibleBuildings)
                {
                    if (searchString != null && !searchString.Where(b=>building.BuildingName.Contains(b)).Any())
                    {
                        continue;
                    }

                    var buildingTimeZone = DataMgr.BuildingDataMapper.GetBuildingByBID(building.BID, false, false, false, false, false, false).TimeZone;
                    var tzi = TimeZoneInfo.FindSystemTimeZoneById(buildingTimeZone);

                    var inputtedStartDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, tzi.Id).Date;


                    var today = inputtedStartDate;
                    //DateTime.UtcNow;
                    var yesterday = today.AddDays(-1);

                    var startDateDaily = DateTimeHelper.GetXDaysAgo(yesterday, BusinessConstants.Diagnostics.DiagnosticsDailyCacheInDays);
                    var startDateWeekly = DateTimeHelper.GetXWeeksAgo(DateTimeHelper.GetLastSunday(yesterday), BusinessConstants.Diagnostics.DiagnosticsWeeklyCacheInWeeks);
                    var startDateMonthly = DateTimeHelper.GetXMonthsAgo(DateTimeHelper.GetFirstOfMonth(yesterday), BusinessConstants.Diagnostics.DiagnosticsMonthlyCacheInMonths);
                    var startDateHalfDay = today;

                    var clientName = DataMgr.ClientDataMapper.GetClientName(building.CID);

                    // YESTERDAY
                    //var drm = new DiagnosticsResultsManager(mDataManager, new DiagnosticsResultsInputs(building.CID, building.BID, null, null, null, false, startDateDaily, yesterday, dailyRange));

                    cacheStatusList.Add(
                        new BuildingDiagnosticsCacheStatus
                        {
                            BID = building.BID,
                            BuildingName = building.BuildingName,
                            CID = building.CID,
                            ClientName = clientName,
                            StartDate = yesterday.ToShortDateString(),
                            EndDate = yesterday.ToShortDateString(),
                            AnalysisRange = AnalysisHelper.GetAnalysisRangeTextValue(dailyRange),
                            //HasData = drm.GetCachedStatus()
                        }
                    );

                    // DAILY
                    //var drm = new DiagnosticsResultsManager(mDataManager, new DiagnosticsResultsInputs(building.CID, building.BID, null, null, null, false, startDateDaily, yesterday, dailyRange));

                    cacheStatusList.Add(
                        new BuildingDiagnosticsCacheStatus
                        {
                            BID = building.BID,
                            BuildingName = building.BuildingName,
                            CID = building.CID,
                            ClientName = clientName,
                            StartDate = startDateDaily.ToShortDateString(),
                            EndDate = yesterday.ToShortDateString(),
                            AnalysisRange = AnalysisHelper.GetAnalysisRangeTextValue(dailyRange),
                            //HasData = drm.GetCachedStatus()
                        }
                    );

                    // WEEKLY
                    //drm = new DiagnosticsResultsManager(mDataManager, new DiagnosticsResultsInputs(building.CID, building.BID, null, null, null, false, startDateWeekly, yesterday, weeklyRange));

                    cacheStatusList.Add(
                        new BuildingDiagnosticsCacheStatus
                        {
                            BID = building.BID,
                            BuildingName = building.BuildingName,
                            CID = building.CID,
                            ClientName = clientName,
                            StartDate = startDateWeekly.ToShortDateString(),
                            EndDate = yesterday.ToShortDateString(),
                            AnalysisRange = AnalysisHelper.GetAnalysisRangeTextValue(weeklyRange),
                            //HasData = drm.GetCachedStatus()
                        }
                    );

                    // MONTHLY
                    //drm = new DiagnosticsResultsManager(mDataManager, new DiagnosticsResultsInputs(building.CID, building.BID, null, null, null, false, startDateMonthly, yesterday, monthlyRange));

                    cacheStatusList.Add(
                        new BuildingDiagnosticsCacheStatus
                        {
                            BID = building.BID,
                            BuildingName = building.BuildingName,
                            CID = building.CID,
                            ClientName = clientName,
                            StartDate = startDateMonthly.ToShortDateString(),
                            EndDate = yesterday.ToShortDateString(),
                            AnalysisRange = AnalysisHelper.GetAnalysisRangeTextValue(monthlyRange),
                            //HasData = drm.GetCachedStatus()
                        }
                    );

                    // HALF DAY
                    //drm = new DiagnosticsResultsManager(mDataManager, new DiagnosticsResultsInputs(building.CID, building.BID, null, null, null, false, startDateHalfDay, yesterday, halfDayRange));

                    cacheStatusList.Add(
                        new BuildingDiagnosticsCacheStatus
                        {
                            BID = building.BID,
                            BuildingName = building.BuildingName,
                            CID = building.CID,
                            ClientName = clientName,
                            StartDate = startDateHalfDay.ToShortDateString(),
                            EndDate = today.ToShortDateString(),
                            AnalysisRange = AnalysisHelper.GetAnalysisRangeTextValue(halfDayRange),
                            //HasData = drm.GetCachedStatus()
                        }
                    );
                }



                return cacheStatusList;
            }

            private IEnumerable<AlarmCacheStatus> QueryAlarmCache(string[] searchString = null)
            {
                var duration = Duration.Daily;
                var durationName = duration.DisplayName;

                var cacheStatusList = new List<AlarmCacheStatus>();

                TimeZoneInfo tzi = DataMgr.BuildingDataMapper.GetLastestBuildingTimeZoneByCID(siteUser.CID);

                //if any buildings
                if(tzi != null)
                {
                    string timezoneID = tzi.Id;

                    var drs = new DateRangeSetter();
                    drs.SetRangeFromUTCDate(
                        duration, 
                        DateTime.UtcNow.AddDays(-BusinessConstants.Alarm.AlarmsDailyCacheInDays), 
                        DateTime.UtcNow, timezoneID);

                    drs.Ranges.Reverse();

                    foreach(DateRange dr in drs.Ranges)
                    {
                        cacheStatusList.Add(new AlarmCacheStatus()
                        {
                            CID = siteUser.CID,
                            ClientName = siteUser.ClientName,
                            StartDate = dr.StartDateLocal.ToShortDateString(),
                            EndDate = dr.EndDateLocal.ToShortDateString(),
                            Duration = durationName
                        });
                    }
                }

                return cacheStatusList;
            }

            private IEnumerable<BuildingDiagnosticsCacheStatus> QueryKioskCache()
            {
                var cacheStatusList = new List<BuildingDiagnosticsCacheStatus>();
                
                    cacheStatusList.Add(
                        new BuildingDiagnosticsCacheStatus
                        {
                            TypeID = (int)KioskCacheType.Building,
                            Label = "Kiosk Buildings",
                            ClientName = siteUser.ClientName,
                            CID = siteUser.CID
                        });

                    cacheStatusList.Add(
                        new BuildingDiagnosticsCacheStatus
                        {
                            TypeID = (int)KioskCacheType.BuildingStatistics,
                            Label = "Kiosk Building Statistics",
                            ClientName = siteUser.ClientName,
                            CID = siteUser.CID
                        });

                    cacheStatusList.Add(
                        new BuildingDiagnosticsCacheStatus
                        {
                            TypeID = (int)KioskCacheType.BuildingUtility,
                            Label = "Kiosk Building Utility (Yesterday)",
                            ClientName = siteUser.ClientName,
                            CID = siteUser.CID
                        });                

                return cacheStatusList;
            }

            private void BindDiagnosticCache()
            {
                IEnumerable<BuildingDiagnosticsCacheStatus> cacheStatusList = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryDiagnosticCache() : QueryDiagnosticCache(txtSearch.Text.Split(' '));

                int count = cacheStatusList.Count();
            
                gridBuildingDiagnosticsCache.DataSource = cacheStatusList;

                // bind grid
                gridBuildingDiagnosticsCache.DataBind();

                SetGridCountLabel(count);
            }

            private void BindAlarmCache()
            {
                IEnumerable<AlarmCacheStatus> cacheStatusList = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryAlarmCache() : QueryAlarmCache(txtSearch.Text.Split(' '));

                int count = cacheStatusList.Count();

                gridAlarmCache.DataSource = cacheStatusList;

                // bind grid
                gridAlarmCache.DataBind();

                SetAlarmGridCountLabel(count);
            }

            private void BindKioskCache()
            {
                IEnumerable<BuildingDiagnosticsCacheStatus> cacheStatusList = Enumerable.Empty<BuildingDiagnosticsCacheStatus>();

                if (DataMgr.ModuleDataMapper.DoesModuleExistForClient(siteUser.CID, Convert.ToInt32(Common.Constants.BusinessConstants.Module.Modules.Kiosk)))
                   cacheStatusList = QueryKioskCache();

                int count = cacheStatusList.Count();

                gridKioskCache.DataSource = cacheStatusList;

                // bind grid
                gridKioskCache.DataBind();

                SetKioskGridCountLabel(count);
            }

            private void BindViewCache()
            {
                Bind<Building>(ddlBuildings, "BuildingName", "BID", DataMgr.BuildingDataMapper.GetAllVisibleBuildingsByCID(siteUser.CID, null));
                BindCacheTypes();
            }

            private void BindInterpreterCache()
            {
                Bind<Client>(ddlClients, "ClientName", "CID", DataMgr.ClientDataMapper.GetAllActiveClients());
            }

            private void BindCacheTypes()
            {
                ddlCacheTypes.DataSource = Enum.GetNames(typeof(CacheType));
                ddlCacheTypes.DataBind();
            }

            private void Bind<T>(DropDownList ddl, string textField, string valueField, IEnumerable<T> data, ListItem customHeader = null)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();

                if (customHeader != null)
                    ddl.Items.Add(customHeader);
                else
                    ddl.Items.Add(lItem);

                ddl.DataTextField = textField;
                ddl.DataValueField = valueField;
                ddl.DataSource = data;
                ddl.DataBind();
            }

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} building diagnostic cache found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} building diagnostic caches found.", count);
                }
                else
                {
                    lblResults.Text = "No building diagnostic caches found.";
                }
            }

            private void SetKioskGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblKioskResults.Text = String.Format("{0} kiosk cache found.", count);
                }
                else if (count > 1)
                {
                    lblKioskResults.Text = String.Format("{0} kiosk caches found.", count);
                }
                else
                {
                    lblKioskResults.Text = "No kiosk caches found.";
                }
            }

            private void SetAlarmGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblAlarmResults.Text = String.Format("{0} alarm cache found.", count);
                }
                else if (count > 1)
                {
                    lblAlarmResults.Text = String.Format("{0} alarm caches found.", count);
                }
                else
                {
                    lblAlarmResults.Text = "No alarm caches found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            private DateTime GetYesterdaysByBuildingTimeZone(int bid)
            {
                var buildingTimeZone = DataMgr.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false).TimeZone;
                var tzi = TimeZoneInfo.FindSystemTimeZoneById(buildingTimeZone);


                return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, tzi.Id).Date.AddDays(-1);
            }


            private bool HasInputError(DropDownList ddl, string ddlName)
            {
                if (HasDefaultSetting(ddl))
                {
                    lblViewError.Text = String.Format("Please select: {0} value", ddlName);
                    return true;
                }

                return false;
            }

            private bool HasDefaultSetting(DropDownList ddl)
            {
                if (ddl.SelectedValue == "-1")
                    return true;

                return false;
            }

        #endregion
    }
}
