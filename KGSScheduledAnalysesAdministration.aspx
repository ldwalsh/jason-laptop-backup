﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSScheduledAnalysesAdministration.aspx.cs" Inherits="CW.Website.KGSScheduledAnalysesAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/ScheduledAnalyses/ViewScheduledAnalyses.ascx" tagname="ViewScheduledAnalyses" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/ScheduledAnalyses/ScheduleAnalysisForAnEquipment.ascx" tagname="ScheduleAnalysisForAnEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/ScheduledAnalyses/ScheduleAnalysisForMultipleEquipment.ascx" tagname="ScheduleAnalysisForMultipleEquipment" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/ScheduledAnalyses/BulkEditScheduledAnalyses.ascx" tagname="BulkEditScheduledAnalyses" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/ScheduledAnalyses/BulkDeleteScheduledAnalyses.ascx" tagname="BulkDeleteScheduledAnalyses" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <h1>KGS Scheduled Analyses Administration</h1>

      <div class="richText">The kgs scheduled analyses administration area is used to view, add, and edit scheduled analyses.</div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>                                      
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div>
        
      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" CausesValidation="false" runat="server" SelectedIndex="0" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="View Sch. Analyses" />
            <telerik:RadTab Text="Sch. Analysis For An Equip." />
            <telerik:RadTab Text="Sch. Analysis For Multiple Equip." />
            <telerik:RadTab Text="Bulk Edit Sch. Analyses" />
            <telerik:RadTab Text="Bulk Delete Sch. Analyses" />
          </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:ViewScheduledAnalyses ID="ViewScheduledAnalyses" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:ScheduleAnalysisForAnEquipment ID="ScheduleAnalysisForAnEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView3" runat="server">
            <CW:ScheduleAnalysisForMultipleEquipment ID="ScheduleAnalysisForMultipleEquipment" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView4" runat="server">  
            <CW:BulkEditScheduledAnalyses ID="BulkEditScheduledAnalyses" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView5" runat="server">                                
            <CW:BulkDeleteScheduledAnalyses ID="BulkDeleteScheduledAnalyses" runat="server" />
          </telerik:RadPageView>                             
        </telerik:RadMultiPage>
      </div>

</asp:Content>