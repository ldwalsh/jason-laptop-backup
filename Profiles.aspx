﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Profile.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="Profiles.aspx.cs" Inherits="CW.Website.Profiles" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

     <div id="intro" class="pod">
    	<div class="top"> </div>
        <div id="middle" class="middle">                    	                  	        	
        	<div class="profilesModuleIcon">
                <h1>Profiles</h1>                        
            </div>
                <div class="richText">   
                    <asp:Literal ID="litProfilesBody" runat="server"></asp:Literal>             
                </div>  
                <div class="divProfileButtons">                    
                    <asp:LinkButton PostBackUrl="~/BuildingProfile.aspx" runat="server" CssClass="lnkButtonBuildingProfiles"></asp:LinkButton>
                    <asp:LinkButton PostBackUrl="~/EquipmentProfile.aspx" runat="server" CssClass="lnkButtonEquipmentProfiles"></asp:LinkButton>                    
                </div>                    
         </div>
    </div>            
    
</asp:Content>                