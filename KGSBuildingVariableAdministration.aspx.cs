﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Helpers;
using CW.Data.Models.Building;
using CW.Data.Models.BuildingVariable;
using CW.Utility;
using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSBuildingVariableAdministration : SitePage
    {
        #region Properties

            private BuildingVariable mBuildingVariable;
            const string addBuildingVariableSuccess = " building variable addition was successful.";
            const string addBuildingVariableFailed = "Adding building variable failed. Please contact an administrator.";
            const string addBuildingVariableNameExists = "Cannot add building variable because the name already exists.";
            const string addBuildingVariableDisplayNameExists = "Cannot add building variable because the display name already exists.";
            const string addBuildingVariableFailedValidation = "Adding building variable failed. Please see error(s) below.";
            const string updateSuccessful = "Building variable update was successful.";
            const string updateFailed = "Building variable update failed. Please contact an administrator.";
            const string updateFailedBuildingVariableValidation = "Building variable update failed. Please see error(s) below.";
            const string updateBuildingVariableNameExists = "Cannot update building variable name because the name already exists.";
            const string updateBuildingVariableDisplayNameExists = "Cannot update building variable display name because the display name already exists.";     
            const string deleteSuccessful = "Building variable deletion was successful.";
            const string deleteFailed = "Building variable deletion failed. Please contact an administrator.";            
            const string associatedToBuilding = "Cannot delete building variable becuase an building requires it.";
            const string associatedToAnalysis = "Cannot delete building variable becuase an analysis requires it.";
            const string associatedToBuildingClass = "Cannot delete building variable becuase its associated to one or more building classes. Please disassociate from classes first.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "BuildingVariableDisplayName";
            const string buildingvariableExists = "Building Variable with provided BVID already exists.";
            const string buildingVariableStringOrCSVOrBool = "Both IP and SI Engineering Units must be the same if one is a string, csv, or a boolean.";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Init(Object sender, EventArgs e)
            {
                litScripts.Text = CW.Website._masters.ReferenceSetter.Get("buildingequipmentvariables");
            }
            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindBuildingVariables();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindAllEngUnitsDropdowns();

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetBuildingVariableIntoEditForm()
            {
                //ID
                hdnEditID.Value = Convert.ToString(mBuildingVariable.BVID);
                //Hidden Building Vairable Name
                hdnBuildingVariableName.Value = mBuildingVariable.BuildingVariableName;
                //Building Variable Name
                txtEditBuildingVariableName.Text = mBuildingVariable.BuildingVariableName;
                //Hidden Building Variable Display Name
                hdnBuildingVariableDisplayName.Value = mBuildingVariable.BuildingVariableDisplayName;
                //Building Variable Display Name
                txtEditBuildingVariableDisplayName.Text = mBuildingVariable.BuildingVariableDisplayName;
                //User Editable
                chkEditUserEditable.Checked = mBuildingVariable.UserEditable;
                //IP Default Value
                txtEditIPDefaultValue.Text = mBuildingVariable.IPDefaultValueCultureFormatted;
                //SI Default Value
                txtEditSIDefaultValue.Text = mBuildingVariable.SIDefaultValueCultureFormatted;

                //Building Variable Description
                txtEditDescription.Value = String.IsNullOrEmpty(mBuildingVariable.BuildingVariableDescription) ? null : mBuildingVariable.BuildingVariableDescription;

                //IP Eng Units
                ddlEditIPEngUnits.SelectedValue = Convert.ToString(mBuildingVariable.IPEngUnitID);
                //SI Eng Units
                ddlEditSIEngUnits.SelectedValue = Convert.ToString(mBuildingVariable.SIEngUnitID);

                SetupEngUnitTextBoxes(false);
                SetEngUnitInfo(false);
                SetValidationFailureState(false, "", "");
            }

            #endregion
        #region Load and Bind Fields

            private System.Text.StringBuilder mText = null;
            private void BindAllEngUnitsDropdowns()
            {
                ddlAddIPEngUnits.DataTextField = ddlAddSIEngUnits.DataTextField = ddlEditIPEngUnits.DataTextField = ddlEditSIEngUnits.DataTextField = "EngUnits";
                ddlAddIPEngUnits.DataValueField = ddlAddSIEngUnits.DataValueField = ddlEditIPEngUnits.DataValueField = ddlEditSIEngUnits.DataValueField = "EngUnitID";
                ddlAddIPEngUnits.DataSource = ddlAddSIEngUnits.DataSource = ddlEditIPEngUnits.DataSource = ddlEditSIEngUnits.DataSource = EngUnitsHelper.EngUnits;

                ddlAddIPEngUnits.DataBind();
                ddlAddSIEngUnits.DataBind();
                ddlEditIPEngUnits.DataBind();
                ddlEditSIEngUnits.DataBind();
                mText = new System.Text.StringBuilder();
                mText.AppendLine("<script>");
                mText.AppendLine("var engUnitInfo = new Object();");
                EngUnitsHelper.ForEachEngUnit(OnEngUnit);
                mText.AppendLine("</script>");
                ClientScript.RegisterStartupScript(GetType(), "engUnitsInfo", mText.ToString());
                ddlEditIPEngUnits.Attributes["onchange"] = "OnEngUnitChange(this, '" + spEditIPInfo.ClientID + "', '" + txtEditIPDefaultValue.ClientID + "', '" + valFailureEditIPDefaultValue.ClientID + "' );";
                ddlEditSIEngUnits.Attributes["onchange"] = "OnEngUnitChange(this, '" + spEditSIInfo.ClientID + "', '" + txtEditSIDefaultValue.ClientID + "', '" + valFailureEditSIDefaultValue.ClientID + "' );";
                ddlAddIPEngUnits.Attributes["onchange"] = "OnEngUnitChange(this, '" + spAddIPInfo.ClientID + "', '" + txtAddIPDefaultValue.ClientID + "', '" + valFailureAddIPDefaultValue.ClientID + "' );";
                ddlAddSIEngUnits.Attributes["onchange"] = "OnEngUnitChange(this, '" + spAddSIInfo.ClientID + "', '" + txtAddSIDefaultValue.ClientID + "', '" + valFailureAddSIDefaultValue.ClientID + "' );";
            }

            private void OnEngUnit(EngUnit engUnit)
            {
                bool isStringOrArray = false;
                switch (engUnit.EngUnits.ToUpper())
                {
                    case BusinessConstants.EngUnit.EngineeringUnitArray:
                    case BusinessConstants.EngUnit.EngineeringUnitString:
                        {
                            isStringOrArray = true;
                            break;
                        }
                }
                mText.AppendLine("engUnitInfo['" + engUnit.EngUnitID + "'] = {engUnit:'" + EngUnitsHelper.CreateEngUnitInfo(engUnit) + "', isStringOrArray: " + isStringOrArray.ToString().ToLower() + "};");
            }
        #endregion

        #region Load Building Variable

            protected void LoadAddFormIntoBuildingVariable(BuildingVariable buildingVariable)
            {
                //Building Variable Name
                buildingVariable.BuildingVariableName = txtAddBuildingVariableName.Text;
                //Building Variable Display Name
                buildingVariable.BuildingVariableDisplayName = txtAddBuildingVariableDisplayName.Text;
                //User Editable
                buildingVariable.UserEditable = chkAddUserEditable.Checked;
                //Building Variable Description
                buildingVariable.BuildingVariableDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                //IP Eng Units
                buildingVariable.IPEngUnitID = Convert.ToInt32(ddlAddIPEngUnits.SelectedValue);
                //SI Eng Units
                buildingVariable.SIEngUnitID = Convert.ToInt32(ddlAddSIEngUnits.SelectedValue);
                //IP Default Value
                buildingVariable.IPDefaultValue = txtAddIPDefaultValue.Text;
                //SI Default Value
                buildingVariable.SIDefaultValue = txtAddSIDefaultValue.Text;

                buildingVariable.DateModified = DateTime.UtcNow;
                SetupEngUnitTextBoxes();
            }

            protected void LoadEditFormIntoBuildingVariable(BuildingVariable buildingVariable)
            {
                //ID
                buildingVariable.BVID = Convert.ToInt32(hdnEditID.Value);
                //Building Variable Name
                buildingVariable.BuildingVariableName = txtEditBuildingVariableName.Text;
                //Building Variable Display Name
                buildingVariable.BuildingVariableDisplayName = txtEditBuildingVariableDisplayName.Text;
                //User Editable
                buildingVariable.UserEditable = chkEditUserEditable.Checked;
                //Building Variable Description
                buildingVariable.BuildingVariableDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //IP Eng Units
                buildingVariable.IPEngUnitID = Convert.ToInt32(ddlEditIPEngUnits.SelectedValue);
                //SI Eng Units
                buildingVariable.SIEngUnitID = Convert.ToInt32(ddlEditSIEngUnits.SelectedValue);

                //IP Default Value
                buildingVariable.IPDefaultValue = txtEditIPDefaultValue.Text;
                //SI Default Value
                buildingVariable.SIDefaultValue = txtEditSIDefaultValue.Text;

                buildingVariable.DateModified = DateTime.UtcNow;
                SetupEngUnitTextBoxes(false);
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add building variable Button on click.
            /// </summary>
            protected void addBuildingVariableButton_Click(object sender, EventArgs e)
            {
                mBuildingVariable = new BuildingVariable();

                //load the form into the building variable
                LoadAddFormIntoBuildingVariable(mBuildingVariable);

                //check if ip or si is string, csv, or bool and make sure they both are the same
                if ((mBuildingVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitStringID || mBuildingVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitArrayID || mBuildingVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitBoolID || mBuildingVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitStringID || mBuildingVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitArrayID || mBuildingVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitBoolID) && mBuildingVariable.IPEngUnitID != mBuildingVariable.SIEngUnitID)
                {
                    LabelHelper.SetLabelMessage(lblAddError, buildingVariableStringOrCSVOrBool, lnkSetFocusAdd);
                }
                //check if building variable name already exists
                else if (DataMgr.BuildingVariableDataMapper.DoesBuildingVariableNameExist(mBuildingVariable.BuildingVariableName))
                {
                    LabelHelper.SetLabelMessage(lblAddError, addBuildingVariableNameExists, lnkSetFocusAdd);        
                }
                //check if building variable display name already exists
                else if (DataMgr.BuildingVariableDataMapper.DoesBuildingVariableDisplayNameExist(mBuildingVariable.BuildingVariableDisplayName))
                {
                    LabelHelper.SetLabelMessage(lblAddError, addBuildingVariableDisplayNameExists, lnkSetFocusAdd);
                }
                else
                {
                    try
                    {
                        //insert new building variable
                        DataMgr.BuildingVariableDataMapper.InsertBuildingVariable(mBuildingVariable);

                        LabelHelper.SetLabelMessage(lblAddError, mBuildingVariable.BuildingVariableName + addBuildingVariableSuccess, lnkSetFocusAdd);     
                    }
                    catch (CW.Data.Exceptions.ValidationException ex)
                    {
                        SetValidationFailureState(true, ex.Errors[0], ex.Errors[1]);

                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building variable." + ex.FlattenErrors);
                        LabelHelper.SetLabelMessage(lblAddError, addBuildingVariableFailedValidation, lnkSetFocusAdd);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building variable.", sqlEx);
                        LabelHelper.SetLabelMessage(lblAddError, addBuildingVariableFailed, lnkSetFocusAdd);
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building variable.", ex);
                        LabelHelper.SetLabelMessage(lblAddError, addBuildingVariableFailed, lnkSetFocusAdd);
                    }
                }
                SetEngUnitInfo();
            }

            private void SetValidationFailureState(bool bAdd, string ipMsg, string siMsg)
            {
                (bAdd ? valFailureAddIPDefaultValue : valFailureEditIPDefaultValue).InnerHtml = ipMsg;
                (bAdd ? valFailureAddIPDefaultValue : valFailureEditIPDefaultValue).Style[HtmlTextWriterStyle.Display] = string.IsNullOrEmpty(ipMsg) ? "none" : "block";
                (bAdd ? valFailureAddSIDefaultValue : valFailureEditSIDefaultValue).InnerHtml = siMsg;
                (bAdd ? valFailureAddSIDefaultValue : valFailureEditSIDefaultValue).Style[HtmlTextWriterStyle.Display] = string.IsNullOrEmpty(siMsg) ? "none" : "block";
            }
            private void SetEngUnitInfo(bool bAdd = true)
            {
                EngUnit engUnit = EngUnitsHelper.GetEngUnit(e => e.EngUnitID == mBuildingVariable.IPEngUnitID);
                (bAdd ? spAddIPInfo : spEditIPInfo).InnerHtml = EngUnitsHelper.CreateEngUnitInfo(engUnit);
                engUnit = EngUnitsHelper.GetEngUnit(e => e.EngUnitID == mBuildingVariable.SIEngUnitID);
                (bAdd ? spAddSIInfo : spEditSIInfo).InnerHtml = EngUnitsHelper.CreateEngUnitInfo(engUnit);
            }

            private void SetupEngUnitTextBoxes(bool bAdd = true)
            {
                (new[] { new { engunitid = mBuildingVariable.IPEngUnitID, textbox = (bAdd ? txtAddIPDefaultValue: txtEditIPDefaultValue) },
                        new { engunitid = mBuildingVariable.SIEngUnitID, textbox = (bAdd ? txtAddSIDefaultValue: txtEditSIDefaultValue) } 
                       }
                       ).ForEach(pair =>
                       {
                           ControlHelper.SetTextBoxSingleMultiple(pair.engunitid, pair.textbox);
                       });
            }

            /// <summary>
            /// Update building variable Button on click. Updates building variable data.
            /// </summary>
            protected void updateBuildingVariableButton_Click(object sender, EventArgs e)
            {
                SetValidationFailureState(false, "", "");
                mBuildingVariable = new BuildingVariable();

                //load the form into the building variable
                LoadEditFormIntoBuildingVariable(mBuildingVariable);

                //check if ip or si is string, csv, or bool and make sure they both are the same
                if ((mBuildingVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitStringID || mBuildingVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitArrayID || mBuildingVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitBoolID || mBuildingVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitStringID || mBuildingVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitArrayID || mBuildingVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitBoolID) && mBuildingVariable.IPEngUnitID != mBuildingVariable.SIEngUnitID)                
                {
                    LabelHelper.SetLabelMessage(lblEditError, buildingVariableStringOrCSVOrBool, lnkSetFocusEdit);
                }
                //check if trying to change building variable name, and check if it already exists.
                else if (mBuildingVariable.BuildingVariableName != hdnBuildingVariableName.Value && (DataMgr.BuildingVariableDataMapper.DoesBuildingVariableNameExist(mBuildingVariable.BuildingVariableName)))
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateBuildingVariableNameExists, lnkSetFocusEdit);
                }
                //check if trying to change building variable display name, and check if it already exists.
                else if (mBuildingVariable.BuildingVariableDisplayName != hdnBuildingVariableDisplayName.Value && (DataMgr.BuildingVariableDataMapper.DoesBuildingVariableDisplayNameExist(mBuildingVariable.BuildingVariableDisplayName)))
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateBuildingVariableDisplayNameExists, lnkSetFocusEdit);
                }
                else
                {
                    //try to update the building variables              
                    try
                    {
                        SetValidationFailureState(false, "", "");
                        DataMgr.BuildingVariableDataMapper.UpdateBuildingVariable(mBuildingVariable);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusEdit);

                        //Bind building variables again
                        BindBuildingVariables();
                    }
                    catch (CW.Data.Exceptions.ValidationException ex)
                    {
                        SetValidationFailureState(false, ex.Errors[0], ex.Errors[1]);

                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding building variable." + ex.FlattenErrors);
                        LabelHelper.SetLabelMessage(lblEditError, updateFailedBuildingVariableValidation, lnkSetFocusEdit);
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusEdit);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating building variable.", ex);
                    }
                }
                SetEngUnitInfo(false);

           }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindBuildingVariables();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindBuildingVariables();
            }

        #endregion

        #region Dropdown events

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind building variable grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind building variables to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindBuildingVariables();
                }
            }

        #endregion

        #region Grid Events

            protected void gridBuildingVariables_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridBuildingVariables_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditBuildingVariable.Visible = false;
                dtvBuildingVariable.Visible = true;
                
                int bvid = Convert.ToInt32(gridBuildingVariables.DataKeys[gridBuildingVariables.SelectedIndex].Values["BVID"]);

                //set data source
                dtvBuildingVariable.DataSource = new List<GetBuildingVariableData>() { DataMgr.BuildingVariableDataMapper.GetFullBuildingVariableByBVID(bvid) };
                //bind building variable to details view
                dtvBuildingVariable.DataBind();
            }

            protected void gridBuildingVariables_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridBuildingVariables_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvBuildingVariable.Visible = false;
                pnlEditBuildingVariable.Visible = true;

                int bvid = Convert.ToInt32(gridBuildingVariables.DataKeys[e.NewEditIndex].Values["BVID"]);

                mBuildingVariable = DataMgr.BuildingVariableDataMapper.GetBuildingVariable(bvid);

                //Set Building Variable data
                SetBuildingVariableIntoEditForm();

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridBuildingVariables_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows buildingVariableid
                int bvid = Convert.ToInt32(gridBuildingVariables.DataKeys[e.RowIndex].Value);
                int top = 10;

                try
                {
                    //Only allow deletion of a building variable if it as not been assocaited with
                    //a piece of building or an analysis.  
                  
                    //check if building variable is associated to any building
                    List<GetBuildingsData> building = DataMgr.BuildingDataMapper.GetTopBuildingsAssociatedWithBVID(bvid, top);

                    if (building != null)
                    {
                        lblErrors.Text = associatedToBuilding;

                        lblErrors.Text += " The " + (building.Count < top ? building.Count.ToString() : "first " + top.ToString()) + " associated building:<br />";

                        foreach (GetBuildingsData b in building)
                        {
                            lblErrors.Text += "<br />" + b.BuildingName + " - " + b.BuildingName;
                        }

                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    //check if building variable is associated to any analysis
                    else if (DataMgr.BuildingVariableDataMapper.IsBuildingVariableAssociatedToAnyAnalysis(bvid))
                    {
                        lblErrors.Text = associatedToAnalysis;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    //check if building variable is associated to any building class
                    else if (DataMgr.BuildingVariableDataMapper.IsBuildingVariableAssociatedToBuildingClass(bvid))
                    {
                        lblErrors.Text = associatedToBuildingClass;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //delete building variable
                        DataMgr.BuildingVariableDataMapper.DeleteBuildingVariableByBVID(bvid);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting building variable.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryBuildingVariables());
                gridBuildingVariables.PageIndex = gridBuildingVariables.PageIndex;
                gridBuildingVariables.DataSource = SortDataTable(dataTable as DataTable, true);
                gridBuildingVariables.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditBuildingVariable.Visible = false;  
            }

            protected void gridBuildingVariables_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBuildingVariables(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridBuildingVariables.DataSource = SortDataTable(dataTable, true);
                gridBuildingVariables.PageIndex = e.NewPageIndex;
                gridBuildingVariables.DataBind();
            }

            protected void gridBuildingVariables_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridBuildingVariables.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedBuildingVariables(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridBuildingVariables.DataSource = SortDataTable(dataTable, false);
                gridBuildingVariables.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetBuildingVariableData> QueryBuildingVariables()
            {
                try
                {
                    //get all building variables 
                    return DataMgr.BuildingVariableDataMapper.GetAllFullBuildingVariables();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building variables.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building variables.", ex);
                    return null;
                }
            }

            private IEnumerable<GetBuildingVariableData> QuerySearchedBuildingVariables(string searchText)
            {
                try
                {
                    //get all building variables 
                    return DataMgr.BuildingVariableDataMapper.GetAllSearchedFullBuildingVariables(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building variables.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving building variables.", ex);
                    return null;
                }
            }

            private void BindBuildingVariables()
            {
                //query building variables
                IEnumerable<GetBuildingVariableData> variables = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryBuildingVariables() : QuerySearchedBuildingVariables(txtSearch.Text);

                var count = variables.Count();

                gridBuildingVariables.DataSource = variables;

                // bind grid
                gridBuildingVariables.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedBuildingVariables(string[] searchText)
            //{
            //    //query building variables
            //    IEnumerable<GetBuildingVariableData> variables = QuerySearchedBuildingVariables(searchText);

            //    int count = variables.Count();

            //    gridBuildingVariables.DataSource = variables;

            //    // bind grid
            //    gridBuildingVariables.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(Int32 count)
            {
                var formattedName = LanguageHelper.GetFormattedName("building variable", (count == 1));
                var formattedTextNoResults = String.Format("No {0} found.", formattedName);
                var formattedTextResults = String.Format("{0} {1} found.", count, formattedName);

                if (count == 0)
                {
                    LabelHelper.SetLabelMessage(lblResults, formattedTextNoResults, null);
                    return;
                }

                LabelHelper.SetLabelMessage(lblResults, formattedTextResults, null);
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

        #endregion
    }
}