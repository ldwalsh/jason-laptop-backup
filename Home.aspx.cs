﻿using CW.Common.Constants;
using CW.Utility;
using CW.Website._framework;
using System;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class Home : SitePage
    {
        #region properties

            private Boolean IsProvider { get; set; }

        #endregion

        #region page events

            protected void Page_Load(Object sender, EventArgs e)
            {
                if (siteUser.ProviderClients != null && siteUser.IsLoggedInUnderProviderClient)
                    IsProvider = true;

                if (!Page.IsPostBack)
                {
                    SetUserThemes();

                    pnlLoggedIn.Visible = !siteUser.IsAnonymous;
                    pnlLoggedOut.Visible = siteUser.IsAnonymous;

                    if (!siteUser.IsAnonymous)
                    {
                        if (!String.IsNullOrWhiteSpace(Request.QueryString[DataConstants.ReferrerQSKey]) && !Request.QueryString[DataConstants.ReferrerQSKey].ToUpper().Contains("HOME.ASPX"))
                            Response.Redirect("~/" + Request.QueryString[DataConstants.ReferrerQSKey]);

                        DefaultWidgets.IsClientView = true;
                        lnkProviderView.Visible = IsProvider;
                        lnkClientView.Visible = IsProvider;
                    }
                }
            }

        #endregion

        #region methods

            private void SetUserThemes()
            {
                if (siteUser.IsAnonymous)
                {
                    var globalSetting = CW.Business.DataManager.Get(Session.SessionID).GlobalDataMapper.GetGlobalSettings();

                    h1HomePageHeaderLoggedOut.InnerText = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.WelcomePageHeader);
                    litHomePageBodyLoggedOut.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.WelcomePageBody);

                    return;
                }

                // client content
                var client = DataMgr.ClientDataMapper.GetClient(siteUser.CID);
                var clientSettings = DataMgr.ClientDataMapper.GetClientSettingsByClientID(client.CID);

                if (!String.IsNullOrEmpty(clientSettings.HomePageHeader) || !String.IsNullOrEmpty(clientSettings.HomePageBody))
                {
                    h1HomePageHeader.InnerText = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, TokenVariableHelper.FormatSpecialClientTokens(client, clientSettings.HomePageHeader));
                    litHomePageBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, clientSettings.HomePageBody);

                    return;
                }

                // if not client content, then global content
                var globalSetting2 = DataMgr.GlobalDataMapper.GetGlobalSettings();

                if (!String.IsNullOrEmpty(globalSetting2.HomePageHeader) || !String.IsNullOrEmpty(globalSetting2.HomePageBody))
                {
                    h1HomePageHeader.InnerText = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting2.HomePageHeader);
                    litHomePageBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting2.HomePageBody);

                    return;
                }

                // else no content
                pnlHomePageContent.Visible = false;               
            }

            protected void SetContentVisibility(Object sender, CommandEventArgs e)
            {
                if (!siteUser.IsAnonymous)
                {
                    var isProviderView = (e.CommandArgument.ToString() == "viewProvider");

                    ProviderWidgets.IsProviderView = (isProviderView && IsProvider);
                    DefaultWidgets.IsClientView = !(isProviderView && IsProvider);

                    pnlHomePageContent.Visible = !(isProviderView && IsProvider);
                }
            }

        #endregion
    }
}