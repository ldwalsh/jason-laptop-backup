﻿<%@ Page Language="C#" EnableEventValidation="false" EnableSessionState="true" MasterPageFile="~/_masters/PerformanceDashboard.master" AutoEventWireup="false" CodeBehind="PerformanceDashboard.aspx.cs" Inherits="CW.Website.PerformanceDashboard" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="extensions" Namespace="CW.Website._extensions" Assembly="CW.Website" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

   <script type="text/javascript">

       function BuildingDataValidateDateRange(source, args) {

           var buildingDataStartDate = document.getElementById("ctl00_plcCopy_txtBuildingDataStartDate").value;
           var buildingDataEndDate = document.getElementById("ctl00_plcCopy_txtBuildingDataEndDate").value;

           if (!buildingDataStartDate || !buildingDataEndDate) {
                args.IsValid = true;
                return true;
           }

           //old
           //if (((new Date(buildingDataEndDate) - new Date(buildingDataStartDate)) / (1000*60*60*24)) < 8) {

           //globalize
           Globalize.culture( "<%= siteUser.CultureName%>" );
           if (((Globalize.parseDate(buildingDataEndDate) - Globalize.parseDate(buildingDataStartDate)) / (1000 * 60 * 60 * 24)) < 8) {
               args.IsValid = true;
               return true;
           }
           args.IsValid = false;
           return false;
       }
       function PortfolioDataValidateDateRange(source, args) {

           var portfolioDataStartDate = document.getElementById("ctl00_plcCopy_txtPortfolioDataStartDate").value;
           var portfolioDataEndDate = document.getElementById("ctl00_plcCopy_txtPortfolioDataEndDate").value;

           if (!portfolioDataStartDate || !portfolioDataEndDate)
           {
               args.IsValid = true;
               return true;
           }

           //old
           //if (((new Date(portfolioDataEndDate) - new Date(portfolioDataStartDate)) / (1000*60*60*24)) < 8) {

           //globalize
           Globalize.culture("<%= siteUser.CultureName%>");
           if (((Globalize.parseDate(portfolioDataEndDate) - Globalize.parseDate(portfolioDataStartDate)) / (1000 * 60 * 60 * 24)) < 8) {
               args.IsValid = true;
               return true;
           }
           args.IsValid = false;
           return false;
       }

    </script>

		<div id="performanceDashboard" class="performanceDashboard">					   
			<div id="dashboardHeader" class="dashboardHeader"> 
				<div class="performanceModuleIcon">
                </div>
                <!--cw dashboard logo-->
				<div class="dashboardHeaderLogo">
					Performance<br />Dashboard
				</div>
				<div class="headerSpacer"></div>
				<div class="headerClientInfo"> 
					<div class="headerClientImage">     
						<asp:Image ID="imgHeaderClientImage" CssClass="imgHeader" runat="server" />
					</div>          
					<div class="headerClientName">
						<label id="lblHeaderClientName" runat="server"></label>
					</div>
				</div>
			</div>
			<div id="dashboardBody" class="dashboardBody">
					<div class="dashboardBodyLeftColumn">
						<ul class="dashboardNav">
							<li><asp:LinkButton CssClass="lnkBtnDashboardActive" ID="btnHome" runat="server" Text="Home" OnClientClick="" OnClick="homeButton_Click"></asp:LinkButton></li>
							<li><asp:LinkButton CssClass="lnkBtnDashboard" ID="btnPortfolioOverview" runat="server" Text="Portfolio Overview" OnClick="portfolioOverviewButton_Click"></asp:LinkButton></li>
                            <li><asp:LinkButton CssClass="lnkBtnDashboard" ID="btnPortfolioData" runat="server" Text="Porfolio Metrics" OnClick="portfolioDataButton_Click"></asp:LinkButton></li>
                            <li><asp:LinkButton CssClass="lnkBtnDashboard" ID="btnBuildingOverview" runat="server" Text="Building Overview" OnClick="buildingOverviewButton_Click"></asp:LinkButton></li>
                            <li><asp:LinkButton CssClass="lnkBtnDashboard" ID="btnBuildingData" runat="server" Text="Building Metrics" OnClick="buildingDataButton_Click"></asp:LinkButton></li>
						</ul>
						<hr />
						<div class="dashboardSeondarySelections">
							<asp:MultiView ID="mvSecondary" ActiveViewIndex="0" runat="server">
								<asp:View ID="vSecondaryHome" runat="server">
								</asp:View> 
                                <asp:View ID="vSecondaryPortfolioOverview" runat="server">
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Metric:</label>
                                        <asp:DropDownList CssClass="dropdown" ID="ddlPortfolioOverviewMetric" runat="server">
                                            <asp:ListItem Value="1">Electric</asp:ListItem> 
                                            <asp:ListItem Value="2">Electric/sqft </asp:ListItem> 
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnGeneratePortfolioOverview" runat="server" Text="Generate"  OnClick="generatePortfolioOverviewButton_Click" ValidationGroup="PortfolioOverview"></asp:LinkButton>
								</asp:View> 
								<asp:View ID="vSecondaryPortfolioData" runat="server">
                                <asp:Panel ID="pnlGeneratePortfolioData" runat="server" DefaultButton="btnGeneratePortfolioData">
									<div class="divSecondaryForm">
										<label class="labelBold">*Start Date:</label>  
										<asp:TextBox ID="txtPortfolioDataStartDate" CssClass="textboxNarrow" runat="server"></asp:TextBox>  
                                        <asp:ImageButton ID="btnPortfolioDataStartCal" CssClass="imgCal" ImageUrl="_assets/images/cal.png" runat="server" CausesValidation="false" />   
                                        <ajaxToolkit:CalendarExtender ID="portfolioDataStartCalExtender" runat="server"
                                            TargetControlID="txtPortfolioDataStartDate"        
                                            Format="d"
                                            PopupPosition="Right"  
                                            PopupButtonID="btnPortfolioDataStartCal"   
                                            OnClientShown="onCalendarShown"                         
                                            />                         
                                        <ajaxToolkit:MaskedEditExtender ID="portfolioDataStartMaskedEditExtender" runat="server"
                                                TargetControlID="txtPortfolioDataStartDate" 
                                                Mask="99/99/9999"
                                                AutoComplete="true"                                
                                                MaskType="Date" 
                                                InputDirection="LeftToRight"                                 
                                                />     
                                        <ajaxToolkit:MaskedEditValidator ID="portfolioDataStartMaskedEditValidator" runat="server"                                 
                                                ControlExtender="portfolioDataStartMaskedEditExtender" 
                                                ControlToValidate="txtPortfolioDataStartDate" 
                                                InvalidValueMessage="Invalid Date"                                                    
                                                IsValidEmpty="true" 
                                                ValidationGroup="PortfolioData" 
                                                SetFocusOnError="true"
                                                >
                                                </ajaxToolkit:MaskedEditValidator>  
                                        <asp:RequiredFieldValidator ID="portfolioDataStartRequiredValidator" runat="server"
                                            ErrorMessage="Start Date is a required field." 
                                            ControlToValidate="txtPortfolioDataStartDate"  
                                            Display="None" 
                                            ValidationGroup="PortfolioData">
                                            </asp:RequiredFieldValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="portfolioStartRequiredValidatorExtender" runat="server"
                                            BehaviorID="portfolioDataStartRequiredValidatorExtender" 
                                            TargetControlID="portfolioDataStartRequiredValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                          
                                    </div>
                                    <div class="divSecondaryForm">
										<label class="labelBold">*End Date:</label>  
										<asp:TextBox ID="txtPortfolioDataEndDate" CssClass="textboxNarrow" runat="server"></asp:TextBox>  
                                        <asp:ImageButton ID="btnPortfolioDataEndCal" CssClass="imgCal" ImageUrl="_assets/images/cal.png" runat="server" CausesValidation="false" />   
                                        <ajaxToolkit:CalendarExtender ID="portfolioDataEndCalExtender" runat="server"
                                            TargetControlID="txtPortfolioDataEndDate"        
                                            Format="d"
                                            PopupPosition="Right"  
                                            PopupButtonID="btnPortfolioDataEndCal"   
                                            OnClientShown="onCalendarShown"                         
                                            />                         
                                        <ajaxToolkit:MaskedEditExtender ID="portfolioDataEndMaskedEditExtender" runat="server"
                                                TargetControlID="txtPortfolioDataEndDate" 
                                                Mask="99/99/9999"
                                                AutoComplete="true"                                
                                                MaskType="Date" 
                                                InputDirection="LeftToRight"                                 
                                                />     
                                        <ajaxToolkit:MaskedEditValidator ID="portfolioDataEndMaskedEditValidator" runat="server"                                 
                                                ControlExtender="portfolioDataEndMaskedEditExtender" 
                                                ControlToValidate="txtPortfolioDataEndDate" 
                                                InvalidValueMessage="Invalid Date"                                                    
                                                IsValidEmpty="true" 
                                                ValidationGroup="PortfolioData" 
                                                SetFocusOnError="true"
                                                >
                                                </ajaxToolkit:MaskedEditValidator>  
                                        <asp:RequiredFieldValidator ID="portfolioDataEndRequiredValidator" runat="server"
                                            ErrorMessage="End Date is a required field." 
                                            ControlToValidate="txtPortfolioDataEndDate"  
                                            Display="None"                                   
                                            ValidationGroup="PortfolioData">
                                            </asp:RequiredFieldValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="portfolioDataEndRequiredValidatorExtender" runat="server"
                                            BehaviorID="portfolioDataEndRequiredValidatorExtender" 
                                            TargetControlID="portfolioDataEndRequiredValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                               
                                        <asp:CompareValidator ID="portfolioDataDateCompareValidator" Display="None" runat="server"
                                            ErrorMessage="Invalid Range"
                                            ControlToValidate="txtPortfolioDataEndDate"
                                            ControlToCompare="txtPortfolioDataStartDate"
                                            Type="Date"
                                            Operator="GreaterThanEqual"
                                            ValidationGroup="PortfolioData"
                                            >
                                            </asp:CompareValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="portfolioDataDateCompareValidatorCalloutExtender" runat="server" 
                                            BehaviorID="portfolioDataDateCompareValidatorCalloutExtender" 
                                            TargetControlID="portfolioDataDateCompareValidator" 
                                            HighlightCssClass="validatorCalloutHighlight" 
                                            Width="175" />                                                                                                                                                                                                   
                                        <asp:CustomValidator ID="portfolioDataDateRangeCustomValidator" runat="server" 
                                            ClientValidationFunction="PortfolioDataValidateDateRange"          
                                            ControlToValidate="txtPortfolioDataEndDate"                                  
                                            Display="None"
                                            ErrorMessage="Max date range is 7 days."
                                            ValidationGroup="PortfolioData" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="portfolioDataDateRangeCustomValidatorCalloutExtender" runat="server" 
                                            BehaviorID="portfolioDataDateRangeCustomValidatorCalloutExtender" 
                                            TargetControlID="portfolioDataDateRangeCustomValidator" 
                                            HighlightCssClass="validatorCalloutHighlight" 
                                            Width="175" />
									</div>
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Buildings:</label>
                                        <extensions:ListBoxExtension ID="lbPortfolioDataBuildings" runat="server" SelectionMode="Multiple" CssClass="listboxShortest" />                                        
                                    </div>
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Metric:</label>
                                        <asp:DropDownList CssClass="dropdown" ID="ddlPortfolioDataMetric" runat="server">
                                            <asp:ListItem Value="1">Electric</asp:ListItem> 
                                            <asp:ListItem Value="2">Electric/sqft </asp:ListItem> 
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnGeneratePortfolioData" runat="server" Text="Generate"  OnClick="generatePortfolioDataButton_Click" ValidationGroup="PortfolioData"></asp:LinkButton>
                                </asp:Panel>
								</asp:View>     
                                <asp:View ID="vSecondaryBuildingOverview" runat="server">
                                <asp:Panel ID="pnlGenerateBuildingOverview" runat="server" DefaultButton="btnGenerateBuildingOverview">
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Buildings:</label>
                                        <asp:DropDownList CssClass="dropdown" ID="ddlBuildingOverviewBuilding" AppendDataBoundItems="true" runat="server" >
                                            <asp:ListItem Value="-1">Select one...</asp:ListItem> 
                                        </asp:DropDownList> 
                                        <asp:RequiredFieldValidator ID="buildingOverviewBuildingRequiredValidator" runat="server"
                                            ErrorMessage="Building is a required field." 
                                            ControlToValidate="ddlBuildingOverviewBuilding"  
                                            SetFocusOnError="true" 
                                            Display="None" 
                                            InitialValue="-1"
                                            ValidationGroup="BuildingOverview">
                                            </asp:RequiredFieldValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingOverviewBuildingRequiredValidatorExtender" runat="server"
                                            BehaviorID="buildingOverviewBuildingRequiredValidatorExtender" 
                                            TargetControlID="buildingOverviewBuildingRequiredValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                        
                                    </div>
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Metric:</label>
                                        <asp:DropDownList CssClass="dropdown" ID="ddlBuildingOverviewMetric" runat="server">
                                            <asp:ListItem Value="1">Electric</asp:ListItem> 
                                            <asp:ListItem Value="2">Electric/sqft </asp:ListItem> 
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnGenerateBuildingOverview" runat="server" Text="Generate"  OnClick="generateBuildingOverviewButton_Click" ValidationGroup="BuildingOverview"></asp:LinkButton>
                                </asp:Panel>
								</asp:View> 
                                <asp:View ID="vSecondaryBuildingData" runat="server">
                                <asp:Panel ID="pnlGenerateBuildingData" runat="server" DefaultButton="btnGenerateBuildingData">
									<div class="divSecondaryForm">
										<label class="labelBold">*Start Date:</label>  
										<asp:TextBox ID="txtBuildingDataStartDate" CssClass="textboxNarrow" runat="server"></asp:TextBox>  
                                        <asp:ImageButton ID="btnBuildingDataStartCal" CssClass="imgCal" ImageUrl="_assets/images/cal.png" runat="server" CausesValidation="false" />   
                                        <ajaxToolkit:CalendarExtender ID="buildingDataStartCalExtender" runat="server"
                                            TargetControlID="txtBuildingDataStartDate"        
                                            Format="d"
                                            PopupPosition="Right"  
                                            PopupButtonID="btnBuildingDataStartCal"   
                                            OnClientShown="onCalendarShown"                         
                                            />                         
                                        <ajaxToolkit:MaskedEditExtender ID="buildingDataStartMaskedEditExtender" runat="server"
                                                TargetControlID="txtBuildingDataStartDate" 
                                                Mask="99/99/9999"
                                                AutoComplete="true"                                
                                                MaskType="Date" 
                                                InputDirection="LeftToRight"                                 
                                                />     
                                        <ajaxToolkit:MaskedEditValidator ID="buildingDataStartMaskedEditValidator" runat="server"                                 
                                                ControlExtender="buildingDataStartMaskedEditExtender" 
                                                ControlToValidate="txtBuildingDataStartDate" 
                                                InvalidValueMessage="Invalid Date"                                                    
                                                IsValidEmpty="true" 
                                                ValidationGroup="BuildingData" 
                                                SetFocusOnError="true"
                                                >
                                                </ajaxToolkit:MaskedEditValidator>  
                                        <asp:RequiredFieldValidator ID="buildingDataStartRequiredValidator" runat="server"
                                            ErrorMessage="Start Date is a required field." 
                                            ControlToValidate="txtBuildingDataStartDate"  
                                            Display="None"                                     
                                            ValidationGroup="BuildingData">
                                            </asp:RequiredFieldValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingStartRequiredValidatorExtender" runat="server"
                                            BehaviorID="buildingDataStartRequiredValidatorExtender" 
                                            TargetControlID="buildingDataStartRequiredValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                               
                                    </div>
                                    <div class="divSecondaryForm">
										<label class="labelBold">*End Date:</label>  
										<asp:TextBox ID="txtBuildingDataEndDate" CssClass="textboxNarrow" runat="server"></asp:TextBox>  
                                        <asp:ImageButton ID="btnBuildingDataEndCal" CssClass="imgCal" ImageUrl="_assets/images/cal.png" runat="server" CausesValidation="false" />   
                                        <ajaxToolkit:CalendarExtender ID="buildingoDataEndCalExtender" runat="server"
                                            TargetControlID="txtBuildingDataEndDate"        
                                            Format="d"
                                            PopupPosition="Right"  
                                            PopupButtonID="btnBuildingDataEndCal"   
                                            OnClientShown="onCalendarShown"                         
                                            />                         
                                        <ajaxToolkit:MaskedEditExtender ID="buildingDataEndMaskedEditExtender" runat="server"
                                                TargetControlID="txtBuildingDataEndDate" 
                                                Mask="99/99/9999"
                                                AutoComplete="true"                                
                                                MaskType="Date" 
                                                InputDirection="LeftToRight"                                 
                                                />     
                                        <ajaxToolkit:MaskedEditValidator ID="buildingDataEndMaskedEditValidator" runat="server"                                 
                                                ControlExtender="buildingDataEndMaskedEditExtender" 
                                                ControlToValidate="txtBuildingDataEndDate" 
                                                InvalidValueMessage="Invalid Date"                                                    
                                                IsValidEmpty="true" 
                                                ValidationGroup="BuildingData" 
                                                SetFocusOnError="true"
                                                >
                                                </ajaxToolkit:MaskedEditValidator>  
                                        <asp:RequiredFieldValidator ID="buildingDataEndRequiredValidator" runat="server"
                                            ErrorMessage="End Date is a required field." 
                                            ControlToValidate="txtBuildingDataEndDate"                                              
                                            Display="None"                                             
                                            ValidationGroup="BuildingData">
                                            </asp:RequiredFieldValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingDataEndRequiredValidatorExtender" runat="server"
                                            BehaviorID="buildingDataEndRequiredValidatorExtender" 
                                            TargetControlID="buildingDataEndRequiredValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                               
                                        <asp:CompareValidator ID="buildingDataDateCompareValidator" Display="None" runat="server"
                                            ErrorMessage="Invalid Range"
                                            ControlToValidate="txtBuildingDataEndDate"
                                            ControlToCompare="txtBuildingDataStartDate"
                                            Type="Date"
                                            Operator="GreaterThanEqual"
                                            ValidationGroup="BuildingData"
                                            >
                                            </asp:CompareValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingDataDateCompareValidatorCalloutExtender" runat="server" 
                                            BehaviorID="buildingDataDateCompareValidatorCalloutExtender" 
                                            TargetControlID="buildingDataDateCompareValidator" 
                                            HighlightCssClass="validatorCalloutHighlight" 
                                            Width="175" />                                                                                                                                                                                                   
                                        <asp:CustomValidator ID="buildingDataDateRangeCustomValidator" runat="server" 
                                            ClientValidationFunction="BuildingDataValidateDateRange"                                                                             
                                            ControlToValidate="txtBuildingDataEndDate"
                                            Display="None"
                                            ErrorMessage="Max date range is 7 days."
                                            ValidationGroup="BuildingData" />        
                                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingDataDateRangeCustomValidatorCalloutExtender" runat="server" 
                                            BehaviorID="buildingDataDateRangeCustomValidatorCalloutExtender" 
                                            TargetControlID="buildingDataDateRangeCustomValidator" 
                                            HighlightCssClass="validatorCalloutHighlight" 
                                            Width="175" />                                                                                
									</div>
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Buildings:</label>
                                        <asp:DropDownList CssClass="dropdown" ID="ddlBuildingDataBuilding" AppendDataBoundItems="true" runat="server" >
                                            <asp:ListItem Value="-1">Select one...</asp:ListItem> 
                                        </asp:DropDownList> 
                                        <asp:RequiredFieldValidator ID="buildingDataBuildingRequiredValidator" runat="server"
                                            ErrorMessage="Building is a required field." 
                                            ControlToValidate="ddlBuildingDataBuilding"  
                                            SetFocusOnError="true" 
                                            Display="None" 
                                            InitialValue="-1"
                                            ValidationGroup="BuildingData">
                                            </asp:RequiredFieldValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender ID="buildingDataBuildingRequiredValidatorExtender" runat="server"
                                            BehaviorID="buildingDataBuildingRequiredValidatorExtender" 
                                            TargetControlID="buildingDataBuildingRequiredValidator"
                                            HighlightCssClass="validatorCalloutHighlight"
                                            Width="175">
                                            </ajaxToolkit:ValidatorCalloutExtender>                                        
                                    </div>
                                    <div class="divSecondaryForm">
                                        <label class="labelBold">*Metric:</label>
                                        <asp:DropDownList CssClass="dropdown" ID="ddlBuildingDataMetric" runat="server">
                                            <asp:ListItem Value="1">Electric</asp:ListItem> 
                                            <asp:ListItem Value="2">Electric/sqft </asp:ListItem> 
                                        </asp:DropDownList> 
                                    </div>
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnGenerateBuildingData" runat="server" Text="Generate"  OnClick="generateBuildingDataButton_Click" ValidationGroup="BuildingData"></asp:LinkButton>
                                </asp:Panel>
								</asp:View>                       
							</asp:MultiView>
						</div>
					</div>

				<div class="dashboardBodyRightColumn"> 
					<asp:MultiView ID="mvMain" ActiveViewIndex="0" runat="server">
						<asp:View ID="vMainHome" runat="server">
							<ajaxToolkit:RoundedCornersExtender ID="rceHome" runat="server" TargetControlID="divHomeInner" Radius="6" Corners="All" />
							<div id="divHomeInner" class="dashboardHomeBodyInner" runat="server">
                            <h1>Performance Dashboard</h1>                                       
							<div class="richText">
								<asp:Literal ID="litDashboardBody" runat="server"></asp:Literal>                                
							</div>  
							<div>
							</div>
                            </div>                             
					    </asp:View>
                        <asp:View ID="vMainPortfolioOverview" runat="server">
                            <ajaxToolkit:RoundedCornersExtender ID="rcePortfolioOverview" runat="server" TargetControlID="divPortfolioOverviewInner" Radius="6" Corners="All"></ajaxToolkit:RoundedCornersExtender>
							<div id="divPortfolioOverviewInner" class="dashboardBodyInner" runat="server">
                            
                            <div id="divPortfolioOverviewButtons" runat='server' visible="false">
                                <div class="divChartTypeButtons">		                                    
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPortfolioYesterday" runat="server" Text="Yesterday" OnClick="portfolioYesterdayButton_Click" />
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPortfolioWeek" runat="server" Text="Week" OnClick="portfolioWeekButton_Click" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPortfolioMonth" runat="server" Text="Month" OnClick="portfolioMonthButton_Click" />									
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnPortfolioYear" runat="server" Text="Year" OnClick="portfolioYearButton_Click" />
								</div>
                            </div>
                               
                            <div id="divPortfolioOverviewDefaultMessage" class="dashboardMessage" runat="server">
							    <asp:Label ID="lblPortfolioOverviewDefaultMessage" runat="server" Text=""></asp:Label>
							</div>

                            <div id="divPortfolioOverviewYearResults" runat="server" visible="false">
							    <asp:Label ID="lblPortfolioOverviewYearResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
														
                                <asp:Chart ID="portfolioOverviewYearChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="portfolioOverviewYearTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="portfolioOverviewYearChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>                                            
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Buildings" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>
                       
                            <div id="divPortfolioOverviewMonthResults" runat="server" visible="false">
							    <asp:Label ID="lblPortfolioOverviewMonthResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
                                
                                <asp:Chart ID="portfolioOverviewMonthChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="portfolioOverviewMonthTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="portfolioOverviewMonthChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Buildings" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                            <div id="divPortfolioOverviewWeekResults" runat="server" visible="false">
							    <asp:Label ID="lblPortfolioOverviewWeekResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
								
                                <asp:Chart ID="portfolioOverviewWeekChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="portfolioOverviewWeekTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="portfolioOverviewWeekChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Buildings" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                            <div id="divPortfolioOverviewYesterdayResults" runat="server" visible="false">
							    <asp:Label ID="lblPortfolioOverviewYesterdayResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
								
                                <asp:Chart ID="portfolioOverviewYesterdayChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="portfolioOverviewYesterdayTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="portfolioOverviewYesterdayChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Buildings" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                            
                            <div id="portfolioOverviewBlocks" runat="server" class="dashboardBodyInnerRight" visible="false">                 
                                <!--<div class="divBodyInnerRightTop">
                                </div>-->
                                <div class="divBodyBlock">
                                    <div class="divBodyBlockHeader">
                                        <span>Statistics</span>
                                    </div>
                                    <div class="divBodyBlockBody">
                                        <span>Total Building(s) Meter Sum:</span><br />
                                        <span id="spanPortfolioOverviewTotalYear" class="blockValueGreen" runat="server" visible="false"></span>
                                        <span id="spanPortfolioOverviewTotalMonth" class="blockValueGreen" runat="server" visible="false"></span>
                                        <span id="spanPortfolioOverviewTotalWeek" class="blockValueGreen" runat="server" visible="false"></span>
                                        <span id="spanPortfolioOverviewTotalYesterday" class="blockValueGreen" runat="server" visible="false"></span>
                                    </div>                                    
                                </div>
                            </div>

							</div>

                             <!--Chart is required in comments, microsoft charting bug.-->
                            <!--<asp:chart id="Chart8" runat="server" Height="1px" Width="1px"></asp:chart>-->
                        </asp:View>
					    <asp:View ID="vMainPortfolioData" runat="server">
							<ajaxToolkit:RoundedCornersExtender ID="rcePortfolioData" runat="server" TargetControlID="divPortfolioDataInner" Radius="6" Corners="All"></ajaxToolkit:RoundedCornersExtender>
							<div id="divPortfolioDataInner" class="dashboardBodyInner" runat="server">
														
							<div id="divPortfolioDataResults" class="dashboardMessage" runat="server">
							    <asp:Label ID="lblPortfolioDataResults"  runat="server" Text=""></asp:Label>
							</div>
							
                            <asp:Chart ID="portfolioDataChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="460px" Width="638px" Visible="false">
                                <Titles>
                                    <asp:Title Name="portfolioDataTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                </Titles>
                                <Legends>
                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                </Legends>                                
                                <Series></Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="portfolioDataChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                        <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                        <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                            <MajorGrid LineColor="#CCCCCC" />
                                            <MajorTickMark LineColor="#CCCCCC" />
                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                        </AxisY>
                                        <AxisX Title="Buildings" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                            <MajorGrid LineColor="#CCCCCC" />
                                            <MajorTickMark LineColor="#CCCCCC" />
                                        </AxisX>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>

                            <div id="portfolioDataBlocks" runat="server" class="dashboardBodyInnerRight" visible="false">                 
                                <!--<div class="divBodyInnerRightTop">
                                </div>-->
                                <div class="divBodyBlock">
                                    <div class="divBodyBlockHeader">
                                        <span>Statistics</span>
                                    </div>
                                    <div class="divBodyBlockBody">
                                        <span>Total Building(s) Meter Sum:</span><br />
                                        <span id="spanPortfolioDataTotal" class="blockValueGreen" runat="server"></span>
                                    </div>                                    
                                </div>
                            </div>

							</div>

                            <!--Chart is required in comments, microsoft charting bug.-->
                            <!--<asp:chart id="Chart2" runat="server" Height="1px" Width="1px"></asp:chart>-->

					    </asp:View>    
                        <asp:View ID="vMainBuildingOverview" runat="server">
                            <ajaxToolkit:RoundedCornersExtender ID="rceBuildingOverview" runat="server" TargetControlID="divBuildingOverviewInner" Radius="6" Corners="All"></ajaxToolkit:RoundedCornersExtender>
							<div id="divBuildingOverviewInner" class="dashboardBodyInner" runat="server">

                            	<div id="divBuildingOverviewButtons" runat='server' visible="false">
                                <div class="divChartTypeButtons">								
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnBuildingTotal" runat="server" Text="Total" OnClick="buildingTotalsButton_Click" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnBuildingDrillDown" runat="server" Text="Drill Down" OnClick="buildingDrillDownButton_Click" />									                                    
								</div>
                                <div class="divButtonsSpacer">|</div>
                                <div class="divChartTypeButtons">									
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnBuildingYesterday" runat="server" Text="Yesterday" OnClick="buildingYesterdayButton_Click" />
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnBuildingWeek" runat="server" Text="Week" OnClick="buildingWeekButton_Click" />
									<asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnBuildingMonth" runat="server" Text="Month" OnClick="buildingMonthButton_Click" />									
                                    <asp:LinkButton CssClass="lnkBtnSmallDashboard" ID="btnBuildingYear" runat="server" Text="Year" OnClick="buildingYearButton_Click" />
								</div>
                                </div>

                                <div id="divBuildingOverviewDefaultMessage" class="dashboardMessage" runat="server">
							        <asp:Label ID="lblBuildingOverviewDefaultMessage"  runat="server" Text=""></asp:Label>
							    </div>
							

                                <div id="divBuildingOverviewTotalYearResults" runat="server" visible="false">
							    <asp:Label ID="lblBuildingOverviewTotalYearResults" CssClass="dashboardMessage defaultMessage" runat="server" Text="" Visible="false"></asp:Label>
														
                                <asp:Chart ID="buildingOverviewTotalYearChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="buildingOverviewTotalYearTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="buildingOverviewTotalYearChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Meter(s)" TitleForeColor="#333333" Interval="1" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" IsStaggered="true" TruncatedLabels="false" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                            <div id="divBuildingOverviewDrillDownYearResults" runat="server" visible="false">
							    <asp:Label ID="lblBuildingOverviewDrillDownYearResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
														
                                <asp:Chart ID="buildingOverviewDrillDownYearChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="buildingOverviewDrillDownYearTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="buildingOverviewDrillDownYearChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>                                            
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Month" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                            <div id="divBuildingOverviewTotalMonthResults" runat="server" visible="false">
							    <asp:Label ID="lblBuildingOverviewTotalMonthResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
                                
                                <asp:Chart ID="buildingOverviewTotalMonthChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="buildingOverviewTotalMonthTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="buildingOverviewTotalMonthChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Meter(s)" TitleForeColor="#333333" Interval="1" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" IsStaggered="true" TruncatedLabels="false" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                             <div id="divBuildingOverviewDrillDownMonthResults" runat="server" visible="false">
							    <asp:Label ID="lblBuildingOverviewDrillDownMonthResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
                                
                                <asp:Chart ID="buildingOverviewDrillDownMonthChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="buildingOverviewDrillDownMonthTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="buildingOverviewDrillDownMonthChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>                                            
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Day" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                            <div id="divBuildingOverviewTotalWeekResults" runat="server" visible="false">
							    <asp:Label ID="lblBuildingOverviewTotalWeekResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
								
                                <asp:Chart ID="buildingOverviewTotalWeekChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="buildingOverviewTotalWeekTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="buildingOverviewTotalWeekChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Meter(s)" TitleForeColor="#333333" Interval="1" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" IsStaggered="true" TruncatedLabels="false" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                            <div id="divBuildingOverviewDrillDownWeekResults" runat="server" visible="false">
							    <asp:Label ID="lblBuildingOverviewDrillDownWeekResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
								
                                <asp:Chart ID="buildingOverviewDrillDownWeekChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="buildingOverviewDrillDownWeekTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="buildingOverviewDrillDownWeekChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>                                            
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Day" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>


                            <div id="divBuildingOverviewTotalYesterdayResults" runat="server" visible="false">
							    <asp:Label ID="lblBuildingOverviewTotalYesterdayResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
								
                                <asp:Chart ID="buildingOverviewTotalYesterdayChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="buildingOverviewTotalYesterdayTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="buildingOverviewTotalYesterdayChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="Meter(s)" TitleForeColor="#333333" Interval="1" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" IsStaggered="true" TruncatedLabels="false" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

                            <div id="divBuildingOverviewDrillDownYesterdayResults" runat="server" visible="false">
							    <asp:Label ID="lblBuildingOverviewDrillDownYesterdayResults" CssClass="dashboardMessage" runat="server" Text="" Visible="false"></asp:Label>
								
                                <asp:Chart ID="buildingOverviewDrillDownYesterdayChart" CssClass="dashboardChart" runat="server" ImageType="Png" EnableViewState="true"  Height="440px" Width="638px" Visible="false">
                                    <Titles>
                                        <asp:Title Name="buildingOverviewDrillDownYesterdayTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                    </Legends>                                
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="buildingOverviewDrillDownYesterdayChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                            <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>
                                            <AxisY Title="Electric/sqf" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                                <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                            </AxisY>
                                            <AxisX Title="DateTime" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                                <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                                <MajorGrid LineColor="#CCCCCC" />
                                                <MajorTickMark LineColor="#CCCCCC" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            </div>

		                    <div id="buildingOverviewBlocks" runat="server" class="dashboardBodyInnerRight" visible="false">                 
                                <!--<div class="divBodyInnerRightTop">
                                </div>-->
                                <div class="divBodyBlock">
                                    <div class="divBodyBlockHeader">
                                        <span>Statistics</span>
                                    </div>
                                    <div class="divBodyBlockBody">
                                        <span>Total Meter Sum:</span><br />
                                        <span id="spanBuildingOverviewTotalYear" class="blockValueGreen" runat="server" visible="false"></span>
                                        <span id="spanBuildingOverviewTotalMonth" class="blockValueGreen" runat="server" visible="false"></span>
                                        <span id="spanBuildingOverviewTotalWeek" class="blockValueGreen" runat="server" visible="false"></span>
                                        <span id="spanBuildingOverviewTotalYesterday" class="blockValueGreen" runat="server" visible="false"></span>
                                    </div>                                    
                                </div>
                            </div>

							</div>

                                                        
                            <!--Chart is required in comments, microsoft charting bug.-->
                            <!--<asp:chart id="Chart12" runat="server" Height="1px" Width="1px"></asp:chart>-->

                        </asp:View>
                        <asp:View ID="vMainBuildingData" runat="server">
                            <ajaxToolkit:RoundedCornersExtender ID="rceBuildingData" runat="server" TargetControlID="divBuildingDataInner" Radius="6" Corners="All"></ajaxToolkit:RoundedCornersExtender>
                            <div id="divBuildingDataInner" class="dashboardBodyInner" runat="server">
                            
                            <div id="divBuildingDataResults" class="dashboardMessage" runat="server">
							    <asp:Label ID="lblBuildingDataResults" runat="server" Text=""></asp:Label>
							</div>
							
                            <asp:Chart ID="buildingDataChart" runat="server" ImageType="Png" EnableViewState="true"  Height="460px" Width="638px" Visible="false">
                                <Titles>
                                    <asp:Title Name="buildingDataTitle" Font="Trebuchet MS, 10pt, style=Bold"></asp:Title>
                                </Titles>
                                <Legends>
                                    <asp:Legend Enabled="true" Alignment="Center" ForeColor="#333333" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Arial, 8pt, style=Bold"></asp:Legend>
                                </Legends>                                
                                <Series></Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="buildingDataChartArea" BorderColor="Black" BorderWidth="1" BorderDashStyle="Solid" BackColor="White">
                                        <Area3DStyle Rotation="8" Perspective="8" LightStyle="Realistic" Inclination="18" IsRightAngleAxes="true" WallWidth="0" IsClustered="false"></Area3DStyle>                                        
                                        <AxisY Title="kWh" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false" IsStartedFromZero="true">
                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                            <MajorGrid LineColor="#CCCCCC" />
                                            <MajorTickMark LineColor="#CCCCCC" />
                                            <MinorTickMark TickMarkStyle="OutsideArea" Enabled="true" LineColor="#CCCCCC" />
                                        </AxisY>
                                        <AxisX Title="Meter(s)" TitleForeColor="#333333" TitleFont="Arial, 8pt, style=Bold" LineColor="Black" IsLabelAutoFit="false">
                                            <LabelStyle ForeColor="#333333" Font="Arial, 8pt" />
                                            <MajorGrid LineColor="#CCCCCC" />
                                            <MajorTickMark LineColor="#CCCCCC" />
                                        </AxisX>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>

                            <div id="buildingDataBlocks" runat="server" class="dashboardBodyInnerRight" visible="false">                 
                                <!--<div class="divBodyInnerRightTop">
                                </div>-->
                                <div class="divBodyBlock">
                                    <div class="divBodyBlockHeader">
                                        <span>Statistics</span>
                                    </div>
                                    <div class="divBodyBlockBody">
                                        <span>Total Meter Sum:</span><br />
                                        <span id="spanBuildingDataTotal" class="blockValueGreen" runat="server"></span>
                                    </div>                                    
                                </div>
                            </div>

							</div>

                            <!--Chart is required in comments, microsoft charting bug.-->
                            <!--<asp:chart id="Chart4" runat="server" Height="1px" Width="1px"></asp:chart>-->
                        </asp:View>                                                  
				   </asp:MultiView>
			   </div> 
			</div>
			<!--dashboard tabs-->
			<div id="dashboardBottomTabs" class="dashboardBottomTabs">

				<!--        OnLoad="loadTabs" 
							OnActiveTabChanged="onActiveTabChanged"
							-->
				<ajaxToolkit:TabContainer ID="TabContainer1"                  
							runat="server"  BackColor="Red"
							AutoPostBack="true"                            
							Width="100%" 
                            Visible="false" Enabled="false">
							<ajaxToolkit:TabPanel ID="TabPanel1" runat="server" 
								HeaderText="Preset Gauges">
								<ContentTemplate>
									
									<div class="dashboardBottomBodyLeftColumn">
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody">
																						
												<!--TODO: RadScheduler for the calendar based chart widget-->

											</div>
										</div>
									</div>
									<div class="dashboardBottomBodyRightColumn"> 
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
									</div> 

								</ContentTemplate>
							</ajaxToolkit:TabPanel>                            
							<ajaxToolkit:TabPanel ID="TabPanel2" runat="server" 
								HeaderText="Utility">
								<ContentTemplate>
								
								<!-- TODO: RadDock for each widget-->
								
                                	<div class="dashboardBottomBodyLeftColumn">
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget
											</div>
											<div class="widgetBody"></div>
										</div>
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
									</div>
									<div class="dashboardBottomBodyRightColumn"> 
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
										<div class="widgetBoxWide">
											<div class="widgetTopWide">Some Widget</div>
											<div class="widgetBody"></div>
										</div>
									</div>

								</ContentTemplate>
							</ajaxToolkit:TabPanel>  
				</ajaxToolkit:TabContainer>
			</div>

			<div id="dashboardFooter" class="dashboardFooter">             
			</div>

        </div>					
</asp:Content>
