﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.PointTypes.nonkgs;
using System;

namespace CW.Website
{
    public partial class ProviderPointTypeAdministration : AdminSitePageTemp
    {
        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewPointTypes).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.PROVIDER; } }

            #endregion

        #endregion
    }
}