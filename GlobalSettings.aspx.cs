﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using CW.Data;
using CW.Business;
using CW.Utility;
using CW.Common.Helpers;
using CW.Website._framework;
using CW.Logging;
using CW.Utility.Web;

//using CuteEditor;
//using FreeTextBoxControls;

namespace CW.Website
{
    public partial class GlobalSettings : SitePage
    {
        #region Properties

            const string updateGlobalSettingsSuccessful = "Global settings update was successful.";
            const string updateGlobalSettingsFailed = "Global settings update failed. Please contact an administrator.";

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page

                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                    SetHomeContentIntoEditForm();
                }
            }

        #endregion

        #region Get/Set Fields and Data
        
            protected void SetHomeContentIntoEditForm()
            {
                //get and set global settings
                GlobalSetting globalSettings = DataMgr.GlobalDataMapper.GetGlobalSettings();

                //Make sure to decode all from ascii to html 

                //Welcome HomePageHeader                
                txtWelcomePageHeader.Value = globalSettings.WelcomePageHeader;
                //Welcome HomePageBody
                editorWelcomePageBody.Content = globalSettings.WelcomePageBody;   
                //HomePageHeader
                txtHomePageHeader.Value = globalSettings.HomePageHeader;               
                //HomePageBody
                editorHomePageBody.Content = globalSettings.HomePageBody;
            }

            protected void SetAdminContentIntoEditForm()
            {
                //get and set global settings
                GlobalSetting globalSettings = DataMgr.GlobalDataMapper.GetGlobalSettings();

                //Make sure to decode all from ascii to html 

                //admin body
                editorAdminBody.Content = globalSettings.AdminBody;              
                //admin data sources body
                editorAdminDataSourcesBody.Content = globalSettings.AdminDataSourcesBody;
                //admin building groups body
                editorAdminBuildingGroupsBody.Content = globalSettings.AdminBuildingGroupsBody;
                //admin buildings body
                editorAdminBuildingsBody.Content = globalSettings.AdminBuildingsBody;           
                //admin equipment body
                editorAdminEquipmentBody.Content = globalSettings.AdminEquipmentBody;
                //admin points body
                editorAdminPointsBody.Content = globalSettings.AdminPointsBody;          
                //admin analyses body
                editorAdminAnalysesBody.Content = globalSettings.AdminAnalysesBody;     
                //admin scheduled analyses body
                editorAdminScheduledAnalysesBody.Content = globalSettings.AdminScheduledAnalysesBody;
                //admin users body
                editorAdminUsersBody.Content = globalSettings.AdminUsersBody;           
                //admin settings body
                editorAdminSettingsBody.Content = globalSettings.AdminSettingsBody;
                //admin organization body
                editorAdminOrganizationBody.Content = globalSettings.AdminOrganizationBody;
                //admin settings body
                editorAdminClientBody.Content = globalSettings.AdminClientBody;   
            }

            protected void SetModuleContentIntoEditForm()
            {
                //get and set global settings
                GlobalSetting globalSettings = DataMgr.GlobalDataMapper.GetGlobalSettings();

                //Make sure to decode all from ascii to html 
                
                //alarms body
                editorAlarmsBody.Content = globalSettings.AlarmsBody;
                //analysis builder body
                editorAnalysisBuilderBody.Content = globalSettings.AnalysisBuilderBody;
                //commissioning dashboard body
                editorCommissioningDashboardBody.Content = globalSettings.CommissioningDashboardBody;
                //diagnostics body
                editorDiagnosticsBody.Content = globalSettings.DiagnosticsBody;
                //documents body
                editorDocumentsBody.Content = globalSettings.DocumentsBody;
                //energy dashboard body
                editorEnergyDashboardBody.Content = globalSettings.EnergyDashboardBody; 
                //operations body
                editorOperationsBody.Content = globalSettings.OperationsBody;
                //performance dashboard body
                editorPerformanceDashboardBody.Content = globalSettings.PerformanceDashboardBody;
                //profiles body
                editorProfilesBody.Content = globalSettings.ProfilesBody;
                //projects body
                editorProjectsBody.Content = globalSettings.ProjectsBody;
                //reports body
                editorReportsBody.Content = globalSettings.ReportsBody;
                //tasks body
                editorTasksBody.Content = globalSettings.TasksBody;       
                //utility billing body
                editorUtilityBillingBody.Content = globalSettings.UtilityBillingBody;
            }

            protected void SetHelpContentIntoEditForm()
            {
                //get and set global settings
                GlobalSetting globalSettings = DataMgr.GlobalDataMapper.GetGlobalSettings();

                //Make sure to decode all from ascii to html 

                //help body
                editorHelpBody.Content = globalSettings.HelpBody;
                //privacy policy body
                editorPrivacyPolicyBody.Content = globalSettings.PrivacyPolicyBody;
                //terms and conditions body
                editorTermsAndConditionsBody.Content = globalSettings.TermsAndConditionsBody;
            }

            protected void SetKioskHelpContentIntoEditForm()
            {
                //get and set global settings
                GlobalSetting globalSettings = DataMgr.GlobalDataMapper.GetGlobalSettings();

                //Make sure to decode all from ascii to html 

                //q1
                editorKioskHelpQ1.Content = globalSettings.KioskHelpQ1;
                //q2
                editorKioskHelpQ2.Content = globalSettings.KioskHelpQ2;
                //q3
                editorKioskHelpQ3.Content = globalSettings.KioskHelpQ3;
                //q4
                editorKioskHelpQ4.Content = globalSettings.KioskHelpQ4;
            }

        #endregion

        #region Load Global Settings

            /// <summary>
            /// Loads the editor form data for global settings for update.
            /// 
            /// Also removes extra characters over max html limits, and then encodes the html.
            /// </summary>
            /// <param name="globalSettings"></param>
            protected void LoadHomeContentEditFormIntoGlobalSettings(GlobalSetting globalSettings)
            {
                //only one record, so id is not needed to be set. 
                //instead the first record is updated. shown in datamapper.

                //if editor's html is greater then defined max html length, 
                //then remove extra characters.
                //Make sure to html encode before putting in the database. From html to ascii.

                //Welcome HomePageHeader   
                globalSettings.WelcomePageHeader = String.IsNullOrEmpty(txtWelcomePageHeader.Value) ? null : txtWelcomePageHeader.Value; 
                //Welcome HomePageBody                
                globalSettings.WelcomePageBody = StringHelper.ClearRadEditorFirefoxBR(editorWelcomePageBody.Content);  //.Length > editorWelcomePageBody.MaxHTMLLength ? editorWelcomePageBody.Text.Remove(editorWelcomePageBody.MaxHTMLLength)) : editorWelcomePageBody.Text);
                //HomePageHeader   
                globalSettings.HomePageHeader = String.IsNullOrEmpty(txtHomePageHeader.Value) ? null : txtHomePageHeader.Value;
                //HomePageBody                
                globalSettings.HomePageBody = StringHelper.ClearRadEditorFirefoxBR(editorHomePageBody.Content); //.Length > editorKGSAdminHomePageBody.MaxHTMLLength ? editorKGSAdminHomePageBody.Text.Remove(editorKGSAdminHomePageBody.MaxHTMLLength)) : editorKGSAdminHomePageBody.Text);

            }

            /// <summary>
            /// Loads the editor form data for global settings for update.
            /// 
            /// Also removes extra characters over max html limits, and then encodes the html.
            /// </summary>
            /// <param name="globalSettings"></param>
            protected void LoadAdminContentEditFormIntoGlobalSettings(GlobalSetting globalSettings)
            {
                //only one record, so id is not needed to be set. 
                //instead the first record is updated. shown in datamapper.

                //if editor's html is greater then defined max html length, 
                //then remove extra characters.
                //Make sure to html encode before putting in the database. From html to ascii.

                //admin body
                globalSettings.AdminBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminBody.Content); //.Length > editorAdminBody.MaxHTMLLength ? editorAdminBody.Text.Remove(editorAdminBody.MaxHTMLLength)) : editorAdminBody.Text);                
                //admin data sources body                
                globalSettings.AdminDataSourcesBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminDataSourcesBody.Content); //.Length > editorAdminDataSourcesBody.MaxHTMLLength ? editorAdminDataSourcesBody.Text.Remove(editorAdminDataSourcesBody.MaxHTMLLength)) : editorAdminDataSourcesBody.Text);                
                //admin buildings groups body                
                globalSettings.AdminBuildingGroupsBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminBuildingGroupsBody.Content); //.Length > editorAdminBuildingGroupsBody.MaxHTMLLength ? editorAdminBuildingGroupsBody.Text.Remove(editorAdminBuildingGroupsBody.MaxHTMLLength)) : editorAdminBuildingGroupsBody.Text);                
                //admin buildings body                
                globalSettings.AdminBuildingsBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminBuildingsBody.Content); //.Length > editorAdminBuildingsBody.MaxHTMLLength ? editorAdminBuildingsBody.Text.Remove(editorAdminBuildingsBody.MaxHTMLLength)) : editorAdminBuildingsBody.Text);                
                //admin equipment body                
                globalSettings.AdminEquipmentBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminEquipmentBody.Content); //.Length > editorAdminEquipmentBody.MaxHTMLLength ? editorAdminEquipmentBody.Text.Remove(editorAdminEquipmentBody.MaxHTMLLength)) : editorAdminEquipmentBody.Text);                
                //admin points body               
                globalSettings.AdminPointsBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminPointsBody.Content); //.Length > editorAdminPointsBody.MaxHTMLLength ? editorAdminPointsBody.Text.Remove(editorAdminPointsBody.MaxHTMLLength)) : editorAdminPointsBody.Text);                
                //admin analyses body                
                globalSettings.AdminAnalysesBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminAnalysesBody.Content); //.Length > editorAdminAnalysesBody.MaxHTMLLength ? editorAdminAnalysesBody.Text.Remove(editorAdminAnalysesBody.MaxHTMLLength)) : editorAdminAnalysesBody.Text);                
                //admin scheduled analyses body                
                globalSettings.AdminScheduledAnalysesBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminScheduledAnalysesBody.Content); //.Length > editorAdminScheduledAnalysesBody.MaxHTMLLength ? editorAdminScheduledAnalysesBody.Text.Remove(editorAdminScheduledAnalysesBody.MaxHTMLLength)) : editorAdminScheduledAnalysesBody.Text);                
                //admin users body
                globalSettings.AdminUsersBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminUsersBody.Content); //.Length > editorAdminUsersBody.MaxHTMLLength ? editorAdminUsersBody.Text.Remove(editorAdminUsersBody.MaxHTMLLength)) : editorAdminUsersBody.Text);                
                //admin settings body
                globalSettings.AdminSettingsBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminSettingsBody.Content); //.Length > editorAdminSettingsBody.MaxHTMLLength ? editorAdminSettingsBody.Text.Remove(editorAdminSettingsBody.MaxHTMLLength)) : editorAdminSettingsBody.Text);                
                //admin organization body
                globalSettings.AdminOrganizationBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminOrganizationBody.Content); //.Length > editorAdminSettingsBody.MaxHTMLLength ? editorAdminSettingsBody.Text.Remove(editorAdminSettingsBody.MaxHTMLLength)) : editorAdminSettingsBody.Text);                
                //admin client body
                globalSettings.AdminClientBody = StringHelper.ClearRadEditorFirefoxBR(editorAdminClientBody.Content); //.Length > editorAdminSettingsBody.MaxHTMLLength ? editorAdminSettingsBody.Text.Remove(editorAdminSettingsBody.MaxHTMLLength)) : editorAdminSettingsBody.Text);                
            }

            /// <summary>
            /// Loads the editor form data for global settings for update.
            /// 
            /// Also removes extra characters over max html limits, and then encodes the html.
            /// </summary>
            /// <param name="globalSettings"></param>
            protected void LoadModuleContentEditFormIntoGlobalSettings(GlobalSetting globalSettings)
            {
                //only one record, so id is not needed to be set. 
                //instead the first record is updated. shown in datamapper.

                //if editor's html is greater then defined max html length, 
                //then remove extra characters.
                //Make sure to html encode before putting in the database. From html to ascii.

                //alarms body
                globalSettings.AlarmsBody = StringHelper.ClearRadEditorFirefoxBR(editorAlarmsBody.Content); 
                //analysis builder body
                globalSettings.AnalysisBuilderBody = StringHelper.ClearRadEditorFirefoxBR(editorAnalysisBuilderBody.Content); //.Length > editorAnalysisBuilderBody.MaxHTMLLength ? editorAnalysisBuilderBody.Text.Remove(editorAnalysisBuilderBody.MaxHTMLLength)) : editorAnalysisBuilderBody.Text);                                
                //commissioning dashboard body
                globalSettings.CommissioningDashboardBody = StringHelper.ClearRadEditorFirefoxBR(editorCommissioningDashboardBody.Content); //.Length > editorDiagnosticsBody.MaxHTMLLength ? editorDiagnosticsBody.Text.Remove(editorDiagnosticsBody.MaxHTMLLength)) : editorDiagnosticsBody.Text);                
                //diagnostics body
                globalSettings.DiagnosticsBody = StringHelper.ClearRadEditorFirefoxBR(editorDiagnosticsBody.Content); //.Length > editorDiagnosticsBody.MaxHTMLLength ? editorDiagnosticsBody.Text.Remove(editorDiagnosticsBody.MaxHTMLLength)) : editorDiagnosticsBody.Text);                
                //documents body
                globalSettings.DocumentsBody = StringHelper.ClearRadEditorFirefoxBR(editorDocumentsBody.Content); //.Length > editorDocumentsBody.MaxHTMLLength ? editorDocumentsBody.Text.Remove(editorDocumentsBody.MaxHTMLLength)) : editorDocumentsBody.Text);                                
                //energy dashboard body
                globalSettings.EnergyDashboardBody = StringHelper.ClearRadEditorFirefoxBR(editorEnergyDashboardBody.Content);                              
                //operations dashboard body
                globalSettings.OperationsBody = StringHelper.ClearRadEditorFirefoxBR(editorOperationsBody.Content); //.Length > editorDiagnosticsBody.MaxHTMLLength ? editorDiagnosticsBody.Text.Remove(editorDiagnosticsBody.MaxHTMLLength)) : editorDiagnosticsBody.Text);                
                //performance dashboard body
                globalSettings.PerformanceDashboardBody = StringHelper.ClearRadEditorFirefoxBR(editorPerformanceDashboardBody.Content); //.Length > editorPerformanceDashboardBody.MaxHTMLLength ? editorPerformanceDashboardBody.Text.Remove(editorPerformanceDashboardBody.MaxHTMLLength)) : editorPerformanceDashboardBody.Text);                                
                //profiles body
                globalSettings.ProfilesBody = StringHelper.ClearRadEditorFirefoxBR(editorProfilesBody.Content); //.Length > editorDiagnosticsBody.MaxHTMLLength ? editorDiagnosticsBody.Text.Remove(editorDiagnosticsBody.MaxHTMLLength)) : editorDiagnosticsBody.Text);                
                //projects body
                globalSettings.ProjectsBody = StringHelper.ClearRadEditorFirefoxBR(editorProjectsBody.Content);
                //reports body
                globalSettings.ReportsBody = StringHelper.ClearRadEditorFirefoxBR(editorReportsBody.Content); //.Length > editorReportsBody.MaxHTMLLength ? editorReportsBody.Text.Remove(editorReportsBody.MaxHTMLLength)) : editorReportsBody.Text);                                
                //tasks body
                globalSettings.TasksBody = StringHelper.ClearRadEditorFirefoxBR(editorTasksBody.Content);
                //utility billing body
                globalSettings.UtilityBillingBody = StringHelper.ClearRadEditorFirefoxBR(editorUtilityBillingBody.Content);              
            }

            /// <summary>
            /// Loads the editor form data for global settings for update.
            /// 
            /// Also removes extra characters over max html limits, and then encodes the html.
            /// </summary>
            /// <param name="globalSettings"></param>
            protected void LoadHelpContentEditFormIntoGlobalSettings(GlobalSetting globalSettings)
            {
                //only one record, so id is not needed to be set. 
                //instead the first record is updated. shown in datamapper.

                //if editor's html is greater then defined max html length, 
                //then remove extra characters.
                //Make sure to html encode before putting in the database. From html to ascii.

                //help body
                globalSettings.HelpBody = StringHelper.ClearRadEditorFirefoxBR(editorHelpBody.Content); //.Length > editorHelpBody.MaxHTMLLength ? editorHelpBody.Text.Remove(editorHelpBody.MaxHTMLLength)) : editorHelpBody.Text);                
                //privacy policy body
                globalSettings.PrivacyPolicyBody = StringHelper.ClearRadEditorFirefoxBR(editorPrivacyPolicyBody.Content); //.Length > editorPrivacyPolicyBody.MaxHTMLLength ? editorPrivacyPolicyBody.Text.Remove(editorPrivacyPolicyBody.MaxHTMLLength)) : editorPrivacyPolicyBody.Text);
                //terms and conditions body
                globalSettings.TermsAndConditionsBody = StringHelper.ClearRadEditorFirefoxBR(editorTermsAndConditionsBody.Content); //.Length > editorTermsAndConditionsBody.MaxHTMLLength ? editorTermsAndConditionsBody.Text.Remove(editorTermsAndConditionsBody.MaxHTMLLength)) : editorTermsAndConditionsBody.Text);
            }

            /// <summary>
            /// Loads the editor form data for global settings for update.
            /// 
            /// Also removes extra characters over max html limits, and then encodes the html.
            /// </summary>
            /// <param name="globalSettings"></param>
            protected void LoadKioskHelpContentEditFormIntoGlobalSettings(GlobalSetting globalSettings)
            {
                //only one record, so id is not needed to be set. 
                //instead the first record is updated. shown in datamapper.

                //if editor's html is greater then defined max html length, 
                //then remove extra characters.
                //Make sure to html encode before putting in the database. From html to ascii.

                //q1
                globalSettings.KioskHelpQ1 = StringHelper.ClearRadEditorFirefoxBR(editorKioskHelpQ1.Content);
                //q2
                globalSettings.KioskHelpQ2 = StringHelper.ClearRadEditorFirefoxBR(editorKioskHelpQ2.Content);
                //q3
                globalSettings.KioskHelpQ3 = StringHelper.ClearRadEditorFirefoxBR(editorKioskHelpQ3.Content);
                //q4
                globalSettings.KioskHelpQ4 = StringHelper.ClearRadEditorFirefoxBR(editorKioskHelpQ4.Content);
            }


        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind content after tab changed back from other tabs
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    SetHomeContentIntoEditForm();
                    lblHomeContentError.Visible = false;
                }
                else if (radTabStrip.MultiPage.SelectedIndex == 1)
                {
                    SetAdminContentIntoEditForm();
                    lblAdminContentError.Visible = false;
                }
                //else if (TabContainer1.ActiveTabIndex == 2)
                //{
                //    //SetProviderAdminContentIntoEditForm();
                //    //lblProviderAdminContentError.Visible = false;
                //}
                else if (radTabStrip.MultiPage.SelectedIndex == 2)
                {
                    SetModuleContentIntoEditForm();
                    lblModuleContentError.Visible = false;
                }
                else if (radTabStrip.MultiPage.SelectedIndex == 3)
                {
                    SetHelpContentIntoEditForm();
                    lblHelpContentError.Visible = false;
                }
                else if (radTabStrip.MultiPage.SelectedIndex == 4)
                {
                    SetKioskHelpContentIntoEditForm();
                    lblKioskHelpContentError.Visible = false;
                }
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Update home content Button on click.
            /// </summary>
            protected void updateHomeContentButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the global settings
                GlobalSetting mGlobalSettings = new GlobalSetting();
                LoadHomeContentEditFormIntoGlobalSettings(mGlobalSettings);

                try
                {
                    DataMgr.GlobalDataMapper.UpdateHomeContent(mGlobalSettings);
                    LabelHelper.SetLabelMessage(lblHomeContentError, updateGlobalSettingsSuccessful, lnkSetFocusHome);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblHomeContentError, updateGlobalSettingsFailed, lnkSetFocusHome);
                    LogMgr.Log(Common.Constants.DataConstants.LogLevel.ERROR, "Unable to update home content global settings.", ex);
                }
            }

            /// <summary>
            /// Update admin content Button on click.
            /// </summary>
            protected void updateAdminContentButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the global settings
                GlobalSetting mGlobalSettings = new GlobalSetting();
                LoadAdminContentEditFormIntoGlobalSettings(mGlobalSettings);

                try
                {
                    DataMgr.GlobalDataMapper.UpdateAdminContent(mGlobalSettings);                    
                    LabelHelper.SetLabelMessage(lblAdminContentError, updateGlobalSettingsSuccessful, lnkSetFocusAdmin);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblAdminContentError, updateGlobalSettingsFailed, lnkSetFocusAdmin);
                    LogMgr.Log(Common.Constants.DataConstants.LogLevel.ERROR, "Unable to update admin content global settings.", ex);
                }
            }

            /// <summary>
            /// Update module content Button on click.
            /// </summary>
            protected void updateModuleContentButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the global settings
                GlobalSetting mGlobalSettings = new GlobalSetting();
                LoadModuleContentEditFormIntoGlobalSettings(mGlobalSettings);

                try
                {
                    DataMgr.GlobalDataMapper.UpdateModuleContent(mGlobalSettings);
                    LabelHelper.SetLabelMessage(lblModuleContentError, updateGlobalSettingsSuccessful, lnkSetFocusModule);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblModuleContentError, updateGlobalSettingsFailed, lnkSetFocusModule);
                    LogMgr.Log(Common.Constants.DataConstants.LogLevel.ERROR, "Unable to update module content global settings.", ex);
                }
            }

            /// <summary>
            /// Update help content Button on click.
            /// </summary>
            protected void updateHelpContentButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the global settings
                GlobalSetting mGlobalSettings = new GlobalSetting();
                LoadHelpContentEditFormIntoGlobalSettings(mGlobalSettings);

                try
                {
                    DataMgr.GlobalDataMapper.UpdateHelpContent(mGlobalSettings);
                    LabelHelper.SetLabelMessage(lblHelpContentError, updateGlobalSettingsSuccessful, lnkSetFocusHelp);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblHelpContentError, updateGlobalSettingsFailed, lnkSetFocusHelp);
                    LogMgr.Log(Common.Constants.DataConstants.LogLevel.ERROR, "Unable to update help content global settings.", ex);
                }
            }

            /// <summary>
            /// Update kiosk help content Button on click.
            /// </summary>
            protected void updateKioskHelpContentButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the global settings
                GlobalSetting mGlobalSettings = new GlobalSetting();
                LoadKioskHelpContentEditFormIntoGlobalSettings(mGlobalSettings);

                try
                {
                    DataMgr.GlobalDataMapper.UpdateKioskHelpContent(mGlobalSettings);
                    LabelHelper.SetLabelMessage(lblKioskHelpContentError, updateGlobalSettingsSuccessful, lnkSetFocusKioskHelp);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblKioskHelpContentError, updateGlobalSettingsFailed, lnkSetFocusKioskHelp);
                    LogMgr.Log(Common.Constants.DataConstants.LogLevel.ERROR, "Unable to update kiosk help content global settings.", ex);
                }
            }


        #endregion       
    }
}
