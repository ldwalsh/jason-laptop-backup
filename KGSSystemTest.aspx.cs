﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.System;
using System;

namespace CW.Website
{
    public partial class KGSSystemTest : AdminSitePageTemp
    {
        #region properties

            protected override String DefaultControl { get { return typeof(IndividualDataSourceCheck).Name; } }

            public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.KGS; } }

        #endregion

        #region events

        private void Page_Load()
        {
            //logged in security check in master page.
            //secondary security check specific to this page.
            if (!siteUser.IsKGSSuperAdminOrHigher) Response.Redirect("/Home.aspx");
        }

        #endregion
    }
}