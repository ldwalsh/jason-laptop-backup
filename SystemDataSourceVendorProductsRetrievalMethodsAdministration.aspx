﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemDataSourceVendorProductsRetrievalMethodsAdministration.aspx.cs" Inherits="CW.Website.SystemDataSourceVendorProductsRetrievalMethodsAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>System Data Source Vendor Products Retrieval Methods Administration</h1>                        
                <div class="richText">The system data source vendor products retrieval methods administration area is used to view, add, and edit data source vendor products retrieval methods.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Data Source Vendor Products Retrieval Methods"></telerik:RadTab>
                            <telerik:RadTab Text="Add Data Source Vendor Product Retrieval Method"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Data Source Vendor Products Retrieval Methods</h2>
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p>                                         
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridDataSourceVendorProductsRetrievalMethods"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="VendorProductRetrievalMethodID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridDataSourceVendorProductsRetrievalMethods_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridDataSourceVendorProductsRetrievalMethods_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridDataSourceVendorProductsRetrievalMethods_Sorting"  
                                             OnSelectedIndexChanged="gridDataSourceVendorProductsRetrievalMethods_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridDataSourceVendorProductsRetrievalMethods_Editing"                                                                                                                                                                                   
                                             OnRowDeleting="gridDataSourceVendorProductsRetrievalMethods_Deleting"
                                             OnDataBound="gridDataSourceVendorProductsRetrievalMethods_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="VendorName" HeaderText="Vendor">   
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("VendorName"),40) %></ItemTemplate>                        
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="VendorProductName" HeaderText="Vendor Product Name">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("VendorProductName"),40) %></ItemTemplate> 
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="RetrievalMethod" HeaderText="Retrieval Method">   
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("RetrievalMethod"),30) %></ItemTemplate>                        
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="ConfigVariable">  
                                                    <ItemTemplate><%# Eval("ConfigVariable")%></ItemTemplate>                           
                                                </asp:TemplateField>                                                                                                                                              
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this data source vendor product retrieval method permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT DATA SOURCE VENDOR PRODUCT RETRIEVAL METHOD DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvVendorProductRetrievalMethod" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Data Source Vendor Product Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Vendor: </strong>" + Eval("VendorName") + "</li>"%>  
                                                            <%# "<li><strong>Vendor Product Name: </strong>" + Eval("VendorProductName") + "</li>"%> 
                                                            <%# "<li><strong>Retrieval Method: </strong>" + Eval("RetrievalMethod") + "</li>"%>  
                                                            <%# "<li><strong>Config Variable: </strong>" + Eval("ConfigVariable") + "</li>"%>  
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT DATA SOURCE VENDOR PRODUCT RETRIEVAL METHOD PANEL -->                              
                                    <asp:Panel ID="pnlEditVendorProductRetrievalMethod" runat="server" Visible="false" DefaultButton="btnUpdateVendorProductRetrievalMethod">                                                                                             
                                        <div>
                                            <h2>Edit Data Source Vendor Product Retrieval Method</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Vendor Name:</label>
                                                <asp:Label ID="lblEditVendorName" MaxLength="50" runat="server"></asp:Label>                                      
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Vendor Product Name:</label>
                                                <asp:Label ID="lblEditVendorProductName" runat="server"></asp:Label>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Retrieval Method:</label>
                                                <asp:Label ID="lblEditRetrievalMethod" runat="server"></asp:Label>                                      
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*Config Variable:</label>
                                                <asp:TextBox ID="txtEditConfigVariable" MaxLength="25" runat="server"></asp:TextBox>                                      
                                            </div>                                                                                                                                           
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateVendorProductRetrievalMethod" runat="server" Text="Edit Method"  OnClick="updateVendorProductRetrievalMethodButton_Click" ValidationGroup="EditVendorProductRetrievalMethod"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editConfigVariableRequiredValidator" runat="server"
                                        ErrorMessage="Config Variable is a required field."
                                        ControlToValidate="txtEditConfigVariable"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditVendorProductRetrievalMethod">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editConfigVariableRequiredValidatorExtender" runat="server"
                                        BehaviorID="editConfigVariableRequiredValidatorExtender"
                                        TargetControlID="editConfigVariableRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    </asp:Panel> 
                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server"> 
                                <asp:Panel ID="pnlAddVendorProductRetrievalMethod" runat="server" DefaultButton="btnAddVendorProductRetrievalMethod">
                                    <h2>Add Data Source Vendor Product Retrieval Method</h2> 
                                    <div>    
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                                
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">   
                                            <label class="label">*Vendor:</label>    
                                            <asp:DropDownList ID="ddlAddVendor" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddVendor_OnSelectedIndexChanged" AutoPostBack="true"  runat="server">                             
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>  
                                        <div class="divForm">   
                                            <label class="label">*Vendor Product:</label>    
                                            <asp:DropDownList ID="ddlAddVendorProduct" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddVendorProduct_OnSelectedIndexChanged" AutoPostBack="true"  runat="server">                             
                                                 <asp:ListItem Value="-1">Select vendor first...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>  
                                        <div class="divForm">   
                                            <label class="label">*Retrieval Method:</label>    
                                            <asp:DropDownList ID="ddlAddRetrievalMethod" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                 <asp:ListItem Value="-1">Select vendor product first...</asp:ListItem>                           
                                            </asp:DropDownList> 
                                        </div>  
                                        <div class="divForm">
                                            <label class="label">*Config Variable:</label>
                                            <asp:TextBox ID="txtAddConfigVariable" CssClass="textbox" MaxLength="25" runat="server"></asp:TextBox>                                            
                                        </div>   
                                                                                                                                                                               
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddVendorProductRetrievalMethod" runat="server" Text="Add Method"  OnClick="addVendorProductRetrievalMethodButton_Click" ValidationGroup="AddVendorProductRetrievalMethod"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->                                          
                                    <asp:RequiredFieldValidator ID="addVendorRequiredValidator" runat="server"
                                        ErrorMessage="Vendor is a required field." 
                                        ControlToValidate="ddlAddVendor"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddVendorProductRetrievalMethod">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addVendorRequiredValidatorExtender" runat="server"
                                        BehaviorID="addVendorRequiredValidatorExtender" 
                                        TargetControlID="addVendorRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="addVendorProductRequiredValidator" runat="server"
                                        ErrorMessage="Vendor Product is a required field." 
                                        ControlToValidate="ddlAddVendorProduct"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddVendorProductRetrievalMethod">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addVendorProductRequiredValidatorExtender" runat="server"
                                        BehaviorID="addVendorProductRequiredValidatorExtender" 
                                        TargetControlID="addVendorProductRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="addRetrievalMethodRequiredValidator" runat="server"
                                        ErrorMessage="Retrieval Method is a required field." 
                                        ControlToValidate="ddlAddRetrievalMethod"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddVendorProductRetrievalMethod">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addRetrievalMethodRequiredValidatorExtender" runat="server"
                                        BehaviorID="addRetrievalMethodRequiredValidatorExtender" 
                                        TargetControlID="addRetrievalMethodRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="addConfigVariableRequiredValidator" runat="server"
                                        ErrorMessage="Config Variable is a required field."
                                        ControlToValidate="txtAddConfigVariable"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddVendorProductRetrievalMethod">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addConfigVariableRequiredValidatorExtender" runat="server"
                                        BehaviorID="addConfigVariableRequiredValidatorExtender"
                                        TargetControlID="addConfigVariableRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>
                            </telerik:RadPageView>                         
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>


                    
                  
