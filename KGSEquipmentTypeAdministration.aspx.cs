﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.EquipmentTypes;
using System;

namespace CW.Website
{
    public partial class KGSEquipmentTypeAdministration : AdminSitePageTemp
    {
        #region events

            private void Page_Init()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher. 
                if (!siteUser.IsKGSSuperAdminOrHigher) Response.Redirect("/Home.aspx");
            }

        #endregion

        public enum TabMessages
        {
            AddEquipmentType,
            EditEquipmentType,
            DeleteEquipmentType
        }

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewEquipmentTypes).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.KGS; } }

            #endregion

        #endregion
    }
}