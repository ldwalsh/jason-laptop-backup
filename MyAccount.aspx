﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Secondary.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="MyAccount.aspx.cs" Inherits="CW.Website.MyAccount" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <h1>My Account</h1>
                               
      <div class="richText">Your account information.</div> 
           
      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>                                       
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>  
      </div> 

      <div class="administrationControls">

        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
          <Tabs>
            <telerik:RadTab Text="Profile" />
            <telerik:RadTab Text="Quick Links" />
          </Tabs>
        </telerik:RadTabStrip>
        
        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">

            <!--EDIT USER PANEL -->                              
            <asp:Panel ID="pnlEditUsers" runat="server" DefaultButton="btnUpdateUser">                                                                                             
            <div>
            <h2>Edit Profile</h2>
            </div>  
            <div>         
            <a id="lnkSetFocus" runat="server"></a>                                               
            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
            <div class="divForm">
            <label class="label">*First Name:</label>
            <asp:TextBox ID="txtEditFirstName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                                                                            
            </div>
            <div class="divForm">
            <label class="label">*Last Name:</label>
            <asp:TextBox ID="txtEditLastName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                                                                                                                                 
            </div>
            <div class="divForm">
                <div class="label" style="display: inline !important">
                    Email:
                </div>
                <div id="emailAddress" class="label" style="display: inline!important; position:relative!important; text-align:left!important">
                    <label id="txtEmail" runat="server"></label>
                </div>
            </div>
            <div class="divForm">
            <label class="label">Address:</label>
            <asp:TextBox ID="txtEditAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                                        
            </div>                                                                                                                                                                                           
            <div class="divForm">
                <label class="label">City:</label>
                <asp:TextBox CssClass="textbox" ID="txtEditCity" runat="server" MaxLength="100"></asp:TextBox>
                                                                                                                  
            </div>
            <div class="divForm">
                <label class="label">*Country:</label>
                <asp:DropDownList CssClass="dropdown" ID="ddlEditCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">                
                </asp:DropDownList>
            </div>
            <div class="divForm">
                <label class="label">State (Required based on Country):</label>
                <asp:DropDownList ID="ddlEditState" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                <asp:ListItem Value="-1">Select country first...</asp:ListItem>
                </asp:DropDownList>                                                         
            </div>
            <div class="divForm">
                <label class="label">Zip Code (Required based on Country):</label>
                <telerik:RadMaskedTextBox ID="txtEditZip" CssClass="textbox" runat="server" 
                    PromptChar="" ValidationGroup="EditUser"></telerik:RadMaskedTextBox>                                                           
            </div>                                                    
            <div class="divForm">
                <label class="label">Phone:</label>
                <telerik:RadMaskedTextBox ID="txtEditPhone" CssClass="textbox" runat="server"
                    Mask="(###)###-####" ValidationGroup="EditUser"></telerik:RadMaskedTextBox>                                  
            </div>  
            <div class="divForm">
                <label class="label">Mobile Phone:</label>
                <telerik:RadMaskedTextBox ID="txtEditMobilePhone" CssClass="textbox" runat="server"
                    Mask="(###)###-####" ValidationGroup="EditUser"></telerik:RadMaskedTextBox>                                                            
            </div>                                                                                                                        
            <div class="divForm">
            <label class="label">*Culture:</label>
            <asp:DropDownList CssClass="dropdown" ID="ddlEditCulture" runat="server">  
            </asp:DropDownList>
            <p>(Note: Changes will take effect on next login.)</p>
            </div>

            <hr />
            <div>
                <h2>Reset Password</h2>
                <div id="externallyManaged" runat="server" style="display: none">
                    Your password is managed by your company and cannot be accessed here. Please contact your company's administrator.
                </div>
                <div id="internallyManaged" runat="server" style="display: none">
                    <p>Enter a new password only if you wish to reset it.</p>
                    <div class="divForm">                                                          
                        <label class="label">New Password:</label>                                                                                          
                        <asp:TextBox ID="txtEditNewPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>
                        </div> 
                        <div class="divForm">                                                          
                            <label class="label">Confirm New Password:</label>  
                            <asp:TextBox ID="txtEditConfirmNewPassword" CssClass="textbox" MaxLength="25" TextMode="Password" runat="server"></asp:TextBox>
                        </div>                                                                                                                                                                                                                                                                            
                    </div>
                </div>
            </div>
            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateUser" runat="server" Text="Update User" OnClientClick="confirmRequired('ctl00_plcCopy_txtEditNewPassword', 'ctl00_plcCopy_editConfirmNewPasswordRequiredValidator', 'editConfirmNewPasswordRequiredValidatorExtender_popupTable')"  OnClick="updateUserButton_Click" ValidationGroup="EditUser"></asp:LinkButton>                                                                                   
                                                
            <!--Ajax Validators-->
            <asp:RequiredFieldValidator ID="editFirstNameRequiredValidator" runat="server"
                ErrorMessage="First Name is a required field."
                ControlToValidate="txtEditFirstName"          
                SetFocusOnError="true"
                Display="None"
                ValidationGroup="EditUser">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="editFirstNameRequiredValidatorExtender" runat="server"
                BehaviorID="editFirstNameRequiredValidatorExtender"
                TargetControlID="editFirstNameRequiredValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
            </ajaxToolkit:ValidatorCalloutExtender> 
            <asp:RequiredFieldValidator ID="editLastNameRequiredValidator" runat="server"
                ErrorMessage="Last Name is a required field."
                ControlToValidate="txtEditLastName"          
                SetFocusOnError="true"
                Display="None"
                ValidationGroup="EditUser">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="editLastNameRequiredValidatorExtender" runat="server"
                BehaviorID="editLastNameRequiredValidatorExtender"
                TargetControlID="editLastNameRequiredValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
            </ajaxToolkit:ValidatorCalloutExtender> 
            <asp:RequiredFieldValidator ID="editCountryRequiredValidator" runat="server"
                ErrorMessage="Country is a required field." 
                ControlToValidate="ddlEditCountry"  
                SetFocusOnError="true" 
                Display="None" 
                InitialValue="-1"
                ValidationGroup="EditUser">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="editCountryRequiredValidatorExtender" runat="server"
                BehaviorID="editCountryRequiredValidatorExtender" 
                TargetControlID="editCountryRequiredValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
            </ajaxToolkit:ValidatorCalloutExtender> 
            <asp:RequiredFieldValidator ID="editStateRequiredValidator" runat="server"
                ErrorMessage="State is a required field." 
                ControlToValidate="ddlEditState"  
                SetFocusOnError="true" 
                Display="None" 
                InitialValue="-1"
                ValidationGroup="EditUser">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="editStateRequiredValidatorExtender" runat="server"
                BehaviorID="editStateRequiredValidatorExtender" 
                TargetControlID="editStateRequiredValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
            </ajaxToolkit:ValidatorCalloutExtender>                                                  
            <asp:RequiredFieldValidator ID="editZipRequiredValidator" runat="server"
                ErrorMessage="Zip Code is a required field."
                ControlToValidate="txtEditZip"          
                SetFocusOnError="true"
                Display="None"
                ValidationGroup="EditUser"
                Enabled="false">
                </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="editZipRequiredValidatorExtender" runat="server"
                BehaviorID="editZipRequiredValidatorExtender"
                TargetControlID="editZipRequiredValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
                </ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator ID="editZipRegExValidator" runat="server"
                ErrorMessage="Invalid Zip Format."
                ValidationExpression="\d{5}"
                ControlToValidate="txtEditZip"
                SetFocusOnError="true" 
                Display="None" 
                ValidationGroup="EditUser"
                Enabled="false">
                </asp:RegularExpressionValidator>  
            <ajaxToolkit:ValidatorCalloutExtender ID="editZipRegExValidatorExtender" runat="server"
                BehaviorID="editZipRegExValidatorExtender"
                TargetControlID="editZipRegExValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
                </ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator ID="editPhoneRegExValidator" runat="server"
                ErrorMessage="Invalid Phone Format."
                ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                ControlToValidate="txtEditPhone"
                SetFocusOnError="true" 
                Display="None" 
                ValidationGroup="EditUser">
                </asp:RegularExpressionValidator>                         
            <ajaxToolkit:ValidatorCalloutExtender ID="editPhoneRegExValidatorExtender" runat="server"
                BehaviorID="editPhoneRegExValidatorExtender" 
                TargetControlID="editPhoneRegExValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
                </ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator ID="editMobilePhoneRegExValidator" runat="server"
                ErrorMessage="Invalid Phone Format."
                ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                ControlToValidate="txtEditMobilePhone"
                SetFocusOnError="true" 
                Display="None" 
                ValidationGroup="EditUser">
                </asp:RegularExpressionValidator>                         
            <ajaxToolkit:ValidatorCalloutExtender ID="editMobilePhoneRegExValidatorExtender" runat="server"
                BehaviorID="editMobilePhoneRegExValidatorExtender" 
                TargetControlID="editMobilePhoneRegExValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
                </ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator ID="editNewPasswordRegExValidator" runat="server"
                ErrorMessage="Password must be 8-20 characters and include at least one non-letter character."
				ValidationExpression="^(?=.*[^a-zA-Z]).{8,20}$"
                ControlToValidate="txtEditNewPassword"
                SetFocusOnError="true" 
                Display="None" 
                ValidationGroup="EditUser">
            </asp:RegularExpressionValidator>                         
            <ajaxToolkit:ValidatorCalloutExtender ID="editNewPaswordRegExValidatorExtender" runat="server"
                BehaviorID="editNewPasswordRegExValidatorExtender" 
                TargetControlID="editNewPasswordRegExValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
            </ajaxToolkit:ValidatorCalloutExtender>
            <asp:RegularExpressionValidator ID="editConfirmNewPasswordRegExValidator" runat="server"
                ErrorMessage="Confirm Password must be 8-20 characters and include at least one non-letter character."
				ValidationExpression="^(?=.*[^a-zA-Z]).{8,20}$"
                ControlToValidate="txtEditConfirmNewPassword"
                SetFocusOnError="true" 
                Display="None" 
                ValidationGroup="EditUser">
                </asp:RegularExpressionValidator>                         
            <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmNewPasswordRegExValidatorExtender" runat="server"
                BehaviorID="editConfirmNewPasswordRegExValidatorExtender" 
                TargetControlID="editConfirmNewPasswordRegExValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
                </ajaxToolkit:ValidatorCalloutExtender>
            <asp:RequiredFieldValidator ID="editConfirmNewPasswordRequiredValidator" runat="server"
                ErrorMessage="Confirm New Password is a required field."
                ControlToValidate="txtEditConfirmNewPassword"          
                SetFocusOnError="true"
                Display="None"
                ValidationGroup="EditUser">
            </asp:RequiredFieldValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmNewPasswordRequiredValidatorExtender" runat="server"
                BehaviorID="editConfirmNewPasswordRequiredValidatorExtender"
                TargetControlID="editConfirmNewPasswordRequiredValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
            </ajaxToolkit:ValidatorCalloutExtender> 
            <asp:CompareValidator ID="editConfirmNewPasswordCompareValidator" runat="server"
                ErrorMessage="The passwords do not match. Passwords are case sensitive." 
                ControlToValidate="txtEditConfirmNewPassword"
                ControlToCompare="txtEditNewPassword" 
                Operator="Equal"
                Display="None"
                ValidationGroup="EditUser">
                </asp:CompareValidator>
            <ajaxToolkit:ValidatorCalloutExtender ID="editConfirmNewPasswordCompareValidatorExtender" runat="server"
                BehaviorID="editConfirmNewPasswordCompareValidatorExtender" 
                TargetControlID="editConfirmNewPasswordCompareValidator"
                HighlightCssClass="validatorCalloutHighlight"
                Width="175">
                </ajaxToolkit:ValidatorCalloutExtender>   
                                                                                                
            </asp:Panel> 

          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">

            <asp:Panel ID="pnlUpdateQuickLinks" runat="server" DefaultButton="btnUpdateQuickLinks">
        
              <div>                                    
                <h2>Assign Quick Links</h2>
              </div>

              <div>

                <asp:Label ID="lblEditQuickLinkError" CssClass="errorMessage" Visible="false" runat="server" />
                                                                  
                <p>Quick links are customizable links on your home page for easy access to specific area's.</p>

                <div class="divForm">
                  <label class="label">Available Quick Links:</label>                                                                                                                                         
                  <asp:ListBox ID="lbEditQuickLinksTop" CssClass="listbox" runat="server" SelectionMode="Multiple" /> 
                                                                           
                  <div class="divArrows">
                    <asp:ImageButton CssClass="up-arrow"  ID="ImageButton5" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditQuickLinksUpButton_Click" />
                    <asp:ImageButton CssClass="down-arrow" ID="ImageButton6" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditQuickLinksDownButton_Click" />
                  </div>

                  <label class="label">Assigned Quick Links:</label> 
                       
                  <asp:ListBox ID="lbEditQuickLinksBottom" CssClass="listbox" runat="server" SelectionMode="Multiple" />                                                                                                        
                </div>
                                                                                                                                                                                                                                                 
                <asp:LinkButton CssClass="lnkButton" ID="btnUpdateQuickLinks" runat="server" Text="Update" OnClick="updateUserQuickLinksButton_Click" ValidationGroup="EditUserQuickLinks" />

              </div>

            </asp:Panel>                                                                                                                                                                                                                                                                                                                                                                                                                                          
          </telerik:RadPageView>
                                                                                                                                                          
        </telerik:RadMultiPage>   
                        
      </div> <!--end of administrationControls div-->                                                                  
</asp:Content>