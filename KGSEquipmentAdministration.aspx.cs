﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.Equipment;
using System;

namespace CW.Website
{
    public partial class KGSEquipmentAdministration : AdminSitePageTemp
    {
        #region events

            private void Page_Init()
            {
                EquipmentVariables.AdminMode = EquipmentVariables.AdminModeEnum.KGS;
            }

        #endregion

        public enum TabMessages
        {
            AddEquipment,
            EditEquipment,
            EditEquipmentVariables,
            DeleteEquipment
        }

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewEquipment).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.KGS; } }

            #endregion

        #endregion
    }
}