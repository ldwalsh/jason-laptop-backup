﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.EquipmentManufacturerContact;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSEquipmentManufacturerContactAdministration : SitePage
    {
        #region Properties

        private EquipmentManufacturerContact mEquipmentManufacturerContact;

        const string addEquipmentManufacturerContactSuccess = " equipment manufacturer contact addition was successful.";
        const string addEquipmentManufacturerContactFailed = "Adding equipment manufacturer contact failed. Please contact an administrator.";
        const string addEquipmentManufacturerContactNameExists = "Cannot add equipment manufacturer contact because the contact already exists.";
        const string updateSuccessful = "Equipment manufacturer contact update was successful.";
        const string updateFailed = "Equipment manufacturer contact update failed. Please contact an administrator.";
        const string updateEquipmentManufacturerContactExists = "Cannot update equipment manufacturer conact because the contact already exists.";
        const string deleteSuccessful = "Equipment manufacturer contact deletion was successful.";
        const string deleteFailed = "Equipment manufacturer contact deletion failed. Please contact an administrator.";
        const string initialSortDirection = "ASC";
        const string initialSortExpression = "ManufacturerName";

        //gets and sets the gridview viewstate sort direction for the dataview
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? string.Empty; }
            set { ViewState["SortDirection"] = value; }
        }

        //gets and sets the gridview viewstate sort expression for the dataview
        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindEquipmentManufacturerContacts();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindManufacturers(ddlEditManufacturer, ddlAddManufacturer);
                    BindCountries(ddlEditCountry, ddlAddCountry);

                    sessionState["Search"] = String.Empty;

                    //if page not postback and cid querystring value exists try to set selected contact
                    if (!String.IsNullOrEmpty(Request.QueryString["cid"]))
                    {
                        try
                        {
                            //set equipment manufacturer contact
                            dtvEquipmentManufacturerContact.DataSource = DataMgr.EquipmentManufacturerContactDataMapper.GetFullEquipmentManufacturerContactByID(Convert.ToInt32(Request.QueryString["cid"].ToString()));
                            //bind equipment manufacturer to details view
                            dtvEquipmentManufacturerContact.DataBind();
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error getting querystring equipment manufacturer contact.", ex);
                        }
                    }
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetEquipmentManufacturerContactIntoEditForm(EquipmentManufacturerContact equipmentManufacturerContact)
            {
                //ID
                hdnEditID.Value = Convert.ToString(equipmentManufacturerContact.ContactID);
                //EquipmentManufacturerContactContact Name
                txtEditName.Text = equipmentManufacturerContact.Name;
                //Phone
                txtEditPhone.Text = equipmentManufacturerContact.Phone;

                //Equipment Manufacturer Contact Email (set both the editable field and the hidden field in case the user changes this value)
                txtEditEmail.Text = equipmentManufacturerContact.Email;
                hdnEditEmail.Value = equipmentManufacturerContact.Email;

                //Notes
                txtEditNotes.Value = equipmentManufacturerContact.Notes;
                //Address
                txtEditAddress.Text = equipmentManufacturerContact.Address;
                //City
                txtEditCity.Text = equipmentManufacturerContact.City;
                //Country Alpha2Code
                try
                {
                    ddlEditCountry.SelectedValue = equipmentManufacturerContact.CountryAlpha2Code;

                    ddlEditCountry_OnSelectedIndexChanged(null, null);

                    //set zip validators accordingly
                    editManufacturerContactZipRegExValidator.Enabled = equipmentManufacturerContact.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                    txtEditZip.Mask = equipmentManufacturerContact.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa"; 

                    //StateID
                    try
                    {
                        ddlEditState.SelectedValue = Convert.ToString(equipmentManufacturerContact.StateID);
                    }
                    catch
                    {
                    }
                }
                catch
                {
                }
                //ZIP
                txtEditZip.Text = equipmentManufacturerContact.Zip;

                //Equipment Manufacturer (set both the editable field and the hidden field in case the user changes this value)
                ddlEditManufacturer.SelectedValue = Convert.ToString(equipmentManufacturerContact.ManufacturerID);
                hdnEditManufacturer.Value = Convert.ToString(equipmentManufacturerContact.ManufacturerID);
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Bind all manufacturers excluding "Unknown" with id as 1.
            /// </summary>
            /// <param name="ddl"></param>
            private void BindManufacturers(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<EquipmentManufacturer> manufacturers = DataMgr.EquipmentManufacturerDataMapper.GetAllEquipmentManufacturers(false);
               
                ddl.DataTextField = "ManufacturerName";
                ddl.DataValueField = "ManufacturerID";
                ddl.DataSource = manufacturers;
                ddl.DataBind();

                ddl2.DataTextField = "ManufacturerName";
                ddl2.DataValueField = "ManufacturerID";
                ddl2.DataSource = manufacturers;
                ddl2.DataBind();
            }

            private void BindCountries(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<Country> countries = DataMgr.CountryDataMapper.GetAllCountries();

                ddl.DataTextField = "CountryName";
                ddl.DataValueField = "Alpha2Code";
                ddl.DataSource = countries;
                ddl.DataBind();

                ddl2.DataTextField = "CountryName";
                ddl2.DataValueField = "Alpha2Code";
                ddl2.DataSource = countries;
                ddl2.DataBind();
            }

            private void BindStates(DropDownList ddl, string alpha2Code)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code);
                ddl.DataBind();
            }

        #endregion

        #region Load Equipment Manufacturer Contact

            protected void LoadAddFormIntoEquipmentManufacturerContact(EquipmentManufacturerContact equipmentManufacturerContact)
            {
                //equipmentManufacturerContact Name
                equipmentManufacturerContact.Name = txtAddName.Text;
                //Phone
                equipmentManufacturerContact.Phone = String.IsNullOrEmpty(txtAddPhone.Text) ? null : txtAddPhone.Text;
                //Email
                equipmentManufacturerContact.Email = String.IsNullOrEmpty(txtAddEmail.Text) ? null : txtAddEmail.Text;
                //Notes
                equipmentManufacturerContact.Notes = String.IsNullOrEmpty(txtAddNotes.Value) ? null : txtAddNotes.Value;
                //Address
                equipmentManufacturerContact.Address = String.IsNullOrEmpty(txtAddAddress.Text) ? null : txtAddAddress.Text;
                //City
                equipmentManufacturerContact.City = String.IsNullOrEmpty(txtAddCity.Text) ? null : txtAddCity.Text;
                //CountryAlpha2Code
                equipmentManufacturerContact.CountryAlpha2Code = ddlAddCountry.SelectedValue != "-1" ? ddlAddCountry.SelectedValue : null;
                //StateID
                equipmentManufacturerContact.StateID = ddlAddState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddState.SelectedValue) : null;
                //ZIP
                    equipmentManufacturerContact.Zip = String.IsNullOrEmpty(txtAddZip.Text) ? null : txtAddZip.Text;
                //Manufacturer
                equipmentManufacturerContact.ManufacturerID = Convert.ToInt32(ddlAddManufacturer.SelectedValue);

                equipmentManufacturerContact.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoEquipmentManufacturerContact(EquipmentManufacturerContact equipmentManufacturerContact)
            {
                //ID
                equipmentManufacturerContact.ContactID = Convert.ToInt32(hdnEditID.Value);
                //equipmentManufacturer Name
                equipmentManufacturerContact.Name = txtEditName.Text;
                //Phone
                equipmentManufacturerContact.Phone = String.IsNullOrEmpty(txtEditPhone.Text) ? null : txtEditPhone.Text;
                //Email
                equipmentManufacturerContact.Email = String.IsNullOrEmpty(txtEditEmail.Text) ? null : txtEditEmail.Text;
                //Notes
                equipmentManufacturerContact.Notes = String.IsNullOrEmpty(txtEditNotes.Value) ? null : txtEditNotes.Value;
                //Address
                equipmentManufacturerContact.Address = String.IsNullOrEmpty(txtEditAddress.Text) ? null : txtEditAddress.Text;
                //City
                equipmentManufacturerContact.City = String.IsNullOrEmpty(txtEditCity.Text) ? null : txtEditCity.Text;
                //CountryAlpha2Code
                equipmentManufacturerContact.CountryAlpha2Code = ddlEditCountry.SelectedValue != "-1" ? ddlEditCountry.SelectedValue : null;
                //StateID
                equipmentManufacturerContact.StateID = ddlEditState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditState.SelectedValue) : null;
                //ZIP
                equipmentManufacturerContact.Zip = String.IsNullOrEmpty(txtEditZip.Text) ? null : txtEditZip.Text; 
                //ManufacturerID
                equipmentManufacturerContact.ManufacturerID = Convert.ToInt32(ddlEditManufacturer.SelectedValue);
            }

        #endregion

        #region Dropdown Events

            protected void ddlAddCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlAddState, ddlAddCountry.SelectedValue);

                addManufacturerContactZipRegExValidator.Enabled = ddlAddCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtAddZip.Mask = ddlAddCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            }
            protected void ddlEditCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlEditState, ddlEditCountry.SelectedValue);

                editManufacturerContactZipRegExValidator.Enabled = ddlEditCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtEditZip.Mask = ddlEditCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Add equipment manufacturer contact Button on click.
            /// </summary>
            protected void addEquipmentManufacturerContactButton_Click(object sender, EventArgs e)
            {
                mEquipmentManufacturerContact = new EquipmentManufacturerContact();

                //load the form into the Equipment Manufacturer contact
                LoadAddFormIntoEquipmentManufacturerContact(mEquipmentManufacturerContact);

                try
                {
                    //insert new equipment manufacturer contact, check if the manufacturer contact has been entered already. If it has display the error and disallow insertion of the record
                    if (DataMgr.EquipmentManufacturerContactDataMapper.DoesEquipmentManufacturerContactExist(mEquipmentManufacturerContact.Email, mEquipmentManufacturerContact.ManufacturerID))
                    {
                        LabelHelper.SetLabelMessage(lblAddError, addEquipmentManufacturerContactNameExists, lnkSetFocusAdd);
                    }
                    else
                    {
                        DataMgr.EquipmentManufacturerContactDataMapper.InsertContact(mEquipmentManufacturerContact);
                        LabelHelper.SetLabelMessage(lblAddError, mEquipmentManufacturerContact.Name + addEquipmentManufacturerContactSuccess, lnkSetFocusAdd);
                    }
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment manufacturer contact.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addEquipmentManufacturerContactFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment manufacturer contact.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addEquipmentManufacturerContactFailed, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update Equipment Manufacturer contact Button on click.
            /// </summary>
            protected void updateEquipmentManufacturerContactButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the Equipment Manufacturer
                EquipmentManufacturerContact mEquipmentManufacturerContact = new EquipmentManufacturerContact();
                LoadEditFormIntoEquipmentManufacturerContact(mEquipmentManufacturerContact);

                //try to update the Equipment Manufacturer          
                try
                {
                    //Only call the method check for existing manufacturer contact if the email or manufacturer value has changed (via the hidden values)
                    if (hdnEditEmail.Value != mEquipmentManufacturerContact.Email || hdnEditManufacturer.Value != Convert.ToString(mEquipmentManufacturerContact.ManufacturerID))
                    {
                        //update existing equipment manufacturer contact, check if the contact already exists. If it has display the error and disallow update of the record
                        if (DataMgr.EquipmentManufacturerContactDataMapper.DoesEquipmentManufacturerContactExist(mEquipmentManufacturerContact.Email, mEquipmentManufacturerContact.ManufacturerID))
                        {
                            LabelHelper.SetLabelMessage(lblEditError, updateEquipmentManufacturerContactExists, lnkSetFocusAdd);
                        }
                        else
                            UpdateEquipmentManufacturerContact(mEquipmentManufacturerContact);
                    }
                    else
                        UpdateEquipmentManufacturerContact(mEquipmentManufacturerContact);
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusAdd);
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment manufacturer contact.", ex);
                }
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindEquipmentManufacturerContacts();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindEquipmentManufacturerContacts();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind buildings grid after tab changed back from add manufacturer contact tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindEquipmentManufacturerContacts();
                }
            }

        #endregion

        #region Grid Events

            protected void gridEquipmentManufacturerContacts_OnDataBound(object sender, EventArgs e)
            {
                //check if kgs admin or higher, hide edit and delete column if not
                //bool kgsAdmin = RoleHelper.IsKGSAdminOrHigher(Session["UserRoleID"].ToString());
                //gridEquipmentManufacturers.Columns[1].Visible = kgsAdmin;
                //gridEquipmentManufacturers.Columns[4].Visible = kgsAdmin;
            }

            protected void gridEquipmentManufacturerContacts_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditEquipmentManufacturerContact.Visible = false;
                dtvEquipmentManufacturerContact.Visible = true;
                
                int equipmentManufacturerContactID = Convert.ToInt32(gridEquipmentManufacturerContacts.DataKeys[gridEquipmentManufacturerContacts.SelectedIndex].Values["ContactID"]);

                //set manufacturer contact
                dtvEquipmentManufacturerContact.DataSource = DataMgr.EquipmentManufacturerContactDataMapper.GetFullEquipmentManufacturerContactByID(equipmentManufacturerContactID);
                //bind manufacturer to details view
                dtvEquipmentManufacturerContact.DataBind();
            }

            protected void gridEquipmentManufacturerContacts_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridEquipmentManufacturerContacts_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvEquipmentManufacturerContact.Visible = false;
                pnlEditEquipmentManufacturerContact.Visible = true;

                int equipmentManufacturerContactID = Convert.ToInt32(gridEquipmentManufacturerContacts.DataKeys[e.NewEditIndex].Values["ContactID"]);

                EquipmentManufacturerContact mEquipmentManufacturerContact = DataMgr.EquipmentManufacturerContactDataMapper.GetContact(equipmentManufacturerContactID);

                //Set manufacturer contact data
                SetEquipmentManufacturerContactIntoEditForm(mEquipmentManufacturerContact);

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridEquipmentManufacturerContacts_Deleting(object sender, GridViewDeleteEventArgs e)
             {
                 //get deleting rows manufacturer contact id
                 int equipmentManufacturerContactID = Convert.ToInt32(gridEquipmentManufacturerContacts.DataKeys[e.RowIndex].Value);

                 try
                 {
                     //remove all manufacturer contacts assigned to a equipment  
                     DataMgr.EquipmentDataMapper.DeleteEquipmentManufacturerContactAssociations(equipmentManufacturerContactID);

                     DataMgr.EquipmentManufacturerContactDataMapper.DeleteEquipmentManufacturerContact(equipmentManufacturerContactID);

                     lblErrors.Text = deleteSuccessful;
                     lblErrors.Visible = true;
                     lnkSetFocusView.Focus();

                     pnlEditEquipmentManufacturerContact.Visible = false;
                     dtvEquipmentManufacturerContact.Visible = false;

                 }
                 catch (Exception ex)
                 {
                     lblErrors.Text = deleteFailed;
                     lblErrors.Visible = true;
                     lnkSetFocusView.Focus();

                     LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting equipment manufacturer.", ex);
                 }
                 
                 //Bind data again keeping current page and sort mode
                 DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryEquipmentManufacturerContacts());
                 gridEquipmentManufacturerContacts.PageIndex = gridEquipmentManufacturerContacts.PageIndex;
                 gridEquipmentManufacturerContacts.DataSource = SortDataTable(dataTable as DataTable, true);
                 gridEquipmentManufacturerContacts.DataBind();

                 //originally was using dataTable.ExtendedProperties.Count, that is used primarily for attributes of rows and columns, not rows directly, see article http://t8852.codeinpro.us/q/508148594f1eba38a44feb81
                 SetGridCountLabel(dataTable.Rows.Count);

                 //hide edit form if shown
                 pnlEditEquipmentManufacturerContact.Visible = false;  
             }

            protected void gridEquipmentManufacturerContacts_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEquipmentManufacturerContacts(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridEquipmentManufacturerContacts.DataSource = SortDataTable(dataTable, true);
                gridEquipmentManufacturerContacts.PageIndex = e.NewPageIndex;
                gridEquipmentManufacturerContacts.DataBind();
            }

            protected void gridEquipmentManufacturerContacts_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridEquipmentManufacturerContacts.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEquipmentManufacturerContacts(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridEquipmentManufacturerContacts.DataSource = SortDataTable(dataTable, false);
                gridEquipmentManufacturerContacts.DataBind();
            }

        #endregion

        #region Helper Methods

            private IEnumerable<GetEquipmentManufacturerContactsData> QueryEquipmentManufacturerContacts()
            {
                try
                {
                    //get all equipment manufacturers contacts
                    return DataMgr.EquipmentManufacturerContactDataMapper.GetAllEquipmentManufacturerContactsWithPartialData();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturer contacts.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturer contacts.", ex);
                    return null;
                }
            }

            private IEnumerable<GetEquipmentManufacturerContactsData> QuerySearchedEquipmentManufacturerContacts(string searchText)
            {
                try
                {
                    //get all equipment manufacturers contacts
                    return DataMgr.EquipmentManufacturerContactDataMapper.GetAllSearchedEquipmentManufacturerContactsWithPartialData(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturer contacts.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment manufacturer contacts.", ex);
                    return null;
                }
            }

            private void BindEquipmentManufacturerContacts()
            {
                //query equipment manufacturer contacts
                IEnumerable<GetEquipmentManufacturerContactsData> equipmentManufacturerContacts = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryEquipmentManufacturerContacts() : QuerySearchedEquipmentManufacturerContacts(txtSearch.Text);

                int count = equipmentManufacturerContacts.Count();

                gridEquipmentManufacturerContacts.DataSource = equipmentManufacturerContacts;

                // bind grid
                gridEquipmentManufacturerContacts.DataBind();

                SetGridCountLabel(count);

                //set search visibility. dont remove when last result from searched results in the grid is deleted.
                pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && count == 0) ? false : true;
            }

            //private void BindSearchedEquipmentManufacturerContacts(string[] searchText)
            //{
            //    //query equipment manufacturer contacts
            //    IEnumerable<GetEquipmentManufacturerContactsData> equipmentManufacturerContacts = QuerySearchedEquipmentManufacturerContacts(searchText);

            //    int count = equipmentManufacturerContacts.Count();

            //    gridEquipmentManufacturerContacts.DataSource = equipmentManufacturerContacts;

            //    // bind grid
            //    gridEquipmentManufacturerContacts.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} contact found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} contacts found.", count);
                }
                else
                {
                    lblResults.Text = "No contacts found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }

            private void UpdateEquipmentManufacturerContact(EquipmentManufacturerContact equipmentManufacturerContact)
            {
                DataMgr.EquipmentManufacturerContactDataMapper.UpdateContact(equipmentManufacturerContact);

                //re-update the hidden values, other if there is another edit, the subsequent changed value checks will be incorrect.
                hdnEditEmail.Value = equipmentManufacturerContact.Email;
                hdnEditManufacturer.Value = Convert.ToString(equipmentManufacturerContact.ManufacturerID);

                lblEditError.Text = updateSuccessful;
                lblEditError.Visible = true;
                lnkSetFocusView.Focus();

                //Bind Equipment Manufacturer Contacts again
                BindEquipmentManufacturerContacts();
            }
        
        #endregion
    }
}

