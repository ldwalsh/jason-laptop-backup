﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI.HtmlControls;
using System.Linq.Expressions;
using System.Diagnostics;

using CW.Data;
using CW.Utility;
using CW.Website._framework;
using CW.Reporting._fileservices;
using CW.Website._framework.httphandlers;
using CW.Reporting.Highstock;
using CW.Reporting.Highcharts;
using CW.Website._operations;

using Telerik.Web.UI;
using DotNet.Highcharts;
using DotNet.Highstock;
using DotNet.Highstock.Options;
using System.Globalization;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class Operations : SitePage
    {
        #region fields

        private SiteUser mSiteUser;
        private int uid;
        private int cid;
        private int roleID;
        private bool isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser;
        private CultureInfo cultureInfo;

        const string noClientImage = "no-client-image.png";        

        //public static Dictionary<string, string> Columns = new Dictionary<string, string>
        //    { 
        //        {"Point Name", "1"},
        //        {"Point Type", "2"},
        //        {"Current Value", "3"},
        //        {"Current Time", "4"},                
        //        {"Previous Value", "5"},                
        //        {"Previous Time", "6"},
        //        {"Min/Max", "7"},
        //        {"Eng. Unit", "8"},
        //        {"Past Ten", "9"},                                                
        //        {"Links", "10"},
        //    };
        //string[] selectedColumns = new string[] { "1", "3", "8" };

        #endregion

        #region Page Events

        private void Page_FirstInit()
        {
        }

        protected override void OnLoad(EventArgs e)
        {
            mSiteUser = siteUser;
            cultureInfo = CultureHelper.GetCultureInfo(siteUser.CultureName);

            //if the page is not a postback
            if (!Page.IsPostBack)
            {
                //set admin global settings
                //Make sure to decode from ascii to html 
                GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                litBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.OperationsBody);

                //set and bind appropriate fields/ddls
                SetClientInfoAndImages();
                BindDropdownLists();
                //BindColumns();

                if (!String.IsNullOrWhiteSpace(Request.QueryString["bid"]))
                {
                    ddlBuildings.SelectedValue = Request.QueryString["bid"];
                    ddlBuildings_SelectedIndexChanged(null, null);

                    if (!String.IsNullOrWhiteSpace(Request.QueryString["ecid"]))
                    {
                        ddlEquipmentClass.SelectedValue = Request.QueryString["ecid"];
                        ddlEquipmentClass_SelectedIndexChanged(null, null);

                        if (!String.IsNullOrWhiteSpace(Request.QueryString["eid"]))
                        {
                            ddlEquipment.SelectedValue = Request.QueryString["eid"];

                            generateButton_Click(null, null);
                        }
                    }
                }
            }

            //set global info
            uid = mSiteUser.UID;
            roleID = mSiteUser.RoleID;
            cid = mSiteUser.CID;
            isKGSFullAdminOrHigher = mSiteUser.IsKGSFullAdminOrHigher;
            isSuperAdminOrFullAdminOrFullUser = mSiteUser.IsSuperAdminOrFullAdminOrFullUser;

            ScriptManager.RegisterStartupScript(radUpdatePanelNested, radUpdatePanelNested.GetType(), "autoUpdate", "autoUpdate();", true);
        }

        #endregion

        #region Bind Methods

        private void BindDropdownLists()
        {
            BindBuildingDropdownList();
            BindEquipmentClasses();
            BindEquipmentTypes();
            BindEquipment();
        }
       

        private void BindBuildingDropdownList()
        {
            ddlBuildings.Items.Clear();
            ddlBuildings.Items.Add(new ListItem{ Text = "View all", Value = "0", });
            ddlBuildings.DataTextField = "BuildingName";
            ddlBuildings.DataValueField = "BID";
            ddlBuildings.DataSource = mSiteUser.VisibleBuildings.OrderBy(b => b.BuildingName);
            ddlBuildings.DataBind();  
          
            divEquipment.Visible = false;
        }

        private void BindEquipmentClasses(int bid = 0)
        {
            ddlEquipmentClass.Items.Clear();
            ddlEquipmentClass.Items.Add(new ListItem{ Text = "Select one...", Value = "-1", });
            ddlEquipmentClass.DataTextField = "EquipmentClassName";
            ddlEquipmentClass.DataValueField = "EquipmentClassID";
            ddlEquipmentClass.DataSource = (bid != 0) ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(bid) : DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses();
            ddlEquipmentClass.DataBind();
        }

        private void BindEquipmentTypes(int bid = 0, int ecid = -1)
        {
            ddlEquipmentType.Items.Clear();            
            ddlEquipmentType.Items.Add(new ListItem{ Text = "Select one...", Value = "-1", });

            if (ecid != -1)
            {
                ddlEquipmentType.DataTextField = "EquipmentTypeName";
                ddlEquipmentType.DataValueField = "EquipmentTypeID";
                ddlEquipmentType.DataSource = (ecid != -1 && bid != 0) ? DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByBuildingIDAndEquipmentClassID(bid, ecid) : DataMgr.EquipmentTypeDataMapper.GetAllEquipmentTypesByEquipmentClassID(ecid);
            }

            ddlEquipmentType.DataBind();
        }


        private void BindEquipment(int bid = 0, int etid = -1)
        {       
            ddlEquipment.Items.Clear();
            ddlEquipment.Items.Add(new ListItem{ Text = "View all", Value = "0", });

            if (bid != 0 && etid != -1)
            {
                ddlEquipment.DataTextField = "EquipmentName";
                ddlEquipment.DataValueField = "EID";
                ddlEquipment.DataSource = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentTypeID(bid, etid);
            }

            ddlEquipment.DataBind();
        }

        //private void BindColumns()
        //{
        //    lbColumns.DataSource = Columns;
        //    lbColumns.DataTextField = "Key";
        //    lbColumns.DataValueField = "Value";
        //    lbColumns.DataBind();

        //    foreach(ListItem i in lbColumns.Items)
        //    {
        //        if (selectedColumns.Contains(i.Value))
        //            i.Selected = true;
        //    }
        //}

        #endregion

        #region DropDownList Events

        protected void ddlBuildings_SelectedIndexChanged(object sender, EventArgs e)
        {
            var bid = Convert.ToInt32(ddlBuildings.SelectedValue);
            divEquipment.Visible = (bid == 0) ? false : true;

            BindEquipmentClasses(bid);
            BindEquipment(bid, Convert.ToInt32(ddlEquipmentClass.SelectedValue));
        }

        protected void ddlEquipmentClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            var bid = Convert.ToInt32(ddlBuildings.SelectedValue);

            BindEquipmentTypes(bid, Convert.ToInt32(ddlEquipmentClass.SelectedValue));
            BindEquipment(bid, Convert.ToInt32(ddlEquipmentClass.SelectedValue));
        }

        protected void ddlEquipmentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindEquipment(Convert.ToInt32(ddlBuildings.SelectedValue), Convert.ToInt32(ddlEquipmentType.SelectedValue));
        }

        #endregion

        #region Button Events

        /// <summary>
        /// Generate button click
        /// </summary>
        protected void generateButton_Click(object sender, EventArgs e)
        {            
            int bid = Convert.ToInt32(ddlBuildings.SelectedValue);
            int ecid = Convert.ToInt32(ddlEquipmentClass.SelectedValue);
            int etid = Convert.ToInt32(ddlEquipmentType.SelectedValue); 
            int eid = Convert.ToInt32(ddlEquipment.SelectedValue);

            GenerateTablesAndCharts(bid, ecid, etid, eid);
        }

        protected void btnDownloadButton_Click(object sender, EventArgs e)
        {
            EssentialObjectsHelper eo = new EssentialObjectsHelper("hdn_container;", LogMgr);

            eo.CreatePdfHeader(siteUser.IsSchneiderTheme, .7);
            eo.CreatePdfFooter(siteUser.IsSchneiderTheme, "Operations Report", .7);

            Byte[] pdfBytes = eo.ConvertHtmlAndReturnBytes(Server.HtmlDecode(hdn_container.Value));

            Boolean result;

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    new ModulePDFGenerator(pdfBytes),
                    LogMgr
                ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productOperations.pdf", true));

            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                return;
            }

            eo.RemoveTempFiles();

            if (result) return;
        }

        #endregion

        #region Chart Methods

        /// <summary>
        /// Generated the tables and charts for all equipment selected.
        /// </summary>
        protected void GenerateTablesAndCharts(int bid, int ecid, int etid, int eid)
        {
                List<PointClass> pointClasses = new List<PointClass>();
                List<PointType> pointTypes = new List<PointType>();
                IEnumerable<Equipment> equipment = Enumerable.Empty<Equipment>();
                Int32[] bids = new Int32[]{};
                var buildings = siteUser.VisibleBuildings;

                DateTime endDate = DateTime.UtcNow;
                DateTime startDate = chkGraphs.Checked ? endDate.AddDays(-1) : endDate.AddHours(-1);
                DateTime startHour = endDate.AddHours(-1);
                Stopwatch stopwatch = new Stopwatch();
                litChart.Text = "";
                litPerf.Text = "";
                phTables.Controls.Clear();
                //TODO column enum
                var columns = lbColumns.Items.Cast<ListItem>().Where(item => item.Selected);
      

                stopwatch.Start();

                if(bid == 0)
                {
                    bids = buildings.Select(b => b.BID).ToArray();
                    equipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBIDsAndEquipmentTypeID(bids, etid, true).OrderBy(e => e.Building.BuildingName).ThenBy(e => e.EquipmentName);                    
                }
                else
                {
                    bids = new Int32[] { bid };
                    equipment = (eid == 0) ? DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentByBuildingIDAndEquipmentTypeID(bid, etid) : new List<Equipment>{ DataMgr.EquipmentDataMapper.GetEquipmentByID(eid) };
                }

                stopwatch.Stop();
                litPerf.Text += "GetEquipment: " + stopwatch.Elapsed.ToString() + "</br>";

                stopwatch.Restart();

                var points = (eid == 0) ? DataMgr.PointDataMapper.GetAllVisiblePointsByBIDsAndEquipmentTypeID(bids, etid, new Expression<Func<CW.Data.Point, Object>>[] { (p => p.PointType), (p => p.Equipment_Points) }, new Expression<Func<PointType, Object>>[] { (pt => pt.PointClass) }, new Expression<Func<Equipment_Point, Object>>[] { (e_p => e_p.Equipment) }).ToList() : DataMgr.PointDataMapper.GetAllVisiblePointsByEID(eid, new Expression<Func<CW.Data.Point, Object>>[] { (p => p.PointType) }, new Expression<Func<PointType, Object>>[] { (pt => pt.PointClass) }).ToList();
                
                stopwatch.Stop();
                litPerf.Text += "GetAllVisiblePointsByBIDAndEquipmentClassID: " + stopwatch.Elapsed.ToString() + "</br>";

                if (points.Any())
                {
                    stopwatch.Restart();

                    var results = DataMgr.RawDataMapper.GetConvertedRawPlotDataForPoints(points, startDate, endDate, false);

                    stopwatch.Stop();
                    litPerf.Text += "GetConvertedRawPlotDataForPoints: " + stopwatch.Elapsed.ToString() + "</br>";
       
                    if (results.Any())
                    {
                        if (chkGraphs.Checked)
                        {
                            stopwatch.Restart();

                            if (bid == 0 || eid == 0)
                            {
                                pointTypes = points.Select(pt => pt.PointType).Distinct().ToList();
                            }
                            else
                            {
                                pointClasses = points.Select(pc => pc.PointType.PointClass).Distinct().ToList();
                            }

                            //STOCK CHARTS------
                            if (pointClasses.Any())
                            {
                                //BY POINT CLASS
                                foreach (PointClass pc in pointClasses)
                                {
                                    var currentPids = points.Where(p => p.PointType.PointClassID == pc.PointClassID).Select(p => p.PID).ToList();

                                    Highstock stockChart = new StockChart(pc.PointClassName, cultureInfo, 280, null, null, pc.PointClassName + " Points").stockChart;
                                    stockChart.SetRangeSelector(new RangeSelector { Enabled = false, });
                                    stockChart.SetExporting(new Exporting { Enabled = false, });
                                    stockChart.SetNavigator(new Navigator { Height = 24, Margin = 6, });

                                    List<Series> seriesList = new List<Series>();

                                    foreach (var pid in currentPids)
                                    {
                                        List<DotNet.Highstock.Options.Point> dataPoints = new List<DotNet.Highstock.Options.Point>();

                                        var rawData = results.Where(r => r.PID == pid).OrderBy(d => d.DateLoggedLocal).ToArray();

                                        var pointName = points.Where(p => p.PID == pid).First().PointName;

                                        //get eng unit from first value
                                        var engUnit = (rawData.Any() ? rawData.First().BuildingSettingBasedEngUnits : "No Data");

                                        //set series name as point name                                                    
                                        var series = new DotNet.Highstock.Options.Series() { Name = pointName + " (" + engUnit + ")" };


                                        //MAYBE quicker itteration?
                                        //var pointArray = rawData.Select(r => new DotNet.Highstock.Options.Point { X = DotNet.Highstock.Helpers.Tools.GetTotalMilliseconds(r.DateLoggedLocal), Y = r.ConvertedRawValue }).ToArray();

                                        //CANT CAST
                                        //var o = rawData.Select(r => new ObjectPoints { dateTime = DotNet.Highstock.Helpers.Tools.GetTotalMilliseconds(r.DateLoggedLocal), value = r.ConvertedRawValue }).Cast<object[,]>().ToArray<object[,]>();
                                        //DotNet.Highstock.Helpers.Data data = new DotNet.Highstock.Helpers.Data(o);


                                        //Fastest?
                                        object[,] pointArray = new object[rawData.Count(), 2];
                                        int i = 0;
                                        foreach (var r in rawData)
                                        {
                                            //highcharts will either convert to utc, or convert based on the server timezone. need to set both highcharts to use utc rather then the server timezone, and then pretend our datetimes are already in utc.
                                            var mockUTCDateTimeTotalMilliseconds = DotNet.Highstock.Helpers.Tools.GetTotalMilliseconds(DateTime.SpecifyKind(r.DateLoggedLocal, DateTimeKind.Utc));

                                            pointArray[i, 0] = mockUTCDateTimeTotalMilliseconds;
                                            pointArray[i, 1] = r.ConvertedRawValue;
                                            i++;
                                        }


                                        DotNet.Highstock.Helpers.Data data = new DotNet.Highstock.Helpers.Data(pointArray);

                                        series.Data = data;

                                        seriesList.Add(series);
                                    }

                                    stockChart.SetSeries(seriesList.ToArray());

                                    litChart.Text += "<hr /><div style='page-break-inside: avoid;'>" + stockChart.ChartContainerHtmlString().ToString() + "</div>";

                                    //remove script tags for non dangerous requiest and reregistering later
                                    //hdnChart.Value += stockChart.ChartScriptHtmlString().ToString().Replace("<script type='text/javascript'>", "").Replace("</script>", "").Replace("turboThreshold", "dataGrouping: { enabled: false }, turboThreshold");

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), pc.PointClassName, stockChart.ChartScriptHtmlString().ToString().Replace("turboThreshold", "dataGrouping: { enabled: false }, turboThreshold"), false);
                                }
                            }
                            else
                            {
                                //Pointtypes 
                                foreach (PointType pt in pointTypes)
                                {
                                    var currentPids = points.Where(p => p.PointTypeID == pt.PointTypeID).Select(p => p.PID).ToList();
                                    var pointTypeNameTrimmmed = StringHelper.RemoveSpecialCharacters(pt.PointTypeName);

                                    Highstock stockChart = new StockChart(pointTypeNameTrimmmed, cultureInfo, 280, null, null, pt.PointTypeName + " Points").stockChart;
                                    stockChart.SetRangeSelector(new RangeSelector { Enabled = false, });
                                    stockChart.SetExporting(new Exporting { Enabled = false, });
                                    stockChart.SetNavigator(new Navigator { Height = 24, Margin = 6, });

                                    List<Series> seriesList = new List<Series>();

                                    foreach (var pid in currentPids)
                                    {
                                        List<DotNet.Highstock.Options.Point> dataPoints = new List<DotNet.Highstock.Options.Point>();

                                        var rawData = results.Where(r => r.PID == pid).OrderBy(d => d.DateLoggedLocal).ToArray();
                                        var point = points.Where(p => p.PID == pid).First();

                                        //get eng unit from first value
                                        var engUnit = (rawData.Any() ? rawData.First().BuildingSettingBasedEngUnits : "No Data");

                                        //set series name as point name                                                    
                                        var series = new DotNet.Highstock.Options.Series() { Name = point.Equipment_Points.First().Equipment.EquipmentName + " - " + point.PointName + " (" + engUnit + ")" };


                                        //MAYBE quicker itteration?
                                        //var pointArray = rawData.Select(r => new DotNet.Highstock.Options.Point { X = DotNet.Highstock.Helpers.Tools.GetTotalMilliseconds(r.DateLoggedLocal), Y = r.ConvertedRawValue }).ToArray();

                                        //CANT CAST
                                        //var o = rawData.Select(r => new ObjectPoints { dateTime = DotNet.Highstock.Helpers.Tools.GetTotalMilliseconds(r.DateLoggedLocal), value = r.ConvertedRawValue }).Cast<object[,]>().ToArray<object[,]>();
                                        //DotNet.Highstock.Helpers.Data data = new DotNet.Highstock.Helpers.Data(o);


                                        //Fastest?
                                        object[,] pointArray = new object[rawData.Count(), 2];
                                        int i = 0;
                                        foreach (var r in rawData)
                                        {
                                            //highcharts will either convert to utc, or convert based on the server timezone. need to set both highcharts to use utc rather then the server timezone, and then pretend our datetimes are already in utc.
                                            var mockUTCDateTimeTotalMilliseconds = DotNet.Highstock.Helpers.Tools.GetTotalMilliseconds(DateTime.SpecifyKind(r.DateLoggedLocal, DateTimeKind.Utc));

                                            pointArray[i, 0] = mockUTCDateTimeTotalMilliseconds;
                                            pointArray[i, 1] = r.ConvertedRawValue;
                                            i++;
                                        }


                                        DotNet.Highstock.Helpers.Data data = new DotNet.Highstock.Helpers.Data(pointArray);

                                        series.Data = data;

                                        seriesList.Add(series);
                                    }

                                    stockChart.SetSeries(seriesList.ToArray());

                                    litChart.Text += "<hr /><div style='page-break-inside: avoid;'>" + stockChart.ChartContainerHtmlString().ToString() + "</div>";                                    

                                    //remove script tags for non dangerous requiest and reregistering later
                                    //hdnChart.Value += stockChart.ChartScriptHtmlString().ToString().Replace("<script type='text/javascript'>", "").Replace("</script>", "").Replace("turboThreshold", "dataGrouping: { enabled: false }, turboThreshold");

                                    ScriptManager.RegisterStartupScript(this, this.GetType(), pointTypeNameTrimmmed, stockChart.ChartScriptHtmlString().ToString().Replace("turboThreshold", "dataGrouping: { enabled: false }, turboThreshold"), false);
                                }
                            }

                            stopwatch.Stop();
                            litPerf.Text += "StockWithoutClassDataSet: " + stopwatch.Elapsed.ToString() + "</br>";                            
                        }

                        stopwatch.Restart();

                        //TABLE AND SPARK CHARTS-----     
                        foreach (var equip in equipment)
                        {
                            string equipmentNameTrimmed =  StringHelper.RemoveSpecialCharacters(equip.EquipmentName);
                            TableBuilder tb = new TableBuilder("dashboardTable");

                            //set header columns
                            TableHeaderRow thr = new TableHeaderRow();
                            tb.AddHeaderCell(thr, buildings.Where(b => b.BID == equip.BID).First().BuildingName + " - " + equip.EquipmentName, "dashboardTableTitle", columns.Count());
                            tb.AddRow(thr);

                            thr = new TableHeaderRow();
                            foreach (var c in columns)
                            {
                                tb.AddHeaderCell(thr, c.Text, c.Value == "9" ? "sparkColumn" : "");
                            }
                            tb.AddRow(thr);

                            var currentPids = (eid == 0) ? points.Where(p => p.Equipment_Points.Select(ep => ep.EID).Contains(equip.EID)).Select(p => p.PID).ToList() : points.Select(p => p.PID).ToList();

                            foreach (var pid in currentPids)
                            {
                                TableRow tr = new TableRow();
                                var point = points.Where(p => p.PID == pid).First();

                                //filter down raw data to past hour if chkGraphs were include and we pull past 24 hours.
                                var pastHourRawData = chkGraphs.Checked ? results.Where(r => r.PID == pid  && r.DateLoggedUTC >= startHour && r.DateLoggedUTC <= endDate).OrderByDescending(d => d.DateLoggedLocal).ToArray() : results.Where(r => r.PID == pid).OrderByDescending(d => d.DateLoggedLocal).ToArray();
                                
                                bool hasEnoughRawData = pastHourRawData.Count() >= 2;
                                string pointName = point.PointName;
                                string pointNameTrimmed = StringHelper.RemoveSpecialCharacters(pointName);  //invalid charts in chartname and scripts
                                
                                
                                //TableCell t = new TableCell { Text = StringHelper.ParseIntArrayToCSVString(point.Equipment_Points.Select(e => e.EID).ToArray()) };
                                //tr.Cells.Add(t);

                                if (columns.Where(c => c.Value == "1").Any())
                                    tb.AddCell(tr, "<strong>" + pointName + "</strong>");
                                //t = new TableCell { Text = "<strong>" + pointName + "</strong><br />(" + point.PointType.DisplayName + ")" };
                                //tr.Cells.Add(t);

                                if (columns.Where(c => c.Value == "2").Any())
                                    tb.AddCell(tr, point.PointType.DisplayName);
                                

                                if (hasEnoughRawData)
                                {
                                    var current = pastHourRawData.First();
                                    var last = pastHourRawData[1];
                                    var max = pastHourRawData.Max(r => r.ConvertedRawValue);
                                    var min = pastHourRawData.Min(r => r.ConvertedRawValue);
                                    var pastTen = pastHourRawData.Take(10).OrderBy(d => d.DateLoggedLocal);

                                    if (columns.Where(c => c.Value == "3").Any())
                                        tb.AddCell(tr, "<strong><span style='color: darkcyan'> " + current.ConvertedRawValue.ToString() + "</span></strong>");
                                    //t = new TableCell { Text = "val:<strong><span style='color: darkcyan'> " + current.ConvertedRawValue.ToString() + "</span></strong><br />time: " + current.DateLoggedLocal.ToString() };
                                    //tr.Cells.Add(t);

                                    if (columns.Where(c => c.Value == "4").Any())
                                        tb.AddCell(tr, current.DateLoggedLocal.ToString());

                                    if (columns.Where(c => c.Value == "5").Any())
                                        tb.AddCell(tr, last.ConvertedRawValue.ToString());
                                    //t = new TableCell { Text = "val: " + last.ConvertedRawValue.ToString() + "<br />time: " + last.DateLoggedLocal.ToString() };
                                    //tr.Cells.Add(t);

                                    if (columns.Where(c => c.Value == "6").Any())
                                        tb.AddCell(tr, last.DateLoggedLocal.ToString());
                                    

                                    if (columns.Where(c => c.Value == "7").Any())
                                        tb.AddCell(tr, "min: " + min.ToString() + "<br />max: " + max.ToString());
                                    //t = new TableCell { Text = "min: " + min.ToString() + "<br />max: " + max.ToString() };
                                    //tr.Cells.Add(t);      

                                    if (columns.Where(c => c.Value == "8").Any())
                                    {                                     
                                        //get eng unit from first value
                                        var engUnit = (pastHourRawData.Any() ? pastHourRawData.First().BuildingSettingBasedEngUnits : String.Empty);
                                        tb.AddCell(tr, engUnit);
                                        //tr.Cells.Add(new TableCell { Text = engUnit });
                                    }

                                    if (columns.Where(c => c.Value == "9").Any())
                                    {
                                        string ranAlpha = StringHelper.GenerateRandomAlphaString(8);

                                        //Past Ten-------
                                        Highcharts sparkChart = new SparkChart("spark" + equipmentNameTrimmed + pointNameTrimmed + "PastTen" + ranAlpha, cultureInfo).sparkChart;

                                        List<DotNet.Highcharts.Options.Point> dataPoints = new List<DotNet.Highcharts.Options.Point>();
                             
                                        var series = new DotNet.Highcharts.Options.Series() { Name = "" };

                                        object[,] pointArray = new object[pastTen.Count(), 2];
                                        int i = 0;

                                        foreach (var r in pastTen)
                                        {
                                            //highcharts will either convert to utc, or convert based on the server timezone. need to set both highcharts to use utc rather then the server timezone, and then pretend our datetimes are already in utc.
                                            var mockUTCDateTimeTotalMilliseconds = DotNet.Highstock.Helpers.Tools.GetTotalMilliseconds(DateTime.SpecifyKind(r.DateLoggedLocal, DateTimeKind.Utc));

                                            pointArray[i, 0] = mockUTCDateTimeTotalMilliseconds;
                                            pointArray[i, 1] = r.ConvertedRawValue;
                                            i++;
                                        }

                                        DotNet.Highcharts.Helpers.Data data = new DotNet.Highcharts.Helpers.Data(pointArray);

                                        series.Data = data;

                                        sparkChart.SetSeries(series);

                                        tb.AddCell(tr, sparkChart.ChartContainerHtmlString().ToString(), "sparkColumn");
                                        //tr.Cells.Add(new TableCell { CssClass = "sparkColumn", Text = sparkChart.ChartContainerHtmlString().ToString() });

                                        //remove script tags for non dangerous requiest and reregistering later
                                        //hdnChart.Value += sparkChart.ChartScriptHtmlString().ToString().Replace("<script type='text/javascript'>", "").Replace("</script>", "").Replace("turboThreshold", "dataGrouping: { enabled: false }, turboThreshold");

                                        ScriptManager.RegisterStartupScript(this, this.GetType(), equipmentNameTrimmed + pointNameTrimmed + "PastTen" + ranAlpha, sparkChart.ChartScriptHtmlString().ToString().Replace("turboThreshold", "dataGrouping: { enabled: false }, turboThreshold"), false);
                                    }                                                                
                                }
                                else
                                {
                                    if (columns.Where(c => c.Value == "3").Any()) tb.AddCell(tr, "");
                                    if (columns.Where(c => c.Value == "4").Any()) tb.AddCell(tr, "");
                                    if (columns.Where(c => c.Value == "5").Any()) tb.AddCell(tr, "");
                                    if (columns.Where(c => c.Value == "6").Any()) tb.AddCell(tr, "");
                                    if (columns.Where(c => c.Value == "7").Any()) tb.AddCell(tr, "");
                                    if (columns.Where(c => c.Value == "8").Any()) tb.AddCell(tr, "");
                                    if (columns.Where(c => c.Value == "9").Any()) tb.AddCell(tr, "");
                                }

                                if (columns.Where(c => c.Value == "10").Any())
                                {
                                    List<Control> ctrls = new List<Control>();

                                    HtmlAnchor a = new HtmlAnchor { HRef = LinkHelper.BuildAnalysisBuilderQuickLink(equip.BID, ecid, equip.EID), Title = "plot raw data", Target = "_blank", InnerText = "plot data" };
                                    a.Attributes.Add("class", "dockLink");
                                    ctrls.Add(a);

                                    a = new HtmlAnchor { HRef = LinkHelper.BuildEquipmentProfileQuickLink(equip.EID), Title = "view equipment profile", Target = "_blank", InnerText = "view profile" };
                                    a.Attributes.Add("class", "dockLink");
                                    ctrls.Add(a);

                                    tb.AddCell(tr, "", "", ctrls);
                                }

                                //Add Row----
                                tb.AddRow(tr);
                                //tblSparks.Rows.Add(tr);
                            }

                            phTables.Controls.Add(tb.table);
                        }

                        stopwatch.Stop();
                        litPerf.Text += "Spark: " + stopwatch.Elapsed.ToString() + "</br>";


                        pnlMessage.Visible = false;
                        pnlDashboard.Visible = true;
                        imgPdfDownload.Enabled = true;
                    }
                    else
                    {
                        imgPdfDownload.Enabled = false;
                        pnlMessage.Visible = true;
                        pnlDashboard.Visible = false;
                        LabelHelper.SetLabelMessage(lblMessage, "No data exists.");
                    }
                }
                else
                {
                    imgPdfDownload.Enabled = false;
                    pnlMessage.Visible = true;
                    pnlDashboard.Visible = false;
                    LabelHelper.SetLabelMessage(lblMessage, "No points exist.");
                }    
            }


        #endregion

        #region Helper Methods

        private void SetClientInfoAndImages()
        {
            var client = DataMgr.ClientDataMapper.GetClient(mSiteUser.CID);

            if (client == null) return;

            imgHeaderClientImage.AlternateText = mSiteUser.ClientName;
            imgHeaderClientImage.Visible = true;
            imgHeaderClientImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;

            if (!String.IsNullOrEmpty(client.ImageExtension))
                imgHeaderClientImage.ImageUrl = HandlerHelper.ClientImageUrl(client.CID, client.ImageExtension);

            lblHeaderClientName.InnerText = mSiteUser.ClientName;
        }

        #endregion

    }
}