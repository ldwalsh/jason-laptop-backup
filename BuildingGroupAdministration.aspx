﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="BuildingGroupAdministration.aspx.cs" Inherits="CW.Website.BuildingGroupAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                      	                  	
            	<h1>Building Group Administration</h1>                        
                <div class="richText">
                    <asp:Literal ID="litAdminBuildingGroupsBody" runat="server"></asp:Literal> 
                </div>                                 
                <div class="updateProgressDiv">
                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  
               </div> 
               <div class="administrationControls">
                   
                    <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnLoad="loadTabs" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Building Groups"></telerik:RadTab>
                            <telerik:RadTab Text="Add Building Group"></telerik:RadTab>
                        </Tabs>
                    </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                        <telerik:RadPageView ID="RadPageView1" runat="server">
                                <h2>View Building Groups</h2> 
                                <p>
                                        <a id="lnkSetFocusView" href="#" runat="server"></a>
                                        <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                            
                                </p>           

                                <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch" Visible="false">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                </asp:Panel>  
                      
                                <div id="gridTbl">                                        
                                       <asp:GridView 
                                         ID="gridBuildingGroups"   
                                         EnableViewState="true"           
                                         runat="server"  
                                         DataKeyNames="BuildingGroupID" 
                                         GridLines="None"                                           
                                         PageSize="20" PagerSettings-PageButtonCount="20"
                                         OnRowCreated="gridBuildingGroups_OnRowCreated" 
                                         AllowPaging="true"  OnPageIndexChanging="gridBuildingGroups_PageIndexChanging"
                                         AllowSorting="true"  OnSorting="gridBuildingGroups_Sorting"                                                                                                               
                                         OnSelectedIndexChanged="gridBuildingGroups_OnSelectedIndexChanged"   
                                         OnRowEditing="gridBuildingGroups_Editing"    
                                         OnRowDeleting="gridBuildingGroups_Deleting"                                                                                                                                                                                                            
                                         OnDataBound="gridBuildingGroups_OnDataBound"
                                         AutoGenerateColumns="false"                                              
                                         HeaderStyle-CssClass="tblTitle" 
                                         RowStyle-CssClass="tblCol1"
                                         AlternatingRowStyle-CssClass="tblCol2" 
                                         RowStyle-Wrap="true"                                                                                      
                                         > 
                                         <Columns> 
                                            <asp:CommandField ShowSelectButton="true" />
                                            <asp:TemplateField>
                                                <ItemTemplate>                                                
                                                     <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-Wrap="true" SortExpression="BuildingGroupName" HeaderText="Building Group Name">  
                                                <ItemTemplate><%# StringHelper.TrimText(Eval("BuildingGroupName").ToString(), 40)%></ItemTemplate>                                    
                                            </asp:TemplateField>   
                                            <asp:TemplateField HeaderText="Buildings">  
                                                <ItemTemplate><asp:Literal runat="server"></asp:Literal></ItemTemplate>                                
                                            </asp:TemplateField>   
                                            <asp:TemplateField>
                                                <ItemTemplate>                                                
                                                     <asp:LinkButton Runat="server" CausesValidation="false"
                                                           OnClientClick="return confirm('Are you sure you wish to delete this building group permanently?');"
                                                           CommandName="Delete">Delete</asp:LinkButton>  
                                                </ItemTemplate>
                                            </asp:TemplateField>                                                                                                                                                                                                             
                                         </Columns>        
                                     </asp:GridView> 
                     
                                    </div>                                    
                                    <br /><br /> 
                                    <div>                                                            
                                    <!--SELECT BUILDING GROUP DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvBuildingGroup" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Building Group Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Building Group Name: </strong>" + Eval("BuildingGroupName") + "</li>"%>                                                             
                                                        </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    
                                   </div>
                                   
                                   <!--EDIT BUILDING PANEL -->                              
                                   <asp:Panel ID="pnlEditBuildingGroup" runat="server" Visible="false" DefaultButton="btnUpdateBuildingGroup">                                                                                             
                                              <div>
                                                    <h2>Edit Building Group</h2>
                                              </div>  
                                              <div>                                                    
                                                    <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                                    <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                                    <div class="divForm">
                                                        <label class="label">*Building Group Name:</label>
                                                        <asp:TextBox ID="txtEditBuildingGroupName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                                    </div>                                                  
                                                    <div class="divForm">                                                
                                                            <label class="label">Available:</label>
                                                            <asp:ListBox ID="lbEditTop" CssClass="listbox" runat="server" SelectionMode="Multiple">
                                                            </asp:ListBox>
                                                            <div class="divArrows">
                                                            <asp:ImageButton CssClass="up-arrow"  ID="btnUp" ImageUrl="_assets/images/up-arrow.png" runat="server" OnClick="btnEditBuildingsUpButton_Click"></asp:ImageButton>
                                                            <asp:ImageButton CssClass="down-arrow" ID="btnDown" ImageUrl="_assets/images/down-arrow.png" runat="server" OnClick="btnEditBuildingsDownButton_Click"></asp:ImageButton>                                                  
                                                            </div>
                                                            <label class="label">Assigned:</label>
                                                            <asp:ListBox ID="lbEditBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">
                                                            </asp:ListBox>                                                                                                           
                                                    </div>                                   
                                                                                                                                                      
                                                    <asp:LinkButton CssClass="lnkButton" ID="btnUpdateBuildingGroup" runat="server" Text="Update Group"  OnClick="updateBuildingGroupButton_Click" ValidationGroup="EditBuildingGroup"></asp:LinkButton>   
                                                </div>
                                                
                                                <!--Ajax Validators-->      
                                                <asp:RequiredFieldValidator ID="editBuildingGroupNameRequiredValidator" runat="server"
                                                    ErrorMessage="Building Group Name is a required field."
                                                    ControlToValidate="txtEditBuildingGroupName"          
                                                    SetFocusOnError="true"
                                                    Display="None"
                                                    ValidationGroup="EditBuildingGroup">
                                                    </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="editBuildingGroupNameRequiredValidatorExtender" runat="server"
                                                    BehaviorID="editBuildingGroupNameRequiredValidatorExtender"
                                                    TargetControlID="editBuildingGroupNameRequiredValidator"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    Width="175">
                                                    </ajaxToolkit:ValidatorCalloutExtender>                                             
                                    </asp:Panel>
                                                                                     
                            </telerik:RadPageView>
                        <telerik:RadPageView ID="RadPageView2" runat="server">
                                    <asp:Panel ID="pnlAddBuildingGroup" runat="server" DefaultButton="btnAddBuildingGroup">
                                    <h2>Add Building Group</h2> 
                                    <div>             
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                       
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Building Group Name:</label>
                                            <asp:TextBox ID="txtAddBuildingGroupName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                                    
                                        </div>
                                        <div class="divForm">
                                                <label class="label">Buildings Available:</label>
                                                <asp:ListBox ID="lbAddTop" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox>
                                                <div class="divArrows">
                                                <asp:ImageButton CssClass="up-arrow"  ID="btnAddUp"  OnClick="btnAddBuildingsUpButton_Click" ImageUrl="_assets/images/up-arrow.png" runat="server"></asp:ImageButton>
                                                <asp:ImageButton CssClass="down-arrow" ID="btnAddDown"  OnClick="btnAddBuildingsDownButton_Click" ImageUrl="_assets/images/down-arrow.png" runat="server"></asp:ImageButton>                                                    
                                                </div>
                                                <label class="label">Buildings Assigned:</label>
                                                <asp:ListBox ID="lbAddBottom" CssClass="listbox" runat="server" SelectionMode="Multiple">                                                        
                                                </asp:ListBox> 
                                        </div>                                                                                              
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddBuildingGroup" runat="server" Text="Add Building Group"  OnClick="addBuildingGroupButton_Click" ValidationGroup="AddBuildingGroup"></asp:LinkButton>                                                                                   
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="buildingGroupNameRequiredValidator" runat="server"
                                        ErrorMessage="Building Group is a required field."
                                        ControlToValidate="txtAddBuildingGroupName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddBuildingGroup">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="buildingGroupNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="buildingGroupNameRequiredValidatorExtender"
                                        TargetControlID="buildingGroupNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                    </asp:Panel>                                                                                                                                                                                                                                                              
                        </telerik:RadPageView>
                    </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>
                    
                  
