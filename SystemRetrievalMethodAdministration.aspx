﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemRetrievalMethodAdministration.aspx.cs" Inherits="CW.Website.SystemRetrievalMethodAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>System Retrieval Method Administration</h1>                        
                <div class="richText">The system retrieval method administration area is used to view, add, and edit retrieval methods.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Retrieval Methods"></telerik:RadTab>
                            <telerik:RadTab Text="Add Retrieval Method"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Retrieval Methods</h2>
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p>       
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>      
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridRetrievalMethods"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="RetrievalMethodID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridRetrievalMethods_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridRetrievalMethods_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridRetrievalMethods_Sorting"  
                                             OnSelectedIndexChanged="gridRetrievalMethods_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridRetrievalMethods_Editing"                                                                                                                                                                                   
                                             OnRowDeleting="gridRetrievalMethods_Deleting"
                                             OnDataBound="gridRetrievalMethods_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="RetrievalMethod" HeaderText="Retrieval Method">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("RetrievalMethod"),50) %></ItemTemplate>      
                                                </asp:TemplateField>       
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Description">  
                                                    <ItemTemplate><%# Eval("RetrievalMethodDescription") %></ItemTemplate>                                              
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>                                        
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this retrieval method permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT RETRIEVAL METHOD DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvRetrievalMethod" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Retrieval Method Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Retrieval Method: </strong>" + Eval("RetrievalMethod") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("RetrievalMethodDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("RetrievalMethodDescription") + "</li>"%>
                                                            <%# "<li><strong>Active: </strong>" + Eval("IsActive") + "</li>"%>  
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT RETRIEVAL METHOD PANEL -->                              
                                    <asp:Panel ID="pnlEditRetrievalMethod" runat="server" Visible="false" DefaultButton="btnUpdateRetrievalMethod">                                                                                             
                                        <div>
                                            <h2>Edit Retrieval Method</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Retrieval Method:</label>
                                                <asp:TextBox ID="txtEditRetrievalMethod" MaxLength="50" runat="server"></asp:TextBox>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>   
                                            <div class="divForm">
                                                <label class="label">*Active:</label>
                                                <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                    
                                            </div>                                                                                                
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateRetrievalMethod" runat="server" Text="Update Method"  OnClick="updateRetrievalMethodButton_Click" ValidationGroup="EditRetrievalMethod"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editRetrievalMethodRequiredValidator" runat="server"
                                        ErrorMessage="Retrieval Method is a required field."
                                        ControlToValidate="txtEditRetrievalMethod"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditRetrievalMethod">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editRetrievalMethodRequiredValidatorExtender" runat="server"
                                        BehaviorID="editRetrievalMethodRequiredValidatorExtender"
                                        TargetControlID="editRetrievalMethodRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>                                                                              
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server"> 
                                <asp:Panel ID="pnlAddRetrievalMethod" runat="server" DefaultButton="btnAddRetrievalMethod"> 
                                    <h2>Add Retrieval Method</h2>
                                    <div>             
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                       
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Retrieval Method:</label>
                                            <asp:TextBox ID="txtAddRetrievalMethod" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div> 
                                        <div class="divForm">
                                             <label class="label">Description:</label>
                                             <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                             <div id="divAddDescriptionCharInfo"></div>
                                        </div>                                                                                        
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddRetrievalMethod" runat="server" Text="Add Method"  OnClick="addRetrievalMethodButton_Click" ValidationGroup="AddRetrievalMethod"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addRetrievalMethodRequiredValidator" runat="server"
                                        ErrorMessage="Retrieval Method is a required field."
                                        ControlToValidate="txtAddRetrievalMethod"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddRetrievalMethod">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addRetrievalMethodRequiredValidatorExtender" runat="server"
                                        BehaviorID="addRetrievalMethodRequiredValidatorExtender"
                                        TargetControlID="addRetrievalMethodRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                </asp:Panel>                                                                                                                                              
                            </telerik:RadPageView>
                        </telerik:RadMultiPage>
                   </div>                                                                 
              
</asp:Content>


                    
                  
