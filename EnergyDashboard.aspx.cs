﻿using System;
using System.Configuration;
using System.IO;
using System.Web.UI;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Web;
using System.Data;
using System.Web.UI.HtmlControls;

using CW.Business;
using CW.Website._framework;
using CW.Data;
using CW.Utility;
using CW.Data.Models.Client;
using CW.Data.Models.VAndVPre;
using CW.Data.Models.Performance;

using Telerik.Web.UI;
using System.Globalization;
using CW.Common.Constants;
using System.Linq.Expressions;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class EnergyDashboard : SitePage
    {
        #region Properties

            private SiteUser mSiteUser;
            const string noClientImage = "no-client-image.png";
            const string noPlots = "No data available.";
            const string failedPlots = "Data failed to plot. Please contact an administrator.";
            const string buildingElecticLegend = "Electric";
            const string totalMeterSum = "Total Meter Sum: ";

            IEnumerable<VDataAndVPreDataFullBase> consumptionVDataAllBuildingsResults = Enumerable.Empty<VDataAndVPreDataFullBase>();
            IEnumerable<VDataAndVPreDataFullBase> comparisonVDataAllBuildingsResults = Enumerable.Empty<VDataAndVPreDataFullBase>();
            IEnumerable<VDataAndVPreDataFullBase> comparisonVPreDataAllBuildingsResults = Enumerable.Empty<VDataAndVPreDataFullBase>();

            IEnumerable<ColumBarData> consumptionAllFormattedBuildingResults = Enumerable.Empty<ColumBarData>();
            IEnumerable<ColumBarData> comparisonAllFormattedBuildingResults = Enumerable.Empty<ColumBarData>();

            DataSet dsConsumption = new DataSet("Consumption");
            DataSet dsComparison = new DataSet("Comparison");

            bool hasConsumptionData = false;
            bool hasComparisonData = false;

            decimal consumptionCalculatableSum;
            decimal comparisonCalculatableSum;

            private int cultureLCID;

            private enum ComparisonType {Baseline, Previous };

        #endregion

        #region Page Events

            protected override void OnLoad(EventArgs e)
            {
                //datamanger in sitepage
                
                mSiteUser = siteUser;
                cultureLCID = CultureInfo.CurrentCulture.LCID;

                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litDashboardBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.EnergyDashboardBody);

                    SetClientInfoAndImages();

                    if (mSiteUser.VisibleBuildings.Any())
                    {
                        //intial binds
                        BindBuildingTypes(ddlBuildingTypes);
                        BindBuildings(ddlMeterBuilding);

                        //generate initial charts
                        generateButton_Click(null, null);
                        generateMeterButton_Click(null, null);
                    }
                }
            }

            protected void Page_Init()
            {
            }

        #endregion

        #region Load and Bind Fields

            private void BindBuildingTypes(DropDownList ddl)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "All";
                lItem.Value = "All";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "BuildingTypeName";
                ddl.DataValueField = "BuildingTypeID";
                ddl.DataSource = DataMgr.BuildingTypeDataMapper.GetAllBuildingTypesForClient(mSiteUser.CID);
                ddl.DataBind();

                ddlBuildingTypes_OnSelectedIndexChanged(null, null);
            }

            /// <summary>
            /// Bind Buildings
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="buildings"></param>
            private void BindBuildings(ListBox lb, string buildingTypeID, string unit)
            {
                lb.DataTextField = "BuildingName";
                lb.DataValueField = "BID";

                //get all buildings by cid, then filter down by users visible buildings
                IEnumerable<Building> allClientBuildings = DataMgr.BuildingDataMapper.GetAllBuildingsByCID(siteUser.CID, new Expression<Func<Building, Object>>[] { (b => b.BuildingSettings) });
                var tempBuidlingsIP = siteUser.VisibleBuildings.Intersect(allClientBuildings.Where(b => b.BuildingSettings.Select(bs => bs.UnitSystem).First() == false), new BuildingDataMapper.BuildingComparer());
                var tempBuidlingsSI = siteUser.VisibleBuildings.Intersect(allClientBuildings.Where(b => b.BuildingSettings.Select(bs => bs.UnitSystem).First() == true), new BuildingDataMapper.BuildingComparer());

                if (String.IsNullOrEmpty(unit))
                {
                    ddlUnits.Items.Clear();

                    if (tempBuidlingsIP.Any())
                    {
                        ListItem lItem = new ListItem();
                        lItem.Text = "IP";
                        lItem.Value = "0";
                        ddlUnits.Items.Add(lItem);
                    }
                    if (tempBuidlingsSI.Any())
                    {
                        ListItem lItem = new ListItem();
                        lItem.Text = "SI";
                        lItem.Value = "1";
                        ddlUnits.Items.Add(lItem);
                    }

                    if (!tempBuidlingsIP.Any() || !tempBuidlingsSI.Any())
                    {
                        divUnits.Visible = false;
                    }

                    lb.DataSource = tempBuidlingsIP.Any() ? tempBuidlingsIP : tempBuidlingsSI;

                    ddlUnits.Items[0].Selected = true;
                    ddlUnits.DataBind();
                }
                else if (unit == "0")
                {
                    //IP
                    lb.DataSource = buildingTypeID.ToUpper() == "ALL" ? tempBuidlingsIP : tempBuidlingsIP.Where(b => b.BuildingTypeID == Convert.ToInt32(buildingTypeID));
                }
                else
                {
                    //SI
                    lb.DataSource = buildingTypeID.ToUpper() == "ALL" ? tempBuidlingsSI : tempBuidlingsSI.Where(b => b.BuildingTypeID == Convert.ToInt32(buildingTypeID));
                }

                lb.DataBind();

                foreach (ListItem i in lb.Items)
                {
                    i.Selected = true;
                }
            }
            
            /// <summary>
            /// Bind Buildings
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="buildings"></param>
            private void BindBuildings(DropDownList ddl)
            {
                ddl.DataTextField = "BuildingName";
                ddl.DataValueField = "BID";
                ddl.DataSource = mSiteUser.VisibleBuildings;
                ddl.DataBind();

                //select first building
                ddl.Items[0].Selected = true;
            }


        #endregion

        #region Set Fields and Data

            private void SetClientInfoAndImages()
            {
                var client = DataMgr.ClientDataMapper.GetClient(mSiteUser.CID);

                if (client == null) { return; }

                imgHeaderClientImage.AlternateText = "";
                imgHeaderClientImage.Visible = true;
                imgHeaderClientImage.ImageUrl = HandlerHelper.ClientImageUrl(client.CID, client.ImageExtension);

                lblHeaderClientName.InnerText = mSiteUser.ClientName;
            }

        #endregion

        #region Dropdown Events

            protected void ddlComparison_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                ComparisonType comparisonType;
                if (Enum.TryParse(ddlComparison.SelectedValue, out comparisonType))
                {
                    switch (comparisonType)
                    {
                        case ComparisonType.Baseline:
                            chkWeather.Checked = false;
                            divWeather.Visible = false;
                            break;
                        case ComparisonType.Previous:
                            divWeather.Visible = true;
                            break;
                    }                    
                }                
            }

            protected void ddlUnits_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindBuildings(lbBuildings, ddlBuildingTypes.SelectedValue, ddlUnits.SelectedValue);
            }

            protected void ddlBuildingTypes_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindBuildings(lbBuildings, ddlBuildingTypes.SelectedValue, ddlUnits.SelectedValue);
            }

        #endregion

        #region ListBox Events

        #endregion

        #region Button Click Events

            /// <summary>
            /// Generate button click
            /// </summary>
            protected void generateButton_Click(object sender, EventArgs e)
            {        
                List<int> bids = new List<int>();

                foreach (ListItem li in lbBuildings.Items)
                {
                    if (li.Selected)
                    {
                        bids.Add(Convert.ToInt32(li.Value));
                    }
                }

                if (bids.Any())
                {
                    GenerateCharts(bids);
                }
            }

            /// <summary>
            /// Generate Building Meter Overview 
            /// </summary>
            protected void generateMeterButton_Click(object sender, EventArgs e)
            {
                PlotBuildingOverview(Convert.ToInt32(ddlMeterBuilding.SelectedValue));

                //set initial as yesterday drilldown
                buildingDrillDownButton_Click(null, null);
                buildingYesterdayButton_Click(null, null);
            }

            protected void buildingTotalsButton_Click(object sender, EventArgs e)
            {
                btnBuildingTotal.CssClass = "lnkBtnSmallestDockActive";
                btnBuildingDrillDown.CssClass = "lnkBtnSmallestDock";

                SetAllBuildingMeterControlsVisibility();
            }
            protected void buildingDrillDownButton_Click(object sender, EventArgs e)
            {
                btnBuildingTotal.CssClass = "lnkBtnSmallestDock";
                btnBuildingDrillDown.CssClass = "lnkBtnSmallestDockActive";

                SetAllBuildingMeterControlsVisibility();              
            }
            protected void buildingYesterdayButton_Click(object sender, EventArgs e)
            {
                btnBuildingYesterday.CssClass = "lnkBtnSmallestDockActive";
                btnBuildingWeek.CssClass = "lnkBtnSmallestDock";
                btnBuildingMonth.CssClass = "lnkBtnSmallestDock";
                btnBuildingYear.CssClass = "lnkBtnSmallestDock";

                SetAllBuildingMeterControlsVisibility();
            }
            protected void buildingWeekButton_Click(object sender, EventArgs e)
            {
                btnBuildingYesterday.CssClass = "lnkBtnSmallestDock";
                btnBuildingWeek.CssClass = "lnkBtnSmallestDockActive";
                btnBuildingMonth.CssClass = "lnkBtnSmallestDock";
                btnBuildingYear.CssClass = "lnkBtnSmallestDock";

                SetAllBuildingMeterControlsVisibility();
            }
            protected void buildingMonthButton_Click(object sender, EventArgs e)
            {
                btnBuildingYesterday.CssClass = "lnkBtnSmallestDock";
                btnBuildingWeek.CssClass = "lnkBtnSmallestDock";
                btnBuildingMonth.CssClass = "lnkBtnSmallestDockActive";
                btnBuildingYear.CssClass = "lnkBtnSmallestDock";

                SetAllBuildingMeterControlsVisibility();
            }
            protected void buildingYearButton_Click(object sender, EventArgs e)
            {               
                btnBuildingYesterday.CssClass = "lnkBtnSmallestDock";
                btnBuildingWeek.CssClass = "lnkBtnSmallestDock";
                btnBuildingMonth.CssClass = "lnkBtnSmallestDock";
                btnBuildingYear.CssClass = "lnkBtnSmallestDockActive";

                SetAllBuildingMeterControlsVisibility();
            }

        #endregion

        #region Binding Methods

            /// <summary>
            /// 
            /// </summary>
            private void BindPresetWidgets()
            {
            }


        #endregion

        #region Chart Methods

            /// <summary>
            /// Plots the buildings selected.
            /// </summary>
            protected void GenerateCharts(List<int> bids)
            {
                int[] pointTypeIDs = new int[0];
                int buildingCounter = 1;
                int timePeriodInMonths = Convert.ToInt32(ddlRange.SelectedValue);
                bool weatherNormalization = chkWeather.Checked;
                bool areaNormalization = chkArea.Checked;
                bool isTotal =  ddlUtility.SelectedValue.ToUpper() == "TOTAL";
                bool units = Convert.ToBoolean(Convert.ToInt32(ddlUnits.SelectedValue));
                string unitName = "";
                DateTime endMonthComparison, startMonthComparison;

                ComparisonType comparisonType;
                Enum.TryParse(ddlComparison.SelectedValue, out comparisonType);


                //clears
                RadHtmlChart1.PlotArea.Series.Clear();
                RadHtmlChart2.PlotArea.Series.Clear();
                RadHtmlChart1.PlotArea.XAxis.Items.Clear();
                RadHtmlChart2.PlotArea.XAxis.Items.Clear();
                RadHtmlChart1.PlotArea.XAxis.Type = Telerik.Web.UI.HtmlChart.AxisType.Category;
                RadHtmlChart2.PlotArea.XAxis.Type = Telerik.Web.UI.HtmlChart.AxisType.Category;                                    

                //time is ignored later in the vdata query after the utc conversions
                DateTime endMonthConsumption = DateTimeHelper.GetXMonthsAgo(DateTimeHelper.GetFirstOfMonth(DateTime.UtcNow), 1);
                DateTime startMonthConsumption = DateTimeHelper.GetXMonthsAgo(endMonthConsumption, timePeriodInMonths - 1);
                
                //TODO: more cases
                switch (comparisonType)
                {
                    case ComparisonType.Baseline:
                        goto default;
                    case ComparisonType.Previous:
                        endMonthComparison = DateTimeHelper.GetXMonthsAgo(endMonthConsumption, 12);
                        startMonthComparison = DateTimeHelper.GetXMonthsAgo(startMonthConsumption, 12);
                        break;
                    default:                    
                        endMonthComparison = endMonthConsumption;
                        startMonthComparison = startMonthConsumption;
                        break;
                }


                //build xaxis labels
                List<AxisItem> radHtmlChart1XAxisLabels = new List<AxisItem>();
                List<AxisItem> radHtmlChart2XAxisLabels = new List<AxisItem>();
                DateTime dateTimeCounter = startMonthConsumption;
                DateTime dateTimeCounter2 = startMonthComparison;

                while (dateTimeCounter <= endMonthConsumption)
                {
                    radHtmlChart1XAxisLabels.Add(new AxisItem(dateTimeCounter.ToString("MMM yyyy")));
                    dateTimeCounter = dateTimeCounter.AddMonths(1);
                }
                while (dateTimeCounter2 <= endMonthComparison)
                {
                    radHtmlChart2XAxisLabels.Add(new AxisItem(dateTimeCounter2.ToString("MMM yyyy")));
                    dateTimeCounter2 = dateTimeCounter2.AddMonths(1);
                }

                //set pointtypeids
                switch (comparisonType)
                {
                    case ComparisonType.Baseline:
                        if (isTotal)
                        {
                            pointTypeIDs = BusinessConstants.PointType.PointTypeUtilityConversionLookup.Keys.ToArray();
                        }
                        else
                        {
                            pointTypeIDs = new int[] { Convert.ToInt32(ddlUtility.SelectedValue.Split('|')[0]) };
                        }
                        //get comparison vpredata                        
                        comparisonVPreDataAllBuildingsResults = DataMgr.VAndVPreDataMapper.GetVPreDataBuildingUtilitiesFull(mSiteUser, bids, startMonthComparison, endMonthComparison, DataConstants.AnalysisRange.Monthly, pointTypeIDs);

                        break;
                    case ComparisonType.Previous:
                        if (isTotal)
                        {
                            pointTypeIDs = weatherNormalization ? BusinessConstants.PointType.PointTypeUtilityWeatherNormalizationConversionLookup.Keys.ToArray() : BusinessConstants.PointType.PointTypeUtilityConversionLookup.Keys.ToArray();
                        }
                        else
                        {
                            pointTypeIDs = new int[] { Convert.ToInt32(ddlUtility.SelectedValue.Split('|')[(weatherNormalization ? 1 : 0)]) };
                        }
                        //get compraison vdata                        
                        comparisonVDataAllBuildingsResults = DataMgr.VAndVPreDataMapper.GetVDataBuildingUtilitiesFull(mSiteUser, bids, startMonthComparison, endMonthComparison, DataConstants.AnalysisRange.Monthly, pointTypeIDs);

                        break;
                }

                //get consimption vdata
                consumptionVDataAllBuildingsResults = DataMgr.VAndVPreDataMapper.GetVDataBuildingUtilitiesFull(mSiteUser, bids, startMonthConsumption, endMonthConsumption, DataConstants.AnalysisRange.Monthly, pointTypeIDs);


                //set unitname
                if (pointTypeIDs.Count() == 1)
                {
                    unitName =  DataMgr.PointTypeDataMapper.GetPointTypeEngUnitNameByID(pointTypeIDs.First(), units);
                }
                else
                {
                    unitName = (units ? DataConstants.TotalEngUnits.kWh : DataConstants.TotalEngUnits.kBTU).ToString();  
                }
                unitName = String.Format("({0})", unitName);


                //foreach building
                foreach (int bid in bids)
                {
                    //get building object
                    Building building = mSiteUser.VisibleBuildings.Where(b => b.BID == bid).First();                   

                    //get building data from sets
                    IEnumerable<VDataAndVPreDataFullBase> consumptionBuildingResults = consumptionVDataAllBuildingsResults.Where(b => b.BID == bid).ToList();
                    var comparisonBuildingResults = comparisonVDataAllBuildingsResults.Any() ? comparisonVDataAllBuildingsResults.Where(b => b.BID == bid).ToList() : comparisonVPreDataAllBuildingsResults.Where(b => b.BID == bid).ToList();

                    //-----CONSUMPTION
                    if (consumptionBuildingResults.Any())
                    {
                        hasConsumptionData = true;

                        //area normalize data
                        if(areaNormalization)
                            ArealNormalizeData(consumptionBuildingResults, building);

                        //group by date and sum if multiple utilities
                        List<ColumBarData> tempConsumptionBuildingResults = consumptionBuildingResults.GroupBy(d => d.StartDate).Select(group =>
                                                                                new ColumBarData
                                                                                {
                                                                                    //Name = group.Count() > 1 ? "Total" : group.First().PointTypeDisplayName,
                                                                                    Name = building.BuildingName,
                                                                                    Value = Convert.ToDecimal(areaNormalization ? group.Sum(v => v.ConvertedRawValue) : Math.Round(group.Sum(v => v.ConvertedRawValue), 0)),
                                                                                    FactorOfTime = group.First().StartDate.ToString("MMM yyyy"),                                                                
                                                                                }).ToList();

                        //concat building data
                        consumptionAllFormattedBuildingResults = consumptionAllFormattedBuildingResults.Concat(tempConsumptionBuildingResults);

                        //create data sets for charts
                        CreateConsumptionChartSeries(tempConsumptionBuildingResults, building, buildingCounter, radHtmlChart1XAxisLabels, unitName);
                        //CreateConsumptionChartDataSet(tempConsumptionBuildingResults, building, counter);

                        //-----COMPARISON
                        if (comparisonBuildingResults.Any())
                        {
                            hasComparisonData = true;

                            //area normalize data
                            if (areaNormalization)
                                ArealNormalizeData(comparisonBuildingResults, building);

                            //group by date and sum if multiple utilities
                            List<ColumBarData> tempComparisonBuildingResults = comparisonBuildingResults.GroupBy(d => d.StartDate).Select(group =>
                                                                                    new ColumBarData
                                                                                    {
                                                                                        //Name = group.Count() > 1 ? "Total" : group.First().PointTypeDisplayName,
                                                                                        Name = building.BuildingName,
                                                                                        Value = Convert.ToDecimal(areaNormalization ? group.Sum(v => v.ConvertedRawValue) : Math.Round(group.Sum(v => v.ConvertedRawValue), 0)),
                                                                                        FactorOfTime = group.First().StartDate.ToString("MMM yyyy"),
                                                                                    }).ToList();

                            foreach (ColumBarData data in tempComparisonBuildingResults)
                            {
                                decimal val;

                                //TODO: more cases
                                switch (comparisonType)
                                {
                                    case ComparisonType.Baseline:
                                        goto default;
                                    case ComparisonType.Previous:
                                        val = Convert.ToDecimal(tempConsumptionBuildingResults.Where(d => Convert.ToDateTime(d.FactorOfTime) == DateTimeHelper.GetXMonthsAgo(Convert.ToDateTime(data.FactorOfTime), -12)).Select(v => v.Value).FirstOrDefault());
                                        break;
                                    default:
                                        val = Convert.ToDecimal(tempConsumptionBuildingResults.Where(d => Convert.ToDateTime(d.FactorOfTime) == Convert.ToDateTime(data.FactorOfTime)).Select(v => v.Value).FirstOrDefault());
                                        break;
                                }

                                if (val == 0 || data.Value == 0)
                                {
                                    data.Percent = 0;
                                }
                                else
                                {
                                    data.Percent = Convert.ToDecimal(Math.Round((((val - data.Value) / data.Value) * 100), 0));

                                    consumptionCalculatableSum = consumptionCalculatableSum + val;
                                    comparisonCalculatableSum = comparisonCalculatableSum + data.Value;
                                }
                            }

                            //concat building data
                            comparisonAllFormattedBuildingResults = comparisonAllFormattedBuildingResults.Concat(tempComparisonBuildingResults);

                            //create data sets for charts
                            CreateComparisonChartSeries(tempComparisonBuildingResults, building, buildingCounter, radHtmlChart2XAxisLabels);
                            //CreateComparisonChartDataSet(tempComparisonBuildingResults, building, counter);
                        }
                    }                    

                    if (consumptionBuildingResults.Any() || comparisonBuildingResults.Any())
                        buildingCounter++;
                }
              
                //bind charts and stats. set labels and visibility
                if (hasConsumptionData)
                {
                    RadHtmlChart1.PlotArea.XAxis.Items.AddRange(radHtmlChart1XAxisLabels);
                    //RadHtmlChart1.DataSource =  dsConsumption; //consumptionAllBuildingResults;
                    //RadHtmlChart1.DataBind();

                    BindConsumptionStatistics(consumptionAllFormattedBuildingResults, areaNormalization);

                    lblConsumptionStatistics.Visible = lblConsumptionResults.Visible = false;
                    divConsumptionStatistics.Visible = RadHtmlChart1.Visible = RadSplitter1.Visible = true;                    
                }
                else
                {
                    lblConsumptionStatistics.Text = lblConsumptionResults.Text = noPlots;
                    lblConsumptionStatistics.Visible = lblConsumptionResults.Visible = true;
                    divConsumptionStatistics.Visible = RadHtmlChart1.Visible = RadSplitter1.Visible = false;
                }
                if (hasComparisonData)
                {
                    RadHtmlChart2.PlotArea.XAxis.Items.AddRange(radHtmlChart2XAxisLabels);
                    //RadHtmlChart2.DataSource = dsComparison; //comparisonAllBuildingResults;
                    //RadHtmlChart2.DataBind();

                    BindComparisonStatistics(consumptionAllFormattedBuildingResults, comparisonAllFormattedBuildingResults);

                    lblComparisonStatistics.Visible = lblComparisonResults.Visible = false;
                    divComparisonStatistics.Visible = RadHtmlChart2.Visible = RadSplitter2.Visible = true;
                }
                else
                {
                    lblComparisonStatistics.Text = lblComparisonResults.Text = noPlots;
                    lblComparisonStatistics.Visible = lblComparisonResults.Visible = true;
                    divComparisonStatistics.Visible = RadHtmlChart2.Visible = RadSplitter2.Visible = false;
                }
            }


            /// <summary>
            /// 
            /// </summary>
            /// <param name="building"></param>
            /// <param name="counter"></param>
            //private void CreateConsumptionChartSeries(Building building, int counter)
            //{
            //    var series = new ColumnSeries
            //    {
            //        Name = building.BuildingName,
            //        DataFieldY = "Value",
            //    };
            //    series.TooltipsAppearance.DataFormatString = "{0}";
            //    series.TooltipsAppearance.BackgroundColor = ColorHelper.ConvertHexToSystemColor("#333333");
            //    series.TooltipsAppearance.Color = Color.WhiteSmoke;
            //    series.LabelsAppearance.Visible = false;

            //    //set se charting colors first before using defaults
            //    if (counter <= ColorHelper.SEChartingColors.Count())
            //    {
            //        series.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColors[counter - 1]);
            //    }

            //    RadHtmlChart1.PlotArea.Series.Add(series);
            //}

            /// <summary>
            /// /
            /// </summary>
            /// <param name="buildingResults"></param>
            /// <param name="building"></param>
            /// <param name="counter"></param>
            //private void CreateConsumptionChartDataSet(IEnumerable<ColumBarData> buildingResults, Building building, int counter)
            //{
            //    DataTable dt = new DataTable(building.BuildingName);
            //    dt.Columns.Add("Id", Type.GetType("System.Int32"));
            //    dt.Columns.Add("StartDate", Type.GetType("System.String"));
            //    dt.Columns.Add("ConvertedRawValue", Type.GetType("System.Double"));

            //    //group by date and sum if multiple utilities
            //    //buildingResults = buildingResults.GroupBy(d => d.ConvertedStartDate).Select(group =>
            //    //                                                        new VDataFull
            //    //                                                        {
            //    //                                                            PointTypeDisplayName = group.Count() > 1 ? "Total" : group.First().PointTypeDisplayName,
            //    //                                                            ConvertedRawValue = Math.Round(group.Sum(v => v.ConvertedRawValue), 0),
            //    //                                                            StartDate = group.First().StartDate
            //    //                                                        });


            //    //int rowIncrementer = 1;
            //    //foreach (VDataFull data in buildingResults)
            //    //{
            //    //    dt.Rows.Add(rowIncrementer, data.StartDate.ToString("MMM yyyy"), data.ConvertedRawValue);
            //        dt.Rows.Add(counter, buildingResults);
            //    //}

            //    if (dsConsumption.Tables.Count > 0)
            //    {
            //        dsConsumption.Tables[0].Merge(dt);                     
            //    }
            //    else
            //    {
            //        dsConsumption.Tables.Add(dt);
            //    }

            //    var series = new ColumnSeries
            //    {
            //        Name = building.BuildingName,
            //        DataFieldY = "Value",
            //    };
            //    series.TooltipsAppearance.DataFormatString = "{0}";
            //    series.TooltipsAppearance.BackgroundColor = ColorHelper.ConvertHexToSystemColor("#333333");
            //    series.TooltipsAppearance.Color = Color.WhiteSmoke;
            //    series.LabelsAppearance.Visible = false;

            //    //set se charting colors first before using defaults
            //    if (counter <= ColorHelper.SEChartingColors.Count())
            //    {
            //        series.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColors[counter - 1]);
            //    }

            //    RadHtmlChart1.PlotArea.Series.Add(series);                
            //}


            /// <summary>
            /// 
            /// </summary>
            /// <param name="buildingResults"></param>
            /// <param name="building"></param>
            /// <param name="counter"></param>
            private void CreateConsumptionChartSeries(IEnumerable<ColumBarData> buildingResults, Building building, int counter, List<AxisItem> axisItems, string unitName)
            {
                var series = new ColumnSeries
                {
                    Name = building.BuildingName,                    
                };
                //can add html to tooltip I think
                series.TooltipsAppearance.DataFormatString = "x=" + series.Name + " y={0:N0}";
                series.TooltipsAppearance.BackgroundColor = ColorHelper.ConvertHexToSystemColor("#3C3C3C");
                series.TooltipsAppearance.Color = Color.WhiteSmoke;
                series.LabelsAppearance.Visible = false;
                
                //set se charting colors first before using defaults
                if (counter <= ColorHelper.SEChartingColorsHex.Count())
                {
                    series.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColorsHex[counter - 1]);
                }

                foreach (AxisItem item in axisItems)
                {
                    ColumBarData result = buildingResults.Where(r => r.FactorOfTime == Convert.ToDateTime(item.LabelText).ToString("MMM yyyy")).FirstOrDefault();

                    if (result != null)
                    {
                        series.SeriesItems.Add(new CategorySeriesItem() { Y = result.Value });
                    }
                    else
                    {
                        series.SeriesItems.Add(new CategorySeriesItem() { Y = 0 });
                    }
                }
                
                //foreach (ColumBarData data in buildingResults)
                //{
                //    series.SeriesItems.Add(new CategorySeriesItem(){ Y = Convert.ToDecimal(data.Value) });
                     
                //    //temp
                //    //if (!RadHtmlChart1.PlotArea.XAxis.Items.Contains(new AxisItem(data.FactorOfTime)))
                //    //    RadHtmlChart1.PlotArea.XAxis.Items.Add(new AxisItem(data.FactorOfTime));                
                //}                

                RadHtmlChart1.PlotArea.YAxis.TitleAppearance.Text = unitName;
                RadHtmlChart1.PlotArea.Series.Add(series);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="allBuildingResults"></param>
            private void BindConsumptionStatistics(IEnumerable<ColumBarData> allBuildingResults, bool areaNormalization)
            {
                //set consumptoin total
                spanTotalConsumption.InnerText = CultureHelper.FormatNumber(cultureLCID, allBuildingResults.Sum(v => v.Value), areaNormalization);
  
                //set peak month
                ColumBarData maxMonth = allBuildingResults.GroupBy(d => d.FactorOfTime).Select(group =>
                                                                                new ColumBarData
                                                                                {
                                                                                    FactorOfTime = group.First().FactorOfTime,
                                                                                    Value = group.Sum(v => v.Value)
                                                                                    })
                                                                                    .OrderByDescending(v => v.Value).First();

                spanPeakMonthConsumption.InnerText = CultureHelper.FormatNumber(cultureLCID, maxMonth.Value, areaNormalization) + " (" + maxMonth.FactorOfTime + ")";



                //set greatest building consumption
                ColumBarData maxBuilding = allBuildingResults.GroupBy(d => d.Name).Select(group =>
                                                                                new ColumBarData
                                                                                {
                                                                                    Name = group.First().Name,                                                                                    
                                                                                    Value = group.Sum(v => v.Value)
                                                                                })
                                                                                .OrderByDescending(v => v.Value).First();

                spanGreatestBuildingConsumption.InnerText = CultureHelper.FormatNumber(cultureLCID, maxBuilding.Value, areaNormalization) + " (" + maxBuilding.Name + ")";



                //TO SHOW BY EACH UTILITY TYPE       
                ////distinct types
                //string[] distinctPointTypes = buildingResults.Select(t => t.PointTypeName).Distinct().ToArray();

                ////add series for each point type
                //foreach (string type in distinctPointTypes)
                //{
                //    IEnumerable<VDataFull> seriesResults = buildingResults.Where(b => b.PointTypeName == type);

                //    string shortName = seriesResults.First().PointTypeDisplayName;

                //    //create drilldown chart series, set chart type and name
                //    var series =
                //    new Series
                //    {
                //        ChartType = SeriesChartType.Bar,
                //        Name = shortName,
                //        ToolTip = "X: #VALX, Y: #VALY",
                //        LegendToolTip = shortName,
                //        LegendText = shortName,
                //    };

                //    series.Points.DataBindXY(seriesResults, "StartDate", seriesResults, "ConvertedRawValue");

                //    //add series to chart
                //    consumptionChart.Series.Add(series);
                //}
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="building"></param>
            /// <param name="counter"></param>
            //private void CreateComparisonChartSeries(Building building, int counter)
            //{
            //    var series = new ColumnSeries
            //    {
            //        Name = building.BuildingName,
            //        DataFieldY = "Value",
            //    };
            //    series.TooltipsAppearance.DataFormatString = "{0}%";
            //    series.TooltipsAppearance.BackgroundColor = ColorHelper.ConvertHexToSystemColor("#333333");
            //    series.TooltipsAppearance.Color = Color.WhiteSmoke;
            //    series.LabelsAppearance.Visible = false;

            //    //set se charting colors first before using defaults
            //    if (counter <= ColorHelper.SEChartingColors.Count())
            //    {
            //        series.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColors[counter - 1]);
            //    }

            //    RadHtmlChart2.PlotArea.Series.Add(series);
            //}

            /// <summary>
            /// 
            /// </summary>
            /// <param name="comparisonBuildingResults"></param>
            /// <param name="building"></param>
            /// <param name="counter"></param>
            //private void CreateComparisonChartDataSet(IEnumerable<ColumBarData> comparisonBuildingResults, Building building, int counter)
            //{
            //    DataTable dt = new DataTable(building.BuildingName + counter.ToString());
            //    dt.Columns.Add("Id", Type.GetType("System.Int32"));
            //    dt.Columns.Add("StartDate", Type.GetType("System.String"));
            //    dt.Columns.Add("ConvertedRawValue", Type.GetType("System.Double"));

            //    //group by date and sum if multiple utilities
            //    //comparisonBuildingResults = comparisonBuildingResults.GroupBy(d => d.ConvertedStartDate).Select(group =>
            //    //                                                        new VDataFull
            //    //                                                        {
            //    //                                                            PointTypeDisplayName = group.Count() > 1 ? "Total" : group.First().PointTypeDisplayName,
            //    //                                                            ConvertedRawValue = Math.Round(group.Sum(v => v.ConvertedRawValue), 0),
            //    //                                                            StartDate = group.First().StartDate
            //    //                                                        });


            //    //int rowIncrementer = 1;
            //    //foreach (VDataFull data in comparisonBuildingResults)
            //    //{
            //    //    var val = consumptionBuildingResults.Where(d => d.StartDate == DateTimeHelper.GetXMonthsAgo(data.StartDate, -timePeriodInMonths)).Select(v => v.ConvertedRawValue).FirstOrDefault();

            //    //    if (val != 0)
            //    //        dt.Rows.Add(rowIncrementer, data.StartDate.ToString("MMM yyyy"), Math.Round((((data.ConvertedRawValue - val) / val) * 100), 0));
            //        dt.Rows.Add(counter, comparisonBuildingResults);
            //    //}

            //    if (dsComparison.Tables.Count > 0)
            //    {
            //        dsComparison.Tables[0].Merge(dt);
            //    }
            //    else
            //    {
            //        dsComparison.Tables.Add(dt);
            //    }

            //    var series = new ColumnSeries
            //    {
            //        Name = building.BuildingName + counter.ToString(),
            //        DataFieldY = "ConvertedRawValue",
            //    };
            //    series.TooltipsAppearance.DataFormatString = "{0}%";
            //    series.TooltipsAppearance.BackgroundColor = ColorHelper.ConvertHexToSystemColor("#333333");
            //    series.TooltipsAppearance.Color = Color.WhiteSmoke;
            //    series.LabelsAppearance.Visible = false;

            //    //set se charting colors first before using defaults
            //    if (counter <= ColorHelper.SEChartingColors.Count())
            //    {
            //        series.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColors[counter - 1]);
            //    }

            //    RadHtmlChart2.PlotArea.Series.Add(series);
            //}


            /// <summary>
            /// 
            /// </summary>
            /// <param name="building"></param>
            /// <param name="counter"></param>
            private void CreateComparisonChartSeries(IEnumerable<ColumBarData> buildingResults, Building building, int counter, List<AxisItem> axisItems)
            {
                var series = new ColumnSeries
                {
                    Name = building.BuildingName,
                };
                //can add html to tooltip I think
                series.TooltipsAppearance.DataFormatString = "x=" + series.Name + " y={0:N0}%";
                series.TooltipsAppearance.BackgroundColor = ColorHelper.ConvertHexToSystemColor("#333333");
                series.TooltipsAppearance.Color = Color.WhiteSmoke;
                series.LabelsAppearance.Visible = false;

                //set se charting colors first before using defaults
                if (counter <= ColorHelper.SEChartingColorsHex.Count())
                {
                    series.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColorsHex[counter - 1]);
                }


                foreach (AxisItem item in axisItems)
                {
                    ColumBarData result = buildingResults.Where(r => r.FactorOfTime == Convert.ToDateTime(item.LabelText).ToString("MMM yyyy")).FirstOrDefault();

                    if (result != null)
                    {
                        series.SeriesItems.Add(new CategorySeriesItem() { Y = result.Percent});
                    }
                    else
                    {
                        series.SeriesItems.Add(new CategorySeriesItem() { Y = 0 });
                    }
                }

                //foreach (ColumBarData data in buildingResults)
                //{
                //    series.SeriesItems.Add(new CategorySeriesItem() { Y = Convert.ToDecimal(data.Value) });

                //    //temp
                //    //if (!RadHtmlChart2.PlotArea.XAxis.Items.Contains(new AxisItem(data.FactorOfTime)))
                //    //    RadHtmlChart2.PlotArea.XAxis.Items.Add(new AxisItem(data.FactorOfTime));
                //}

                RadHtmlChart2.PlotArea.Series.Add(series);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="allConsumptionBuildingResults"></param>
            /// <param name="allComparisonBuildingResults"></param>
            private void BindComparisonStatistics(IEnumerable<ColumBarData> allConsumptionBuildingResults, IEnumerable<ColumBarData> allComparisonBuildingResults)
            {
                //**calculatable data can only be based off data that was comparible, not just from the whole data sets.
                if (consumptionCalculatableSum != 0 && comparisonCalculatableSum != 0)
                {
                    spanTotalComparison.InnerText = Convert.ToDecimal(Math.Round((((consumptionCalculatableSum - comparisonCalculatableSum) / comparisonCalculatableSum) * 100), 0)).ToString() + "%";
                }
                else
                {
                    spanTotalComparison.InnerText = "0%";
                }

                //OLD
                //spanTotalComparison.InnerText = Math.Round(allComparisonBuildingResults.Sum(v => v.Value), 0).ToString() + "%";

                //set peak month
                //decimal max = allComparisonBuildingResults.GroupBy(d => d.FactorOfTime).Select(group => group.Sum(v => v.Value)).Max();
                //spanPeakMonthComparison.InnerText = Math.Round(max, 0).ToString() + "%";

                //set total comparison difference
                //spanTotalComparisonDifference.InnerText =  string.Format("{0:0.0}", Math.Round(allConsumptionBuildingResults.Sum(v => v.Value) - allComparisonBuildingResults.Sum(v => v.Value), 0).ToString());                              
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="buildingResults"></param>
            /// <param name="building"></param>
            /// <param name="counter"></param>
            //private void BindTestChart1(IEnumerable<VDataFull> buildingResults, Building building, int counter)
            //{
            //    //group by date and sum if multiple utilities
            //    buildingResults = buildingResults.GroupBy(d => d.ConvertedStartDate).Select(group =>
            //                                                            new VDataFull
            //                                                            {
            //                                                                PointTypeDisplayName = group.Count() > 1 ? "Total" : group.First().PointTypeDisplayName,
            //                                                                ConvertedRawValue = group.Sum(v => v.ConvertedRawValue),
            //                                                                StartDate = group.First().StartDate
            //                                                            });


            //    //add series for buildingresults
            //    var series = new ColumnSeries
            //    {
            //        DataFieldY = "ConvertedRawValue",
            //        Name = building.BuildingName,
            //    };
            //    series.TooltipsAppearance.DataFormatString = "{0}";
            //    series.LabelsAppearance.Visible = false;

            //    //set se charting colors first before using defaults
            //    if (counter <= ColorHelper.SEChartingColors.Count())
            //    {
            //        series.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColors[counter - 1]);
            //    }

            //    foreach (VDataFull data in buildingResults)
            //    {
            //        series.SeriesItems.Add((decimal)data.ConvertedRawValue);

            //        //set based on chart time period
            //        RadHtmlChart1.PlotArea.XAxis.Items.Add(new AxisItem(data.StartDate.ToString("MMM yyyy")));
            //    }

            //    RadHtmlChart1.PlotArea.Series.Add(series);

            //    RadHtmlChart1.DataBind();



            //    //TO SHOW BY EACH UTILITY TYPE       
            //    ////distinct types
            //    //string[] distinctPointTypes = buildingResults.Select(t => t.PointTypeName).Distinct().ToArray();

            //    ////add series for each point type
            //    //foreach (string type in distinctPointTypes)
            //    //{
            //    //    IEnumerable<VDataFull> seriesResults = buildingResults.Where(b => b.PointTypeName == type);

            //    //    string shortName = seriesResults.First().PointTypeDisplayName;

            //    //    //create drilldown chart series, set chart type and name
            //    //    var series =
            //    //    new Series
            //    //    {
            //    //        ChartType = SeriesChartType.Bar,
            //    //        Name = shortName,
            //    //        ToolTip = "X: #VALX, Y: #VALY",
            //    //        LegendToolTip = shortName,
            //    //        LegendText = shortName,
            //    //    };

            //    //    series.Points.DataBindXY(seriesResults, "StartDate", seriesResults, "ConvertedRawValue");

            //    //    //add series to chart
            //    //    consumptionChart.Series.Add(series);
            //    //}
            //}

            /// <summary>
            /// 
            /// </summary>
            /// <param name="buildingResults"></param>
            /// <param name="building"></param>
            /// <param name="counter"></param>
            //private void BindTestChart2(IEnumerable<VDataFull> buildingResults, Building building, int counter)
            //{

            //    //group by date and sum if multiple utilities
            //    IEnumerable<CategorySeriesItem> items = buildingResults.GroupBy(d => d.ConvertedStartDate).Select(group =>
            //                                                            new CategorySeriesItem
            //                                                            {
            //                                                                Y = (decimal)group.Sum(v => v.ConvertedRawValue),
            //                                                                //PointTypeDisplayName = group.Count() > 1 ? "Total" : group.First().PointTypeDisplayName,
            //                                                                //ConvertedRawValue = group.Sum(v => v.ConvertedRawValue),
            //                                                                //StartDate = group.First().StartDate
            //                                                            });


            //    IEnumerable<AxisItem> axisItems = buildingResults.Select(b => new AxisItem
            //    {
            //        //set based on time period
            //        LabelText = b.StartDate.ToString("MMM yyyy"),
            //    });

            //    //add series for buildingresults
            //    var series = new ColumnSeries
            //    {
            //        Name = building.BuildingName,
            //    };
            //    series.TooltipsAppearance.DataFormatString = "{0}";
            //    series.LabelsAppearance.Visible = false;

            //    //set se charting colors first before using defaults
            //    if (counter <= ColorHelper.SEChartingColors.Count())
            //    {
            //        series.Appearance.FillStyle.BackgroundColor = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColors[counter - 1]);
            //    }

            //    series.SeriesItems.AddRange(items);

            //    RadHtmlChart2.PlotArea.XAxis.Items.AddRange(axisItems);

            //    RadHtmlChart2.PlotArea.Series.Add(series);

            //    RadHtmlChart2.DataBind();
            //}

        #endregion

        #region Building Overview Methods

            /// <summary>
            /// Plots the buildings selected.
            /// </summary>
            protected void PlotBuildingOverview(int bid)
            {
                //clear chart and plot if data
                buildingOverviewTotalYearChart.Series.Clear();
                buildingOverviewTotalMonthChart.Series.Clear();
                buildingOverviewTotalWeekChart.Series.Clear();
                buildingOverviewTotalYesterdayChart.Series.Clear();
                buildingOverviewDrillDownYearChart.Series.Clear();
                buildingOverviewDrillDownMonthChart.Series.Clear();
                buildingOverviewDrillDownWeekChart.Series.Clear();
                buildingOverviewDrillDownYesterdayChart.Series.Clear();

                //new tables format
                DataTable yearTotalTable = new DataTable();
                yearTotalTable.Columns.Add("PointName", typeof(string));
                yearTotalTable.Columns.Add("Actual", typeof(double));
                DataTable monthTotalTable = new DataTable();
                monthTotalTable.Columns.Add("PointName", typeof(string));
                monthTotalTable.Columns.Add("Actual", typeof(double));
                DataTable weekTotalTable = new DataTable();
                weekTotalTable.Columns.Add("PointName", typeof(string));
                weekTotalTable.Columns.Add("Actual", typeof(double));
                DataTable yesterdayTotalTable = new DataTable();
                yesterdayTotalTable.Columns.Add("PointName", typeof(string));
                yesterdayTotalTable.Columns.Add("Actual", typeof(double));

                List<CW.Data.Point> points = new List<CW.Data.Point>();

                DateTime yesterdayDate = DateTime.UtcNow.Date.AddDays(-1);
                DateTime endDate = DateTime.UtcNow.Date;
                DateTime firstOfYear = DateTimeHelper.GetFirstOfYear(DateTime.UtcNow);
                DateTime firstOfMonth = DateTimeHelper.GetFirstOfMonth(DateTime.UtcNow);
                DateTime firstOfWeek = DateTimeHelper.GetLastSunday(DateTime.UtcNow);

                IEnumerable<PerformanceResult> results = Enumerable.Empty<PerformanceResult>();
                IEnumerable<PerformanceResult> tempFilteredResults = Enumerable.Empty<PerformanceResult>();
                IEnumerable<PerformanceResult> tempSubFilteredResults = Enumerable.Empty<PerformanceResult>();

                //get building
                Building b = DataMgr.BuildingDataMapper.GetBuildingByBID(bid, false, false, false, false, false, false);

                //GET FROM CACHE
                results = DataMgr.PerformanceDataMapper.GetPerformanceResults(b);
                                   
                //if points exist here
                if (results != null && results.Any())
                {
                    foreach (PerformanceResult r in results)
                    {
                        //if metric type is 2, and value is not 0 then divide by sqft
                        if (Convert.ToInt32(ddlMeterNormalization.SelectedValue) == 2)
                        {
                            r.Value = Math.Round((r.Value / Convert.ToDouble(b.Sqft)), 5);
                        }
                        else
                        {
                            r.Value = Math.Round(r.Value, 0);
                        }
                    }


                    //Current Year Total (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Year);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Year);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Year);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Year);

                    if (tempFilteredResults.Any())
                    {
                        foreach (PerformanceResult pr in tempFilteredResults)
                        {
                            //add row
                            yearTotalTable.Rows.Add(pr.PointName, pr.Value);
                        }

                        //has year plot data-----

                        //convert datatable to a IEnumerable form
                        var IListTable = (yearTotalTable as System.ComponentModel.IListSource).GetList();
                        buildingOverviewTotalYearChart.DataBindTable(IListTable, "PointName");

                        //set titles based on metric
                        //buildingOverviewTotalYearChart.ChartAreas["buildingOverviewTotalYearChartArea"].AxisY.Title =
                        //    buildingOverviewDrillDownYearChart.ChartAreas["buildingOverviewDrillDownYearChartArea"].AxisY.Title = ddlMeterNormalization.SelectedIndex != 1 ? buildingElecticLegend : buildingElecticLegend + ddlMeterNormalization.SelectedItem.Text;

                        //set chart title
                        buildingOverviewTotalYearChart.Titles["buildingOverviewTotalYearTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Year. (Jan 1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                        buildingOverviewTotalYearChart.Series[0].Color = Color.ForestGreen;

                        double yearTotal = 0;

                        foreach (DataRow r in yearTotalTable.Rows)
                        {
                            yearTotal = yearTotal + Convert.ToDouble(r[1]);
                        }

                        lblBuildingMeterSum.Text = totalMeterSum + CultureHelper.FormatNumber(cultureLCID, Math.Round(yearTotal, 0));
                        lblBuildingMeterSum.CssClass = "";

                        buildingOverviewTotalYearChart.Visible = true;
                        lblBuildingOverviewTotalYearResults.Visible = false;
                    }
                    else
                    {
                        lblBuildingMeterSum.Text = noPlots;
                        lblBuildingMeterSum.CssClass = "dashboardMessage";

                        buildingOverviewTotalYearChart.Visible = false;
                        lblBuildingOverviewTotalYearResults.Text = noPlots;
                        lblBuildingOverviewTotalYearResults.Visible = true;
                    }



                    //Current Year Drilldown (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Year);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Year);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Year);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Year);

                    if (tempFilteredResults.Any())
                    {
                        List<int> pids = tempFilteredResults.Select(p => p.PID).Distinct().ToList();
                        int counter = 1;
                        double yearTotal = 0;

                        foreach (int pid in pids)
                        {
                            //sub temp results
                            tempSubFilteredResults = tempFilteredResults.Where(r => r.PID == pid).ToList();

                            string shortName = tempSubFilteredResults.First().PointName;

                            //if name already exists in series collection, increment
                            int i = 2;
                            while(buildingOverviewDrillDownYearChart.Series.Select(s => s.Name).Contains(shortName))
                            {
                                shortName = tempSubFilteredResults.First().PointName + i.ToString();                                
                                i++;
                            }  

                            //create drilldown chart series, set chart type and name
                            var series =
                            new Series
                            {
                                ChartType = SeriesChartType.StackedColumn,
                                Name = shortName,
                                ToolTip = "X: #VALX, Y: #VALY",
                                LegendToolTip = shortName,
                                LegendText = shortName,
                            };

                            //set se charting colors first before using defaults
                            if (counter <= ColorHelper.SEChartingColorsHex.Count())
                            {
                                series.Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColorsHex[counter - 1]);
                            }

                            series.Points.DataBindXY(tempSubFilteredResults, "BreakdownFactorOfTime", tempSubFilteredResults, "Value");                                                    

                            //add series to chart
                            buildingOverviewDrillDownYearChart.Series.Add(series);

                            yearTotal = yearTotal + tempFilteredResults.Sum(v => v.Value);

                            counter++;
                        }


                        //has year plot data-----
                        //set titles based on metric
                        //buildingOverviewTotalYearChart.ChartAreas["buildingOverviewTotalYearChartArea"].AxisY.Title =
                        //    buildingOverviewDrillDownYearChart.ChartAreas["buildingOverviewDrillDownYearChartArea"].AxisY.Title = ddlMeterNormalization.SelectedIndex != 1 ? buildingElecticLegend : buildingElecticLegend + ddlMeterNormalization.SelectedItem.Text;

                        //set chart title
                        buildingOverviewDrillDownYearChart.Titles["buildingOverviewDrillDownYearTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Year Monthly. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";
                        buildingOverviewDrillDownYearChart.Series[0].Color = Color.ForestGreen;

                        lblBuildingMeterSum.Text = totalMeterSum + CultureHelper.FormatNumber(cultureLCID, Math.Round(yearTotal, 0));
                        lblBuildingMeterSum.CssClass = "";

                        buildingOverviewDrillDownYearChart.Visible = true;
                        lblBuildingOverviewDrillDownYearResults.Visible = false;
                    }
                    else
                    {
                        lblBuildingMeterSum.Text = noPlots;
                        lblBuildingMeterSum.CssClass = "dashboardMessage";

                        buildingOverviewDrillDownYearChart.Visible = false;
                        lblBuildingOverviewDrillDownYearResults.Text = noPlots;
                        lblBuildingOverviewDrillDownYearResults.Visible = true;
                    }


                    //Current Month Total (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Month);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Month);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Month);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Month);

                    if (tempFilteredResults.Any())
                    {
                        foreach (PerformanceResult pr in tempFilteredResults)
                        {
                            //add row
                            monthTotalTable.Rows.Add(pr.PointName, pr.Value);
                        }

                        //has month plot data-----


                        //set titles based on metric
                        //buildingOverviewTotalMonthChart.ChartAreas["buildingOverviewTotalMonthChartArea"].AxisY.Title =
                        //        buildingOverviewDrillDownMonthChart.ChartAreas["buildingOverviewDrillDownMonthChartArea"].AxisY.Title = ddlMeterNormalization.SelectedIndex != 1 ? buildingElecticLegend : buildingElecticLegend + ddlMeterNormalization.SelectedItem.Text;

                        //convert datatable to a IEnumerable form
                        var IListTable = (monthTotalTable as System.ComponentModel.IListSource).GetList();
                        buildingOverviewTotalMonthChart.DataBindTable(IListTable, "PointName");

                        //set chart title
                        buildingOverviewTotalMonthChart.Titles["buildingOverviewTotalMonthTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Month. (The 1st through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                        buildingOverviewTotalMonthChart.Series[0].Color = Color.ForestGreen;

                        double monthTotal = 0;

                        foreach (DataRow r in monthTotalTable.Rows)
                        {
                            monthTotal = monthTotal + Convert.ToDouble(r[1]);
                        }

                        lblBuildingMeterSum.Text = totalMeterSum + CultureHelper.FormatNumber(cultureLCID, Math.Round(monthTotal, 0));
                        lblBuildingMeterSum.CssClass = "";

                        buildingOverviewTotalMonthChart.Visible = true;
                        lblBuildingOverviewTotalMonthResults.Visible = false;
                    }
                    else
                    {
                        lblBuildingMeterSum.Text = noPlots;
                        lblBuildingMeterSum.CssClass = "dashboardMessage";

                        buildingOverviewTotalMonthChart.Visible = false;
                        lblBuildingOverviewTotalMonthResults.Text = noPlots;
                        lblBuildingOverviewTotalMonthResults.Visible = true;
                    }



                    //Current Month Drilldown (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Month);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Month);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Month);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Month);

                    if (tempFilteredResults.Any())
                    {
                        List<int> pids = tempFilteredResults.Select(p => p.PID).Distinct().ToList();
                        int counter = 1;
                        double monthTotal = 0;

                        foreach (int pid in pids)
                        {
                            //sub temp results
                            tempSubFilteredResults = tempFilteredResults.Where(r => r.PID == pid).ToList();

                            string shortName = tempSubFilteredResults.First().PointName;

                            //if name already exists in series collection, increment
                            int i = 2;
                            while (buildingOverviewDrillDownMonthChart.Series.Select(s => s.Name).Contains(shortName))
                            {
                                shortName = tempSubFilteredResults.First().PointName + i.ToString();
                                i++;
                            }  

                            //create drilldown chart series, set chart type and name
                            var series =
                            new Series
                            {
                                ChartType = SeriesChartType.StackedColumn,
                                Name = shortName,
                                ToolTip = "X: #VALX, Y: #VALY",
                                LegendToolTip = shortName,
                                LegendText = shortName,
                            };

                            //set se charting colors first before using defaults
                            if (counter <= ColorHelper.SEChartingColorsHex.Count())
                            {
                                series.Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColorsHex[counter - 1]);
                            }

                            series.Points.DataBindXY(tempSubFilteredResults, "BreakdownFactorOfTime", tempSubFilteredResults, "Value");

                            //add series to chart
                            buildingOverviewDrillDownMonthChart.Series.Add(series);

                            monthTotal = monthTotal + tempFilteredResults.Sum(v => v.Value);

                            counter++;
                        }

                        //has month plot data-----

                        //set titles based on metric
                        //buildingOverviewTotalMonthChart.ChartAreas["buildingOverviewTotalMonthChartArea"].AxisY.Title =
                        //        buildingOverviewDrillDownMonthChart.ChartAreas["buildingOverviewDrillDownMonthChartArea"].AxisY.Title = ddlMeterNormalization.SelectedIndex != 1 ? buildingElecticLegend : buildingElecticLegend + ddlMeterNormalization.SelectedItem.Text;

                        buildingOverviewDrillDownMonthChart.Titles["buildingOverviewDrillDownMonthTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Month Daily. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                        lblBuildingMeterSum.Text = totalMeterSum + CultureHelper.FormatNumber(cultureLCID, Math.Round(monthTotal, 0));
                        lblBuildingMeterSum.CssClass = "";
                
                        buildingOverviewDrillDownMonthChart.Visible = true;
                        lblBuildingOverviewDrillDownMonthResults.Visible = false;
                    }
                    else
                    {
                        lblBuildingMeterSum.Text = noPlots;
                        lblBuildingMeterSum.CssClass = "dashboardMessage";

                        buildingOverviewDrillDownMonthChart.Visible = false;
                        lblBuildingOverviewDrillDownMonthResults.Text = noPlots;
                        lblBuildingOverviewDrillDownMonthResults.Visible = true;
                    }


                    //Current Week Total (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Week);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Week);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Week);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Week);

                    if (tempFilteredResults.Any())
                    {
                        foreach (PerformanceResult pr in tempFilteredResults)
                        {
                            //add row
                            weekTotalTable.Rows.Add(pr.PointName, pr.Value);
                        }

                        //has week plot data----

                        //set titles based on metric
                        //buildingOverviewTotalWeekChart.ChartAreas["buildingOverviewTotalWeekChartArea"].AxisY.Title =
                        //        buildingOverviewDrillDownWeekChart.ChartAreas["buildingOverviewDrillDownWeekChartArea"].AxisY.Title = ddlMeterNormalization.SelectedIndex != 1 ? buildingElecticLegend : buildingElecticLegend + ddlMeterNormalization.SelectedItem.Text;

                        //convert datatable to a IEnumerable form
                        var IListTable = (weekTotalTable as System.ComponentModel.IListSource).GetList();
                        buildingOverviewTotalWeekChart.DataBindTable(IListTable, "PointName");

                        //set chart title
                        buildingOverviewTotalWeekChart.Titles["buildingOverviewTotalWeekTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Week. (Sunday through Yesterday)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                        buildingOverviewTotalWeekChart.Series[0].Color = Color.ForestGreen;

                        double weekTotal = 0;

                        foreach (DataRow r in weekTotalTable.Rows)
                        {
                            weekTotal = weekTotal + Convert.ToDouble(r[1]);
                        }

                        lblBuildingMeterSum.Text = totalMeterSum + CultureHelper.FormatNumber(cultureLCID, Math.Round(weekTotal, 0));
                        lblBuildingMeterSum.CssClass = "";

                        buildingOverviewTotalWeekChart.Visible = true;
                        lblBuildingOverviewTotalWeekResults.Visible = false;
                    }
                    else
                    {
                        lblBuildingMeterSum.Text = noPlots;
                        lblBuildingMeterSum.CssClass = "dashboardMessage";

                        buildingOverviewTotalWeekChart.Visible = false;
                        lblBuildingOverviewTotalWeekResults.Text = noPlots;
                        lblBuildingOverviewTotalWeekResults.Visible = true;
                    }



                    //Current Week Drilldown (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Week);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Week);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Week);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Week);

                    if (tempFilteredResults.Any())
                    {
                        List<int> pids = tempFilteredResults.Select(p => p.PID).Distinct().ToList();
                        int counter = 1;
                        double weekTotal = 0;

                        foreach (int pid in pids)
                        {
                            //sub temp results
                            tempSubFilteredResults = tempFilteredResults.Where(r => r.PID == pid).ToList();

                            string shortName = tempSubFilteredResults.First().PointName;

                            //if name already exists in series collection, increment
                            int i = 2;
                            while (buildingOverviewDrillDownWeekChart.Series.Select(s => s.Name).Contains(shortName))
                            {
                                shortName = tempSubFilteredResults.First().PointName + i.ToString();
                                i++;
                            }  

                            //create drilldown chart series, set chart type and name
                            var series =
                            new Series
                            {
                                ChartType = SeriesChartType.StackedColumn,
                                Name = shortName,
                                ToolTip = "X: #VALX, Y: #VALY",
                                LegendToolTip = shortName,
                                LegendText = shortName,
                            };

                            //set se charting colors first before using defaults
                            if (counter <= ColorHelper.SEChartingColorsHex.Count())
                            {
                                series.Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColorsHex[counter - 1]);
                            }

                            series.Points.DataBindXY(tempSubFilteredResults, "BreakdownFactorOfTime", tempSubFilteredResults, "Value");

                            //add series to chart
                            buildingOverviewDrillDownWeekChart.Series.Add(series);

                            weekTotal = weekTotal + tempFilteredResults.Sum(v => v.Value);

                            counter++;
                        }

                        //has week plot data-----


                        //set titles based on metric
                        //buildingOverviewTotalWeekChart.ChartAreas["buildingOverviewTotalWeekChartArea"].AxisY.Title =
                        //        buildingOverviewDrillDownWeekChart.ChartAreas["buildingOverviewDrillDownWeekChartArea"].AxisY.Title = ddlMeterNormalization.SelectedIndex != 1 ? buildingElecticLegend : buildingElecticLegend + ddlMeterNormalization.SelectedItem.Text;

                        //set chart title                    
                        buildingOverviewDrillDownWeekChart.Titles["buildingOverviewDrillDownWeekTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for the Current Week Daily. (Stacked Bar)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                        buildingOverviewDrillDownWeekChart.Series[0].Color = Color.ForestGreen;

                        lblBuildingMeterSum.Text = totalMeterSum + CultureHelper.FormatNumber(cultureLCID, Math.Round(weekTotal, 0));
                        lblBuildingMeterSum.CssClass = "";

                        buildingOverviewDrillDownWeekChart.Visible = true;
                        lblBuildingOverviewDrillDownWeekResults.Visible = false;
                    }
                    else
                    {
                        lblBuildingMeterSum.Text = noPlots;
                        lblBuildingMeterSum.CssClass = "dashboardMessage";

                        buildingOverviewDrillDownWeekChart.Visible = false;
                        lblBuildingOverviewDrillDownWeekResults.Text = noPlots;
                        lblBuildingOverviewDrillDownWeekResults.Visible = true;
                    }


                    //Current Yesterday Total (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Yesterday);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Yesterday);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Yesterday);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Total && r.PerformanceRange == DataConstants.PerformanceRange.Yesterday);

                    if (tempFilteredResults.Any())
                    {
                        foreach (PerformanceResult pr in tempFilteredResults)
                        {
                            //add row
                            yesterdayTotalTable.Rows.Add(pr.PointName, pr.Value);
                        }

                        //has yesterday plot data-----

                        //set titles based on metric
                        //buildingOverviewTotalYesterdayChart.ChartAreas["buildingOverviewTotalYesterdayChartArea"].AxisY.Title =
                        //        buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisY.Title = ddlMeterNormalization.SelectedIndex != 1 ? buildingElecticLegend : buildingElecticLegend + ddlMeterNormalization.SelectedItem.Text;

                        //convert datatable to a IEnumerable form
                        var IListTable = (yesterdayTotalTable as System.ComponentModel.IListSource).GetList();
                        buildingOverviewTotalYesterdayChart.DataBindTable(IListTable, "PointName");

                        //set chart title
                        buildingOverviewTotalYesterdayChart.Titles["buildingOverviewTotalYesterdayTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for Yesterday.";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                        buildingOverviewTotalYesterdayChart.Series[0].Color = Color.ForestGreen;

                        double yesterdayTotal = 0;

                        foreach (DataRow r in yesterdayTotalTable.Rows)
                        {
                            yesterdayTotal = yesterdayTotal + Convert.ToDouble(r[1]);
                        }

                        lblBuildingMeterSum.Text = totalMeterSum + CultureHelper.FormatNumber(cultureLCID, Math.Round(yesterdayTotal, 0));
                        lblBuildingMeterSum.CssClass = "";

                        buildingOverviewTotalYesterdayChart.Visible = true;
                        lblBuildingOverviewTotalYesterdayResults.Visible = false;
                    }
                    else
                    {
                        lblBuildingMeterSum.Text = noPlots;
                        lblBuildingMeterSum.CssClass = "dashboardMessage";

                        buildingOverviewTotalYesterdayChart.Visible = false;
                        lblBuildingOverviewTotalYesterdayResults.Text = noPlots;
                        lblBuildingOverviewTotalYesterdayResults.Visible = true;
                    }


                    //Current Yesterday Drilldown (excluding today)----

                    //try to get power submeters first
                    tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Yesterday);
                    //try to get power meters second
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricPowerPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Yesterday);
                    //try to get energy submeters third
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingSubmeterElectricEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Yesterday);
                    //try to get energy meters fourth
                    if (!tempFilteredResults.Any())
                        tempFilteredResults = results.Where(r => r.PointTypeID == BusinessConstants.PointType.BuildingElectricalEnergyPointTypeID && r.PerformanceLevel == DataConstants.PerformanceLevel.Drilldown && r.PerformanceRange == DataConstants.PerformanceRange.Yesterday);

                    if (tempFilteredResults.Any())
                    {
                        List<int> pids = tempFilteredResults.Select(p => p.PID).Distinct().ToList();
                        int counter = 1;
                        double yesterdayTotal = 0;

                        //buildingOverviewDrillDownYesterdayChart.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Hours;
                        //buildingOverviewDrillDownYesterdayChart.ChartAreas[0].AxisX.Minimum = 0;
                        //buildingOverviewDrillDownYesterdayChart.ChartAreas[0].AxisX.Maximum = 24;
                        //buildingOverviewDrillDownYesterdayChart.ChartAreas[0].AxisX.Interval = 1;

                        foreach (int pid in pids)
                        {
                            //sub temp results
                            tempSubFilteredResults = tempFilteredResults.Where(r => r.PID == pid).ToList();                            

                            string shortName = tempSubFilteredResults.First().PointName;

                            //if name already exists in series collection, increment
                            int i = 2;
                            while (buildingOverviewDrillDownYesterdayChart.Series.Select(s => s.Name).Contains(shortName))
                            {
                                shortName = tempSubFilteredResults.First().PointName + i.ToString();
                                i++;
                            } 

                            //create drilldown chart series, set chart type and name
                            var series =
                            new Series
                            {
                                Name = shortName,
                                //ToolTip = "X: #VALX{0:00}, Y: #VALY",
                                LegendToolTip = shortName,
                                LegendText = shortName,
                            };

                            //set se charting colors first before using defaults
                            if (counter <= ColorHelper.SEChartingColorsHex.Count())
                            {
                                series.Color = ColorHelper.ConvertHexToSystemColor(ColorHelper.SEChartingColorsHex[counter - 1]);
                            }

                            //add drilldown series

                            //set axis intervaltype
                            //buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Hours;

                            //set axis format:  g = (short date and time), t = time
                            buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisX.LabelStyle.Format = "h:mm tt";

                            //buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].Area3DStyle.Enable3D = true;


                            //nessisary for stacked area default datetime sorting with 
                            List<TimeAreaData> timeAreaDate = tempSubFilteredResults.Select(t =>
                                    new TimeAreaData
                                    {
                                        Value = t.Value,
                                        OATime = Convert.ToDateTime(t.BreakdownFactorOfTime, CultureInfo.InvariantCulture).ToOADate(),
                                    }).ToList();

                            series.XValueType = ChartValueType.DateTimeOffset;
                            //series.IsXValueIndexed = true;

                            series.Points.DataBindXY(timeAreaDate, "OATime", timeAreaDate, "Value");

                            //add series to chart
                            buildingOverviewDrillDownYesterdayChart.Series.Add(series);

                            yesterdayTotal = yesterdayTotal + tempFilteredResults.Sum(v => v.Value);

                            counter++;
                        }


                        //try catch a few options here
                        try
                        {
                            //-----
                            // TODO: revisit syncronization to add/remove empty points based on a sampling interval. looping and sampling through all data might take to long.
                            // Consider using another chart control instead.
                            
                            bool missingPointsInSeries = false;
                            int largestSeriesPointCount = 0;

                            //Check Point Counts
                            //do before setting chart type to check for series point counts are not the same.
                            foreach (Series series in buildingOverviewDrillDownYesterdayChart.Series)
                            {
                                int count = series.Points.Count();

                                if (largestSeriesPointCount == 0)
                                {
                                    largestSeriesPointCount = count;
                                }
                                else if (count > largestSeriesPointCount)
                                {
                                    largestSeriesPointCount = count;
                                    missingPointsInSeries = true;
                                }
                                else if (count < largestSeriesPointCount)
                                {
                                    missingPointsInSeries = true;
                                }
                            }

                            //insert empty points for series with missing points interval.
                            if (missingPointsInSeries)
                            {
                                //Add Empty Points To Syncronize.
                                foreach (Series series in buildingOverviewDrillDownYesterdayChart.Series)
                                {
                                    for (int i = series.Points.Count; i < largestSeriesPointCount; i++)
                                    {
                                        //add about 1 tick in time from the last point
                                        series.Points.AddXY(series.Points.Last().XValue + .0000000001, 0);
                                        //buildingOverviewDrillDownYesterdayChart.DataManipulator.InsertEmptyPoints(1, IntervalType.Number, series);
                                    }

                                    series.ChartType = SeriesChartType.StackedArea;
                                }
                            }

                            //------                            
                        }
                        catch
                        {
                            try
                            {
                                //This ensures all data series have the same number of data points by filling in any empty spaces with 'fake' points.                       
                                buildingOverviewDrillDownYesterdayChart.AlignDataPointsByAxisLabel();

                                //set chart type after AlignDataPointsByAxisLabel
                                foreach (Series series in buildingOverviewDrillDownYesterdayChart.Series)
                                {
                                    series.ChartType = SeriesChartType.StackedArea;
                                }
                            }
                            catch
                            {
                            }
                        }

                        //has yesterday plot data-----

                        //set titles based on metric
                        //buildingOverviewTotalYesterdayChart.ChartAreas["buildingOverviewTotalYesterdayChartArea"].AxisY.Title =
                        //        buildingOverviewDrillDownYesterdayChart.ChartAreas["buildingOverviewDrillDownYesterdayChartArea"].AxisY.Title = ddlMeterNormalization.SelectedIndex != 1 ? buildingElecticLegend : buildingElecticLegend + ddlMeterNormalization.SelectedItem.Text;

                        //set chart title                        
                        buildingOverviewDrillDownYesterdayChart.Titles["buildingOverviewDrillDownYesterdayTitle"].Text = "Energy Consumption for " + b.BuildingName + " per Meter for Yesterday over Time. (Stacked Area)";  //"Plot metrics for " + ddlLiveEquipment.SelectedItem.Text + ".";

                        buildingOverviewDrillDownYesterdayChart.Series[0].Color = Color.ForestGreen;

                        lblBuildingMeterSum.Text = totalMeterSum + CultureHelper.FormatNumber(cultureLCID, Math.Round(yesterdayTotal, 0));
                        lblBuildingMeterSum.CssClass = "";

                        buildingOverviewDrillDownYesterdayChart.Visible = true;
                        lblBuildingOverviewDrillDownYesterdayResults.Visible = false;

                    }
                    else
                    {
                        lblBuildingMeterSum.Text = noPlots;
                        lblBuildingMeterSum.CssClass = "dashboardMessage";

                        buildingOverviewDrillDownYesterdayChart.Visible = false;
                        lblBuildingOverviewDrillDownYesterdayResults.Text = noPlots;
                        lblBuildingOverviewDrillDownYesterdayResults.Visible = true;                   
                    }

                    lblBuildingOverviewDefault.Visible = lblBuildingOverviewStatisticsDefault.Visible = false;
                }
                else
                {
                    buildingOverviewDrillDownYesterdayChart.Visible = buildingOverviewDrillDownWeekChart.Visible = buildingOverviewDrillDownMonthChart.Visible = buildingOverviewDrillDownYearChart.Visible = false;
                    buildingOverviewTotalYesterdayChart.Visible = buildingOverviewTotalWeekChart.Visible = buildingOverviewTotalMonthChart.Visible = buildingOverviewTotalYearChart.Visible = false;
                    lblBuildingOverviewDrillDownYesterdayResults.Text = lblBuildingOverviewDrillDownWeekResults.Text = lblBuildingOverviewDrillDownMonthResults.Text = lblBuildingOverviewDrillDownYearResults.Text = noPlots;
                    lblBuildingOverviewTotalYesterdayResults.Text = lblBuildingOverviewTotalWeekResults.Text = lblBuildingOverviewTotalMonthResults.Text = lblBuildingOverviewTotalYearResults.Text = noPlots;

                    lblBuildingOverviewDefault.Text = lblBuildingOverviewStatisticsDefault.Text = noPlots;
                    lblBuildingOverviewDefault.Visible = lblBuildingOverviewStatisticsDefault.Visible = true;
                }
            }

        #endregion

        #region Models

            public class BarData
            {
                public string FactorOfTime
                {
                    get;
                    set;
                }
                public double Value
                {
                    get;
                    set;
                }
            }
            public class TimeBarData
            {
                public DateTime Time
                {
                    get;
                    set;
                }
                public double Value
                {
                    get;
                    set;
                }
            }
            public class ColumBarData
            {
                public string Name
                {
                    get;
                    set;
                }
                public string FactorOfTime
                {
                    get;
                    set;
                }
                public decimal Value
                {
                    get;
                    set;
                }
                public decimal Percent
                {
                    get;
                    set;
                }
                public DateTime StartDate
                {
                    get;
                    set;
                }
            }



            public class TimeAreaData
            {
                public double OATime
                {
                    get;
                    set;
                }
                public double Value
                {
                    get;
                    set;
                }
            }

        #endregion

        #region RadAjaxPanel Events

            //protected void radUpdatePanelNested2_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
            //{
            //    AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
            //    trigger.ControlID = btnGenerate.UniqueID;
            //    e.UpdatePanel.Triggers.Add(trigger);
            //}
            //protected void radUpdatePanelNested3_AjaxSettingCreating(object sender, AjaxSettingCreatingEventArgs e)
            //{
            //    AsyncPostBackTrigger trigger = new AsyncPostBackTrigger();
            //    trigger.ControlID = btnGenerate.UniqueID;               
            //    e.UpdatePanel.Triggers.Add(trigger);
            //}

            //protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
            //{
            //    if (e.Argument == "InitialLoad")
            //    {
            //        System.Threading.Thread.Sleep(1000);
            //    }
            //}

        #endregion

        #region Helper Methods

            private void ArealNormalizeData(IEnumerable<VDataAndVPreDataFullBase> buildingResults, Building building)
            {
                //if results
                if (buildingResults.Any())
                {
                    //normalization area
                    foreach (VDataAndVPreDataFullBase data in buildingResults)
                    {
                        //sqft
                        data.ConvertedRawValue = data.ConvertedRawValue / Convert.ToDouble(building.Sqft);
                    }
                }
            }

            private void SetAllBuildingMeterControlsVisibility()
            {
                divBuildingOverviewTotalYesterdayResults.Visible = btnBuildingTotal.CssClass == "lnkBtnSmallestDockActive" && btnBuildingYesterday.CssClass == "lnkBtnSmallestDockActive";
                divBuildingOverviewTotalWeekResults.Visible = btnBuildingTotal.CssClass == "lnkBtnSmallestDockActive" && btnBuildingWeek.CssClass == "lnkBtnSmallestDockActive";
                divBuildingOverviewTotalMonthResults.Visible = btnBuildingTotal.CssClass == "lnkBtnSmallestDockActive" && btnBuildingMonth.CssClass == "lnkBtnSmallestDockActive";
                divBuildingOverviewTotalYearResults.Visible = btnBuildingTotal.CssClass == "lnkBtnSmallestDockActive" && btnBuildingYear.CssClass == "lnkBtnSmallestDockActive";

                divBuildingOverviewDrillDownYesterdayResults.Visible = btnBuildingDrillDown.CssClass == "lnkBtnSmallestDockActive" && btnBuildingYesterday.CssClass == "lnkBtnSmallestDockActive";
                divBuildingOverviewDrillDownWeekResults.Visible = btnBuildingDrillDown.CssClass == "lnkBtnSmallestDockActive" && btnBuildingWeek.CssClass == "lnkBtnSmallestDockActive";
                divBuildingOverviewDrillDownMonthResults.Visible = btnBuildingDrillDown.CssClass == "lnkBtnSmallestDockActive" && btnBuildingMonth.CssClass == "lnkBtnSmallestDockActive";
                divBuildingOverviewDrillDownYearResults.Visible = btnBuildingDrillDown.CssClass == "lnkBtnSmallestDockActive" && btnBuildingYear.CssClass == "lnkBtnSmallestDockActive";                
            }

        #endregion
    }
}