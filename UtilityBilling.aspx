﻿<%@ Page Language="C#" MasterPageFile="~/_masters/UtilityBilling.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="UtilityBilling.aspx.cs" Inherits="CW.Website.UtilityBilling" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register TagPrefix="cwcontrols" Namespace="CW.Website._controls" Assembly="CW.Website" %>
<%@ Register Assembly="CW.Website" Namespace="CW.Website._extensions" TagPrefix="extensions" %>
<%@ Import Namespace="CW.Utility" %>
<%@ Import Namespace="CW.Utility.Web" %>
<%@ Import Namespace="CW.Website.Reports" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">    
          <telerik:RadAjaxLoadingPanel ID="lpNested" runat="server" Skin="Default" />    	                  	
        	<div class="utilityModuleIcon">
                <h1>Utility Billing</h1>       
            </div>              
                <div class="richText">
                    <asp:Literal ID="litUtilityBillingBody" runat="server"></asp:Literal>
                </div>
                <div class="updateProgressDiv">
<%--                    <asp:UpdateProgress ID="updateProgressTop" runat="server">
                        <ProgressTemplate>
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                    </asp:UpdateProgress>  --%>
                </div> 
                                            
                <div class="administrationControls">
                    
                    <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="Report Browser"></telerik:RadTab>
                            <telerik:RadTab Text="Report Generator"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                
                                <!--Utility Billing Report Browser-->
                                <h2>Report Browser</h2> 
                                <p>
                                    The report browser allows you to view and download your utility billing allocation reports accross a date range by building, tenant, or a utility allocation.
                                </p>
                    
                    <telerik:RadAjaxPanel ID="radAjaxPanelNestedTop" runat="server" LoadingPanelID="lpNested">
                    <%--<asp:UpdatePanel ID="updatePanelNestedTop" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>--%>

                        <fieldset class="moduleSearch">
                        <legend>Search Criteria <asp:HyperLink ID="lnkSearch" runat="server" CssClass="searchToggle"><asp:Label ID="lblSearch" runat="server"></asp:Label></asp:HyperLink>&nbsp;</legend>                    
                        <ajaxToolkit:CollapsiblePanelExtender ID="detailsCollapsiblePanelExtender" runat="Server"
                                    TargetControlID="pnlCriteria"
                                    CollapsedSize="0"                                
                                    ExpandControlID="lnkSearch"
                                    CollapseControlID="lnkSearch"
                                    AutoCollapse="false"
                                    AutoExpand="false"
                                    ScrollContents="false"
                                    ExpandDirection="Vertical"
                                    TextLabelID="lblSearch"
                                    CollapsedText="+"
                                    ExpandedText="-"                              
                                    />

                        <asp:Panel ID="pnlCriteria" runat="server" DefaultButton="btnGenerateData" CssClass="criteria">

                        <!--View By-->                        
                        <div class="divCriteria">
                                <div>
                                    <h3>View By</h3>     
                                </div> 
                                <div class="divFormWidest">                     
                                    <asp:RadioButtonList ID="rblViewBy" CssClass="radio" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="rblViewBy_OnSelectedIndexChanged" runat="server">
                                        <asp:ListItem Value="Building" Text="Building"></asp:ListItem>
                                        <asp:ListItem Value="Equipment" Text="Tenant"></asp:ListItem>
                                        <asp:ListItem Value="Analysis" Text="Utility Allocation"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                         </div> 

                         <div class="divCriteria divViewByCol2">
                            <!--Building-->     
                            <asp:Panel ID="pnlBuildings" runat="server">                               
                                    <div class="dropDownPanel">   
                                        <label>*Select Building:</label> 
                                        <br />  
                                        <extensions:DropDownExtension ID="ddlBuildings" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                            <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                        </extensions:DropDownExtension> 
                                    </div>                            
                                    <asp:RequiredFieldValidator ID="buildingRequiredValidator" runat="server"
                                        ErrorMessage="Building is a required field." 
                                        ControlToValidate="ddlBuildings"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="Utility">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="buildingRequiredValidatorExtender" runat="server"
                                        BehaviorID="buildingRequiredValidatorExtender" 
                                        TargetControlID="buildingRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                            </asp:Panel>                                          
               
                            <!--Equipment-->
                            <asp:Panel ID="pnlEquipment" runat="server">
                                    <div class="dropDownPanel">   
                                        <label>*Select Tenant:</label>
                                        <br />  
                                        <extensions:DropDownExtension ID="ddlEquipment" CssClass="dropdownNarrow" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                            <asp:ListItem Value="-1" >Select building first...</asp:ListItem>                           
                                        </extensions:DropDownExtension>
                                    </div>                                                                              
                                    <asp:RequiredFieldValidator ID="equipmentRequiredValidator" runat="server"
                                        ErrorMessage="Equipment is a required field." 
                                        ControlToValidate="ddlEquipment"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="Utility">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="equipmentRequiredValidatorExtender" runat="server"
                                        BehaviorID="equipmentRequiredValidatorExtender" 
                                        TargetControlID="equipmentRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                          
                            </asp:Panel>
               
                            <!--Analysis-->
                            <asp:Panel ID="pnlAnalysis" runat="server">                                    
                                    <div class="dropDownPanel">   
                                        <label>*Select Utility Allocation:</label>
                                        <br />
                                        <extensions:DropDownExtension ID="ddlAnalysis" CssClass="dropdownNarrow" AppendDataBoundItems="true" AutoPostBack="true"  runat="server">    
                                            <asp:ListItem Value="-1" >Select tenant first...</asp:ListItem>                           
                                        </extensions:DropDownExtension>
                                    </div>  
                                    <asp:RequiredFieldValidator ID="analysisRequiredValidator" runat="server"
                                        ErrorMessage="Analysis is a required field." 
                                        ControlToValidate="ddlAnalysis"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="Utility">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="analysisRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="analysisRequiredValidatorExtender" 
                                        TargetControlID="analysisRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                          
                            </asp:Panel>  
                        </div>

                         <div class="divCriteria"> 
                         <!--Use bottomright popup validator positions for this column because the browser window will not always have space for the right most positioning. 
                            Since the button is below, the browser window will always have room for the botom validator positioning.-->                                                                                                                            
                                <div>
                                    <h3>Date Range</h2>     
                                </div>  
                                <div class="divFormWidest">    
                                    <label>*Starting End Date:</label>
                                    <br /> 
                                    <telerik:RadDatePicker ID="txtStartingEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                     
                                    <br />   
                                    <asp:RequiredFieldValidator ID="dateStartRequiredValidator" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtStartingEndDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="Utility">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="dateStartRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="dateStartRequiredValidatorExtender" 
                                        TargetControlID="dateStartRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <br />
                                    <label>*Ending End Date:</label>
                                    <br />
                                    <telerik:RadDatePicker ID="txtEndingEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                                            
                                    <br />                                         
                                    <asp:RequiredFieldValidator ID="dateEndRequiredValidator" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtEndingEndDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="Utility">
                                        </asp:RequiredFieldValidator> 
                                    <ajaxToolkit:ValidatorCalloutExtender ID="dateEndRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="dateEndRequiredValidatorExtender" 
                                        TargetControlID="dateEndRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                       
                                    <asp:CompareValidator ID="dateCompareValidator" Display="None" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Invalid range."
                                        ControlToValidate="txtEndingEndDate"
                                        ControlToCompare="txtStartingEndDate"
                                        Type="Date"
                                        Operator="GreaterThanEqual"
                                        ValidationGroup="Utility"
                                        SetFocusOnError="true"
                                        >
                                        </asp:CompareValidator>    
                                    <ajaxToolkit:ValidatorCalloutExtender ID="dateCompareValidatorExtender" runat="server" 
                                        BehaviorID="dateCompareValidatorExtender" 
                                        TargetControlID="dateCompareValidator" 
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175" />                                                                    
                        </div>
                        </div>

                    </asp:Panel>  
                    </fieldset>                                                       
                 <%--   </ContentTemplate>
                </asp:UpdatePanel>--%>
                </telerik:RadAjaxPanel>

                <telerik:RadAjaxPanel ID="radAjaxPanelNestedBottom" runat="server" LoadingPanelID="lpNested">
                <%--<asp:UpdatePanel ID="updatePanelNestedBottom" ChildrenAsTriggers="true" runat="server" UpdateMode="Conditional">
                    <Triggers>
                        <asp:PostBackTrigger ControlID="imgPdfDownload"/>
                        <asp:PostBackTrigger ControlID="lnkPdfDownload"/>
                    </Triggers>  
                    <ContentTemplate>--%>
                           <div class="divGenerateButton">
                                <asp:LinkButton ID="btnGenerateData" CssClass="lnkButton" ValidationGroup="Utility" OnClick="generateButton_Click" Text="Generate Data" runat="server" />
                           </div> 
                           
                           <div class="clear"></div>                                                                                                                                            
                           <div class="updateProgressDiv">
                                    <%--<asp:UpdateProgress ID="updateProgressBottom" AssociatedUpdatePanelID="updatePanelNestedBottom" runat="server">
                                        <ProgressTemplate>                                       
                                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Updating...</span>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>  --%>
                           </div>
                            <div>
                                <p>                                                                                                        
                                    <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnResultCount" runat="server" Value=""></asp:HiddenField>  
                                    <br /><asp:Label ID="lblErrorTop" CssClass="errorMessage" runat="server" Text="" Visible="false"></asp:Label>
                                </p>
                            </div>                                

                          
                            <div id="gridTbl">                                                                                                                                 
                            <!-- New Grid View with details view-->
                            <!--AutoGenerateSelectButton="true"  OnSelectedIndexChanged="gridUtility_OnSelectedIndexChanged" -->
                            <asp:GridView ID="gridUtility" 
                                 Width="100%"               
                                 runat="server" 
                                 EnableViewState="true" 
                                 DataKeyNames="AID,EID,BID,CID,EndDate"
                                 GridLines="None"                                           
                                 PageSize="25" PagerSettings-PageButtonCount="20"                  
                                 HeaderStyle-CssClass="tblTitle"
                                 RowStyle-CssClass="tblCol1"
                                 AlternatingRowStyle-CssClass="tblCol2"                                      
                                 AutoGenerateColumns="false" 
                                 OnDataBound="gridUtility_OnDataBound"
                                 OnRowCreated="gridUtility_OnRowCreated" 
                                 AllowPaging="true"  OnPageIndexChanging="gridUtility_PageIndexChanging"
                                 AllowSorting="true"  OnSorting="gridUtility_Sorting"                                     
                                 OnSelectedIndexChanged="gridUtility_OnSelectedIndexChanged"
                                 OnRowDeleting="gridUtility_Deleting"
                                 > 
                                     <Columns>                                                                                                                    
                                        <asp:TemplateField SortExpression="BuildingName" HeaderText="Building">                                        
                                        <ItemTemplate>
                                            <a runat="server" title='<%# Eval("BuildingName")%>' href='<%# LinkHelper.BuildUtilityBillingQuickLink(Container.DataItem, LinkHelper.ViewByMode.Building)%>'><%# StringHelper.TrimText(Eval("BuildingName").ToString(), 12)%></a>
                                            <a runat="server" visible="false" title='<%# Eval("BuildingName")%>' href='<%# LinkHelper.BuildUtilityBillingQuickLink(Container.DataItem, LinkHelper.ViewByMode.Building)%>'><%# Eval("BuildingName")%></a>
                                        </ItemTemplate>
                                        </asp:TemplateField>                                                                                  
                                        <asp:TemplateField SortExpression="EquipmentName" HeaderText="Allocation Group">
                                        <ItemTemplate>     
                                            <a runat="server" title='<%# Eval("EquipmentName")%>' href='<%# LinkHelper.BuildUtilityBillingQuickLink(Container.DataItem, LinkHelper.ViewByMode.Equipment)%>'><%# StringHelper.TrimText(Eval("EquipmentName").ToString(), 20)%></a>
                                            <a runat="server" visible="false" title='<%# Eval("EquipmentName")%>' href='<%# LinkHelper.BuildUtilityBillingQuickLink(Container.DataItem, LinkHelper.ViewByMode.Equipment)%>'><%# Eval("EquipmentName")%></a>
                                        </ItemTemplate> 
                                        </asp:TemplateField>      
                                        <asp:TemplateField SortExpression="AnalysisName" HeaderText="Analysis">                               
                                        <ItemTemplate>     
                                            <a runat="server" title='<%# Eval("AnalysisTeaser")%>'  href='<%# LinkHelper.BuildUtilityBillingQuickLink(Container.DataItem, LinkHelper.ViewByMode.Analysis)%>'><%# StringHelper.TrimText(Eval("AnalysisName").ToString(), 15)%></a>
                                            <a runat="server" visible="false" title='<%# Eval("AnalysisTeaser")%>'  href='<%# LinkHelper.BuildUtilityBillingQuickLink(Container.DataItem, LinkHelper.ViewByMode.Analysis)%>'><%# Eval("AnalysisName")%></a>
                                        </ItemTemplate> 
                                        </asp:TemplateField>                                                                                                                                                                                                        
                                        <asp:TemplateField SortExpression="EndDate" HeaderText="End Date">  
                                        <ItemTemplate>
                                            <%# DateTime.Parse(Eval("EndDate").ToString()).ToShortDateString()%>
                                        </ItemTemplate> 
                                        </asp:TemplateField>    
                                        <asp:TemplateField ItemStyle-CssClass="gridAlignRight" SortExpression="AllocatedCost" HeaderText="Allocated Cost ($)">  
                                        <ItemTemplate> 
                                                <%# Eval("AllocatedCost").ToString() %>
                                        </ItemTemplate> 
                                        </asp:TemplateField> 
                                        <asp:TemplateField ItemStyle-CssClass="gridAlignRight" HeaderText="Allocated Use">  
                                        <ItemTemplate> 
                                                <%# ((((double?)Eval("ElectricAllocatedUse")) != null) ? Eval("ElectricAllocatedUse").ToString() : 
                                                        ((((double?)Eval("GasAllocatedUse")) != null) ? Eval("GasAllocatedUse").ToString() : 
                                                            ((((double?)Eval("OilAllocatedUse")) != null) ? Eval("OilAllocatedUse").ToString() : 
                                                                ((((double?)Eval("PropaneAllocatedUse")) != null) ? Eval("PropaneAllocatedUse").ToString() : 
                                                                    ((((double?)Eval("SteamAllocatedUse")) != null) ? Eval("SteamAllocatedUse").ToString() :
                                                                        ((((double?)Eval("CHWAllocatedUse")) != null) ? Eval("CHWAllocatedUse").ToString() :  
                                                                            ((((double?)Eval("WaterAllocatedUse")) != null) ? Eval("WaterAllocatedUse").ToString() :  
                                                                                ((((double?)Eval("EnergyAllocatedUse")) != null) ? Eval("EnergyAllocatedUse").ToString() : ""
                                                            ))))))))%>
                                        </ItemTemplate> 
                                        </asp:TemplateField> 
                                        <asp:TemplateField ItemStyle-CssClass="gridAlignRight" SortExpression="AllocatedFraction" HeaderText="Allocated Percent">  
                                        <ItemTemplate> 
                                                <%# Math.Round(Convert.ToDouble(Eval("AllocatedFraction").ToString()), 1) %>
                                        </ItemTemplate> 
                                        </asp:TemplateField> 
                                        <asp:TemplateField ItemStyle-CssClass="gridAlignRight" HeaderText="Rate">  
                                        <ItemTemplate> 
                                                <%# ((((double?)Eval("ElectricRate")) != null) ? Eval("ElectricRate").ToString() :
                                                        ((((double?)Eval("GasRate")) != null) ? Eval("GasRate").ToString() :
                                                            ((((double?)Eval("OilRate")) != null) ? Eval("OilRate").ToString() :
                                                                ((((double?)Eval("PropaneRate")) != null) ? Eval("PropaneRate").ToString() :
                                                                    ((((double?)Eval("SteamRate")) != null) ? Eval("SteamRate").ToString() :
                                                                        ((((double?)Eval("CHWRate")) != null) ? Eval("CHWRate").ToString() :
                                                                            ((((double?)Eval("WaterRate")) != null) ? Eval("WaterRate").ToString() :
                                                                                ((((double?)Eval("EnergyRate")) != null) ? Eval("EnergyRate").ToString() : ""
                                                            ))))))))%>
                                        </ItemTemplate> 
                                        </asp:TemplateField> 
                                        <asp:TemplateField ItemStyle-CssClass="gridAlignRight" HeaderText="Peak Use">  
                                        <ItemTemplate> 
                                                <%# ((((double?)Eval("ElectricPeakUse")) != null) ? Eval("ElectricPeakUse").ToString() :
                                                        ((((double?)Eval("GasPeakUse")) != null) ? Eval("GasPeakUse").ToString() :
                                                            ((((double?)Eval("OilPeakUse")) != null) ? Eval("OilPeakUse").ToString() :
                                                                ((((double?)Eval("PropanePeakUse")) != null) ? Eval("PropanePeakUse").ToString() :
                                                                    ((((double?)Eval("SteamPeakUse")) != null) ? Eval("SteamPeakUse").ToString() :
                                                                        ((((double?)Eval("CHWPeakUse")) != null) ? Eval("CHWPeakUse").ToString() :
                                                                            ((((double?)Eval("WaterPeakUse")) != null) ? Eval("WaterPeakUse").ToString() :
                                                                                ((((double?)Eval("EnergyPeakUse")) != null) ? Eval("EnergyPeakUse").ToString() : ""
                                                            ))))))))%>
                                        </ItemTemplate> 
                                        </asp:TemplateField> 
                                        <asp:TemplateField ItemStyle-CssClass="gridAlignRight" HeaderText="Peak Use Time">  
                                        <ItemTemplate> 
                                                <%# String.IsNullOrEmpty(Convert.ToString(Eval("PeakUseTime"))) ? "" : Convert.ToDateTime(Eval("PeakUseTime").ToString()).ToShortDateString() %>
                                        </ItemTemplate> 
                                        </asp:TemplateField>                                                                                                                                                                                                                                   
                                        <asp:TemplateField>                                           
                                            <ItemTemplate>
                                                <!-- please check gridUtility_OnDataBound, gridUtility_OnRowCommand, and equipmentNotesButton_Click if you add or move any controls below-->
                                                <div class="divIcons">
                                                <asp:LinkButton runat="server" CssClass="utilityDetails" CausesValidation="false" ToolTip="View Details" CommandArgument="Select" CommandName="Select"></asp:LinkButton>
                                                <a class="utilityReports" target="_blank" title="Goto Reports" href='Reports.aspx?reportType=<%# ReportCriteria.ReportType.Raw.ToString() %>&cid=<%# Eval("CID")%>&bid=<%# Eval("BID")%>&ecid=<%# Eval("EquipmentClassID")%>&eid=<%# Eval("EID")%>&sd=<%# DateTime.Parse(Eval("StartDate").ToString())%>'></a>
                                                </div>                                                
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:TemplateField>
                                            <ItemTemplate>                                                
                                                    <asp:LinkButton Runat="server" CausesValidation="false"
                                                        OnClientClick="return confirm('Are you sure you wish to delete this utility report permanently?');"
                                                        CommandName="Delete">Delete</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                     </Columns>        
                                 </asp:GridView>  
                                 <br />   
                            </div>                                                                                     

                            
                                                        
                            <div id="divDownloadBoxInTabControl" class="divDownloadBoxInTabControl" visible="false" runat="server">
                                    <div id="divEmail" class="divEmail" runat="server">                                    
                                        <asp:ImageButton ID="imgEmail" CssClass="imgEmail" ImageUrl="_assets/images/email-icon.jpg" OnClick="emailButton_Click" AlternateText="email" runat="server" />
                                        <asp:LinkButton ID="lnkEmail" CssClass="lnkEmail" OnClick="emailButton_Click" runat="server" Text="Email Me Report"></asp:LinkButton>
                                    </div>
                                    <div id="divPdfDownload" class="divDownload" runat="server">
                                        <asp:ImageButton ID="imgPdfDownload" CssClass="imgDownload" ImageUrl="_assets/images/pdf-icon.jpg" OnClick="downloadButton_Click" AlternateText="download" runat="server" />
                                        <asp:LinkButton ID="lnkPdfDownload" CssClass="lnkDownload" OnClick="downloadButton_Click" runat="server" Text="Download Report"></asp:LinkButton>
                                    </div>
                            </div>
                            <div id="analysisDetailsTop" visible="false" runat="server">
                                <h2>Details</h2>
                                <asp:Label ID="lblError" CssClass="errorMessage" runat="server" Text="" Visible="false"></asp:Label>                                
                                <asp:Label ID="lblDetails" CssClass="narrowMessage" runat="server" Text=""></asp:Label>  
                                <a id="lnkSetFocusMessage" runat="server"></a>
                            </div> 
                            <div id="analysisDetailsBottom" visible="false" runat="server">                                                                                                                            
                                <div id="divAnalysisClient" visible="false" runat="server" class="divFormWideAndShort">
                                    <label class="labelNarrowBold">Client Name:</label>
                                    <asp:Label CssClass="labelContent" ID="lblClientName" runat="server" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnCID" runat="server" Visible="false" />
                                </div>
                                <div class="divFormWideAndShort">
                                    <label class="labelNarrowBold">Building Name:</label>
                                    <asp:Label CssClass="labelContent" ID="lblBuildingName" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="divFormWideAndShort">
                                    <label class="labelNarrowBold">Equipment Name:</label>
                                    <asp:Label CssClass="labelContent" ID="lblEquipmentName" runat="server" Text=""></asp:Label>
                                    <asp:HiddenField ID="hdnEID" runat="server" Visible="false" />
                                </div>
                                <div class="divFormWideAndShort">
                                    <label class="labelNarrowBold">Analysis Name:</label>
                                    <asp:Label CssClass="labelContent" ID="lblAnalysisName" runat="server" Text=""></asp:Label>
                                </div>
                                <div id="divPointsContent" class="divFormWideAndShort" runat="server">
                                    <label class="labelNarrowBold">Points:</label>
                                    <asp:HyperLink ID="lnkCollapsiblePoints" runat="server" CssClass="togglePadded"><asp:Label ID="lblCollapsiblePoints" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="pointsCollapsiblePanelExtender" runat="Server"
                                            TargetControlID="pnlCollapsiblePoints"
                                            CollapsedSize="0"
                                            Collapsed="true" 
                                            ExpandControlID="lnkCollapsiblePoints"
                                            CollapseControlID="lnkCollapsiblePoints"
                                            AutoCollapse="false"
                                            AutoExpand="false"
                                            ScrollContents="false"
                                            ExpandDirection="Vertical"
                                            TextLabelID="lblCollapsiblePoints"
                                            CollapsedText="show points"
                                            ExpandedText="hide points" 
                                            /> 
                                            <asp:Panel ID="pnlCollapsiblePoints" runat="server"> 
                                                <asp:Label CssClass="labelContent" ID="lblPoints" runat="server" Text=""></asp:Label>
                                            </asp:Panel>
                                </div>
                                <div class="divFormWideAndShort">
                                    <label class="labelNarrowBold">Start Date:</label>
                                    <asp:Label CssClass="labelContent" ID="lblStartDate" runat="server" Text=""></asp:Label>
                                </div>     
                                <div class="divFormWideAndShort">
                                    <label class="labelNarrowBold">End Date:</label>
                                    <asp:Label CssClass="labelContent" ID="lblEndDate" runat="server" Text=""></asp:Label>
                                </div>                                        
                                <div id="divNotes" class="divFormWideAndShort" runat="server">
                                    <label class="labelNarrowBold">Notes:</label>
                                    <span class="richText">
                                        <asp:Literal ID="litNotes" runat="server" Text=""></asp:Literal>
                                    </span>
                                </div> 
                                <div id="divVariables" class="divFormWideAndShort" runat="server">
                                    <label class="labelNarrowBold">Variables List:</label>
                                    <asp:HyperLink ID="lnkVars" runat="server" CssClass="togglePadded"><asp:Label ID="lblVars" CssClass="toggle" runat="server"></asp:Label></asp:HyperLink>
                                    <ajaxToolkit:CollapsiblePanelExtender ID="varsCollapsiblePanelExtender" runat="Server"
                                            TargetControlID="pnlVars"
                                            CollapsedSize="0"
                                            Collapsed="true" 
                                            ExpandControlID="lnkVars"
                                            CollapseControlID="lnkVars"
                                            AutoCollapse="false"
                                            AutoExpand="false"
                                            ScrollContents="false"
                                            ExpandDirection="Vertical"
                                            TextLabelID="lblVars"
                                            CollapsedText="show variables"
                                            ExpandedText="hide variables" 
                                            /> 
                                            <asp:Panel ID="pnlVars" runat="server">    
                                            <br />
                                            <asp:Label CssClass="labelContent" ID="lblEquipmentVarsEmpty" Visible="false" runat="server"></asp:Label>                                                                                                                                                                                                                                                                                                                                                                 
                                    
                                            <asp:Repeater ID="rptEquipmentVars" Visible="false" runat="server">
                                                <HeaderTemplate>                                                                                           
                                                    <ul class="detailsListProperties">
                                                </HeaderTemplate>
                                                <ItemTemplate>                                                                                                                                                                               
                                                        <%# "<li><strong>" + Eval("EquipmentVariableDisplayName") + (String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentVariableDescription"))) ? ":</strong>" + (String.IsNullOrEmpty(Convert.ToString(Eval("Value"))) ? "" : "<dl><dt>Value: </dt><dd>" + Eval("Value").ToString() + "</dd><dt>Default Value: </dt><dd>" + Eval("BuildingSettingBasedDefaultValue").ToString()) + "</dd></dl>" : ":</strong>" + (String.IsNullOrEmpty(Convert.ToString(Eval("Value"))) ? "" : "<dl><dt>Value: </dt><dd>" + Eval("Value").ToString() + "</dd>") + "<dt>Default Value: </dt><dd>" + Eval("BuildingSettingBasedDefaultValue").ToString() + "</dd><dt>Description: </dt><dd>" + Eval("EquipmentVariableDescription")) + "</dd></dl></li>"%>                                                      
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </ul>
                                                </FooterTemplate>                                                   
                                            </asp:Repeater>  

                                            </asp:Panel>
                                </div>
                                
                                <!--Set focus to body of details-->
                                <a id="lnkSetFocusView" href="#" runat="server"></a>   
                                  
                                <div id="divGraph" class="divGraph" runat="server">
                                    <br />
                                    <cwcontrols:CustomRadBinaryImage ID="binImageGraph" runat="server" />
                                </div>
                            </div>

                            <br />

                            <div id="divAllocatedUse" visible="false" runat="server">
                                <hr />

                                <asp:Chart ID="chartCHWAllocatedUse" CssClass="radDockLarge" Visible="false" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="CHW Allocated Use (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas >
                                        <asp:ChartArea Name="chwAllocatedUseChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Use (TON-HRS)" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>

				                <asp:Chart ID="chartElectricAllocatedUse" CssClass="radDockLarge" Visible="false" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="Electric Allocated Use (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas >
                                        <asp:ChartArea Name="electricAllocatedUseChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Use (kWHr)" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>


				                <asp:Chart ID="chartEnergyAllocatedUse" CssClass="radDockLarge" Visible="false" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="Energy Allocated Use (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas >
                                        <asp:ChartArea Name="energyAllocatedUseChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Use (MBTU)" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>

                                <asp:Chart ID="chartGasAllocatedUse" CssClass="radDockLarge" Visible="false" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="Gas Allocated Use (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="gasAllocatedUseChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Use (therm)" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>

                                <asp:Chart ID="chartOilAllocatedUse" CssClass="radDockLarge" Visible="false" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="Oil Allocated Use (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas >
                                        <asp:ChartArea Name="oilAllocatedUseChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Use (gal)" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>

				                <asp:Chart ID="chartPropaneAllocatedUse" CssClass="radDockLarge" Visible="false" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="Propane Allocated Use (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas >
                                        <asp:ChartArea Name="propaneAllocatedUseChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Use (gal)" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>

				                <asp:Chart ID="chartSteamAllocatedUse" CssClass="radDockLarge" Visible="false" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="Steam Allocated Use (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas >
                                        <asp:ChartArea Name="steamAllocatedUseChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Use (kLbs)" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
								
				                <asp:Chart ID="chartWaterAllocatedUse" CssClass="radDockLarge" Visible="false" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="Water Allocated Use (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas >
                                        <asp:ChartArea Name="waterAllocatedUseChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Use (gal)" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            
                            </div>
                            <div id="divAllocatedCost" visible="false" runat="server">
                            
                                <asp:Chart ID="chartAllocatedCost" CssClass="radDockLarge" runat="server" ImageType="Png" Width="1050px" EnableViewState="true">
                                    <Titles>
                                        <asp:Title Text="Allocated Cost (Based on Above Results)" Font="Trebuchet MS, 10pt, style=Bold" ForeColor="26, 59, 105"></asp:Title>                                           
                                    </Titles>
                                    <Legends>
                                        <asp:Legend Enabled="true" Alignment="Center" BorderColor="LightGray" IsTextAutoFit="true" InterlacedRows="false" Docking="Top" Name="Legend" BackColor="Transparent" Font="Trebuchet MS, 8pt, style=Bold"></asp:Legend>
                                    </Legends>
                                    <Series></Series>
                                    <ChartAreas >
                                        <asp:ChartArea Name="allocatedCostChartArea" BackSecondaryColor="White" BackColor="WhiteSmoke" BackGradientStyle="TopBottom">                                        
                                            <AxisY Title="Allocated Cost" LineColor="64, 64, 64, 64" IsLabelAutoFit="false" IsStartedFromZero="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisY>
                                            <AxisX Title="End Date" LineColor="64, 64, 64, 64" IsLabelAutoFit="false">
                                                <LabelStyle Font="Trebuchet MS, 8pt, style=Bold" />
                                                <MajorGrid LineColor="64, 64, 64, 64" />
                                            </AxisX>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>
                            
                            </div>
                                                       
                             
                            <br />
                            <br />
    <%--                    </ContentTemplate>
                </asp:UpdatePanel>--%>
                </telerik:RadAjaxPanel>
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server"> 

                                <!--Utility Billing Report Generator-->                      
                                <h2>Report Generator</h2>   
                                <p>
                                    The report generator allows you to generate new utility billing allocation reports. Please select a building, tenant, and utility allocation in order to view and edit allocation variables.
                                </p>
                                <div>
                                    <a id="lnkSetFocusVariables" runat="server"></a>
                                    <asp:Label ID="lblVariablesError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                </div>                        
                                <div class="divForm" runat="server">   
                                    <label class="label">*Select Building:</label>    
                                    <asp:DropDownList ID="ddlVariablesBuildings" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVariablesBuildings_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                        <asp:ListItem Value="-1">Select one...</asp:ListItem>                           
                                    </asp:DropDownList> 
                                    <asp:RequiredFieldValidator ID="variablesBuildingRequiredValidator" runat="server"
                                        ErrorMessage="Building is a required field." 
                                        ControlToValidate="ddlVariablesBuildings"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="GenerateAllocation">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="variablesBuildingRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="variablesBuildingRequiredValidatorExtender" 
                                        TargetControlID="variablesBuildingRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                </div>                                                                                                                                                       
                                <div class="divForm">   
                                    <label class="label">*Select Utility Group:</label>    
                                    <asp:DropDownList ID="ddlVariablesEquipment" CssClass="dropdown" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlVariablesEquipment_OnSelectedIndexChanged" AutoPostBack="true" runat="server">    
                                        <asp:ListItem Value="-1" >Select building first...</asp:ListItem>                           
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="variablesEquipmentRequiredValidator" runat="server"
                                        ErrorMessage="Allocation Group is a required field." 
                                        ControlToValidate="ddlVariablesEquipment"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="GenerateAllocation">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="variablesEquipmentRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="variablesEquipmentRequiredValidatorExtender" 
                                        TargetControlID="variablesEquipmentRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                </div> 
                                <div class="divForm">   
                                    <label class="label">*Select Utility Allocation:</label>    
                                    <asp:DropDownList ID="ddlVariablesAnalysis" CssClass="dropdown" AppendDataBoundItems="true" runat="server">    
                                        <asp:ListItem Value="-1" >Select utility group first...</asp:ListItem>                           
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="variablesAnalysisRequiredValidator" runat="server"
                                        ErrorMessage="Analysis is a required field." 
                                        ControlToValidate="ddlVariablesAnalysis"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="GenerateAllocation">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="variablesAnalysisRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="variablesAnalysisRequiredValidatorExtender" 
                                        TargetControlID="variablesAnalysisRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                </div> 
                                <div class="divForm">    
                                    <label class="label">*Start Date:</label>
                                    <telerik:RadDatePicker ID="txtVariablesStartDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                                                                                                        
                                    <br />   
                                    <asp:RequiredFieldValidator ID="variablesStartRequiredValidator" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtVariablesStartDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="GenerateAllocation">
                                        </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="variablesStartRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="variablesStartRequiredValidatorExtender" 
                                        TargetControlID="variablesStartRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                               
                                </div>
                                <div class="divForm">  
                                    <label class="label">*End Date:</label>
                                    <telerik:RadDatePicker ID="txtVariablesEndDate" MinDate="01/01/2005" runat="server"></telerik:RadDatePicker>                                    
                                    <br />   
                                    <asp:RequiredFieldValidator ID="variablesEndRequiredValidator" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtVariablesEndDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="GenerateAllocation">
                                        </asp:RequiredFieldValidator>      
                                    <ajaxToolkit:ValidatorCalloutExtender ID="variablesEndRequiredValidatorExtender" runat="server"                                        
                                        BehaviorID="variablesEndRequiredValidatorExtender" 
                                        TargetControlID="variablesEndRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        PopupPosition="Right"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                
                                    <asp:CompareValidator ID="variablesCompareValidator" Display="None" runat="server"
                                        CssClass="errorMessage" 
                                        ErrorMessage="Invalid range."
                                        ControlToValidate="txtVariablesEndDate"
                                        ControlToCompare="txtVariablesStartDate"
                                        Type="Date"
                                        Operator="GreaterThanEqual"
                                        ValidationGroup="GenerateAllocation"
                                        SetFocusOnError="true"
                                        >
                                        </asp:CompareValidator> 
                                    <ajaxToolkit:ValidatorCalloutExtender ID="variablesCompareValidatorExtender" runat="server" 
                                        BehaviorID="variablesCompareValidatorExtender" 
                                        TargetControlID="variablesCompareValidator" 
                                        HighlightCssClass="validatorCalloutHighlight" 
                                        PopupPosition="Right"
                                        Width="175" />                                                                               
                                </div>

                                <div id="editableEquipmentVariables" visible="false" runat="server">    
                                <hr />

                                <h2>Editable Allocation Variables</h2>
                                <p>
                                    Note: Default values are established on an allocation group variable level.
                                </p>
                                <div>
                                    <asp:Label ID="lblEditableEquipmentVarsEmpty" Visible="false" runat="server"></asp:Label>                  
                                </div> 
                                <asp:Repeater ID="rptEditableEquipmentVars" Visible="false" runat="server">
                                    <HeaderTemplate>                                                                                                                                                                                                                                                                                                                                
                                    </HeaderTemplate>                                                        
                                    <ItemTemplate> 
                                        <div class="divForm">                                                                                                                  
                                            <label class="label">*<%# Eval("EquipmentVariableDisplayName") %> (<span id="spanDefaultValue" runat="server"><%# "Default Value = " + Eval("BuildingSettingBasedDefaultValue")%></span>):</label>   
                                            <asp:TextBox CssClass="textbox" ID="txtEditValue" runat="server" MaxLength="25" Text='<%# Eval("Value") %>'></asp:TextBox>
                                            <!--span used in code behind-->
                                            (<span id="spanEngUnits" runat="server"><%# Eval("BuildingSettingBasedEngUnits")%></span>)                
                                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("ID") %>' />                                                                                                                        
                                            <ajaxToolkit:FilteredTextBoxExtender ID="editValueFilteredTextBoxExtender" runat="server"
                                                Enabled='<%# !Convert.ToBoolean(Eval("IsBuildingSettingBasedEngUnitStringOrArray"))%>'
                                                TargetControlID="txtEditValue"         
                                                FilterType="Custom, Numbers"
                                                ValidChars="-." />
                                            <asp:RequiredFieldValidator ID="editValueRequiredValidator" runat="server"                                                            
                                                CssClass="errorMessage" 
                                                ErrorMessage="Required" 
                                                ControlToValidate="txtEditValue"  
                                                SetFocusOnError="true"                                                                                                                    
                                                ValidationGroup="GenerateAllocation">
                                                </asp:RequiredFieldValidator>                                                                                                                
                                        </div>
                                    </ItemTemplate>                                                        
                                    <FooterTemplate>                                                                                                                
                                        </FooterTemplate>                                                     
                                </asp:Repeater>

                                <hr />
                                                                                                      
                                <asp:LinkButton CssClass="lnkButton" ID="btnGenerateAllocation" runat="server" Text="Generate"  OnClick="generateAllocationButton_Click" ValidationGroup="GenerateAllocation"></asp:LinkButton>    
                                </div>

                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />

                            </telerik:RadPageView>                                                                
                        </telerik:RadMultiPage>
                    </div>
                                                                           
             <br />
             <br />
               
</asp:Content>