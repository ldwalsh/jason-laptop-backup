﻿using System;
using System.Collections.Generic;
using CW.Business;
using CW.Data.AzureStorage;
using CW.Utility;
using CW.Website._framework;
using CW.Data;
using CW.Data.Models;
using CW.Data.Models.Diagnostics;
using System.Threading.Tasks;

namespace CW.Website._projects
{
    public sealed class ProjectsBinder
    {
        #region CLASS

            [Serializable()]
            public sealed class 
                BindingParameters
            {
                public Int32 cid;
                public List<int> bids;
                public Int32? equipmentClassID;
                public Int32? equipmentID;
                public Int32? analysisID;                
                //public DateTime? createdFrom;
                //public DateTime? createdTo;
                public DateTime startFrom;
                public DateTime startTo;
                public Int32? projectStatusID;
                public String projectPriority;
                public String filter;
                public Int32? lcid;

                public static Int32? ToInt(String value)
                {
                    if (value.ToLower() == "all" || value == "0" || value == "-1") return null;

                    return Int32.Parse(value);
                }
                public static String ToString(String value)
                {
                    if (value.ToLower() == "all" || value == "0") return null;

                    return value;
                }
            }

        #endregion

        private readonly
                SiteUser
                siteUser;

        private readonly
                DataManager
                dataManager;

        public ProjectsBinder(SiteUser siteUser, DataManager dataManager)
        {
            this.siteUser = siteUser;
            this.dataManager = dataManager;
        }
    }
}