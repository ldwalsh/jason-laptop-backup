﻿using System;
using CW.Data.AzureStorage;
using CW.Utility;
using CW.Data;
using CW.Utility.Web;

namespace CW.Website._projects
{
    public sealed class ProjectsParamsObject: QueryString.IParamsObject
    {
        #region QueryString.IParamsObject

            Boolean QueryString.IParamsObject.RequirePropertyAttribute
            {
                get {return false;}
            }

        #endregion

        #pragma warning disable 0649

        [
        QueryString.Property(Optional = true)
        ]
        public Int32
            tab;

        //[
        //QueryString.Property(Optional = true)
        //]
        //public  DateTime?
        //        cfd;

        //[
        //QueryString.Property(Optional=true)
        //]
        //public  DateTime?
        //        ctd;

        [
        QueryString.Property(Optional = true)
        ]
        public DateTime
                sfd;

        [
        QueryString.Property(Optional = true)
        ]
        public DateTime?
                std;
            
        public  Int32
                cid;

        [
        QueryString.Property(Optional = true, Requires = "cid")
        ]
        public  Int32?
                bid;

        [
        QueryString.Property(Optional=true, Requires="bid")
        ]
        public  Int32?
                ecid;

        [
        QueryString.Property(Optional=true, Requires="ecid")
        ]
        public  Int32?
                eid;

        [
        QueryString.Property(Optional=true, Requires="eid")
        ]
        public  Int32?
                aid;

        [
        QueryString.Property(Optional = true)
        ]
        public Int32?
                lcid;

        #pragma warning restore 0649
    }
}