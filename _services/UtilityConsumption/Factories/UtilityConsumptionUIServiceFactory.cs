﻿using CW.Business;
using CW.Website._services.PerformanceDashboard.Factories;
using CW.Website._services.PerformanceDashboard.Interfaces;
using CW.Website._services.UtilityConsumption.Interfaces;
using CW.Website._services.UtilityConsumption.Models;

namespace CW.Website._services.UtilityConsumption.Factories
{
    public class UtilityConsumptionUIServiceFactory
    {
         #region field(s)

            IAnalysisTypesDDLService _analysisTypesDDLSvc;

            IUtilitiesDDLService _utilitiesDDLSvc;

        #endregion

        #region constructor(s)

            public UtilityConsumptionUIServiceFactory(DataManager dataMgr, UtilityConsumptionCriteriaUIModel model)
            {
                _analysisTypesDDLSvc = new AnalysisTypesDDLServiceFactory(model).GetAnalysisTypesDDLService();

                _utilitiesDDLSvc = new UtiltiesDDLServiceFactory(dataMgr, model).GetUtilitiesDDLService();
            }

        #endregion

        #region method(s)

            public IUtilityConsumptionUIService GetUtilitiesConsumptionUIService()
            {
                return new UtilityConsumptionUIService(_analysisTypesDDLSvc, _utilitiesDDLSvc);
            }

        #endregion
    }
}