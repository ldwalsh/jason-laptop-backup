﻿using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.SearchCriteria.Models;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._services.UtilityConsumption.Models
{
    // <summary>
    /// Model to hold all utility consumption widget specific filter related values
    /// </summary>
    public sealed class UtilityConsumptionCriteriaUIModel : WidgetCriteriaUIModel
    {
        #region constructor(s)

            public UtilityConsumptionCriteriaUIModel(SearchCriteriaUIModel model) : base(model) { }

        #endregion

        #region properties

            public CheckBox WeatherCHK { get; set; }

            public HtmlGenericControl TotalConsumption { get; set; }

            public HtmlGenericControl PeakMonthConsumption { get; set; }

            public HtmlGenericControl GreatestBuildingConsumption { get; set; }

        #endregion
    }
}