﻿namespace CW.Website._services.UtilityConsumption.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for Utility consumption widget
    /// </summary>
    public interface IUtilityConsumptionUIService
    {
        /// <summary>
        /// Get the necessary information and wire up the UI with its data
        /// </summary>
        void WireUpUI();
    }
}