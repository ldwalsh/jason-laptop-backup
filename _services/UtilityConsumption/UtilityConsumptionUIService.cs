﻿using CW.Website._services.PerformanceDashboard.Interfaces;
using CW.Website._services.UtilityConsumption.Interfaces;

namespace CW.Website._services.UtilityConsumption
{
    /// <summary>
    /// Implements <see cref="IUtilityConsumptionUIService"/>.
    /// </summary>
    public class UtilityConsumptionUIService : IUtilityConsumptionUIService
    {
        #region field(s)

            IAnalysisTypesDDLService _analysisTypesDDLSvc;

            IUtilitiesDDLService _utiltiesDDLSvc;

        #endregion

        #region constructor(s)

            public UtilityConsumptionUIService(IAnalysisTypesDDLService analysisTypesDDLSvc, IUtilitiesDDLService utiltiesDDLSvc)
            {
                _analysisTypesDDLSvc = analysisTypesDDLSvc;

                _utiltiesDDLSvc = utiltiesDDLSvc;
            }

        #endregion

        #region interface method(s)

            /// <summary>
            /// Wires up all the appropriate UI elements for the utility consumption widget
            /// </summary>
            public void WireUpUI()
            {
                _analysisTypesDDLSvc.WireUpAnalysisTypes();

                _utiltiesDDLSvc.WireUpUtilities();
            }

        #endregion
    }
}