﻿namespace CW.Website._services.PerformanceDashboard.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for utilities
    /// </summary>
    public interface IUtilitiesDDLService
    {
        #region method(s)

            /// <summary>
            /// Get the utility information and wire up the UI with its data
            /// </summary>
            void WireUpUtilities();

        #endregion
    }
}