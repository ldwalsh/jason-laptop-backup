﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW.Website._services.PerformanceDashboard.Interfaces
{
    public interface IByAreaDDLService
    {
        #region method(s)

        /// <summary>
        /// Get the by area ddl.
        /// </summary>
        void WireUpByArea();

        /// <summary>
        /// Refreshes visibility;
        /// </summary>
        void RefreshVisibility();

        #endregion
    }
}
