﻿namespace CW.Website._services.PerformanceDashboard.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for analysis types
    /// </summary>
    public interface IAnalysisTypesDDLService
    {
        #region method(s)

            /// <summary>
            /// Get the analysis types information and wire up the UI with its data
            /// </summary>
            void WireUpAnalysisTypes();

        #endregion
    }
}