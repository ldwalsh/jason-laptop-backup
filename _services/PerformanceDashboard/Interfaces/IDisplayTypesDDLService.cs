﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW.Website._services.PerformanceDashboard.Interfaces
{
    public interface IDisplayTypesDDLService
    {
        #region method(s)

        /// <summary>
        /// Get the dsiplay types information and wire up the UI with its data
        /// </summary>
        void WireUpDisplayTypes(IDictionary<string, string> items);

        #endregion
    }
}
