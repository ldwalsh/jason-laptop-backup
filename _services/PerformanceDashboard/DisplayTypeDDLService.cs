﻿using CW.Common.Constants;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.PerformanceDashboard
{
    public class DisplayTypeDDLService : IDisplayTypesDDLService
    {
        #region field(s)

        WidgetCriteriaUIModel _model;

        #endregion

        #region constructor(s)

        public DisplayTypeDDLService(WidgetCriteriaUIModel model) { _model = model; }

        #endregion

        #region interface method(s)

        /// <summary>
        /// Wires up the display type types drop down with preset data
        /// </summary>
        public void WireUpDisplayTypes(IDictionary<string, string> items)
        {           
            SetDisplayTypesDDL(items);
        }

        #endregion

        #region method(s)


        /// <summary>
        /// Convenience method to bind the preset display type items to the display types ddl
        /// </summary>
        /// <param name="items">Dictionary of preset analysis type items</param>
        void SetDisplayTypesDDL(IDictionary<string, string> items)
        {
            var displayTypesDDL = _model.DisplayModeDDL;

            if (displayTypesDDL == null) return;

            displayTypesDDL.Items.Clear();

            displayTypesDDL.DataSource = items;

            displayTypesDDL.DataTextField = "Key";

            displayTypesDDL.DataValueField = "Value";

            displayTypesDDL.DataBind();
        }

        #endregion
    }
}