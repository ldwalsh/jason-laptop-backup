﻿using CW.Business;
using CW.Common.Constants;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._services.PerformanceDashboard
{
    /// <summary>
    /// Implements <see cref="IUtilitiesDDLService"/>.
    /// </summary>
    public class UtilitiesService : IUtilitiesDDLService
    { 
        #region field(s)

            DataManager _dataMgr;

            WidgetCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public UtilitiesService(DataManager dataMgr, WidgetCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _model = model;
            }

        #endregion

        #region inteface method(s)

            /// <summary>
            /// Wires up the utilities drop down with point class data
            /// </summary>
            public void WireUpUtilities()
            {
                var items = GetUtilityItems();

                SetUtiltiesDDL(items);
            }

        #endregion

        #region method(s)

            /// <summary>
            /// Convenience method to get utility items from point class data
            /// </summary>
            /// <returns>Dictionary of utility items</returns>
            IDictionary<string, string> GetUtilityItems()
            {
                var utilityItems = new Dictionary<string, string>();

                var bids = _model.BuildingsLBE.Items.OfType<ListItem>()
                                              .Where(i => i.Value != "0")
                                              .Select(i => int.Parse(i.Value))
                                              .ToArray();
                
                utilityItems.Add("All Utilities", BusinessConstants.Dropdowns.DefaultAllIndex);

                var pointClasses =_dataMgr.PointClassDataMapper.GetPointClassesByBIDsAndPCIDs
                (
                    bids, 
                    BusinessConstants.PointClass.KioskTopLevelPointClassIDs
                ).GroupBy(pc => pc.PointClassName).ToList();

                pointClasses.ForEach(pc => utilityItems.Add(pc.Key, pc.First().PointClassID.ToString()));
            
                return utilityItems;
            }

            /// <summary>
            /// Convenience method to bind the utility items to the utility ddl
            /// </summary>
            /// <param name="items">Dictionary of utility items</param>
            void SetUtiltiesDDL(IDictionary<string, string> items)
            {
                var utilitiesDDL = _model.UtilitiesDDL;
            
                utilitiesDDL.DataSource = items;

                utilitiesDDL.DataTextField = "Key";

                utilitiesDDL.DataValueField = "Value";

                utilitiesDDL.DataBind();
            }

        #endregion
    }
}