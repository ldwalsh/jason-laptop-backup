﻿using CW.Common.Constants;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.PerformanceDashboard
{
    public class ByAreaDDLService : IByAreaDDLService
    {
        #region field(s)

        WidgetCriteriaUIModel _model;

        #endregion

        #region constructor(s)

        public ByAreaDDLService(WidgetCriteriaUIModel model) { _model = model; }

        #endregion

        #region interface method(s)

        /// <summary>
        /// Wires up the by area ddl
        /// </summary>
        public void WireUpByArea()
        {
            var items = GetByAreaItems();

            SetByAreaDDL(items);
        }

        public void RefreshVisibility()
        {
            var byAreaDDL = _model.AreaDDL;

            var ddlAnalysisType = _model.AnalysisTypesDDL;

            if (byAreaDDL == null || ddlAnalysisType == null) return;

            byAreaDDL.Visible = ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Trees ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Cars ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Cost ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Energy ||
                ddlAnalysisType.SelectedValue == BusinessConstants.Dropdowns.AnalysisType.Carbon;
        }

        #endregion

        #region method(s)

        /// <summary>
        /// Convenience method to get by area items
        /// </summary>
        /// <returns>Preset analysis types dictionary</returns>
        IDictionary<string, string> GetByAreaItems()
        {
            var items = new Dictionary<string, string>();

            items.Add("-----", BusinessConstants.Dropdowns.ByArea.None);

            items.Add("By Area", BusinessConstants.Dropdowns.ByArea.Area);

            return items;
        }

        /// <summary>
        /// Convenience method to bind the preset area items to the area ddl
        /// </summary>        
        void SetByAreaDDL(IDictionary<string, string> items)
        {
            var byAreaDDL = _model.AreaDDL;

            if (byAreaDDL == null) return;

            byAreaDDL.DataSource = items;

            byAreaDDL.DataTextField = "Key";

            byAreaDDL.DataValueField = "Value";

            byAreaDDL.DataBind();
        }

        #endregion
    }
}