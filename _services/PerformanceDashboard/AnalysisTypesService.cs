﻿using CW.Common.Constants;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Interfaces;
using System.Collections.Generic;

namespace CW.Website._services.PerformanceDashboard
{
    /// <summary>
    /// Implements <see cref="IAnalysisTypesDDLService"/>.
    /// </summary>
    public class AnalysisTypesService : IAnalysisTypesDDLService
    {
        #region field(s)

            WidgetCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public AnalysisTypesService(WidgetCriteriaUIModel model) { _model = model; }

        #endregion

        #region interface method(s)

            /// <summary>
            /// Wires up the analysis types drop down with preset data
            /// </summary>
            public void WireUpAnalysisTypes()
            {
                var items = GetAnalysisTypeItems();

                SetAnalysisTypesDDL(items);
            }

        #endregion

        #region method(s)

            /// <summary>
            /// Convenience method to get analysis types from preset data
            /// </summary>
            /// <returns>Preset analysis types dictionary</returns>
            IDictionary<string, string> GetAnalysisTypeItems()
            {
                var items = new Dictionary<string, string>();

                items.Add("Avoidable Costs", BusinessConstants.Dropdowns.AnalysisType.AvoidableCosts);

                items.Add("Carbon", BusinessConstants.Dropdowns.AnalysisType.Carbon);

                items.Add("Cooling Efficiency", BusinessConstants.Dropdowns.AnalysisType.CoolingEfficiency);

                items.Add("Cost", BusinessConstants.Dropdowns.AnalysisType.Cost);

                items.Add("Energy", BusinessConstants.Dropdowns.AnalysisType.Energy);

                items.Add("Heating Efficiency", BusinessConstants.Dropdowns.AnalysisType.HeatingEfficiency);

                items.Add("# Cars", BusinessConstants.Dropdowns.AnalysisType.Cars);

                items.Add("# Trees", BusinessConstants.Dropdowns.AnalysisType.Trees);

                items.Add("Process Water Efficiency", BusinessConstants.Dropdowns.AnalysisType.ProcessWaterEfficiency);

                items.Add("Ventilation Efficiency", BusinessConstants.Dropdowns.AnalysisType.VentilationEfficiency);

                items.Add("Water Use", BusinessConstants.Dropdowns.AnalysisType.WaterUse);

                return items;
            }

            /// <summary>
            /// Convenience method to bind the preset analysis type items to the analysis types ddl
            /// </summary>
            /// <param name="items">Dictionary of preset analysis type items</param>
            void SetAnalysisTypesDDL(IDictionary<string, string> items)
            {
                var analysisTypesDDL = _model.AnalysisTypesDDL;
            
                analysisTypesDDL.DataSource = items;

                analysisTypesDDL.DataTextField = "Key";

                analysisTypesDDL.DataValueField = "Value";

                analysisTypesDDL.DataBind();
            }

        #endregion
    }
}