﻿using CW.Website._controls.performance;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.SearchCriteria.Models;

namespace CW.Website._services.PerformanceDashboard.Models
{
    public class EnergyChartUICriteriaModel : WidgetCriteriaUIModel
    {
        #region constructor(s)

            public EnergyChartUICriteriaModel(SearchCriteriaUIModel model) : base(model) { }

        #endregion

        #region method(s)

            public DropdownPanelDataModel EnergyDDLData { get; set; }

        #endregion
    }
}