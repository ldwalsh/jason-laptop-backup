﻿using CW.Website._services.SearchCriteria.Models;
using System.Web.UI.WebControls;

namespace CW.Website._models.PerformanceDashboard.Models
{
    /// <summary>
    /// Model to hold all common widget filter related values
    /// </summary>
    public class WidgetCriteriaUIModel : SearchCriteriaUIModel
    {
        public DropDownList AnalysisTypesDDL { get; set; }

        public CheckBox AreaCHK { get; set; }

        public DropDownList AreaDDL { get; set; }

        public DropDownList UtilitiesDDL { get; set; }

        public DropDownList DisplayModeDDL { get; set; }

        public WidgetCriteriaUIModel(SearchCriteriaUIModel model)
        {
            if (model != null)
            {
                BuildingTypesDDL = model.BuildingTypesDDL;

                BuildingGroupsLBE = model.BuildingGroupsLBE;

                BuildingsLBE = model.BuildingsLBE;

                RangesDDL = model.RangesDDL;

                UnitsDDL = model.UnitsDDL;

                CurrenciesDDL = model.CurrenciesDDL;
            }
        }
    }
}