﻿using CW.Website._controls.performance;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.SearchCriteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website._services.PerformanceDashboard.Models
{
    public class SEUChartCriteriaUIModel : WidgetCriteriaUIModel
    {
        public DropdownPanelDataModel ddlData { get; set; }
        public SEUChartCriteriaUIModel(SearchCriteriaUIModel searchCriteria)
            : base(searchCriteria) { }
    }
}