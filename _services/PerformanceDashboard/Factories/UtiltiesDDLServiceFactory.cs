﻿using CW.Business;
using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Interfaces;

namespace CW.Website._services.PerformanceDashboard.Factories
{
    public class UtiltiesDDLServiceFactory
    {
        #region field(s)

            DataManager _dataMgr;

            WidgetCriteriaUIModel _model;

        #endregion
        
        #region constructor(s)

            public UtiltiesDDLServiceFactory(DataManager dataMgr, WidgetCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _model = model;
            }

        #endregion

        #region method(s)

            public IUtilitiesDDLService GetUtilitiesDDLService() => new UtilitiesService(_dataMgr, _model);

        #endregion
    }
}