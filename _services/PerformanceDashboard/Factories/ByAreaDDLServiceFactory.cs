﻿using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.PerformanceDashboard.Factories
{
    public class ByAreaDDLServiceFactory
    {
        #region field(s)

        WidgetCriteriaUIModel _model;

        #endregion

        #region constructor(s)

        public ByAreaDDLServiceFactory(WidgetCriteriaUIModel model) { _model = model; }

        #endregion

        #region method(s)

        public IByAreaDDLService GetByAreaDDLService() => new ByAreaDDLService(_model);

        #endregion
    }
}