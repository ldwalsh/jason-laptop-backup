﻿using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.PerformanceDashboard.Factories
{
    public class DisplayTypesDDLServiceFactory
    {
        #region field(s)

        WidgetCriteriaUIModel _model;        

        #endregion

        #region constructor(s)

        public DisplayTypesDDLServiceFactory(WidgetCriteriaUIModel model) { _model = model; }

        #endregion

        #region method(s)

        public IDisplayTypesDDLService GetDisplayTypeDDLService() => new DisplayTypeDDLService(_model);

        #endregion
    }
}