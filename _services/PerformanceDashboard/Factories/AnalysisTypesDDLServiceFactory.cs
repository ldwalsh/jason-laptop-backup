﻿using CW.Website._models.PerformanceDashboard.Models;
using CW.Website._services.PerformanceDashboard.Interfaces;

namespace CW.Website._services.PerformanceDashboard.Factories
{
    public class AnalysisTypesDDLServiceFactory
    {
        #region field(s)

            WidgetCriteriaUIModel _model;

        #endregion
        
        #region constructor(s)

            public AnalysisTypesDDLServiceFactory(WidgetCriteriaUIModel model) { _model = model; }

        #endregion

        #region method(s)

            public IAnalysisTypesDDLService GetAnalysisTypesDDLService() => new AnalysisTypesService(_model);

        #endregion
    }
}