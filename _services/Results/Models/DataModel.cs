﻿using System;

namespace CW.Website._services.Results.Models
{
    public sealed class DataModel
    {
        #region method(s)

            public int BID { get; set; }

            public int EID { get; set; }

            public DateTime TimePeriod { get; set; }

            //represents whether this specific data point has all the data
            //e.g. if there's 10 equipment in the query, must have 10/10 equipment
            //for this data point to be true
            public bool HasAllData { get; set; }

            public double? Value { get; set; }

        #endregion
    }
}