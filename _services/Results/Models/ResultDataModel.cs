﻿using System.Collections.Generic;

namespace CW.Website._services.Results.Models
{
    public sealed class ResultDataModel
    {
        #region method(s)

            public string Name { get; set; }

            public bool IsPredicted { get; set; }

            public DataModel[] Data { get; set; }

        #endregion
    }
}