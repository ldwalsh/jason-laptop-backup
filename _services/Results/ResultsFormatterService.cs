﻿using CW.Website._services.MetricCalculation.Helpers;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Interfaces;
using CW.Website._services.Results.Models;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System.Collections.Generic;
using System.Linq;
using static CW.Website._services.MetricCalculation.Models.MetricCalcServiceConfigModel;

namespace CW.Website._services.Results
{
    public sealed class ResultsFormatterService : IResultsFormatterService
    {
        #region field(s)

            MetricCalcServiceConfigModel _configModel;

            IDataService _dataSvc;

        #endregion

        #region constructor(s)

            public ResultsFormatterService(MetricCalcServiceConfigModel configModel, IDataService dataSvc)
            {
                _configModel = configModel;

                _dataSvc = dataSvc;
            }

        #endregion

        #region interface method(s)

            public List<ResultDataModel> FormatResults()
            {
                var results = _dataSvc.GetData();

                if (results == null || !results.Any()) return null;

                var formattedResults = new List<ResultDataModel>();

                foreach (var result in results)
                {
                    var formattedResult = GetGroupedData(result);

                    formattedResults.Add(formattedResult);
                }

                return formattedResults;
            }

        #endregion

        #region method(s)

            ResultDataModel GetGroupedData(VDataQueryResult result)
            {
                var dataModel = new ResultDataModel
                {
                    Name = result.ResultName,

                    IsPredicted = result.IsPredicted
                };

                var dataHelper = new DataPointHelper(_configModel.Multiplier);

                //TODO: add switch for by equipment (and whatever else we need)
                switch (_configModel.GroupBy)
                {
                    case Grouping.Building:

                        var buildingGrouping = result.Data.GroupBy(_ => new { _.ID, _.StartDate }).ToList();

                        var buildingData = buildingGrouping.Select(_ => new DataModel
                        {
                            BID = _.Key.ID,
                            TimePeriod = _.Key.StartDate,
                            Value = _.Select(vd => dataHelper.GetConvertedValue(vd.RawValue)).Sum(),
                            HasAllData = _.Select(vd => dataHelper.IsValidDouble(vd.RawValue)).Count() == result.NumQueryItems
                        }).OrderBy(_ => new { _.BID, _.TimePeriod }).ToArray();

                        dataModel.Data = buildingData;

                        return dataModel;

                    default:

                        var defaultGrouping = result.Data.GroupBy(_ => _.StartDate).ToList();

                        var defaultData = defaultGrouping.Select(_ => new DataModel
                        {
                            TimePeriod = _.Key,
                            Value = _.Select(vd => dataHelper.GetConvertedValue(vd.RawValue)).Sum(),
                            HasAllData = _.Select(vd => dataHelper.IsValidDouble(vd.RawValue)).Count() == result.NumQueryItems,

                        }).OrderBy(_ => _.TimePeriod).ToArray();

                        dataModel.Data = defaultData;

                        return dataModel;
                }
            }

        #endregion
    }
}