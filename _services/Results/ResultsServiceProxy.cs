﻿using CW.Website._services.MetricCalculation;
using CW.Website._services.MetricCalculation.Factories;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Factories;
using CW.Website._services.VDataRetrieval.Factories;

namespace CW.Website._services.Results
{
    public sealed class ResultsServiceProxy
    {
        #region method(s)

            public IMetricCalculationService GetCalculationService(MetricCalcServiceConfigModel model)
            {
                var dataSvc = new DataServiceFactory().GetDataService(model);

                var resultsFormatterFactory = new ResultsFormatterServiceFactory();

                var resultsFormatterSvc = resultsFormatterFactory.GetResultsFormatterService(model, dataSvc);

                return new MetricCalculationServiceFactory().GetCalculationService(model, resultsFormatterSvc);
            }

        #endregion
    }
}