﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Interfaces;
using CW.Website._services.VDataRetrieval.Interfaces;

namespace CW.Website._services.Results.Factories
{
    public sealed class ResultsFormatterServiceFactory
    {
        #region method(s)

            public IResultsFormatterService GetResultsFormatterService
            (
                MetricCalcServiceConfigModel model,
                IDataService dataSvc
            )
            {
                return new ResultsFormatterService(model, dataSvc);
            }
           
        #endregion
    }
}