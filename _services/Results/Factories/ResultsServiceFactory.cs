﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Interfaces;

namespace CW.Website._services.Results.Factories
{
    public sealed class ResultsServiceFactory
    {
        #region method(s)

            public IResultsService GetResultsService(MetricCalcServiceConfigModel model)
            {
                var calcSvc = new ResultsServiceProxy().GetCalculationService(model);

                return new ResultsService(calcSvc);
            }

        #endregion

    }
}