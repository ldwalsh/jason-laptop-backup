﻿using CW.Website._services.Results.Models;
using System.Collections.Generic;

namespace CW.Website._services.Results.Interfaces
{
    public interface IResultsFormatterService
    {
        #region method(s)

            List<ResultDataModel> FormatResults();

        #endregion
    }
}