﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Models;
using System.Collections.Generic;

namespace CW.Website._services.Results.Interfaces
{
    public interface IResultsService
    {
        #region method(s)

            List<ResultDataModel> GetResults();

        #endregion
    }
}