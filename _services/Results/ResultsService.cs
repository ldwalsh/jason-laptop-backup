﻿using CW.Website._services.MetricCalculation;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Interfaces;
using CW.Website._services.Results.Models;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._services.Results
{
    public sealed class ResultsService : IResultsService
    {
        #region field(s)

            IMetricCalculationService _calcSvc;

        #endregion

        #region constructor(s)

            public ResultsService(IMetricCalculationService calcSvc)
            {
                _calcSvc = calcSvc;
            }

        #endregion

        #region interface method(s)

            public List<ResultDataModel> GetResults()
            {
                var results = _calcSvc.CalculateResults();

                if (results == null || !results.Any()) return null;

                // var results = new List<MetricResultModel>();

                // var result = new MetricResultModel();

                // result.Name = _configModel.Name;

                ////??? results.isPredicted = isPredicted; //???

                // //result.data = _resultsModel.ToList();

                // results.Add(result);

                return results;// _resultsModel;
            }

        #endregion
    }
}