﻿using CW.Business;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.MetricHandler.Interfaces;
using CW.Website._services.MetricHandler.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Linq;
using static CW.Website._services.MetricCalculation.Models.MetricCalcServiceConfigModel;

namespace CW.Website._services.MetricHandler
{
    /// <summary>
    /// Service that serializes/deserializes the metric config model.
    /// </summary>
    public class MetricConfigSerializer : IMetricConfigSerializer
    {
        #region Interface Method(s)

        /// <summary>
        /// Serializes metric config model into json string.
        /// </summary>
        /// <param name="model">The metric config model.</param>
        /// <returns>Serialized json string.</returns>
        public string Serialize(MetricCalcServiceConfigModel model)
        {          
            MetricQueryModel queryParams = getQueryParams(model);

            if (queryParams == null) return null;

            return JsonConvert.SerializeObject(queryParams);
        }

        /// <summary>
        /// Deserializes queryparams from httpcontext into metric config model.
        /// </summary>
        /// <param name="queryParams">The name value collection of query params from the http request</param>
        /// <param name="dmgr">The data manager.</param>
        /// <returns>The deserialized metric config model.</returns>
        public MetricCalcServiceConfigModel Deserialize(NameValueCollection queryParams, DataManager dmgr)
        {
            if (queryParams == null || dmgr == null) return null;

            MetricCalcServiceConfigModel metricConfig = new MetricCalcServiceConfigModel();
            
            double multiplier = 0.0;

            metricConfig.DataManager = dmgr;
            metricConfig.BIDS = parseIntArray(queryParams["BIDS[]"]);
            metricConfig.AnalysisRange = queryParams["AnalysisRange"];
            metricConfig.StartDate = DateTime.Parse(queryParams["StartDate"]);
            metricConfig.EndDate = DateTime.Parse(queryParams["EndDate"]);
            metricConfig.GetPredictedData = queryParams["GetPredictedData"] == "true" ? true : false;
            metricConfig.DataType = getDataType(queryParams["AnalysisType"]);
            metricConfig.Multiplier = queryParams["Multiplier"] != null && Double.TryParse(queryParams["Multiplier"], out multiplier) ? multiplier : 1.0;
            metricConfig.EquipmentClassID = parseNullableInt(queryParams["EquipmentClassID"]);
            metricConfig.EquipmentID = parseNullableInt(queryParams["EquipmentID"]); 
            metricConfig.UtilityTypeID = parseNullableInt(queryParams["UtilityTypeID"]); 
            metricConfig.SubmeterID = parseNullableInt(queryParams["SubmeterID"]);
            metricConfig.ConsumptionPointClassID = parseNullableInt(queryParams["ConsumptionPointClassID"]); 
            metricConfig.PlantID = parseNullableInt(queryParams["PlantID"]); 
            metricConfig.EntityName = queryParams["pointName"];

            return metricConfig;
        }

        #endregion

        #region Method(s)

        /// <summary>
        /// Maps a metric service config model to a generic query model with only the 
        /// (serializable) fields that need to be passed to the client side.
        /// 
        /// TODO: is there a more elegant way? Automapper? Custum contract resolvers and 
        /// deal directly with metric config model?
        /// </summary>
        /// <param name="metricConfig">Metric service config model</param>
        /// <returns>Generic serializable query model.</returns>
        private MetricQueryModel getQueryParams(MetricCalcServiceConfigModel metricConfig)
        {
            if (metricConfig == null) return null;

            MetricQueryModel model = new MetricQueryModel();

            model.BIDS = metricConfig.BIDS;
            model.AnalysisRange = metricConfig.AnalysisRange;
            model.Name = metricConfig.Name;
            model.StartDate = metricConfig.StartDate;
            model.EndDate = metricConfig.EndDate;
            model.GetPredictedData = metricConfig.GetPredictedData;
            model.AnalysisType = metricConfig.DataType.ToString();
            model.Units = metricConfig.Units.ToString();
            model.Multiplier = metricConfig.Multiplier;
            model.EquipmentClassID = metricConfig.EquipmentClassID;
            model.EquipmentID = metricConfig.EquipmentID;
            model.UtilityTypeID = metricConfig.UtilityTypeID;
            model.SubmeterID = metricConfig.SubmeterID;
            model.ConsumptionPointClassID = metricConfig.ConsumptionPointClassID;
            model.PlantID = metricConfig.PlantID;
            model.EntityName = metricConfig.EntityName;

            return model;
        }

        /// <summary>
        /// Parses the conusmption/analysis data type.
        /// </summary>
        private ConsumptionDataType getDataType(string type)
        {
            switch (type)
            {
                default: return ConsumptionDataType.Energy;
            }
        }

        /// <summary>
        /// Parses a json string representation of an int array into
        /// a c# int array.
        /// </summary>
        private int[] parseIntArray(string json)
        {
            if (json == null) return null;

            string[] vals = json.Split(',');

            try
            {
                int[] intArray = vals.Select(val => int.Parse(val)).ToArray();

                return intArray;
            }
            catch
            {
                return null;
            }          
        }

        /// <summary>
        /// Parses a nullable int from json string.
        /// </summary>
        private int? parseNullableInt(string json)
        {
            if (json == null) return null;

            int val;

            bool isInt = int.TryParse(json, out val);

            return isInt ? (int?)val : null;
        }

        #endregion
    }
}