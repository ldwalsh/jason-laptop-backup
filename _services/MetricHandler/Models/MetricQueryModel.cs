﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.MetricHandler.Models
{
    public class MetricQueryModel
    {        
        public int[] BIDS { get; set; }
        public string AnalysisRange { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool GetPredictedData { get; set; }
        public string AnalysisType { get; set; }
        public string Units { get; set; }
        public double Multiplier { get; set; }
        public int? EquipmentClassID { get; set; }
        public int? EquipmentID { get; set; }
        public int? UtilityTypeID { get; set; }
        public int? SubmeterID { get; set; }
        public int? ConsumptionPointClassID { get; set; }
        public int? PlantID { get; set; }
        public string EntityName { get; set; }
    }
}