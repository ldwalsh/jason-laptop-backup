﻿using CW.Website._services.Chart.Models;
using CW.Website._services.MetricCalculation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.MetricHandler.Models
{
    public class MetricResponseModel
    {                           
        public HiChartDataPointModel[] Data { get; set; }

        public string SerializedQueryParams { get; set; }
    }

}