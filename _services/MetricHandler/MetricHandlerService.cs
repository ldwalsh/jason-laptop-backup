﻿using CW.Website._services.MetricHandler.Interfaces;
using System.Web;
using CW.Website._services.MetricHandler.Models;
using CW.Business;
using CW.Website._services.Chart.Models;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.MetricHandler.Factories;

namespace CW.Website._services.MetricHandler
{
    public class MetricHandlerService : IMetricHandlerService
    {
        #region Fields

        private HttpContext _context;

        #endregion

        #region Constructor(s)
        public MetricHandlerService(HttpContext context)
        {
            _context = context;
        }
        #endregion

        #region Interface Method(s)

        /// <summary>
        /// Generates a response to the request by getting the metric
        /// calculation and putting into a response model.
        /// </summary>
        /// <returns>Response model</returns>
        public MetricResponseModel GetMetricResult()
        {                 
            DataManager dmgr = getDataManager();

            IMetricConfigSerializer metricSerializer = new MetricConfigSerializerFactory().GetSerializer();

            MetricCalcServiceConfigModel metricConfig = metricSerializer.Deserialize(_context?.Request?.QueryString, dmgr);

            if (dmgr == null || metricConfig == null) return null;            

            //todo - add metric calc service here
            //var dataSvc = new MetricCalculationServiceFactory().GetDataCalcService<DataPoint>(queryParams);

            //if (dataSvc == null) return null;

            //var results = dataSvc.CalculateResults();            

            MetricResponseModel response = new MetricResponseModel();            

            string queryParams = metricSerializer.Serialize(metricConfig);

            response.SerializedQueryParams = queryParams;

            response.Data = new HiChartDataPointModel[]
            {
                new HiChartDataPointModel { name = "equip 1", y = 13.3, drilldown = true },

                new HiChartDataPointModel { name = "equip 3", y = 18.4, drilldown = true },

                new HiChartDataPointModel { name = "equip 15", y = 18.4, drilldown = true },

                new HiChartDataPointModel { name = "equip 22", y = 18.4, drilldown = true },

                new HiChartDataPointModel { name = "equip 22", y = 18.4, drilldown = true }

            };

            return response;
        }

        #endregion

        #region Methods      

        /// <summary>
        /// Gets the data manager from the logged in user's 
        /// session.
        /// </summary>        
        /// <returns>The data manager</returns>
        private DataManager getDataManager()
        {
            if (_context?.Request?.Cookies == null) return null;

            HttpCookie cookie = _context.Request.Cookies["ASP.NET_SessionID"];

            if (cookie == null) return null;

            string key = cookie.Value;

            DataManager dmgr = DataManager.Get(key);

            return dmgr;
        }
           
        #endregion
    }
}