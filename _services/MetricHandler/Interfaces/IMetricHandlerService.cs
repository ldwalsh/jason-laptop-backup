﻿using CW.Website._services.MetricHandler.Models;

namespace CW.Website._services.MetricHandler.Interfaces
{
    /// <summary>
    /// An interface for http handlers handling metric calculation requests.
    /// </summary>
    public interface IMetricHandlerService
    {
        /// <summary>
        /// Generates a response to the request by getting the metric
        /// calculation and putting into a response model.
        /// </summary>
        /// <returns>Response model</returns>
        MetricResponseModel GetMetricResult();

    }
}
