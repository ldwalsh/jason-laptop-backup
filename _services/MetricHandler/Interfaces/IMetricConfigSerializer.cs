﻿using CW.Business;
using CW.Website._services.MetricCalculation.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW.Website._services.MetricHandler.Interfaces
{
    /// <summary>
    /// Interface for a service that serializes/deserializes the metric config
    /// model to/from json.
    /// </summary>
    public interface IMetricConfigSerializer
    {
        string Serialize(MetricCalcServiceConfigModel model);

        MetricCalcServiceConfigModel Deserialize(NameValueCollection queryParams, DataManager dmgr);
    }
}
