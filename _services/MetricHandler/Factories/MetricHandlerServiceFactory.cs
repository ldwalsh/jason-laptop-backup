﻿using CW.Website._services.MetricHandler.Interfaces;
using System;
using System.Web;

namespace CW.Website._services.MetricHandler.Factories
{
    public class MetricHandlerServiceFactory
    {
        public IMetricHandlerService GetMetricHandlerService(HttpContext context)
        {
            try
            {
                return (IMetricHandlerService)Activator.CreateInstance(typeof(MetricHandlerService), new object[] { context });
            }
            catch
            {
                return null;
            }
        }
    }
}