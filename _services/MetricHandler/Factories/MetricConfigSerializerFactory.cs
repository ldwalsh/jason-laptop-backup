﻿using CW.Website._services.MetricHandler.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.MetricHandler.Factories
{
    public class MetricConfigSerializerFactory
    {
        public IMetricConfigSerializer GetSerializer()
        {
            return new MetricConfigSerializer();
        }
    }
}