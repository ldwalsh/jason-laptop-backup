﻿using CW.Common.Constants;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.VDataRetrieval.Helpers;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval
{
    public class HeatingEfficiencyVDataRetrievalService : IDataService
    {
        private MetricCalcServiceConfigModel mConfigData;
        private int mAnalysisTypeID;
        private int[] mEquipmentTypeIDs;        
        private bool mIsSingleBoiler;
        private int[] mEquipmentIDs;

        public HeatingEfficiencyVDataRetrievalService(MetricCalcServiceConfigModel configData)
        {
            mConfigData = configData;
            configure();
        }

        private void configure()
        {
            if (mConfigData == null) return;

            //single boiler
            if (mConfigData.EquipmentID.HasValue)
            {
                mAnalysisTypeID = BusinessConstants.Analyses.BoilerID;
                mEquipmentTypeIDs = new int[] { mConfigData.EquipmentID.Value };                
                mIsSingleBoiler = true;
            }
            //multiple boilers, or one or more plants selected
            else
            {
                mAnalysisTypeID = BusinessConstants.Analyses.HeatingPlantEnergyID;
                mEquipmentTypeIDs = new int[] { BusinessConstants.EquipmentType.HeatingPlantTypeID, BusinessConstants.EquipmentType.SteamPlantTypeID };
                mIsSingleBoiler = false;
            }

            setEquipmentIDs();
        }

        /// <summary>
        /// Sets the equipment ids to query V/PreData points for.
        /// </summary>
        private void setEquipmentIDs()
        {
            //if is single boiler,  is specified, get based on bids and equiptypeid
            if (mIsSingleBoiler)
            {
                mEquipmentIDs = new int[] { (int)mConfigData.EquipmentID.Value };
            }
            //else get by bids and equipmentCLASS id
            else
            {
                List<int> equipIDs = new List<int>();

                foreach (int equipTypeID in mEquipmentTypeIDs)
                {
                    var ids = mConfigData.DataManager.EquipmentDataMapper
                        .GetAllVisibleEquipmentByBIDsAndEquipmentTypeID(mConfigData.BIDS, equipTypeID, false)                        
                        .Select(_ => _.EID).ToList();
                    if (ids != null) equipIDs.AddRange(ids);
                }

                mEquipmentIDs = equipIDs.ToArray();
            }
        }

        public List<VDataQueryResult> GetData()
        {
            bool isPredicted = false; //todo- temp to get Jason's code committe - need to change

            //get vpoints or vprepoints from sql
            List<GeneralVPointModel> vPoints = SqlVPointQueryHelper.GetGeneralizedVPoints(mConfigData.DataManager, 
                mEquipmentIDs, new int[] { mAnalysisTypeID }, isPredicted);

            if (vPoints == null) return null;

            List<VDataQueryResult> results = mIsSingleBoiler ? getSingleBoilerResults(vPoints, isPredicted) : getPlantResults(vPoints, isPredicted);

            return results;
        }

        private List<VDataQueryResult> getSingleBoilerResults(List<GeneralVPointModel> vPoints, bool isPredicted)
        {
            VDataQueryResult boilerEfficiency = getQueryResult(vPoints, "BoilerEfficiency", isPredicted);

            return new List<VDataQueryResult> { boilerEfficiency };
        }

        private List<VDataQueryResult> getPlantResults(List<GeneralVPointModel> vPoints, bool isPredicted)
        {
            List<GeneralVPointModel> heatingUseVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.HeatingUseTypeID).ToList();
            List<GeneralVPointModel> plantGasEnergyVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.PlantGasEnergyTypeID).ToList();
            List<GeneralVPointModel> plantSteamEnergyVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.PlantSteamEnergyTypeID).ToList();

            VDataQueryResult heatingUse = getQueryResult(heatingUseVPoints, "HeatingUse", isPredicted);
            VDataQueryResult plantGasEnergy = getQueryResult(plantGasEnergyVPoints, "PlantGasEnergy", isPredicted);
            VDataQueryResult plantSteamEnergy = getQueryResult(plantSteamEnergyVPoints, "PlantSteamEnergy", isPredicted);

            List<VDataQueryResult> results = new List<VDataQueryResult>();

            results.Add(heatingUse);
            results.Add(plantGasEnergy);
            results.Add(plantSteamEnergy);

            return results;            
        }

        private VDataQueryResult getQueryResult(List<GeneralVPointModel> vPoints, string resultName, bool isPredicted)
        {
            if (vPoints == null || resultName == null) return null;

            VDataQueryResult result = new VDataQueryResult();

            result.ResultName = resultName;
            result.NumQueryItems = vPoints.Count();
            result.Data = getData(vPoints, isPredicted);
            result.IsPredicted = isPredicted;

            return result;
        }

        private IEnumerable<GeneralVDataModel> getData(List<GeneralVPointModel> vPoints, bool isPredicted)
        {
            return VDataQueryHelper.GetGeneralizedVData(mConfigData.DataManager, vPoints.Select(_ => _.ID),
                mConfigData.AnalysisRange, mConfigData.StartDate, mConfigData.EndDate, isPredicted);
        }
    }
}