﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.VDataRetrieval.Helpers;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval
{
    public class CoolingEfficiencyVDataRetrievalService : VDataRetrievalServiceBase
    {
        private MetricCalcServiceConfigModel mConfigData;
        private int mAnalysisTypeID;
        private int? mEquipmentTypeID;
        private int? mEquipmentClassID;
        private int mVPointTypeIDToNullCheck;
        List<int> mEquipmentIDs;

        public CoolingEfficiencyVDataRetrievalService(MetricCalcServiceConfigModel model) : 
        base(model)
        {
            configure();                      
        }

        private void configure()
        {
            if (_model.EquipmentID.HasValue)
            {
                mAnalysisTypeID = BusinessConstants.Analyses.ChillerEfficiencyID;
                mEquipmentClassID = BusinessConstants.EquipmentClass.ChillerEquipmentClassID;
                mVPointTypeIDToNullCheck = BusinessConstants.PointType.ChillerAvgEfficiency;
            }
            else
            {
                mAnalysisTypeID = BusinessConstants.Analyses.CoolingPlantEnergyID;
                mEquipmentTypeID = BusinessConstants.EquipmentType.CoolingPlantID;
                mVPointTypeIDToNullCheck = BusinessConstants.PointType.CoolingPlantkWperTon;
            }

            setEquipmentIDs();
        }

        /// <summary>
        /// Sets the equipment ids to query V/PreData points for.
        /// </summary>
        private void setEquipmentIDs()
        {
            //if equipmenttype is specified, get based on bids and equiptypeid
            if (mEquipmentTypeID.HasValue)
            {
                mEquipmentIDs = mConfigData.DataManager.EquipmentDataMapper
                    .GetAllVisibleEquipmentByBIDSAndEquipmentTypeID(mConfigData.BIDS, (int)mEquipmentTypeID)
                    .Select(_ => _.EID).ToList();
            }
            //else get by bids and equipmentCLASS id
            else
            {
                mEquipmentIDs = mConfigData.DataManager.EquipmentDataMapper
                    .GetAllVisibleEquipmentByEquipmentClassAndBuildingIDS(mConfigData.BIDS, (int)mEquipmentClassID)
                    .Select(_ => _.EID).ToList();
            }
        }

        public override List<VDataQueryResult> GetData(bool isPredicted)
        {          
            //get vpointIDs
            List<GeneralVPointModel> vPoints = SqlVPointQueryHelper.GetGeneralizedVPoints(mConfigData.DataManager, mEquipmentIDs,
                new int[] { mAnalysisTypeID }, isPredicted);

            if (vPoints == null) return null;            

            //get vpointids for electric use and cooling use
            List<GeneralVPointModel> electricUseVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.ElectricUse).ToList();
            List<GeneralVPointModel> coolingUseVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.CoolingUse).ToList();
            //get vpoint ids for the point type we need to check is null (e.g. ChillerAvgEfficiency)
            List<GeneralVPointModel> nullCheckVPoints = vPoints.Where(_ => _.PointTypeID == mVPointTypeIDToNullCheck).ToList();

            VDataQueryResult electricUse = getQueryResult(electricUseVPoints, nullCheckVPoints, "ElectricUse", isPredicted);
            VDataQueryResult coolingUse = getQueryResult(coolingUseVPoints, nullCheckVPoints, "CoolingUse", isPredicted);

            //return results
            List<VDataQueryResult> results = new List<VDataQueryResult>();
            results.Add(electricUse);
            results.Add(coolingUse);

            return results;
        }   

        private VDataQueryResult getQueryResult(List<GeneralVPointModel> vPoints, List<GeneralVPointModel> nullCheckVPoints, 
            string resultName, bool isPredicted)
        {
            if(vPoints == null || nullCheckVPoints == null || resultName == null) return null;            

            VDataQueryResult result = new VDataQueryResult();
            result.ResultName = resultName;
            result.NumQueryItems = vPoints.Count();
            result.Data = getData(vPoints, nullCheckVPoints, isPredicted);
            result.IsPredicted = isPredicted;

            return result;
        }

        /// <summary>
        /// Gets vData or vPreData in the form of a GeneralVDataModel. 
        /// Includes the null check vpoint ids, and will exclude any vdata
        /// where that value is null for the time period. TODO: optimize?
        /// </summary>
        /// <param name="vPoints">The vPoints to query</param>
        /// <param name="nullCheckVPoints">The null check vPoints.</param>
        /// <returns></returns>
        private IEnumerable<GeneralVDataModel> getData(List<GeneralVPointModel> vPoints, List<GeneralVPointModel> nullCheckVPoints, bool isPredicted)
        {
            //create list to return
            List<GeneralVDataModel> results = new List<GeneralVDataModel>();

            //get a list of all the points by combining 1) the point type of interest (e.g. "ElectricUse") and
            // 2) the point we need to check if null (e.g. "ChillerAvgEfficiency")
            List<GeneralVPointModel> queryPoints = new List<GeneralVPointModel>();
            queryPoints.AddRange(vPoints);
            queryPoints.AddRange(nullCheckVPoints);

            //get VData for all the above VPoints
            IEnumerable<GeneralVDataModel> allVData = VDataQueryHelper.GetGeneralizedVData(mConfigData.DataManager, queryPoints.Select(_ => _.ID),
                mConfigData.AnalysisRange, mConfigData.StartDate, mConfigData.EndDate, isPredicted);                

            //group the vpoints by equipment ID
            IEnumerable<IGrouping<int, GeneralVPointModel>> equipGroups = queryPoints.GroupBy(_ => _.EID);

            //add the VData to the results to return, if it has:
            //1) a VPoint for both the regular query point of interest and the nullable check and
            //2) non-null VData for both VPoints above
            foreach(IGrouping<int, GeneralVPointModel> equipGroup in equipGroups)
            {               
                //get VData that correspond to the VPoints for this equipment
                IEnumerable<GeneralVDataModel> equipVData = allVData.Where(_ => equipGroup.Select(vp => vp.ID).Contains(_.ID));

                if (equipVData.Count() == 0) continue;

                //group VData by time period
                IEnumerable<IGrouping<DateTime, GeneralVDataModel>> groupedVData = equipVData.GroupBy(_ => _.StartDate);

                //get vpoint of interest that may have its VData added to results (if it passes checks below)
                GeneralVPointModel dataVPoint = equipGroup.Where(vp => vp.PointTypeID != mVPointTypeIDToNullCheck).FirstOrDefault();
                if (dataVPoint == null) continue;

                foreach (IGrouping<DateTime, GeneralVDataModel> group in groupedVData)
                {
                    //make sure there are two VData
                    if (group.Count() != 2) continue;

                    //ensure both vdata are not null
                    double dummy;
                    if (group.Where(_ => !double.TryParse(_.RawValue, out dummy)).Any()) continue;

                    //add vdata for the vpoint of interest to results                                 
                    GeneralVDataModel data = group.Where(_ => _.ID == dataVPoint.ID).FirstOrDefault();
                    if (data != null) results.Add(data);
                }
            }

            return results;
        }
    }
}