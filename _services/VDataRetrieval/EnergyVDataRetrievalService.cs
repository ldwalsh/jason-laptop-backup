﻿using System.Collections.Generic;
using System.Linq;
using CW.Common.Constants;
using CW.Website._services.MetricCalculation.Models;

namespace CW.Website._services.VDataRetrieval
{
    public class EnergyVDataRetrievalService : UtilityVDataRetrievalService
    {
        public EnergyVDataRetrievalService(MetricCalcServiceConfigModel configData) :
            base(configData)
        {
            
        }

        #region Virtual Overrides
        protected override int[] getEquipmentTypeIDs()
        {
            bool isOneBuilding = mConfigData.BIDS?.Count() > 1;

            if (isOneBuilding)
            {
                return BusinessConstants.EquipmentType.EnergyEquipmentTypeIDs;
            }
            else
            {
                return base.getEquipmentTypeIDs();
            }
        }
        protected override Dictionary<int, int> getPointTypeEquipTypeLookup()
        {
            return BusinessConstants.PointType.EquipTypePointTypePairsForCalculations;
        }

        #endregion
    }
}