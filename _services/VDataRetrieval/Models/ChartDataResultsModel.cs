﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.Chart.Models
{
    /// <summary>
    /// A model representing data returned from the chart data service, to be
    /// used to create/populate the chart series data in the chart service.
    /// </summary>
    public class ChartDataResultsModel
    {
        //represents a 'series'; can have multiple per results set
        //e.g. if there's predicted and actual data
        public IEnumerable<ResultSet> resultSets { get; set; }

        public class ResultSet
        {
            public string Name { get; set; }
            public bool isPreData { get; set; }
            //represents whether ALL data for ALL aggregated points exist 
            //e.g. if there are 10 equipment in the query, must have 10/10
            //equipment for ALL time periods in the query to be true      
            public bool hasAllData { get; set; }
            //false if the there's no data for the specified time range
            public bool hasAnyData { get; set; }
            public IEnumerable<DataPoint> dataPoints { get; set; }
        }

        public class DataPoint
        {
            public DateTime timePeriod;
            //represents whether this specific data point has all the data
            //e.g. if there's 10 equipment in the query, must have 10/10 equipment
            //for this data point to be true
            public bool hasAllData { get; set; }
            public double? value { get; set; }
        }
    }
}