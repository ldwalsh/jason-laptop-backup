﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval.Models
{
    /// <summary>
    /// Represents the important fields in common to both VPoint and VPrePoint. We
    /// need this "abstraction" because metric calculations use the same code for both
    /// VPoints and VPrePoints, but the fields (like ID) on the objects are named differently.
    /// </summary>
    public class GeneralVPointModel
    {
        public int ID { get; set; }
        public int EID { get; set; }
        public int PointTypeID { get; set; }
    }
}