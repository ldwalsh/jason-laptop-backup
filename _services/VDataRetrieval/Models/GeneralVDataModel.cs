﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval.Models
{
    /// <summary>
    /// Represents the important fields in common to both VData and VPreData. We
    /// need this "abstraction" because metric calculations use the same code for both
    /// VData and VPreData, but the fields (like ID) on the objects are named differently.
    /// </summary>
    public class GeneralVDataModel
    {
        public int ID { get; set; }

        public DateTime StartDate { get; set; }

        public string RawValue { get; set; }
    }
}