﻿using System.Collections.Generic;

namespace CW.Website._services.VDataRetrieval.Models
{
    /// <summary>
    /// Represents a query result containing vData and a count of 
    /// number of objects included in query.
    /// </summary>
    public class VDataQueryResult
    {
        public string ResultName { get; set; }

        //represents the number of objects (e.g. equipment) used in the query
        public int NumQueryItems { get; set; }

        public bool IsPredicted { get; set; }

        public IEnumerable<GeneralVDataModel> Data { get; set; }

    }
}