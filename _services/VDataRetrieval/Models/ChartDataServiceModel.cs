﻿using CW.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static CW.Website._services.Chart.Models.ConsumptionEnums;

namespace CW.Website._services.Chart.Models
{
    public class ConsumptionDataServiceConfigModel
    {
        public DataManager DataManager { get; set; }
        public int[] BIDS { get; set; }
        public string AnalysisRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool getPredictedData { get; set; }
        public ConsumptionDataType DataType { get; set; }
        public MetricUnit Units { get; set; }
        public int? EquipmentClassID { get; set; }
        public int? EquipmentID { get; set; }
        public int? UtilityTypeID { get; set; }
        public int? SubmeterID { get; set; }
        public int? ConsumptionPointClassID { get; set; }

        #region Enums        

        public enum MetricUnit { SI, IP };

        #endregion
    }
}
