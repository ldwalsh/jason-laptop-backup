﻿using CW.Website._services.VDataRetrieval.Models;
using System.Collections.Generic;

namespace CW.Website._services.VDataRetrieval.Interfaces
{
    /// <summary>
    /// An interface for consumption data retrieval services.
    /// </summary>
    public interface IDataService
    {
        #region method(s)

            List<VDataQueryResult> GetData();

        #endregion
    }
}