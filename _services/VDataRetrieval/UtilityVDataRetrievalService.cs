﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Website._services.Chart.Interfaces;
using CW.Website._services.Chart.Models;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.VDataRetrieval.Helpers;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval
{
    /// <summary>
    /// Base class for utility data retrieval services.
    /// </summary>
    public class UtilityVDataRetrievalService : IDataService
    {
        protected MetricCalcServiceConfigModel mConfigData;
        List<Equipment> mEquipment;
        //lookup of valid pointtype/equiptype combos
        private IDictionary<int, int> mPointTypeEquipTypeLookup;
        //lookup of equipment id and equipment type id
        IDictionary<int, int> mEquipmentTypeLookup;

        public UtilityVDataRetrievalService(MetricCalcServiceConfigModel configData)
        {
            mConfigData = configData;
            configureEquipmentSettings();
        }

        #region Overrides
        public List<VDataQueryResult> GetData()
        {
            bool isPredicted = false; //todo- temp to get Jason's code committe - need to change

            VDataQueryResult queryResult = getVDataResult(isPredicted);   
            
            return new List<VDataQueryResult> { queryResult };
        }

        #endregion


        #region Virtual Methods
        protected virtual int[] getEquipmentTypeIDs()
        {
            return new int[] { BusinessConstants.EquipmentType.WholeBuildingUtilitiesTypeID };
        }

        protected virtual Dictionary<int, int> getPointTypeEquipTypeLookup()
        {
            return mapOneEquipTypeToManyPointTypes
                (BusinessConstants.EquipmentType.WholeBuildingUtilitiesTypeID, BusinessConstants.PointType.CarbonPointTypdIDs);
        }
        #endregion

        #region Base Class Methods

        private void configureEquipmentSettings()
        {
            //get the list of equipment
            mEquipment = getEquipment();

            //retrieve lookup of valid pointtype/equiptype combos
            mPointTypeEquipTypeLookup = getPointTypeEquipTypeLookup();

            //create lookup of equipment id and equipment type id
            mEquipmentTypeLookup = mEquipment.ToDictionary(_ => _.EID, _ => _.EquipmentTypeID);
        }

        private VDataQueryResult getVDataResult(bool isPredicted)
        {
            VDataQueryResult queryResult = new VDataQueryResult();

            //get list of vpoints based on equipmentID list       
            List<int> vPointIDs = getVDataPointIDs(isPredicted);

            queryResult.NumQueryItems = vPointIDs.Count(); //equipmentIDs.Count(); TODO: need to confirm business rules
            queryResult.Data = VDataQueryHelper.GetGeneralizedVData(mConfigData.DataManager, vPointIDs, mConfigData.AnalysisRange,
                mConfigData.StartDate, mConfigData.EndDate, isPredicted);
            queryResult.IsPredicted = isPredicted;

            return queryResult;
        }

        protected List<int> getVDataPointIDs(bool isPredicted)
        {
            //get the sql vpoints from the db, filtered by valid pointtype/equipment point combos
            List<GeneralVPointModel> sqlVPoints = SqlVPointQueryHelper.GetGeneralizedVPoints(mConfigData.DataManager, mEquipment.Select(_ => _.EID),
                new int[] { BusinessConstants.Analyses.MVBuildingEnergyAID }, isPredicted);

            //if user selected a specfic consumption type, filter by that pointclass id
            if (mConfigData.ConsumptionPointClassID.HasValue)
            {
                List<int> pointTypeIDs = mConfigData.DataManager.PointTypeDataMapper.GetAllPointTypesByPointClassID(mConfigData.ConsumptionPointClassID.Value)
                    .Select(_ => _.PointTypeID).ToList();

                sqlVPoints = sqlVPoints.Where(_ => pointTypeIDs.Contains(_.PointTypeID)).ToList();
            }

            return sqlVPoints.Select(_ => _.ID).ToList();
        }

        protected List<Equipment> getEquipment()
        {
            //get the equipment types
            int[] equipmentTypeIDs = getEquipmentTypeIDs();

            List<Equipment> equipment = new List<Equipment>();

            foreach (int etid in equipmentTypeIDs)
            {
                equipment.AddRange(mConfigData.DataManager.EquipmentDataMapper.GetAllVisibleEquipmentByBIDSAndEquipmentTypeID(mConfigData.BIDS, etid).ToList());
            }

            //if user selected a specific submeter (i.e. equipment), check to make sure it's in the list and filter
            if (mConfigData.SubmeterID.HasValue)
            {
                equipment = equipment.Where(_ => _.EID == mConfigData.SubmeterID.Value).ToList();
            }

            return equipment;
        }

        protected Dictionary<int, int> mapOneEquipTypeToManyPointTypes(int equipmentTypeID, int[] pointTypeIDs)
        {
            Dictionary<int, int> lookup = new Dictionary<int, int>();

            foreach (int ptid in pointTypeIDs)
            {
                lookup.Add(ptid, equipmentTypeID);
            }

            return lookup;
        }

        private bool isValidEquipTypePointTypeCombo(int ptid, int eid, IDictionary<int, int> pointTypeEquipTypeLookup, IDictionary<int, int> equipmentTypeLookup)
        {
            if (pointTypeEquipTypeLookup == null || equipmentTypeLookup == null) return false;

            //lookup the equipment TYPE id from the piece of equipment based on the equipment's id; return false if doesn't exist
            int equipmentTypeID;
            if (!equipmentTypeLookup.TryGetValue(eid, out equipmentTypeID)) return false;

            //lookup the VALID equipment type id based on the point type id (lookup is static based on specs); return false if doesn't exist
            int validEquipmentTypeID;
            if (!pointTypeEquipTypeLookup.TryGetValue(ptid, out validEquipmentTypeID)) return false;

            //return wether or not the valid equipment type id matches the etid from the piece of equipment
            return equipmentTypeID == validEquipmentTypeID;
        }

        #endregion  
    }
}