﻿using CW.Common.Constants;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.VDataRetrieval.Models;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._services.VDataRetrieval
{
    public sealed class AvoidableCostsVDataRetrievalService : VDataRetrievalServiceBase
    {
        #region constructor(s)

            public AvoidableCostsVDataRetrievalService(MetricCalcServiceConfigModel model) :
            base(model) { }

        #endregion

        #region method(s)      

            #region override method(s)

                public override List<VDataQueryResult> GetData(bool isPredicted)
                {
                    var queryResult = new VDataQueryResult();

                    var costSavingsPointTypeID = BusinessConstants.PointType.CostSavingsPointTypeID;

                    //get equipment ids for the selected buildings
                    var equipmentIDs = GetEquipmentIDs();

                    if (equipmentIDs != null || equipmentIDs.Any())
                    {
                        //get the vpoint ids for the equipment and cost savings point type
                        var vPointIDs = _model.DataManager.VAndVPreDataMapper.GetVPointsByEquipIDsAndPointTypeID
                        (
                            equipmentIDs,
                            costSavingsPointTypeID
                        ).Select(_ => _.VPID);

                        //get the actual vdata for all the vpoints
                        var vPoints = _model.DataManager.VAndVPreDataMapper.GetVData
                        (
                            vPointIDs,
                            _model.AnalysisRange,
                            _model.StartDate,
                            _model.EndDate
                        );

                        //equipmentIDs.Count(); TODO: need to confirm business rules
                        queryResult.NumQueryItems = vPointIDs.Count();

                        queryResult.Data = vPoints.Select(_ => new GeneralVDataModel
                        {
                            ID = _.VPID,
                            StartDate = _.StartDate,
                            RawValue = _.RawValue
                        });

                        queryResult.IsPredicted = isPredicted;
                    }

                    return new List<VDataQueryResult> { queryResult };
                }

            #endregion

            /// <summary>
            /// Retrieves IDs of the equipment based on selection of buildings and equipment types/ids.
            /// </summary>
            /// <returns>List of equip ids</returns>
            List<int> GetEquipmentIDs()
            {
                //if user selected specific piece of equipment, get that equipment id
                if (_model.EquipmentID.HasValue)
                {
                    var eq = _model.DataManager.EquipmentDataMapper.GetEquipmentByID(_model.EquipmentID.Value);

                    if (eq != null)
                    {
                        var equipmentIDs = new List<int>();

                        equipmentIDs.Add(eq.EID);

                        return equipmentIDs;
                    }

                    return null;
                }

                //if user only selected equipment class, get all equipment for that class
                if (_model.EquipmentClassID.HasValue)
                {
                    return _model.DataManager.EquipmentDataMapper.GetAllVisibleEquipmentByEquipmentClassAndBuildingIDS
                    (
                        _model.BIDS,
                        _model.EquipmentClassID.Value
                    ).Select(_ => _.EID).ToList();
                }

                //get all equipment ids for the buildings
                return _model.DataManager.EquipmentDataMapper.GetAllVisibleEquipmentByBIDS(_model.BIDS)
                                                             .Select(_ => _.EID)
                                                             .Distinct()
                                                             .ToList();
            }

        #endregion  
    }
}