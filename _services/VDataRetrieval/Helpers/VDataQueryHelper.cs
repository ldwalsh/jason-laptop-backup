﻿using CW.Business;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval.Helpers
{
    public static class VDataQueryHelper
    {
        /// <summary>
        /// Retrieves the "generalized" vData or vPreData from table storage.
        /// </summary>
        /// <param name="dMgr">The data manager</param>
        /// <param name="vPointIDs">VPoint IDs to query on</param>
        /// <param name="analysisRange">Analysis range</param>
        /// <param name="startDate">Start date</param>
        /// <param name="endDate">End date</param>
        /// <param name="isPredicted">Whether to get predicted data</param>
        /// <returns>Generalized data model</returns>
        public static List<GeneralVDataModel> GetGeneralizedVData(DataManager dMgr, IEnumerable<int> vPointIDs, string analysisRange,
            DateTime startDate, DateTime endDate, bool isPredicted)
        {
            if (dMgr == null || vPointIDs == null || analysisRange == null || startDate == null || endDate == null) return null;

            if (isPredicted)
            {
                return dMgr.VAndVPreDataMapper.GetVPreData(vPointIDs, analysisRange, startDate, endDate)
                    .Select(_ => new GeneralVDataModel
                    {
                        ID = _.VPrePID,
                        StartDate = _.StartDate,
                        RawValue = _.RawValue
                    }).ToList();
            }
            else
            {
                return dMgr.VAndVPreDataMapper.GetVData(vPointIDs, analysisRange, startDate, endDate)
                    .Select(_ => new GeneralVDataModel
                    {
                        ID = _.VPID,
                        StartDate = _.StartDate,
                        RawValue = _.RawValue
                    }).ToList();
            }
        }

    }
}