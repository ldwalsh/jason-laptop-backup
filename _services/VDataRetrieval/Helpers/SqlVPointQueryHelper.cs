﻿using CW.Business;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval.Helpers
{
    public static class SqlVPointQueryHelper
    {
        /// <summary>
        /// Retrieves the "generalized" vPoints or vPrePoints from sql. 
        /// </summary>
        /// <param name="dMgr">The DataManager instance</param>
        /// <param name="equipIDs">Equipment IDs</param>
        /// <param name="analysisTypeIDs">Analysis type IDs</param>
        /// <param name="isPredicted">Whether to get vPoints or vPrePoints</param>
        /// <returns></returns>
        public static List<GeneralVPointModel> GetGeneralizedVPoints(DataManager dMgr, IEnumerable<int> equipIDs, 
            IEnumerable<int> analysisTypeIDs, bool isPredicted)
        {
            if (dMgr == null || equipIDs == null || analysisTypeIDs == null) return null;

            if (isPredicted)
            {
                return dMgr.VAndVPreDataMapper.GetVPrePoints(equipIDs, analysisTypeIDs)
                    .Select(_ => new GeneralVPointModel
                    {
                        ID = _.VPrePID,
                        EID = _.EID,
                        PointTypeID = _.PointTypeID
                    }).ToList();
            }
            else
            {
                return dMgr.VAndVPreDataMapper.GetVPoints(equipIDs, analysisTypeIDs)
                    .Select(_ => new GeneralVPointModel
                    {
                        ID = _.VPID,
                        EID = _.EID,
                        PointTypeID = _.PointTypeID
                    }).ToList();
            }
        }
    }
}