﻿using CW.Common.Constants;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.VDataRetrieval.Helpers;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval
{
    public class VentEfficiencyVDataRetrievalService : IDataService
    {
        private MetricCalcServiceConfigModel mConfigData;
        private int[] mEquipmentIDs;

        public VentEfficiencyVDataRetrievalService(MetricCalcServiceConfigModel configData)
        {
            mConfigData = configData;
            configure();
        }

        private void configure()
        {
            if (mConfigData == null || mConfigData.BIDS == null || mConfigData.DataManager == null) return;

            if (mConfigData.EquipmentID.HasValue)
            {
                mEquipmentIDs = new int[] { mConfigData.EquipmentID.Value };
            }
            else
            {
                mEquipmentIDs = mConfigData.DataManager.EquipmentDataMapper
                    .GetAllVisibleEquipmentByEquipmentClassAndBuildingIDS(mConfigData.BIDS, BusinessConstants.EquipmentClass.AirHandlerEquipmentClassID)
                    .Select(_ => _.EID).ToArray();
            }
        }

        public List<VDataQueryResult> GetData()
        {
            bool isPredicted = false; //todo- temp to get Jason's code committe - need to change

            //get vpointIDs
            List<GeneralVPointModel> vPoints = SqlVPointQueryHelper.GetGeneralizedVPoints(mConfigData.DataManager, mEquipmentIDs,
                new int[] { BusinessConstants.Analyses.AHUEnergyAID }, isPredicted);

            if (vPoints == null) return null;

            //get vpoint ids for ProcessWaterProduced and MakeupWaterUse
            List<GeneralVPointModel> supplyFanElectricUseVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.SupplyFanElectricUseID).ToList();
            List<GeneralVPointModel> supplyAirVolumeVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.SupplyAirVolumeID).ToList();

            VDataQueryResult supplFanElectricUse = getQueryResult(supplyFanElectricUseVPoints, "SupplyFanElectricUse", isPredicted);
            VDataQueryResult supplyAirVolume = getQueryResult(supplyAirVolumeVPoints, "SupplyAirVolume", isPredicted);

            List<VDataQueryResult> results = new List<VDataQueryResult>();
            results.Add(supplFanElectricUse);
            results.Add(supplyAirVolume);

            return results;
        }

        private VDataQueryResult getQueryResult(List<GeneralVPointModel> vPoints, string resultName, bool isPredicted)
        {
            if (vPoints == null || resultName == null) return null;

            VDataQueryResult result = new VDataQueryResult();

            result.ResultName = resultName;
            result.NumQueryItems = vPoints.Count();
            result.Data = getData(vPoints, isPredicted);
            result.IsPredicted = isPredicted;

            return result;
        }

        private IEnumerable<GeneralVDataModel> getData(List<GeneralVPointModel> vPoints, bool isPredicted)
        {
            return VDataQueryHelper.GetGeneralizedVData(mConfigData.DataManager, vPoints.Select(_ => _.ID),
                mConfigData.AnalysisRange, mConfigData.StartDate, mConfigData.EndDate, isPredicted);
        }
    }
}