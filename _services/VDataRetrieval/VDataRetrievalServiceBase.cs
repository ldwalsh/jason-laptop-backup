﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System.Collections.Generic;

namespace CW.Website._services.VDataRetrieval
{
    public abstract class VDataRetrievalServiceBase : IDataService
    {
        #region field(s)

            public MetricCalcServiceConfigModel _model;

        #endregion

        #region constructor(s)

            public VDataRetrievalServiceBase(MetricCalcServiceConfigModel model)
            {
                _model = model;
            }

        #endregion

        #region interface method(s)

            public List<VDataQueryResult> GetData()
            {
                var data = new List<VDataQueryResult>();

                data = GetData(false);

                if (_model.GetPredictedData) //get predicted data
                {
                    var preData = GetData(true);

                    data.AddRange(preData);
                }

                return data;
            }

        #endregion

        #region method(s)

            #region abstract method(s)

                public abstract List<VDataQueryResult> GetData(bool isPredicted);

            #endregion
        
        #endregion
    }
}