﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.VDataRetrieval.Interfaces;
using System;
using static CW.Website._services.MetricCalculation.Models.MetricCalcServiceConfigModel;

namespace CW.Website._services.VDataRetrieval.Factories
{
    public sealed class DataServiceFactory
    {
        #region method(s)

            public IDataService GetDataService(MetricCalcServiceConfigModel model)
            {
                var type = GetServiceType(model);

                try
                {
                    return (IDataService)Activator.CreateInstance(type, new object[] { model });
                }
                catch
                {
                    return null;
                }
            }

            /// <summary>
            /// Returns type of data retrieval service to instantiate based on config data.
            /// </summary>
            /// <param name="configData">The service config data.</param>
            /// <returns></returns>
            Type GetServiceType(MetricCalcServiceConfigModel configData)
            {
                switch (configData.DataType)
                {
                    case ConsumptionDataType.AvoidableCosts:
                        return typeof(AvoidableCostsVDataRetrievalService);
                    case ConsumptionDataType.Carbon: //fall through
                    case ConsumptionDataType.Cars: //fall through
                    case ConsumptionDataType.Cost: //fall through
                    case ConsumptionDataType.Trees:
                        return typeof(UtilityVDataRetrievalService);
                    case ConsumptionDataType.Energy:
                        return typeof(EnergyVDataRetrievalService);
                    case ConsumptionDataType.CoolingEfficiency:
                        return typeof(CoolingEfficiencyVDataRetrievalService);
                    case ConsumptionDataType.HeatingEfficiency:
                        return typeof(HeatingEfficiencyVDataRetrievalService);
                    case ConsumptionDataType.ProcWaterEfficiency:
                        return typeof(ProcWaterVDataRetrievalService);
                    case ConsumptionDataType.VentEfficiency:
                        return typeof(VentEfficiencyVDataRetrievalService);
                    default:
                        return null;
                }
            }

        #endregion
    }
}