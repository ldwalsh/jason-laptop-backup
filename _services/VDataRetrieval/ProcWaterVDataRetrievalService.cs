﻿using CW.Common.Constants;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.VDataRetrieval.Helpers;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.VDataRetrieval
{
    public class ProcWaterVDataRetrievalService : IDataService
    {
        private MetricCalcServiceConfigModel mConfigData;
        private int[] mEquipmentIDs;

        public ProcWaterVDataRetrievalService(MetricCalcServiceConfigModel configData)
        {
            mConfigData = configData;
            configure();
        }

        private void configure()
        {
            if (mConfigData == null || mConfigData.BIDS == null || mConfigData.DataManager == null) return;

            mEquipmentIDs = mConfigData.DataManager.EquipmentDataMapper
                .GetAllVisibleEquipmentByBIDsAndEquipmentTypeID(mConfigData.BIDS, BusinessConstants.EquipmentType.PWPlantTypeID, false)
                .Select(_ => _.EID).ToArray();
        }

        public List<VDataQueryResult> GetData()
        {
            bool isPredicted = false; //todo- temp to get Jason's code committe - need to change

            //get vpointIDs
            List<GeneralVPointModel> vPoints = SqlVPointQueryHelper.GetGeneralizedVPoints(mConfigData.DataManager, mEquipmentIDs,
                new int[] { BusinessConstants.Analyses.ProcessWaterPlantAID }, isPredicted);

            if (vPoints == null) return null;

            //get vpoint ids for ProcessWaterProduced and MakeupWaterUse
            List<GeneralVPointModel> processWaterProdVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.ProcessWaterProducedTypeID).ToList();
            List<GeneralVPointModel> makeupWaterUseVPoints = vPoints.Where(_ => _.PointTypeID == BusinessConstants.PointType.MakeupWaterUseTypeID).ToList();

            VDataQueryResult processWater = getQueryResult(processWaterProdVPoints, "ProcessWaterProduced", isPredicted);
            VDataQueryResult makeupWater = getQueryResult(makeupWaterUseVPoints, "MakeupWaterUse", isPredicted);

            List<VDataQueryResult> results = new List<VDataQueryResult>();
            results.Add(processWater);
            results.Add(makeupWater);

            return results;            
        }

        private VDataQueryResult getQueryResult(List<GeneralVPointModel> vPoints, string resultName, bool isPredicted)
        {
            if (vPoints == null || resultName == null) return null;

            VDataQueryResult result = new VDataQueryResult();

            result.ResultName = resultName;
            result.NumQueryItems = vPoints.Count();
            result.Data = getData(vPoints, isPredicted);
            result.IsPredicted = isPredicted;

            return result;
        }

        private IEnumerable<GeneralVDataModel> getData(List<GeneralVPointModel> vPoints, bool isPredicted)
        {
            return VDataQueryHelper.GetGeneralizedVData(mConfigData.DataManager, vPoints.Select(_ => _.ID),
                mConfigData.AnalysisRange, mConfigData.StartDate, mConfigData.EndDate, isPredicted);
        }
    }
}