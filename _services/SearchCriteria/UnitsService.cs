﻿using CW.Business;
using CW.Data;
using CW.Website._framework;
using CW.Website._services.SearchCriteria.Factories;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI.WebControls;

namespace CW.Website._services.SearchCriteria
{
    /// <summary>
    /// Implements <see cref="IUnitsService"/>.
    /// </summary>
    public sealed class UnitsService : IUnitsService
    {
        #region field(s)

            DataManager _dataMgr;

            SiteUser _siteUser;

            SearchCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public UnitsService(DataManager dataMgr, SiteUser siteUser, SearchCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _siteUser = siteUser;

                _model = model;
            }

        #endregion

        #region interface method(s)

            /// <summary>
            ///  Gets the visible buildings for the user, and then sets the units based on those visible buildings
            /// </summary>
            public void WireUpUnits()
            {
                var visibleBuildings = _siteUser.VisibleBuildings;

                SetUnits(visibleBuildings);
            }
        
            /// <summary>
            /// Sets the units (IP, SI) based on the inputted building list compared to all of their buildings
            /// by their client.  Units will only be added (and drop down visible) if data exists
            /// </summary>
            /// <param name="buildings">List of buildings used to retrieve units</param>
            public void SetUnits(IEnumerable<Building> buildings)
            {
                var unitsDDL = _model.UnitsDDL;

                unitsDDL.Visible = false;

                if (buildings.Any())
                {
                    var allBuildings = _dataMgr.BuildingDataMapper.GetAllBuildingsByCID
                    (
                        _siteUser.CID, 
                        new Expression<Func<Building, object>>[] { (b => b.BuildingSettings) }
                    );

                    var comparer = new BuildingDataMapper.BuildingComparer();

                    var ip = buildings.Intersect(GetUnitType(allBuildings, false), comparer);

                    var si = buildings.Intersect(GetUnitType(allBuildings, true), comparer);

                    if (ip.Any()) AddUnitTypeItem(unitsDDL, "IP", "0");

                    if (si.Any()) AddUnitTypeItem(unitsDDL, "SI", "1");

                    if (ip.Any() || si.Any())
                    {
                        unitsDDL.Visible = true;

                        unitsDDL.DataBind();
                    }
                }
            }

            /// <summary>
            /// When an event change has occured for units, perform the following steps:
            /// 1. Reset and wire up the buildings list
            /// 2. Reset and wire up the building groups list
            /// 3. Reset and write up the building types drop down
            /// </summary>
            public void ExecuteUnitsEvent()
            {
                var buildingsSvc = new BuildingsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingsService();

                var buildingGroupsSvc = new BuildingGroupsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingGroupsService();

                var buildingTypesSvc = new BuildingTypesServiceFactory(_dataMgr, _siteUser, _model).GetBuildingTypesService();
            
                buildingsSvc.WireUpBuildings();

                buildingGroupsSvc.WireUpBuildingGroups();

                buildingTypesSvc.WireUpBuildingTypes();
            }

        #endregion

        #region method(s)

            /// <summary>
            /// Convenience method for getting the unit types, based on the inputting building list and boolean for if the
            /// records being search are for unit systems or not.
            /// </summary>
            /// <param name="allBuildings">List of building to search for unit system or not</param>
            /// <param name="isUnitSystem">value for whether unit system should be searched or not</param>
            /// <returns></returns>
            IEnumerable<Building> GetUnitType(IEnumerable<Building> allBuildings, bool isUnitSystem)
            {
                return allBuildings.Where(b => b.BuildingSettings.Select(bs => bs.UnitSystem).First() == isUnitSystem);
            }

            /// <summary>
            /// Convenience method for wiring up a new list item and attaching that list to the units drop down
            /// </summary>
            /// <param name="unitsDDL">Drop down for appending list item to</param>
            /// <param name="text">List items text</param>
            /// <param name="value">List items value</param>
            void AddUnitTypeItem(DropDownList unitsDDL, string text, string value)
            {
                var lItem = new ListItem();

                lItem.Text = text;

                lItem.Value = value;

                unitsDDL.Items.Add(lItem);
            }

        #endregion
    }
}