﻿using CW.Business;
using CW.Data.Models.BuildingGroup;
using CW.Utility;
using CW.Website._extensions;
using CW.Website._framework;
using CW.Website._services.SearchCriteria.Factories;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._services.SearchCriteria
{
    /// <summary>
    /// Implements <see cref="IBuildingGroupsService"/>.
    /// </summary>
    public sealed class BuildingGroupsService : IBuildingGroupsService
    {
        #region field(s)

            DataManager _dataMgr;

            SiteUser _siteUser;

            SearchCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public BuildingGroupsService(DataManager dataMgr, SiteUser siteUser, SearchCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _siteUser = siteUser;

                _model = model;
            }

        #endregion

        #region interface method(s)

            /// <summary>
            /// Gets the building groups drop down, adds all the values of the drop down to an array of
            /// building ids, and then sets the building groups based on those building ids
            /// </summary>
            public void WireUpBuildingGroups()
            {
                var buildingsLBE = _model.BuildingsLBE;

                var bids = buildingsLBE.Items
                                       .OfType<ListItem>()
                                       .Where(i => i.Value != "0")
                                       .Select(i => int.Parse(i.Value))
                                       .ToArray();

                SetBuildingGroups(bids);
            }

            /// <summary>
            /// Sets the building groups based on the following logic, based on the input:
            /// 1. If there aren't any building ids at all, clear out the building groups UI list box
            /// 2. If there aren't buildings groups found, set a new group named "unassigned" and add the building ids to it
            /// 3. If there are not any unassigned building groups, set the list of building groups
            /// 4. If there are a mixture of both assigned and unassigned, set the list of building groups, and append 
            /// "unassigned" to the end of the list, and add the unassigned building ids to it
            /// </summary>
            /// <param name="bids">The input of building ids to search for building groups</param>
            public void SetBuildingGroups(int[] bids)
            {
                if (!bids.Any()) //no buildings, therefore no groups to populate
                {
                    _model.BuildingGroupsLBE.Items.Clear();

                    return; 
                }

                var buildingGroups = _dataMgr.BuildingGroupDataMapper.GetBuildingGroupsByBids(bids).ToList();

                if (!buildingGroups.Any())
                {
                    //no groups were found, add default "unassigned" group to the existing empty "building groups" 
                    //ojbect, and connect all of the bids to this group
                    AppendToBuildingGroups(bids, buildingGroups); 

                    BindBuildingGroups(buildingGroups);

                    return;
                }
                
                //compare the buildings groups set to the inputted bid list, and search for bids not in the group set
                var buildingGroupBids = buildingGroups.Select(bg => bg.BID).Distinct().ToArray();

                var unassignedBids = bids.Except(buildingGroupBids).ToArray();

                //next we must flatten the building groups so that the format is:
                //item key: Building Group Name (distinct)
                //item value: comma separated collection of bids for that group
                buildingGroups = FlattenGroups(buildingGroups);
            
                if (!unassignedBids.Any())
                {
                    //no groups were unassigned, simply bind the building groups
                    BindBuildingGroups(buildingGroups);

                    return;
                }

                //some of the buildings were unassigned to a building group. add "unassigned" group to the existing 
                //"building groups" object (at the end), and connect the unassigned bids to this group
                AppendToBuildingGroups(unassignedBids, buildingGroups);

                BindBuildingGroups(buildingGroups);
            }

            /// <summary>
            /// When a selection has changed for the building groups list box, we must determine what was selected,
            /// set their values to a building ids collection, and set the buildings based on the selected input.
            /// Secondarily, the building service will handle which building type, unit and currency was selected
            /// </summary>
            public void ExecuteBuildingGroupsEvent()
            {
                var buildingGroupsLBE = _model.BuildingGroupsLBE;

                var selectedItems = ListBoxExtension.GetSelectedItems(buildingGroupsLBE.Items);

                if (selectedItems.Any())
                {
                    var selectedBids = new List<int>();

                    //we must split out the building id values from the item value and convert to ints,
                    //then add them to the collection of selected building ids, and set only the distinct items
                    foreach (var item in selectedItems)
                    {
                        var bids = Array.ConvertAll(item.Value.Split(','), int.Parse);

                        for (var i = 0; i < bids.Length; i++)
                            selectedBids.Add(bids[i]);
                    } 

                    selectedBids = selectedBids.Distinct().ToList();
                 
                    //set the buildings list box via the buildings service, the buildings service will handle
                    //figuring out the user selected building type, as well as the unit and currency selected.
                    var buildingsSvc = new BuildingsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingsService();

                    var buildingsByType = buildingsSvc.GetBuildingsByBuildingType();

                    buildingsByType = buildingsByType.Where(b => selectedBids.Contains(b.BID));

                    buildingsSvc.SetBuildings(buildingsByType);
                }
            }

        #endregion

        #region method(s)

            /// <summary>
            /// Method for appending an "Unassigned" building group to the building group collection
            /// </summary>
            /// <param name="bids">building ids to be added as the "unassigned" building ids</param>
            /// <param name="buildingGroups">building groups collection to append to</param>
            void AppendToBuildingGroups(int[] bids, List<GetBuildingGroupsData> buildingGroups)
            {
                var unassignedItem = new GetBuildingGroupsData
                {
                    BIDs = string.Join(",", bids),

                    BuildingGroupName = "Unassigned"
                };

                buildingGroups.Add(unassignedItem);
            }

            /// <summary>
            /// Once the building groups are ready for UI display, we must group by the building group
            /// name and set the building ids to an array of ids, hence "flattening" the building groups
            /// </summary>
            /// <param name="buildingGroups">List of building groups to be flattened</param>
            /// <returns>Flattened list of building groups</returns>
            List<GetBuildingGroupsData> FlattenGroups(List<GetBuildingGroupsData> buildingGroups)
            {
                return buildingGroups.GroupBy(bg => bg.BuildingGroupName).Select
                (

                    bg => new GetBuildingGroupsData
                    {
                        BIDs = string.Join(",", bg.Select(b => b.BID).ToArray()),

                        BuildingGroupName = bg.Key
                    }

                ).ToList();
            }

            /// <summary>
            /// Bind the building groups based on the building groups being inputted
            /// </summary>
            /// <param name="buildingGroups">List of building groups to be bound</param>
            void BindBuildingGroups(IList<GetBuildingGroupsData> buildingGroups)
            {
                var buildingGroupsLBE = _model.BuildingGroupsLBE;

                buildingGroupsLBE.DataTextField = PropHelper.G<GetBuildingGroupsData>(bg => bg.BuildingGroupName);

                buildingGroupsLBE.DataValueField = PropHelper.G<GetBuildingGroupsData>(bg => bg.BIDs);

                buildingGroupsLBE.DataSource = buildingGroups;

                buildingGroupsLBE.DataBind();   
            }

        #endregion
    }
}