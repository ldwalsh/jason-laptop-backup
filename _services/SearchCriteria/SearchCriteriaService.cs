﻿using CW.Business;
using CW.Website._framework;
using CW.Website._services.SearchCriteria.Factories;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;

namespace CW.Website._services.SearchCriteria
{
    /// <summary>
    /// Implements <see cref="ISearchCriteriaService"/>.
    /// </summary>
    public sealed class SearchCriteriaService : ISearchCriteriaService
    {
        #region field(s)

            DataManager _dataMgr;

            SiteUser _siteUser;

            SearchCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public SearchCriteriaService(DataManager dataMgr, SiteUser siteUser, SearchCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _siteUser = siteUser; 
            
                _model = model;
            }

        #endregion

        #region interface method(s)

            /// <summary>
            /// Get the necessary information and wire up the UI with its data
            /// </summary>
            public void WireUpUI()
            {
                //These must be wired up in the following order, so data is available for each UI element required
                WireUpUnits();

                WireUpCurrencies();

                WireUpBuildings();

                WireUpBuildingTypes();
            
                WireUpBuildingGroups();    
            }
        
        #endregion

        #region method(s)

            /// <summary>
            /// Convenience method for wiring up the units via the units service
            /// </summary>
            void WireUpUnits()
            {
                var unitsSvc = new UnitsServiceFactory(_dataMgr, _siteUser, _model).GetUnitsService();

                unitsSvc.WireUpUnits();
            }

            /// <summary>
            /// Convenience method for wiring up the currencies via the currencies service
            /// </summary>
            void WireUpCurrencies()
            {
                var currenciesSvc = new CurrenciesServiceFactory(_dataMgr, _siteUser, _model).GetCurrenciesService();

                currenciesSvc.WireUpCurrencies();
            }

            
            /// <summary>
            /// Convenience method for wiring up the buildings via the buildings service
            /// </summary>
            void WireUpBuildings()
            {
                var buildingsSvc = new BuildingsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingsService();

                buildingsSvc.WireUpBuildings();
            }

            /// <summary>
            /// Convenience method for wiring up the building types via the buildings types service
            /// </summary>
            void WireUpBuildingTypes()
            {
                var buildingTypesSvc = new BuildingTypesServiceFactory(_dataMgr, _siteUser, _model).GetBuildingTypesService();

                buildingTypesSvc.WireUpBuildingTypes();
            }
        
            /// <summary>
            /// Convenience method for wiring up the building groups via the buildings groups service
            /// </summary>
            void WireUpBuildingGroups()
            {
                var buildingGroupsSvc = new BuildingGroupsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingGroupsService();

                buildingGroupsSvc.WireUpBuildingGroups();
            }
        
        #endregion
    }
}