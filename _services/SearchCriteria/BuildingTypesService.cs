﻿using CW.Business;
using CW.Data;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using CW.Website._services.SearchCriteria.Factories;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._services.SearchCriteria
{
    /// <summary>
    /// Implements <see cref="IBuildingTypesService"/>.
    /// </summary>
    public class BuildingTypesService : IBuildingTypesService
    {
        #region field(s)

            DataManager _dataMgr;

            SiteUser _siteUser;

            SearchCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public BuildingTypesService(DataManager dataMgr, SiteUser siteUser, SearchCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _siteUser = siteUser;

                _model = model;
            }

        #endregion

        #region interface method(s)

            /// <summary>
            /// Gets the building types for the users client, and then sets the building types that were found
            /// </summary>
            public void WireUpBuildingTypes()
            {
                var buildingTypesDDL = _model.BuildingTypesDDL;

                var bids = _model.BuildingsLBE.Items
                                              .OfType<ListItem>()
                                              .Where(i => i.Value != "0")
                                              .Select(i => int.Parse(i.Value))
                                              .ToArray();

                buildingTypesDDL.AppendDataBoundItems = true;

                DropDownSequencer.ClearAndCreateDefaultItems(buildingTypesDDL, false, true, false);

                var buildingTypes = _dataMgr.BuildingTypeDataMapper.GetAllBuildingTypesByBIDs(bids);

                if (buildingTypes.Any())
                {
                    buildingTypesDDL.DataTextField = PropHelper.G<BuildingType>(bt => bt.BuildingTypeName);

                    buildingTypesDDL.DataValueField = PropHelper.G<BuildingType>(bt => bt.BuildingTypeID);

                    buildingTypesDDL.DataSource = buildingTypes;

                    buildingTypesDDL.DataBind();
                }
            }

            /// <summary>
            /// When an event change has occured for building types, perform the following steps:
            /// 1. Set the building groups based on the buildings found by building type selected
            /// 2. Set the buildings based on the buildings found by building type selected
            /// Further filtering will occur within the buildings groups and buildings services
            /// </summary>
            public void ExecuteBuildingTypesEvent()
            {
                var buildingsSvc = new BuildingsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingsService();

                var buildingGroupsSvc = new BuildingGroupsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingGroupsService();

                var buildingsByType = buildingsSvc.GetBuildingsByBuildingType();

                var bidsByType = buildingsByType.Select(b => b.BID).ToArray();
            
                buildingGroupsSvc.SetBuildingGroups(bidsByType);
            
                buildingsSvc.SetBuildings(buildingsByType);
            }
        
        #endregion
    }
}