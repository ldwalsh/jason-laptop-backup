﻿using CW.Business;
using CW.Data;
using CW.Utility;
using CW.Website._framework;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._services.SearchCriteria
{
    /// <summary>
    /// Implements <see cref="IBuildingsService"/>.
    /// </summary>
    public class BuildingsService : IBuildingsService
    {
         #region field(s)

            DataManager _dataMgr;

            SiteUser _siteUser;

            SearchCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public BuildingsService(DataManager dataMgr, SiteUser siteUser, SearchCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _siteUser = siteUser;

                _model = model;
            }

        #endregion

        #region inteface method(s)

            /// <summary>
            /// Gets the visible buildings for the user with filtered building settings, if any, 
            /// and then sets the buildings based on those visible buildings (filtered if any)
            /// </summary>
            public void WireUpBuildings()
            {
                 var buildings = FilterBuildingSettings(_siteUser.VisibleBuildings);

                SetBuildings(buildings);
            }

            /// <summary>
            /// Bind buildings to the list box
            /// </summary>
            /// <param name="buildings">List of buildings to be bound</param>
            public void SetBuildings(IEnumerable<Building> buildings)
            {
                var buildingsLBE = _model.BuildingsLBE;

                buildingsLBE.DataTextField = PropHelper.G<Building>(b => b.BuildingName);

                buildingsLBE.DataValueField = PropHelper.G<Building>(b => b.BID);

                buildingsLBE.DataSource = buildings;

                buildingsLBE.DataBind();
            }

            /// <summary>
            /// Get the buildings based on the user selected building type.  If building type is default
            /// (i.e. All), return the complete list of visible buildings, otherwise return buildings based on
            /// the user selected building type, these buildings are also filtered, if any filters exist
            /// </summary>
            /// <returns>List of buildings based on the user selected building type</returns>
            public IEnumerable<Building> GetBuildingsByBuildingType()
            {
                var buildingsLBE = _model.BuildingsLBE;

                var id = int.Parse(_model.BuildingTypesDDL.SelectedValue);
            
                var buildings = _siteUser.VisibleBuildings;

                if (id == 0) return FilterBuildingSettings(buildings);
                  
                var bids = buildings.Select(b => b.BID).ToArray();

                var buildingsByType = _dataMgr.BuildingDataMapper.GetVisibleBuildingsByTypeID(id, bids);

                return FilterBuildingSettings(buildingsByType);
            }

        #endregion

        #region method(s)

            /// <summary>
            /// Filter the inputted buildings by building settings, if any filters exist (units or currencies)
            /// </summary>
            /// <param name="buildings">List of buildings to apply building setting filters</param>
            /// <returns>List of filtered buildings, if filters exist, otherwise current buildings set</returns>
            IEnumerable<Building> FilterBuildingSettings(IEnumerable<Building> buildings)
            {
                var unitsDDL = _model.UnitsDDL;

                var currenciesDDL = _model.CurrenciesDDL;

                var hasUnits = (unitsDDL.Items.Count > 0);

                var hasCurrencies = (currenciesDDL.Items.Count > 0);

                var bids = buildings.Select(b => b.BID).ToArray();

                if (hasUnits || hasCurrencies)
                {
                    var buildingSettings = _dataMgr.BuildingDataMapper.GetBuildingSettingsByBids(bids);

                    if (hasUnits)
                    {
                        var isUnitSystem = (unitsDDL.SelectedItem.Text == "SI");

                        buildingSettings = buildingSettings.Where(bs => bs.UnitSystem == isUnitSystem);
                    }
                    
                    if (hasCurrencies)
                    {
                        var lcids = currenciesDDL.SelectedValue.Split('|').Select(i => int.Parse(i));

                        buildingSettings = buildingSettings.Where(bs => lcids.Contains(bs.LCID));
                    } 
                    
                    var buildingSettingsBids = buildingSettings.Select(b => b.BID).ToArray();

                    buildings = buildings.Where(b => buildingSettingsBids.Contains(b.BID));
                }

                return buildings;
            }

        #endregion
    }
}