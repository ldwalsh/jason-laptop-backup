﻿using AjaxControlToolkit;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;

namespace CW.Website._services.SearchCriteria
{
    /// <summary>
    /// Implements <see cref="ICollapsiblePanelService"/>.
    /// </summary>
    public class CollaspiblePanelService : ICollapsiblePanelService
    {
        #region field(s)

            CollapsiblePanelModel _model;

        #endregion

        #region constructor(s)

            public CollaspiblePanelService(CollapsiblePanelModel model) { _model = model; }

        #endregion

        #region interface method(s)

            /// <summary>
            /// Wire up the collapsible panel by setting all necessary attributes for the control
            /// </summary>
            public void WireUpCollapsiblePanel()
            {
                var selectionTxt = "selection";

                var searchCriteriaCPE = _model.SearchCriteriaCPE;

                var lnkSelectionID = _model.SelectionLNK.ID;

                searchCriteriaCPE.TargetControlID = _model.SelectionPNL.ID;

                searchCriteriaCPE.CollapsedSize = 0;

                searchCriteriaCPE.Collapsed = false;

                searchCriteriaCPE.ExpandControlID = lnkSelectionID;

                searchCriteriaCPE.CollapseControlID = lnkSelectionID;

                searchCriteriaCPE.AutoCollapse = false;

                searchCriteriaCPE.AutoExpand = false;

                searchCriteriaCPE.ScrollContents = false;

                searchCriteriaCPE.ExpandDirection = CollapsiblePanelExpandDirection.Vertical;

                searchCriteriaCPE.TextLabelID = _model.SelectionLBL.ID;

                searchCriteriaCPE.CollapsedText = $"show {selectionTxt}";

                searchCriteriaCPE.ExpandedText = $"hide {selectionTxt}";
            }

        #endregion
    }
}