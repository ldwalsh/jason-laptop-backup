﻿using CW.Website._extensions;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._services.SearchCriteria
{
    /// <summary>
    /// Implements <see cref="ILinkButtonService"/>.
    /// </summary>
    public class LinkButtonService : ILinkButtonService
    {
         #region field(s)

            LinkButtonModel _lbModel;

            IBuildingTypesService _buildingTypesSvc;

            SearchCriteriaUIModel _scModel;

        #endregion

        #region constructor(s)

            public LinkButtonService(LinkButtonModel lbModel, IBuildingTypesService buidingTypesSvc, SearchCriteriaUIModel scModel)
            {
                _lbModel = lbModel;

                _buildingTypesSvc = buidingTypesSvc;

                _scModel = scModel;
            }

        #endregion

        #region interface method(s)

            /// <summary>
            /// Wire up the link buttons by setting all necessary attributes for the controls
            /// </summary>
            public void WireUpLinkButtons()
            {
                foreach (var linkButton in _lbModel.LinkButtons)
                {
                    linkButton.Text = _lbModel.Text;

                    linkButton.CausesValidation = _lbModel.CausesValidation;

                    linkButton.CssClass = _lbModel.CssClass;
                }
            }

            /// <summary>
            /// Event method to be executed when the link "all" is clicked.  The following steps are performed:
            /// 1. Unselect all values in the building groups and buildings list boxes
            /// 2. Execute the building types event via the service to repopulate the list boxes
            /// </summary>
            public void ExecuteLinkButtonEvent()
            {
                if (_scModel != null && _buildingTypesSvc != null)
                {
                    DeselectItems(_scModel.BuildingGroupsLBE);
                
                    DeselectItems(_scModel.BuildingsLBE);
                
                    _buildingTypesSvc.ExecuteBuildingTypesEvent();
                }
            }

        #endregion

        #region method(s)

            /// <summary>
            /// Convenience method used to deselect all items from list box supplied
            /// </summary>
            /// <param name="lb">List box to deselect all items from</param>
            void DeselectItems(ListBoxExtension lb) => lb.Items.Cast<ListItem>().ToList().ForEach(i => i.Selected = false);
        
        #endregion
    }
}