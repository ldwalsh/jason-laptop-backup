﻿namespace CW.Website._services.SearchCriteria.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for collapsible panels
    /// </summary>
    public interface ICollapsiblePanelService
    {
        #region method(s)

            /// <summary>
            /// Get the collasible panel and related elements and wire up the UI
            /// </summary>
            void WireUpCollapsiblePanel();

        #endregion
    }
}