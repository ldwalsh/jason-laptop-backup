﻿using CW.Data;
using System.Collections.Generic;

namespace CW.Website._services.SearchCriteria.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for currencies, setting currencies
    /// and currencies event execution
    /// </summary>
    public interface ICurrenciesService
    {
        #region method(s)

            /// <summary>
            /// Get the currencies information and wire up the UI with its data
            /// </summary>
            void WireUpCurrencies();

            /// <summary>
            /// Given the enumerable buildings input, set the currencies for the inputted buildings.
            /// </summary>
            /// <param name="buildings">The list of building to set</param>
            void SetCurrencies(IEnumerable<Building> buildings);

            /// <summary>
            /// Performs execution of currencies event, to manipulate UI and data if necessary
            /// </summary>
            void ExecuteCurrenciesEvent();
        
        #endregion
    }
}