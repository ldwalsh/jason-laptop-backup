﻿using CW.Data;
using System.Collections.Generic;

namespace CW.Website._services.SearchCriteria.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for units, setting uints
    /// and units event execution
    /// </summary>
    public interface IUnitsService
    {
         #region method(s)

            /// <summary>
            /// Get the units information and wire up the UI with its data
            /// </summary>
            void WireUpUnits();

            /// <summary>
            /// Given the enumerable buildings input, set the units for the inputted buildings.
            /// </summary>
            /// <param name="buildings">>The list of building to set</param>
            void SetUnits(IEnumerable<Building> buildings);

            /// <summary>
            /// Performs execution of units event, to manipulate UI and data if necessary
            /// </summary>
            void ExecuteUnitsEvent();
        
        #endregion
    }
}