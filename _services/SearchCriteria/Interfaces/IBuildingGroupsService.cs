﻿namespace CW.Website._services.SearchCriteria.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for building groups, setting building groups 
    /// and building groups event execution
    /// </summary>
    public interface IBuildingGroupsService
    {
        #region method(s)

            /// <summary>
            /// Get the building groups information and wire up the UI with its data
            /// </summary>
            void WireUpBuildingGroups();

            /// <summary>
            /// Given the int array input, find and set the building groups, if some are not found.
            /// set that building group as "unassigned" for those ints.
            /// </summary>
            /// <param name="bids">The list of building ids for finding and settings groups</param>
            void SetBuildingGroups(int[] bids);

            /// <summary>
            /// Performs execution of building groups event, to manipulate UI and data if necessary
            /// </summary>
            void ExecuteBuildingGroupsEvent();

        #endregion
    }
}