﻿namespace CW.Website._services.SearchCriteria.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for search criteria panel
    /// </summary>
    public interface ISearchCriteriaService
    {
        #region method(s)

            /// <summary>
            /// Get the necessary information and wire up the UI with its data
            /// </summary>
            void WireUpUI();

        #endregion
    }
}