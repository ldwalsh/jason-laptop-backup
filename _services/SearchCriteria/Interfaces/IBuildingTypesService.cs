﻿namespace CW.Website._services.SearchCriteria.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for building types,
    /// and building types event execution
    /// </summary>
    public interface IBuildingTypesService
    {
        #region method(s)

            /// <summary>
            /// Get the building types information and wire up the UI with its data
            /// </summary>
            void WireUpBuildingTypes();
            
            /// <summary>
            /// Performs execution of building types event, to manipulate UI and data if necessary
            /// </summary>
            void ExecuteBuildingTypesEvent();

        #endregion
    }
}