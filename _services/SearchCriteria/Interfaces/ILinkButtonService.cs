﻿namespace CW.Website._services.SearchCriteria.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for link buttons
    /// </summary>
    public interface ILinkButtonService
    {
        #region method(s)

            /// <summary>
            /// Get the link buttons and wire up the UI
            /// </summary>
            void WireUpLinkButtons();

            /// <summary>
            /// Performs execution of link button event, to manipulate UI and data if necessary
            /// </summary>
            void ExecuteLinkButtonEvent();

        #endregion
    }
}