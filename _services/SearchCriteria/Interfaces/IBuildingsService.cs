﻿using CW.Data;
using System.Collections.Generic;

namespace CW.Website._services.SearchCriteria.Interfaces
{
    /// <summary>
    /// Interface to provide means to wire up the UI for buildings, setting buildings 
    /// and getting buildings based on user selected building type
    /// </summary>
    public interface IBuildingsService
    {
        #region method(s)

            /// <summary>
            /// Get the buildings information and wire up the UI with its data
            /// </summary>
            void WireUpBuildings();

            /// <summary>
            /// Given the enumerable buildings input, set the buildings.
            /// </summary>
            /// <param name="buildings">The list of building to set</param>
            void SetBuildings(IEnumerable<Building> buildings);

            /// <summary>
            /// Get the buildings by user selected building type
            /// </summary>
            /// <returns>IEnumerable list of buildings, if found, null otherwise</returns>
            IEnumerable<Building> GetBuildingsByBuildingType();
        
        #endregion
    }
}