﻿using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;

namespace CW.Website._services.SearchCriteria.Factories
{
    public class CollapsiblePanelServiceFactory
    {
        #region field(s)

            CollapsiblePanelModel _model;

        #endregion

        #region constructor(s)

            public CollapsiblePanelServiceFactory(CollapsiblePanelModel model) { _model = model; }

        #endregion

        #region method(s)

            public ICollapsiblePanelService GetCollapsiblePanelService() => new CollaspiblePanelService(_model);

        #endregion
    }
}