﻿using CW.Business;
using CW.Website._framework;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;

namespace CW.Website._services.SearchCriteria.Factories
{
    public class SearchCriteriaServiceFactory
    {
        #region field(s)

            DataManager _dataMgr;

            SiteUser _siteUser;

            SearchCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public SearchCriteriaServiceFactory(DataManager dataMgr, SiteUser siteUser, SearchCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _siteUser = siteUser;

                _model = model;
            }

        #endregion

        #region method(s)

            public ISearchCriteriaService GetSearchCriteriaService() => new SearchCriteriaService(_dataMgr, _siteUser, _model);

        #endregion
    }
}