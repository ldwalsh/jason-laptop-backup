﻿using CW.Business;
using CW.Website._framework;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;

namespace CW.Website._services.SearchCriteria.Factories
{
    public class LinkButtonServiceFactory
    {
        #region field(s)

            SearchCriteriaUIModel _scModel;

            LinkButtonModel _lbModel;

            IBuildingTypesService _buildingTypesSvc;

        #endregion

        #region constructor(s)

            public LinkButtonServiceFactory
            (
                LinkButtonModel lbModel, 
                DataManager dataMgr = null, 
                SiteUser siteUser = null,
                SearchCriteriaUIModel scModel = null
            )
            {
                if (dataMgr != null && siteUser != null && scModel != null)
                {
                    _buildingTypesSvc = new BuildingTypesServiceFactory(dataMgr, siteUser, scModel).GetBuildingTypesService();

                    _scModel = scModel;
                }

                _lbModel = lbModel;
            }

        #endregion

        #region method(s)

            public ILinkButtonService GetLinkButtonService() => new LinkButtonService(_lbModel, _buildingTypesSvc, _scModel);

        #endregion
    }
}