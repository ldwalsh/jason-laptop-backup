﻿using CW.Business;
using CW.Data;
using CW.Website._framework;
using CW.Website._services.SearchCriteria.Factories;
using CW.Website._services.SearchCriteria.Interfaces;
using CW.Website._services.SearchCriteria.Models;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;

namespace CW.Website._services.SearchCriteria
{
    /// <summary>
    /// Implements <see cref="ICurrenciesService"/>.
    /// </summary>
    public class CurrenciesService : ICurrenciesService
    {
        #region field(s)

            DataManager _dataMgr;

            SiteUser _siteUser;

            SearchCriteriaUIModel _model;

        #endregion

        #region constructor(s)

            public CurrenciesService(DataManager dataMgr, SiteUser siteUser, SearchCriteriaUIModel model)
            {
                _dataMgr = dataMgr;

                _siteUser = siteUser;

                _model = model;
            }

        #endregion

        #region interface method(s)

            /// <summary>
            /// Gets the visible buildings for the user, and then sets the currencies based on those visible buildings
            /// </summary>
            public void WireUpCurrencies()
            {
                var buildings = _siteUser.VisibleBuildings;

                SetCurrencies(buildings);
            }

            /// <summary>
            /// Set the currency or currencies based on the list of buildings inputted.  The format for currencies is
            /// as follows:  USD $, etc.
            /// </summary>
            /// <param name="buildings">List of buildings used to retrieve currencies</param>
            public void SetCurrencies(IEnumerable<Building> buildings)
            {
                var currenciesDDL = _model.CurrenciesDDL;

                var bids = buildings.Select(vb => vb.BID).ToArray();

                var buildingCultures = _dataMgr.BuildingDataMapper.GetBuildingSettingsCultureByBids(bids).Select
                (
                    b => b.Value
                ).Distinct();

                if (buildingCultures.Any())
                {
                    var cultures = FlattenCultures(buildingCultures);

                    currenciesDDL.DataTextField = "Key";

                    currenciesDDL.DataValueField = "Value";

                    currenciesDDL.DataSource = cultures;

                    currenciesDDL.DataBind();
                }
            }

            /// <summary>
            /// When an event change has occured for currencies, perform the following steps:
            /// 1. Reset and wire up the buildings list
            /// 2. Reset and wire up the building groups list
            /// 3. Reset and write up the building types drop down
            /// </summary>
            public void ExecuteCurrenciesEvent()
            {
                var buildingsSvc = new BuildingsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingsService();

                var buildingGroupsSvc = new BuildingGroupsServiceFactory(_dataMgr, _siteUser, _model).GetBuildingGroupsService();

                var buildingTypesSvc = new BuildingTypesServiceFactory(_dataMgr, _siteUser, _model).GetBuildingTypesService();
            
                buildingsSvc.WireUpBuildings();

                buildingGroupsSvc.WireUpBuildingGroups();

                buildingTypesSvc.WireUpBuildingTypes();
            }

        #endregion

        #region method(s)

            /// <summary>
            /// Convenience method to flatten cultures for display within the currencies selector
            /// Display text is the as follows: USD $, etc., the value is the lcid group separated by |
            /// </summary>
            /// <param name="buildingCultures">List of lcids that will need to be flattened</param>
            /// <returns>flattened list of building cultures</returns>
            IList<KeyValuePair<string, string>> FlattenCultures(IEnumerable<int> buildingCultures)
            {
                var cultures = new List<KeyValuePair<string, int>>();

                buildingCultures.ToList().ForEach(c => cultures.Add(new KeyValuePair<string, int>
                (
                    $"{new RegionInfo(c).ISOCurrencySymbol} {new RegionInfo(c).CurrencySymbol}",
                    c
                )));
                    
                return cultures.GroupBy(c => c.Key).Select(c => new KeyValuePair<string, string>
                (

                    c.Key, 
                    string.Join("|", c.Select(kv => kv.Value)

                ))).ToList();
            }

        #endregion
    }
}