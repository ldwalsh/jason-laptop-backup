﻿using AjaxControlToolkit;
using System.Web.UI.WebControls;

namespace CW.Website._services.SearchCriteria.Models
{
    /// <summary>
    /// Model for setting the necessary UI elements to be used for a collasible panel
    /// Technically this is a view model, but contains UI elements, not data
    /// </summary>
    public class CollapsiblePanelModel
    {
        #region properties

            public CollapsiblePanelExtender SearchCriteriaCPE { get; set; }
            
            public HyperLink SelectionLNK { get; set; }

            public Panel SelectionPNL { get; set; }

            public Label SelectionLBL { get; set; }

        #endregion
    }
}