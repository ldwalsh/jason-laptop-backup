﻿using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace CW.Website._services.SearchCriteria.Models
{
    /// <summary>
    /// Model for setting the necessary UI elements to be used for a link button
    /// </summary>
    public class LinkButtonModel
    {
        public LinkButton LinkButton { get; set; } 

        public IList<LinkButton> LinkButtons { get; set; }

        public string Text { get; set; }

        public bool CausesValidation { get; set; }

        public string CssClass { get; set; }
    }
}