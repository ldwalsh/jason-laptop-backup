﻿using CW.Website._extensions;
using System.Web.UI.WebControls;

namespace CW.Website._services.SearchCriteria.Models
{
    /// <summary>
    /// Model for setting the necessary UI elements to be used for the search criteria filters
    /// Technically this is a view model, but contains UI elements, not data
    /// </summary>
    public class SearchCriteriaUIModel
    {
        #region properties
        
            public DropDownList BuildingTypesDDL { get; set; }

            public ListBoxExtension BuildingGroupsLBE { get; set; }

            public ListBoxExtension BuildingsLBE { get; set; }

            public DropDownList RangesDDL { get; set; }

            public DropDownList UnitsDDL { get; set; }

            public DropDownList CurrenciesDDL { get; set; }
        
        #endregion
    }
}