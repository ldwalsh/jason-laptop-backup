﻿using CW.Business;
using CW.Website._services.Chart.Models;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.UtilityConsumption.Models;
using DotNet.Highcharts.Options;
using System.Collections.Generic;

namespace CW.Website._services.Chart
{
    public sealed class UtilityConsumptionChartService : BaseConsumptionChartService
    {
        #region constructor(s)

            public UtilityConsumptionChartService
            (
                DataManager dataMgr, 
                UtilityConsumptionCriteriaUIModel uiModel,
                HighChartUIModel highChartUIModel
            ) : base(dataMgr, highChartUIModel, uiModel)
            {                     
                _highChartDateModel.DateFormat = "MMM yyyy";

                SetConsumptionStats();                
            }

        #endregion

        #region method(s)

            public void SetConsumptionStats()
            {
                var uiModel = (UtilityConsumptionCriteriaUIModel)_uiModel;

                if (_results != null && _results.Count > 0)
                {
                    uiModel.TotalConsumption.InnerText = FormatValue("12345", true);

                    uiModel.PeakMonthConsumption.InnerText = FormatValue("500000", true);

                    uiModel.GreatestBuildingConsumption.InnerText = FormatValue("34356", true);
                }
            }

            /// <summary>
            /// Gets the data series for the chart, based on the data in the config model.
            /// </summary>
            /// <returns></returns>
            protected override Series[] GetChartSeries()
            {
                // return HiChartColumnSeriesHelper.Instance.GetSeries(_highChartDateModel.Dates, _results);

                var series = new List<Series>();

                series.Add(new Series
                {
                    Name = "Building 1",
                    Data = new DotNet.Highcharts.Helpers.Data(new object[] { 123, 455 })
                });

                series.Add(new Series
                {
                    Name = "Building 2",
                    Data = new DotNet.Highcharts.Helpers.Data(new object[] { 322, 687 })
                });

                return series.ToArray();
            }

            protected override void SetDataConfigModel()
            {
                SetCommonDataConfigModelFields();

                _dataConfigModel.GroupBy = MetricCalcServiceConfigModel.Grouping.Building;

                var uiModel = (UtilityConsumptionCriteriaUIModel)_uiModel;

                //parse ids from dropdowns if selected
                var utilityTypeID = int.Parse(uiModel.UtilitiesDDL.SelectedValue);

                if (utilityTypeID != 0) _dataConfigModel.UtilityTypeID = utilityTypeID;

                SetChartDataType(uiModel.AnalysisTypesDDL.SelectedValue);
            }
        
        #endregion
    }
}