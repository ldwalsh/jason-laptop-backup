﻿using CW.Business;
using CW.Website._services.Chart.Interfaces;
using System;

namespace CW.Website._services.Chart.Factories
{
    /// <summary>
    /// A factory for chart services.
    /// </summary>
    public sealed class ChartServiceFactory<TChartService>
    {
        #region field(s)

            DataManager _dataMgr;

        #endregion

        #region constructor(s)

            public ChartServiceFactory(DataManager dataMgr) { _dataMgr = dataMgr; }

        #endregion

        #region method(s)

            /// <summary>
            /// Gets instance of an IChartService based on the configuration model type.
            /// </summary>
            /// <returns>The instantiated chart service implementing IChartService</returns>
            public IChartService GetChartService<TUIModel, TChartUIModel>
            (
                TUIModel uiModel,
                TChartUIModel chartUIModel
            )
            {
                try
                {
                    return (IChartService)Activator.CreateInstance
                    (
                        typeof(TChartService),
                        new object[] { _dataMgr, uiModel, chartUIModel }
                    );
                }
                catch            
                {
                    return null;
                }            
            }

        #endregion
    }
}