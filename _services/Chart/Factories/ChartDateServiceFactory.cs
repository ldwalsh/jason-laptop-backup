﻿using CW.Website._services.Chart.Interfaces;
using System;

namespace CW.Website._services.Chart.Factories
{
    public sealed class ChartDateServiceFactory<TChartDateService>
    {
        #region method(s)

            public IChartDateService<TChartDataModel> GetChartDateService<TChartDataModel>
            (
                TChartDataModel chartDateModel
            )
            {
                return (IChartDateService<TChartDataModel>)Activator.CreateInstance
                (
                    typeof(TChartDateService), 
                    new object[] { chartDateModel }
                );
            }

        #endregion
    }
}