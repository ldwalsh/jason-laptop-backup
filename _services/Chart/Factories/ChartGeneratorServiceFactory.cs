﻿using CW.Website._services.Chart.Interfaces;
using System;

namespace CW.Website._services.Chart.Factories
{
    public sealed class ChartGeneratorServiceFactory<TChartGeneratorService>
    {
        #region method(s)

            public IChartGeneratorService GetChartGeneratorService
            <
                TChartUIModel, 
                TChartDataModel, 
                TChartDateModel
            >
            (
                TChartUIModel uiModel,
                TChartDataModel dataModel,
                TChartDateModel dateModel
            )
            {
                try
                {
                    return (IChartGeneratorService)Activator.CreateInstance
                    (
                        typeof(TChartGeneratorService),
                        new object[] { uiModel, dataModel, dateModel }
                    );
                }
                catch
                {
                    return null;
                }
            }

        #endregion
    }
}