﻿using CW.Business;
using CW.Common.Constants;
using CW.Utility;
using CW.Website._services.Chart.Factories;
using CW.Website._services.Chart.Interfaces;
using CW.Website._services.Chart.Models;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Models;
using CW.Website._services.SearchCriteria.Models;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Options;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using static CW.Website._services.MetricCalculation.Models.MetricCalcServiceConfigModel;

namespace CW.Website._services.Chart
{
    public abstract class BaseConsumptionChartService : IChartService
    {
        #region field(s)

            DataManager _dataMgr;

            protected HighChartDateModel _highChartDateModel;

            protected HighChartUIModel _highChartUIModel;

            protected MetricCalcServiceConfigModel _dataConfigModel;

            protected List<ResultDataModel> _results;            

            protected SearchCriteriaUIModel _uiModel;

        #endregion

        #region Properties

            public MetricCalcServiceConfigModel DataConfigModel { get { return _dataConfigModel;  } }

        #endregion

        #region constructor(s)

        public BaseConsumptionChartService(DataManager dataMgr, HighChartUIModel highChartUIModel, SearchCriteriaUIModel uiModel)
            {
                _dataMgr = dataMgr;

                _highChartUIModel = highChartUIModel;

                _uiModel = uiModel;

                _highChartDateModel = new HighChartDateModel();

                SetDates();

                SetDataConfigModel();

                SetResults();

                SetXAxis();
            }

        #endregion

        #region interface method(s)

            public void CreateChart()
            {
                var highChartDataModel = new HighChartDataModel { Series = GetChartSeries() };

                var chartGenFactory = new ChartGeneratorServiceFactory<HighChartGeneratorService>();

                var chartGenSvc = chartGenFactory.GetChartGeneratorService
                (
                    _highChartUIModel,
                    highChartDataModel,
                    _highChartDateModel
                );

                chartGenSvc.GenerateChart();
            }

        #endregion

        #region method(s)

            #region abstract method(s)

                protected abstract Series[] GetChartSeries();

                protected abstract void SetDataConfigModel();


            #endregion

            #region virtual method(s)               
                
                protected virtual string GetTimePeriod()
                {
                    return BusinessConstants.Dropdowns.TimePeriod.Monthly;
                }   

                protected virtual void SetChartDataType(string type)
                {
                    switch (type)
                    {
                        case BusinessConstants.Dropdowns.AnalysisType.AvoidableCosts:
                            _dataConfigModel.DataType = ConsumptionDataType.AvoidableCosts;
                            _dataConfigModel.Name = "Avoidable Costs";
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.Carbon:
                            _dataConfigModel.DataType = ConsumptionDataType.Carbon;
                            _dataConfigModel.Name = "Cabon";
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.Cars:
                            _dataConfigModel.DataType = ConsumptionDataType.Cars;
                            _dataConfigModel.Name = "Cars";
                            _dataConfigModel.Multiplier = BusinessConstants.GoalConversions.CarbonToCars;
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.Cost:
                            _dataConfigModel.DataType = ConsumptionDataType.Cost;
                            _dataConfigModel.Name = "Cost";
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.Energy:
                            _dataConfigModel.DataType = ConsumptionDataType.Energy;
                            _dataConfigModel.Name = "Energy";
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.Trees:
                            _dataConfigModel.DataType = ConsumptionDataType.Trees;
                            _dataConfigModel.Name = "Trees";
                            _dataConfigModel.Multiplier = BusinessConstants.GoalConversions.CarbonToTrees;
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.CoolingEfficiency:
                            _dataConfigModel.DataType = ConsumptionDataType.CoolingEfficiency;
                            _dataConfigModel.Name = "Cooling Efficiency";
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.HeatingEfficiency: //fall through   
                            _dataConfigModel.DataType = ConsumptionDataType.HeatingEfficiency;
                            _dataConfigModel.Name = "Heating Efficiency";
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.ProcessWaterEfficiency:
                            _dataConfigModel.DataType = ConsumptionDataType.ProcWaterEfficiency;
                            _dataConfigModel.Name = "Process Water Efficiency";
                            break;
                        case BusinessConstants.Dropdowns.AnalysisType.VentilationEfficiency:
                            _dataConfigModel.DataType = ConsumptionDataType.VentEfficiency;
                            _dataConfigModel.Name = "Ventilation Efficiency";
                            break;
                        default:
                            _dataConfigModel.DataType = ConsumptionDataType.Energy;
                            _dataConfigModel.Name = "Energy";
                            break;
                    }
                }

                protected virtual void SetXAxis()
                {
                    var xAxisCategories = new List<string>();

                    var format = _highChartDateModel.DateFormat;

                    foreach (var dt in _highChartDateModel.Dates)
                    {
                        var formattedDate = (!string.IsNullOrWhiteSpace(format)) ? dt.ToString(format, _highChartUIModel.CultureInfo)
                                                                                 : dt.ToShortDateString();
                        xAxisCategories.Add(formattedDate);
                    }

                    _highChartUIModel.XAxis = new XAxis { Categories = xAxisCategories.ToArray(), Type = AxisTypes.Datetime };                    
                }

            #endregion

            protected void SetDates()
            {
                _highChartDateModel.Months = int.Parse(_uiModel.RangesDDL.SelectedValue);

                _highChartDateModel.TimePeriod = GetTimePeriod();

                var chartDateFactory = new ChartDateServiceFactory<HighChartDateService>();

                var chartDateSvc = chartDateFactory.GetChartDateService(_highChartDateModel);

                chartDateSvc.SetDates();
            }
            

            protected void SetCommonDataConfigModelFields()
            {
                _dataConfigModel = new MetricCalcServiceConfigModel();

                _dataConfigModel.BIDS = _uiModel.BuildingsLBE.Items.OfType<ListItem>()
                                           .Where(i => i.Value != "0")
                                           .Select(i => int.Parse(i.Value))
                                           .ToArray();
                _dataConfigModel.StartDate = _highChartDateModel.StartDate;

                _dataConfigModel.EndDate = _highChartDateModel.EndDate;

                _dataConfigModel.DataManager = _dataMgr;

                _dataConfigModel.Multiplier = 1.0;

                _dataConfigModel.AnalysisRange = GetTimePeriod();
            }

            protected void SetResults()
            {
                //var dataSvc = new MetricCalculationServiceFactory().GetDataCalcService<DataPoint>(_dataConfigModel);

                //if (dataSvc == null) return;

                //_results = dataSvc.CalculateResults();
            }

            protected string FormatValue(string value, bool isCurrency = false)
            {
                return CultureHelper.FormatCurrencyAsString(_highChartUIModel.CultureInfo.LCID, value);
            }

        #endregion
    }
}