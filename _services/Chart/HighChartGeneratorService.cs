﻿using CW.Utility;
using CW.Website._services.Chart.Interfaces;
using CW.Website._services.Chart.Models;
using DotNet.Highcharts;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System.Drawing;

namespace CW.Website._services.Chart
{
    public sealed class HighChartGeneratorService : IChartGeneratorService
    {
        #region field(s)

            HighChartUIModel _uiModel;

            HighChartDataModel _dataModel;

            HighChartDateModel _dateModel;

        #endregion

        #region constructor(s)

            public HighChartGeneratorService
            (
                HighChartUIModel uiModel, 
                HighChartDataModel dataModel,
                HighChartDateModel dateModel
            )
            {
                _uiModel = uiModel;

                _dataModel = dataModel;

                _dateModel = dateModel;
            }

        #endregion

        #region interface method(s)

            /// <summary>
            /// TODO: detail what this does
            /// </summary>
            public void GenerateChart()
            {
                var chart = CreateChart();         
            
                var script = chart.ChartScriptHtmlString().ToString();

                script = script.Replace("$(document).ready(function() {", string.Empty);

                script = ReplaceLastOccurrence(script, "});", string.Empty);

                _uiModel.Container.Text = chart.ChartContainerHtmlString().ToString();

                _uiModel.Script.Text = script;                
            }

        #endregion

        #region method(s)

            Highcharts CreateChart()
            {          
                var chart = new Highcharts(_uiModel.Name);

                var globalOptions = GetGlobalOptions();

                chart.SetOptions(globalOptions);

                var toolTip = GetToolTip();

                chart.SetTooltip(toolTip);

                var chartOptions = new DotNet.Highcharts.Options.Chart { Type = _uiModel.Type };
                
                var events = GetChartEvents();

                if (events != null) chartOptions.Events = events;

                chart.InitChart(chartOptions);

                var title = _uiModel.Title;

                var subTitle = _uiModel.SubTitle;

                var unitsLabel = _uiModel.UnitsLabel;

                chart.SetTitle(new Title { Text = (title != null) ? title : string.Empty });

                if (subTitle != null) chart.SetSubtitle(new Subtitle { Text = subTitle });

                if (unitsLabel != null) chart.SetYAxis(new YAxis { Title = new YAxisTitle { Text = unitsLabel }});

                chart.SetCredits(new Credits { Enabled = false });
            
                chart.SetSeries(_dataModel.Series);

                chart.SetXAxis(_uiModel.XAxis);

                SetChartLegend(chart);

                SetChartDateFormat(chart);

                return chart;
            }

            ChartEvents GetChartEvents()
            {
                if (_uiModel == null) return null;

                var events = new ChartEvents();

                if(!string.IsNullOrEmpty(_uiModel.LoadEvents)) events.Load = _uiModel.LoadEvents;

                if (!string.IsNullOrEmpty(_uiModel.DrilldownEvents)) events.Drilldown = _uiModel.DrilldownEvents;

                if (!string.IsNullOrEmpty(_uiModel.DrillupEvents)) events.Drillup = _uiModel.DrillupEvents;

                return events;
            }

            void SetChartLegend(Highcharts chart)
            {
                chart.SetLegend(new Legend
                {
                    BorderWidth = 1,

                    BackgroundColor = new BackColorOrGradient(Color.White),

                    Shadow = true
                });
            }

            string ReplaceLastOccurrence(string source, string item, string replace)
            {
                var place = source.LastIndexOf(item);

                return source.Remove(place, item.Length).Insert(place, replace);
            }

            void SetChartDateFormat(Highcharts chart)
            {
                if (chart == null || _uiModel?.CultureInfo == null || _dateModel?.DateFormat == null) return;

                string dateFormat;

                switch(_dateModel.DateFormat.ToLower())
                {
                    case "y":
                        dateFormat = _uiModel.CultureInfo.DateTimeFormat.YearMonthPattern;
                        break;
                    case "d": //fall through
                    default:
                        dateFormat = _uiModel.CultureInfo.DateTimeFormat.ShortDatePattern;
                        break;
                }

                //'save' the format string in the highchart container - may be used client side
                //to parse dates if using a table (highcharts does not support all cultures)
                chart.AddJavascripVariable($"dotnetDateFormat_{ _uiModel.Name }", $"'{ dateFormat }'");
            }

            GlobalOptions GetGlobalOptions()
            {
                var userCulture = _uiModel.CultureInfo;
            
                var globalOptions = new GlobalOptions(); //set global options

                if (userCulture != null)
                {
                    var cultureName = userCulture.Name;

                    if (!string.IsNullOrEmpty(cultureName))
                        globalOptions.Lang = new DotNet.Highcharts.Helpers.Lang().SetAndUseCulture(cultureName);
                }
                
                globalOptions.Colors = ColorHelper.SEChartingColorsSystem; //set colors                

                return globalOptions;
            }

            Tooltip GetToolTip()
            {
                var valPrefix = string.Empty;

                var valSuffix = string.Empty;

                var lcid = _uiModel.CurrencyLCID;

                if (lcid != 0) valPrefix = CultureHelper.GetCurrencySymbol(lcid);

                return new Tooltip //set tooltip options
                {
                    ValuePrefix = $"{valPrefix} ",

                    ValueSuffix = $" {valSuffix}",

                    ValueDecimals = 1
                };
            }

        #endregion
    }
}