﻿using CW.Business;
using CW.Website._services.Chart.Models;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.PerformanceDashboard.Models;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using System.Collections.Generic;
using static CW.Website._services.MetricCalculation.Models.MetricCalcServiceConfigModel;

namespace CW.Website._services.Chart
{
    public class SEUChartService : BaseConsumptionChartService
    {
        #region constructor(s)
        public SEUChartService
        (
            DataManager dataMgr,
            SEUChartCriteriaUIModel uiModel,
            HighChartUIModel highChartUIModel
        ) : base(dataMgr, highChartUIModel, uiModel)
        {                    
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Gets the data series for the chart, based on the data in the config model.
        /// </summary>
        /// <returns></returns>
        protected override Series[] GetChartSeries()
        {
            // return HiChartColumnSeriesHelper.Instance.GetSeries(_highChartDateModel.Dates, _results);

            var series = new List<Series>();

            Drilldown d = new Drilldown();
            Point p = new Point();

            HiChartDataPointModel test1 = new HiChartDataPointModel { name = "Building 1", y = 123.0, drilldown = true };            
            HiChartDataPointModel test2 = new HiChartDataPointModel { name = "Building 2", y = 322.0, drilldown = true };
            HiChartDataPointModel test3 = new HiChartDataPointModel { name = "Building 3", y = 687.0, drilldown = true };

            series.Add(new Series
            {
                Name = "Buildings",
                Data = new DotNet.Highcharts.Helpers.Data(new object[] { test1, test2, test3 } )
            });

            return series.ToArray();
        }

        protected override void SetXAxis()
        {
            string[] categories = { "Building 1", "Building 2", "Building 3" };

            _highChartUIModel.XAxis = new XAxis { Categories = categories, Type = AxisTypes.Category };
        }
        #endregion

        #region Methods
        /// <summary>
        /// Gets/sets the chart data service.
        /// </summary>
        /// <returns></returns>
        protected override void SetDataConfigModel()
        {
            SetCommonDataConfigModelFields();

            SetChartDataType(((SEUChartCriteriaUIModel)_uiModel).ddlData.AnalysisType);         

            if (_dataConfigModel.DataType == ConsumptionDataType.VentEfficiency)            
                _dataConfigModel.Multiplier = _dataConfigModel.Units == MetricCalcServiceConfigModel.MetricUnit.SI ? 3600 : 60;
        }
        #endregion
    }
}