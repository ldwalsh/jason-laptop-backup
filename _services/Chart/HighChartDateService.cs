﻿using CW.Common.Constants;
using CW.Website._services.Chart.Interfaces;
using CW.Website._services.Chart.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._services.Chart
{
    public sealed class HighChartDateService : IChartDateService<HighChartDateModel>
    {
        #region field(s)

            HighChartDateModel _highChartDateModel;

        #endregion

        #region constructor(s)

            public HighChartDateService(HighChartDateModel highChartDateModel)
            {
                _highChartDateModel = highChartDateModel;
            }

        #endregion

        #region interface method(s)

            public void SetDates()
            {
                _highChartDateModel.StartDate = GetStartDate();

                _highChartDateModel.EndDate = GetEndDate();

                _highChartDateModel.Dates = GetDatesBetween().ToArray();

                _highChartDateModel.DateFormat = GetDateFormat();
            }

            public HighChartDateModel GetChartDateModel() => _highChartDateModel;
        
        #endregion

        #region method(s)

            DateTime GetStartDate()
            {
                var months = _highChartDateModel.Months;

                switch (_highChartDateModel.TimePeriod)
                {
                    case BusinessConstants.Dropdowns.TimePeriod.Weekly:
                        DateTime startMonth = DateTime.Now.AddMonths(-months);                        
                        return GetNextSunday(startMonth);
                    case BusinessConstants.Dropdowns.TimePeriod.Daily:                         
                        return DateTime.Now.AddMonths(-months);
                    case BusinessConstants.Dropdowns.TimePeriod.Monthly: //fall through
                    default:
                        DateTime rawStartDate = DateTime.Now.AddMonths(-months);
                        return new DateTime(rawStartDate.Year, rawStartDate.Month, 1);
                }
            }

            //TODO: add in logic to calculate other time ranges
            DateTime GetEndDate() => DateTime.UtcNow;

            /// <summary>
            /// Increments the date to the next Sunday. We do this because the start
            /// of a week in table storage is considered a Sunday.
            /// </summary>
            /// <param name="startDate">The start date.</param>
            /// <returns>The next Sunday.</returns>
            DateTime GetNextSunday(DateTime startDate)
            {
                while (startDate.DayOfWeek != DayOfWeek.Sunday)
                    startDate = startDate.AddDays(1);
                
                return startDate;
            }

            /// <summary>
            /// Gets all DateTime values between two dates, at the specified time period
            /// increment.
            /// </summary>
            /// <param name="startDate">The starting DateTime</param>
            /// <param name="endDate">The ending DateTime</param>
            /// <returns></returns>
            IEnumerable<DateTime> GetDatesBetween()
            {
                DateTime iterator, limit;

                var startDate = _highChartDateModel.StartDate;

                var endDate = _highChartDateModel.EndDate;

                if (endDate > startDate)
                {
                    iterator = new DateTime(startDate.Year, startDate.Month, startDate.Day);

                    limit = endDate;
                }
                else
                {
                    iterator = new DateTime(endDate.Year, endDate.Month, endDate.Day);

                    limit = startDate;
                }

                while (iterator <= limit)
                {
                    yield return new DateTime(iterator.Year, iterator.Month, iterator.Day);

                    iterator = IncrementDateIterator(iterator);
                }
            }

            /// <summary>
            /// Sets the date format, according to DateTimeFormatInfo Class specification.
            /// E.g "d" is equivalent to ShortDatePattern.
            /// </summary>            
            string GetDateFormat()
            {
                switch (_highChartDateModel.TimePeriod)
                {
                    case BusinessConstants.Dropdowns.TimePeriod.Monthly:
                        return "Y";
                    case BusinessConstants.Dropdowns.TimePeriod.Daily: //fall through
                    case BusinessConstants.Dropdowns.TimePeriod.Weekly: //fall through                        
                    default:
                        return "d";                  
                }
            }

            /// <summary>
            /// Increments a datetime "iterator" by an amount specified by the
            /// configmodel time period.
            /// </summary>
            /// <param name="iterator">DateTime value to be incremented</param>
            DateTime IncrementDateIterator(DateTime iterator)
            {
                switch (_highChartDateModel.TimePeriod)
                {
                    case BusinessConstants.Dropdowns.TimePeriod.Daily:
                        iterator = iterator.AddDays(1);
                        break;
                    case BusinessConstants.Dropdowns.TimePeriod.Weekly:
                        iterator = iterator.AddDays(7);
                        break;
                    case BusinessConstants.Dropdowns.TimePeriod.Monthly:
                        iterator = iterator.AddMonths(1);
                        break;
                    default:
                        iterator = iterator.AddMonths(1);
                        break;
                }

                return iterator;
            }

        #endregion
    }
}