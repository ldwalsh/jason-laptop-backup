﻿using CW.Business;
using CW.Website._services.Chart.Helpers;
using CW.Website._services.Chart.Models;
using CW.Website._services.MetricCalculation.Factories;
using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.PerformanceDashboard.Models;
using CW.Website._services.Results.Factories;
using CW.Website._services.Results.Models;
using CW.Website._services.VDataRetrieval.Factories;
using DotNet.Highcharts.Options;
using System.Collections.Generic;
using static CW.Website._services.MetricCalculation.Models.MetricCalcServiceConfigModel;

namespace CW.Website._services.Chart
{
    /// <summary>
    /// Sets up the energy chart data model for the energy chart.
    /// </summary>
    public sealed class EnergyConsupmtionChartService : BaseConsumptionChartService
    {
        public System.Object ConsumptionEnums { get; private set; }
        #region constructor(s)

        public EnergyConsupmtionChartService
            (
                DataManager dataMgr,
                EnergyChartUICriteriaModel uiModel,
                HighChartUIModel highChartUIModel
            ) : base(dataMgr, highChartUIModel, uiModel)
            {            
                
            }

        #endregion

        #region method(s)

            protected override string GetTimePeriod()
            {
                return ((EnergyChartUICriteriaModel)_uiModel).EnergyDDLData.TimePeriod;
            }

            /// <summary>
            /// Gets the data series for the chart, based on the data in the config model.
            /// </summary>
            /// <returns></returns>
            protected override Series[] GetChartSeries()
            {
                var dataRetrievalSvc = new DataServiceFactory().GetDataService(_dataConfigModel);

                var resultsFormatter = new ResultsFormatterServiceFactory().GetResultsFormatterService(_dataConfigModel, dataRetrievalSvc);

                var dataSvc = new MetricCalculationServiceFactory().GetCalculationService(_dataConfigModel, resultsFormatter);

                if (dataSvc == null) return null;

                //var results = dataSvc.CalculateResults();

                List<ResultDataModel> results = null;

                if (results == null || results.Count == 0) return null;

                return HiChartLineSeriesHelper.Instance.GetSeries(_highChartDateModel.Dates, results);
            }

            /// <summary>
            /// Gets/sets the chart data service.
            /// </summary>
            /// <returns></returns>
            protected override void SetDataConfigModel()
            {
                var uiModel = (EnergyChartUICriteriaModel)_uiModel;

                SetCommonDataConfigModelFields();                
                
                SetChartDataType(uiModel.EnergyDDLData.AnalysisType);                       
            
                //parse ids from dropdowns if selected (Luke: hopefully we can make this somewhat easier somehow)
                int equipmentClassID, equipmentID, utilityTypeID, submeterID, consumptionPointClassID, plantID;

                _dataConfigModel.EquipmentClassID = int.TryParse(uiModel.EnergyDDLData.EquipmentClass, out equipmentClassID) &&
                    equipmentClassID > 0 ? (int?)equipmentClassID : null;

                _dataConfigModel.EquipmentID = int.TryParse(uiModel.EnergyDDLData.Equipment, out equipmentID) &&
                    equipmentID > 0 ? (int?)equipmentID : null;

                _dataConfigModel.PlantID = int.TryParse(uiModel.EnergyDDLData.Plant, out plantID) &&
                    plantID > 0 ? (int?)plantID : null;

                _dataConfigModel.UtilityTypeID = int.TryParse(uiModel.EnergyDDLData.ConstumptionType, out utilityTypeID)
                    && utilityTypeID > 0 ? (int?)utilityTypeID : null;

                _dataConfigModel.SubmeterID = int.TryParse(uiModel.EnergyDDLData.BuildingSubmeter, out submeterID) &&
                    submeterID > 0 ? (int?)submeterID : null;

                _dataConfigModel.ConsumptionPointClassID = int.TryParse(uiModel.EnergyDDLData.ConstumptionType, out consumptionPointClassID) &&
                    consumptionPointClassID > 0 ? (int?)consumptionPointClassID : null;                

                if (_dataConfigModel.DataType == ConsumptionDataType.VentEfficiency)
                        _dataConfigModel.Multiplier = _dataConfigModel.Units == MetricCalcServiceConfigModel.MetricUnit.SI ? 3600 : 60;            
                }                
           
        #endregion
    }
}