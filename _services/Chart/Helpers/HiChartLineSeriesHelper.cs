﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Models;
using DotNet.Highcharts.Options;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace CW.Website._services.Chart.Helpers
{
    /// <summary>
    /// A singleton class that helps create Series for HiChart line graphs. Helps
    /// calculate predicted, cumulative, and differences in series.
    /// </summary>
    public sealed class HiChartLineSeriesHelper
    {
        #region Instance
        //ensures full lazy loading
        private static readonly Lazy<HiChartLineSeriesHelper> mInstance = 
            new Lazy<HiChartLineSeriesHelper>(() => new HiChartLineSeriesHelper());     

        public static HiChartLineSeriesHelper Instance { get { return mInstance.Value; } }

        private HiChartLineSeriesHelper() { }

        #endregion

        /// <summary>
        /// Gets the series for the linear high chart from the MetricResultModels.
        /// </summary>
        /// <param name="xAxisTimeValues">The x-axis datetime values</param>
        /// <param name="results">The results for the metric</param>
        /// <returns>An array of hichart Series objects.</returns>
        public Series[] GetSeries(DateTime[] xAxisTimeValues, IEnumerable<ResultDataModel> results)
        {
            if (xAxisTimeValues == null || results == null || results.Count() == 0) return null;

            //group the results by the series name (there can be up to two per group - predicted and actual)
            var groupedResults = results.GroupBy(_ => _.Name);

            var series = new List<Series>();

            foreach (var group in groupedResults)
            {
                if (group.Count() > 2) continue;

                //add predicted and actual line series for the group
                var actual = group.Where(_ => !_.IsPredicted).FirstOrDefault();
                var predicted = group.Where(_ => _.IsPredicted).FirstOrDefault();

                var actualSeries = createSeries(xAxisTimeValues, actual);
                var preSeries = createSeries(xAxisTimeValues, predicted);

                if(actualSeries != null) series.Add(actualSeries);
                if(preSeries != null) series.Add(preSeries);
                
                //if actual and predicted data exist, add the delta line and cumulative delta line
                if(actualSeries?.Data != null && preSeries?.Data != null)
                {
                    List<Series> calculatedSeries = getDeltaAndCumSeries(xAxisTimeValues, actualSeries, preSeries);
                    series.AddRange(calculatedSeries);
                }
            }

            return series.ToArray();
        }

        /// <summary>
        /// Creates a single hichart Series object by matching results to the x-axis
        /// by datetime.
        /// </summary>
        /// <param name="xAxisTimeValues">The x axis values.</param>
        /// <param name="result">The metric results.</param>
        /// <returns>A single hichart Series object.</returns>
        private Series createSeries(DateTime[] xAxisTimeValues, ResultDataModel result)
        {
            if (xAxisTimeValues == null || result?.Data == null) return null;

            Series series = null;

            DotNet.Highcharts.Helpers.Data data = getSeriesData(xAxisTimeValues, result.Data.Cast<DataModel>());

            if(data != null)
            {
                string name = result.IsPredicted ? "Predicted " + result.Name : result.Name;

                series = new Series { Name = name, Data = data };                
            }

            return series;
        }

        //todo: optimize/get rid of query
        /// <summary>
        /// Creates the data object for the hi chart series.
        /// </summary>
        /// <param name="xAxisTimeValues">X axis values.</param>
        /// <param name="dataPoints">Data points from the results</param>
        /// <returns>Hi charts Data object</returns>
        DotNet.Highcharts.Helpers.Data getSeriesData(DateTime[] xAxisTimeValues, IEnumerable<DataModel> dataPoints)
        {
            if (xAxisTimeValues == null || dataPoints == null) return null;

            var data = new object[xAxisTimeValues.Length];

            for (int i = 0; i < data.Length; i++)
            {
                var xTime = xAxisTimeValues[i];

                var dp = dataPoints.Where(_ => _.TimePeriod.Date == xTime.Date).FirstOrDefault();

                data[i] = dp == null ? null : dp.Value;                
            }

            return new DotNet.Highcharts.Helpers.Data(data);
        }

        /// <summary>
        /// Returns a list of hichart series for the delta and cum delta lines,
        /// if they should be added.
        /// </summary>
        /// <param name="actualSeries">The actual series.</param>
        /// <param name="preSeries">The predicted series.</param>
        /// <returns>The delta and cum delta series (if exists/should be added)</returns>
        private List<Series> getDeltaAndCumSeries(DateTime[] xAxisTimeValues, Series actualSeries, Series preSeries)
        {
            if (actualSeries?.Data?.ArrayData == null || preSeries?.Data?.ArrayData == null || 
                actualSeries.Data.ArrayData.Length != preSeries.Data.ArrayData.Length || 
                actualSeries.Data.ArrayData.Length == 0) return null;

            bool deltaSeriesDataComplete = true;
            int numDeltaPointsCalculated = 0;

            //create a data array for delta and cumulative data series
            var deltaData = new object[actualSeries.Data.ArrayData.Length];
            var cumData = new object[actualSeries.Data.ArrayData.Length];

            //initialize the cum difference between actual and predicted
            double cumDelta = 0.0;

            //populate the delta and cum data arrays
            for(int i = 0; i < actualSeries.Data.ArrayData.Length; i++)
            {
                var actualVal = actualSeries.Data.ArrayData[i];
                var preVal = preSeries.Data.ArrayData[i];

                if(actualVal != null && preVal != null)
                {
                    double deltaVal = (double)actualVal - (double)preVal;
                    cumDelta = deltaVal + cumDelta;

                    deltaData[i] = deltaVal;
                    cumData[i] = cumDelta;

                    numDeltaPointsCalculated++;
                }
                else
                {
                    deltaData[i] = null;
                    deltaSeriesDataComplete = false;
                    continue;
                }
            }

            //return list of series
            List<Series> series = new List<Series>();

            //add delta series
            Series deltaSeries = new Series
            {
                Name = actualSeries.Name + " - " + preSeries.Name,
                Data = new DotNet.Highcharts.Helpers.Data(deltaData)
            };
            series.Add(deltaSeries);

            //get best fit line for the delta series if at least 2 points
            if(numDeltaPointsCalculated >=2)
            {
                Series deltaTrend = getTrendLine(xAxisTimeValues, deltaSeries);
                if (deltaTrend != null) series.Add(deltaTrend);                
            }

            //add cum series if delta series is complete
            if (deltaSeriesDataComplete)
            {
                Series cumSeries = new Series
                {
                    Name = "Cumulative ( " + actualSeries.Name + " - " + preSeries.Name + " )",
                    Data = new DotNet.Highcharts.Helpers.Data(cumData)
                };
                series.Add(cumSeries);
            }

            return series;
        }

        /// <summary>
        /// Gets the trend line for delta.
        /// </summary>
        /// <param name="delta"></param>
        /// <returns></returns>
        private Series getTrendLine(DateTime[] xVals, Series series)
        {
            double[] dateTimeDoubleFormat = convertXValsToDouble(xVals);

            if (dateTimeDoubleFormat == null || series == null) return null;

            double?[] yVals;

            //try to cast the series data to double array
            try { yVals = series.Data.ArrayData.Cast<double?>().ToArray(); }
            catch { return null; }

            double m;
            double b;

            BestFitLineHelper.Instance.GetLinearBestFit(dateTimeDoubleFormat, yVals, out m, out b);

            object[] data = getTrendData(dateTimeDoubleFormat, m, b);
            
            Series trendSeries = new Series
            {
                Name = "Linear Trend for " + series.Name,
                Data = new DotNet.Highcharts.Helpers.Data(data),
                PlotOptionsArea = new PlotOptionsArea
                {
                    DashStyle = DotNet.Highcharts.Enums.DashStyles.LongDash,
                    Color = Color.Black,
                    Marker = new PlotOptionsAreaMarker { Enabled = false }
                }
            };            

            return trendSeries;
        }

        private double[] convertXValsToDouble(DateTime[] xVals)
        {
            //return null if anything's null
            if (xVals == null || xVals.Where(_ => _ == null).Any()) return null;

            double[] result = new double[xVals.Length];

            for(int i = 0; i < xVals.Length; i++)
            {
                result[i] = xVals[i].ToOADate();
            }

            return result;
        }

        /// <summary>
        /// Gets yvalue for every x value on trend line.
        /// </summary>
        private object[] getTrendData(double[] xVals, double m, double b)
        {
            if (xVals == null) return null;

            object[] yVals = new object[xVals.Length];

            for(int i = 0; i < xVals.Length; i++)
            {
                yVals[i] = Math.Round(m * xVals[i] + b, 3);
            }

            return yVals;
        }
    }
}