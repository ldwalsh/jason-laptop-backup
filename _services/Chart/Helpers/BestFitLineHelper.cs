﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.Chart.Helpers
{
    public sealed class BestFitLineHelper
    {
        #region Instance
        //ensures full lazy loading
        private static readonly Lazy<BestFitLineHelper> mInstance =
            new Lazy<BestFitLineHelper>(() => new BestFitLineHelper());

        public static BestFitLineHelper Instance { get { return mInstance.Value; } }

        private BestFitLineHelper() { }

        #endregion

        /// <summary>
        /// Calculates m and b in the equation y = mx + b (line of best fit)
        /// for a given series of x and y points. The arrays of x and y values
        /// must have the same length.
        /// </summary>
        /// <param name="xPoints">The x values</param>
        /// <param name="yPoints">The y values</param>
        /// <param name="m">m</param>
        /// <param name="b">b</param>
        public void GetLinearBestFit(double[] xPoints, double?[] yPoints, out double m, out double b)
        {
            if (xPoints == null || yPoints == null || xPoints.Length != yPoints.Length || xPoints.Length == 0)
                throw new ArgumentOutOfRangeException("X value and Y value arrays must contain values and must be equal lengths.");

            // calculate m and b for y = mx + b   
            double numPoints = xPoints.Length;
            double sumXVals = 0;
            double sumYVals = 0;
            double sumXValsSq = 0;
            double sumXValProdYVal = 0;

            for(int i = 0; i < yPoints.Length; i++)
            {
                double? yPoint = yPoints[i];

                if (yPoint.HasValue)
                {
                    double x = xPoints[i];
                    double y = yPoint.Value;

                    sumXVals += x;
                    sumYVals += y;
                    sumXValsSq += x * x;
                    sumXValProdYVal += x * y;
                }
            }

            // Solve for m and b.
            m = (sumXValProdYVal * numPoints - sumXVals * sumYVals) / (sumXValsSq * numPoints - sumXVals * sumXVals);
            b = (sumXValProdYVal * sumXVals - sumYVals * sumXValsSq) / (sumXVals * sumXVals - numPoints * sumXValsSq);
        }
    }
}