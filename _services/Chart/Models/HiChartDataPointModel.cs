﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.Chart.Models
{
    /// <summary>
    /// A server-side representation of a high chart data point.
    /// 
    /// NOTE - DO NOT CHANGE CAPITALIZATION: This model is directly
    /// serialized to the client side.
    /// </summary>
    public class HiChartDataPointModel
    {
        public string name { get; set; }
        public double y { get; set; }
        public bool drilldown { get; set; }
    }
}