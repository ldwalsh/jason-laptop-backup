﻿using DotNet.Highcharts.Options;
using System;

namespace CW.Website._services.Chart.Models
{
    public sealed class HighChartDataModel
    {
        #region method(s)

            public Series[] Series { get; set; }
        
        #endregion
    }
}