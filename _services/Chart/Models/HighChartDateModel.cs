﻿using System;

namespace CW.Website._services.Chart.Models
{
    public sealed class HighChartDateModel
    {
        #region properties

            public int Months { get; set; }

            public string TimePeriod { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }
        
            public DateTime[] Dates { get; set; }

            public string DateFormat { get; set; }

        #endregion
    }
}