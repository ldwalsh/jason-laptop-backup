﻿using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Options;
using System.Globalization;
using System.Web.UI.WebControls;

namespace CW.Website._services.Chart.Models
{
    /// <summary>
    /// Contains all necessary UI information for the high chart
    /// </summary>
    public sealed class HighChartUIModel
    {
        #region properties

            public ChartTypes Type { get; set; }

            public string LoadEvents { get; set; }            

            public string DrilldownEvents { get; set; }

            public string DrillupEvents { get; set; }

            public string Name { get; set; }

            public string Title { get; set; }

            public string SubTitle { get; set; }

            public string UnitsLabel { get; set; }

            public XAxis XAxis { get; set; }

            public Literal Container { get; set; }

            public Literal Script { get; set; }

            public CultureInfo CultureInfo { get; set; }

            public int CurrencyLCID { get; set; }

        #endregion
    }
}