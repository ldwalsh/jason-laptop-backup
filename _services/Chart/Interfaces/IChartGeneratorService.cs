﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW.Website._services.Chart.Interfaces
{
    public interface IChartGeneratorService
    {
        #region method(s)

            void GenerateChart();

        #endregion
    }
}