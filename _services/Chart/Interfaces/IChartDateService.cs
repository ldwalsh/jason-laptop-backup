﻿namespace CW.Website._services.Chart.Interfaces
{
    public interface IChartDateService<TChartDateModel>
    {
        #region method(s)

            void SetDates();

            TChartDateModel GetChartDateModel();

        #endregion
    }
}