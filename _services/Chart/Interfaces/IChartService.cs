﻿using CW.Website._services.MetricCalculation.Models;

namespace CW.Website._services.Chart.Interfaces
{
    /// <summary>
    /// A general chart service that can be called by the UI to set up and render a chart.
    /// </summary>
    public interface IChartService
    {
        #region Properties

            MetricCalcServiceConfigModel DataConfigModel { get; }

        #endregion

        #region method(s)

        /// <summary>
        /// TODO: explain
        /// </summary>
            void CreateChart();

        #endregion
    }
}