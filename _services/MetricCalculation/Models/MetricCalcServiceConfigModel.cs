﻿using CW.Business;
using System;

namespace CW.Website._services.MetricCalculation.Models
{
    public class MetricCalcServiceConfigModel
    {
        #region enum(s)        
        public string EntityName { get; set; }

            public enum MetricUnit { SI, IP };

            public enum ConsumptionDataType
            {
                AvoidableCosts,
                Carbon,
                Cars,
                CoolingEfficiency,
                Cost,
                Energy,
                HeatingEfficiency,
                ProcWaterEfficiency,
                Trees,
                VentEfficiency,
            }

            public enum Grouping { Building, Equipment };

        #endregion

        #region properties

            public DataManager DataManager { get; set; }

            public int[] BIDS { get; set; }

            public string AnalysisRange { get; set; }

            public string Name { get; set; }

            public DateTime StartDate { get; set; }

            public DateTime EndDate { get; set; }

            public bool GetPredictedData { get; set; }

            public ConsumptionDataType DataType { get; set; }

            public MetricUnit Units { get; set; }

            public double Multiplier { get; set; }

            public int? EquipmentClassID { get; set; }

            public int? EquipmentID { get; set; }

            public int? UtilityTypeID { get; set; }

            public int? SubmeterID { get; set; }

            public int? ConsumptionPointClassID { get; set; }

            public int? PlantID { get; set; }

            public Grouping GroupBy { get; set; }

        #endregion
    }
}