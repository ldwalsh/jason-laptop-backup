﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.MetricCalculation;
using CW.Website._services.Results.Models;
using CW.Website._services.VDataRetrieval.Factories;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.MetricCalculation
{
    public class HeatingEfficiencyMetricService<TMetricResult> : IMetricCalculationService
    {
        private MetricCalcServiceConfigModel mConfigData;
        private IDataService mDataSvc;

        public HeatingEfficiencyMetricService(MetricCalcServiceConfigModel configData)
        {
            mConfigData = configData;
            mDataSvc = new DataServiceFactory().GetDataService(mConfigData);
        }

        public List<ResultDataModel> CalculateResults()
        {
            List<ResultDataModel> results = new List<ResultDataModel>();
            /*
            IResultFormatter resultFormatter = new ResultFormatterFactory().GetResultFormatter<TMetricResult>();

            //get actual result
            ResultDataModel actual = getResult(resultFormatter, false);
            if(actual != null) results.Add(actual);

            //get predicted result
            if (mConfigData.GetPredictedData)
            {
                ResultDataModel predicted = getResult(resultFormatter, true);
                if(predicted != null) results.Add(predicted);
            }*/

            return results;
        }

        /*
        private ResultDataModel getResult(IResultFormatter resultFormatter, bool isPredicted)
        {
            List<VDataQueryResult> queryResults = mDataSvc.GetData(isPredicted);

            List<DataModel> calculation = null;

            //if one boiler selected
            if(mConfigData.EquipmentID.HasValue)
            {
                VDataQueryResult boilerEfficiencyUseQuery = queryResults?.Where(_ => _.ResultName == "BoilerEfficiency").FirstOrDefault();

                if (boilerEfficiencyUseQuery == null) return null;

                DataModel[] boilerEfficiencyUseData = resultFormatter.GetDataPoints(boilerEfficiencyUseQuery, mConfigData.Multiplier);

                calculation = getSingleBoilerEfficiency(boilerEfficiencyUseData);
            }
            //all plants or one plant/all boilers
            else
            {
                VDataQueryResult heatingUseQuery = queryResults?.Where(_ => _.ResultName == "HeatingUse").FirstOrDefault();
                VDataQueryResult plantGasEnergyQuery = queryResults?.Where(_ => _.ResultName == "PlantGasEnergy").FirstOrDefault();
                VDataQueryResult plantSteamEnergyQuery = queryResults?.Where(_ => _.ResultName == "PlantSteamEnergy").FirstOrDefault();

                if (heatingUseQuery == null || plantGasEnergyQuery == null || plantSteamEnergyQuery == null) return null;

                DataModel[] heatingUseData = resultFormatter.GetDataPoints(heatingUseQuery, mConfigData.Multiplier);
                DataModel[] plantGasEnergyData = resultFormatter.GetDataPoints(plantGasEnergyQuery, mConfigData.Multiplier);
                DataModel[] plantSteamEnergyData = resultFormatter.GetDataPoints(plantSteamEnergyQuery, mConfigData.Multiplier);

                calculation = getPlantHeatingEfficiency(heatingUseData, plantGasEnergyData, plantSteamEnergyData);
            }

            MetricResultModel result = new MetricResultModel();
            result.Name = mConfigData.Name;
            result.data = calculation;
            result.isPredicted = isPredicted;

            return null;
        }
        */

        private List<DataModel> getSingleBoilerEfficiency(DataModel[] boilerEfficiencyUseData)
        {
            if (boilerEfficiencyUseData == null) return null;            

            return boilerEfficiencyUseData.ToList();
        }

        private List<DataModel> getPlantHeatingEfficiency(DataModel[] heatingUseData, DataModel[] plantGasEnergyData,
            DataModel[] plantSteamEnergyData)
        {
            if (heatingUseData == null || plantGasEnergyData == null || plantSteamEnergyData == null) return null;

            List<DataModel> results = new List<DataModel>();

            DateTime?[] allTimePeriods = getTimePeriods(new List<DataModel[]> { heatingUseData, plantGasEnergyData, plantSteamEnergyData });

            if (allTimePeriods == null) return null;

            int timePeriodCounter = 0;
            int heatingUseCounter = 0;
            int plantGasCounter = 0;
            int plantSteamCounter = 0;

            while(timePeriodCounter < allTimePeriods.Length)
            {
                DateTime? timePeriod = allTimePeriods[timePeriodCounter];

                if (timePeriod == null)
                {
                    timePeriodCounter++;
                    continue;
                }

                //get the datapoints for heating, plantgas, plantsteam that correspond
                //to the datetime (null if no value exists), and increment counter
                DataModel heatingUseDP = getDataPointForTimePeriod(timePeriod, heatingUseData, ref heatingUseCounter);
                DataModel plantGasDP = getDataPointForTimePeriod(timePeriod, plantGasEnergyData, ref plantGasCounter);
                DataModel plantSteamDP = getDataPointForTimePeriod(timePeriod, plantSteamEnergyData, ref plantSteamCounter);

                DataModel result = calculateEfficiency(heatingUseDP, plantGasDP, plantSteamDP);

                if (result != null) results.Add(result);
            }

            return results;            
        }

        private DateTime?[] getTimePeriods(List<DataModel[]> dataPointArrays)
        {
            if (dataPointArrays == null) return null;

            return dataPointArrays.Where(arr => arr != null)
                .SelectMany(arr => arr.Select(_ => _?.TimePeriod))
                .Distinct()
                .OrderBy(_ => _)
                .ToArray();            
        }

        private DataModel getDataPointForTimePeriod(DateTime? timePeriod, DataModel[] data, ref int counter)
        {
            if (data == null || counter >= data.Length) return null;

            DateTime? dataTimePeriod = data[counter]?.TimePeriod;

            //if the datetime from the data is less than the given time period, advance counter
            while (Nullable.Compare(dataTimePeriod, timePeriod) < 0)
            {
                counter++;
                if (counter >= data.Length)
                {
                    dataTimePeriod = null;
                    return null;
                }
                else
                {
                    dataTimePeriod = data[counter]?.TimePeriod;
                }
            }

            //if the data's datetime is equal, return that datapoint else return null
            //(it is greater than the time period of interest
            if(Nullable.Compare(dataTimePeriod, timePeriod) == 0)
            {
                return data[counter];
            }
            else
            {
                return null;
            }            
        }

        private DataModel calculateEfficiency(DataModel heatingUseDP, DataModel plantGasDP, DataModel plantSteamDP)
        {
            DataModel result = new DataModel();

            //set the resulting datapoint's timeperiod to the first non-null timeperiod
            DateTime? timePeriod = heatingUseDP?.TimePeriod;
            if (!timePeriod.HasValue) timePeriod = plantGasDP?.TimePeriod;
            if (!timePeriod.HasValue) timePeriod = plantSteamDP?.TimePeriod;

            //get the data
            double heating = (heatingUseDP == null || !heatingUseDP.Value.HasValue) ? 0.0 : heatingUseDP.Value.Value;
            double plantGas = (plantGasDP == null || !plantGasDP.Value.HasValue) ? 0.0 : plantGasDP.Value.Value;
            double plantSteam = (plantSteamDP == null || !plantSteamDP.Value.HasValue) ? 0.0 : plantSteamDP.Value.Value;    

            //perform calculation
            if(plantGas == 0.0 && plantSteam == 0.0)
            {
                result.Value = 0.0;
            }
            else 
            {
                result.Value = heating / (plantGas + plantSteam);
            }

            return result;
        }
    }
}