﻿using CW.Website._services.Results.Interfaces;
using CW.Website._services.Results.Models;
using System.Collections.Generic;
using System.Linq;

namespace CW.Website._services.MetricCalculation
{
    public abstract class MetricServiceBase : IMetricCalculationService
    {
        #region field(s)

            List<ResultDataModel> _model;

        #endregion

        #region properties

            protected virtual bool IsDefaultCalc { get; set; } = true;

            protected virtual string Numerator { get; set; }

            protected virtual string Denominator { get; set; }

            protected virtual string ResultName { get; set; }

        #endregion

        #region constructor(s)

            public MetricServiceBase(IResultsFormatterService resultsFormtterSvc)
            {
                _model = resultsFormtterSvc.FormatResults();
            }

        #endregion

        #region interface method(s)

            public List<ResultDataModel> CalculateResults()
            {
                var caluatedResults = GetResults();

                return caluatedResults;
            }

        #endregion

        #region method(s)

            #region virtual method(s)

                protected virtual ResultDataModel GetCalculatedResult(bool isPredicted)
                {
                    var num = GetDataModel(Numerator, isPredicted);

                    var denom = GetDataModel(Denominator, isPredicted);

                    if (num == null || denom == null) return null;

                    var efficiencyResults = GetEfficiency(num.Data, denom.Data);

                    return new ResultDataModel()
                    {
                        Name = ResultName,

                        IsPredicted = isPredicted,

                        Data = efficiencyResults
                    };
                }

            #endregion

            protected ResultDataModel GetDataModel(string name, bool isPredicted)
            {
                var results = _model.Where(_ => _.Name == name && _.IsPredicted == isPredicted);

                return results.FirstOrDefault();
            }

            List<ResultDataModel> GetResults()
            {
                if (IsDefaultCalc) return _model;

                var results = new List<ResultDataModel>();

                var actualModel = GetCalculatedResult(false);

                results.Add(actualModel);

                var hasPredicted = _model.Where(_ => _.IsPredicted == true).Any();

                if (hasPredicted)
                {
                    var preDataModel = GetCalculatedResult(true);

                    results.Add(preDataModel);
                }

                return results;
            }

            /// <summary>
            /// Gets efficiency by matching numerator and denominator data by datetime and calculating
            /// efficiency when there's a match. Assumes that the array parameters are already sorted
            /// by DateTime in increasing order and does not contain null values.
            /// </summary>
            /// <param name="numData">Array of numerator data points</param>
            /// <param name="denomData">Array of denominator use data points</param>
            /// <returns>List of efficiency data points</returns>
            protected DataModel[] GetEfficiency
            (
                DataModel[] numData, 
                DataModel[] denomData, 
                double multiplier = 1.0
            )
            {
                if (numData == null || denomData == null) return null;

                var efficiencyResults = new List<DataModel>();

                int n = 0;
                int d = 0;

                while (n < numData.Length)
                {
                    var numDataPoint = numData[n];

                    var denomDataPoint = denomData[d];

                    if (numDataPoint.TimePeriod == denomDataPoint.TimePeriod)
                    {
                        var efficiency = CalculateEfficiency
                        (
                            numDataPoint, 
                            denomDataPoint, 
                            multiplier
                        );

                        efficiencyResults.Add(efficiency);

                        n++;

                        d++;
                    }
                    else if (numDataPoint.TimePeriod < denomDataPoint.TimePeriod)
                    {
                        n++;
                    }
                    else
                    {
                        d++;
                    }
                }

                return efficiencyResults.ToArray();
            }

            /// <summary>
            /// Calculates efficiency for one time period. Returns null if denom is 0.
            /// </summary>
            /// <param name="num">Numerator data point.</param>
            /// <param name="denom">Denominator use data point.</param>
            /// <returns></returns>
            DataModel CalculateEfficiency(DataModel num, DataModel denom, double multiplier)
            {
                if (num == null || denom == null || denom.Value == 0.0) return null;

                var efficiency = new DataModel();

                efficiency.TimePeriod = num.TimePeriod;

                efficiency.HasAllData = num.HasAllData && denom.HasAllData;

                efficiency.Value = num.Value / denom.Value;

                return efficiency;
            }

        #endregion
    }
}