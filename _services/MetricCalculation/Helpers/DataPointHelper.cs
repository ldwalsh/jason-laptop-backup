﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.MetricCalculation.Helpers
{
    public class DataPointHelper
    {
        private double mMultiplier;

        public DataPointHelper(double multiplier)
        {
            mMultiplier = multiplier;
        }

        public double GetConvertedValue(string rawValue)
        {
            double? value = getDouble(rawValue);
            if (value == null) return 0;
            
            return mMultiplier * value.Value;
        }

        private double? getDouble(string rawValue)
        {
            double result;

            double.TryParse(rawValue, out result);

            return result;
        }

        public bool IsValidDouble(string rawValue)
        {
            double dummy;

            return double.TryParse(rawValue, out dummy);
        }
    }
}