﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.MetricCalculation.ResultFormatter.Interfaces;
using CW.Website._services.VDataRetrieval.Interfaces;
using CW.Website._services.VDataRetrieval.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website._services.MetricCalculation.Helpers
{
    /// <summary>
    /// A class that contains calculation logic for calculations that involve only
    /// one "series", e.g. summing "CostSavings" to get Avoidable Costs.
    /// </summary>
    public class SingleInputResultsHelper
    {
        private IDataRetrievalService mDataSvc;
        private IResultFormatter mFormatter;
        private MetricCalcServiceConfigModel mConfigData;

        public SingleInputResultsHelper(IDataRetrievalService dataSvc, IResultFormatter formatter, MetricCalcServiceConfigModel configData)
        {
            mDataSvc = dataSvc;
            mFormatter = formatter;
            mConfigData = configData;
        }

        public List<MetricResultModel> GetResultSet()
        {
            List<MetricResultModel> results = new List<MetricResultModel>();

            //get the actual data
            MetricResultModel result = getResult(false);
            results.Add(result);

            //get predicted data
            if (mConfigData.GetPredictedData)
            {
                MetricResultModel preResult = getResult(true);
                results.Add(preResult);
            }

            return results;
        }

        private MetricResultModel getResult(bool isPredicted)
        {
            VDataQueryResult queryResult = mDataSvc.GetData(isPredicted).FirstOrDefault();

            if (queryResult != null)
            {
                DataPoint[] dataPoints = mFormatter.GetDataPoints(queryResult, mConfigData.Multiplier);
                MetricResultModel result = mFormatter.GetResultSet(dataPoints, mConfigData.Name, isPredicted);
                return result;
            }

            return null;
        }

    }
}