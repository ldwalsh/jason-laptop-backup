﻿using CW.Website._services.Results.Models;
using System.Collections.Generic;

namespace CW.Website._services.MetricCalculation
{
    public interface IMetricCalculationService
    {
        #region method(s)

            List<ResultDataModel> CalculateResults();

        #endregion
    }
}