﻿using CW.Website._services.MetricCalculation.Models;
using CW.Website._services.Results.Interfaces;
using CW.Website._services.Results.Models;
using System.Collections.Generic;
using static CW.Website._services.MetricCalculation.Models.MetricCalcServiceConfigModel;

namespace CW.Website._services.MetricCalculation.Factories
{
    public sealed class MetricCalculationServiceFactory
    {
        #region method(s)

            public IMetricCalculationService GetCalculationService
            (
                MetricCalcServiceConfigModel model,
                IResultsFormatterService resultsFormatterSvc
            )
            {
                switch (model.DataType)
                {
                    case ConsumptionDataType.AvoidableCosts: //fall through
                    case ConsumptionDataType.Carbon: //fall through
                    case ConsumptionDataType.Cars: //fall through
                    case ConsumptionDataType.Cost: //fall through
                    case ConsumptionDataType.Trees: //fall through                
                        return new SingleMetricService(resultsFormatterSvc);
            //        //case Chart.Models.ConsumptionEnums.ConsumptionDataType.Energy:
            //        //    return typeof(EnergyMetricService);
            //        //case Chart.Models.ConsumptionEnums.ConsumptionDataType.CoolingEfficiency:
            //        //    return typeof(CoolingEfficiencyMetricService);
            //        //case Chart.Models.ConsumptionEnums.ConsumptionDataType.HeatingEfficiency:
            //        //    return typeof(HeatingEfficiencyMetricService);
            //        //case Chart.Models.ConsumptionEnums.ConsumptionDataType.ProcWaterEfficiency:
            //        //    return typeof(ProcWaterEfficiencyMetricService);
            //        //case Chart.Models.ConsumptionEnums.ConsumptionDataType.VentEfficiency:
            //        //    return typeof(VentilationEfficiencyMetricService);
                    default:
                        return null;
                }
            }

        #endregion
    }
}