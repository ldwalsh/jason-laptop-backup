﻿using CW.Website._services.Results.Interfaces;

namespace CW.Website._services.MetricCalculation
{
    public sealed class SingleMetricService : MetricServiceBase
    {
        #region constructor(s)

            public SingleMetricService(IResultsFormatterService resultsFormtterSvc) :
            base(resultsFormtterSvc) { }

        #endregion
    }
}