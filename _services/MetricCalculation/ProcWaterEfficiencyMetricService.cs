﻿using CW.Website._services.Results.Interfaces;

namespace CW.Website._services.MetricCalculation
{
    public sealed class ProcWaterEfficiencyMetricService : MetricServiceBase
    {
        #region properties

            #region overrides

                protected override bool IsDefaultCalc { get { return false; } }

                protected override string Numerator { get { return "ProcessWaterProduced"; } }

                protected override string Denominator { get { return "MakeupWaterUse"; } }

                protected override string ResultName { get { return "Process Water Efficiency"; } }

            #endregion

        #endregion

        #region constructor(s)

            public ProcWaterEfficiencyMetricService(IResultsFormatterService resultsFormtterSvc) :
            base(resultsFormtterSvc) { }

        #endregion   
    }
}