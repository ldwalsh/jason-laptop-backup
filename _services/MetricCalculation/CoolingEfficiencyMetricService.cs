﻿using CW.Website._services.Results.Interfaces;

namespace CW.Website._services.MetricCalculation
{
    public sealed class CoolingEfficiencyMetricService : MetricServiceBase
    {
        #region properties

            #region overrides

                protected override bool IsDefaultCalc { get { return false; } }

                protected override string Numerator { get { return "ElectricUse"; } }

                protected override string Denominator { get { return "CoolingUse"; } }

                protected override string ResultName { get { return "Cooling Efficiency"; } }

            #endregion

        #endregion

        #region constructor(s)

            public CoolingEfficiencyMetricService(IResultsFormatterService resultsFormtterSvc) :
            base(resultsFormtterSvc) { }

        #endregion       
    }
}