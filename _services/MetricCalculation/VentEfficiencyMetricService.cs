﻿using CW.Website._services.Results.Interfaces;

namespace CW.Website._services.MetricCalculation
{
    public class VentEfficiencyMetricService : MetricServiceBase
    {
        #region properties

            #region overrides

                protected override bool IsDefaultCalc { get { return false; } }

                protected override string Numerator { get { return "SupplyFanElectricUse"; } }

                protected override string Denominator { get { return "SupplyAirVolume"; } }

                protected override string ResultName { get { return "Vent Efficiency"; } }

            #endregion

        #endregion

        #region constructor(s)

            public VentEfficiencyMetricService(IResultsFormatterService resultsFormtterSvc) :
            base(resultsFormtterSvc) { }

        #endregion
    }
}