﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSEquipmentManufacturerAdministration.aspx.cs" Inherits="CW.Website.KGSEquipmentManufacturerAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                       	                  	
            	<h1>KGS Equipment Manufacturer Administration</h1>                        
                <div class="richText">The kgs equipment manufacturer administration area is used to view, add, and edit equipment manufacturer.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Equipment Manufacturers"></telerik:RadTab>
                            <telerik:RadTab Text="Add Equipment Manufacturer"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Equipment Manufacturers</h2> 
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p>  
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>          
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridEquipmentManufacturers"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="ManufacturerID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridEquipmentManufacturers_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridEquipmentManufacturers_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridEquipmentManufacturers_Sorting"  
                                             OnSelectedIndexChanged="gridEquipmentManufacturers_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridEquipmentManufacturers_Editing"                                                                                                                                                                                   
                                             OnRowDeleting="gridEquipmentManufacturers_Deleting"
                                             OnDataBound="gridEquipmentManufacturers_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ManufacturerName" HeaderText="Manufacturer">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("ManufacturerName"),50) %></ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Phone">  
                                                    <ItemTemplate><%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerPhone"))) ? "" : Eval("ManufacturerPhone")%></ItemTemplate>                                    
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Email">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("ManufacturerEmail"),30) %></ItemTemplate>
                                                </asp:TemplateField>                                                 
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this equipment manufacturer permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT EQUIPMENT MANUFACTURER DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvEquipmentManufacturer" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid">
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Equipment Manufacturer Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Manufacturer: </strong>" + Eval("ManufacturerName") + "</li>"%>                                                                                                                                               	                                                                	                                                                                                                            
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerPhone"))) ? "" : "<li><strong>Phone: </strong>" + Eval("ManufacturerPhone") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerEmail"))) ? "" : "<li><strong>Email: </strong>" + Eval("ManufacturerEmail") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("ManufacturerDescription") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerAddress"))) ? "" : "<li><strong>Address: </strong>" + Eval("ManufacturerAddress") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerCity"))) ? "" : "<li><strong>City: </strong>" + Eval("ManufacturerCity") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerStateName"))) ? "" : "<li><strong>State: </strong>" + Eval("ManufacturerStateName") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerZip"))) ? "" : "<li><strong>Zip: </strong>" + Eval("ManufacturerZip") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("ManufacturerCountryName"))) ? "" : "<li><strong>Country: </strong>" + Eval("ManufacturerCountryName") + "</li>"%>                                                                                                                                                                                                                                                                   	                                                                	                                                                
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT EQUIPMENT MANUFACTURER PANEL -->                              
                                    <asp:Panel ID="pnlEditEquipmentManufacturer" runat="server" Visible="false" DefaultButton="btnUpdateEquipmentManufacturer">                                                                                             
                                        <div>
                                            <h2>Edit Equipment Manufacturer</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Manufacturer Name:</label>
                                                <asp:TextBox ID="txtEditManufacturerName" MaxLength="50" runat="server"></asp:TextBox>
                                                <asp:HiddenField ID="hdnEditManufacturerName" runat="server" Visible="false" />                                    
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Phone:</label>
                                                <telerik:RadMaskedTextBox ID="txtEditManufacturerPhone" CssClass="textbox" runat="server"
                                                    Mask="(###)###-####" ValidationGroup="EditEquipmentManufacturer"></telerik:RadMaskedTextBox>                                                                                                                                                                
                                            </div>                                                                                                                                                                                           
                                            <div class="divForm">
                                                 <label class="label">Email:</label>
                                                 <asp:TextBox CssClass="textbox" ID="txtEditManufacturerEmail" runat="server" MaxLength="100"></asp:TextBox>
                                            </div>                                                                                                                                                                                                   
                                            <div class="divForm">
                                                 <label class="label">Description:</label>
                                                 <textarea name="txtEditDescription" id="txtEditDescription" cols="40" rows="5" onkeyup="limitChars(this, 1000, 'divEditDescriptionCharInfo')" runat="server"></textarea>
                                                 <div id="divEditDescriptionCharInfo"></div>                                                           
                                            </div>            
                                            <div class="divForm">
                                                <label class="label">Address:</label>
                                                <asp:TextBox ID="txtEditManufacturerAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                                                                              
                                            </div>
                                            <div class="divForm">
                                                <label class="label">City:</label>
                                                <asp:TextBox CssClass="textbox" ID="txtEditManufacturerCity" runat="server" MaxLength="100"></asp:TextBox>
                                            </div>
                                            <div class="divForm">
                                                <label class="label">Country:</label>
                                                <asp:DropDownList CssClass="dropdown" ID="ddlEditManufacturerCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlEditManufacturerCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                <asp:ListItem Value="-1">Select one...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="divForm">
                                                <label class="label">State:</label>
                                                <asp:DropDownList ID="ddlEditManufacturerState" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                <asp:ListItem Value="-1">Select country first...</asp:ListItem>
                                                </asp:DropDownList>                                                                                                                                                              
                                            </div>   
                                            <div class="divForm">
                                                <label class="label">Zip:</label>
                                                <telerik:RadMaskedTextBox ID="txtEditManufacturerZip" CssClass="textbox" runat="server" 
                                                    Mask="aaaaaaaaaa" PromptChar="" ValidationGroup="EditEquipmentManufacturer"></telerik:RadMaskedTextBox>                                                                                                                                                                  
                                            </div>                                                                                           
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentManufacturer" runat="server" Text="Edit Manufacturer"  OnClick="updateEquipmentManufacturerButton_Click" ValidationGroup="EditEquipmentManufacturer"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editManufacturerNameRequiredValidator" runat="server"
                                        ErrorMessage="Manufacturer Name is a required field."
                                        ControlToValidate="txtEditManufacturerName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditEquipmentManufacturer">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editManufacturerNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editManufacturerNameRequiredValidatorExtender"
                                        TargetControlID="editManufacturerNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                     <asp:RegularExpressionValidator ID="editManufacturerPhoneRegExValidator" runat="server"
                                            ErrorMessage="Invalid Phone Format."
                                            ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                            ControlToValidate="txtEditManufacturerPhone"
                                            SetFocusOnError="true" 
                                            Display="None" 
                                            ValidationGroup="EditEquipmentManufacturer">
                                            </asp:RegularExpressionValidator>                         
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editManufacturerPhoneRegExValidatorExtender" runat="server"
                                        BehaviorID="editManufacturerPhoneRegExValidatorExtender" 
                                        TargetControlID="editManufacturerPhoneRegExValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RegularExpressionValidator ID="editEquipmentManufacturerEmailRegExValidator" runat="server"
                                        ErrorMessage="Invalid Email Format."
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="txtEditManufacturerEmail"
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        ValidationGroup="EditEquipmentManufacturer">
                                        </asp:RegularExpressionValidator>                         
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editEquipmentManufacturerEmailRegExValidatorExtender" runat="server"
                                        BehaviorID="editEquipmentManufacturerEmailRegExValidatorExtender" 
                                        TargetControlID="editEquipmentManufacturerEmailRegExValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RegularExpressionValidator ID="editManufacturerZipRegExValidator" runat="server"
                                        ErrorMessage="Invalid Zip Format."
                                        ValidationExpression="\d{5}"
                                        ControlToValidate="txtEditManufacturerZip"
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        ValidationGroup="EditEquipmentManufacturer"
                                        Enabled="false">
                                        </asp:RegularExpressionValidator>  
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editManufacturerZipRegExValidatorExtender" runat="server"
                                        BehaviorID="editManufacturerZipRegExValidatorExtender"
                                        TargetControlID="editManufacturerZipRegExValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>                                                                              
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddEquipmentManufacturer" runat="server" DefaultButton="btnAddEquipmentManufacturer">
                                    <h2>Add Equipment Manufacturer</h2> 
                                    <div>        
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                            
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Manufacturer Name:</label>
                                            <asp:TextBox ID="txtAddManufacturerName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div>
                                        <div class="divForm">
                                            <label class="label">Phone:</label>
                                            <telerik:RadMaskedTextBox ID="txtAddManufacturerPhone" CssClass="textbox" runat="server"
                                                Mask="(###)###-####" ValidationGroup="AddEquipmentManufacturer"></telerik:RadMaskedTextBox>                                                                                                          
                                        </div>                                                                                                                                                                                           
                                        <div class="divForm">
                                             <label class="label">Email:</label>
                                             <asp:TextBox CssClass="textbox" ID="txtAddManufacturerEmail" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>                                                    
                                        <div class="divForm">
                                             <label class="label">Description:</label>
                                             <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 1000, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                             <div id="divAddDescriptionCharInfo"></div>
                                        </div>                                                                                                                                                                                                          
                                        <div class="divForm">
                                            <label class="label">Address:</label>
                                            <asp:TextBox ID="txtAddManufacturerAddress" CssClass="textbox" MaxLength="200" runat="server"></asp:TextBox>                                                                                                        
                                        </div>      
                                        <div class="divForm">
                                            <label class="label">City:</label>
                                            <asp:TextBox CssClass="textbox" ID="txtAddManufacturerCity" runat="server" MaxLength="100"></asp:TextBox>
                                        </div>                                                                                                                                                                                     
                                        <div class="divForm">
                                            <label class="label">Country:</label>
                                            <asp:DropDownList CssClass="dropdown" ID="ddlAddManufacturerCountry" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlAddManufacturerCountry_OnSelectedIndexChanged" AutoPostBack="true" runat="server">
                                            <asp:ListItem Value="-1">Select one...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>                                                                                                                                                                                         
                                        <div class="divForm">
                                            <label class="label">State:</label>
                                            <asp:DropDownList CssClass="dropdown" ID="ddlAddManufacturerState" AppendDataBoundItems="true" runat="server">
                                            <asp:ListItem Value="-1">Select country first...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>        
                                        <div class="divForm">
                                            <label class="label">Zip:</label>
                                            <telerik:RadMaskedTextBox ID="txtAddManufacturerZip" CssClass="textbox" runat="server" 
                                                Mask="aaaaaaaaaa" PromptChar="" ValidationGroup="AddEquipmentManufacturer"></telerik:RadMaskedTextBox>                                                                                                                                                              
                                        </div>                                                                                        
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddEquipmentManufacturer" runat="server" Text="Add Manufacturer"  OnClick="addEquipmentManufacturerButton_Click" ValidationGroup="AddEquipmentManufacturer"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addManufacturerNameRequiredValidator" runat="server"
                                        ErrorMessage="Manufacturer Name is a required field."
                                        ControlToValidate="txtAddManufacturerName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddEquipmentManufacturer">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addManufacturerNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addManufacturerNameRequiredValidatorExtender"
                                        TargetControlID="addManufacturerNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                    <asp:RegularExpressionValidator ID="addManufacturerPhoneRegExValidator" runat="server"
                                        ErrorMessage="Invalid Phone Format."
                                        ValidationExpression="^([\(]{1}\d{3}[\)]{1}\d{3}[\-]{1}\d{4})$"
                                        ControlToValidate="txtAddManufacturerPhone"
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        ValidationGroup="AddEquipmentManufacturer">
                                        </asp:RegularExpressionValidator>                         
                                    <ajaxToolkit:ValidatorCalloutExtender ID="AddManufacturerPhoneRegExValidatorExtender" runat="server"
                                        BehaviorID="addManufacturerPhoneRegExValidatorExtender" 
                                        TargetControlID="addManufacturerPhoneRegExValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RegularExpressionValidator ID="addEquipmentManufacturerEmailRegExValidator" runat="server"
                                        ErrorMessage="Invalid Email Format."
                                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="txtAddManufacturerEmail"
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        ValidationGroup="AddEquipmentManufacturer">
                                        </asp:RegularExpressionValidator>                         
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentManufacturerEmailRegExValidatorExtender" runat="server"
                                        BehaviorID="addEquipmentManufacturerEmailRegExValidatorExtender" 
                                        TargetControlID="addEquipmentManufacturerEmailRegExValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RegularExpressionValidator ID="addManufacturerZipRegExValidator" runat="server"
                                        ErrorMessage="Invalid Zip Format."
                                        ValidationExpression="\d{5}"
                                        ControlToValidate="txtAddManufacturerZip"
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        ValidationGroup="AddEquipmentManufacturer"
                                        Enabled="false">
                                        </asp:RegularExpressionValidator> 
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addManufacturerZipRegExValidatorExtender" runat="server"
                                        BehaviorID="addManufacturerZipRegExValidatorExtender"
                                        TargetControlID="addManufacturerZipRegExValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                </asp:Panel>                                                                                                                                                     
                            </telerik:RadPageView>                         
                        </telerik:RadMultiPage>
                   </div>                                                                 
                
</asp:Content>


                    
                  
