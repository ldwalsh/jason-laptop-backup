﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.DataSourceVendor;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSDataSourceVendorAdministration : SitePage
    {
        #region Properties

        private DataSourceVendor mDataSourceVendor;

        const string addDataSourceVendorSuccess = " data source vendor addition was successful.";
        const string addDataSourceVendorFailed = "Adding data source vendor failed. Please contact an administrator.";
        const string updateSuccessful = "Data source vendor update was successful.";
        const string updateFailed = "Data source vendor update failed. Please contact an administrator.";
        const string dataSourceVendorExists = "Data source vendor with provided name already exists.";
        const string dsExist = "Cannot delete data source vendor because data sources are associated. Please dissassociate data sources first.";
        const string prodExist = "Cannot delete data source vendor because products are associated. Please dissassociate data source products first.";
        const string deleteSuccessful = "Data source vendor deletion was successful.";
        const string deleteFailed = "Data source vendor deletion failed. Please contact an administrator.";
        const string initialSortDirection = "ASC";
        const string initialSortExpression = "VendorName";

        //gets and sets the gridview viewstate sort direction for the dataview
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? string.Empty; }
            set { ViewState["SortDirection"] = value; }
        }

        //gets and sets the gridview viewstate sort expression for the dataview
        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            //logged in security check in master page
            //secondary security check specific to this page.
            //Check kgs super admin or higher.
            if (!siteUser.IsKGSSuperAdminOrHigher)
                Response.Redirect("/Home.aspx");
        
            //hide labels
            lblErrors.Visible = false;
            lblAddError.Visible = false;
            lblEditError.Visible = false;

            //if the page is not a postback, clear the viewstate and set the initial viewstate settings
            if (!Page.IsPostBack)
            {
                BindDataSourceVendors();

                ViewState.Clear();
                GridViewSortDirection = initialSortDirection;
                GridViewSortExpression = initialSortExpression;

                BindCountries(ddlEditVendorCountry, ddlAddVendorCountry);

                sessionState["Search"] = String.Empty;
            }
        }

        #endregion

        #region Set Fields and Data

        protected void SetDataSourceVendorIntoEditForm(DataSourceVendor dataSourceVendor)
        {
            //ID
            hdnEditID.Value = Convert.ToString(dataSourceVendor.VendorID);
            //DataSourceVendor Name
            txtEditVendorName.Text = dataSourceVendor.VendorName;
            //Phone
            txtEditVendorPhone.Text = dataSourceVendor.VendorPhone;
            //ReferenceID
            txtEditVendorEmail.Text = dataSourceVendor.VendorEmail;
            //Description
            txtEditDescription.Value = dataSourceVendor.VendorDescription;
            //Address
            txtEditVendorAddress.Text = dataSourceVendor.VendorAddress;
            //City
            txtEditVendorCity.Text = dataSourceVendor.VendorCity;
            //Country Alpha2Code
            try
            {
                ddlEditVendorCountry.SelectedValue = dataSourceVendor.VendorCountryAlpha2Code;

                ddlEditVendorCountry_OnSelectedIndexChanged(null, null);

                //set zip validators accordingly
                editVendorZipRegExValidator.Enabled = dataSourceVendor.VendorCountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtEditVendorZip.Mask = dataSourceVendor.VendorCountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa"; 

                //StateID
                try
                {
                    ddlEditVendorState.SelectedValue = Convert.ToString(dataSourceVendor.VendorStateID);
                }
                catch
                {
                }
            }
            catch
            {
            }            
            //ZIP
            txtEditVendorZip.Text = dataSourceVendor.VendorZip;
        }

        #endregion

        #region Load and Bind Fields

            private void BindCountries(DropDownList ddl, DropDownList ddl2)
            {
                IEnumerable<Country> countries = DataMgr.CountryDataMapper.GetAllCountries();

                ddl.DataTextField = "CountryName";
                ddl.DataValueField = "Alpha2Code";
                ddl.DataSource = countries;
                ddl.DataBind();

                ddl2.DataTextField = "CountryName";
                ddl2.DataValueField = "Alpha2Code";
                ddl2.DataSource = countries;
                ddl2.DataBind();
            }

            private void BindStates(DropDownList ddl, string alpha2Code)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code);
                ddl.DataBind();
            }

        #endregion

        #region Load Data Source Vendor

        protected void LoadAddFormIntoDataSourceVendor(DataSourceVendor dataSourceVendor)
        {
            //DataSourceVendor Name
            dataSourceVendor.VendorName = txtAddVendorName.Text;
            //Phone
            dataSourceVendor.VendorPhone = String.IsNullOrEmpty(txtAddVendorPhone.Text) ? null : txtAddVendorPhone.Text;
            //Email
            dataSourceVendor.VendorEmail = String.IsNullOrEmpty(txtAddVendorEmail.Text) ? null : txtAddVendorEmail.Text; 
            //Description
            dataSourceVendor.VendorDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value; 
            //Address
            dataSourceVendor.VendorAddress = String.IsNullOrEmpty(txtAddVendorAddress.Text) ? null : txtAddVendorAddress.Text;
            //City
            dataSourceVendor.VendorCity = String.IsNullOrEmpty(txtAddVendorCity.Text) ? null : txtAddVendorCity.Text; 
            //CountryAlpha2Code
            dataSourceVendor.VendorCountryAlpha2Code = ddlAddVendorCountry.SelectedValue != "-1" ? ddlAddVendorCountry.SelectedValue : null;
            //StateID
            dataSourceVendor.VendorStateID = ddlAddVendorState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddVendorState.SelectedValue) : null;
            //ZIP
            dataSourceVendor.VendorZip = String.IsNullOrEmpty(txtAddVendorZip.Text) ? null : txtAddVendorZip.Text;

            dataSourceVendor.DateModified = DateTime.UtcNow;
        }

        protected void LoadEditFormIntoDataSourceVendor(DataSourceVendor dataSourceVendor)
        {
            //ID
            dataSourceVendor.VendorID = Convert.ToInt32(hdnEditID.Value);
            //DataSourceVendor Name
            dataSourceVendor.VendorName = txtEditVendorName.Text;
            //Phone
            dataSourceVendor.VendorPhone = String.IsNullOrEmpty(txtEditVendorPhone.Text) ? null : txtEditVendorPhone.Text;
            //Email
            dataSourceVendor.VendorEmail = String.IsNullOrEmpty(txtEditVendorEmail.Text) ? null : txtEditVendorEmail.Text;
            //Description
            dataSourceVendor.VendorDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
            //Address
            dataSourceVendor.VendorAddress = String.IsNullOrEmpty(txtEditVendorAddress.Text) ? null : txtEditVendorAddress.Text;
            //City
            dataSourceVendor.VendorCity = String.IsNullOrEmpty(txtEditVendorCity.Text) ? null : txtEditVendorCity.Text;
            //CountryAlpha2Code
            dataSourceVendor.VendorCountryAlpha2Code = ddlEditVendorCountry.SelectedValue != "-1" ? ddlEditVendorCountry.SelectedValue : null;
            //StateID
            dataSourceVendor.VendorStateID = ddlEditVendorState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditVendorState.SelectedValue) : null;
            //ZIP
            dataSourceVendor.VendorZip = String.IsNullOrEmpty(txtEditVendorZip.Text) ? null : txtEditVendorZip.Text;
        }

        #endregion

        #region Dropdown Events

            protected void ddlAddVendorCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlAddVendorState, ddlAddVendorCountry.SelectedValue);

                addVendorZipRegExValidator.Enabled = ddlAddVendorCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtAddVendorZip.Mask = ddlAddVendorCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            }
            protected void ddlEditVendorCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlEditVendorState, ddlEditVendorCountry.SelectedValue);

                editVendorZipRegExValidator.Enabled = ddlEditVendorCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtEditVendorZip.Mask = ddlEditVendorCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
            }

        #endregion

        #region Button Events

        /// <summary>
        /// Add data source vendor Button on click.
        /// </summary>
        protected void addDataSourceVendorButton_Click(object sender, EventArgs e)
        {
            mDataSourceVendor = new DataSourceVendor();

            //load the form into the datasource vendor
            LoadAddFormIntoDataSourceVendor(mDataSourceVendor);

            //check if data source vendor exists
            if (DataMgr.DataSourceVendorDataMapper.DoesDataSourceVendorExist(mDataSourceVendor.VendorName))
            {
                LabelHelper.SetLabelMessage(lblAddError, dataSourceVendorExists, lnkSetFocusAdd);
            }
            else
            {
                try
                {
                    //insert new data source vendor
                    DataMgr.DataSourceVendorDataMapper.InsertVendor(mDataSourceVendor);
                    LabelHelper.SetLabelMessage(lblAddError, mDataSourceVendor.VendorName + addDataSourceVendorSuccess, lnkSetFocusAdd);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding data source vendor.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addDataSourceVendorFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding data source vendor.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addDataSourceVendorFailed, lnkSetFocusAdd);
                }
            }
        }

        /// <summary>
        /// Update Data source vendor Button on click.
        /// </summary>
        protected void updateDataSourceVendorButton_Click(object sender, EventArgs e)
        {
            //create and load the form into the data source vendor
            DataSourceVendor mDataSourceVendor = new DataSourceVendor();
            LoadEditFormIntoDataSourceVendor(mDataSourceVendor);

            //try to update the data source vendor           
            try
            {
                DataMgr.DataSourceVendorDataMapper.UpdateDataSourceVendor(mDataSourceVendor);

                LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);
                
                BindDataSourceVendors();
            }
            catch (Exception ex)
            {
                LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating data source vendor.", ex);
            }
        }

        /// <summary>
        /// Search button on click, searches by all individual words entered and intersects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void searchButton_Click(object sender, EventArgs e)
        {
            sessionState["Search"] = txtSearch.Text;
            BindDataSourceVendors();
        }

        /// <summary>
        /// Rebinds grid to view all.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void viewAll_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            sessionState["Search"] = String.Empty;
            BindDataSourceVendors();
        }

        #endregion

        #region Tab Events

        /// <summary>
        /// Tab changed event, bind buildings grid after tab changed back from add buidling tab
        /// </summary>
        protected void onTabClick(object sender, EventArgs e)
        {
            if (radTabStrip.MultiPage.SelectedIndex == 0)
            {
                BindDataSourceVendors();
            }
        }

        #endregion

        #region Grid Events

        protected void gridDataSourceVendors_OnDataBound(object sender, EventArgs e)
        {
            //check if kgs admin or higher, hide edit and delete column if not
            //bool kgsAdmin = RoleHelper.IsKGSAdminOrHigher(Session["UserRoleID"].ToString());
            //gridDataSourceVendors.Columns[1].Visible = kgsAdmin;
            //gridDataSourceVendors.Columns[4].Visible = kgsAdmin;
        }

        protected void gridDataSourceVendors_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            pnlEditDataSourceVendor.Visible = false;
            dtvDataSourceVendor.Visible = true;
            
            int dataSourceVendorID = Convert.ToInt32(gridDataSourceVendors.DataKeys[gridDataSourceVendors.SelectedIndex].Values["VendorID"]);

            //set data source vendor
            dtvDataSourceVendor.DataSource = DataMgr.DataSourceVendorDataMapper.GetFullDataSourceVendorByID(dataSourceVendorID);
            //bind data source vendor to details view
            dtvDataSourceVendor.DataBind();
        }

        protected void gridDataSourceVendors_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                //Note: The autogenerated pager table is not shown with just one page.

                Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                tblPager.CssClass = "pageTable";

                TableRow theRow = tblPager.Rows[0];

                LinkButton ctrlPrevious = new LinkButton();
                ctrlPrevious.CommandArgument = "Prev";
                ctrlPrevious.CommandName = "Page";
                ctrlPrevious.Text = "« Previous Page";
                ctrlPrevious.CssClass = "pageLink";

                TableCell cellPreviousPage = new TableCell();
                cellPreviousPage.Controls.Add(ctrlPrevious);
                tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                LinkButton ctrlNext = new LinkButton();
                ctrlNext.CommandArgument = "Next";
                ctrlNext.CommandName = "Page";
                ctrlNext.Text = "Next Page »";
                ctrlNext.CssClass = "pageLink";


                TableCell cellNextPage = new TableCell();
                cellNextPage.Controls.Add(ctrlNext);
                tblPager.Rows[0].Cells.Add(cellNextPage);
            }
        }

        protected void gridDataSourceVendors_Editing(object sender, GridViewEditEventArgs e)
        {
            dtvDataSourceVendor.Visible = false;
            pnlEditDataSourceVendor.Visible = true;

            int dataSourceVendorID = Convert.ToInt32(gridDataSourceVendors.DataKeys[e.NewEditIndex].Values["VendorID"]);

            DataSourceVendor mDataSourceVendor = DataMgr.DataSourceVendorDataMapper.GetVendor(dataSourceVendorID);

            //Set data source vendor data
            SetDataSourceVendorIntoEditForm(mDataSourceVendor);

            //Cancels the edit auto grid selected.
            e.Cancel = true;
        }

         protected void gridDataSourceVendors_Deleting(object sender, GridViewDeleteEventArgs e)
         {
             //get deleting rows datasource vendor id
             int dataSourceVendorID = Convert.ToInt32(gridDataSourceVendors.DataKeys[e.RowIndex].Value);
             
             try
             {
                 //Only allow deletion of a data source vendor if no data sources vendor 
                 //products have been associated

                 //check if data source vendor products are assigned to that datasource vendor       
                 if (DataMgr.DataSourceVendorDataMapper.AreVendorProductsAssignedToDataSourceVendor(dataSourceVendorID))
                 {
                     lblErrors.Text = prodExist;
                     lblErrors.Visible = true;
                     lnkSetFocusView.Focus();
                 }
                 else
                 {

                     DataMgr.DataSourceVendorDataMapper.DeleteDataSourceVendor(dataSourceVendorID);

                     lblErrors.Text = deleteSuccessful;
                     lblErrors.Visible = true;
                     lnkSetFocusView.Focus();

                     pnlEditDataSourceVendor.Visible = false;
                     dtvDataSourceVendor.Visible = false;
                 }
             }
             catch (Exception ex)
             {
                 lblErrors.Text = deleteFailed;
                 lblErrors.Visible = true;
                 lnkSetFocusView.Focus();

                 LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting data source vendor.", ex);
             }
             
             //Bind data again keeping current page and sort mode
             DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryDataSourceVendors());
             gridDataSourceVendors.PageIndex = gridDataSourceVendors.PageIndex;
             gridDataSourceVendors.DataSource = SortDataTable(dataTable as DataTable, true);
             gridDataSourceVendors.DataBind();

             SetGridCountLabel(dataTable.Rows.Count);

             //hide edit form if shown
             pnlEditDataSourceVendor.Visible = false;  
         }

        protected void gridDataSourceVendors_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedDataSourceVendors(sessionState["Search"].ToString()));

            //maintain current sort direction and expresstion on paging
            gridDataSourceVendors.DataSource = SortDataTable(dataTable, true);
            gridDataSourceVendors.PageIndex = e.NewPageIndex;
            gridDataSourceVendors.DataBind();
        }

        protected void gridDataSourceVendors_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Reset the edit index.
            gridDataSourceVendors.EditIndex = -1;

            DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedDataSourceVendors(sessionState["Search"].ToString()));

            GridViewSortExpression = e.SortExpression;

            gridDataSourceVendors.DataSource = SortDataTable(dataTable, false);
            gridDataSourceVendors.DataBind();
        }

        #endregion

        #region Helper Methods

        private IEnumerable<GetDataSourceVendorsData> QueryDataSourceVendors()
        {
            try
            {
                //get all data sources vendors
                return DataMgr.DataSourceVendorDataMapper.GetAllDataSourceVendorsWithPartialData();
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendors.", sqlEx);
                return null;
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendors.", ex);
                return null;
            }
        }

        private IEnumerable<GetDataSourceVendorsData> QuerySearchedDataSourceVendors(string searchText)
        {
            try
            {
                //get all data sources vendors
                return DataMgr.DataSourceVendorDataMapper.GetAllSearchedDataSourceVendorsWithPartialData(searchText);
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendors.", sqlEx);
                return null;
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving data source vendors.", ex);
                return null;
            }
        }

        private void BindDataSourceVendors()
        {
            //query data source vendors
            IEnumerable<GetDataSourceVendorsData> dataSourceVendors = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryDataSourceVendors() : QuerySearchedDataSourceVendors(txtSearch.Text);

            int count = dataSourceVendors.Count();

            gridDataSourceVendors.DataSource = dataSourceVendors;

            // bind grid
            gridDataSourceVendors.DataBind();

            SetGridCountLabel(count);

            //set search visibility. dont remove when last result from searched results in the grid is deleted.
            pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && count == 0) ? false : true;
        }

        //private void BindSearchedDataSourceVendors(string[] searchText)
        //{
        //    query data source vendors
        //    IEnumerable<GetDataSourceVendorsData> dataSourceVendors = QuerySearchedDataSourceVendors(searchText);

        //    int count = dataSourceVendors.Count();

        //    gridDataSourceVendors.DataSource = dataSourceVendors;

        //     bind grid
        //    gridDataSourceVendors.DataBind();

        //    SetGridCountLabel(count);
        //}

        private void SetGridCountLabel(int count)
        {
            //check if grid empty 
            if (count == 1)
            {
                lblResults.Text = String.Format("{0} data source vendor found.", count);
            }
            else if (count > 1)
            {
                lblResults.Text = String.Format("{0} data source vendors found.", count);
            }
            else
            {
                lblResults.Text = "No data source vendors found.";
            }
        }

        /// <summary>
        /// Helper method to switch sort direction
        /// </summary>
        /// <returns>Alternated Sort Direction</returns>
        private string GetSortDirection()
        {
            //switch sorting directions
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    ViewState["SortDirection"] = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    ViewState["SortDirection"] = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        /// <summary>
        /// Sorts the data table based on view state.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="isPageIndexChanging"></param>
        /// <returns>DataView</returns>
        protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
        {
            if (dataTable != null && dataTable.Rows.Count != 0)
            {
                DataView dataView = new DataView(dataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        //maintains the currect viewstate sorting for paging
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        //reverses the sort direction on sort
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                    }
                }
                return dataView;
            }
            else
            {
                return new DataView();
            }
        } 

        #endregion
    }
}

