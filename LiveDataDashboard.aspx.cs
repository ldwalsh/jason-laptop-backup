﻿using System;
using System.Configuration;
using System.IO;
using System.Web.UI;
using CW.Business;
using CW.Data;
using CW.Website.Dashboard;
using CW.Website._framework;
using System.Xml;
using System.Windows.Threading;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;

using System.Drawing;
using System.Web;


using Microsoft.WindowsAzure;


using CW.Data.AzureStorage;
using CW.Data.AzureStorage.DataContexts.Blob;
using CW.Data.AzureStorage.Helpers;
using CW.Data.AzureStorage.Models.Blob;
using CW.Data.AzureStorage.Models;
using CW.Utility;
using CW.Data.Models.Client;
using CW.Data.Models.Point;
using CW.Data.Models.Building;
using CW.Data.Models.Raw;
using CW.Website._controls;

using Telerik.Web.UI;
using Telerik.Charting;
using CW.Common.Constants;
using System.Linq.Expressions;

namespace CW.Website
{
    public partial class LiveDataDashboard: SitePage
    {
        #region Properties

            private int uid;
            private int cid;
            const string noClientImage = "no-client-image.png";
            private int roleID;
            private bool isKGSFullAdminOrHigher, isSuperAdminOrFullAdminOrFullUser;
            //private string siteAddress;

            //public string airHandlerTempChartString = "";

            //building
            //public string serializedBuildings;
            //public byte[] buildingImage = null;
            //public string buildingImageUrl = "", buildingImageAlternateText = "";
            const string noBuildingImage = "no-building-image.png";

            //const string successfulPlots = "Data points were successfully plotted.";
            //const string emptyPlots = "One or more data points could not be plotted because no data exists.";
            const string noPlots = "No data has been plotted.";
            const string failedPlots = "Data points failed to plot. Please contact an administrator.";
            const string defaultPlots = "Please select one or more points to be plotted or view the pre plots below.";

        #endregion

        #region Page Events

            protected override void OnLoad(EventArgs e)
            {
                //datamanger in sitepage

                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                    //set admin global settings
                    //Make sure to decode from ascii to html 
                    GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
                    litDashboardBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.LiveDataDashboardBody);

                    //get and bind buildings
                    BindBuildings(ddlBuilding, siteUser.VisibleBuildings);

                    //disable nav buttons
                    btnLive.Enabled = false;

                    SetClientInfoAndImages();

                    lblResults.Text = defaultPlots;
                }   

                //set global info
                uid = siteUser.UID;
                roleID = siteUser.RoleID;
                cid = siteUser.CID;
                //siteAddress = ConfigurationManager.AppSettings["SiteAddress"];            
                //serializedBuildings = GetParam("Buildings");
                isKGSFullAdminOrHigher = siteUser.IsKGSFullAdminOrHigher;
                isSuperAdminOrFullAdminOrFullUser = siteUser.IsSuperAdminOrFullAdminOrFullUser;
            }

            protected void Page_Init()
            {
            }

        #endregion

        #region Load and Bind Fields

            /// <summary>
            /// Bind a dropdownlist and scroller will all visible buildings
            /// </summary>
            /// <param name="ddl"></param>
            /// <param name="buildings"></param>
            private void BindBuildings(DropDownList ddl, IEnumerable<Building> buildings)
            {
                ddl.DataTextField = "BuildingName";
                ddl.DataValueField = "BID";
                ddl.DataSource = buildings;
                ddl.DataBind();

                //bind building image scroller
                //BindBuildingScroller(buildings);
            }

            /// <summary>
            /// Binds a dropdownlist with all equipment classes for the building
            /// </summary>
            /// <param name="ddl"></param>
            private void BindEquipmentClasses(DropDownList ddl, IEnumerable<EquipmentClass> equipmentClasses)
            {
                //TODO:  this.client.GetEquipmentClassInBuildingDataAsync(buildingID, Convert.ToInt32(selected.Tag));

                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "EquipmentClassName";
                ddl.DataValueField = "EquipmentClassID";
                ddl.DataSource = equipmentClasses;
                ddl.DataBind();
            }

            /// <summary>
            /// Binds a dropdownlist with all equipment
            /// </summary>
            /// <param name="ddl"></param>
            private void BindEquipment(DropDownList ddl, int bid, int equipmentClassID)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "EquipmentName";
                ddl.DataValueField = "EID";
                ddl.DataSource = DataMgr.EquipmentDataMapper.GetAllActiveEquipmentByBuildingIDAndEquipmentClassID(bid, equipmentClassID);
                ddl.DataBind();
            }

            private void BindPoints(ListBox lb, int eid)
            {
                //clear points
                lb.Items.Clear();
                lb.DataTextField = "PointName";
                lb.DataValueField = "PID";

                //get all visible points, regardless of its active state because we stll need to plot if it was once active.
                var points = DataMgr.PointDataMapper.GetAllVisiblePointsByEID(eid, new Expression<Func<CW.Data.Point, Object>>[] { (p => p.PointType) });

                lb.DataSource = points;
                lb.DataBind();

                //populate point name, and type in listbox items by added a title attribute
                var counter = 0;
                foreach (var p in points)
                {
                    lb.Items[counter].Attributes.Add("title", String.Format("{0}, {1}", p.PointName, p.PointType.PointTypeName));
                    counter++;
                }

                ControlHelper.IncrementDuplicateTextValues(lb.Items);
            }

        #endregion

        #region Set Fields and Data

            private void SetClientInfoAndImages()
            {
                var client = DataMgr.ClientDataMapper.GetClient(siteUser.CID);

                if (client == null){ return; }

                string clientName = siteUser.ClientName;

                imgHeaderClientImage.AlternateText = clientName;
                imgHeaderClientImage.Visible = true;
                imgHeaderClientImage.ImageUrl = HandlerHelper.ClientImageUrl(siteUser.CID, client.ImageExtension);

                //// Only assign a blob lookup if image extension exists in relational database
                //// If lookup is not set, control will display the default image and save on transactions ($$$)
                //if (!String.IsNullOrEmpty(client.ImageExtension))
                //{
                //    var blob = mDataManager.ClientDataMapper.GetClientImage(client.CID, client.ImageExtension);

                //    radHeaderClientImage.DataValue = blob != null ? blob.Image : null;
                //}

                //// Before doing a blob lookup, check if image extension exists in relational database
                //// Save on transactions ($$$)
                //if (String.IsNullOrEmpty(client.First().ImageExtension))
                //{

                //    //imgHeaderClientImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;
                //    //imgHeaderClientImage.AlternateText = clientName;

                //    //radHeaderClientImage.ImageLoadFunction = null;

                //    ////radHeaderClientImage.DataValue = null;
                //    //radHeaderClientImage.Visible = false;
                //}
                //else
                //{
                //    //ClientBlob clientBlob = mDataManager.ClientDataMapper.GetClientImage(client.First().CID, client.First().ImageExtension);
 
                //    //if (clientBlob == null)
                //    //{
                //    //    imgHeaderClientImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noClientImage;
                //    //    imgHeaderClientImage.AlternateText = clientName;

                //    //    radHeaderClientImage.Action = null;
                //    //    //radHeaderClientImage.DataValue = null;
                //    //    radHeaderClientImage.Visible = false;
                //    //}
                //    //else
                //    //{
                //        //radHeaderClientImage.Action = null;
                //        ////radHeaderClientImage.DataValue = clientBlob.Image;

                //        //imgHeaderClientImage.ImageUrl = "";
                //        //imgHeaderClientImage.Visible = false;
                //    //}
                //}

                lblHeaderClientName.InnerText = StringHelper.TrimText(clientName, 22);
            }

            private void SetBuildingInfoAndImages(GetBuildingsData building)
            {
                string buildingName = building.BuildingName;

                imgHeaderBuildingImage.AlternateText = buildingName;
                imgHeaderBuildingImage.Visible = true;

				imgHeaderBuildingImage.ImageUrl = HandlerHelper.BuildingImageUrl(building.CID, building.BID, building.ImageExtension);

                //// if extension is not found, set default image
                //if (String.IsNullOrEmpty(building.ImageExtension))
                //{
                //    imgHeaderClientImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage;
                //}   

                //// Before doing a blob lookup, check if image extension exists in relational database
                //// Save on transactions ($$$)
                //if (String.IsNullOrEmpty(building.ImageExtension))
                //{
                //    ////keep visible propetry, as an empty imageurl trys to get the image from a default location. building image only as not loaded initially.
                //    //imgHeaderBuildingImage.Visible = true;
                //    //imgHeaderBuildingImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage;
                //    //imgHeaderBuildingImage.AlternateText = buildingName;

                //    //radHeaderBuildingImage.DataValue = null;
                //    //radHeaderBuildingImage.Visible = false;
                //}
                //else
                //{
                //    // Depreciated by BuildingDataMapper functions
                //    //BuildingBlobDataContext blobContext = new BuildingBlobDataContext(mAccount, bid);
                //    //BuildingBlob buildingBlob = blobContext.GetBlob(BlobHelper.GenerateBuildingProfileImageBlobName(bid, building.First().ImageExtension));

                //    //BuildingBlob buildingBlob = mDataManager.BuildingDataMapper.GetBuildingImage(building.BID, building.ImageExtension);

                //    //if (buildingBlob == null)
                //    //{
                //    //    imgHeaderBuildingImage.Visible = true;
                //    //    imgHeaderBuildingImage.ImageUrl = ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage;
                //    //    imgHeaderBuildingImage.AlternateText = buildingName;

                //    //    radHeaderBuildingImage.DataValue = null;
                //    //    radHeaderBuildingImage.Visible = false;
                //    //}
                //    //else
                //    //{
                //        //radHeaderBuildingImage.ImageLoadFunction = action;
                //        ////radHeaderBuildingImage.DataValue = buildingBlob.Image;

                //        //imgHeaderBuildingImage.ImageUrl = "";
                //        //imgHeaderBuildingImage.Visible = true;
                //    //    imgHeaderBuildingImage.Visible = false;
                //    //}
                //}                

                lblHeaderBuildingName.InnerText = StringHelper.TrimText(buildingName, 22);
            }

        #endregion

        #region Dropdown Events

            protected void ddlBuilding_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                //precautionary, even thought we remove the "select one..."
                if (ddlBuilding.SelectedValue != "-1")
                {
                    int index = 1;

                    //foreach (RadRotatorItem rri in rrBuildingScroller.Items)
                    //{
                    //    if (((HiddenField)rri.Controls[1]).Value == ddlBuilding.SelectedValue)
                    //    {
                    //        index = rri.Index;
                    //    }
                    //}

                    //bind based on building select
                    BindOnBuildingSelect(Convert.ToInt32(ddlBuilding.SelectedValue), index);

                    liveButton_Click(null, null);
                }
                else
                {
                    //disable nav buttons
                    btnLive.Enabled = false;
                }
            }


            /// <summary>
            /// ddlLiveEquipmentClass_OnSelectedIndexChanged, binds equipment dropdown by selected class
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlLiveEquipmentClass_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlLiveEquipmentClass.SelectedValue != "-1")
                {
                    BindEquipment(ddlLiveEquipment, Convert.ToInt32(ddlBuilding.SelectedValue), Convert.ToInt32(ddlLiveEquipmentClass.SelectedValue));
                }

                //clear points
                lbLivePoints.Items.Clear();
            }

            /// <summary>
            /// ddlLiveEquipment_OnSelectedIndexChanged, binds points lb by eid
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void ddlLiveEquipment_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlLiveEquipment.SelectedValue != "-1")
                {
                    BindPoints(lbLivePoints, Convert.ToInt32(ddlLiveEquipment.SelectedValue));

                    //clear hdn live points list, which retains which values points are alredy plotted
                    hdnLivePointsList.Value = String.Empty;
                }
            }

        #endregion

        #region ListBox Events

            protected void lbLivePoints_OnSelectedIndexChanged(object sender, EventArgs e)
            {          
                int counter = 1;
                //int cid = (siteUser.IsKGSFullAdminOrHigher ? reportCriteria.clientEntity.ID : siteUser.CID);

                if (String.IsNullOrEmpty(hdnLivePointsList.Value))
                {
                    //clear chart
                    rawChart.Series.Clear();
                    //radChart.Series.Clear();
                }

                //get current ploted points set
                List<int> currentPlotedPoints = String.IsNullOrEmpty(hdnLivePointsList.Value) ? null : (hdnLivePointsList.Value.Split(',')).Select(x => int.Parse(x)).ToList();

                //get current ploted points names set
                //List<string> currentPlotedPointNames = (hdnLivePointNamesList.Value.Split(',')).ToList();

                //clear hdn points 
                hdnLivePointsList.Value = String.Empty;

                //clear hdn points series
                //hdnLivePointNamesList.Value = String.Empty;

                //Used to be a foreach ListItem in lbPoints.Items with a query for each
                //Now it's 1 query using linq to sql where contains that translates to a sql query using in
                var newPlotPointPIDs = new List<int>();
                var removePlotPointPIDs = new List<int>();

                foreach (ListItem point in lbLivePoints.Items)
                {
                    int pid = Convert.ToInt32(point.Value);

                    if (point.Selected == true)
                    {
                        if (currentPlotedPoints == null || (currentPlotedPoints != null && !(currentPlotedPoints.Contains(pid))))
                        {
                            newPlotPointPIDs.Add(pid);
                        }

                        //add point to hdn point list
                        hdnLivePointsList.Value += counter == 1 ? pid.ToString() : "," + pid.ToString();

                        counter++;
                    }
                    else
                    {
                        if (currentPlotedPoints != null && currentPlotedPoints.Contains(pid))
                        {
                            removePlotPointPIDs.Add(pid);
                        }
                    }
                }

                //Plot point and add series
                //PlotAdvancedRawDataPoints(newPlotPointPIDs, currentPlotedPoints);
                PlotSimpleRawDataPoints(newPlotPointPIDs, currentPlotedPoints);

                //Remove series
                //RemoveAdvancedRawDataPointPlots(removePlotPointPIDs);
                RemoveSimpleRawDataPointPlots(removePlotPointPIDs);

                SetChartVisiblity();
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// On Init for Tabs
            /// </summary>
            protected void loadTabs(object sender, EventArgs e)
            {
            }

            /// <summary>
            /// Tab changed event, bind content after tab changed back from other tabs
            /// </summary>
            protected void onActiveTabChanged(object sender, EventArgs e)
            {
            }

        #endregion

        #region Main Nav Button Click Events

            /// <summary>
        /// Home view button click
        /// </summary>
        protected void homeButton_Click(object sender, EventArgs e)
        {
            mvMain.ActiveViewIndex = 0;
            mvSecondary.ActiveViewIndex = 0;

            SetActiveNavButton(btnHome);
        }

        /// <summary>
        /// Live view button click
        /// </summary>
        protected void liveButton_Click(object sender, EventArgs e)
        {
            mvMain.ActiveViewIndex = 1;
            mvSecondary.ActiveViewIndex = 1;

            SetActiveNavButton(btnLive);

            //TODO: maybe user service client side
            //dashboardService.GetAllEquipmentClassData();
        }


        #endregion

        #region Plot Button Click Events
        

        #endregion

        #region Binding Methods

            private void BindBuildingScroller(IEnumerable<Building> buildings)
            {
                if (buildings.Any())
                {
                    List<BuildingImage> buildingImages = new List<BuildingImage>();

                    foreach (Building b in buildings)
                    {
                        BuildingImage img = new BuildingImage();

                        //// Before doing a blob lookup, check if image extension exists in relational database
                        //// Save on transactions ($$$)
                        //if (String.IsNullOrEmpty(b.ImageExtension))
                        //{
                        //    img.src = ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage;
                        //    //img.image = null;
                        //    img.radVisible = false;
                        //    img.imgVisible = true;
                        //}
                        //else
                        //{
                        //    // Depreciated by BuildingDataMapper functions
                        //    //BuildingBlobDataContext blobContext = new BuildingBlobDataContext(mAccount, b.BID);
                        //    //BuildingBlob buildingBlob = blobContext.GetBlob(BlobHelper.GenerateBuildingProfileImageBlobName(b.BID, b.ImageExtension));

                        //    //BuildingBlob buildingBlob = mDataManager.BuildingDataMapper.GetBuildingImage(b.BID, b.ImageExtension);

                        //    if (buildingBlob == null)
                        //    {
                        //        img.src = ConfigurationManager.AppSettings["ImageAssetPath"] + noBuildingImage;
                        //        //img.image = null;
                        //        img.radVisible = false;
                        //        img.imgVisible = true;
                        //    }
                        //    else
                        //    {
                        //        //img.image = buildingBlob.Image;
                        //        img.src = "";
                        //        img.radVisible = true;
                        //        img.imgVisible = false;
                        //    }
                        //}

						img.src = HandlerHelper.BuildingImageUrl(b.CID, b.BID, b.ImageExtension);
                        img.visible = true;
                        img.alt = b.BuildingName;
                        img.bid = b.BID;

                        buildingImages.Add(img);
                    }

                    //if (buildings.Count() == 1)
                    //{
                    //    //rrBuildingScroller.FindControl(rrBuildingScroller.ControlButtons.LeftButtonID).Visible = false;
                    //    //rrBuildingScroller.FindControl(rrBuildingScroller.ControlButtons.RightButtonID).Visible = false;
                    //}

                    rrBuildingScroller.DataSource = buildingImages;
                    rrBuildingScroller.DataBind();

                    rrBuildingScroller.InitialItemIndex = siteUser.VisibleBuildings.Count() > 1 ? Convert.ToInt32(Math.Ceiling((double)(siteUser.VisibleBuildings.Count() / 2))) - 1 : 0;
                }
                else
                {
                    divSelectedBuilding.InnerText = "No buildings available.";
                }
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="bid"></param>
            private void BindOnBuildingSelect(int bid, int scrollerIndex)
            {
                //enable nav buttons
                btnHome.Enabled = true;
                btnLive.Enabled = true;

                //try remove item in dropdown "select one" if exists
                ddlBuilding.Items.Remove(ddlBuilding.Items.FindByValue("-1"));

                divSelectedBuilding.InnerText = ddlBuilding.SelectedItem.Text;

                //set buidling scroller building item
                //rrBuildingScroller.InitialItemIndex = scrollerIndex;

                //get building
                GetBuildingsData building = DataMgr.BuildingDataMapper.GetFullBuildingByBID(bid).First();

                //set buildinginfo and images
                SetBuildingInfoAndImages(building);

                //bind equipment classes
                IEnumerable<EquipmentClass> equipmentClasses = DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(Convert.ToInt32(ddlBuilding.SelectedValue));
                BindEquipmentClasses(ddlLiveEquipmentClass, equipmentClasses);

                //clear equipment
                ListItem lItem = new ListItem();
                lItem.Text = "Select equipment class first...";
                lItem.Value = "-1";
                lItem.Selected = true;
                ddlLiveEquipment.Items.Clear();
                ddlLiveEquipment.Items.Add(lItem);

                //clear points
                lbLivePoints.Items.Clear();

                //bind widgets
                BindWidgets(building, equipmentClasses);
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="bid"></param>
            /// <param name="equipmentClassID"></param>
            private void BindOnEquipmentClassSelect(int bid, int equipmentClassID)
            {
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="bid"></param>
            /// <param name="eid"></param>
            private void BindOnEquipmentSelect(int bid, int eid)
            {
            }


            /// <summary>
            /// Bind all widgets for past 12 hours.
            /// </summary>
            private void BindWidgets(GetBuildingsData building, IEnumerable<EquipmentClass> equipmentClasses)
            {
                //current day times
                DateTime twelveHoursAgoUTCDateTime = DateTimeHelper.GetXHoursAgo(DateTime.UtcNow, 12);//DateTimeHelper.GetStartOfCurrentUTCDate(tzi);
                DateTime currentUTCDateTime = DateTime.UtcNow; //DateTimeHelper.GetStartOfTomorrowsCurrentDate(tzi);

                ResetTabAndWidgetVisibility();

                //bind equipment class tabs
                foreach (EquipmentClass ec in equipmentClasses)
                {
                    List<int> pointTypeIDs = null;
                    List<GetPointsData> points = null;
                    IEnumerable<GetRawPlotData> plots = null;
                    List<int> pids = null;

                    switch (ec.EquipmentClassID)
                    {
                        case BusinessConstants.EquipmentClass.AirHandlerEquipmentClassID:

                            //AIR HANDLER TEMP CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.OutdoorAirTempPointTypeID, BusinessConstants.PointType.SupplyAirTempPointTypeID };

                            //set air handler temp chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.AirHandlerEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartAirHandlerTemperatures.Series.Clear();

                                //Google chart attempt--------
                                //airHandlerTempChartString = "google.visualization.arrayToDataTable([['PointName', 'DateTime', 'Temperature'],";

                                //foreach (GetRawPlotData plot in plots)
                                //{
                                //    airHandlerTempChartString += String.Format("['{0}', {1}, {2}],", plot.PointName, plot.DateLoggedLocal, plot.ConvertedRawValue);
                                //}

                                //airHandlerTempChartString += "])";

                                //set axis label
                                chartAirHandlerTemperatures.ChartAreas["airHandlerTemperaturesChartArea"].AxisY.Title = "Temperature (" + plots.First().BuildingSettingBasedEngUnits + ")";
                                   
                                //---------
                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartAirHandlerTemperatures.ChartAreas["airHandlerTemperaturesChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartAirHandlerTemperatures.ChartAreas["airHandlerTemperaturesChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartAirHandlerTemperatures.ChartAreas["airHandlerTemperaturesChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartAirHandlerTemperatures.ChartAreas["airHandlerTemperaturesChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartAirHandlerTemperatures.Series.Add(series);
                                }

                                tabStrip.Visible = widgetAirHandlerTemperatures.Visible = true;
                                //tabPnlAirHandlers.Attributes.Add("visibility", "visible");
                                //tabPnlAirHandlers.Enabled = true;
                                tabStrip.Tabs[0].Visible = true;
                                //tabPnlAirHandlers.CssClass = "";
                            }


                            //AIR HANDLER FAN STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.SupplyFanStatusPointTypeID };

                            //set air handler fan status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.AirHandlerEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartAirHandlerSupplyFanStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartAirHandlerSupplyFanStatus.ChartAreas["airHandlerSupplyFanStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartAirHandlerSupplyFanStatus.ChartAreas["airHandlerSupplyFanStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartAirHandlerSupplyFanStatus.ChartAreas["airHandlerSupplyFanStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartAirHandlerSupplyFanStatus.ChartAreas["airHandlerSupplyFanStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartAirHandlerSupplyFanStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetAirHandlerSupplyFanStatus.Visible = true;
                                //tabPnlAirHandlers.Attributes.Add("visibility", "visible");
                                //tabPnlAirHandlers.Enabled = true;
                                tabStrip.Tabs[0].Visible = true;
                                //tabPnlAirHandlers.CssClass = "";
                            }

                            break;
                        case BusinessConstants.EquipmentClass.BoilerEquipmentClassID:

                            //BOILER HOT WATER STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.BoilerHWStatusPointTypeID };

                            //set boilers hw status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.BoilerEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartBoilerHWStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartBoilerHWStatus.ChartAreas["boilerHWStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartBoilerHWStatus.ChartAreas["boilerHWStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartBoilerHWStatus.ChartAreas["boilerHWStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartBoilerHWStatus.ChartAreas["boilerHWStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartBoilerHWStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetBoilerHWStatus.Visible = true;
                                //tabPnlBoilers.Attributes.Add("visibility", "visible");
                                //tabPnlBoilers.Enabled = true;
                                tabStrip.Tabs[1].Visible = true;
                                //tabPnlBoilers.CssClass = "";
                            }

                            break;
                        case BusinessConstants.EquipmentClass.ChillerEquipmentClassID:

                            //CHILLER STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.ChillerStatusPointTypeID };

                            //set chiller status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.ChillerEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartChillerStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartChillerStatus.ChartAreas["chillerStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartChillerStatus.ChartAreas["chillerStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartChillerStatus.ChartAreas["chillerStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartChillerStatus.ChartAreas["chillerStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartChillerStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetChillerStatus.Visible = true;
                                //tabPnlChillers.Attributes.Add("visibility", "visible");
                                //tabPnlChillers.Enabled = true;
                                tabStrip.Tabs[2].Visible = true;
                                //tabPnlChillers.CssClass = "";
                            }

                            //CHILLER POWER CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.ChillerPowerPointTypeID };

                            //set chiller power chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.ChillerEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartChillerPower.Series.Clear();

                                //set axis label
                                chartChillerPower.ChartAreas["chillerPowerChartArea"].AxisY.Title = "Power (" + plots.First().BuildingSettingBasedEngUnits + ")";

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartChillerPower.ChartAreas["chillerPowerChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartChillerPower.ChartAreas["chillerPowerChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartChillerPower.ChartAreas["chillerPowerChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartChillerPower.ChartAreas["chillerPowerChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartChillerPower.Series.Add(series);
                                }

                                tabStrip.Visible = widgetChillerPower.Visible = true;
                                //tabPnlChillers.Attributes.Add("visibility", "visible");
                                //tabPnlChillers.Enabled = true;
                                tabStrip.Tabs[2].Visible = true;
                                //tabPnlChillers.CssClass = "";
                            }
                            break;
                        case BusinessConstants.EquipmentClass.FanEquipmentClassID:

                            //FAN SUPPLY STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.SupplyFanStatusPointTypeID };

                            //set fan supply status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.FanEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartFanSupplyStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartFanSupplyStatus.ChartAreas["fanSupplyStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartFanSupplyStatus.ChartAreas["fanSupplyStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartFanSupplyStatus.ChartAreas["fanSupplyStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartFanSupplyStatus.ChartAreas["fanSupplyStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartFanSupplyStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetFanSupplyStatus.Visible = true;
                                tabStrip.Tabs[3].Visible = true;
                            }

                            //FAN RETURN STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.ReturnFanStatusPointTypeID };

                            //set fan return status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.FanEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartFanReturnStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartFanReturnStatus.ChartAreas["fanReturnStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartFanReturnStatus.ChartAreas["fanReturnStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartFanReturnStatus.ChartAreas["fanReturnStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartFanReturnStatus.ChartAreas["fanReturnStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartFanReturnStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetFanReturnStatus.Visible = true;
                                tabStrip.Tabs[3].Visible = true;
                            }

                            //FAN EXHAUST STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.ExhaustFanStatusPointTypeID };

                            //set fan exhaust status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.FanEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartFanExhaustStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartFanExhaustStatus.ChartAreas["fanExhaustStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartFanExhaustStatus.ChartAreas["fanExhaustStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartFanExhaustStatus.ChartAreas["fanExhaustStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartFanExhaustStatus.ChartAreas["fanExhaustStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartFanExhaustStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetFanExhaustStatus.Visible = true;
                                tabStrip.Tabs[3].Visible = true;
                            }
                            break;
                        case BusinessConstants.EquipmentClass.HeatRejectionEquipmentClassID:
                            
                            //HEAT REJECTION COOLING TOWER ENABLE CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.CoolingTowerEnablePointTypeID };

                            //set heat rejection cooling tower enable chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.HeatRejectionEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartHeatRejectionCoolingTowerEnable.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartHeatRejectionCoolingTowerEnable.ChartAreas["heatRejectionCoolingTowerEnableChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartHeatRejectionCoolingTowerEnable.ChartAreas["heatRejectionCoolingTowerEnableChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartHeatRejectionCoolingTowerEnable.ChartAreas["heatRejectionCoolingTowerEnableChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartHeatRejectionCoolingTowerEnable.ChartAreas["heatRejectionCoolingTowerEnableChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartHeatRejectionCoolingTowerEnable.Series.Add(series);
                                }

                                tabStrip.Visible = widgetHeatRejectionCoolingTowerEnable.Visible = true;
                                tabStrip.Tabs[4].Visible = true;
                            }

                            //HEAT REJECTION COOLING TOWER FAN STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.CoolingTowerFanStatusPointTypeID };

                            //set heat rejection cooling tower fan status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.HeatRejectionEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartHeatRejectionCoolingTowerFanStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartHeatRejectionCoolingTowerFanStatus.ChartAreas["heatRejectionCoolingTowerFanStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartHeatRejectionCoolingTowerFanStatus.ChartAreas["heatRejectionCoolingTowerFanStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartHeatRejectionCoolingTowerFanStatus.ChartAreas["heatRejectionCoolingTowerFanStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartHeatRejectionCoolingTowerFanStatus.ChartAreas["heatRejectionCoolingTowerFanStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartHeatRejectionCoolingTowerFanStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetHeatRejectionCoolingTowerFanStatus.Visible = true;
                                tabStrip.Tabs[4].Visible = true;
                            }

                            //HEAT REJECTION COOLING TOWER SPRAY STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.CoolingTowerSprayStatusPointTypeID };

                            //set heat rejection cooling tower spray status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.HeatRejectionEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartHeatRejectionCoolingTowerSprayStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartHeatRejectionCoolingTowerSprayStatus.ChartAreas["heatRejectionCoolingTowerSprayStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartHeatRejectionCoolingTowerSprayStatus.ChartAreas["heatRejectionCoolingTowerSprayStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartHeatRejectionCoolingTowerSprayStatus.ChartAreas["heatRejectionCoolingTowerSprayStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartHeatRejectionCoolingTowerSprayStatus.ChartAreas["heatRejectionCoolingTowerSprayStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartHeatRejectionCoolingTowerSprayStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetHeatRejectionCoolingTowerSprayStatus.Visible = true;
                                tabStrip.Tabs[4].Visible = true;
                            }

                            //HEAT REJECTION COOLING TOWER SUMP TEMP CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.CoolingTowerSumpTempPointTypeID };

                            //set heat rejection cooling tower sump temp chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.HeatRejectionEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartHeatRejectionCoolingTowerSumpTemp.Series.Clear();

                                //set axis label
                                chartHeatRejectionCoolingTowerSumpTemp.ChartAreas["heatRejectionCoolingTowerSumpTempChartArea"].AxisY.Title = "Temperature (" + plots.First().BuildingSettingBasedEngUnits + ")";

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartHeatRejectionCoolingTowerSumpTemp.ChartAreas["heatRejectionCoolingTowerSumpTempChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartHeatRejectionCoolingTowerSumpTemp.ChartAreas["heatRejectionCoolingTowerSumpTempChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartHeatRejectionCoolingTowerSumpTemp.ChartAreas["heatRejectionCoolingTowerSumpTempChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartHeatRejectionCoolingTowerSumpTemp.ChartAreas["heatRejectionCoolingTowerSumpTempChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartHeatRejectionCoolingTowerSumpTemp.Series.Add(series);
                                }

                                tabStrip.Visible = widgetHeatRejectionCoolingTowerSumpTemp.Visible = true;
                                tabStrip.Tabs[4].Visible = true;
                            }
                            break;
                        case BusinessConstants.EquipmentClass.PumpEquipmentClassID:

                            //PUMP CHW LOOP STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.CHW1LoopPumpStatusPointTypeID };

                            //set pump chw loop status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.PumpEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartPumpCHWLoopStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartPumpCHWLoopStatus.ChartAreas["pumpCHWLoopStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartPumpCHWLoopStatus.ChartAreas["pumpCHWLoopStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartPumpCHWLoopStatus.ChartAreas["pumpCHWLoopStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartPumpCHWLoopStatus.ChartAreas["pumpCHWLoopStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartPumpCHWLoopStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetPumpCHWLoopStatus.Visible = true;
                                tabStrip.Tabs[5].Visible = true;
                            }

                            //PUMP HW LOOP STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.HW1LoopPumpStatusPointTypeID };

                            //set pump hw loop status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.PumpEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartPumpHWLoopStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartPumpHWLoopStatus.ChartAreas["pumpHWLoopStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartPumpHWLoopStatus.ChartAreas["pumpHWLoopStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartPumpHWLoopStatus.ChartAreas["pumpHWLoopStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartPumpHWLoopStatus.ChartAreas["pumpHWLoopStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartPumpHWLoopStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetPumpHWLoopStatus.Visible = true;
                                tabStrip.Tabs[5].Visible = true;
                            }

                            //PUMP CW LOOP STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.CW1LoopPumpStatusPointTypeID };

                            //set pump hw loop status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.PumpEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartPumpCWLoopStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartPumpCWLoopStatus.ChartAreas["pumpCWLoopStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartPumpCWLoopStatus.ChartAreas["pumpCWLoopStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartPumpCWLoopStatus.ChartAreas["pumpCWLoopStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartPumpCWLoopStatus.ChartAreas["pumpCWLoopStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartPumpCWLoopStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetPumpCWLoopStatus.Visible = true;
                                tabStrip.Tabs[5].Visible = true;
                            }


                            //PUMP DHW LOOP STATUS CHART---
                            pointTypeIDs = new List<int> { BusinessConstants.PointType.DHWLoopPumpStatusPointTypeID };

                            //set pump dhw loop status chart
                            points = DataMgr.PointDataMapper.GetAllPointsByBIDAndEquipmentClassIDAndPointTypeIDsWithFirstEquipmentName(Convert.ToInt32(ddlBuilding.SelectedValue), BusinessConstants.EquipmentClass.PumpEquipmentClassID, pointTypeIDs);

                            pids = points.Select(p => p.PID).ToList();

                            plots = points != null && points.Any() ? DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, twelveHoursAgoUTCDateTime, currentUTCDateTime, false) : null;

                            if (plots != null && plots.Any())
                            {
                                chartPumpDHWLoopStatus.Series.Clear();

                                foreach (GetPointsData p in points)
                                {
                                    //get raw data
                                    var rawData = plots.Where(pt => pt.PID == p.PID).ToArray();

                                    string longName = p.PointName + " (" + p.EquipmentName + ")";
                                    string shortName = p.PointName;

                                    //create new series, set chart type and name
                                    var series =
                                    new Series
                                    {
                                        ChartType = SeriesChartType.FastLine,
                                        BorderWidth = 2,
                                        Name = longName,
                                        ToolTip = "X: #VALX{g}, Y: #VALY",
                                        LegendToolTip = longName,
                                        LegendText = longName,
                                    };

                                    //set series name as point name

                                    //set axis intervaltype
                                    chartPumpDHWLoopStatus.ChartAreas["pumpDHWLoopStatusChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                                    //set axis format:  g = short date and time, d = short date
                                    chartPumpDHWLoopStatus.ChartAreas["pumpDHWLoopStatusChartArea"].AxisX.LabelStyle.Format = "g";

                                    //add new rawData to series
                                    series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                                    //TODO: on front end after microsoft bug fix.
                                    //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                                    chartPumpDHWLoopStatus.ChartAreas["pumpDHWLoopStatusChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                                    chartPumpDHWLoopStatus.ChartAreas["pumpDHWLoopStatusChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                                    //add series to chart
                                    chartPumpDHWLoopStatus.Series.Add(series);
                                }

                                tabStrip.Visible = widgetPumpDHWLoopStatus.Visible = true;
                                tabStrip.Tabs[5].Visible = true;
                            }
                            break;
                    }
                }

                //set active tab index as first one thats visible
                SetActiveTab();
            }


        #endregion

        #region Building Scroller

            /// <summary>
            /// building item click
            /// </summary>
            protected void radRotaterBuildingScroller_OnItemClick(object sender, RadRotatorEventArgs e)
            {
                try
                {
                    HiddenField hdnBID = (HiddenField)e.Item.Controls[1];
                    ddlBuilding.SelectedValue = hdnBID.Value;

                    BindOnBuildingSelect(Convert.ToInt32(hdnBID.Value), e.Item.Index);

                    liveButton_Click(null, null);
                }
                catch
                {
                }
            }

            private class BuildingImage
            {
                //public byte[] image { get; set; }
                public string src { get; set; }
                public string alt { get; set; }
                //public bool radVisible { get; set; }
                //public bool imgVisible { get; set; }
                public bool visible { get; set; }
                public int bid { get; set; }
            }

        #endregion

        #region Plot Data Classes

            //public class StatPlot
            //{
            //    public double val { get; set; }
            //    public DateTime? day { get; set; }
            //}

        #endregion

        #region Plot Methods

            /// <summary>
            /// Plots the raw data points selected.
            /// </summary>
            protected void PlotSimpleRawDataPoints(List<int> pids, List<int> currentPlotedPoints)
            {
                //continue if one or more points were selected
                if (pids.Any())
                {
                    var result = DataMgr.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, DateTimeHelper.GetXHoursAgo(DateTime.UtcNow, 12) , DateTime.UtcNow, false);

                    if (result.Any())
                    {
                        //foreach selected pid
                        foreach (int pid in pids)
                        {
                            //get raw data
                            var rawData = result.Where(p => p.PID == pid).ToArray();

                            //get point name from first value
                            var pointName = lbLivePoints.Items.FindByValue(pid.ToString()).Text;

                            //get eng unit from first value
                            var engUnit = (rawData.Any() ? rawData.First().BuildingSettingBasedEngUnits : String.Empty);

                            string longName = pointName + " (" + engUnit + ")";
                            string shortName = pointName;

                            //create new series, set chart type and name
                            var series =
                            new Series
                            {
                                ChartType = ddlLiveChartType.SelectedValue == "Line" ? SeriesChartType.FastLine : SeriesChartType.FastPoint,
                                BorderWidth = 2,
                                Name = longName,
                                ToolTip = "X: #VALX{g}, Y: #VALY",
                                LegendToolTip = longName,
                                LegendText = longName,
                            };

                            //set series name as point name

                            //set axis intervaltype
                            rawChart.ChartAreas["rawChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;

                            //set axis format: g = short date and time, d = short date
                            rawChart.ChartAreas["rawChartArea"].AxisX.LabelStyle.Format = "g";

                            //add new rawData to series
                            series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                            //TODO: on front end after microsoft bug fix.
                            //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                            rawChart.ChartAreas["rawChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                            rawChart.ChartAreas["rawChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                            //add series to chart
                            rawChart.Series.Add(series);
                        }
                    }

                    //set chart title
                    rawChart.Titles["rawTitle"].Text = "Plot points for " + ddlLiveEquipment.SelectedItem.Text + ".";
                }
            }

            /// <summary>
            /// Plots the raw data points selected.
            /// </summary>
            protected void PlotAdvancedRawDataPoints(List<int> pids, List<int> currentPlotedPoints)
            {
                ////int counter = 1;
                //int numOfEmptyPlots = 0;
                //bool hasPlotData = false;

                ////continue if p one or more points were selected
                //if (pids.Any())
                //{
                //    var result = mDataManager.RawDataMapper.GetConvertedRawPlotDataForPIDs(pids, DateTimeHelper.GetStartOfCurrentDate(), DateTime.UtcNow, false);

                //    if (result.Any())
                //    {
                //        //foreach selected pid
                //        foreach (int pid in pids)
                //        {
                //            //get raw data
                //            var rawData = result.Where(p => p.PID == pid).ToArray();

                //            //get point name from first value
                //            var pointName = lbLivePoints.Items.FindByValue(pid.ToString()).Text;

                //            //get eng unit from first value
                //            var engUnit = (rawData.Any() ? rawData.First().EngUnits : String.Empty);

                //            string seriesName = pointName + " (" + engUnit + ")";

                //            //create new series, set chart type and name
                //            var chartSeries =
                //            new ChartSeries
                //            {
                //                Type = ChartSeriesType.Line,
                //                Name = seriesName
                //            };

                //            //set series name as point name

                //            foreach (var r in rawData)
                //            {
                //                var item = new ChartSeriesItem(r.ConvertedDateLogged, r.ConvertedRawValue);

                //                item.Label.Appearance.Visible = false;
                //                chartSeries.AddItem(item);
                //            }

                //            chartSeries.DataXColumn = "ConvertedDateLogged";
                //            chartSeries.DataYColumn = "ConvertedRawValue";
                            
                //            radChart.Series.Add(chartSeries);
                            
                //            chartSeries.PlotArea.XAxis.Visible = Telerik.Charting.Styles.ChartAxisVisibility.True;
                //            chartSeries.PlotArea.YAxis.Visible = Telerik.Charting.Styles.ChartAxisVisibility.True;
                //            chartSeries.PlotArea.XAxis.IsZeroBased = false;
                //            chartSeries.PlotArea.YAxis.IsZeroBased = false;

                //            chartSeries.PlotArea.XAxis.Appearance.ValueFormat = Telerik.Charting.Styles.ChartValueFormat.LongDate;
                //            chartSeries.PlotArea.XAxis.Appearance.CustomFormat = "g";
                //            //chartSeries.PlotArea.XAxis.Step = .25;


                //            //chartSeries.PlotArea.YAxis.Appearance.ValueFormat = Telerik.Charting.Styles.ChartValueFormat.Number;
                //            //chartSeries.Appearance.ShowLabels = false;

                //            //TODO: on front end after microsoft bug fix.
                //            //Workaround, cannot adjust title on front end, has to be done after bind in code behind.

                //            if (rawData.Any())
                //            {
                //                hasPlotData = true;
                //            }
                //            else
                //            {
                //                numOfEmptyPlots++;
                //            }

                //            //if (counter == 1)
                //            //{
                //            //    lblRawPoints.Text = pointName;
                //            //}
                //            //else
                //            //{
                //            //    chartRegion.lblRawPoints.Text += ", " + pointName;
                //            //}                

                //            //counter++;
                //        }


                //    }

                //    if (hasPlotData)
                //    {
                //        //set chart title
                //        radChart.ChartTitle.TextBlock.Text = "Plot points for " + ddlLiveEquipment.SelectedItem.Text + ".";
                //        //set 3d, and point depth if 3d selected


                //        //set results
                //        if (numOfEmptyPlots > 0)
                //        {
                //            lblResults.Text = emptyPlots;
                //        }
                //        else
                //        {
                //            lblResults.Text = successfulPlots;
                //        }                        
                //    }
                //    else
                //    {
                //        lblResults.Text = noPlots;
                //    }
                //}
                //else if (!currentPlotedPoints.Any())
                //{
                //    lblResults.Text = noPlots;
                //}

                //lblResults.Visible = true;
            }

            /// <summary>
            /// Removed the unselected rad data point plots.
            /// </summary>
            protected void RemoveSimpleRawDataPointPlots(List<int> pids)
            {
                if (pids.Any())
                {
                    //get full points
                    List<GetPointsData> points = DataMgr.PointDataMapper.GetFullPointsByPIDList(pids, true);

                    //foreach pointName
                    foreach (GetPointsData p in points)
                    {
                        string name = p.PointName + " (" + p.BuildingSettingBasedEngUnits + ")";

                        //if series exists remove... which might not be the cases if no data was ploted even though the point was selected previously
                        if (rawChart.Series.IndexOf(name) != -1)
                        {
                            Series s = rawChart.Series[name];                                                    
                            rawChart.Series.Remove(s);
                        }
                    }
                }
            }
     
            /// <summary>
            /// Removed the unselected rad data point plots.
            /// </summary>
            protected void RemoveAdvancedRawDataPointPlots(List<int> pids)
            {
                //if (pids.Any())
                //{
                //    //get full points
                //    List<PointDataMapper.GetPointsData> points = PointDataMapper.GetFullPointsByPIDList(pids);

                //    //foreach pointName
                //    foreach (PointDataMapper.GetPointsData p in points)
                //    {
                //        ChartSeries cs = radChart.GetSeries(p.PointName + " (" + p.EngUnits + ")");
                //        radChart.Series.Remove(cs);
                //    }
                //}
            } 

        #endregion

        #region Helper Methods

        private void SetActiveNavButton(LinkButton lb)
        {
            btnHome.CssClass = "lnkBtnDock";
            btnLive.CssClass = "lnkBtnDock";

            lb.CssClass = "lnkBtnDockActive";
        }

        public void ResetTabAndWidgetVisibility()
        {
            tabStrip.Visible =
            widgetAirHandlerTemperatures.Visible = widgetAirHandlerSupplyFanStatus.Visible =
            widgetBoilerHWStatus.Visible =
            widgetChillerStatus.Visible = widgetChillerPower.Visible = 
            widgetFanExhaustStatus.Visible = widgetFanReturnStatus.Visible = widgetFanSupplyStatus.Visible =
            widgetHeatRejectionCoolingTowerEnable.Visible = widgetHeatRejectionCoolingTowerFanStatus.Visible = widgetHeatRejectionCoolingTowerSprayStatus.Visible = widgetHeatRejectionCoolingTowerSumpTemp.Visible =
            widgetPumpCHWLoopStatus.Visible = widgetPumpCWLoopStatus.Visible = widgetPumpDHWLoopStatus.Visible = widgetPumpHWLoopStatus.Visible = false;

            tabStrip.Tabs[0].Visible = tabStrip.Tabs[1].Visible = tabStrip.Tabs[2].Visible = tabStrip.Tabs[3].Visible = tabStrip.Tabs[4].Visible = tabStrip.Tabs[5].Visible = false;

            //tabPnlAirHandlers.Enabled = tabPnlBoilers.Enabled = tabPnlChillers.Enabled = tabPnlFans.Enabled = tabPnlHeatRejections.Enabled = tabPnlPumps.Enabled = false;

            //tabPnlAirHandlers.Attributes.Add("visibility", "hidden");
            //tabPnlBoilers.Attributes.Add("visibility", "hidden"); 
            //tabPnlChillers.Attributes.Add("visibility", "hidden");
            //tabPnlFans.Attributes.Add("visibility", "hidden");
            //tabPnlHeatRejections.Attributes.Add("visibility", "hidden");
            //tabPnlPumps.Attributes.Add("visibility", "hidden");


            //tabPnlAirHandlers.CssClass = "ajax__tab_disabled";
            //tabPnlBoilers.CssClass = "ajax__tab_disabled";
            //tabPnlChillers.CssClass = "ajax__tab_disabled";
            //tabPnlFans.CssClass = "ajax__tab_disabled";
            //tabPnlHeatRejections.CssClass = "ajax__tab_disabled";
            //tabPnlPumps.CssClass = "ajax__tab_disabled";
        }

        /// <summary>
        /// set active tab index as first one thats visible
        /// </summary>
        public void SetActiveTab()
        {
            //if tabcontainer is visible, set first enabled tab as active
            if (tabStrip.Visible)
            {
                for (int index = 0; index < tabStrip.Tabs.Count; index++)
                {
                    if (tabStrip.Tabs[index].Visible)
                    {                        
                        tabStrip.SelectedIndex = index;
                        radMultiPage.SelectedIndex = index;
                        break;
                    }
                }
            }
        }

        public void SetChartVisiblity()
        {
            //if no data was already plotted or newly plotted. check by series count, not by current result set.
            if (rawChart.Series.Count() > 0)
            {
                divResults.Visible = false;
                rawChart.Visible = true;
            }
            else
            {
                lblResults.Text = noPlots;
                divResults.Visible = true;
                rawChart.Visible = false;
            }
        }

        #endregion
    }
}