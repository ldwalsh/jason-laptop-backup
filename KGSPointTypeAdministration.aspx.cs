﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Models.Point;
using CW.Data.Models.PointType;
using CW.Utility.Web;
using CW.Website._controls.admin;
using CW.Website._controls.admin.Points;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSPointTypeAdministration : SitePage
    {
        #region Properties

        private PointType mPointType;

        const string addPointTypeSuccess = " point type addition was successful.";
        const string addPointTypeFailed = "Adding point type failed. Please contact an administrator.";
        const string pointTypeExists = "A point type with that name already exists.";
        protected string selectedValue;
        const string updateSuccessful = "Point type update was successful.";
        const string updateFailed = "Point type update failed. Please contact an administrator.";
        const string deleteSuccessful = "Point type deletion was successful.";
        const string deleteFailed = "Point type deletion failed. Please contact an administrator.";

        const string pointExists = "Cannot delete point type because one or more points are associated to equipment with that point type. Please disassociate points from equipment, or change the point type first.";
        const string vPointExists = "Cannot delete point type because one or more vpoints are associated to equipment with that point type. Please disassociate vpoints from equipment, or change the point type first.";
        const string vPrePointExists = "Cannot delete point type because one or more vprepoints are associated to equipment with that point type. Please disassociate vprepoints from equipment, or change the point type first.";
        const string updatePointExists = "Cannot disable PointEnabled because one or more points are associated to equipment with that point type. Please disassociate points from equipment, or change the point type first.";
        const string updateVPointExists = "Cannot disable VPointEnabled because one or more vpoints are associated to equipment with that point type. Please disassociate vpoints from equipment, or change the point type first.";
        const string updateVPrePointExists = "Cannot disable VPrePointEnabled because one or more vprepoints are associated to equipment with that point type. Please disassociate vprepoints from equipment, or change the point type first.";

        const string analysisPointExists = "Cannot delete point type because one or more points are associated to analyses with that point type. Please disassociate points from analyses, or change the point type first.";
        const string analysisVPointExists = "Cannot delete point type because one or more vpoints are associated to analyses with that point type. Please disassociate vpoints from analyses, or change the point type first.";
        const string analysisVPrePointExists = "Cannot delete point type because one or more vprepoints are associated to analyses with that point type. Please disassociate vprepoints from analyses, or change the point type first.";
        const string updateAnalysisPointExists = "Cannot disable PointEnabled because one or more points are associated to analyses with that point type. Please disassociate points from analyses, or change the point type first.";
        const string updateAnalysisVPointExists = "Cannot disable VPointEnabled because one or more vpoints are associated to analyses with that point type. Please disassociate vpoints from analyses, or change the point type first.";
        const string updateAnalysisVPrePointExists = "Cannot disable VPrePointEnabled because one or more vprepoints are associated to analyses with that point type. Please disassociate vprepoints from analyses, or change the point type first.";

        const string equipmentTypeExists = "Cannot delete point type because one or more equipment types are associated. Please disassociate from point type first.";

        const string initialSortDirection = "ASC";
        const string initialSortExpression = "PointTypeName";

        //gets and sets the gridview viewstate sort direction for the dataview
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? string.Empty; }
            set { ViewState["SortDirection"] = value; }
        }

        //gets and sets the gridview viewstate sort expression for the dataview
        private string GridViewSortExpression
        {
            get { return ViewState["SortExpression"] as string ?? string.Empty; }
            set { ViewState["SortExpression"] = value; }
        }

        public IClearCache ClearCacher { get; set; }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            ClearCacher = new ClearCachePoints();

            //datamanger in sitepage
            //logged in security check in master page.
            //secondary security check specific to this page.
            //Check kgs super admin or higher.
            if (!siteUser.IsKGSSuperAdminOrHigher) Response.Redirect("/Home.aspx");

            //hide labels
            lblErrors.Visible = false;
            lblAddError.Visible = false;
            lblEditError.Visible = false;

            //if the page is not a postback, clear the viewstate and set the initial viewstate settings
            if (!Page.IsPostBack)
            {
                BindPointTypes();

                ViewState.Clear();
                GridViewSortDirection = initialSortDirection;
                GridViewSortExpression = initialSortExpression;

                BindPointClasses(ddlAddPointClass);

                sessionState["Search"] = String.Empty;
            }
        }

        #endregion

        #region Set Fields and Data

        protected void SetPointTypeIntoEditForm(GetPointTypeData pointType)
        {
            //ID
            hdnEditID.Value = Convert.ToString(pointType.PointTypeID);
            //Point Type Name
            txtEditPointTypeName.Text = pointType.PointTypeName;
            //Display Name
            txtEditDisplayName.Text = pointType.DisplayName;
            //Point Type Description
            txtEditPointTypeDescription.Value = pointType.PointTypeDescription;
            //Point Class
            lblEditPointClass.Text = pointType.PointClassName;
            //Point Enabled
            chkEditPointEnabled.Checked = pointType.PointEnabled;
            chkEditPointEnabled.Enabled = pointType.PointEnabled;
            //VPoint Enabled
            chkEditVPointEnabled.Checked = pointType.VPointEnabled;
            chkEditVPointEnabled.Enabled = pointType.VPointEnabled;
            //VPrePoint Enabled
            chkEditVPrePointEnabled.Checked = pointType.VPrePointEnabled;
            chkEditVPrePointEnabled.Enabled = pointType.VPrePointEnabled;
            //Unassigned
            lblEditUnassigned.Text = pointType.IsUnassigned.ToString();
        }

        #endregion

        #region Load and Bind Fields

        private void BindPointClasses(DropDownList ddl)
        {
            ddl.DataTextField = "PointClassName";
            ddl.DataValueField = "PointClassID";
            ddl.DataSource = DataMgr.PointClassDataMapper.GetAllPointClasses();
            ddl.DataBind();
        }

        #endregion

        #region Load Point Type

        protected void LoadAddFormIntoPointType(PointType pointType)
        {
            //Point Type Name
            pointType.PointTypeName = txtAddPointTypeName.Text;
            //Display Name
            pointType.DisplayName = txtAddDisplayName.Text;
            //Point Type Description
            pointType.PointTypeDescription = String.IsNullOrEmpty(txtAddPointTypeDescription.Value) ? null : txtAddPointTypeDescription.Value;
            //Point Class
            pointType.PointClassID = Convert.ToInt32(ddlAddPointClass.SelectedValue);
            //Point Enabled
            pointType.PointEnabled = chkAddPointEnabled.Checked;
            //VPoint Enabled
            pointType.VPointEnabled = chkAddVPointEnabled.Checked;
            //VPrePoint Enabled
            pointType.VPrePointEnabled = chkAddVPrePointEnabled.Checked;
            //Unassigned
            pointType.IsUnassigned = chkAddUnassigned.Checked;

            pointType.DateModified = DateTime.UtcNow;
        }

        protected void LoadEditFormIntoPointType(PointType pointType)
        {
            //ID
            pointType.PointTypeID = Convert.ToInt32(hdnEditID.Value);
            //Point Type Name
            pointType.PointTypeName = txtEditPointTypeName.Text;
            //Display Name
            pointType.DisplayName = txtEditDisplayName.Text;
            //Point Type Description
            pointType.PointTypeDescription = String.IsNullOrEmpty(txtEditPointTypeDescription.Value) ? null : txtEditPointTypeDescription.Value;

            //Point Class
            //cannot edit point types class

            //Point Enabled
            pointType.PointEnabled = chkEditPointEnabled.Checked;
            //VPoint Enabled
            pointType.VPointEnabled = chkEditVPointEnabled.Checked;
            //VPrePoint Enabled
            pointType.VPrePointEnabled = chkEditVPrePointEnabled.Checked;

            //Unassigned
            //cannot edit unassigned
        }

        #endregion

        #region Button Events

        /// <summary>
        /// Add point type Button on click.
        /// </summary>
        protected void addPointTypeButton_Click(object sender, EventArgs e)
        {
            //check that point type does not already exist
            if (!DataMgr.PointTypeDataMapper.DoesPointTypeExist(txtAddPointTypeName.Text))
            {
                mPointType = new PointType();

                //load the form into the point type
                LoadAddFormIntoPointType(mPointType);

                try
                {
                    //insert new point type
                    DataMgr.PointTypeDataMapper.InsertPointType(mPointType);

                    LabelHelper.SetLabelMessage(lblAddError, mPointType.PointTypeName + addPointTypeSuccess, lnkSetFocusAdd);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding point type.", sqlEx);
                    LabelHelper.SetLabelMessage(lblAddError, addPointTypeFailed, lnkSetFocusAdd);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding point type.", ex);
                    LabelHelper.SetLabelMessage(lblAddError, addPointTypeFailed, lnkSetFocusAdd);
                }
            }
            else
            {
                //point type exists
                LabelHelper.SetLabelMessage(lblAddError, pointTypeExists, lnkSetFocusAdd);
            }
        }

        /// <summary>
        /// Update point type Button on click. Updates point type data.
        /// </summary>
        protected void updatePointTypeButton_Click(object sender, EventArgs e)
        {
            //set to false if a check fails
            bool passedUpdateChecks = true;

            //create and load the form into the point type
            PointType mPointType = new PointType();
            LoadEditFormIntoPointType(mPointType);

            //check if the pointtype name exists for another pointtype if trying to change
            if (DataMgr.PointTypeDataMapper.DoesPointTypeExist(mPointType.PointTypeID, mPointType.PointTypeName))
            {
                LabelHelper.SetLabelMessage(lblEditError, pointTypeExists, lnkSetFocusView);
            }
            //update the point type
            else
            {
                //check if user is trying to disable/uncheck point,vpoint,or vprepoint use.

                //compare againts current point type values
                PointType originalPointType = DataMgr.PointTypeDataMapper.GetPointType(mPointType.PointTypeID);

                //was pointenabled set true orginally, and user tryed to disable
                if (originalPointType.PointEnabled && originalPointType.PointEnabled != mPointType.PointEnabled)
                {
                    //if so, check if points are using the point type
                    if (DataMgr.PointDataMapper.IsPointAssociatedWithPointType(mPointType.PointTypeID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updatePointExists, lnkSetFocusView);
                        passedUpdateChecks = false;
                    }
                    else if (DataMgr.AnalysisPointTypeDataMapper.IsAnalysisAssociatedWithInputPointType(mPointType.PointTypeID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateAnalysisPointExists, lnkSetFocusView);
                        passedUpdateChecks = false;
                    }
                }
                //was vpointenabled set true orginally, and user tryed to disable
                else if (passedUpdateChecks && originalPointType.VPointEnabled && originalPointType.VPointEnabled != mPointType.VPointEnabled)
                {
                    //if so, check if vpoints are using the point type
                    if (DataMgr.PointDataMapper.IsVPointAssociatedWithPointType(mPointType.PointTypeID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateVPointExists, lnkSetFocusView);
                        passedUpdateChecks = false;
                    }
                    else if (DataMgr.AnalysisPointTypeDataMapper.IsAnalysisAssociatedWithOutputVPointType(mPointType.PointTypeID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateAnalysisVPointExists, lnkSetFocusView);
                        passedUpdateChecks = false;
                    }
                }
                //was vprepointenabled set true orginally, and user tryed to disable
                else if (passedUpdateChecks && originalPointType.VPrePointEnabled && originalPointType.VPrePointEnabled != mPointType.VPointEnabled)
                {
                    //if so, check if vprepoints are using the point type
                    if (DataMgr.PointDataMapper.IsVPrePointAssociatedWithPointType(mPointType.PointTypeID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateVPrePointExists, lnkSetFocusView);
                        passedUpdateChecks = false;
                    }
                    else if (DataMgr.AnalysisPointTypeDataMapper.IsAnalysisAssociatedWithOutputVPrePointType(mPointType.PointTypeID))
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateAnalysisVPrePointExists, lnkSetFocusView);
                        passedUpdateChecks = false;
                    }
                }

                //if all checks passed
                if (passedUpdateChecks)
                {
                    //try to update the point type
                    try
                    {
                        DataMgr.PointTypeDataMapper.UpdatePointType(mPointType);

                        LabelHelper.SetLabelMessage(lblEditError, updateSuccessful, lnkSetFocusView);

                        ClearCache();

                        BindPointTypes();
                    }
                    catch (Exception ex)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating point type.", ex);
                    }
                }
            }
        }

        /// <summary>
        /// Search button on click, searches by all individual words entered and intersects.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void searchButton_Click(object sender, EventArgs e)
        {
            sessionState["Search"] = txtSearch.Text;
            BindPointTypes();
        }

        /// <summary>
        /// Rebinds grid to view all.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void viewAll_Click(object sender, EventArgs e)
        {
            txtSearch.Text = "";
            sessionState["Search"] = String.Empty;
            BindPointTypes();
        }

        #endregion

        #region Checkbox events

        /// <summary>
        /// Enables and disables appropriate checkboxes accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkAddPointEnabled_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkAddPointEnabled.Checked)
            {
                chkAddVPointEnabled.Enabled = false;
                chkAddVPrePointEnabled.Enabled = false;
            }
            else
            {
                chkAddVPointEnabled.Enabled = true;
                chkAddVPrePointEnabled.Enabled = true;
            }
        }

        /// <summary>
        /// Enables and disables appropriate checkboxes accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkAddVPointEnabled_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkAddVPointEnabled.Checked)
            {
                chkAddPointEnabled.Enabled = false;
            }
            else if (!chkAddVPrePointEnabled.Checked)
            {
                chkAddPointEnabled.Enabled = true;
            }
        }

        /// <summary>
        /// Enables and disables appropriate checkboxes accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkAddVPrePointEnabled_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkAddVPrePointEnabled.Checked)
            {
                chkAddPointEnabled.Enabled = false;
            }
            else if (!chkAddVPointEnabled.Checked)
            {
                chkAddPointEnabled.Enabled = true;
            }
        }

        /// <summary>
        /// Enables and disables appropriate checkboxes accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkEditPointEnabled_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkEditPointEnabled.Checked)
            {
                chkEditVPointEnabled.Enabled = false;
                chkEditVPrePointEnabled.Enabled = false;
            }
            else
            {
                chkEditVPointEnabled.Enabled = true;
                chkEditVPrePointEnabled.Enabled = true;
            }
        }

        /// <summary>
        /// Enables and disables appropriate checkboxes accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkEditVPointEnabled_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkEditVPointEnabled.Checked)
            {
                chkEditPointEnabled.Enabled = false;
            }
            else if (!chkEditVPrePointEnabled.Checked)
            {
                chkEditPointEnabled.Enabled = true;
            }
        }

        /// <summary>
        /// Enables and disables appropriate checkboxes accordingly
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkEditVPrePointEnabled_OnCheckedChanged(object sender, EventArgs e)
        {
            if (chkEditVPrePointEnabled.Checked)
            {
                chkEditPointEnabled.Enabled = false;
            }
            else if (!chkEditVPointEnabled.Checked)
            {
                chkEditPointEnabled.Enabled = true;
            }
        }

        #endregion

        #region Tab Events

        /// <summary>
        /// Tab changed event, bind point type grid after tab changed back from add user tab
        /// </summary>
        protected void onTabClick(object sender, EventArgs e)
        {
            //if active tab index is 0, bind point classes to grid
            if (radTabStrip.MultiPage.SelectedIndex == 0)
            {
                BindPointTypes();
            }
        }

        #endregion

        #region Grid Events

        protected void gridPointTypes_OnDataBound(object sender, EventArgs e)
        {
            //CANNOT DELETE CERTAIN TYPES
            GridViewRowCollection rows = gridPointTypes.Rows;

            foreach (GridViewRow row in rows)
            {
                if ((row.RowState & DataControlRowState.Edit) == 0)
                {
                    //hide edit and delete link for undeletable point types and unassigned point types, which cannot be edited or deleted
                    try
                    {
                        //get rows datakey which right now is just one single key. pointtype id.
                        if (BusinessConstants.PointType.UnDeletablePointTypesDictionary.ContainsValue(Convert.ToInt32(((GridView)sender).DataKeys[row.RowIndex].Value)))
                        {
                            //hide delete link
                            ((LinkButton)row.Cells[9].Controls[1]).Visible = false;
                        }
                        else if (Convert.ToBoolean(((DataBoundLiteralControl)row.Cells[8].Controls[0]).Text))
                        {
                            //hide delete link
                            ((LinkButton)row.Cells[9].Controls[1]).Visible = false;
                        }
                    }
                    catch
                    {
                    }
                }
            }
        }

        protected void gridPointTypes_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            pnlEditPointType.Visible = false;
            dtvPointType.Visible = true;

            int pointTypeID = Convert.ToInt32(gridPointTypes.DataKeys[gridPointTypes.SelectedIndex].Values["PointTypeID"]);

            //set data source
            dtvPointType.DataSource = DataMgr.PointTypeDataMapper.GetFullPointTypeByID(pointTypeID);
            //bind point type to details view
            dtvPointType.DataBind();
        }

        protected void gridPointTypes_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)
            {
                //Note: The autogenerated pager table is not shown with just one page.

                Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                tblPager.CssClass = "pageTable";

                TableRow theRow = tblPager.Rows[0];

                LinkButton ctrlPrevious = new LinkButton();
                ctrlPrevious.CommandArgument = "Prev";
                ctrlPrevious.CommandName = "Page";
                ctrlPrevious.Text = "« Previous Page";
                ctrlPrevious.CssClass = "pageLink";

                TableCell cellPreviousPage = new TableCell();
                cellPreviousPage.Controls.Add(ctrlPrevious);
                tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                LinkButton ctrlNext = new LinkButton();
                ctrlNext.CommandArgument = "Next";
                ctrlNext.CommandName = "Page";
                ctrlNext.Text = "Next Page »";
                ctrlNext.CssClass = "pageLink";


                TableCell cellNextPage = new TableCell();
                cellNextPage.Controls.Add(ctrlNext);
                tblPager.Rows[0].Cells.Add(cellNextPage);
            }
        }

        protected void gridPointTypes_Editing(object sender, GridViewEditEventArgs e)
        {
            dtvPointType.Visible = false;
            pnlEditPointType.Visible = true;

            int pointTypeID = Convert.ToInt32(gridPointTypes.DataKeys[e.NewEditIndex].Values["PointTypeID"]);

            GetPointTypeData mPointType = DataMgr.PointTypeDataMapper.GetFullPointTypeByID(pointTypeID).First();

            //Set Point Type data
            SetPointTypeIntoEditForm(mPointType);

            //Cancels the edit auto grid selected.
            e.Cancel = true;
        }

        protected void gridPointTypes_Deleting(object sender, GridViewDeleteEventArgs e)
        {
            //get deleting rows PointClassid
            int pointTypeID = Convert.ToInt32(gridPointTypes.DataKeys[e.RowIndex].Value);
            int top = 10;

            try
            {
                //Only allow deletion of a point type if no point,vpoint, or vprepoint
                //has been associated.                    

                //check if a point,vpoint,or vprepoint, is associated to that point type
                List<GetPointsData> pts = DataMgr.PointDataMapper.GetTopPointsAssociatedWithPointType(pointTypeID, top);

                if (pts != null)
                {
                    lblErrors.Text = pointExists;

                    lblErrors.Text += " The " + (pts.Count < top ? pts.Count.ToString() : "first " + top.ToString()) + " associated points:<br />";

                    foreach (GetPointsData p in pts)
                    {
                        lblErrors.Text += "<br />" + p.BuildingName + " - " + p.EquipmentName + " - " + p.PointName;
                    }

                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                else if (DataMgr.PointDataMapper.IsVPointAssociatedWithPointType(pointTypeID))
                {
                    lblErrors.Text = vPointExists;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                else if (DataMgr.PointDataMapper.IsVPrePointAssociatedWithPointType(pointTypeID))
                {
                    lblErrors.Text = vPrePointExists;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                else if (DataMgr.AnalysisPointTypeDataMapper.IsAnalysisAssociatedWithInputPointType(pointTypeID))
                {
                    lblErrors.Text = analysisPointExists;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                else if (DataMgr.AnalysisPointTypeDataMapper.IsAnalysisAssociatedWithOutputVPointType(pointTypeID))
                {
                    lblErrors.Text = analysisVPointExists;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                else if (DataMgr.AnalysisPointTypeDataMapper.IsAnalysisAssociatedWithOutputVPrePointType(pointTypeID))
                {
                    lblErrors.Text = analysisVPrePointExists;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                else if (DataMgr.EquipmentTypeDataMapper.IsPointTypeAssociatedWithEquipmentTypes(pointTypeID))
                {
                    lblErrors.Text = equipmentTypeExists;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
                else
                {
                    //delete equipmenttype_pointtype associations, we should be safe because of the pointtype checks prior, meaning no points should have that point type anymore.
                    DataMgr.EquipmentTypeDataMapper.DeleteEquipmentTypePointTypesByPointTypeID(pointTypeID);

                    //delete point type
                    DataMgr.PointTypeDataMapper.DeletePointType(pointTypeID);

                    lblErrors.Text = deleteSuccessful;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();
                }
            }
            catch (Exception ex)
            {
                lblErrors.Text = deleteFailed;
                lblErrors.Visible = true;
                lnkSetFocusView.Focus();

                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting point type.", ex);
            }

            //Bind data again keeping current page and sort mode
            DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryPointTypes());
            gridPointTypes.PageIndex = gridPointTypes.PageIndex;
            gridPointTypes.DataSource = SortDataTable(dataTable as DataTable, true);
            gridPointTypes.DataBind();

            SetGridCountLabel(dataTable.Rows.Count);

            //hide edit form if shown
            pnlEditPointType.Visible = false;
        }

        protected void gridPointTypes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedPointTypes(sessionState["Search"].ToString()));

            //maintain current sort direction and expresstion on paging
            gridPointTypes.DataSource = SortDataTable(dataTable, true);
            gridPointTypes.PageIndex = e.NewPageIndex;
            gridPointTypes.DataBind();
        }

        protected void gridPointTypes_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Reset the edit index.
            gridPointTypes.EditIndex = -1;

            DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedPointTypes(sessionState["Search"].ToString()));

            GridViewSortExpression = e.SortExpression;

            gridPointTypes.DataSource = SortDataTable(dataTable, false);
            gridPointTypes.DataBind();
        }

        #endregion

        #region Helper Methods

        private void ClearCache()
        {
            //TODO: Figure out a way to clear the appropriate bids, and not just visible ones for the client.  There is no clear way to obtain bids, based off of points.
            ClearCacher.ClearCache(siteUser.CID, siteUser.VisibleBuildings.Select(vb => vb.BID), siteUser.UID);
        }

        private IEnumerable<GetPointTypeData> QueryPointTypes()
        {
            try
            {
                //get all point types 
                return DataMgr.PointTypeDataMapper.GetAllFullPointTypes();
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving point types.", sqlEx);
                return null;
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving point types.", ex);
                return null;
            }
        }

        private IEnumerable<GetPointTypeData> QuerySearchedPointTypes(string searchText)
        {
            try
            {
                //get all point types 
                return DataMgr.PointTypeDataMapper.GetAllFullSearchedPointTypes(searchText);
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving point types.", sqlEx);
                return null;
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving point types.", ex);
                return null;
            }
        }

        private void BindPointTypes()
        {
            //query point types
            IEnumerable<GetPointTypeData> types = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryPointTypes() : QuerySearchedPointTypes(txtSearch.Text);

            int count = types.Count();

            gridPointTypes.DataSource = types;

            // bind grid
            gridPointTypes.DataBind();

            SetGridCountLabel(count);
        }

        private void SetGridCountLabel(int count)
        {
            //check if grid empty 
            if (count == 1)
            {
                lblResults.Text = String.Format("{0} point type found.", count);
            }
            else if (count > 1)
            {
                lblResults.Text = String.Format("{0} point types found.", count);
            }
            else
            {
                //gridCompanies.EmptyDataText
                lblResults.Text = "No point types found.";
            }
        }

        /// <summary>
        /// Helper method to switch sort direction
        /// </summary>
        /// <returns>Alternated Sort Direction</returns>
        private string GetSortDirection()
        {
            //switch sorting directions
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    ViewState["SortDirection"] = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    ViewState["SortDirection"] = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        /// <summary>
        /// Sorts the data table based on view state.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="isPageIndexChanging"></param>
        /// <returns>DataView</returns>
        protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
        {
            if (dataTable != null && dataTable.Rows.Count != 0)
            {
                DataView dataView = new DataView(dataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        //maintains the currect viewstate sorting for paging
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        //reverses the sort direction on sort
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                    }
                }
                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        #endregion
    }
}