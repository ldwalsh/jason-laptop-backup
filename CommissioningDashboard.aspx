﻿<%@ Page Language="C#" EnableEventValidation="false" EnableSessionState="true" MasterPageFile="~/_masters/CommissioningDashboard.Master" AutoEventWireup="false" CodeBehind="CommissioningDashboard.aspx.cs" Inherits="CW.Website.CommissioningDashboard" Async="true" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <script type="text/javascript" src="_assets/scripts/CommissioningDashboardProperties.js"></script>
  <script type="text/javascript" src="_assets/scripts/CommissioningDashboardWidgets.js"></script>

  <telerik:RadAjaxManager ID="radAjaxManager" runat="server">  
    <AjaxSettings>
      <telerik:AjaxSetting AjaxControlID="ddlBuildings">
        <UpdatedControls>                    
          <telerik:AjaxUpdatedControl ControlID="pnlFilters" LoadingPanelID="loadingPanel" />
        </UpdatedControls>
      </telerik:AjaxSetting>

      <telerik:AjaxSetting AjaxControlID="ddlEquipmentClass">
        <UpdatedControls>                    
          <telerik:AjaxUpdatedControl ControlID="pnlFilters" LoadingPanelID="loadingPanel" />
        </UpdatedControls>
      </telerik:AjaxSetting>

      <telerik:AjaxSetting AjaxControlID="ddlInterval">
        <UpdatedControls>                    
          <telerik:AjaxUpdatedControl ControlID="pnlFilters" LoadingPanelID="loadingPanel" />
        </UpdatedControls>
      </telerik:AjaxSetting>

      <telerik:AjaxSetting AjaxControlID="imgPdfDownload">
        <UpdatedControls>                    
          <telerik:AjaxUpdatedControl ControlID="pnlFilters" LoadingPanelID="loadingPanel" />
        </UpdatedControls>
      </telerik:AjaxSetting>
    </AjaxSettings>
  </telerik:RadAjaxManager>

  <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server" Skin="Default" />  

  <div id="commissioningDashboard" class="commissioningDashboard">

    <div id="dashboardHeader" class="dashboardHeader">  

      <div class="commissioningModuleIcon"></div>

      <div class="headerTitle">
	    Commissioning Dashboard
	  </div>

      <div class="headerSpacer"></div>

	  <div class="headerClientInfo">

        <div class="headerClientImage">     
          <asp:Image ID="imgHeaderClientImage" CssClass="imgHeader" runat="server" />
        </div>

        <div class="headerClientName">
		  <label id="lblHeaderClientName" runat="server" />
	    </div>

	  </div>

      <div class="headerSpacer"></div>

      <div class="headerContent">
        <asp:Literal ID="litDashboardBody" runat="server" />
      </div>

    </div> <!--end of dashboardHeader div-->
    
    <hr />

    <div id="dashboardBody">

        <div class="dashboardBody dockWrapper">

        <asp:Panel ID="pnlFilters" ValidateRequestMode="Disabled" runat="server">

          <div id="formWrapper" class="divDockFormWrapperShort">

            <div class="divDockForm">
              <label class="labelBold">Building:</label>
              <asp:DropDownList ID="ddlBuildings" CssClass="dropdown" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlBuildings_SelectedIndexChanged" />
              <input type="hidden" id="cid" value="<%= siteUser.CID %>" />
            </div>
  
            <div class="divDockForm">
              <label class="labelBold">Equipment Class:</label>
              <asp:DropDownList ID="ddlEquipmentClass" CssClass="dropdown" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlEquipmentClass_SelectedIndexChanged" />
            </div>

            <div class="divDockForm">
              <label class="labelBold">Equipment:</label>
              <asp:DropDownList ID="ddlEquipment" CssClass="dropdown" runat="server" AppendDataBoundItems="true" />
            </div>

            <div class="divDockForm">
              <label class="labelBold">Interval:</label>
              <asp:DropDownList ID="ddlInterval" CssClass="dropdown" runat="server" AppendDataBoundItems="true" AutoPostBack="true" CausesValidation="false" OnSelectedIndexChanged="ddlInterval_SelectedIndexChanged">
                <asp:ListItem Text="Daily" Value="Daily" Selected="True" />
                <asp:ListItem Text="Weekly" Value="Weekly" />
                <asp:ListItem Text="Monthly" Value="Monthly" />
              </asp:DropDownList>
            </div>

            <div class="divDockForm">
              <label class="labelBold">Range:</label>
              <asp:DropDownList ID="ddlRange" CssClass="dropdown" runat="server" />
            </div>

            <div class="divDockForm">
              <span id="refreshData" class="labelBold">Refresh Data:</span>
              <div id="refreshBtn" class="imgBtnRefreshDiv">
                <img src="_assets/styles/images/refresh.jpg" class="imgBtn" onclick="LoadCharts();" alt="Refresh" />
              </div>
            </div>

            <input type="hidden" runat="server" id="hdn_container" clientidmode="Static" />

            <asp:ImageButton ID="imgPdfDownload" CssClass="imgDashboardDownload" ImageUrl="../../_assets/images/pdf-icon.jpg" OnClientClick="GetHtml('hdn_container', 'dashboardBody');" OnClick="btnDownloadButton_Click" AlternateText="download" runat="server" CausesValidation="false" />

          </div>

        </asp:Panel>
      
        <div class="dashboardLeftColumn">

          <telerik:RadDockLayout ID="radDockLayout1" runat="server">
            <telerik:RadDockZone ID="radDockZone1" runat="server">
           
              <telerik:RadDock ID="radDockBuildingAvoidableEnergyCostsByBuilding" runat="server" Title="Top Building Avoidable Energy Costs">
                <ContentTemplate>
                  <div id="updateProgressBuildingAvoidableEnergyCostsByBuilding" class="updateProgressDivForWidgets"></div>
                  <div id="buildingAvoidableEnergyCostsByBuilding" class="processingWidgetDiv"></div>
                </ContentTemplate>
              </telerik:RadDock>

            </telerik:RadDockZone>
          </telerik:RadDockLayout>

        </div>

        <div class="dashboardRightColumn">

          <telerik:RadDockLayout ID="radDockLayout2" runat="server">
            <telerik:RadDockZone ID="radDockZone2" runat="server">
           
              <telerik:RadDock ID="radDockBuildingFaultsByBuilding" runat="server" Title="Top Building Faults">
                <ContentTemplate>
                  <div id="updateProgressBuildingFaultsByBuilding" class="updateProgressDivForWidgets"></div>
                  <div id="buildingFaultsByBuilding" class="processingWidgetDiv"></div>
                </ContentTemplate>
              </telerik:RadDock>

            </telerik:RadDockZone>
          </telerik:RadDockLayout>

        </div>

        <telerik:RadDockLayout ID="radDockLayout3" runat="server">
          <telerik:RadDockZone ID="radDockZone3" runat="server">
           
            <telerik:RadDock ID="radDockBuildingAvoidableEnergyCosts" runat="server" Title="Avoidable Energy Costs">
              <ContentTemplate>
                <div id="updateProgressBuildingAvoidableEnergyCosts" class="updateProgressDivForWidgets"></div>
                <div id="buildingAvoidableEnergyCosts" class="processingWidgetDiv"></div>
              </ContentTemplate>
            </telerik:RadDock>
                
            <telerik:RadDock ID="radDockBuildingFaults" runat="server" Title="Faults">
              <ContentTemplate>
                <div id="updateProgressBuildingFaults" class="updateProgressDivForWidgets"></div>
                <div id="buildingFaults" class="processingWidgetDiv"></div>
              </ContentTemplate>
            </telerik:RadDock>

            <telerik:RadDock ID="radDockBuildingPriorities" runat="server" Title="Average Priorities">
              <ContentTemplate>
                <div id="updateProgressBuildingPriorities" class="updateProgressDivForWidgets"></div>
                <div id="buildingPriorities" class="processingWidgetDiv"></div>
              </ContentTemplate>
            </telerik:RadDock>

          </telerik:RadDockZone>
        </telerik:RadDockLayout>
      
      </div> <!--end of dashboardBody dockWrapper class div for EO-->
    
    </div> <!--end of dashboardBody div-->

    <div id="dashboardFooter" class="dashboardFooter">             
    </div>

  </div> <!--end of commissioningDashboard div-->

  <ul id="ulContextMenu" class="commissionDashboardPointContextMenu">
    <li>Open in new window</li>
    <li>Open here</li>
  </ul>

</asp:Content>