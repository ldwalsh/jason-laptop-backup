﻿using AjaxControlToolkit;
using CW.Business.Blob.Images;
using CW.Business.Query;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Helpers;
using CW.Data.Models.BuildingVariable;
using CW.Data.Models.Diagnostics;
using CW.Data.Models.EquipmentVariable;
using CW.Reporting._fileservices;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._controls.tasks.Models;
using CW.Website._diagnostics;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using CW.Website.entity;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CW.Website.Diagnostics
{
    public partial class DiagnosticsPage : SiteModulePage
    {
        #region Properties

        private LinkHelper.ViewByMode currentMode;
        private ClientEntity clientEntity;

        private DropDownSequencer dropDownSequencer;
        private CacheManager<IEnumerable<DiagnosticsResult>> cacheManager;

        const string postponeSuccessful = "Scheduled analysis postponement was successful.";
        const string postponeRemoveSuccessful = "Scheduled analysis existing postponement was removed.";
        const string postponeFailed = "Scheduled analysis postponement failed. Please contact an administrator.";
        const string postponeAnalysisNotScheduled = "The analysis is not currently scheduled to run for this equipment, and cannot be postponed.";
        const string equipmentNotesSuccessful = "Equipment notes update was successful.";
        const string equipmentNotesFailed = "Equipment notes update failed. Please contact an administrator.";
        const string equipmentVarsEmpty = "Not applicable.";
        const string buildingVarsEmpty = "Not applicable.";
        const string associatedEquipmentEmpty = "Not applicable.";
        const string associatedEquipmentPointsEmpty = "Not applicable.";
        const string requestSupportSuccessful = "Support request was successful. A support team member has been assigned your request and will respond as soon as possible.";
        const string requestSupportFailed = "Support request failed. Please contact an administrator.";
        const string outOfDate = "Support request failed. The displayed data has changed and it out of date. Please refresh the page and try again.";

        const string requestSupportBodyTop = "User $firstname $lastname has requested support pertaining to the diagnostic below.";
        const string requestSupportSubject = "Support Request for {0}";

        private const string pdfFailed = "Failed to generate pdf report. Please try again later.";

        private const string diagnosticsGridCacheName = "PB_DiagnosticsGridCache";
        private const string CacheDelimiter = "|";
        private Boolean HasTaskModuleAccess { get { return (siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Tasks)); } }

        [ViewStateProperty]
        private bool HasMultipleCurrencies { get; set; }

        [ViewStateProperty]
        private SortDirection GridViewSortDirection
        {
            get;
            set;
        }

        [ViewStateProperty]
        private String gridViewSortExpression
        {
            get;
            set;
        }

        private DiagnosticsBinder.BindingParameters bindingParameters
        {
            get { return viewState.Get<DiagnosticsBinder.BindingParameters>("BindingParameters"); }
            set { viewState.Set("BindingParameters", value); }
        }
        private Int32 ddlColumnIndex { get; set; }

        #endregion

        #region fields

        private ListItem rblBuildingGroupItem = new ListItem("Building Group", "BuildingGroup");
        private Boolean buildingGroupsExist = false;
        private String worksheetName = "Diagnostics";

        #endregion

        #region Page Events

        protected void Page_Load(Object sender, EventArgs e)
        {
            //datamanger in sitepage

            //logged in security check in master page

            dropDownSequencer = new DropDownSequencer(new[] { ddlBuildingGroups, ddlBuildings, ddlEquipmentClasses, ddlEquipment, ddlAnalysis });

            clientEntity = new ClientEntity(siteUser.CID, String.Empty);

            cacheManager =
            new CacheManager<IEnumerable<DiagnosticsResult>>(DataMgr.DefaultCacheRepository, CreateCacheKey, TimeSpan.FromMinutes(ConfigMgr.GetConfigurationSetting("DiagnosticsUserCacheExpirationInMinutes", 20)));

            buildingGroupsExist = siteUser.BuildingGroups.Any();

            ddlColumnIndex = GridViewHelper.GetColumnIndexByHeaderName(gridDiagnostics, "Actions");
        }

        protected void Page_FirstLoad(Object sender, EventArgs e)
        {
            if (buildingGroupsExist)
            {
                BindBuildingGroupDropDownList();
            }

            //no need to set initial gridviewsortexpression as its by the combination of comfort,energy,maintenance,and cost savings. 
            BindBuildingDropdownList();

            if (Request.QueryString.HasKeys())
            {
                ProcessRequestWithQueryString();
            }
            else
            {
                ProcessRequest();
            }

            rblViewBy.SelectedValue = currentMode.ToString();

            VisibilityHelper(currentMode);

            //set admin global settings
            //Make sure to decode from ascii to html 
            GlobalSetting globalSetting = DataMgr.GlobalDataMapper.GetGlobalSettings();
            litDiagnosticsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, globalSetting.DiagnosticsBody);


        }

        protected void Page_AsyncPostbackLoad(Object sender, EventArgs e)
        {
            currentMode = EnumerableHelper.ParseEnum<LinkHelper.ViewByMode>(rblViewBy.SelectedValue);
        }

        #endregion

        #region Load and Bind Fields

        private void BindBuildingGroupDropDownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlBuildingGroups, true, true, false);

            ddlBuildingGroups.DataTextField = "BuildingGroupName";
            ddlBuildingGroups.DataValueField = "BuildingGroupID";

            var buildingGroups = GetBuildingGroups();

            ddlBuildingGroups.DataSource = buildingGroups;
            ddlBuildingGroups.DataBind();
        }

        private void BindBuildingDropdownList()
        {

            //TEMP: remove view all on buildings because we need a buidlings timezone.
            //we would need to pass a list of building instead into the queries and then do a foreach to get each timezone.
            DropDownSequencer.ClearAndCreateDefaultItems(ddlBuildings, true, true, false);

            ddlBuildings.DataTextField = "BuildingName";
            ddlBuildings.DataValueField = "BID";

            //get all buildings by client id. kgs and provider will have selected client.  get all regardless of visible or active, as they once may have been visible or active.                   
            //if session clientid empty then admin
            var buildings = siteUser.VisibleBuildings;

            ddlBuildings.DataSource = buildings;
            ddlBuildings.DataBind();

            //populate address in dropdown items by added a title attribute
            int counter = 2;
            foreach (var b in buildings)
            {
                ddlBuildings.Items[counter].Attributes.Add("title", String.Format("{0}, {1}, {2} {3}", b.Address, b.City, (b.StateID != null ? b.State.StateName : ""), b.Zip));
                counter++;
            }
        }

        private void BindEquipmentClassesDropDownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipmentClasses, true, true, false);

            ddlEquipmentClasses.DataTextField = "EquipmentClassName";
            ddlEquipmentClasses.DataValueField = "EquipmentClassID";

            ddlEquipmentClasses.DataSource =
            (
                (ddlBuildings.SelectedValue == "0") ? DataMgr.EquipmentClassDataMapper.GetAllEquipmentClasses() : DataMgr.EquipmentClassDataMapper.GetAllEquipmentClassesByBuildingID(Int32.Parse(ddlBuildings.SelectedValue))
            );

            ddlEquipmentClasses.DataBind();
        }

        private void BindEquipmentDropdownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlEquipment, true, true, false);

            ddlEquipment.DataTextField = "EquipmentName";
            ddlEquipment.DataValueField = "EID";

            //
            //get all visible equipment, regardless of its active state because we stll need to plot if it was once active.

            var bid = ((ddlBuildings.SelectedValue == "0") ? null : new Int32?(Int32.Parse(ddlBuildings.SelectedValue)));
            var classID = ((ddlEquipmentClasses.SelectedValue == "0") ? null : new Int32?(Int32.Parse(ddlEquipmentClasses.SelectedValue)));

            var equipment = QueryManager.Equipment.LoadWith<EquipmentType>(_ => _.EquipmentType, _ => _.EquipmentClass).FinalizeLoadWith.GetVisibleOnly().ByBuilding(bid).ByClass(classID).ToList();

            ddlEquipment.DataSource = equipment;

            ddlEquipment.DataBind();

            //
            //populate class and type in dropdown items by adding a title attribute

            var counter = 2;

            foreach (var e in equipment)
            {
                ddlEquipment.Items[counter].Attributes.Add("title", String.Format("{0}, {1}", e.EquipmentType.EquipmentClass.EquipmentClassName, e.EquipmentType.EquipmentTypeName));

                counter++;
            }
        }

        private void BindAnalysisDropdownList()
        {
            DropDownSequencer.ClearAndCreateDefaultItems(ddlAnalysis, true, true, false);

            ddlAnalysis.DataTextField = "AnalysisName";
            ddlAnalysis.DataValueField = "AID";

            //
            //get all visible analyses, regardless of its active state because we stll need to plot if it was once active.

            IEnumerable<Analyse> analyses = Enumerable.Empty<Analyse>();

            if (ddlBuildings.SelectedValue == "0" && ddlEquipmentClasses.SelectedValue == "0" && ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByCID(siteUser.CID);
            }
            else if (ddlBuildings.SelectedValue == "0" && ddlEquipmentClasses.SelectedValue != "0" && ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByCIDAndEquipmentClassID(siteUser.CID, Int32.Parse(ddlEquipmentClasses.SelectedValue));
            }
            else if (ddlEquipmentClasses.SelectedValue == "0" && ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByBID(Int32.Parse(ddlBuildings.SelectedValue));
            }
            else if (ddlEquipmentClasses.SelectedValue != "0" && ddlEquipment.SelectedValue == "0")
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByBIDAndEquipmentClassID(Int32.Parse(ddlBuildings.SelectedValue), Int32.Parse(ddlEquipmentClasses.SelectedValue));
            }
            else
            {
                analyses = DataMgr.AnalysisDataMapper.GetAllVisibleAnalysesByEID(Int32.Parse(ddlEquipment.SelectedValue));
            }

            ddlAnalysis.DataSource = analyses;

            ddlAnalysis.DataBind();

            //populate analysis teaser in dropdown items by added a title attribute
            int counter = 2;
            foreach (Analyse a in analyses)
            {
                ddlAnalysis.Items[counter].Attributes.Add("title", a.AnalysisTeaser);
                counter++;
            }
        }

        /// <summary>
        /// Binds equipment variables repeater list by eid           
        /// </summary>
        /// <param name="eid"></param>
        private void BindEquipmentVariables(Repeater rpt, Label lbl, int[] eids, List<EquipmentVariable> analysisEquipmentVars)
        {
            //get equipment variables associated to equipment, fiter down by analysis 
            IEnumerable<GetEquipmentEquipmentVariableData> mEquipmentVariables = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesWithDataByEIDs(eids)
                                                                                        .Where(ev => analysisEquipmentVars.Select(aev => aev.EVID).Contains(ev.EVID));
            if (mEquipmentVariables.Any())
            {
                //bind equipment variables to repeater
                rpt.DataSource = mEquipmentVariables;
                rpt.DataBind();
                rpt.Visible = true;

                lbl.Visible = false;
            }
            else
            {
                rpt.Visible = false;
                lbl.Text = equipmentVarsEmpty;
                lbl.Visible = true;
            }
        }

        /// <summary>
        /// Binds buildings variables repeater list by bid           
        /// </summary>
        /// <param name="eid"></param>
        private void BindBuildingVariables(Repeater rpt, Label lbl, int bid, List<BuildingVariable> analysisBuildingVars)
        {
            //get building variables associated to building, fiter down by analysis
            IEnumerable<GetBuildingBuildingVariableData> mBuildingVariables = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesWithDataByBID(bid)
                                                                                   .Where(bv => analysisBuildingVars.Select(abv => abv.BVID).Contains(bv.BVID));

            if (mBuildingVariables.Any())
            {
                //bind building variables to repeater
                rpt.DataSource = mBuildingVariables;
                rpt.DataBind();
                rpt.Visible = true;

                lbl.Visible = false;
            }
            else
            {
                rpt.Visible = false;
                lbl.Text = buildingVarsEmpty;
                lbl.Visible = true;
            }
        }

        #endregion

        #region Button Click Events

        protected void generateButton_Click(Object sender, EventArgs e)
        {
            //clear existing gridviewsortexpression as its by the combination of comfort,energy,maintenance,and cost savings. 
            gridViewSortExpression = String.Empty;

            if (!String.IsNullOrWhiteSpace(txtTrackingCode.Text)) ParseTrackingCodeAndSetValues();

            var analysisRange = EnumHelper.Parse<DataConstants.AnalysisRange>(rblRange.SelectedValue);
            var bids = DiagnosticsBinder.BindingParameters.ToInt(ddlBuildings.SelectedValue) != null ? new List<int>(new int[] { Convert.ToInt32(ddlBuildings.SelectedValue) }) : siteUser.VisibleBuildings.Select(b => b.BID).ToList();

            if (EnumerableHelper.ParseEnum<LinkHelper.ViewByMode>(rblViewBy.SelectedValue) == LinkHelper.ViewByMode.BuildingGroup)
            {
                //TODO, eventually set db.BuildingGroups_Buildings to cache to avoid db call below, jp
                using (var db = new CWDataContext(DataMgr.ConnectionString))
                {
                    var buildingGroups = GetBuildingGroups(ddlBuildingGroups.SelectedValue);
                    var buildingGroupBuildings = buildingGroups.Join(db.BuildingGroups_Buildings, bg => bg.BuildingGroupID, bgb => bgb.BuildingGroupID, (bg, bgb) => bgb).Select(bgb => bgb);

                    bids = buildingGroupBuildings.Join(siteUser.VisibleBuildings, bgb => bgb.BID, vb => vb.BID, (bgb, vb) => vb).Select(vb => vb.BID).ToList();
                }
            }

            bindingParameters =
            new DiagnosticsBinder.BindingParameters
            {
                cid = clientEntity.ID,
                bids = bids,
                //buildingID = DiagnosticsBinder.BindingParameters.ToInt(ddlBuildings.SelectedValue),
                equipmentClassID = DiagnosticsBinder.BindingParameters.ToInt(ddlEquipmentClasses.SelectedValue),
                equipmentID = DiagnosticsBinder.BindingParameters.ToInt(ddlEquipment.SelectedValue),
                analysisID = DiagnosticsBinder.BindingParameters.ToInt(ddlAnalysis.SelectedValue),

                start = Convert.ToDateTime(txtStartDate.SelectedDate),
                end = Convert.ToDateTime(txtEndDate.SelectedDate),

                range = analysisRange,

                notesFilter = txtNotesFilter.Text,
            };

            Generate();
        }

        protected void emailButton_Click(Object sender, EventArgs e)
        {
            ClearMessaging();

            var analysisPDFReport = CreateAnalysisPDFReportDTO();

            var reportService = ReportFileGenerator.Create(siteUser.IsSchneiderTheme, siteUser.FullName, LogMgr, analysisPDFReport, null, null, null, null, null, null);

            if (reportService == null)
            {
                DisplayErrorOutput(pdfFailed);

                return;
            }

            Boolean result;

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    reportService,
                    LogMgr
                ).SendInEmail(Email.GenerateReportSubject(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product Analysis Report for "), analysisPDFReport, null, null, null), Email.GenerateReportBody(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product Analysis Report for "), analysisPDFReport, null, null, null));
            }

            catch (FilePublishService.MonthlyReportEmailAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot email report, max monthly email limit has been reached.");

                return;
            }

            DisplayErrorOutput
            (
                result ?
                    "Analysis report was emailed successfully." :
                    "Analysis report email failed. Please try again later."
            );
        }

        protected void downloadButton_Click(Object sender, EventArgs e)
        {
            ClearMessaging();

            var reportService = ReportFileGenerator.Create(siteUser.IsSchneiderTheme, siteUser.FullName, LogMgr, CreateAnalysisPDFReportDTO(), null, null, null, null, null, null);

            if (reportService == null)
            {
                DisplayErrorOutput(pdfFailed);

                return;
            }

            Boolean result;

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    reportService,
                    LogMgr
                ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, String.Format("$product_{0}_{1}_{2}_{3}_Report.pdf", lblBuildingName.Text, lblEquipmentName.Text, lblAnalysisName.Text, StringHelper.RemoveSpecialCharactersInDate(lblStartDate.Text)).Replace(" ", ""), true));
            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                return;
            }

            if (result) return;

            DisplayErrorOutput("An error occured attempting to download. Please try again later.");
        }

        protected void downloadExcelFullButton_Click(Object sender, EventArgs e)
        {
            ClearMessaging();

            Boolean result;

            int originalPageIndex = gridDiagnostics.PageIndex;
            List<DiagnosticsResult> list = CallCacheManager().ToList();
            int downloadRowQuota = ConfigMgr.GetConfigurationSetting("DownloadRowQuote", 100000);
            bool isDownloadRowQuoteExceeded = list.Count > downloadRowQuota;

            if (isDownloadRowQuoteExceeded)
            {
                list = list.Take(downloadRowQuota).ToList();
                DisplayErrorOutput(String.Format("Exceeded maximum row download, only first {0} rows have been included.", downloadRowQuota), lblErrorTop);
            }

            //join with equipment notes
            list = DataMgr.EquipmentDataMapper.GetEquipmentNotes(list);

            gridDiagnostics.AllowPaging = false;

            gridDiagnostics.DataSource = String.IsNullOrWhiteSpace(gridViewSortExpression) ? list : EnumerableHelper.CreateSortedEnumerable(list, gridViewSortExpression, GridViewSortDirection);
            gridDiagnostics.DataBind();

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    new DiagnosticsXLSFileGenerator(gridDiagnostics, false, new int[] { (HasTaskModuleAccess) ? 12 : 11 }, true, siteUser.IsSchneiderTheme, worksheetName),
                    LogMgr
                ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productDiagnostics.xls", true));
            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot download grid, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.", lblErrorTop);

                return;
            }

            if (isDownloadRowQuoteExceeded)
            {
                gridDiagnostics.DataSource = String.IsNullOrWhiteSpace(gridViewSortExpression) ? CallCacheManager() : EnumerableHelper.CreateSortedEnumerable(CallCacheManager(), gridViewSortExpression, GridViewSortDirection);
            }

            gridDiagnostics.AllowPaging = true;
            gridDiagnostics.DataBind();

            if (result) return;

            DisplayErrorOutput("An error occured attempting to download. Please try again later.", lblErrorTop);
        }

        protected void downloadExcelCurrentPageButton_Click(Object sender, EventArgs e)
        {
            ClearMessaging();

            Boolean result;

            List<DiagnosticsResult> list = CallCacheManager().ToList();

            //join with equipment notes
            list = DataMgr.EquipmentDataMapper.GetEquipmentNotes(list);

            gridDiagnostics.DataSource = String.IsNullOrWhiteSpace(gridViewSortExpression) ? list : EnumerableHelper.CreateSortedEnumerable(list, gridViewSortExpression, GridViewSortDirection);
            gridDiagnostics.DataBind();

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    new DiagnosticsXLSFileGenerator(gridDiagnostics, true, new int[] { (HasTaskModuleAccess) ? 12 : 11 }, true, siteUser.IsSchneiderTheme, worksheetName),
                    LogMgr
                ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$productDiagnostics.xls", true));
            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot download grid, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.", lblErrorTop);

                return;
            }

            BindGrid(CallCacheManager());

            if (result) return;

            DisplayErrorOutput("An error occured attempting to download. Please try again later.", lblErrorTop);

        }

        protected void postponeButton_Click(Object sender, EventArgs e)
        {
            ClearMessaging();

            DateTime? postponeDate = txtPostponeDate.SelectedDate;

            //bid, eid, aid
            string[] values = hdnPostponeValues.Value.ToString().Split(',');

            //try to postpone the scheduled analysis              
            try
            {
                if (DataMgr.ScheduledAnalysisDataMapper.DoesScheduledAnalysisExist(Convert.ToInt32(values[1]), Convert.ToInt32(values[2])))
                {
                    //update scheduled analysis with new postpone date
                    DataMgr.ScheduledAnalysisDataMapper.PostponeScheduledAnalysis(Convert.ToInt32(values[0]), Convert.ToInt32(values[1]), Convert.ToInt32(values[2]), postponeDate);

                    lblPostponeError.Text = postponeDate == null ? postponeRemoveSuccessful : postponeSuccessful;
                    lblPostponeError.Visible = true;
                }
                else
                {
                    lblPostponeError.Text = postponeAnalysisNotScheduled;
                    lblPostponeError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblPostponeError.Text = postponeFailed;
                lblPostponeError.Visible = true;

                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error postponing scheduled analysis on diagnostics.", ex);
            }

            pnlPostpone.Style.Value = "display:block;";
            mpePostpone.Show();
        }

        protected void equipmentNotesButton_Click(Object sender, EventArgs e)
        {
            ClearMessaging();

            //Make sure to html encode before putting in the database. From html to ascii.
            //equipment notes
            string equipmentNotes = StringHelper.ClearRadEditorFirefoxBR(editorEquipmentNotesNotes.Content);

            //try to update the equipment equipment notes              
            try
            {
                //update equipment equipment notes
                DataMgr.EquipmentDataMapper.UpdateEquipmentNotes(Convert.ToInt32(hdnEquipmentNotesValue.Value), equipmentNotes);

                lblEquipmentNotesError.Text = equipmentNotesSuccessful;
                lblEquipmentNotesError.Visible = true;
            }
            catch (Exception ex)
            {
                lblEquipmentNotesError.Text = equipmentNotesFailed;
                lblEquipmentNotesError.Visible = true;

                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment notes on diagnostics.", ex);
            }

            pnlEquipmentNotes.Style.Value = "display:block;";
            mpeEquipmentNotes.Show();
        }

        protected void requestSupportButton_Click(Object sender, EventArgs e)
        {
            ClearMessaging();

            //values: analysis links which includes the item index, we can remove this item before sending the email, one with invariant dates and one without
            var reportInaccuracyInvariantValues = new QueryString(hdnRequestSupportInvariantValues.Value);
            var reportInaccuracyValues = new QueryString(hdnRequestSupportValues.Value);

            int gridIndex = Convert.ToInt32(reportInaccuracyInvariantValues.Get("itemIndex", typeof(String))) - (gridDiagnostics.PageIndex * gridDiagnostics.PageSize);

            if (siteUser.CID.ToString() != gridDiagnostics.DataKeys[gridIndex].Values["CID"].ToString())
            {
                lblRequestSupportError.Text = outOfDate + "</br></br>";
                lblRequestSupportError.Visible = true;

                pnlRequestSupport.Style.Value = "display:block;";
                mpeRequestSupport.Show();

                return;
            }

            string client = siteUser.ClientName;
            string building = ((HtmlGenericControl)gridDiagnostics.Rows[gridIndex].FindControl("buildingName")).InnerText;
            string equipment = ((HtmlGenericControl)gridDiagnostics.Rows[gridIndex].FindControl("equipmentName")).InnerText;
            string analysis = ((HtmlGenericControl)gridDiagnostics.Rows[gridIndex].FindControl("analysisName")).InnerText;
            string usersDate = ((HtmlGenericControl)gridDiagnostics.Rows[gridIndex].FindControl("startDate")).InnerText;

            string reportInaccuracyBodyMiddle = "Client: " + client + "<br>";
            string extendedSubjectInfo = String.Format("{0} {1} {2} on {3} regarding {4}", client, building, equipment, usersDate, StringHelper.TrimText(StringHelper.RemoveCarraigeReturns(editorRequestSupportMessage.Text), 50));

            DateTime queryStringStartDate = reportInaccuracyInvariantValues.GetDate("sd", true) ?? DateTime.MinValue;
            string invariantStartDate = queryStringStartDate == DateTime.MinValue ? "N/A" : queryStringStartDate.ToString(Common.Constants.AzureConstants.DATETIME_INVARIANT_DAY_FORMAT);
           
            reportInaccuracyBodyMiddle += "Building: " + building + "<br />";
            reportInaccuracyBodyMiddle += "Equipment: " + equipment + "<br />";
            reportInaccuracyBodyMiddle += "Analysis: " + analysis + "<br />";
            reportInaccuracyBodyMiddle += "Start Date: " + invariantStartDate + "<br />";
            reportInaccuracyBodyMiddle += "Start Date (Reporting Users Culture): " + usersDate + "<br />";
            reportInaccuracyBodyMiddle += "Interval: " + reportInaccuracyInvariantValues.Get("rng", typeof(String));

            //remove the itemIndex before sending the email, itemIndex qs value is not necessary
            reportInaccuracyInvariantValues.Remove("itemIndex");

            string overrideSupportEmail  = DataMgr.ClientDataMapper.GetSupportOverrideEmailIfExists(siteUser.CID, Convert.ToInt32(gridDiagnostics.DataKeys[gridIndex].Values["BID"]));

            SendInaccuracyReportEmail(reportInaccuracyInvariantValues, reportInaccuracyValues, reportInaccuracyBodyMiddle, extendedSubjectInfo, overrideSupportEmail);
        }

        protected void openEquipmentNotesButton_Click(Object sender, EventArgs e)
        {
            hdnEquipmentNotesValue.Value = hdnEID.Value;
            lblEquipmentNotesError.Visible = false;
            //get equipment notes from sql not the cache because it is user editable
            editorEquipmentNotesNotes.Content = DataMgr.EquipmentDataMapper.GetEquipmentNotesByEID(Convert.ToInt32(hdnEID.Value));
            pnlEquipmentNotes.Style.Value = "display:block;";
            mpeEquipmentNotes.Show();
        }

        protected void openRequestSupportButton_Click(Object sender, EventArgs e)
        {
            //TODO:  this whole function assumes the grid doesnt change. fix later to be off analysisinfo content rather then grid values.
                                                                                                        
            GridViewRow row = gridDiagnostics.Rows[Convert.ToInt32(hdnAnalysisInfoTempGridRowIndex.Value)];

            var diagnosticsQuickLinks = ((HiddenField)row.FindControl("diagnosticsQuickLinks")).Value.Split('|');

            hdnRequestSupportInvariantValues.Value = diagnosticsQuickLinks[0] + "&itemIndex=" + row.DataItemIndex;
            hdnRequestSupportValues.Value = diagnosticsQuickLinks[1];

            lblRequestSupportError.Visible = false;
            editorRequestSupportMessage.Content = String.Empty;
            pnlRequestSupport.Style.Value = "display:block;";
            mpeRequestSupport.Show();
        }

        #endregion

        #region Dropdown and Checkbox Events

        protected void ddlBuildings_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequencer.ResetSubDropDownsOf(ddlBuildings);
            ResetTrackingCode();

            if (ddlBuildings.SelectedIndex == -1) return;

            BindEquipmentClassesDropDownList();
        }

        protected void ddlEquipmentClasses_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequencer.ResetSubDropDownsOf(ddlEquipmentClasses);
            ResetTrackingCode();

            if (ddlEquipmentClasses.SelectedIndex == -1) return;

            BindEquipmentDropdownList();
        }

        protected void ddlEquipment_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            dropDownSequencer.ResetSubDropDownsOf(ddlEquipment);
            ResetTrackingCode();

            if (ddlEquipment.SelectedIndex == -1) return;

            BindAnalysisDropdownList();
        }

        protected void rblRange_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            DateTime today;
            DateTimeHelper.GenerateDefaultDateCurrentDay(out today, siteUser.UserTimeZoneOffset);

            switch (EnumHelper.Parse<DataConstants.AnalysisRange>(rblRange.SelectedValue))
            {
                case DataConstants.AnalysisRange.Weekly:

                    DateTime newWeekEndDate = DateTimeHelper.GetLastSunday(DateTimeHelper.GetXDaysAgo(today, 1));
                    DateTime newWeekStartDate = DateTimeHelper.GetXWeeksAgo(newWeekEndDate, 1);

                    if (txtStartDate.SelectedDate < newWeekEndDate) break;

                    txtEndDate.SelectedDate = DateTimeHelper.GetXDaysAgo(newWeekEndDate, 1);
                    txtStartDate.SelectedDate = newWeekStartDate;
                    break;
                case DataConstants.AnalysisRange.Monthly:

                    DateTime newMonthEndDate = DateTimeHelper.GetLastOfMonth(DateTimeHelper.GetXMonthsAgo(today, 1));
                    DateTime newMonthStartDate = DateTimeHelper.GetFirstOfMonth(newMonthEndDate);

                    if (txtStartDate.SelectedDate < newMonthEndDate) break;

                    txtEndDate.SelectedDate = newMonthEndDate;
                    txtStartDate.SelectedDate = newMonthStartDate;
                    break;
            }
        }

        protected void rblViewBy_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            ResetTrackingCode();

            VisibilityHelper(EnumerableHelper.ParseEnum<LinkHelper.ViewByMode>(rblViewBy.SelectedValue));
        }

        protected void ddlActions_OnSelectedIndexChanged(Object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;

            //get row
            GridViewRow row = (GridViewRow)ddl.NamingContainer;

            //if the on row command is executed by the paging links, then return
            if (row.DataItemIndex < 0) return;

            int index = row.DataItemIndex >= gridDiagnostics.PageSize ? row.DataItemIndex % gridDiagnostics.PageSize : row.DataItemIndex;

            //get the datakey
            IOrderedDictionary dka = gridDiagnostics.DataKeys[index].Values;

            switch (ddl.SelectedValue.ToUpper())
            {
                case "POSTPONE":
                    hdnPostponeValues.Value = dka["BID"] + "," + dka["EID"] + "," + dka["AID"];
                    lblPostponeError.Visible = false;

                    //scheduled analysis might not exist if its not scheduled, or postponedate might be null .
                    ScheduledAnalyse sa = DataMgr.ScheduledAnalysisDataMapper.GetScheduledAnalysisByAIDAndEID(Convert.ToInt32(dka["AID"]), Convert.ToInt32(dka["EID"]));

                    txtPostponeDate.SelectedDate = sa != null && sa.RunPostponeDate != null ? sa.RunPostponeDate : null;
                    pnlPostpone.Style.Value = "display:block;";
                    mpePostpone.Show();
                    break;
                case "EQUIPMENTNOTES":
                    hdnEquipmentNotesValue.Value = dka["EID"].ToString();
                    lblEquipmentNotesError.Visible = false;
                    //get equipment notes from sql not the cache because it is user editable
                    editorEquipmentNotesNotes.Content = DataMgr.EquipmentDataMapper.GetEquipmentNotesByEID(Convert.ToInt32(dka["EID"]));
                    pnlEquipmentNotes.Style.Value = "display:block;";
                    mpeEquipmentNotes.Show();
                    break;
                case "SUPPORT":
                    var diagnosticsQuickLinks = ((HiddenField)row.FindControl("diagnosticsQuickLinks")).Value.Split('|');

                    hdnRequestSupportInvariantValues.Value = diagnosticsQuickLinks[0] + "&itemIndex=" + row.DataItemIndex;
                    hdnRequestSupportValues.Value = diagnosticsQuickLinks[1];

                    lblRequestSupportError.Visible = false;
                    editorRequestSupportMessage.Content = String.Empty;
                    pnlRequestSupport.Style.Value = "display:block;";
                    mpeRequestSupport.Show();
                    break;
                case "RAWDATA":
                    var eid = Convert.ToInt32(dka["EID"]);

                    var classID = Int32.Parse(ddlEquipmentClasses.SelectedValue);
                    classID = ((classID < 1) ? DataMgr.EquipmentClassDataMapper.GetEquipmentClassIDByEID(eid) : classID);

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "newWindow", "window.open('" + LinkHelper.BuildAnalysisBuilderQuickLink(Convert.ToInt32(dka["BID"]), classID, eid) + "')", true);
                    break;
                case "VARIABLES":
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "newWindow", "window.open('EquipmentAdministration?tab=1&bid=" + dka["BID"] + "&eid=" + dka["EID"] + "')", true);
                    break;
                case "CREATEPROJECT":
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "newWindow", "window.open('Projects.aspx?tab=2&sfd=" + dka["StartDate"] + "&cid=" + dka["CID"] + "&bid=" + dka["BID"] + "&ecid=0&eid=" + dka["EID"] + "&aid=" + dka["AID"] + "&LCID=" + dka["LCID"] + "')", true);
                    break;
                case "EXISTINGPROJECT":
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "newWindow", "window.open('Projects.aspx')", true);
                    break;
                case "CREATETASK":
                    var equipmentId = int.Parse(dka["EID"].ToString());

                    var analysisId = int.Parse(dka["AID"].ToString());

                    var analysisEquipment = DataMgr.AnalysisEquipmentDataMapper.GetAnalysisToEquipmentByEIDAndAID(equipmentId, analysisId);

                    //this check is to resolve issues on QA for bugs such as:
                    //WEB-3421: Tasks | Diagnostics page hangs when creating a task for a specific diagnostic result
                    //this should NEVER happen in production, but putting this fail safe in regardless
                    if (analysisEquipment != null)
                    {
                        var formFields = new ViewModelTaskForm
                        {
                            Cid = int.Parse(dka["CID"].ToString()),
                            Aeid = analysisEquipment.AEID,
                            AnalysisStartDate = DateTime.Parse(dka["StartDate"].ToString()),
                            AnalysisRange = ((HtmlGenericControl)row.FindControl(PropHelper.G<DiagnosticsResult>(_ => _.AnalysisRange))).InnerText,
                            Bid = analysisEquipment.Equipment.Building.BID,
                            BuildingName = analysisEquipment.Equipment.Building.BuildingName,
                            Eid = analysisEquipment.EID,
                            EquipmentName = analysisEquipment.Equipment.EquipmentName,
                            Aid = analysisEquipment.AID,
                            AnalysisName = analysisEquipment.Analyse.AnalysisName,
                            NotesSummary = ((HtmlGenericControl)row.FindControl(PropHelper.G<DiagnosticsResult>(_ => _.NotesSummary))).InnerText,
                            CMMSReferenceId = analysisEquipment.Equipment.CMMSReferenceID,
                            FormType = "Add"
                        };

                        //TaskAddFormModal.FindControl<HiddenField>("hdnDiagnosticsQuickLink").Value = ((HiddenField)row.FindControl("diagnosticsQuickLinks")).Value.Split('|')[0];

                        TaskAddFormModal.InitializeForm(formFields);

                        TaskAddFormModal.FindControl<ModalPopupExtender>("createTaskMPE").Show();
                    }

                    break;
            }

            ddl.ClearSelection();
        }

        #endregion

        #region Grid Events

        protected void gridDiagnostics_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ClearMessaging();

            GridViewRow row = gridDiagnostics.SelectedRow;
            hdnAnalysisInfoTempGridRowIndex.Value = row.DataItemIndex.ToString();

            IOrderedDictionary dka = gridDiagnostics.DataKeys[gridDiagnostics.SelectedIndex].Values;

            PopulateAnalysisInfo(Convert.ToInt32(dka["CID"]), Convert.ToInt32(dka["BID"]), Convert.ToInt32(dka["EID"]), Convert.ToInt32(dka["AID"]), DateTime.Parse(dka["StartDate"].ToString()));
        }

        protected void gridDiagnostics_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                foreach (TableCell cell in e.Row.Cells)
                {
                    foreach (Control ctl in cell.Controls)
                    {
                        if (ctl.GetType().ToString().Contains("DataControlLinkButton"))
                        {
                            switch (((LinkButton)ctl).Text)
                            {
                                case "Cost":
                                    cell.Attributes.Add("title", "Cost Savings");
                                    break;
                                case "C":
                                    cell.Attributes.Add("title", "Comfort Priority (0 - 10)");
                                    break;
                                case "E":
                                    cell.Attributes.Add("title", "Energy Priority (0 - 10)");
                                    break;
                                case "M":
                                    cell.Attributes.Add("title", "Maintenance Priority (0 - 10)");
                                    break;
                            }
                        }
                    }
                }
            }
        }

        protected void gridDiagnostics_OnDataBound(object sender, EventArgs e)
        {
            GridViewRowCollection rows = gridDiagnostics.Rows;

            //build action list
            ListItem[] actionList = BuildActionList().Cast<ListItem>().ToArray();
            
            foreach (GridViewRow row in rows)
            {
                if ((row.RowState & DataControlRowState.Edit) == 0)
                {
                    var defaultCostValue = (HtmlGenericControl)row.Cells[7].FindControl("defaultCostValue");
                    var costvalueWithISO = (HtmlGenericControl)row.Cells[7].FindControl("costvalueWithISO");

                    defaultCostValue.Attributes.Add("class", $"cost {((HasMultipleCurrencies) ? "hideInDownload" : "showInDownload")}");
                    costvalueWithISO.Attributes.Add("class", (HasMultipleCurrencies) ? "showInDownload" : "hideInDownload");
                    
                    try
                    {
                        row.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.outline='2px solid #999';";
                        row.Attributes["onmouseout"] = "this.style.cursor='pointer';this.style.outline='0 none';";

                        //set all cells except actions and tasks to have onclicks. 
                        //we dont do it on the row anymore because the buttons in the last cells caused double click events, errors, aborts, and more transactions.                         
                        int counter = row.Cells.Count;
                        foreach (TableCell tc in row.Cells)
                        {
                            if (counter != 1 && counter != 7) tc.Attributes["onclick"] = this.Page.ClientScript.GetPostBackClientHyperlink(this.gridDiagnostics, "Select$" + row.RowIndex);
                            counter--;
                        }
                    }
                    catch
                    {
                    }

                    try
                    {
                        DropDownList ddl = ((DropDownList)row.Cells[ddlColumnIndex].Controls[1]);

                        ddl.Items.AddRange(actionList);

                        ddl.DataBind();
                    }
                    catch
                    {
                    }
                }
            }
        }

        protected void gridDiagnostics_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            // no-op. required to be declared
        }

        protected void gridDiagnostics_OnRowCreated(object sender, GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    {
                        var columnIndex = ControlHelper.GridViewControl.GetSortColumnIndex(gridDiagnostics, gridViewSortExpression);

                        if (columnIndex != -1)
                        {
                            e.Row.Cells[columnIndex].Attributes.Add("class", String.Concat("gridSort", GridViewSortDirection.ToString()));
                        }

                        break;
                    }
                case DataControlRowType.Pager:
                    {
                        //Note: The autogenerated pager table is not shown with just one page.

                        Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                        tblPager.CssClass = "pageTable";

                        TableRow theRow = tblPager.Rows[0];

                        LinkButton ctrlPrevious = new LinkButton();
                        ctrlPrevious.CommandArgument = "Prev";
                        ctrlPrevious.CommandName = "Page";
                        ctrlPrevious.Text = "« Previous Page";
                        ctrlPrevious.CssClass = "pageLink";

                        TableCell cellPreviousPage = new TableCell();
                        cellPreviousPage.Controls.Add(ctrlPrevious);
                        tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                        LinkButton ctrlNext = new LinkButton();
                        ctrlNext.CommandArgument = "Next";
                        ctrlNext.CommandName = "Page";
                        ctrlNext.Text = "Next Page »";
                        ctrlNext.CssClass = "pageLink";


                        TableCell cellNextPage = new TableCell();
                        cellNextPage.Controls.Add(ctrlNext);
                        tblPager.Rows[0].Cells.Add(cellNextPage);

                        break;
                    }
            }
        }

        protected void gridDiagnostics_PageIndexChanging(Object sender, GridViewPageEventArgs e)
        {
            gridDiagnostics.PageIndex = e.NewPageIndex;
            BindGrid(CallCacheManager());

            //analysis info box links are grid based, so we need to do this on sorting and paging
            ControlHelper.HideControls(new Control[] { analysisDetailsTop, analysisDetailsBottom, divDownloadBox, lblErrorTop });
        }

        protected void gridDiagnostics_Sorting(Object sender, GridViewSortEventArgs e)
        {
            if (gridViewSortExpression == e.SortExpression)
            {
                GridViewSortDirection = ((GridViewSortDirection == SortDirection.Ascending) ? SortDirection.Descending : SortDirection.Ascending);
            }
            else
            {
                gridViewSortExpression = e.SortExpression;
                GridViewSortDirection = SortDirection.Descending;

                if (typeof(DiagnosticsResult).GetProperty(gridViewSortExpression).PropertyType == typeof(String))
                {
                    GridViewSortDirection = SortDirection.Ascending;
                }
            }

            gridDiagnostics.DataSource = EnumerableHelper.CreateSortedEnumerable(CallCacheManager(), e.SortExpression, GridViewSortDirection);

            gridDiagnostics.DataBind();

            //analysis info box links are grid based, so we need to do this on sorting and paging
            ControlHelper.HideControls(new Control[] { analysisDetailsTop, analysisDetailsBottom, divDownloadBox, lblErrorTop });
        }

        #endregion

        #region Grid Helper Methods

        private IEnumerable<DiagnosticsResult> CallCacheManager()
        {
            var results = DataMgr.DiagnosticsDataMapper.ApplyNotesFilters(cacheManager.Retrieve(BuildDiagnostics), bindingParameters.notesFilter);

            if (HasTaskModuleAccess) results = DataMgr.DiagnosticsDataMapper.AttachTaskCounts(results, siteUser.CID);

            return results == null ? Enumerable.Empty<DiagnosticsResult>() : results.ToList();
        }

        private void Generate()
        {
            gridDiagnostics.PageIndex = 0;

            var results = CallCacheManager();

            if (HasTaskModuleAccess) GridViewHelper.GetColumnByHeaderName(gridDiagnostics, "Tasks").Visible = true;

            HasMultipleCurrencies = (results.GroupBy(_ => _.LCID).Count() > 1);

            gridDiagnostics.DataSource = results;
            gridDiagnostics.DataBind();

            SetResultsLabel(results);

            ControlHelper.HideControls(new Control[] { analysisDetailsTop, analysisDetailsBottom, divDownloadBox, lblErrorTop });
        }

        private void BindGrid(IEnumerable<DiagnosticsResult> list)
        {
            gridDiagnostics.DataSource = String.IsNullOrWhiteSpace(gridViewSortExpression) ? list : EnumerableHelper.CreateSortedEnumerable(list, gridViewSortExpression, GridViewSortDirection);
            gridDiagnostics.DataBind();
        }

        private IEnumerable<DiagnosticsResult> BuildDiagnostics()
        {
            switch (currentMode)
            {
                case LinkHelper.ViewByMode.Building:
                    {
                        bindingParameters.equipmentClassID = null;

                        goto case LinkHelper.ViewByMode.EquipmentClass;
                    }
                case LinkHelper.ViewByMode.EquipmentClass:
                    {
                        bindingParameters.equipmentID = null;

                        goto case LinkHelper.ViewByMode.Equipment;
                    }
                case LinkHelper.ViewByMode.Equipment:
                    {
                        bindingParameters.analysisID = null;

                        break;
                    }
            }

            var pb = new DiagnosticsBinder(siteUser, DataMgr);

            var result = (ddlPrioritiesCount.SelectedValue.ToUpper() != "ALL" ?
                pb.BuildTopDiagnosticsResults(bindingParameters, Convert.ToInt16(ddlPrioritiesCount.SelectedValue)) :
                pb.BuildDiagnosticsResults(bindingParameters));

            return result;
        }

        protected void PopulateAnalysisInfo(int cid, int bid, int eid, int aid, DateTime startDate, bool setFocus = true)
        {
            try
            {
                //get based on submitted range, not by selected at the moment
                var analysisRange = bindingParameters.range;

                //get analysis
                IEnumerable<DiagnosticsResult> diagnosticsResults = DataMgr.DiagnosticsDataMapper.GetDiagnosticsResults(
                    new DiagnosticsResultsInputs()
                    {
                        AID = aid,
                        BID = bid,
                        CID = cid,
                        EID = eid,
                        AnalysisRange = analysisRange,
                        StartDate = startDate,
                        EndDate = startDate
                    });

                DiagnosticsResult diagnosticsResult = diagnosticsResults.FirstOrDefault();

                if (diagnosticsResult != null)
                {
                    //Query the url of the graphic

                    if (!String.IsNullOrEmpty(diagnosticsResult.FigureBlobExt))
                    {
                        imgGraph.Visible = true;
                        imgGraph.Src = HandlerHelper.FigureImageUrl(cid, eid, aid, startDate, analysisRange, diagnosticsResult.FigureBlobExt);

                        if (String.IsNullOrWhiteSpace(imgGraph.Src))
                        {
                            imgGraph.Visible = false;
                            litGraphNotAvailable.Text = "Graph not available.";
                        }
                        else
                        {
                            litGraphNotAvailable.Text = "";
                        }

                        var figure = new FigureImage(analysisRange, diagnosticsResult, DataMgr.OrgBasedBlobStorageProvider).Retrieve();

                        if (figure != null)
                            DataMgr.DefaultCacheRepository.Upsert(GetImageKey(), figure);
                    }
                    else
                    {
                        imgGraph.Visible = false;
                        litGraphNotAvailable.Text = "Graph not available.";
                    }

                    lblClientName.Text = diagnosticsResult.ClientName;
                    hdnCID.Value = cid.ToString();

                    lblBuildingName.Text = diagnosticsResult.BuildingName;
                    lblEquipmentName.Text = diagnosticsResult.EquipmentName;

                    IEnumerable<Equipment> associatedEquipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentAssociatedByPEID(eid, false);
                    lblAssociatedEquipment.Text = associatedEquipment.Any() ? StringHelper.ParseStringArrayToCSVString(associatedEquipment.Select(ae => ae.EquipmentName).ToArray(), true) : associatedEquipmentEmpty;

                    //used to get extra equipment info for the report on downloadclick
                    hdnEID.Value = eid.ToString();

                    lblAnalysisName.Text = diagnosticsResult.AnalysisName;
                    lblStartDate.Text = diagnosticsResult.StartDate.ToShortDateString();
                    lblDisplayInterval.Text = analysisRange.ToString();
                    lblCost.Text = CultureHelper.FormatCurrencyAsString(diagnosticsResult.LCID, diagnosticsResult.CostSavings);
                    lblComfort.Text = diagnosticsResult.ComfortPriority.ToString();
                    lblEnergy.Text = diagnosticsResult.EnergyPriority.ToString();
                    lblMaintenance.Text = diagnosticsResult.MaintenancePriority.ToString();

                    litNotes.Text = diagnosticsResult.Notes;
                    divNotes.Visible = String.IsNullOrEmpty(diagnosticsResult.Notes) ? false : true;

                    lblPoints.Text = "";
                    IEnumerable<Point> points = DataMgr.PointDataMapper.GetAllVisiblePointsByCommonPointTypesByAIDAndEID(diagnosticsResult.AID, diagnosticsResult.EID, (p => p.PointType));
                    foreach (Point p in points)
                        lblPoints.Text += p.PointName + " (" + p.PointType.DisplayName + "), ";

                    lblPoints.Text = lblPoints.Text.TrimEnd(' ').TrimEnd(',');

                    lblAssociatedEquipmentPoints.Text = "";
                    foreach (Equipment e in associatedEquipment)
                    {
                        IEnumerable<Point> associatedEquipmentPoints = DataMgr.PointDataMapper.GetAllVisiblePointsByCommonPointTypesByAIDAndEID(diagnosticsResult.AID, e.EID, (p => p.PointType));
                        foreach (Point p in associatedEquipmentPoints)
                            lblAssociatedEquipmentPoints.Text += p.PointName + " (" + p.PointType.DisplayName + " on " + e.EquipmentName + "), ";
                    }
                    lblAssociatedEquipmentPoints.Text = lblAssociatedEquipmentPoints.Text.TrimEnd(' ').TrimEnd(',');
                    if (String.IsNullOrEmpty(lblAssociatedEquipmentPoints.Text))
                        lblAssociatedEquipmentPoints.Text = associatedEquipmentPointsEmpty;

                    //show top and bottom details
                    analysisDetailsTop.Visible = true;
                    analysisDetailsBottom.Visible = true;

                    //details label
                    switch (analysisRange)
                    {
                        case DataConstants.AnalysisRange.Daily:
                            lblDetails.Text = String.Format("Daily {0} analysis data for {1} performed on {2}.", diagnosticsResult.AnalysisName, diagnosticsResult.EquipmentName, diagnosticsResult.StartDate.ToShortDateString());
                            break;
                        case DataConstants.AnalysisRange.Weekly:
                            lblDetails.Text = String.Format("Weekly {0} analysis data for {1} performed on {2}.", diagnosticsResult.AnalysisName, diagnosticsResult.EquipmentName, diagnosticsResult.StartDate.ToShortDateString());
                            break;
                        case DataConstants.AnalysisRange.Monthly:
                            lblDetails.Text = String.Format("Monthly {0} analysis data for {1} performed on {2}.", diagnosticsResult.AnalysisName, diagnosticsResult.EquipmentName, diagnosticsResult.StartDate.ToShortDateString());
                            break;
                        case DataConstants.AnalysisRange.HalfDay:
                            lblDetails.Text = String.Format("Half day {0} analysis data for {1} performed on {2}.", diagnosticsResult.AnalysisName, diagnosticsResult.EquipmentName, diagnosticsResult.StartDate.ToShortDateString());
                            break;
                    }


                    //get analysis vars
                    var analysisEquipmentVars = DataMgr.EquipmentVariableDataMapper.GetAllEquipmentVariablesAssociatedToAID(aid).ToList();
                    var analysisBuildingVars = DataMgr.BuildingVariableDataMapper.GetAllBuildingVariablesAssociatedToAID(aid).ToList(); 

                    //bind equipment variables
                    BindEquipmentVariables(rptEquipmentVars, lblEquipmentVarsEmpty, new int[] { eid }, analysisEquipmentVars);
                    BindEquipmentVariables(rptAssociatedEquipmentVars, lblAssociatedEquipmentVarsEmpty, associatedEquipment.Select(ae => ae.EID).ToArray(), analysisEquipmentVars);

                    //bind building variables
                    BindBuildingVariables(rptBuildingVars, lblBuildingVarsEmpty, diagnosticsResult.BID, analysisBuildingVars);

                    if (setFocus) lnkSetFocusView.Focus();
                }
                else
                {
                    //hide bottom details
                    analysisDetailsBottom.Visible = false;

                    //show top
                    lblDetails.Text = "No analysis data exists for your selection.";
                    if (setFocus) lnkSetFocusMessage.Focus();
                    analysisDetailsTop.Visible = true;
                }

                divDownloadBox.Visible = analysisDetailsBottom.Visible;
            }
            catch (SqlException sqlEx)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving and populating analysis diagnostics.", sqlEx);
            }
            catch (Exception ex)
            {
                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving and populating analysis diagnostics.", ex);
            }
        }

        #endregion

        #region Helper Methods

        private void SendInaccuracyReportEmail(QueryString requestSupportInvariantValues, QueryString requestSupportValues, string requestSupportBodyMiddle, string extendedSubjectInfo, string supportOverrideEmail)
        {
            //message
            string requestSupportBody = editorRequestSupportMessage.Content;

            //set siteuser to user
            User mUser = new User();
            mUser.Email = siteUser.Email;
            mUser.FirstName = siteUser.FirstName;
            mUser.LastName = siteUser.LastName;
            mUser.UID = siteUser.UID;

            //try to send email
            try
            {
                Email.SendRequestSupportEmail(mUser,
                                    LinkHelper.DiagnosticsPage + "?" + requestSupportInvariantValues.ToString(),
                                    LinkHelper.DiagnosticsPage + "?" + requestSupportValues.ToString(),
                                    String.Format(requestSupportSubject, extendedSubjectInfo),
                                    requestSupportBodyTop,
                                    requestSupportBodyMiddle,
                                    requestSupportBody,
                                    siteUser.IsSchneiderTheme, 
									supportOverrideEmail);


                lblRequestSupportError.Text = requestSupportSuccessful;
                lblRequestSupportError.Visible = true;
            }
            catch (Exception ex)
            {
                lblRequestSupportError.Text = requestSupportFailed;
                lblRequestSupportError.Visible = true;

                LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error requesting diagnostic support from user: UserEmail={0}.", siteUser.Email), ex);
            }

            pnlRequestSupport.Style.Value = "display:block;";
            mpeRequestSupport.Show();
        }

        private void SetResultsLabel(IEnumerable<DiagnosticsResult> results)
        {
            var resultCount = results.Count();
            var rangeText = String.Format("{0} to {1}", bindingParameters.start.ToShortDateString(), bindingParameters.end.ToShortDateString());
            var intervalText = bindingParameters.range.ToString().ToLower();

            lblResults.Text =
            String.Format
            (
                String.Concat("{0} data record", ((resultCount == 1) ? String.Empty : "s"), " found for {1} in {2} intervals. "), new[] { resultCount.ToString(), rangeText, intervalText, }
            );

            divDownloadBoxTop.Visible = resultCount > 0;
            hdnResultCount.Value = resultCount.ToString();
        }

        private string CreateCacheKey()
        {
            var sb = new StringBuilder();

            sb.Append(diagnosticsGridCacheName);
            sb.Append(CacheDelimiter);

            // TODO: optimize to only cache superset (building) rather than 
            // smaller subset from the superset (EquipmentClass, Equipment, etc)
            // to avoid duplicated cached data
            sb.Append("Building");

            foreach (int bid in bindingParameters.bids.OrderBy(b => b))
            {
                sb.Append(bid);
                sb.Append(CacheDelimiter);
            }

            if (bindingParameters.equipmentClassID.HasValue && bindingParameters.equipmentClassID.Value != -1)
            {
                sb.Append("EquipmentClass");
                sb.Append(bindingParameters.equipmentClassID);
                sb.Append(CacheDelimiter);
            }

            if (bindingParameters.equipmentID.HasValue && bindingParameters.equipmentID.Value != -1)
            {
                sb.Append("Equipment");
                sb.Append(bindingParameters.equipmentID);
                sb.Append(CacheDelimiter);
            }

            if (bindingParameters.analysisID.HasValue && bindingParameters.analysisID.Value != -1)
            {
                sb.Append("Analysis");
                sb.Append(bindingParameters.analysisID);
                sb.Append(CacheDelimiter);
            }

            sb.Append("Range");
            sb.Append(AnalysisHelper.GetAnalysisRangeTextValue(bindingParameters.range));
            sb.Append(CacheDelimiter);

            sb.Append("Start");
            sb.Append(bindingParameters.start.ToString(AzureConstants.DATETIME_DAY_FORMAT));
            sb.Append(CacheDelimiter);

            sb.Append("End");
            sb.Append(bindingParameters.end.ToString(AzureConstants.DATETIME_DAY_FORMAT));
            sb.Append(CacheDelimiter);


            sb.Append("Count");
            sb.Append(ddlPrioritiesCount.SelectedValue);
            sb.Append(CacheDelimiter);

            return sb.ToString();
        }

        private void ClearMessaging()
        {
            DisplayErrorOutput("", lblError);
            DisplayErrorOutput("", lblErrorTop);
        }

        private void ProcessRequestWithQueryString()
        {
            bool isInvariant = false;
            bool.TryParse(queryString["i"], out isInvariant);
            var qs = queryString.ToObject<DiagnosticsParamsObject>(isInvariant);

            if (qs == null)
            {
                ProcessRequest();

                return;
            }

            if (qs.cid != siteUser.CID) return;

            var modes =
                new Dictionary<String, LinkHelper.ViewByMode>
                {
                        {PropHelper.G<DiagnosticsParamsObject>(x => x.aid),  LinkHelper.ViewByMode.Analysis},
                        {PropHelper.G<DiagnosticsParamsObject>(x => x.eid),  LinkHelper.ViewByMode.Equipment},
                        {PropHelper.G<DiagnosticsParamsObject>(x => x.ecid), LinkHelper.ViewByMode.EquipmentClass},
                        {PropHelper.G<DiagnosticsParamsObject>(x => x.bid),  LinkHelper.ViewByMode.Building},
                };

            LinkHelper.ViewByMode? mode = null;

            foreach (var m in modes)
            {
                if (queryString.Get<Int32>(m.Key) == null)
                {
                    if (mode == null) continue;

                    return;
                }

                if (mode == null)
                {
                    mode = m.Value;
                }
            }

            if (mode == null) return;

            currentMode = mode.Value;

            bindingParameters =
            new DiagnosticsBinder.BindingParameters
            {
                range = qs.rng,
                start = qs.sd,
                end = (qs.ed ?? qs.sd),
                
                cid = qs.cid,
                bids = ((qs.bid == 0) ? siteUser.VisibleBuildings.Select(b => b.BID).ToList() : new List<Int32>(new[] { qs.bid.Value })), //revise not to use 0

                equipmentClassID = qs.ecid,
                equipmentID = qs.eid,
                analysisID = qs.aid,
            };

            Generate();

            ApplyBindingParameters();

            //if bid not 0 and bid, eid, and aid are not null then show details
            if (qs.bid != null && qs.bid != 0 && bindingParameters.equipmentID != null && bindingParameters.analysisID != null)
            {
                //simpulate gridselect and set for index 0 if poulated details.
                hdnAnalysisInfoTempGridRowIndex.Value = "0";
                PopulateAnalysisInfo(bindingParameters.cid, bindingParameters.bids.First(), (int)bindingParameters.equipmentID, (int)bindingParameters.analysisID, bindingParameters.start, false);
            }
        }

        private void ProcessRequest()
        {
            currentMode = LinkHelper.ViewByMode.Building;

            bindingParameters = new DiagnosticsBinder.BindingParameters
            {
                range = DataConstants.AnalysisRange.Daily,

                cid = siteUser.CID,
                bids = siteUser.VisibleBuildings.Select(b => b.BID).ToList(),
            };

            DateTimeHelper.GenerateDefaultDatesYesterday(out bindingParameters.start, out bindingParameters.end, siteUser.UserTimeZoneOffset);

            Generate();

            ApplyBindingParameters();
        }

        private void ApplyBindingParameters()
        {
            rblRange.SelectedValue = bindingParameters.range.ToString();

            txtStartDate.SelectedDate = bindingParameters.start;
            txtEndDate.SelectedDate = bindingParameters.end;

            ControlHelper.DropDownListControl.SelectItem(ddlBuildings, bindingParameters.bids.Count == 1 ? bindingParameters.bids.First().ToString() : "0");
            ControlHelper.DropDownListControl.SelectItem(ddlEquipmentClasses, bindingParameters.equipmentClassID ?? 0);
            ControlHelper.DropDownListControl.SelectItem(ddlEquipment, bindingParameters.equipmentID ?? 0);

            ddlAnalysis.SelectedValue = bindingParameters.analysisID.ToString();
        }

        private void VisibilityHelper(LinkHelper.ViewByMode mode)
        {
            switch (mode)
            {
                case LinkHelper.ViewByMode.BuildingGroup:
                    {
                        pnlBuildingGroups.Visible = true;
                        buildingGroupRFV.Enabled = true;

                        pnlBuildings.Visible = false;
                        buildingRequiredValidator.Enabled = false;

                        pnlEquipmentClass.Visible = false;
                        equipmentClassRequiredValidator.Enabled = false;

                        pnlEquipment.Visible = false;
                        equipmentRequiredValidator.Enabled = false;

                        pnlAnalysis.Visible = false;
                        analysisRequiredValidator.Enabled = false;

                        break;
                    }
                case LinkHelper.ViewByMode.Building:
                    {
                        ToggleBuildingGroupItem(buildingGroupsExist);

                        pnlBuildingGroups.Visible = false;
                        buildingGroupRFV.Enabled = false;

                        pnlBuildings.Visible = true;
                        buildingRequiredValidator.Enabled = true;

                        pnlEquipmentClass.Visible = false;
                        equipmentClassRequiredValidator.Enabled = false;

                        pnlEquipment.Visible = false;
                        equipmentRequiredValidator.Enabled = false;

                        pnlAnalysis.Visible = false;
                        analysisRequiredValidator.Enabled = false;


                        dropDownSequencer.ResetSubDropDownsOf(ddlBuildings, true);

                        break;
                    }
                case LinkHelper.ViewByMode.EquipmentClass:
                    {
                        ToggleBuildingGroupItem(false);

                        pnlBuildingGroups.Visible = false;
                        buildingGroupRFV.Enabled = false;

                        pnlEquipmentClass.Visible = true;
                        equipmentClassRequiredValidator.Enabled = true;

                        pnlEquipment.Visible = false;
                        equipmentRequiredValidator.Enabled = false;

                        pnlAnalysis.Visible = false;
                        analysisRequiredValidator.Enabled = false;

                        dropDownSequencer.ResetSubDropDownsOf(ddlEquipmentClasses, true);

                        break;
                    }
                case LinkHelper.ViewByMode.Equipment:
                    {
                        ToggleBuildingGroupItem(false);

                        pnlBuildingGroups.Visible = false;
                        buildingGroupRFV.Enabled = false;

                        pnlEquipmentClass.Visible = true;
                        equipmentClassRequiredValidator.Enabled = true;

                        pnlEquipment.Visible = true;
                        equipmentRequiredValidator.Enabled = true;

                        pnlAnalysis.Visible = false;
                        analysisRequiredValidator.Enabled = false;

                        dropDownSequencer.ResetSubDropDownsOf(ddlEquipment, true);

                        break;
                    }
                case LinkHelper.ViewByMode.Analysis:
                    {
                        ToggleBuildingGroupItem(false);

                        pnlBuildingGroups.Visible = false;
                        buildingGroupRFV.Enabled = false;

                        pnlEquipmentClass.Visible = true;
                        equipmentClassRequiredValidator.Enabled = true;

                        pnlEquipment.Visible = true;
                        equipmentRequiredValidator.Enabled = true;

                        pnlAnalysis.Visible = true;
                        analysisRequiredValidator.Enabled = true;

                        break;
                    }
            }
        }

        protected ITextSharpHelper.AnalysisPDFReport CreateAnalysisPDFReportDTO()
        {
            var dto = new ITextSharpHelper.AnalysisPDFReport();

            Equipment equipment = DataMgr.EquipmentDataMapper.GetEquipmentByID(Convert.ToInt32(hdnEID.Value));

            dto.AnalysisName = lblAnalysisName.Text;
            dto.ClientName = lblClientName.Text;
            dto.BuildingName = lblBuildingName.Text;
            dto.EquipmentName = lblEquipmentName.Text;
            dto.AssociatedEquipment = Convert.ToBoolean(associatedEquipmentCollapsiblePanelExtender.ClientState) ? "" : lblAssociatedEquipment.Text;
            dto.EquipmentLocation = equipment.EquipmentLocation;
            dto.Points = Convert.ToBoolean(pointsCollapsiblePanelExtender.ClientState) ? "" : lblPoints.Text;
            dto.AssociatedEquipmentPoints = Convert.ToBoolean(associatedEquipmentPointsCollapsiblePanelExtender.ClientState) ? "" : lblAssociatedEquipmentPoints.Text;
            dto.StartDate = lblStartDate.Text;
            dto.DisplayInterval = lblDisplayInterval.Text;
            dto.CostSavings = lblCost.Text;
            dto.ComfortPriority = lblComfort.Text;
            dto.EnergyPriority = lblEnergy.Text;
            dto.MaintenancePriority = lblMaintenance.Text;
            dto.Notes = litNotes.Text;
            dto.ImageByteArray = DataMgr.DefaultCacheRepository.Get<byte[]>(GetImageKey());

            return dto;
        }

        private string GetImageKey()
        {
            return String.Format("{0}-{1}-{2}", siteUser.UID, ID, "imgStream");
        }

        private void HideModal()
        {
            //POSTPONE-----------

            lblPostponeError.Visible = false;

            //to avoid panel showing on page load.
            pnlPostpone.Style.Value = "display:none;";
            pnlPostpone.Style.Value = "visibility:hidden;";

            //hide panel so it doesnt cause validation
            mpePostpone.Hide();


            //EQUIPMENT NOTES-----------

            lblEquipmentNotesError.Visible = false;

            //to avoid panel showing on page load.
            pnlEquipmentNotes.Style.Value = "display:none;";
            pnlEquipmentNotes.Style.Value = "visibility:hidden;";

            //hide panel so it doesnt cause validation
            mpeEquipmentNotes.Hide();
        }

        protected static string SetPriorityClass(string value)
        {
            return String.IsNullOrEmpty(value) ? "0" : (Convert.ToDecimal(value) > 10 ? "10" : Math.Ceiling(Convert.ToDecimal(value)).ToString());
        }

        private ListItemCollection BuildActionList()
        {
            ListItemCollection actionList = new ListItemCollection();

            actionList.Add(new ListItem() { Text = "Equipment Notes", Value = "EQUIPMENTNOTES" });

            //show postpone, and equipvars actions if fulladmin or higher
            if (siteUser.IsFullAdminOrHigher)
            {
                actionList.Add(new ListItem() { Text = "Equipment Variables", Value = "VARIABLES" });
                actionList.Add(new ListItem() { Text = "Postpone Analysis", Value = "POSTPONE" });
            }

            actionList.Add(new ListItem() { Text = "Request Support", Value = "SUPPORT" });

            if (siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.AnalysisBuilder)) actionList.Add(new ListItem() { Text = "View Raw Data", Value = "RAWDATA" });

            if (siteUser.Modules.Any(s => s.ModuleID == (Int32)BusinessConstants.Module.Modules.Projects))
            {
                actionList.Add(new ListItem() { Text = "Create New Project", Value = "CREATEPROJECT" });
                actionList.Add(new ListItem() { Text = "Add to Existing Project", Value = "EXISTINGPROJECT" });
            }

            if (HasTaskModuleAccess) actionList.Add(new ListItem() { Text = "Create New Task", Value = "CREATETASK" });

            return actionList;
        }

        private void ToggleBuildingGroupItem(Boolean addItem)
        {
            if (addItem)
            {
                if (!rblViewBy.Items.Contains(rblBuildingGroupItem)) rblViewBy.Items.Insert(0, rblBuildingGroupItem);
            }
            else
            {
                if (rblViewBy.Items.Contains(rblBuildingGroupItem)) rblViewBy.Items.Remove(rblBuildingGroupItem);
            }
        }

        private IEnumerable<BuildingGroup> GetBuildingGroups(String buildingGroupID = "")
        {
            var bgid = (!String.IsNullOrWhiteSpace(buildingGroupID) && buildingGroupID != "0") ? Convert.ToInt32(buildingGroupID) : 0;

            return (bgid > 0) ? siteUser.BuildingGroups.Where(bg => bg.BuildingGroupID == bgid) : siteUser.BuildingGroups;
        }

        private void ParseTrackingCodeAndSetValues()
        {
            var parsedTrackingCode = TrackingCodeHelper.ParseTrackingCode(txtTrackingCode.Text);

            //TODO set the faultID data, jp
            if (parsedTrackingCode.Length == 3)
            {
                var eid = parsedTrackingCode[0];
                var aid = parsedTrackingCode[1];
                var fid = parsedTrackingCode[2];

                var equipment = DataMgr.EquipmentDataMapper.GetEquipmentByEID(eid, false, false, false, false, false, false);
                var building = DataMgr.BuildingDataMapper.GetBuildingByEID(eid);
                var equipmentClass = (equipment != null) ? DataMgr.EquipmentClassDataMapper.GetEquipmentClassByEquipmentTypeID(equipment.EquipmentTypeID) : null;
                var analysis = DataMgr.AnalysisDataMapper.GetAnalysis(aid);

                var isDataValid = (equipment != null && building != null && equipmentClass != null && analysis != null);

                if (isDataValid)
                {
                    var buildingItem = new ListItem { Text = building.BuildingName, Value = building.BID.ToString() };

                    if (ddlBuildings.Items.Contains(buildingItem)) ddlBuildings.SelectedValue = building.BID.ToString();

                    //

                    BindEquipmentClassesDropDownList();

                    var equipmentClassID = equipmentClass.EquipmentClassID.ToString();
                    var equipmentClassItem = new ListItem { Text = equipmentClass.EquipmentClassName, Value = equipmentClassID };

                    if (ddlEquipmentClasses.Items.Contains(equipmentClassItem)) ddlEquipmentClasses.SelectedValue = equipmentClassID;

                    //

                    BindEquipmentDropdownList();

                    var equipmentItem = new ListItem { Text = equipment.EquipmentName, Value = eid.ToString() };

                    if (ddlEquipment.Items.Contains(equipmentItem)) ddlEquipment.SelectedValue = eid.ToString();

                    //

                    BindAnalysisDropdownList();

                    var analysisItem = new ListItem { Text = analysis.AnalysisName, Value = aid.ToString() };

                    if (ddlAnalysis.Items.Contains(analysisItem)) ddlAnalysis.SelectedValue = aid.ToString();

                    var isDataSelected = (ddlEquipmentClasses.SelectedValue != "-1" && ddlEquipment.SelectedValue != "-1" && ddlAnalysis.SelectedValue != "-1");

                    if (isDataSelected)
                    {
                        rblViewBy.SelectedValue = "Analysis";
                        currentMode = LinkHelper.ViewByMode.Analysis;
                        VisibilityHelper(currentMode);
                    }
                }
            }
        }

        private void ResetTrackingCode()
        {
            txtTrackingCode.Text = String.Empty;
        }

        #endregion
    }
}