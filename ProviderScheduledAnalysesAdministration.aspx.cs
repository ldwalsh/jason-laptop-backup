﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.ScheduledAnalyses;
using System;

namespace CW.Website
{
    public partial class ProviderScheduledAnalysesAdministration : AdminSitePageTemp
    {
        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewScheduledAnalyses).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.PROVIDER; } }

            #endregion

        #endregion
    }
}