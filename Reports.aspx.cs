﻿using CW.Business.Blob.Images;
using CW.Common.Constants;
using CW.Data;
using CW.Data.Helpers;
using CW.Data.Models.Diagnostics;
using CW.Data.Models.Point;
using CW.Utility;
using CW.Reporting._fileservices;
using CW.Website._framework;
using CW.Website._framework.httphandlers;
using CW.Website._reports;
using CW.Website.Reports;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using Telerik.Charting;
using Telerik.Web.UI;
using CW.Utility.Web;

namespace CW.Website
{
    public partial class ReportsPage : SitePage
    {
        #region fields

        protected ChartRegion priChartRegion;
        protected ChartRegion secChartRegion;

        #endregion

        #region const

        const string successfulAnalysis = "Analysis data shown below for your selection.";
        const string noAnalysisData = "No analysis data exists for your selection.";
        const string noGraphAvailable = "Graph not available.";

        const string successfulPlots = "Data points were successfully plotted.";
        const string emptyPlots = "One or more data points could not be plotted because no data exists.";
        const string noPlots = "No data has been plotted.";
        const string failedPlots = "Data points failed to plot. Please contact an administrator.";
        const Char delimeter = DataConstants.DefaultDelimiter;

        #endregion

        #region Page Events

        protected void Page_Init(Object sender, EventArgs e)
        {
        }

        protected void Page_Load(Object sender, EventArgs e)
        {
            DisplayErrorOutput(null);
        }

        protected void Page_FirstInit(Object sender, EventArgs e)
        {
            litReportsBody.Text = TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, DataMgr.GlobalDataMapper.GetGlobalSettings().ReportsBody);
        }

        protected void Page_FirstLoad(Object sender, EventArgs e)
        {
            if (!queryString.IsEmpty)
            {
                ProcessRequestWithQueryString();
            }
        }

        #endregion

        #region Checkbox Events

        protected void chkCompareWith_OnCheckedChanged(Object sender, EventArgs e)
        {
            reportCriteriaRight.enabled = chkCompareWith.Checked;

            if (!chkCompareWith.Checked)
            {
                secChartRegion.Visible = false;
            }
        }

        #endregion

        #region Button Click Events

        protected void generateButton_Click(Object sender, EventArgs e)
        {
            var priChartNotEmpty = RenderChartRegion(reportCriteriaLeft, priChartRegion, "Primary Chart");
            var secChartNotEmpty = RenderChartRegion(reportCriteriaRight, secChartRegion, "Secondary Chart");

            //if either charts have data and not empty then show ability to email and download report 
            //and add additional comments to report
            divDownloadBox.Visible = (priChartNotEmpty || secChartNotEmpty);
            divEmail.Visible = (priChartNotEmpty || secChartNotEmpty);
            divPdfDownload.Visible = (priChartNotEmpty || secChartNotEmpty);
            divReportComments.Visible = (priChartNotEmpty || secChartNotEmpty);
        }

        protected void emailButton_Click(Object sender, EventArgs e)
        {
            ITextSharpHelper.AnalysisPDFReport analysisPDFReport = null;
            ITextSharpHelper.AnalysisPDFReport analysisPDFReport2 = null;
            ITextSharpHelper.RawPDFReport rawPDFReport = null;
            ITextSharpHelper.RawPDFReport rawPDFReport2 = null;
            ITextSharpHelper.CalculatedPDFReport calculatedPDFReport = null;
            ITextSharpHelper.CalculatedPDFReport calculatedPDFReport2 = null;

            LoadReportInfoIntoPDFReportClasses(ref analysisPDFReport, ref analysisPDFReport2, ref rawPDFReport, ref rawPDFReport2, ref calculatedPDFReport, ref calculatedPDFReport2);

            var reportService = ReportFileGenerator.Create(siteUser.IsSchneiderTheme, siteUser.FullName, LogMgr, analysisPDFReport, analysisPDFReport2,
                rawPDFReport, rawPDFReport2, calculatedPDFReport, calculatedPDFReport2, editorReportComments.Content);

            if (reportService == null)
            {
                DisplayErrorOutput("Failed to generate pdf report. Please try again later.");

                return;
            }

            Boolean result;

            try
            {
                result = new FilePublishService(DataMgr.ClientDataMapper.UpdateClientAccountSettings, DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID), siteUser.IsKGSFullAdminOrHigher, siteUser.IsSchneiderTheme, siteUser.Email, reportService, LogMgr).SendInEmail(Email.GenerateReportSubject(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product Analysis Report for "), analysisPDFReport, analysisPDFReport2, rawPDFReport, rawPDFReport2), Email.GenerateReportBody(TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product Analysis Report for "), analysisPDFReport, analysisPDFReport2, rawPDFReport, rawPDFReport2));
            }
            catch (FilePublishService.MonthlyReportEmailAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot email report, max monthly email limit has been reached.");

                return;
            }

            DisplayErrorOutput
            (
                result ?
                "Analysis report was emailed successfully." :
                "Analysis report email failed. Please try again later."
            );
        }

        protected void downloadPdfButton_Click(object sender, EventArgs e)
        {
            ITextSharpHelper.AnalysisPDFReport analysisPDFReport = null;
            ITextSharpHelper.AnalysisPDFReport analysisPDFReport2 = null;
            ITextSharpHelper.RawPDFReport rawPDFReport = null;
            ITextSharpHelper.RawPDFReport rawPDFReport2 = null;
            ITextSharpHelper.CalculatedPDFReport calculatedPDFReport = null;
            ITextSharpHelper.CalculatedPDFReport calculatedPDFReport2 = null;

            //load the report info into the  pdf report class
            LoadReportInfoIntoPDFReportClasses(ref analysisPDFReport, ref analysisPDFReport2, ref rawPDFReport, ref rawPDFReport2, ref calculatedPDFReport, ref calculatedPDFReport2);

            var reportService = ReportFileGenerator.Create(siteUser.IsSchneiderTheme, siteUser.FullName, LogMgr, analysisPDFReport, analysisPDFReport2, rawPDFReport, rawPDFReport2, calculatedPDFReport, calculatedPDFReport2, editorReportComments.Content);

            if (reportService == null)
            {
                DisplayErrorOutput("Failed to generate pdf report. Please try again later.");

                return;
            }

            Boolean result;

            try
            {
                result = new FilePublishService
                (
                    DataMgr.ClientDataMapper.UpdateClientAccountSettings,
                    DataMgr.ClientDataMapper.GetClientSettingsByClientID(siteUser.CID),
                    siteUser.IsKGSFullAdminOrHigher,
                    siteUser.IsSchneiderTheme,
                    siteUser.Email,
                    reportService,
                    LogMgr
                ).AttachAsDownload(new DownloadRequestHandler.HandledFileAttacher(), TokenVariableHelper.FormatSpecialThemeBasedTokens(siteUser.IsSchneiderTheme, "$product_Report.pdf", true));
            }
            catch (FilePublishService.MonthlyDownloadAllowanceReachedException)
            {
                DisplayErrorOutput("Cannot download report, max monthly download limit has been reached. Please try again later, contact your provider, or use the contact us page in request an increase in download limits.");

                return;
            }

            if (result) return;

            DisplayErrorOutput("An error occured attempting to download. Please try again later.");
        }
        #endregion

        #region Load PDF

        protected void LoadReportInfoIntoPDFReportClasses(ref ITextSharpHelper.AnalysisPDFReport analysisPDFReport, ref ITextSharpHelper.AnalysisPDFReport analysisPDFReport2, ref ITextSharpHelper.RawPDFReport rawPDFReport, ref ITextSharpHelper.RawPDFReport rawPDFReport2, ref ITextSharpHelper.CalculatedPDFReport calculatedPDFReport, ref ITextSharpHelper.CalculatedPDFReport calculatedPDFReport2)
        {
            priChartRegion.LoadReportInfoIntoPDFReportClasses(ref analysisPDFReport, ref rawPDFReport, ref calculatedPDFReport);
            secChartRegion.LoadReportInfoIntoPDFReportClasses(ref analysisPDFReport2, ref rawPDFReport2, ref calculatedPDFReport2);
        }

        #endregion

        #region Helper Methods

        private Boolean RenderChartRegion(ReportCriteria reportCriteria, ChartRegion chartRegion, String title)
        {
            if (!reportCriteria.enabled) return false;

            var chartNotEmpty = false;

            chartRegion.Visible = true;
            chartRegion.parentReportCriteria = reportCriteria;

            switch (reportCriteria.reportType)
            {
                case ReportCriteria.ReportType.Analyzed:
                    {
                        chartNotEmpty = PopulateAnalysisInfo(reportCriteria, chartRegion);

                        chartRegion.rawInfo.Visible = false;

                        if (chartRegion.binImageGraph.DataValue != null)
                            DataMgr.DefaultCacheRepository.Upsert(GetImageKey(chartRegion.ID), chartRegion.binImageGraph.DataValue);

                        break;
                    }
                case ReportCriteria.ReportType.Calculated:
                    {
                        chartRegion.hdnAnalysisRange.Value = reportCriteria.displayRange.ToString();
                        goto case ReportCriteria.ReportType.Raw;
                    }
                //raw data selection
                case ReportCriteria.ReportType.Raw:
                    {
                        switch (reportCriteria.chartMode)
                        {
                            case ReportCriteria.ChartMode.Simple:
                                {
                                    chartRegion.rawChart.Visible = true;
                                    chartRegion.radChart.Visible = false;
                                    chartRegion.resetChart.Visible = false;

                                    if (reportCriteria.reportType == ReportCriteria.ReportType.Calculated)
                                    {
                                        chartNotEmpty = PlotSimpleVDataPoints(reportCriteria, chartRegion);
                                    }
                                    else
                                    {
                                        chartNotEmpty = PlotSimpleRawDataPoints(reportCriteria, chartRegion);
                                    }


                                    //save image form chart to stream to session
                                    using (var imgStream = new MemoryStream())
                                    {
                                        chartRegion.rawChart.SaveImage(imgStream);

                                        DataMgr.DefaultCacheRepository.Upsert(GetImageKey(chartRegion.ID), imgStream.ToArray());
                                    }

                                    break;
                                }
                            case ReportCriteria.ChartMode.Advanced:
                                {
                                    chartRegion.rawChart.Visible = false;
                                    chartRegion.radChart.Visible = true;
                                    chartRegion.resetChart.Visible = true;

                                    if (reportCriteria.reportType == ReportCriteria.ReportType.Calculated)
                                    {
                                        chartNotEmpty = PlotAdvancedVDataPoints(reportCriteria, chartRegion);
                                    }
                                    else
                                    {
                                        chartNotEmpty = PlotAdvancedRawDataPoints(reportCriteria, chartRegion);
                                    }

                                    //needed to allow the image to have axis
                                    chartRegion.radChart.ClientSettings.ScrollMode = ChartClientScrollMode.None;

                                    try
                                    {
                                        using (var imgStream = new MemoryStream())
                                        {
                                            chartRegion.radChart.Save(imgStream, ImageFormat.Png);

                                            DataMgr.DefaultCacheRepository.Upsert(GetImageKey(chartRegion.ID), imgStream.ToArray());
                                        }
                                    }
                                    catch (Exception)
                                    {
                                    }

                                    //zoom must be disabled and no scrolling for pie chart.
                                    if (reportCriteria.chartType == ChartSeriesType.Pie.ToString())
                                    {
                                        chartRegion.radChart.ClientSettings.EnableZoom = false;
                                        chartRegion.radChart.ClientSettings.ScrollMode = ChartClientScrollMode.None;
                                    }
                                    else
                                    {
                                        chartRegion.radChart.ClientSettings.ScrollMode = ChartClientScrollMode.Both;
                                    }

                                    break;
                                }

                        }

                        chartRegion.analysisInfo.Visible = false;

                        break;
                    }
            }

            chartRegion.regionTitle.InnerText = title;

            return chartNotEmpty;
        }

        private string GetImageKey(string controlID)
        {
            return String.Format("{0}-{1}-{2}", HttpContext.Current.Session.SessionID, controlID, "_imgStream");
        }

        /// <summary>
        /// Populates the analyzed data, and returns a bool if the chart is empty.
        /// </summary>
        protected bool PopulateAnalysisInfo(ReportCriteria reportCriteria, ChartRegion chartRegion)
        {
            if (reportCriteria.aid == null) return false;

            chartRegion.reportInfo.Visible = true;

            var aid = reportCriteria.aid.Value;
            var eid = reportCriteria.eid.Value;
            var bid = reportCriteria.bid.Value;
            var cid = (siteUser.IsKGSFullAdminOrHigher ? reportCriteria.clientEntity.ID : siteUser.CID);

            var analysisRange = AnalysisHelper.GetAnalysisRange(reportCriteria.displayRange.ToString());

            var startDate = reportCriteria.analysisDate;

            switch (analysisRange)
            {
                case DataConstants.AnalysisRange.Weekly:
                    {
                        startDate = DateTimeHelper.GetLastSunday(startDate);

                        break;
                    }
                case DataConstants.AnalysisRange.Monthly:
                    {
                        startDate = DateTimeHelper.GetFirstOfMonth(startDate);

                        break;
                    }
            }

            var diagnosticsResults = DataMgr.DiagnosticsDataMapper.GetDiagnosticsResults(
                new DiagnosticsResultsInputs()
                {
                    AID = aid,
                    BID = bid,
                    CID = cid,
                    EID = eid,
                    StartDate = startDate,
                    EndDate = startDate,
                    AnalysisRange = analysisRange
                });

            DiagnosticsResult diagnosticsResult = diagnosticsResults.FirstOrDefault();


            if (diagnosticsResult == null)
            {
                //hide info
                chartRegion.analysisInfo.Visible = false;
                chartRegion.lblResults.Text = noAnalysisData;
                chartRegion.lblResults.Visible = true;

                return false;
            }

            if (String.IsNullOrEmpty(diagnosticsResult.FigureBlobExt))
            {
                // clear image graph and add text instead using literal control
                chartRegion.binImageGraph.Visible = false;
                chartRegion.litGraphNotAvailable.Text = "Graph not available.";
            }
            else
            {
                chartRegion.binImageGraph.DataValue = new FigureImage(analysisRange, diagnosticsResult, DataMgr.OrgBasedBlobStorageProvider).Retrieve();
            }

            //show client details
            chartRegion.lblClientName.Text = reportCriteriaLeft.clientEntity.name;
            chartRegion.hdnCID.Value = cid.ToString();

            chartRegion.lblBuildingName.Text = diagnosticsResult.BuildingName;
            chartRegion.lblEquipmentName.Text = diagnosticsResult.EquipmentName;
            chartRegion.hdnEID.Value = eid.ToString();

            IEnumerable<Equipment> associatedEquipment = DataMgr.EquipmentDataMapper.GetAllVisibleEquipmentAssociatedByPEID(eid, false);
            chartRegion.lblAssociatedEquipment.Text = StringHelper.ParseStringArrayToCSVString(associatedEquipment.Select(ae => ae.EquipmentName).ToArray(), true);

            chartRegion.lblAnalysisName.Text = diagnosticsResult.AnalysisName;
            chartRegion.lblDisplayInterval.Text = reportCriteria.displayRange.ToString();
            chartRegion.lblStartDate.Text = diagnosticsResult.StartDate.ToShortDateString();

            chartRegion.lblCost.Text = CultureHelper.FormatCurrencyAsString(diagnosticsResult.LCID, diagnosticsResult.CostSavings).ToString();
            chartRegion.lblComfort.Text = diagnosticsResult.ComfortPriority.ToString();
            chartRegion.lblEnergy.Text = diagnosticsResult.EnergyPriority.ToString();
            chartRegion.lblMaintenance.Text = diagnosticsResult.EnergyPriority.ToString();
            chartRegion.litNotes.Text = (diagnosticsResult.Notes ?? String.Empty);

            chartRegion.lblPoints.Text = "";
            var points = DataMgr.PointDataMapper.GetAllVisiblePointsByCommonPointTypesByAIDAndEID(diagnosticsResult.AID, diagnosticsResult.EID, (p => p.PointType));
            foreach (CW.Data.Point p in points)
                chartRegion.lblPoints.Text += p.PointName + " (" + p.PointType.DisplayName + "), ";

            chartRegion.lblPoints.Text = chartRegion.lblPoints.Text.TrimEnd(' ').TrimEnd(',');

            chartRegion.lblAssociatedEquipmentPoints.Text = "";
            foreach (Equipment e in associatedEquipment)
            {
                var associatedEquipmentPoints = DataMgr.PointDataMapper.GetAllVisiblePointsByCommonPointTypesByAIDAndEID(diagnosticsResult.AID, e.EID, (p => p.PointType));
                foreach (CW.Data.Point p in associatedEquipmentPoints)
                    chartRegion.lblAssociatedEquipmentPoints.Text += p.PointName + " (" + p.PointType.DisplayName + " on " + e.EquipmentName + "), ";
            }

            //show info
            chartRegion.analysisInfo.Visible = true;
            chartRegion.lblResults.Text = successfulAnalysis;
            chartRegion.lblResults.Visible = true;

            return true;
        }

        /// <summary>
        /// Plots the raw data points based on the charting selections, and points. SIMPLE CHART
        /// </summary>
        protected bool PlotSimpleRawDataPoints(ReportCriteria reportCriteria, ChartRegion chartRegion)
        {
            int counter = 1;
            int numOfEmptyPlots = 0;
            bool hasPlotData = false;
            int cid = (siteUser.IsKGSFullAdminOrHigher ? reportCriteria.clientEntity.ID : siteUser.CID);

            //clears
            chartRegion.rawChart.Series.Clear();

            //Used to be a foreach ListItem in lbPoints.Items with a query for each
            //Now it's 1 query using linq to sql where contains that translates to a aql query using in

            var pointSelections = new List<PointSelection>();

            foreach (ListItem point in reportCriteria.points)
            {
                pointSelections.Add(ReportCriteria.ParseListItemPointValue(point, reportCriteria.buildingName, reportCriteria.bid.Value));
            }

            chartRegion.hdnPIDs.Value = string.Join(",", pointSelections.AsEnumerable());

            //continue if p one or more points were selected
            if (pointSelections.Any())
            {
                chartRegion.reportInfo.Visible = true;

                var result = DataMgr.RawDataMapper.GetConvertedRawPlotDataForPoints(pointSelections.Select(ps => ps.PID).ToList(), reportCriteria.startDate, reportCriteria.endDate);

                if (result.Any())
                {
                    List<string> engineeringUnits = new List<string>();

                    //determine how many different engineering units for all points
                    //foreach selected pid
                    foreach (var pointSelection in pointSelections)
                    {
                        //get raw data
                        var rawData = result.Where(p => p.PID == pointSelection.PID).ToArray();

                        //get eng unit from first value
                        string engUnit = (rawData.Any() ? rawData.First().BuildingSettingBasedEngUnits : String.Empty);

                        if (!String.IsNullOrEmpty(engUnit) && !engineeringUnits.Contains(engUnit)) engineeringUnits.Add(engUnit);
                    }

                    //set x axis
                    //set axis intervaltype
                    chartRegion.rawChart.ChartAreas["rawChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;
                    chartRegion.rawChart.ChartAreas["rawChartArea"].AxisX.LabelStyle.Format = "g";
                    chartRegion.rawChart.ChartAreas["rawChartArea"].AxisX.Title = "Date and Time";

                    //establish primary and secondary y axis
                    if (engineeringUnits.Count() == 2)
                    {
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.Enabled = AxisEnabled.True;
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.LabelStyle.Enabled = true;

                        //set titles with engineering units for both y axis
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY.Title = "Engineering Units (" + engineeringUnits[0] + ")";
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.Title = "Engineering Units (" + engineeringUnits[1] + ")";
                    }
                    else
                    {
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.Enabled = AxisEnabled.False;
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.LabelStyle.Enabled = false;

                        //reset back if was changed in a previous plot with secondary axis
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY.Title = "Engineering Units";
                    }

                    //foreach selected pid
                    foreach (var pointSelection in pointSelections)
                    {
                        //get raw data
                        var rawData = result.Where(p => p.PID == pointSelection.PID).ToArray();

                        //get point name from first value

                        var filterListItem = reportCriteria.points.SingleOrDefault(p => ReportCriteria.ParseListItemPID(p) == pointSelection.PID && ReportCriteria.ParseListItemEID(p) == pointSelection.EID);

                        var pointName = "";

                        if (filterListItem != null)
                            pointName = filterListItem.Text;

                        //get eng unit from first value
                        var engUnit = (rawData.Any() ? rawData.First().BuildingSettingBasedEngUnits : String.Empty);

                        string longName = pointName + " (" + engUnit + ")";
                        string shortName = pointName;

                        //set fast chart type if possible
                        SeriesChartType type = EnumHelper.Parse<SeriesChartType>(reportCriteria.chartType);
                        type = (type == SeriesChartType.Line ? SeriesChartType.FastLine : (type == SeriesChartType.Point ? SeriesChartType.Point : SeriesChartType.FastPoint));

                        //create new series and set chart type
                        var series =
                        new System.Web.UI.DataVisualization.Charting.Series
                        {
                            ChartType = type,
                            BorderWidth = 2,
                                //set series name as point name with engineering unit
                                Name = longName,
                            ToolTip = "X: #VALX{g}, Y: #VALY",
                            LegendToolTip = longName,
                            LegendText = longName,
                        };

                        //set to plot on primary or secondary y axis
                        if (engineeringUnits.Count() == 2 && engineeringUnits[1].ToString() == engUnit)
                        {
                            series.YAxisType = AxisType.Secondary;
                        }
                        else
                        {
                            series.YAxisType = AxisType.Primary;
                        }

                        //add new rawData to series
                        series.Points.DataBindXY(rawData, "DateLoggedLocal", rawData, "ConvertedRawValue");

                        //TODO: on front end after microsoft bug fix.
                        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                        //add series to chart
                        chartRegion.rawChart.Series.Add(series);


                        if (rawData.Any())
                        {
                            hasPlotData = true;
                        }
                        else
                        {
                            numOfEmptyPlots++;
                        }

                        if (counter == 1)
                        {
                            chartRegion.lblRawPoints.Text = pointName;
                        }
                        else
                        {
                            chartRegion.lblRawPoints.Text += ", " + pointName;
                        }

                        counter++;
                    }
                }

                if (hasPlotData)
                {
                    string equipmentString = String.Empty;
                    int counter2 = 1;
                    foreach (var equipment in reportCriteria.equipment)
                    {
                        equipmentString += counter2 == 1 ? equipment.Text : ", " + equipment.Text;
                        counter2++;
                    }

                    //set chart title
                    chartRegion.rawChart.Titles["rawTitle"].Text = "Plot points for " + equipmentString + ".";

                    //set 3d, and point depth if 3d selected
                    if (reportCriteria.In3DMode)
                    {
                        chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.Enable3D = true;

                        //set quick point depth based on datetime range
                        var tspanTwoMin = new TimeSpan(0, 2, 0);
                        var tspanFourMin = new TimeSpan(0, 4, 0);
                        var tspanSixMin = new TimeSpan(0, 6, 0);
                        var tspan = reportCriteria.startDate.Subtract(reportCriteria.endDate);

                        if (tspan <= tspanTwoMin)
                        {
                            chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.PointDepth = 45;
                        }
                        else if (tspan <= tspanFourMin)
                        {
                            chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.PointDepth = 600;
                        }
                        else if (tspan <= tspanSixMin)
                        {
                            chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.PointDepth = 900;
                        }
                        else
                        {
                            chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.PointDepth = 1000;
                        }
                    }

                    //set results
                    if (numOfEmptyPlots > 0)
                    {
                        chartRegion.lblResults.Text = emptyPlots;
                        chartRegion.lblResults.Visible = true;
                    }
                    else
                    {
                        chartRegion.lblResults.Text = successfulPlots;
                        chartRegion.lblResults.Visible = true;
                    }

                    //show client details
                    chartRegion.lblRawClientName.Text = reportCriteria.clientEntity.name;
                    chartRegion.hdnRawCID.Value = cid.ToString();

                    //show divPoints
                    SetDivPointsOrCalculatedPoints(chartRegion, true);

                    chartRegion.lblRawBuildingName.Text = reportCriteria.buildingName;
                    chartRegion.lblRawEquipmentName.Text = equipmentString;
                    chartRegion.hdnRawEID.Value = reportCriteria.eid.ToString();
                    chartRegion.lblRawStartDate.Text = reportCriteria.startDate.ToShortDateString();
                    chartRegion.lblRawEndDate.Text = reportCriteria.endDate.ToShortDateString();

                    chartRegion.rawInfo.Visible = true;

                    return true;
                }
            }

            chartRegion.lblResults.Text = noPlots;
            chartRegion.lblResults.Visible = true;

            chartRegion.rawInfo.Visible = false;

            return false;
        }

        protected bool PlotSimpleVDataPoints(ReportCriteria reportCriteria, ChartRegion chartRegion)
        {
            int counter = 1;
            int numOfEmptyPlots = 0;
            bool hasPlotData = false;
            int cid = (siteUser.IsKGSFullAdminOrHigher ? reportCriteria.clientEntity.ID : siteUser.CID);

            chartRegion.rawChart.Series.Clear();

            var vpids = new List<Int32>();

            foreach (ListItem vpoint in reportCriteria.vpoints)
            {
                vpids.Add(Convert.ToInt32(vpoint.Value.Split(delimeter)[0]));
            }

            chartRegion.hdnVPIDs.Value = string.Join(",", vpids.AsEnumerable());

            //continue if p one or more points were selected
            if (vpids.Any())
            {
                chartRegion.reportInfo.Visible = true;

                var result = DataMgr.VAndVPreDataMapper.GetConvertedVDataPlotDataForVPIDs(vpids, reportCriteria.startDate, reportCriteria.endDate, AnalysisHelper.GetAnalysisRange(reportCriteria.displayRange.ToString()));

                if (result.Any())
                {
                    List<string> engineeringUnits = new List<string>();

                    //determine how many different engineering units for all vpoints
                    //foreach selected vpid
                    foreach (var vpid in vpids)
                    {
                        //get calculated data
                        var vData = result.Where(p => p.VPID == vpid).ToArray();

                        //get eng unit from first value
                        string engUnit = (vData.Any() ? vData.First().BuildingSettingBasedEngUnits : String.Empty);

                        if (!String.IsNullOrEmpty(engUnit) && !engineeringUnits.Contains(engUnit)) engineeringUnits.Add(engUnit);
                    }

                    //set x axis
                    //set axis intervaltype
                    chartRegion.rawChart.ChartAreas["rawChartArea"].AxisX.LabelStyle.IntervalType = DateTimeIntervalType.Days;
                    chartRegion.rawChart.ChartAreas["rawChartArea"].AxisX.LabelStyle.Format = "g";
                    chartRegion.rawChart.ChartAreas["rawChartArea"].AxisX.Title = "Day";

                    //establish primary and secondary y axis
                    if (engineeringUnits.Count() == 2)
                    {
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.Enabled = AxisEnabled.True;
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.LabelStyle.Enabled = true;

                        //set titles with engineering units for both y axis
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY.Title = "Values (" + engineeringUnits[0] + ")";
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.Title = "Values (" + engineeringUnits[1] + ")";
                    }
                    else
                    {
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.Enabled = AxisEnabled.False;
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.LabelStyle.Enabled = false;

                        //reset back if was changed in a previous plot with secondary axis
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY.Title = "Values";
                    }

                    //foreach selected vpid
                    foreach (var vpid in vpids)
                    {
                        //get calculated data
                        var vData = result.Where(p => p.VPID == vpid).ToArray();

                        //get point type name from first value
                        var pointTypeName = reportCriteria.vpoints.Where(vp => vp.Value.Split(delimeter)[0] == vpid.ToString()).First().Text;

                        //get eng unit from first value
                        var engUnit = (vData.Any() ? vData.First().BuildingSettingBasedEngUnits : String.Empty);

                        string longName = pointTypeName + " (" + engUnit + ")";
                        string shortName = pointTypeName;

                        //set fast chart type if possible
                        SeriesChartType type = EnumHelper.Parse<SeriesChartType>(reportCriteria.chartType);
                        type = (type == SeriesChartType.Line ? SeriesChartType.FastLine : (type == SeriesChartType.Point ? SeriesChartType.Point : SeriesChartType.FastPoint));

                        //create new series and set chart type
                        var series =
                        new System.Web.UI.DataVisualization.Charting.Series
                        {
                            ChartType = EnumHelper.Parse<SeriesChartType>(reportCriteria.chartType),
                            BorderWidth = 2,
                                //set series name as point type name with engineering unit
                                Name = longName,
                            ToolTip = "X: #VALX{g}, Y: #VALY",
                            LegendToolTip = longName,
                            LegendText = longName,
                        };

                        //set to plot on primary or secondary y axis
                        if (engineeringUnits.Count() == 2 && engineeringUnits[1].ToString() == engUnit)
                        {
                            series.YAxisType = AxisType.Secondary;
                        }
                        else
                        {
                            series.YAxisType = AxisType.Primary;
                        }

                        //add new rawData to series
                        series.Points.DataBindXY(vData, "StartDate", vData, "ConvertedRawValue");

                        //TODO: on front end after microsoft bug fix.
                        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisX.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);
                        chartRegion.rawChart.ChartAreas["rawChartArea"].AxisY2.TitleFont = new Font("Trebuchet MS", 9, FontStyle.Bold, GraphicsUnit.Point);

                        //add series to chart
                        chartRegion.rawChart.Series.Add(series);


                        if (vData.Any())
                        {
                            hasPlotData = true;
                        }
                        else
                        {
                            numOfEmptyPlots++;
                        }

                        if (counter == 1)
                        {
                            chartRegion.lblCalculatedPoints.Text = pointTypeName;
                        }
                        else
                        {
                            chartRegion.lblCalculatedPoints.Text += ", " + pointTypeName;
                        }

                        counter++;
                    }
                }

                if (hasPlotData)
                {
                    chartRegion.rawChart.Titles["rawTitle"].Text = "Plot points for " + reportCriteria.equipmentAnalysesList + ".";

                    //set 3d, and point depth if 3d selected
                    if (reportCriteria.In3DMode)
                    {
                        chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.Enable3D = true;

                        //set quick point depth based on datetime range
                        var tspanTwoMin = new TimeSpan(0, 2, 0);
                        var tspanFourMin = new TimeSpan(0, 4, 0);
                        var tspanSixMin = new TimeSpan(0, 6, 0);
                        var tspan = reportCriteria.startDate.Subtract(reportCriteria.endDate);

                        if (tspan <= tspanTwoMin)
                        {
                            chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.PointDepth = 45;
                        }
                        else if (tspan <= tspanFourMin)
                        {
                            chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.PointDepth = 600;
                        }
                        else if (tspan <= tspanSixMin)
                        {
                            chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.PointDepth = 900;
                        }
                        else
                        {
                            chartRegion.rawChart.ChartAreas["rawChartArea"].Area3DStyle.PointDepth = 1000;
                        }
                    }

                    //set results
                    if (numOfEmptyPlots > 0)
                    {
                        chartRegion.lblResults.Text = emptyPlots;
                        chartRegion.lblResults.Visible = true;
                    }
                    else
                    {
                        chartRegion.lblResults.Text = successfulPlots;
                        chartRegion.lblResults.Visible = true;
                    }

                    //show client details
                    chartRegion.lblRawClientName.Text = reportCriteria.clientEntity.name;
                    chartRegion.hdnRawCID.Value = cid.ToString();

                    //show divCalculatedPoints
                    SetDivPointsOrCalculatedPoints(chartRegion, false);

                    chartRegion.lblRawBuildingName.Text = reportCriteria.buildingName;
                    chartRegion.lblRawEquipmentName.Text = (reportCriteria.reportType == ReportCriteria.ReportType.Calculated) ? String.Join(", ", reportCriteria.equipment.Select(_ => _.Text).ToList()) : reportCriteria.equipment.First().Text;
                    chartRegion.hdnRawEID.Value = reportCriteria.eid.ToString();
                    chartRegion.lblRawStartDate.Text = reportCriteria.startDate.ToShortDateString();
                    chartRegion.lblRawEndDate.Text = reportCriteria.endDate.ToShortDateString();

                    chartRegion.rawInfo.Visible = true;

                    return true;
                }
            }

            chartRegion.lblResults.Text = noPlots;
            chartRegion.lblResults.Visible = true;

            chartRegion.rawInfo.Visible = false;

            return false;
        }

        /// <summary>
        /// Plots the raw data points based on the charting selections, and points. ADVANCED CHART
        /// </summary>
        protected bool PlotAdvancedRawDataPoints(ReportCriteria reportCriteria, ChartRegion chartRegion)
        {
            int counter = 1;
            int numOfEmptyPlots = 0;
            bool hasPlotData = false;
            int cid = (siteUser.IsKGSFullAdminOrHigher ? reportCriteria.clientEntity.ID : siteUser.CID);

            //clears
            chartRegion.radChart.Series.Clear();

            //Used to be a foreach ListItem in lbPoints.Items with a query for each
            //Now it's 1 query using linq to sql where contains that translates to a sql query using in

            var pointSelections = new List<PointSelection>();

            foreach (ListItem point in reportCriteria.points)
            {
                pointSelections.Add(ReportCriteria.ParseListItemPointValue(point, reportCriteria.buildingName, reportCriteria.bid.Value));
            }

            chartRegion.hdnPIDs.Value = string.Join(",", pointSelections.AsEnumerable());

            //continue if p one or more points were selected
            if (pointSelections.Any())
            {
                chartRegion.reportInfo.Visible = true;

                SetSeriesColors(chartRegion.radChart);

                var result = DataMgr.RawDataMapper.GetConvertedRawPlotDataForPoints(pointSelections.Select(ps => ps.PID).ToList(), reportCriteria.startDate, reportCriteria.endDate);

                if (result.Any())
                {
                    //foreach selected pid
                    foreach (var pointSelection in pointSelections)
                    {
                        //get raw data
                        var rawData = result.Where(p => p.PID == pointSelection.PID).ToArray();

                        //get point name from first value
                        //var pointName = reportCriteria.points.FindByValue(pid.ToString()).Text;
                        var filterListItem = reportCriteria.points.SingleOrDefault(p => ReportCriteria.ParseListItemPID(p) == pointSelection.PID && ReportCriteria.ParseListItemEID(p) == pointSelection.EID);

                        var pointName = "";

                        if (filterListItem != null)
                            pointName = filterListItem.Text;

                        //get eng unit from first value
                        var engUnit = (rawData.Any() ? rawData.First().BuildingSettingBasedEngUnits : String.Empty);

                        //create new series, set chart type and name
                        var chartSeries =
                        new ChartSeries
                        {
                            Type = EnumHelper.Parse<ChartSeriesType>(reportCriteria.chartType),
                            Name = pointName + " (" + engUnit + ")",
                        };

                        //set series name as point name

                        foreach (var r in rawData)
                        {
                            var item = new ChartSeriesItem(r.ConvertedDateLogged, r.ConvertedRawValue);

                            item.Label.Appearance.Visible = false;
                            chartSeries.AddItem(item);
                        }

                        chartSeries.DataXColumn = "ConvertedDateLogged";
                        chartSeries.DataYColumn = "ConvertedRawValue";

                        chartRegion.radChart.Series.Add(chartSeries);

                        chartSeries.PlotArea.XAxis.Visible = Telerik.Charting.Styles.ChartAxisVisibility.True;
                        chartSeries.PlotArea.YAxis.Visible = Telerik.Charting.Styles.ChartAxisVisibility.True;
                        chartSeries.PlotArea.XAxis.IsZeroBased = false;
                        chartSeries.PlotArea.YAxis.IsZeroBased = false;

                        chartSeries.PlotArea.XAxis.Appearance.ValueFormat = Telerik.Charting.Styles.ChartValueFormat.LongDate;
                        chartSeries.PlotArea.XAxis.Appearance.CustomFormat = "g";
                        //chartSeries.PlotArea.XAxis.Step = .25;

                        chartSeries.Appearance.FillStyle.FillType = Telerik.Charting.Styles.FillType.Solid;
                        //chartSeries.PlotArea.YAxis.Appearance.ValueFormat = Telerik.Charting.Styles.ChartValueFormat.Number;
                        //chartSeries.Appearance.ShowLabels = false;

                        //TODO: on front end after microsoft bug fix.
                        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.

                        if (rawData.Any())
                        {
                            hasPlotData = true;
                        }
                        else
                        {
                            numOfEmptyPlots++;
                        }

                        if (counter == 1)
                        {
                            chartRegion.lblRawPoints.Text = pointName;
                        }
                        else
                        {
                            chartRegion.lblRawPoints.Text += ", " + pointName;
                        }

                        counter++;
                    }
                }

                if (hasPlotData)
                {
                    string equipmentString = String.Empty;
                    int counter2 = 1;
                    foreach (var equipment in reportCriteria.equipment)
                    {
                        equipmentString += counter2 == 1 ? equipment.Text : ", " + equipment.Text;
                        counter2++;
                    }

                    //set chart title
                    chartRegion.radChart.ChartTitle.TextBlock.Text = "Plot points for " + equipmentString + ".";
                    //set 3d, and point depth if 3d selected


                    //set results
                    if (numOfEmptyPlots > 0)
                    {
                        chartRegion.lblResults.Text = emptyPlots;
                        chartRegion.lblResults.Visible = true;
                    }
                    else
                    {
                        chartRegion.lblResults.Text = successfulPlots;
                        chartRegion.lblResults.Visible = true;
                    }

                    //show client details
                    chartRegion.lblRawClientName.Text = reportCriteria.clientEntity.name;
                    chartRegion.hdnRawCID.Value = cid.ToString();

                    //show divPoints
                    SetDivPointsOrCalculatedPoints(chartRegion, true);

                    chartRegion.lblRawBuildingName.Text = reportCriteria.buildingName;
                    chartRegion.lblRawEquipmentName.Text = equipmentString;
                    chartRegion.hdnRawEID.Value = reportCriteria.eid.ToString();
                    chartRegion.lblRawStartDate.Text = reportCriteria.startDate.ToShortDateString();
                    chartRegion.lblRawEndDate.Text = reportCriteria.endDate.ToShortDateString();

                    chartRegion.rawInfo.Visible = true;

                    return true;
                }
            }

            chartRegion.lblResults.Text = noPlots;
            chartRegion.lblResults.Visible = true;

            chartRegion.rawInfo.Visible = false;

            return false;
        }

        protected bool PlotAdvancedVDataPoints(ReportCriteria reportCriteria, ChartRegion chartRegion)
        {
            int counter = 1;
            int numOfEmptyPlots = 0;
            bool hasPlotData = false;
            int cid = (siteUser.IsKGSFullAdminOrHigher ? reportCriteria.clientEntity.ID : siteUser.CID);

            //clears
            chartRegion.radChart.Series.Clear();

            //Used to be a foreach ListItem in lbPoints.Items with a query for each
            //Now it's 1 query using linq to sql where contains that translates to a sql query using in

            var vpids = new List<int>();

            foreach (ListItem vpoint in reportCriteria.vpoints)
            {
                vpids.Add(Convert.ToInt32(vpoint.Value));
            }

            chartRegion.hdnVPIDs.Value = string.Join(",", vpids.AsEnumerable());

            //continue if p one or more points were selected
            if (vpids.Any())
            {
                chartRegion.reportInfo.Visible = true;

                var result = DataMgr.VAndVPreDataMapper.GetConvertedVDataPlotDataForVPIDs(vpids, reportCriteria.startDate, reportCriteria.endDate,
                                    AnalysisHelper.GetAnalysisRange(reportCriteria.displayRange.ToString()));

                if (result.Any())
                {
                    //foreach selected pid
                    foreach (int vpid in vpids)
                    {
                        //get raw data
                        var vData = result.Where(p => p.VPID == vpid).ToArray();

                        //get point type name from first value
                        var pointTypeName = reportCriteria.vpoints.Where(vp => ReportCriteria.ParsePIDFromListItemValue(vp.Value) == vpid).First().Text;

                        //create new series, set chart type and name
                        var chartSeries =
                        new ChartSeries
                        {
                            Type = EnumHelper.Parse<ChartSeriesType>(reportCriteria.chartType),
                            Name = pointTypeName
                        };

                        //set series name as point name

                        foreach (var d in vData)
                        {
                            var item = new ChartSeriesItem(d.ConvertedStartDate, d.ConvertedRawValue);

                            item.Label.Appearance.Visible = false;
                            chartSeries.AddItem(item);
                        }

                        chartSeries.DataXColumn = "ConvertedStartDate";
                        chartSeries.DataYColumn = "ConvertedRawValue";

                        chartRegion.radChart.Series.Add(chartSeries);

                        chartSeries.PlotArea.XAxis.Visible = Telerik.Charting.Styles.ChartAxisVisibility.True;
                        chartSeries.PlotArea.YAxis.Visible = Telerik.Charting.Styles.ChartAxisVisibility.True;
                        chartSeries.PlotArea.XAxis.IsZeroBased = false;
                        chartSeries.PlotArea.YAxis.IsZeroBased = false;

                        //set X and Y Axis labels
                        chartSeries.PlotArea.XAxis.AxisLabel.TextBlock.Text = "Day";
                        chartSeries.PlotArea.YAxis.AxisLabel.TextBlock.Text = "Values";

                        chartSeries.PlotArea.XAxis.Appearance.ValueFormat = Telerik.Charting.Styles.ChartValueFormat.LongDate;
                        chartSeries.PlotArea.XAxis.Appearance.CustomFormat = "g";
                        //chartSeries.PlotArea.XAxis.Step = .25;


                        //chartSeries.PlotArea.YAxis.Appearance.ValueFormat = Telerik.Charting.Styles.ChartValueFormat.Number;
                        //chartSeries.Appearance.ShowLabels = false;

                        //TODO: on front end after microsoft bug fix.
                        //Workaround, cannot adjust title on front end, has to be done after bind in code behind.

                        if (vData.Any())
                        {
                            hasPlotData = true;
                        }
                        else
                        {
                            numOfEmptyPlots++;
                        }

                        if (counter == 1)
                        {
                            chartRegion.lblCalculatedPoints.Text = pointTypeName;
                        }
                        else
                        {
                            chartRegion.lblCalculatedPoints.Text += ", " + pointTypeName;
                        }

                        counter++;
                    }
                }

                if (hasPlotData)
                {
                    //set chart title
                    chartRegion.radChart.ChartTitle.TextBlock.Text = "Plot points for " + reportCriteria.equipment.First().Text + " and " + reportCriteria.analysisName + ".";
                    //set 3d, and point depth if 3d selected


                    //set results
                    if (numOfEmptyPlots > 0)
                    {
                        chartRegion.lblResults.Text = emptyPlots;
                        chartRegion.lblResults.Visible = true;
                    }
                    else
                    {
                        chartRegion.lblResults.Text = successfulPlots;
                        chartRegion.lblResults.Visible = true;
                    }

                    //show client details
                    chartRegion.lblRawClientName.Text = reportCriteria.clientEntity.name;
                    chartRegion.hdnRawCID.Value = cid.ToString();

                    //show divCalculatedPoints
                    SetDivPointsOrCalculatedPoints(chartRegion, false);

                    chartRegion.lblRawBuildingName.Text = reportCriteria.buildingName;
                    chartRegion.lblRawEquipmentName.Text = reportCriteria.equipment.First().Text;
                    chartRegion.hdnRawEID.Value = reportCriteria.eid.ToString();
                    chartRegion.lblRawStartDate.Text = reportCriteria.startDate.ToShortDateString();
                    chartRegion.lblRawEndDate.Text = reportCriteria.endDate.ToShortDateString();

                    chartRegion.rawInfo.Visible = true;

                    return true;
                }
            }

            chartRegion.lblResults.Text = noPlots;
            chartRegion.lblResults.Visible = true;

            chartRegion.rawInfo.Visible = false;

            return false;
        }

        private void ProcessRequestWithQueryString()
        {
            var qs = queryString.ToObject<ReportsParamsObject>();

            if (qs == null) return;

            if (qs.cid != siteUser.CID) return;

            reportCriteriaLeft.reportType = qs.reportType;
            reportCriteriaLeft.bid = qs.bid;
            reportCriteriaLeft.equipmentClassID = qs.ecid;
            reportCriteriaLeft.eid = qs.eid;

            switch (qs.reportType)
            {
                case ReportCriteria.ReportType.Raw:
                    reportCriteriaLeft.startDate = qs.sd;
                    reportCriteriaLeft.endDate = qs.ed;
                    break;
                case ReportCriteria.ReportType.Analyzed:
                    reportCriteriaLeft.aid = qs.aid;
                    reportCriteriaLeft.displayRange = qs.rng;
                    reportCriteriaLeft.analysisDate = qs.ad;
                    break;
                case ReportCriteria.ReportType.Calculated:
                    reportCriteriaLeft.aid = qs.aid;
                    reportCriteriaLeft.displayRange = qs.rng;
                    reportCriteriaLeft.startDate = qs.sd;
                    reportCriteriaLeft.endDate = qs.ed;
                    break;
            }
        }

        private void SetDivPointsOrCalculatedPoints(ChartRegion chartRegion, bool isPoints)
        {
            chartRegion.divPoints.Visible = isPoints;
            chartRegion.divCalculatedPoints.Visible = !isPoints;
        }

        private void SetSeriesColors(RadChart chart)
        {
            Color[] seriesColors = new[]
                                   {
                                           Color.DarkBlue,
                                           Color.DarkRed,
                                           Color.DarkGreen,
                                           Color.DarkGray,
                                           Color.DarkViolet,
                                           Color.DarkOrange,
                                           Color.DarkTurquoise,
                                           Color.DarkGoldenrod,
                                           Color.DarkMagenta,
                                           Color.DarkCyan,
                                           Color.DarkSalmon,
                                           Color.DarkKhaki,
                                           Color.DarkOliveGreen,
                                           Color.DarkOrchid,
                                           Color.DarkSeaGreen,
                                           Color.DarkSlateBlue,
                                           Color.DarkSlateGray,
                                           //
                                           Color.Blue,
                                           Color.Red,
                                           Color.BlueViolet,
                                           Color.Brown,
                                           Color.BurlyWood,
                                           Color.CadetBlue,
                                           Color.Chartreuse,
                                           Color.Chocolate,
                                           Color.CornflowerBlue,
                                           Color.Crimson,
                                           Color.Cyan,
                                           Color.DarkBlue,
                                           Color.DarkCyan,
                                           Color.DarkGoldenrod,
                                           Color.DarkGray,
                                           Color.DarkGreen,
                                           Color.DarkKhaki,
                                           Color.DarkMagenta,
                                           Color.DarkOliveGreen,
                                           Color.DarkOrange,
                                           Color.DarkOrchid,
                                           Color.DarkRed,
                                           Color.DarkSalmon,
                                           Color.DarkSeaGreen,
                                           Color.DarkSlateBlue,
                                           Color.DarkSlateGray,
                                           Color.DarkTurquoise,
                                           Color.DarkViolet,
                                           Color.DeepPink,
                                           Color.DeepSkyBlue,
                                           Color.DodgerBlue,
                                           Color.Firebrick,
                                           Color.ForestGreen,
                                           Color.Fuchsia,
                                           Color.Gold,
                                           Color.Goldenrod,
                                           Color.Gray,
                                           Color.Green,
                                           Color.GreenYellow,
                                           Color.HotPink,
                                           Color.IndianRed,
                                           Color.Indigo,
                                           Color.LimeGreen,
                                           Color.Magenta,
                                           Color.Maroon,
                                           Color.Navy,
                                           Color.Olive,
                                           Color.Orange,
                                           Color.OrangeRed,
                                           Color.Orchid,
                                           Color.Pink,
                                           Color.Plum,
                                           Color.RoyalBlue,
                                           Color.SaddleBrown,
                                           Color.Salmon,
                                           Color.Sienna,
                                           Color.SlateGray,
                                           Color.Tan,
                                           Color.Teal,
                                           Color.Turquoise,
                                           Color.Violet,
                                           Color.Yellow,
                                           Color.YellowGreen
                                       };

            Palette seriesPalette = new Palette("seriesPalette", seriesColors, true);
            chart.CustomPalettes.Add(seriesPalette);
            chart.SeriesPalette = "seriesPalette";
        }

        #endregion
    }
}