﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI;

namespace CW.Website
{
    public partial class Error: Page
    {
        private static readonly String[] error404;
        private static readonly String[] error500;
        private static readonly string errorFormat = "{0}<br /><br />{1}";
        private static readonly string operationIdMessageFormat = "Operation Id: {0}<br /><br />Please click Send Error Report below to notify support.<br /><br />";

        static Error()
        {
            error404 =
            new[]
            {
                "You are in the wrong location, but our gears are still cranking away.",
                "You are in the wrong location, but your buildings haven't moved.",
                "You are in the wrong location, but your data is still crunching.",
            };

            error500 =
            new[]
            {
                "Greasing up the gears to improve your experience next time.",
                "Tightening up some bolts to improve your experience next time.",
            };
        }

        protected void Page_Load(Object sender, EventArgs e)
        {
            //Random.Next(0,2) will be either 0 or 1
            var random = new Random();            
            var ex = Server.GetLastError();

            if (ex == null)
            {
                //
                //error handled by IIS
                headerErrorPage.InnerText = headerErrorIframe.InnerText = error404[random.Next(0, (error404.Length))];

                return;
            }

            //
            //error handled by ASP.NET
            var guid = string.Empty;

            if (HttpContext.Current.Session["LastErrorOperationId"] != null)
            {
                guid = HttpContext.Current.Session["LastErrorOperationId"].ToString();

                HttpContext.Current.Session["LastErrorOperationId"] = string.Empty;
            }

            hdnEmail.Value = ConfigurationManager.AppSettings["Mail.DefaultSupportAddress"];
            hdnBody.Value = string.Format("Enter steps to reproduce:\r\n\r\nDateTime (UTC): {0}\r\n\r\nOperation Id: {1}", DateTime.UtcNow, guid);
            
            var request = HttpContext.Current.Request;
            var userAgent = request.UserAgent;
            var browser = request.Browser;
            var browserMsgFormat = "{0}\r\n\r\nBrowser: {1}\r\n\r\nBrowser Version: {2}";
            var edge = "edge";
            var isEdgeBrowser = userAgent.ToLower().Contains(edge);

            if (isEdgeBrowser) //edge case (no pun intended) for the MS Edge browser
            {
                var startIndex = userAgent.ToLower().IndexOf(edge);
                var edgeSubstring = userAgent.Substring(startIndex, userAgent.Length - startIndex);
                var edgeSplit = edgeSubstring.Split('/');

                hdnBody.Value = string.Format(browserMsgFormat, hdnBody.Value, edgeSplit[0], edgeSplit[1]);
            }
            else if (!String.IsNullOrWhiteSpace(browser.Browser)) hdnBody.Value = string.Format(browserMsgFormat, hdnBody.Value, browser.Browser, browser.Version);

            if ((ex is HttpException) && ex.Message.Contains("does not exist"))
            {
                SetMessage(error404[random.Next(0, (error404.Length))], guid);
                
                return;
            }

            SetMessage(error500[random.Next(0, (error500.Length))], guid);

            return;
        }

        private void SetMessage(string error, string guid)
        {
            var operationIdMsg = string.Format(operationIdMessageFormat, guid);

            headerErrorPage.InnerHtml = error;
            litOperationIdMessage.Text = operationIdMsg;

            headerErrorIframe.InnerHtml = string.Format(errorFormat, error, operationIdMsg);
        }
    }
}