﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Helpers;
using CW.Data.Models.ServiceKey;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class SystemServiceKeyAdministration : SitePage
    {
        #region Properties

            private ServiceKey mServiceKey;           
            const string serviceKeyExists = "Service key already exists.";
            const string addServiceKeySuccess = " service key addition was successful.";
            const string addServiceKeyFailed = "Adding service key failed. Please contact an administrator.";
            const string addingAuditForServiceKeyFailed = " Error adding audit for servicekey.";                 
            const string updateSuccessful = " service key update was successful.";
            const string updateFailed = "Service key update failed. Please contact an administrator.";
            const string deleteSuccessful = "Service key deletion was successful.";
            const string deleteFailed = "Service key deletion failed. Please contact an administrator.";
            const string regenerateSuccess = "Service key regeneration was successful.";
            const string regenerateFailed = "Service key regeneration failed. Please contact an administrator.";
            const string publicKeyRequired = "Public key file is required for data transfer service access.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "ServiceKeyName";
            const string invalidPublicKey = "Public key File is invalid";

            static CharHelper.CharSets StripCharSets = CharHelper.CharSets.Tab | CharHelper.CharSets.NewLine;

            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs system admin.
                if (!siteUser.IsKGSSystemAdmin)
                    Response.Redirect("/Home.aspx");   

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindServiceKeys();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindClients(ddlEditClient, ddlAddClient);

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetServiceKeyIntoEditForm(ServiceKey serviceKey)
            {
                //ID
                hdnEditID.Value = Convert.ToString(serviceKey.SKID);
                //Key
                hdnEditKey.Value = Convert.ToString(serviceKey.PortalKey);

                //Service Key Name
                txtEditServiceKeyName.Text = serviceKey.ServiceKeyName;
                //Service Key Description
                txtEditServiceKeyDescription.Value = serviceKey.ServiceKeyDescription;
                //User Name
                txtEditUserName.Text = serviceKey.PortalUserName;
                //Client
                ddlEditClient.SelectedValue = Convert.ToString(serviceKey.CID);
                //Portal Access
                chkEditPortalAccess.Checked = serviceKey.PortalAccess;
                //Service Access
                chkEditServiceAccess.Checked = serviceKey.ServiceAccess;
                //Data Transfer Service Access
                chkEditDataTransferServiceAccess.Checked = serviceKey.DataTransferServiceAccess;
                //Active
                chkEditActive.Checked = serviceKey.IsActive;

                //ResetPassword
                txtEditNewPassword.Text = "";


                SetEditPortalVisibility(String.IsNullOrEmpty(serviceKey.PortalPassword));                

                rauEditPublicKey.Enabled = serviceKey.DataTransferServiceAccess;
            }

        #endregion

        #region Load and Bind Fields


            /// <summary>
            /// Binds a dropdown list with all clients
            /// </summary>
            /// <param name="ddl"></param>
            private void BindClients(DropDownList ddl, DropDownList ddl2)
            {
                IList<Client> clients = DataMgr.ClientDataMapper.GetAllClients();

                ddl.DataTextField = "ClientName";
                ddl.DataValueField = "CID";
                ddl.DataSource = clients;
                ddl.DataBind();

                ddl2.DataTextField = "ClientName";
                ddl2.DataValueField = "CID";
                ddl2.DataSource = clients;
                ddl2.DataBind();
            }

        #endregion

        #region Load Service Key

            protected bool LoadAddFormIntoServiceKey(ServiceKey serviceKey)
            {
                //First Name
                serviceKey.ServiceKeyName = txtAddServiceKeyName.Text;

                //Service Key Description
                serviceKey.ServiceKeyDescription = txtAddServiceKeyDescription.Value;

                if (chkAddPortalAccess.Checked)
                {
                    //Portal User Name
                    serviceKey.PortalUserName = txtAddUserName.Text;

                    //insterting new portal user password encription
                    serviceKey.PortalPassword = Encryptor.getMd5Hash(txtAddInitialPassword.Text);    
                }

                //Client
                serviceKey.CID = Convert.ToInt32(ddlAddClient.SelectedValue);                
     
                serviceKey.PortalAccess = chkAddPortalAccess.Checked;
                serviceKey.ServiceAccess = chkAddServiceAccess.Checked;
                serviceKey.DataTransferServiceAccess = chkAddDataTransferServiceAccess.Checked;

                if (chkAddDataTransferServiceAccess.Checked )
                {
                    if (rauAddPublicKey.UploadedFiles.Count > 1)
                    {
                        throw new NotImplementedException("Public Key Upload contains more than one uploaded file, only one upload file is allowed");
                    }
                    else if (rauAddPublicKey.UploadedFiles.Count == 0)
                    {
                        LabelHelper.SetLabelMessage(lblAddError, publicKeyRequired, lnkSetFocusAdd);
                        return false;
                    }

                    X509Certificate2 cert = CryptographicHelper.GetCertficate(StreamHelper.ReadAll(rauAddPublicKey.UploadedFiles[0].InputStream));

                    if (cert == null)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, invalidPublicKey, lnkSetFocusView);
                        return false;
                    }

                    serviceKey.PublicKeyAsXml = Server.HtmlEncode(CryptographicHelper.GetCertificateAsXmlString(cert));
                }

                serviceKey.IsActive = true;
                serviceKey.DateModified = DateTime.UtcNow;

                return true;
            }

            protected bool LoadEditFormIntoServiceKey(ServiceKey serviceKey)
            {
                //ID
                serviceKey.SKID = Convert.ToInt32(hdnEditID.Value);

                //Service Key Name
                serviceKey.ServiceKeyName = txtEditServiceKeyName.Text;

                //Service Key Description
                serviceKey.ServiceKeyDescription = txtEditServiceKeyDescription.Value;

                //Client
                serviceKey.CID = Convert.ToInt32(ddlEditClient.SelectedValue);

                if (chkEditPortalAccess.Checked)
                {
                    //New Password
                    if(!String.IsNullOrEmpty(txtEditNewPassword.Text))
                        serviceKey.PortalPassword = Encryptor.getMd5Hash(txtEditNewPassword.Text);

                    //Portal User Name
                    serviceKey.PortalUserName = txtEditUserName.Text;

                    //Portal Key
                    serviceKey.PortalKey = new Guid(hdnEditKey.Value);
                }

                //PortalAccess
                serviceKey.PortalAccess = chkEditPortalAccess.Checked;
                //ServiceAccess
                serviceKey.ServiceAccess = chkEditServiceAccess.Checked;
                //DataTransferServiceAccess
                serviceKey.DataTransferServiceAccess = chkEditDataTransferServiceAccess.Checked;

                //Cert
                if (chkEditDataTransferServiceAccess.Checked)
                {
                    if (rauEditPublicKey.UploadedFiles.Count > 1)
                    {
                        throw new NotImplementedException("Public Key Upload contains more than one uploaded file, only one upload file is allowed");
                    }
                    else if (rauEditPublicKey.UploadedFiles.Count == 0)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, publicKeyRequired, lnkSetFocusView);
                        return false;
                    }

                    //Public Key
                    X509Certificate2 cert = CryptographicHelper.GetCertficate(StreamHelper.ReadAll(rauEditPublicKey.UploadedFiles[0].InputStream));

                    if (cert == null)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, invalidPublicKey, lnkSetFocusView);
                        return false;
                    }

                    serviceKey.PublicKeyAsXml = Server.HtmlEncode(CryptographicHelper.GetCertificateAsXmlString(cert));
                }

                //Active
                serviceKey.IsActive = chkEditActive.Checked;

                return true;
            }

        #endregion

        #region Button Events
       
            /// <summary>
            /// Add service key Button on click.
            /// </summary>
            protected void addServiceKeyButton_Click(object sender, EventArgs e)
            {
                //check that service key does not already exist
                if (!DataMgr.ServiceKeyDataMapper.DoesServiceKeyNameOrPortalUserNameExist(txtAddUserName.Text, txtAddServiceKeyName.Text))
                {
                    int newSKID = 0;
                    bool serviceKeyInsertSuccess = true, auditInsertSuccess = true;
                    mServiceKey = new ServiceKey();

                    //load the form into the service key
                    if (!LoadAddFormIntoServiceKey(mServiceKey))
                        return;

                    //try to insert the new service key
                    try
                    {
                        //insert new service key
                        newSKID = DataMgr.ServiceKeyDataMapper.InsertServiceKeyAndReturnSKID(mServiceKey);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding service key in kgs servicekey administration.", sqlEx);
                        serviceKeyInsertSuccess = false;
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding service key in kgs servicekey administration.", ex);
                        serviceKeyInsertSuccess = false;
                    }


                    //add serviceKeyAudit table entry
                    if (serviceKeyInsertSuccess)
                    {
                        //declare new serviceKeyAudit
                        ServiceKeysAudit mServiceKeyAudit = new ServiceKeysAudit();
                        mServiceKeyAudit.SKID = newSKID;
                        mServiceKeyAudit.DateModified = DateTime.UtcNow;

                        //try to insert the audit for servicekey
                        try
                        {
                            DataMgr.ServiceKeyDataMapper.InsertServiceKeyAudit(mServiceKeyAudit);
                        }
                        catch (SqlException sqlEx)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding serviceKeyAudit for servicekey in kgs servicekey administration.", sqlEx);
                            auditInsertSuccess = false;
                        }
                        catch (Exception ex)
                        {
                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding serviceKeyAudit for servicekey in kgs servicekey administration.", ex);
                            auditInsertSuccess = false;
                        }
                    }

                    //set appropriate message
                    if (serviceKeyInsertSuccess)
                    {
                        LabelHelper.SetLabelMessage(lblAddError, mServiceKey.ServiceKeyName + addServiceKeySuccess, lnkSetFocusAdd);

                        if (!auditInsertSuccess)
                        {
                            LabelHelper.SetLabelMessage(lblAddError, addingAuditForServiceKeyFailed, lnkSetFocusAdd, true);
                        }
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblAddError, addServiceKeyFailed, lnkSetFocusAdd);
                    }

                }
                else
                {
                    //user exists
                    LabelHelper.SetLabelMessage(lblAddError, serviceKeyExists, lnkSetFocusAdd);
                }
            }

            /// <summary>
            /// Update service key Button on click.
            /// </summary>
            protected void updateServiceKeyButton_Click(object sender, EventArgs e)
            {
                bool serviceKeyEditSuccess = true;

                //create and load the form into the service key
                ServiceKey mServiceKey = new ServiceKey();

                if (!LoadEditFormIntoServiceKey(mServiceKey))
                {
                    return;
                }

                //check if the service key exists for another user if trying to change
                if (DataMgr.ServiceKeyDataMapper.DoesServiceKeyAlreadyExist(mServiceKey.SKID, mServiceKey.PortalUserName, mServiceKey.ServiceKeyName))
                {
                    lblEditError.Text = serviceKeyExists;
                    lblEditError.Visible = true;
                    lnkSetFocusView.Focus();
                }
                //update the service key
                else
                {
                    try
                    {
                        //if new password was entered
                        if (!String.IsNullOrEmpty(txtEditNewPassword.Text))
                        {
                            DataMgr.ServiceKeyDataMapper.UpdateServiceKey(mServiceKey, true);
                        }
                        else
                        {
                            DataMgr.ServiceKeyDataMapper.UpdateServiceKey(mServiceKey, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error updating service key in kgs service key administration: SKID={0}, ServiceKeyName={1}.", mServiceKey.SKID, mServiceKey.ServiceKeyName), ex);
                        serviceKeyEditSuccess = false;
                    }


                    //set appropriate message
                    if (serviceKeyEditSuccess)
                    {
                        LabelHelper.SetLabelMessage(lblEditError, mServiceKey.ServiceKeyName + updateSuccessful, lnkSetFocusView);

                        //Bind service keys again
                        BindServiceKeys();
                    }
                    else
                    {
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkSetFocusView);
                    }
                }

            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindServiceKeys();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindServiceKeys();
            }

            /// <summary>
            /// Updates a portal key, basically regenerates a new key for the same record. button click event.
            /// </summary>
            /// <param name="skid"></param>
            protected void regeneratePortalKeyButton_Click(object sender, EventArgs e)
            {
                try
                {
                    ServiceKey newPortalKey = DataMgr.ServiceKeyDataMapper.RegeneratePortalKey(new Guid(hdnEditKey.Value));                   

                    //Bind service keys again because when we regenearte the service key we delete and add a new record thus 
                    //resulting in the change of the skid so we cannot edit that old record anymore
                    BindServiceKeys();

                    //also reassign the hidden skid and hidden servicekey
                    hdnEditID.Value = newPortalKey.SKID.ToString();
                    hdnEditKey.Value = newPortalKey.PortalKey.ToString();

                    LabelHelper.SetLabelMessage(lblEditError, regenerateSuccess, lnkSetFocusView);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error regenerating portal key.", sqlEx);
                    LabelHelper.SetLabelMessage(lblEditError, regenerateFailed, lnkSetFocusView);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error regenerating portal key.", ex);
                    LabelHelper.SetLabelMessage(lblEditError, regenerateFailed, lnkSetFocusView);
                }
            }

        #endregion

        #region Checkbox Events

            protected void addPortal_OnCheckChanged(object sender, EventArgs e)
            {
                addUserNameRequiredValidator.Enabled = chkAddPortalAccess.Checked;
                addInitialPasswordRegExValidator.Enabled = chkAddPortalAccess.Checked;
                addInitialPasswordRequiredValidator.Enabled = chkAddPortalAccess.Checked;
                addConfirmPasswordCompareValidator.Enabled = chkAddPortalAccess.Checked;
                addConfirmPasswordRegExValidator.Enabled = chkAddPortalAccess.Checked;
                addConfirmPasswordRequiredValidator.Enabled = chkAddPortalAccess.Checked;

                divAddPortal.Visible = chkAddPortalAccess.Checked;
            }

            protected void addDataTransfer_OnCheckChanged(object sender, EventArgs e)
            {
                rauAddPublicKey.Enabled = chkAddDataTransferServiceAccess.Checked;
            }

            protected void editPortal_OnCheckChanged(object sender, EventArgs e)
            {
                SetEditPortalVisibility(String.IsNullOrEmpty(DataMgr.ServiceKeyDataMapper.GetServiceKeyByID(Convert.ToInt32(hdnEditID.Value)).PortalPassword));
            }

            protected void editDataTransfer_OnCheckChanged(object sender, EventArgs e)
            {
                rauEditPublicKey.Enabled = chkEditDataTransferServiceAccess.Checked;
            }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind service keys grid after tab changed back from add service key tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindServiceKeys();
                }
            }

        #endregion

        #region Grid Events

            protected void gridServiceKeys_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditServiceKeys.Visible = false;
                dtvServiceKey.Visible = true;
                
                int skid = Convert.ToInt32(gridServiceKeys.DataKeys[gridServiceKeys.SelectedIndex].Values["SKID"]);

                //set data source
                dtvServiceKey.DataSource = DataMgr.ServiceKeyDataMapper.GetFullServiceKeyByID(skid);
                //bind service key to details view
                dtvServiceKey.DataBind();                                    
            }

            protected void gridServiceKeys_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridServiceKeys_Editing(object sender, GridViewEditEventArgs e)
            {    
                dtvServiceKey.Visible = false;
                pnlEditServiceKeys.Visible = true;

                int skid = Convert.ToInt32(gridServiceKeys.DataKeys[e.NewEditIndex].Values["SKID"]);

                ServiceKey mServiceKey = DataMgr.ServiceKeyDataMapper.GetServiceKeyByID(skid);

                SetServiceKeyIntoEditForm(mServiceKey);

                //Cancels the edit auto grid selected. Fixes the select link diasspearing in selected mode, and the edit link performing the delete method.
                e.Cancel = true;
            }

            protected void gridServiceKeys_Deleting(object sender, GridViewDeleteEventArgs e)
            {                
                //get deleting rows skid
                int skid = Convert.ToInt32(gridServiceKeys.DataKeys[e.RowIndex].Value);

                try
                {
                    //deletes servicekey audit
                    DataMgr.ServiceKeyDataMapper.DeleteServiceKeyAudit(skid);

                    //delete servicekey
                    DataMgr.ServiceKeyDataMapper.DeleteServiceKey(skid);

                    LabelHelper.SetLabelMessage(lblErrors, deleteSuccessful, lnkSetFocusView);          
                }
                catch (Exception ex)
                {
                    LabelHelper.SetLabelMessage(lblErrors, deleteFailed, lnkSetFocusView);      
                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error deleting service key in kgs service key administration: SKID={0}, ServiceKeyName={1}.", mServiceKey.SKID, mServiceKey.ServiceKeyName), ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryServiceKeys());
                gridServiceKeys.PageIndex = gridServiceKeys.PageIndex;
                gridServiceKeys.DataSource = SortDataTable(dataTable as DataTable, true);
                gridServiceKeys.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                pnlEditServiceKeys.Visible = false;
                dtvServiceKey.Visible = false;
            }

            protected void gridServiceKeys_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedServiceKeys(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridServiceKeys.DataSource = SortDataTable(dataTable, true);
                gridServiceKeys.PageIndex = e.NewPageIndex;
                gridServiceKeys.DataBind();
            }

            protected void gridServiceKeys_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridServiceKeys.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedServiceKeys(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridServiceKeys.DataSource = SortDataTable(dataTable, false);
                gridServiceKeys.DataBind();
            }
        
        #endregion

        #region Dropdown and Checkbox events

        #endregion

        #region Helper Methods

            private IEnumerable<GetServiceKeyData> QueryServiceKeys()
            {
                try
                {
                    //get all service keys 
                    return DataMgr.ServiceKeyDataMapper.GetAllServiceKeys();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving service keys in kgs service key administration.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving service keys in kgs service key administration.", ex);
                    return null;
                }
            }

            private IEnumerable<GetServiceKeyData> QuerySearchedServiceKeys(string searchText)
            {
                try
                {
                    //get all service keys 
                    return DataMgr.ServiceKeyDataMapper.GetAllSearchedServiceKeys(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving service keys in kgs service key administration.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving service keys in kgs service key administration.", ex);
                    return null;
                }
            }

            private void BindServiceKeys()
            {
                //query service keys
                IEnumerable<GetServiceKeyData> serviceKeys = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryServiceKeys() : QuerySearchedServiceKeys(txtSearch.Text);

                int count = serviceKeys.Count();

                //maintain sort--- doesnt work!
                //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
                //gridUsers.Sort(GridViewSortExpression, tempSD);

                gridServiceKeys.DataSource = serviceKeys;

                // bind grid
                gridServiceKeys.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedServiceKeys(string[] searchText)
            //{
            //    //query users
            //    IEnumerable<GetServiceKeyData> serviceKeys = QuerySearchedServiceKeys(searchText);

            //    int count = serviceKeys.Count();

            //    //maintain sort--- doesnt work!
            //    //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
            //    //gridUsers.Sort(GridViewSortExpression, tempSD);

            //    gridServiceKeys.DataSource = serviceKeys;

            //    // bind grid
            //    gridServiceKeys.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} service key found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} service keys found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No service keys found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }


            private void SetEditPortalVisibility(bool passwordRequired)
            {
                editNewPasswordRegExValidator.Enabled = chkEditPortalAccess.Checked;
                editUserNameRequiredValidator.Enabled = chkEditPortalAccess.Checked;

                //if no password exists yet
                editNewPasswordRequiredValidator.Enabled = (chkEditPortalAccess.Checked && passwordRequired) ? true : false;

                divEditPortal.Visible = chkEditPortalAccess.Checked;
            }

        #endregion
    }
}

