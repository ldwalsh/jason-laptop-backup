﻿using CW.Website._controls.admin;
using CW.Website._controls.admin.Buildings;
using System;

namespace CW.Website
{
    public partial class KGSBuildingAdministration : AdminSitePageTemp
    {
        #region events

            private void Page_FirstLoad()
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs full admin or higher.            
                AddBuilding.Visible = siteUser.IsKGSFullAdminOrHigher;
                TabStateMonitor.ChangeState(new Enum[] { KGSBuildingAdministration.TabMessages.Init });
            }

            private void Page_Init()
            {
                BuildingVariables.AdminMode = BuildingVariables.AdminModeEnum.KGS;
            }

        #endregion

        public enum TabMessages
        {
            Init,
            AddBuilding,
            EditBuilding,
            EditBuildingVariables,
            DeleteBuilding
        }

        #region properties

            #region overrides

                protected override String DefaultControl { get { return typeof(ViewBuildings).Name; } }

                public override AdminModeEnum AdminModeEnum { get { return AdminModeEnum.KGS; } }

            #endregion

        #endregion
    }
}