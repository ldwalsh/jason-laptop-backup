﻿<%@ Page Language="C#" MasterPageFile="~/_masters/Admin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="BuildingAdministration.aspx.cs" Inherits="CW.Website.BuildingAdministration" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register src="~/_controls/admin/Buildings/nonkgs/ViewBuildings.ascx" tagname="ViewBuildings" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Buildings/BuildingVariables.ascx" tagname="BuildingVariables" tagprefix="CW" %>
<%@ Register src="~/_controls/admin/Buildings/BuildingCMMSSettings.ascx" TagName="BuildingCMMSSettings" TagPrefix="CW"  %>
<%@ Register src="~/_administration/building/BuildingGoalsSettingsTab.ascx" tagPrefix="CW" tagName="BuildingGoalsSettingsTab" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

      <h1>Building Administration</h1>

      <div class="richText">
        <asp:Literal ID="litAdminBuildingsBody" runat="server" />
      </div>

      <div class="updateProgressDiv">
        <asp:UpdateProgress ID="updateProgressTop" runat="server">
          <ProgressTemplate>
            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
          </ProgressTemplate>
        </asp:UpdateProgress>
      </div>

      <div class="administrationControls">
        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" MultiPageID="radMultiPage">
          <Tabs>
            <telerik:RadTab Text="View Buildings" />
            <telerik:RadTab Text="Building Variables" />
            <telerik:RadTab Text="Building WO Settings" />
            <telerik:RadTab Text="Building Goal Settings" />
          </Tabs>
        </telerik:RadTabStrip>

        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">

          <telerik:RadPageView ID="RadPageView1" runat="server">
            <CW:ViewBuildings ID="ViewBuildings" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView2" runat="server">
            <CW:BuildingVariables ID="BuildingVariables" runat="server" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView6" runat="server">
            <CW:BuildingCMMSSettings ID="BuildingCMMSSettings" runat="server" IsReadOnly="True" />
          </telerik:RadPageView>

          <telerik:RadPageView ID="RadPageView7" runat="server">
            <CW:BuildingGoalsSettingsTab ID="BuildingGoalsSettingsTab" runat="server" />
          </telerik:RadPageView>

        </telerik:RadMultiPage>
      </div>

</asp:Content>