﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Collections.Generic;

using CW.Data;
using CW.Business;
using CW.Utility;
using CW.Common.Constants;
using CW.Common.Helpers;
using CW.Website._framework;

using Content = CW.Data.Content;
using System.Data.SqlClient;
using System.Globalization;
using CW.Logging;

namespace CW.Website
{
    public partial class CustomerValueReports : SitePage
    {
        #region Properties

        private IEnumerable<Content> mContent;

        #endregion

        #region Page Events

            private void Page_Init()
            {
                //logged in security check in master page.

                //GetAndSetReportContent(siteUser.CultureName);
            }

            private void Page_FirstLoad()
            {
                //datamanger in sitepage

                BindThemes(ddlTheme);

                BindBuildings(lbBuildings, siteUser.CID);

                BindCultures(ddlCulture);
                BindLanguageCultures(ddlLanguage);
                BindCountries(ddlCustomerCountryAlpha2Code);

                SetEnabledCheckboxes();

                SetHiddenFields();
            }

            protected void Page_Load(object sender, EventArgs e)
            {
                //if the page is not a postback
                if (!Page.IsPostBack)
                {
                }
            }

        #endregion

        #region Load and Bind Fields

            private void BindCountries(DropDownList ddl)
            {
                ddl.DataTextField = "CountryName";
                ddl.DataValueField = "Alpha2Code";
                ddl.DataSource = DataMgr.CountryDataMapper.GetAllCountries();
                ddl.DataBind();
            }

            private void BindStates(DropDownList ddl, string alpha2Code)
            {
                ListItem lItem = new ListItem();
                lItem.Text = "Select one...";
                lItem.Value = "-1";
                lItem.Selected = true;

                ddl.Items.Clear();
                ddl.Items.Add(lItem);
                ddl.DataTextField = "StateName";
                ddl.DataValueField = "StateID";
                ddl.DataSource = DataMgr.StateDataMapper.GetAllStatesByAlpha2Code(alpha2Code);
                ddl.DataBind();
            }

            private void BindBuildings(ListBox lb, int clientID)
            {
                //clear just in case
                lb.Items.Clear();

                lb.DataSource = siteUser.VisibleBuildings;
                lb.DataTextField = "BuildingName";
                lb.DataValueField = "BID";
                lb.DataBind();
            }

            private void BindThemes(DropDownList ddl)
            {
                ddl.Items.Clear();

                Dictionary<int, string> themes = Common.Constants.BusinessConstants.Theme.ReportThemes;

                if (!siteUser.IsKGSFullAdminOrHigher && !siteUser.IsSchneiderTheme)
                    themes = themes.Where(t => t.Key != 1).ToDictionary(t => t.Key, t => t.Value);
                else if (!siteUser.IsKGSFullAdminOrHigher && siteUser.IsSchneiderTheme)
                    themes = themes.Where(t => t.Key != 0).ToDictionary(t => t.Key, t => t.Value);

                ddl.DataValueField = "Key";
                ddl.DataTextField = "Value";
                ddl.DataSource = themes;
				ddl.DataBind();
            }

            internal void BindLanguageCultures(DropDownList ddl)
            {
                var cultureItems = CultureHelper.AllowedContentCultures(CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.DisplayName).ToArray());

                ddl.DataTextField = "DisplayName";
                ddl.DataValueField = "Name";
                ddl.DataSource = cultureItems.Select(c => new { DisplayName = c.DisplayName, Name = c.Name });

                if(cultureItems.Contains(CultureHelper.GetCultureInfo(siteUser.CultureName)))
                {                   
                    ddl.SelectedValue = siteUser.CultureName;
                }
                else
                {
                    ddl.SelectedValue = Common.Constants.BusinessConstants.Culture.DefaultCultureName;
                }

                ddl.DataBind();
            }

            private void BindCultures(DropDownList ddl)
            {
                ddl.DataTextField = "DisplayName";
                ddl.DataValueField = "LCID";
                ddl.DataSource = CultureHelper.FilterUserCultures(CultureInfo.GetCultures(CultureTypes.SpecificCultures).OrderBy(c => c.DisplayName).ToArray());
                ddl.SelectedValue = CultureHelper.GetCultureInfo(siteUser.CultureName).LCID.ToString();
                ddl.DataBind();
            }

        #endregion

        #region Dropdown and Checkbox Events

            protected void chkManualCustomerDetailsEntry_OnCheckedChanged(object sender, EventArgs e)
            {
                divCustomerDetails.Visible = chkManualCustomerDetailsEntry.Checked;
            }

            protected void ddlCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlCustomerStateID, ddlCustomerCountryAlpha2Code.SelectedValue);

                customerZipRequiredValidator.Enabled = customerZipRegExValidator.Enabled = ddlCustomerCountryAlpha2Code.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtCustomerZip.Mask = ddlCustomerCountryAlpha2Code.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
                customerStateRequiredValidator.Enabled = ddlCustomerStateID.Items.Count > 1;
            }

            protected void chkIncludeKeyIndicatorsReport_OnCheckedChanged(object sender, EventArgs e)
            {
                divKeyIndicators.Visible = chkIncludeKeyIndicatorsReport.Checked;
            }
            protected void chkManualKeyIndicatorsEntry_OnCheckedChanged(object sender, EventArgs e)
            {
                divManualKeyIndicators.Visible = chkManualKeyIndicatorsEntry.Checked;
            }

            protected void chkIncludeItemsRequiringAttention_OnCheckedChanged(object sender, EventArgs e)
            {
                divItemsRequiringAttention.Visible = chkIncludeItemsRequiringAttention.Checked;
            }


            protected void chkIncludePerformanceCharts_OnCheckedChanged(object sender, EventArgs e)
            {
                divPerformanceCharts.Visible = chkIncludePerformanceCharts.Checked;
            }
            protected void chkManualPerformanceChartsEntry_OnCheckedChanged(object sender, EventArgs e)
            {
                divManualPerformanceCharts.Visible = chkManualPerformanceChartsEntry.Checked;
            }

            protected void chkIncludeServiceDetails_OnCheckedChanged(object sender, EventArgs e)
            {
                divServiceDetails.Visible = chkIncludeServiceDetails.Checked;
            }

            protected void chkIncludeMetrics_OnCheckedChanged(object sender, EventArgs e)
            {
                divMetrics.Visible = chkIncludeMetrics.Checked;
            }

            protected void chkIncludeSiteVisits_OnCheckedChanged(object sender, EventArgs e)
            {
                divSiteVisits.Visible = chkIncludeSiteVisits.Checked;
            }

        #endregion

        #region Helper Methods

            private void SetEnabledCheckboxes()
            {
                if(!siteUser.Modules.Any(m => m.ModuleID == Convert.ToInt32(BusinessConstants.Module.Modules.Diagnostics)))
                {
                    chkManualKeyIndicatorsEntry.Checked = true;
                    chkManualKeyIndicatorsEntry.Enabled = false;
                    chkManualKeyIndicatorsEntry_OnCheckedChanged(null, null);
                }
            }

            //private void GetAndSetReportContent(string cultureName)
            //{
            //    //get content            
            //    //mContent = DataMgr.ContentDataMapper.GetSafeContentSections(new int[] { 2 }, cultureName);

            //    //editorAutomatedSummary.Content = new ContentHelper(LogMgr).GetSafeContentItem(mContent, "AutomatedSummaryDefaultValue", this.GetType().Name);
            //}

            private void SetHiddenFields()
            {
                hdnUID.Value = siteUser.UID.ToString();
                hdnCID.Value = siteUser.CID.ToString();
                hdnOID.Value = siteUser.UserOID.ToString();
                hdnUserCulture.Value = siteUser.CultureName;
            }


         #endregion
    }
}
